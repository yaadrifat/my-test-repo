-- INSERTING DATA INTO CB_QUESTION TABLE FOR IDM FORMS --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_question=(select pk_questions from cb_questions where ques_code='cms_cert_lab_ind') 
    and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	update cb_form_questions set UNEXPECTED_RESPONSE_VALUE='idm_unres1' where fk_question=(select pk_questions from cb_questions where ques_code='cms_cert_lab_ind') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_question=(select pk_questions from cb_questions where ques_code='fda_lic_mfg_inst_ind') 
    and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 1) then
	update cb_form_questions set UNEXPECTED_RESPONSE_VALUE='idm_unres1' where fk_question=(select pk_questions from cb_questions where ques_code='fda_lic_mfg_inst_ind') and fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
commit;
  end if;
end;
/
--END--

-- INSERTING DATA INTO CB_QUESTION TABLE FOR IDM FORMS --
--N2/N2B/N2C--
--QUESTION 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igg_sts';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-CMV (IgG or Total)', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cmv_igg_sts',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igg_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'cmv_igg_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igm_sts';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-CMV (IgM)', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cmv_igm_sts',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igm_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'cmv_igm_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_react_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'STS', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'stsyph_react_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_react_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'stsyph_react_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_hcv_perfrmd_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'NAT Test Done for HIV-1/HCV', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'nat_hiv_hcv_perfrmd_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_hcv_perfrmd_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'nat_hiv_hcv_perfrmd_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_ct_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'HBsAg Neut', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hbsag_ct_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_ct_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'hbsag_ct_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_ct_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HCV', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hcv_ct_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_ct_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_hcv_ct_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_west_blot_cde';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HIV 1 Western Blot', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_hiv_west_blot_cde',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_west_blot_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_hiv_west_blot_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_neut_rslt';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'HIV p24 Antigen Neut', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hiv_antigen_neut_rslt',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_neut_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'hiv_antigen_neut_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv_1_2_west_blot_cde';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Anti-HTLV I/II', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'anti_htlv_1_2_west_blot_cde',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv_1_2_west_blot_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'anti_htlv_1_2_west_blot_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--QUESTION 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_ct_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'FTA ABS', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'stsyph_ct_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_ct_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'stsyph_ct_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--N2E--
--QUESTION 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'ripa_ct_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'RIPA', 'null', '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'ripa_ct_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--QUESTION 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'ripa_ct_date';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Test Date', 'null', '1', '0', '1', '0', '0', NULL, '0', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'ripa_ct_date',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_QUESTION_GRP--

--IDM N2 FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cms_cert_lab_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_QUESTION_GRP--
--IDM N2B FORM--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cms_cert_lab_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_QUESTION_GRP--

--IDM N2C FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cms_cert_lab_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_QUESTION_GRP--

--IDM N2D FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cms_cert_lab_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igg_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_QUESTION_GRP--

--IDM N2E FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cms_cert_lab_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_total_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_total_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_sts'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_igm_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_hcv_perfrmd_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='ripa_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='ripa_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE --

--IDM N2 FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idmtest_subques'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres1'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'2.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),null,null,null,'3',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),null,null,null,'4',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),null,null,null,'5',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),null,null,null,'6',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),null,null,null,'7',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),null,null,null,'8',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),null,null,null,'9',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),null,null,null,'10',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),null,null,null,'11',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),null,null,null,'12',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),null,null,null,'13',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),null,null,null,'14',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),null,null,null,'15',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),null,null,null,'18',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'18.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),null,null,null,'19',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'19.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE --

--IDM N2B FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idmtest_subques'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres1'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'2.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),null,null,null,'3',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),null,null,null,'4',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),null,null,null,'5',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),null,null,null,'6',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),null,null,null,'7',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),null,null,null,'8',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),null,null,null,'9',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),null,null,null,'10',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),null,null,null,'11',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),null,null,null,'12',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),null,null,null,'13',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),null,null,null,'14',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),null,null,null,'15',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),null,null,null,'18',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'18.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),null,null,null,'19',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2B'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'19.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE --

--IDM N2C FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idmtest_subques'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres1'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'2.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),null,null,null,'3',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),null,null,null,'4',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),null,null,null,'5',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),null,null,null,'6',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),null,null,null,'7',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),null,null,null,'8',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),null,null,null,'9',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),null,null,null,'10',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),null,null,null,'11',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),null,null,null,'12',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),null,null,null,'13',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),null,null,null,'14',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),null,null,null,'15',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),null,null,null,'18',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'18.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),null,null,null,'19',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'19.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE --

--IDM N2D FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idmtest_subques'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres1'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'2.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),null,null,null,'3',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),null,null,null,'4',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),null,null,null,'5',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),null,null,null,'6',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),null,null,null,'7',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),null,null,null,'8',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),null,null,null,'9',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),null,null,null,'10',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),null,null,null,'11',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),null,null,null,'12',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),null,null,null,'13',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),null,null,null,'14',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),null,null,null,'15',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE --

--IDM N2E FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idmtest_subques'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres1'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'2.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),null,null,null,'3',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='idm_unres2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'3.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),null,null,null,'4',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'4.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),null,null,null,'5',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'5.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),null,null,null,'6',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'6.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),null,null,null,'7',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'7.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),null,null,null,'8',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'8.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),null,null,null,'9',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'9.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),null,null,null,'10',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'10.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),null,null,null,'11',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'11.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),null,null,null,'12',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'12.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),null,null,null,'13',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'13.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),null,null,null,'14',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'14.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),null,null,null,'15',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'15.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
