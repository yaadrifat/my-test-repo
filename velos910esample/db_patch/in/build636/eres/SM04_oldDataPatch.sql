--Old Data Patch for bug #10427

set define off;

declare
	v_table_exists number := 0;
begin
	Select count(*) into v_table_exists
    from ALL_TABLES
    where TABLE_NAME = 'ER_SCHPATCH_TEMP'; 

	if (v_table_exists = 0) then
		execute immediate 'create table er_schpatch_temp ( pk_patprot number, fk_per number, fk_protocol number, displacement number)';
		dbms_output.put_line('Table er_schpatch_temp is created in eres schema');
	else
		dbms_output.put_line('Table er_schpatch_temp exists in eres schema');
    execute immediate 'drop table er_schpatch_temp';
    execute immediate 'create table er_schpatch_temp ( pk_patprot number, fk_per number, fk_protocol number, displacement number)';
		dbms_output.put_line('Table er_schpatch_temp is dropped and created in eres schema');
	end if;
end;
/

comment on table "ER_SCHPATCH_TEMP"  is 'An intermediate table created for PATPROT_SCHDAY change.';

Insert into eres.er_schpatch_temp (pk_patprot, fk_per, fk_protocol, displacement)
(select * from
(select pk_patprot, fk_per, pVisit.fk_protocol, min(DECODE(pVisit.NUM_DAYS,0,0,pVisit.displacement)) as displacement 
from sch_protocol_visit pVisit,  er_patprot patprot
where pVisit.fk_protocol = patprot.fk_protocol and PATPROT_SCHDAY = 1 
group by pk_patprot, fk_per, pVisit.fk_protocol
order by 1));

commit;

update er_patprot 
set PATPROT_SCHDAY = null
where pk_patprot in (Select pk_patprot from er_schpatch_temp);

commit;
