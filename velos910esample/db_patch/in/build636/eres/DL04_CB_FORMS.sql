--STARTS INSERTING RECORD INTO CB_FORMS TABLE--
--N2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'IDM'
    AND VERSION = 'N2';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'IDM','N2','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--N2B--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'IDM'
    AND VERSION = 'N2B';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'IDM','N2B','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--N2C--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'IDM'
    AND VERSION = 'N2C';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'IDM','N2C','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--N2D--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'IDM'
    AND VERSION = 'N2D';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'IDM','N2D','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--N2E--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'IDM'
    AND VERSION = 'N2E';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'IDM','N2E','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--N2F--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'IDM'
    AND VERSION = 'N2F';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'IDM','N2F','1',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--END--
