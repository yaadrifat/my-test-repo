Set define off;
declare 
max_value number default 0;
v_currval number default 0;
v_nextval number default 1;
begin 
  Select max(pk_proc) into max_value from cbb_processing_procedures;
  select seq_cbb_processing_procedures.nextval  into v_currval from dual;

  If max_value is null or max_value = 0 then 
	v_nextval:=1; 
  end if;

  if max_value > v_currval then
    v_nextval := max_value - v_currval;
    v_nextval := v_nextval + 1;
  end if;

  execute immediate 'Alter sequence seq_cbb_processing_procedures increment by '|| v_nextval ;
  Select seq_cbb_processing_procedures.nextval into max_value from dual;
  execute immediate 'Alter sequence seq_cbb_processing_procedures increment by 1';
end ;
/