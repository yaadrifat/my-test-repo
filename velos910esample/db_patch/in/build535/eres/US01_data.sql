--Setting correct datatype for clob type columns
--Bug #2989, #4884, #4883 related to ad-hoc queries

Update er_lkpcol set LKPCOL_DATATYPE='clob' where lower(LKPCOL_NAME) = lower('STSEC_SECTN_TEXT')
and lower(LKPCOL_TABLE)=lower('rep_study_sections');

commit;

Update er_lkpcol set LKPCOL_DATATYPE='clob' where lower(LKPCOL_NAME) = lower('PTDEM_NOTES')
and lower(LKPCOL_TABLE)=lower('ERV_PERSON_COMPLETE');

commit;



