CREATE OR REPLACE TRIGGER "ER_PATSTUDYSTAT_BI0" BEFORE INSERT ON ER_PATSTUDYSTAT
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
   usr := Getuser(:NEW.creator);
                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;



  Audit_Trail.record_transaction(raid, 'ER_PATSTUDYSTAT',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
    -- Ankit : Bug- 10937 Date- 22-Aug-2012
    
       insert_data:=:NEW.SCREEN_NUMBER||'|'|| :NEW.SCREENED_BY||'|'||
    :NEW.SCREENING_OUTCOME||'|'||:NEW.IP_ADD||'|'|| :NEW.PATSTUDYSTAT_REASON||'|'||
    :NEW.PK_PATSTUDYSTAT||'|'|| :NEW.FK_CODELST_STAT||'|'|| :NEW.FK_PER||'|'||
    :NEW.FK_STUDY||'|'|| TO_CHAR(:NEW.PATSTUDYSTAT_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.PATSTUDYSTAT_ENDT,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.PATSTUDYSTAT_NOTE||'|'|| :NEW.RID||'|'||
    :NEW.CREATOR||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
	TO_CHAR(:NEW.NEXT_FOLLOWUP_ON) || '|' || TO_CHAR(:NEW.INFORM_CONSENT_VER) || '|' || :NEW.CURRENT_STAT ;--KM
 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/