ALTER TABLE ERES.ER_SUBMISSION_PROVISO
ADD (fk_codelst_provisotype NUMBER);

COMMENT ON COLUMN 
ERES.ER_SUBMISSION_PROVISO.fk_codelst_provisotype IS 
'Stores FK to er_codelst, code_type=prov_type';

alter trigger ER_SUBMISSION_PROVISO_AU0 disable;