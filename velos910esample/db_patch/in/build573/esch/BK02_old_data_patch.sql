set define off;
declare
v_count number default 0;
begin
select count(*) into v_count from sch_protocol_visit where insert_after!=0  and num_days != displacement;
dbms_output.put_line(v_count);
if v_count > 0 then 
  Update sch_protocol_visit set num_days = displacement where insert_after != 0 and num_days != displacement;
  commit;
end if;
end;