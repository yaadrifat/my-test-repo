set define off;

update er_browserconf set browserconf_seq = 3 where fk_browser = 2 and browserconf_colname = 'STUDY_NUMBER' and browserconf_seq = 2;
commit;

update er_browserconf set browserconf_seq = 33 where fk_browser = 3 and browserconf_colname = 'RIGHT_MASK' and browserconf_seq = 32;
commit;

update er_browserconf set browserconf_seq = 34 where fk_browser = 3 and browserconf_colname = 'MASK_PATADDRESS' and browserconf_seq = 32;
commit;


begin

for i in (select browserconf_colname,browserconf_seq from er_browserconf where browserconf_seq between 12 and 31 and fk_browser = 3 order by browserconf_seq ) loop
update er_browserconf set browserconf_seq = (i.browserconf_seq +1)  where browserconf_seq = i.browserconf_seq and browserconf_colname = i.browserconf_colname and fk_browser = 3;
commit;
end loop;

end;
/

update er_browserconf set browserconf_seq = 12 where fk_browser = 3 and browserconf_colname = 'PATSTUDYCUR_STAT' and browserconf_seq = 11;
commit;


