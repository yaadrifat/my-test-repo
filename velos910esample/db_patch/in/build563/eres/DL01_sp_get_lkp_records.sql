set define off;

CREATE OR REPLACE PROCEDURE        "SP_GET_LKP_RECORDS" (p_page IN NUMBER, p_rec_per_page IN NUMBER,p_viewId IN NUMBER,p_search IN VARCHAR2, p_cursor OUT Gk_Cv_Types.GenericCursorType,p_retrows OUT NUMBER)
    IS
        l_query LONG := 'select ';
 		v_colname VARCHAR2(4000);
		v_coldisplayName VARCHAR2(100);
		v_collist LONG;
		v_countSQL VARCHAR2(5000);
		v_retrows NUMBER;
		v_lkplib NUMBER ;
		v_from VARCHAR2(500);
		v_search VARCHAR2(4000);
		v_where VARCHAR2(4000);
		v_dispcolcount NUMBER;
		v_orderstr VARCHAR2(100);
		v_dispflag CHAR(1);
		v_colorder_setting NUMBER;
		v_where_order NUMBER;

		/* YK 12Jan- BUG 5713*/
		v_increment NUMBER;	-- Used for loop increment value
		v_increment_value NUMBER; -- Used to store v_orderBy_pos value after casting for comparison
		v_column_name VARCHAR2(500); -- Used to store column name
		v_where_Col VARCHAR2(4000); -- Used to construct where condition on column name
		v_orderBy_pos VARCHAR2(10); -- Used to get the order by position value

		pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Sp_Get_Lkp_Records', pLEVEL  => Plog.ldebug);
/*
Parameters:
		   p_page - Page Number for which records are required
		   p_rec_per_page - Number of records per page
		   p_cursor - output type, will hold rows returned
		   p_retrows - output type, returns total number of rows for the query, used by pagination object
*/
 BEGIN
 -- p_cursor in out cursorType
 -- get column names
 -- get from list
	v_search  := p_search;
	v_dispcolcount := 0;
	/* YK 12Jan- BUG 5713*/
	v_increment := 0;
	v_increment_value :=0; 
	v_colorder_setting := 0;
	v_colorder_setting := NVL(INSTR(LOWER(p_search),'order by'),0);
  /* YK 12Jan- BUG 5713*/
  IF v_colorder_setting > 0 THEN
  v_orderBy_pos := SUBSTR(p_search, NVL(INSTR(LOWER(p_search),'order by'),0)+9,2);
  v_increment_value := to_number(v_orderBy_pos);
  END IF;
  
 FOR j IN (SELECT DISTINCT trim(lkpcol_table) AS lkpcol_table FROM ER_LKPCOL a,ER_LKPVIEWCOL b WHERE 	b.fk_lkpcol=a.pk_lkpcol AND b.fk_lkpview=p_viewId)
 LOOP
 v_from := v_from ||','|| j.lkpcol_table;
 END LOOP;
 -- remove  extra ','
 v_from := SUBSTR(v_from, 2, LENGTH(v_from));
 FOR i IN (SELECT a.lkpcol_name,a.lkpcol_dispval,NVL(b.lkpview_seq,0) lkpview_seq ,a.FK_LKPLIB,a.lkpcol_table, lkpview_is_display FROM ER_LKPCOL a,ER_LKPVIEWCOL b WHERE b.fk_lkpcol=a.pk_lkpcol AND b.fk_lkpview=p_viewId   ORDER BY lkpview_seq)
 LOOP
  -- this will get all column rows
 
  v_colname := i.LKPCOL_NAME;
  /* YK 12Jan- BUG 5713*/
  v_increment := v_increment+1;
  IF v_colorder_setting > 0 THEN
    IF v_increment_value=v_increment THEN
     v_column_name:=v_colname;
    END IF;
   END IF;
  v_coldisplayName := i.LKPCOL_DISPVAL;
  v_lkplib:=i.FK_LKPLIB ;
  
  v_coldisplayName := SUBSTR(v_coldisplayName,1,30);
  
  -- generate column list
  v_collist := v_collist || ',' || v_colname  || ' "' || v_coldisplayName ||'"';
  --check if its a visible column
  v_dispflag := i.lkpview_is_display;
  IF v_dispflag = 'Y' AND v_dispcolcount < 4 THEN
    v_orderstr := v_orderstr || ','  || i.lkpview_seq;
	v_dispcolcount := v_dispcolcount + 1;
  END IF;
 END LOOP;
 v_increment:=0; -- reset Loop value
   -- remove first ',' from v_orderstr and add order by
   /* YK 12Jan- BUG 5713*/
   IF v_colorder_setting > 0 THEN
      v_orderstr := ' order by lower(' || v_column_name ||')';
    ELSE
      v_orderstr := ' order by ' || SUBSTR(v_orderstr,2);
   END IF;
     v_collist := SUBSTR(v_collist,2);
  l_query := l_query || v_collist;
   IF INSTR(LOWER(v_from),'er_lkpdata')>0 THEN
	v_where := ' Where  fk_lkplib = '|| v_lkplib ;
 END IF	;
 --Add the defaukt filter to where clause
 /* YK 12Jan- BUG 5713*/
  IF v_colorder_setting > 0 THEN
      v_where_Col:=SUBSTR(SUBSTR(p_search, INSTR(p_search,'and')+ 3),1, NVL(INSTR(LOWER(p_search),'order by'),0)+3)|| ' lower('|| v_column_name ||') '|| SUBSTR(p_search,NVL(INSTR(LOWER(p_search),'order by'),0)+10);
    ELSE
      v_where_Col:=SUBSTR(p_search,INSTR(p_search,'and')+ 3);
  END IF	;
 IF LENGTH(p_search) >0 THEN
  IF LENGTH(v_where)>0 THEN
  v_where := v_where || p_search ;
  ELSE
  IF INSTR(p_search,'and')>0 THEN
  v_where:=	' Where ' || v_where_Col ; /* YK 12Jan- BUG 5713*/
   ELSE
   v_where:=p_search;
  END IF ;
  END IF;
 END IF;
 l_query := l_query || ' from  ' ||v_from || v_where ;
 IF v_colorder_setting <= 0 THEN
 	 l_query := l_query || v_orderstr;
 END IF;
 --remove order by
  v_where_order := NVL(INSTR(LOWER(v_where),'order by'),0);
  IF v_where_order > 0 THEN
    v_where := SUBSTR(v_where,1, v_where_order - 1);
  END IF;

--   INSERT INTO T(C) VALUES (l_query);
--COMMIT;
--  v_countSQL := 'Select count(*) from ' ||v_from || v_where ;
  v_countSQL := 'Select count(*) from (' ||l_query||')' ;

 -- we have final query for sp_get_page_records()
 -- Prepare count sql for sp_get_page_records()
--	open p_cursor for l_query;

  plog.DEBUG(pctx,'LOOKUP SQL' || l_query);

	Sp_Get_Page_Records (p_page,p_rec_per_page, l_query,v_countSQL,p_cursor,p_retrows);
   END; 
/


CREATE SYNONYM ESCH.SP_GET_LKP_RECORDS FOR SP_GET_LKP_RECORDS;


CREATE SYNONYM EPAT.SP_GET_LKP_RECORDS FOR SP_GET_LKP_RECORDS;


GRANT EXECUTE, DEBUG ON SP_GET_LKP_RECORDS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_GET_LKP_RECORDS TO ESCH;

