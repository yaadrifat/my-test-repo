CREATE OR REPLACE PACKAGE BODY ERES."PKG_RESCHEDULE" IS

   PROCEDURE SP_DISC_ALLSCHS (
    P_CURRENT_PROTOCOL    IN   NUMBER,
   P_DISCDATE   IN   DATE,
   P_DISCREASON IN VARCHAR2,
   P_NEW_PROTOCOL IN NUMBER,
   P_NEW_SCHDATE_FLAG IN VARCHAr2,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2   ,
   o_success OUT Number
   )

   AS
   v_new_protocol Number;
   v_new_patprot Number;
   v_new_startdate Date;
   v_new_startday Number;
   v_NEW_PROTOCOL_enr Number;
    BEGIN

/*
    VARIOUS CASES Patient protoocl can be changed:
    1. if patient is not on any protocol, same record in er_patprot is updated
      In sch_events1, new schedule has status = 0

    2.Change patient's calendar/schedule start date/or simply discontinue,
    insert a new row in er_patprot with patprot_stat = 1, and update old row with discontinuation details and
    patprot_stat = 0      In sch_events1 disc old schedule, set status = 5. any messages in sch_msgtran, status set = 2

*/
 begin -- for transaction

    select decode(P_NEW_PROTOCOL,null,0,P_NEW_PROTOCOL)
    into v_NEW_PROTOCOL_enr
    from dual;

    if (v_NEW_PROTOCOL_enr <= 0) then
        v_new_startdate := null;
           v_new_startday := null;
       else
           v_new_startday := 1;
       end if;



    --get all current enrollments for the calendar to be discontinued:

    for i in (select pk_patprot ,fk_per,fk_study , patprot_start,patprot_enroldt from er_patprot
        where fk_protocol = P_CURRENT_PROTOCOL and patprot_stat = 1)
    loop
        --reset the value
        v_NEW_PROTOCOL := v_NEW_PROTOCOL_enr;
         v_new_startday := 1;

        -- discontinue schedule for old enrollment
        SP_DEACTIVATE_SCHEDULE(P_CURRENT_PROTOCOL,i.fk_per,i.patprot_start,i.pk_patprot,
            'P', P_MODIFIED_BY,P_IPADD,o_success );

        if (o_success = 0) then

            select seq_er_patprot.nextval
            into  v_new_patprot
            from dual;

            --update old patprot row with protocol discontinuation reasons

            update er_patprot
            set  PATPROT_DISCDT = P_DISCDATE, PATPROT_REASON = P_DISCREASON , patprot_stat = 0,
            last_modified_by = P_MODIFIED_BY, ip_add = P_IPADD
            where pk_patprot = i.pk_patprot;

            if (v_new_protocol >  0) then
                if (P_NEW_SCHDATE_FLAG = 'E') then
                     if (nvl(i.patprot_enroldt,to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) = to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) then
                         v_new_startdate  := null;
                        v_new_protocol := null;
                        v_new_startday := null;

                    else
                        v_new_startdate  := i.patprot_enroldt;

                    end if;

                elsif (P_NEW_SCHDATE_FLAG = 'D') then
                    v_new_startdate  := P_DISCDATE;
                elsif (P_NEW_SCHDATE_FLAG = 'C') then
                    v_new_startdate  := trunc(sysdate);
                elsif (P_NEW_SCHDATE_FLAG = 'S') then
                    v_new_startdate  := i.patprot_start;
                else
                    v_new_startdate  := null;
                    v_new_protocol := null;
                    v_new_startday := null;

                end if;
            else
                v_new_protocol := null;
                  end if;

            --insert new er_patprot record - need more columns
            Insert into er_patprot   (PK_PATPROT, FK_PROTOCOL, FK_STUDY, FK_USER, PATPROT_ENROLDT, FK_PER,
            PATPROT_START, PATPROT_NOTES, PATPROT_STAT,  CREATOR,
            CREATED_ON, IP_ADD, PATPROT_SCHDAY, FK_TIMEZONE, PATPROT_PATSTDID,
            PATPROT_CONSIGN, PATPROT_CONSIGNDT, PATPROT_RESIGNDT1, PATPROT_RESIGNDT2, FK_USERASSTO,
            FK_CODELSTLOC, PATPROT_RANDOM, PATPROT_PHYSICIAN, FK_CODELST_PTST_EVAL_FLAG, FK_CODELST_PTST_EVAL,
            FK_CODELST_PTST_INEVAL, FK_CODELST_PTST_SURVIVAL, FK_CODELST_PTST_DTH_STDREL, DEATH_STD_RLTD_OTHER,
            INFORM_CONSENT_VER, NEXT_FOLLOWUP_ON, DATE_OF_DEATH, FK_SITE_ENROLLING,PATPROT_TREATINGORG)
            Select v_new_patprot,v_NEW_PROTOCOL,  FK_STUDY,FK_USER, PATPROT_ENROLDT,
            FK_PER,v_new_startdate,PATPROT_NOTES,1,P_MODIFIED_BY,sysdate,p_ipadd,v_new_startday,FK_TIMEZONE,
            PATPROT_PATSTDID,PATPROT_CONSIGN, PATPROT_CONSIGNDT, PATPROT_RESIGNDT1, PATPROT_RESIGNDT2, FK_USERASSTO,
            FK_CODELSTLOC, PATPROT_RANDOM, PATPROT_PHYSICIAN, FK_CODELST_PTST_EVAL_FLAG, FK_CODELST_PTST_EVAL,
            FK_CODELST_PTST_INEVAL, FK_CODELST_PTST_SURVIVAL, FK_CODELST_PTST_DTH_STDREL, DEATH_STD_RLTD_OTHER,
            INFORM_CONSENT_VER, NEXT_FOLLOWUP_ON, DATE_OF_DEATH, FK_SITE_ENROLLING,PATPROT_TREATINGORG
             from er_patprot where pk_patprot = i.pk_patprot;


             --generate new schedule if protocol cal selected

             if (nvl(v_new_protocol ,0) > 0) then
                 sp_generate_schedule(v_new_protocol,i.fk_per,v_new_startdate,v_new_patprot,v_new_startday,P_MODIFIED_BY,p_ipadd,o_success);
             end if;

        end if; --o_success = 0

    end loop;
      o_success := 0;
  exception when OTHERS then
      plog.fatal(pctx,'Exception in SP_DISC_ALLSCHS:' || sqlerrm);
      o_success := -1;
  end;
  END SP_DISC_ALLSCHS;



PROCEDURE SP_UPDATE_EVENTS  (
   P_PROTOCOL    IN   NUMBER,
   P_SCHEDULE_TYPE_FLAG IN Varchar2,
   P_EVENT_RANGE_FLAG  IN Varchar2,
   P_EVENT_DATE  IN DATE,
   P_EVENT_STATUS IN NUMBER,
   P_EVENT_STATUS_DATE  IN DATE,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT Number
)
AS
  v_status Number;
  v_operator Varchar2(10);
  v_sql varchar2(4000);
  v_sql_oldstat varchar2(4000);
  v_sql_newstat varchar2(4000);

  ---
   v_study Number;
   v_patprot_stat number;
   v_pk_patprot number;



    v_oldStatus  varchar2(4000);
    v_oldStatusTotal varchar2(4000);
    v_oldEventStat number;

    v_index number;
    v_length number;

    v_evtName varchar2(4000);
    v_evtNameTotal varchar2(4000);
    v_evtNameTmp varchar2(4000);

    v_evtIndex number;
    v_evtLength number;

    v_eventName varchar2(4000);


  --
BEGIN


 if P_SCHEDULE_TYPE_FLAG = 'D' then
    v_status := 5 ;
    v_patprot_stat := 0;
 else --current schedule
     v_status := 0 ;
     v_patprot_stat := 1;
 end if;



 if P_EVENT_RANGE_FLAG = 'B' then
         v_operator := ' < ';
  elsif  P_EVENT_RANGE_FLAG = 'OB' then
         v_operator := ' <= ';
  elsif    P_EVENT_RANGE_FLAG = 'A' then
         v_operator := ' > ';
  else -- OA
        v_operator := ' >= ';
 end if;

--------------------------------------------
 select chain_id into v_study from EVENT_ASSOC where event_id = P_PROTOCOL AND UPPER (event_type) = 'P';

begin
  for eachPatProt in (select pk_patprot from er_patprot
  where fk_study = v_study and fk_protocol = P_PROTOCOL and patprot_stat = v_patprot_stat)

  loop


--get the old status in some variable, in this case ',' separated string
 for k in (select isconfirmed from sch_events1 where session_id = LPAD(TO_CHAR(P_PROTOCOL),10,'0') and  fk_patprot=eachPatProt.pk_patprot and status = v_status)

 loop

      v_oldStatus := k.isconfirmed || ',' ;
      v_oldStatusTotal := v_oldStatusTotal || v_oldStatus ;


 end loop;
end loop;--for eachPatProt
-------------------------------------------


 v_sql_oldstat := ' update sch_eventstat set EVENTSTAT_ENDDT = :dt, last_modified_by = :lm,ip_add = :ip ,
 last_modified_date =:lmd
 where  FK_EVENT in ( select event_id from sch_events1 where session_id = :protocol' ||
     ' and actual_schdate '|| v_operator ||' :evdate and status = :schstatus ) and EVENTSTAT_ENDDT is null ';

 v_sql := ' update sch_events1 set isconfirmed = :status , event_exeon = :dt , last_modified_by = :lm,ip_add = :ip,event_exeby = :eby,last_modified_date = :lmd where session_id = :protocol' ||
     ' and actual_schdate '|| v_operator ||' :evdate and status = :schstatus' ;



 v_sql_newstat := ' Insert into SCH_EVENTSTAT   (PK_EVENTSTAT, EVENTSTAT_DT, FK_EVENT,
    CREATOR, CREATED_ON, IP_ADD,    EVENTSTAT)
 Select sch_eventstat_seq.nextval, :statdt,event_id, :creator, :createdon, :ip,  :status from
    sch_events1 where session_id = :protocol' ||
     ' and actual_schdate '|| v_operator ||' :evdate and status = :schstatus  ';


 --begin
    --update old status
    --JM: 29Oct2008: Modified the following line
    --execute immediate v_sql_oldstat using P_EVENT_STATUS_DATE,P_MODIFIED_BY,p_ipadd,sysdate, P_PROTOCOL,  P_EVENT_DATE ,v_status;
    execute immediate v_sql_oldstat using sysdate,P_MODIFIED_BY,p_ipadd,sysdate, P_PROTOCOL,  P_EVENT_DATE ,v_status;

    execute immediate v_sql using P_EVENT_STATUS,P_EVENT_STATUS_DATE,P_MODIFIED_BY,p_ipadd,P_MODIFIED_BY, sysdate, P_PROTOCOL,  P_EVENT_DATE ,v_status;

    --insert new status history

    execute immediate v_sql_newstat using P_EVENT_STATUS_DATE,
    P_MODIFIED_BY,sysdate,p_ipadd,P_EVENT_STATUS ,P_PROTOCOL,P_EVENT_DATE, v_status;


    --JM: 12Jul2010: #3455 ----------------------------

      -- get the new event statuses
      for g in ( select event_id, isconfirmed, fk_assoc  from sch_events1 where session_id = LPAD(TO_CHAR(P_PROTOCOL),10,'0')
                            and status = v_status )--and fk_patprot=eachPatProt.pk_patprot)

      loop

         select INSTR(v_oldStatusTotal, ',',1, 1) into v_index FROM dual;
         SELECT SUBSTR(v_oldStatusTotal, 1 , v_index-1) into v_oldEventStat FROM dual;
         SELECT SUBSTR(v_oldStatusTotal, v_index+1 ,  length(v_oldStatusTotal)) into v_oldStatusTotal FROM dual;

         v_oldStatusTotal := v_oldStatusTotal;

         select NAME into v_eventName from EVENT_ASSOC where EVENT_ID=g.fk_assoc;


         for x in (select ALNOT_MOBEVE from SCH_ALERTNOTIFY where fk_study=v_study and fk_protocol=P_PROTOCOL
          and FK_CODELST_AN=P_EVENT_STATUS and fk_patprot is null
          --and  alnot_globalflag = 'G' and alnot_type = 'N'
          )

         loop

           if ((v_eventName = x.ALNOT_MOBEVE) or (x.ALNOT_MOBEVE = 'All')) then

            if (v_oldEventStat <> g.isconfirmed) then

              SP_EVENTNOTIFY(g.event_id, P_EVENT_STATUS, P_MODIFIED_BY, p_ipadd, v_oldEventStat );

            end if;


          end if;


         end loop; --for x




      end loop;--for G



     o_success := 0;

    -------------

 exception when OTHERS then
     plog.fatal(pctx,'Exception in SP_UPDATE_EVENTS:' || sqlerrm);
    plog.fatal(pctx,'Exception in SP_UPDATE_EVENTS:v_sql_newstat' || v_sql_newstat);
    plog.fatal(pctx,'Exception in SP_UPDATE_EVENTS:v_old_newstat' || v_sql_oldstat);
      o_success := -1;
 end;


END SP_UPDATE_EVENTS;


PROCEDURE SP_GENERATE_SCHEDULES  (
   P_PROTOCOL    IN   NUMBER,
   P_SCH_START_DATE IN DATE,
   P_USE_ENROLL_DATE IN NUMBER,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT Number
)
AS
    /* generates schedules for the patients that are not on any current protocol calendar*/
    v_study Number;
    v_start_date Date;

BEGIN

 begin
    -- get study
    select chain_id
    into v_study
    from EVENT_ASSOC
    where event_id = P_PROTOCOL AND UPPER (event_type) = 'P';

    for i in (select pk_patprot ,fk_per,fk_study , patprot_start,patprot_enroldt from er_patprot
        where fk_study = v_study and fk_protocol is null and patprot_stat = 1)
    loop
        --update this record

        if P_USE_ENROLL_DATE = 1 then
            v_start_date := i.patprot_enroldt;
        else
            v_start_date := P_SCH_START_DATE;
        end if;

        if (nvl(v_start_date,to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) <>  to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) then
             update er_patprot
            set  fk_protocol = P_PROTOCOL, patprot_start = v_start_date, PATPROT_SCHDAY = 1,
                    last_modified_by = P_MODIFIED_BY, ip_add = P_IPADD
            where pk_patprot = i.pk_patprot;


            -- generate schedule
            sp_generate_schedule(P_PROTOCOL,i.fk_per,v_start_date,i.pk_patprot,1,P_MODIFIED_BY,p_ipadd,o_success);

        end if;
    end loop;
    o_success := 0;
 exception when NO_DATA_FOUND then
     plog.fatal(pctx,'Exception in SP_GENERATE_SCHEDULES:' || sqlerrm);
      o_success := -1;
 end;

END SP_GENERATE_SCHEDULES;

PROCEDURE SP_DEACTIVATE_SCHEDULE(
p_protocol IN NUMBER,
p_id IN NUMBER,
p_date IN DATE,
p_patprot IN NUMBER,
p_sch_type CHAR :='',
p_last_modified_by IN NUMBER,
p_ipadd in Varchar2,
o_success OUT NUMBER
)
/**
Procedure to deactivate the current schedule
**/
AS
BEGIN
    begin
        IF (p_sch_type = 'P') THEN -- patient schedule

            --discontinue old schedule
            UPDATE SCH_EVENTS1 SET status = 5,
            last_modified_date = SYSDATE WHERE  fk_patprot = p_patprot;

            --delete generated emails where emails are not yet sent
            DELETE FROM  SCH_DISPATCHMSG
            WHERE fk_schevent is not null and fk_patprot = p_patprot and nvl(msg_status,0) = 0;

            --update flag for 'transient emails' so that they don't get sent in clubbed email
            UPDATE SCH_MSGTRAN
            SET msg_status= 2
            WHERE fk_patprot=p_patprot and fk_schevent is not null;
         ELSE  --study schedule

            DELETE FROM  SCH_DISPATCHMSG
            WHERE fk_schevent IN ( SELECT event_id
                                   FROM SCH_EVENTS1
                                    WHERE fk_study= (p_id)
                                    AND SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND
                              status=0) and nvl(msg_status,0) = 0;


            UPDATE SCH_EVENTS1 SET status = 5, last_modified_date = SYSDATE WHERE  fk_study= p_id  AND
            SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND status=0 ;

            UPDATE SCH_MSGTRAN
            SET msg_status= 2
            WHERE fk_schevent IN ( SELECT event_id
                                   FROM SCH_EVENTS1
                                    WHERE fk_study= (p_id)
                                    AND SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND
                               status=0 ) ;

         END IF;

        o_success := 0;
    exception when OTHERS then
        o_success := -1;
        plog.fatal(pctx,'Exception in SP_DEACTIVATE_SCHEDULE:' || sqlerrm);
    end;

END;


PROCEDURE sp_generate_schedule(p_protocol    IN   NUMBER,
      p_patientid   IN   NUMBER,
      p_date        IN   DATE,
      p_patprotid   IN   NUMBER,
      p_days        IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
      o_success OUT Number
   )
   AS

      v_study            NUMBER;
      v_notdone          NUMBER;
      vcursch            NUMBER          := 0;
      v_calassoc         CHAR (1);
      v_protocol_study   NUMBER;
      v_patprot          NUMBER;
      v_err VARCHAR(4000);
      v_mindisp NUMBER;
      v_days NUMBER;
      v_displacement NUMBER;
      ntpd Number;
   BEGIN
       --Check if the schedule is generated from Study admin. If passed from Study admin calendar parameter will contains different values
      -- fk_patprot will contain study primary key if called from study admin calendar. If called from patient schedule, it will contain patprot Id.
      --PatientId is not needed for studyadmin schedule,so will contain 0
      BEGIN
         v_patprot := p_patprotid;
          ntpd :=0;
          Select F_SCH_CODELST_ID('timepointtype','NTPD') into ntpd from dual;

         SELECT event_calassocto, chain_id
         INTO v_calassoc, v_study
         FROM EVENT_ASSOC
         WHERE event_id = p_protocol AND UPPER (event_type) = 'P';

          v_protocol_study := v_study;

          IF (p_days=1)  THEN
                         SELECT MIN(displacement) INTO v_mindisp FROM EVENT_ASSOC WHERE chain_id=p_protocol AND UPPER(event_type)='A';

                      IF (v_mindisp<0) THEN
                            v_days:=v_mindisp;
                      ELSE
                            v_days:=p_days;
                      END IF;
           --KM, 28Sep09 -- For No interval defined visits.
           ELSIF (p_days=0) THEN
                   v_days := null;
           ELSE
                   v_days:=p_days;
          END IF;


         IF (v_calassoc = 'S')    THEN
            SELECT COUNT (1)
             INTO vcursch
             FROM SCH_EVENTS1
            WHERE fk_study = v_study
            AND session_id=LPAD(TO_CHAR(p_protocol),10,'0')
            AND status = 0;

             v_patprot := 0;

         ELSE
            v_protocol_study := 0;

                SELECT COUNT (1)
              INTO vcursch
              FROM SCH_EVENTS1
             WHERE fk_patprot = v_patprot AND status = 0;
       END IF;


           IF vcursch = 0        THEN

            SELECT pk_codelst
              INTO v_notdone
              FROM SCH_CODELST
             WHERE codelst_subtyp = 'ev_notdone';

			INSERT INTO SCH_EVENTS1  ( EVENT_ID, LOCATION_ID, NOTES, DESCRIPTION, BOOKED_BY,
			START_DATE_TIME, END_DATE_TIME, PATIENT_ID, OBJECT_ID, STATUS, TYPE_ID,
			SESSION_ID, VISIT_TYPE_ID, FK_ASSOC,  ISCONFIRMED, BOOKEDON, CREATOR , LAST_MODIFIED_BY ,
			LAST_MODIFIED_DATE ,CREATED_ON, IP_ADD, FK_PATPROT,ACTUAL_SCHDATE, VISIT,
			FK_VISIT,event_notes ,fk_study ,EVENT_SEQUENCE,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE)
			SELECT lpad (to_char (SCH_EVENTS1_SEQ1.NEXTVAL), 10, '0'), '173', null,NAME,  '0000000525' ,
			case when no_interval_flag = ntpd then (p_date) else ((p_date + f_get_displacement(p_protocol,FK_VISIT)) - 1) end ,
			(( p_date +f_get_displacement(p_protocol,FK_VISIT)) - 1) + DURATION,
			lpad (to_char (p_patientid), 10, '0'), '0000001476', 0, '165',
			lpad (to_char (p_protocol), 10, '0'), FUZZY_PERIOD, EVENT_ID,
			v_notdone , p_date , p_dmlby , NULL, NULL , SYSDATE ,  p_ipadd , v_patprot,
			case when no_interval_flag = ntpd then null else ((p_date + f_get_displacement(p_protocol,FK_VISIT)) - 1) end ,
			(select act_num from(select rownum act_num,a.*    from((SELECT v.pk_protocol_visit, v.visit_no, v.displacement  FROM SCH_PROTOCOL_VISIT v
			where v.fk_protocol = p_protocol order by v.displacement asc, v.visit_no)) a)b where b.pk_protocol_visit = assoc.fk_visit) visit,
			fk_visit ,notes,v_protocol_study,assoc.EVENT_SEQUENCE,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
			FROM EVENT_ASSOC  assoc, SCH_PROTOCOL_VISIT pVisit
			WHERE CHAIN_ID = p_protocol
			AND pVisit.fk_protocol =  p_protocol 
			and pVisit.pk_protocol_visit = fk_visit
			and EVENT_TYPE = 'A' and (DECODE(pVisit.NUM_DAYS,0,0,pVisit.displacement) >=  v_days or pVisit.DISPLACEMENT is null) and fk_visit is not null
			and (assoc.HIDE_FLAG is null or assoc.HIDE_FLAG <>1)   ;
			--plog.fatal(pctx,'new proc:' );
            --
--and nvl(displacement,-1) <> 0 added by sonia, without this cluase 'selected --events' were also pulled in schedule

               IF v_calassoc = 'S' THEN
                     -- for admin schedule
                    ----KLUDGE     -- commit needs to be removed from pkg_gensch.sp_admin_msg_to_tran
                    Pkg_Gensch.sp_admin_msg_to_tran (p_protocol, v_study , p_dmlby,    p_ipadd );

                ELSE -- patient schedule

                    -- for clubbed notifications
                    ----KLUDGE     -- commit needs to be removed from pkg_gensch.sp_admin_msg_to_tran
                    Pkg_Gensch.sp_addmsg_to_tran (p_patientid,v_study,p_dmlby,p_ipadd,v_patprot);

                    Sp_Copyprotcrf (v_patprot, p_dmlby, p_ipadd);

                       --change status of mails according to current alert/notify setting
                     -- KLUDGE --commit needs to be removed from pkg_gensch.sp_setmailstat
                        Pkg_Gensch.sp_setmailstat (v_patprot, p_dmlby, p_ipadd);

                END IF;

         END IF;
         o_success := 0;
      EXCEPTION  WHEN OTHERS    THEN
        v_err := SQLERRM;
        o_success := -1;
        plog.fatal(pctx,'Exception in sp_generate_schedule:' || v_err);

      END;
   END sp_generate_schedule ;

function f_get_eventstatus_newpk return Number
as
v_newpk number;
    begin
    select sch_eventstat_seq.nextval
    into  v_newpk from dual;

    return v_newpk;

end;
function f_get_displacement(p_protocol NUMBER,p_visit NUMBER) return Number
as
v_displacement number default 0;
v_day0_condition1 number;
v_num_days number;
v_p_visit_is_day0 boolean default false;

begin
     
    select nvl(a.displacement,0), nvl(num_days,0)
    into  v_displacement , v_num_days 
    from esch.sch_protocol_visit a where a.pk_protocol_visit = p_visit;
         
    if (v_displacement = 1 and v_num_days = 0 ) then
        v_p_visit_is_day0 := true;
    end if;
         
    if (v_p_visit_is_day0 = false) then

        /* 
        --BK-MAR-03-2010 -FIX 5885
        --BK-MAR-29-2010 -FIX 5989 & 5990
		select count(distinct a.pk_protocol_visit) into v_day0_condition1 from esch.sch_protocol_visit a,event_assoc b,event_def c
        where  (a.pk_protocol_visit = b.fk_visit or a.pk_protocol_visit = c.fk_visit)
        and a.fk_protocol = p_protocol
        and a.displacement = 1 and a.num_days = 0 and p_visit not in(
        select pk_protocol_visit from esch.sch_protocol_visit where
        displacement = 1 and num_days = 0 and fk_protocol = p_protocol); */
             
        select count(a.pk_protocol_visit) 
        into v_day0_condition1 
        from esch.sch_protocol_visit a
        where  
        fk_protocol = p_protocol
        and a.displacement = 1 and a.num_days = 0 ;
             
        if(v_day0_condition1 >=1 ) then
            v_displacement := v_displacement+1;
        end if;
             
    end if;   

    return v_displacement;
 
end;
FUNCTION   F_SCH_CODELST_ID (p_type VARCHAR2, p_subtype VARCHAR2)
RETURN NUMBER
IS
  v_pk_codelst NUMBER;
BEGIN
	 SELECT pk_codelst INTO v_pk_codelst FROM SCH_CODELST WHERE trim(codelst_type) = p_type AND trim(codelst_subtyp) = p_subtype;
  RETURN v_pk_codelst ;
END ;
END Pkg_Reschedule;
/
CREATE OR REPLACE SYNONYM ESCH.Pkg_Reschedule FOR Pkg_Reschedule;
CREATE OR REPLACE SYNONYM EPAT.Pkg_Reschedule FOR Pkg_Reschedule;
GRANT ALL ON ERES.PKG_RESCHEDULE TO ESCH with grant option;
GRANT ALL ON ERES.PKG_RESCHEDULE TO EPAT with grant option;