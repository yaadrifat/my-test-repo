set define off;


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'cbu_ids';
 IF (index_count > 0) THEN
   execute immediate 'DELETE FROM er_codelst where  CODELST_TYPE = ''cbu_ids''';
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/

DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'maternal_ids';
 IF (index_count > 0) THEN
   execute immediate 'DELETE FROM er_codelst where  CODELST_TYPE = ''maternal_ids''';
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/

commit;