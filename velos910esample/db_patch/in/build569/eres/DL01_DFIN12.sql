set define off;

DECLARE
 col_count NUMBER;
BEGIN
 select count(*) into col_count from ER_OBJECT_SETTINGS where object_name='protocol_tab' and OBJECT_DISPLAYTEXT='Calendar Milestone Setup';
 if col_count = 0 then
   insert into ER_OBJECT_SETTINGS values(SEQ_ER_OBJECT_SETTINGS.nextval, 'T','9','protocol_tab',9,1,'Calendar Milestone Setup',0);
    commit;
 end if;
END;
/