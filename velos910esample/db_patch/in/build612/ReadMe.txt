#Build 612

=====================================================================================================================================
Garuda :

1. HLA Antigen data has been released in this build. This data is specific to NMDP deployment.
The data and readme file is uploaded over FTP server 66.237.42.81 in the directory -  /Garuda/HLA-Data/Jan-20 

2.The "DL06_NEW_ACCESS_RIGHT.sql"  patch released with this build is specific to NMDP.

3. The "DL07_ER_OBJECT_SETTING.sql"  patch patch released with this build is specific to NMDP.
This patch consist two new menu items:-
i)   Funding Menu
ii)  CBU Tools/Application Messages
iii) Pending Request as Top Menu( Update Script)


=====================================================================================================================================
eResearch:

1. In this build we have released CTRP21401 for Internal review. This is not for QA release. 


=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	calMilestoneSetup.js
2	calMilestoneSetup.js
3	coverage.js
4	createMultiMilestones.jsp
5	datagrid.js
6	ereslogin.jsp
7	eventmessage.jsp
8	labelBundle.properties
9	LC.java
10	LC.jsp
11	login.css
12	manageVisitsGrid.js
13	MC.java
14	MC.jsp
15	messageBundle.properties
16	milestonegrid.js
17	multipleChoiceBox.jsp
18	multiSpecimens.js
19	patientlogin.jsp
20	studybudget.jsp
21	studyschedule.jsp
22	subjectgrid.js

ereslogin.jsp and patientlogin.jsp files are changed for some values coming from database and properties file.
New ereslogin screens are implemented
=====================================================================================================================================
