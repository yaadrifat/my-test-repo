--Sql for Table CB_NOTES to add column NOTE_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'note_seq_1'; 
  if (v_column_exists = 0) then
      execute immediate ('Alter table cb_notes add note_seq_1 varchar2(15)');
  end if;
end;
/




update cb_notes set note_seq_1 = note_seq; 

update cb_notes set note_seq = null;

--Sql for Table CB_NOTES to add column NOTE_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'note_seq'; 
  if (v_column_exists > 0) then
      execute immediate ('alter table cb_notes drop column note_seq');
  end if;
end;
/


--Sql for Table CB_NOTES to add column NOTE_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'note_seq_1'; 
  if (v_column_exists > 0) then
      execute immediate ('alter table cb_notes rename column note_seq_1 to note_seq');
  end if;
end;
/

--Sql for Table CBB to add column PICKUP_ADD_SPL_INSTRUCTION.

DECLARE
  v_column_exists number := 0;   
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'PICKUP_ADD_SPL_INSTRUCTION'; 

  if (v_column_exists = 0) then
      execute immediate ('ALTER TABLE CBB ADD PICKUP_ADD_SPL_INSTRUCTION VARCHAR2(200)');
  end if;  
end;
/
COMMENT ON COLUMN CBB.PICKUP_ADD_SPL_INSTRUCTION IS 'Store Pick-UP Address Special Instruction.'; 

commit;
