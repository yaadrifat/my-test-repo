--When records are inserted in SCH_BGTUSERS table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE
TRIGGER "ESCH"."SCH_BGTUSERS_AI0" 
AFTER INSERT ON ESCH.SCH_BGTUSERS 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION; 
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
		--Inserting row in AUDIT_ROW_MODULE table.
		PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid, 'SCH_BGTUSERS', :NEW.RID,:NEW.FK_BUDGET,'I',:NEW.Creator);
	
		--Inserting rows in AUDIT_COLUMN_MODULE table.
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','PK_BGTUSERS',NULL,:NEW.PK_BGTUSERS,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','FK_BUDGET',NULL,:NEW.FK_BUDGET,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','FK_USER',NULL,:NEW.FK_USER,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','BGTUSERS_RIGHTS',NULL,:NEW.BGTUSERS_RIGHTS,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','BGTUSERS_TYPE',NULL,:NEW.BGTUSERS_TYPE,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','RID',NULL,:NEW.RID,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
		
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_BGTUSERS_AI0 ');
END;
