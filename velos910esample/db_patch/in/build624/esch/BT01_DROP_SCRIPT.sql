drop package "ESCH"."PKG_AUDIT_TRAIL_MODULE"
drop procedure "ESCH"."SP_AUDIT_ROW_MODULE_UPDATE"
BEGIN
DBMS_SCHEDULER.drop_job (job_name => 'AUDIT_ROW_MODULE_UPDATE');
END;
drop table "ESCH"."AUDIT_COLUMN_MODULE" cascade constraints
drop table "ESCH"."AUDIT_ROW_MODULE" cascade constraints
drop sequence "ESCH"."SEQ_AUDIT_ROW_MODULE"
drop sequence "ESCH"."SEQ_AUDIT_COLUMN_MODULE"



