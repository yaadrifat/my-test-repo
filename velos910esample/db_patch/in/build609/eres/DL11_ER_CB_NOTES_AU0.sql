create or replace
TRIGGER "ERES".ER_CB_NOTES_AU0  AFTER UPDATE OF 
PK_NOTES,
ENTITY_ID,
ENTITY_TYPE,
FK_NOTES_TYPE,
FK_NOTES_CATEGORY,
NOTE_ASSESSMENT,
AVAILABLE_DATE,
EXPLAINATION,
NOTES,
FK_REASON,
KEYWORD,
SEND_TO,
VISIBILITY,
CREATOR,
CREATED_ON,
IP_ADD,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
RID,
DELETEDFLAG,
REVIEWED_BY,
CONSULTED_BY,
APPROVED_BY,
AMENDED,
FK_CBU_STATUS,
FLAG_FOR_LATER,
NOTE_SEQ,
COMMENTS,
SUBJECT,
REQUEST_DATE,
REQUEST_TYPE,
EXPLAINATION_TU  ON CB_NOTES REFERENCING OLD AS OLD NEW AS NEW

FOR EACH ROW
DECLARE
   raid NUMBER(10);
   usr VARCHAR(200); 
   old_modified_by VARCHAR2(100);
   new_modified_by VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
   usr := getuser(:NEW.LAST_MODIFIED_BY);
   
  audit_trail.record_transaction
    (raid, 'CB_NOTES', :OLD.RID, 'U', usr);
    
  IF NVL(:OLD.PK_NOTES,0) !=
     NVL(:NEW.PK_NOTES,0) THEN
     audit_trail.column_update
       (raid, 'PK_NOTES',
       :OLD.PK_NOTES, :NEW.PK_NOTES);
  END IF;
  IF NVL(:OLD.ENTITY_ID,0) !=
     NVL(:NEW.ENTITY_ID,0) THEN
     audit_trail.column_update
       (raid, 'ENTITY_ID',
       :OLD.ENTITY_ID, :NEW.ENTITY_ID);
  END IF;  
  IF NVL(:OLD.ENTITY_TYPE,0) !=
     NVL(:NEW.ENTITY_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'ENTITY_TYPE',
       :OLD.ENTITY_TYPE, :NEW.ENTITY_TYPE);
  END IF;  
   IF NVL(:OLD.FK_NOTES_TYPE,0) !=
     NVL(:NEW.FK_NOTES_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'FK_NOTES_TYPE',
       :OLD.FK_NOTES_TYPE, :NEW.FK_NOTES_TYPE);
  END IF;
   IF NVL(:OLD.FK_NOTES_CATEGORY,0) !=
     NVL(:NEW.FK_NOTES_CATEGORY,0) THEN
     audit_trail.column_update
       (raid, 'FK_NOTES_CATEGORY',
       :OLD.FK_NOTES_CATEGORY, :NEW.FK_NOTES_CATEGORY);
  END IF;
   IF NVL(:OLD.NOTE_ASSESSMENT,' ') !=
     NVL(:NEW.NOTE_ASSESSMENT,' ') THEN
     audit_trail.column_update
       (raid, 'NOTE_ASSESSMENT',
       :OLD.NOTE_ASSESSMENT, :NEW.NOTE_ASSESSMENT);
  END IF;  
  IF NVL(:OLD.AVAILABLE_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.AVAILABLE_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'AVAILABLE_DATE',
       to_char(:OLD.AVAILABLE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.AVAILABLE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.EXPLAINATION,' ') !=
     NVL(:NEW.EXPLAINATION,' ') THEN
     audit_trail.column_update
       (raid, 'EXPLAINATION',
       :OLD.EXPLAINATION, :NEW.EXPLAINATION);
  END IF;
  IF NVL(:OLD.NOTES,' ') !=
     NVL(:NEW.NOTES,' ') THEN
     audit_trail.column_update
       (raid, 'NOTES',
       :OLD.NOTES, :NEW.NOTES);
    END IF;
  IF NVL(:OLD.FK_REASON,0) !=
     NVL(:NEW.FK_REASON,0) THEN
     audit_trail.column_update
       (raid, 'FK_REASON',
       :OLD.FK_REASON, :NEW.FK_REASON);
  END IF;
  IF NVL(:OLD.KEYWORD,' ') !=
     NVL(:NEW.KEYWORD,' ') THEN
     audit_trail.column_update
       (raid, 'KEYWORD',
       :OLD.KEYWORD, :NEW.KEYWORD);
    END IF;    
    IF NVL(:OLD.SEND_TO,' ') !=
     NVL(:NEW.SEND_TO,' ') THEN
     audit_trail.column_update
       (raid, 'SEND_TO',
       :OLD.SEND_TO, :NEW.SEND_TO);
    END IF;    
    IF NVL(:OLD.VISIBILITY,' ') !=
     NVL(:NEW.VISIBILITY,' ') THEN
     audit_trail.column_update
       (raid, 'VISIBILITY',
       :OLD.VISIBILITY, :NEW.VISIBILITY);
    END IF;
   IF NVL(:OLD.CREATOR,0) !=
     NVL(:NEW.CREATOR,0) THEN
     audit_trail.column_update
       (raid, 'CREATOR',
       :OLD.CREATOR, :NEW.CREATOR);
  END IF;  
  IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
  IF NVL(:OLD.IP_ADD,0) !=
     NVL(:NEW.IP_ADD,0) THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);     
    END IF;    
  if nvl(:old.LAST_MODIFIED_BY,0) !=
    NVL(:new.LAST_MODIFIED_BY,0) then
    Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
			old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
			new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);
    end if;

    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    IF NVL(:OLD.RID,0) !=
     NVL(:NEW.RID,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.RID, :NEW.RID);     
    END IF;  
     IF NVL(:OLD.DELETEDFLAG,' ') !=
     NVL(:NEW.DELETEDFLAG,' ') THEN
     audit_trail.column_update
       (raid, 'DELETEDFLAG',
       :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
    END IF;
    IF NVL(:OLD.REVIEWED_BY,0) !=
     NVL(:NEW.REVIEWED_BY,0) THEN
     audit_trail.column_update
       (raid, 'REVIEWED_BY',
       :OLD.REVIEWED_BY, :NEW.REVIEWED_BY);
  END IF;
    IF NVL(:OLD.CONSULTED_BY,0) !=
     NVL(:NEW.CONSULTED_BY,0) THEN
     audit_trail.column_update
       (raid, 'CONSULTED_BY',
       :OLD.CONSULTED_BY, :NEW.CONSULTED_BY);
  END IF;
  IF NVL(:OLD.APPROVED_BY,0) !=
     NVL(:NEW.APPROVED_BY,0) THEN
     audit_trail.column_update
       (raid, 'APPROVED_BY',
       :OLD.APPROVED_BY, :NEW.APPROVED_BY);
  END IF;
   IF NVL(:OLD.AMENDED,' ') !=
     NVL(:NEW.AMENDED,' ') THEN
     audit_trail.column_update
       (raid, 'AMENDED',
       :OLD.AMENDED, :NEW.AMENDED);
    END IF;
  IF NVL(:OLD.FK_CBU_STATUS,0) !=
     NVL(:NEW.FK_CBU_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CBU_STATUS',
       :OLD.FK_CBU_STATUS, :NEW.FK_CBU_STATUS);
  END IF;
  IF NVL(:OLD.FLAG_FOR_LATER,' ') !=
     NVL(:NEW.FLAG_FOR_LATER,' ') THEN
     audit_trail.column_update
       (raid, 'FLAG_FOR_LATER',
       :OLD.FLAG_FOR_LATER, :NEW.FLAG_FOR_LATER);
    END IF;  
  IF NVL(:OLD.NOTE_SEQ,0) !=
     NVL(:NEW.NOTE_SEQ,0) THEN
     audit_trail.column_update
       (raid, 'NOTE_SEQ',
       :OLD.NOTE_SEQ, :NEW.NOTE_SEQ);
  END IF; 
   IF NVL(:OLD.COMMENTS,' ') !=
     NVL(:NEW.COMMENTS,' ') THEN
     audit_trail.column_update
       (raid, 'COMMENTS',
       :OLD.COMMENTS, :NEW.COMMENTS);
    END IF;
  IF NVL(:OLD.SUBJECT,' ') !=
     NVL(:NEW.SUBJECT,' ') THEN
     audit_trail.column_update
       (raid, 'SUBJECT',
       :OLD.SUBJECT, :NEW.SUBJECT);
    END IF;	
	IF NVL(:OLD.REQUEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.REQUEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
       (raid, 'REQUEST_DATE',
       to_char(:OLD.REQUEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.REQUEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    IF NVL(:OLD.REQUEST_TYPE,0) !=
     NVL(:NEW.REQUEST_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'REQUEST_TYPE',
       :OLD.REQUEST_TYPE, :NEW.REQUEST_TYPE);
  END IF;
     IF NVL(:OLD.EXPLAINATION_TU,' ') !=
     NVL(:NEW.EXPLAINATION_TU,' ') THEN
     audit_trail.column_update
       (raid, 'EXPLAINATION_TU',
       :OLD.EXPLAINATION_TU, :NEW.EXPLAINATION_TU);
    END IF;
END ;
/