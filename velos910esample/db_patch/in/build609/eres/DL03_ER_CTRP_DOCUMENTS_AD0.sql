set define off;

create or replace
TRIGGER "ERES"."ER_CTRP_DOCUMENTS_AD0"
AFTER  DELETE
ON ER_CTRP_DOCUMENTS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	raid NUMBER(10);
	deleted_data VARCHAR2(4000);
BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	audit_trail.record_transaction (raid, 'ER_CTRP_DOCUMENTS', :OLD.rid, 'D');

	deleted_data := TO_CHAR(:OLD.PK_CTRP_DOC) ||'|'||
	TO_CHAR(:OLD.FK_CTRP_DRAFT) ||'|'||
	TO_CHAR(:OLD.FK_STUDYAPNDX) ||'|'||
	TO_CHAR(:OLD.FK_CODELST_CTRP_DOCTYPE) ||'|'||
	TO_CHAR(:OLD.RID) ||'|'||
	TO_CHAR(:OLD.IP_ADD) ||'|'||
	TO_CHAR(:OLD.CREATOR) ||'|'||
	TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||
	TO_CHAR(:OLD.LAST_MODIFIED_BY) ||'|'||
	TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);

	INSERT INTO audit_delete(raid, row_data) VALUES (raid, deleted_data);
END;
/