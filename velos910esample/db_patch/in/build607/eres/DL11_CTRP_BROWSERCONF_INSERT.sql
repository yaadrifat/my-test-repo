set define off;

DECLARE
  v_item_exists number := 0;  
  browser_Pk number := 0;  
BEGIN
 select pk_browser into browser_Pk from ER_BROWSER where browser_module = 'ctrpDraftBrowser';
  select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='STUDYSTAT_NOTE';
     if (v_item_exists = 0) then   
       INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'STUDYSTAT_NOTE',
        13,
        '',
        'Study Note'
        );
      end if;
      select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='STUDYSTAT_DATE_DATESORT';
      if (v_item_exists = 0) then   
        INSERT
        INTO ER_BROWSERCONF VALUES
        (
        SEQ_ER_BROWSERCONF.nextval,
        browser_Pk,
        'STUDYSTAT_DATE_DATESORT',
        14,
        '',
        'Study Status Date'
        );
       end if;
     COMMIT;

end;
/
