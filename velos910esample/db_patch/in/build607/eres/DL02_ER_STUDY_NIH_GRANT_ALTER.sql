set define off;
    --STARTS MODIFYING COLUMN CREATED_ON TO ER_STUDY_NIH_GRANT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_STUDY_NIH_GRANT'
    AND column_name = 'CREATED_ON';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE ERES.ER_STUDY_NIH_GRANT MODIFY(CREATED_ON DATE DEFAULT sysdate)';
  end if;
end;
/
--END--
