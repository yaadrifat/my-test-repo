SET define OFF;
DECLARE
  v_item_exists NUMBER := 0;
    browser_Pk number := 0;  
BEGIN

select pk_browser into browser_Pk from ER_BROWSER where browser_module = 'ctrpDraftBrowser';
  SELECT COUNT(*)
  INTO v_item_exists
  FROM ER_BROWSERCONF
  WHERE fk_browser    = browser_Pk
  AND BROWSERCONF_COLNAME ='RESEARCH_TYPE';
  IF (v_item_exists = 1) THEN
    UPDATE ER_BROWSERCONF
    SET BROWSERCONF_SETTINGS='{"key":"RESEARCH_TYPE", "label":"Trial Submission Category", "sortable":true, "resizeable":true,"format":"resType"}'
    WHERE fk_browser    = browser_Pk
    AND BROWSERCONF_COLNAME ='RESEARCH_TYPE';
  END IF; 
  COMMIT;

end;
/