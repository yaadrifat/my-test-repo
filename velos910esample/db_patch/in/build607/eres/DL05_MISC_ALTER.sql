set define off;


--Sql for Table CBB to add column DATE_FORMAT.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_TIME_FORMAT'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add FK_TIME_FORMAT NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB.FK_TIME_FORMAT IS 'Stores TIME format.'; 
commit;

--Sql for Table CBB to add column DATE_FORMAT.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_DATE_FORMAT'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add FK_DATE_FORMAT NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB.FK_DATE_FORMAT IS 'Stores Date format.'; 
commit;



--Sql for Table CBB to add column ACCREDITATION.'; 


DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_ACCREDITATION'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add FK_ACCREDITATION NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB.FK_ACCREDITATION IS 'Stores accreditation.'; 
commit;


 --Sql for Table CB_CORD to add column BACT_COMMENT.'; 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'BACT_COMMENT'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add BACT_COMMENT VARCHAR2(255 BYTE)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.BACT_COMMENT IS 'This Column will store the comment for the Bacterial Culture.'; 
commit;


 --Sql for Table CB_CORD to add column FUNG_COMMENT.'; 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FUNG_COMMENT'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FUNG_COMMENT VARCHAR2(255 BYTE)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FUNG_COMMENT IS 'This Column will store the comment for the Fungal Culture .'; 
commit;


Set define off;

--STARTS ADDING COLUMN REPORT_DATE TO CB_UPLOAD_INFO TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_UPLOAD_INFO'
    AND column_name = 'REPORT_DATE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_UPLOAD_INFO ADD(REPORT_DATE DATE)';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_UPLOAD_INFO.REPORT_DATE IS 'Report Date for lab test';

--Sql for Table CB_HLA to add column FK_SPEC_TYPE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_HLA' AND column_name = 'FK_SPEC_TYPE';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_HLA add FK_SPEC_TYPE number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_HLA.FK_SPEC_TYPE IS 'This field store FK of Specimen Type.';
commit;

--Sql for Table ER_ADD to add column ADDRESS2
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_ADD' AND column_name = 'ADDRESS2';
  if (v_column_exists = 0) then
      execute immediate ('alter table ER_ADD add ADDRESS2 varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN ER_ADD.ADDRESS2 IS 'This field store Address 2 of Address.';
commit;


--STARTS ADDING COLUMN INFUSION_DATE TO CB_RECEIPANT_INFO TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_RECEIPANT_INFO'
    AND column_name = 'INFUSION_DATE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_RECEIPANT_INFO ADD(INFUSION_DATE DATE)';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_RECEIPANT_INFO.INFUSION_DATE IS 'Date entered by the CM once the CBU has been infused (indication by the TC)';



--Sql for Table CB_ANTIGEN to add column HLA_LOCUS
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ANTIGEN' AND column_name = 'HLA_LOCUS';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ANTIGEN add HLA_LOCUS varchar2(200 byte)');
  end if;
end;
/
COMMENT ON COLUMN CB_ANTIGEN.HLA_LOCUS IS 'This field store locus of HLA.';
commit;

--Sql for Table CB_ANTIGEN to add column HLA_TYPING_METHOD
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ANTIGEN' AND column_name = 'HLA_TYPING_METHOD';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ANTIGEN add HLA_TYPING_METHOD varchar2(200 byte)');
  end if;
end;
/
COMMENT ON COLUMN CB_ANTIGEN.HLA_TYPING_METHOD IS 'This field store typing method of HLA.';
commit;

--Sql for Table CB_ANTIGEN to add column ALT_ANTIGEN_ID
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ANTIGEN' AND column_name = 'ALT_ANTIGEN_ID';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ANTIGEN add ALT_ANTIGEN_ID number(10)');
  end if;
end;
/
COMMENT ON COLUMN CB_ANTIGEN.ALT_ANTIGEN_ID IS 'This field store alternate antigen id.';
commit;

--Sql for Table CB_ANTIGEN to add column LAST_UPDATED_SOURCE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ANTIGEN' AND column_name = 'LAST_UPDATED_SOURCE';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ANTIGEN add LAST_UPDATED_SOURCE varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_ANTIGEN.LAST_UPDATED_SOURCE IS 'This field store last updated source of antigen.';
commit;

--Sql for Table CB_ANTIGEN to add column CREATED_BY_SOURCE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ANTIGEN' AND column_name = 'CREATED_BY_SOURCE';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ANTIGEN add CREATED_BY_SOURCE varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_ANTIGEN.CREATED_BY_SOURCE IS 'This field store created source of antigen.';
commit;

--Sql for Table CB_ANTIGEN_ENCOD to add column LAST_UPDATED_SOURCE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ANTIGEN_ENCOD' AND column_name = 'LAST_UPDATED_SOURCE';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ANTIGEN_ENCOD add LAST_UPDATED_SOURCE varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_ANTIGEN_ENCOD.LAST_UPDATED_SOURCE IS 'This field store last updated source of antigen.';
commit;

--Sql for Table CB_ANTIGEN_ENCOD to add column CREATED_BY_SOURCE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ANTIGEN_ENCOD' AND column_name = 'CREATED_BY_SOURCE';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ANTIGEN_ENCOD add CREATED_BY_SOURCE varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_ANTIGEN_ENCOD.CREATED_BY_SOURCE IS 'This field store created source of antigen.';
commit;

--Sql for Table CB_HLA to add column FK_SOURCE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_HLA' AND column_name = 'FK_SOURCE';
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_HLA add FK_SOURCE varchar2(50 char)');
  end if;
end;
/
COMMENT ON COLUMN CB_HLA.FK_SOURCE IS 'This field store source of antigen.';
commit;
