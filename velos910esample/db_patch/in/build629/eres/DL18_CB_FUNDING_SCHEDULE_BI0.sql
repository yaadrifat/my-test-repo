CREATE OR REPLACE TRIGGER CB_FUNDING_SCHEDULE_BI0 BEFORE INSERT ON CB_FUNDING_SCHEDULE 
REFERENCING OLD AS OLD NEW AS NEW
	FOR EACH ROW
	DECLARE
	raid NUMBER(10);
	erid NUMBER(10);
	usr VARCHAR(2000);
	insert_data VARCHAR2(4000);
	 BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_FUNDING_SCHEDULE',erid, 'I',usr);
       insert_data:=:NEW.PK_FUNDING_SCHEDULE || '|' ||
to_char(:NEW.FK_CBB_ID) || '|' ||
to_char(:NEW.DATE_TO_GENERATE) || '|' ||
to_char(:NEW.CBB_REGISTRATION_DATE) || '|' ||
to_char(:NEW.NOTIFICATION_BEFORE) || '|' ||
to_char(:NEW.GENERATED_STATUS) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.DELETEDFLAG);      
    INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/