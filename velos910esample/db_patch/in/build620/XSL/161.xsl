<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
<xsl:apply-templates select="ROWSET/ROW/HLA/HLA_ROW"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--> <font size ="4"> National Marrow Donor Program� </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--><font size ="4"> CBU Summary Report for HPC, Cord Blood </font>
</b>
</td>
</tr>
</table>
<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
<xsl:value-of select="ROW/CORD_REGISTRY_ID"/>
</td>
<td align="right"> Cord Blood Bank ID: 
<xsl:value-of select="ROW/CBBID"/>
</td>

</tr>
</table>
<hr class="thickLine" width="100%"/>
<xsl:apply-templates select="ROW"/>

<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 


<xsl:template match="ROW">
<xsl:for-each select="//ROW">

 <b><font size="3">IDS </font></b> 

<table>
<tr>
<TD> CBU Registry ID: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="CORD_REGISTRY_ID" /> 
</TD>
<TD> Maternal Registry ID: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="REGISTRY_MATERNAL_ID" />
</TD>
</tr>

<tr>
<TD> CBU Local ID: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CORD_LOCAL_CBU_ID" />
</TD>
<TD> Maternal Local ID: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="MATERNAL_LOCAL_ID" />
</TD>
</tr>

<tr>
<TD> ISBT Donation Identification Number:  </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CORD_ISBI_DIN_CODE" />
</TD>

<TD> Notes:  </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="IDNOTES" />
</TD>
</tr>
</table>


<br></br> <br></br>
<b><font size="3">CBU Information </font></b> 

<table>
<tr>
<TD> Cord Blood Bank ID: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="CBBID" /> 
</TD>
<TD> CBU Storage Location: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="STORAGE_LOC" />
</TD>
</tr>

<tr>
<TD> CBU Collection Site: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CBU_COLLECTION_SITE" />
</TD>
<TD> Notes: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CBU_INFO_NOTES_VISIBLE_TC" />
</TD>
</tr>

</table>


<br></br> <br></br>
<b><font size="3">Licensure and Eligibility </font></b> 

<table>
<tr>
<TD> Eligibility Determination: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="ELIGIBLE_STATUS" /> 
</TD>
<TD> Licensure Status: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="LIC_STATUS" />
</TD>
</tr>

<tr>
<TD> Notes: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="ELIGIBILITY_NOTES_VISIBLE_TC" />
</TD>
</tr>

</table>


<br></br> <br></br>
<b><font size="3">Lab Summary </font></b> 

<table>
<tr>
<TD> Processing Start Date: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PRCSNG_START_DATE" /> 
</TD>
<TD> Bacterial Culture: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="BACT_CULTURE" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="BACT_COMMENT" />
</TD>
</tr>

<tr>
<TD> Freeze Date: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FRZ_DATE" />
</TD>
<TD> Fungal Culture: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="FUNGAL_CULTURE" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="FUNG_COMMENT" />
</TD>
</tr>

<tr>
<TD> ABO Blood Type: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="ABO_BLOOD_TYPE" /> 
</TD>
<TD> Hemoglobinopathy Testing: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="HEMOGLOBIN_SCRN" />
</TD>
</tr>

<tr>
<TD> Rh Type: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="RH_TYPE" /> 
</TD>
<TD> Notes: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="LAB_SUM_NOTES_VISIBLE_TC" />
</TD>
</tr>
</table>



<br></br> <br></br>
<b><font size="3">Product Modification </font></b> 

<table>
<tr>
<TD> Processing: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PROCESSING" /> 
</TD>
<TD> Type of automated process used to modify the CBU: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="AUTOMATED_TYPE" />
</TD>
</tr>

<tr>
<TD> Please Specify: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHER_PROCESSING" />
</TD>
<TD> Product Modification:  </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="PRODUCT_MODIFICATION" />
</TD>
</tr>

<tr>
<TD> Please Specify: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="OTHER_PRODUCT_MODI" /> 
</TD>

<TD> Number Of Individual Fractions that can be Thawed: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="INDIVIDUAL_FRAC" /> 
</TD>

</tr>

<tr>
<TD> Number of bags: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_BAGS" /> 
</TD>

<TD> Please Specify : </TD> <TD class="reportData" align="left" >
<xsl:value-of select="OTHER_BAG" /> 
</TD>

</tr>
</table>


<br></br> <br></br>
<b><font size="3">CBU Samples </font></b> 
<table>
<tr>
<TD> Number of Filter Paper Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="FILTER_PAPER" /> 
</TD>

<TD> Number of RBC Pellets : </TD> <TD class="reportData" align="left" >
<xsl:value-of select="RPC_PELLETS" /> 
</TD>
</tr>
<tr>
<TD> Number of Extracted DNA Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="FILTER_PAPER" /> 
</TD>

<TD> Number of Serum Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="RPC_PELLETS" /> 
</TD>
</tr>
<tr>
<TD> Number of Plasma Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="RPC_PELLETS" /> 
</TD>

<TD> Number of Non-Viable Cell samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="FILTER_PAPER" /> 
</TD>
</tr>
<tr>
<TD> Number of Segments : </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_SEGMENTS" /> 
</TD>
<TD> Number of other representative aliquots
stored under conditions consistent
with those of the final product:</TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_OTH_REP_ALLIQUOTS_F_PROD" /> 
</TD>
</tr>
<tr>
<TD> Number of other representative
aliquots stored under alternate conditions: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_OTH_REP_ALLIQ_ALTER_COND" /> 
</TD>
</tr>

</table>


<br></br> <br></br>
<b><font size="3"> Maternal Samples </font></b> 
<table>
<tr>
<TD> Number of Maternal Serum Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_SERUM_MATER_ALIQUOTS" /> 
</TD>

<TD> Number of Maternal Plasma Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_PLASMA_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD> Number of Maternal Extracted DNA Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_EXTR_DNA_MATER_ALIQUOTS" /> 
</TD>

<TD> Number of Miscellaneous Maternal Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_CELL_MATER_ALIQUOTS" /> 
</TD>
</tr>
</table>

</xsl:for-each>
</xsl:template> 


<!--xsl:template match="/ROW/HLA/HLA_ROW"-->
<!--xsl:template match="/">

<xsl:for-each select="../HLA/HLA_ROW">
<table>
<tr>
<td>hla1</td>
<td class="reportData" align="left" >
<xsl:value-of select="HLA_VALUE_TYPE1" /> 
</td>
</tr>

<tr>
<td>hla2 </td>
<td class="reportData" align="left" >
<xsl:value-of select="HLA_VALUE_TYPE2" /> 
</td>
</tr>
</table>

</xsl:for-each>

</xsl:template--> 

</xsl:stylesheet>