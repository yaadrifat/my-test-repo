declare
v_ct number;
begin

	select count(*) into v_ct from ER_IRBFORMS_SETUP where IRB_TAB_SUBTYPE = 'irb_view_all';
	
	if (v_ct = 0 ) then

		Insert into ER_IRBFORMS_SETUP
		   (PK_IRBFORMS_SETUP, IRB_TAB_SUBTYPE, CODELST_SUBMISSION_SUBTYPE, FORM_CATLIB_SUBTYPE, FORM_SUBMISSION_SUBTYPE)
		 Values
		   (seq_ER_IRBFORMS_SETUP.nextval, 'irb_view_all', '', 'irb_check1', '');

		Insert into ER_IRBFORMS_SETUP
		   (PK_IRBFORMS_SETUP, IRB_TAB_SUBTYPE, CODELST_SUBMISSION_SUBTYPE, FORM_CATLIB_SUBTYPE, FORM_SUBMISSION_SUBTYPE)
		 Values
		   (seq_ER_IRBFORMS_SETUP.nextval, 'irb_view_all', '', 'irb_check2', '');

		Insert into ER_IRBFORMS_SETUP
		   (PK_IRBFORMS_SETUP, IRB_TAB_SUBTYPE, CODELST_SUBMISSION_SUBTYPE, FORM_CATLIB_SUBTYPE, FORM_SUBMISSION_SUBTYPE)
		 Values
		   (seq_ER_IRBFORMS_SETUP.nextval, 'irb_view_all', '', 'irb_check3', '');

		   
		COMMIT;
	
	end if;
end;
/