<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByVisit" match="ROW" use="VISIT_NAME" />
<xsl:key name="RecordsByVisitEvent" match="ROW" use="concat(VISIT_NAME, ' ', EVENT_NAME)" />

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repId"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="dtFrom"/>
<xsl:param name="dtTo"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

	</HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >
<tr class="reportGreyRow">
<td class="reportPanel"> 
VELMESSGE[M_Download_ReportIn]<!-- Download the report in -->: 
<A href='{$wd}' >
<img border="0" title="VELLABEL[L_Word_Format]" alt="VELLABEL[L_Word_Format]" src="./images/word.GIF" ></img><!-- Word Format -->
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
<img border="0" title="VELLABEL[L_Excel_Format]" alt="VELLABEL[L_Excel_Format]" src="./images/excel.GIF" ></img><!-- Excel Format -->
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
</A> 
</td>
</tr>
</table>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>
<TABLE WIDTH="100%" >
	<TR>
		<TD class="reportGrouping" ALIGN="Left" width="22%">
		VELLABEL[L_Protocol_CalName]<!-- Protocol Calendar Name -->: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_NAME" /></b>
		</TD>
	</TR>
	<TR>
		<TD class="reportGrouping" ALIGN="Left" width="22%">
		VELLABEL[L_Description]<!-- Description -->: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_DESC" /></b>
		</TD>
	</TR>
	<TR>
		<TD class="reportGrouping" ALIGN="Left" width="22%">
		VELLABEL[L_Duration]<!-- Duration -->: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_DURATION" /></b>
		</TD>
	</TR>
</TABLE>
<hr class="thickLine" />
<TABLE WIDTH="100%" BORDER="1">
<TR>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Event]<!-- Event --></th>
	<th class="reportHeading" WIDTH="15%" ALIGN="CENTER">VELLABEL[L_Event_Description]<!--Event Description--></th>
	<th class="reportHeading" WIDTH="15%" ALIGN="CENTER">VELLABEL[L_Event_Window]<!--Event Window--></th>
    <th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Cpt_Code]<!--CPT Code--></th>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Associated_Costs]<!--Associated Costs--></th>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Associated_Resources]<!--Associated Resources--></th>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Associated_FilesOrUrls]<!--Associated Files/URLs--></th>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Associated_Forms]<!--Associated Forms--></th>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Associated_Messages]<!--Associated Messages--></th>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Hidden]<!--Hidden--></th>
	<th class="reportHeading" WIDTH="10%" ALIGN="CENTER">VELLABEL[L_Coverage_Type]<!--Coverage Type--></th>
</TR>

<xsl:for-each select="ROW[count(. | key('RecordsByVisit', VISIT_NAME)[1])=1]">
<TR>
	<TD class="reportGrouping" ALIGN="Left" colspan="2">
	VELLABEL[L_Visit]<!-- Visit -->: <xsl:value-of select="VISIT_NAME" />
	</TD>

	<TD class="reportGrouping" ALIGN="Left" colspan="2">
	VELLABEL[L_Interval]<!-- Interval -->: <xsl:value-of select="VISIT_INTERVAL" />&#xa0;<xsl:value-of select="INSERT_AFTER" />
	</TD>

	<TD class="reportGrouping" ALIGN="Left" colspan="3">
	VELLABEL[L_Description]<!-- Description -->: <xsl:value-of select="VISIT_DESC" />
	</TD>

	<TD class="reportGrouping" ALIGN="Left" colspan="3">
	<!--KM-#4867-->
	VELLABEL[L_Visit_Window]<!-- Visit Window -->: <xsl:value-of select="VISIT_WIN_BEFORE" />   <xsl:value-of select="VISIT_WIN_AFTER" />
	</TD>


</TR>
<xsl:variable name="str" select="key('RecordsByVisit', VISIT_NAME)" />
<xsl:for-each select="$str[count(. | key('RecordsByVisitEvent',concat(VISIT_NAME, ' ', EVENT_NAME))[1])=1]">

<xsl:variable name="class">
<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
<xsl:otherwise>reportOddRow</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<TR> <xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="EVENT_NAME"/>&#xa0;</td>
	<td WIDTH="20%" ALIGN="LEFT"><xsl:value-of select="EVENT_DESC"/>&#xa0;</td>
<!--	<td WIDTH="10%" ALIGN="LEFT"><xsl:if test="FUZZY_PERIOD!=''">&#0177;&#xa0;<xsl:value-of select="FUZZY_PERIOD"/></xsl:if>&#xa0;</td> -->
	<td WIDTH="10%" ALIGN="LEFT">
<!--	<xsl:choose>
	<xsl:when test="starts-with(FUZZY_DURATION,'0')"></xsl:when>
	<xsl:when test="contains(FUZZY_DURATION,'+')"></xsl:when>
	<xsl:when test="contains(FUZZY_DURATION,'-')"></xsl:when>
	<xsl:otherwise>&#0177;</xsl:otherwise>
	</xsl:choose>
-->
	<xsl:value-of select="VWINDOW_BEF"/><BR/><xsl:value-of select="VWINDOW_AFT"/>
	</td> 
    <td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="EVENT_CPTCODE"/>&#xa0;</td>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="EVENT_COST"/>&#xa0;</td>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="RESOURCES"/>&#xa0;</td>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="APPENDIX"/>&#xa0;</td>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="FORMS"/>&#xa0;</td>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="MESSAGES"/>&#xa0;</td>
        <td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="IS_HIDDEN"/>&#xa0;</td>
	<td WIDTH="10%" ALIGN="LEFT"><xsl:value-of select="COVERAGE_TYPE"/>&#xa0;</td>
</TR>
</xsl:for-each>
</xsl:for-each>
</TABLE>
<BR/>

<hr class="thickLine" />
<TABLE WIDTH="100%" >
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>