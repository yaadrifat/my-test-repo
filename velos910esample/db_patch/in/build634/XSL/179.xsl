<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:variable name="VAR1"></xsl:variable>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 
			

<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><font size ="4"> National Marrow Donor Program® </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><font size ="4">Family and Medical Quetionnaire Report (<xsl:value-of select="ROW/LICSTATUS"/>) for HPC, Cord Blood</font>
</b>
</td>
</tr>
</table>

<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
	<xsl:value-of select="ROW/SITENAME"/>
</td>
<td align="right"> Cord Blood Bank ID:
	<xsl:value-of select="ROW/SITEID" />
</td>
</tr>

<tr>
<td align="left"> CBU Registry Id: 
	<xsl:value-of select="ROW/REGID" />
</td>
<td align="right"> CBU Identification Number on Bag: 

<xsl:for-each select="//ROW/IDSONBAG/IDSONBAG_ROW">
<xsl:if test="IDONBAGVAL='CBU Registry ID'">
	<xsl:value-of select="../../REGID" />&#160; 
</xsl:if>
<xsl:if test="IDONBAGVAL='CBU Local ID'">
	<xsl:value-of select="../../LOCALCBUID" />&#160; 
</xsl:if>
<xsl:if test="IDONBAGVAL='ISBT DIN'">
	<xsl:value-of select="../../ISTBTDIN" />&#160;  
</xsl:if>
<xsl:if test="(position()=1)">

 <xsl:for-each select="../../ADDIDS/ADDIDS_ROW">
		<xsl:value-of select="ADDIDVAL" />&#160;
 </xsl:for-each>

</xsl:if>

</xsl:for-each>


</td>
</tr>
</table>


<hr class="thickLine" width="100%"/>

<br></br>
<br></br>
<br></br>
<xsl:variable name="VAR_DEPEN_VALUE" /> 
<xsl:variable name="VAR_DEPEN_QUES_PK" /> 
<xsl:variable name="VAR_PRE_RESPONSE" /> 
<xsl:variable name="FKDEPEN_VAL" /> 
<xsl:variable name="VAR_TEMP" />
<xsl:variable name="VAR_SPLITED" />
<xsl:variable name="VALUE1" />
<xsl:variable name="VALUE2" />
<xsl:variable name="VALUE3" />

<xsl:for-each select="//ROW/FMHQ_GRP/FMHQ_GRP_ROW">
	
	<table border="0" ><tr><td colspan="4"><xsl:value-of select="QUESTION_GRP_NAME" />&#160; </td></tr></table>


		<xsl:variable name="PKGRPVAL" select="PK_QUESTION_GRP" /> 
		<xsl:for-each select="//ROW/FMHQ/FMHQ_ROW">
			<xsl:if test="$PKGRPVAL=FK_QUES_GRP">

					<xsl:variable name="FKDEPEN_PKVAL" select="DEPENT_QUES_PK" /> 
					<xsl:variable name="VAR_DEPEN_QUES_PK" select="DEPENT_QUES_PK"/> 
					<xsl:variable name="VAR_DEPEN_VALUE" select="DEPENT_QUES_VALUE"/> 
					<xsl:variable name="VAR_RESPONSEVAL" select="RESPONSE_VAL"/>

										
					<xsl:variable name="VAR_PRE_RESTYPE" select="preceding-sibling::*[1]/RESPONSE_TYPE"/> 
					<xsl:variable name="VAR_PRE_RESTYPE1" select="preceding-sibling::*[2]/RESPONSE_TYPE"/> 
					<xsl:variable name="VAR_PRE_RESVAL" select="preceding-sibling::*[1]/RESPONSE_VAL12"/> 

					



					<xsl:if test="$FKDEPEN_PKVAL!='0' and $VAR_PRE_RESTYPE='multiselect' and $VAR_PRE_RESTYPE1='dropdown' and RESPONSE_TYPE='multiselect'">
					
							<xsl:if test="$VAR_PRE_RESVAL!='0' and contains($VAR_PRE_RESVAL, ',')">
								
								<xsl:variable name="VAR1"/>
								<xsl:variable name="VAR1" select="substring-before($VAR_PRE_RESVAL,',')"/>
								<xsl:variable name="SPLTD_STR1" select="substring-after($VAR_PRE_RESVAL,', ')"/>
								

								<xsl:variable name="VAR2">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR1,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR1,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring-after($VAR_PRE_RESVAL,', ')"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>

								
								<xsl:variable name="SPLTD_STR2" select="substring-after($SPLTD_STR1,', ')"/>

								<xsl:variable name="VAR3">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR2,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR2,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR2"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>
							
							
								<xsl:variable name="SPLTD_STR3" select="substring-after($SPLTD_STR2,', ')"/>

								<xsl:variable name="VAR4">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR3,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR3,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR3"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>
						
								<xsl:variable name="SPLTD_STR4" select="substring-after($SPLTD_STR3,', ')"/>

								<xsl:variable name="VAR5">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR4,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR4,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR4"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>

								
								<xsl:variable name="SPLTD_STR5" select="substring-after($SPLTD_STR4,', ')"/>
								
								<xsl:variable name="VAR6">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR5,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR5,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR5"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>
				
								
								<xsl:variable name="FINDMATCH">
								    <xsl:choose>
									 <xsl:when test="$VAR1=$VAR_DEPEN_VALUE">
									    <xsl:value-of select="'matched'" />
									</xsl:when>
									 <xsl:when test="$VAR2=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR3=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR4=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR4=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR6=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
								    </xsl:choose>
							      </xsl:variable>

							     <xsl:if test="$FINDMATCH='matched'">
									<table border="1" bordercolor="black">
									<tr>
									<td><xsl:value-of select="QUES_SEQ" /></td>
									<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
									<td>
									<xsl:if test="RESPONSE_TYPE='dropdown'">
										<xsl:value-of select="DROPDOWN_VALUE" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									</td>
									<td><xsl:value-of select="ASSES_RESPONSE" /></td>
									<td><xsl:value-of select="ASSES_NOTES" /></td>
									</tr>
									</table>
							    </xsl:if>

								
						</xsl:if>
						<xsl:if test="$VAR_PRE_RESVAL!='0' and not(contains($VAR_PRE_RESVAL, ','))">
								<xsl:variable name="FINDMATCH">
								    <xsl:choose>
									 <xsl:when test="$VAR_PRE_RESVAL=$VAR_DEPEN_VALUE">
									    <xsl:value-of select="'matched'" />
									</xsl:when>
								    </xsl:choose>
								</xsl:variable>
								<xsl:if test="$FINDMATCH='matched'">
									<table border="1" bordercolor="black">
									<tr>
									<td><xsl:value-of select="QUES_SEQ" /></td>
									<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
									<td>
									<xsl:if test="RESPONSE_TYPE='dropdown'">
										<xsl:value-of select="DROPDOWN_VALUE" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									</td>
									<td><xsl:value-of select="ASSES_RESPONSE" /></td>
									<td><xsl:value-of select="ASSES_NOTES" /></td>
									</tr>
									</table>
							    </xsl:if>
						</xsl:if>
			</xsl:if>
				

							<xsl:variable name="VAR_TEMP">
								  <xsl:choose>
									    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[1]/DEPENT_QUES_PK='0' and preceding-sibling::*[1]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[1]/RESPONSE_CODE" />
									    </xsl:when>
									    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[2]/DEPENT_QUES_PK='0' and preceding-sibling::*[2]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[2]/RESPONSE_CODE" />
									    </xsl:when>
									     <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[3]/DEPENT_QUES_PK='0' and preceding-sibling::*[3]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[3]/RESPONSE_CODE" />
									    </xsl:when>
									    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[4]/DEPENT_QUES_PK='0' and preceding-sibling::*[4]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[4]/RESPONSE_CODE" />
									    </xsl:when>
								 </xsl:choose>
							</xsl:variable>




							
							<xsl:if test="$FKDEPEN_PKVAL!='0' and not(contains($VAR_DEPEN_VALUE, ','))">
						
								<xsl:if test="$VAR_DEPEN_VALUE=$VAR_TEMP">
									<table border="1" bordercolor="black">
									<tr>
									<td><xsl:value-of select="QUES_SEQ" /></td>
									<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
									<td>
									<xsl:if test="RESPONSE_TYPE='dropdown'">
										<xsl:value-of select="DROPDOWN_VALUE" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									</td>
									<td><xsl:value-of select="ASSES_RESPONSE" /></td>
									<td><xsl:value-of select="ASSES_NOTES" /></td>
									</tr>
									</table>
								</xsl:if>
							</xsl:if>

					
					<xsl:if test="$FKDEPEN_PKVAL='0' or (DEPENT_QUES_PK!='0' and preceding-sibling::*[1]/DEPENT_QUES_PK=DEPENT_QUES_PK and RESPONSE_VAL12!='0')">
					<table border="1" bordercolor="black">
					<tr>
					
			
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<td><xsl:value-of select="DROPDOWN_VALUE" /></td>
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<td><xsl:value-of select="RESPONSE_VAL" /></td>
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
									<td><xsl:value-of select="RESPONSE_VAL" /></td>
								</xsl:if>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
				
		
					</tr>
					</table>
					</xsl:if>

					
					
					
					
					
					
				
			</xsl:if>
		</xsl:for-each>
		

	<br></br>
	<br></br>	
		
		
	

	
</xsl:for-each>


</xsl:template> 


</xsl:stylesheet>