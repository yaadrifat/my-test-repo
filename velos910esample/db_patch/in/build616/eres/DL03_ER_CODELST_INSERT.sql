--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'test_outcome'
    AND codelst_subtyp = 'indeter';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'test_outcome','indeter','Indeterminate','N',1,'Y',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--
