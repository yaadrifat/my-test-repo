update er_ctrp_draft set summ4_sponsor_type =
(select PK_CODELST from er_codelst where codelst_subtyp = 'indus')
where CTRP_DRAFT_TYPE = 1 and nvl(deleted_flag,0) =0;

commit;

declare
begin

	for rec in (Select pk_ctrp_draft from er_ctrp_draft where CTRP_DRAFT_TYPE = 0 
	and nvl(deleted_flag,0) = 0
	and summ4_sponsor_type not in (select PK_CODELST from er_codelst where 
	codelst_type = 'research_type'  and codelst_subtyp in('insti','national','other')))
	loop
		update er_ctrp_draft set summ4_sponsor_type = (select PK_CODELST from er_codelst where 
		PK_codelst = (select FK_CODELST_RESTYPE from er_study s where s.pk_study = fk_study)
		and codelst_type = 'research_type') where pk_ctrp_draft = rec.pk_ctrp_draft;
	end loop;
	commit;
end;
/