CREATE OR REPLACE TRIGGER "ERES"."CB_MULTIPLE_VALUES_AI1"  AFTER INSERT ON ERES.CB_MULTIPLE_VALUES 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
--Inserting row in AUDIT_ROW_MODULE table.
PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid, 'CB_MULTIPLE_VALUES', :NEW.rid,:NEW.PK_MULTIPLE_VALUES,'I',:NEW.Creator);

IF :NEW.PK_MULTIPLE_VALUES IS NOT NULL THEN	  
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'PK_MULTIPLE_VALUES',NULL,:NEW.PK_MULTIPLE_VALUES,NULL,NULL);
END IF;
IF :NEW.FK_CODELST_TYPE IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'FK_CODELST_TYPE',NULL,:NEW.FK_CODELST_TYPE,NULL,NULL);
END IF;
IF :NEW.FK_CODELST IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'FK_CODELST',NULL,:NEW.FK_CODELST,NULL,NULL);
END IF;
IF :NEW.ENTITY_ID IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ENTITY_ID',NULL,:NEW.ENTITY_ID,NULL,NULL);
END IF;
IF :NEW.ENTITY_TYPE IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ENTITY_TYPE',NULL,:NEW.ENTITY_TYPE,NULL,NULL);
END IF;
IF :NEW.CREATOR IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
END IF;
IF :NEW.CREATED_ON IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CREATED_ON',NULL,:NEW.CREATED_ON,NULL,NULL);
END IF;
IF :NEW.IP_ADD IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
END IF;
IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
END IF;
IF :NEW.RID IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'RID',NULL,:NEW.RID,NULL,NULL);
END IF;
IF :NEW.DELETEDFLAG IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'DELETEDFLAG',NULL,:NEW.DELETEDFLAG,NULL,NULL);
END IF;
IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
END IF;
END;
/