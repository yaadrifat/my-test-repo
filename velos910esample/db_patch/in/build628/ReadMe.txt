/* This readMe is specific to Velos eResearch version 9.0 build #628 */

=====================================================================================================================================
Garuda :
	1.E-MAIL Configuration:-
	
		A)For development team only:
		
			Please go to this place in CVS  

    	 	doc/tools/maven/Jars-garuda/
    	
    	 	and, after taking a  CVS update, 
    	 	
    	 	for WINDOWS:double-click on storeJarsInMavenForGaruda.bat
    		for Linux:double-click on storeJarsInMavenForGaruda.sh
    		
    	
		B)Place the two jar files  i.e activation.jar and mail.jar under the following directory:
		 	JBOSS_HOME/server/eresearch/lib/
		 
		 	Those two jar files  i.e activation.jar and mail.jar will be available under following directory:
		 	build628/External jar files/ 
		 	
		 	NOTE:if activation.jar and mail.jar already exist in the same location please remove or replace with updated one.
	
		C)Place the aithentmail_garuda.properties file under the following directory:
		 	JBOSS_HOME/server/eresearch/conf/
	
		 	The aithentmail_garuda.properties file will be available under the following directory:
		 	build628/External jar files/
	
		D)need to be modify aithentmail_garuda.properties file:
		-------------------------------------------------------
			INITIAL_CONTEXT_FACTORY=org.jnp.interfaces.NamingContextFactory
			PROVIDER_URL=jnp://<system_ip_address_of_jboss_server>:1099
			URL_PKG_PREFIXES =
			
			#mail will be sent if the value is true
			mailenable = true
			
			#Data Base connection for mail
			
			ORACLE_CONNECTION =jdbc:oracle:thin:@<current_DB_ip_address>:<corresponding_DB_port_number>:<SID_name>
			
			ORACLE_USER_NAME =<user_name>
			
			ORACLE_PASSWORD=<password>
						
	
	     
=====================================================================================================================================
eResearch:



=====================================================================================================================================
eResearch Localization:


=====================================================================================================================================
