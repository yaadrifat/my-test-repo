CREATE TABLE CB_NOTES(
PK_NOTES NUMBER(10) Primary Key,
ENTITY_ID NUMBER(10),
ENTITY_TYPE NUMBER(10),
FK_NOTES_TYPE NUMBER(10),
FK_NOTES_CATEGORY NUMBER(10),
NOTE_ASSESSMENT VARCHAR2(15),
AVAILABLE_DATE DATE, 
EXPLAINATION VARCHAR2(25), 
NOTES VARCHAR2(4000),
FK_REASON NUMBER(10),
KEYWORD VARCHAR2(100),
SEND_TO VARCHAR2(25),
VISIBILITY VARCHAR(1),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_ON DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_NOTES" IS 'This table will store the clinical notes entered for a cord';
COMMENT ON COLUMN "CB_NOTES"."PK_NOTES" IS 'Primary Key of the table'; 
COMMENT ON COLUMN "CB_NOTES"."ENTITY_ID" IS 'Id of the entity like PK_CORD etc, Doner etc'; 
COMMENT ON COLUMN "CB_NOTES"."ENTITY_TYPE" IS 'Type of entity'; 
COMMENT ON COLUMN "CB_NOTES"."FK_NOTES_TYPE" IS 'PK_CODLST for type of note (Clinical/Non Clinical) referenced from Code List(ER_CODELST)'; 
COMMENT ON COLUMN "CB_NOTES"."FK_NOTES_CATEGORY" IS 'Code for the category of note referenced from Code List(ER_CODELST)'; 
COMMENT ON COLUMN "CB_NOTES"."NOTE_ASSESSMENT" IS 'Code for assessment referenced from Code List(ER_CODELST)';	 
COMMENT ON COLUMN "CB_NOTES"."AVAILABLE_DATE" IS 'Date on which cord will be available. This date will be part of subsequent information after note assessment selection.'; 
COMMENT ON COLUMN "CB_NOTES"."EXPLAINATION" IS 'This column will store explantion for non availabilty of cord'; 
COMMENT ON COLUMN "CB_NOTES"."NOTES" IS 'This column will store notes entered for cord'; 
COMMENT ON COLUMN "CB_NOTES"."FK_REASON" IS 'This column will store reason for non avaliablity of cord referenced from Code List(ER_CODELST) '; 
COMMENT ON COLUMN "CB_NOTES"."KEYWORD" IS 'Any keywords associated with notes ';
COMMENT ON COLUMN "CB_NOTES"."SEND_TO" IS 'This column will store to whom note is sent to review or consult'; 
COMMENT ON COLUMN "CB_NOTES"."VISIBILITY" IS 'Wheather note is visible to transplant center users or not'; 
COMMENT ON COLUMN "CB_NOTES"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. '; 
COMMENT ON COLUMN "CB_NOTES"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_NOTES"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_NOTES"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_NOTES"."LAST_MODIFIED_ON" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_NOTES"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_NOTES"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE "SEQ_CB_NOTES" MINVALUE 1 MAXVALUE 999999999999999999999999999 
INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;


CREATE TABLE CB_ENTITY_STATUS(
PK_ENTITY_STATUS NUMBER(10) Primary key,
ENTITY_ID NUMBER(10),
FK_ENTITY_TYPE NUMBER(10),
STATUS_TYPE_CODE VARCHAR2(15),
FK_STATUS_VALUE NUMBER(10),
STATUS_DATE DATE,
STATUS_REMARKS VARCHAR2(200),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_ON DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_ENTITY_STATUS" IS 'This table used to store status values for entity types while editing';

COMMENT ON COLUMN "CB_ENTITY_STATUS"."PK_ENTITY_STATUS" IS 'Primary key.'; 
COMMENT ON COLUMN "CB_ENTITY_STATUS"."ENTITY_ID" IS 'Id of the entity like CBU, Doner etc';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."FK_ENTITY_TYPE" IS 'Type of entity';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."STATUS_TYPE_CODE" IS 'This column is used to categorize the status types stored in the table.';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."FK_STATUS_VALUE" IS 'Code for status value for different status types referenced from code list(ER_CODELST)';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."STATUS_DATE" IS 'Date on which status of the cord modified';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."STATUS_REMARKS" IS 'Explanations entered while editing the status';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."LAST_MODIFIED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
COMMENT ON COLUMN "CB_ENTITY_STATUS"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';

CREATE SEQUENCE "SEQ_CB_ENTITY_STATUS" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;



CREATE TABLE CB_ENTITY_STATUS_REASON(
PK_ENTITY_STATUS_REASON NUMBER(10) Primary key,
FK_ENTITY_STATUS NUMBER(10),
FK_REASON_ID NUMBER(10),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_ON DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_ENTITY_STATUS_REASON" IS 'This table used to store reason for status for every edit';

COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."PK_ENTITY_STATUS_REASON" IS 'Primary key.';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."FK_ENTITY_STATUS" IS 'Foriegn key reference from Entity status(CB_ENTITY_STATUS). ';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."FK_REASON_ID" IS 'Code for reason for a status referenced from code list(Er_CODELST)';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."LAST_MODIFIED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';

CREATE SEQUENCE "SEQ_CB_ENTITY_STATUS_REASON" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;
