-- INSERTING CODE LIST VALUES FOR MRQ REFERENCE CHART-
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'albania';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','albania','Albania','N',1,null,null,null,null ,sysdate,sysdate,null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'austria';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','austria','Austria','N',2,null,null,null,null ,sysdate,sysdate,null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'belgium';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','belgium','Belgium','N',3,null,null,null,null ,sysdate,sysdate,null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'bosnia';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','bosnia','Bosnia-Herzegovina','N',4,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'bulgaria';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','bulgaria','Bulgaria','N',5,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'croatia';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','croatia','Croatia','N',6,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'czech';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','czech','Czech Republic','N',7,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'denmark';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','denmark','Denmark','N',8,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'finland';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','finland','Finland','N',9,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'france';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','france','France','N',10,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'germany';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','germany','Germany','N',11,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'greece';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','greece','Greece','N',12,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'hungary';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','hungary','Hungary','N',13,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'ireland';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','ireland','Ireland (Republic of)','N',14,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'italy';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','italy','Italy','N',15,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'liechtenstein';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','liechtenstein','Liechtenstein','N',16,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'luxembourg';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','luxembourg','Luxembourg','N',17,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'macedonia';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','macedonia','Macedonia','N',18,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'netherlands';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','netherlands','Netherlands (Holland)','N',19,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'norway';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','norway','Norway','N',20,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'poland';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','poland','Poland','N',21,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'portugal';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','portugal','Portugal','N',22,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'romania';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','romania','Romania','N',23,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'slovak';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','slovak','Slovak Republic','N',24,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'slovenia';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','slovenia','Slovenia','N',25,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'spain';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','spain','Spain','N',26,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'sweden';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','sweden','Sweden','N',27,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'switzer';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','switzer','Switzerland','N',28,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'uk';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','uk','United Kindom: England, Northern Ireland, Scotland, Wales, the Isle of Man, the Channel Islands, Gibraltar, or the Falkland Islands','N',29,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'uyugo';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'chart_vCJD','uyugo','UYugoslavia (Federal Republic of), Kosovo, Montenegro, Serbia','N',30,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'benin';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','benin','Benin*','N',1,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'cameroon';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','cameroon','Cameroon','N',2,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'carepublic';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','carepublic','Central African Republic (previously named Central African Empire)','N',3,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'chad';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','chad','Chad','N',4,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'congo';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','congo','Congo (refers to Republic of the Congo) Note: This does not include the Democratic Republic of the Congo (previously named Zaire)','N',5,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'guinea';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','guinea','Equatorial Guinea (includes the territory of Rio Muni and the islands of Corisco, Elobey, Ferrando Po (Bioko) and Annonbon)','N',6,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'gabon';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','gabon','Gabon','N',7,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'kenya';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','kenya','Kenya*','N',8,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'niger';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','niger','Niger','N',9,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'nigeria';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','nigeria','Nigeria','N',10,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'sengal';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','sengal','Sengal','N',11,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'togo';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','togo','Togo*','N',12,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'zambia';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','zambia','Zambia*','N',13,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'note';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'risk_HIV1','note','*Countries added as African risk countries on 10-30-09 per FDA Guidance (8-09)','N',13,null,null,null,null,sysdate, sysdate, null,null,null,null);
  end if;
end;
/
--END--
commit;

set define off;
--Sql for Table ER_CODELST  for nmdp_broad_race code type
DECLARE
  value_exists number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='NAM';
	if (value_exists = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','NAM','American Indian or Alaska Native','N',1,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  end;
/


DECLARE
  VALUE_EXISTS1 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS1 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='API';
	if (VALUE_EXISTS1 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','API','Asian or Pacific Islander','N',2,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS2 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS2 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='AFA';
	if (VALUE_EXISTS2 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','AFA','Black or African American','N',3,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS3 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS3 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='DEC';
	if (VALUE_EXISTS3 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','DEC','Declines','N',4,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS4 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS4 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='HIS';
	if (VALUE_EXISTS4 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','HIS','Hispanic','N',5,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS5 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS5 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='OTH';
	if (VALUE_EXISTS5 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','OTH','Multiple Race','N',6,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS6 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS6 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='HAW';
	if (VALUE_EXISTS6 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','HAW','Native Hawaiian or Other Pacific Islander','N',7,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS7 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS7 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='UNK';
	if (VALUE_EXISTS7 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','UNK','Unknown','N',8,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS8 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS8 FROM ER_CODELST WHERE CODELST_TYPE='nmdp_broad_race' AND CODELST_SUBTYP ='CAU';
	if (VALUE_EXISTS8 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'nmdp_broad_race','CAU','White','N',9,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

set define off;
--Sql for Table ER_CODELST  for race_roll_up codetype
DECLARE
  value_exists number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='NAM';
	if (value_exists = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','NAM','American Indian or Alaska Native','N',1,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  end;
/


DECLARE
  VALUE_EXISTS1 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS1 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='API';
	if (VALUE_EXISTS1 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','API','Asian or Pacific Islander','N',2,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS2 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS2 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='AFA';
	if (VALUE_EXISTS2 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','AFA','Black or African American','N',3,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS3 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS3 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='DEC';
	if (VALUE_EXISTS3 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','DEC','Declines','N',4,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS4 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS4 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='HIS';
	if (VALUE_EXISTS4 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','HIS','Hispanic','N',5,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS5 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS5 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='OTH';
	if (VALUE_EXISTS5 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','OTH','Multiple Race','N',6,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS6 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS6 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='HAW';
	if (VALUE_EXISTS6 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','HAW','Native Hawaiian or Other Pacific Islander','N',7,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS7 number := 0;  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS7 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP ='UNK';
	if (VALUE_EXISTS7 = 0) then
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) VALUES
	    (SEQ_ER_CODELST.nextval,null,'race_roll_up','UNK','Unknown','N',8,'Y',null,
	    NULL,NULL,SYSDATE,SYSDATE,NULL,NULL,NULL,NULL);
    commit;
	 end if;
  END;
/

DECLARE
  VALUE_EXISTS8 number(5);  
  BEGIN
  SELECT COUNT(*) INTO VALUE_EXISTS8 FROM ER_CODELST WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP='cau';
	IF (VALUE_EXISTS8 =1) THEN
    UPDATE ER_CODELST SET CODELST_DESC='WHITE', CODELST_SEQ=9 WHERE CODELST_TYPE='race_roll_up' AND CODELST_SUBTYP='cau';
COMMIT;
	 END IF;   
  END;
/
