--STARTS MODIFYING COLUMN CURRENT_DIAGNOSIS TO CB_RECEIPANT_INFO TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_RECEIPANT_INFO' AND COLUMN_NAME = 'CURRENT_DIAGNOSIS';
IF (v_column_exists = 1) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_RECEIPANT_INFO MODIFY(CURRENT_DIAGNOSIS VARCHAR2(50 CHAR))';
END IF;
END;
/
--END
