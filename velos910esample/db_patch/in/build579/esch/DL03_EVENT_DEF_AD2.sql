set define off;
create or replace TRIGGER esch."EVENT_DEF_AD2" AFTER DELETE ON EVENT_DEF
REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
WHEN ( old.event_type='P')
declare
	pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_DEF_AD2', pLEVEL  => Plog.LFATAL);
begin
	-- YK 09May2011 Bug# 5821 $ #5818
	--update SCH_SUBCOST_ITEM SET SUBCOST_ITEM_DELFLAG ='Y' where FK_CALENDAR = :old.event_id;
	DELETE FROM SCH_SUBCOST_ITEM where FK_CALENDAR = :old.event_id;
	exception when others then
		plog.fatal(pctx,'Exception:'||sqlerrm);
end;
/