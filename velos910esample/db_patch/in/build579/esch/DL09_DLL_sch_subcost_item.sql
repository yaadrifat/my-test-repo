CREATE TABLE "ESCH"."SCH_SUBCOST_ITEM" 
   (	"PK_SUBCOST_ITEM" NUMBER NOT NULL ENABLE, 
	"FK_CALENDAR" NUMBER, 
	"SUBCOST_ITEM_NAME" VARCHAR2(250 BYTE), 
	"SUBCOST_ITEM_COST" NUMBER DEFAULT 0, 
	"SUBCOST_ITEM_UNIT" NUMBER DEFAULT 0, 
	"SUBCOST_ITEM_SEQ" NUMBER DEFAULT 0, 
	"FK_CODELST_CATEGORY" NUMBER, 
	"FK_CODELST_COST_TYPE" NUMBER, -- YK 09May2011 Bug# 5821 $ #5818
	"RID" NUMBER, 
	"CREATOR" NUMBER, 
	"CREATED_ON" DATE DEFAULT sysdate, 
	"LAST_MODIFIED_BY" NUMBER, 
	"LAST_MODIFIED_DATE" DATE DEFAULT NULL,
	"IP_ADD" VARCHAR2(15 BYTE), 
	 CONSTRAINT "PK_SCH_SUBCOST_ITEM" PRIMARY KEY ("PK_SUBCOST_ITEM"));
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."PK_SUBCOST_ITEM" IS 'Primary Key';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."FK_CALENDAR" IS 'This column stores the calendar id for which the subject cost items are defined. FK to event_assoc/event_def';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."SUBCOST_ITEM_NAME" IS 'This column stores the Subject Cost Item Name';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."SUBCOST_ITEM_COST" IS 'This column stores the Unit cost';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."SUBCOST_ITEM_UNIT" IS 'This column stores the No. of Units';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."SUBCOST_ITEM_SEQ" IS  'Stores sequence of subject cost items within its section';
   
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."FK_CODELST_CATEGORY" IS 'This column stores the Subject Cost item category, like Patient Care';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."FK_CODELST_COST_TYPE" IS 'This column stores cost type for the lineitem. FK to sch_codelst';
   
   -- YK 09May2011 Bug# 5821 $ #5818

   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. Stores PK of table ER_USER.';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."LAST_MODIFIED_BY" IS 'The last_modified_by identifies the user who last modified this row. Stores PK of table ER_USER.';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "ESCH"."SCH_SUBCOST_ITEM"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 

create or REPLACE synonym eres.SCH_SUBCOST_ITEM for esch.SCH_SUBCOST_ITEM;

GRANT ALL ON ESCH.SCH_SUBCOST_ITEM TO eres;

GRANT ALL ON ESCH.SCH_SUBCOST_ITEM TO epat;
 