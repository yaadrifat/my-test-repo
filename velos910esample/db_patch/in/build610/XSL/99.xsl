<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByForm" match="ROW" use="FORM_NAME" />
<xsl:key name="RecordsByFormField" match="ROW" use="concat(FORM_NAME, ' ', FA_SYSTEMID, ' ', FA_DATETIME)" />

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:param name="mode"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

</HEAD>


<SCRIPT  src="validations.js" LANGUAGE="JavaScript" ></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/prototype.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/dojo/dojo.js"></SCRIPT>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
<script>
function  validate(formobj){

 if (!(validate_col('eSign',formobj.eSign))) return false
}
var lastTimeSubmitted = 0;
function fnOnceEnterKeyPress(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted &lt;= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}
</script>

<BODY class="repBody">
<form method="post" name="audit" action="updateReason.jsp" id="auditForm"  onsubmit = "return validate(document.audit)">

<xsl:if test="$mode=''">

	<xsl:if test="$cond='T'">
	<table width="100%" >
	<tr class="reportGreyRow">
	<td class="reportPanel"> 
	VELMESSGE[M_Download_ReportIn]:<!-- Download the report in --> 
	<A href='{$wd}' >
	VELLABEL[L_Word_Format]<!-- Word Format -->
	</A> 
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<A href='{$xd}' >
	VELLABEL[L_Excel_Format]<!-- Excel Format -->
	</A>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<A href='{$hd}' >
	VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
	</A> 
	</td>
	</tr>
	</table>
	</xsl:if>
</xsl:if>
	
<xsl:apply-templates select="ROWSET"/>

<xsl:if test="$mode='E'">
<table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cccccc" >
	<tr bgcolor="#cccccc">
	  <td width="50%" align="right" bgcolor="#cccccc">
		 <span id="eSignMessage"></span>
				VELLABEL[L_Esignature]<!-- e-Signature --> <FONT class="Mandatory">* </FONT>
 			  
	   </td>

	   <td width="20%" bgcolor="#cccccc"><input type="password" name="eSign" id="eSign" maxlength="8"
	   onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','VELLABEL[L_Valid_Esign]','VELLABEL[L_Invalid_Esign]','sessUserId')" onkeypress="return fnOnceEnterKeyPress(event)"/>	  	    </td>	
	       
     	
		<td width="20%" bgcolor="#cccccc"> 
		
		<button id="submit_btn" type="submit">VELLABEL[L_Submit]</button>
	    
	  </td>
  </tr> 
</table>
</xsl:if>

</form>

</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>
<hr class="thickLine" />
<xsl:for-each select="ROW[count(. | key('RecordsByForm', FORM_NAME)[1])=1]">
<xsl:text>&#xa;</xsl:text>
<TABLE WIDTH="100%" >

<xsl:if test="FORM_TYPE='P'">
<TR>
<TD class="reportGrouping">
VELLABEL[L_Patients_StudyId]:<!-- Patient's Study ID --> <xsl:value-of select="PATPROT_PATSTDID" />
</TD>
</TR>
</xsl:if>

<xsl:if test="FORM_TYPE='S'">
<TR>
<TD class="reportGrouping">
VELLABEL[L_Study_Number]:<!-- Study Number --> <xsl:value-of select="STUDYNUM" />
</TD>
</TR>
</xsl:if>

<TR>
<TD class="reportGrouping">
VELLABEL[L_Frm_Name]:<!-- Form Name -->: <xsl:value-of select="FORM_NAME" />
</TD>
</TR>

<TR>
<TD class="reportGrouping">
VELLABEL[L_Forms_IdDt]:<!-- Form's ID Date -->: <xsl:value-of select="FORMS_FILLDATE" />
</TD>
</TR>

</TABLE>



<hr class="thinLine" />

<TABLE WIDTH="100%" BORDER="1">
<TR>
<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER">
VELLABEL[L_Fld_Name]<!-- Field name -->
</TH>
<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER">
VELLABEL[L_Modified_On]<!-- Modified On -->
</TH>
<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER">
VELLABEL[L_Modified_By]<!-- Modified By -->
</TH>
<TH class="reportHeading" WIDTH="20%" ALIGN="CENTER">
VELLABEL[L_Old_Value]<!-- Old Value -->
</TH>
<TH class="reportHeading" WIDTH="20%" ALIGN="CENTER">
VELLABEL[L_New_Value]<!-- New Value -->
</TH>
<TH class="reportHeading" WIDTH="20%" ALIGN="CENTER">
VELLABEL[L_Reason_ForChange]<!-- Reason for Change -->
</TH>
</TR>

<xsl:variable name="str" select="key('RecordsByForm', FORM_NAME)" />
<xsl:for-each select="$str[count(. | key('RecordsByFormField', concat(FORM_NAME, ' ', FA_SYSTEMID, ' ', FA_DATETIME))[1])=1]">
<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >
<TR class="reportEvenRow">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="FA_FLDNAME" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="FA_DATETIME" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="FA_MODIFIEDBY_NAME" />
</TD>
<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="FA_OLDVALUE" />
</TD>
<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="FA_NEWVALUE" />
</TD>
<TD class="reportData" WIDTH="20%" >
 
 <xsl:choose>
		<xsl:when  test="$mode='E'">
		<Textarea name="FA_REASON"><xsl:value-of select="FA_REASON" />
			</Textarea>
		 
 			<input type="hidden" name="pk_formaudit">
				<xsl:attribute name="value"><xsl:value-of select="PK_FORMAUDITCOL" /></xsl:attribute>
			</input>


		</xsl:when>
	 
		<xsl:otherwise>
			<xsl:value-of select="FA_REASON" />
		</xsl:otherwise>
</xsl:choose>


 

</TD>
</TR>
</xsl:when> 
<xsl:otherwise>
<TR class="reportOddRow">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="FA_FLDNAME" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="FA_DATETIME" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="FA_MODIFIEDBY_NAME" />
</TD>
<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="FA_OLDVALUE" />
</TD>
<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="FA_NEWVALUE" />
</TD>
<TD class="reportData" WIDTH="20%" >


 <xsl:choose>
		<xsl:when  test="$mode='E'">
			<Textarea name="FA_REASON"><xsl:value-of select="FA_REASON" />
			</Textarea>
			
			 
 			<input type="hidden" name="pk_formaudit">
				<xsl:attribute name="value"><xsl:value-of select="PK_FORMAUDITCOL" /></xsl:attribute>
			</input>


		</xsl:when>
	 
		<xsl:otherwise>
			<xsl:value-of select="FA_REASON" />
		</xsl:otherwise>
</xsl:choose>



</TD>

</TR>
</xsl:otherwise>
</xsl:choose> 
</xsl:for-each>
</TABLE>

</xsl:for-each>
<hr class="thickLine" />
<TABLE WIDTH="100%" >
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>