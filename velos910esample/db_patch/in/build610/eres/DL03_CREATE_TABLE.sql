set define off;

CREATE TABLE ER_ORDER_HEADER(
	PK_ORDER_HEADER NUMBER(10,0) PRIMARY KEY,
	ORDER_GUID VARCHAR2(50),
	ORDER_NUM VARCHAR2(50),
	ORDER_ENTITYID NUMBER(10,0),
	ORDER_ENTITYTYPE NUMBER(10,0),
	ORDER_OPEN_DATE DATE,
	ORDER_CLOSE_DATE DATE,
	FK_ORDER_TYPE NUMBER(10,0),
	FK_ORDER_STATUS NUMBER(10,0),
	ORDER_STATUS_DATE DATE,
	ORDER_REMARKS VARCHAR2(255),
	ORDER_REQUESTED_BY NUMBER(10,0),
	ORDER_APPROVED_BY NUMBER(10,0),
	CREATOR NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15),
	DELETEDFLAG NUMBER(2),
	RID NUMBER(10,0)
);

COMMENT ON TABLE ER_ORDER_HEADER IS 'Stores order details which comes through ESB';

COMMENT ON COLUMN ER_ORDER_HEADER.PK_ORDER_HEADER IS 'Stores sequence generated unique value as primary key';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_GUID IS 'Stores global unique id';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_NUM IS 'Stores unique user friendly order number';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_ENTITYID IS 'Stores foreign key references based on entity type.(ex:''PK_CORD'',''PK_DONAR'')';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_ENTITYTYPE IS 'Stores pk_codelst value based on entity type.(ex:''CBU'',''DONAR'')';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_OPEN_DATE IS 'Stores The date when the Case Management resources started working on the Order';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_CLOSE_DATE IS 'The close date of the Order';
COMMENT ON COLUMN ER_ORDER_HEADER.FK_ORDER_TYPE IS 'Stores pk_codelst value for order type.(ex:''CUSTOMER ORDER'',''PRODUCT FULFILLMENT ORDER'')';
COMMENT ON COLUMN ER_ORDER_HEADER.FK_ORDER_STATUS IS 'Stores pk_codelst value for order status (ex:''CLOSED'',''RESOLVED'',''PENDING'',''REJECTED'',''OPEN'')';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_STATUS_DATE IS 'Stores current order status date';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_REMARKS IS 'The comments for the Order';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_REQUESTED_BY IS 'foreign key reference to er_user table. stores who requested the order';
COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_APPROVED_BY IS 'foreign key reference to er_user table. stores who approved the order';
COMMENT ON COLUMN ER_ORDER_HEADER.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN ER_ORDER_HEADER.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN ER_ORDER_HEADER.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN ER_ORDER_HEADER.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN ER_ORDER_HEADER.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN ER_ORDER_HEADER.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN ER_ORDER_HEADER.RID IS 'Uses for Audit trail. Stores sequence generated unique value';


CREATE SEQUENCE ERES.SEQ_ER_ORDER_HEADER MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;

CREATE TABLE ER_ORDER_INSTRUCTIONS(
	PK_ORDER_INSTRUCTIONS NUMBER(10,0) PRIMARY KEY,
	FK_ER_ORDER NUMBER(10,0),
	INSTRUCTION_CLOB CLOB,
	CREATOR NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15),
	DELETEDFLAG NUMBER(2),
	RID NUMBER(10,0)
	);

COMMENT ON TABLE ER_ORDER_INSTRUCTIONS IS 'Stores order instructions data whcih comes through ESB';

COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.PK_ORDER_INSTRUCTIONS IS 'Stores sequence generated unique value as primary key';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.FK_ER_ORDER IS 'Stores foreign key referecne to ER_ORDER table to map with order';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.INSTRUCTION_CLOB IS 'Stores instructions description';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN ER_ORDER_INSTRUCTIONS.RID IS 'Uses for Audit trail. Stores sequence generated unique value';


CREATE SEQUENCE ERES.SEQ_ER_ORDER_INSTRUCTIONS MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;


--table for Order Options

CREATE TABLE ER_ORDER_OPTIONS(
	PK_ORDER_OPTIONS NUMBER(10,0) PRIMARY KEY,
	FK_ER_ORDER NUMBER(10,0),
	FK_OPTION_CODE NUMBER(10,0),
	FK_OPTION_VALUE_CODE NUMBER(10,0),
	CREATOR NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15),
	DELETEDFLAG NUMBER(2),
	RID NUMBER(10,0)
);

COMMENT ON TABLE ER_ORDER_OPTIONS IS 'Stores order options data which comes through ESB message';
COMMENT ON COLUMN ER_ORDER_OPTIONS.PK_ORDER_OPTIONS IS 'Stores sequence generated unique value as primary key';
COMMENT ON COLUMN ER_ORDER_OPTIONS.FK_ER_ORDER IS 'Stores foreign key reference to ER_ORDER table to map with order';
COMMENT ON COLUMN ER_ORDER_OPTIONS.FK_OPTION_CODE IS 'Stores pk_codelst values for order options';
COMMENT ON COLUMN ER_ORDER_OPTIONS.FK_OPTION_VALUE_CODE  IS 'Stores pk_codelst values for option value';
COMMENT ON COLUMN ER_ORDER_OPTIONS.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN ER_ORDER_OPTIONS.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN ER_ORDER_OPTIONS.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN ER_ORDER_OPTIONS.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN ER_ORDER_OPTIONS.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN ER_ORDER_OPTIONS.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN ER_ORDER_OPTIONS.RID IS 'Uses for Audit trail. Stores sequence generated unique value';

CREATE SEQUENCE ERES.SEQ_ER_ORDER_OPTIONS MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;

--table for Order Tracking

CREATE TABLE ER_ORDER_TRACKING(
	PK_ORDER_TRACKING NUMBER(10,0) PRIMARY KEY,
	FK_ER_ORDER NUMBER(10,0),
	TRACKING_CODE NUMBER(10,0),
	TRACKING_VALUE_DATE TIMESTAMP(0),
	TRACKING_VALUE_NUM NUMBER(9,2),
	TRACKING_VALUE_TEXT VARCHAR2(255),
	CREATOR NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15),
	DELETEDFLAG NUMBER(2),
	RID NUMBER(10,0)
);

COMMENT ON TABLE ER_ORDER_TRACKING IS 'Stores order tracking details which comes from ESB message';
COMMENT ON COLUMN ER_ORDER_TRACKING.PK_ORDER_TRACKING IS 'Stores sequence generated unique value as primary key';
COMMENT ON COLUMN ER_ORDER_TRACKING.FK_ER_ORDER IS 'Stores foreign key reference to ER_ORDER table to map with order';
COMMENT ON COLUMN ER_ORDER_TRACKING.TRACKING_CODE IS 'Stores pk_codelst value for order tracking';
COMMENT ON COLUMN ER_ORDER_TRACKING.TRACKING_VALUE_DATE IS 'Stores order tracking value date';
COMMENT ON COLUMN ER_ORDER_TRACKING.TRACKING_VALUE_NUM IS 'Stores order tracking value number';
COMMENT ON COLUMN ER_ORDER_TRACKING.TRACKING_VALUE_TEXT IS 'Stores order tracking value text/description';
COMMENT ON COLUMN ER_ORDER_TRACKING.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN ER_ORDER_TRACKING.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN ER_ORDER_TRACKING.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN ER_ORDER_TRACKING.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN ER_ORDER_TRACKING.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN ER_ORDER_TRACKING.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN ER_ORDER_TRACKING.RID IS 'Uses for Audit trail. Stores sequence generated unique value';

CREATE SEQUENCE ERES.SEQ_ER_ORDER_TRACKING MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;


commit;