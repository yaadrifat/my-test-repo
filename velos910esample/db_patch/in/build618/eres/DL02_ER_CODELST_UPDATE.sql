--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'product_modi'
    AND codelst_subtyp = 'other';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Other' where codelst_type = 'product_modi'AND codelst_subtyp = 'other';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RSO';
  if (v_record_exists = 1) then
	UPDATE CB_CBU_STATUS SET DELETEDFLAG='1' where CBU_STATUS_CODE = 'RSO';
	commit;
  end if;
end;
/
--END--
