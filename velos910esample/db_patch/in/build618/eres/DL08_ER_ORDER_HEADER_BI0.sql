create or replace TRIGGER "ERES".ER_ORDER_HEADER_BI0 BEFORE INSERT ON ER_ORDER_HEADER
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_ORDER_HEADER',erid, 'I',usr);

  INSERT_DATA:=:NEW.PK_ORDER_HEADER || '|' ||
to_char(:NEW.ORDER_GUID) || '|' ||
to_char(:NEW.ORDER_NUM) || '|' ||
to_char(:NEW.ORDER_ENTITYID) || '|' ||
to_char(:NEW.ORDER_ENTITYTYPE) || '|' ||
to_char(:NEW.ORDER_OPEN_DATE) || '|' ||
to_char(:NEW.ORDER_CLOSE_DATE) || '|' ||
to_char(:NEW.FK_ORDER_TYPE) || '|' ||
to_char(:NEW.FK_ORDER_STATUS) || '|' ||
to_char(:NEW.ORDER_STATUS_DATE) || '|' ||
to_char(:NEW.ORDER_REMARKS) || '|' ||
to_char(:NEW.ORDER_REQUESTED_BY) || '|' ||
to_char(:NEW.ORDER_APPROVED_BY) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.FK_SITE_ID);
INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
END;
/