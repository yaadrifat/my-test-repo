--STARTS INSERTING RECORD INTO ER_LABGROUP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABGROUP
    where GROUP_NAME = 'IDMOthers'
    AND GROUP_TYPE = 'IOG';
  if (v_record_exists = 0) then
     INSERT INTO ER_LABGROUP(PK_LABGROUP,GROUP_NAME,GROUP_TYPE) VALUES(SEQ_ER_LABGROUP.nextval,'IDMOthers','IOG');
	commit;
  end if;
end;
/
--END--
