set define off;
create or replace
PACKAGE      "PKG_FORMREP"
AS
      PROCEDURE SP_GENFORMLAYOUT(P_FORM IN NUMBER);
       PROCEDURE SP_GENFORMLAYOUT(P_FORM IN NUMBER, P_FORMTYPE IN VARCHAR2, O_SUCCESS OUT VARCHAR2);
    PROCEDURE SP_PROCESSFORMDATA;
    --KM-#3919
    PROCEDURE SP_GET_DATA_COLUMN_SQL(P_FORM IN NUMBER, P_FORMTYPE IN VARCHAR2, P_FORMLIBVER IN NUMBER, O_LIST OUT CLOB, O_DATACOL_COUNT OUT NUMBER);
    PROCEDURE SP_INSERT_ACCTFORM_EXP(p_form IN NUMBER, p_account IN NUMBER, p_formxml IN CLOB,
    p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2,   p_datastr IN VARCHAR2,
    p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER, p_filledform IN NUMBER, o_ret OUT NUMBER, p_imp IN NUMBER);
    PROCEDURE SP_INSERT_STUDYFORM_EXP(p_form IN NUMBER, p_study IN NUMBER, p_formxml IN CLOB,
    p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2,
    p_datastr IN VARCHAR2, p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER, p_filledform IN NUMBER,
    o_ret OUT NUMBER, p_imp IN NUMBER);
    PROCEDURE SP_INSERT_PATFORM_EXP(p_form IN NUMBER, p_pat IN NUMBER, p_patprot IN NUMBER, p_formxml IN CLOB,
    p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2,  p_datastr IN VARCHAR2,
    p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER, p_filledform IN NUMBER,p_customcol IN VARCHAR2 ,o_ret OUT NUMBER, p_imp IN NUMBER);
    PROCEDURE SP_EXPTOFORMXML;
    PROCEDURE sp_updatexml(p_xml sys.XMLTYPE, p_values TYPES.SMALL_STRING_ARRAY, o_updatedclob OUT CLOB);
    PROCEDURE sp_parse_list (p_mainstring VARCHAR2,  o_list OUT TYPES.SMALL_STRING_ARRAY );
    pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_FORMREP', pLEVEL  => Plog.LFATAL);
END;
/

create or replace
PACKAGE BODY      Pkg_Formrep AS
    PROCEDURE SP_GENFORMLAYOUT(P_FORM IN NUMBER)
	AS
	  v_formtype_disp VARCHAR2(5);
  	  v_formtype VARCHAR2(5);
	  v_success NUMBER;
	  v_html CLOB;
	  BEGIN
		   	--delete existing data , if any Sonia - 7 apr 04

			-- sonia 7th july, do not delete old data, we will update it while generating mapform.

			--DELETE FROM
			--ER_MAPFORM
			--WHERE fk_form = p_form;



			SELECT  trim(LF_DISPLAYTYPE)
			INTO  v_formtype_disp
			FROM ER_LINKEDFORMS
			WHERE fk_formlib = p_form;
			-- call PKG_FORM.SP_CLUBFORMXSL to generate form XML and XSL
			Pkg_Form.SP_CLUBFORMXSL(p_form, v_html);
			IF v_formtype_disp = 'S' OR v_formtype_disp = 'SA' THEN
			    v_formtype := 'S';
			ELSIF  v_formtype_disp = 'SP' OR v_formtype_disp = 'PA' OR v_formtype_disp = 'PS' OR v_formtype_disp = 'PR'  THEN
			    v_formtype := 'P';
			ELSE
				v_formtype := v_formtype_disp;
			END IF;
			SP_GENFORMLAYOUT(P_FORM, v_formtype, V_SUCCESS);
	  END;
	PROCEDURE SP_GENFORMLAYOUT(P_FORM IN NUMBER, P_FORMTYPE IN VARCHAR2, O_SUCCESS OUT VARCHAR2)
	AS
	  v_orig_count NUMBER;
	  v_rep_count NUMBER;
	  v_tot NUMBER;
      i NUMBER;
	  v_name VARCHAR2(2000);
	  v_keyword VARCHAR2(500);
	  v_uid VARCHAR2(250);
	  v_browser VARCHAR2(2);
	  v_type VARCHAR2(2);
	  v_origsysid VARCHAR2(50);
	  v_systemid VARCHAR2(50);
	  v_pkfld NUMBER;
	  v_fldcharsno NUMBER;
	  o_html CLOB;
	  v_formxml sys.XMLTYPE;
	  v_mapcolname VARCHAR(100);
	  v_pksec NUMBER;
	  v_secname VARCHAR2(250);
	  v_count NUMBER;
	  v_fldseq NUMBER;
	  v_prev_mapformcount NUMBER;
	  v_repeatflag  NUMBER;
	  v_insert BOOLEAN ;
	  v_fld_in_mapform NUMBER;
	  v_newver_counter NUMBER;
      v_sysid_count NUMBER;

	BEGIN
    --get the form xml
	SELECT form_xml
	INTO v_formxml
	FROM ER_FORMLIB
	WHERE pk_formlib = p_form;
	-- implement null check , sonia
	IF v_formxml IS NOT NULL THEN
		 -- find total number of fields in XMl
	 	SELECT COUNT(*)
		INTO v_orig_count
		FROM erv_formflds
		WHERE  pk_formlib = p_form AND FLD_TYPE <> 'C' AND FLD_TYPE  <> 'H' ;

		SELECT COUNT(*)
		INTO v_rep_count
		FROM ER_REPFORMFLD, ER_FORMSEC, ER_FLDLIB
		WHERE ER_FORMSEC.fk_formlib = p_form AND
		ER_REPFORMFLD.fk_formsec = pk_formsec AND
		NVL(ER_REPFORMFLD.record_type,'D') <> 'D' AND
		ER_FLDLIB.pk_field = fk_field AND FLD_TYPE <> 'C' AND FLD_TYPE  <> 'H' ;
	 	v_tot := v_rep_count +  v_orig_count;

		-- get v_prev_mapformcount

		SELECT COUNT(*)
		INTO v_prev_mapformcount
		FROM ER_MAPFORM
		WHERE fk_form = p_form;



		--insert into t values('v_tot' || v_tot);
		-- call  PKG_FORM.SP_CLUBFORMXSL to refresh form xsl/xml
		--(dont need it if u execute from process data :
		--PKG_FORM.SP_CLUBFORMXSL(p_form, o_html);
		 v_count := 0;
		 v_newver_counter := v_prev_mapformcount;

			-- iterate throughthe xml
		    FOR i IN 1..v_tot
		      LOOP
			   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@name')),1,200),
			   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@keyword')),1,255),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@uid')),1,50),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@browser')),1,2),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@type')),1,2),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@origsysid')),1,50),
		   	   --substr(sys.xmlType.getStringVal(sys.xmlType.extract(v_formxml,'//rowset/*[' || i || ']/@systemid')),1,50)
			   SELECT
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@name').getStringVal(),
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@keyword').getStringVal(),
		  	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@uid').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@browser').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@type').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@origsysid').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@systemid').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@pksec').getStringVal(),
		   	   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@secname').getStringVal(),
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@pkfld').getStringVal(),
			   sys.XMLTYPE.EXTRACT(v_formxml,'//rowset/*[' || i || ']/@fldcharsno').getStringVal()
		   	   INTO  v_name, v_keyword, v_uid, v_browser,v_type,v_origsysid,v_systemid,v_pksec,v_secname,v_pkfld,v_fldcharsno
			   FROM dual;

			   --added by sonia, 10/01/04 to un escape special characters in section name
			   v_secname := Pkg_Util.f_unescapeSpecialCharsForXML(v_secname);
			   v_name:=Pkg_Util.f_unescapeSpecialCharsForXML(v_name);
			   v_keyword:=Pkg_Util.f_unescapeSpecialCharsForXML(v_keyword);
			   v_uid:=Pkg_Util.f_unescapeSpecialCharsForXML(v_uid);



			   IF v_type <> 'ML' THEN

			  	  v_insert := FALSE;

			     IF v_prev_mapformcount <= 0 THEN
  				   v_count := v_count + 1;
				   v_mapcolname := 'col' || v_count;
				   v_insert := TRUE;
				  ELSE -- check if the field is new for this version or if it already exists

				  	   SELECT  COUNT(*)
					   INTO v_fld_in_mapform FROM ER_MAPFORM
					   WHERE fk_form = p_form AND MP_PKFLD = v_pkfld;

					   --
					   IF v_fld_in_mapform <= 0 THEN
					       	v_insert := TRUE; -- set insert flag
							v_newver_counter :=  v_newver_counter + 1;
		  				    v_count := v_newver_counter;
							v_mapcolname := 'col' || v_newver_counter;
						ELSE
						   	v_insert := FALSE; -- field exits, do not change sequence, col name just change  other values
					   END IF;


			     END IF;

				 -- get the sequence numbers for the fields, update to mapform

				  BEGIN
	  				 SELECT formfld_seq INTO v_fldseq FROM ER_FORMFLD WHERE fk_field=v_pkfld;
				  EXCEPTION WHEN NO_DATA_FOUND THEN
				  SELECT repformfld_seq INTO v_fldseq FROM ER_REPFORMFLD WHERE fk_field=v_pkfld;
				  END;



                -- changed 06/10/08 for eliminating duplicates in er_mapform

				 IF v_insert = TRUE THEN
                    Plog.DEBUG(pCTX,'p_form='|| p_form||',v_systemid='||v_systemid);
                   select count(*) into v_sysid_count from er_mapform where mp_systemid=v_systemid and fk_form=p_form;
                    Plog.DEBUG(pCTX,'v_sysid_count='||v_sysid_count);
                   IF v_sysid_count=0 THEN
                    Plog.DEBUG(pCTX,'inserting');

				    INSERT INTO ER_MAPFORM  (PK_MP,
				   		  	   			    FK_FORM,
											MP_FORMTYPE,
											MP_MAPCOLNAME,
											MP_SYSTEMID,
											MP_KEYWORD,
											MP_UID,
											MP_DISPNAME,
											MP_ORIGSYSID,
											MP_FLDDATATYPE,
											MP_BROWSER,MP_SEQUENCE,MP_PKSEC,MP_SECNAME,MP_PKFLD,MP_FLDCHARSNO,MP_FLDSEQNO)
				   VALUES (SEQ_ER_MAPFORM.NEXTVAL,p_form,p_formtype,
				   v_mapcolname ,
				   v_systemid,v_keyword,v_uid,v_name,v_origsysid,
				   v_type,TO_CHAR(v_browser),v_count,v_pksec,v_secname,v_pkfld,v_fldcharsno,v_fldseq);
                   COMMIT;
                  END IF;
				 ELSE
                 Plog.DEBUG(pCTX,'updating');
				 	 -- update the old record
					UPDATE ER_MAPFORM
					SET  MP_FORMTYPE = p_formtype,MP_SYSTEMID = v_systemid,	MP_KEYWORD =v_keyword,
						MP_UID = v_uid,	MP_DISPNAME =v_name ,MP_ORIGSYSID = v_origsysid,
						MP_FLDDATATYPE = v_type, MP_BROWSER = TO_CHAR(v_browser),
						MP_PKSEC = v_pksec,
						MP_SECNAME = v_secname,
						MP_FLDCHARSNO = v_fldcharsno,
						MP_FLDSEQNO=v_fldseq
						WHERE 	FK_form = p_form AND
					MP_PKFLD = v_pkfld ;

                    COMMIT;

				 END IF; -- for v_insert = true


			 END IF;
		    END LOOP;

	 ELSE
		 P('NULL XML: Record not processed');
	 END IF; -- end of null check
 END;

 PROCEDURE Sp_Processformdata
  AS
  	v_oldxml sys.XMLTYPE;
	v_newxml sys.XMLTYPE;
	v_filledformpk NUMBER;
	v_form NUMBER;
	v_recordtype CHAR(1);
	v_fftdate DATE;
	v_filledformtype VARCHAR2(10);
	v_creator NUMBER;
	v_lastmodified_by NUMBER;
	v_created_on DATE;
	v_lastmodified_date DATE;
	v_ipadd VARCHAR2(15);
	v_rep_count NUMBER;
	v_orig_count NUMBER;
	v_tot NUMBER;
	v_fldvalue_prefix VARCHAR2(4000);
	v_fldvalue_suffix VARCHAR2(4000);
	v_fldvalue VARCHAR2(32000);
	v_fldvaluedata_prefix VARCHAR2(4000);
	v_fldvaluedata_suffix VARCHAR2(4000);
	v_fldvaluedata VARCHAR2(32000);
	v_ins_sqlstr LONG;
	v_ins_sqlstrdata LONG;
	v_values_sqlstr  VARCHAR2(32000);
	v_values_sqlstrdata  VARCHAR2(32000);
	o_rows NUMBER;
	v_ins_sql  LONG;
	v_ins_sqldata LONG;
	v_linpk NUMBER;
	v_fld_datatype VARCHAR(2);
	v_repcount NUMBER;
	v_respval VARCHAR2(32000);
	v_respdataval VARCHAR2(32000);
	v_mapcount NUMBER;
	o_layout VARCHAR2(100);
	v_pk_fft NUMBER;
	v_id NUMBER;
	v_fk_patprot NUMBER;
	-- for audit taril
v_old_fld_value_prefix VARCHAR2(4000);
	v_old_fld_value_suffix VARCHAR2(4000);
	v_old_fld_value VARCHAR2(32000);
	v_old_fld_valuedata VARCHAR2(32000);
	v_old_respval VARCHAR2(32000);
	v_old_respvaldata VARCHAR2(32000);
	v_old_repcount NUMBER;
	v_fld_systemid VARCHAR2(100);
	v_fld_name VARCHAR2(2000);
	v_modified_by_name VARCHAR2(1000);
	v_fld_count NUMBER;
	v_fldval_for_audit VARCHAR2(32000);

	v_mapform_col VARCHAR2(100);
	v_pkfld NUMBER;

	v_formorigxml sys.XMLTYPE;
	v_form_completed NUMBER;

    v_sysid_orig VARCHAR2(100);

	v_wip NUMBER;

    l_stmt          DBMS_SQL.VARCHAR2S;
    l_cursor        INTEGER  ;
	v_len NUMBER;

	V_COLNAMES            Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
    V_COLVALUES        Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	V_DUMMYVAL NUMBER;

--	  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_FORMREP_TEMP', pLEVEL  => Plog.LDEBUG);

	BEGIN
	  --change the record_type for this record to 'E'' - for exported
--	  update er_filledformtran
--	  set fft_recordtype  = 'E';
		 --get distinct forms

		 Plog.DEBUG(pCTX,'starting sp_processformdata');

		 SELECT pk_codelst INTO v_wip FROM ER_CODELST WHERE codelst_type = 'fillformstat' AND codelst_subtyp = 'working';

		  FOR j IN (  SELECT DISTINCT FK_FORM, FFT_TYPE,FK_FORMLIBVER FROM ER_FILLEDFORMTRAN )
		  	LOOP
		   		   v_form := j.fk_form;

				   BEGIN

				    Plog.DEBUG(pCTX,'v_form' || v_form||'V_formlibver'||j.fk_formlibver);
				   --get original xml
				   SELECT formlibver_xml
				   INTO    v_formorigxml
				   FROM ER_FORMLIBVER
				   WHERE pk_formlibver = j.fk_formlibver AND fk_formlib = v_form;



				   --see if there is a form layout already created
				   SELECT NVL(COUNT(*),0)
				   INTO v_mapcount
				   FROM ER_MAPFORM
				   WHERE fk_form = v_form;
				   -- if there is no layout, create one
				   IF v_mapcount = 0 THEN
	   			   		 Plog.DEBUG(pCTX,'have to generate mapping');
	   				   Pkg_Formrep.SP_GENFORMLAYOUT(v_form, j.FFT_TYPE, o_layout);
	   	   			   		 Plog.DEBUG(pCTX,'generated mapping');
					END IF;
				   --get field infor for he form
				    SELECT COUNT(*)
					INTO v_orig_count
					FROM erv_formflds
					WHERE  pk_formlib = v_form ;
					SELECT COUNT(*)
					INTO v_rep_count
					FROM ER_REPFORMFLD, ER_FORMSEC
					WHERE ER_FORMSEC.fk_formlib = v_form AND
					ER_REPFORMFLD.fk_formsec = pk_formsec AND
					NVL(ER_REPFORMFLD.record_type,'D') <> 'D';
			   		 Plog.DEBUG(pCTX,'v_rep_count' || v_rep_count || 'v_orig_count' || v_orig_count);

					v_tot := v_rep_count +  v_orig_count;
			   		 Plog.DEBUG(pCTX,'v_tot' || v_tot);
		   		  		 -- get data from er_filledformtran

						 --changed by sonia, 11/02/04, added where clause for formlibver.

						  FOR i IN (  SELECT pk_fft,FK_FILLEDFORM, FK_FORM,  FFT_DATE, FFT_TYPE,  FFT_RECORDTYPE,
									  FFT_OLDXML, FFT_NEWXML, CREATOR, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, CREATED_ON, IP_ADD,ID,fk_patprot,
									  form_completed
									  FROM ER_FILLEDFORMTRAN WHERE fk_form = v_form
									  AND fk_formlibver = j.fk_formlibver
									  ORDER BY fft_recordtype DESC,pk_fft ASC)
						  LOOP
						  BEGIN
		  						 V_COLNAMES := Types.SMALL_STRING_ARRAY ();
							     V_COLVALUES := Types.SMALL_STRING_ARRAY ();

								 Plog.DEBUG(pCTX,'Sonia : V_COLNAMES.count' || V_COLNAMES.COUNT);
 								 Plog.DEBUG(pCTX,'Sonia : V_COLVALUES.count' || V_COLVALUES.COUNT);

					  	  	   v_pk_fft := 	  i.pk_fft;

							  Plog.DEBUG(pCTX,' v_pk_fft .. ' ||   v_pk_fft );

							   v_filledformpk := i.FK_FILLEDFORM;

							  plog.DEBUG(pctx,'v_filledformpk .. ' ||   v_filledformpk );

							   v_fftdate := i.FFT_DATE;
							   v_filledformtype := i.fft_type;
							   v_recordtype := i.FFT_RECORDTYPE;
							   v_oldxml := i.FFT_OLDXML;
							   v_newxml := i.FFT_NEWXML;
							   v_creator := i.creator;
							   v_lastmodified_by :=  i.last_modified_by;

							   v_lastmodified_date :=  TO_DATE(TO_CHAR(i.last_modified_date,PKG_DATEUTIL.f_get_datetimeformat),PKG_DATEUTIL.f_get_datetimeformat);
							   v_created_on := TO_DATE(TO_CHAR(i.created_on,PKG_DATEUTIL.f_get_datetimeformat),PKG_DATEUTIL.f_get_datetimeformat);

							   v_ipadd := i.ip_Add;
							   v_id := i.ID;
							   v_fk_patprot := i.fk_patprot;

							   v_form_completed := i.form_completed;

							   IF v_form_completed IS NULL THEN
									   	 v_form_completed := v_wip;
								END IF;


							   SELECT seq_er_formslinear.NEXTVAL
							   INTO v_linpk
							   FROM dual;
								--null check
								IF v_newxml IS NOT NULL THEN
								   IF (v_recordtype = 'M' AND v_oldxml IS NOT NULL) OR (v_recordtype = 'N') THEN
									   IF v_recordtype = 'M' THEN --delete existing record from er_formslinear for this filled form
									   	  DELETE FROM ER_FORMSLINEAR
									   	  WHERE  FK_FILLEDFORM =  v_filledformpk AND FORM_TYPE = v_filledformtype;
										  BEGIN
										      v_modified_by_name := Getuser(v_lastmodified_by);
										      EXCEPTION WHEN NO_DATA_FOUND THEN
										      v_modified_by_name := 'New User' ;
										  END ;
									   END IF;




	   						   	       v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,form_completed,created_on,fk_filledform,fk_form,filldate,form_type,export_flag,id,fk_patprot,creator,last_modified_by,last_modified_date ';

									   	  v_values_sqlstr := ' VALUES (:pk_formslinear,:form_completed,:created_on,:fk_filledform,:fk_form,:filldate,:form_type,:export_flag,:id,:fk_patprot ,:creator,:last_modified_by,:last_modified_date' ;


									   --loop through xml and get filled data
									   v_fld_count := 0;
								   		 Plog.DEBUG(pCTX,'v_fld_count' || v_fld_count);
 								   		 Plog.DEBUG(pCTX,'v_tot' || v_tot);
									    FOR k IN 1..v_tot
										    LOOP



											v_fldval_for_audit := '';

											--changed by sonia, 09/08/04, read the attribute from blank xml instead of filledform xml
											--changed by Sonia Abrol, 08/17/05, get system id from blank XML and use this system id to get data from filled form
											SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_formorigxml,'//rowset/*[' || k || ']/@type')),1,3),
										     sys.XMLTYPE.getNumberVal(sys.XMLTYPE.EXTRACT(v_formorigxml,'//rowset/*[' || k || ']/@pkfld')),
											   SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_formorigxml,'//rowset/*[' || k || ']/@systemid')),1,50)
											INTO v_fld_datatype, v_pkfld,v_sysid_orig
											FROM dual;


										 IF v_fld_datatype <> 'ML' THEN
										 	   v_fld_count := v_fld_count + 1;
											   v_fldvalue := '';
											   v_fldvaluedata:='';
											IF v_fld_datatype = 'MC' THEN
											-- handle case for checkbox :
											   SELECT NVL(sys.XMLTYPE.getNumberVal(sys.XMLTYPE.EXTRACT(v_newxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1][last()]/@respcount')),0)
											   INTO v_repcount
											   FROM dual;
												--v_repcount := 4;
												FOR ct IN 1..v_repcount
												LOOP
													SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_newxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1]['|| ct ||']/@dispval')),1,4000)
													INTO v_respval
													FROM dual;
													SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_newxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1]['|| ct ||']/@dataval')),1,4000)
													INTO v_respdataval
													FROM dual;
													IF LENGTH(trim(NVL(v_respval,''))) > 0 THEN
														v_fldvalue := v_fldvalue || ',[' || v_respval || ']';
														v_fldvaluedata:=v_fldvaluedata || '[VELCOMMA]' || v_respdataval ;
													END IF;
												END LOOP;
												v_fldvalue := SUBSTR(v_fldvalue,2);
												v_fldvaluedata := SUBSTR(v_fldvaluedata,11);
												IF (LENGTH(v_fldvalue)>0) THEN
												   v_fldval_for_audit := v_fldvalue; --set the original checkbox display val for audit trail
												   IF LENGTH( trim(NVL(v_fldvalue,'') )) > 0  THEN --changed by sonia abrol, 04/21/05, the old condition to check if the field value is empty was not working
												      	  v_fldvalue := v_fldvalue ||'[VELSEP1]'||v_fldvaluedata;
												END IF;
												   --v_fldvalue:=nvl(v_fldvalue,v_fldvalue||'[VELSEP1]'||v_fldvaluedata);
												END IF;
												IF v_recordtype = 'M' THEN
												   v_old_fld_value := '';
												   v_old_fld_valuedata := '';
													SELECT NVL(sys.XMLTYPE.getNumberVal(sys.XMLTYPE.EXTRACT(v_oldxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1][last()]/@respcount')),0)
											   		INTO v_old_repcount
													FROM dual;
													FOR ct IN 1..v_repcount
													LOOP
															SELECT SUBSTR(sys.XMLTYPE.getStringVal(sys.XMLTYPE.EXTRACT(v_oldxml,'//rowset/' || v_sysid_orig || '/resp[@checked=1]['|| ct ||']/@dispval')),1,4000)
															INTO v_old_respval
															FROM dual;
															IF LENGTH(trim(NVL(v_old_respval,''))) > 0 THEN
																v_old_fld_value := v_old_fld_value || ',[' || v_old_respval || ']';
															END IF;
													END LOOP;
													v_old_fld_value := SUBSTR(v_old_fld_value,2);
												END IF;
											ELSE

								--Populate v_fldvalue
											   SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,1),'')
												INTO  v_fldvalue_prefix
												FROM dual;
												SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,2001),'')
												INTO  v_fldvalue_suffix
												FROM dual;
												v_fldvalue:=v_fldvalue_prefix||v_fldvalue_suffix;
												--end v_fldvalue populate

												--populate v_fldvaluedata
											   SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dataval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dataval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,1),'')
												INTO  v_fldvaluedata_prefix
												FROM dual;
												SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
												'MD',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dataval').getClobVal() ,
												'MR',v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dataval').getClobVal(),
												v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,2001),'')
												INTO  v_fldvaluedata_suffix
												FROM dual;
												v_fldvaluedata:=v_fldvaluedata_prefix||v_fldvaluedata_suffix;
												--end populating v_fldvaluedata
												v_fldval_for_audit := v_fldvalue; --set the original display val for audit trail
												IF v_recordtype = 'M' THEN -- for audit trail
												   v_old_fld_value := '';
												   v_old_fld_valuedata:='';
												    SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
													'MD',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
													'MR',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
													v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,1),'')
													INTO  v_old_fld_value_prefix
													FROM dual;
													  SELECT NVL(DBMS_LOB.SUBSTR(DECODE (v_fld_datatype,
													'MD',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@selected="1"]/@dispval').getClobVal() ,
													'MR',v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/resp[@checked="1"]/@dispval').getClobVal(),
													v_oldxml.EXTRACT('//rowset/' || v_sysid_orig || '/text()').getClobVal() ),2000,2001),'')
													INTO  v_old_fld_value_suffix
													FROM dual;
													v_old_fld_value:=v_old_fld_value_prefix||v_old_fld_value_suffix;

													END IF;
											END IF;
												v_fldvalue := REPLACE(v_fldvalue,'''','''''');
												v_fldvaluedata := REPLACE(v_fldvaluedata,'''','''''');
												v_fldval_for_audit := REPLACE(v_fldval_for_audit,'''','''''');

											 Plog.DEBUG(pCTX,'sonia...1v_fld_datatype '|| v_fld_datatype);
 											 Plog.DEBUG(pCTX,'sonia...2 v_fldvalue'|| v_fldvalue);
  											 Plog.DEBUG(pCTX,'sonia...3 v_fldvaluedata' || v_fldvaluedata);

												IF ((v_fld_datatype='MD') OR (v_fld_datatype='MR')) THEN
												Plog.DEBUG(pCTX,'sonia...4 v_fld_datatype' || v_fld_datatype);
													--v_fldvalue:=nvl(v_fldvalue,v_fldvalue||'[VELSEP1]'||v_fldvaluedata);
													 IF LENGTH( trim(NVL(v_fldvalue,'') )) > 0  THEN --changed by sonia abrol, 04/21/05, the old condition to check if the field value is empty was not working
													  	  v_fldvalue := v_fldvalue ||'[VELSEP1]'||v_fldvaluedata;
			   											 Plog.DEBUG(pCTX,'sonia...5 v_fldvalue'|| v_fldvalue);
													END IF;
												END IF;
											IF v_old_fld_value = 'er_textarea_tag' THEN
												   v_old_fld_value := '';
											END IF;
											IF v_fldvalue = 'er_textarea_tag' THEN
												   v_fldvalue := '';
												   v_fldvaluedata:='';
												   v_fldval_for_audit := '';
											END IF;
											--replace &nbsp; from fld values
											  v_fldvalue := REPLACE (v_fldvalue,'&amp;nbsp;','');
  											  v_fldval_for_audit := REPLACE (v_fldval_for_audit,'&amp;nbsp;','');
											  v_old_fld_value := REPLACE (v_old_fld_value,'&amp;nbsp;','');




											  --escape the special characters from data values
													v_fldvalue:=Pkg_Util.f_unescapeSpecialCharsForXML(v_fldvalue);
													v_fldval_for_audit:=Pkg_Util.f_unescapeSpecialCharsForXML(v_fldval_for_audit);
													v_old_fld_value:=Pkg_Util.f_unescapeSpecialCharsForXML(v_old_fld_value);


												--for audit trail
												IF v_recordtype = 'M' THEN
												   IF NVL(v_old_fld_value,' ') = NVL(v_fldval_for_audit,' ') THEN
												     	v_recordtype := 'M' ;
												   ELSE
		   	  											v_old_fld_value := REPLACE(v_old_fld_value,'''','''''');
														--get information for modified column
														 SELECT NVL(v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/@systemid').getStringVal(),'') ,
														 NVL(v_newxml.EXTRACT('//rowset/' || v_sysid_orig || '/@name').getStringVal(),'')
														 INTO  v_fld_systemid,v_fld_name
														 FROM dual;
														-- insert audit record for modified column
														INSERT INTO ER_FORMAUDITCOL( PK_FORMAUDITCOL,
														 FK_FILLEDFORM,
														 FK_FORM,
														 FA_FORMTYPE,
														 FA_SYSTEMID,
														 FA_DATETIME,
														 FA_FLDNAME,
														 FA_OLDVALUE,
														 FA_NEWVALUE,
														 FA_MODIFIEDBY_NAME )
														VALUES(seq_er_formauditcol.NEXTVAL,
														v_filledformpk,
														v_form,
														v_filledformtype,
														v_fld_systemid,
														v_lastmodified_date,-- value from er_filledformtran
														v_fld_name,
														v_old_fld_value,
														v_fldval_for_audit,
														v_modified_by_name);
												   END IF;
												END IF;

												 Plog.DEBUG(pCTX,'v_form'||v_form||'mp_pkfld'||v_pkfld);
											   --Get col name
											    SELECT MP_MAPCOLNAME
											   INTO v_mapform_col
											   FROM ER_MAPFORM
											   WHERE fk_form = v_form AND mp_pkfld = v_pkfld;

												v_ins_sqlstr := v_ins_sqlstr || ',' || v_mapform_col ;
												v_values_sqlstr := v_values_sqlstr || ',:' || v_mapform_col;


												Plog.DEBUG(pCTX,'v_pkfld' || v_pkfld || 'v_pkfld' || 'v_fld_datatype' || v_fld_datatype || '*');
												Plog.DEBUG(pCTX,'k' || k || ' v_fld_count' ||  v_fld_count);

												 V_COLNAMES.EXTEND;
									             V_COLNAMES(v_fld_count) := ':' || v_mapform_col;

   												 V_COLVALUES.EXTEND;
									             V_COLVALUES(v_fld_count) :=  REPLACE(v_fldvalue,'''','''''');

												  Plog.DEBUG(pCTX,'Sonia 3.5: VV_COLNAMES.count' || V_COLNAMES.COUNT);


											END IF ; -- for lookup filter
									 END LOOP; -- for xml
									 	v_ins_sqlstr := v_ins_sqlstr || ')';
										v_values_sqlstr := v_values_sqlstr || ')';

										Plog.DEBUG(pCTX,'v_ins_sqlstr' || v_ins_sqlstr);
										Plog.DEBUG(pCTX,'v_values_sqlstr' || v_values_sqlstr);


									   v_ins_sql := v_ins_sqlstr || v_values_sqlstr;

									   --insert into t values(v_ins_sql);
									   --commit;

								Plog.DEBUG(pCTX,'SQL'||v_ins_sql);
								 --EXECUTE IMMEDIATE v_ins_sql ;
																	   --use dbms_sql:

									   		 --open cursor
									    l_cursor      := DBMS_SQL.OPEN_CURSOR;
									    DBMS_SQL.PARSE( l_cursor,  v_ins_sql , dbms_sql.native );

										-- bind static fields
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':pk_formslinear',v_linpk    );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':form_completed',  v_form_completed );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':created_on',  v_created_on   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_filledform', v_filledformpk   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_form',  v_form  );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':filldate',  v_fftdate   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':form_type',  v_filledformtype  );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':export_flag', 0  );

										DBMS_SQL.BIND_VARIABLE( l_cursor, ':creator',NVL(v_creator,NULL)   );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':last_modified_by',  NVL(v_lastmodified_by,NULL) );
										DBMS_SQL.BIND_VARIABLE( l_cursor, ':last_modified_date', i.last_modified_date  );


									   IF v_filledformtype ='A' OR v_filledformtype = 'S' THEN
									   	   IF LENGTH(v_id) > 0 THEN
										   	  --v_values_sqlstr := v_values_sqlstr || ',' || v_id || ', NULL';
										    	DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    v_id);
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  V_DUMMYVAL );
										  ELSE
										   	   -- v_values_sqlstr := v_values_sqlstr || ', NULL, NULL';
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    V_DUMMYVAL);
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  V_DUMMYVAL );
										   END IF;
										ELSE
											IF LENGTH(v_id) > 0 THEN
												--v_values_sqlstr := v_values_sqlstr || ',' || v_id ;
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    v_id);
											 ELSE
												 --v_values_sqlstr := v_values_sqlstr || ', NULL';
												  DBMS_SQL.BIND_VARIABLE( l_cursor, ':id',    V_DUMMYVAL);
											END IF;
											IF LENGTH(v_fk_patprot) > 0 THEN
												--v_values_sqlstr := v_values_sqlstr || ',' || v_fk_patprot ;
												DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  v_fk_patprot);
											 ELSE
												-- v_values_sqlstr := v_values_sqlstr || ', NULL';
												 DBMS_SQL.BIND_VARIABLE( l_cursor, ':fk_patprot',  V_DUMMYVAL );
											END IF;
										END IF;

										--bind column variables
											 Plog.DEBUG(pCTX,'Sonia 3: VV_COLNAMES.count' || V_COLNAMES.COUNT);
								 	    FOR cl IN 1..V_COLNAMES.COUNT
								           LOOP -- loop for all message types
	   											 Plog.DEBUG(pCTX,'Sonia 3: VV_COLNAMES.cl' || V_COLNAMES(cl) || '*' ||V_COLVALUES(cl));
												DBMS_SQL.BIND_VARIABLE( l_cursor, V_COLNAMES(cl), V_COLVALUES(cl) );
								          END LOOP;

										 v_len:=DBMS_SQL.EXECUTE(l_cursor);
							  		    DBMS_SQL.CLOSE_CURSOR( l_cursor );

								--Plog.DEBUG(pCTX,'SQL'||v_ins_sql);
								 --dbms_output.put_line('inserted ' || i.FK_FILLEDFORM);
								 ELSE
			 							  Plog.DEBUG(pCTX,'(v_recordtype = M and v_oldxml is null) or (v_recordtype = N) ');
								 END IF; -- null check for v_oldxml and recordtypes
							 ELSE
							 Plog.DEBUG(pCTX,'v_newxml is null');
							 END IF ; -- null check for v_newxml
 							 Plog.DEBUG(pCTX,'finished for  v_pk_fft ' ||  v_pk_fft );

							--  UPDATE ER_FILLEDFORMTRAN
							  --SET fft_recordtype  = 'E' WHERE pk_fft = v_pk_fft ; --mark the record as exported

							    DELETE FROM ER_FILLEDFORMTRAN WHERE pk_fft = v_pk_fft ;
								COMMIT;
								EXCEPTION WHEN OTHERS THEN
								 Plog.DEBUG(pCTX,'SKIPPED:ID='||v_pk_fft || SQLERRM);
								END;

						  END LOOP; -- for filled forms for the selected form

						  EXCEPTION WHEN OTHERS THEN
						 			 Plog.DEBUG(pCTX,'SKIPPED:FORM ID='||v_form || SQLERRM);
						  END;
		    END LOOP; -- for distinct forms

--	  DELETE FROM ER_FILLEDFORMTRAN WHERE fft_recordtype  = 'E';

	  COMMIT;
 	END;

  --KM-#3919 
	PROCEDURE SP_GET_DATA_COLUMN_SQL(P_FORM IN NUMBER, P_FORMTYPE IN VARCHAR2, P_FORMLIBVER IN NUMBER, O_LIST OUT CLOB, O_DATACOL_COUNT OUT NUMBER)
	IS
	      v_sql LONG;
	  	  i NUMBER := 1;
		  v_cols NUMBER := 1;
		  v_collist LONG;
		  VALUE Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	BEGIN
	 SELECT COUNT(*)
	 INTO v_cols
	 FROM ER_MAPFORM WHERE fk_form = p_form;
	 FOR i IN 1..v_cols  LOOP
		  VALUE.EXTEND;
		  VALUE(i) := NULL;
		  v_collist := v_collist || ', col' || i;
	  END LOOP;
	  O_DATACOL_COUNT := v_cols;
   	  v_collist := SUBSTR(v_collist,2);
     -- Build a query
	  IF P_FORMTYPE = 'A' THEN
	        v_sql := 'SELECT ' || v_collist || ', pk_impacctform, nvl(fk_filledform,-1) fk_filledform, fk_account,filldate,form_completed,is_valid,notification_sent,creator,ip_Add FROM er_impacctform WHERE fk_form = ' || p_form || ' and fk_formlibver = '||  P_FORMLIBVER ||' and export_flag = 1 ';
	  ELSIF P_FORMTYPE = 'S' THEN
	        v_sql := 'SELECT ' || v_collist || ', pk_impstudyform, nvl(fk_filledform,-1) fk_filledform, fk_account, fk_study,filldate,form_completed,is_valid,notification_sent,creator,ip_Add FROM er_impstudyform WHERE fk_form = ' || p_form || ' and fk_formlibver = '||  P_FORMLIBVER ||' and export_flag = 1 ';
	  ELSIF P_FORMTYPE = 'P'  THEN
	  		v_sql := 'SELECT ' || v_collist || ', pk_imppatform, nvl(fk_filledform,-1) fk_filledform, fk_account,fk_per,fk_patprot,filldate,form_completed,is_valid,notification_sent,custom_col,creator,ip_Add FROM er_imppatform WHERE fk_form = ' || p_form || ' and fk_formlibver = '||  P_FORMLIBVER ||' and export_flag = 1 ';
	  ELSIF P_FORMTYPE = 'L'  THEN -- from er_formslinear
			v_sql := 'SELECT ' || v_collist || ', PK_FORMSLINEAR, FK_FILLEDFORM, FK_FORM, FILLDATE, FORM_COMPLETED,		IS_VALID, NOTIFICATION_SENT, FORM_TYPE, EXPORT_FLAG, ID, FK_PATPROT  FROM ER_FORMSLINEAR  WHERE fk_form = ' || p_form  ;

	  END IF;
	  o_list := v_sql;
	END;
	PROCEDURE SP_INSERT_ACCTFORM_EXP(p_form IN NUMBER, p_account IN NUMBER, p_formxml IN CLOB,
	p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2,
	p_datastr IN VARCHAR2, p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER, p_filledform IN NUMBER,  o_ret OUT NUMBER, p_imp IN NUMBER)
	AS
	 /****************************************************************************************************
	   ** Procedure to insert a filled account related form with process_data = 0
	   ** Author: Sonia Sahni 21 Nov 2003
	   ** Input parameter p_form  - Form ID
	   ** Input parameter p_account  - Account
	   ** Input parameter p_formxml - Form xml
	   ** Input parameter p_completed - Form compeletion status
	   ** Input parameter p_creator - Form record creator
	   ** Input parameter p_valid - Form validity flag
	   ** Input parameter p_ipAdd - User's IP ADD
	   ** Output parameter p_ret - Returns primary key of new record
	   **/
	  v_xmlclob    CLOB;
	  v_pkfilledform NUMBER;
	  v_linpk NUMBER;
	  v_ins_sqlstr LONG;
	  v_values_sqlstr LONG;
	  v_lin_sql LONG  ;
	  v_fftdate DATE;
	  v_datastr LONG;
	  v_error VARCHAR2(4000);
	  BEGIN
	  v_xmlclob :=  p_formxml;


	  IF  p_filledform <= 0 THEN
		  SELECT SEQ_ER_ACCTFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
		  INSERT INTO ER_ACCTFORMS(PK_ACCTFORMS,FK_FORMLIB,FK_ACCOUNT,
					ACCTFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
					CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
		  VALUES ( v_pkfilledform,p_form,p_account,
					XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2 ,p_formlibver);
			o_ret := v_pkfilledform;
	  ELSE

	  	  v_pkfilledform := p_filledform;

	  	  UPDATE ER_ACCTFORMS
		  SET FK_FORMLIB = p_form,FK_ACCOUNT = p_account,
					ACCTFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed ,
					LAST_MODIFIED_BY = p_creator,ISVALID =p_valid ,LAST_MODIFIED_DATE = SYSDATE,
					IP_ADD = p_ipadd,PROCESS_DATA = 2,FK_FORMLIBVER =  p_formlibver
		  WHERE PK_ACCTFORMS = v_pkfilledform;
		  o_ret := 0;
	  END IF;
	  --insert data in er_formslinear for this form, we are bypassing xml processing
  	  IF  p_filledform <= 0 THEN
		  SELECT seq_er_formslinear.NEXTVAL
		  INTO v_linpk
		  FROM dual;
		  SELECT NVL( ACCTFORMS_FILLDATE,SYSDATE)
		  INTO v_fftdate
		  FROM ER_ACCTFORMS
		  WHERE PK_ACCTFORMS = v_pkfilledform;
		  v_datastr := p_datastr;
		  v_datastr := REPLACE(v_datastr,'[*velquote*]','''''');
		  v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,id,fk_filledform,fk_form,filldate,form_type,export_flag,creator,created_on,form_completed ' || p_datacolStr || ')';
		  v_values_sqlstr := ' VALUES (' || v_linpk || ' , ' || p_account || ' , ' || v_pkfilledform || ',' || p_form || ',''' || v_fftdate || ''',''A'',0 ,' ||
									   p_creator || ', sysdate,' || p_completed  ||  v_datastr || ')' ;
	  	  v_lin_sql := v_ins_sqlstr || v_values_sqlstr ;
		  EXECUTE IMMEDIATE v_lin_sql ;
	  END IF;
	  UPDATE ER_IMPACCTFORM SET export_flag = 0 WHERE pk_impacctform = p_imp;
	  EXCEPTION  WHEN OTHERS THEN
	  		v_error := SQLERRM;
	  		INSERT INTO ER_IMPLOG VALUES (seq_er_implog.NEXTVAL, 'A',p_imp,v_error,SYSDATE);
	       o_ret := -1;
	       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in PKG_REPFORM.SP_INSERT_ACCTFORMEXP :'||SQLERRM ||  v_lin_sql);
	  END ; --end of SP_INSERT_ACCTFORM_EXP
	   PROCEDURE SP_INSERT_STUDYFORM_EXP(p_form IN NUMBER, p_study IN NUMBER, p_formxml IN CLOB,
 	 p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER, p_ipAdd IN VARCHAR2,  p_datastr IN VARCHAR2,
	 p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER,  p_filledform IN NUMBER, o_ret OUT NUMBER, p_imp IN NUMBER)
	   	AS
	 /****************************************************************************************************
	   ** Procedure to insert a filled study related form with process_data = 0
	   ** Author: Sonia Sahni 21 Nov 2003
	   ** Input parameter p_form  - Form ID
	   ** Input parameter p_account  - Account
	   ** Input parameter p_formxml - Form xml
	   ** Input parameter p_completed - Form compeletion status
	   ** Input parameter p_creator - Form record creator
	   ** Input parameter p_valid - Form validity flag
	   ** Input parameter p_ipAdd - User's IP ADD
	   ** Output parameter p_ret - Returns primary key of new record
	   **/
	  v_xmlclob    CLOB;
	  v_pkfilledform NUMBER;
	  v_linpk NUMBER;
	  v_ins_sqlstr LONG;
	  v_values_sqlstr LONG;
	  v_lin_sql LONG  ;
	  v_fftdate DATE;
	  v_datastr LONG;
	  v_error VARCHAR2(4000);
	  BEGIN
	  v_xmlclob :=  p_formxml;
	  IF  p_filledform <= 0 THEN
		  SELECT SEQ_ER_STUDYFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
		  INSERT INTO ER_STUDYFORMS(PK_STUDYFORMS,FK_FORMLIB,FK_STUDY,
					STUDYFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
					CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
			VALUES ( v_pkfilledform,p_form,p_study,
					XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2,p_formlibver );
			o_ret := v_pkfilledform;
	  ELSE
	  	  	UPDATE ER_STUDYFORMS
			SET FK_FORMLIB = p_form,FK_STUDY = p_study,
					STUDYFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed,
					LAST_MODIFIED_BY = p_creator,ISVALID = p_valid ,LAST_MODIFIED_DATE = SYSDATE,
					IP_ADD = p_ipAdd,PROCESS_DATA = 2,FK_FORMLIBVER = p_formlibver
			WHERE PK_STUDYFORMS = p_filledform;
			o_ret := 0;
	  END IF;
	  --insert data in er_formslinear for this form, we are bypassing xml processing
	  IF  p_filledform <= 0 THEN
		  SELECT seq_er_formslinear.NEXTVAL
		  INTO v_linpk
		  FROM dual;
		  SELECT NVL( STUDYFORMS_FILLDATE,SYSDATE)
		  INTO v_fftdate
		  FROM ER_STUDYFORMS
		  WHERE PK_STUDYFORMS = v_pkfilledform;
		  v_datastr := p_datastr;
		  v_datastr := REPLACE(v_datastr,'[*velquote*]','''''');
		  v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,id,fk_filledform,fk_form,filldate,form_type,export_flag ,creator,created_on,form_completed ' || p_datacolStr || ')';
		  v_values_sqlstr := ' VALUES (' || v_linpk || ' , ' || p_study || ' , ' ||  v_pkfilledform || ',' || p_form || ',''' || v_fftdate || ''',''S'',0 ,' ||
									   p_creator || ', sysdate,' || p_completed  || v_datastr || ')' ;

	  	  v_lin_sql := v_ins_sqlstr || v_values_sqlstr ;
		  EXECUTE IMMEDIATE v_lin_sql ;
	  END IF;
	  UPDATE ER_IMPSTUDYFORM SET export_flag = 0 WHERE pk_impstudyform = p_imp;
	  EXCEPTION  WHEN OTHERS THEN
	  		v_error := SQLERRM;
	  		INSERT INTO ER_IMPLOG VALUES (seq_er_implog.NEXTVAL, 'S',p_imp,v_error,SYSDATE);
	  	   o_ret := -1;
	       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in PKG_REPFORM.SP_INSERT_STUDYFORMEXP :'||SQLERRM ||  v_lin_sql);
	  END ; --end of SP_INSERT_STUDYFORM_EXP
	 PROCEDURE SP_INSERT_PATFORM_EXP(p_form IN NUMBER, p_pat IN NUMBER, p_patprot IN NUMBER, p_formxml IN CLOB,
 	 p_completed IN NUMBER, p_creator IN NUMBER, p_valid IN NUMBER,
	 p_ipAdd IN VARCHAR2,  p_datastr IN VARCHAR2, p_datacolStr IN VARCHAR2, p_formlibver IN NUMBER,  p_filledform IN NUMBER,
	 p_customCol IN VARCHAR2,o_ret OUT NUMBER, p_imp IN NUMBER)
	   	AS
	 /****************************************************************************************************
	   ** Procedure to insert a filled patient related form with process_data = 0
	   ** Author: Sonia Sahni 21 Nov 2003
	   ** Input parameter p_form  - Form ID
	   ** Input parameter p_account  - Account
	   ** Input parameter p_formxml - Form xml
	   ** Input parameter p_completed - Form compeletion status
	   ** Input parameter p_creator - Form record creator
	   ** Input parameter p_valid - Form validity flag
	   ** Input parameter p_ipAdd - User's IP ADD
	   ** Output parameter p_ret - Returns primary key of new record
	   **/
	  v_xmlclob    CLOB;
	  v_pkfilledform NUMBER;
	  v_linpk NUMBER;
	  v_ins_sqlstr LONG;
	  v_values_sqlstr LONG;
	  v_lin_sql LONG  ;
	  v_fftdate DATE;
	  v_datastr LONG;
	  v_customcount  VARCHAR2(250);
	  v_lmap VARCHAR2(250);
	  v_pkimpmap NUMBER ;
	  v_error VARCHAR2(4000);
	  	  BEGIN
	  v_xmlclob :=  p_formxml;
	    IF p_customcol='[VELDEFAULT]' THEN
	     IF  p_filledform <= 0 THEN
	  	  SELECT SEQ_ER_PATFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
	  	  INSERT INTO ER_PATFORMS(PK_PATFORMS,FK_FORMLIB,FK_PER,FK_PATPROT,
				PATFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
				CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
			VALUES ( v_pkfilledform,p_form,DECODE(p_pat,0,NULL,p_pat),DECODE(p_patprot,0,NULL,p_patprot),
				XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2 ,p_formlibver);
		o_ret := v_pkfilledform;
	  ELSE
	  	    UPDATE ER_PATFORMS
			SET FK_FORMLIB = p_form, FK_PER =DECODE(p_pat,0,NULL,p_pat) ,FK_PATPROT = DECODE(p_patprot,0,NULL,p_patprot),
				PATFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed,
				LAST_MODIFIED_BY = p_creator,ISVALID = p_valid,LAST_MODIFIED_DATE = SYSDATE,IP_ADD = p_ipAdd,
				PROCESS_DATA = 2,FK_FORMLIBVER = p_formlibver
			WHERE PK_PATFORMS = p_filledform;
			o_ret := 0;
	  END IF;
	  ELSE

	   --check if the record already exist in mapping table
	   SELECT NVL(COUNT(*),0) INTO v_customcount FROM ER_IMPPKMAP WHERE  imppkmap_rmap=p_customCol AND imppkmap_object='er_patforms' ;
	     --end checking
	     IF  v_customcount <= 0 THEN
		 Plog.DEBUG(pCTX,'Insert Statement');
	  	  SELECT SEQ_ER_PATFORMS.NEXTVAL
		  INTO  v_pkfilledform
		  FROM dual;
	  	  INSERT INTO ER_PATFORMS(PK_PATFORMS,FK_FORMLIB,FK_PER,FK_PATPROT,
				PATFORMS_XML,RECORD_TYPE,FORM_COMPLETED,
				CREATOR,ISVALID ,CREATED_ON,IP_ADD,PROCESS_DATA,FK_FORMLIBVER)
			VALUES ( v_pkfilledform,p_form,DECODE(p_pat,0,NULL,p_pat),DECODE(p_patprot,0,NULL,p_patprot),
				XMLTYPE(v_xmlclob),	'N', p_completed ,p_creator,p_valid, SYSDATE, p_ipAdd,2 ,p_formlibver);
		o_ret := v_pkfilledform;
		--update custom mapping table
		  SELECT SEQ_ER_imppkmap.NEXTVAL
		  INTO  v_pkimpmap
		  FROM dual;
		  INSERT INTO ER_IMPPKMAP(pk_imppkmap,imppkmap_object,imppkmap_rmap,imppkmap_lmap) VALUES
		   (v_pkimpmap,'er_patforms',p_customCol,v_pkfilledform);
		 ELSE
		 Plog.DEBUG(pCTX,'Update statement');
		    SELECT imppkmap_lmap INTO v_lmap FROM ER_IMPPKMAP WHERE  imppkmap_rmap=p_customCol AND imppkmap_object='er_patforms';
				   Plog.DEBUG(pCTX,' v_lmap '||v_lmap);
	  	    UPDATE ER_PATFORMS
			SET FK_FORMLIB = p_form, FK_PER =DECODE(p_pat,0,NULL,p_pat) ,FK_PATPROT = DECODE(p_patprot,0,NULL,p_patprot),
				PATFORMS_XML = XMLTYPE(v_xmlclob),RECORD_TYPE = 'M',FORM_COMPLETED = p_completed,
				LAST_MODIFIED_BY = p_creator,ISVALID = p_valid,LAST_MODIFIED_DATE = SYSDATE,IP_ADD = p_ipAdd,
				PROCESS_DATA = 2,FK_FORMLIBVER = p_formlibver
			WHERE PK_PATFORMS = TO_NUMBER(v_lmap);
			o_ret := 0;
	  END IF;

	  END IF; --end if for

	  --insert data in er_formslinear for this form, we are bypassing xml processing

  	  --IF  p_filledform <= 0 THEN
	  IF  o_ret> 0 THEN
	  Plog.DEBUG(pCTX,'Will add in er_formslinear');
		  SELECT seq_er_formslinear.NEXTVAL
		  INTO v_linpk
		  FROM dual;
		  Plog.DEBUG(pCTX,'Get the value');
		  SELECT NVL( PATFORMS_FILLDATE,SYSDATE)
		  INTO v_fftdate
		  FROM ER_PATFORMS
		  WHERE PK_PATFORMS = v_pkfilledform;
		  		  Plog.DEBUG(pCTX,'Get the value after'||v_fftdate );
		  v_datastr := p_datastr;
		  v_datastr := REPLACE(v_datastr,'[*velquote*]','''''');
		  v_ins_sqlstr := 'INSERT INTO er_formslinear(pk_formslinear,id,fk_patprot, fk_filledform,fk_form,filldate,form_type,export_flag ,creator,created_on,form_completed ' || p_datacolStr || ')';
		  v_values_sqlstr := ' VALUES (' || v_linpk || ' , ' || p_pat || ' ,  DECODE('|| p_patprot ||',0,NULL,'|| p_patprot ||') , ' ||   v_pkfilledform || ',' || p_form || ',''' || v_fftdate || ''',''P'',0 ,' ||
									   p_creator || ', sysdate,' || p_completed ||   v_datastr || ')' ;
	  	  v_lin_sql := v_ins_sqlstr || v_values_sqlstr ;
	  	  EXECUTE IMMEDIATE v_lin_sql ;
	  END IF;
	  UPDATE ER_IMPPATFORM SET export_flag = 0 WHERE pk_imppatform = p_imp;
	  EXCEPTION  WHEN OTHERS THEN
	  		v_error := SQLERRM;
	  		INSERT INTO ER_IMPLOG VALUES (seq_er_implog.NEXTVAL, 'P',p_imp,v_error,SYSDATE);
	       o_ret := -1;
	       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in PKG_REPFORM.SP_INSERT_PATFORMEXP :'||SQLERRM ||  v_lin_sql);
	  END ; --end of SP_INSERT_PATORM_EXP
--not in use now:
PROCEDURE SP_EXPTOFORMXML
IS
    v_cursor NUMBER;
    v_sql LONG;
    v_result NUMBER;
    v_val VARCHAR2(4000);
	i NUMBER := 1;
	v_cols NUMBER := 1;
	v_collist LONG;
	VALUE Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	v_form NUMBER;
	v_formxml XMLTYPE;
--	v_filledformxml xmltype;
	v_fldtype VARCHAR(3);
	tmpxml  XMLTYPE;
	v_fld_value VARCHAR2(4000);
	v_updatedclob CLOB;
BEGIN
--p('on line 1');
 --get distinct forms
  FOR j IN (  SELECT DISTINCT FK_FORM FROM ER_FORMSLINEAR WHERE export_flag = 1)
  	LOOP
	--p(' on line  2');
   		 v_form := j.fk_form;
		 -- get XML for this form
		 SELECT form_xml
		 INTO v_formxml
		 FROM ER_FORMLIB
		 WHERE pk_formlib = v_form;
		  SELECT COUNT(*)
		  INTO v_cols
		  FROM ER_MAPFORM WHERE fk_form = v_form;
		  FOR i IN 1..v_cols  LOOP
		  	  VALUE.EXTEND;
		      VALUE(i) := NULL;
		 	  v_collist := v_collist || ', col' || i;
		   END LOOP;
   		   v_collist := SUBSTR(v_collist,2);
 		     -- Build a query
		    v_sql := 'SELECT ' || v_collist || ' FROM er_formslinear WHERE fk_form = ' || v_form || ' and export_flag = 1 ';
			--p('v_sql' || v_sql);
		    -- Open the cursor.
		    v_cursor := DBMS_SQL.OPEN_CURSOR;
		    -- Parse the query.
		    DBMS_SQL.PARSE(v_cursor, v_sql, DBMS_SQL.native);
		    -- Set up the columns
			FOR i IN 1..v_cols
			LOOP
			    DBMS_SQL.DEFINE_COLUMN(v_cursor, i, VALUE(i),4000);
			END LOOP;
		    -- Execute the query
		    v_result := DBMS_SQL.EXECUTE(v_cursor);
		    -- Fetch the rows
		    LOOP
		        EXIT WHEN DBMS_SQL.FETCH_ROWS (v_cursor) = 0;
					-- start updating blank xml
					--v_filledformxml := v_formxml;
					tmpxml := v_formxml;
					FOR i IN 1..v_cols
					LOOP
						DBMS_SQL.COLUMN_VALUE(v_cursor, i, VALUE(i));
					END LOOP;
					-- will update all text fields
					sp_updatexml(tmpxml, VALUE, v_updatedclob);
					--insert the record in respective table;
					tmpxml := sys.XMLTYPE.createXML(v_updatedclob);
					INSERT INTO ER_ACCTFORMS ( PK_ACCTFORMS, FK_FORMLIB, FK_ACCOUNT , ACCTFORMS_FILLDATE , ACCTFORMS_XML)
				   VALUES (SEQ_ER_ACCTFORMS.NEXTVAL,v_form,46,SYSDATE,tmpxml);
					--insert a record
		    END LOOP; --for fetching rows for forms
		    -- Close the cursor.
		    DBMS_SQL.CLOSE_CURSOR(v_cursor);
	 --get all export records for this form
	  END LOOP; --for all forms to export
	 COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_SQL.CLOSE_CURSOR(v_cursor);
END SP_EXPTOFORMXML;
PROCEDURE sp_updatexml(p_xml sys.XMLTYPE ,p_values Types.SMALL_STRING_ARRAY, o_updatedclob OUT CLOB)
   /****************************************************************************************************
   ** Procedure to get XML dtagram for a field
   ** Author: Sonia Sahni 18th Nov 2003
   ******************************************NOT IN USE
   ********************************************************************************************************/
AS
    v_cnt NUMBER;
	i NUMBER := 1;
	o_clob    CLOB;
	v_doc       dbms_xmldom.DOMDocument;
    ndoc      dbms_xmldom.DOMNode;
    nodelist  dbms_xmldom.DOMNodelist;
	node      dbms_xmldom.DOMNode;
    childnode dbms_xmldom.DOMNode;
	textnode dbms_xmldom.DOMText;
	nodeM dbms_xmldom.DOMNode;
	nodeV dbms_xmldom.DOMNode;
	nodeTemp dbms_xmldom.DOMNode;
    nodelistSize NUMBER := 0;
    nodelistSizeCount NUMBER := 0;
	v_nodename VARCHAR2(50);
	v_counter NUMBER := 1;
	v_fldtype VARCHAR2(3);
	v_domelem dbms_xmldom.DOMElement;
	v_resp_nodelist dbms_xmldom.DOMNodeList;
	v_resp_count NUMBER;
	resp_ctr NUMBER;
	v_resp_elem dbms_xmldom.DOMElement;
	v_resp_dispval VARCHAR2(4000);
	v_resp_val VARCHAR2(4000);
	v_check_list Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	v_chk_idx NUMBER;
 	--  myParser    dbms_xmlparser.Parser;
    BEGIN
	 v_cnt := p_values.COUNT(); --get the # of elements in array
	-- Create DOMDocument handle:
	 v_doc     := dbms_xmldom.newDOMDocument(p_xml);
     ndoc    := dbms_xmldom.makeNode(v_doc);
	-----------------------------------------------------------
	-- Get First Child Of the Node
	node := dbms_xmldom.getFirstChild( ndoc );
	nodeM := dbms_xmldom.getFirstChild( node );
	--dbms_output.put_line('Node Name <'|| dbms_xmldom.getNodeNAME(nodeM) || '>');
    nodeV := dbms_xmldom.getFirstChild(nodeM);
    v_nodename := dbms_xmldom.getNodeNAME(nodeV);
    IF NVL(LENGTH(trim(v_nodename)),0) = 0 THEN
   	   textnode := dbms_xmldom.createTextNode( v_doc,p_values(1));
	   nodeTemp := dbms_xmldom.appendChild(nodeM,dbms_xmldom.makeNode(textnode));
	   nodeV := dbms_xmldom.getFirstChild(nodeTemp);
      -- v_nodename := dbms_xmldom.getNodeValue(nodeTemp);
	END IF;
	--dbms_output.put_line('Node value <'|| dbms_xmldom.getNodeValue(nodeM) || '>');
	node := nodeM ;
	<<PARESE_RECORD>>
		 LOOP
		 	 IF dbms_xmldom.isNull( node ) = TRUE THEN
			 	EXIT PARESE_RECORD;
			  END IF;
			  v_counter := v_counter + 1;
			  nodeM := dbms_xmldom.getNextSibling( node ); --Get Next Sibbling
			  --dbms_output.put_line('v_counter' || v_counter || 'Node Name <'|| dbms_xmldom.getNodeNAME(nodeM) || '>');
			  nodeV := dbms_xmldom.getFirstChild( nodeM );
			  v_nodename := dbms_xmldom.getNodeNAME(nodeV);
			  --dbms_output.put_line('Node Name <'||  v_nodename  || '>');
			  IF v_nodename = '#text' THEN
			  	 dbms_xmldom.setNodeValue(nodeV,p_values(v_counter));
			  ELSIF NVL(LENGTH(trim(v_nodename)),0) = 0 THEN
			     textnode := dbms_xmldom.createTextNode( v_doc,p_values(v_counter));
				 nodeTemp := dbms_xmldom.appendChild(nodeM,dbms_xmldom.makeNode(textnode));
				 nodeV := dbms_xmldom.getFirstChild(nodeTemp);
				 v_nodename := dbms_xmldom.getNodeValue(nodeTemp);
			  ELSIF v_nodename = 'resp' THEN --its multiple choice
				v_domelem := dbms_xmldom.makeElement(nodeM);
				v_fldtype :=   dbms_xmldom.getAttribute(v_domelem,'type');
				-- make nodes of all resp
				v_resp_nodelist :=	dbms_xmldom.getChildrenByTagName(v_domelem,'resp');
				v_resp_count := dbms_xmldom.getLength(v_resp_nodelist);
				resp_ctr := 0;
				 <<PARSE_RESP>>
				 LOOP
				 	IF resp_ctr = v_resp_count THEN
					  	EXIT PARSE_RESP;
					 END IF;
			  	  	 v_resp_elem :=  dbms_xmldom.makeElement(dbms_xmldom.item(v_resp_nodelist,resp_ctr));
					 v_resp_dispval  :=   dbms_xmldom.getAttribute(v_resp_elem,'dispval');
				  	 v_resp_val  := p_values(v_counter);
					 IF v_fldtype = 'MD' OR v_fldtype = 'MR' THEN --for radio button and dropdown fields
					 	 --p('v_resp_dispval' || v_resp_dispval);
 					 	 --p('v_resp_val' || v_resp_val);
						 IF trim(v_resp_dispval) = trim(v_resp_val) THEN
						 	IF v_fldtype = 'MD' THEN
							    dbms_xmldom.setAttribute(v_resp_elem,'selected','1');
							ELSE
							    dbms_xmldom.setAttribute(v_resp_elem,'checked','1');
							END IF;
						 END IF;
					 ELSIF v_fldtype = 'MC' THEN
						   sp_parse_list (p_values(v_counter), v_check_list);
						   v_chk_idx := v_check_list.FIRST;
						   << display_loop >>
							WHILE v_chk_idx IS NOT NULL LOOP
								 IF '[' || trim(v_resp_dispval) || ']' = trim(v_check_list(v_chk_idx)) THEN
								 	dbms_xmldom.setAttribute(v_resp_elem,'checked','1');
									EXIT display_loop;
								 END IF;
							   -- DBMS_OUTPUT.PUT_LINE('The val ' || v_check_list(v_chk_idx));
							    v_chk_idx := v_check_list.NEXT(v_chk_idx);
							 END LOOP display_loop;
					 END IF;
				  resp_ctr := resp_ctr + 1;
				 END LOOP PARSE_RESP;
			  END IF;
		  --dbms_output.put_line('Node value <'|| dbms_xmldom.getNodeValue(nodeV) || '>');
			  node := nodeM ;
  		 	 IF v_counter = v_cnt THEN
			 	EXIT PARESE_RECORD;
			 END IF;
		 END LOOP PARESE_RECORD;
		  dbms_xmldom.writeToClob(ndoc,o_clob);
		  dbms_xmldom.freeDocument(v_doc); -- Free The Document
	o_updatedclob := o_clob;
	-------------------------------------------------------------
	END;
	PROCEDURE sp_parse_list (p_mainstring VARCHAR2, o_list OUT Types.SMALL_STRING_ARRAY)
	AS
   /****************************************************************************************************
   ** Procedure to parse the checkbox string
   ** Author: Sonia Sahni 19th Nov 2003
   ******************************************NOT IN USE
   ********************************************************************************************************/
	   V_STR      VARCHAR2 (4000) DEFAULT p_mainstring || ',[';
	   V_PARAMS   VARCHAR2 (4001) DEFAULT p_mainstring || ',[';
	   V_POS      NUMBER         := 0;
	   v_count NUMBER;
	   v_list Types.SMALL_STRING_ARRAY := Types.SMALL_STRING_ARRAY ();
	 BEGIN
	   LOOP
	       V_POS := INSTR (V_STR, ',[');
		   V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
	        EXIT WHEN V_PARAMS IS NULL;
				 v_list.EXTEND;
				 v_count := v_list.LAST;
				 v_list(v_count) := V_PARAMS;
	      V_STR := SUBSTR (V_STR, V_POS + 1 );
	   END LOOP ;
	   o_list := v_list;
	END ;
END Pkg_Formrep;
/

create or replace PACKAGE      "PKG_FILLEDFORM"
AS
  PROCEDURE SP_GENERATE_PRINTXSL(p_formid NUMBER) ;

  PROCEDURE SP_GETPRINTFORM_HTML(p_formid NUMBER, p_filledform_id NUMBER, p_linkfrom STRING, o_xml OUT CLOB,o_xsl OUT CLOB) ;

  PROCEDURE SP_GETFLDPRINTXSL(p_datatype VARCHAR2, p_fldsystemid VARCHAR2, p_fldlinesno NUMBER, o_fldxsl IN OUT VARCHAR2);
  PROCEDURE SP_GET_PRN_SECTION_HEADER (p_formid NUMBER, p_section NUMBER, o_xsl OUT VARCHAR2);
  --KM-#4016
  PROCEDURE SP_INSERT_FORM_QUERY(p_filledform_id IN NUMBER, p_called_from NUMBER, p_query_type NUMBER,
  												 p_query_type_id IN ARRAY_STRING,
		  									    p_fldsys_ids IN ARRAY_STRING,  p_fld_notes IN ARRAY_STRING,
												p_query_status IN ARRAY_STRING,p_mode IN ARRAY_STRING, p_pk_query_status IN ARRAY_STRING, p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER);
PROCEDURE SP_INSERT_QUERY_RESPONSE(p_filledform_id IN NUMBER, p_called_from NUMBER, p_fld_id NUMBER,
		  									   p_query_type NUMBER, p_query_type_id NUMBER,
		  									   p_query_status_id NUMBER,  p_fld_comments VARCHAR2,
											   p_entered_on VARCHAR2, p_entered_by NUMBER,
											   p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT
NUMBER);


/* returns 1: if user has acccess to form's filled responses, 0: if user does not have access to form's filled responses
p_form : the form
p_user : the loggedin user
 p_filledform_creator : the creator of filled form record
by Sonia Abrol, 12/14/05, to use in AdHoc reports
*/

FUNCTION fn_getUserAccess (p_form NUMBER,p_user NUMBER,p_filledform_creator NUMBER) RETURN NUMBER;

FUNCTION fn_formfld_validate(p_form_id NUMBER, p_response_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2,p_operator VARCHAR2 ) RETURN NUMBER;

FUNCTION fn_formfld_validate_partial(p_form_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2) RETURN varchar2 ;

END Pkg_Filledform;
/

create or replace
PACKAGE BODY      "PKG_FILLEDFORM"
AS
PROCEDURE SP_GENERATE_PRINTXSL(p_formid NUMBER)
 /****************************************************************************************************
   ** Procedure to create xsl for printer friendly format, called when the form status is changed to active
   ** When the user makes the form Active, we traverse through all the field xsls, same way as we do in Form Preview.
   ** We change each xsl to make it a label and then club that xsl.
   ** Input parameter: formid - PK of the form
   ** Output parameter: formxsl
   ** Author: Sonika Talwar Dec 24, 2003
   ** Modified By: Sonika Talwar on April 19, 04
   **/
  AS
   v_formnametag VARCHAR2(500);
   v_fldxsl VARCHAR2(32000);
   v_clubxsl CLOB;
   v_formxsl CLOB;
   v_xslstarttags VARCHAR2(4000);
   v_xslmidtags VARCHAR2(4000);
   v_xslendtags VARCHAR2(4000);
   v_fldsameline NUMBER;
   v_fldseq NUMBER;
   v_section_seq NUMBER;
   v_secname VARCHAR2(250);
   v_formname VARCHAR2(250);
   v_pksec NUMBER;
   v_prevsec NUMBER := 0;
   v_fldname VARCHAR2(2000);
   v_fldkeyword VARCHAR2(255);
   v_flduniqueid VARCHAR2(250);
   v_fldsystemid VARCHAR2(50);
   v_pk_field NUMBER;
   v_fldtype CHAR(1);
   v_fld_datatype VARCHAR2(2);
   v_formstarttag VARCHAR2(500);
   v_formendtag VARCHAR2(100);
   v_xslns VARCHAR2(1000);
   v_colcount NUMBER;
   --salil
   v_fldlinesno NUMBER ;
   v_count NUMBER := 1;
   v_sec_count NUMBER := 1;
   v_secrepno NUMBER := 0;
   v_secformat CHAR(1) ;
   v_prev_sec_format CHAR(1) ;
   v_repfldxsl VARCHAR2(32000);
   v_pkformfld NUMBER;
   v_repfldsameline NUMBER;
   v_repfldname VARCHAR2(2000);
   v_repfldkeyword VARCHAR2(255);
   v_repflduniqueid VARCHAR2(50);
   v_repfldsystemid VARCHAR2(50);
   v_rep_pk_field NUMBER;
   v_repfldtype CHAR(1);
   v_rep_fld_datatype VARCHAR2(2);
   v_rep_fld_defresp VARCHAR2(500);
   v_repcolcount NUMBER;
   --salil
   v_repfldlinesno NUMBER ;
   v_prev_sec_repno NUMBER :=0 ;
   v_prevset NUMBER := 0;
   v_repfld_set NUMBER := 0;
   v_repfld_count NUMBER := 1;
   v_strlen NUMBER;
   v_pos_1 NUMBER := 0;
   v_pos_2 NUMBER := 0;
   v_pos_endtd NUMBER :=0;
   len NUMBER := 0;
   v_repfld_xsl_2 VARCHAR2(4000);
   v_repfld_xsl_3 VARCHAR2(4000);
   v_grid_title_xsl VARCHAR2(100);
   v_sec_grid_header VARCHAR2(4000);
   v_counter NUMBER;
   v_tempstr VARCHAR2(4000);
   v_tempclubxsl CLOB;
   v_origsystemid VARCHAR2(50);
   v_reporigsystemid VARCHAR2(50);
   v_valuestr VARCHAR2(2000);
   v_replacestr VARCHAR2(2000);
   v_custom_js VARCHAR2(4000);
   v_fld_align VARCHAR2(20);
   v_form_preserve_formatting number;

BEGIN

        BEGIN
             SELECT NVL(FORM_SAVEFORMAT,0) INTO v_form_preserve_formatting FROM ER_FORMLIB  WHERE PK_FORMLIB = p_formid ;

        EXCEPTION WHEN NO_DATA_FOUND THEN
                v_form_preserve_formatting := 0;

        END;

    if v_form_preserve_formatting = 0 then
        --add xsl namespace tags
        v_xslns := '<?xml version="1.0" ?> <xsl:stylesheet version="1.0" ';
        v_xslns := v_xslns || 'xmlns:xsl="http://www.w3.org/1999/XSL/Transform" ';
        v_xslns := v_xslns || 'xmlns:xdb="http://xmlns.oracle.com/xdb" ';
        v_xslns := v_xslns || 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> ' || CHR(13);
        v_xslstarttags := ' <xsl:template match="/"> ';
        v_xslstarttags :=  v_xslstarttags  || '<HTML> ' || CHR(13) || '<HEAD> ' || CHR(13);
        v_xslmidtags := ' </HEAD> ' || CHR(13) || '<BODY id="forms"> ' || CHR(13) || '<xsl:apply-templates select="rowset"/> ';
        v_xslmidtags := v_xslmidtags || CHR(13) || '</BODY> ' || CHR(13) ||' </HTML> ' || CHR(13) || '</xsl:template> ' || CHR(13) || '<xsl:template match="rowset"> ';
        v_xslendtags := '</xsl:template> ' || CHR(13) ||' </xsl:stylesheet> ';
        /************Sonia - 16th June for field Actions ********************************/
         -- create function for custom js
          v_custom_js := ' <script> ' || CHR(13) ||' function performAction(formobj, currentfld)' ||
                          CHR(13) || '{' || CHR(13) ||'}' || CHR(13) ||'</script>'  ;
         /************Sonia - 16th June for field Actions *********************************/
        v_formstarttag := '<Form name="er_fillform1" method="post" >';
        v_formendtag := '</Form>';
        v_clubxsl :=  v_clubxsl  || '<table class="dynFormTable" width="100%" border="0"><tr>';
        FOR i IN (SELECT pk_formfld,form_name , pk_formsec, formsec_name,formsec_seq ,formfld_seq ,NVL(fld_sameline,0) fld_sameline,formfld_xsl ,
                    NVL(formfld_javascr,' ') formfld_javascr,fld_name,fld_keyword,fld_uniqueid,
                  fld_systemid, pk_field,fld_type,fld_datatype, fld_defresp, fld_colcount , fld_linesno , formsec_fmt,NVL(formsec_repno,0) formsec_repno,
                  NVL(fld_align,'left') fld_Align
                   FROM erv_formflds
                  WHERE  pk_formlib = p_formid
                  AND NVL(fld_isvisible,0) <> 1
                  AND NVL(fld_datatype,'Z') <> 'ML' AND
                  NVL(fld_type,'Z') <> 'F'
                  UNION
                  SELECT NULL, NULL , pk_formsec, formsec_name,formsec_seq ,NULL ,NULL,NULL ,
                    NULL,NULL,NULL,NULL,
                  NULL, NULL,NULL,NULL, NULL, NULL,NULL,NULL,NULL,NULL
                  FROM ER_FORMSEC
                  WHERE fk_formlib = p_formid
                  AND NVL(record_type,'Z') <> 'D'
                  AND pk_formsec NOT IN (SELECT pk_formsec
                  FROM erv_formflds WHERE pk_formlib = p_formid)
                  ORDER BY formsec_seq,pk_formsec,formfld_seq
                  )
        LOOP
            IF (i.form_name IS NOT NULL) THEN
              v_formname := i.form_name;
           END IF;
           v_pksec := i.pk_formsec;
           v_secname := i.formsec_name;
           v_section_seq := i.formsec_seq;
           v_fldseq := i.formfld_seq;
           v_fldsameline := i.fld_sameline;
           v_fldxsl := i.formfld_xsl;
            v_fldname := i.fld_name;
           v_fldkeyword := i.fld_keyword;
           v_flduniqueid := i.fld_uniqueid;
           v_fldsystemid := i.fld_systemid;
           v_pk_field := i.pk_field;
           v_fldtype :=  i.fld_type;
           v_fld_datatype := i.fld_datatype;
           v_colcount := i.fld_colcount;
           v_fldlinesno := i.fld_linesno;
           v_secformat := i.formsec_fmt;
           v_secrepno :=  i.formsec_repno;
           v_pkformfld := i.pk_formfld;
           v_origsystemid := i.fld_systemid;
           v_fld_align := i.fld_align;
           --get the changed field xsl
           IF v_fldtype = 'S' THEN
              v_fldxsl := v_fldxsl; --no change in xsl
           ELSE
                  SP_GETFLDPRINTXSL(v_fld_datatype, v_fldsystemid, v_fldlinesno, v_fldxsl);
           END IF;
             --cut label for grid
             IF v_secformat = 'T' THEN
             -------------------------------
             -- if condition to hide horizontal line for tabular section
                 IF v_fldtype = 'H' THEN
                     v_fldxsl := '&#xa0;';
                 END IF;
                           --modified by sonia abrol, 03/16/05 to use a function to cut the label
                SELECT  Pkg_Form.f_cut_fieldlabel(v_fldxsl,v_fldtype,v_fld_datatype,v_fld_align,v_pk_field)
                INTO v_fldxsl FROM dual;
                     /* IF v_fldtype <> 'C' AND v_fldtype <> 'H' THEN
                    --modified by sonika on Nov 21, 03 search only for <td width=''15%'' as align option has also been added
                    --modified by sonika on April 19, 04 search only for <td
                        v_pos_1 := INSTR( v_fldxsl, '<td') ;
                       v_repfld_xsl_2 := SUBSTR(v_fldxsl, 0 ,v_pos_1-1);
                        len := LENGTH(v_fldxsl);
                       v_pos_2 := INSTR(v_fldxsl , '</label>');
                       --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' );
                           --Modified by Sonika Talwar on April 22, 04
                            v_pos_endtd := INSTR(v_fldxsl , '</td>',v_pos_2);
                           --# of characters between </label> and </td>
                        v_strlen := LENGTH('</td>');
                        v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
                          v_repfld_xsl_3 := SUBSTR( v_fldxsl , v_pos_2+v_strlen , len-1 ) ;
                          v_fldxsl := v_repfld_xsl_2 || v_repfld_xsl_3 ;
                      END IF ;
                      */
                    v_fldsameline := 1;
             END IF; --if v_secformat = 'T'
           IF v_count > 1 AND v_fldsameline = 0 AND v_fldtype <> 'H' THEN
                 v_fldxsl := '</tr><tr>' || v_fldxsl ;
           ELSIF v_count > 1 AND v_fldtype = 'H' AND v_secformat <> 'T' THEN
                    v_fldxsl := '</tr></table>' || v_fldxsl || '<table class="dynFormTable" width="100%"><tr>' ;
           END IF;
           IF v_prevsec <> v_pksec THEN -- if form section changes, create section tag
                        -- for section repeat fields of the previous section
                  IF v_prev_sec_repno  > 0 THEN
                      v_repfld_count := 1;
                        FOR x IN (SELECT repfrmfld.PK_REPFORMFLD, repfrmfld.FK_FORMFLD,repfrmfld.FK_FORMSEC,repfrmfld.FK_FIELD,repfrmfld.REPFORMFLD_SEQ,
                                       repfrmfld.REPFORMFLD_XSL,repfrmfld.REPFORMFLD_JAVASCR,repfrmfld.REPFORMFLD_SET, NVL(rep.FLD_SAMELINE,0) FLD_SAMELINE,rep.FLD_NAME,
                                 rep.FLD_KEYWORD, rep.FLD_UNIQUEID,rep.FLD_SYSTEMID,rep.FLD_TYPE,  rep.FLD_DATATYPE,
                                 rep.FLD_DEFRESP ,rep.FLD_COLCOUNT,rep.FLD_LINESNO, orig.FLD_SYSTEMID orig_systemid --salil
                                 FROM ER_REPFORMFLD repfrmfld, ER_FLDLIB rep, ER_FORMFLD, ER_FLDLIB orig
                                 WHERE repfrmfld.FK_FORMSEC = v_prevsec AND
                                 rep.PK_FIELD = repfrmfld.FK_FIELD AND
                                 NVL(rep.fld_isvisible,0) <> 1 AND
                                 NVL(rep.fld_datatype,'Z') <> 'ML' AND
                                  NVL(rep.fld_type,'Z') <> 'F'  AND
                                 NVL(repfrmfld.record_type,'Z') <> 'D' AND
                                 ER_FORMFLD.PK_FORMFLD = repfrmfld.fk_formfld AND
                                 orig.PK_FIELD = ER_FORMFLD.fk_field
                                 ORDER BY REPFORMFLD_SET,REPFORMFLD_SEQ
                                 )
                      LOOP
                          v_rep_pk_field := x.FK_FIELD;
                          v_repfldxsl := x.REPFORMFLD_XSL;
                         v_repfldsameline  :=x.FLD_SAMELINE;
                         v_repfldname := x.FLD_NAME;
                         v_repfldkeyword := x.FLD_KEYWORD;
                         v_repflduniqueid :=  x.FLD_UNIQUEID ;
                         v_repfldsystemid := x.FLD_SYSTEMID;
                         v_repfldtype := x.FLD_TYPE ;
                         v_rep_fld_datatype := x.FLD_DATATYPE ;
                         v_rep_fld_defresp := x.FLD_DEFRESP ;
                         v_repcolcount := x.FLD_COLCOUNT;
                         v_repfld_set := x.REPFORMFLD_SET;
                         v_repfldlinesno := x.FLD_LINESNO ;
                         v_reporigsystemid := x.orig_systemid;
                         --get the changed field xsl
                                IF v_repfldtype = 'S' THEN
                                         v_repfldxsl := v_repfldxsl; --no change in xsl
                                 ELSE
                                       SP_GETFLDPRINTXSL(v_rep_fld_datatype, v_repfldsystemid, v_repfldlinesno, v_repfldxsl);
                                END IF;
                         -- add xsl
                        IF v_prev_sec_format = 'T' THEN
                        -- if condition to hide horizontal line for tabular section
                           IF v_repfldtype = 'H' THEN
                              v_repfldxsl := '&#xa0;';
                           END IF;
                           IF v_prevset <> v_repfld_set AND v_repfld_count = 1 THEN
                             v_repfldsameline := 0;
                           ELSIF v_repfld_count > 1 AND v_prevset = v_repfld_set THEN
                             v_repfldsameline := 1;
                            ELSE
                                 v_repfldsameline := 0;
                           END IF;
                        END IF;
                        v_repfldxsl := REPLACE(v_repfldxsl,'&', '&amp;');
                         IF  v_repfldsameline = 0  THEN
                              IF v_prev_sec_format = 'T' THEN -- sonia 16th june for tabular alignment
                                 v_clubxsl := v_clubxsl || '</tr><tr>' || v_repfldxsl ; -- sonia 16th june for tabular alignment
                               ELSE -- sonia 16th june for tabular alignment
                                    --changed by Sonika on April 19, 04
                               v_clubxsl := v_clubxsl || '</tr> </table> <table class="dynFormTable" width="100%" border="0"> <tr>' || v_repfldxsl ;
                               END IF; -- sonia 16th june for tabular alignment
                          ELSE
                            v_clubxsl := v_clubxsl || v_repfldxsl ;
                        END IF;
                       v_prevset := v_repfld_set ;
                       v_repfld_count := v_repfld_count + 1;
                      END LOOP ;
                  END IF;--if v_prev_sec_repno  > 0
              v_sec_count := 0;
               IF v_secformat = 'T' THEN
                   v_fldxsl := REPLACE(v_fldxsl,'&', '&amp;');
                  SP_GET_PRN_SECTION_HEADER (p_formid, v_pksec , v_sec_grid_header);
                  --remove tooltip overlib script from section header
                     v_pos_1 := INSTR( v_sec_grid_header, 'onmouseover="return overlib') ;
                  WHILE (v_pos_1 > 0)
                  LOOP
                       v_strlen := LENGTH( 'return nd();"');
                        v_pos_2 := INSTR( v_sec_grid_header, 'return nd();"',v_pos_1) ;
                      v_replacestr := SUBSTR(v_sec_grid_header, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
                      v_valuestr := '';
                       v_sec_grid_header := REPLACE(v_sec_grid_header,v_replacestr,v_valuestr);
                    v_pos_1 := INSTR( v_sec_grid_header, 'onmouseover="return overlib',v_pos_1+1) ;
                  END LOOP;
                    --sonia 21st June, changed border=0 for tabular, revreted back the change for tabular grid
                               v_secname:=Pkg_Util.f_escapeSpecialCharsForXML(v_secname);
                        v_clubxsl := v_clubxsl || '</tr></table><P class="dynSection" > ' || v_secname || '</P><table class="dynFormTable" width="100%" border="0"><tr>' || v_sec_grid_header || '</tr><tr>' || v_fldxsl ;
             ELSE
                       v_secname:=Pkg_Util.f_escapeSpecialCharsForXML(v_secname);
                   v_clubxsl := v_clubxsl || '</tr></table><P class="dynSection" > ' || v_secname || '</P><table class="dynFormTable" width="100%" border="0"><tr>' || v_fldxsl ;
             END IF;
           ELSE
                 v_sec_count := v_sec_count + 1;
             --replace special characters
              v_fldxsl := REPLACE(v_fldxsl,'&', '&amp;');
              IF  v_fldsameline = 0  THEN
                     v_clubxsl := v_clubxsl || '</tr> </table> <table class="dynFormTable" width="100%" border="0"> <tr>' || v_fldxsl ;
              ELSE
                       v_clubxsl := v_clubxsl || v_fldxsl ;
              END IF;
           END IF;
            v_prevsec := v_pksec;
            v_prev_sec_repno := v_secrepno;
            v_prev_sec_format := v_secformat;
            v_count := v_count + 1;
        END LOOP ;
        -------------------------------------------------------------------
              IF v_prev_sec_repno > 0 AND v_prevsec > 0 THEN -- for last section
                      v_repfld_count := 1;
                      v_prevset := 0;
                        FOR y IN (SELECT repfrmfld.PK_REPFORMFLD, repfrmfld.FK_FORMFLD,repfrmfld.FK_FORMSEC,repfrmfld.FK_FIELD,repfrmfld.REPFORMFLD_SEQ,
                                       repfrmfld.REPFORMFLD_XSL,repfrmfld.REPFORMFLD_JAVASCR,repfrmfld.REPFORMFLD_SET, NVL(rep.FLD_SAMELINE,0) FLD_SAMELINE,rep.FLD_NAME,
                                 rep.FLD_KEYWORD, rep.FLD_UNIQUEID,rep.FLD_SYSTEMID,rep.FLD_TYPE,  rep.FLD_DATATYPE,
                                 rep.FLD_DEFRESP ,rep.FLD_COLCOUNT,rep.FLD_LINESNO, orig.FLD_SYSTEMID orig_systemid --salil
                                 FROM ER_REPFORMFLD repfrmfld, ER_FLDLIB rep, ER_FORMFLD, ER_FLDLIB orig
                                 WHERE repfrmfld.FK_FORMSEC = v_prevsec AND
                                 rep.PK_FIELD = repfrmfld.FK_FIELD AND
                                  NVL(rep.fld_isvisible,0) <> 1 AND
                                 NVL(rep.fld_datatype,'Z') <> 'ML' AND
                                 NVL(rep.fld_type,'Z') <> 'F'  AND
                                 NVL(repfrmfld.record_type,'Z') <> 'D' AND
                                 ER_FORMFLD.PK_FORMFLD = repfrmfld.fk_formfld AND
                                 orig.PK_FIELD = ER_FORMFLD.fk_field
                                 ORDER BY REPFORMFLD_SET,REPFORMFLD_SEQ
                                 )
                      LOOP
                          v_rep_pk_field := y.FK_FIELD;
                         v_repfldxsl := y.REPFORMFLD_XSL;
                         v_repfldsameline  := y.FLD_SAMELINE;
                         v_repfldname := y.FLD_NAME;
                         v_repfldkeyword := y.FLD_KEYWORD;
                         v_repflduniqueid :=  y.FLD_UNIQUEID ;
                         v_repfldsystemid := y.FLD_SYSTEMID;
                         v_repfldtype := y.FLD_TYPE ;
                         v_rep_fld_datatype := y.FLD_DATATYPE ;
                         v_rep_fld_defresp := y.FLD_DEFRESP ;
                         v_repcolcount := y.FLD_COLCOUNT;
                         v_repfldlinesno := y.FLD_LINESNO ; --salil
                          v_repfld_set := y.REPFORMFLD_SET;
                         v_reporigsystemid := y.orig_systemid;
                          --get the changed field xsl
                         IF v_repfldtype = 'S' THEN
                                         v_repfldxsl := v_repfldxsl; --no change in xsl
                                 ELSE
                                          SP_GETFLDPRINTXSL(v_rep_fld_datatype, v_repfldsystemid, v_repfldlinesno, v_repfldxsl);
                            END IF;
                         -- add xsl
                        IF v_prev_sec_format = 'T' THEN
                         -- if condition to hide horizontal line for tabular section
                         IF v_repfldtype = 'H' THEN
                              v_repfldxsl := '&#xa0;';
                         END IF;
                           IF v_prevset <> v_repfld_set AND v_repfld_count = 1 THEN
                             v_repfldsameline := 0;
                           ELSIF v_repfld_count > 1 AND v_prevset = v_repfld_set THEN
                             v_repfldsameline := 1;
                           ELSE
                                 v_repfldsameline := 0;
                           END IF;
                        END IF;
                         v_repfldxsl := REPLACE(v_repfldxsl,'&', '&amp;');
                         IF  v_repfldsameline = 0  THEN
                            v_clubxsl := v_clubxsl || '</tr>  <tr>' || v_repfldxsl ;
                          ELSE
                            v_clubxsl := v_clubxsl || v_repfldxsl ;
                        END IF;
                       v_prevset := v_repfld_set ;
                       v_repfld_count := v_repfld_count + 1;
                      END LOOP ;
                  END IF;
        --------------------------------------------------------------------
         v_clubxsl := v_clubxsl || '</tr></table>';
          v_formname:=Pkg_Util.f_escapeSpecialCharsForXML(v_formname);
         v_formnametag := '<table width="100%"><tr><td align="center" class="reportName">'||v_formname||'</td></tr></table>';
          --combine the xsl namespace tags, xsl start tags, form tags ,club xsl, xsl end tags to make the entire form xsl
         --sonia 16th June, added v_custom_js
         v_formxsl := v_xslns || v_xslstarttags ||v_xslmidtags ||  v_custom_js || v_formstarttag || v_formnametag || v_clubxsl || v_formendtag || v_xslendtags ;
         --remove mandatory *
          v_formxsl := REPLACE(v_formxsl,'<font class = "mandatory">*</font>',' ');
         -- INSERT INTO T VALUES (v_formxsl,'VerXSL');
          --COMMIT;
         UPDATE ER_FORMLIB
            SET form_viewxsl = v_formxsl
            WHERE pk_formlib = p_formid;
     COMMIT;
   end if;-- if v_form_preserve_formatting = 0

END; -- end of sp_generate_printxsl
------------------------------------------------------------------------------------------------------------
PROCEDURE SP_GETFLDPRINTXSL(p_datatype VARCHAR2, p_fldsystemid VARCHAR2, p_fldlinesno NUMBER, o_fldxsl IN OUT VARCHAR2)
 /****************************************************************************************************
   ** Procedure to change field xsl to make it a label instead of editable
   ** Author: Sonika Talwar Dec 24, 2003
   ** Input parameter: datatype of field
   ** Output parameter: fldxsl
   ** Modified by Sonika Talwar on April 20, 04 to remove the help icon
   ** Modified by Sonika Talwar on May 20, 04 to display radio buttons and checkboxes as such in printout
   ** Modified by Sonika Talwar on June 04, 04 to add line ------- in xsl if there's no value in xml
 **/
AS
 v_fldxsl VARCHAR2(4000);
 v_strlen NUMBER;
 v_pos_1 NUMBER := 0;
 v_pos_2 NUMBER := 0;
 v_valuestr VARCHAR2(2000);
 v_casestr VARCHAR2(2000);
 v_replacestr VARCHAR2(2000);
 v_fldsystemid VARCHAR2(50);
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_GETFLDPRINTXSL', pLEVEL  => Plog.LFATAL);
BEGIN
        v_fldxsl := o_fldxsl;
       --remove the help icon
        v_pos_1 := INSTR( v_fldxsl, '<img src="../images/jpg/help.jpg"') ;
        IF (v_pos_1 > 0) THEN
          v_strlen := LENGTH( '</img>');
           v_pos_2 := INSTR( v_fldxsl, '</img>',v_pos_1) ;
          v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
          v_valuestr := '';
            v_fldxsl := REPLACE(v_fldxsl,v_replacestr,v_valuestr);
        END IF;
           --remove script for tooltip overlib
        v_pos_1 := INSTR( v_fldxsl, 'onmouseover="return overlib') ;
        IF (v_pos_1 > 0) THEN
          v_strlen := LENGTH( 'return nd();"');
           v_pos_2 := INSTR( v_fldxsl, 'return nd();"',v_pos_1) ;
          v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
          v_valuestr := '';
            v_fldxsl := REPLACE(v_fldxsl,v_replacestr,v_valuestr);
        END IF;
       --its an edit type date or number field, replace input tag
       IF NVL(p_datatype,'Z') = 'ED' OR NVL(p_datatype,'Z') = 'EN' THEN
           v_pos_1 := INSTR( v_fldxsl, '<xsl:value-of select') ;
           v_strlen := LENGTH( '/>');
           v_pos_2 := INSTR( v_fldxsl, '/>',v_pos_1) ;
           v_valuestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
         v_casestr := '<xsl:choose> <xsl:when test="string-length('||p_fldsystemid || ') =0"><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
           v_pos_1 := INSTR( v_fldxsl, '<input ') ;
          v_strlen := LENGTH( '</input>');
           v_pos_2 := INSTR( v_fldxsl, '</input>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
        END IF; --ED and EN (edit box date, edit box number)


          --if its an edit type text or textarea, replace input tag or textarea
          IF NVL(p_datatype,'Z') = 'ET' THEN
         IF (NVL(p_fldlinesno,0) <=1) THEN --text
           v_pos_1 := INSTR( v_fldxsl, '<xsl:value-of select') ;
           v_strlen := LENGTH( '/>');
           v_pos_2 := INSTR( v_fldxsl, '/>',v_pos_1) ;
           v_valuestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
           v_pos_1 := INSTR( v_fldxsl, '<input ') ;
          v_strlen := LENGTH( '</input>');
           v_pos_2 := INSTR( v_fldxsl, '</input>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
         v_casestr := '<xsl:choose> <xsl:when test="string-length('||p_fldsystemid || ') =0"><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
         ELSE --textarea
         v_pos_1 := INSTR( v_fldxsl, '<xsl:value-of select') ;
           v_strlen := LENGTH( '/>');
           v_pos_2 := INSTR( v_fldxsl, '/>',v_pos_1) ;
           v_valuestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
           v_pos_1 := INSTR( v_fldxsl, '<textarea ') ;
          v_strlen := LENGTH( '</textarea>');
           v_pos_2 := INSTR( v_fldxsl, '</textarea>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
          v_casestr := '<xsl:choose> <xsl:when test="'||p_fldsystemid || '= ''er_textarea_tag'' "><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
         END IF;
        END IF; --ET (edit box text and textareaa)
           --if its dropdown, replace select
       IF NVL(p_datatype,'Z') = 'MD' THEN
          v_valuestr := '<U> <xsl:for-each select = "' || p_fldsystemid || '/resp"> <xsl:if test="@selected=''1''">  <xsl:value-of select="@dispval"/> </xsl:if> </xsl:for-each></U>';
           v_pos_1 := INSTR( v_fldxsl, '<select ') ;
          v_strlen := LENGTH( '</select>');
           v_pos_2 := INSTR( v_fldxsl, '</select>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
         v_casestr := '<xsl:choose> <xsl:when test="string-length('||p_fldsystemid || '/@checkboxesdata) =0"><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
        END IF; --MD (dropdown)
       --let the checkboxes and radios display as such in printout
           --if its checkbox or radio button, replace <xsl:for-each select
/*       if nvl(p_datatype,'Z') = 'MC' or  nvl(p_datatype,'Z') = 'MR' then
         if nvl(p_datatype,'Z') = 'MC' then
             v_valuestr := '<xsl:for-each select = "' || p_fldsystemid || '/resp"> <xsl:if test="@checked=''1''"> [<xsl:value-of select="@dispval"/>] </xsl:if> </xsl:for-each>';
        elsif nvl(p_datatype,'Z') = 'MR' then
             v_valuestr := '<xsl:for-each select = "' || p_fldsystemid || '/resp"> <xsl:if test="@checked=''1''"> <xsl:value-of select="@dispval"/> </xsl:if> </xsl:for-each>';
        end if;
         v_pos_1 := instr( v_fldxsl, '<xsl:for-each select ') ;
          v_strlen := length( '</xsl:for-each>');
           v_pos_2 := instr( v_fldxsl, '</xsl:for-each>') ;
           v_replacestr := substr(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
        end if; --MC (checkbox)*/
          v_valuestr := v_valuestr || '&#xa0;&#xa0;';
       v_fldxsl := REPLACE(v_fldxsl,v_replacestr,v_valuestr);

          --added by sonia, 11/03/05remove the calendar picture, now calendar picture is added after the input tag
           IF NVL(p_datatype,'Z') = 'ED'  THEN

           --find the calendar link
          -- plog.fatal(pctx,'v_fldxsl1' || v_fldxsl);
                   --v_pos_1 := INSTR( v_fldxsl, '<A onClick="javascript:return fnShowCalendar') ;
                --v_pos_2 := INSTR( v_fldxsl, '</img></A>') ;
                --v_fldxsl := SUBSTR(v_fldxsl,0,v_pos_1) || SUBSTR(v_fldxsl,(v_pos_2 + 11));
                v_fldxsl := replace(v_fldxsl,'<A onClick=','<A style="display:none;" onClick=');
            --    plog.fatal(pctx,'v_fldxsl2' || v_fldxsl);

        END IF;

       o_fldxsl := v_fldxsl;
END; --end of sp_getfldprintxsl
-----------------------------------------------------------------------------------------------

  PROCEDURE SP_GETPRINTFORM_HTML(p_formid NUMBER, p_filledform_id NUMBER, p_linkfrom STRING, o_xml OUT CLOB,o_xsl OUT CLOB)
 /****************************************************************************************************
   ** Procedure to generate printer friendly html
   ** Author: Sonika Talwar Dec 24, 2003
   ** Modified by: Sonika Talwar March 10, 2004, to generate the printxsl if it does not exist
   ** Modified by: Sonika Talwar May 12, 2004, to read the xsl according to form version
   ** Input parameter: formid - PK of the form
   ** Input parameter: filledformid - PK of the filled form
   ** Input parameter: linkfrom
   ** Output parameter: form xml
   ** Output parameter: form xsl

   ** Modified by Sonia Abrol, 11/02/05,  to return xml and xsl, tranformation will be done in java
   **/
AS
   v_html sys.XMLTYPE;
   v_len NUMBER;
   i NUMBER;
   v_returnhtml CLOB;
   tempstr VARCHAR2(4000);
   viewstr VARCHAR2(10);

   v_xmlclob CLOB;
   v_xslClob CLOB;

BEGIN
   --check if the printxsl has been generated or not
   BEGIN
   SELECT 'nullview' INTO viewstr
   FROM ER_FORMLIB
   WHERE  pk_formlib = p_formid
   AND form_viewxsl IS NULL ;
   IF (viewstr='nullview') THEN
      --generate the print xsl
     SP_GENERATE_PRINTXSL(p_formid);
   END IF;

   EXCEPTION WHEN NO_DATA_FOUND THEN
     P('no data');
   END;
   BEGIN


   IF (p_filledform_id > 0) THEN  --the user has filled the form
    IF (p_linkfrom = 'A') THEN

      SELECT e.ACCTFORMS_XML.getClobVal(), formlibver_viewxsl
        INTO V_XMLCLOB , V_XSLCLOB
        FROM ER_ACCTFORMS e, ER_FORMLIBVER
        WHERE PK_ACCTFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;

   ELSIF (p_linkfrom = 'S') THEN

   SELECT e.STUDYFORMS_XML.getClobval(), formlibver_viewxsl
        INTO V_XMLCLOB, V_XSLCLOB
        FROM ER_STUDYFORMS e, ER_FORMLIBVER
        WHERE PK_STUDYFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;

   ELSIF (p_linkfrom = 'P') THEN

          SELECT     e.PATFORMS_XML.getclobval(), formlibver_viewxsl
        INTO V_XMLCLOB,  V_XSLCLOB
        FROM ER_PATFORMS e, ER_FORMLIBVER
        WHERE PK_PATFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;
   ELSIF (p_linkfrom = 'C')THEN

     SELECT e.CRFFORMS_XML.getclobval(),formlibver_viewxsl
        INTO V_XMLCLOB, V_XSLCLOB
        FROM ER_CRFFORMS e, ER_FORMLIBVER
        WHERE PK_CRFFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;

   END IF;
 ELSE --user hasn't entered any data

  SELECT a.form_xml.getClobVal() ,a.form_viewxsl
        INTO V_XMLCLOB,  V_XSLCLOB
      FROM     ER_FORMLIB a WHERE pk_formlib = p_formid ;
 END IF;

/*        -- Return the transformed XML instance as a CLOB value.
     o_html := v_html.getClobVal();
     v_len := LENGTH(o_html);
     i:=1;
      WHILE i<=v_len
      LOOP
          tempstr := dbms_lob.SUBSTR(o_html,3000,i);
           tempstr := REPLACE(tempstr,'&quot;','''');
          tempstr := REPLACE(tempstr,'&apos;','''');
          tempstr := REPLACE(tempstr,'&amp;','&'); --replace an ampersand
          tempstr := REPLACE(tempstr, '&lt;' , '<');
           tempstr := REPLACE(tempstr, '&gt;', '>');
            tempstr := REPLACE(tempstr, '\\', '\');
          tempstr := REPLACE(tempstr,'er_textarea_tag<','<');
          tempstr := REPLACE(tempstr, '&#xa0;' , ' ');
          v_returnhtml := v_returnhtml || tempstr;
           i := i + 3000 ;
      END LOOP;
        o_html := v_returnhtml;
        o_html := REPLACE(o_html,'&amp;','&');
        o_html := REPLACE(o_html,'&quot;','''');
        o_html := REPLACE(o_html,'&apos;','''');
        o_html := REPLACE(o_html,'&lt;', '<');
        o_html := REPLACE(o_html,'&gt;', '>');
        o_html := REPLACE(o_html,'\\', '\');
        o_html := REPLACE(o_html, '&#xa0;' , ' ');
        -- replace the er_textarea_tag added to the text area field
        -- "er_textarea_tag<"is being replaced with "<" since replace  didn't work  when the string to be replaced is blank
        o_html := REPLACE(o_html,'er_textarea_tag<','<'); */

        o_xml := V_XMLCLOB;
        o_xsl :=   V_XSLCLOB;

  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_GET_FORM_HTML :'||SQLERRM);
  END;
END; --end of sp_getprintform_html
------------------
PROCEDURE SP_GET_PRN_SECTION_HEADER (p_formid NUMBER, p_section NUMBER, o_xsl OUT VARCHAR2)
AS
 /****************************************************************************************************
   ** Procedure to get XSl for printing tabular type section's header
   ** Author: Anu Khanna 3 June 2004
   ** Input parameter: formid - PK of the form
   ** Input parameter: section id  - PK of the section
   ** Output parameter: headerXSL
   **/
   v_fldxsl VARCHAR2(4000);
   v_fld_esign_validate VARCHAR2(4000);
   v_sec_grid_label_beg_pos NUMBER;
   v_sec_grid_label_end_pos  NUMBER;
   v_sec_grid_header_label VARCHAR2(1000);
   v_help_beg_pos NUMBER;
   v_help_end_pos NUMBER;
   v_help_icon VARCHAR2(1000);
   v_sec_grid_header VARCHAR2(4000);
   v_fld_type CHAR(1) ;
   v_fld_datatype VARCHAR2(3);
BEGIN
    FOR i IN (SELECT  formfld_seq , formfld_xsl , fld_type , fld_datatype, fld_systemid
          FROM ER_FORMFLD, ER_FLDLIB
          WHERE  fk_formsec = p_section AND
          pk_field = fk_field AND
          NVL(fld_isvisible,0) <> 1 AND
          ER_FORMFLD.record_type <> 'D' AND fld_type <> 'H' AND NVL(fld_datatype,'Z') <> 'ML'  AND NVL(fld_type,'Z') <> 'F'
          ORDER BY formfld_seq  )
    LOOP
       v_fldxsl := i.formfld_xsl;
       v_fld_type := i.fld_type;
       v_fld_datatype := i.fld_datatype;
        IF v_fld_type = 'S' THEN
           v_sec_grid_header_label := '<td>Space Break</td>';
       ELSIF v_fld_datatype = 'ML' THEN
              v_sec_grid_header_label := '<td>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</td>';
       --added by Sonika as per issue # 984, they want Comments as header in case of Comments type field
       ELSIF v_fld_type = 'C' THEN
              v_sec_grid_header_label := '<td>Comments</td>';
       ELSE
           v_sec_grid_label_beg_pos := INSTR(v_fldxsl, '<label');
           v_sec_grid_label_end_pos  := INSTR(v_fldxsl, '</label>');
              v_sec_grid_label_end_pos := v_sec_grid_label_end_pos -  v_sec_grid_label_beg_pos + 8;
           v_sec_grid_header_label := '<td>' || SUBSTR(v_fldxsl,v_sec_grid_label_beg_pos,v_sec_grid_label_end_pos ) || '</td>';
        END IF;
         v_sec_grid_header := v_sec_grid_header || v_sec_grid_header_label  ;
    END LOOP;
    --p(substr(v_sec_grid_header,1,1000));
    o_xsl := v_sec_grid_header;
END; -- end of SP_GET_PRN_SECTION_HEADER
------------
--------------------------------anu----------
/****************************************************************************************************
   ** Procedure insert records in query and query status.
   ** Author: Anu Khanna 4th Jan 2005
   ** Input parameter: p_filledform_id - PK of the filled form
   ** Input parameter: p_called_from - module linked to (Stores 1- for patient forms, 2- for study forms, 3- for account  forms)
   ** Input parameter: p_query_type - (Stores 1- for System Query ,  2- for Manual Query , 3- for Response)
   ** Input parameter: p_query_type_id - Stores codelst id for a query type
   ** Input parameter: p_fldsys_ids - field system id for which query is entered
   ** Input parameter: p_fld_notes - notes for the query entered
    ** Input parameter: p_query_status - query status
  ** Input parameter: p_creator  - creator of the query
   ** Input parameter: p_ip_Add - ip address
   ** Output parameter: o_ret_number
   **/
--KM-#4016   
PROCEDURE SP_INSERT_FORM_QUERY(p_filledform_id IN NUMBER, p_called_from NUMBER, p_query_type NUMBER, p_query_type_id IN ARRAY_STRING,
                                                  p_fldsys_ids IN ARRAY_STRING,  p_fld_notes IN ARRAY_STRING,
                                                p_query_status IN ARRAY_STRING,
                                                p_mode IN ARRAY_STRING, p_pk_query_status IN ARRAY_STRING,
                                                p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER)
AS
v_count NUMBER;
v_pk_form_query NUMBER;
v_pk_form_query_status NUMBER;
i NUMBER;
v_fldsys_id VARCHAR2(50);
v_reason NUMBER;
v_note VARCHAR2(4000);
v_query_status NUMBER;
v_query_type_id NUMBER;
v_systemQueryId NUMBER;
v_mode VARCHAR2(50);
v_pk_query_status NUMBER;--KM
BEGIN
v_count := p_fldsys_ids.COUNT();
i := 1;
 SELECT pk_codelst INTO v_systemQueryId
     FROM ER_CODELST
     WHERE codelst_type ='form_query'
     AND codelst_subtyp = 'sys_gen_val';
     SELECT pk_codelst INTO v_query_status
     FROM ER_CODELST WHERE codelst_type = 'query_status' AND
     codelst_subtyp= 'open';
WHILE (i<= v_count) LOOP
     v_fldsys_id := p_fldsys_ids(i);
     v_note := p_fld_notes(i);
    -- v_query_status := TO_NUMBER(p_query_status(i));
     v_query_type_id := TO_NUMBER(p_query_type_id(i));
     v_mode :=  trim(p_mode(i));
     IF (v_mode = 'M') THEN
     v_pk_query_status := TO_NUMBER(p_pk_query_status(i)); --KM
     END IF;
    
     
    IF (v_mode = 'N') THEN
       SELECT seq_er_form_query.NEXTVAL
       INTO v_pk_form_query FROM dual ;
    INSERT INTO ER_FORMQUERY
     (PK_FORMQUERY ,FK_QUERYMODULE,
     QUERYMODULE_LINKEDTO, FK_FIELD,
     CREATOR,IP_ADD, created_on
      )
     VALUES
      (v_pk_form_query,p_filledform_id,
       p_called_from, (SELECT  pk_field FROM ER_FLDLIB WHERE FLD_SYSTEMID = v_fldsys_id),
      p_creator, p_ip_Add,SYSDATE);
      SELECT seq_er_form_query_status.NEXTVAL
       INTO v_pk_form_query_status FROM dual ;
       -- insert the system generated query for the fields
       INSERT INTO ER_FORMQUERYSTATUS
       (PK_FORMQUERYSTATUS, FK_FORMQUERY,
       FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
       QUERY_NOTES, ENTERED_ON, ENTERED_BY,
       FK_CODELST_QUERYSTATUS,
       CREATOR, IP_ADD, created_on)
       VALUES
       (v_pk_form_query_status, v_pk_form_query,
       1, v_systemQueryId ,
       v_note, SYSDATE, 0,
       v_query_status,
        p_creator, p_ip_Add, SYSDATE
       );
       SELECT seq_er_form_query_status.NEXTVAL
       INTO v_pk_form_query_status FROM dual ;
       -- enter the response for the query
       INSERT INTO ER_FORMQUERYSTATUS
       (PK_FORMQUERYSTATUS, FK_FORMQUERY,
       FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
       QUERY_NOTES, ENTERED_ON, ENTERED_BY,
       FK_CODELST_QUERYSTATUS,
       CREATOR, IP_ADD, created_on)
       VALUES
       (v_pk_form_query_status, v_pk_form_query,
       3, v_query_type_id,
       '', SYSDATE, 0,
       v_query_status,
        p_creator, p_ip_Add, SYSDATE
       );
ELSE
     --KM-30Nov09-Edit mode reason updation
       /*
       UPDATE ER_FORMQUERYSTATUS  
       SET fk_codelst_querytype = v_query_type_id
       WHERE
       pk_formquerystatus IN (SELECT pk_formquerystatus FROM ER_FORMQUERY, ER_FORMQUERYSTATUS
       WHERE fk_formquery = pk_formquery
       AND FORMQUERY_TYPE = 3
       AND fk_querymodule =  p_filledform_id
       ---AND entered_by =0 --KM
       AND fk_field = (SELECT  pk_field FROM ER_FLDLIB WHERE FLD_SYSTEMID = v_fldsys_id));
        END IF;
        */
       --KM-#4016
        UPDATE ER_FORMQUERYSTATUS  
       SET fk_codelst_querytype = v_query_type_id
       WHERE pk_formquerystatus = v_pk_query_status ;
        END IF;
        
        
i := i+1;
END LOOP;--while
COMMIT;
o_ret_number := v_pk_form_query;
RETURN;
END SP_INSERT_FORM_QUERY;
/****************************************************************************************************
   ** Procedure insert records in query and query status for a query or response
   ** Author: Anu Khanna 4th Jan 2005
   ** Input parameter: p_filledform_id - PK of the filled form
   ** Input parameter: p_called_from - module linked to (Stores 1- for patient forms, 2- for study forms, 3- for account  forms)
   ** Input parameter: p_query_type - (Stores 1- for System Query ,  2- for Manual Query , 3- for Response)
   ** Input parameter: p_query_type_id - Stores codelst id for a query type
   ** Input parameter: p_fldsys_ids - field system id for which query is entered
   ** Input parameter: p_fld_notes - notes for the query entered
    ** Input parameter: p_query_status - query status
  ** Input parameter: p_creator  - creator of the query
   ** Input parameter: p_ip_Add - ip address
   ** Output parameter: o_ret_number
   ** Modified by Jnanamay Majumdar on 12Mar2009: PKG_DATEUTIL.F_GET_DATEFORMAT added in place of hard coded date format
   **/
PROCEDURE SP_INSERT_QUERY_RESPONSE(p_filledform_id IN NUMBER, p_called_from NUMBER, p_fld_id NUMBER,
                                                 p_query_type NUMBER, p_query_type_id NUMBER,
                                                 p_query_status_id NUMBER,  p_fld_comments VARCHAR2,
                                               p_entered_on VARCHAR2, p_entered_by NUMBER,
                                               p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER)
AS
v_count NUMBER;
v_pk_form_query NUMBER;
v_pk_form_query_status NUMBER;
i NUMBER;
v_fldsys_id VARCHAR2(50);
v_reason NUMBER;
v_note VARCHAR2(4000);
v_query_status NUMBER;
v_query_type_id NUMBER;
v_systemQueryId NUMBER;
BEGIN
       SELECT seq_er_form_query.NEXTVAL
       INTO v_pk_form_query FROM dual ;
    INSERT INTO ER_FORMQUERY
     (PK_FORMQUERY ,FK_QUERYMODULE,
     QUERYMODULE_LINKEDTO, FK_FIELD,
     CREATOR,IP_ADD, created_on
      )
     VALUES
      (v_pk_form_query,p_filledform_id,
       p_called_from, p_fld_id,
      p_creator, p_ip_Add,SYSDATE);
      SELECT seq_er_form_query_status.NEXTVAL
       INTO v_pk_form_query_status FROM dual ;
       -- insert
       INSERT INTO ER_FORMQUERYSTATUS
       (PK_FORMQUERYSTATUS, FK_FORMQUERY,
       FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
       QUERY_NOTES, ENTERED_ON, ENTERED_BY,
       FK_CODELST_QUERYSTATUS,
       CREATOR, IP_ADD, created_on)
       VALUES
       (v_pk_form_query_status, v_pk_form_query,
       p_query_type, p_query_type_id ,
       p_fld_comments, TO_DATE(p_entered_on,PKG_DATEUTIL.F_GET_DATEFORMAT), p_entered_by,
       p_query_status_id,
        p_creator, p_ip_Add, SYSDATE
       );
COMMIT;
o_ret_number := v_pk_form_query;
RETURN;
END SP_INSERT_QUERY_RESPONSE;
--------------------------------anu----------

/* returns 1: if user has acccess to form's filled responses, 0: if user does not have access to form's filled responses
p_form : the form
p_user : the loggedin user
 p_filledform_creator : the creator of filled form record
by Sonia Abrol, 12/14/05, to use in AdHoc reports
*/

FUNCTION fn_getUserAccess (p_form NUMBER,p_user NUMBER,p_filledform_creator NUMBER) RETURN NUMBER
IS
v_ct NUMBER;
v_pk_lf NUMBER;
BEGIN

-- check if the linked form has 'keep response to private' defined

SELECT COUNT(*)
INTO v_pk_lf
FROM ER_LINKEDFORMS , ER_OBJECTSHARE a
WHERE fk_formlib = p_form AND a.fk_object = pk_lf AND
a.object_number=6  ;

IF v_pk_lf = 0 THEN
RETURN 1;
END IF;

SELECT DISTINCT pk_lf
INTO v_pk_lf
FROM ER_LINKEDFORMS , ER_OBJECTSHARE a
WHERE fk_formlib = p_form AND a.fk_object = pk_lf AND
a.object_number=6  ;

SELECT COUNT( *)
INTO v_ct
FROM ER_OBJECTSHARE a
WHERE fk_objectshare_id=p_user AND fk_object = v_pk_lf AND
(
( ( a.object_number=4 OR a.object_number=5 OR a.object_number=6) AND a.objectshare_type='U' )
OR p_user =p_filledform_creator ) ;

IF v_ct <= 0 THEN
RETURN 0;
ELSE
RETURN 1;
END IF;

END;

FUNCTION fn_formfld_validate_partial(p_form_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2) RETURN VARCHAR2 IS
v_retData NUMBER :=1;
v_targetForm NUMBER;
v_targetFld VARCHAR2(250);
v_operator VARCHAR2(5);
v_message VARCHAR2(4000);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'fn_formfld_validate_partial', pLEVEL  => Plog.LDEBUG);
BEGIN
Plog.DEBUG(pCTX,'Start Request with p_form_id='||p_form_id|| '  p_system_id'||p_system_id );
SELECT formaction_targetform,formaction_targetfld,formaction_operator,formaction_msg INTO v_targetForm,v_targetFld,v_operator,v_message FROM ER_FORMACTION
WHERE FORMACTION_SOURCEFORM=p_form_id AND FORMACTION_SOURCEFLD=p_system_id;

Plog.DEBUG(pCTX,'v_targetform=' ||v_targetForm || 'v_targetFld' || v_targetfld || 'p_datavalue '||p_datavalue|| 'p_account_id' ||p_account_id);

v_retData:=fn_formfld_validate(v_targetForm, 0, v_targetFld, p_study_id , p_patient_id , p_account_id , p_datavalue ,v_operator );
IF (v_retData>0) THEN
IF (LENGTH(v_message)>0) THEN
RETURN v_message;
ELSE
RETURN 'Validation Successful';
END IF;
END IF;

RETURN '';

EXCEPTION WHEN OTHERS THEN
 RAISE_APPLICATION_ERROR (-20102, 'fn_formfld_validate_partial:'||SQLERRM);
END;

FUNCTION fn_formfld_validate(p_form_id NUMBER, p_response_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2,p_operator VARCHAR2 ) RETURN NUMBER IS


v_retdata NUMBER :=1;
v_condition VARCHAR2(8000);
v_condition_expanded VARCHAR2(8000);
v_colname VARCHAR2(1000);
v_formtype_query_final VARCHAR2(1000);
v_colDataQuery VARCHAR2(1000);
v_formtype_query VARCHAR2(1000);
v_formtype VARCHAR2(10);
v_datacond VARCHAR2(1000);
v_fldType VARCHAR2(50);
v_date DATE;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'fn_formfld_validate', pLEVEL  => Plog.LDEBUG);
BEGIN
     v_condition:='';
     v_colname:='';

        v_colDataQuery:='SELECT count(*) FROM er_formslinear ';

        v_condition:=' WHERE fk_form='||p_form_id|| ' ';

              Plog.DEBUG(pCTX,'1....p_response_id' ||p_response_id || 'length(p_response_id)' || LENGTH(p_response_id)|| 'p_form_id is '||p_form_id);

        IF LENGTH(p_response_id)>0 AND p_response_id<>0  THEN
           v_condition:= v_condition||' and fk_filledform='||p_response_id;
        END IF;

         SELECT    mp_formtype, mp_mapcolname,mp_flddatatype  INTO v_formtype,v_colname,v_fldType FROM ER_MAPFORM WHERE
        mp_systemid=p_system_id AND fk_form=p_form_id;

        Plog.DEBUG(pCTX,'from type & Column Name  '|| v_formtype || 'and' ||v_colname||' and Field Type' ||v_fldType);

         IF(v_formtype='A')  THEN
                 v_condition :=v_condition|| ' and ID = '||p_account_id;
             ELSIF (v_formtype='P') AND LENGTH(LTRIM(RTRIM(p_patient_id)))>0 THEN
                  IF(LENGTH(LTRIM(RTRIM(p_study_id)))>0) AND (p_study_id<>0) THEN
                v_condition :=v_condition|| ' and ID = '||p_patient_id||' and fk_patprot =(select pk_patprot from er_patprot where fk_per='||p_patient_id||'and'||
                'fk_study='||p_study_id||')';
                ELSE
                v_condition :=v_condition|| ' and ID = '||p_patient_id;
                END IF;

                Plog.DEBUG(pCTX,'v_formtype and p_study_id' ||v_formtype||'and'||p_study_id);
             ELSIF(v_formtype='S') AND LENGTH(LTRIM(RTRIM(p_study_id)))>0 THEN
                         v_condition :=v_condition|| ' and ID = '||p_study_id;
         END IF;


             IF(v_fldType='EN') THEN
               Plog.DEBUG(pCTX,'responseID' ||p_response_id);
                  IF (p_response_id>0) THEN
                           v_condition:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'pkg_util.f_to_number('||v_colname||')';
                       ELSE

                         IF (p_operator='>') THEN
                             v_condition_expanded:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'(select max(pkg_util.f_to_number('||v_colname||')) from er_formslinear ' || v_condition||')';
                             ELSIF (p_operator='<') THEN
                             v_condition_expanded:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'(select min(pkg_util.f_to_number('||v_colname||')) from er_formslinear ' || v_condition||')';
                              ELSE
                              v_condition_expanded:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'pkg_util.f_to_number('||v_colname||')';
                            END IF;--for p_opeator
                        v_condition:=v_condition_expanded;
                    END IF;-- for p_response

              ELSIF (v_fldType='ED') THEN
               IF (p_response_id>0) THEN
                  v_condition:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'f_to_date('||v_colname||')';

                     ELSE
                  IF (p_operator='>') THEN
                       v_condition_expanded:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'(select max(f_to_date('||v_colname||')) from er_formslinear '||v_condition||')';
                      ELSIF(p_operator='<') THEN
                       v_condition_expanded:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'(select min(f_to_date('||v_colname||')) from er_formslinear '||v_condition||')';
                  ELSE
                      v_condition_expanded:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'f_to_date('||v_colname||')';

                    END IF;
                  v_condition:=v_condition_expanded;
               END IF; -- for p_response_id
              END IF;

              Plog.DEBUG(pCTX,'Final condition' ||v_condition);
              IF (LENGTH(v_condition)>0) THEN
                 v_coldataquery:=v_coldataquery||v_condition;
              END IF;


         Plog.DEBUG(pCTX,'Final Query'||v_coldataquery);

         EXECUTE IMMEDIATE v_coldataquery INTO v_retdata;

         Plog.DEBUG(pCTX,'Final Return value'|| v_retdata);

    RETURN v_retdata ;
    EXCEPTION WHEN OTHERS THEN
 RAISE_APPLICATION_ERROR (-20102, 'fn_formfld_validate:'||SQLERRM);
END;


END Pkg_Filledform;
/

