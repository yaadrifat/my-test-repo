set define off;

DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_INVOICE'
    AND COLUMN_NAME = 'COVERAGE_TYPE_CRITERIA'; 
	
	if (v_column_exists < 1) then
		execute immediate 'ALTER TABLE ER_INVOICE ADD COVERAGE_TYPE_CRITERIA VARCHAR2(100)';
		dbms_output.put_line('Column COVERAGE_TYPE_CRITERIA is added to ER_INVOICE');
	else
		dbms_output.put_line('Column COVERAGE_TYPE_CRITERIA already exists in ER_INVOICE');
	end if;
END;
/

commit;

COMMENT ON COLUMN ER_INVOICE.COVERAGE_TYPE_CRITERIA IS 'This column stores the Coverage Type Criteria';

commit;