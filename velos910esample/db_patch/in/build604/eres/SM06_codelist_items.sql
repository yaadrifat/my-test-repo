set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpSubmType' and codelst_subtyp = 'O';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpSubmType','O','Original','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpSubmType Subtype:O inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpSubmType Subtype:O already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpSubmType' and codelst_subtyp = 'A';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpSubmType','A','Amendment','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpSubmType Subtype:A inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpSubmType Subtype:A already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpSubmType' and codelst_subtyp = 'U';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpSubmType','U','Update','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpSubmType Subtype:U inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpSubmType Subtype:U already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = '0';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','0','0','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:0 inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:0 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = '1';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','1','1','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:1 inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:1 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = 'I/11';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','I/11','I/11','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:I/11 inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:I/11 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = 'II';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','II','II','N',4, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:II inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:II already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = 'II/III';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','II/III','II/III','N',5, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:II/III inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:II/III already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = 'III';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','III','III','N',6, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:III inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:III already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = 'IV';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','IV','IV','N',7, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:IV inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:IV already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyPhase' and codelst_subtyp = 'NA';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyPhase','NA','NA (Not Applicable)','N',8, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:NA inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyPhase Subtype:NA already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'drug';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','drug','Drug','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:drug inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:drug already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'device';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','device','Device','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:device inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:device already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'bioOrVaccine';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','bioOrVaccine','Biological/Vaccine','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:bioOrVaccine inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:bioOrVaccine already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'procOrSurgery';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','procOrSurgery','Procedure/Surgery','N',4, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:procOrSurgery inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:procOrSurgery already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'radBehavioral';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','radBehavioral','Radiation, Behavioral','N',5, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:radBehavioral inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:radBehavioral already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'genetic';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','genetic','Genetic','N',6, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:genetic inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:genetic already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'dietSupplement';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','dietSupplement','Dietary Supplement','N',7, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:dietSupplement inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:dietSupplement already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpIntvnType' and codelst_subtyp = 'other';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpIntvnType','other','Other','N',8, null);
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:other inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpIntvnType Subtype:other already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'treatment';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','treatment','Treatment','N',1, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:treatment inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:treatment already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'prevention';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','prevention','Prevention','N',2, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:prevention inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:prevention already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'supportiveCare';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','supportiveCare','Supportive Care','N',3, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:supportiveCare inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:supportiveCare already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'screening';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','screening','Screening','N',4, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:screening inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:screening already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'diagnostic';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','diagnostic','Diagnostic','N',5, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:diagnostic inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:diagnostic already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'healthSvcRsrch';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','healthSvcRsrch','Health Services Research','N',6, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:healthSvcRsrch inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:healthSvcRsrch already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'basicScience';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','basicScience','Basic Science','N',7, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:basicScience inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:basicScience already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'studyPurpose' and codelst_subtyp = 'other';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'studyPurpose','other','Other','N',8, null);
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:other inserted');
	else
		dbms_output.put_line('Code-list item Type:studyPurpose Subtype:other already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'institution';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','institution','Institution','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:institution inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:institution already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'orderingGroup ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','orderingGroup ','Ordering group ','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:orderingGroup  inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:orderingGroup  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'repository ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','repository ','Repository ','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:repository  inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:repository  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'researchBased ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','researchBased ','Research based ','N',4, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:researchBased  inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:researchBased  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'cooperativeGrp';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','cooperativeGrp','Cooperative group','N',5, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:cooperativeGrp inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:cooperativeGrp already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'cancerCenter ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','cancerCenter ','Cancer center ','N',6, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:cancerCenter  inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:cancerCenter  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'consortium';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','consortium','Consortium','N',7, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:consortium inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:consortium already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'drugCompany';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','drugCompany','Drug company','N',8, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:drugCompany inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:drugCompany already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpOrgType' and codelst_subtyp = 'network';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpOrgType','network','Network','N',9, null);
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:network inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpOrgType Subtype:network already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'inReview';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','inReview','In Review','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:inReview inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:inReview already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'approved';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','approved','Approved','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:approved inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:approved already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'active';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','active','Active','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:active inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:active already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'closedAccrual';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','closedAccrual','Closed to Accrual','N',4, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:closedAccrual inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:closedAccrual already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'closedAccIntv';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','closedAccIntv','Closed to Accrual and Intervention','N',5, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:closedAccIntv inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:closedAccIntv already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'tempClosedAcc';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','tempClosedAcc','Temporarily Closed to Accrual','N',6, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:tempClosedAcc inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:tempClosedAcc already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'tempClosAccIntv';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','tempClosAccIntv','Temporarily Closed to Accrual and Intervention','N',7, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:tempClosAccIntv inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:tempClosAccIntv already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'complete';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','complete','Complete','N',8, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:complete inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:complete already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'adminComplete ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','adminComplete ','Administratively Complete ','N',9, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:adminComplete  inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:adminComplete  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpStudyStatus' and codelst_subtyp = 'withdrawn';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpStudyStatus','withdrawn','Withdrawn','N',10, null);
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:withdrawn inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpStudyStatus Subtype:withdrawn already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'notYetRecruit';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','notYetRecruit','Not yet recruiting','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:notYetRecruit inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:notYetRecruit already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'recruiting';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','recruiting','Recruiting','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:recruiting inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:recruiting already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'enrollByInvite';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','enrollByInvite','Enrolling by invitation','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:enrollByInvite inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:enrollByInvite already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'active';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','active','Active','N',4, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:active inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:active already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'notRecruiting';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','notRecruiting','Not recruiting','N',5, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:notRecruiting inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:notRecruiting already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'completed';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','completed','Completed','N',6, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:completed inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:completed already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'suspended';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','suspended','Suspended','N',7, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:suspended inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:suspended already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'terminated';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','terminated','Terminated','N',8, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:terminated inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:terminated already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpRecruitStat' and codelst_subtyp = 'withdrawn';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpRecruitStat','withdrawn','Withdrawn','N',9, null);
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:withdrawn inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpRecruitStat Subtype:withdrawn already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEGrantor' and codelst_subtyp = 'CDRH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEGrantor','CDRH','CDRH','N',1, null);
		dbms_output.put_line('Code-list item Type:INDIDEGrantor Subtype:CDRH inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEGrantor Subtype:CDRH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEGrantor' and codelst_subtyp = 'CDER';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEGrantor','CDER','CDER','N',2, null);
		dbms_output.put_line('Code-list item Type:INDIDEGrantor Subtype:CDER inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEGrantor Subtype:CDER already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEGrantor' and codelst_subtyp = 'CBER';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEGrantor','CBER','CBER','N',3, null);
		dbms_output.put_line('Code-list item Type:INDIDEGrantor Subtype:CBER inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEGrantor Subtype:CBER already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'investigator';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEHolder','investigator','Investigator','N',1, null);
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:investigator inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:investigator already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'organization';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEHolder','organization','Organization','N',2, null);
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:organization inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:organization already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'industry';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEHolder','industry','Industry','N',3, null);
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:industry inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:industry already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'NIH';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEHolder','NIH','NIH','N',4, null);
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:NIH inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:NIH already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'NCI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEHolder','NCI','NCI','N',5, null);
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:NCI inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:NCI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEHolder' and codelst_subtyp = 'PI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEHolder','PI','PI','N',6, null);
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:PI inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEHolder Subtype:PI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEAccess' and codelst_subtyp = 'available';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEAccess','available','Available','N',1, null);
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:available inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:available already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEAccess' and codelst_subtyp = 'nolongavailable';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEAccess','nolongavailable','No longer available','N',2, null);
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:nolongavailable inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:nolongavailable already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEAccess' and codelst_subtyp = 'tempunavailable';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEAccess','tempunavailable','Temporarily not available','N',3, null);
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:tempunavailable inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:tempunavailable already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'INDIDEAccess' and codelst_subtyp = 'ApprovedMarket';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'INDIDEAccess','ApprovedMarket','Approved for marketing','N',4, null);
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:ApprovedMarket inserted');
	else
		dbms_output.put_line('Code-list item Type:INDIDEAccess Subtype:ApprovedMarket already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'protocolDoc';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','protocolDoc','Protocol Document','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:protocolDoc inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:protocolDoc already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'IRBApproval';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','IRBApproval','IRB Approval','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:IRBApproval inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:IRBApproval already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'PartSitesList';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','PartSitesList','List of Participating Sites','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:PartSitesList inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:PartSitesList already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'informedConsent';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','informedConsent','Informed Consent Document ','N',4, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:informedConsent inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:informedConsent already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'changeMemoDoc';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','changeMemoDoc','Change Memo Document','N',5, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:changeMemoDoc inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:changeMemoDoc already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'protHighlight';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','protHighlight','Protocol Highlight Document ','N',6, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:protHighlight inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:protHighlight already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'abbrTrialTempl';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','abbrTrialTempl','Abbreviated Trial Template ','N',7, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:abbrTrialTempl inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:abbrTrialTempl already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDocType' and codelst_subtyp = 'other';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDocType','other','Other','N',8, null);
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:other inserted');
	else
		dbms_output.put_line('Code-list item Type:ctrpDocType Subtype:other already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'add_type' and codelst_subtyp = 'leadOrg';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'add_type','leadOrg','Lead organization','N',1, null);
		dbms_output.put_line('Code-list item Type:add_type Subtype:leadOrg inserted');
	else
		dbms_output.put_line('Code-list item Type:add_type Subtype:leadOrg already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'add_type' and codelst_subtyp = 'SubmittingOrg';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'add_type','SubmittingOrg','Submitting organization','N',2, null);
		dbms_output.put_line('Code-list item Type:add_type Subtype:SubmittingOrg inserted');
	else
		dbms_output.put_line('Code-list item Type:add_type Subtype:SubmittingOrg already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'add_type' and codelst_subtyp = 'PI';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'add_type','PI','Principal investigator','N',3, null);
		dbms_output.put_line('Code-list item Type:add_type Subtype:PI inserted');
	else
		dbms_output.put_line('Code-list item Type:add_type Subtype:PI already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'add_type' and codelst_subtyp = 'summary4Sponsor';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'add_type','summary4Sponsor','Summary4 sponsor','N',4, null);
		dbms_output.put_line('Code-list item Type:add_type Subtype:summary4Sponsor inserted');
	else
		dbms_output.put_line('Code-list item Type:add_type Subtype:summary4Sponsor already exists');
	end if;
	
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'add_type' and codelst_subtyp = 'summary4Sponsor';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'add_type','sponsorContact','Sponsor Contact','N',5, null);
		dbms_output.put_line('Code-list item Type:add_type Subtype:sponsorContact inserted');
	else
		dbms_output.put_line('Code-list item Type:add_type Subtype:sponsorContact already exists');
	end if;

COMMIT;

end;
/
