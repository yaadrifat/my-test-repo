CREATE OR REPLACE TRIGGER "ERES"."ER_CTRP_DOCUMENTS_BI0" BEFORE INSERT ON ER_CTRP_DOCUMENTS 
FOR EACH ROW 
DECLARE
	raid NUMBER(10);
	erid NUMBER(10);
	usr VARCHAR(2000);
	insert_data CLOB;
BEGIN
	BEGIN
		SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
		WHERE pk_user = :NEW.creator ;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		USR := 'New User' ;
	END ;

	SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
	:NEW.rid := erid ;
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;

	audit_trail.record_transaction(raid, 'ER_CTRP_DOCUMENTS',erid, 'I',usr);
	insert_data:= :NEW.PK_CTRP_DOC||'|'||
	:NEW.FK_CTRP_DRAFT||'|'||
	:NEW.FK_STUDYAPNDX||'|'||
	:NEW.FK_CODELST_CTRP_DOCTYPE||'|'||
	:NEW.RID||'|'||
	:NEW.IP_ADD||'|'||
	:NEW.CREATOR||'|'||
	TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||
	:NEW.LAST_MODIFIED_BY||'|'||
	TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);
	
	INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END; 
/


CREATE OR REPLACE TRIGGER ERES.ER_CTRP_DOCUMENTS_AU0
AFTER UPDATE OF PK_CTRP_DOC,FK_CTRP_DRAFT,FK_STUDYAPNDX,FK_CODELST_CTRP_DOCTYPE,
IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE
ON ERES.ER_CTRP_DOCUMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);
	old_modby VARCHAR2(100);
	new_modby VARCHAR2(100);
BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);
	audit_trail.record_transaction
	(raid, 'ER_CTRP_DOCUMENTS', :OLD.rid, 'U', usr);

	IF NVL(:OLD.PK_CTRP_DOC,0) != NVL(:NEW.PK_CTRP_DOC,0) then
		audit_trail.column_update (raid, 'PK_CTRP_DOC',:OLD.PK_CTRP_DOC,:NEW.PK_CTRP_DOC);
	end if;
	IF NVL(:OLD.FK_CTRP_DRAFT,0) != NVL(:NEW.FK_CTRP_DRAFT,0) then
		audit_trail.column_update (raid, 'FK_CTRP_DRAFT',:OLD.FK_CTRP_DRAFT,:NEW.FK_CTRP_DRAFT);
	end if;
	IF NVL(:OLD.FK_STUDYAPNDX,0) != NVL(:NEW.FK_STUDYAPNDX,0) then
		audit_trail.column_update (raid, 'FK_STUDYAPNDX',:OLD.FK_STUDYAPNDX,:NEW.FK_STUDYAPNDX);
	end if;
	IF NVL(:OLD.FK_CODELST_CTRP_DOCTYPE,0) != NVL(:NEW.FK_CODELST_CTRP_DOCTYPE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_CTRP_DOCTYPE',:OLD.FK_CODELST_CTRP_DOCTYPE,:NEW.FK_CODELST_CTRP_DOCTYPE);
	end if;
	IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') then
		audit_trail.column_update (raid, 'IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD);
	end if;

	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) then
		BEGIN
			SELECT TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
			INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				old_modby := NULL;
		END ;
		BEGIN
			SELECT TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
			INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				new_modby := NULL;
		 END ;
		audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
	end if;
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
		audit_trail.column_update (raid, 'LAST_MODIFIED_DATE',to_char(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
END;
/

CREATE OR REPLACE TRIGGER "ERES"."ER_CTRP_DOCUMENTS_AD0" 
AFTER  UPDATE
ON ER_CTRP_DOCUMENTS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	raid NUMBER(10);
	deleted_data VARCHAR2(4000);
BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	audit_trail.record_transaction (raid, 'ER_CTRP_DOCUMENTS', :OLD.rid, 'D');

	deleted_data := TO_CHAR(:OLD.PK_CTRP_DOC) ||'|'||
	TO_CHAR(:OLD.FK_CTRP_DRAFT) ||'|'||
	TO_CHAR(:OLD.FK_STUDYAPNDX) ||'|'||
	TO_CHAR(:OLD.FK_CODELST_CTRP_DOCTYPE) ||'|'||
	TO_CHAR(:OLD.RID) ||'|'||
	TO_CHAR(:OLD.IP_ADD) ||'|'||
	TO_CHAR(:OLD.CREATOR) ||'|'||
	TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||
	TO_CHAR(:OLD.LAST_MODIFIED_BY) ||'|'||
	TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);
	
	INSERT INTO audit_delete(raid, row_data) VALUES (raid, deleted_data);
END;
/

CREATE OR REPLACE TRIGGER "ERES"."ER_CTRP_DOCUMENTS_BU0"
BEFORE UPDATE
ON ERES.ER_CTRP_DOCUMENTS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW   WHEN ( NEW.last_modified_by is not null) BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
/