create table sch_portal_forms(PK_PF number primary key not null,
FK_portal NUMBER,
Fk_calendar number,
FK_event number,
FK_form number,
RID NUMBER, 
CREATOR NUMBER, 
LAST_MODIFIED_BY NUMBER, 
LAST_MODIFIED_DATE DATE, 
CREATED_ON DATE DEFAULT sysdate, 
IP_ADD VARCHAR2(15 BYTE) );

Comment on column sch_portal_forms.PK_PF is 'this is the primary key';
Comment on column sch_portal_forms.FK_portal is 'this stores the portal id';
Comment on column sch_portal_forms.Fk_calendar is 'this stores the calendar id';
Comment on column sch_portal_forms.FK_event is 'this stores the event id';
Comment on column sch_portal_forms.FK_form is 'this stores the form id, Portal login user has access to';

COMMENT ON COLUMN sch_portal_forms.RID IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

COMMENT ON COLUMN sch_portal_forms.CREATOR IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. Stores PK of table ER_USER.';

COMMENT ON COLUMN sch_portal_forms.LAST_MODIFIED_BY IS 'The last_modified_by identifies the user who last modified this row. Stores PK of table ER_USER.';

COMMENT ON COLUMN sch_portal_forms.LAST_MODIFIED_DATE IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';

COMMENT ON COLUMN sch_portal_forms.CREATED_ON IS 'This column is used for Audit Trail. Stores the date on which this row was created.';

COMMENT ON COLUMN sch_portal_forms.IP_ADD IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';

