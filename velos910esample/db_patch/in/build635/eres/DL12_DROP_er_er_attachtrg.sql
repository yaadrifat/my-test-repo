SET define OFF;
/*This code disables update triggers */
DECLARE 
valueCountDisable NUMBER(5);
valueCountDisable1 NUMBER(5);
BEGIN
  SELECT COUNT(*) into valueCountDisable FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'ER_ER_ATTACHMENTS_AU0';
    IF (valueCountDisable = 1) THEN
       EXECUTE IMMEDIATE 'drop trigger ER_ER_ATTACHMENTS_AU0';
        COMMIT;
    END IF;    
	 SELECT COUNT(*) into valueCountDisable1 FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'ER_ER_ATTACHMENTS_BI0';
    IF (valueCountDisable1 = 1) THEN
       EXECUTE IMMEDIATE 'drop trigger ER_ER_ATTACHMENTS_BI0';
        COMMIT;
    END IF;  
END;
/


