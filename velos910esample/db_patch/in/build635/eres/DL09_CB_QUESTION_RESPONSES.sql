--INSERTING DATA INTO CB_QUESTION_RESPONSES TABLE --
--MRQ N2F FORM--
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='Hep_B_imm_glob_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='Hep_B_imm_glob_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_hmn_pit_gh_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_hmn_pit_gh_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_desc');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_desc'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_smallpox_vaccine_8wk_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_smallpox_vaccine_8wk_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4mo_illness_symptom_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4mo_illness_symptom_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_desc');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_desc'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_disease_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_disease_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_prblm_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_prblm_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hep_pos_tst_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hep_pos_tst_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='para_chagas_babesiosis_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='para_chagas_babesiosis_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diag_neuro_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diag_neuro_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='blood_transf_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='blood_transf_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tissue_graft_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tissue_graft_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_shared_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_shared_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='piercing_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='piercing_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 28--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_gono_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_gono_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 29--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_mn_dr_pmt_for_sex_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_mn_dr_pmt_for_sex_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 30--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_tk_mn_dr_pmt_sex_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_tk_mn_dr_pmt_sex_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 31--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_cntc_liv_jaund_hep_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_cntc_liv_jaund_hep_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 37--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_tk_mn_dr_pmt_sex_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_tk_mn_dr_pmt_sex_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 40.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 40.h--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 41--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='htlv_incl_screen_test_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='htlv_incl_screen_test_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 46--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 47--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 53--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='addendum_questions_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='addendum_questions_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--MRQ N2E FORM--
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='propecia_last_month_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='propecia_last_month_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--Question 3.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='accutane_last_month_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='accutane_last_month_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='soriatane_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='soriatane_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.f--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ever_tkn_tegison_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ever_tkn_tegison_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4wk_illness_symptom_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4wk_illness_symptom_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='west_nile_preg_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='west_nile_preg_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acc_ns_stk_cntc_bld_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acc_ns_stk_cntc_bld_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_gono_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_gono_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 33--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_RESPONSES
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind');
  if (v_record_exists = 0) then
	Insert into CB_QUESTION_RESPONSES(PK_QUESTION_RESPONSES,FK_QUESTION,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) values ( SEQ_CB_QUESTION_RESPONSES.nextval,(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind'),(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--END--
