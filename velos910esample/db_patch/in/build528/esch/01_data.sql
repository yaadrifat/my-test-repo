--Adding Site of Service column to event_def

ALTER TABLE ESCH.EVENT_DEF ADD (SERVICE_SITE_ID NUMBER);

COMMENT ON COLUMN ESCH.EVENT_DEF.SERVICE_SITE_ID IS 'This column represents Site of service id (fk_site) of event.';

--Adding Facility ID column to event_def

ALTER TABLE ESCH.EVENT_DEF ADD (FACILITY_ID NUMBER);

COMMENT ON COLUMN ESCH.EVENT_DEF.FACILITY_ID IS 'This column represents Facility id (fk_site) of event.';

--Adding Coverage Type column to event_def

ALTER TABLE ESCH.EVENT_DEF ADD (FK_CODELST_COVERTYPE NUMBER);

COMMENT ON COLUMN ESCH.EVENT_DEF.FK_CODELST_COVERTYPE IS 'This column represents fk sch_codelst for Coverage Type of event.';

--Adding Site of Service column to EVENT_ASSOC

ALTER TABLE ESCH.EVENT_ASSOC ADD (SERVICE_SITE_ID NUMBER);

COMMENT ON COLUMN ESCH.EVENT_ASSOC.SERVICE_SITE_ID IS 'This column represents Site of service id (fk_site) of event.';

--Adding Facility ID column to EVENT_ASSOC

ALTER TABLE ESCH.EVENT_ASSOC ADD (FACILITY_ID NUMBER);

COMMENT ON COLUMN ESCH.EVENT_ASSOC.FACILITY_ID IS 'This column represents Facility id (fk_site) of event.';

--Adding Coverage Type column to EVENT_ASSOC

ALTER TABLE ESCH.EVENT_ASSOC
ADD (FK_CODELST_COVERTYPE NUMBER);

COMMENT ON COLUMN ESCH.EVENT_ASSOC.FK_CODELST_COVERTYPE IS 'This column represents fk sch_codelst for Coverage Type of event.';

--Adding Site of Service column to SCH_EVENTS1

ALTER TABLE ESCH.SCH_EVENTS1 ADD (SERVICE_SITE_ID NUMBER);

COMMENT ON COLUMN ESCH.SCH_EVENTS1.SERVICE_SITE_ID IS 'This column represents Site of service id (fk_site) of event.';

--Adding Facility ID column to SCH_EVENTS1

ALTER TABLE ESCH.SCH_EVENTS1 ADD (FACILITY_ID NUMBER);

COMMENT ON COLUMN ESCH.SCH_EVENTS1.FACILITY_ID IS 'This column represents Facility id (fk_site) of event.';

--Adding Coverage Type column to SCH_EVENTS1

ALTER TABLE ESCH.SCH_EVENTS1
ADD (FK_CODELST_COVERTYPE NUMBER);

COMMENT ON COLUMN
ESCH.SCH_EVENTS1.FK_CODELST_COVERTYPE IS
'This column represents fk sch_codelst for Coverage Type of event.'

-- INSERTING into SCH_CODELST
Insert into SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
values (SCH_CODELST_SEQ1.nextval,'coverage_type','M','Billable to Patient and/or Third Party Payer','N',1);

Insert into SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
values (SCH_CODELST_SEQ1.nextval,'coverage_type','S','Billable to Research Study','N',2);

Insert into SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
values (SCH_CODELST_SEQ1.nextval,'coverage_type','M/S','Coverage Determined by Patient''s Insurance Payer','N',3);

Insert into SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
values (SCH_CODELST_SEQ1.nextval,'coverage_type','NB','Non-billable','N',4);

commit;


alter table sch_codelst modify codelst_custom_col1 varchar2(4000);

alter table sch_codelst add codelst_study_role Varchar2 (4000);

comment on column sch_codelst.codelst_study_role  is 'This is used to store the logged in user''s Study Team Role';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,1,'01_data.sql',sysdate,'8.9.0 Build#528');

commit;
