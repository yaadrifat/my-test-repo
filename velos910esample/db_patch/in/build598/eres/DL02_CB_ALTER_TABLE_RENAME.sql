--Alter table to correct column name errors. Used the Select statements to avoid the duplicate columns if these are created in QA database --by hibernate.

Set Define off;

DECLARE
    cnt1 NUMBER;
begin
 
    SELECT count(1) into cnt1 FROM user_tab_columns alt WHERE lower(alt.column_name) = lower('LAST_MODIFIED_DATE') and lower(alt.table_name) =lower('CB_RECEIPANT_INFO');
    IF  ( cnt1=0) THEN
       EXECUTE immediate('alter table CB_RECEIPANT_INFO rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE'); 
    END IF;

    SELECT count(1) into cnt1 FROM user_tab_columns alt WHERE lower(alt.column_name) = lower('LAST_MODIFIED_DATE') and lower(alt.table_name) =lower('CB_REQUEST');
    IF  ( cnt1=0) THEN
       EXECUTE immediate('alter table CB_REQUEST rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE'); 

  END IF;

    SELECT count(1) into cnt1 FROM user_tab_columns alt WHERE lower(alt.column_name) = lower('LAST_MODIFIED_DATE') and lower(alt.table_name) =lower('CB_SHIPMENT');
    IF  ( cnt1=0) THEN
       EXECUTE immediate('alter table CB_SHIPMENT rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE'); 
  END IF;

    SELECT count(1) into cnt1 FROM user_tab_columns alt WHERE lower(alt.column_name) = lower('LAST_MODIFIED_DATE') and lower(alt.table_name) =lower('CB_ENTITY_SAMPLES');
    IF  ( cnt1=0) THEN
       EXECUTE immediate('alter table CB_ENTITY_SAMPLES rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE'); 
  END IF;

    SELECT count(1) into cnt1 FROM user_tab_columns alt WHERE lower(alt.column_name) = lower('LAST_MODIFIED_DATE') and lower(alt.table_name) =lower('CB_NOTES');
    IF  ( cnt1=0) THEN
       EXECUTE immediate('alter table CB_NOTES rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE'); 

  END IF;

    SELECT count(1) into cnt1 FROM user_tab_columns alt WHERE lower(alt.column_name) = lower('LAST_MODIFIED_DATE') and lower(alt.table_name) =lower('CB_ENTITY_STATUS');
    IF  ( cnt1=0) THEN
       EXECUTE immediate('alter table CB_ENTITY_STATUS rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE'); 

  END IF;

    SELECT count(1) into cnt1 FROM user_tab_columns alt WHERE lower(alt.column_name) = lower('LAST_MODIFIED_DATE') and lower(alt.table_name) =lower('CB_ENTITY_STATUS_REASON');
    IF  ( cnt1=0) THEN
       EXECUTE immediate('alter table CB_ENTITY_STATUS_REASON rename column LAST_MODIFIED_ON to LAST_MODIFIED_DATE'); 

  END IF;

end;
/


