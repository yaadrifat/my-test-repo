set define off;

create or replace FUNCTION "ESCH"."LST_ADDITIONALCODES_FINANCIAL" (event number,p_additional_code_type varchar2, accessYN number) return varchar is
  cursor addl_code_all
  is
    select trim(' &bull; ' || (codelst_desc) || ' - ' || md_modelementdata || '<br>') event_addlcode
    from  er_moredetails, er_codelst
    where pk_codelst = md_modelementpk
    and fk_modpk = event and md_modname = p_additional_code_type
    and CODELST_TYPE = p_additional_code_type
    order by codelst_seq;
  cursor addl_code_financial
  is
    select trim(' &bull; ' || (codelst_desc) || ' - ' || md_modelementdata || '<br>') event_addlcode
    from  er_moredetails, er_codelst
    where pk_codelst = md_modelementpk
    and fk_modpk = event and md_modname = p_additional_code_type
    and CODELST_TYPE = p_additional_code_type and codelst_kind ='financial'
    order by codelst_seq;
  cursor addl_code_nonFinancial
  is
    select trim(' &bull; ' || (codelst_desc) || ' - ' || md_modelementdata || '<br>') event_addlcode
    from  er_moredetails, er_codelst
    where pk_codelst = md_modelementpk
    and fk_modpk = event and md_modname = p_additional_code_type
    and CODELST_TYPE = p_additional_code_type and (codelst_kind is null or codelst_kind !='financial')
    order by codelst_seq;

  addl_lst varchar2(32000);
begin
  case (accessYN)
  when 0 then
    for l_rec in addl_code_nonFinancial loop
		addl_lst := addl_lst || l_rec.event_addlcode ;
    end loop;
  when -1 then
	for l_rec in addl_code_financial loop
		addl_lst := addl_lst || l_rec.event_addlcode ;
    end loop;
  else
    for l_rec in addl_code_all loop
		addl_lst := addl_lst || l_rec.event_addlcode ;
    end loop;
  end case;
 --dbms_output.put_line (substr(addl_lst,1,length(addl_lst)-1) ) ;

return addl_lst ;
end ;
/
