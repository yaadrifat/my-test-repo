set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_CORD_COMPLETE_REQ_INFO WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_CORD_COMPLETE_REQ_INFO  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/