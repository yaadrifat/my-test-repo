set define off;

create or replace view rep_cbu_details as 
select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
(SELECT notes FROM cb_notes WHERE entity_id = pk_cord AND fk_notes_category = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'note_cat' AND codelst_subtyp = 'id')) idnotes, 
(select site_name from er_site where pk_site = fk_cbb_id) cbbid, (select codelst_desc from er_codelst where pk_codelst = fk_cbu_stor_loc) storage_loc, 
(select site_name from er_site where pk_site = fk_cbu_coll_site) cbu_collection_site, (SELECT notes FROM cb_notes WHERE visibility = '1' and entity_id = pk_cord AND fk_notes_category = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'note_cat' AND codelst_subtyp = 'cbu_info')) cbu_info_notes, 
(select codelst_desc from er_codelst where pk_codelst = fk_cord_cbu_eligible_status) eligible_status,  (select codelst_desc from er_codelst where pk_codelst = fk_cord_cbu_lic_status) lic_status, (SELECT notes FROM cb_notes WHERE visibility = '1' and entity_id = pk_cord AND fk_notes_category = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'note_cat' AND codelst_subtyp = 'eligiblity')) eligibility_notes, 
prcsng_start_date, (select codelst_desc from er_codelst where pk_codelst = fk_cord_bact_cul_result) bact_culture , bact_comment,
frz_date, (select codelst_desc from er_codelst where pk_codelst = fk_cord_fungal_cul_result) fungal_culture, fung_comment, (select codelst_desc from er_codelst where pk_codelst = fk_cord_abo_blood_type) abo_blood_type,
(select codelst_desc from er_codelst where pk_codelst = hemoglobin_scrn) hemoglobin_scrn,
(select codelst_desc from er_codelst where pk_codelst =fk_cord_rh_type) rh_type, (SELECT notes FROM cb_notes WHERE visibility = '1' and entity_id = pk_cord AND fk_notes_category = (SELECT pk_codelst FROM er_codelst WHERE codelst_type = 'note_cat' AND codelst_subtyp = 'lab_sum')) lab_sum_notes,
(select codelst_desc from er_codelst where pk_codelst= (select fk_proc_meth_id from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) )) processing,
(select codelst_desc from er_codelst where pk_codelst= (select fk_if_automated from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) )) automated_type,
(select other_processing from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) other_processing,
(select codelst_desc from er_codelst where pk_codelst= (select fk_product_modification from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) )) product_modification,
(select other_prod_modi from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) other_product_modi,
(select no_of_indi_frac from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) individual_frac,
(select codelst_desc from er_codelst where pk_codelst= (select fk_num_of_bags from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) )) no_of_bags,
(select other_bag_type from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) other_bag,
(select filter_paper from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) filter_paper,
(select rbc_pallets from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) rpc_pellets,
(select no_of_extr_dna_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) extr_dna,
(select no_of_serum_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) serum_aliquotes,
(select no_of_plasma_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) plasma_aliquotes,
(select no_of_nonviable_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) nonviable_aliquotes,
(select no_of_viable_smpl_final_prod from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) viable_samp_final_prod,
(select no_of_segments from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) no_of_segments,
(select no_of_oth_rep_alliquots_f_prod from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) no_of_oth_rep_alliquots_f_prod,
(select no_of_oth_rep_alliq_alter_cond from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) no_of_oth_rep_alliq_alter_cond,
(select no_of_serum_mater_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) no_of_serum_mater_aliquots,
(select no_of_plasma_mater_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) no_of_plasma_mater_aliquots,
(select no_of_extr_dna_mater_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) no_of_extr_dna_mater_aliquots,
(select no_of_cell_mater_aliquots from cbb_processing_procedures_info where fk_processing_id = (select fk_cbb_procedure from cb_cord where pk_cord = a.pk_cord) ) no_of_cell_mater_aliquots
FROM cb_cord  a; 

commit;