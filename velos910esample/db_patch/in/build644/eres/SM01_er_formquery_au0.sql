CREATE OR REPLACE TRIGGER "ER_FORMQUERY_AU0"
  AFTER UPDATE OF
  pk_formquery,
  fk_querymodule,
  querymodule_linkedto,
 fk_field,
query_name,
query_desc,
  last_modified_by,
  last_modified_date,
  ip_add,
  rid
  ON er_formquery
  FOR EACH ROW
DECLARE
  raid NUMBER(10);

   usr VARCHAR2(100);

   old_modby VARCHAR2(100);

   new_modby VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_FORMQUERY', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_formquery,0) !=
     NVL(:NEW.pk_formquery,0) THEN
     audit_trail.column_update
       (raid, 'PK_FORMQUERY',
       :OLD.pk_formquery, :NEW.pk_formquery);
  END IF;
  IF NVL(:OLD.fk_querymodule,0) !=
     NVL(:NEW.fk_querymodule,0) THEN
     audit_trail.column_update
       (raid, 'FK_QUERYMODULE',
       :OLD.fk_querymodule, :NEW.fk_querymodule);
  END IF;
  IF NVL(:OLD.querymodule_linkedto,0) !=
     NVL(:NEW.querymodule_linkedto,0) THEN
     audit_trail.column_update
       (raid, 'QUERYMODULE_LINKEDTO',
       :OLD.querymodule_linkedto, :NEW.querymodule_linkedto);
  END IF;
  IF NVL(:OLD.FK_FIELD,0) !=
     NVL(:NEW.FK_FIELD,0) THEN
     audit_trail.column_update
       (raid, 'FK_FIELD',
       :OLD.FK_FIELD, :NEW.FK_FIELD);
  END IF;
  IF NVL(:OLD.query_name,'') !=
     NVL(:NEW.query_name,0) THEN
     audit_trail.column_update
       (raid, 'QUERY_NAME',
       :OLD.query_name, :NEW.query_name);
  END IF;
  IF NVL(:OLD.query_desc,'') !=
     NVL(:NEW.query_desc,0) THEN
     audit_trail.column_update
       (raid, 'QUERY_DESC',
       :OLD.query_desc, :NEW.query_desc);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
 IF NVL(:OLD.LAST_MODIFIED_BY,0) !=
 NVL(:NEW.LAST_MODIFIED_BY,0) THEN
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.last_modified_by ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			old_modby := NULL;
	END ;
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			new_modby := NULL;
	 END ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 END IF;
 IF NVL(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
 END IF;
END;
/

