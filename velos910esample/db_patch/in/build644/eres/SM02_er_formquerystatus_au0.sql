CREATE OR REPLACE TRIGGER "ER_FORMQUERYSTATUS_AU0"
  AFTER UPDATE OF
  pk_formquerystatus,
  fk_formquery,
 formquery_type,
 fk_codelst_querytype,
query_notes,
entered_by,
entered_on,
fk_codelst_querystatus,
  last_modified_by,
  last_modified_date,
  ip_add,
  rid
  ON er_formquerystatus
  FOR EACH ROW
DECLARE
  raid NUMBER(10);

   usr VARCHAR2(100);

   old_modby VARCHAR2(100);

   new_modby VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_FORMQUERYSTATUS', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_formquerystatus,0) !=
     NVL(:NEW.pk_formquerystatus,0) THEN
     audit_trail.column_update
       (raid, 'PK_FORMQUERYSTATUS',
       :OLD.pk_formquerystatus, :NEW.pk_formquerystatus);
  END IF;
  IF NVL(:OLD.fk_formquery,0) !=
     NVL(:NEW.fk_formquery,0) THEN
     audit_trail.column_update
       (raid, 'FK_FORMQUERY',
       :OLD.fk_formquery, :NEW.fk_formquery);
  END IF;
  IF NVL(:OLD.formquery_type,0) !=
     NVL(:NEW.formquery_type,0) THEN
     audit_trail.column_update
       (raid, 'FORMQUERY_TYPE',
       :OLD.formquery_type, :NEW.formquery_type);
  END IF;
   IF NVL(:OLD.fk_codelst_querytype,0) !=
     NVL(:NEW.fk_codelst_querytype,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_QUERYTYPE',
       :OLD.fk_codelst_querytype, :NEW.fk_codelst_querytype);
  END IF;
  IF NVL(:OLD.query_notes,'') !=
     NVL(:NEW.query_notes,0) THEN
     audit_trail.column_update
       (raid, 'QUERY_NOTES',
       :OLD.query_notes, :NEW.query_notes);
  END IF;
  IF NVL(:OLD.ENTERED_BY,0) !=
     NVL(:NEW.ENTERED_BY,0) THEN
     audit_trail.column_update
       (raid, 'ENTERED_BY',
       :OLD.ENTERED_BY, :NEW.ENTERED_BY);
  END IF;
  IF NVL(:old.ENTERED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ENTERED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'ENTERED_ON',
       to_char(:old.ENTERED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ENTERED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.FK_CODELST_QUERYSTATUS,0) !=
     NVL(:NEW.FK_CODELST_QUERYSTATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_QUERYSTATUS',
       :OLD.FK_CODELST_QUERYSTATUS, :NEW.FK_CODELST_QUERYSTATUS);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
 IF NVL(:OLD.LAST_MODIFIED_BY,0) !=
 NVL(:NEW.LAST_MODIFIED_BY,0) THEN
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.last_modified_by ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			old_modby := NULL;
	END ;
	BEGIN
		SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			new_modby := NULL;
	 END ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 END IF;
 IF NVL(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
 END IF;
END;
/
