CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_AU0" AFTER UPDATE ON ESCH.SCH_EVENTS1 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTS1', :OLD.rid, 'U', usr );

   IF NVL(:OLD.event_id,' ') !=
      NVL(:NEW.event_id,' ') THEN
      audit_trail.column_update
        (raid, 'EVENT_ID',
        :OLD.event_id, :NEW.event_id);
   END IF;
   IF NVL(:OLD.object_id,' ') !=
      NVL(:NEW.object_id,' ') THEN
      audit_trail.column_update
        (raid, 'OBJECT_ID',
        :OLD.object_id, :NEW.object_id);
   END IF;
   IF NVL(:OLD.visit_type_id,' ') !=
      NVL(:NEW.visit_type_id,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE_ID',
        :OLD.visit_type_id, :NEW.visit_type_id);
   END IF;
   IF NVL(:OLD.child_service_id,' ') !=
      NVL(:NEW.child_service_id,' ') THEN
      audit_trail.column_update
        (raid, 'CHILD_SERVICE_ID',
        :OLD.child_service_id, :NEW.child_service_id);
   END IF;
   IF NVL(:OLD.session_id,' ') !=
      NVL(:NEW.session_id,' ') THEN
      audit_trail.column_update
        (raid, 'SESSION_ID',
        :OLD.session_id, :NEW.session_id);
   END IF;
   IF NVL(:OLD.occurence_id,' ') !=
      NVL(:NEW.occurence_id,' ') THEN
      audit_trail.column_update
        (raid, 'OCCURENCE_ID',
        :OLD.occurence_id, :NEW.occurence_id);
   END IF;
   IF NVL(:OLD.location_id,' ') !=
      NVL(:NEW.location_id,' ') THEN
      audit_trail.column_update
        (raid, 'LOCATION_ID',
        :OLD.location_id, :NEW.location_id);
   END IF;
   IF NVL(:OLD.notes,' ') !=
      NVL(:NEW.notes,' ') THEN
      audit_trail.column_update
        (raid, 'NOTES',
        :OLD.notes, :NEW.notes);
   END IF;
   IF NVL(:OLD.type_id,' ') !=
      NVL(:NEW.type_id,' ') THEN
      audit_trail.column_update
        (raid, 'TYPE_ID',
        :OLD.type_id, :NEW.type_id);
   END IF;
   IF NVL(:OLD.ROLES,0) !=
      NVL(:NEW.ROLES,0) THEN
      audit_trail.column_update
        (raid, 'ROLES',
        :OLD.ROLES, :NEW.ROLES);
   END IF;
   IF NVL(:OLD.svc_type_id,' ') !=
      NVL(:NEW.svc_type_id,' ') THEN
      audit_trail.column_update
        (raid, 'SVC_TYPE_ID',
        :OLD.svc_type_id, :NEW.svc_type_id);
   END IF;
   IF NVL(:OLD.status,0) !=
      NVL(:NEW.status,0) THEN
      audit_trail.column_update
        (raid, 'STATUS',
        :OLD.status, :NEW.status);
   END IF;
   IF NVL(:OLD.service_id,' ') !=
      NVL(:NEW.service_id,' ') THEN
      audit_trail.column_update
        (raid, 'SERVICE_ID',
        :OLD.service_id, :NEW.service_id);
   END IF;
   IF NVL(:OLD.booked_by,' ') !=
      NVL(:NEW.booked_by,' ') THEN
      audit_trail.column_update
        (raid, 'BOOKED_BY',
        :OLD.booked_by, :NEW.booked_by);
   END IF;
   IF NVL(:OLD.isconfirmed,0) !=
      NVL(:NEW.isconfirmed,0) THEN
      audit_trail.column_update
        (raid, 'ISCONFIRMED',
        :OLD.isconfirmed, :NEW.isconfirmed);
   END IF;
   IF NVL(:OLD.description,' ') !=
      NVL(:NEW.description,' ') THEN
      audit_trail.column_update
        (raid, 'DESCRIPTION',
        :OLD.description, :NEW.description);
   END IF;
   IF NVL(:OLD.attended,0) !=
      NVL(:NEW.attended,0) THEN
      audit_trail.column_update
        (raid, 'ATTENDED',
        :OLD.attended, :NEW.attended);
   END IF;
   IF NVL(:OLD.start_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.start_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'START_DATE_TIME',
        to_char(:OLD.start_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.start_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.end_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.end_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'END_DATE_TIME',
        to_char(:OLD.end_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.end_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.bookedon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.bookedon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'BOOKEDON',
        to_char(:OLD.bookedon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.bookedon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.iswaitlisted,0) !=
      NVL(:NEW.iswaitlisted,0) THEN
      audit_trail.column_update
        (raid, 'ISWAITLISTED',
        :OLD.iswaitlisted, :NEW.iswaitlisted);
   END IF;
   IF NVL(:OLD.object_name,' ') !=
      NVL(:NEW.object_name,' ') THEN
      audit_trail.column_update
        (raid, 'OBJECT_NAME',
        :OLD.object_name, :NEW.object_name);
   END IF;
   IF NVL(:OLD.location_name,' ') !=
      NVL(:NEW.location_name,' ') THEN
      audit_trail.column_update
        (raid, 'LOCATION_NAME',
        :OLD.location_name, :NEW.location_name);
   END IF;
   IF NVL(:OLD.session_name,' ') !=
      NVL(:NEW.session_name,' ') THEN
      audit_trail.column_update
        (raid, 'SESSION_NAME',
        :OLD.session_name, :NEW.session_name);
   END IF;
   IF NVL(:OLD.user_name,' ') !=
      NVL(:NEW.user_name,' ') THEN
      audit_trail.column_update
        (raid, 'USER_NAME',
        :OLD.user_name, :NEW.user_name);
   END IF;
   IF NVL(:OLD.patient_id,' ') !=
      NVL(:NEW.patient_id,' ') THEN
      audit_trail.column_update
        (raid, 'PATIENT_ID',
        :OLD.patient_id, :NEW.patient_id);
   END IF;
   IF NVL(:OLD.svc_type_name,' ') !=
      NVL(:NEW.svc_type_name,' ') THEN
      audit_trail.column_update
        (raid, 'SVC_TYPE_NAME',
        :OLD.svc_type_name, :NEW.svc_type_name);
   END IF;
   IF NVL(:OLD.event_num,0) !=
      NVL(:NEW.event_num,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_NUM',
        :OLD.event_num, :NEW.event_num);
   END IF;
   IF NVL(:OLD.visit_type_name,' ') !=
      NVL(:NEW.visit_type_name,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE_NAME',
        :OLD.visit_type_name, :NEW.visit_type_name);
   END IF;
   IF NVL(:OLD.obj_location_id,' ') !=
      NVL(:NEW.obj_location_id,' ') THEN
      audit_trail.column_update
        (raid, 'OBJ_LOCATION_ID',
        :OLD.obj_location_id, :NEW.obj_location_id);
   END IF;
   IF NVL(:OLD.fk_assoc,0) !=
      NVL(:NEW.fk_assoc,0) THEN
      audit_trail.column_update
        (raid, 'FK_ASSOC',
        :OLD.fk_assoc, :NEW.fk_assoc);
   END IF;
   IF NVL(:OLD.event_exeon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.event_exeon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'EVENT_EXEON',
        to_char(:OLD.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.event_exeby,0) !=
      NVL(:NEW.event_exeby,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_EXEBY',
        :OLD.event_exeby, :NEW.event_exeby);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.actual_schdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.actual_schdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'ACTUAL_SCHDATE',
        to_char(:OLD.actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.adverse_count,0) !=
      NVL(:NEW.adverse_count,0) THEN
      audit_trail.column_update
        (raid, 'ADVERSE_COUNT',
        :OLD.adverse_count, :NEW.adverse_count);
   END IF;
   IF NVL(:OLD.visit,0) !=
      NVL(:NEW.visit,0) THEN
      audit_trail.column_update
        (raid, 'VISIT',
        :OLD.visit, :NEW.visit);
   END IF;

   IF NVL(:OLD.alnot_sent,0) !=
      NVL(:NEW.alnot_sent,0) THEN
      audit_trail.column_update
        (raid, 'ALNOT_SENT',
        :OLD.alnot_sent, :NEW.alnot_sent);
   END IF;

   IF NVL(:OLD.fk_visit,0) !=
      NVL(:NEW.fk_visit,0) THEN
      audit_trail.column_update
        (raid, 'FK_VISIT',
        :OLD.fk_visit, :NEW.fk_visit);
   END IF;

  --JM: 02/16/2007
  IF NVL(:OLD.fk_study,0) !=
      NVL(:NEW.fk_study,0) THEN
      audit_trail.column_update
        (raid, 'FK_STUDY',
        :OLD.fk_study, :NEW.fk_study);
   END IF;

   IF NVL(:OLD.event_notes,0) !=
      NVL(:NEW.event_notes,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_NOTES',
        :OLD.event_notes, :NEW.event_notes);
   END IF;

   IF NVL(:OLD.EVENT_SEQUENCE,0) !=
      NVL(:NEW.EVENT_SEQUENCE,0) THEN
      audit_trail.column_update
	(raid, 'EVENT_SEQUENCE',:OLD.EVENT_SEQUENCE, :NEW.EVENT_SEQUENCE);
   END IF;

   IF NVL(:OLD.SERVICE_SITE_ID,0) !=
      NVL(:NEW.SERVICE_SITE_ID,0) THEN
      audit_trail.column_update
	(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
   END IF;

   IF NVL(:OLD.FACILITY_ID,0) !=
      NVL(:NEW.FACILITY_ID,0) THEN
      audit_trail.column_update
	(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
   END IF;

   IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
      NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
      audit_trail.column_update
	(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
   END IF;


   IF NVL(:OLD.REASON_FOR_COVERAGECHANGE,0) !=
      NVL(:NEW.REASON_FOR_COVERAGECHANGE,0) THEN
      audit_trail.column_update
	(raid, 'REASON_FOR_COVERAGECHANGE',:OLD.REASON_FOR_COVERAGECHANGE, :NEW.REASON_FOR_COVERAGECHANGE);
   END IF;


END; 
/

