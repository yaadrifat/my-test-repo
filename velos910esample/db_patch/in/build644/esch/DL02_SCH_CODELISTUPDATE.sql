set define off;
declare 

qCount Number;

begin

select count(*) into qCount from SCH_CODELST where trim(CODELST_TYPE)='sch_alert' and trim(CODELST_SUBTYP)='al_death';

if qCount=1 then
  Update SCH_CODELST set CODELST_DESC ='Send alert if death is reported as Adverse Event outcome' where trim(CODELST_TYPE)='sch_alert' and trim(CODELST_SUBTYP)='al_death';
  commit;
end if;

end;
