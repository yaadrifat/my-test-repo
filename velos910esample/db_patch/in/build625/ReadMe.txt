/* This readMe is specific to Velos eResearch version 9.0 build #625 */

=====================================================================================================================================
Garuda :
Updating the visibility of one menu item specific to Garuda. This menu item release visible for all. 
As per the norms, this should not be visible/application to other eResearch deployments. 
THE PATCH DL08_ER_OBJECT_SETTING_UPDATE SHOULD ONLY BE RUN IN ERES ENVIRONMENTS.
	   
=====================================================================================================================================
eResearch:

INF-22311: Sheet1 of this enhancement is completely released in this build.Please find the updated .xls file 
released with this build. The .xls file contains 3 sheets and as per the communication done with velos only 
two sheets i.e Sheet1 and Sheet2 are needed to be released in this version9.0. There are 16 places in sheet1
which are not done and marked as "Not Done" in Bold in the .xls file. As per the communication with Sonia and
Sammie those 16 places should be considered as Future enhancement. Please find below the forum link referring 
to communication with Sammie on those 16 places which are not done:
http://66.237.42.91:8080/viewtopic.php?p=8247#8247

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. calendarAuditReport.jsp
2. budgetAuditReport.jsp
3. labelBundle.properties
4. LC.java
5. MC.java
6. messageBundle.properties

Few XSL based report has been corrected. The related script (BT01_data.sql) is Specific to this.
We need to execute the following in the sequence: 
1. BT01_data.sql
2. loadxsl.bat
=====================================================================================================================================
