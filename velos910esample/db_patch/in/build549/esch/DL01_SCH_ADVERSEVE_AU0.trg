create or replace
TRIGGER ESCH.SCH_ADVERSEVE_AU0 AFTER UPDATE OF RID,FK_PER,IP_ADD,AE_DESC,CREATOR,AE_NOTES,FK_STUDY,AE_STDATE,PK_ADVEVE,AE_ADDINFO,AE_ENDDATE,AE_ENTERBY,AE_OUTDATE,AE_OUTTYPE,CREATED_ON,FK_EVENTS1,AE_OUTNOTES,AE_SEVERITY,AE_LOGGEDDATE,AE_REPORTEDBY,AE_DISCVRYDATE,AE_BDSYSTEM_AFF,AE_RELATIONSHIP,AE_RECOVERY_DESC,FK_CODLST_AETYPE,FK_LINK_ADVERSEVE ON ESCH.SCH_ADVERSEVE FOR EACH ROW
declare
  raid number(10);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVERSEVE', :old.rid, 'U', :new.LAST_MODIFIED_BY);

  if nvl(:old.pk_adveve,0) !=
     NVL(:new.pk_adveve,0) then
     audit_trail.column_update
       (raid, 'PK_ADVEVE',
       :old.pk_adveve, :new.pk_adveve);
  end if;
  if nvl(:old.fk_codlst_aetype,0) !=
     NVL(:new.fk_codlst_aetype,0) then
     audit_trail.column_update
       (raid, 'FK_CODLST_AETYPE',
       :old.fk_codlst_aetype, :new.fk_codlst_aetype);
  end if;
  if nvl(:old.ae_desc,' ') !=
     NVL(:new.ae_desc,' ') then
     audit_trail.column_update
       (raid, 'AE_DESC',
       :old.ae_desc, :new.ae_desc);
  end if;
  if nvl(:old.ae_stdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ae_stdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'AE_STDATE',
       to_char(:old.ae_stdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_stdate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_enddate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ae_enddate,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'AE_ENDDATE',
       to_char(:old.ae_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_enterby,0) !=
     NVL(:new.ae_enterby,0) then
     audit_trail.column_update
       (raid, 'AE_ENTERBY',
       :old.ae_enterby, :new.ae_enterby);
  end if;
   if nvl(:old.ae_reportedby,0) !=
     NVL(:new.ae_reportedby,0) then
     audit_trail.column_update
       (raid, 'AE_REPORTEDBY',
       :old.ae_reportedby, :new.ae_reportedby);
  end if;
  if nvl(:old.ae_outtype,' ') !=
     NVL(:new.ae_outtype,' ') then
     audit_trail.column_update
       (raid, 'AE_OUTTYPE',
       :old.ae_outtype, :new.ae_outtype);
  end if;

  if nvl(:old.fk_link_adverseve,0) !=
     NVL(:new.fk_link_adverseve,0) then
     audit_trail.column_update
       (raid,'FK_LINK_ADVERSEVE',
       :old.fk_link_adverseve, :new.fk_link_adverseve);
  end if;

  if nvl(:old.ae_outdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ae_outdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'AE_OUTDATE',
       to_char(:old.ae_outdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_outdate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_discvrydate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ae_discvrydate,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'AE_DISCVRY',
       to_char(:old.ae_discvrydate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_discvrydate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_loggeddate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ae_loggeddate,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'AE_LOGGEDDATE',
       to_char(:old.ae_loggeddate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_loggeddate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ae_outnotes,' ') !=
     NVL(:new.ae_outnotes,' ') then
     audit_trail.column_update
       (raid, 'AE_OUTNOTES',
       :old.ae_outnotes, :new.ae_outnotes);
  end if;
  if nvl(:old.ae_addinfo,' ') !=
     NVL(:new.ae_addinfo,' ') then
     audit_trail.column_update
       (raid, 'AE_ADDINFO',
       :old.ae_addinfo, :new.ae_addinfo);
  end if;
  if nvl(:old.ae_notes,' ') !=
     NVL(:new.ae_notes,' ') then
     audit_trail.column_update
       (raid, 'AE_NOTES',
       :old.ae_notes, :new.ae_notes);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
  end if;
  if nvl(:old.fk_per,0) !=
     NVL(:new.fk_per,0) then
     audit_trail.column_update
       (raid, 'FK_PER',
       :old.fk_per, :new.fk_per);
  end if;
  if nvl(:old.ae_severity,0) !=
     NVL(:new.ae_severity,0) then
     audit_trail.column_update
       (raid, 'AE_SEVERITY',
       :old.ae_severity, :new.ae_severity);
  end if;
  if nvl(:old.ae_bdsystem_aff,0) !=
     NVL(:new.ae_bdsystem_aff,0) then
     audit_trail.column_update
       (raid, 'AE_BDSYSTEM_AFF',
       :old.ae_bdsystem_aff, :new.ae_bdsystem_aff);
  end if;
  if nvl(:old.ae_relationship,0) !=
     NVL(:new.ae_relationship,0) then
     audit_trail.column_update
       (raid, 'AE_RELATIONSHIP',
       :old.ae_relationship, :new.ae_relationship);
  end if;
  if nvl(:old.ae_recovery_desc,0) !=
     NVL(:new.ae_recovery_desc,0) then
     audit_trail.column_update
       (raid, 'AE_RECOVERY_DESC',
       :old.ae_recovery_desc, :new.ae_recovery_desc);
  end if;

end;
