<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
    
    <!-- parameters-->
    
    <xsl:param name="hdrFileName" />
    <xsl:param name="ftrFileName" />
    <xsl:param name="repTitle"/>
    <xsl:param name="repName"/>
    <xsl:param name="repBy"/>
    <xsl:param name="repDate"/>
    <xsl:param name="argsStr"/>
    <xsl:param name="wd"/>
    <xsl:param name="xd"/>
    <xsl:param name="hd"/>
    <xsl:param name="cond"/>
    <xsl:param name="hdrflag"/>
    <xsl:param name="ftrflag"/>
    <xsl:param name="tz"/>
    
    <xsl:template match="/">
        <HTML>
            <HEAD><TITLE><xsl:value-of select="$repTitle" /></TITLE>
                
                <link rel="stylesheet" href="./styles/common.css" type="text/css"/>
                             
            </HEAD>
            <BODY class="repBody">
                <xsl:if test="$cond='T'">
                    <table width="100%" bgcolor="lightgrey" >
                        <tr>
                            <td class="reportPanel"> 
                                VELMESSGE[M_Download_ReportIn]:<!-- Download the report in --> 
                                <A href='{$wd}' >
                                    VELLABEL[L_Word_Format]<!-- Word Format -->
                                </A> 
                                <xsl:text>&#xa0;</xsl:text>
                                <xsl:text>&#xa0;</xsl:text>
                                <xsl:if test="$cond='T'">           <xsl:text>&#xa0;</xsl:text> </xsl:if>
                                    <xsl:text>&#xa0;</xsl:text>
                                    <xsl:text>&#xa0;</xsl:text>
                                    <A href='{$xd}' >
                                  VELLABEL[L_Excel_Format]<!-- Excel Format -->
                                    </A>
                                    <xsl:text>&#xa0;</xsl:text>
                                    <xsl:text>&#xa0;</xsl:text>
                                    <xsl:text>&#xa0;</xsl:text>
                                    <xsl:text>&#xa0;</xsl:text>
                                    <xsl:text>&#xa0;</xsl:text>
                                    <A href='{$hd}' >
                                     VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
                                    </A> 
                            </td>                   
                        </tr>
                    </table>
                </xsl:if>
                <xsl:apply-templates select="user"/>
            </BODY>
        </HTML>
    </xsl:template>
    
    <xsl:template match="user">
        <TABLE WIDTH="100%">
            <xsl:if test="$hdrflag='1'">
                <TR>
                    <TD WIDTH="100%" ALIGN="CENTER">
                        <img src="{$hdrFileName}"/>
                    </TD>
                </TR>
            </xsl:if>
            <TR>
                <TD class="reportName" WIDTH="100%" ALIGN="CENTER">
                    <xsl:value-of select="$repName" />
                </TD>
            </TR>
        </TABLE>
        <hr class="thickLine" />
        
        
        <!-- User Information-->
        
        <xsl:text>&#xa;</xsl:text>
        <TABLE WIDTH="100%" >
            <TR class="reportGreyRow">
                <TD class="reportHeading" WIDTH="90%" ALIGN="LEFT" >
                   VELLABEL[L_User_Information]<!--  User Information -->
                </TD>
            </TR>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE >
            <TR>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Name]<!-- Name -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@name"/></TD>
            </TR>
            <TR>
                <TD class="reportHeading" WIDTH="30%">VELMESSGE[M_ClinicalRes_Experience]<!-- Clinical Research Experience (in Years) -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@experience"/></TD>
                <TD class="reportHeading" WIDTH="30%">VELMESSGE[M_Involvement_InResPhases]<!-- Involvement in Research Phases -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@phases"/></TD>
            </TR>
            <TR>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Job_Type]<!-- Job Type -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@jobtype"/></TD>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Speciality]<!-- Speciality --> :</TD>
                <TD class="reportData" ><xsl:value-of select="@special"/></TD>
            </TR>
            <TR>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Street_Address]<!-- Street Address -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@Address"/></TD>
            </TR>
            <TR>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_City]<!-- City -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@city"/></TD>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_State]<!-- State -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@state"/></TD>
            </TR>
            <TR>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Zip_Code]<!-- Zip Code -->:</TD>
                <TD class="reportData" > <xsl:value-of select="@zip"/></TD>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Country]<!-- Country -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@country"/></TD>
            </TR>
            <TR>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Phone_Number]<!-- Phone Number -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@phone"/></TD>
                <TD class="reportHeading" WIDTH="30%">VELLABEL[L_Email]<!-- Email -->:</TD>
                <TD class="reportData" ><xsl:value-of select="@email"/></TD>
            </TR>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE WIDTH="100%" >
            <TR class="reportGreyRow">
                <TD class="reportHeading" WIDTH="100%" ALIGN="LEFT" >
                    VELMESSGE[M_UsrBelongs_Grp]<!-- Groups To Which User Belongs -->
                </TD>
            </TR>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE border = "1" >
            <TR><TH class="reportHeading">VELLABEL[L_Group_Name]<!-- Group Name --></TH><TH class="reportHeading">VELLABEL[L_Default_Group]<!-- Default Group -->?</TH></TR>
                <xsl:for-each select="//group">
                    <TR>
                        <TD class="reportData" ><xsl:value-of select="@name"/></TD>
                        <TD class="reportData" ><xsl:value-of select="@default"/></TD>
                    </TR>
                </xsl:for-each>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE WIDTH="100%" >
            <TR class="reportGreyRow">
                <TD class="reportHeading" WIDTH="100%" ALIGN="LEFT" >
                    VELMESSGE[M_UsrSiteAces_AsUsrDetPage]<!-- User site access as per the User Details page -->
                </TD>
            </TR>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE border = "1" >
            <TR><TH class="reportHeading">VELLABEL[L_Site_Name]<!-- Site Name --></TH><TH class="reportHeading">VELLABEL[L_Primary_Site]<!-- Primary Site -->?</TH></TR>
            <xsl:for-each select="//site">
                <TR>
                    <TD class="reportData" ><xsl:value-of select="@name"/></TD>
                    <TD class="reportData" ><xsl:value-of select="@primary"/></TD>
                </TR>
            </xsl:for-each>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE WIDTH="100%" >
            <TR class="reportGreyRow">
                <TD class="reportHeading" WIDTH="100%" ALIGN="LEFT" >
                    VELMESSGE[M_StdWhich_UsrHasAces]<!-- Study To Which User has Access -->
                </TD>
            </TR>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE border = "1" >
            <TR><TH class="reportHeading">VELLABEL[L_Study_Number]<!-- Study Number --></TH><TH class="reportHeading">VELLABEL[L_Study_Title]<!-- Study Title --></TH><TH class="reportHeading">VELLABEL[L_Std_TeamRole]<!-- Study Team Role --></TH>
            <TH class="reportHeading">VELMESSGE[M_OrgsAces_InThisStd]<!-- Organizations Access in this Study --></TH></TR>
            <xsl:for-each select="//study">
                <TR>
                    <TD class="reportData"  ><xsl:value-of select="@number"/></TD>
                    <TD class="reportData" ><xsl:value-of select="@title"/></TD>
                    <TD class="reportData" ><xsl:value-of select="@role"/></TD>
                    <TD class="reportData" ><xsl:value-of select="@org"/></TD>
                </TR>
            </xsl:for-each>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE WIDTH="100%" >
            <TR class="reportGreyRow">
                <TD class="reportHeading" WIDTH="100%" ALIGN="LEFT" >
                    VELMESSGE[M_Notific_ScheduleforUser]<!-- Notifications Schedule for User -->
                </TD>
            </TR>
        </TABLE>
        
        <hr class="thinLine" />
        
        <TABLE border = "1" >
            <TR><TH class="reportHeading">VELLABEL[L_Notification_Type]<!-- Notification Type --></TH><TH class="reportHeading">VELLABEL[L_Linked_To]<!-- Linked To --></TH><TH class="reportHeading">VELLABEL[L_Description]<!-- Description --></TH></TR>
            <xsl:for-each select="//notify">
                <TR>
                    <TD class="reportData" ><xsl:value-of select="@type"/></TD>
                    <TD class="reportData" ><xsl:value-of select="@link"/></TD>
                    <TD class="reportData" ><xsl:value-of select="@Description"/></TD>
                </TR>
            </xsl:for-each>
        </TABLE>
        
        <hr class="thickLine" />
        <TABLE WIDTH="100%">
            <TR>
                <TD class="reportFooter" WIDTH="30%" ALIGN="LEFT">
                   VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />
                </TD>
                <TD class="reportFooter" WIDTH="40%"
                    ALIGN="CENTER">
                    VELLABEL[L_Server_Timezone]<!-- Server Timezone -->:
                    <xsl:value-of select="$tz" />
                </TD>
                <TD class="reportFooter" WIDTH="30%" ALIGN="RIGHT">
                    VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />
                </TD>
            </TR>
        </TABLE>
        <xsl:if test="$ftrflag='1'">
            <TABLE>
                <TR>
                    <TD WIDHT="100%" ALIGN="CENTER">
                        <img src="{$ftrFileName}"/>
                    </TD>
                </TR>
            </TABLE>
        </xsl:if>
    </xsl:template>
    
    
    
</xsl:stylesheet>