set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_ENTITY_SAMPLES WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_ENTITY_SAMPLES  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/