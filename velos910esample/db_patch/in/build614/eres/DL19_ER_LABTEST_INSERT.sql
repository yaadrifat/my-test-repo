Set Define off;

 --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total CBU Nucleated Cell Count
     
      Insert into ER_LABGROUP (PK_LABGROUP,GROUP_NAME,GROUP_TYPE) values 
      (SEQ_ER_LABGROUP.nextval,'PrcsGrp','PC');
          
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total CBU Nucleated Cell Count','TCNCC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

  
      commit;
      
      
      
      --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and CBU Nucleated Cell Count Concentration
     
          
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'CBU Nucleated Cell Count Concentration','CBUNCCC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

  
      commit;
      
      
    --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Final Product Volume
        
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Final Product Volume','FNPV',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;
      
    --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total CBU Nucleated Cell Count (unknown if uncorrected)
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total CBU Nucleated Cell Count (unknown if uncorrected)','TCBUUN',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

  
      commit;
      
      
   --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total CBU Nucleated Cell Count (uncorrected)
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total CBU Nucleated Cell Count (uncorrected)','UNCRCT',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

  
      commit;

      
   --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total CD34+ Cell Count
          
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total CD34+ Cell Count','TCDAD',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;

      
   --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and % of CD34+  Viable
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'% of CD34+  Viable','PERCD',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;   
 
   
     
       --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and % of CD34+ Cells in Total Monunuclear Cell Count 
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'% of CD34+ Cells in Total Monunuclear Cell Count','PERTMON',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;   
      
     
      
      
     --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and  % of Mononuclear Cells in Total Nucleated Cell Count
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,' % of Mononuclear Cells in Total Nucleated Cell Count','PERTNUC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;   
 
      
     
     
     --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Total CD3 Cell Count
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Total CD3 Cell Count','TOTCD',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;   
 
 
 
       --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and % CD3 Cells in Total Mononuclear Cell Count
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'% CD3 Cells in Total Mononuclear Cell Count','PERCD3',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;  
 
 
 
      
    --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and CBU nRBC Absolute Number
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'CBU nRBC Absolute Number','CNRBC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;  

     
      
      --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and   CBU nRBC %
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'CBU nRBC %','PERNRBC',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;


	 --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and CFU Count
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'CFU Count','CFUCNT',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);
   
         commit;  

          --Sql for Lab Summary Widget to create the Group in ER_LABGROUP and Viability
     
      Insert into ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC) values 
      (SEQ_ER_LABTEST.nextval,'Viability','VIAB',null,null,null);
   
      Insert into ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP) values
      (SEQ_ER_LABTESTGRP.nextval,SEQ_ER_LABTEST.currval,SEQ_ER_LABGROUP.currval);

      commit;    

      