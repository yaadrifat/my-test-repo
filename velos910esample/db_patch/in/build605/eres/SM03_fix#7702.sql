-- Fix #7702 Chnage the grade column datatype from varchar to number
 
update er_lkpcol set LKPCOL_DATATYPE = 'number' 
where upper(LKPCOL_NAME)='GRADE'	and upper(lkpcol_keyword)='GRADE'
and fk_lkplib in (select pk_lkplib from er_lkplib where LKPTYPE_NAME='dynReports'
and LKPTYPE_DESC='Patient Adverse Events' and LKPTYPE_TYPE='dyn_p');

commit;
