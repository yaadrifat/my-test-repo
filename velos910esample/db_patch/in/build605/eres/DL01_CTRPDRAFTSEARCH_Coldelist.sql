set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDraftSearch' and codelst_subtyp = 'SWD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) 
		values (SEQ_ER_CODELST.nextval,null,'ctrpDraftSearch','SWD','Study Without Drafts','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SWD inserted');
	else 
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SWD already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDraftSearch' and codelst_subtyp = 'SD';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDraftSearch','SD','Study With Drafts','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SD inserted');
	else 
		dbms_output.put_line('Code-list item Type:ctrpDraftSearch Subtype:SD already exists');
	end if;

COMMIT;

end;
/
