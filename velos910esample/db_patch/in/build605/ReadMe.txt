-- Build 605

=========================================================================================================================
eResearch :

While working on UI skin requirement we have restructured the CSS files in ersearch application. Please keep these changes into consideration

1. ie_1024.css earlier this file was in styles root folder as well on individual skin folders. But now we have moved it from individual skin folder to styles root folder and made it common for all the individual skins. 
2. ns_1024.css, ns_gt1024.css and ie_1024.css which exist in styles root folder are contains noncolor properties which is common for all the skins
3. common.css which exisit in individual skin folder is contains color properties which supports IE and Firefox for the specified individual skin.

========================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	addFieldToFormSubmit.jsp
2	additemstudy.jsp
3	budget.jsp
4	budgetapndx.jsp
5	budgetsections.jsp
6	calDoesNotExist.jsp
7	editBudgetCalAttributes.jsp
8	editPersonnelCost.jsp
9	editRepeatLineItems.jsp
10	formfldbrowser.jsp
11	getlookup.jsp
12	horizontalRuleType.jsp
13	importStudyForms.jsp
14	labelBundle.properties
15	LC.java
16	LC.jsp
17	MC.java
18	MC.jsp
19	messageBundle.properties
20	patientbudget.jsp
21	patientbudgetView.jsp
22	prepareSpecimen.jsp
23	reportcentral.jsp
24	specimenbrowser.jsp
25	studybudget.jsp
26	studypatients.jsp
27	studyprotocols.jsp
========================================================================================================================
Garuda :

DB Patch :1. DL13_SP_WORKFLOW.sql,DL14_SP_WORKFLOW_TEMP.sql & DL15_WORKFLOW_JOB.sql - These three are specific to WorkFlow Component.
				Note:Please run the script in the below mentioned order to avoid compile errors
					1)Run the create table script
					2)DL12_Functions.sql
					3)DL13_SP_WORKFLOW.sql
					4)DL14_SP_WORKFLOW_TEMP.sql
					5)DL15_WORKFLOW_JOB.sql
		  2. DL16_Er_Codelst_insert .sql - This SQL contains the  ER_CODELST entries specific to NMDP. 	

Steps to Integrate Workflow:

	 In this build Workflow for CT orders is released. To make workflow run 
	
		1)Get the gems.jar into GarudaMissingJar from CVS and include this jar file in BuildPath.
		2)copy the gems_configuration.properties file.
			This file is available at db_patch/in/build605
			This file has to be copied at server/eresearch/conf folder
			
		Note:In gems_configuration.properties file we need to replace the IP Address at line no 6 where
				PROVIDER_URL=jnp://<IP Address>:1299
			
		3)Add below code in server/eresearch/deploy/eres-ds.xml
		
			<xa-datasource> 
		        <jndi-name>gems</jndi-name> 
		        <track-connection-by-tx>true</track-connection-by-tx> 
		        <isSameRM-override-value>false</isSameRM-override-value> 
		        <xa-datasource-class>oracle.jdbc.xa.client.OracleXADataSource</xa-datasource-class> 
		        <xa-datasource-property name="URL">jdbc:oracle:thin:@<IP ADDRESS>:<PORT>:<SID></xa-datasource-property> 
		        <xa-datasource-property name="User">USERNAME</xa-datasource-property> 
		        <xa-datasource-property name="Password">PASSWORD</xa-datasource-property> 
				<valid-connection-check-class-name>org.jboss.resource.adapter.jdbc.vendor.OracleValidconnectionChecker</valid-connection-check-class-name>
				<min-pool-size>5</min-pool-size>
				<max-pool-size>50</max-pool-size>
				<idle-timeout-minutes>5</idle-timeout-minutes>
				<blocking-timeout-millis>5000</blocking-timeout-millis>
				<query-timeout>300</query-timeout>
		        <exception-sorter-class-name>org.jboss.resource.adapter.jdbc.vendor.OracleXAExceptionFormatter
		            </exception-sorter-class-name> 
		        <no-tx-separate-pools/> 
			</xa-datasource> 
		
		Note:Replace <IP ADDRESS>,<PORT>,<SID>,<USERNAME>,<PASSWORD> values accordingly.
		
To Test Workflow functionality:
		
	To Test the workflow functionality,use the below script to insert records in er_order table.
	
			DECLARE
			  v_orderId number := 0;
			BEGIN
			
			  Insert into ER_ORDER (PK_ORDER,ORDER_NUMBER,ORDER_ENTITYID,ORDER_ENTITYTYP,ORDER_DATE,FK_ORDER_STATUS,FK_ORDER_TYPE)
			values (SEQ_ER_ORDER.nextval,'ORD107',<PK_CORD>,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU'),sysdate,(select pk_codelst from er_codelst where codelst_type='order_status' and codelst_subtyp='NEW'),(select pk_codelst from er_codelst where codelst_type='order_type' and codelst_subtyp='CT'));
			
			Select SEQ_ER_ORDER.currval into v_orderId from dual;
			
			
			insert into ER_ORDER_TEMP(ORDER_ID,ORDER_TYPE) values(v_orderId,'CTORDER');
			
			insert into cb_shipment(pk_shipment,fk_order_id) values(seq_cb_shipment.nextval,v_orderId);
			commit;
			
			end;
			/
	Note: Change the <PK_CORD> with existing CB_CORD.PK_CORD value.
		
	
	
		
		MENU: In this build two new menus has been released.
			1.References
			2.Contact Info
		  These menus has been enabled for Garuda. 		
		  
		  
	hibernate-validator Note:
	
		Please keep only hibernate-validator.jar in lib directory. Remove hibernate-validator-3.1.0.GA.jar file.

=========================================================================================================================