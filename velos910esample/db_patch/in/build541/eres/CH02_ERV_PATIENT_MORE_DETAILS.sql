CREATE OR REPLACE FORCE VIEW ERV_PATIENT_MORE_DETAILS
(
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTMPD_PAT_ID,
   PTMPD_PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTMPD_RESPONSE_ID
)
AS
   SELECT   (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_idtype)
               AS field_name,
            perid_id AS field_value,
            fk_per,
	    to_date(pkg_util.date_to_char_with_time (a.CREATED_ON) , PKG_DATEUTIL.f_get_datetimeformat) CREATED_ON,
            fk_account,
            PER_CODE PTMPD_PAT_ID,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PER = PK_PERSON)
               PTMPD_PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
	    to_date(pkg_util.date_to_char_with_time (a.LAST_MODIFIED_DATE) , PKG_DATEUTIL.f_get_datetimeformat) LAST_MODIFIED_DATE,
            A.RID,
            PK_PERID PTMPD_RESPONSE_ID
     FROM   pat_perid a, ER_PER
    WHERE   pk_per = fk_per;