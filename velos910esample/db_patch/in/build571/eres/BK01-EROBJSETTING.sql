DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = '12' and 
  OBJECT_NAME = 'study_tab';
  if (row_count = 1) then 
  
	Update ER_OBJECT_SETTINGS Set OBJECT_VISIBLE = 1 where OBJECT_NAME = 'study_tab' and OBJECT_SEQUENCE = 6;   
  end if;	
  
   
END;
/

commit;