#Build 608

=====================================================================================================================================
Garuda :

	For Testing the Work flow functionality we are providing "Orders_Insert_script.sql" file with build which contans script to insert CT,HE and OR Orders.
Before executing the script we need to change the values for "ORDER_NUMBER" and "ORDER_ENTITYID" fields.
	For ORDER_NUMER we can give values like "ORD001" and 
	For ORDER_ENTITYID we need to provide the existing CB_CORD.PK_CORD value.

IMPORTANT NOTE:
================
This build consists of two insert patches specific to NMDP. These patches have been made with the source data provided by NMDP.

1. DL09_CB_ANTIGEN_ENCODE_INSERT.SQL 
2. DL10_CB_ANTIGEN_INSERT.SQL

This build consists of  new menu(Maintenance) and some updates  in menu item specific to Garuda.
1.DL13_ER_OBJECT_SETTINGS_INSERT.sql 
This script creates New Top menu and sub-menus of Maintenance menu:- 
i)Center Management(It itself contains two sub-menus as follows:-CBB Profile and Processing Procedure).
ii)My Profile 
visibility of these menu is by default false. To view these menu we need to set the visibility as true.

Updates in Menu(specific to Garuda):-

1.DL14_ER_OBJECT_SETTINGS_ALTER.sql
This script is meant to rename two menu items Manage CBB,CBB Processing Procedure of Product Fulfillment to CBB Profile,Processing Procedures respectively.

2.DL12_ER_OBJECT_SETTINGS_DELETE.SQL
This script is meant to delete menu item of Product Fulfillment menu. 
	
=====================================================================================================================================
eResearch:
IMPORTANT NOTE:
===============
Upon the request from Garuda team we have added one Column GUID in ER_SITE table.All Audit triggers related to this table are modified accordingly.

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	labelBundle.properties
2	LC.java
3	LC.jsp
4	MC.java
5	MC.jsp
6	messageBundle.properties
7.  patientsearchworkflow.jsp
=====================================================================================================================================