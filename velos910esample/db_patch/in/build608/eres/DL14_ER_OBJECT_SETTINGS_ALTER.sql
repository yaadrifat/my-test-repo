Set define off;

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_OBJECT_SETTINGS'
    AND column_name = 'OBJECT_SUBTYPE';
  if (v_column_value_update =1) then
   UPDATE ER_OBJECT_SETTINGS SET OBJECT_DISPLAYTEXT ='Landing Page', OBJECT_NAME='pf_menu', OBJECT_SEQUENCE='1' 
   WHERE OBJECT_NAME='pf_menu_ext' AND OBJECT_TYPE='M' AND OBJECT_SUBTYPE='opn_task';
  end if;
end;
/


Set define off;

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_OBJECT_SETTINGS'
    AND column_name = 'OBJECT_SUBTYPE';
  if (v_column_value_update =1) then
  
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_DISPLAYTEXT ='Pending Requests', OBJECT_NAME='pf_menu', OBJECT_SEQUENCE='2' 
  WHERE OBJECT_NAME='pf_menu_ext' AND OBJECT_TYPE='M' AND OBJECT_SUBTYPE='pending_ordr';
  end if;
end;
/

commit;


