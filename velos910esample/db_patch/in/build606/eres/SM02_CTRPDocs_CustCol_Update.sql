update er_codelst set CODELST_CUSTOM_COL = 'industrial'
where CODELST_TYPE='ctrpDocType' 
and CODELST_SUBTYP in ('other', 'abbrTrialTempl');

update er_codelst set CODELST_CUSTOM_COL = 'nonIndustrial'
where CODELST_TYPE='ctrpDocType' 
and CODELST_SUBTYP not in ('other', 'abbrTrialTempl');

commit;