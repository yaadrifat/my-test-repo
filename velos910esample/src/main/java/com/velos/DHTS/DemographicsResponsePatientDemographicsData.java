/**
 * DemographicsResponsePatientDemographicsData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class DemographicsResponsePatientDemographicsData  implements java.io.Serializable {
    private java.lang.String MRN;

    private java.lang.String HCFACILITY;

    private java.lang.String FAMILYNAME;

    private java.lang.String GIVENNAME;

    private java.lang.String RESTOFNAME;

    private java.lang.String DATEOFBIRTH;

    private java.lang.String SEX;

    private java.lang.String SSN;

    private java.lang.String BIRTHCITY;

    private java.lang.String BIRTHSTATE;

    private java.lang.String NATIONALITY;

    private java.lang.String RACE;

    private java.lang.String DEATHINDICATOR;

    private java.lang.String BLOODTYPE;

    private java.lang.Integer PERSONID;

    public DemographicsResponsePatientDemographicsData() {
    }

    public DemographicsResponsePatientDemographicsData(
           java.lang.String MRN,
           java.lang.String HCFACILITY,
           java.lang.String FAMILYNAME,
           java.lang.String GIVENNAME,
           java.lang.String RESTOFNAME,
           java.lang.String DATEOFBIRTH,
           java.lang.String SEX,
           java.lang.String SSN,
           java.lang.String BIRTHCITY,
           java.lang.String BIRTHSTATE,
           java.lang.String NATIONALITY,
           java.lang.String RACE,
           java.lang.String DEATHINDICATOR,
           java.lang.String BLOODTYPE,
           java.lang.Integer PERSONID) {
           this.MRN = MRN;
           this.HCFACILITY = HCFACILITY;
           this.FAMILYNAME = FAMILYNAME;
           this.GIVENNAME = GIVENNAME;
           this.RESTOFNAME = RESTOFNAME;
           this.DATEOFBIRTH = DATEOFBIRTH;
           this.SEX = SEX;
           this.SSN = SSN;
           this.BIRTHCITY = BIRTHCITY;
           this.BIRTHSTATE = BIRTHSTATE;
           this.NATIONALITY = NATIONALITY;
           this.RACE = RACE;
           this.DEATHINDICATOR = DEATHINDICATOR;
           this.BLOODTYPE = BLOODTYPE;
           this.PERSONID = PERSONID;
    }


    /**
     * Gets the MRN value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return MRN
     */
    public java.lang.String getMRN() {
        return MRN;
    }


    /**
     * Sets the MRN value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param MRN
     */
    public void setMRN(java.lang.String MRN) {
        this.MRN = MRN;
    }


    /**
     * Gets the HCFACILITY value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return HCFACILITY
     */
    public java.lang.String getHCFACILITY() {
        return HCFACILITY;
    }


    /**
     * Sets the HCFACILITY value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param HCFACILITY
     */
    public void setHCFACILITY(java.lang.String HCFACILITY) {
        this.HCFACILITY = HCFACILITY;
    }


    /**
     * Gets the FAMILYNAME value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return FAMILYNAME
     */
    public java.lang.String getFAMILYNAME() {
        return FAMILYNAME;
    }


    /**
     * Sets the FAMILYNAME value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param FAMILYNAME
     */
    public void setFAMILYNAME(java.lang.String FAMILYNAME) {
        this.FAMILYNAME = FAMILYNAME;
    }


    /**
     * Gets the GIVENNAME value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return GIVENNAME
     */
    public java.lang.String getGIVENNAME() {
        return GIVENNAME;
    }


    /**
     * Sets the GIVENNAME value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param GIVENNAME
     */
    public void setGIVENNAME(java.lang.String GIVENNAME) {
        this.GIVENNAME = GIVENNAME;
    }


    /**
     * Gets the RESTOFNAME value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return RESTOFNAME
     */
    public java.lang.String getRESTOFNAME() {
        return RESTOFNAME;
    }


    /**
     * Sets the RESTOFNAME value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param RESTOFNAME
     */
    public void setRESTOFNAME(java.lang.String RESTOFNAME) {
        this.RESTOFNAME = RESTOFNAME;
    }


    /**
     * Gets the DATEOFBIRTH value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return DATEOFBIRTH
     */
    public java.lang.String getDATEOFBIRTH() {
        return DATEOFBIRTH;
    }


    /**
     * Sets the DATEOFBIRTH value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param DATEOFBIRTH
     */
    public void setDATEOFBIRTH(java.lang.String DATEOFBIRTH) {
        this.DATEOFBIRTH = DATEOFBIRTH;
    }


    /**
     * Gets the SEX value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return SEX
     */
    public java.lang.String getSEX() {
        return SEX;
    }


    /**
     * Sets the SEX value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param SEX
     */
    public void setSEX(java.lang.String SEX) {
        this.SEX = SEX;
    }


    /**
     * Gets the SSN value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return SSN
     */
    public java.lang.String getSSN() {
        return SSN;
    }


    /**
     * Sets the SSN value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param SSN
     */
    public void setSSN(java.lang.String SSN) {
        this.SSN = SSN;
    }


    /**
     * Gets the BIRTHCITY value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return BIRTHCITY
     */
    public java.lang.String getBIRTHCITY() {
        return BIRTHCITY;
    }


    /**
     * Sets the BIRTHCITY value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param BIRTHCITY
     */
    public void setBIRTHCITY(java.lang.String BIRTHCITY) {
        this.BIRTHCITY = BIRTHCITY;
    }


    /**
     * Gets the BIRTHSTATE value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return BIRTHSTATE
     */
    public java.lang.String getBIRTHSTATE() {
        return BIRTHSTATE;
    }


    /**
     * Sets the BIRTHSTATE value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param BIRTHSTATE
     */
    public void setBIRTHSTATE(java.lang.String BIRTHSTATE) {
        this.BIRTHSTATE = BIRTHSTATE;
    }


    /**
     * Gets the NATIONALITY value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return NATIONALITY
     */
    public java.lang.String getNATIONALITY() {
        return NATIONALITY;
    }


    /**
     * Sets the NATIONALITY value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param NATIONALITY
     */
    public void setNATIONALITY(java.lang.String NATIONALITY) {
        this.NATIONALITY = NATIONALITY;
    }


    /**
     * Gets the RACE value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return RACE
     */
    public java.lang.String getRACE() {
        return RACE;
    }


    /**
     * Sets the RACE value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param RACE
     */
    public void setRACE(java.lang.String RACE) {
        this.RACE = RACE;
    }


    /**
     * Gets the DEATHINDICATOR value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return DEATHINDICATOR
     */
    public java.lang.String getDEATHINDICATOR() {
        return DEATHINDICATOR;
    }


    /**
     * Sets the DEATHINDICATOR value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param DEATHINDICATOR
     */
    public void setDEATHINDICATOR(java.lang.String DEATHINDICATOR) {
        this.DEATHINDICATOR = DEATHINDICATOR;
    }


    /**
     * Gets the BLOODTYPE value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return BLOODTYPE
     */
    public java.lang.String getBLOODTYPE() {
        return BLOODTYPE;
    }


    /**
     * Sets the BLOODTYPE value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param BLOODTYPE
     */
    public void setBLOODTYPE(java.lang.String BLOODTYPE) {
        this.BLOODTYPE = BLOODTYPE;
    }


    /**
     * Gets the PERSONID value for this DemographicsResponsePatientDemographicsData.
     * 
     * @return PERSONID
     */
    public java.lang.Integer getPERSONID() {
        return PERSONID;
    }


    /**
     * Sets the PERSONID value for this DemographicsResponsePatientDemographicsData.
     * 
     * @param PERSONID
     */
    public void setPERSONID(java.lang.Integer PERSONID) {
        this.PERSONID = PERSONID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DemographicsResponsePatientDemographicsData)) return false;
        DemographicsResponsePatientDemographicsData other = (DemographicsResponsePatientDemographicsData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MRN==null && other.getMRN()==null) || 
             (this.MRN!=null &&
              this.MRN.equals(other.getMRN()))) &&
            ((this.HCFACILITY==null && other.getHCFACILITY()==null) || 
             (this.HCFACILITY!=null &&
              this.HCFACILITY.equals(other.getHCFACILITY()))) &&
            ((this.FAMILYNAME==null && other.getFAMILYNAME()==null) || 
             (this.FAMILYNAME!=null &&
              this.FAMILYNAME.equals(other.getFAMILYNAME()))) &&
            ((this.GIVENNAME==null && other.getGIVENNAME()==null) || 
             (this.GIVENNAME!=null &&
              this.GIVENNAME.equals(other.getGIVENNAME()))) &&
            ((this.RESTOFNAME==null && other.getRESTOFNAME()==null) || 
             (this.RESTOFNAME!=null &&
              this.RESTOFNAME.equals(other.getRESTOFNAME()))) &&
            ((this.DATEOFBIRTH==null && other.getDATEOFBIRTH()==null) || 
             (this.DATEOFBIRTH!=null &&
              this.DATEOFBIRTH.equals(other.getDATEOFBIRTH()))) &&
            ((this.SEX==null && other.getSEX()==null) || 
             (this.SEX!=null &&
              this.SEX.equals(other.getSEX()))) &&
            ((this.SSN==null && other.getSSN()==null) || 
             (this.SSN!=null &&
              this.SSN.equals(other.getSSN()))) &&
            ((this.BIRTHCITY==null && other.getBIRTHCITY()==null) || 
             (this.BIRTHCITY!=null &&
              this.BIRTHCITY.equals(other.getBIRTHCITY()))) &&
            ((this.BIRTHSTATE==null && other.getBIRTHSTATE()==null) || 
             (this.BIRTHSTATE!=null &&
              this.BIRTHSTATE.equals(other.getBIRTHSTATE()))) &&
            ((this.NATIONALITY==null && other.getNATIONALITY()==null) || 
             (this.NATIONALITY!=null &&
              this.NATIONALITY.equals(other.getNATIONALITY()))) &&
            ((this.RACE==null && other.getRACE()==null) || 
             (this.RACE!=null &&
              this.RACE.equals(other.getRACE()))) &&
            ((this.DEATHINDICATOR==null && other.getDEATHINDICATOR()==null) || 
             (this.DEATHINDICATOR!=null &&
              this.DEATHINDICATOR.equals(other.getDEATHINDICATOR()))) &&
            ((this.BLOODTYPE==null && other.getBLOODTYPE()==null) || 
             (this.BLOODTYPE!=null &&
              this.BLOODTYPE.equals(other.getBLOODTYPE()))) &&
            ((this.PERSONID==null && other.getPERSONID()==null) || 
             (this.PERSONID!=null &&
              this.PERSONID.equals(other.getPERSONID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMRN() != null) {
            _hashCode += getMRN().hashCode();
        }
        if (getHCFACILITY() != null) {
            _hashCode += getHCFACILITY().hashCode();
        }
        if (getFAMILYNAME() != null) {
            _hashCode += getFAMILYNAME().hashCode();
        }
        if (getGIVENNAME() != null) {
            _hashCode += getGIVENNAME().hashCode();
        }
        if (getRESTOFNAME() != null) {
            _hashCode += getRESTOFNAME().hashCode();
        }
        if (getDATEOFBIRTH() != null) {
            _hashCode += getDATEOFBIRTH().hashCode();
        }
        if (getSEX() != null) {
            _hashCode += getSEX().hashCode();
        }
        if (getSSN() != null) {
            _hashCode += getSSN().hashCode();
        }
        if (getBIRTHCITY() != null) {
            _hashCode += getBIRTHCITY().hashCode();
        }
        if (getBIRTHSTATE() != null) {
            _hashCode += getBIRTHSTATE().hashCode();
        }
        if (getNATIONALITY() != null) {
            _hashCode += getNATIONALITY().hashCode();
        }
        if (getRACE() != null) {
            _hashCode += getRACE().hashCode();
        }
        if (getDEATHINDICATOR() != null) {
            _hashCode += getDEATHINDICATOR().hashCode();
        }
        if (getBLOODTYPE() != null) {
            _hashCode += getBLOODTYPE().hashCode();
        }
        if (getPERSONID() != null) {
            _hashCode += getPERSONID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DemographicsResponsePatientDemographicsData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", ">>DemographicsResponse>PatientDemographics>Data"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MRN");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MRN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HCFACILITY");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HCFACILITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FAMILYNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FAMILYNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GIVENNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GIVENNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RESTOFNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RESTOFNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATEOFBIRTH");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATEOFBIRTH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEX");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SEX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSN");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SSN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BIRTHCITY");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BIRTHCITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BIRTHSTATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BIRTHSTATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NATIONALITY");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NATIONALITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RACE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RACE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DEATHINDICATOR");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DEATHINDICATOR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BLOODTYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BLOODTYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PERSONID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PERSONID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
