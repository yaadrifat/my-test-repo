/**
 * GetPatientInfoEx.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class GetPatientInfoEx  implements java.io.Serializable {
    private java.lang.String userID;

    private java.lang.String password;

    private java.lang.String HCFacilityList;

    private java.lang.String MRNList;

    private int howmanyEncounters;

    public GetPatientInfoEx() {
    }

    public GetPatientInfoEx(
           java.lang.String userID,
           java.lang.String password,
           java.lang.String HCFacilityList,
           java.lang.String MRNList,
           int howmanyEncounters) {
           this.userID = userID;
           this.password = password;
           this.HCFacilityList = HCFacilityList;
           this.MRNList = MRNList;
           this.howmanyEncounters = howmanyEncounters;
    }


    /**
     * Gets the userID value for this GetPatientInfoEx.
     * 
     * @return userID
     */
    public java.lang.String getUserID() {
        return userID;
    }


    /**
     * Sets the userID value for this GetPatientInfoEx.
     * 
     * @param userID
     */
    public void setUserID(java.lang.String userID) {
        this.userID = userID;
    }


    /**
     * Gets the password value for this GetPatientInfoEx.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this GetPatientInfoEx.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the HCFacilityList value for this GetPatientInfoEx.
     * 
     * @return HCFacilityList
     */
    public java.lang.String getHCFacilityList() {
        return HCFacilityList;
    }


    /**
     * Sets the HCFacilityList value for this GetPatientInfoEx.
     * 
     * @param HCFacilityList
     */
    public void setHCFacilityList(java.lang.String HCFacilityList) {
        this.HCFacilityList = HCFacilityList;
    }


    /**
     * Gets the MRNList value for this GetPatientInfoEx.
     * 
     * @return MRNList
     */
    public java.lang.String getMRNList() {
        return MRNList;
    }


    /**
     * Sets the MRNList value for this GetPatientInfoEx.
     * 
     * @param MRNList
     */
    public void setMRNList(java.lang.String MRNList) {
        this.MRNList = MRNList;
    }


    /**
     * Gets the howmanyEncounters value for this GetPatientInfoEx.
     * 
     * @return howmanyEncounters
     */
    public int getHowmanyEncounters() {
        return howmanyEncounters;
    }


    /**
     * Sets the howmanyEncounters value for this GetPatientInfoEx.
     * 
     * @param howmanyEncounters
     */
    public void setHowmanyEncounters(int howmanyEncounters) {
        this.howmanyEncounters = howmanyEncounters;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPatientInfoEx)) return false;
        GetPatientInfoEx other = (GetPatientInfoEx) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userID==null && other.getUserID()==null) || 
             (this.userID!=null &&
              this.userID.equals(other.getUserID()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.HCFacilityList==null && other.getHCFacilityList()==null) || 
             (this.HCFacilityList!=null &&
              this.HCFacilityList.equals(other.getHCFacilityList()))) &&
            ((this.MRNList==null && other.getMRNList()==null) || 
             (this.MRNList!=null &&
              this.MRNList.equals(other.getMRNList()))) &&
            this.howmanyEncounters == other.getHowmanyEncounters();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserID() != null) {
            _hashCode += getUserID().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getHCFacilityList() != null) {
            _hashCode += getHCFacilityList().hashCode();
        }
        if (getMRNList() != null) {
            _hashCode += getMRNList().hashCode();
        }
        _hashCode += getHowmanyEncounters();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPatientInfoEx.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", ">GetPatientInfoEx"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "userID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HCFacilityList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "HCFacilityList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MRNList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "MRNList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("howmanyEncounters");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "howmanyEncounters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
