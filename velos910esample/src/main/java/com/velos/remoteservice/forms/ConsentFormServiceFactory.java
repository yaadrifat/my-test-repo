/**
 * 
 */
package com.velos.remoteservice.forms;

import com.velos.remoteservice.AbstractServiceFactory;

/**
 * @author dylan
 *
 */
public class ConsentFormServiceFactory extends AbstractServiceFactory {
	static private IConsentFormService serviceSingleton = null;
	public static IConsentFormService getService() throws ClassNotFoundException{
		//This sweet code will only work with Java 1.6. For 1.5, we need the
		//Class.newInstance()
		//		final  ServiceLoader<ILabService> labServiceLoader =
		//			ServiceLoader.load(ILabService.class);
		//		
		//		for (ILabService labService : labServiceLoader){
		//			return labService;
		//		}
		//		return null;

		if (serviceSingleton == null){
			Class<IConsentFormService>  consentFormServiceClass = IConsentFormService.class;
			for (IConsentFormService service : load(consentFormServiceClass)){
				serviceSingleton = service;
				break;
			}
		}
		return serviceSingleton;


	}
}
