package com.velos.remoteservice;


public class VelosServiceUnmarshallingException extends VelosServiceException {

	public VelosServiceUnmarshallingException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VelosServiceUnmarshallingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public VelosServiceUnmarshallingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public VelosServiceUnmarshallingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}



}
