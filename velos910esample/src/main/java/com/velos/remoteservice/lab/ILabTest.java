package com.velos.remoteservice.lab;

import java.util.List;
import java.util.Date;


public interface ILabTest {

	public List<ILabTestDetail> getTestDetails();
	
	public String getAccession();
	
	public String getCode(); 
	
	public int getConceptID();
	
	public String getConceptName();
	
	public Date getLastUpdateDate();
	
	public String getTestName();
	
	public Date getOrderDate();
	
	public String getOrderingProvider();
	
	public Date getSpecimenCollectionDate();
	
	public String getStatus();
	
	public int getTestID();
	

	public String getFolder();
	
	public String getMnemonic();
	
	public String getPatientType();
	
	public int getSystemID();
	
}
