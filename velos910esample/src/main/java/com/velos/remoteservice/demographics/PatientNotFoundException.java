/**
 * 
 */
package com.velos.remoteservice.demographics;

import com.velos.remoteservice.VelosServiceException;

/**
 * @author dylan
 *
 */
public class PatientNotFoundException extends Exception {

	public PatientNotFoundException() {
		super();

	}

	public PatientNotFoundException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public PatientNotFoundException(String message) {
		super(message);

	}

	public PatientNotFoundException(Throwable cause) {
		super(cause);
	
	}

}
