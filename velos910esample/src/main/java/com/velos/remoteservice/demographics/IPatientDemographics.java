package com.velos.remoteservice.demographics;

import java.util.Date;
import java.util.Map;

/**
 * Interface for creating a facade between the Velos patient search functions in eResearch,
 * and a custom implementation of a patient service.
 * 
 * @author dylan
 *
 */
public interface IPatientDemographics{

	public Map<String, String> getFacilityIds();
	
	public String getFirstName();
	
	public String getMiddleName();
	
	public String getLastName();
	
	public String getMRN();
	
	public Date getDOB();
		
	public String getSurvivalStatus();
	
	public String getMaritalStatus();
	
	public String getBloodGroup();
	
	public String getSSN();
	
	public String getEmailAddress();
	
	public String getAddress1();
	
	public String getAddress2();
	
	public String getCity();
	
	public String getCounty();
	
	public String getState();
	
	public String getZip();
	
	public String getCountry();
	
	public String getHomePhones();
	
	public String getWorkPhones();
	
	@Deprecated
	public String getEmail();
	
	public String getGender();
		
	public String getPrimaryFacility();
	
	public String getRace();
	
	public String getEthnicity();
	
	public Date getDateOfDeath();
	
	public String getCauseOfDeath();
	
	public String getCauseOfDeathOther();

}
