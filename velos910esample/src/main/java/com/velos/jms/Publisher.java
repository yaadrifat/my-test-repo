package com.velos.jms;

import java.util.Vector;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;

import com.velos.eres.service.util.StringUtil;
import com.velos.impex.Impex;

/**
 * The class simply grabs a connection and a session. It then creates a topic
 * and begins to publish messages.
 */

public class Publisher {

    protected TopicConnectionFactory factory = null;

    protected TopicConnection connection = null;

    protected String factoryJNDI = "";

    protected String factoryName = "";

    protected String topicJNDI = "";

    public PublisherExceptionListener publisherExceptionListener = new PublisherExceptionListener();

    protected boolean bPersistent = false;

    protected int ackMode = Session.AUTO_ACKNOWLEDGE;

    protected boolean bJMSConnected = false;

    protected int connectionRetryPeriod = 10000; // Default milliseconds (10

    // sec)

    protected int waitForServerRestartPeriod = 150000; // Default milliseconds

    // (150 sec)

    private int maxNoOfRetry = 5;

    private String topicName = null;

    protected int delivery_mode = DeliveryMode.PERSISTENT;

    protected TopicSession session = null;

    protected Topic topic = null;

    private TopicPublisher topicPublisher = null;

    protected long messageNumber = 0;

    public Vector queuedMessages = new Vector();

    public int getAckMode() {
        return this.ackMode;
    }

    public void setAckMode(int ackMode) {
        this.ackMode = ackMode;
    }

    public boolean getBJMSConnected() {
        return this.bJMSConnected;
    }

    public void setBJMSConnected(boolean bJMSConnected) {
        this.bJMSConnected = bJMSConnected;
    }

    public boolean getBPersistent() {
        return this.bPersistent;
    }

    public void setBPersistent(boolean bPersistent) {
        this.bPersistent = bPersistent;
    }

    public TopicConnection getConnection() {
        return this.connection;
    }

    public void setConnection(TopicConnection connection) {
        this.connection = connection;
    }

    public int getConnectionRetryPeriod() {
        return this.connectionRetryPeriod;
    }

    public void setConnectionRetryPeriod(int connectionRetryPeriod) {
        this.connectionRetryPeriod = connectionRetryPeriod;
    }

    public int getDelivery_mode() {
        return this.delivery_mode;
    }

    public void setDelivery_mode(int delivery_mode) {
        this.delivery_mode = delivery_mode;
    }

    public TopicConnectionFactory getFactory() {
        return this.factory;
    }

    public void setFactory(TopicConnectionFactory factory) {
        this.factory = factory;
    }

    public int getMaxNoOfRetry() {
        return this.maxNoOfRetry;
    }

    public void setMaxNoOfRetry(int maxNoOfRetry) {
        this.maxNoOfRetry = maxNoOfRetry;
    }

    public long getMessageNumber() {
        return this.messageNumber;
    }

    public void setMessageNumber(long messageNumber) {
        this.messageNumber = messageNumber;
    }

    // end of class

    public TopicPublisher getTopicPublisher() {
        return this.topicPublisher;
    }

    public void setTopicPublisher(TopicPublisher topicPublisher) {
        this.topicPublisher = topicPublisher;
    }

    public PublisherExceptionListener getPublisherExceptionListener() {
        return this.publisherExceptionListener;
    }

    public void setPublisherExceptionListener(
            PublisherExceptionListener publisherExceptionListener) {
        this.publisherExceptionListener = publisherExceptionListener;
    }

    public Vector getQueuedMessages() {
        return this.queuedMessages;
    }

    public void setQueuedMessages(Vector queuedMessages) {
        this.queuedMessages = queuedMessages;
    }

    public TopicSession getSession() {
        return this.session;
    }

    public void setSession(TopicSession session) {
        this.session = session;
    }

    public Topic getTopic() {
        return this.topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getTopicName() {
        return this.topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    /**
     * This method evaluate the exception and returns the status of JMS
     * connection whether it is open or not
     * 
     * @Param JMSException
     *            jmsException
     * @return boolean
     */
    public boolean isConnectionDropped(JMSException jmsException) {
        // find out if connection is droppped from teh esception
        return true;

    }

    /**
     * This function closes the JMS Coonection.
     * 
     * @Param void
     * @return void
     * 
     */
    public void closeJMSConnection() {
        try {
            if (this.connection != null)
                connection.close();
            bJMSConnected = false;
        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println("Exception in Publisher::closeJMSConnection:"
                    + e.getMessage());
        }

        connection = null;
    }

    public Publisher(String connFactoryJNDI, String topicJNDI) {
        System.out.println("[Publisher]: Publisher()");

        this.factoryJNDI = connFactoryJNDI;
        this.topicJNDI = topicJNDI;

        if (createJMSConnection() && this.bJMSConnected ) {
            if (createJMSTopicPublisher()) {
                System.out
                        .println("[Publisher]: Publisher() : Got topic publisher");
            } else {
                System.out
                        .println("[Publisher]: Publisher() : Could not get topic publisher");

            }
        }
        else
        {
        	
            System.out.println("[Publisher]: Publisher() : Could not get JMS Connection, sending Notification....");
            
            String mailStatus = "";
            
            mailStatus = Impex.sendA2AConnFailEmail(connFactoryJNDI);
       	 
       	 if (StringUtil.isEmpty(mailStatus ))
       	 {
       		 mailStatus = "";
       	 }
       	 System.out.println("[Publisher]: Publisher() :  ----> : Sent Connection Failure email (if configured)" + mailStatus );
       	
        }

    }

    public boolean createJMSConnection() {
        int currentRetry = 0;

        try {
            Context context = new InitialContext();

            while (!this.bJMSConnected && !this.reachedMaximumNoOfRetry(currentRetry))
            {
            	   System.out.println("[Publisher]: createJMSConnection() :in while: " + currentRetry );
            
                try {

                    // Get the connection factory
                    TopicConnectionFactory topicFactory = (TopicConnectionFactory) context
                            .lookup(this.factoryJNDI);
                    // Create the connection
                    this.connection = topicFactory.createTopicConnection();

                } catch (Exception jmse) {
                    currentRetry++;
                    System.out
                            .println("[Publisher]: createJMSConnection() : Trying to COnnect.........currentRetry:"
                                    + currentRetry);
                    jmse.printStackTrace();
                    try {
                        Thread.sleep(this.connectionRetryPeriod);
                    } catch (java.lang.InterruptedException ie) {
                        // ie.printStackTrace();
                    }
                    continue;
                }

                this.connection
                        .setExceptionListener((javax.jms.ExceptionListener) this.publisherExceptionListener);

                // this.connection.setExceptionListener(this);
                this.connection.start();

                System.out
                        .println("[Publisher]: createJMSConnection() : Created JMSConnection");

                if (currentRetry > 1) {
                    // wait for server to restart fully
                    System.out
                            .println("[Publisher]: createJMSConnection() : Waiting for the remote server to fully restart. Will wait for "
                                    + this.waitForServerRestartPeriod
                                    / 1000
                                    + " seconds");
                    try {
                        Thread.sleep(this.waitForServerRestartPeriod);
                    } catch (java.lang.InterruptedException ie) {
                         ie.printStackTrace();
                    }

                    System.out.println("[Publisher]: continue processing...");
                }

                this.bJMSConnected = true;

            }
            if (!this.bJMSConnected) {
            }
        } catch (Exception e) {
             e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean reachedMaximumNoOfRetry(int currentRetry) {
        if (currentRetry == maxNoOfRetry)
            return true;

        return false;
    }

    public String getFactoryJNDI() {
        return this.factoryJNDI;
    }

    public void setFactoryJNDI(String factoryJNDI) {
        this.factoryJNDI = factoryJNDI;
    }

    /**
     * This function reinitialises session and publisher.
     * 
     * @return void
     * 
     */
    public void reInit() {
        this.session = null;
        this.topicPublisher = null;
    }

    public String getFactoryName() {
        return this.factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public boolean createJMSTopicPublisher() {
        try {
            Context context = new InitialContext();

            if (this.session == null) {
                this.session = this.connection.createTopicSession(false,
                        this.ackMode);

                System.out
                        .println("[Publisher]: createJMSTopicPublisher() : Got Session");

                System.out
                        .println("[Publisher]: createJMSTopicPublisher() : this.topicJNDI:"
                                + this.topicJNDI);

                if (this.topicJNDI == null) {
                    return false;
                }

                // Look up the topic destination

                this.topic = (Topic) context.lookup(topicJNDI);

                System.out
                        .println("[Publisher]: createJMSTopicPublisher() : Lookup Topic");

                if (this.getTopicPublisher() == null) {
                    this.setTopicPublisher(this.session
                            .createPublisher(this.topic));

                    if (this.topicName == null
                            || this.topicName.trim().equals("")) {
                        this.topicName = topicJNDI;
                    }
                    System.out
                            .println("[Publisher]: createJMSTopicPublisher() : Created JMSTopicPublisher");

                    this.publisherExceptionListener.addPublisher(this);

                } else {
                    System.out
                            .println("[Publisher]: createJMSTopicPublisher() : TopicPublisher is not null");
                }
            } else {
                System.out
                        .println("[Publisher]: createJMSTopicPublisher() : Session is not null");
            }
        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println("[Publisher]: Exception : " + e);
            return false;
        }

        return true;
    }

    /*
     * This is called when queuedMessages are to be published after a
     * restoration of connection. This is mainly called from ExceptionListener.
     * After a connection restoration, these messages are published one by one.
     * Note : In between publishing Queued messges if there is a break in
     * connection these are queded in temp bugger. But this is not follproof way
     * of sequencing of messages.
     */

    public void publishQueuedMessages() {
        System.out.println("Size of queuedMessages:" + queuedMessages.size());
        int i = 0;
        for (i = 0; i < queuedMessages.size(); i++) {

            Message jmsMessage = null;

            jmsMessage = (Message) queuedMessages.elementAt(i);

            if (jmsMessage != null) {
                try {
                    System.out
                            .println("in publishQueuedMessages... publishing a message  ");
                    topicPublisher.publish(jmsMessage, delivery_mode, 1, 0);
                    System.out
                            .println("in publishQueuedMessages... after publishing ");

                } catch (Exception e) {
                    System.out
                            .println(" Exception While Publishing Again. .. .");
                    System.out.println(" e.getMessage():" + e.getMessage());
                    // e.printStackTrace();
                    break;
                }
            }
        }

        // Remove messages that has been published
        for (int j = 0; j < i; j++)
            queuedMessages.removeElementAt(0);
    }

    public void closeJMSTopicPublisher() {
        try {
            if (getTopicPublisher() != null) {
                getTopicPublisher().close();
                publisherExceptionListener.removePublisher(this);
                setTopicPublisher(null);
            }
            if (getSession() != null) {
                session.close();
                session = null;
            }

            System.out.println("[Publisher]: closeJMSTopicPublisher() ");
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    // ///////////////////////////

    public void publishMessageViaPublisher(Message jmsMessage) {
        if (getBJMSConnected() && queuedMessages.size() == 0) {
            // Publish the JMS Message
            if (jmsMessage != null) {
                try {
                    topicPublisher.publish(jmsMessage, delivery_mode, 1, 0);
                } catch (Exception e) {
                    System.out
                            .println("[Publisher]: Exception While Publishng.. Will store these to process them later ");

                    // get only those packets that were rejected bec of failure
                    // of connection.
                    // e.printStackTrace();
                    queuedMessages.addElement(jmsMessage);
                }
            }
        } else {
            // there are some messages in Q that needs to be processed
            // so add this to Q and return
            queuedMessages.addElement(jmsMessage);
            if (getBJMSConnected())
                publishQueuedMessages();
        }

    }

    /**
     * This method publishes message on the topic
     * 
     * @Param Message
     *            message
     * @return void
     */
    public void PublishMessage(Message message) {
        try {
            System.out.println("[Publisher]: PublishMessage() ");
            topicPublisher.publish(message, DeliveryMode.NON_PERSISTENT, 1, 0);
        } catch (javax.jms.JMSException e) {
            System.out.println("[Publisher]: PublishMessage() Exception " + e);
            queuedMessages.addElement(message);
            interpretException(e);
        }
    }

    public String getTopicJNDI() {
        return this.topicJNDI;
    }

    public void setTopicJNDI(String topicJNDI) {
        this.topicJNDI = topicJNDI;
    }

    public void interpretException(javax.jms.JMSException jsme) {
        System.out
                .println("in PublisherExceptionListner.... got exception Connection dropped ");
        System.out
                .println("in PublisherExceptionListner.......... trying to re connect ");

        // Reestablish the connection

        closeJMSConnection();
        setBJMSConnected(false);
        if (createJMSConnection()) {
            System.out
                    .println("in PublisherExceptionListner.......... re connected ");

            String tName = getTopicName();
            reInit();
            createJMSTopicPublisher();
            publishQueuedMessages();

            System.out
                    .println("in PublisherExceptionListner.......... publishQueuedMessages ");
        }

    }

    // end of class
}