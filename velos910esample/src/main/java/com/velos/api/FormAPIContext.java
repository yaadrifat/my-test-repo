package com.velos.api;

import com.velos.api.objects.FormObject;
import com.velos.eres.service.formLibAgent.*;
import com.velos.eres.service.util.EJBUtil;

/**
 * Velos Form Facade
 *
 */
public class FormAPIContext extends APIContext {
	
protected FormAPIContext()
{
	
}

/**
 * Generates and return a new FormObject Instance 
 *
 */

public FormObject createNewFormObject()
{
	return new FormObject();
}


/**
 * Persist the passed parameter formObject into the database
 * @param formobj FormObject Typ
 * @return id of the new created form record If successful 
 *
 */
public int insertForm(FormObject formObj)
{
	int ret=0;
	FormLibAgentRObj formLibAgentRObj = EJBUtil.getFormLibAgentHome();
	ret = formLibAgentRObj.insertFilledFormBySP(formObj.getStateKeeper());
	formObj.setFilledFormId(ret);
	return ret;
}


}
