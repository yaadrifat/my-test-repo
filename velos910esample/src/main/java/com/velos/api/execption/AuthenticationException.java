package com.velos.api.execption;


import org.apache.log4j.Logger;
/**
 * Represents any authentication error with API Access 
 * 
 * 
 * @author Vishal Abrol
 */
public class AuthenticationException extends Exception {
    private static Logger logger = Logger.getLogger(AuthenticationException.class
            .getName());

    /**
     * Constructs an instance of <code>ApplicationException</code> with the
     * specified detail message.
     */
    public AuthenticationException(String msg) {
        super(msg);
       logger.fatal(msg);
    }
}


