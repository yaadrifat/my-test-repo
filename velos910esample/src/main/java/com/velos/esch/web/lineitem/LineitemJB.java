/* 
 * Classname			LineitemJB.class
 * 
 * Version information
 *
 * Date					03/28/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.web.lineitem;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.persistence.Column;

import com.velos.esch.business.common.LineitemDao;
import com.velos.esch.business.lineitem.impl.LineitemBean;
import com.velos.esch.service.lineitemAgent.LineitemAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for Budget Appendix
 * 
 * @author Sajal
 * @version 1.0 03/28/2002
 */

public class LineitemJB {
    /**
     * the lineitem id
     */
    private int lineitemId;

    /**
     * the lineitem budget section
     */
    private String lineitemBgtSection;

    /**
     * the lineitem name
     */
    private String lineitemName;

    /**
     * the lineitem description
     */
    private String lineitemDesc;

    /**
     * the lineitem sponsor or unit cost
     */
    private String lineitemSponsorUnit;

    /**
     * the lineitem clinic cost or number of units
     */

    private String lineitemClinicNOfUnit;

    /**
     * the lineitem other cost
     */
    private String lineitemOtherCost;

    /**
     * the lineitem delete flag
     */
    private String lineitemDelFlag;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * line item notes
     */
    private String lineitemNotes;

    /*
     * line item include in all sections or in personnel section
     */
    private String lineitemInPerSec;

    /*
     * line item standard of care cost
     */
    private String lineitemStdCareCost;

    /*
     * line item research cost
     */
    private String lineitemResCost;

    /*
     * line item invoice cost
     */
    private String lineitemInvCost;

    /*
     * line item include cost discount
     */
    private String lineitemInCostDisc;

    /*
     * line item cpt code
     */
    private String lineitemCptCode;

    /*
     * line item repeat
     */
    private String lineitemRepeat;

    /*
     * line item parent id
     */
    private String lineitemParentId;

    /*
     * line item apply in future
     */
    private String lineitemAppInFuture;

    /*
     * line item category
     */
    private String lineitemCategory;

    /*
     * line item TMID
     */
    private String lineitemTMID;

    /*
     * line item CDM
     */
    private String lineitemCDM;

    /*
     * line item Apply indirects flag
     */
    private String lineitemAppIndirects;
    
    
    private String lineitemTotalCost;
    
    
    //added by IA 11.03.2006
    
    private String lineItemSponsorAmount;
    
    private String lineItemVariance;
    
    private  String lineItemSequence;
    private  String lineItemCostType;

    private  Integer lineItemFkEvent;
    //end added

    // GETTER SETTER METHODS

    /**
     * @param lineitemId
     *            The value that is required to be registered as primary key of
     *            lineitem Id
     */

    public void setLineitemId(int lineitemId) {
        this.lineitemId = lineitemId;
    }

    /**
     * @return lineitem id
     */
    public int getLineitemId() {
        return this.lineitemId;
    }

    /**
     * @param lineitemId
     *            The value that is required to be registered as lineitem budget
     *            section
     */

    public void setLineitemBgtSection(String lineitemBgtSection) {
        this.lineitemBgtSection = lineitemBgtSection;
    }

    /**
     * @return lineitem budget section
     */
    public String getLineitemBgtSection() {
        return this.lineitemBgtSection;
    }

    /**
     * @param lineitemName
     *            The value that is required to be registered as lineitem name
     */

    public void setLineitemName(String lineitemName) {
        this.lineitemName = lineitemName;
    }

    /**
     * @return lineitem name
     */
    public String getLineitemName() {
        return this.lineitemName;
    }

    /**
     * @param lineitemDesc
     *            The value that is required to be registered as lineitem
     *            description
     */

    public void setLineitemDesc(String lineitemDesc) {
        this.lineitemDesc = lineitemDesc;
    }

    /**
     * @return lineitem description
     */
    public String getLineitemDesc() {
        return this.lineitemDesc;
    }

    /**
     * @param lineitemSponsorUnit
     *            The value that is required to be registered as lineitem
     *            sponsor cost or unit cost
     */

    public void setLineitemSponsorUnit(String lineitemSponsorUnit) {
        this.lineitemSponsorUnit = lineitemSponsorUnit;
    }

    /**
     * @return lineitem sponsor cost or unit cost
     */
    public String getLineitemSponsorUnit() {
        return this.lineitemSponsorUnit;
    }

    /**
     * @param lineitemClinicNOfUnit
     *            The value that is required to be registered as the description
     *            for linitem clinic cost or number of units
     */

    public void setLineitemClinicNOfUnit(String lineitemClinicNOfUnit) {
        this.lineitemClinicNOfUnit = lineitemClinicNOfUnit;
    }

    /**
     * @return linitem clinic cost or number of units
     */
    public String getLineitemClinicNOfUnit() {
        return this.lineitemClinicNOfUnit;
    }

    /**
     * @param lineitemOtherCost
     *            The value that is required to be registered as lineitem other
     *            cost
     */

    public void setLineitemOtherCost(String lineitemOtherCost) {
        this.lineitemOtherCost = lineitemOtherCost;
    }

    /**
     * @return lineitem other cost
     */

    public String getLineitemOtherCost() {
        return this.lineitemOtherCost;
    }

    /**
     * @param lineitemDelFlag
     *            The value that is required to be registered as lineitem delete
     *            flag
     */

    public void setLineitemDelFlag(String lineitemDelFlag) {
        this.lineitemDelFlag = lineitemDelFlag;
    }

    /**
     * @return lineitem delete flag
     */
    public String getLineitemDelFlag() {
        return this.lineitemDelFlag;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return last modified by
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return IP
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Gets the flag which indicates apply lineitem changes in future or
     * previous lineitems
     * 
     * @return apply in future flag
     */

    public String getLineitemAppInFuture() {
        return this.lineitemAppInFuture;
    }

    /**
     * Sets the apply in future flag
     * 
     * @param apply
     *            in future flag
     */

    public void setLineitemAppInFuture(String lineitemAppInFuture) {
        this.lineitemAppInFuture = lineitemAppInFuture;
    }

    /**
     * Gets the lineitem CPT code
     * 
     * @return lineitem CPT code
     */
    public String getLineitemCptCode() {
        return this.lineitemCptCode;
    }

    /**
     * Sets the lineitem CPT code
     * 
     * @param lineitem
     *            CPT code
     */

    public void setLineitemCptCode(String lineitemCptCode) {
        this.lineitemCptCode = lineitemCptCode;
    }

    /**
     * Gets the flag which indicates include cost discount
     * 
     * @return cost discount flag
     */
    public String getLineitemInCostDisc() {
        return this.lineitemInCostDisc;
    }

    /**
     * Sets the cost discount flag
     * 
     * @param cost
     *            discount flag
     */

    public void setLineitemInCostDisc(String lineitemInCostDisc) {
        this.lineitemInCostDisc = lineitemInCostDisc;
    }

    /**
     * Gets the flag which indicates include cost in all Sections
     * 
     * @return include cost in all sections flag
     */
    public String getLineitemInPerSec() {
        return this.lineitemInPerSec;
    }

    /**
     * Sets the include cost in all Sections flag
     * 
     * @param include
     *            cost in all Sections flag
     */
    public void setLineitemInPerSec(String lineitemInPerSec) {
        this.lineitemInPerSec = lineitemInPerSec;
    }

    /**
     * Gets the lineitem invoice cost
     * 
     * @return lineitem invoice cost
     */
    public String getLineitemInvCost() {
        return this.lineitemInvCost;
    }

    /**
     * Sets the lineitem invoice cost
     * 
     * @param lineitem
     *            invoice cost
     */
    public void setLineitemInvCost(String lineitemInvCost) {
        this.lineitemInvCost = lineitemInvCost;
    }

    /**
     * Gets the lineitem notes
     * 
     * @return lineitem notes
     */
    public String getLineitemNotes() {
        return this.lineitemNotes;
    }

    /**
     * Sets the lineitem notes
     * 
     * @param lineitem
     *            notes
     */
    public void setLineitemNotes(String lineitemNotes) {
        this.lineitemNotes = lineitemNotes;
    }

    /**
     * Gets the lineitem parent
     * 
     * @return lineitem parent
     */
    public String getLineitemParentId() {
        return this.lineitemParentId;
    }

    /**
     * Sets the lineitem parent
     * 
     * @param lineitem
     *            parent
     */
    public void setLineitemParentId(String lineitemParentId) {
        this.lineitemParentId = lineitemParentId;
    }

    /**
     * Gets the lineitem repeat flag
     * 
     * @return lineitem repeat flag
     */
    public String getLineitemRepeat() {
        return this.lineitemRepeat;
    }

    /**
     * Sets the lineitem repeat flag
     * 
     * @param lineitem
     *            repeat flag
     */
    public void setLineitemRepeat(String lineitemRepeat) {
        this.lineitemRepeat = lineitemRepeat;
    }

    /**
     * Gets the lineitem research cost
     * 
     * @return lineitem research cost
     */
    public String getLineitemResCost() {
        return this.lineitemResCost;
    }

    /**
     * Sets the lineitem research cost
     * 
     * @param lineitem
     *            research cost
     */
    public void setLineitemResCost(String lineitemResCost) {
        this.lineitemResCost = lineitemResCost;
    }

    /**
     * Gets the Standard Care Cost
     * 
     * @return standard care cost
     */
    public String getLineitemStdCareCost() {
        return this.lineitemStdCareCost;
    }

    /**
     * Sets the Standard Care Cost
     * 
     * @param Standard
     *            Care Cost
     */
    public void setLineitemStdCareCost(String lineitemStdCareCost) {
        this.lineitemStdCareCost = lineitemStdCareCost;
    }

    /**
     * Gets category of a line item
     * 
     * @return line item category
     */

    public String getLineitemCategory() {
        return this.lineitemCategory;
    }

    /**
     * Sets the category of a lineitem
     * 
     * @param lineitem
     *            category
     */

    public void setLineitemCategory(String lineitemCategory) {
        this.lineitemCategory = lineitemCategory;
    }

    /**
     * Gets TMID of a line item
     * 
     * @return line item TMID
     */

    public String getLineitemTMID() {
        return this.lineitemTMID;
    }

    /**
     * Sets the TMID of a lineitem
     * 
     * @param lineitem
     *            TMID
     */

    public void setLineitemTMID(String lineitemTMID) {
        this.lineitemTMID = lineitemTMID;
    }

    /**
     * Gets CDM of a line item
     * 
     * @return line item CDM
     */

    public String getLineitemCDM() {
        return this.lineitemCDM;
    }

    /**
     * Sets the CDM of a lineitem
     * 
     * @param lineitem
     *            CDM
     */

    public void setLineitemCDM(String lineitemCDM) {
        this.lineitemCDM = lineitemCDM;
    }

    /**
     * Gets apply indirects flag of a line item
     * 
     * @return line item indirects
     */

    public String getLineitemAppIndirects() {
        return this.lineitemAppIndirects;
    }

    /**
     * Sets the AppIndirects of a lineitem
     * 
     * @param lineitem
     *            AppIndirects
     */

    public void setLineitemAppIndirects(String lineitemAppIndirects) {
        this.lineitemAppIndirects = lineitemAppIndirects;
    }

    /**
     * Gets fk_event of a line item
     * 
     * @return line item fk_event
     */

    public Integer getLineitemFkEvent() {
        return this.lineItemFkEvent;
    }

    /**
     * Sets the fk_event of a line item
     * 
     * @param lineitem
     *            fk_event
     */

    public void setLineitemFkEvent(Integer lineitemFkEvent) {
        this.lineItemFkEvent = lineitemFkEvent;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts lineitemId
     * 
     * @param lineitemId
     *            Id of the Lineitem
     */
    public LineitemJB(int lineitemId) {
        setLineitemId(lineitemId);
    }

    
    public void setLineitemTotalCost(String lineitemTotalCost) {
        this.lineitemTotalCost = lineitemTotalCost;
    }

    /**
     * @return lineitem id
     */
    public String getLineitemTotalCost() {
        return this.lineitemTotalCost;
    }
    
    
    /**
     * Default Constructor
     * 
     */
    public LineitemJB() {
        Rlog.debug("Lineitem", "LineitemJB.LineitemJB() ");
    }

    public LineitemJB(int lineitemId, String lineitemBgtSection,
            String lineitemName, String lineitemDesc,
            String lineitemSponsorUnit, String lineitemClinicNOfUnit,
            String lineitemOtherCost, String lineitemDelFlag,
            String lineitemNotes, String lineitemInPerSec,
            String lineitemStdCareCost, String lineitemResCost,
            String lineitemInvCost, String lineitemInCostDisc,
            String lineitemCptCode, String lineitemRepeat,
            String lineitemParentId, String lineitemAppInFuture,
            String lineitemCategory, String lineitemTMID, String lineitemCDM,
            String lineitemAppIndirects, String lineitemTotalCost,String costtype,String lineseq, 
            Integer lineitemFkEvent) {
        setLineitemId(lineitemId);
        setLineitemBgtSection(lineitemBgtSection);
        setLineitemName(lineitemName);
        setLineitemDesc(lineitemDesc);
        setLineitemSponsorUnit(lineitemSponsorUnit);
        setLineitemClinicNOfUnit(lineitemClinicNOfUnit);
        setLineitemOtherCost(lineitemOtherCost);
        setLineitemDelFlag(lineitemDelFlag);
        setLineitemNotes(lineitemNotes);
        setLineitemInPerSec(lineitemInPerSec);
        setLineitemStdCareCost(lineitemStdCareCost);
        setLineitemResCost(lineitemResCost);
        setLineitemInvCost(lineitemInvCost);
        setLineitemInCostDisc(lineitemInCostDisc);
        setLineitemCptCode(lineitemCptCode);
        setLineitemRepeat(lineitemRepeat);
        setLineitemParentId(lineitemParentId);
        setLineitemAppInFuture(lineitemAppInFuture);
        setLineitemCategory(lineitemCategory);
        setLineitemTMID(lineitemTMID);
        setLineitemCDM(lineitemCDM);
        setLineitemAppIndirects(lineitemAppIndirects);
        setLineitemTotalCost(lineitemTotalCost);
        
        setLineItemSequence(lineseq);
        setLineitemFkEvent(lineitemFkEvent);
        setLineItemCostType(costtype);
        
        Rlog.debug("Lineitem", "LineitemJB.LineitemJB()(all parameters)");
    }

    /**
     * Calls getLineitemDetails() of Lineitem Session Bean: LineitemAgentBean
     * 
     * @return The Details of the lineitem in the LineitemStateKeeper Object
     * @See LineitemAgentBean
     */

    public LineitemBean getLineitemDetails() {
        LineitemBean lsk = null;

        try {
            LineitemAgentRObj lineitemAgent = EJBUtil.getLineitemAgentHome();
            lsk = lineitemAgent.getLineitemDetails(this.lineitemId);
            Rlog.debug("Lineitem",
                    "LineitemJB.getLineitemDetails() LineitemStateKeeper "
                            + lsk);
        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "Error in getLineitemDetails() in LineitemJB " + e);
        }
        if (lsk != null) {
            this.lineitemId = lsk.getLineitemId();
            this.lineitemBgtSection = lsk.getLineitemBgtSection();
            this.lineitemName = lsk.getLineitemName();
            this.lineitemDesc = lsk.getLineitemDesc();
            this.lineitemSponsorUnit = lsk.getLineitemSponsorUnit();
            this.lineitemClinicNOfUnit = lsk.getLineitemClinicNOfUnit();
            this.lineitemOtherCost = lsk.getLineitemOtherCost();
            this.lineitemDelFlag = lsk.getLineitemDelFlag();
            this.creator = lsk.getCreator();
            this.modifiedBy = lsk.getModifiedBy();
            this.ipAdd = lsk.getIpAdd();
            this.lineitemNotes = lsk.getLineitemNotes();
            this.lineitemInPerSec = lsk.getLineitemInPerSec();
            this.lineitemStdCareCost = lsk.getLineitemStdCareCost();
            this.lineitemResCost = lsk.getLineitemResCost();
            this.lineitemInvCost = lsk.getLineitemInvCost();
            this.lineitemInCostDisc = lsk.getLineitemInCostDisc();
            this.lineitemCptCode = lsk.getLineitemCptCode();
            this.lineitemRepeat = lsk.getLineitemRepeat();
            this.lineitemParentId = lsk.getLineitemParentId();
            this.lineitemAppInFuture = lsk.getLineitemAppInFuture();
            this.lineitemCategory = lsk.getLineitemCategory();
            this.lineitemTMID = lsk.getLineitemTMID();
            this.lineitemCDM = lsk.getLineitemCDM();
            this.lineitemAppIndirects = lsk.getLineitemAppIndirects();
            this.lineitemTotalCost = lsk.getLineitemTotalCost();
        
            setLineItemSequence(lsk.getLineItemSequence());
            setLineitemFkEvent(lsk.getLineItemFkEvent());
            setLineItemCostType(lsk.getLineItemCostType());
            setLineItemSponsorAmount(lsk.getLineItemSponsorAmount());
            
        }
        return lsk;
    }

    /**
     * Calls setLineitemDetails() of Lineitem Session Bean: LineitemAgentBean
     * 
     */

    public void setLineitemDetails() {

        try {
            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            this.setLineitemId(lineitemAgentRObj.setLineitemDetails(this
                    .createLineitemStateKeeper()));
            Rlog.debug("Lineitem", "LineitemJB.setLineitemDetails()");
        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "Error in setLineitemDetails() in LineitemJB " + e);
        }
    }

    /**
     * Calls updateLineitem() of Lineitem Session Bean: LineitemAgentBean
     * 
     * @return -2 in case of exception
     */
    public int updateLineitem() {
        int output;
        try {
            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            output = lineitemAgentRObj.updateLineitem(this
                    .createLineitemStateKeeper());
        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "EXCEPTION IN SETTING LINEITEM DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    // Overloaded for INF-18183 ::: Akshi
    public int updateLineitem(Hashtable<String, String> args) {
        int output;
        try {
            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            output = lineitemAgentRObj.updateLineitem(this
                    .createLineitemStateKeeper(),args);
        } catch (Exception e) {
            Rlog.debug("Lineitem",
                    "EXCEPTION IN SETTING LINEITEM DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * @return the Lineitem StateKeeper Object with the current values of the
     *         Bean
     */

    public LineitemBean createLineitemStateKeeper() {
        Rlog.debug("Lineitem", "LineitemJB.createLineitemStateKeeper ");

        return new LineitemBean(lineitemId, lineitemBgtSection, lineitemName,
                lineitemDesc, lineitemSponsorUnit, lineitemClinicNOfUnit,
                lineitemOtherCost, lineitemDelFlag, creator, modifiedBy, ipAdd,
                lineitemNotes, lineitemInPerSec, lineitemStdCareCost,
                lineitemResCost, lineitemInvCost, lineitemInCostDisc,
                lineitemCptCode, lineitemRepeat, lineitemParentId,
                lineitemAppInFuture, lineitemCategory, lineitemTMID,
                lineitemCDM, lineitemAppIndirects, lineitemTotalCost, lineItemSponsorAmount, lineItemVariance,
                lineItemCostType,lineItemSequence,lineItemFkEvent);
    }

    /**
     * Get the detials of all the lineitems associated to a budget calendar
     * 
     * @param bgtcalId
     */

    public LineitemDao getLineitemsOfBgtcal(int bgtcalId) {
        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            lineitemDao = lineitemAgentRObj.getLineitemsOfBgtcal(bgtcalId);
        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.getAllBgtCals " + e);
        }
        return lineitemDao;
    }

    /**
     * Saves the line items of Patient Budget
     */

    public int updateAllLineitems(ArrayList lineitemIds, ArrayList discounts,
            ArrayList unitCosts, ArrayList noUnits, ArrayList researchCosts,
            ArrayList stdCareCosts, ArrayList categories,
            ArrayList applyIndirects, String ipAdd, String usr, ArrayList lineItemTotalCosts, ArrayList lineItemSponsorAmount, ArrayList lineItemVariance ) {

        int output = 0;
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            output = lineitemAgentRObj.updateAllLineitems(lineitemIds,
                    discounts, unitCosts, noUnits, researchCosts, stdCareCosts,
                    categories, applyIndirects, ipAdd, usr, lineItemTotalCosts, lineItemSponsorAmount, lineItemVariance);
        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.updateAllLineitems "
                    + e);
            return -2;
        }
        return output;
    }

    public int setRepeatLineitems(String[] lineitemNames,
            String[] lineitemDescs, String[] lineitemCpts,
            String[] lineitemRepeatOpts, String[] categories, String[] tmids,
            String[] cdms, String bgtSectionId, String budgetCalId,
            String ipAdd, String usr) {
        int output = 0;
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            output = lineitemAgentRObj.setRepeatLineitems(lineitemNames,
                    lineitemDescs, lineitemCpts, lineitemRepeatOpts,
                    categories, tmids, cdms, bgtSectionId, budgetCalId, ipAdd,
                    usr);
        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.setRepeatLineitems "
                    + e);
            return -2;
        }
        return output;
    }

    /**
     * Delete Section from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */
    public int lineitemDelete(int lineitemId) {
        try {
            int ret = 0;

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            ret = lineitemAgentRObj.lineitemDelete(lineitemId);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("lineitem", "Error in lineitemJB LineitemDelete " + e);
            return -2;
        }

    }

    public LineitemDao insertPersonnelCost(int budgetCalId, String[] perTypes,
            String[] rates, String[] inclusions, String[] notes,
            String[] categories, String[] tmids, String[] cdms, int creator,
            String ipAdd) {
        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();

            lineitemDao = lineitemAgentRObj.insertPersonnelCost(budgetCalId,
                    perTypes, rates, inclusions, notes, categories, tmids,
                    cdms, creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.insertPersonnelCost "
                    + e);
        }
        return lineitemDao;
    }

    public LineitemDao getLineitemsOfDefaultSection(int budgetCalId) {
        Rlog.debug("Lineitem", "LineitemJB.anu JB 1");
        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();

            lineitemDao = lineitemAgentRObj
                    .getLineitemsOfDefaultSection(budgetCalId);

        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "Error in LineitemJB.getLineitemsOfDefaultSection " + e);
        }
        return lineitemDao;
    }

    /* method to update personnel dependinng on inclusion type */
    public LineitemDao updatePersonnelCost(int budgetCalId, String[] parentIds,
            String[] personnelTypes, String[] rates, String[] inclusions,
            String[] notes, String[] applyTypes, String[] categories,
            String[] tmids, String[] cdms, int p_creator, String ipAdd) {

        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();

            lineitemDao = lineitemAgentRObj.updatePersonnelCost(budgetCalId,
                    parentIds, personnelTypes, rates, inclusions, notes,
                    applyTypes, categories, tmids, cdms, p_creator, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.updatePersonnelCost "
                    + e);
        }
        return lineitemDao;
    }

    public LineitemDao deleteLineitemPerCost(int lineitemId, int applyType,
            int lastModifiedBy, String ipAdd) {
        Rlog.debug("Lineitem", "LineitemJB.deleteLineitemPerCost");
        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            lineitemDao = lineitemAgentRObj.deleteLineitemPerCost(lineitemId,
                    applyType, lastModifiedBy, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.deleteLineitemPerCost "
                    + e);
        }
        return lineitemDao;
    }

    //Overloaded for INF-18183 ::: Ankit
    public LineitemDao deleteLineitemPerCost(int lineitemId, int applyType,
            int lastModifiedBy, String ipAdd, Hashtable<String, String> auditInfo) {
        Rlog.debug("Lineitem", "LineitemJB.deleteLineitemPerCost");
        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            lineitemDao = lineitemAgentRObj.deleteLineitemPerCost(lineitemId,
                    applyType, lastModifiedBy, ipAdd, auditInfo);
        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.deleteLineitemPerCost "
                    + e);
        }
        return lineitemDao;
    }
    
    /* method to update all repeating line items */

    public int updateRepeatLineitems(int budgetCalId, String[] parentIds,
            String[] lineitemNames, String[] lineitemDescs,
            String[] lineitemCpts, String[] lineitemRepeat,
            String[] applyTypes, String[] categories, String[] tmids,
            String[] cdms, int creator, String ipAdd) {
        int output = 0;
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            output = lineitemAgentRObj.updateRepeatLineitems(budgetCalId,
                    parentIds, lineitemNames, lineitemDescs, lineitemCpts,
                    lineitemRepeat, applyTypes, categories, tmids, cdms,
                    creator, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("Lineitem", "Error in LineitemJB.updateRepeatLineitems "
                    + e);
            return -2;
        }
        return output;
    }

    /* Method to get all the repeat default lineitems of a budgetcal */
    public LineitemDao getLineitemsOfRepeatDefaultSec(int budgetCalId) {

        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();

            lineitemDao = lineitemAgentRObj
                    .getLineitemsOfRepeatDefaultSec(budgetCalId);

        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "Error in LineitemJB.getLineitemsOfRepeatDefaultSec " + e);
        }
        return lineitemDao;
    }

    /**
     * Method to add the multiple lineitems to a sections
     * 
     * @param LineitemDao
     * 
     */

    public int insertAllLineItems(ArrayList lineitemNames,
            ArrayList lineitemDescs, ArrayList lineitemCpts,
            ArrayList lineitemCategory, ArrayList TMIDs, ArrayList CDMs,
            String bgtSectionId, String ipAdd, String usr,Hashtable htMoreData) {

        int ret = 0;
        Rlog.debug("Lineitem", "insertAllLineItems() in LineitemJB 11111111");

        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            ret = lineitemAgentRObj.insertAllLineItems(lineitemNames,
                    lineitemDescs, lineitemCpts, lineitemCategory, TMIDs, CDMs,
                    bgtSectionId, ipAdd, usr,htMoreData);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("Lineitem", "insertAllLineItems() in LineitemJB " + e);
            return -2;
        }

    }

    public int deleteSelectedLineitems(ArrayList lineitemIds,
            int lastModifiedBy, String ipAdd) {

        int ret = 0;
        Rlog.debug("Lineitem", "LineitemJB.deleteSelectedLineitems");
        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            ret = lineitemAgentRObj.deleteSelectedLineitems(lineitemIds,
                    lastModifiedBy, ipAdd);
        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "Error in LineitemJB.deleteSelectedLineitems " + e);

        }
        return ret;
    }

    //Overloaded for INF-18183 ::: Ankit
    public int deleteSelectedLineitems(ArrayList lineitemIds,
            int lastModifiedBy, String ipAdd, Hashtable<String, String> auditInfo) {

        int ret = 0;
        Rlog.debug("Lineitem", "LineitemJB.deleteSelectedLineitems");
        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();
            ret = lineitemAgentRObj.deleteSelectedLineitems(lineitemIds,
                    lastModifiedBy, ipAdd, auditInfo);
        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "Error in LineitemJB.deleteSelectedLineitems " + e);

        }
        return ret;
    }

    /* Method to get all the lineitems of a section*/

    public LineitemDao getSectionLineitems(int sectionPK) {

        LineitemDao lineitemDao = new LineitemDao();
        try {

            LineitemAgentRObj lineitemAgentRObj = EJBUtil
                    .getLineitemAgentHome();

            lineitemDao = lineitemAgentRObj
                    .getSectionLineitems(sectionPK);

        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "Error in LineitemJB.getSectionLineitems " + e);
        }
        return lineitemDao;
    }
    
     
	public String getLineItemSequence() {
		return lineItemSequence;
	}

	public void setLineItemSequence(String lineItemSequence) {
		this.lineItemSequence = lineItemSequence;
	}

	 
	public String getLineItemCostType() {
		return lineItemCostType;
	}

	public void setLineItemCostType(String lineItemCostType) {
		this.lineItemCostType = lineItemCostType;
	}

	public String getLineItemSponsorAmount() {
		return lineItemSponsorAmount;
	}

	public void setLineItemSponsorAmount(String lineItemSponsorAmount) {
		this.lineItemSponsorAmount = lineItemSponsorAmount;
	}
 

}// end of class
