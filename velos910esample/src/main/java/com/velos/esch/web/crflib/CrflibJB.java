/*
 * Classname : CrflibJB
 * 
 * Version information: 1.0 
 *
 * Date: 1/31/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh
 */

package com.velos.esch.web.crflib;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import com.velos.esch.business.common.CrflibDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.crflib.impl.CrflibBean;
import com.velos.esch.service.crflibAgent.CrflibAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for CRF
 * 
 * @author Dinesh
 * @version 1.0 01/31/2002
 * 
 */

public class CrflibJB {

    /**
     * crflib id
     */
    private int crflibId;

    /**
     * event1 id
     */
    private String eventsId;

    /**
     * crflib number
     */
    private String crflibNumber;

    /**
     * crflib name
     */

    private String crflibName;

    /*
     * creator
     */
    private String creator;

    /*
     * creator
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * crflibflag
     */
    private String crflibFlag;

    // GETTER SETTER METHODS

    public int getCrflibId() {
        return this.crflibId;
    }

    public void setCrflibId(int crflibId) {
        this.crflibId = crflibId;
    }

    public String getEventsId() {
        return this.eventsId;
    }

    public void setEventsId(String eventsId) {
        this.eventsId = eventsId;
    }

    public String getCrflibNumber() {
        return this.crflibNumber;
    }

    public void setCrflibNumber(String crflibNumber) {
        this.crflibNumber = crflibNumber;
    }

    public String getCrflibName() {
        return this.crflibName;
    }

    public void setCrflibName(String crflibName) {
        this.crflibName = crflibName;
    }

    public String getCrflibFlag() {
        return this.crflibFlag;
    }

    public void setCrflibFlag(String crflibFlag) {
        this.crflibFlag = crflibFlag;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return IP Address
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Crflib Id
     * 
     * @param crflibId
     *            Id of the Crflib
     */
    public CrflibJB(int crflibId) {
        setCrflibId(crflibId);
    }

    /**
     * Default Constructor
     * 
     */
    public CrflibJB() {
        Rlog.debug("crflib", "CrflibJB.CrflibJB() ");
    }

    public CrflibJB(int crflibId, String eventsId, String crflibNumber,
            String crflibName, String crflibFlag, String creator,
            String modifiedBy, String ipAdd) {

        setCrflibId(crflibId);
        setEventsId(eventsId);
        setCrflibNumber(crflibNumber);
        setCrflibName(crflibName);
        setCrflibFlag(crflibFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("crflib", "CrflibJB.CrflibJB(all parameters)");
    }

    /**
     * Calls getCrflibDetails() of Crflib Session Bean: CrflibAgentBean
     * 
     * 
     * @return The Details of the crflib in the Crflib StateKeeper Object
     * @See CrflibAgentBean
     */

    public CrflibBean getCrflibDetails() {
        CrflibBean edsk = null;

        try {
            CrflibAgentRObj crflibAgent = EJBUtil.getCrflibAgentHome();
            edsk = crflibAgent.getCrflibDetails(this.crflibId);
            Rlog.debug("crflib",
                    "CrflibJB.getCrflibDetails() CrflibStateKeeper " + edsk);
        } catch (Exception e) {
            Rlog
                    .debug("crflib", "Error in getCrflibDetails() in CrflibJB "
                            + e);
        }
        if (edsk != null) {
            this.crflibId = edsk.getCrflibId();
            this.eventsId = edsk.getEventsId();
            this.crflibNumber = edsk.getCrflibNumber();
            this.crflibName = edsk.getCrflibName();
            this.crflibFlag = edsk.getCrflibFlag();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
        }
        return edsk;
    }

    /**
     * Calls setCrflibDetails() of Crflib Session Bean: CrflibAgentBean
     * 
     */

    public void setCrflibDetails() {

        try {
            CrflibAgentRObj crflibAgent = EJBUtil.getCrflibAgentHome();
            this.setCrflibId(crflibAgent.setCrflibDetails(this
                    .createCrflibStateKeeper()));
            Rlog.debug("crflib", "CrflibJB.setCrflibDetails()");
        } catch (Exception e) {
            Rlog
                    .fatal("crflib", "Error in setCrflibDetails() in CrflibJB "
                            + e);
        }
    }

    /**
     * Calls updateCrflib() of Crflib Session Bean: CrflibAgentBean
     * 
     * @return
     */
    public int updateCrflib() {
        int output;
        try {

            CrflibAgentRObj crflibAgentRObj = EJBUtil.getCrflibAgentHome();
            output = crflibAgentRObj.updateCrflib(this
                    .createCrflibStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("crflib",
                    "EXCEPTION IN SETTING CRF DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls propagateCrflib() of eventdefdao.
     * 
     * @return
     */
    public int propagateCrflib(int protocol_id, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType) {
        int output;
        CrflibBean ecsk;

        try {
            EventdefDao eventdefdao = new EventdefDao();

            CrflibAgentRObj crflibAgentRObj = EJBUtil.getCrflibAgentHome();
            ecsk = this.createCrflibStateKeeper();
            output = eventdefdao.propagateEventUpdates(protocol_id, EJBUtil
                    .stringToNum(ecsk.getEventsId()), "EVENT_CRF", ecsk
                    .getCrflibId(), propagateInVisitFlag, propagateInEventFlag,
                    mode, calType);

        } catch (Exception e) {
            Rlog.debug("eventdef",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public EventInfoDao getEventCrfs(int eventId) {
        EventInfoDao eventinfoDao = new EventInfoDao();
        try {

            CrflibAgentRObj crflibAgentRObj = EJBUtil.getCrflibAgentHome();
            Rlog.debug("crflib", "CrflibJB.getEventCrfs after remote");
            eventinfoDao = crflibAgentRObj.getEventCrfs(eventId);
            Rlog.debug("crflib", "CrflibJB.getEventCrfs after Dao");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("crflib",
                    "Error in getEventCrfs(int eventId) in CrflibJB " + e);
        }
        return eventinfoDao;
    }

    public int removeCrflib(int crflibId) {
        int output;
        try {
            CrflibAgentRObj crflibAgentRObj = EJBUtil.getCrflibAgentHome();
            output = crflibAgentRObj.removeCrflib(crflibId);
        } catch (Exception e) {
            Rlog.debug("crflib", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    
 // Overloaded for INF-18183 ::: Akshi
    public int removeCrflib(int crflibId, Hashtable<String, String> args) {
        int output;
        try {
            CrflibAgentRObj crflibAgentRObj = EJBUtil.getCrflibAgentHome();
            output = crflibAgentRObj.removeCrflib(crflibId,args);
        } catch (Exception e) {
            Rlog.debug("crflib", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @return the Crflib StateKeeper Object with the current values of the Bean
     */

    public CrflibBean createCrflibStateKeeper() {

        return new CrflibBean(crflibId, eventsId, crflibNumber, crflibName,
                crflibFlag, creator, modifiedBy, ipAdd);
    }

    /**
     * Method to add the multiple CRF Names and Numbers
     * 
     * @param CrfLibDao
     * 
     */

    public CrflibDao insertCrfNamesNums(CrflibDao crflibDao) {

        /*
         * SV, 10/21/04, changed to return crflibdao with id's created in
         * agentbean step, for the propagation routine.
         * 
         * int ret=0;
         * 
         */
        CrflibDao crflibIds = null;
        try {
            Rlog.debug("crflib", "CrfLibJB.insertCrfNamesNums starting");
            CrflibAgentRObj crflibAgentRObj = EJBUtil.getCrflibAgentHome();
            crflibIds = crflibAgentRObj.insertCrfNamesNums(crflibDao); // SV,
            // 10/21/04
            Rlog.debug("crflib", "return value in JB");
            return crflibIds; // SV, 10/21/04, part of change for propagation
            // routine.
        } catch (Exception e) {
            Rlog.fatal("crflib", "insertCrfNamesNums() in crfLibJB " + e);
            return new CrflibDao();
        }

    }

}// end of class
