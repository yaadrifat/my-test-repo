/*
 * Classname : EventAssocJB
 *
 * Version information: 1.0
 *
 * Date: 06/12/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.web.eventassoc;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.persistence.Column;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.velos.esch.business.common.EventAssocDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;


/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for EventAssoc
 *
 * @author Dinesh
 * @version 1.0 05/14/2001
 *
 */

public class EventAssocJB {

    /**
     * event id
     */
    private int event_id;

    private int org_id;

    /**
     * chain id
     */
    private String chain_id;

    /**
     * event type
     */
    private String event_type;

    /**
     * event name
     */

    private String name;

    /**
     * event notes
     */
    private String notes;

    /**
     * cost of the event
     */
    private String cost;

    /**
     * cost description
     */
    private String cost_description;

    /**
     * event duration
     */
    private String duration;

    /**
     * user id
     */

    private String user_id;

    /**
     * linked url
     */

    private String linked_uri;

    /**
     * fuzzy_period
     */
    private String fuzzy_period;

    /**
     * msg_to
     */
    private String msg_to;

    /**
     * description
     */

    private String description;

    /**
     * displacement
     */
    private String displacement;

    /**
     * event_msg
     */

    private String event_msg;

    /**
     * patDaysBefore
     */

    private String patDaysBefore;

    /**
     * patDaysAfter
     */

    private String patDaysAfter;

    /**
     * usrDaysBefore
     */

    private String usrDaysBefore;

    /**
     * usrDaysAfter
     */

    private String usrDaysAfter;

    /**
     * patMsgBefore
     */

    private String patMsgBefore;

    /**
     * patMsgAfter
     */

    private String patMsgAfter;

    /**
     * usrMsgBefore
     */

    private String usrMsgBefore;

    /**
     * usrMsgAfter
     */

    private String usrMsgAfter;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * status date
     */
    private String statusDate;

    /*
     * status changed by
     */
    private String statusUser;

    private String origCal;

    /*
     * Duration Type
     */
    private String durationUnit;

    /*
     * Event's visit Key
     */
    private String eventVisit;

    private String cptCode;

    private String durationUnitAfter;

    private String fuzzyAfter;

    private String durationUnitBefore;

    private String eventCalType;

    private String calAssoc;

    private String calSchDate;

    private String eventCategory;//KM

    private String sequence; //JM:

    private String eventFlag; //JM: 06May2008, added for, Enh. #PS16
    
    private String eventLibType;//KM
    
    private String eventLineCategory;
    
    /* Event Site of Service */
    private String eventSOSId;
    
    /* Event Facility */
    private String eventFacilityId;
    
    /* Event Coverage Type */
    private String eventCoverageType;

    /* Calendar Budget Template */
    private String budgetTemplate;
    
    /**     
     * Coverage Notes
     */
    private String coverageNotes;
    
    private String statCode;
    
    private String eventHideFlag; //YK: Fix for Bug#7264
    
    // GETTER SETTER METHODS

    public int getEvent_id() {
        return this.event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getOrg_id() {
        return this.org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public String getChain_id() {
        return this.chain_id;
    }

    public void setChain_id(String chain_id) {
        this.chain_id = chain_id;
    }

    public String getEvent_type() {
        return this.event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCost() {
        return this.cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCost_description() {
        return this.cost_description;
    }

    public void setCost_description(String cost_description) {
        this.cost_description = cost_description;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLinked_uri() {
        return this.linked_uri;
    }

    public void setLinked_uri(String linked_uri) {
        this.linked_uri = linked_uri;
    }

    public String getFuzzy_period() {
        return this.fuzzy_period;
    }

    public void setFuzzy_period(String fuzzy_period) {
        this.fuzzy_period = fuzzy_period;
    }

    public String getMsg_to() {
        return this.msg_to;
    }

    public void setMsg_to(String msg_to) {
        this.msg_to = msg_to;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplacement() {
        return this.displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public String getEvent_msg() {
        return this.event_msg;
    }

    public void setEvent_msg(String event_msg) {
        this.event_msg = event_msg;
    }

    public String getMsg_details() {
        return getLinked_uri();
    }

    public void setMsg_details(String msg_details) {
        setLinked_uri(msg_details);
    }

    public String getPatDaysBefore() {
        return this.patDaysBefore;
    }

    public void setPatDaysBefore(String patDaysBefore) {
        this.patDaysBefore = patDaysBefore;
    }

    public String getPatDaysAfter() {
        return this.patDaysAfter;
    }

    public void setPatDaysAfter(String patDaysAfter) {
        this.patDaysAfter = patDaysAfter;
    }

    public String getUsrDaysBefore() {
        return this.usrDaysBefore;
    }

    public void setUsrDaysBefore(String usrDaysBefore) {
        this.usrDaysBefore = usrDaysBefore;
    }

    public String getUsrDaysAfter() {
        return this.usrDaysAfter;
    }

    public void setUsrDaysAfter(String usrDaysAfter) {
        this.usrDaysAfter = usrDaysAfter;
    }

    public String getPatMsgBefore() {
        return this.patMsgBefore;
    }

    public void setPatMsgBefore(String patMsgBefore) {
        this.patMsgBefore = patMsgBefore;
    }

    public String getPatMsgAfter() {
        return this.patMsgAfter;
    }

    public void setPatMsgAfter(String patMsgAfter) {
        this.patMsgAfter = patMsgAfter;
    }

    public String getUsrMsgBefore() {
        return this.usrMsgBefore;
    }

    public void setUsrMsgBefore(String usrMsgBefore) {
        this.usrMsgBefore = usrMsgBefore;
    }

    public String getUsrMsgAfter() {
        return this.usrMsgAfter;
    }

    public void setUsrMsgAfter(String usrMsgAfter) {
        this.usrMsgAfter = usrMsgAfter;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getStatusDate() {
        return this.statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusUser() {
        return this.statusUser;
    }

    public void setStatusUser(String statusUser) {
        this.statusUser = statusUser;
    }

    public String getOrigCal() {
        return this.origCal;
    }

    public void setOrigCal(String origCal) {
        this.origCal = origCal;
    }

    // SV, 9/14/04, cal-enh-02
    public String getDurationUnit() {
        return this.durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    // SV, 9/14/04, cal-enh-02

    // SV, 9/29/04, cal-enh-03
    public String getEventVisit() {
        return this.eventVisit;
    }

    public void setEventVisit(String eventVisit) {
        this.eventVisit = eventVisit;
    }

    // END, SV, 9/29/04, cal-enh-03

    public String getCptCode() {

        return cptCode;
    }

    public void setCptCode(String cptCode) {
        this.cptCode = cptCode;
    }

    public String getDurationUnitAfter() {
        return durationUnitAfter;
    }

    public void setDurationUnitAfter(String durationUnitAfter) {
        this.durationUnitAfter = durationUnitAfter;
    }

    public String getFuzzyAfter() {
        return fuzzyAfter;
    }

    public void setFuzzyAfter(String fuzzyAfter) {
        this.fuzzyAfter = fuzzyAfter;
    }

    public String getDurationUnitBefore() {
        return durationUnitBefore;
    }

    public void setDurationUnitBefore(String durationUnit) {
        this.durationUnitBefore = durationUnit;
    }

    public String getEventCalType() {
        return eventCalType;
    }

    public void setEventCalType(String eventCalType) {
        this.eventCalType = eventCalType;
    }

    public String getCalAssocTo() {
        return calAssoc;
    }

    public void setCalAssocTo(String calAssoc) {
        this.calAssoc = calAssoc;
    }

    public String getCalSchDate() {
        return this.calSchDate;
    }

    public void setCalSchDate(String schDate) {
        this.calSchDate = schDate;
    }

    //KM
    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }


    //JM: 27Mar
    public String getEventSequence() {
        return sequence;
    }

    public void setEventSequence(String sequence) {
        this.sequence = sequence;
    }


  //JM: 06May2008, added for, Enh. #PS16
    public String getEventFlag() {
        return eventFlag;
    }

    public void setEventFlag(String eventFlag) {
        this.eventFlag = eventFlag;
    }

    public String getEventLibType() {
        return eventLibType;
    }

    public void setEventLibType(String eventLibType) {
        this.eventLibType = eventLibType;
    }
    
    public String getEventLineCategory() {
        return eventLineCategory;
    }

    public void setEventLineCategory(String eventLineCategory) {
        this.eventLineCategory = eventLineCategory;
    }

    public String getEventSOSId() {
        return eventSOSId;
    }

    public void setEventSOSId(String eventSOSId) {
        this.eventSOSId = eventSOSId;
    }
    
    public String getEventFacilityId() {
        return eventFacilityId;
    }

    public void setEventFacilityId(String eventFacilityId) {
        this.eventFacilityId = eventFacilityId;
    }
    
    public String getEventCoverageType() {
        return eventCoverageType;
    }

    public void setEventCoverageType(String eventCoverageType) {
        this.eventCoverageType = eventCoverageType;
    }
    
    public String getBudgetTemplate() {
        return budgetTemplate;
    }
    
    public void setBudgetTemplate(String budgetTemplate) {
        this.budgetTemplate = budgetTemplate;
    }
    
  //BK 23rd July 2010
    public String getCoverageNotes() {
		return this.coverageNotes;
	}

	public void setCoverageNotes(String coverageNotes) {
		this.coverageNotes = coverageNotes;
	}
	
	
	
	
	//JM: 25JAN2011: #Enh-D-FIN9
	
	public String getStatCode() {
        return statCode;
    }
    
    public void setStatCode(String statCode) {
        this.statCode = statCode;
    }
    //YK: Fix for Bug#7264
    public String getEventHideFlag() {
        return eventHideFlag;
    }

    public void setEventHideFlag(String eventHideFlag) {
        this.eventHideFlag = eventHideFlag;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts EventAssoc Id
     *
     * @param eventassocId
     *            Id of the EventAssoc
     */
    public EventAssocJB(int event_id) {
        setEvent_id(event_id);
    }

    /**
     * Default Constructor
     *
     */
    public EventAssocJB() {
        Rlog.debug("eventassoc", "EventAssocJB.EventAssocJB() ");
    }

    public EventAssocJB(int event_id, String chain_id, String event_type,
            String name, String notes, String cost, String cost_description,
            String duration, String user_id, String linked_uri,
            String fuzzy_period, String msg_to, String description, String displacement, int org_id,
            String event_msg, String patDaysBefore, String patDaysAfter,
            String usrDaysBefore, String usrDaysAfter, String patMsgBefore,
            String patMsgAfter, String usrMsgBefore, String usrMsgAfter,
            String creator, String modifiedBy, String ipAdd, String statusDate,
            String statusUser, String OrigCal, String eventCalType,
            String calAssoc, String schDate, String eventCategory, String sequence, String evtFlg,String eventLibType, String eventLineCategory,
            String eventSOSId, String eventFacilityId, String eventCoverageType, String budgetTemplate,String coverageNotes,String statCode, String eventHideFlag) {

        setEvent_id(event_id);
        setChain_id(chain_id);
        setEvent_type(event_type);
        setName(name);
        setNotes(notes);
        setCost(cost);
        setCost_description(cost_description);
        setDuration(duration);
        setUser_id(user_id);
        setLinked_uri(linked_uri);
        setFuzzy_period(fuzzy_period);
        setMsg_to(msg_to);
        setDescription(description);
        setDisplacement(displacement);
        setOrg_id(org_id);
        setEvent_msg(event_msg);
        setPatDaysBefore(patDaysBefore);
        setPatDaysAfter(patDaysAfter);
        setUsrDaysBefore(usrDaysBefore);
        setUsrDaysAfter(usrDaysAfter);
        setPatMsgBefore(patMsgBefore);
        setPatMsgAfter(patMsgAfter);
        setUsrMsgBefore(usrMsgBefore);
        setUsrMsgAfter(usrMsgAfter);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setStatusDate(statusDate);
        setStatusUser(statusUser);
        setOrigCal(OrigCal);
        setCptCode(cptCode);
        setDurationUnitAfter(durationUnitAfter);
        setDurationUnitAfter(durationUnitBefore);
        setFuzzyAfter(fuzzyAfter);
        setEventCalType(eventCalType);
        this.setCalAssocTo(calAssoc);
        this.setCalSchDate(schDate);
        setEventCategory(eventCategory);//KM

        setEventSequence(sequence);
        setEventFlag(evtFlg);//JM: 06May2008, added for, Enh. #PS16
        setEventLibType(eventLibType);//KM -#4564
        setEventLineCategory(eventLineCategory);
        
        setEventSOSId(eventSOSId);
        setEventFacilityId(eventFacilityId);
        setEventCoverageType(eventCoverageType);
        setBudgetTemplate(budgetTemplate);
        this.setCoverageNotes(coverageNotes);
        setStatCode(statCode);
        setEventHideFlag(eventHideFlag); //YK: Fix for Bug#7264
    }

    /**
     * Calls getEventAssocDetails() of EventAssoc Session Bean:
     * EventAssocAgentBean
     *
     *
     * @return The Details of the eventassoc in the EventAssoc StateKeeper
     *         Object
     * @See EventAssocAgentBean
     */

    public EventAssocBean getEventAssocDetails() {
        EventAssocBean edsk = null;

        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            edsk = eventassocAgent.getEventAssocDetails(this.event_id);
            Rlog.debug("eventassoc",
                    "EventAssocJB.getEventAssocDetails() EventAssocStateKeeper "
                            + edsk);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getEventAssocDetails() in EventAssocJB " + e);
        }
        if (edsk != null) {
            this.event_id = edsk.getEvent_id();
            this.chain_id = edsk.getChain_id();
            this.event_type = edsk.getEvent_type();
            this.name = edsk.getName();
            this.notes = edsk.getNotes();
            this.cost = edsk.getCost();
            this.cost_description = edsk.getCost_description();
            this.duration = edsk.getDuration();
            this.user_id = edsk.getUser_id();
            this.linked_uri = edsk.getLinked_uri();
            this.fuzzy_period = edsk.getFuzzy_period();
            this.msg_to = edsk.getMsg_to();
            this.description = edsk.getDescription();
            this.displacement = edsk.getDisplacement();
            this.org_id = edsk.getOrg_id();
            this.event_msg = edsk.getEvent_msg();

            this.patDaysBefore = edsk.getPatDaysBefore();
            this.patDaysAfter = edsk.getPatDaysAfter();
            this.usrDaysBefore = edsk.getUsrDaysBefore();
            this.usrDaysAfter = edsk.getUsrDaysAfter();
            this.patMsgBefore = edsk.getPatMsgBefore();
            this.patMsgAfter = edsk.getPatMsgAfter();
            this.usrMsgBefore = edsk.getUsrMsgBefore();
            this.usrMsgAfter = edsk.getUsrMsgAfter();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
            this.statusDate = edsk.getStatusDate();
            this.statusUser = edsk.getStatusUser();
            this.origCal = edsk.getOrigCal();
            this.durationUnit = edsk.getDurationUnit(); // SV, 9/14/04, handling
            // duration type.
            this.eventVisit = edsk.getEventVisit(); // SV, 9/29/04, handling
            // event' visit.
            this.cptCode = edsk.getCptCode();
            this.durationUnitAfter = edsk.getDurationUnitAfter();
            this.durationUnitBefore = edsk.getDurationUnitBefore();
            this.fuzzyAfter = edsk.getFuzzyAfter();
            this.eventCalType = EJBUtil.integerToString(edsk.getEventCalType());
            this.calAssoc = edsk.getCalAssocTo();
            this.calSchDate = edsk.getCalSchDate();
            this.eventCategory = edsk.getEventCategory();//KM

            //JM: 06May2008, added for, Enh. #PS16
            this.sequence = edsk.getEventSequence();
            this.eventFlag = edsk.getEventFlag();
            //KM-#4564
            this.eventLibType = edsk.getEventLibType();
            this.eventLineCategory = edsk.getEventLineCategory();

            this.eventSOSId = edsk.getEventSOSId();
            this.eventFacilityId = edsk.getEventFacilityId();
            this.eventCoverageType = edsk.getEventCoverageType();
            this.budgetTemplate = edsk.getBudgetTemplate();
            this.setCoverageNotes(edsk.getCoverageNotes());
            this.setStatCode(edsk.getStatCode());
        }
        return edsk;
    }

    /**
     * Calls setEventAssocDetails() of EventAssoc Session Bean:
     * EventAssocAgentBean
     *
     */

    public int setEventAssocDetails() {

        try {
            int output = 0;
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            // SV, 10/27/04,
            // this.setEvent_id(eventassocAgent.setEventAssocDetails(this.createEventAssocStateKeeper()));

            output = eventassocAgent.setEventAssocDetails(this
                    .createEventAssocStateKeeper());
            this.setEvent_id(output);
            Rlog.debug("eventassoc", "EventAssocJB.setEventAssocDetails()");

            return (output);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in setEventAssocDetails() in EventAssocJB " + e);
            return -2;
        }
    }

    /**
     * Calls updateEventAssoc() of EventAssoc Session Bean: EventAssocAgentBean
     *
     * @return
     */
    public int updateEventAssoc() {
        int output;
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgentRObj.updateEventAssoc(this
                    .createEventAssocStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls updateEventAssoc() of Eventdef Session Bean: EventdefAgentBean
     *
     * @return
     */
    public int propagateEventUpdates(String propagateInVisitFlag,
            String propagateInEventFlag) {
        int output;
        EventAssocBean esk;

        try {
            EventAssocDao eventAssocdao = new EventAssocDao();

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            esk = this.createEventAssocStateKeeper();
            // SV, 10/20/04, 'S' for study calendar, 'U' for update. We are
            // always updating event details.
            output = eventAssocdao.propagateEventUpdates(EJBUtil
                    .stringToNum(esk.getChain_id()), esk.getEvent_id(),
                    "EVENT_DEF", 0, propagateInVisitFlag, propagateInEventFlag,
                    "M", "S");
        } catch (Exception e) {
            Rlog.debug("eventassoc",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls updateEventAssoc() of Eventdef Session Bean: EventdefAgentBean
     *
     * @return
     */
    public int propagateEventMessage(String propagateInVisitFlag,
            String propagateInEventFlag) {
        int output;
        EventAssocBean esk;

        try {
            EventAssocDao eventAssocdao = new EventAssocDao();

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            esk = this.createEventAssocStateKeeper();
            // SV, 10/20/04, 'S' for study calendar, 'M' for update. We are
            // always updating event details.
            output = eventAssocdao.propagateEventUpdates(EJBUtil
                    .stringToNum(esk.getChain_id()), esk.getEvent_id(),
                    "EVENT_MESSAGE", 0, propagateInVisitFlag,
                    propagateInEventFlag, "M", "S");
        } catch (Exception e) {
            Rlog.debug("eventassoc",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls Delete EventAssoc() of EventAssoc Session Bean: EventAssocJB
     *
     * @return
     */

    public int removeEventAssocChild(int event_id) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgentRObj.removeEventAssocChild(event_id);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeEventAssocChilds(int event_ids[]) {
        int output;
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgentRObj.removeEventAssocChilds(event_ids);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "EXCEPTION IN DELETING THE CHILD NODES");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls Delete EventAssoc() of EventAssoc Session Bean: EventAssocJB
     *
     * @return
     */

    public int removeEventAssocHeader(int chain_id) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgentRObj.removeEventAssocHeader(chain_id);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeEventAssocHeaders(int chain_ids[]) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgentRObj.removeEventAssocHeaders(chain_ids);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "EXCEPTION IN DELETING THE CHILD NODE");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int AddProtToStudy(String protocol_id, String study_id,
            String userId, String ipAdd, String type) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.AddProtToStudy(protocol_id, study_id,
                    userId, ipAdd, type);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "inside EventAssocJB::AddProtToStudy");
            return -2;
        }
        return output;
    }

    public int createProtocolBudget(int protocol_id, int bgtTemplate, String bgtName,
            int userId, String ipAdd, int studyId) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.createProtocolBudget(protocol_id, bgtTemplate, bgtName,
                    userId, ipAdd, studyId);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "inside EventAssocJB::createProtocolBudget");
            return -2;
        }
        return output;
    }
    
    public int copyStudyProtocol(String protocolId, String protocolName,
            String userId, String ipAdd,String accId) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.copyStudyProtocol(protocolId,
                    protocolName, userId, ipAdd,accId);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "inside EventAssocJB::copyStudyProtocol");
            return -2;
        }
        return output;
    }
    
    public int createEventsAssoc(String[] eIdList, String[] vIdList, String[] seqList, 
    		String[] dispList, String[] hideList,
    		int userId, String ipAdd) {
    	int output = -1;
    	try {
            EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
            output = eventassocAgent.createEventsAssoc(eIdList, vIdList, seqList,
            		dispList, hideList,
            		userId, ipAdd);
    	} catch (Exception e) {
            Rlog.fatal("eventassoc", "inside EventAssocJB::createEventsAssoc");
        }
    	return output;
    }

//  JM: 04May2007, active protocol calendars only
    public EventAssocDao getStudyProtsActive(int study_id) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getStudyProtsActive(study_id);
            Rlog.debug("eventassoc", "EventAssocJB.getStudyProtsActive after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getStudyProtsActive(int userId) in EventAssocJB " + e);
        }
        return eventassocDao;
    }


    public EventAssocDao getStudyProts(int study_id) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getStudyProts(study_id);
            Rlog.debug("eventassoc", "EventAssocJB.getStudyProts after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getByUserId(int userId) in EventAssocJB " + e);
        }
        return eventassocDao;
    }

    public EventAssocDao getStudyAdminProts(int study_id,String status) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getStudyAdminProts(study_id,status);
            Rlog.debug("eventassoc", "EventAssocJB.getStudyProts after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getByUserId(int userId) in EventAssocJB " + e);
        }
        return eventassocDao;
    }

    public EventAssocDao getStudyProts(int study_id, String status) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getStudyProts(study_id, status);
            Rlog
                    .debug("eventassoc",
                            "EventAssocJB.getStudyProts (int study_id, String status) after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in EventAssocJB.getStudyProts(int study_id, String status) "
                            + e);
        }
        return eventassocDao;
    }

    public EventAssocDao getStudyProts(int study_id, int budgetId) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getStudyProts(study_id,
                    budgetId);
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getByUserId(int userId) in EventAssocJB " + e);
        }
        return eventassocDao;
    }

    /**
     *
     *
     * @return the EventAssoc StateKeeper Object with the current values of the
     *         Bean
     */

    public EventAssocBean createEventAssocStateKeeper() {

        return new EventAssocBean(event_id, chain_id, org_id, event_type, name,
                notes, cost, cost_description, duration, user_id, linked_uri,
                fuzzy_period, msg_to, 
                description, displacement,
                event_msg, patDaysBefore, patDaysAfter, usrDaysBefore,
                usrDaysAfter, patMsgBefore, patMsgAfter, usrMsgBefore,
                usrMsgAfter, creator, modifiedBy, ipAdd, statusDate,
                statusUser, origCal, durationUnit, eventVisit, cptCode,
                durationUnitAfter, fuzzyAfter, durationUnitBefore,
                eventCalType, calAssoc, calSchDate,eventCategory,sequence, eventFlag, eventLibType,eventLineCategory,
                eventSOSId, eventFacilityId, eventCoverageType, budgetTemplate, this.getCoverageNotes(),statCode, eventHideFlag);
    }

    public int DeleteProtocolEvents(String chain_id) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.DeleteProtocolEvents(chain_id);
        } catch (Exception e) {
            System.out.println("inside EventAssocJB::CopyProtocol");
            return -1;
        }
        return output;

    }

    // SV, 8/19/04, part of the fix for bug #1602,
    public int DeleteProtocolEventsPastDuration(String chain_id, int duration) {
        int output;
        try {

            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.DeleteProtocolEventsPastDuration(chain_id,
                    duration);
        } catch (Exception e) {
            System.out
                    .println("inside EventAssocJB::DeleteProtocol Past Duration");
            return -1;
        }
        return output;

    }

    /**
     * Delete calendar associated with a study
     *
     * @param studyId
     * @param calendarId
     * @returns 0:successful, -1:error, -2 if patients enrolled in the calendar,
     *          -3 if budget linked to the calendar
     */
    public int studyCalendarDelete(int studyId, int calendarId) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.studyCalendarDelete(studyId, calendarId);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "ERROR in studyCalendarDelete inside EventAssocJB " + e);
            return -1;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int studyCalendarDelete(int studyId, int calendarId,Hashtable<String, String> argsvalues) {
        int output;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.studyCalendarDelete(studyId, calendarId, argsvalues);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "ERROR in studyCalendarDelete inside EventAssocJB " + e);
            return -1;
        }
        return output;
    }

    public void AddEventsToProtocol(String chain_id, String[][] events_disps,
            int fromDisp, int toDisp, String userId, String ipAdd) {
        try {
            EventAssocDao eventassocdao = new EventAssocDao();

            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            eventassocAgent.AddEventsToProtocol(chain_id, events_disps,
                    fromDisp, toDisp, userId, ipAdd);
            eventassocdao.UpdateEventVisitIds(chain_id, userId, ipAdd); // SV,
            // 10/27/04,
            // update
            // the
            // visit
            // id's
            // of
            // assoc
            // events

        } catch (Exception e) {
            Rlog
                    .fatal("eventassoc",
                            "inside EventAssocJB::addeventstoprotocol");
        }

    }

    public EventAssocDao getAllProtAndEvents(int event_id) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getAllProtAndEvents(event_id);
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAllProtAndEvents in EventdefJB " + e);
        }
        return eventassocDao;
    }

    //Modified by Manimaran to fix the issue #3394
    public EventAssocDao getProtSelectedEvents(int protocolId, String orderType, String orderBy) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj
                    .getProtSelectedEvents(protocolId,orderType,orderBy);
            Rlog
                    .debug("eventassoc",
                            "EventdefJB.getAllProtAndEvents after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAllProtAndEvents in EventdefJB " + e);
        }
        return eventassocDao;
    }

    public EventAssocDao getFilteredEvents(int user_id, int protocol_id) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getFilteredEvents(user_id,
                    protocol_id);
            Rlog.debug("eventassoc", "EventdefJB.getByUserId after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getByUserId(int userId) in EventdefJB " + e);
        }
        return eventassocDao;
    }

    public EventAssocDao getPrevStatus(int protocolId) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getPrevStatus(protocolId);
            Rlog
                    .debug("eventassoc",
                            "EventdefJB.getAllProtAndEvents after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAllProtAndEvents in EventdefJB " + e);
        }
        return eventassocDao;
    }

    public EventAssocDao getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getFilteredRangeEvents(
                    protocolId, beginDisp, endDisp);
            Rlog.debug("eventassoc",
                    "EventAssocJB.getFilteredRangeEvents after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getFilteredRangeEvents in EventAssocJB " + e);
        }
        return eventassocDao;
    }

    public int RemoveProtEventInstance(int eventId, String whichTable) {
        int output = 0;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            output = eventassocAgent.RemoveProtEventInstance(eventId,
                    whichTable);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in RemoveProtEventInstance(event_id, whichTable)  in EventAssocJB "
                            + e);
            return -1;
        }
        return output;

    }

    public EventAssocDao getProtRowEvents(int protocolId, int rownum,
            int beginDisp, int endDisp) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.getProtRowEvents(protocolId,
                    rownum, beginDisp, endDisp);
            Rlog.debug("eventassoc", "EventAssocJB.getProtRowEvents after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getProtRowEvents in EventAssocJB " + e);
        }
        return eventassocDao;
    }

    public EventAssocDao fetchCalTemplate(String protocolId) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj.fetchCalTemplate(protocolId);
            Rlog.debug("eventassoc", "EventAssocJB.fetchCalTemplate after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in fetchCalTemplate in EventAssocJB " + e);
        }
        return eventassocDao;
    }

    public EventInfoDao getAlertNotifyValues(int patprotId) {
        EventInfoDao eventinfoDao = new EventInfoDao();
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventinfoDao = eventassocAgentRObj.getAlertNotifyValues(patprotId);
            Rlog.debug("eventdef", "EventdefJB.getByUserId after Dao");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getEventDefInfo(int eventId) in EventdefJB " + e);
        }
        return eventinfoDao;
    }

    public int getMaxCost(String chainId) {
        int cost = 0;
        try {
            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            cost = eventassocAgent.getMaxCost(chainId);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "EventAssocJB.GetMaxCost(chainId) " + e);
            return -2;
        }
        return cost;

    }

    public EventAssocDao getStudyProtsWithAlnotStat(int study_id) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj
                    .getStudyProtsWithAlnotStat(study_id);
            Rlog.debug("eventassoc",
                    "EventAssocJB.getStudyProtsWithAlnotStat after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getStudyProtsWithAlnotStat in EventAssocJB " + e);
        }
        return eventassocDao;
    }

    /*
     * generates a String of XML for the eventassoc details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("eventassoc", "EventAssocJB.toXML()");
        return "test";
        // to be implemented later
        /*
         * return new String("<?xml version=\"1.0\"?>" + "<?cocoon-process
         * type=\"xslt\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<head>" + "</head>" + "<input
         * type=\"hidden\" name=\"eventassocId\" value=\"" +
         * this.getEventAssocId()+ "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"eventassocCodelstJobtype\" value=\"" +
         * this.getEventAssocCodelstJobtype() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventassocAccountId\" value=\"" +
         * this.getEventAssocAccountId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventassocSiteId\" value=\"" +
         * this.getEventAssocSiteId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventassocPerAddressId\" value=\"" +
         * this.getEventAssocPerAddressId() + "\" size=\"38\"/>" + "<input
         * type=\"text\" name=\"eventassocLastName\" value=\"" +
         * this.getEventAssocLastName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventassocFirstName\" value=\"" +
         * this.getEventAssocFirstName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventassocMidName \" value=\"" +
         * this.getEventAssocMidName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventassocWrkExp\" value=\"" +
         * this.getEventAssocWrkExp() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventassocPhaseInv\" value=\"" +
         * this.getEventAssocPhaseInv() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventassocSessionTime\" value=\"" +
         * this.getEventAssocSessionTime() + "\" size=\"3\"/>" + "<input
         * type=\"text\" name=\"eventassocLoginName\" value=\"" +
         * this.getEventAssocLoginName() + "\" size=\"20\"/>" + "<input
         * type=\"text\" name=\"eventassocPwd\" value=\"" +
         * this.getEventAssocPwd() + "\" size=\"20\"/>" + "<input type=\"text\"
         * name=\"eventassocSecQues\" value=\"" + this.getEventAssocSecQues() +
         * "\" size=\"150\"/>" + "<input type=\"text\"
         * name=\"eventassocAnswer\" value=\"" + this.getEventAssocAnswer() +
         * "\" size=\"150\"/>" + "<input type=\"text\"
         * name=\"eventassocStatus\" value=\"" + this.getEventAssocStatus() +
         * "\" size=\"1\"/>" + "<input type=\"text\"
         * name=\"eventassocCodelstSpl\" value=\"" +
         * this.getEventAssocCodelstSpl() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventassocGrpDefault\" value=\"" +
         * this.getEventAssocGrpDefault() + "\" size=\"10\"/>" + "</form>" );
         */

    }// end of method

    // ///////////////////////////////////////////////////////////////////
    // /// Changes Made for enhancements Phase II
    // / By Sonia Kaura
    // Date : 11/3/2003

    public EventAssocDao getAllProtSelectedEvents(int protocolId, String visitId, String evtName) {
        EventAssocDao eventassocDao = new EventAssocDao();
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj
                    .getAllProtSelectedEvents(protocolId, visitId, evtName);
            Rlog.debug("eventassoc",
                    "EventdefJB.getAllProtSelectedEvents after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAllProtSelectedEvents in EventdefJB " + e);
        }
        return eventassocDao;
    }
        //YK: FOR PCAL-20461
    public EventAssocDao getAllProtSelectedVisitsEvents(int protocolId, String visitId, String evtName) {
    	EventAssocDao eventassocDao = new EventAssocDao();
    	try {
    		EventAssocAgentRObj eventassocAgentRObj = EJBUtil
    		.getEventAssocAgentHome();
    		eventassocDao = eventassocAgentRObj
    		.getAllProtSelectedVisitsEvents(protocolId, visitId, evtName);
    		Rlog.debug("eventassoc",
    				"EventdefJB.getAllProtSelectedVisitsEvents after Dao");
    		return eventassocDao;
    	} catch (Exception e) {
    		Rlog.fatal("eventassoc",
    				"Error in getAllProtSelectedVisitsEvents in EventdefJB " + e);
    	}
    	return eventassocDao;
    }

    public EventAssocDao getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight) {
        EventAssocDao eventassocDao = null;
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj
                    .getProtSelectedAndGroupedEvents(protocolId, isExport, finDetRight);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "In EventAssocJB.getProtSelectedAndGroupedEvents: "+e);
            eventassocDao = new EventAssocDao();
        }
        return eventassocDao;
    }
    
    /*YK :: DFIN12 FIX FOR BUG#5954-- RESOLVES  #5947 #5943 BUGS ALSO*/
    public EventAssocDao getEventVisitMileStoneGrid(int protocolId) {
    	EventAssocDao eventassocDao = null;
    	try {
    		EventAssocAgentRObj eventassocAgentRObj = EJBUtil
    		.getEventAssocAgentHome();
    		eventassocDao = eventassocAgentRObj
    		.getEventVisitMileStoneGrid(protocolId);
    	} catch (Exception e) {
    		Rlog.fatal("eventassoc",
    				"In EventAssocJB.getEventVisitMileStoneGrid: "+e);
    		eventassocDao = new EventAssocDao();
    	}
    	return eventassocDao;
    }
    
    /* Sonia Abrol, 10/30/06*/
    public int refreshCalendarMessages(String protocolId, String studyId, String modifiedBy,
            String ipAdd)
    {

        int success = 0;
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            success = eventassocAgentRObj.refreshCalendarMessages(protocolId, studyId, modifiedBy,
                            ipAdd);
            Rlog.debug("eventassoc",
                    "EventAssocJB.refreshCalendarMessages after Dao");
            return success;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in refreshCalendarMessages in EventAssocJB " + e);
        }
        return success;
    }

    /**
     *
     * @param protocolId
     * @return EventAssocDao
     */
    public EventAssocDao getProtVisitAndEvents(int protocolId, String search, String resetSort) { /*YK 04Jan- SUT_1_Requirement_18489*/
        EventAssocDao eventassocDao = new EventAssocDao();
        try {

            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj
                    .getProtVisitAndEvents(protocolId, search, resetSort); /*YK 04Jan- SUT_1_Requirement_18489*/
            Rlog
                    .debug("eventassoc",
                            "EventdefJB.getProtVisitAndEvents after Dao");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getProtVisitAndEvents() in EventAssocJB " + e);
        }
        return eventassocDao;
    }



    //JM: 10April2008: #C8
    public int getMaxSeqForVisitEvents(int visitId, String  eventName, String tableName) {
        int ret=0;
        try {
        	EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
            ret = eventassocAgent.getMaxSeqForVisitEvents(visitId, eventName, tableName);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "Error in getMaxSeqForVisitEvents() in EventAssocJB" + e);
            return -2;
        }
        return ret;
    }


    //JM: 17April2008: #C8: copy event details
    public int CopyEventDetails(String old_event_id, String new_event_id, String crfInfo, String userId, String ipAdd) {
        int ret=0;
        try {
        	EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
        	eventassocAgent.CopyEventDetails(old_event_id,  new_event_id,  crfInfo,  userId,  ipAdd);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "Error in CopyEventDetails() in EventAssocJB" + e);
            return -2;
        }
        return ret;
    }


  //JM: 23Apr2008, added for, Enh. #C9
    public void updateOfflnFlgForEventsVisits(int event_id, String offlineFlag) {
        try {

            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            eventassocAgent.updateOfflnFlgForEventsVisits(event_id, offlineFlag);

        } catch (Exception e) {
            Rlog
                    .fatal("eventassoc",
                            "inside EventAssocJB::updateOfflnFlgForEventsVisits()");
        }

    }

  //JM: 25Apr2008, added for, Enh. #C9
    public int hideUnhideEventsVisits(String[] eventIds, String hide_flag, String[] flags) {

    	int output = 0;
        try {

        	EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
            output = eventassocAgent.hideUnhideEventsVisits(eventIds, hide_flag, flags );
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in hideUnhideEventsVisits()  in EventdefJB "
                            + e);
            return -1;
        }
        return output;


    }

  //JM: 28Apr2008, added for, Enh. #C9
    public void updatePatientSchedulesNow(int calId, int checkedVal, int statusId, java.sql.Date selDate, int userId, String ipAdd ) {
        try {

            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            eventassocAgent.updatePatientSchedulesNow(calId, checkedVal,statusId, selDate, userId, ipAdd );

        } catch (Exception e) {
            Rlog
                    .fatal("eventassoc",
                            "inside EventAssocJB::updatePatientSchedulesNow()" + e);
        }

    }


  //JM: 06May2008, added for, Enh. #PS16




    //public void updateUnscheduledEvents(String chain_id, String[][] events_disps,int fromDisp, int toDisp, int seqMax, String userId, String ipAdd) {


    public  String[] updateUnscheduledEvents(String chain_id, String[][] events_disps,int fromDisp, int toDisp, int seqMax, String userId, String ipAdd) {

    	String[] strArrEventIds = new String[1000];
    	try {

            EventAssocAgentRObj eventassocAgent = EJBUtil
                    .getEventAssocAgentHome();
            strArrEventIds   = eventassocAgent.updateUnscheduledEvents(chain_id, events_disps,fromDisp, toDisp, seqMax, userId, ipAdd);


            //return strArrEventIds;

        } catch (Exception e) {
            Rlog
                    .fatal("eventassoc",
                            "inside EventAssocJB::updateUnscheduledEvents()"+e);
        }

        return strArrEventIds;

    }


    //JM: 07May2008, added for, Enh. #PS16
    public void updateUnscheduledEvents1(int patId, int patProtId,  int calId, int userId, String ipAdd, String[] eventsArr   ){
    	try{

    		EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
    		eventassocAgent.updateUnscheduledEvents1(patId, patProtId, calId, userId, ipAdd, eventsArr);

    	} catch (Exception e) {
    		 Rlog
             .fatal("eventassoc","inside EventAssocJB::updateUnscheduledEvents1()" + e);
            return;
        }

    }

    public  Hashtable getVisitEventForScheduledEvent(String schevents1_pk)
    {
    	Hashtable htRet = new Hashtable();
    	try{

    		EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
    		htRet = eventassocAgent.getVisitEventForScheduledEvent(schevents1_pk);

    	} catch (Exception e) {
    		 Rlog
             .fatal("eventassoc","inside EventAssocJB::getVisitEventForScheduledEvent()" + e);

        }
    	return htRet;

    }


//    JM: 16July2008, added for the issue #3586
    
 // Overloaded for INF-18183 ::: Akshi
	public int deleteEventStatusHistory(int eventStatusHistId, Hashtable<String, String> args){

		int retme = 0;

		try {


			EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
            retme = eventassocAgent.deleteEventStatusHistory(eventStatusHistId,args);

        } catch (Exception e) {
            Rlog.fatal("eventassoc","inside EventAssocJB::deleteEventStatusHistory()" + e);
             return -2;
        }
        return retme;

	}
	
	// Overloaded for INF-18183 ::: Akshi
	public int deleteEventStatusHistory(int eventStatusHistId){

		int retme = 0;

		try {


			EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
            retme = eventassocAgent.deleteEventStatusHistory(eventStatusHistId);

        } catch (Exception e) {
            Rlog.fatal("eventassoc","inside EventAssocJB::deleteEventStatusHistory()" + e);
             return -2;
        }
        return retme;

	}

	public void updateCurrentStatus(int eventId){

		try {

			EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
            eventassocAgent.updateCurrentStatus(eventId);

        } catch (Exception e) {
            Rlog.fatal("eventassoc","inside EventAssocJB::updateCurrentStatus()" + e);
             return ;
        }

	}
	
	public ArrayList sortListByKeyword(ArrayList inList, String key) {
	    Collections.sort(inList, new KeywordComparator(key));
	    return inList;
	}
	
	private class KeywordComparator implements Comparator {
	    String key = null;
	    private KeywordComparator(String key) { this.key = key; }
        public int compare(Object o1, Object o2) {
            if (!(o1 instanceof HashMap) || !(o2 instanceof HashMap)) { return 0; }
            if (key == null || key.length() == 0) { return 0; }
            String s1 = (String)((HashMap)o1).get(key);
            String s2 = (String)((HashMap)o2).get(key);
            if (s1 == null || s2 == null) { return 0; }
            return s1.compareToIgnoreCase(s2); //Ashu:Fixed BUG#5740,5741 3rdMarch11
        }
	}
	
	 //Yk: Added for Enhancement :PCAL-19743 For Descending Sorting
	public ArrayList sortListByKeywordDesc(ArrayList inList, String key) {
	    Collections.sort(inList, new KeywordComparatorDesc(key));
	    return inList;
	}
	 //Yk: Added for Enhancement :PCAL-19743  For Descending Sorting 
	private class KeywordComparatorDesc implements Comparator {
	    String key = null;
	    private KeywordComparatorDesc(String key) { this.key = key; }
        public int compare(Object o1, Object o2) {
            if (!(o1 instanceof HashMap) || !(o2 instanceof HashMap)) { return 0; }
            if (key == null || key.length() == 0) { return 0; }
            String s1 = (String)((HashMap)o1).get(key);
            String s2 = (String)((HashMap)o2).get(key);
            if (s1 == null || s2 == null) { return 0; }
            return s2.compareToIgnoreCase(s1); 
        }
	}
	
//JM: 08Dec2010: PP-1
	public int getEventFormAccees(int crfLinkedForm, int assocId, int portalId){

		int retme = 0;

		try {


			EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
            retme = eventassocAgent.getEventFormAccees(crfLinkedForm, assocId, portalId);

        } catch (Exception e) {
            Rlog.fatal("eventassoc","inside EventAssocJB::getEventFormAccees()" + e);
             return -2;
        }
        return retme;

	}
	
	/**
     * Created by : Ankit Kumar
     * Created On : 07/14/2011 
     * Enhancement: FIN-CostType-1
     * Purpose    : Identify the role assigned to the user/team on study.
     * @return    : roleCodePk;
     */
    public  String getStudyUserRole(String studyId, String userId){
    	String roleCodePk = "";
		try {

			EventAssocAgentRObj eventassocAgent = EJBUtil.getEventAssocAgentHome();
			roleCodePk = eventassocAgent.getStudyUserRole(studyId, userId);

        } catch (Exception e) {
            Rlog.fatal("eventassoc","inside EventAssocJB::getStudyUserRole()" + e);
             return roleCodePk;
        }
        return roleCodePk;
    }
 //Yk: Added for Enhancement :PCAL-19743
    public EventAssocDao getProtSelectedAndVisitEvents(int protocolId) {
        EventAssocDao eventassocDao = null;
        try {
            EventAssocAgentRObj eventassocAgentRObj = EJBUtil
                    .getEventAssocAgentHome();
            eventassocDao = eventassocAgentRObj
                    .getProtSelectedAndVisitEvents(protocolId);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "In EventAssocJB.getProtSelectedAndVisitEvents: "+e);
            eventassocDao = new EventAssocDao();
        }
        return eventassocDao;
    }



}// end of class
