/*
 * Classname : CrfStatAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 11/26/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.crfStatAgent;

/* Import Statements */

import javax.ejb.Remote;

import com.velos.esch.business.common.CrfStatDao;
import com.velos.esch.business.crfStat.impl.CrfStatBean;

// import com.velos.esch.business.common.EventAssocDao;
@Remote
public interface CrfStatAgentRObj {

    CrfStatBean getCrfStatDetails(int crfStatId);

    public int setCrfStatDetails(CrfStatBean account);

    public int updateCrfStat(CrfStatBean usk);

    public CrfStatDao getCrfValues(int patProtId, int visit);

}