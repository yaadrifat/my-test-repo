/*
 * Classname : EventStatAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 08/27/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.service.eventstatAgent;

/* Import Statements */
import javax.ejb.Remote;


import com.velos.esch.business.eventstat.impl.EventStatBean;
//import com.velos.esch.business.common.EventStatDao;

/* End of Import Statements */
@Remote
public interface EventStatAgentRObj {

	EventStatBean getEventStatDetails(int evtStatId);

    public int setEventStatDetails(EventStatBean estsk);

    //public int removeEventStat(int eventId);

    public int updateEventStat(EventStatBean estsk) ;


}