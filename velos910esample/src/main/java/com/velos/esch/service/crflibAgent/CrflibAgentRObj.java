/*
 * Classname : CrflibAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 01/31/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh
 */

package com.velos.esch.service.crflibAgent;

/* Import Statements */
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.esch.business.common.CrflibDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.crflib.impl.CrflibBean;

@Remote
public interface CrflibAgentRObj {

    CrflibBean getCrflibDetails(int crflibId);

    public int setCrflibDetails(CrflibBean crflib);

    public int updateCrflib(CrflibBean usk);

    public EventInfoDao getEventCrfs(int eventId);

 // Overloaded for INF-18183 ::: Akshi
    public int removeCrflib(int crflibId,Hashtable<String, String> args);
    
    public int removeCrflib(int crflibId);

    public CrflibDao insertCrfNamesNums(CrflibDao crflibDao); // SV, 10/21/04
}