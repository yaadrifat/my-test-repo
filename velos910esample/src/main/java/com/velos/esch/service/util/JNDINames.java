/*
 * $Id: JNDINames.java,v 1.9 2012/03/14 19:00:02 smhalagi Exp $
 * Copyright 2000 Velos, Inc. All rights reserved.
 */

package com.velos.esch.service.util;

/**
 * This class is to be used as a central repository to store the Internal JNDI
 * names of various components. Any sort of change at this location should also
 * reflect in the Deployment Descriptor
 */

public interface JNDINames {
	
	public final static String prefixEsch = "velos/";
	public final static String suffix = "/remote";

	public final static String AdvEveAgentHome = prefixEsch+"AdvEveAgentBean"+suffix;
	public final static String AdvInfoAgentHome = prefixEsch+"AdvInfoAgentBean"+suffix;
	public final static String AdvNotifyAgentHome = prefixEsch+"AdvNotifyAgentBean"+suffix;
	public final static String AlertNotifyAgentHome = prefixEsch+"AlertNotifyAgentBean"+suffix;
	public final static String BgtApndxAgentHome = prefixEsch+"BgtApndxAgentBean"+suffix;
	public final static String BgtSectionAgentHome = prefixEsch+"BgtSectionAgentBean"+suffix;
	public final static String BudgetAgentHome = prefixEsch+"BudgetAgentBean"+suffix;
	public final static String BudgetcalAgentHome = prefixEsch+"BudgetcalAgentBean"+suffix;
	public final static String BudgetUsersAgentHome = prefixEsch+"BudgetUsersAgentBean"+suffix;
	public final static String CrfAgentHome = prefixEsch+"CrfAgentBean"+suffix;
	public final static String CrflibAgentHome = prefixEsch+"CrflibAgentBean"+suffix;
	public final static String CrfNotifyAgentHome = prefixEsch+"CrfNotifyAgentBean"+suffix;
	public final static String CrfStatAgentHome = prefixEsch+"CrfStatAgentBean"+suffix;
	public final static String DocrefAgentHome = prefixEsch+"DocrefAgentBean"+suffix;
	public final static String EventAssocAgentHome = prefixEsch+"EventAssocAgentBean"+suffix;
	public final static String EventcostAgentHome = prefixEsch+"EventcostAgentBean"+suffix;
	public final static String EventCrfAgentHome = prefixEsch+"EventCrfAgentBean"+suffix;
	public final static String EventdefAgentHome = prefixEsch+"EventdefAgentBean"+suffix;
	public final static String EventdocAgentHome = prefixEsch+"EventdocAgentBean"+suffix;
	public final static String EventKitAgentHome = prefixEsch+"EventKitAgentBean"+suffix;
	public final static String EventResourceAgentHome = prefixEsch+"EventResourceAgentBean"+suffix;
	public final static String EventStatAgentHome = prefixEsch+"EventStatAgentBean"+suffix;
	public final static String EventuserAgentHome = prefixEsch+"EventuserAgentBean"+suffix;
	public final static String LineitemAgentHome = prefixEsch+"LineitemAgentBean"+suffix;
	public final static String ProtVisitAgentHome = prefixEsch+"ProtVisitAgentBean"+suffix;
	public final static String PortalFormsAgentHome = prefixEsch+"PortalFormsAgentBean"+suffix;
	public final static String SubCostItemAgentHome = prefixEsch+"SubCostItemAgentBean"+suffix;
	public final static String SubCostItemVisitAgentHome = prefixEsch+"SubCostItemVisitAgentBean"+suffix;
	public final static String AuditRowEschAgentHome = prefixEsch+"AuditRowEschAgentBean"+suffix;


    /* JNDI names of EJB home objects */
    public static final String EVENTDEFAGENT_EJBHOME = "EventdefAgentBean";

    public static final String EVENTDEF_EJBHOME = "EventdefBean";

    // public static final String SCHEDULEAGENT_EJBHOME = "ScheduleAgentBean";
    // public static final String SCHEDULE_EJBHOME = "ScheduleBean";
    public static final String EVENTCOSTAGENT_EJBHOME = "EventcostAgentBean";

    public static final String EVENTCOST_EJBHOME = "EventcostBean";

    public static final String EVENTASSOCAGENT_EJBHOME = "EventAssocAgentBean";

    public static final String EVENTASSOC_EJBHOME = "EventAssocBean";

    public static final String EVENTUSERAGENT_EJBHOME = "EventuserAgentBean";

    public static final String EVENTUSER_EJBHOME = "EventuserBean";

    public static final String EVENTDOCAGENT_EJBHOME = "EventdocAgentBean";

    public static final String EVENTDOC_EJBHOME = "EventdocBean";

    public static final String CRFAGENT_EJBHOME = "CrfAgentBean";

    public static final String CRF_EJBHOME = "CrfBean";

    public static final String ADVEVEAGENT_EJBHOME = "AdvEveAgentBean";

    public static final String ADVEVE_EJBHOME = "AdvEveBean";

    public static final String ADVINFOAGENT_EJBHOME = "AdvInfoAgentBean";

    public static final String ADVINFO_EJBHOME = "AdvInfoBean";

    public static final String ADVNOTIFYAGENT_EJBHOME = "AdvNotifyAgentBean";

    public static final String ADVNOTIFY_EJBHOME = "AdvNotifyBean";

    public static final String CRFNOTIFYAGENT_EJBHOME = "CrfNotifyAgentBean";

    public static final String CRFNOTIFY_EJBHOME = "CrfNotifyBean";

    public static final String ALERTNOTIFYAGENT_EJBHOME = "AlertNotifyAgentBean";

    public static final String ALERTNOTIFY_EJBHOME = "AlertNotifyBean";

    public static final String CRFSTATAGENT_EJBHOME = "CrfStatAgentBean";

    public static final String CRFSTAT_EJBHOME = "CrfStatBean";

    public static final String CRFLIBAGENT_EJBHOME = "CrflibAgentBean";

    public static final String CRFLIB_EJBHOME = "CrflibBean";

    public static final String BUDGETAGENT_EJBHOME = "BudgetAgentBean";

    public static final String BUDGET_EJBHOME = "BudgetBean";

    public static final String BUDGETCALAGENT_EJBHOME = "BudgetcalAgentBean";

    public static final String BUDGETCAL_EJBHOME = "BudgetcalBean";

    public static final String BUDGETUSERSAGENT_EJBHOME = "BudgetUsersAgentBean";

    public static final String BUDGETUSERS_EJBHOME = "BudgetUsersBean";

    public static final String BGTSECTIONAGENT_EJBHOME = "BgtSectionAgentBean";

    public static final String BGTSECTION_EJBHOME = "BgtSectionBean";

    public static final String BGTAPNDXAGENT_EJBHOME = "BgtApndxAgentBean";

    public static final String BGTAPNDX_EJBHOME = "BgtApndxBean";

    public static final String DOCREF_EJBHOME = "DocrefBean";

    public static final String ESCHEDULING_DATASOURCE = "java:comp/env/esch";

    public static final String LINEITEMAGENT_EJBHOME = "LineitemAgentBean";

    public static final String LINEITEM_EJBHOME = "LineitemBean";

    public static final String PROTVISIT_EJBHOME = "ProtVisitBean"; // SV,

    // 9/22/2004

    public static final String PROTVISITAGENT_EJBHOME = "ProtVisitAgentBean"; // SV,
    // 9/22/2004

//  Added by Manimaran on 26,June2006
    public static final String EVENTCRFAGENT_EJBHOME = "EventCrfAgentBean";


    //JM: 15Feb2008
    public static final String EVENTRESOURCEAGENT_EJBHOME = "EventResourceAgentBean";

    public static final String EVENTRESOURCE_EJBHOME = "EventResourceBean";

    public static final String EVENTSTAT_EJBHOME = "EventStatBean"; //JM: 28Aug2008

    public static final String PORTALFORMSAGENT_EJBHOME = "PortalFormsBean"; //JM: 03DEC2010
}