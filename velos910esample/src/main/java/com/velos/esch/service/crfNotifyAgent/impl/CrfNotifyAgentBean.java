/*
 * Classname : CrfNotifyAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/27/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.crfNotifyAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.common.CrfNotifyDao;
import com.velos.esch.business.crfNotify.impl.CrfNotifyBean;
import com.velos.esch.service.crfNotifyAgent.CrfNotifyAgentRObj;
import com.velos.esch.service.util.Rlog;

// import com.velos.esch.business.eventdef.*;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * CrfNotifyBean.<br>
 * <br>
 * 
 */
@Stateless
public class CrfNotifyAgentBean implements CrfNotifyAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /**
     * Calls getCrfNotifyDetails() on CrfNotify Entity Bean CrfNotifyBean. Looks
     * up on the CrfNotify id parameter passed in and constructs and returns a
     * Crf state holder. containing all the summary details of the CrfNotify<br>
     * 
     * @param crfId
     *            the CrfNotify id
     * @ee CrfNotifyBean
     */
    public CrfNotifyBean getCrfNotifyDetails(int crfNotifyId) {

        try {
            return (CrfNotifyBean) em.find(CrfNotifyBean.class, new Integer(
                    crfNotifyId));
        } catch (Exception e) {
            System.out
                    .print("Error in getCrfNotifyDetails() in CrfNotifyAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setCrfNotifyDetails() on CrfNotify Entity Bean
     * crfNotifyBean.Creates a new CrfNotify with the values in the StateKeeper
     * object.
     * 
     * @param crf
     *            a crfNotify state keeper containing crfNotify attributes for
     *            the new CrfNotify.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setCrfNotifyDetails(CrfNotifyBean edsk) {
        try {
            em.merge(edsk);
            CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
            crfNotifyDao.copyCrfNotifyDetails(edsk);
            return (edsk.getCrfNotifyId());
        } catch (Exception e) {
            System.out
                    .print("Error in setCrfNotifyDetails() in CrfNotifyAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateCrfNotify(CrfNotifyBean edsk) {

        CrfNotifyBean retrieved = null; // CrfNotify Entity Bean Remote Object
        int output;

        try {

            retrieved = (CrfNotifyBean) em.find(CrfNotifyBean.class,
                    new Integer(edsk.getCrfNotifyId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateCrfNotify(edsk);

        } catch (Exception e) {
            Rlog.debug("crfNotify",
                    "Error in updateCrfNotify in CrfNotifyAgentBean" + e);
            return -2;
        }
        return output;
    }

    public CrfNotifyDao getCrfNotifyValues(int patProtId) {
        try {
            Rlog
                    .debug("crfNotify",
                            "In getCrfNotifyValues in CrfNotifyAgentBean line number 0");
            CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
            Rlog
                    .debug("crfNotify",
                            "In getCrfNotifyValues in CrfNotifyAgentBean line number 1");
            crfNotifyDao.getCrfNotifyValues(patProtId);
            Rlog
                    .debug("crfNotify",
                            "In getCrfNotifyValues in CrfNotifyAgentBean line number 2");
            return crfNotifyDao;
        } catch (Exception e) {
            Rlog.fatal("crfNotify",
                    "Exception In getCrfNotifyValues in CrfNotifyAgentBean "
                            + e);
        }
        return null;

    }

    public CrfNotifyDao getCrfNotifyValues(int FK_STUDY, int FK_PROTOCOL) {
        try {
            Rlog
                    .debug("crfNotify",
                            "In getCrfNotifyValues in CrfNotifyAgentBean line number 0");
            CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
            Rlog
                    .debug("crfNotify",
                            "In getCrfNotifyValues in CrfNotifyAgentBean line number 1");
            crfNotifyDao.getCrfNotifyValues(FK_STUDY, FK_PROTOCOL);
            Rlog
                    .debug("crfNotify",
                            "In getCrfNotifyValues in CrfNotifyAgentBean line number 2");
            return crfNotifyDao;
        } catch (Exception e) {
            Rlog.fatal("crfNotify",
                    "Exception In getCrfNotifyValues in CrfNotifyAgentBean "
                            + e);
        }
        return null;

    }

}// end of class
