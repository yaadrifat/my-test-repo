package com.velos.esch.service.util;

import com.velos.esch.service.util.Rlog;

public class StringUtil {
	
    static int cReturn;
    static float cFloatReturn;

    public static int stringToNum(String str) {
    	int iReturn = 0;
    	try {
    		if ((str == null) || str.equals("null")) {
    			return cReturn;
    		} else if (str.equals("")) {
    			return cReturn;
    		} else {
    			iReturn = Integer.parseInt(str);
    			return iReturn;
    		}
    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO NUMBER:}" + e);
    		return iReturn;
    	}
    }

    public static float stringToFloat(String str) {
    	float iReturn = 0;
    	try {

    		if (str == null) {
    			return cFloatReturn;
    		} else if (str == "") {
    			return cFloatReturn;
    		} else {
    			iReturn = Float.parseFloat(str);
    			return iReturn;
    		}
    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO FLOAT:}" + e);
    		return iReturn;
    	}
    }
    
    public static String floatToString(Float i) {
        String returnString = null;
        try {
            if (i == null) {
                return returnString;
            }
            returnString = i.toString();
            return returnString;
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
            return returnString;
        }
    }
    
    public static String integerToString(Integer i) {

    	String returnString = null;
    	try {
    		if (i == null) {
    			return returnString;
    		}

    		returnString = i.toString();
    		return returnString;

    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
    		return returnString;
    	}

    }

    public static Integer stringToInteger(String str) {
    	Integer returnInteger = null;

    	// Rlog.debug("common","EJBUtil stringToInteger value of Str" + str);

    	try {

    		if (str != null) {
    			// Rlog.debug("common","inside str!=null");
    			str = str.trim();
    			if (str.length() > 0) {
    				// Rlog.debug("common","inside str.length>0");
    				returnInteger = Integer.valueOf(str);
    			}
    		}
    		return returnInteger;
    	} catch (Exception e) {
    		Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO Integer:}" + e);
    		return returnInteger;
    	}
    }
	
    public static boolean isEmpty(String param) {
        if ((!(param == null)) && (!(param.trim()).equals(""))
                && ((param.trim()).length() > 0))
            return false;
        else
            return true;
    }

}
