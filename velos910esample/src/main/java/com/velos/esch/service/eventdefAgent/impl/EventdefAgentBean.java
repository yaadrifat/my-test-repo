/*
 * Classname : EventdefAgentBean
 *
 * Version information: 1.0
 *
 * Date: 05/16/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana/Vikas Chawla
 */

package com.velos.esch.service.eventdefAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Enumeration;
import javax.ejb.Remote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.util.Hashtable;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;

import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP EventdefBean.<br>
 * <br>
 *
 */

@Stateless
@Remote( { EventdefAgentRObj.class })
public class EventdefAgentBean implements EventdefAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Calls getEventdefDetails() on Eventdef Entity Bean EventdefBean. Looks up
     * on the Eventdef id parameter passed in and constructs and returns a
     * Eventdef state holder. containing all the summary details of the Eventdef<br>
     *
     * @param eventdefId
     *            the Eventdef id
     * @ee EventdefBean
     */
    public EventdefBean getEventdefDetails(int event_id) {
        int status = 0;
        EventdefBean eventdef = null;

        try {
            eventdef = (EventdefBean) em.find(EventdefBean.class, new Integer(
                    event_id));
        }

        catch (Exception e) {
            Rlog.debug("eventdef",
                    "Error in getEventdefDetails() in EventdefAgentBean" + e);
        }
        return eventdef;
    }

    /**
     * Calls setEventdefDetails() on Eventdef Entity Bean EventdefBean.Creates a
     * new Eventdef with the values in the StateKeeper object.
     *
     * @param eventdef
     *            a eventdef state keeper containing eventdef attributes for the
     *            new Eventdef.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setEventdefDetails(EventdefBean edsk) {
        int status = 0;

        try {
            EventdefDao eventdefdao = new EventdefDao();
            // SV, 10/25/04, bug #1756, the event needs to be validated for
            // duplicate name. -1 here means new event being created.
            
            if(edsk.getName()!=null)// Bug #6752
            	edsk.setName(edsk.getName().trim());
            
            int count = eventdefdao.ValidateEventName(-1, EJBUtil
                    .stringToNum(edsk.getUser_id()), edsk.getName(), edsk
                    .getEvent_type(),"", "");
            if (count == -1) {
                return -1;
            }
            EventdefBean eventDefBean = new EventdefBean();
            eventDefBean.updateEventdef(edsk);
            em.persist(eventDefBean);
            Rlog.debug("eventdef",
                    "EventdefAgentBean.setEventdefDetails value of the status is "
                            + eventDefBean.getEvent_id());
            if (eventDefBean.getEvent_type().equalsIgnoreCase("L")
                    || eventDefBean.getEvent_type().equalsIgnoreCase("P")) {
                eventDefBean.setChain_id(Integer.toString(eventDefBean
                        .getEvent_id()));
            }
            em.merge(eventDefBean);
            return (eventDefBean.getEvent_id());
        } catch (Exception e1) {
            Rlog.fatal("eventdef",
                    "EventdefAgentBean.setEventdefDetails exception :" + e1);
            return -1;
        }

    }

    public int updateEventdef(EventdefBean edsk) {

        int output;
        EventdefBean eventdef = null;

        String oldBudgetLineItemCategory="";
        String newBudgetLineItemCategory="";
        String eventType="";
        //KM-#4550
        String oldEvtLibType = "";
        String newEvtLibType = "";

        try {

            EventdefDao eventdefdao = new EventdefDao();
            
            if(edsk.getName()!=null)// Bug #6752
            	edsk.setName(edsk.getName().trim());
            
            int count = eventdefdao.ValidateEventName(edsk.getEvent_id(),
                    EJBUtil.stringToNum(edsk.getUser_id()), edsk.getName(),
                    edsk.getEvent_type(), "", "");
            if (count == -1) {
                return -1;
            }

            eventdef = (EventdefBean) em.find(EventdefBean.class, new Integer(
                    edsk.getEvent_id()));

            if (eventdef == null) {
                return -2;
            }

            eventType = eventdef.getEvent_type();

            if (StringUtil.isEmpty(eventType))
            {
            	eventType="";

            }

            oldBudgetLineItemCategory = eventdef.getEventLineCategory();

            newBudgetLineItemCategory = edsk.getEventLineCategory();
            oldEvtLibType = eventdef.getEventLibType();
            newEvtLibType = edsk.getEventLibType();


            if (StringUtil.isEmpty(oldBudgetLineItemCategory))
            {
            	oldBudgetLineItemCategory="";

            }
            if (StringUtil.isEmpty(newBudgetLineItemCategory))
            {
            	newBudgetLineItemCategory="";

            }

            if (StringUtil.isEmpty(oldEvtLibType))
            {
            	oldEvtLibType="";

            }
            if (StringUtil.isEmpty(newEvtLibType))
            {
            	newEvtLibType="";
            }

            output = eventdef.updateEventdef(edsk);
            em.merge(eventdef);


            //if the record is for category, check if old budget lineitem is different from new

            if (eventType.equals("L")) //for Event Category record
            {
            	if (! newEvtLibType.equals(oldEvtLibType))
            	{
            		eventdefdao.updateEventLibTypeForAllEvents((edsk.getEvent_id()).intValue(),
            				newEvtLibType,edsk.getModifiedBy(),edsk.getIpAdd());
            	}


            	if (! newBudgetLineItemCategory.equals(oldBudgetLineItemCategory))
            	{
            		eventdefdao.updateBudgetLineitemCategoryForAllEvents((edsk.getEvent_id()).intValue(),
            				newBudgetLineItemCategory,edsk.getModifiedBy(),edsk.getIpAdd());
            	}
            }


        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in updateEventdef in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    public int createEventsDef(String[] eIdList, String[] vIdList, String[] seqList,
    		String[] dispList, String[] hideList,
    		int userId, String ipAdd) {
    	EventdefDao eventdefDao = new EventdefDao();
    	return eventdefDao.createEventsDef(eIdList, vIdList, seqList, dispList, hideList, userId, ipAdd);
    }
    
    public int removeEventdefChild(int event_id) {

        int output = 0;
        EventdefBean eventdef = null;
        try {

            EventdefDao eventdefdao = new EventdefDao();
            // if(eventdefdao.hasInstance(event_id,"E"))
            // {
            // return -1;
            // }
            eventdef = (EventdefBean) em.find(EventdefBean.class, new Integer(
                    event_id));
            em.remove(eventdef);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in removeEventdefChild  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int removeEventdefChilds(int event_ids[]) {
        int output = 0;
        try {
            for (int count = 0; count < event_ids.length; count++) {

                EventdefBean eventdef = (EventdefBean) em.find(
                        EventdefBean.class, new Integer(event_ids[count]));
                em.remove(eventdef);

            }
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in removeEventdefChild  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }
    // Overloaded for INF-18183 ::: Ankit
    public int removeEventdefChild(int event_id, Hashtable<String, String> auditInfo) {

        int output = 0;
        EventdefBean eventdef = null;
        try {

            EventdefDao eventdefdao = new EventdefDao();
            // if(eventdefdao.hasInstance(event_id,"E"))
            // {
            // return -1;

            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)auditInfo.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("EVENT_DEF","esch","event_id="+event_id);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("EVENT_DEF",String.valueOf(event_id),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            
            eventdef = (EventdefBean) em.find(EventdefBean.class, new Integer(
                    event_id));
            em.remove(eventdef);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("eventdef",
                    "Error in removeEventdefChild  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }


    public int removeEventdefHeader(int chain_id) {
        int output = 0;
        Enumeration event_ids;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            Query query = em.createNamedQuery("findEventIdsDef");
            query.setParameter("chainId", chain_id);
            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    EventdefBean eventdef = (EventdefBean) list.get(i);
                    em.remove(eventdef);
                }
            }
            // //////

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in removeEventdefChild  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int removeEventdefHeaders(int chain_ids[]) {
        int output = 0;
        Enumeration event_ids;

        try {

            Query query = em.createNamedQuery("findEventIdsDef");

            for (int count = 0; count < chain_ids.length; count++) {

                query.setParameter("chainId", chain_ids[count]);
                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        EventdefBean eventdef = (EventdefBean) list.get(i);
                        em.remove(eventdef);
                    }
                }
            }
            // ///////

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in removeEventdefChild  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: Ankit
    public int removeEventdefHeader(int chain_id, Hashtable<String, String> auditInfo) {
        int output = 0;
        Enumeration event_ids;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            Query query = em.createNamedQuery("findEventIdsDef");
            query.setParameter("chainId", chain_id);
            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            
            AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)auditInfo.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("EVENT_DEF","chain_id ="+chain_id,"esch");/*Fetches the RID/PK_VALUE*/
            ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
    		
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("EVENT_DEF",pkVal,ridVal,
        			userID,currdate,appmodule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
            
            
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    EventdefBean eventdef = (EventdefBean) list.get(i);
                    em.remove(eventdef);
                }
            }
            // //////

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("eventdef",
                    "Error in removeEventdefChild  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    public void AddEventsToProtocol(String chain_id, String[][] events_disps,
            int fromDisp, int toDisp, String userId, String ipAdd) {

        int output;
        String crfInfo = "C";
        try {

            Integer event_id;
            String eventIds = null;

            EventdefDao eventDao = new EventdefDao();
            eventDao.EditProtoEvent(chain_id, fromDisp, toDisp);
            String costStr = "";

            for (int i = 0; i < events_disps.length; i++) {
                Rlog.debug("eventdef", "Inside SessionBean:AddEventsToProtocol"
                        + events_disps[i][0] + " events_disps[i][1] "+events_disps[i][1] +" events_disps[i][2] "+events_disps[i][2] );
                event_id = new Integer(events_disps[i][0]);

                EventdefBean edsk = (EventdefBean) em.find(EventdefBean.class,
                        event_id);
                if (edsk == null) {
                    return;
                }

                EventdefBean edsknew = new EventdefBean();

                edsknew.cloneObject(edsk);

                edsknew.setChain_id(chain_id);
                edsknew.setDisplacement(events_disps[i][1]);
                edsknew.setEvent_type("A");
                edsknew.setOrg_id(event_id.intValue());
                edsknew.setCost(events_disps[i][2]);
                edsknew.setEventCategory(events_disps[i][4]);//KM



                try {
                if (!(StringUtil.isEmpty(events_disps[i][3])))
                edsknew.setEventVisit(events_disps[i][3]);
                }
                catch(Exception e)
                {
                }


                edsknew.setCreator(userId);
                edsknew.setIpAdd(ipAdd);

                Rlog.debug("eventdef", "inside the for loop" + edsk.getName());

                em.persist(edsknew);

                Integer oldID = new Integer(edsknew.getEvent_id());
                eventDao.CopyEventDetails(events_disps[i][0], oldID.toString(),
                        crfInfo, userId, ipAdd);
            }// for loop
        }// try
        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in AddEventsToProtocol in EventdefAgentBean" + e);
            e.printStackTrace();
            return;
        }
    }

    public int CopyProtocol(String protocol_id, String name, String calType,String userId,
            String ipAdd) {

        int output;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.CopyProtocol(protocol_id, name, calType, userId, ipAdd);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in CopyProtocol  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    //KM-02Sep09
    public int CopyEventToCat(int event_id, int whichTable, int category_id, int evtLibType,
            String name, String userId, String ipAdd) {

        int output;

        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.CopyEventToCat(event_id, whichTable,
                    category_id, evtLibType, name, userId, ipAdd);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in CopyEventToCat  in EventDefAgentBean" + e);
            return -1;
        }
        return output;
    }

    public int RemoveProtEventInstance(String[] deleteIds, String whichTable) {

        int output;

        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.RemoveProtEventInstance(deleteIds, whichTable);//KM
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in CopyEventToCat  in EventDefAgentBean" + e);
            return -1;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: Ankit
    public int RemoveProtEventInstance(String[] deleteIds, String whichTable, Hashtable<String, String> auditInfo) {

        int output;
        String condition = "";

        try {
        	//condition = "EVENT_ID = "+deleteIds;
        	for(int i=0; i<deleteIds.length; i++)
        		condition = condition + ", " + deleteIds[i];  
        	
        	condition = "EVENT_ID IN ("+condition.substring(1)+")";
        	
        	AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues(whichTable,condition,"esch");/*Fetches the RID/PK_VALUE*/
            ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
    		
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean(whichTable.toUpperCase(),pkVal,ridVal,
        			userID,currdate,moduleName,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
        	
        	EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.RemoveProtEventInstance(deleteIds, whichTable);//KM
            if(output == 0){context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return 0 else, rollback */
        }

        catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("eventdef",
                    "Error in CopyEventToCat  in EventDefAgentBean" + e);
            return -1;
        }
        return output;
    }

    public int fetchEventIdByVisitAndDisplacement(String anEventId, String whichTable,
            String fkVisit, String displacement) {
        int output = 0;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.fetchEventIdByVisitAndDisplacement(anEventId, whichTable,
                    fkVisit, displacement);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in fetchEventIdByDisplacement in EventdefAgentBean " + e);
            output = -1;
        }
        return output;
    }

    public int fetchEventIdByVisit(String anEventId, String whichTable,
            String fkVisit) {
        int output = 0;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.fetchEventIdByVisit(anEventId, whichTable, fkVisit);
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in fetchEventIdByVisit in EventdefAgentBean " + e);
            output = -1;
        }
        return output;
    }

    //JM: #C8.5:
    //KM-#5949
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user) {

        int output;

        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.deleteEvtOrVisits(deleteIds, whichTable, flags, user);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in deleteEvtOrVisits  in EventDefAgentBean" + e);
            return -1;
        }
        return output;
    }

    // Overloaded for INF-18183 ::: Ankit
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user,Hashtable<String, String> auditInfo) {

        int output;
        try {
        	
        	String visitString = "";
        	String eventString = "";
        	String condition = "";
        	String pkVal = "";
        	String ridVal = "";
        	String tableName = "";
        	int flagDelVis = 0;
        	int flagDelEvt = 0;
        	
        	for(int i=0; i<deleteIds.length;i++)
			{
	              if (flags[i].equals("1")){
	                  visitString = visitString + deleteIds[i] + ',';
	                  flagDelVis  = flagDelVis + 1 ;
					  }
	              else{
	                  eventString = eventString + deleteIds[i] + ',';
	                  flagDelEvt = flagDelEvt + 1 ;
					}
			}
        	
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)auditInfo.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            
            if (flagDelVis > 0 ){
            	visitString = visitString.substring(0, visitString.length()-1);
            	condition = " pk_protocol_visit in (" + visitString + ") and (OFFLINE_FLAG <>1 or OFFLINE_FLAG is null)";//sch_protocol_visit
            	Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("SCH_PROTOCOL_VISIT",condition,"esch");/*Fetches the RID/PK_VALUE*/
                ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
        		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
        		
        		for(int i=0; i<rowPK.size(); i++)
    			{
        			pkVal = rowPK.get(i);
        			ridVal = rowRID.get(i);
        			audit = new AuditBean("SCH_PROTOCOL_VISIT",pkVal,ridVal,
            			userID,currdate,appmodule,ipAdd,reason);
        			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
    			}
            }
            if (flagDelEvt>0){
            	eventString = eventString.substring(0, eventString.length()-1);
            	if ("event_def".equals(whichTable.toLowerCase())){
            		condition = " event_id in ("+eventString +") and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) ";//event_def
            		tableName = "EVENT_DEF";
            	}
                else{
                	condition = " event_id in ("+eventString +") and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) and (not offline_flag >0 or offline_flag is null)";//event_assoc
                	tableName = "EVENT_ASSOC";
                }
            	
            	Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues(tableName,condition,"esch");/*Fetches the RID/PK_VALUE*/
                ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
        		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
        		
        		for(int i=0; i<rowPK.size(); i++)
    			{
        			pkVal = rowPK.get(i);
        			ridVal = rowRID.get(i);
        			audit = new AuditBean(tableName,pkVal,ridVal,
            			userID,currdate,appmodule,ipAdd,reason);
        			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
    			}
            }
        	
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.deleteEvtOrVisits(deleteIds, whichTable, flags, user);
            
            if(output!=0){
            	context.setRollbackOnly();
            	}
        }

        catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("eventdef",
                    "Error in deleteEvtOrVisits  in EventDefAgentBean" + e);
            return -1;
        }
        return output;
    }




    public int GenerateSchedule(int protocol_id, int patient_id,
            java.sql.Date st_date, int patProtId, Integer days, String modifiedBy,
            String ipAdd) {
        int output;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.GenerateSchedule(protocol_id, patient_id,
                    st_date, patProtId, days, modifiedBy, ipAdd);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in GenerateSchedule  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int DeactivateEvents(int protocol_id, int patient_id,
            java.sql.Date st_date,String type) {

        int output;
        try {
            Rlog.debug("eventdef",
                    "DeactivateEvents  in EventdefAgentBean line 1");
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.DeactivateEvents(protocol_id, patient_id,
                    st_date,type);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in DeactivateEvents  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int DeleteProtocolEvents(String chain_id) {

        /*
         * int[] eve_ids = new int[event_ids.length]; for(int i=0;i<event_ids.length;i++) {
         * Integer temp = new Integer(event_ids[i]); eve_ids[i]=temp.intValue(); }
         * removeEventdefChilds(eve_ids); return 0;
         */

        // eventDao.EditProtoEvent(chain_id);
        int output;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            // eventdefdao.EditProtoEvent(chain_id);

            output = eventdefdao.DeleteDuplicateEvents(chain_id);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in DeleteProtocolEvents  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    // SV, 8/19/04, part of fix for #1602, added to call the new
    // DeleteProtocolEvents with duration parameter added.
    // This will be called, when duration is reduced on the protocol calendar.
    public int DeleteProtocolEventsPastDuration(String chain_id, int duration) {
        int output;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.DeleteProtocolEvents(chain_id, duration);
        }

        catch (Exception e) {
            Rlog.fatal("EventdefAgentBean",
                    "Error in DeleteProtocolEventsPastDuration  in EventdefAgentAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public EventdefDao FetchSchedule(int protocol_id, int patient_id,
            String pro_start_date) {

        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.FetchSchedule(protocol_id, patient_id, pro_start_date);
            return eventdefdao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in FetchSchedule  in EventdefAgentBean" + e);
        }
        return null;
    }

    public EventdefDao FetchFilteredSchedule(int protocol_id, int patient_id,
            String pro_start_date, String visit, String month, String year,
            String event, String status) {
        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.FetchFilteredSchedule(protocol_id, patient_id,
                    pro_start_date, visit, month, year, event, status);
            return eventdefdao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in FetchFilteredSchedule  in EventdefAgentBean" + e);
        }
        return null;
    }

    public EventdefDao getTotalVisit(int protocol_id, int patient_id,
            String pro_start_date) {
        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.getTotalVisit(protocol_id, patient_id, pro_start_date);
            return eventdefdao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getTotalVsisit in EventdefAgentBean" + e);
        }
        return null;
    }

    public EventdefDao getSchEvents(int protocol_id, int patient_id,
            String pro_start_date) {
        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.getSchEvents(protocol_id, patient_id, pro_start_date);
            return eventdefdao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getSchEvents  in EventdefAgentBean" + e);
        }
        return null;
    }

    /**
     * Calls getEventdefValues() on EventdefDao;
     *
     * @param Id
     *            of a Group
     * @return EventdefDao object with details of all Eventdefs of a specified
     *         Group
     * @ee EventdefDao
     */

    public EventdefDao getAllLibAndEvents(int user_id) {
        try {
            Rlog.debug("eventdef",
                    "In getAllLibAndEvents in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getAllLibAndEvents(user_id);

            return eventdefDao;
        } catch (Exception e) {
            Rlog
                    .fatal("eventdef",
                            "Exception In getAllLibAndEvents in EventdefAgentBean "
                                    + e);
        }
        return null;

    }

    public EventInfoDao getEventDefInfo(int eventId) {
        try {
            Rlog.debug("eventdef",
                    "In getEventDefInfo in EventdefAgentBean line number 0");
            EventInfoDao eventinfoDao = new EventInfoDao();

            eventinfoDao.getEventDefInfo(eventId);

            return eventinfoDao;
        } catch (Exception e) {
            Rlog
                    .fatal("eventdef",
                            "Exception In getAllLibAndEvents in EventdefAgentBean "
                                    + e);
        }
        return null;

    }

    public EventdefDao getEventStatusHistory(String eventId) {
        try {
            Rlog
                    .debug("eventdef",
                            "In getEventStatusHistory in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getEventStatusHistory(eventId);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getEventStatusHistory in EventdefAgentBean "
                            + e);
        }
        return null;

    }

    public EventdefDao getFilteredEvents(int user_id, int protocol_id) {
        try {
            Rlog.debug("eventdef",
                    "In getAllLibAndEvents in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getFilteredEvents(user_id, protocol_id);

            return eventdefDao;
        } catch (Exception e) {
            Rlog
                    .fatal("eventdef",
                            "Exception In getAllLibAndEvents in EventdefAgentBean "
                                    + e);
        }
        return null;

    }

    public EventdefDao getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp) {
        try {
            Rlog
                    .debug("eventdef",
                            "In getFilteredRangeEvents in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getFilteredRangeEvents(protocolId, beginDisp, endDisp);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getFilteredRangeEvents in EventdefAgentBean "
                            + e);
        }
        return null;

    }

    public EventdefDao FetchPreviousSchedule(int studyId, int patientId) {
        try {
            Rlog
                    .debug("eventdef",
                            "In FetchPreviousSchedule in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.FetchPreviousSchedule(studyId, patientId);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In FetchPreviousSchedule in EventdefAgentBean "
                            + e);
        }
        return null;

    }

    public EventdefDao getUserProtAndEvents(int user_id) {
        try {
            Rlog.debug("eventdef",
                    "In getAllLibAndEvents in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getUserProtAndEvents(user_id);

            return eventdefDao;
        } catch (Exception e) {
            Rlog
                    .fatal("eventdef",
                            "Exception In getAllLibAndEvents in EventdefAgentBean "
                                    + e);
        }
        return null;

    }

    //KM
    public EventdefDao getHeaderList(int user_id, String type, int evtLibType) {
        try {
            Rlog.debug("eventdef",
                    "In getHeaderList in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getHeaderList(user_id, type, evtLibType);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getHeaderList in EventdefAgentBean " + e);
        }
        return null;

    }

    //Added by IA multiple organizations bug 2613
    public EventdefDao getHeaderList(int account_id, int user_id, String type, String orgId) {
     try {
            Rlog.debug("eventdef",
                    "In getHeaderList in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getHeaderList(account_id, user_id, type, orgId);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getHeaderList in EventdefAgentBean " + e);
        }
        return null;

    }
    public EventdefDao getHeaderList(int account_id, int user_id, String type) {
        try {
            Rlog.debug("eventdef",
                    "In getHeaderList in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getHeaderList(account_id, user_id, type);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getHeaderList in EventdefAgentBean " + e);
        }
        return null;

    }

    public EventdefDao getHeaderList(int account_id, int user_id, String type,
            int budgetId) {
        try {
            Rlog.debug("eventdef",
                    "In getHeaderList in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getHeaderList(account_id, user_id, type, budgetId);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getHeaderList in EventdefAgentBean " + e);
        }
        return null;

    }

    public EventdefDao getAllProtAndEvents(int event_id) {
        try {
            Rlog.debug("eventdef",
                    "In getHeaderList in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getAllProtAndEvents(event_id);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getHeaderList in EventdefAgentBean " + e);
        }
        return null;

    }

    //Modified by Manimaran to fix the issue #3394
    public EventdefDao getProtSelectedEvents(int protocolId,String orderType,String orderBy) {
        try {
            Rlog.debug("eventdef",
                    "In getHeaderList in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();
            eventdefDao.getProtSelectedEvents(protocolId,orderType,orderBy);
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getHeaderList in EventdefAgentBean " + e);
        }
        return null;

    }

    public EventdefDao getProtSelectedAndGroupedEvents(int protocolId, boolean isExport, int finDetRight) {
        try {
            EventdefDao eventdefDao = new EventdefDao();
            eventdefDao.getProtSelectedAndGroupedEvents(protocolId, isExport, finDetRight);
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "In EventdefAgentBean.getProtSelectedAndGroupedEvents: "+e);
        }
        return null;
    }

    public int MarkDone(int event_id, String notes, java.sql.Date exe_date,
            int exe_by, int status, int oldStatus, String ipAdd, String mode, 
            int eventSOSId, int eventCoverType, String reasonForCoverageChange) {

        int output;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.MarkDone(event_id, notes, exe_date, exe_by,
                    status, oldStatus, ipAdd, mode, eventSOSId, eventCoverType, reasonForCoverageChange);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "Error in MarkDone  in EventdefAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    public EventdefDao getProtRowEvents(int protocolId, int rownum,
            int beginDisp, int endDisp) {
        try {
            Rlog.debug("eventdef",
                    "In getProtRowEvents in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao
                    .getProtRowEvents(protocolId, rownum, beginDisp, endDisp);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getProtRowEvents in EventdefAgentBean " + e);
        }
        return null;

    }

    public ArrayList findAllMovingEvents(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag, String calAssoc ,int synchSugDate) {

        ArrayList eventIds = new ArrayList();
        try {
            EventdefDao eventdefdao = new EventdefDao();
            Rlog.debug("eventdef", "before execute from agent bean");
            eventIds = eventdefdao.findAllMovingEvents(prevDate, actualDate,
                    eventId, patprotId, statusFlag, calAssoc, synchSugDate);
        }
        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in findAllMovingEvents in EventdefAgentBean" + e);
            return null;
        }
        return eventIds;
    }
    
    public int ChangeActualDate(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag, String ipAdd,
            String modifiedBy,String calAssoc ,int synchSugDate) {

        int output;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            Rlog.debug("eventdef", "before execute from agent bean");
            output = eventdefdao.ChangeActualDate(prevDate, actualDate,
                    eventId, patprotId, statusFlag, ipAdd, modifiedBy,calAssoc, synchSugDate);

        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in ChangeActualDate  in EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    public EventInfoDao getAdverseEvents(int fkStudy, int fkPer) {

        try {

            EventInfoDao eventinfodao = new EventInfoDao();
            eventinfodao.getAdverseEvents(fkStudy, fkPer);
            return eventinfodao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAdverseEvents  in EventdefAgentBean" + e);
            return null;
        }

    }

    public EventInfoDao getAdvInfo(int fkAdverseEvent, String flag) {

        try {

            EventInfoDao eventinfodao = new EventInfoDao();
            eventinfodao.getAdvInfo(fkAdverseEvent, flag);
            return eventinfodao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef", "Error in getAdvInfo  in EventdefAgentBean"
                    + e);
            return null;
        }

    }

    public EventInfoDao getAdvNotify(int fkAdverseEvent) {

        try {

            EventInfoDao eventinfodao = new EventInfoDao();
            eventinfodao.getAdvNotify(fkAdverseEvent);
            return eventinfodao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getAdvNotify  in EventdefAgentBean" + e);
            return null;
        }

    }

    public EventdefDao FetchSchedule(int eventId) {

        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.FetchSchedule(eventId);
            return eventdefdao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in FetchSchedule  in EventdefAgentBean" + e);
        }
        return null;
    }

    public EventdefDao getVisitEvents(int patProtId, int visit) {

        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.getVisitEvents(patProtId, visit);
            return eventdefdao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getVisitEvents in EventdefAgentBean" + e);
        }
        return null;
    }

    public EventdefDao getVisitEvents(int patProtId, int visit,String type,int protocolId) {

        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.getVisitEvents(patProtId, visit,type,protocolId);
            return eventdefdao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getVisitEvents in EventdefAgentBean" + e);
        }
        return null;
    }


    public EventdefDao getVisitEvents(int patProtId, int status, int visit,
            String exeon) {

        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.getVisitEvents(patProtId, status, visit, exeon);
            return eventdefdao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getVisitEvents(int patProtId) in EventdefAgentBean"
                            + e);
        }
        return null;
    }

    public EventdefDao getVisitEvents(int patProtId, int status, int visit,
            String exeon,String type,int protocolId) {

        try {
            EventdefDao eventdefdao = new EventdefDao();
            eventdefdao.getVisitEvents(patProtId, status, visit, exeon,type,protocolId);
            return eventdefdao;
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getVisitEvents(int patProtId) in EventdefAgentBean"
                            + e);
        }
        return null;
    }


    public int copyNotifySetting(int p_old_fkpatprot, int p_new_fkpatprot,
            int p_copyflag, int p_creator, String p_ip) {

        int output;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            output = eventdefdao.copyNotifySetting(p_old_fkpatprot,
                    p_new_fkpatprot, p_copyflag, p_creator, p_ip);
        }

        catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in copyNotifySetting EventdefAgentBean" + e);
            return -2;
        }
        return output;
    }

    public int getMaxCost(String chainId) {
        int cost = 0;
        try {
            EventdefDao eventdefdao = new EventdefDao();
            cost = eventdefdao.getMaxCost(chainId);
        } catch (Exception e) {
            Rlog.fatal("eventdef", "EventdefAgentBean.GetMaxCost " + e);
            return -2;
        }
        return cost;
    }

    // ///////////
    // / Changes for Phase II by Sonia Kaura
    // / Date : 11/3/2003

    public EventdefDao getAllProtSelectedEvents(int protocolId, String visitId, String evtName) {
        try {
            Rlog.debug("eventdef",
                    "In getHeaderList in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            eventdefDao.getAllProtSelectedEvents(protocolId, visitId, evtName);

            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getAllProtSelectedEvents in EventdefAgentBean "
                            + e);
        }
        return null;

    }
        //YK: FOR PCAL-20461
    public EventdefDao getAllProtSelectedVisitsEvents(int protocolId, String visitId, String evtName) {
    	try {
    		Rlog.debug("eventdef",
    		"In getHeaderList in EventdefAgentBean line number 0");
    		EventdefDao eventdefDao = new EventdefDao();
    		
    		eventdefDao.getAllProtSelectedVisitsEvents(protocolId, visitId, evtName);
    		
    		return eventdefDao;
    	} catch (Exception e) {
    		Rlog.fatal("eventdef",
    				"Exception In getAllProtSelectedVisitsEvents in EventdefAgentBean "
    				+ e);
    	}
    	return null;
    	
    }

    // by Sonia Sahni
    // / Date : 12/24/2003

    public EventdefDao getScheduleEventsVisits(int patprot) {
        try {
            EventdefDao eventdefDao = new EventdefDao();
            eventdefDao.getScheduleEventsVisits(patprot);
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getScheduleEventsVisits in  EventdefAgentBean "
                            + e);
        }
        return null;

    }

    public EventdefDao getStudyScheduleEventsVisits(int studyId,int protocolId) {
        try {
            EventdefDao eventdefDao = new EventdefDao();
            eventdefDao.getStudyScheduleEventsVisits(studyId,protocolId);
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getStudyScheduleEventsVisits in  EventdefAgentBean "
                            + e);
        }
        return null;

    }


    /**
     * Delete calendar from library
     *
     * @return 0 for successful delete, -2 in case of exception
     */
    public int calendarDelete(int calId) {

        int ret = 0;

        int output = 0;
        Enumeration event_ids;

        try {

            EventdefBean eventdefR = (EventdefBean) em.find(EventdefBean.class,
                    new Integer(calId));
            em.remove(eventdefR);

            /* Remove events associated to the calendar */

            Query query = em.createNamedQuery("findEventIdsDef");
            query.setParameter("chainId", new Integer(calId));
            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    EventdefBean eventdef = (EventdefBean) list.get(i);
                    em.remove(eventdef);
                }
            }

            Rlog.debug("eventdef", "After removing the event");

            return 0;
        }

        catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("eventdef", "EXCEPTION IN REMOVING eventdef" + e);
            return -2;
        }

    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int calendarDelete(int calId, Hashtable<String, String> auditInfo) {

        int ret = 0;

        int output = 0;
        Enumeration event_ids;

        try {

            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            String ridValue= AuditUtils.getRidValue("EVENT_DEF","esch","EVENT_ID="+calId);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("EVENT_DEF",String.valueOf(calId),ridValue,
        			userID,currdate,moduleName,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        	
            EventdefBean eventdefR = (EventdefBean) em.find(EventdefBean.class,
            		
                    new Integer(calId));
            em.remove(eventdefR);

            /* Remove events associated to the calendar */

            Query query = em.createNamedQuery("findEventIdsDef");
            query.setParameter("chainId", new Integer(calId));
            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    EventdefBean eventdef = (EventdefBean) list.get(i);
                    em.remove(eventdef);
                }
            }

            Rlog.debug("eventdef", "After removing the event");

            return 0;
        }

        catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("eventdef", "EXCEPTION IN REMOVING eventdef" + e);
            return -2;
        }

    }

    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In saveMultipleEventsVisits in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            ret = eventdefDao.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon,
                    arrNote, arrSos, arrCoverage, reasonForCoverageChng, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In saveMultipleEventsVisits in EventdefAgentBean "
                            + e);
        }
        return ret;

    }
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In saveMultipleEventsVisits in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            ret = eventdefDao.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon,
                    ipAdd);

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In saveMultipleEventsVisits in EventdefAgentBean "
                            + e);
        }
        return ret;

    }
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd,String type) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In saveMultipleEventsVisits in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            ret = eventdefDao.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon,
                    arrNote, arrSos, arrCoverage, reasonForCoverageChng, ipAdd,type);

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In saveMultipleEventsVisits in EventdefAgentBean "
                            + e);
        }
        return ret;

    }
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd,String type) {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "In saveMultipleEventsVisits in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            ret = eventdefDao.saveMultipleEventsVisits(enrollId, userId,
                    arrEventIds, arrStatusId, arrOldStatusId, arrExeon,ipAdd,type);

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In saveMultipleEventsVisits in EventdefAgentBean "
                            + e);
        }
        return ret;

    }
    

    public int copyProtToLib(String protocol_id, String study_id,String name,String desc,String type,String shareWith,
            String userId, String ipAdd)
    {
        int ret = 0;
        try {
            Rlog
                    .debug("eventdef",
                            "CopyProtToLib in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            ret = eventdefDao.copyProtToLib(protocol_id,  study_id,name,desc,type,shareWith,
                    userId,  ipAdd);

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In copyProtToLib in EventdefAgentBean "
                            + e);
        }
        return ret;

    }

    public int propagateEventUpdates(int protocolId, int eventId,
            String tableName, int primary_key, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType)
    {
        int ret = 0;
        try {
            Rlog.debug("eventdef",
                            "propagateEventUpdates in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();

            ret = eventdefDao.propagateEventUpdates(protocolId,  eventId,
                     tableName,  primary_key,  propagateInVisitFlag,
                     propagateInEventFlag,  mode,  calType);

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In propagateEventUpdates in EventdefAgentBean "
                            + e);
        }
        return ret;

    }

    /**
     *JM: 10Apr2008: for #C8 March2008-enh.
     * Parameter protocolId
     *
     *
     */
    public EventdefDao getProtVisitAndEvents(int protocolId, String search,String resetSort) { /*YK 04Jan- SUT_1_Requirement_18489*/
        try {
            Rlog.debug("eventdef",
                    "In getProtVisitAndEvents in EventdefAgentBean line number 0");
            EventdefDao eventdefDao = new EventdefDao();
            eventdefDao.getProtVisitAndEvents(protocolId, search, resetSort); /*YK 04Jan- SUT_1_Requirement_18489*/
            return eventdefDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getProtVisitAndEvents in EventdefAgentBean " + e);
        }
        return null;

    }

  //JM: 10April2008: #C8
    public int getMaxSeqForVisitEvents(int visitId, String eventName, String tableName) {
        int ret=0;
        try {
        	EventdefDao eventdefDao = new EventdefDao();
            ret = eventdefDao.getMaxSeqForVisitEvents(visitId, eventName, tableName);
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Exception In getMaxSeqForVisitEvents" + e);
            return -2;
        }
        return ret;
    }

    //JM: 17Apr2008
    public int CopyEventDetails(String old_event_id, String new_event_id, String crfInfo, String userId, String ipAdd){
        int ret=0;
        try {
        	EventdefDao eventdefDao = new EventdefDao();
            eventdefDao.CopyEventDetails( old_event_id,  new_event_id,  crfInfo,  userId,  ipAdd);
        } catch (Exception e) {
            Rlog.fatal("eventdef", "Exception In CopyEventDetails() in eventDefAgentBean" + e);
            return -2;
        }
        return ret;
    }

    /**
     * This method shall update the fk_patprot column of ER_PATTXARM table with the current protocol id/the current patient schedule
     * @param StudyId
     */
      public int updatePatTxtArmsForTheNewPatProt(int patProtIdOld, int patProtId) {
          try {
        	  EventdefDao eventdefDao = new EventdefDao();
              eventdefDao.updatePatTxtArmsForTheNewPatProt(patProtIdOld,patProtId);
          } catch (Exception e) {
              Rlog.fatal("eventdef", "Error in updatePatTxtArmsForTheNewPatProt() in eventDefAgentBean" + e);
              return -1;
          }
          return 1;
      }
      //Added by Virendra
      /**
       * This method shall remove event cost
      */
        
      public int removeEventCost(int eventId, int eventcostId, String mode){
            try {
          	  EventdefDao eventdefDao = new EventdefDao();
                eventdefDao.removeEventCost(eventId, eventcostId, mode);
            } catch (Exception e) {
                Rlog.fatal("eventdef", "Error in removeEventCost() in eventDefAgentBean" + e);
                return -1;
            }
            return 1;
        }


 //Yk: Added for Enhancement :PCAL-19743
      public EventdefDao getProtSelectedAndVisitEvents(int protocolId) {
      	try {
      		EventdefDao eventdefDao = new EventdefDao();
      		eventdefDao.getProtSelectedAndVisitEvents(protocolId);
      		return eventdefDao;
      	} catch (Exception e) {
      		Rlog.fatal("eventdef",
      				"In EventdefAgentBean.getProtSelectedAndVisitEvents: "+e);
      	}
      	return null;
      }


}// end of class
