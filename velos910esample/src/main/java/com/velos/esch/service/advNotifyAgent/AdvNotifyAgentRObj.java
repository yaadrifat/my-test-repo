/*
 * Classname : AdvNotifyAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 12/11/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.advNotifyAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.esch.business.advNotify.impl.AdvNotifyBean;

// import com.velos.esch.business.common.EventInfoDao;
@Remote
public interface AdvNotifyAgentRObj {

    AdvNotifyBean getAdvNotifyDetails(int advNotifyId);

    public int setAdvNotifyDetails(AdvNotifyBean account);

    public int updateAdvNotify(AdvNotifyBean usk);

}