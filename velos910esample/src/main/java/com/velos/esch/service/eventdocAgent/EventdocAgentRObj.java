/*
 * Classname : EventdocAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 07/04/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.eventdocAgent;

/* Import Statements */

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.eventdoc.impl.EventdocBean;

/* End of Import Statements */
@Remote
public interface EventdocAgentRObj {

    EventdocBean getEventdocDetails(int docId);

    public int setEventdocDetails(EventdocBean edsk);

    public int updateEventdoc(EventdocBean edsk);
    
    public int removeEventdoc(int eventdocId, String docType);    
    
    //Overloaded for INF-18183 ::: Ankit
    public int removeEventdoc(int eventdocId, String docType, Hashtable<String, String> argsvalues);
}