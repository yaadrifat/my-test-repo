/*
 * Classname	BudgetDao.class
 *
 * Version information 	1.0
 *
 * Date	04/04/2002
 *
 * Author Sajal
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

public class BudgetDao extends CommonDAO implements java.io.Serializable {
    private ArrayList bgtIds;

    private ArrayList bgtNames;

    private ArrayList bgtVersions;

    private ArrayList bgtDescs;

    private ArrayList bgtCreators;

    private ArrayList bgtTemplates;

    private ArrayList bgtStats;

    private ArrayList bgtCurrs;

    private ArrayList bgtSiteFlags;

    private ArrayList bgtCalFlags;

    private ArrayList bgtStudyIds;

    private ArrayList bgtAccIds;

    private ArrayList bgtSiteIds;

    private ArrayList bgtRightScopes;

    private ArrayList bgtRights;

    private ArrayList bgtStudyTitles;

    private ArrayList bgtStudyNumbers;

    private ArrayList bgtSites;

    private ArrayList bgtIdWithFlgs;

    private ArrayList bgtTypes;

    private ArrayList bgtCodeListStatuses;
    private ArrayList bgtCodeListStatusesDescs;

    private ArrayList bgtCombBgtFlags;


    private ArrayList codelstHideUnhides;

    private int bRows;

    public BudgetDao() {
        bgtIds = new ArrayList();
        bgtNames = new ArrayList();
        bgtVersions = new ArrayList();
        bgtDescs = new ArrayList();
        bgtCreators = new ArrayList();
        bgtTemplates = new ArrayList();
        bgtStats = new ArrayList();
        bgtCurrs = new ArrayList();
        bgtSiteFlags = new ArrayList();
        bgtCalFlags = new ArrayList();
        bgtStudyIds = new ArrayList();
        bgtAccIds = new ArrayList();
        bgtSiteIds = new ArrayList();
        bgtRightScopes = new ArrayList();
        bgtRights = new ArrayList();
        bgtStudyTitles = new ArrayList();
        bgtStudyNumbers = new ArrayList();
        bgtSites = new ArrayList();
        bgtIdWithFlgs = new ArrayList();
        bgtTypes = new ArrayList();
        bgtCodeListStatuses = new ArrayList();
        bgtCodeListStatusesDescs = new ArrayList();
        bgtCombBgtFlags = new ArrayList();

        codelstHideUnhides = new ArrayList();

    }

    // Getter and Setter methods

    public ArrayList getBgtIds() {
        return this.bgtIds;
    }

    public void setBgtIds(ArrayList bgtIds) {
        this.bgtIds = bgtIds;
    }

    public ArrayList getBgtNames() {
        return this.bgtNames;
    }

    public void setBgtNames(ArrayList bgtNames) {
        this.bgtNames = bgtNames;
    }

    public ArrayList getBgtVersions() {
        return this.bgtVersions;
    }

    public void setBgtVersions(ArrayList bgtVersions) {
        this.bgtVersions = bgtVersions;
    }

    public ArrayList getBgtDescs() {
        return this.bgtDescs;
    }

    public void setBgtDescs(ArrayList bgtDescs) {
        this.bgtDescs = bgtDescs;
    }

    public ArrayList getBgtIdWithFlgs() {
        return this.bgtIdWithFlgs;
    }

    public void setBgtIdWithFlgs(ArrayList bgtIdWithFlgs) {
        this.bgtIdWithFlgs = bgtIdWithFlgs;
    }

    public ArrayList getBgtCreators() {
        return this.bgtCreators;
    }

    public void setBgtCreators(ArrayList bgtCreators) {
        this.bgtCreators = bgtCreators;
    }

    public ArrayList getBgtTemplates() {
        return this.bgtTemplates;
    }

    public void setBgtTemplates(ArrayList bgtTemplates) {
        this.bgtTemplates = bgtTemplates;
    }

    public ArrayList getBgtTypes() {
        return this.bgtTypes;
    }

    public void setBgtTypes(ArrayList bgtTypes) {
        this.bgtTypes = bgtTypes;
    }

    public ArrayList getBgtStats() {
        return this.bgtStats;
    }

    public void setBgtStats(ArrayList bgtStats) {
        this.bgtStats = bgtStats;
    }

    public ArrayList getBgtCurrs() {
        return this.bgtCurrs;
    }

    public void setBgtCurrs(ArrayList bgtCurrs) {
        this.bgtCurrs = bgtCurrs;
    }

    public ArrayList getBgtSiteFlags() {
        return this.bgtSiteFlags;
    }

    public void setBgtSiteFlags(ArrayList bgtSiteFlags) {
        this.bgtSiteFlags = bgtSiteFlags;
    }

    public ArrayList getBgtCalFlags() {
        return this.bgtCalFlags;
    }

    public void setBgtCalFlags(ArrayList bgtCalFlags) {
        this.bgtCalFlags = bgtCalFlags;
    }

    public ArrayList getBgtStudyIds() {
        return this.bgtStudyIds;
    }

    public void setBgtStudyIds(ArrayList bgtStudyIds) {
        this.bgtStudyIds = bgtStudyIds;
    }

    public ArrayList getBgtAccIds() {
        return this.bgtAccIds;
    }

    public void setBgtAccIds(ArrayList bgtAccIds) {
        this.bgtAccIds = bgtAccIds;
    }

    public ArrayList getBgtSiteIds() {
        return this.bgtSiteIds;
    }

    public void setBgtSiteIds(ArrayList bgtSiteIds) {
        this.bgtSiteIds = bgtSiteIds;
    }

    public ArrayList getBgtRightScopes() {
        return this.bgtRightScopes;
    }

    public void setBgtRightScopes(ArrayList bgtRightScopes) {
        this.bgtRightScopes = bgtRightScopes;
    }

    public ArrayList getBgtRights() {
        return this.bgtRights;
    }

    public void setBgtRights(ArrayList bgtRights) {
        this.bgtRights = bgtRights;
    }

    public ArrayList getBgtStudyTitles() {
        return this.bgtStudyTitles;
    }

    public void setBgtStudyTitles(ArrayList bgtStudyTitles) {
        this.bgtStudyTitles = bgtStudyTitles;
    }

    public ArrayList getBgtStudyNumbers() {
        return this.bgtStudyNumbers;
    }

    public void setBgtStudyNumbers(ArrayList bgtStudyNumbers) {
        this.bgtStudyNumbers = bgtStudyNumbers;
    }

    public ArrayList getBgtSites() {
        return this.bgtSites;
    }

    public void setBgtSites(ArrayList bgtSites) {
        this.bgtSites = bgtSites;
    }

    public void setBgtIds(Integer bgtId) {
        bgtIds.add(bgtId);
    }

    public void setBgtNames(String bgtName) {
        bgtNames.add(bgtName);
    }

    public void setBgtVersions(String bgtVersion) {
        bgtVersions.add(bgtVersion);
    }

    public void setBgtDescs(String bgtDesc) {
        bgtDescs.add(bgtDesc);
    }

    public void setBgtIdWithFlgs(String bgtIdWithFlg) {
        bgtIdWithFlgs.add(bgtIdWithFlg);
    }

    public void setBgtCreators(String bgtCreator) {
        bgtCreators.add(bgtCreator);
    }

    public void setBgtTemplates(String bgtTemplate) {
        bgtTemplates.add(bgtTemplate);
    }

    public void setBgtTypes(String bgtType) {
        bgtTypes.add(bgtType);
    }

    public void setBgtStats(String bgtStat) {
        bgtStats.add(bgtStat);
    }

    public void setBgtCurrs(String bgtCurr) {
        bgtCurrs.add(bgtCurr);
    }

    public void setBgtSiteFlags(String bgtSiteFlag) {
        bgtSiteFlags.add(bgtSiteFlag);
    }

    public void setBgtCalFlags(String bgtCalFlag) {
        bgtCalFlags.add(bgtCalFlag);
    }

    public void setBgtStudyIds(String bgtStudyId) {
        bgtStudyIds.add(bgtStudyId);
    }

    public void setBgtAccIds(String bgtAccId) {
        bgtAccIds.add(bgtAccId);
    }

    public void setBgtSiteIds(String bgtSiteId) {
        bgtSiteIds.add(bgtSiteId);
    }

    public void setBgtRightScopes(String bgtRightScope) {
        bgtRightScopes.add(bgtRightScope);
    }

    public void setBgtRights(String bgtRight) {
        bgtRights.add(bgtRight);
    }

    public void setBgtStudyTitles(String bgtStudyTitle) {
        bgtStudyTitles.add(bgtStudyTitle);
    }

    public void setBgtStudyNumbers(String bgtStudyNumber) {
        bgtStudyNumbers.add(bgtStudyNumber);
    }

    public void setBgtSites(String bgtSite) {
        bgtSites.add(bgtSite);
    }

    public void setBRows(int bRows) {
        this.bRows = bRows;
    }

    public int getBRows() {
        return this.bRows;
    }

    public ArrayList getBgtCombBgtFlags() {
        return this.bgtCombBgtFlags;
    }

    public void setBgtCombBgtFlags(String bgtCombBgtFlag) {
    	bgtCombBgtFlags.add(bgtCombBgtFlag);
    }


    public ArrayList getCodelstHideUnhide() {
        return this.codelstHideUnhides;
    }

    public void setCodelstHideUnhide(String codelstHideUnhide) {
    	codelstHideUnhides.add(codelstHideUnhide);
    }


    // end of getter and setter methods

    /**
     * Get budget details
     *
     * @param budgetId
     */

    public void getBudgetInfo(int budgetId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select budget_name, " + "budget_status, "
                    + "budget_CURRENCY, " + "a.site_name, " + "b.study_title, "
                    + "b.study_number , (select codelst_desc from sch_codelst where pk_codelst = fk_codelst_status) status_desc , fk_codelst_status,"
                    + "BUDGET_COMBFLAG "
                    + "from sch_budget, "
                    + "er_site a, " + "er_study b "
                    + "where sch_budget.pk_budget = ? "
                    + "and sch_budget.fk_site = a.pk_site (+) "
                    + "and sch_budget.fk_study = b.pk_study (+) ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetId);

            Rlog.debug("budget", "BudgetDao.getBudgetInfo SQL**** " + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            setBgtNames(rs.getString("budget_name"));
            setBgtStats(rs.getString("budget_status"));
            setBgtCurrs(rs.getString("budget_CURRENCY"));
            setBgtSites(rs.getString("site_name"));
            setBgtStudyTitles(rs.getString("study_title"));
            setBgtStudyNumbers(rs.getString("study_number"));
            setBgtCodeListStatuses(rs.getString("fk_codelst_status"));
            setBgtCodeListStatusesDescs(rs.getString("status_desc"));
            setBgtCombBgtFlags(rs.getString("BUDGET_COMBFLAG"));
            rows++;
            Rlog.debug("budget", "BudgetDao.getBudgetInfo rows " + rows);
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("budget",
                    "BudgetDao.getBudgetInfo EXCEPTION IN FETCHING FROM Users table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Get budgets for a study
     * @param userId
     * @param studyId
     */

    public void getCombBudgetForStudy(int userId, int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();

            sqlStr = " select a.PK_BUDGET, a.budget_name, a.budget_version "
                + " from sch_budget  a"
                + " where nvl(BUDGET_DELFLAG, 'N') != 'Y' "
                + " and BUDGET_COMBFLAG ='Y' and fk_study = " + studyId;

            pstmt = conn.prepareStatement(sqlStr);

            Rlog.debug("budget", "BudgetDao.getCombBudgetForStudy SQL**** "
                    + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtIds(new Integer(rs.getInt("PK_BUDGET")));
                setBgtNames(rs.getString("budget_name"));
                setBgtVersions(rs.getString("budget_version"));
                rows++;
            }

            Rlog.debug("budget", "BudgetDao.getCombBudgetForStudy rows " + rows);
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("budget",
                    "BudgetDao.getCombBudgetForStudy EXCEPTION IN FETCHING FROM Users table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }


    /**
     * Get budgets for user
     *
     * @param userId
     */

    public void getBudgetsForUser(int userId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();

            sqlStr = " select a.PK_BUDGET, a.budget_name, a.budget_version "
                    + " from sch_budget  a"
                    + " where a.pk_budget in (select d.fk_budget from sch_bgtusers d"
                    + "                        Where d.fk_user = " + userId
                    + " ) ";

            pstmt = conn.prepareStatement(sqlStr);
            // pstmt.setInt(1,userId);

            Rlog.debug("budget", "BudgetDao.getBudgetsForUsers SQL**** "
                    + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtIds(new Integer(rs.getInt("PK_BUDGET")));
                setBgtNames(rs.getString("budget_name"));
                setBgtVersions(rs.getString("budget_version"));
                rows++;
            }

            Rlog.debug("budget", "BudgetDao.getBudgetInfo rows " + rows);
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("budget",
                    "BudgetDao.getBudgetInfo EXCEPTION IN FETCHING FROM Users table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    //

    public int copyBudget(int oldBudget, String newBudName, String newBudVer,
            int creator, String ipAdd) {

        CallableStatement cstmt = null;
        CallableStatement cstmt1 = null;
        int success = -1;
        int retPK = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {

            conn = getConnection();

            cstmt1 = conn
                    .prepareCall("{call PKG_BGT.sp_create_copy_budget(?,?,?,?,?,?)}");
            cstmt1.setInt(1, oldBudget);

            cstmt1.setString(2, newBudName);

            cstmt1.setString(3, newBudVer);

            cstmt1.setInt(4, creator);

            cstmt1.setString(5, ipAdd);

            cstmt1.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt1.execute();

            retPK = cstmt1.getInt(6);

        }

        catch (SQLException ex) {
            Rlog.fatal("budget",
                    "In create copybudget EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int checkDupBudget(int accId, String newBudName, String newBudVer,
            String mode, int bgtId) {

        Rlog.debug("budget", "copy budget");
        CallableStatement cstmt = null;
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            Rlog.debug("budget", "in try block");
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_BGT.sp_check_duplicate(?,?,?,?,?,?)}");

            cstmt.setInt(1, accId);
            cstmt.setString(2, newBudName);
            cstmt.setString(3, newBudVer);
            cstmt.setString(4, mode);
            cstmt.setInt(5, bgtId);

            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();
            success = cstmt.getInt(6);

        }

        catch (SQLException ex) {
            Rlog.fatal("budget",
                    "In copybudget EXCEPTION IN calling the procedure" + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public int addStudyBudgetSections(int budgetId, int creator, String ipAdd) {

        Rlog.debug("budget", "addStudyBudgetSections");
        CallableStatement cstmt = null;
        int success = 0;

        Connection conn = null;
        try {
            Rlog.debug("budget", "in try block");
            conn = getConnection();
            cstmt = conn.prepareCall("{call PKG_BGT.sp_poststudybgt(?,?,?)}");
            cstmt.setInt(1, budgetId);
            cstmt.setInt(2, creator);
            cstmt.setString(3, ipAdd);
            cstmt.execute();
        }

        catch (SQLException ex) {
            Rlog.fatal("budget",
                    "In addStudyBudgetSections  EXCEPTION IN calling the procedure"
                            + ex);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    /**
     * Get budgets for user on the basis of the search criteria provided.
     *
     * @param userId
     */

    public void getBudgetsForUser(int userId, String searchCriteria) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();

            sqlStr = " select a.PK_BUDGET, a.budget_name, a.budget_version , (select codelst_desc from sch_codelst where pk_codelst = budget_template ) as budget_template, b.STUDY_NUMBER ,"
                    + " b.STUDY_TITLE,  c.SITE_NAME ,  decode(a.BUDGET_STATUS,'W','Work in Progress','F','Freeze','T','Template','null') BUDGET_STATUS, (select codelst_desc from sch_codelst where pk_codelst = fk_codelst_status) status_desc,fk_codelst_status  from sch_budget a, er_study b, er_site c "
                    + " where a.pk_budget in (select d.fk_budget from sch_bgtusers d"
                    + "                        Where d.fk_user = "
                    + userId
                    + " ) and "
                    + "a.FK_SITE=c.PK_SITE(+) and  a.FK_STUDY=b.PK_STUDY(+) and "
                    + "(lower(b.study_number) like lower('%"
                    + searchCriteria
                    + "%') or lower(b.study_keywrds) like lower('%"
                    + searchCriteria
                    + "%') or  lower(a.budget_name) like lower('%"
                    + searchCriteria + "%'))";

            pstmt = conn.prepareStatement(sqlStr);

            // pstmt.setInt(1,userId);
            // pstmt.setString(1,searchCriteria);

            Rlog.debug("budget", "BudgetDao.getBudgetsForUsers SQL**** "
                    + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setBgtIds(new Integer(rs.getInt("PK_BUDGET")));
                setBgtNames(rs.getString("budget_name"));
                setBgtVersions(rs.getString("budget_version"));
                setBgtTemplates(rs.getString("budget_template"));
                setBgtStats(rs.getString("BUDGET_STATUS"));
                setBgtSites(rs.getString("SITE_NAME"));
                setBgtStudyTitles(rs.getString("STUDY_TITLE"));
                setBgtStudyNumbers(rs.getString("STUDY_NUMBER"));
                setBgtCodeListStatuses(rs.getString("fk_codelst_status"));
                setBgtCodeListStatusesDescs(rs.getString("status_desc"));

                rows++;

            }

            Rlog.debug("budget", "BudgetDao.getBudgetInfo rows " + rows);
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("budget",
                    "BudgetDao EXCEPTION IN FETCHING FROM Users getbgtusers"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * changed by sonia sahni - 20 Jul 2004, fixed a bug where the deleted
     * templates were also getting displayed
     */

    /**
     * Populates the Data Access Object with Budget template ids and code ids
     * for creating a new budget
     *
     * @param userId
     *            User for which the list is required (Budget template access
     *            rights for this user are checked)
     * @param codeType
     *            code type (er_codelst) for Budget Type
     * @return boolean true if successful; false if unsuccessful
     *
     */
    public boolean getCodeValAndBgtTemplateIds(int userId, String codeType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select pk_budget || ':T' TEMPLATE_ID ,budget_name, 'N' as codelst_hide, 999999 as codelst_seq "
                            + " from sch_budget a "
                            + " where "
                            + " a.pk_budget in (select d.fk_budget from sch_bgtusers d where d.fk_user = ? ) "
                            + " and budget_status = 'T' and nvl(BUDGET_DELFLAG,'N') = 'N' "
                            + " union "
                            + " (select pk_codelst || case when cd.codelst_desc = 'Study' then ':S' when cd.codelst_desc = 'Patient' then ':P' when cd.codelst_desc = 'Comparative Budget' then ':C' end "
                            + " ,cd.codelst_desc,  codelst_hide, cd.codelst_seq "
                            + " from sch_codelst cd "
                            + " where " + " cd.codelst_type = ? " + " ) order by codelst_seq ");

            pstmt.setInt(1, userId);
            pstmt.setString(2, codeType);
            // pstmt.setInt(3,userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtDescs(rs.getString("BUDGET_NAME"));
                setBgtIdWithFlgs(rs.getString("TEMPLATE_ID"));
                setCodelstHideUnhide(rs.getString("codelst_hide"));

                rows++;
            }

            return true;
        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "EXCEPTION IN FETCHING getCodeValAndBgtTemplateIds TABLE "
                            + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Populates the Data Access Object with Budget template ids and code ids
     * of all patient and comparative budget templates for creating a new combined budget
     *(special check for 'Study Template' and 'Budget saved as study template' is put,
     * which is not there in above function)
     *
     * @param userId
     *            User for which the list is required (Budget template access
     *            rights for this user are checked)
     * @param codeType
     *            code type (er_codelst) for Budget Type
     * @return boolean true if successful; false if unsuccessful
     *
     */
    public boolean getBgtTemplatesForCombinedBgt(int userId, String codeType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select pk_budget || ':T' TEMPLATE_ID ,budget_name, 999999 as codelst_seq "
                            + " from sch_budget a "
                            + " where "
                            + " a.pk_budget in (select d.fk_budget from sch_bgtusers d where d.fk_user = ? ) "
                            + " and budget_status = 'T' and nvl(BUDGET_DELFLAG,'N') = 'N' "
                            + " and budget_type != 'S' "
                            + " union "
                            + " (select pk_codelst || case when cd.codelst_desc = 'Patient' then ':P' when cd.codelst_desc = 'Comparative Budget' then ':C' end "
                            + " ,cd.codelst_desc,  cd.codelst_seq "
                            + " from sch_codelst cd "
                            + " where " + " cd.codelst_type = ? "
                            + " and trim(cd.codelst_subtyp) != 'study_bgt' "
                            + " ) order by codelst_seq "
                            );

            pstmt.setInt(1, userId);
            pstmt.setString(2, codeType);
            // pstmt.setInt(3,userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtDescs(rs.getString("BUDGET_NAME"));
                setBgtIdWithFlgs(rs.getString("TEMPLATE_ID"));

                rows++;
            }

            return true;
        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "EXCEPTION IN FETCHING getBgtTemplatesForCombinedBgt TABLE "
                            + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Populates the Data Access Object with Budget template ids and code ids
     * of all patient and comparative budget templates for creating a new combined budget
     *(special check for 'Study Template' and 'Budget saved as study template' is put,
     * which is not there in above function)
     *
     * @param userId
     *            User for which the list is required (Budget template access
     *            rights for this user are checked)
     * @param codeType
     *            code type (er_codelst) for Budget Type
     * @return boolean true if successful; false if unsuccessful
     *
     */
    public boolean getSavedBgtTemplates(int userId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type

        try {
            conn = getConnection();
            pstmt = conn
                .prepareStatement("select pk_budget TEMPLATE_ID ,budget_name, 999999 as codelst_seq "
                    + " from sch_budget a "
                    + " where "
                    + " a.pk_budget in (select d.fk_budget from sch_bgtusers d where d.fk_user = ? ) "
                    + " and budget_status = 'T' and nvl(BUDGET_DELFLAG,'N') = 'N' "
                    + " and budget_type != 'S' and nvl(BUDGET_COMBFLAG,'N') = 'N' "
                    + " order by codelst_seq "
                    );

            pstmt.setInt(1, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtDescs(rs.getString("BUDGET_NAME"));
                setBgtIdWithFlgs(rs.getString("TEMPLATE_ID"));

                rows++;
            }

            return true;
        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "EXCEPTION IN FETCHING getSavedBgtTemplates TABLE "
                            + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }


    /**
     * Method to create copy of a budget
     *
     * @param Budget
     *            Id of the budget to be copied
     * @param Budget
     *            Id of budget to which the budget is to be copied
     * @param creator
     *            of the budget
     * @param ipAdd
     * @return 0 if successful
     */

    public int copyBudgetForTemplate(int oldBudget, int budgetId, int creator,
            String ipAdd) {

        String tempFlag = "T";
        Rlog.debug("budget", "copyBudgetForTemplate in Dao");
        CallableStatement cstmt = null;
        int success = -1;
        int retPK = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_BGT.sp_copy_budget(?,?,?,?,?,?)}");

            cstmt.setInt(1, oldBudget);
            cstmt.setInt(2, creator);
            cstmt.setString(3, ipAdd);
            cstmt.setInt(4, budgetId);
            cstmt.setString(5, tempFlag);

            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();
            success = cstmt.getInt(6);

        }

        catch (SQLException ex) {
            Rlog.fatal("budget",
                    "In copyBudgetForTemplate EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    public void getBgtTemplateSubType(int bgtTemplate, int budgetId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String subType = "";

        // set code type

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select nvl((select codelst_subtyp "
                            + "from sch_codelst  where codelst_type = 'budget_template' "
                            + " and pk_codelst = ? ), "
                            + " (select budget_name from sch_budget where pk_budget = ?)) as budget_template, "
                            + " nvl((select codelst_desc from sch_codelst  where codelst_type = 'budget_template' "
                            + " and pk_codelst = ? ), "
                            + " (select budget_name from sch_budget where pk_budget = ?)) as budget_desc, "
                            + "  budget_type "
                            + " from sch_budget where pk_budget=? ");

            pstmt.setInt(1, bgtTemplate);
            pstmt.setInt(2, bgtTemplate);
            pstmt.setInt(3, bgtTemplate);
            pstmt.setInt(4, bgtTemplate);
            pstmt.setInt(5, budgetId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtTemplates(rs.getString("budget_template"));
                setBgtDescs(rs.getString("budget_desc"));
                setBgtTypes(rs.getString("budget_type"));

                rows++;
            }

        } catch (SQLException ex) {
            Rlog.fatal("common",
                    "EXCEPTION IN FETCHING getBgtTemplateSubType TABLE " + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

	public ArrayList getBgtCodeListStatuses() {
		return bgtCodeListStatuses;
	}

	public void setBgtCodeListStatuses(ArrayList bgtCodeListStatuses) {
		this.bgtCodeListStatuses = bgtCodeListStatuses;
	}

	public void setBgtCodeListStatuses(String bgtCodeListStatus) {
		this.bgtCodeListStatuses.add(bgtCodeListStatus);
	}

	public ArrayList getBgtCodeListStatusesDescs() {
		return bgtCodeListStatusesDescs;
	}

	public void setBgtCodeListStatusesDescs(ArrayList bgtCodeListStatusesDescs) {
		this.bgtCodeListStatusesDescs = bgtCodeListStatusesDescs;
	}

	public void setBgtCodeListStatusesDescs(String bgtCodeListStatusesDesc) {
		this.bgtCodeListStatusesDescs.add(bgtCodeListStatusesDesc);
	}

    /**
     * Get Default Budget PK For a Calendar
     *
     * @param PKProtocol Protocol ID
     */

    public int getDefaultBudgetForCalendar(int PKProtocol) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        int budgetPK = 0;

        try {
            conn = getConnection();

            sqlStr = " select PK_BUDGET from sch_budget  a where nvl(budget_calendar,0)  = ? and nvl(BUDGET_DELFLAG, 'N') != 'Y' ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1,PKProtocol);



            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	budgetPK=rs.getInt("PK_BUDGET");//typically there will be only ONE such row for a calendar
                rows++;
            }


            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("budget",
                    "Exception in getDefaultBudgetForCalendar(int PKProtocol) "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return budgetPK;

    }

    // end of class
}
