/*
 * Classname : EntityAdapter
 * 
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 *
 * Author: from eClinical
 * Base class for all the Entity Beans in the Application
 */

package com.velos.esch.business.common;

/* Import Statements */
import java.sql.Connection;

import javax.ejb.EJBException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.velos.esch.service.util.Configuration;
import com.velos.esch.service.util.EnvUtil;
import com.velos.esch.service.util.Rlog;

/* Import Statements */

public class EntityAdapter implements EntityBean {

    protected EntityContext ejbContext;

    public void ejbStore() throws EJBException {
    }

    public void ejbLoad() throws EJBException {
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void ejbRemove() {
    }

    public void setEntityContext(EntityContext cntx) {
        ejbContext = cntx;
    }

    public EntityContext getEJBContext() {
        return ejbContext;
    }

    public void unsetEntityContext() {
        ejbContext = null;
    }

    /**
     * This method gets the connection object from connection pool.
     * 
     * @return Connection
     */
    public Connection getConnection() {
        InitialContext iCtx = null;
        Connection conn = null;
        Configuration conf = null;

        Rlog.debug("common", "GetConnection Function Call");
        try {
            iCtx = new InitialContext(EnvUtil.getContextProp());
            // Rlog.debug("common","INITIAL CONTECXT:" + iCtx +"conf Path"
            // +JNDINames.ERESEARCH_CONF_PATH);
            if (conf.dsJndiName == null) {
                conf.readSettings();
            }
            Rlog.debug("common", "JNDI NAME DS NAME" + conf.dsJndiName);
            DataSource ds = (DataSource) iCtx.lookup(conf.dsJndiName);
            Rlog.debug("common", "DATASOURCE " + ds);
            return ds.getConnection();
        } catch (NamingException ne) {
            Rlog.fatal("common", this.toString()
                    + ":getConnection():Naming exception:" + ne);
            return null;
        } catch (Exception se) {
            Rlog.fatal("common", this.toString()
                    + ":getConnection():Sql exception:" + se);
            return null;
        } finally {
            try {
                if (iCtx != null)
                    iCtx.close();
            } catch (NamingException ne) {
                Rlog.fatal("common", this.toString()
                        + ":getConnection():context close exception:" + ne);
            }
        }
    }

}