/*
 * Classname	BudgetUsersDao.class
 * 
 * Version information 	1.0
 *
 * Date	03/26/2002
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

/**
 * budgetUsersDao for getting users who have/don't have rights for the budget
 * 
 * @author Sonika
 * @version : 1.0 03/26/2002
 */

public class BudgetUsersDao extends CommonDAO implements java.io.Serializable {
    private ArrayList bgtUsersIds;

    private ArrayList bgtUsersBudgets;

    private ArrayList bgtUsers;

    private ArrayList bgtUsersRights;

    private ArrayList userIds;

    private ArrayList usrLastNames;

    private ArrayList usrFirstNames;

    private ArrayList usrSiteNames;

    private ArrayList bgtUsersType;

    private ArrayList usr;

    private int bRows;

    public BudgetUsersDao() {
        bgtUsersIds = new ArrayList();
        bgtUsersBudgets = new ArrayList();
        bgtUsers = new ArrayList();
        bgtUsersRights = new ArrayList();
        userIds = new ArrayList();
        usrLastNames = new ArrayList();
        usrFirstNames = new ArrayList();
        usrSiteNames = new ArrayList();
        bgtUsersType = new ArrayList();
        usr = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getBgtUsersIds() {
        return this.bgtUsersIds;
    }

    public void setBgtUsersIds(ArrayList bgtUsersIds) {
        this.bgtUsersIds = bgtUsersIds;
    }

    public ArrayList getBgtUsersBudgets() {
        return this.bgtUsersBudgets;
    }

    public void setBgtUsersBudgets(ArrayList bgtUsersBudgets) {
        this.bgtUsersBudgets = bgtUsersBudgets;
    }

    public ArrayList getBgtUsers() {
        return this.bgtUsers;
    }

    public void setBgtUsers(ArrayList bgtUsers) {
        this.bgtUsers = bgtUsers;
    }

    public ArrayList getBgtUsersRights() {
        return this.bgtUsersRights;
    }

    public void setBgtUsersRights(ArrayList bgtUsersRights) {
        this.bgtUsersRights = bgtUsersRights;
    }

    public ArrayList getUserIds() {
        return this.userIds;
    }

    public void setUserIds(ArrayList userIds) {
        this.userIds = userIds;
    }

    public ArrayList getUsrLastNames() {
        return this.usrLastNames;
    }

    public void setUsrLastNames(ArrayList usrLastNames) {
        this.usrLastNames = usrLastNames;
    }

    public ArrayList getUsrFirstNames() {
        return this.usrFirstNames;
    }

    public void setUsrFirstNames(ArrayList usrFirstNames) {
        this.usrFirstNames = usrFirstNames;
    }

    public ArrayList getUsrSiteNames() {
        return this.usrSiteNames;
    }

    public void setUsrSiteNames(ArrayList usrSiteNames) {
        this.usrSiteNames = usrSiteNames;
    }

    public ArrayList getUsr() {
        return this.usr;
    }

    public void setUsr(ArrayList usr) {
        this.usr = usr;
    }

    public ArrayList getBgtUsersType() {
        return this.bgtUsersType;
    }

    public void setBgtUsersType(ArrayList bgtUsersType) {
        this.bgtUsersType = bgtUsersType;
    }

    public void setBgtUsersType(String bgtUsersType) {
        this.bgtUsersType.add(bgtUsersType);
    }

    public void setBgtUsersIds(String bgtUsersId) {
        bgtUsersIds.add(bgtUsersId);
    }

    public void setBgtUsersBudgets(String bgtUsersBudget) {
        bgtUsersBudgets.add(bgtUsersBudget);
    }

    public void setBgtUsers(String bgtUser) {
        bgtUsers.add(bgtUser);
    }

    public void setBgtUsersRights(String bgtUsersRight) {
        bgtUsersRights.add(bgtUsersRight);
    }

    public void setUserIds(String userId) {
        userIds.add(userId);
    }

    public void setUsrLastNames(String usrLastName) {
        usrLastNames.add(usrLastName);
    }

    public void setUsrFirstNames(String usrFirstName) {
        usrFirstNames.add(usrFirstName);
    }

    public void setUsrSiteNames(String usrSiteName) {
        usrSiteNames.add(usrSiteName);
    }

    public void setUsr(String usr) {
        this.usr.add(usr);
    }

    public void setBRows(int bRows) {
        this.bRows = bRows;
    }

    public int getBRows() {
        return this.bRows;
    }

    // end of getter and setter methods

    /**
     * Returns User objects for the users who don't have access rights for the
     * budget
     * 
     * @param accountId
     *            account of the logged in User
     * @param budgetId
     *            selected Budget
     * @param lName
     *            Last Name of the user
     * @param fName
     *            First Name of the user
     * @param organization
     *            Organization of the user
     * @param userType
     *            type of the budget users
     */

    public void getUsrNotInBudget(int accountId, int budgetId, String lName,
            String fName, String role, String organization, String userType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String lWhere = null;
        String fWhere = null;
        String sqlStr = "";
        StringBuffer completeSelect = new StringBuffer();
        try {
            conn = getConnection();
            sqlStr = "select a.pk_user, "
                    + "a.usr_lastname, "
                    + "(select SITE_NAME from er_site c where c.PK_SITE  = a.FK_SITEID) as site_name, "
                    + "a.usr_firstname "
                    + "from er_user a "
                    + "where a.fk_account= ? "
                    + "and a.usr_stat in ('A','B') "
                    + "and a.usr_type <> 'X' "
                    + "and a.pk_user not in (select fk_user from sch_bgtusers where bgtusers_type=? and fk_budget= ? ) ";

            if ((fName != null) && (!fName.trim().equals(""))) {
                fWhere = " and UPPER(a.USR_FIRSTNAME) like UPPER('";
                fWhere += fName;
                fWhere += "%')";
            }

            if ((lName != null) && (!lName.trim().equals(""))) {
                lWhere = " and UPPER(a.USR_LASTNAME) like UPPER('";
                lWhere += lName;
                lWhere += "%')";
            }

            if ((organization != null) && (!organization.trim().equals(""))) {
                fWhere = " and UPPER(a.FK_SITEID) = UPPER(";
                fWhere += organization;
                fWhere += ")";
            }

            if ((role != null) && (!role.trim().equals(""))) {
                fWhere = " and UPPER(a.FK_CODELST_JOBTYPE) = UPPER(";
                fWhere += role;
                fWhere += ")";
            }

            completeSelect.append(sqlStr);

            if (fWhere != null) {
                completeSelect.append(fWhere);
            }

            if (lWhere != null) {
                completeSelect.append(lWhere);
            }

            pstmt = conn.prepareStatement(completeSelect.toString());
            pstmt.setInt(1, accountId);
            pstmt.setString(2, userType);
            pstmt.setInt(3, budgetId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUsr(rs.getString("pk_user"));
                setUsrLastNames(rs.getString("usr_lastname"));
                setUsrFirstNames(rs.getString("usr_firstname"));
                setUsrSiteNames(rs.getString("SITE_NAME"));
                rows++;
            }

            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("BudgetUser",
                    "BudgetUser.getUsrNotInBudget EXCEPTION IN FETCHING FROM Users table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets all Users who have access rights for the budget
     * 
     * @param budgetId
     */

    public void getUsrInBudget(int budgetId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select pk_bgtusers, "
                    + "a.usr_lastname, "
                    + "(select SITE_NAME from er_site c where c.PK_SITE  = a.FK_SITEID) as site_name, "
                    + "a.usr_firstname, " + "b.bgtusers_rights, "
                    + "to_char(a.pk_user) userId "
                    + "from er_user a, sch_bgtusers b "
                    + "where a.pk_user= b.fk_user "
                    + "and b.fk_budget= ? and bgtusers_type='I' "
                    + "and a.usr_stat in('A','B') ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtUsers(rs.getString("pk_bgtusers"));
                setBgtUsersRights(rs.getString("pk_bgtusers"));
                setUsrLastNames(rs.getString("usr_lastname"));
                setUsrSiteNames(rs.getString("SITE_NAME"));
                setUsrFirstNames(rs.getString("usr_firstname"));
                setUserIds(rs.getString("userId"));
                rows++;
            }

            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("BudgetUser",
                    "BudgetUser.getUsrInBudget EXCEPTION IN FETCHING FROM BudgetUsers table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets User Rights for the Budget
     * 
     * @param budgetId
     * @param userId
     * @return String of rights, null in case of exception
     */

    public String getBudgetUserRight(int budgetId, int userId) {// first get the
        // individual
        // right. If
        // there's no
        // value
        // for individual right then we should check the global right for user
        // Individual right has high priority
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        String right = "";
        String usrType = "";
        try {
            conn = getConnection();
            sqlStr = "select bgtusers_rights, bgtusers_type "
                    + "from sch_bgtusers  "
                    + "where fk_budget= ? and bgtusers_type='I' "
                    + "and fk_user= ? " + "union "
                    + "select bgtusers_rights, bgtusers_type "
                    + "from sch_bgtusers  "
                    + "where fk_budget= ? and bgtusers_type='G' "
                    + "and fk_user= ? order by bgtusers_type desc  ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetId);
            pstmt.setInt(2, userId);
            pstmt.setInt(3, budgetId);
            pstmt.setInt(4, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                right = rs.getString("bgtusers_rights");
                usrType = rs.getString("bgtusers_type");
                if (usrType.equals("I"))
                    break;
            }
            return right;

        } catch (SQLException ex) {
            Rlog.fatal("BudgetUser",
                    "BudgetUser.getBudgetUserRight EXCEPTION IN FETCHING FROM BudgetUsers table"
                            + ex);
            return null;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

} // end of class

