/*
 * Classname : EventStatBean
 *
 * Version information: 1.0
 *
 * Date: 08/27/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.business.eventstat.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */

@Entity
@Table(name = "SCH_EVENTSTAT")
public class EventStatBean implements Serializable {

    private static final long serialVersionUID = 3618700816186357304L;

    /**
     * pk of the table PK_EVENTSTAT
     */

    public int evtStatId; //PK_EVENTSTAT

    /**
     * EVENTSTAT_DT
     */

    public Date evtStatDate; //EVENTSTAT_DT


    /**
     * EVENTSTAT_NOTES
     */

    public String  evtStatNotes; //EVENTSTAT_NOTES


    /**
     * FK_EVENT
     */
    public String evt_fk; //FK_EVENT

    /**
     * EVENTSTAT
     */

    public Integer evtStat; //EVENTSTAT

    /**
     * EVENTSTAT_ENDDT
     */

    public Date evtStatEndDate; //EVENTSTAT_ENDDT


    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;



    //empty constructor...
    public EventStatBean() {

    }

    public EventStatBean( int evtStatId, String evtStatDate, String evtStatNotes, String evt_fk, String evtStat,
    		String evtStatEndDate, String creator, String modifiedBy, String ipAdd ) {
        super();
        // TODO Auto-generated constructor stub
        setEvtStatId(evtStatId);
        setEventStatDate(evtStatDate);
        setEvtStatNotes(evtStatNotes);
        setFkEvtStat(evt_fk);
        setEvtStatus(evtStat);
        setEventStatEndDate(evtStatEndDate);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SCH_EVENTSTAT_SEQ", allocationSize=1)
    @Column(name = "PK_EVENTSTAT")
    public int getEvtStatId() {
        return this.evtStatId;
    }

    public void setEvtStatId(int evtStatId) {
        this.evtStatId = evtStatId;
    }


    /**
     * Gets Event Stat date
     *
     * @return event status date
     */
    @Column(name = "EVENTSTAT_DT")
    public Date getEventStatDt() {
        return this.evtStatDate;
    }

    /**
     * Sets Event Status date
     *
     * @param evtStatDate
     *           Event Status date
     */

    public void setEventStatDt(Date evtStatDate) {
        this.evtStatDate = evtStatDate;

    }

    @Transient
    public String getEventStatDate() {
        return DateUtil.dateToString(getEventStatDt());
    }

    /**
     * Sets Event Status date
     *
     * @param evtStatDate
     *            Event Status date
     */
    public void setEventStatDate(String evtStatDate) {
    	//setEventStatDt(DateUtil.stringToDate(evtStatDate, "MM/dd/yyyy"));
    	setEventStatDt(DateUtil.stringToDate(evtStatDate, ""));

    }

    /**
     * Gets event Status Notes
     *
     * @return event Status Notes
     */
    @Column(name = "EVENTSTAT_NOTES")
    public String getEvtStatNotes() {
        return this.evtStatNotes;
    }

    /**
     * Sets event Status Notes
     *
     * @param evtStatNotes
     *           event Status Notes
     */
    public void setEvtStatNotes(String evtStatNotes) {
        this.evtStatNotes = evtStatNotes;
    }


    @Column(name = "FK_EVENT")
    public String getFkEvtStat() {
    	return this.evt_fk;
    }

    public void setFkEvtStat(String evt_fk) {
        	this.evt_fk = evt_fk;
    }


    @Column(name = "EVENTSTAT")
    public String getEvtStatus() {
    	return StringUtil.integerToString(this.evtStat);
    }

    public void setEvtStatus(String evtStat) {

        if (evtStat != null) {
        	this.evtStat = Integer.valueOf(evtStat);
        }
    }



    /**
     * Gets Event Stat End date
     *
     * @return event status End date
     */
    @Column(name = "EVENTSTAT_ENDDT")
    public Date getEventStatEndDt() {
        return this.evtStatEndDate;
    }

    /**
     * Sets Event Status End date
     *
     * @param evtStatEndDate
     *           Event Status End date
     */

    public void setEventStatEndDt(Date evtStatEndDate) {
        this.evtStatEndDate = evtStatEndDate;

    }

    @Transient
    public String getEventStatEndDate() {
        return DateUtil.dateToString(getEventStatEndDt());
    }

    /**
     * Sets Event Status End date
     *
     * @param evtStatEndDate
     *            Event Status End date
     */
    public void setEventStatEndDate(String evtStatEndDate) {
    	//setEventStatEndDt(DateUtil.stringToDate(evtStatEndDate, "MM/dd/yyyy"));
    	setEventStatEndDt(DateUtil.stringToDate(evtStatEndDate, ""));

    }


    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    // END OF GETTER SETTER METHODS



    /**
     * updates the Event status details
     *
     * @param evresSk
     * @return 0 if successful; -2 for exception
     */
    public int updateEventStat(EventStatBean evtstsk) {
        try {

        	setEvtStatId(evtstsk.getEvtStatId());
            setEventStatDate(evtstsk.getEventStatDate());
            setEvtStatNotes(evtstsk.getEvtStatNotes());
            setFkEvtStat(evtstsk.getFkEvtStat());
            setEvtStatus(evtstsk.getEvtStatus());
            setEventStatEndDate(evtstsk.getEventStatEndDate());
            setCreator(evtstsk.getCreator());
            setModifiedBy(evtstsk.getModifiedBy());
            setIpAdd(evtstsk.getIpAdd());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("eventstatus", " error in EventStatBean.updateEventStat : " + e);
            return -2;
        }
    }
}// end of class
