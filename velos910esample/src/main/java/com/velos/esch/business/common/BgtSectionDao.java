/*
 * Classname	BgtSectionDao.class
 * 
 * Version information 	1.0
 *
 * Date	04/10/2002
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;

/**
 * BgtSectionDao for getting budget sections
 * 
 * @author Sonika
 * @version : 1.0 04/10/2002 Modified By: Arvind on 04/11/2002
 */

public class BgtSectionDao extends CommonDAO implements java.io.Serializable {
    private ArrayList bgtSectionIds;

    private ArrayList bgtCals;

    private ArrayList bgtSectionNames;

    private ArrayList bgtSectionVisits;

    private ArrayList bgtSectionDelFlags;

    private ArrayList bgtSectionSequences;

    private ArrayList bgtSectionPatNos;

    private ArrayList bgtSectionPersonlFlags;

    private ArrayList bgtSectionTypes;

    private ArrayList bgtSectionNotes;

    private ArrayList bgtSectionTypeDescs;

    private int sRows;
    
    private ArrayList bgtCalNames;

    private ArrayList arFKVisits;

    
    public ArrayList getBgtCalNames() {
		return bgtCalNames;
	}

	public void setBgtCalNames(ArrayList bgtCalNames) {
		this.bgtCalNames = bgtCalNames;
	}

	public void setBgtCalNames(String bgtCalName) {
		this.bgtCalNames.add(bgtCalName);
	}

	
	public BgtSectionDao() {
        bgtSectionIds = new ArrayList();
        bgtCals = new ArrayList();
        bgtSectionNames = new ArrayList();
        bgtSectionVisits = new ArrayList();
        bgtSectionDelFlags = new ArrayList();
        bgtSectionSequences = new ArrayList();
        bgtSectionPatNos = new ArrayList();
        bgtSectionPersonlFlags = new ArrayList();
        bgtSectionTypes = new ArrayList();
        bgtSectionNotes = new ArrayList();
        bgtSectionTypeDescs = new ArrayList();
        bgtCalNames = new ArrayList();
        arFKVisits = new ArrayList();

    }

    // Getter and Setter methods

    public ArrayList getBgtSectionIds() {
        return this.bgtSectionIds;
    }

    public void setBgtSectionIds(ArrayList bgtSectionIds) {
        this.bgtSectionIds = bgtSectionIds;
    }

    public void setBgtSectionIds(Integer bgtSectionId) {
        this.bgtSectionIds.add(bgtSectionId);
    }
    
    public void setBgtSectionIds(String bgtSectionId) {
        this.bgtSectionIds.add(bgtSectionId);
    }

    public ArrayList getBgtCals() {
        return this.bgtCals;
    }

    public void setBgtCals(ArrayList bgtCals) {
        this.bgtCals = bgtCals;
    }

    public void setBgtCals(String bgtCal) {
        this.bgtCals.add(bgtCal);
    }

    public ArrayList getBgtSectionNames() {
        return this.bgtSectionNames;
    }

    public void setBgtSectionNames(ArrayList bgtSectionName) {
        this.bgtSectionNames = bgtSectionNames;
    }

    public void setBgtSectionNames(String bgtSectionName) {
        this.bgtSectionNames.add(bgtSectionName);
    }

    public ArrayList getBgtSectionVisits() {
        return this.bgtSectionVisits;
    }

    public void setBgtSectionVisits(ArrayList bgtSectionVisits) {
        this.bgtSectionVisits = bgtSectionVisits;
    }

    public void setBgtSectionVisits(String bgtSectionVisit) {
        this.bgtSectionVisits.add(bgtSectionVisit);
    }

    public ArrayList getBgtSectionDelFlags() {
        return this.bgtSectionDelFlags;
    }

    public void setBgtSectionDelFlags(ArrayList bgtSectionDelFlags) {
        this.bgtSectionDelFlags = bgtSectionDelFlags;
    }

    public void setBgtSectionDelFlags(String bgtSectionDelFlag) {
        this.bgtSectionDelFlags.add(bgtSectionDelFlag);
    }

    public ArrayList getBgtSectionSequences() {
        return this.bgtSectionSequences;
    }

    public void setBgtSectionSequences(ArrayList bgtSectionSequences) {
        this.bgtSectionSequences = bgtSectionSequences;
    }

    public void setBgtSectionSequences(String bgtSectionSequence) {
        this.bgtSectionSequences.add(bgtSectionSequence);
    }

    public ArrayList getBgtSectionPatNos() {
        return this.bgtSectionPatNos;
    }

    public void setBgtSectionPatNos(ArrayList bgtSectionPatNos) {
        this.bgtSectionPatNos = bgtSectionPatNos;
    }

    public void setBgtSectionPatNos(String bgtSectionPatNo) {
        this.bgtSectionPatNos.add(bgtSectionPatNo);
    }

    public ArrayList getBgtSectionPersonlFlags() {
        return this.bgtSectionPersonlFlags;
    }

    public void setBgtSectionPersonlFlags(ArrayList bgtSectionPersonlFlags) {
        this.bgtSectionPersonlFlags = bgtSectionPersonlFlags;
    }

    public void setBgtSectionPersonlFlags(String bgtSectionPersonlFlag) {
        this.bgtSectionPersonlFlags.add(bgtSectionPersonlFlag);
    }

    public ArrayList getBgtSectionTypes() {
        return this.bgtSectionTypes;
    }

    public void setBgtSectionTypes(ArrayList bgtSectionTypes) {
        this.bgtSectionTypes = bgtSectionTypes;
    }

    public void setBgtSectionTypes(String bgtSectionType) {
        this.bgtSectionTypes.add(bgtSectionType);
    }

    public ArrayList getBgtSectionNotes() {
        return this.bgtSectionNotes;
    }

    public void setBgtSectionNotes(ArrayList bgtSectionNotes) {
        this.bgtSectionNotes = bgtSectionNotes;
    }

    public void setBgtSectionNotes(String bgtSectionNote) {
        this.bgtSectionNotes.add(bgtSectionNote);
    }

    public ArrayList getBgtSectionTypeDescs() {
        return this.bgtSectionTypeDescs;
    }

    public void setBgtSectionTypeDescs(ArrayList bgtSectionTypeDescs) {
        this.bgtSectionTypeDescs = bgtSectionTypeDescs;
    }

    public void setBgtSectionTypeDescs(String bgtSectionTypeDesc) {
        this.bgtSectionTypeDescs.add(bgtSectionTypeDesc);
    }

    public void setSRows(int sRow) {
        this.sRows = sRow;
    }

    public int getSRows() {
        return this.sRows;
    }

    // end of getter and setter methods

    /**
     * Gets all sections within the budget protocol calendar
     * 
     * @param calId
     *            calendar Id associated with the budget
     */
    public void getBgtSections(int calId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            //KM-#5286
            sqlStr = "select pk_budgetsec, "
                    + "BGTSECTION_NAME, "
                    + "BGTSECTION_VISIT, "
                    + "BGTSECTION_SEQUENCE, "
                    + "BGTSECTION_PATNO, "
                    + "BGTSECTION_PERSONLFLAG, "
                    + "BGTSECTION_TYPE, "
                    + "case when BGTSECTION_TYPE='P' then 'Per Patient Fees' when BGTSECTION_TYPE='O'  then 'One time Fees' end as BGTSECTION_TYPEDESC, "
                    + "BGTSECTION_NOTES ,FK_VISIT "
                    + "from sch_bgtsection "
                    + "where fk_bgtcal= ? "
                    + "and nvl(bgtsection_delflag,'N')='N' order by BGTSECTION_SEQUENCE,fk_visit ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, calId);

            Rlog.debug("bgtSection", "getBgtSections SQL**** " + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtSectionIds(new Integer(rs.getInt("pk_budgetsec")));
                setBgtSectionNames(rs.getString("BGTSECTION_NAME"));
                setBgtSectionVisits(rs.getString("BGTSECTION_VISIT"));
                setBgtSectionSequences(rs.getString("BGTSECTION_SEQUENCE"));
                setBgtSectionPatNos(rs.getString("BGTSECTION_PATNO"));
                setBgtSectionPersonlFlags(rs
                        .getString("BGTSECTION_PERSONLFLAG"));
                setBgtSectionTypes(rs.getString("BGTSECTION_TYPE"));
                setBgtSectionTypeDescs(rs.getString("BGTSECTION_TYPEDESC"));
                setBgtSectionNotes(rs.getString("BGTSECTION_NOTES"));
                
                setArFKVisits(rs.getString("FK_VISIT"));
            
                rows++;
                Rlog.debug("bgtSection", "getBgtSections rows " + rows);
            }

            setSRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("bgtSection",
                    "getBgtSections EXCEPTION IN FETCHING FROM Budget Section table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * Method to copy all the lineitems of default section whose inclusion type
     * is 0 to section
     */

    public int insertDefaultSecLineitems(int bgtSecId, int bgtCalId,
            int creator, String ipAdd) {
        Rlog.debug("bgtSection", "insertDefaultSecLineitems in dao");

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("bgtSection", "insertDefaultSecLineitems in dao conn"
                    + conn);

            cstmt = conn
                    .prepareCall("{call PKG_BGT.SP_INSERT_DEFAULT_SEC_LINEITMS(?,?,?,?,?)}");

            cstmt.setInt(1, bgtSecId);

            cstmt.setInt(2, bgtCalId);

            cstmt.setInt(3, creator);

            cstmt.setString(4, ipAdd);

            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(5);
            Rlog.debug("bgtSection", "insertDefaultSecLineitems in dao ret"
                    + ret);

            return ret;

        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "bgtSection",
                            "In insertDefaultSecLineitems in bgtsectiondao line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }
    
    //////////////
    /**
     * Get all the sections (with calendar names) for a budget
     * 
     * @param budgetId
     */
    /* modified by sonia abrol, 01/30/06, to get only visible sections (filter hidden and deleted sections)*/
    public void getAllBgtSections(int budgetId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlStr = new StringBuffer();
        try {
            conn = getConnection();
            sqlStr = sqlStr.append("select pk_bgtcal, BGTCAL_PROTID, name, pk_budgetsec, bgtsection_name,event_assoc.fk_visit  "); 
            		sqlStr.append(" from sch_bgtcal, event_assoc, sch_bgtsection where fk_budget = ? ");
            		sqlStr.append(" and  BGTCAL_PROTID = EVENT_ID and nvl(sch_bgtcal.BGTCAL_DELFLAG,'N') <> 'Y' ");
            		sqlStr.append(" and fk_bgtcal =  pk_bgtcal and nvl(BGTSECTION_DELFLAG,'N') = 'N' UNION ");
            		sqlStr.append(" select pk_bgtcal, BGTCAL_PROTID, name , pk_budgetsec, bgtsection_name,event_def.fk_visit ");
            		sqlStr.append(" from sch_bgtcal, event_def , sch_bgtsection  where fk_budget = ? ");
            		sqlStr.append(" and  BGTCAL_PROTID = EVENT_ID " );
            		sqlStr.append(" and nvl(sch_bgtcal.BGTCAL_DELFLAG,'N') <> 'Y' ");
            		sqlStr.append(" and fk_bgtcal =  pk_bgtcal  and nvl(BGTSECTION_DELFLAG,'N') = 'N' UNION ");
            		sqlStr.append(" select pk_bgtcal, 0, 'Baseline' , pk_budgetsec, bgtsection_name, null from sch_bgtcal , sch_bgtsection   ");
            		sqlStr.append(" where fk_budget = ?  and nvl(BGTCAL_DELFLAG,'Z')<>'Y' ");
            		sqlStr.append(" and BGTCAL_PROTID is null and fk_bgtcal =  pk_bgtcal  and nvl(BGTSECTION_DELFLAG,'N') = 'N' order by 1 ");

         
        		   
            pstmt = conn.prepareStatement(sqlStr.toString());
            pstmt.setInt(1, budgetId);
            pstmt.setInt(2, budgetId);
            pstmt.setInt(3, budgetId);
           

            Rlog.debug("budgetcal", "bgtsectiondao.getAllBgtSections SQL**** "
                    + sqlStr.toString());

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                this.setBgtCals(rs.getString("BGTCAL_PROTID"));
                this.setBgtCalNames(rs.getString("name"));
                this.setBgtSectionIds(rs.getString("pk_budgetsec"));
                this.setBgtSectionNames(rs.getString("bgtsection_name"));
                this.setArFKVisits(rs.getString("FK_VISIT"));
                
                rows++;
                Rlog.debug("budgetcal", "bgtsectiondao.getAllBgtSections  rows "
                        + rows);
            }
            
        } catch (SQLException ex) {
            Rlog.fatal("budgetcal",
                    "bgtsectiondao.getAllBgtSections EXCEPTION IN FETCHING FROM "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void setArFKVisits(String arFKVisit) {
		this.arFKVisits.add(arFKVisit);
	}
	
    
	public ArrayList getArFKVisits() {
		return arFKVisits;
	}

	public void setArFKVisits(ArrayList arFKVisits) {
		this.arFKVisits = arFKVisits;
	}

/** 
 * Update patient number for all per patient sections in a budget
 *  */
	public int  applyNumberOfPatientsToAllSections(String sourceSectionPK,String bgtCalPK,String patNo,String last_modified_by,String ipAdd) {
         
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlStr = new StringBuffer();
        int ret = 0;
        try {
            conn = getConnection();
            sqlStr = sqlStr.append(" update sch_bgtsection set bgtsection_patno = ? , last_modified_by = ? , ip_add = ? ");
            		sqlStr.append(" where fk_bgtcal = ?  and pk_budgetsec <> ? and bgtsection_type='P' and nvl(bgtsection_delflag,'N') in ('N','M') ");

         
        		   
            pstmt = conn.prepareStatement(sqlStr.toString());
            pstmt.setString(1, patNo);
            pstmt.setString(2, last_modified_by);
            pstmt.setString(3, ipAdd);
            pstmt.setString(4, bgtCalPK);
            pstmt.setString(5, sourceSectionPK);
           

            
            ret= pstmt.executeUpdate();
            conn.commit();
            
        } catch (SQLException ex) {
            Rlog.fatal("budgetcal",
                    "bgtsectiondao.applyNumberOfPatientsToAllSections EXCEPTION "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            
        }
        return ret;
        

    }

} // end of class
