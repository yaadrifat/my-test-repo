/**Created by Jnanamay Majumdar dt. 19Jan2005
 This dao is for deleting the adverse event from the db.
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.Statement;

import com.velos.esch.service.util.Rlog;

public class AdvEveDao extends CommonDAO implements java.io.Serializable {

    public int reomoveAdvEve(int advId) {

        Connection conn = null;
        // PreparedStatement pstmt=null;
        // ResultSet rs=null;
        Statement st = null;

        try {

            conn = getConnection();
            st = conn.createStatement();
            // String temp="";
            String sql1 = "";
            String sql2 = "";
            String sql3 = "";

            sql1 = " delete from SCH_ADVINFO where FK_ADVERSE=" + advId;
            sql2 = " delete from SCH_ADVNOTIFY where FK_ADVERSEVE=" + advId;
            sql3 = "delete from SCH_ADVERSEVE where PK_ADVEVE=" + advId;

            st.addBatch(sql1);
            st.addBatch(sql2);
            st.addBatch(sql3);

            Rlog.debug("advEve", "sql in reomoveAdvEve" + sql1);
            Rlog.debug("advEve", "sql in reomoveAdvEve" + sql2);
            Rlog.debug("advEve", "sql in reomoveAdvEve" + sql3);

            // pstmt=conn.prepareStatement(sql);
            // pstmt.clearParameters();
            // pstmt.executeUpdate();

            st.executeBatch();

            conn.commit();

        } catch (Exception e) {
            Rlog
                    .fatal("advEve", "{EXCEPTION IN advEveDao:reomoveAdvEve()}"
                            + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -1;

        } finally {
            try {
                if (st != null)
                    st.close();
                ;
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 1;

    }

}// end of class

