/*
 * Classname	BudgetcalDao.class
 * 
 * Version information 	1.0
 *
 * Date	04/04/2002
 * 
 * Author Sajal
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.aithent.audittrail.reports.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.web.budgetcal.BudgetcalJB;

public class BudgetcalDao extends CommonDAO implements java.io.Serializable {
    private ArrayList bgtcalIds;

    private ArrayList bgtcalBudgetIds;

    private ArrayList bgtcalProtIds;

    private ArrayList bgtcalProtTypes;

    private ArrayList bgtcalDelFlags;

    private ArrayList bgtcalSponsorOHeads;

    private ArrayList bgtcalClinicOHeads;

    private ArrayList bgtcalSponsorFlags;

    private ArrayList bgtcalClinicFlags;

    private ArrayList bgtcalIndirectCosts;

    private ArrayList bgtcalProtNames;
    
    private ArrayList budgetResearchCost;
    
    private ArrayList budgetResearchTotalCost;
    
    private ArrayList budgetSOCCost;
    
    private ArrayList budgetSOCTotalCost;
    
    private ArrayList budgetTotalCost;
    
    private ArrayList budgetTotalTotalCost;
    
    private ArrayList budgetSalaryCost;
    
    private ArrayList budgetSalaryTotalCost;
    
    private ArrayList budgetFringeCost;
    
    private ArrayList budgetFringeTotalCost;
    
    private ArrayList budgetDiscontinueCost;
    
    private ArrayList budgetDiscontinueTotalCost;
    
    private ArrayList budgetIndirectCost;
    
    private ArrayList budgetIndirectTotalCost;
    
    
    private int bRows;

    public BudgetcalDao() {
        bgtcalIds = new ArrayList();
        bgtcalBudgetIds = new ArrayList();
        bgtcalProtIds = new ArrayList();
        bgtcalProtTypes = new ArrayList();
        bgtcalDelFlags = new ArrayList();
        bgtcalSponsorOHeads = new ArrayList();
        bgtcalClinicOHeads = new ArrayList();
        bgtcalSponsorFlags = new ArrayList();
        bgtcalClinicFlags = new ArrayList();
        bgtcalIndirectCosts = new ArrayList();
        bgtcalProtNames = new ArrayList();
        budgetResearchCost = new ArrayList(); 
        budgetResearchTotalCost = new ArrayList();
        budgetSOCCost = new ArrayList();
        budgetSOCTotalCost = new ArrayList();
        budgetTotalCost = new ArrayList();
        budgetTotalTotalCost = new ArrayList();
        budgetSalaryCost = new ArrayList();
        budgetSalaryTotalCost = new ArrayList();
        budgetFringeCost = new ArrayList();
        budgetFringeTotalCost = new ArrayList();
        budgetDiscontinueCost = new ArrayList();
        budgetDiscontinueTotalCost = new ArrayList();
        budgetIndirectCost = new ArrayList();
        budgetIndirectTotalCost = new ArrayList();

    }

    // Getter and Setter methods

    public ArrayList getBgtcalIds() {
        return this.bgtcalIds;
    }

    public void setBgtcalIds(ArrayList bgtcalIds) {
        this.bgtcalIds = bgtcalIds;
    }

    public ArrayList getBgtcalBudgetIds() {
        return this.bgtcalBudgetIds;
    }

    public void setBgtcalBudgetIds(ArrayList bgtcalBudgetIds) {
        this.bgtcalBudgetIds = bgtcalBudgetIds;
    }

    public ArrayList getBgtcalProtIds() {
        return this.bgtcalProtIds;
    }

    public void setBgtcalProtIds(ArrayList bgtcalProtIds) {
        this.bgtcalProtIds = bgtcalProtIds;
    }

    public ArrayList getBgtcalProtTypes() {
        return this.bgtcalProtTypes;
    }

    public void setBgtcalProtTypes(ArrayList bgtcalProtTypes) {
        this.bgtcalProtTypes = bgtcalProtTypes;
    }

    public ArrayList getBgtcalDelFlags() {
        return this.bgtcalDelFlags;
    }

    public void setBgtcalDelFlags(ArrayList bgtcalDelFlags) {
        this.bgtcalDelFlags = bgtcalDelFlags;
    }

    public ArrayList getBgtcalSponsorOHeads() {
        return this.bgtcalSponsorOHeads;
    }

    public void setBgtcalSponsorOHeads(ArrayList bgtcalSponsorOHeads) {
        this.bgtcalSponsorOHeads = bgtcalSponsorOHeads;
    }

    public ArrayList getBgtcalClinicOHeads() {
        return this.bgtcalClinicOHeads;
    }

    public void setBgtcalClinicOHeads(ArrayList bgtcalClinicOHeads) {
        this.bgtcalClinicOHeads = bgtcalClinicOHeads;
    }

    public ArrayList getBgtcalSponsorFlags() {
        return this.bgtcalSponsorFlags;
    }

    public void setBgtcalSponsorFlags(ArrayList bgtcalSponsorFlags) {
        this.bgtcalSponsorFlags = bgtcalSponsorFlags;
    }

    public ArrayList getBgtcalClinicFlags() {
        return this.bgtcalClinicFlags;
    }

    public void setBgtcalClinicFlags(ArrayList bgtcalClinicFlags) {
        this.bgtcalClinicFlags = bgtcalClinicFlags;
    }

    public ArrayList getBgtcalIndirectCosts() {
        return this.bgtcalIndirectCosts;
    }

    public void setBgtcalIndirectCosts(ArrayList bgtcalIndirectCosts) {
        this.bgtcalIndirectCosts = bgtcalIndirectCosts;
    }

    public ArrayList getBgtcalProtNames() {
        return this.bgtcalProtNames;
    }

    public void setBgtcalProtNames(ArrayList bgtcalProtNames) {
        this.bgtcalProtNames = bgtcalProtNames;
    }

    public void setBgtcalIds(Integer bgtcalId) {
        bgtcalIds.add(bgtcalId);
    }

    public void setBgtcalBudgetIds(String bgtcalBudgetId) {
        bgtcalBudgetIds.add(bgtcalBudgetId);
    }

    public void setBgtcalProtIds(String bgtcalProtId) {
        bgtcalProtIds.add(bgtcalProtId);
    }

    public void setBgtcalProtTypes(String bgtcalProtType) {
        bgtcalProtTypes.add(bgtcalProtType);
    }

    public void setBgtcalDelFlags(String bgtcalDelFlag) {
        bgtcalDelFlags.add(bgtcalDelFlag);
    }

    public void setBgtcalSponsorOHeads(String bgtcalSponsorOHead) {
        bgtcalSponsorOHeads.add(bgtcalSponsorOHead);
    }

    public void setBgtcalClinicOHeads(String bgtcalClinicOHead) {
        bgtcalClinicOHeads.add(bgtcalClinicOHead);
    }

    public void setBgtcalSponsorFlags(String bgtcalSponsorFlag) {
        bgtcalSponsorFlags.add(bgtcalSponsorFlag);
    }

    public void setBgtcalClinicFlags(String bgtcalClinicFlag) {
        bgtcalClinicFlags.add(bgtcalClinicFlag);
    }

    public void setBgtcalIndirectCosts(String bgtcalIndirectCost) {
        bgtcalIndirectCosts.add(bgtcalIndirectCost);
    }

    public void setBgtcalProtNames(String bgtcalProtName) {
        bgtcalProtNames.add(bgtcalProtName);
    }

    public void setBRows(int bRows) {
        this.bRows = bRows;
    }

    public int getBRows() {
        return this.bRows;
    }
    
    
  	public ArrayList getBudgetResearchCost() {
        return this.budgetResearchCost;
    }

    public void setBudgetResearchCost(ArrayList budgetResearchCost) {
        this.budgetResearchCost = budgetResearchCost;
    }
    
    public ArrayList getBudgetResearchTotalCost() {
        return this.budgetResearchTotalCost;
    }

    public void setBudgetResearchTotalCost(ArrayList budgetResearchTotalCost) {
        this.budgetResearchTotalCost = budgetResearchTotalCost;
    }
    
    public ArrayList getBudgetSOCCost() {
        return this.budgetSOCCost;
    }

    public void setBudgetSOCCost(ArrayList budgetSOCCost) {
        this.budgetSOCCost = budgetSOCCost;
    }
    
    
    public ArrayList getBudgetSOCTotalCost() {
        return this.budgetSOCTotalCost;
    }

    public void setBudgetSOCTotalCost(ArrayList budgetSOCTotalCost) {
        this.budgetSOCTotalCost = budgetSOCTotalCost;
    }
    
    public ArrayList getBudgetTotalCost() {
        return this.budgetTotalCost;
    }

    public void setBudgetTotalCost(ArrayList budgetTotalCost) {
        this.budgetTotalCost = budgetTotalCost;
    }
    
    public ArrayList getBudgetTotalTotalCost() {
        return this.budgetTotalTotalCost;
    }

    public void setBudgetTotalTotalCost(ArrayList budgetTotalTotalCost) {
        this.budgetTotalTotalCost = budgetTotalTotalCost;
    }
    
    public ArrayList getBudgetSalaryCost() {
        return this.budgetSalaryCost;
    }

    public void setBudgetSalaryCost(ArrayList budgetSalaryCost) {
        this.budgetSalaryCost = budgetSalaryCost;
    }
    
    public ArrayList getBudgetSalaryTotalCost() {
        return this.budgetSalaryTotalCost;
    }

    public void setBudgetSalaryTotalCost(ArrayList budgetSalaryTotalCost) {
        this.budgetSalaryTotalCost = budgetSalaryTotalCost;
    }
    
    public ArrayList getBudgetFringeCost() {
        return this.budgetFringeCost;
    }

    public void setBudgetFringeCost(ArrayList budgetFringeCost) {
        this.budgetFringeCost = budgetFringeCost;
    }
    
    public ArrayList getBudgetFringeTotalCost() {
        return this.budgetFringeTotalCost;
    }

    public void setBudgetFringeTotalCost(ArrayList budgetFringeTotalCost) {
        this.budgetFringeTotalCost = budgetFringeTotalCost;
    }
    
    public ArrayList getBudgetDiscontinueCost() {
        return this.budgetDiscontinueCost;
    }

    public void setBudgetDiscontinueCost(ArrayList budgetDiscontinueCost) {
        this.budgetDiscontinueCost = budgetDiscontinueCost;
    }
    
    public ArrayList getbudgetDiscontinueTotalCost() {
        return this.budgetDiscontinueTotalCost;
    }

    public void setbudgetDiscontinueTotalCost(ArrayList budgetDiscontinueTotalCost) {
        this.budgetDiscontinueTotalCost = budgetDiscontinueTotalCost;
    }
    
    public ArrayList getBudgetIndirectCost() {
        return this.budgetIndirectCost;
    }

    public void setBudgetIndirectCost(ArrayList budgetIndirectCost) {
        this.budgetIndirectCost = budgetIndirectCost;
    }
    
    public ArrayList getBudgetIndirectTotalCost() {
        return this.budgetIndirectTotalCost;
    }

    public void setBudgetIndirectTotalCost(ArrayList budgetIndirectTotalCost) {
        this.budgetIndirectTotalCost = budgetIndirectTotalCost;
    }
    
    // end of getter and setter methods

    /**
     * Get the details of all the protocols associated to the budget
     * 
     * @param budgetId
     */

    public void getAllBgtCals(int budgetId) {
    	//Calling the function internally to prevent scattered code.
    	getAllBgtCals(budgetId, 1);
    }

    public void getAllBgtCals(int budgetId, int ignoreDeactivated) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select pk_bgtcal, BGTCAL_PROTID, name, BGTCAL_PROTTYPE, event_calassocto "
                    + "from sch_bgtcal, event_assoc " 
                    + "where fk_budget = ? "
                    + "and  BGTCAL_PROTID = EVENT_ID ";
                    
                    if (ignoreDeactivated == 1){
                    	sqlStr = sqlStr 
                    	+ "and FK_CODELST_CALSTAT not in (select pk_codelst from sch_codelst " 
                    	+ "where trim(CODELST_TYPE) = 'calStatStd' AND trim(CODELST_SUBTYP) = 'D')";
                    }
                    sqlStr = sqlStr 
                    + "and (sch_bgtcal.BGTCAL_DELFLAG <> 'Y' "
                    + "or sch_bgtcal.BGTCAL_DELFLAG is null) " 
                    + "UNION "
                    + "select pk_bgtcal, BGTCAL_PROTID, name, BGTCAL_PROTTYPE, 'L' event_calassocto "
                    + "from sch_bgtcal, event_def " + "where fk_budget = ? "
                    + "and  BGTCAL_PROTID = EVENT_ID "
                    + "and (sch_bgtcal.BGTCAL_DELFLAG <> 'Y' "
                    + "or sch_bgtcal.BGTCAL_DELFLAG is null) " 
                    + "UNION "
                    + "select pk_bgtcal, 0, 'Baseline', BGTCAL_PROTTYPE, '' event_calassocto " + "from sch_bgtcal "
                    + "where fk_budget = ?  and nvl(BGTCAL_DELFLAG,'Z')<>'Y' "
                    + "and pk_bgtcal not in ( " + "select pk_bgtcal "
                    + "from sch_bgtcal, event_assoc "
                    + "where fk_budget = ?  and nvl(BGTCAL_DELFLAG,'Z')<>'Y' "
                    + "and  BGTCAL_PROTID = EVENT_ID ";
                    
                    if (ignoreDeactivated == 1){
                    	sqlStr = sqlStr 
                    	+ "and FK_CODELST_CALSTAT not in (select pk_codelst from sch_codelst " 
                    	+ "where trim(CODELST_TYPE) = 'calStatStd' AND trim(CODELST_SUBTYP) = 'D')";
                    }
                    sqlStr = sqlStr 
                    + "UNION "
                    + "select pk_bgtcal " + "from sch_bgtcal, event_def "
                    + "where fk_budget = ? and nvl(BGTCAL_DELFLAG,'Z')<>'Y' "
                    + "and  BGTCAL_PROTID = EVENT_ID) ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetId);
            pstmt.setInt(2, budgetId);
            pstmt.setInt(3, budgetId);
            pstmt.setInt(4, budgetId);
            pstmt.setInt(5, budgetId);

            Rlog.debug("budgetcal", "BudgetcalDao.getAllBgtCals SQL**** "
                    + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setBgtcalIds(new Integer(rs.getInt("pk_bgtcal")));
                setBgtcalProtIds(new Integer(rs.getInt("BGTCAL_PROTID"))
                        .toString());
                setBgtcalProtNames(rs.getString("name"));
                setBgtcalProtTypes(rs.getString("BGTCAL_PROTTYPE"));
                setBgtcalClinicFlags(rs.getString("event_calassocto"));
                rows++;
                Rlog.debug("budgetcal", "BudgetcalDao.getAllBgtCals rows "
                        + rows);
            }
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("budgetcal",
                    "BudgetcalDao.getAllBgtCals EXCEPTION IN FETCHING FROM "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public int addBudgetSection(int bgtcalProtId, int bgtcalId,
            String bgtcalProtType, int creator, String ipAdd, String source) {

        CallableStatement cstmt = null;
        int success = -1;

        // OracleCallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call pkg_bgt.sp_pat_bgtsection(?,?,?,?,?,?)}");

            cstmt.setInt(1, bgtcalProtId);
            Rlog.debug("budgetcal", "In addBudgetSection bgtcalProtId"
                    + bgtcalProtId);

            cstmt.setInt(2, bgtcalId);
            Rlog.debug("budgetcal", "In addBudgetSection bgtcalId" + bgtcalId);

            cstmt.setString(3, bgtcalProtType);
            Rlog.debug("budgetcal", "In addBudgetSection bgtcalProtType" + bgtcalProtType);

            cstmt.setInt(4, creator);
            Rlog.debug("budgetcal", "In addBudgetSection creator" + creator);

            cstmt.setString(5, ipAdd);
            Rlog.debug("budgetcal", "In addBudgetSection ipAdd" + ipAdd);

            cstmt.setString(6, source);
            Rlog.debug("budgetcal", "In addBudgetSection source" + source);
            cstmt.execute();
            success = 1;

        }

        catch (SQLException ex) {
            Rlog.fatal("budgetcal",
                    "In addBudgetSection in EventdefDAO EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return success;
    }

    /**
     * Get the details of the protocol calendar associated to the budget
     * 
     * @param bgtcalId
     */

    public void getBgtcalDetail(int bgtcalId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select b.name " + "from " + "sch_bgtcal a , "
                    + "event_assoc b  " + "where a.pk_bgtcal = ? "
                    + "and a.bgtcal_protid=b.event_id " + "UNION "
                    + "select b.name " + "from " + "sch_bgtcal a , "
                    + "event_def b " + "where a.pk_bgtcal = ? "
                    + "and a.bgtcal_protid=b.event_id " + "UNION "
                    + "select 'None' " + "from sch_bgtcal "
                    + "where pk_bgtcal = ? " + "and pk_bgtcal not in ( "
                    + "select pk_bgtcal " + "from " + "sch_bgtcal a , "
                    + "event_assoc b  " + "where a.pk_bgtcal = ? "
                    + "and a.bgtcal_protid=b.event_id " + "UNION "
                    + "select pk_bgtcal " + "from " + "sch_bgtcal a , "
                    + "event_def b " + "where a.pk_bgtcal = ? "
                    + "and a.bgtcal_protid=b.event_id )";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, bgtcalId);
            pstmt.setInt(2, bgtcalId);
            pstmt.setInt(3, bgtcalId);
            pstmt.setInt(4, bgtcalId);
            pstmt.setInt(5, bgtcalId);

            Rlog.debug("budgetcal", "BudgetcalDao.getBgtcalDetail SQL**** "
                    + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            setBgtcalProtNames(rs.getString("name"));

        } catch (SQLException ex) {
            Rlog.fatal("budgetcal",
                    "BudgetcalDao.getBgtcalDetail EXCEPTION IN FETCHING FROM "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ///////////////////////
    /*
     * Update lineitem_othercost when 'exclude SOC' is updated
     */

    public int updateOtherCostToExcludeSOC(String bgtCalId, int excludeFlag,String modifiedBy,String ipAdd) {
         
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
        
            if (excludeFlag == 0)
            {
            	sqlStr = " Update sch_lineitem set lineitem_othercost = to_char(to_number(LINEITEM_SPONSORUNIT ) * to_number(LINEITEM_CLINICNOFUNIT)) , "
                + " last_modified_by = ?, ip_add = ? where fk_bgtsection in (select pk_budgetsec from sch_bgtsection where fk_bgtcal=? ) and nvl(lineitem_delflag,'N')  = 'N' " +
                		" and fk_codelst_cost_type = (SELECT pk_codelst FROM SCH_CODELST WHERE trim(LOWER(codelst_type))='cost_desc'  AND trim(LOWER(codelst_subtyp))='stdcare_cost')";
                            	
            }else
            {
            	sqlStr = " Update sch_lineitem set lineitem_othercost = '0.0', "
                    + " last_modified_by = ?, ip_add = ? where fk_bgtsection in (select pk_budgetsec from sch_bgtsection where fk_bgtcal=? ) and nvl(lineitem_delflag,'N')  = 'N' " +
            		" and fk_codelst_cost_type = (SELECT pk_codelst FROM SCH_CODELST WHERE trim(LOWER(codelst_type))='cost_desc'  AND trim(LOWER(codelst_subtyp))='stdcare_cost')";;
                    
            }
            
            pstmt = conn.prepareStatement(sqlStr);
            
            pstmt.setString(1, modifiedBy);
            pstmt.setString(2, ipAdd);
            pstmt.setString(3, bgtCalId);
            
            pstmt.executeUpdate();
            conn.commit();

        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "EXCEPTION IN BgtCalDao:updateOtherCostToExcludeSOC()" + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -1;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 1;
    }

    public int updateAllBgtCalendars(int budgetId, BudgetcalJB budgetcalB) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
        	String sponsorOHeadApply = "";
        	String sponsorOHead = "";
        	String excludeSOCApply = "";    	 
    		String fringeBenefit = "";
    		String fringeBenefitApply = "";
    		String costDiscount = "";
    		String costDiscountApply = "";
    		
    		sponsorOHeadApply=budgetcalB.getSpFlag();			 
    		sponsorOHead= budgetcalB.getSpOverHead();
    		fringeBenefit=budgetcalB.getBudgetFrgBenefit();
    		fringeBenefitApply=budgetcalB.getBudgetFrgFlag();
    		costDiscount=budgetcalB.getBudgetDiscount();
    		costDiscountApply=budgetcalB.getBudgetDiscountFlag();			
    		excludeSOCApply=budgetcalB.getBudgetExcldSOCFlag();
    		
            conn = getConnection();
            sqlStr = "Update ESCH.sch_bgtcal set "
            		+ "BGTCAL_SPONSORFLAG=?, "
            		+ "BGTCAL_SPONSOROHEAD=?, "
            		+ "BGTCAL_EXCLDSOCFLAG=?, "
            		+ "BGTCAL_FRGBENEFIT=?, "
            		+ "BGTCAL_FRGFLAG=?, "
            		+ "BGTCAL_DISCOUNT=?, "
            		+ "BGTCAL_DISCOUNTFLAG=? "
                    + "where fk_budget = ?  and nvl(BGTCAL_DELFLAG,'Z')<>'Y' ";
            pstmt = conn.prepareStatement(sqlStr);

            pstmt.setString(1, sponsorOHeadApply);
            pstmt.setFloat(2, EJBUtil.stringToFloat(sponsorOHead));           
            pstmt.setInt(3, EJBUtil.stringToNum(excludeSOCApply));
            pstmt.setFloat(4, EJBUtil.stringToFloat(fringeBenefit));
            pstmt.setInt(5, EJBUtil.stringToNum(fringeBenefitApply));
            pstmt.setFloat(6, EJBUtil.stringToFloat(costDiscount));
            pstmt.setInt(7, EJBUtil.stringToNum(costDiscountApply));
            pstmt.setInt(8, budgetId);
            
            pstmt.executeUpdate();
            conn.commit();           
        } catch (Exception e) {
            Rlog.fatal("BgtCal",
                    "EXCEPTION IN BudgetCalDao:updateAllBgtCalendars()" + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -2;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 1;

    }
    public int updateOtherCostToExSOCAllCals(int budgetId, int excludeFlag,String modifiedBy,String ipAdd) {
        
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
        
            if (excludeFlag == 0)
            {
            	sqlStr = " Update sch_lineitem set lineitem_othercost = to_char(to_number(LINEITEM_SPONSORUNIT ) * to_number(LINEITEM_CLINICNOFUNIT)) , "
                + " last_modified_by = ?, ip_add = ? where fk_bgtsection in (select pk_budgetsec from sch_bgtsection where fk_bgtcal IN "
        		+ " (Select PK_BGTCAL from sch_bgtcal where FK_BUDGET = ? )) and nvl(lineitem_delflag,'N')  = 'N' "
        		+ " and fk_codelst_cost_type = (SELECT pk_codelst FROM SCH_CODELST WHERE trim(LOWER(codelst_type))='cost_desc'  AND trim(LOWER(codelst_subtyp))='stdcare_cost')";
                            	
            }else
            {
            	sqlStr = " Update sch_lineitem set lineitem_othercost = '0.0', "
                + " last_modified_by = ?, ip_add = ? where fk_bgtsection in (select pk_budgetsec from sch_bgtsection where fk_bgtcal IN " 
        		+ " (Select PK_BGTCAL from sch_bgtcal where FK_BUDGET = ? )) and nvl(lineitem_delflag,'N')  = 'N' " 
        		+ " and fk_codelst_cost_type = (SELECT pk_codelst FROM SCH_CODELST WHERE trim(LOWER(codelst_type))='cost_desc'  AND trim(LOWER(codelst_subtyp))='stdcare_cost')";;
                    
            }
            
            pstmt = conn.prepareStatement(sqlStr);
            
            pstmt.setString(1, modifiedBy);
            pstmt.setString(2, ipAdd);
            pstmt.setInt(3, budgetId);
            
            pstmt.executeUpdate();
            conn.commit();

        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "EXCEPTION IN BgtCalDao:updateOtherCostToExSOCAllCals()" + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -1;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 1;
    }

}
