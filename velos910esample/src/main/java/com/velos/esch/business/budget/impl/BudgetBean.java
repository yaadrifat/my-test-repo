/*
 * Classname : BudgetBean
 *
 * Version information: 1.0
 *
 * Date: 03/20/2002
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.business.budget.impl;

/* Import Statements */
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */
@Entity
@Table(name = "sch_budget")
public class BudgetBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3257571715172282424L;

    /**
     * budgetId
     */
    public int budgetId;

    /**
     * budgetName
     */
    public String budgetName;

    /**
     * budgetVersion
     */
    public String budgetVersion;

    /**
     * budgetdesc
     */
    public String budgetdesc;

    /**
     * budgetCreator
     */
    public Integer budgetCreator;

    /**
     * budgetTemplate
     */
    public Integer budgetTemplate;

    /**
     * budgetStatus
     */
    public String budgetStatus;

    /**
     * budgetCurrency
     */
    public Integer budgetCurrency;

    /**
     * budgetSFlag
     */
    public String budgetSFlag;

    /**
     * budgetCFlag
     */
    public String budgetCFlag;

    /**
     * budgetStudyId
     */
    public Integer budgetStudyId;

    /**
     * budgetAccountId
     */
    public Integer budgetAccountId;

    /**
     * budgetSiteId
     */
    public Integer budgetSiteId;

    /**
     * budgetRScope
     */
    public String budgetRScope;

    /**
     * budgetRights
     */
    public String budgetRights;

    /**
     * creator
     */
    public Integer creator;

    /**
     * modifiedBy
     */
    public Integer modifiedBy;

    /**
     * ipAdd
     */
    public String ipAdd;

    /**
     * Budget Delete Flag
     */
    public String budgetDelFlag;

    public Timestamp lastModifiedDate;

    /**
     * budgetType
     */
    public String budgetType;

    /**
     * Budget Codelist Status
     */
    public String budgetCodeListStatus;

    
    /**
     * Budget default calendar
     */
    
    public String budgetDefaultCalendar;
    
    /**
     * Budget Combined Flag
     */
    
    public String budgetCombinedFlag;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_BUDGET", allocationSize=1)
    @Column(name = "PK_BUDGET")
    public int getBudgetId() {
        return this.budgetId;
    }

    public void setBudgetId(int budgetId) {
        this.budgetId = budgetId;
    }

    @Column(name = "BUDGET_NAME")
    public String getBudgetName() {
        return this.budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    @Column(name = "BUDGET_VERSION")
    public String getBudgetVersion() {
        return this.budgetVersion;
    }

    public void setBudgetVersion(String budgetVersion) {
        this.budgetVersion = budgetVersion;
    }

    @Column(name = "BUDGET_DESC")
    public String getBudgetdesc() {
        return this.budgetdesc;
    }

    public void setBudgetdesc(String budgetdesc) {
        this.budgetdesc = budgetdesc;
    }

    @Column(name = "BUDGET_CREATOR")
    public String getBudgetCreator() {
        return StringUtil.integerToString(this.budgetCreator);
    }

    public void setBudgetCreator(String budgetCreator) {
        this.budgetCreator = StringUtil.stringToInteger(budgetCreator);
    }

    @Column(name = "BUDGET_TEMPLATE")
    public String getBudgetTemplate() {
        return StringUtil.integerToString(this.budgetTemplate);
    }

    public void setBudgetTemplate(String budgetTemplate) {
        this.budgetTemplate = StringUtil.stringToInteger(budgetTemplate);
    }

    @Column(name = "BUDGET_STATUS")
    public String getBudgetStatus() {
        return this.budgetStatus;
    }

    public void setBudgetStatus(String budgetStatus) {
        this.budgetStatus = budgetStatus;
    }

    @Column(name = "BUDGET_CURRENCY")
    public String getBudgetCurrency() {
        return StringUtil.integerToString(this.budgetCurrency);
    }

    public void setBudgetCurrency(String budgetCurrency) {
        this.budgetCurrency = StringUtil.stringToInteger(budgetCurrency);
    }

    @Column(name = "BUDGET_SITEFLAG")
    public String getBudgetSFlag() {
        return this.budgetSFlag;
    }

    public void setBudgetSFlag(String budgetSFlag) {
        this.budgetSFlag = budgetSFlag;
    }

    @Column(name = "BUDGET_CALFLAG")
    public String getBudgetCFlag() {
        return this.budgetCFlag;
    }

    public void setBudgetCFlag(String budgetCFlag) {
        this.budgetCFlag = budgetCFlag;
    }

    @Column(name = "FK_STUDY")
    public String getBudgetStudyId() {
        return StringUtil.integerToString(this.budgetStudyId);
    }

    public void setBudgetStudyId(String budgetStudyId) {

        if (budgetStudyId != null) {
            this.budgetStudyId = StringUtil.stringToInteger(budgetStudyId);
        } else {
            this.budgetStudyId = null;
        }
    }

    @Column(name = "FK_ACCOUNT")
    public String getBudgetAccountId() {
        return StringUtil.integerToString(this.budgetAccountId);
    }

    public void setBudgetAccountId(String budgetAccountId) {
        this.budgetAccountId = StringUtil.stringToInteger(budgetAccountId);
    }

    @Column(name = "FK_SITE")
    public String getBudgetSiteId() {
        return StringUtil.integerToString(this.budgetSiteId);
    }

    public void setBudgetSiteId(String budgetSiteId) {
        if (budgetSiteId != null) {
            this.budgetSiteId = StringUtil.stringToInteger(budgetSiteId);
        } else {
            this.budgetSiteId = null;
        }
    }

    @Column(name = "BUDGET_RIGHTSCOPE")
    public String getBudgetRScope() {
        return this.budgetRScope;
    }

    public void setBudgetRScope(String budgetRScope) {
        this.budgetRScope = budgetRScope;
    }

    @Column(name = "BUDGET_RIGHTS")
    public String getBudgetRights() {
        return this.budgetRights;
    }

    public void setBudgetRights(String budgetRights) {
        this.budgetRights = budgetRights;
    }

    @Column(name = "BUDGET_DELFLAG")
    public String getBudgetDelFlag() {
        return this.budgetDelFlag;
    }

    public void setBudgetDelFlag(String budgetDelFlag) {
        this.budgetDelFlag = budgetDelFlag;
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        this.creator = StringUtil.stringToInteger(creator);
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "BUDGET_TYPE")
    public String getBudgetType() {
        return this.budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    /**
     * Gets the last modified date of the budget
     *
     * @return last modified date
     */
    @Column(name = "LAST_MODIFIED_DATE")
    public Timestamp getLastModifiedDt() {

        return this.lastModifiedDate;

    }

    /**
     * Sets the Last modified date
     *
     * @param last
     *            modified date
     */

    public void setLastModifiedDt(Timestamp lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;

    }

    @Transient
    public String getLastModifiedDate() {

        return DateUtil.dateToString(getLastModifiedDt(), null);
        //return DateUtil.dateToString(getLastModifiedDt());

    }

    /**
     * Sets the Last modified date
     *
     * @param last
     *            modified date
     */

    public void setLastModifiedDate(String lastModifiedDate) {

        setLastModifiedDt(DateUtil.stringToTimeStamp(lastModifiedDate,null));

    }

    // / END OF GETTER SETTER METHODS




    /**
     * updates the budget details
     *
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateBudget(BudgetBean bdsk) {
        try {

            setBudgetName(bdsk.getBudgetName());
            setBudgetVersion(bdsk.getBudgetVersion());
            setBudgetdesc(bdsk.getBudgetdesc());
            setBudgetCreator(bdsk.getBudgetCreator());
            setBudgetTemplate(bdsk.getBudgetTemplate());
            setBudgetStatus(bdsk.getBudgetStatus());
            setBudgetCurrency(bdsk.getBudgetCurrency());
            setBudgetSFlag(bdsk.getBudgetSFlag());
            setBudgetCFlag(bdsk.getBudgetCFlag());
            setBudgetStudyId(bdsk.getBudgetStudyId());
            setBudgetAccountId(bdsk.getBudgetAccountId());
            setBudgetSiteId(bdsk.getBudgetSiteId());
            setBudgetRScope(bdsk.getBudgetRScope());
            setBudgetRights(bdsk.getBudgetRights());
            setBudgetType(bdsk.getBudgetType());
            setCreator(bdsk.getCreator());
            setModifiedBy(bdsk.getModifiedBy());
            setIpAdd(bdsk.getIpAdd());
            setBudgetDelFlag(bdsk.getBudgetDelFlag());
            setLastModifiedDate(bdsk.getLastModifiedDate());

            setBudgetCodeListStatus(bdsk.getBudgetCodeListStatus());
            setBudgetCombinedFlag(bdsk.getBudgetCombinedFlag());

            Rlog.debug("budget", "bdsk.getBudgetDelFlag() in update bean"
                    + bdsk.getBudgetDelFlag());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("budget", " error in BudgetBean.updateBudget : " + e);
            return -2;
        }
    }

    public BudgetBean() {

    }

    public BudgetBean(int budgetId, String budgetName, String budgetVersion,
            String budgetdesc, String budgetCreator, String budgetTemplate,
            String budgetStatus, String budgetCurrency, String budgetSFlag,
            String budgetCFlag, String budgetStudyId, String budgetAccountId,
            String budgetSiteId, String budgetRScope, String budgetRights,
            String creator, String modifiedBy, String ipAdd,
            String budgetDelFlag, String lastModifiedDate, String budgetType,String cstat, 
            String combBgtFlag) {
        super();
        // TODO Auto-generated constructor stub
        setBudgetId(budgetId);
        setBudgetName(budgetName);
        setBudgetVersion(budgetVersion);
        setBudgetdesc(budgetdesc);
        setBudgetCreator(budgetCreator);
        setBudgetTemplate(budgetTemplate);
        setBudgetStatus(budgetStatus);
        setBudgetCurrency(budgetCurrency);
        setBudgetSFlag(budgetSFlag);
        setBudgetCFlag(budgetCFlag);
        setBudgetStudyId(budgetStudyId);
        setBudgetAccountId(budgetAccountId);
        setBudgetSiteId(budgetSiteId);
        setBudgetRScope(budgetRScope);
        setBudgetRights(budgetRights);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setBudgetDelFlag(budgetDelFlag);
        // Modified by amarnadh for Bugzilla issue #3010
       // setLastModifiedDate(lastModifiedDate);
        setBudgetType(budgetType);
        setBudgetCodeListStatus(cstat);
        setBudgetCombinedFlag(combBgtFlag);
    }

    /**
     * gets budget's status. refers to sch_codelst pk
     * */
    @Column(name = "FK_CODELST_STATUS")
	public String getBudgetCodeListStatus() {
		return budgetCodeListStatus;
	}

	public void setBudgetCodeListStatus(String budgetCodeListStatus) {
		this.budgetCodeListStatus = budgetCodeListStatus;
	}

	@Column(name = "BUDGET_CALENDAR")
	public String getBudgetDefaultCalendar() {
		return budgetDefaultCalendar;
	}

	public void setBudgetDefaultCalendar(String budgetDefaultCalendar) {
		this.budgetDefaultCalendar = budgetDefaultCalendar;
	}

	@Column(name = "BUDGET_COMBFLAG")
	public String getBudgetCombinedFlag() {
		return budgetCombinedFlag;
	}

	public void setBudgetCombinedFlag(String budgetCombinedFlag) {
		this.budgetCombinedFlag = budgetCombinedFlag;
	}
	
}// end of class
