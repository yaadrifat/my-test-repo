/* 
 * Classname			LineitemBean.class
 * 
 * Version information
 *
 * Date					03/28/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.business.lineitem.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * The Lineitem CMP entity bean.<br>
 * <br>
 * 
 * @author Sajal
 */
@Entity
@Table(name = "sch_lineitem")
public class LineitemBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3258412850258065715L;

    /**
     * the lineitem id
     */
    public int lineitemId;

    /**
     * the lineitem budget section
     */
    public Integer lineitemBgtSection;

    /**
     * the lineitem name
     */
    public String lineitemName;

    /**
     * the lineitem description
     */
    public String lineitemDesc;

    /**
     * the lineitem sponsor or unit cost
     */
    public String lineitemSponsorUnit;

    /**
     * the lineitem clinic cost or number of units
     */

    public String lineitemClinicNOfUnit;

    /**
     * the lineitem other cost
     */
    public String lineitemOtherCost;

    /**
     * the lineitem delete flag
     */
    public String lineitemDelFlag;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    /*
     * line item notes
     */
    public String lineitemNotes;

    /*
     * line item include in all sections or in personnel section
     */
    public Integer lineitemInPerSec;

    /*
     * line item standard of care cost
     */
    public String lineitemStdCareCost;

    /*
     * line item research cost
     */
    public String lineitemResCost;

    /*
     * line item invoice cost
     */
    public String lineitemInvCost;

    /*
     * line item include cost discount
     */
    public Integer lineitemInCostDisc;

    /*
     * line item cpt code
     */
    public String lineitemCptCode;

    /*
     * line item repeat
     */
    public Integer lineitemRepeat;

    /*
     * line item parent id
     */
    public Integer lineitemParentId;

    /*
     * line item apply in future
     */
    public Integer lineitemAppInFuture;

    /*
     * line item Category
     */
    public Integer lineitemCategory;

    /**
     * the lineitem TMID
     */
    public String lineitemTMID;

    /**
     * the lineitem CDM
     */
    public String lineitemCDM;

    /*
     * line item apply in future
     */
    public String lineitemAppIndirects;
    
    //Store the Total Cost Item
    
    public String lineitemTotalCost;
    
    //Added by IA 11.03.2006
    
    public String lineItemSponsorAmount;
    
    public String lineItemVariance;
    
    public String lineItemSequence;
    public String lineItemCostType;
    
    public Integer lineItemFkEvent;
    //End Added
    
    
    

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_LINEITEM", allocationSize=1)
    @Column(name = "PK_LINEITEM")
    public int getLineitemId() {
        return this.lineitemId;
    }

    public void setLineitemId(int lineitemId) {
        this.lineitemId = lineitemId;
    }

    @Column(name = "FK_BGTSECTION")
    public String getLineitemBgtSection() {
        return StringUtil.integerToString(this.lineitemBgtSection);
    }

    public void setLineitemBgtSection(String lineitemBgtSection) {
        this.lineitemBgtSection = StringUtil.stringToInteger(lineitemBgtSection);
    }

    @Column(name = "LINEITEM_NAME")
    public String getLineitemName() {
        return this.lineitemName;
    }

    public void setLineitemName(String lineitemName) {
        this.lineitemName = lineitemName;
    }

    @Column(name = "LINEITEM_DESC")
    public String getLineitemDesc() {
        return this.lineitemDesc;
    }

    public void setLineitemDesc(String lineitemDesc) {
        this.lineitemDesc = lineitemDesc;
    }

    @Column(name = "LINEITEM_SPONSORUNIT")
    public String getLineitemSponsorUnit() {
        return this.lineitemSponsorUnit;
    }

    public void setLineitemSponsorUnit(String lineitemSponsorUnit) {

        this.lineitemSponsorUnit = lineitemSponsorUnit;

    }

    @Column(name = "LINEITEM_CLINICNOFUNIT")
    public String getLineitemClinicNOfUnit() {
        return this.lineitemClinicNOfUnit;
    }

    public void setLineitemClinicNOfUnit(String lineitemClinicNOfUnit) {

        this.lineitemClinicNOfUnit = lineitemClinicNOfUnit;

    }

    @Column(name = "LINEITEM_OTHERCOST")
    public String getLineitemOtherCost() {
        return this.lineitemOtherCost;
    }

    public void setLineitemOtherCost(String lineitemOtherCost) {

        this.lineitemOtherCost = lineitemOtherCost;

    }

    @Column(name = "LINEITEM_DELFLAG")
    public String getLineitemDelFlag() {
        return this.lineitemDelFlag;
    }

    public void setLineitemDelFlag(String lineitemDelFlag) {
        this.lineitemDelFlag = lineitemDelFlag;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);

    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Gets the flag which indicates apply lineitem changes in future or
     * previous lineitems
     * 
     * @return apply in future flag
     */
    @Column(name = "LINEITEM_APPLYINFUTURE")
    public String getLineitemAppInFuture() {
        return StringUtil.integerToString(this.lineitemAppInFuture);
    }

    /**
     * Sets the apply in future flag
     * 
     * @param apply
     *            in future flag
     */

    public void setLineitemAppInFuture(String lineitemAppInFuture) {

        this.lineitemAppInFuture = StringUtil.stringToInteger(lineitemAppInFuture);

    }

    /**
     * Gets the lineitem CPT code
     * 
     * @return lineitem CPT code
     */
    @Column(name = "LINEITEM_CPTCODE")
    public String getLineitemCptCode() {
        return this.lineitemCptCode;
    }

    /**
     * Sets the lineitem CPT code
     * 
     * @param lineitem
     *            CPT code
     */

    public void setLineitemCptCode(String lineitemCptCode) {
        this.lineitemCptCode = lineitemCptCode;
    }

    /**
     * Gets the flag which indicates include cost discount
     * 
     * @return cost discount flag
     */
    @Column(name = "LINEITEM_INCOSTDISC")
    public String getLineitemInCostDisc() {
        return StringUtil.integerToString(this.lineitemInCostDisc);
    }

    /**
     * Sets the cost discount flag
     * 
     * @param cost
     *            discount flag
     */

    public void setLineitemInCostDisc(String lineitemInCostDisc) {

        this.lineitemInCostDisc = StringUtil.stringToInteger(lineitemInCostDisc);

    }

    /**
     * Gets the flag which indicates include cost in all Sections
     * 
     * @return include cost in all sections flag
     */
    @Column(name = "LINEITEM_INPERSEC")
    public String getLineitemInPerSec() {
        return StringUtil.integerToString(this.lineitemInPerSec);
    }

    /**
     * Sets the include cost in all Sections flag
     * 
     * @param include
     *            cost in all Sections flag
     */
    public void setLineitemInPerSec(String lineitemInPerSec) {

        this.lineitemInPerSec = StringUtil.stringToInteger(lineitemInPerSec);
    }

    /**
     * Gets the lineitem invoice cost
     * 
     * @return lineitem invoice cost
     */
    @Column(name = "LINEITEM_INVCOST")
    public String getLineitemInvCost() {
        return this.lineitemInvCost;
    }

    /**
     * Sets the lineitem invoice cost
     * 
     * @param lineitem
     *            invoice cost
     */
    public void setLineitemInvCost(String lineitemInvCost) {

        this.lineitemInvCost = lineitemInvCost;

    }

    /**
     * Gets the lineitem notes
     * 
     * @return lineitem notes
     */
    @Column(name = "LINEITEM_NOTES")
    public String getLineitemNotes() {
        return this.lineitemNotes;
    }

    /**
     * Sets the lineitem notes
     * 
     * @param lineitem
     *            notes
     */
    public void setLineitemNotes(String lineitemNotes) {
        this.lineitemNotes = lineitemNotes;
    }

    /**
     * Gets the lineitem parent
     * 
     * @return lineitem parent
     */
    @Column(name = "LINEITEM_PARENTID")
    public String getLineitemParentId() {
        return StringUtil.integerToString(this.lineitemParentId);
    }

    /**
     * Sets the lineitem parent
     * 
     * @param lineitem
     *            parent
     */
    public void setLineitemParentId(String lineitemParentId) {

        this.lineitemParentId = StringUtil.stringToInteger(lineitemParentId);
    }

    /**
     * Gets the lineitem repeat flag
     * 
     * @return lineitem repeat flag
     */
    @Column(name = "LINEITEM_REPEAT")
    public String getLineitemRepeat() {
        return StringUtil.integerToString(this.lineitemRepeat);
    }

    /**
     * Sets the lineitem repeat flag
     * 
     * @param lineitem
     *            repeat flag
     */

    public void setLineitemRepeat(String lineitemRepeat) {

        this.lineitemRepeat = StringUtil.stringToInteger(lineitemRepeat);
    }

    /**
     * Gets the lineitem research cost
     * 
     * @return lineitem research cost
     */
    @Column(name = "LINEITEM_RESCOST")
    public String getLineitemResCost() {
        return this.lineitemResCost;
    }

    /**
     * Sets the lineitem research cost
     * 
     * @param lineitem
     *            research cost
     */
    public void setLineitemResCost(String lineitemResCost) {

        this.lineitemResCost = lineitemResCost;

    }

    /**
     * Gets the Standard Care Cost
     * 
     * @return standard care cost
     */
    @Column(name = "LINEITEM_STDCARECOST")
    public String getLineitemStdCareCost() {
        return this.lineitemStdCareCost;
    }

    /**
     * Sets the Standard Care Cost
     * 
     * @param Standard
     *            Care Cost
     */
    public void setLineitemStdCareCost(String lineitemStdCareCost) {

        this.lineitemStdCareCost = lineitemStdCareCost;
        Rlog.debug("Lineitem", " this.lineitemStdCareCost in bean*** "
                + this.lineitemStdCareCost);

    }

    /**
     * Gets category for a line item
     * 
     * @return lineitem category
     */
    @Column(name = "FK_CODELST_CATEGORY")
    public String getLineitemCategory() {
        Rlog.debug("Lineitem", "IN EJB bean get****this.lineitemCategory"
                + this.lineitemCategory);
        return StringUtil.integerToString(this.lineitemCategory);
    }

    /**
     * Sets the Category for lineitem
     * 
     * @param lineitem
     *            category
     */

    public void setLineitemCategory(String lineitemCategory) {

        Rlog.debug("Lineitem", "IN EJB bean set ****lineitemCategory"
                + lineitemCategory);
        this.lineitemCategory = StringUtil.stringToInteger(lineitemCategory);

    }

    /**
     * Gets TMID of a lineitems
     * 
     * @return TMID
     */
    @Column(name = "LINEITEM_TMID")
    public String getLineitemTMID() {
        return this.lineitemTMID;
    }

    /**
     * Sets TMID for a lineitem
     * 
     * @param TMID
     */

    public void setLineitemTMID(String lineitemTMID) {

        this.lineitemTMID = lineitemTMID;

    }

    /**
     * Gets CDM of a lineitems
     * 
     * @return CDM
     */
    @Column(name = "LINEITEM_CDM")
    public String getLineitemCDM() {
        return this.lineitemCDM;
    }

    /**
     * Sets CDM for a lineitem
     * 
     * @param CDM
     */

    public void setLineitemCDM(String lineitemCDM) {

        this.lineitemCDM = lineitemCDM;

    }

    /**
     * Gets apply indirects flag of a lineitems
     * 
     * @return lineitemAppIndirects
     */
    @Column(name = "LINEITEM_APPLYINDIRECTS")
    public String getLineitemAppIndirects() {
        return this.lineitemAppIndirects;
    }

    /**
     * Sets Apply Indirects flag for a lineitem
     * 
     * @param lineitemAppIndirects
     */
    public void setLineitemAppIndirects(String lineitemAppIndirects) {
        this.lineitemAppIndirects = lineitemAppIndirects;
    }
    
    
    //LINEITEM_TOTALCOST
    @Column(name = "LINEITEM_TOTALCOST")
    public String getLineitemTotalCost() {

        return this.lineitemTotalCost ;

    }

    public void setLineitemTotalCost(String lineitemTotalCost) {

        if (lineitemTotalCost != null) {
            this.lineitemTotalCost = lineitemTotalCost;
        }
    }
    

    // END OF GETTER SETTER METHODS
    
    
    
    //Added by Ia 11.03.2006
    @Column(name = "LINEITEM_SPONSORAMOUNT")
    public String getLineItemSponsorAmount() {

        return this.lineItemSponsorAmount ;

    }

    public void setLineItemSponsorAmount(String lineItemSponsorAmount) {

        if (lineItemSponsorAmount != null) {
            this.lineItemSponsorAmount = lineItemSponsorAmount;
        }
    }
    
    @Column(name = "LINEITEM_VARIANCE")
    public String getLineItemVariance() {

        return this.lineItemVariance;

    }

    public void setLineItemVariance(String lineItemVariance) {

        if (lineItemVariance != null) {
            this.lineItemVariance = lineItemVariance;
        }
    }
    
    @Column(name = "FK_EVENT")
    public Integer getLineItemFkEvent() {
        return this.lineItemFkEvent;
    }

    public void setLineItemFkEvent(Integer lineItemFkEvent) {
        this.lineItemFkEvent = lineItemFkEvent;
    }
    
    //end added

    /**
     * returns the state keeper object associated with this lineitem
     */
    /*
     * public LineitemStateKeeper getLineitemStateKeeper() {
     * Rlog.debug("Lineitem", "GET Lineitem KEEPER"); return new
     * LineitemStateKeeper(getLineitemId(), getLineitemBgtSection(),
     * getLineitemName(), getLineitemDesc(), getLineitemSponsorUnit(),
     * getLineitemClinicNOfUnit(), getLineitemOtherCost(), getLineitemDelFlag(),
     * getCreator(), getModifiedBy(), getIpAdd(), getLineitemNotes(),
     * getLineitemInPerSec(), getLineitemStdCareCost(), getLineitemResCost(),
     * getLineitemInvCost(), getLineitemInCostDisc(), getLineitemCptCode(),
     * getLineitemRepeat(), getLineitemParentId(), getLineitemAppInFuture(),
     * getLineitemCategory(), getLineitemTMID(), getLineitemCDM(),
     * getLineitemAppIndirects()); }
     */

    /**
     * sets up a state keeper containing details of the lineitem
     */
    /*
     * public void setLineitemStateKeeper(LineitemStateKeeper lsk) {
     * Rlog.debug("Lineitem", "IN Lineitem STATE KEEPER"); GenerateId genId =
     * null; try { Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("Lineitem", "Connection :" + conn); lineitemId =
     * genId.getId("SEQ_SCH_LINEITEM", conn); conn.close();
     * 
     * setLineitemBgtSection(lsk.getLineitemBgtSection());
     * setLineitemName(lsk.getLineitemName());
     * setLineitemInCostDisc(lsk.getLineitemInCostDisc());
     * setLineitemDesc(lsk.getLineitemDesc());
     * setLineitemSponsorUnit(lsk.getLineitemSponsorUnit());
     * setLineitemClinicNOfUnit(lsk.getLineitemClinicNOfUnit());
     * setLineitemOtherCost(lsk.getLineitemOtherCost());
     * setLineitemDelFlag(lsk.getLineitemDelFlag());
     * setLineitemCptCode(lsk.getLineitemCptCode());
     * setLineitemRepeat(lsk.getLineitemRepeat());
     * setLineitemResCost(lsk.getLineitemResCost());
     * setLineitemStdCareCost(lsk.getLineitemStdCareCost());
     * setLineitemInvCost(lsk.getLineitemInvCost());
     * setLineitemCategory(lsk.getLineitemCategory());
     * setLineitemTMID(lsk.getLineitemTMID());
     * setLineitemCDM(lsk.getLineitemCDM()); setCreator(lsk.getCreator());
     * setModifiedBy(lsk.getModifiedBy());
     * setLineitemNotes(lsk.getLineitemNotes()); setIpAdd(lsk.getIpAdd());
     * setLineitemAppIndirects(lsk.getLineitemAppIndirects()); } catch
     * (Exception e) { Rlog.fatal("Lineitem", "EXCEPTION IN CREATING NEW
     * LINEITEM" + e); } }
     */

    public int updateLineitem(LineitemBean lsk) {

        try {
            setLineitemBgtSection(lsk.getLineitemBgtSection());
            Rlog.debug("Lineitem", "  bean getLineitemBgtSection:"
                    + lsk.getLineitemBgtSection());
            setLineitemName(lsk.getLineitemName());

            setLineitemDesc(lsk.getLineitemDesc());

            setLineitemInCostDisc(lsk.getLineitemInCostDisc());

            setLineitemSponsorUnit(lsk.getLineitemSponsorUnit());

            setLineitemClinicNOfUnit(lsk.getLineitemClinicNOfUnit());

            setLineitemResCost(lsk.getLineitemResCost());

            setLineitemStdCareCost(lsk.getLineitemStdCareCost());

            setLineitemInvCost(lsk.getLineitemInvCost());

            setLineitemOtherCost(lsk.getLineitemOtherCost());

            setLineitemDelFlag(lsk.getLineitemDelFlag());

            setLineitemCptCode(lsk.getLineitemCptCode());

            setLineitemRepeat(lsk.getLineitemRepeat());

            setLineitemNotes(lsk.getLineitemNotes());

            setLineitemCategory(lsk.getLineitemCategory());

            setLineitemTMID(lsk.getLineitemTMID());

            setLineitemCDM(lsk.getLineitemCDM());
            
            setLineitemTotalCost(lsk.getLineitemTotalCost());
            
            
            //Added by IA 11.03.2006
            
            setLineItemSponsorAmount(lsk.getLineItemSponsorAmount());
            
            
            setLineItemVariance(lsk.getLineItemVariance());
            
            //end added

            setCreator(lsk.getCreator());
            setModifiedBy(lsk.getModifiedBy());
            setIpAdd(lsk.getIpAdd());
            setLineitemAppIndirects(lsk.getLineitemAppIndirects());
            
            setLineItemCostType( lsk.getLineItemCostType());
            setLineItemSequence(lsk.getLineItemSequence());
            setLineItemFkEvent(lsk.getLineItemFkEvent());
            
            return 0;
        } catch (Exception e) {
            Rlog.fatal("Lineitem", " error LineitemBean.update");
            return -2;
        }
    }

    public LineitemBean() {

    }

    public LineitemBean(int lineitemId, String lineitemBgtSection,
            String lineitemName, String lineitemDesc,
            String lineitemSponsorUnit, String lineitemClinicNOfUnit,
            String lineitemOtherCost, String lineitemDelFlag, String creator,
            String modifiedBy, String ipAdd, String lineitemNotes,
            String lineitemInPerSec, String lineitemStdCareCost,
            String lineitemResCost, String lineitemInvCost,
            String lineitemInCostDisc, String lineitemCptCode,
            String lineitemRepeat, String lineitemParentId,
            String lineitemAppInFuture, String lineitemCategory,
            String lineitemTMID, String lineitemCDM, String lineitemAppIndirects, String lineitemTotalCost, String lineItemSponsorAmount, String lineItemVariance,
            String lineItemCostType,String lineItemSequence, Integer lineItemFkEvent) {
        super();
        setLineitemId(lineitemId);
        setLineitemBgtSection(lineitemBgtSection);
        setLineitemName(lineitemName);
        setLineitemDesc(lineitemDesc);
        setLineitemSponsorUnit(lineitemSponsorUnit);
        setLineitemClinicNOfUnit(lineitemClinicNOfUnit);
        setLineitemOtherCost(lineitemOtherCost);
        setLineitemDelFlag(lineitemDelFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setLineitemNotes(lineitemNotes);
        setLineitemInPerSec(lineitemInPerSec);
        setLineitemStdCareCost(lineitemStdCareCost);
        setLineitemResCost(lineitemResCost);
        setLineitemInvCost(lineitemInvCost);
        setLineitemInCostDisc(lineitemInCostDisc);
        setLineitemCptCode(lineitemCptCode);
        setLineitemRepeat(lineitemRepeat);
        setLineitemParentId(lineitemParentId);
        setLineitemAppInFuture(lineitemAppInFuture);
        setLineitemCategory(lineitemCategory);
        setLineitemTMID(lineitemTMID);
        setLineitemCDM(lineitemCDM);
        setLineitemAppIndirects(lineitemAppIndirects);
        setLineitemTotalCost(lineitemTotalCost);
        
        //Added by IA 11.03.2006
        
        setLineItemSponsorAmount(lineItemSponsorAmount);
        
        setLineItemVariance(lineItemVariance);
        setLineItemCostType( lineItemCostType);
        setLineItemSequence(lineItemSequence);
        setLineItemFkEvent(lineItemFkEvent);
        //end added
        
    }

    @Column(name = "lineitem_seq")
	public String getLineItemSequence() {
		return lineItemSequence;
	}

	public void setLineItemSequence(String lineItemSequence) {
		this.lineItemSequence = lineItemSequence;
	}

	@Column(name = "fk_codelst_cost_type")
	public String getLineItemCostType() {
		return lineItemCostType;
	}

	public void setLineItemCostType(String lineItemCostType) {
		this.lineItemCostType = lineItemCostType;
	}



}// end of class

