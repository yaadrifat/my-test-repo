/*
 * Classname			:	EventKitBean.class
 * 
 * Version information	:	1.0	
 *
 * Date					:	05/09/2008
 * 
 * Copyright notice		:	Velos Inc.
 * 
 */

package com.velos.esch.business.eventKit.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * The EventKit CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Abrol
 */

@Entity
@Table(name = "sch_event_kit")
public class EventKitBean implements Serializable {
	  

    /**
     * the eventkit Id
     */
    public int eventKitId;

    /**
     * the eventcrf fkForm
     */
    
	public Integer eventStorageKit;

	/**
	 * the eventcrf  fkEvent
	 */
	
	public int fkEvent;
	
			
	/**
     * the record creator
     */
    
	public Integer creator;

    /**
     * last modified by
     */
    
	public Integer lastModifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;


    // GETTER SETTER METHODS
  
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_EVENT_KIT", allocationSize=1)
    @Column(name = "PK_EVENTKIT")
    public int getEventKitId() {
        return this.eventKitId;
    }

    public void setEventKitId(int eventKitId) {
        this.eventKitId = eventKitId;
    }

    
    @Column(name = "FK_EVENT")
    public int getFkEvent() {
        return this.fkEvent;
    }

    public void setFkEvent(int fkEvent) {
        
            this.fkEvent = fkEvent;
    }

    
    @Column(name = "FK_STORAGE")
    public String getEventStorageKit() {
        return StringUtil.integerToString(this.eventStorageKit);
    }

    public void setEventStorageKit(String kit) {
        if (kit!="")
            this.eventStorageKit = Integer.valueOf(kit);
    }
    
 
    
    
    /**
     * @return The Id of the creator of the record
     */
    
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creatorId) {
        if (creatorId != null) {
            this.creator = Integer.valueOf(creatorId);
        }
    }

    /**
     * @return The Id of the user modified this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return StringUtil.integerToString(this.lastModifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
   
    public void setLastModifiedBy(String eventKitLastModBy) {
        if (eventKitLastModBy != null) {
            this.lastModifiedBy = Integer.valueOf(eventKitLastModBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String eventKitIPAdd) {
        this.ipAdd = eventKitIPAdd;
    }

    // END OF GETTER SETTER METHODS

    
    public EventKitBean() {

    }

    public EventKitBean(int eventKitId, String eventStorageKit, int fkEvent, String eventKitCreator , String eventKitLastModBy , String eventKitIPAdd) {
        super();
        // TODO Auto-generated constructor stub
        setEventKitId(eventKitId);
        setEventStorageKit(eventStorageKit);
        setFkEvent(fkEvent);
    	 
        setCreator(eventKitCreator);
        setLastModifiedBy(eventKitLastModBy);
        setIpAdd(eventKitIPAdd);
    }
   
  
    public int updateEventKit(EventKitBean kit) {

        try {
        	setEventKitId(kit.getEventKitId());
        	setFkEvent(kit.getFkEvent());
        	setEventStorageKit(kit.getEventStorageKit());
        	setCreator(kit.getCreator());
          	setLastModifiedBy(kit.getLastModifiedBy());
        	setIpAdd(kit.getIpAdd());
        	
             
            return 0;
        } catch (Exception e) {
            Rlog.fatal("event", " Exception in EventKitBean.updateEventKit");
            return -2;
        }
    }

}// end of class
 
