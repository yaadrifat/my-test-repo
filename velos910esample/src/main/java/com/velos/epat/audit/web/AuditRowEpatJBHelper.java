package com.velos.epat.audit.web;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.epat.audit.service.AuditRowEpatAgent;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;

/**
 * A helper class for AuditRowJB. The purpose of this class is to reduce the code size of AuditRowJB
 * by delegating some methods. This class object requires a reference to a AuditRowJB object. When
 * executing a method, this class fills the AuditRowJB object with the data retrieved from DB.
 * 
 * @author Isaac Huang
 */
public class AuditRowEpatJBHelper {
	private AuditRowEpatJB AuditRowJB = null;
	
	@SuppressWarnings("unused")
	private AuditRowEpatJBHelper() {}

	public AuditRowEpatJBHelper(AuditRowEpatJB AuditRowJB) {
		this.AuditRowJB = AuditRowJB;
	}
	
	private AuditRowEpatAgent getAuditRowEpatAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AuditRowEpatAgent)ic.lookup(JNDINames.AuditRowEpatAgentHome);
	}

	public int getRID(int pkey, String tableName, String pkColumnName) {
		AuditRowEpatAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEpatAgent();
			output = auditAgent.getRID(pkey, tableName, pkColumnName);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.getRID for pKey="+pkey+" :tableName="+tableName+" "+e);
			output = -1;
		}
		return output;
	}
	public int findLatestRaid(int rid) {
		AuditRowEpatAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEpatAgent();
			output = auditAgent.findLatestRaid(rid);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.findLatestRaid(int rid) "+e);
			output = -1;
		}
		return output;
	}
	
	public int findLatestRaid(int rid, int userId) {
		AuditRowEpatAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEpatAgent();
			output = auditAgent.findLatestRaid(rid, userId);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.findLatestRaid(int rid, int userId) "+e);
			output = -1;
		}
		return output;
	}

	public int setReasonForChange(int raid, String remarks) {
		AuditRowEpatAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEpatAgent();
			if (raid > 0)
				output = auditAgent.setReasonForChange(raid,remarks);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.setReasonForChange for raid="+raid+" "+e);
			output = -1;
		}
		return output;
	}
}
