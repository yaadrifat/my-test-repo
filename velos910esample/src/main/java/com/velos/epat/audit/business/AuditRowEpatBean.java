package com.velos.epat.audit.business;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "AUDIT_ROW")
@NamedQueries( {
    @NamedQuery(name = "findLatestAuditRowEpatByUserIdRID",
    		query = "SELECT OBJECT(auditRow) FROM AuditRowEpatBean auditRow " +
    		" where auditRow.RID = :rid " +
    		" AND (auditRow.userName = :userId OR auditRow.userName like :userIdLike) " +
    		" ORDER BY 1 DESC"),
    @NamedQuery(name = "findLatestAuditRowEpatByRID",
			query = "SELECT OBJECT(auditRow) FROM AuditRowEpatBean auditRow " +
			" where auditRow.RID = :rid " +
			" ORDER BY 1 DESC")
})
public class AuditRowEpatBean implements Serializable {
	
	private Integer id;
    private String tableName;
	private Integer RID;
    private String action;
    private Date timeStamp;
    private String userName;

    public AuditRowEpatBean(){}
    
    public void setId(Integer id) {
        this.id = id;
    }

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_AUDIT", allocationSize=1)
    @Column(name = "RAID")
    public Integer getId() {
        return id;
    }

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Column(name = "TABLE_NAME")
	public String getTableName() {
		return tableName;
	}

	public void setRID(Integer rid) {
		this.RID = rid;
	}
	@Column(name = "RID")
	public Integer getRID() {
		return RID;
	}

	public void setAction(String action) {
		this.action = action;
	}
	@Column(name = "ACTION")
	public String getAction() {
		return action;
	}
	
	public void setTimeStamp(Date timeStamp){
		this.timeStamp=timeStamp;
	}
	@Column(name = "TIMESTAMP")
	public Date getTimeStamp(){
		return this.timeStamp;
	}
	
	public void setUserName(String userName){
		this.userName=userName;
	}
	@Column(name = "USER_NAME")
	public String getUserName(){
		return this.userName;
	}
}
