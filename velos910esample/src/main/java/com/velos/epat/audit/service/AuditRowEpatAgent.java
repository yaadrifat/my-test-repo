package com.velos.epat.audit.service;
import javax.ejb.Remote;

import com.velos.epat.audit.business.AuditRowEpatBean;
@Remote
public interface AuditRowEpatAgent {
	public AuditRowEpatBean getAuditRowEpatBean(Integer id);
	public int getRID(int pkey, String tableName, String pkColumnName);
	public int findLatestRaid(int rid);
	public int findLatestRaid(int rid, int userId);
	public int setReasonForChange(int raid, String remarks);
}
