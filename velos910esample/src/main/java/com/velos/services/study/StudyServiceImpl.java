                                                                     
                                                                     
                                                                     
                                             
/**
 * 
 */
package com.velos.services.study;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.statusHistory.impl.StatusHistoryBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.studyId.impl.StudyIdBean;
import com.velos.eres.business.studySite.impl.StudySiteBean;
import com.velos.eres.business.studyStatus.impl.StudyStatusBean;
import com.velos.eres.business.team.impl.TeamBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.statusHistoryAgent.StatusHistoryAgentRObj;
import com.velos.eres.service.stdSiteRightsAgent.StdSiteRightsAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studyIdAgent.StudyIdAgentRObj;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.teamAgent.TeamAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.NewStudyTeamAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.Duration;
import com.velos.services.model.NVPair;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyOrganization;
import com.velos.services.model.StudyOrganizationIdentifier;
import com.velos.services.model.StudyOrganizations;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudyStatuses;
import com.velos.services.model.StudySummary;
import com.velos.services.model.StudyTeamMember;
import com.velos.services.model.StudyTeamMembers;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;
 

/**
 * @author dylan
 *
 */
@Stateless
@Remote(StudyService.class)
public class StudyServiceImpl 
extends AbstractService
implements StudyService {

	public static final String ACTIVE_STUDY_STATUS_CODE = "active";

	private static Logger logger = Logger.getLogger(StudyServiceImpl.class.getName());
	@EJB
	SiteAgentRObj siteAgent;

	@EJB
	private StudyAgent studyAgent;

	@EJB
	private StudyIdAgentRObj studyIdAgent;
	
	@EJB 
	private StudySiteAgentRObj studySiteAgent;

	@EJB
	private TeamAgentRObj teamAgent;
	
	@EJB
	private StudyStatusAgentRObj studyStatusAgent;
	
	@EJB
	private UserAgentRObj userAgent;


	@EJB
	private ObjectMapService objectMapService;

	@EJB
	GrpRightsAgentRObj groupRightsAgent;

	@EJB
	StatusHistoryAgentRObj statusHistoryAgent;
	
	@EJB
	StdSiteRightsAgentRObj studySiteRightsAgent;
	
	@EJB
	SummaryCLOBService summaryClobService;
	
	@Resource 
	private SessionContext sessionContext;
	
	private Hashtable<String, UserBean> newNonSystemUsers = null;

	private static final Integer STUDY_NUMBER_NOT_UNIQUE_ERROR = -3;


	private static final String ROLE_CODE_PRINCIPAL_INVESTIGATOR = "role_prin";
	private static final String ROLE_CODE_STUDY_COORDINATOR = "role_coord";
	
	private static final String STUDY_TEAM_MODULE_NAME="er_studyteam";
	private static final String STUDY_STATUS_HISTORY_MODULE_NAME="er_status_history";
	private static final String PIPE = "|";
	

	
//	/**
//	 * Updates an existing study
//	 * 
//	 * @param study
//	 * @param createNonSystemUsers 
//	 * 	Determines whether to create non-system users if users who were identified
//	 * 	in the study team cannot be matched. If true, will create non-system
//	 * 	users with the information provided. If false and there is no match, will
//	 * 	report as a fatal issue in the ResponseHolder.
//	 * 
//	 * @exception ValidationException - thrown when one or more validation issues
//	 * occur, including the special requirement that a study author must exist
//	 */
//	@TransactionAttribute(REQUIRED)
//	public ResponseHolder updateStudy(
//			Study study, 
//			boolean createNonSystemUsers)
//	throws 
//	OperationException, 
//	ValidationException{
//
//		if  (study.getStudyId() == null){
//			throw new OperationException(new Issue("Study Identifier is required"));
//		}
//
//
//		StudySummary studySummary = study.getStudySummary();
//		//check for any validation issues on the incoming study object
//		List<Issue> validationIssues = getDataValidationIssues(study);
//		if (validationIssues.size() > 0){
//			response.getIssues().addAll(validationIssues);
//			if (logger.isDebugEnabled()) logger.debug("Validation Issues found for study " + studySummary.getStudyNumber());
//			throw new ValidationException(validationIssues);
//		}
//
//		StudyIdentifier studySysId = study.getStudyId();
//
//
//		//Instantiate a StudyBean object. We will populate this 
//		//with information from our study then persist it.
//		Integer studyPK = locateStudyPK(study.getStudyId());
//		StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
//
//		ResponseHolder response = persistStudy(
//				study, 
//				studyBean, 
//				createNonSystemUsers,
//				false);
//		return response;
//	}

	/**
	 * Creates a new Study.
	 * 
	 * @param study
	 * @param createNonSystemUsers 
	 * 	Determines whether to create non-system users if users who were identified
	 * 	in the study team cannot be matched. If true, will create non-system
	 * 	users with the information provided. If false and there is no match, will
	 * 	report as a fatal issue in the ResponseHolder.
	 * 
	 * @exception ValidationException - thrown when one or more validation issues
	 * occur, including the special requirement that a study author must exist
	 */
	@TransactionAttribute(REQUIRED)
	public ResponseHolder createStudy(
			Study study, 
			boolean createNonSystemUsers)
	throws 
	OperationException{

		try{

			StudySummary studySummary = study.getStudySummary();
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasNewPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to create studies"));
				throw new AuthorizationException("User Not Authorized to create studies");
			}


			validate(study);

			//Instantiate a StudyBean object. We will populate this 
			//with information from our study then persist it.
			StudyBean newStudyBean = new StudyBean();

			Integer studyPK = createStudy(
					study, 
					newStudyBean, 
					createNonSystemUsers);
			//DRM Removed references to system ID and object map service for 2.0 build 100
			//create SystemId for new Study
//			String externalId = 
//				objectMapService.createObjectMap(
//						ObjectMapService.PERSISTENCE_UNIT_STUDY, 
//						newStudyBean.getId());
			StudyIdentifier studyId = 
				new StudyIdentifier(study.getStudySummary().getStudyNumber());
			
			studyId.setStudyNumber(study.getStudySummary().getStudyNumber());
			//DRM Removed references to system ID and object map service for 2.0 build 100
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			studyId.setOID(map.getOID()); 
			studyId.setPK(studyPK);
			response.addObjectCreatedAction(studyId);

		} 
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}
	
	/**
	 * @param summary
	 * @param studyBean
	 * @param isNew
	 * @return
	 */
	private Integer createStudy(
			Study study,
			StudyBean studyBean,
			boolean createNonSystemUsers) 
	throws OperationException, ValidationException{
	    
	    if (newNonSystemUsers != null) {
	        newNonSystemUsers.clear();
	        newNonSystemUsers = null;
	    }
	    newNonSystemUsers = new Hashtable<String, UserBean>();
		
		//We wrap up the persistence of a number of objects into the same
		//transaction here. StudyBean, TeamBean, even UserBeans. Start with
		//StudyBean, then use its new primary key when creating the referenced beans.
		
		//first, we'll overlay the study summary fields into the studyBean...things
		//we can do without persisting.
		studySummaryIntoBean(
				0,
				study.getStudySummary(), 
				studyBean, 
				createNonSystemUsers,
				true);
		
		Integer studyPK = null;

		//persist new study
		studyPK = studyAgent.setStudyDetails(studyBean);	
		
		studyBean.setId(studyPK);
		
		//odd api here...if studypk could be the pk of the new study,
		//or it could be an error indicator (-3) if study number exists.
		//if we get that, rollback and get out!
		if (studyPK == STUDY_NUMBER_NOT_UNIQUE_ERROR){
			sessionContext.setRollbackOnly();
			addIssue(new Issue(IssueTypes.STUDY_NUMBER_EXISTS));
			throw new OperationException("Study Number Already Exists");

		}
		
		//DRM - Fix for 5400
		//flush the results of the study to the database.
		//In early versions of this code, the code is 
		//distributed in a separate file from the study EJB.
		//This created an issue where subsequent calls to EJB that involve
		//the newly created study fail because it hasn't really been flushed.
		//This will not be a problem when the files are merged.
		studyAgent.flush();
		//end Fix for 5400
		
		
		//now populate team auth module and pass it to the various
		//internal methods that require access to the various privileges
		//that you suck up in the constructor
		//TeamAuthModule teamAuth = getTeamAuthModule(studyPK);
		//KM-#5531- Checking new study team authorization while creating new study.
		NewStudyTeamAuthModule teamAuth = new NewStudyTeamAuthModule();
		
		updateSummaryClobFields(studyPK, study.getStudySummary());
	
		//virendra: #5686
		persistsStudySummaryMoreStudyDetails(study.getStudySummary(),studyPK,callingUser);
		//end #5686
		//TODO set default version
		//		    if (output > 0) {
		//                try {
		//                    conn = EJBUtil.getConnection();
		//                    cstmt = conn
		//                            .prepareCall("{call PKG_STUDYSTAT.SP_CREATE_DEFAULT_VERSION(?,?,?)}");
		//                    cstmt.setInt(1, output);
		//                    cstmt.setInt(2, Integer.parseInt(this.creator));
		//                    cstmt.setString(3, this.ipAdd);
		//                    cstmt.execute();
		//                } catch (SQLException ex) {
		//                    Rlog
		//                            .fatal(
		//                                    "study",
		//                                    "In setStudyDetails call to sp_create_default_Version EXCEPTION IN calling the procedure"
		//                                            + ex);
		//                } finally {
		//                    try {
		//                        if (cstmt != null)
		//                            cstmt.close();
		//                    } catch (Exception e) {
		//                    }
		//                    try {
		//                        if (conn != null)
		//                            conn.close();
		//                    } catch (Exception e) {
		//                    }
		//                }
		//            }
		
		
		//Add primary keys of the team members that are stored 
		//at the study level (er_study) . This is a separate task from 
		//creating the rows in team (er_studyteam). But we do it here
		//so that the keys are in the newStudyBean before updating.
		//The task of adding to er_studyteam has to be done later
		//after the primary key for this study has been created.
		persistStudyLevelTeamMembers(
				studyBean, 
				study.getStudyTeamMembers(), 
				createNonSystemUsers);
		
		//now that the study exists and we have it's primary key,
		//we can start hanging items on reference tables onto it
		//begin Study Statuses
		//virendra
		
		
		
		//begin study organizations
		createStudyOrganizations(
					studyBean.getId(), 
					study.getStudyOrganizations(),
					teamAuth);

		//end study organizations


		//set study team - we can do this now that we 
		//have a primary key for the study
		createStudyTeam(
					studyBean.getId(), 
					study.getStudyTeamMembers(), 
					teamAuth,
					createNonSystemUsers);
		
		//5642 and 5643
		createStudyStatuses(
				studyBean.getId(),
				study.getStudyStatuses(),
				teamAuth);

		//end study team
		//create a studyId to send in the response
		
		//Create Study Statuses
	
	//	StudyIdentifier studyId = new StudyIdentifier(study.getStudySummary().getStudyNumber()); // studyId is not used anywhere
		
		return studyPK; 
	}

	/**
	 * Updates fields in the study summary that are 
	 * represented as CLOBs, which means they can't
	 * accurately be done just with studyAgent.
	 * 
	 * 
	 * @param studyPK
	 * @param summary
	 * @throws OperationException
	 */
	private void updateSummaryClobFields(
			Integer studyPK, 
			StudySummary summary)
	throws
		OperationException{
		
		try{
			//DRM - fixed for 5246 1/12/2010.
			//Used to use CommonDAO, but when creating the study,
			//the stored procedure it used was silently failing because
			//the container-managed transaction hasn't flushed, because that 
			//the primary key for the study (created in the container-managed 
			//transaction) is not known to the stored procedure.
			//So, we created a little EJB for these fields and use it here.
			SummaryCLOB sc = new SummaryCLOB();
			sc.setSummary(summary.getSummaryText());
			sc.setObjective(summary.getObjective());
			sc.setStudyPK(studyPK);
			
			summaryClobService.updateCLOB(sc);
			
		}
		catch(Exception e){
			throw new OperationException(e);
		}
	}
	
	private void studyTeamMemberIntoBean(
			StudyTeamMember studyTeamMember,
			TeamBean legacyBean,
			Integer studyId,
			boolean createNonSystemUsers) throws OperationException{
		
		UserBean studyTeamUserB = null;
		try {
			studyTeamUserB = ObjectLocator.userBeanFromIdentifier(
					callingUser,
					studyTeamMember.getUserId(), 
					userAgent, 
					sessionContext, 
					objectMapService);
		} catch (MultipleObjectsFoundException e) {

			addIssue(
					new Issue(
							IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched study team by user"));
			throw new OperationException();
		}
		
        // At this time the new non-system user has not been committed to DB until transaction is complete.
        // So studyTeamUserB is still null; thus we need to use the bean in memory
		if (createNonSystemUsers && studyTeamUserB == null) {
		    studyTeamUserB = getNonSystemUserFromHash(studyTeamMember.getUserId());
		}
		
		if (studyTeamUserB == null ||
				studyTeamUserB.getUserId() == null){
			addIssue(new Issue(
							IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, "User not found"));
			throw new OperationException();
		}
		
		//virendra: #5620,5736
		//if user is hidden do not add it to the study
		if(studyTeamUserB.getUserHidden().equals("1")){
			addIssue(
					new Issue(
							IssueTypes.USER_HIDDEN, 
							"User is hidden, Cannot be added to Study, USER_HIDDEN=(" +
							"UserLoginName= " + studyTeamUserB.getUserLoginName()
							+", UserFirstName= "+studyTeamUserB.getUserFirstName()
							+", UserlastName= "+studyTeamUserB.getUserLastName()
							+studyTeamMember.getUserId().getOID() != null? studyTeamMember.getUserId().getOID() : ""));
			throw new OperationException();
		}
		//virendra: #5620 end
		legacyBean.setTeamUser(
				EJBUtil.integerToString(
						studyTeamUserB.getUserId()));
		
		String userRoleCode = null;
		try {
			userRoleCode = dereferenceCodeStr(
					studyTeamMember.getTeamRole(), 
					CodeCache.CODE_TYPE_ROLE, 
					callingUser);
		} catch (CodeNotFoundException e) {
			addIssue(
					new Issue(
						IssueTypes.DATA_VALIDATION, 
						"Team Role Code Not Found: " + studyTeamMember.getTeamRole().getCode()));
			throw new OperationException();
		}
		
		String userTypeStr = "D";
		/*
        StudyTeamMember.UserType userType = studyTeamMember.getUserType();
		if (userType != null){
			if (userType == StudyTeamMember.UserType.DEFAULT){
				userTypeStr = "D";
			}
			else if (userType == StudyTeamMember.UserType.SUPER_USER){
				userTypeStr = "S";
			}
		}
		*/
		
		
		
		legacyBean.setCreator(getCreatorString());
		legacyBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		legacyBean.setTeamUserRole(userRoleCode);
		legacyBean.setTeamUserType(userTypeStr);
		legacyBean.setStudyId(studyId.toString());	
		legacyBean.setTeamStatus(studyTeamMember.getStatus().getCode());
	}
	
	private void updateStudyOrganization(
			Integer studyPK,
			StudyOrganization studyOrganization,
			TeamAuthModule teamAuthModule) throws OperationException{
		if (!TeamAuthModule.hasEditPermission(
				teamAuthModule.getStudyTeamPrivileges())){
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have team edit permission"));
			throw new AuthorizationException("User does not have team edit permission");
		}
		
		StudySiteBean legacyBean =
			locateStudyOrganizationPK(studyPK, studyOrganization.getOrganizationIdentifier());
		
		if (legacyBean == null){
			Issue issue = new Issue(IssueTypes.STUDY_ORG_NOT_FOUND, 
					studyOrganization.getOrganizationIdentifier().toString());
			addIssue(issue);
			throw new OperationException();
		}
		studySiteIntoBean(
				studyOrganization, 
				legacyBean,
				studyPK,
				EJBUtil.stringToInteger(legacyBean.getSiteId())); //send existing study site id...no need to recalculate
		Integer studySitePK = studySiteAgent.updateStudySite(legacyBean);
		if (studySitePK == -2){
			Issue issue = new Issue(IssueTypes.STUDY_ERROR_CREATE_ORGANIZATION, 
					studyOrganization.getOrganizationIdentifier().toString());
			addIssue(issue);
			throw new OperationException();
		}

	}
	
	private void updateStudyTeamMember(
			Integer studyPK,
			StudyTeamMember studyTeamMember,
			TeamAuthModule teamAuthModule) throws OperationException{
		if (!TeamAuthModule.hasEditPermission(
				teamAuthModule.getStudyTeamPrivileges())){
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have team edit permission"));
			throw new AuthorizationException("User does not have team edit permission");
		}
		
		TeamBean legacyBean = 
			locateStudyTeamMember(studyPK, studyTeamMember.getUserId());
		
		//get the initial status code PK...if this changes, we'll
		//need to add a status history row, too.
		String initialStatusCd = legacyBean.getTeamStatus();
		
		studyTeamMemberIntoBean(
					studyTeamMember, 
					legacyBean,
					studyPK, 
					false); // createNonSystemUsers = false

		Integer studySitePK = teamAgent.updateTeam(legacyBean);
		
		if (studySitePK == -2){
			Issue issue = new Issue(IssueTypes.STUDY_ERROR_CREATE_ORGANIZATION, 
					studyTeamMember.getUserId().toString());
			addIssue(issue);
			throw new OperationException();
		}
		String updatedStatusCd = legacyBean.getTeamStatus();
		if (!initialStatusCd.equals(updatedStatusCd)){
			//dereference the status code so we can get the primary key
			Integer statusCdPK = null;
			CodeCache codes = CodeCache.getInstance();
			try{
				statusCdPK = codes.dereferenceCode(
					studyTeamMember.getStatus(), 
					CodeCache.CODE_TYPE_TEAM_STATUS, 
					EJBUtil.stringToNum(callingUser.getUserAccountId()));
			}
			catch(CodeNotFoundException e){
				logger.error(
						"Could not find code: " + studyTeamMember.getStatus().getCode() , 
						e);
				addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Code not found for more study details: " + 
						studyTeamMember.getStatus().getCode()));
				throw new OperationException();
			}
			
			//StdSiteRightsBean stdSiteRightsBean = new StdSiteRightsBean();
			//if updating the study team member resulted in 
			//a changed in their status code, then we need to 
			//write a row to status history.
			
			StatusHistoryBean statusHistoryBean = new StatusHistoryBean();
			statusHistoryBean.setCreator(getCreatorString());
			//statusHistoryBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
			statusHistoryBean.setIsCurrentStat("1");
			statusHistoryBean.setModifiedBy(getCreatorString());
			statusHistoryBean.setRecordType("N"); //????
			statusHistoryBean.setStatusCodelstId(EJBUtil.integerToString(statusCdPK));
			statusHistoryBean.setStatusModuleTable(STUDY_TEAM_MODULE_NAME);
			statusHistoryBean.setStatusModuleId(EJBUtil.integerToString(legacyBean.getId()));
			//statusHistoryBean.setStatusEndDate(new Date(""));
			//virendra: Fixed Bug No. 5428
			statusHistoryBean.setStatusStartDate(new Date());//start date hard coded
			int statusHistoryPK = 
				statusHistoryAgent.setStatusHistoryDetails(statusHistoryBean);
				//statusHistoryAgent.updateStatusHistory(statusHistoryBean);
				//statusHistoryAgent.setStatusHistoryDetailsWithEndDate(statusHistoryBean);
				//statusHistoryAgent.updateStatusHistoryWithEndDate(statusHistoryBean);
			
			if (statusHistoryPK == 0){
				Issue issue = new Issue(
						IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, 
						"Error setting team memeber status" + 
						studyTeamMember.getUserId().toString());
				addIssue(issue);
				throw new OperationException();

			}
		}

	}

	
	private void updateStudySummary(
			Integer studyPK,
			StudySummary studySummary,
			StudyBean studyBean,
			boolean createNonSystemUsers,
			TeamAuthModule teamAuthModule) throws OperationException{
		if (!TeamAuthModule.hasEditPermission(
				teamAuthModule.getStudySummaryPrivileges())){	
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have team summary edit permission"));
			throw new AuthorizationException("User does not have summary edit permission");
		}
		
		//5543 DRM - need to check that we're not trying 
		//to update the study number to an existing study number
		if (studySummary.getStudyNumber() != null){
			Integer otherStudyPK =  
				StudyDao.getStudyPK(
							studySummary.getStudyNumber(), 
							EJBUtil.stringToNum(callingUser.getUserAccountId()));
			//check that the provided study number either doesn't exist
			if ((otherStudyPK != null && otherStudyPK > 0) &&
					otherStudyPK.intValue() != studyPK.intValue()){
				addIssue(
						new Issue(
								IssueTypes.STUDY_NUMBER_EXISTS, 
								"Cannot update study number, study number already exists: " + studySummary.getStudyNumber()));
				throw new OperationException();
			}
			
		}
		
		studySummaryIntoBean(
				studyPK,
				studySummary, 
				studyBean, 
				createNonSystemUsers,
				false);
		
		studyAgent.updateStudy(studyBean);
		updateSummaryClobFields(studyPK, studySummary);
		//virendra: #5686
		persistsStudySummaryMoreStudyDetails(studySummary,studyPK,callingUser);
		
		
		
	}	
	
	/**
	 * Takes a sets values on a StudyBean based on values from the StudySummary object
	 * @param studyPK
	 * @param studySummary
	 * @param persistentStudyBean
	 * @param createNonSystemUsers
	 * @param isNew
	 * @throws OperationException
	 */
	private void studySummaryIntoBean(
			Integer studyPK,
			StudySummary studySummary, 
			StudyBean persistentStudyBean,
			boolean createNonSystemUsers,
			boolean isNew)
	throws 
	OperationException{
		
		//We wrap up the persistence of a number of objects into the same
		//transaction here. StudyBean, TeamBean, even UserBeans. Start with
		//StudyBean, then use its new primary key when creating the referenced beans.
	
		persistentStudyBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		
		if (isNew){
			persistentStudyBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		}
		
		persistentStudyBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
		persistentStudyBean.setStudyNumber(studySummary.getStudyNumber());
		persistentStudyBean.setAccount(callingUser.getUserAccountId());
		
		//KM-#5594
		if (studySummary.getStudyTitle() == null || studySummary.getStudyTitle().equals("")) {
			response.getIssues().add(
					new Issue(
							IssueTypes.DATA_VALIDATION,
							"Study Title Is null"));
			throw new ValidationException("Study Title Is Null");
		}
		
		persistentStudyBean.setStudyTitle(studySummary.getStudyTitle());
		persistentStudyBean.setStudyProduct(studySummary.getAgentDevice());
		
		
		//KM-#5610
		Integer stdDivisionCodePk = 0;
		Integer therapeuticAreaCodePK = 0;
		String divSubType = "";
		
		//----begin validating that division/tarea follow proper relationship
		if (studySummary.getDivision() != null) {
			try{
					stdDivisionCodePk = dereferenceCode(studySummary.getDivision(), CodeCache.CODE_TYPE_DIVISION, callingUser);
			}catch(CodeNotFoundException e){
				addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Division Code not found"));
			}
			
			CodeCache codes = CodeCache.getInstance();
			CodeDao cdDiv = codes.getCodes(CodeCache.CODE_TYPE_DIVISION,EJBUtil.stringToNum(callingUser.getUserAccountId()));
			divSubType = cdDiv.getCodeSubtype(stdDivisionCodePk);
		}

		if (studySummary.getTherapeuticArea() != null){
			try{
			therapeuticAreaCodePK = dereferenceCode(studySummary.getTherapeuticArea(),CodeCache.CODE_TYPE_THERAPUTIC_AREA, callingUser);
			}catch(CodeNotFoundException e){
				addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Therapeutic Area Code not found"));
			}
		} else {
			response.getIssues().add(
					new Issue(
							IssueTypes.DATA_VALIDATION,
							"Therapeutic Area Is null"));
			throw new ValidationException("Therapeutic Area Is Null");
		}
		
		CodeCache codes = CodeCache.getInstance();
		CodeDao cdTarea = codes.getCodes(CodeCache.CODE_TYPE_THERAPUTIC_AREA,EJBUtil.stringToNum(callingUser.getUserAccountId()));
		String custCol1 = cdTarea.getCodeCustomCol1(therapeuticAreaCodePK);
		
		if(StringUtil.isEmpty(custCol1) && (StringUtil.isEmpty(divSubType))) {
			persistentStudyBean.setStudyTArea(EJBUtil.integerToString(therapeuticAreaCodePK));
		}
		
		if (!StringUtil.isEmpty(custCol1) && (StringUtil.isEmpty(divSubType))){
			response.getIssues().add(
					new Issue(
							IssueTypes.DATA_VALIDATION,
							"Division is null"));
			throw new ValidationException("Division is null");
		}
		
		if (StringUtil.isEmpty(custCol1) && (!StringUtil.isEmpty(divSubType))){
			response.getIssues().add(
					new Issue(
							IssueTypes.DATA_VALIDATION,
							"Division and Therapeutic Area mismatch found"));
			throw new ValidationException("Division and Therapeutic Area mismatch found");
		}
		
		if ((!StringUtil.isEmpty(divSubType) && (!StringUtil.isEmpty(custCol1)))) {
			
			if(custCol1.indexOf(divSubType) != -1) {
				persistentStudyBean.setStudyDivision(stdDivisionCodePk.toString());
				persistentStudyBean.setStudyTArea(EJBUtil.integerToString(therapeuticAreaCodePK));
			} else
			{
				response.getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION,
								"Division and Therapeutic Area mismatch found"));
				throw new ValidationException("Division and Therapeutic Area mismatch found");
			}
		}
		//---end validating that division/tarea follow proper relationship	
		
		//----set disease sites
		boolean isFirstDiseaseSite = true;
		StringBuilder diseaseSitePKs = new StringBuilder();
		for (Code diseaseSite : studySummary.getDiseaseSites()){

			if (isFirstDiseaseSite){
				isFirstDiseaseSite = false;					
			}
			else{
				diseaseSitePKs.append(",");
			}
			try{
				diseaseSitePKs.append(
						dereferenceCodeStr(diseaseSite, CodeCache.CODE_TYPE_DISEASE_SITE, callingUser));
			}catch(CodeNotFoundException e){
				addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Disease Site not found"));
			}

		}
	
		persistentStudyBean.setDisSite(diseaseSitePKs.toString());
		
		//-----end set disease sites

		//KM-#5383 
		String studyLinkedToStr = "";
		if (studySummary.getStudyLinkedTo() != null) {
			studyLinkedToStr = EJBUtil.integerToString(locateStudyPK(studySummary.getStudyLinkedTo()));
		}
		persistentStudyBean.setStudyAssoc(studyLinkedToStr);
		
		persistentStudyBean.setStudyICDCode1(studySummary.getSpecificSite1());
		persistentStudyBean.setStudyICDCode2(studySummary.getSpecificSite2());
		persistentStudyBean.setStudyICDCode3(studySummary.getSpecificSite3());
		persistentStudyBean.setStudyNSamplSize(
				EJBUtil.integerToString(
						studySummary.getNationalSampleSize()));
		
		
		String studyDurationUnit="";
		if (studySummary.getStudyDuration() != null){
			persistentStudyBean.setStudyDuration(String.valueOf(studySummary.getStudyDuration().getValue()));
			
			switch(studySummary.getStudyDuration().getUnit()){
			case DAY:
				studyDurationUnit = "days";
				break;
			case WEEK:
				studyDurationUnit = "weeks";
				break;
			case MONTH:
				studyDurationUnit = "months";
				break;
			case YEAR:
				studyDurationUnit = "years";			
			}
		}
		persistentStudyBean.setStudyDurationUnit(studyDurationUnit);
		
		
		persistentStudyBean.setStudyEstBeginDt(studySummary.getEstimatedBeginDate());
		
		try{//TODO phase is required, but if we can't find a code, what to do?
			persistentStudyBean.setStudyPhase(
					dereferenceCodeStr(studySummary.getPhase(), CodeCache.CODE_TYPE_PHASE, callingUser));	
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Phase Code not found"));
		}
		
		try{
			persistentStudyBean.setStudyScope(
					dereferenceCodeStr(studySummary.getStudyScope(), CodeCache.CODE_TYPE_STUDY_SCOPE, callingUser));
		
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Scope Code not found"));
		}
		
		try{
			persistentStudyBean.setStudyResType(
				dereferenceCodeStr(studySummary.getResearchType(), CodeCache.CODE_TYPE_RESEARCH_TYPE, callingUser));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Research Type Code not found"));
		}
		
		try{
			persistentStudyBean.setStudyBlinding(
				dereferenceCodeStr(studySummary.getBlinding(), CodeCache.CODE_TYPE_BLINDING, callingUser));
		
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Blinding Code not found"));
		}
		
		try{
			persistentStudyBean.setStudyRandom(
					dereferenceCodeStr(studySummary.getRandomization(), CodeCache.CODE_TYPE_RANDOMIZATION, callingUser));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Randomization Code not found"));
		}

		///begin sponsor info
		try{
			persistentStudyBean.setStudySponsorName(dereferenceCodeStr(
				studySummary.getSponsorName(), CodeCache.CODE_TYPE_SPONSOR_NAME, callingUser));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Sponsorname Code not found"));
		}
		
		//Bug Fix - 6427 HardCoding $ as it is done in updateNewStudy.jsp. If want to provide functionlity for user specific values it will be enhancement. 
		try{
			Code code = new Code(); 
			code.setCode("$"); 
			persistentStudyBean.setStudyCurrency(dereferenceSchCodeStr(code, CodeCache.CODE_TYPE_CURRENCY, callingUser)); 
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Currency Code not found"));
		}
		persistentStudyBean.setStudySponsor(studySummary.getSponsorNameOther());

		persistentStudyBean.setStudySponsorIdInfo(studySummary.getSponsorID());
		persistentStudyBean.setStudyContact(studySummary.getSponsorContact());
		persistentStudyBean.setStudyInfo(studySummary.getSponsorOtherInfo());
		///end sponsor info

		persistentStudyBean.setStudyKeywords(studySummary.getKeywords());

		//Study Author is a required field ( according to a database trigger.)
		if (studySummary.getStudyAuthor() == null){
			response.getIssues().add(
					new Issue(
							IssueTypes.DATA_VALIDATION,
							"Study Author Is null"));
			throw new ValidationException("Study Author Is Null");
		}

		//use the information from the studyauthor object to find
		//a UserBean.
		UserBean studyAuthorBean = null;
		try {
			studyAuthorBean = ObjectLocator.userBeanFromIdentifier(
					callingUser,
					studySummary.getStudyAuthor(), 
					userAgent, 
					sessionContext, 
					objectMapService);
		} catch (MultipleObjectsFoundException e) {
			addIssue(
					new Issue(
							IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple study authors for " + studySummary.getStudyAuthor().toString()));
			throw new OperationException("Multiple study authors found");
		}

		if (studyAuthorBean == null && createNonSystemUsers){
//			studyAuthorBean = createNonSystemUser(
//					userAgent, 
//					objectMapService,
//					studySummary.getStudyAuthor(),
//					callingUser);
		    addIssue(
		            new Issue(
		                    IssueTypes.DATA_VALIDATION, 
		                    "Cannot create non-system user for study author " + studySummary.getStudyAuthor().toString()
		                    + "; use existing user for study author"));
		    throw new OperationException();
		}

		if (studyAuthorBean == null){
			addIssue(new Issue(IssueTypes.STUDY_AUTHOR_NOT_FOUND));
			if (logger.isDebugEnabled()) logger.debug("no study author found for study " + studySummary.getStudyNumber());
			throw new ValidationException();
		}
		else{
			persistentStudyBean.setStudyAuthor(
					EJBUtil.integerToString(
							studyAuthorBean.getUserId()));
		}
		
		//KM-#5383
		if (studySummary.getPrincipalInvestigator() != null){
			
			UserBean studyPiBean = null;
			try {
				studyPiBean = ObjectLocator.userBeanFromIdentifier(
						callingUser,
						studySummary.getPrincipalInvestigator(), 
						userAgent, 
						sessionContext, 
						objectMapService);
			} catch (MultipleObjectsFoundException e) {
				addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple study prinicipal investigator for " + studySummary.getPrincipalInvestigator().toString()));
				throw new OperationException("Multiple study prinicipal investigator found");
			}
			
			if (studyPiBean == null && 
					createNonSystemUsers){
				studyPiBean = getFromHashOrCreateNonSystemUser(
						userAgent, 
						objectMapService,
						studySummary.getPrincipalInvestigator(),
						callingUser);
			}
			
			if (studyPiBean == null){
				addIssue(new Issue(IssueTypes.STUDY_PRINCIPAL_INVESTIGATOR_NOT_FOUND));
				if (logger.isDebugEnabled()) logger.debug("no prinicipal investigator found for study " + studySummary.getStudyNumber());
				throw new ValidationException();
			}
			else{
				persistentStudyBean.setStudyPrimInv(
						EJBUtil.integerToString(
							studyPiBean.getUserId()));
			}
		}
		else{
			persistentStudyBean.setStudyPrimInv("");
		}
		
		if (studySummary.getStudyContact()!= null){
			UserBean studyContactBean = null;
			try {
				studyContactBean = ObjectLocator.userBeanFromIdentifier(
						callingUser,
						studySummary.getStudyContact(), 
						userAgent, 
						sessionContext, 
						objectMapService);
			} catch (MultipleObjectsFoundException e) {
				addIssue(
						new Issue(
								IssueTypes.MULTIPLE_OBJECTS_FOUND, 
								"Found multiple study coordinator for " + studySummary.getStudyContact().toString()));
				throw new OperationException("Multiple study coordinator found");
			}
			
			if (studyContactBean == null && 
					createNonSystemUsers){
				studyContactBean = getFromHashOrCreateNonSystemUser(
						userAgent, 
						objectMapService,
						studySummary.getStudyContact(),
						callingUser);
			}
			
			if (studyContactBean == null){
				addIssue(new Issue(IssueTypes.STUDY_COORDINATOR_NOT_FOUND));
				if (logger.isDebugEnabled()) logger.debug("no study coordinator found for study " + studySummary.getStudyNumber());
				throw new ValidationException();
			}
			else{
				persistentStudyBean.setStudyCoordinator(
						EJBUtil.integerToString(
								studyContactBean.getUserId()));
			}
		}
		else{
			persistentStudyBean.setStudyCoordinator("");
		}
		
		persistentStudyBean.setMajAuthor(studySummary.isPIMajorAuthor()==true ? "Y" : "N");
		//KM-#5384
		persistentStudyBean.setStudyOtherPrinv(studySummary.getPrincipalInvestigatorOther());
		persistentStudyBean.setStudyIndIdeNum(studySummary.getStudyIndIdeNum());
		
		//virendra:for #5246 start
		
		Integer studyTypeCodePK = dereferenceCode(studySummary.getStudyType(),CodeCache.CODE_TYPE_STUDY_TYPE, callingUser);
		persistentStudyBean.setStudyType(EJBUtil.integerToString(studyTypeCodePK));
		
		String strInvestigatorHeldIndIde = "0";
		if(studySummary.getIsInvestigatorHeldIndIde() != null){
			strInvestigatorHeldIndIde = (studySummary.getIsInvestigatorHeldIndIde()== true ? "1":"0");
		}
		persistentStudyBean.setStudyInvIndIdeFlag(strInvestigatorHeldIndIde);
		//virendra:for #5246 end

		//Virendra: Fixed:5248
		
		//KM-#5629
		String strIsStdDefPublic = "0";
		String strIsStdDesignPublic = "0";
		String strIsStdDetailPublic = "0";
		String strIsStdSponsorInfoPublic = "0";
		
		if (studySummary.getIsStdDefPublic() != null) 
			strIsStdDefPublic = (studySummary.getIsStdDefPublic()== true ? "1":"0");
		
		if (studySummary.getIsStdDesignPublic() != null) 
			strIsStdDesignPublic =(studySummary.getIsStdDesignPublic()== true ? "1":"0");
		
		if (studySummary.getIsStdDetailPublic() != null) 
			strIsStdDetailPublic =(studySummary.getIsStdDetailPublic()== true ? "1":"0");
		
		if (studySummary.getIsStdSponsorInfoPublic() != null) 
			strIsStdSponsorInfoPublic =(studySummary.getIsStdSponsorInfoPublic()== true ? "1":"0");
		
		String strComboIsPublic = strIsStdDefPublic
										+strIsStdDesignPublic
										+strIsStdDetailPublic
										+strIsStdSponsorInfoPublic;

		persistentStudyBean.setStudyPubCol(strComboIsPublic);

		
		//if we have found multiple issues, let's get out now before trying to persists
		if (response.getIssues().getIssue().size() > 0){
			throw new OperationException();
		}

	


	}

	//	@TransactionAttribute(REQUIRED)
	//	public Study getStudyByStudyNumber(String studyNumber)
	//			throws OperationException {
	//
	//		try{
	//			if (logger.isDebugEnabled()) logger.debug("user found");
	//			
	//			Integer studyPK = 
	//				getStudyPKFromStudyNum(
	//						studyNumber, 
	//					EJBUtil.stringToNum(callingUser.getUserAccountId()));
	//			
	//			if (logger.isDebugEnabled()) logger.debug("study pk found for study " + studyNumber + " pk=" + studyPK);
	//			
	//			Study study = marshalStudyByStudyPK(studyPK);
	//			
	//			if (logger.isDebugEnabled()) logger.debug("study found for studyNumber" + studyNumber);
	//			
	//			if (study.getStudyId() == null){
	//				//now that we have a pk, let's get the systemId
	//				ObjectMap map = 
	//					objectMapService.getObjectMapFromPK(
	//							ObjectMapService.
	//							PERSISTENCE_UNIT_STUDY, studyPK);
	//				if (map != null){
	//					if (logger.isDebugEnabled()) logger.debug("Map found for studyNumber: " + studyNumber);
	//					study.setStudyId(new StudyIdentifier(new SystemId(map.getSystemId())));
	//				}
	//			}
	//			return study;
	//		}
	//		catch(OperationException e){
	//			response.getIssues().addAll(e.getIssues());
	//			sessionContext.setRollbackOnly();
	//			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", e);
	//			throw e;
	//		}
	//		catch(Throwable t){
	//			Issue issue = 
	//				new Issue(
	//						Issue.IssueSeverity.FATAL, 
	//						Issue.IssueType.THROWABLE, 
	//						"error: " + t.getMessage());
	//			
	//			response.getIssues().add(issue);
	//			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", t);
	//			throw new OperationException(response.getIssues());
	//		}
	//
	//	}

	@TransactionAttribute(REQUIRED)
	public Study getStudy(StudyIdentifier studyIdentifier) 
	throws OperationException {

		try{
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			StudyBean sb = studyAgent.getStudyDetails(studyPK);
			//Kanwal - fixed bug 6246
			if (sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + studyIdentifier.getOID()); 
				}
				if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + studyIdentifier.getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(studyIdentifier.getPK())))
				{
					errorMessage.append( " Study Pk: " + studyIdentifier.getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(); 
			}
			//Get the user's permission for viewing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view studies"));
				throw new AuthorizationException("User Not Authorized to view studies");
			}

			//now populate team auth module and pass it to the various
			//internal methods that require access to the various privileges
			//that you suck up in the constructor
			TeamAuthModule teamAuth = getTeamAuthModule(studyPK);
			
			//Kanwal - fixed bug 6193 - Check if user has privilege to view any tab in study
			Integer studySummaryAuth = teamAuth.getStudySummaryPrivileges();
			Integer studyTeamAuth = teamAuth.getStudyTeamPrivileges(); 
			Integer studystatAuth = teamAuth.getStudyStatusPrivileges(); 
			
			
			boolean hasViewPermissionStudySummary = TeamAuthModule.hasViewPermission(studySummaryAuth); 
			boolean hasViewPermissionStudyTeam = TeamAuthModule.hasViewPermission(studyTeamAuth); 
			boolean hasViewPermissionStudyStatus = TeamAuthModule.hasViewPermission(studystatAuth); 
			
			if(!(hasViewPermissionStudySummary || hasViewPermissionStudyStatus || hasViewPermissionStudyTeam))
			{
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to view study"));
				throw new AuthorizationException("User Not Authorized to view study");
			}
			

			Study study =
				marshalStudy(sb, teamAuth);
			//removed for build 102
//			study.setStudyId(studyIdentifier);
			if (logger.isDebugEnabled()) logger.debug("returning study studyNumber: " + studyIdentifier);
			return study;

		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", t);
			throw new OperationException(t);
		}

	}
	@TransactionAttribute(REQUIRED)
	public StudySummary getStudySummary(StudyIdentifier studyIdentifier) 
	throws OperationException {

		try{
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			//Get the user's permission for viewing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view studies"));
				throw new AuthorizationException("User Not Authorized to view studies");
			}

			//now populate team auth module and pass it to the various
			//internal methods that require access to the various privileges
			//that you suck up in the constructor
			TeamAuthModule teamAuth = getTeamAuthModule(studyPK);

			StudyBean sb = studyAgent.getStudyDetails(studyPK);
			//Kanwal - fixed bug 6246
			if (sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + studyIdentifier.getOID()); 
				}
				if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + studyIdentifier.getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(studyIdentifier.getPK())))
				{
					errorMessage.append( " Study Pk: " + studyIdentifier.getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException();
			}


			StudySummary study =
				marshalStudySummary(sb, teamAuth);
		
			if (logger.isDebugEnabled()) logger.debug("returning study studyNumber: " + sb.getStudyNumber());
			return study;

		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudySummary", e); 
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudySummary", t);
			throw new OperationException(t);
		}

	}
	
	/**
	 * Marshalls a study object instance from teh studyBean.
	 * @param studyBean
	 * @param teamAuth
	 * @return
	 * @throws OperationException
	 * @throws AuthorizationException
	 */
	private Study marshalStudy(StudyBean studyBean, TeamAuthModule teamAuth) 
	throws 
	OperationException, 
	AuthorizationException{


		//marshal the individual pieces of the study
		Study returnStudy = new Study();
		Integer userAccountPK = EJBUtil.stringToInteger(callingUser.getUserAccountId());

		//DRM for 5529, we added an authentication check
		//that throws an AuthenticationException. But we don't 
		//want that to blow up a full getStudy megamessage.
		try{
			returnStudy.setStudySummary(
				marshalStudySummary(studyBean, teamAuth));
		}
		catch(AuthorizationException e){
			if (logger.isDebugEnabled()) logger.debug("User does not have authorization to view Study Summary");
		}
		returnStudy.setStudyOrganizations(
				marshalStudyOrganizations(
						studyBean.getId(), 
						callingUser.getUserId(), 
						userAccountPK,
						teamAuth));		
		
		returnStudy.setStudyTeamMembers(
				marshalStudyTeam(
						studyBean.getId(), 
						userAccountPK,
						teamAuth));
		
		//DRM for 5540, we added an authentication check
		//that throws an AuthenticationException. But we don't 
		//want that to blow up a full getStudy megamessage.
		try{
			returnStudy.setStudyStatuses(
			        marshallStudyStatusList(
							studyBean.getId(),
							teamAuth));
		}
		catch(AuthorizationException e){
			if (logger.isDebugEnabled()) logger.debug("User does not have authorization to view Study Statuses");
		}
		
		returnStudy.setStudyIdentifier(marshallStudyIdentifier(studyBean)); 
				
		return returnStudy;

	} 


	private StudyIdentifier marshallStudyIdentifier(StudyBean studyBean) {
		StudyIdentifier studyId = new StudyIdentifier(); 
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyBean.getId()); 
		
		studyId.setOID(map.getOID());
		studyId.setPK(studyBean.getId());
		studyId.setStudyNumber(studyBean.getStudyNumber()); 
		
		return studyId; 
	}

	private void createStudyOrganizations(
			Integer studyPK,
			StudyOrganizations organizations,
			TeamAuthModule teamAuth) 
			throws 
			OperationException{
		
		//capture whether an exception occured creating one
		//of the items from the list. This lets us report exceptions
		//for all items, rather than just one.
		boolean exceptionOccured = false;
		//Bug Fix 6625
		//new Study - no organizations in DB yet	
		//maintain list of organizations added, we are not allowing duplicate organizations
		List<Integer> orgs = new ArrayList<Integer>(); 
		for (StudyOrganization studyOrganization : organizations.getStudyOrganizaton()){
			try{
				
				createStudyOrganization(studyPK, studyOrganization, teamAuth, orgs);
			}
			catch(OperationException e){
				if (logger.isDebugEnabled()) logger.debug(e);
				addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE, e.getMessage()));
				exceptionOccured = true;
				continue;
			}
		}
		
		if (exceptionOccured){
			//if at least one exception occured, throw so that 
			//the calling method can roll back.
			throw new OperationException();
		}

		 
	}

	private void studySiteIntoBean(
			StudyOrganization studyOrg, 
			StudySiteBean legacyStudySite,
			Integer studyPK,
			Integer sitePK)
	throws OperationException{

		//if this is a new study site, we create a blank 
		//StudySiteBean and fill it in. If this is an update
		//to an existing StudySite, we need to marshal the existing StudySiteBean, 
		//then update with existing values.

		legacyStudySite.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		legacyStudySite.setCreator(getCreatorString());
		//Virendra: Commented to fix #5397
//		legacyStudySite.setCurrentSitePatientCount(
//				EJBUtil.integerToString(studyOrg.getCurrentSitePatientCount()));

		//Virendra: Fixed #5481
		legacyStudySite.setIsReviewBasedEnrollment(
				booleanToOneZeroFlag(studyOrg.isReviewBasedEnrollmentFlag()));
		
		legacyStudySite.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		legacyStudySite.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));

		legacyStudySite.setRepInclude(booleanToOneZeroFlag(studyOrg.isReportableFlag()));
		//virendra: fixed #5558
		legacyStudySite.setSampleSize(EJBUtil.integerToString(studyOrg.getLocalSampleSize()));

		//////begin set enrollNotifyTo
		//build up a comma-separated list of primary keys for users

		if (studyOrg.getNewEnrollNotificationTo() != null){
			StringBuilder enrollToUsers = new StringBuilder();
			boolean isLast = false;
			int newEnrollUserNum = studyOrg.getNewEnrollNotificationTo().size();
			int counter = 0;
			for (UserIdentifier enrollToUser : studyOrg.getNewEnrollNotificationTo()){
				
				if (counter == newEnrollUserNum - 1 ){
					isLast = true;
				}

				UserBean enrollNotifyTo = null;
				try {
					enrollNotifyTo = ObjectLocator.userBeanFromIdentifier(
							callingUser,
							enrollToUser, 
							userAgent, 
							sessionContext, 
							objectMapService);
				} catch (MultipleObjectsFoundException e) {
					addIssue(
							new Issue(
									IssueTypes.MULTIPLE_OBJECTS_FOUND, 
									"Found multiple objects that matched notify enrollment user" + enrollToUser));
					continue;
				}//virendra
				if(enrollNotifyTo == null){
					addIssue(
							new Issue(
									IssueTypes.ENROLLMENT_NOTIFICATION_USER_NOT_FOUND, 
									"No objects that matched notify enrollment to user" + enrollNotifyTo));
				}
				if(enrollNotifyTo != null){
					enrollToUsers.append(enrollNotifyTo.getUserId());
				
					
					if (!isLast){
						enrollToUsers.append(",");
					}
						
				}
				counter++;
			}
			while(enrollToUsers.toString().endsWith(",")){
				enrollToUsers.deleteCharAt(enrollToUsers.length() -1);
			}
			legacyStudySite.setSendEnrNotificationTo(enrollToUsers.toString());
		}
		//////end enrollNotifyTo

		//////begin set enrollApproveDeny
		//build up a comma-separated list of primary keys for users
		if (studyOrg.getApproveEnrollNotificationTo() != null){
			StringBuilder enrollApproveDenyPKList = new StringBuilder();
			boolean isLast = false;
			//if(studyOrg.getApproveEnrollNotificationTo())
			int approveEnrollUserNum = studyOrg.getApproveEnrollNotificationTo().size();
			int counter = 0;
			//boolean isFirst = true;
			for (UserIdentifier enrollApprDeny : studyOrg.getApproveEnrollNotificationTo()){
				if (counter == approveEnrollUserNum -1 ){
					isLast = true;
				}


				UserBean approveEnrollTo = null;
				try {
					approveEnrollTo = ObjectLocator.userBeanFromIdentifier(
							callingUser,
							enrollApprDeny, 
							userAgent, 
							sessionContext, 
							objectMapService);
				} catch (MultipleObjectsFoundException e) {
					addIssue(
							new Issue(
									IssueTypes.MULTIPLE_OBJECTS_FOUND, 
									"Found multiple objects that matched approve/deny enrollment user" + approveEnrollTo));
					continue;
				}//virendra
				if(approveEnrollTo == null){
					addIssue(
							new Issue(
									IssueTypes.GET_APPROVE_ENROLLMENT_NOTIFICATION_USER_NOT_FOUND, 
									"No objects that matched notify approve enrollment to user" + approveEnrollTo));
				}
				if(approveEnrollTo != null){
				enrollApproveDenyPKList.append(approveEnrollTo.getUserId());
				
				if (!isLast){
					
					enrollApproveDenyPKList.append(",");
				}
				
			}
			counter++;
			
			}
			while(enrollApproveDenyPKList.toString().endsWith(",")){
				enrollApproveDenyPKList.deleteCharAt(enrollApproveDenyPKList.length() -1);
			}
			legacyStudySite.setSendEnrStatNotificationTo(enrollApproveDenyPKList.toString());
			
		}
		//////end enrollApproveDeny
		//legacyStudySite.set
		legacyStudySite.setSiteId(EJBUtil.integerToString(sitePK));
		legacyStudySite.setStudyId(studyPK.toString());
		legacyStudySite.setStudySitePublic(booleanToOneZeroFlag(studyOrg.isPublicInformationFlag()));
		try{
			legacyStudySite.setStudySiteType( 
				EJBUtil.integerToString(
						dereferenceCode(
								studyOrg.getType(), 
								CodeCache.CODE_TYPE_STUDY_ORG_TYPE,
								callingUser)));
		}catch(CodeNotFoundException e){
			addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "studySiteType Code not found"));
		}

	}


	private void persistStudyLevelTeamMembers(
			StudyBean studyB,
			StudyTeamMembers users,
			boolean createNonSystemUsers) 
	throws 
	OperationException{ 
		try{
			for (StudyTeamMember teamMember : users.getStudyTeamMember()){

				//marshal user identitities
				//TODO can we cache these for each operation???
				UserBean user = 
					ObjectLocator.userBeanFromIdentifier(
							callingUser,
							teamMember.getUserId(), 
							userAgent, 
							sessionContext, 
							objectMapService);
				//if still no user and createNonSystemUser == true, create a new non-System user
				if (createNonSystemUsers && user == null){
				    user = getFromHashOrCreateNonSystemUser(
				            userAgent, 
				            objectMapService, 
				            teamMember.getUserId(),
				            callingUser
				    );
				}
				// if still no user, skip to next
				if (user == null) { continue; }
				//look for "special" team member types and also add them to the studybean
				if (ROLE_CODE_PRINCIPAL_INVESTIGATOR.
						equalsIgnoreCase(teamMember.getTeamRole().getCode())){
					studyB.setStudyPrimInv(String.valueOf(user.getUserId()));
				}
				if (ROLE_CODE_STUDY_COORDINATOR.
						equalsIgnoreCase(teamMember.getTeamRole().getCode())){
					studyB.setStudyCoordinator(String.valueOf(user.getUserId()));
				}
			}
		}
		catch(MultipleObjectsFoundException mce)
		{
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple Users" ));
			throw new OperationException();
		}
		catch(Throwable t){
			addUnknownThrowableIssue(t);
			logger.debug("setStudyLevelTeamMembersInternal", t);
			throw new OperationException();
		}
	}

	private UserBean getFromHashOrCreateNonSystemUser(
	        UserAgentRObj userAgent,
	        ObjectMapService objectMapService,
	        UserIdentifier userIdent,
	        UserBean callingUser)
	throws OperationException {
	    UserBean nonSystemUser = getNonSystemUserFromHash(userIdent);
	    if (nonSystemUser == null) {
	        nonSystemUser = createNonSystemUser(userAgent, objectMapService, userIdent, callingUser);
	        putNonSystemUserIntoHash(nonSystemUser);
	    }
	    return nonSystemUser;
	}

	private void putNonSystemUserIntoHash(UserBean aNSUser) {
        if (aNSUser == null) { return; }
        StringBuffer pipedFullName = new StringBuffer();
        pipedFullName.append(StringUtil.trueValue(aNSUser.getUserFirstName()));
        pipedFullName.append(PIPE);
        pipedFullName.append(StringUtil.trueValue(aNSUser.getUserLastName()));
        if (newNonSystemUsers == null) { newNonSystemUsers = new Hashtable<String, UserBean>(); }
        newNonSystemUsers.put(pipedFullName.toString(), aNSUser);
	}
	
	private UserBean getNonSystemUserFromHash(UserIdentifier userIdentifier) {
	    if (userIdentifier == null || newNonSystemUsers == null) { return null; }
        StringBuffer pipedFullName = new StringBuffer();
        pipedFullName.append(StringUtil.trueValue(userIdentifier.getFirstName()));
        pipedFullName.append(PIPE);
        pipedFullName.append(StringUtil.trueValue(userIdentifier.getLastName()));
        return newNonSystemUsers.get(pipedFullName.toString());
	}

	private void createStudyTeam(
			Integer studyPK,
			StudyTeamMembers teamMembers,
			TeamAuthModule teamAuth,
			boolean createNonSystemUsers) 
			throws 
			OperationException{ 
		
		//capture whether an exception occurred creating one
		//of the items from the list. This lets us report exceptions
		//for all items, rather than just one.
		boolean exceptionOccured = false;
		//Bug Fix 6625 - maintain list of coming team members so duplicate team members can't be added
		List<Integer> addedTeamMember = new ArrayList<Integer>(); 
		for (StudyTeamMember teamMember : teamMembers.getStudyTeamMember()){
			try{
			    createStudyTeamMember(studyPK, teamMember, teamAuth, 
			            createNonSystemUsers, addedTeamMember);
			}
			catch(OperationException e){
				exceptionOccured = true;
				continue;
			}
		}
	
		if (exceptionOccured){
			//if at least one exception occured, throw so that 
			//the calling method can roll back.
			throw new OperationException();
		}

	}

	private void createStudyStatus(
			Integer studyPK,
		StudyStatus newStatus,
		TeamAuthModule teamAuthModule) 
		throws 
		OperationException{ 
		//5642 and 5643
		if (!TeamAuthModule.hasNewPermission(
				teamAuthModule.getStudyStatusPrivileges())){
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have team add permission"));
			throw new AuthorizationException("User does not have team add permission");
		}
		StudyStatusBean legacyBean = new StudyStatusBean();
		 
		//add details from the incoming StudyStatus into the legacy bean
		studyStatusIntoBean(newStatus, legacyBean, studyPK);
		
		int newStatusPK = studyStatusAgent.setStudyStatusDetails(legacyBean);
		//Begin Bug Fix : 7979 : Tarandeep Singh Bali
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_STATUS, newStatusPK);
		StudyStatusIdentifier studyStatusIdentifier = new StudyStatusIdentifier();
		studyStatusIdentifier.setOID(map.getOID());
		studyStatusIdentifier.setPK(newStatusPK);
		response.addObjectCreatedAction(studyStatusIdentifier);
        // End Bug Fix : 7979 : Tarandeep Singh Bali
		if ( newStatusPK < 1){
			addIssue(new Issue(IssueTypes.STUDY_ERROR_CREATE_STATUS));
			throw new OperationException();
		}
		
	}
	
	/**
	 * Overlay values from the StudyStatus bean into the 
	 * lecacy StudyStatusBean.
	 * 
	 * 	 * 
	 * @param newStatus
	 * @param legacyBean
	 * @param studyId
	 * @throws OperationException
	 */
	private void studyStatusIntoBean(
			StudyStatus newStatus,
			StudyStatusBean legacyBean,
			Integer studyId)
	throws OperationException{
		//12/06/2010 DRM Modified for 5399 and 5406
		
		//boolean lets us collect errors instead of 
		//throwing on the first one
		boolean operationSuccessful = true;
		String isCurrentStatStr = newStatus.isCurrentForStudy() ? "1" : "0";
		UserBean documentedByBean = null;
		try {
			if(newStatus.getDocumentedBy() != null){
				documentedByBean = ObjectLocator.userBeanFromIdentifier(
					callingUser,
					newStatus.getDocumentedBy(), 
					userAgent, 
					sessionContext, 
					objectMapService);
				if (documentedByBean == null){
					operationSuccessful = false;
					addIssue(
						new Issue(
								IssueTypes.GET_DOCUMENTED_BY_USERID_NOT_FOUND, 
								"Get Documented by User Id provided, but user not found:::" + newStatus.getDocumentedBy()));
				}
			}
		} catch (MultipleObjectsFoundException e) {
			operationSuccessful = false;
			addIssue(
					new Issue(
							IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched notify documented by user" + documentedByBean));
		}
		
		UserBean assignedToBean = null;
		try {
			if(newStatus.getAssignedTo() != null){
				assignedToBean = ObjectLocator.userBeanFromIdentifier(
						callingUser,
						newStatus.getAssignedTo(), 
						userAgent, 
						sessionContext, 
						objectMapService);
				if (assignedToBean == null){
					operationSuccessful = false;
					addIssue(
						new Issue(
								IssueTypes.GET_ASSIGNED_TO_USERID_NOT_FOUND, 
								"Get Assigned to User Id provided, but user not found:::" + newStatus.getAssignedTo()));
				}
			}
		
			
		
		} catch (MultipleObjectsFoundException e) {
			operationSuccessful = false;
			addIssue(
					new Issue(
							IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched notify documented by user" + assignedToBean));
		}
		
		Integer statusCodePK = null;
		try {
			statusCodePK = dereferenceCode(
					newStatus.getStatus(), 
					CodeCache.CODE_TYPE_STATUS, 
					callingUser);
		} catch (CodeNotFoundException e) {
			operationSuccessful = false;
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Study Status Code Not Found: " + newStatus.getStatus()));
		}
		
		Integer statusOutcomeCodePK = null;
		try {
			statusOutcomeCodePK = dereferenceCode(
					newStatus.getOutcome(), 
					CodeCache.CODE_TYPE_OUTCOME, 
					callingUser);
		} catch (CodeNotFoundException e) {
			operationSuccessful = false;
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Status Outcome Code Not Found: " + newStatus.getOutcome()));
		}
				
		Integer reviewBoardCodePK = null;
		try {
			reviewBoardCodePK = dereferenceCode(
					newStatus.getReviewBoard(), 
					CodeCache.CODE_TYPE_REVIEW_BOARD, 
					callingUser);
		} catch (CodeNotFoundException e) {
			operationSuccessful = false;
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Review Board Code Not Found: " + newStatus.getReviewBoard()));
		}
		
		Integer statusTypeCodePK = null;
		try {
			statusTypeCodePK = dereferenceCode(
					newStatus.getStatusType(), 
					CodeCache.CODE_TYPE_STATUS_TYPE,
					callingUser);
		} catch (CodeNotFoundException e) {
			operationSuccessful = false;
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Status Type " +
							"Code Not Found: " + newStatus.getStatusType()));
		}
		Integer sitePK = null;
		if (newStatus.getOrganizationId() != null){
			try {
				sitePK = 
					ObjectLocator.sitePKFromIdentifier(
							callingUser, 
							newStatus.getOrganizationId(), 
							sessionContext, 
							objectMapService);
				
				if (sitePK == null){
					operationSuccessful = false;
					addIssue(
							new Issue(
									IssueTypes.ORGANIZATION_NOT_FOUND, 
									"SiteID provided, but site not found " + newStatus.getOrganizationId()));
				}
				
			} catch (MultipleObjectsFoundException e) {
				operationSuccessful = false;
				addIssue(
						new Issue(
								IssueTypes.ORGANIZATION_NOT_FOUND, 
								"Multiple sites found for " + newStatus.getOrganizationId()));
			}
		}
	
		if (sitePK != null){
			//dylan 12/21/10 5631 and 5682 need to check that site is included in study			
			StudySiteBean studySiteB =
				studySiteAgent.findBeanByStudySite(studyId, sitePK);
			if (studySiteB == null){
				operationSuccessful = false;
				addIssue( 
						new Issue(
								IssueTypes.STUDY_STATUS_ORG_NOT_IN_STUDY,
								newStatus.getOrganizationId().toString()));
			}
			legacyBean.setSiteId(EJBUtil.integerToString(sitePK));
		}
		
		legacyBean.setCreator(getCreatorString());
		legacyBean.setCurrentStat(isCurrentStatStr);
		//legacyBean.setId(id);
		legacyBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		//legacyBean.setStatAppRenNo(statAppRenNo);
		//legacyBean.setStatApproval(statApproval);
		if(documentedByBean != null && documentedByBean.getUserId() != null){
			legacyBean.setStatDocUser(EJBUtil.integerToString(documentedByBean.getUserId()));
		}
	
		//legacyBean.setStatHSPN(statHSPN);
		//legacyBean.setStatMeetingDate(statMeetingDate);
		if (newStatus.getMeetingDate() != null) 
			legacyBean.setStatMeetingDt(newStatus.getMeetingDate());
		
		if (newStatus.getNotes() != null) 
			legacyBean.setStatNotes(newStatus.getNotes());
		
		if (newStatus.getValidFromDate() != null)
			legacyBean.setStatStartDate(newStatus.getValidFromDate());
		
		legacyBean.setStatStudy(EJBUtil.integerToString(studyId));
		//legacyBean.setStatValidDate(statValidDate);
		if (newStatus.getValidUntilDate() != null)
			legacyBean.setStatValidDt(newStatus.getValidUntilDate());
		
		if (statusCodePK != null)legacyBean.setStudyStatus(EJBUtil.integerToString(statusCodePK));
		
		if(assignedToBean != null && assignedToBean.getUserId() != null){
			legacyBean.setStudyStatusAssignedTo(EJBUtil.integerToString(assignedToBean.getUserId()));
		}

		if (statusOutcomeCodePK != null)
				legacyBean.setStudyStatusOutcome(EJBUtil.integerToString(statusOutcomeCodePK));
		
		if (reviewBoardCodePK != null)
			legacyBean.setStudyStatusReviewBoard(EJBUtil.integerToString(reviewBoardCodePK));
		
		if (statusTypeCodePK != null)
			legacyBean.setStudyStatusType(EJBUtil.integerToString(statusTypeCodePK));

		if (!operationSuccessful){
			throw new OperationException();
		}
	}

	
	/**
	 * Returns a list of study organizations that the user has access to.
	 * If the user does not have team access to view study organizations,
	 * returns an empty list.
	 * @param studyPK
	 * @param userPK
	 * @param userAccountPK
	 * @param teamAuth
	 * @return
	 */
	private StudyTeamMembers marshalStudyTeam(
			int studyPK, 
			int userAccountPK,
			TeamAuthModule teamAuth){
		ArrayList<StudyTeamMember> teamMembers = new ArrayList<StudyTeamMember>();
		if (logger.isDebugEnabled()) logger.debug("marshaling study team members");

		Integer studyOrgPrivileges = teamAuth.getStudyTeamPrivileges();

		if (!TeamAuthModule.hasViewPermission(studyOrgPrivileges)){
			if (logger.isDebugEnabled()) logger.debug("user does not have permission to view study team members");
			return new StudyTeamMembers();
		}

		TeamDao teamDAO =
			teamAgent.getTeamValuesBySite(studyPK, userAccountPK,0);
		
		CodeCache codeCache = CodeCache.getInstance();
		if (teamDAO.getTeamIds().size() > 0){
			for (int x = 0; x < teamDAO.getTeamIds().size(); x++){
				//String status = (String)teamDAO.getTeamStatus().get(x);
				String status = null;

				Integer roleCodePK = Integer.valueOf((String)teamDAO.getTeamRoleIds().get(x));
				Integer teamMemberUserPK = (Integer)teamDAO.getUserIds().get(x);

				String teamUserTypeStr = (String)teamDAO.getTeamUserTypes().get(x);

				//Get the current status history bean
				//so we can get the status code.
				String latestStatusCode =
					statusHistoryAgent.getLatestStatus(
							(Integer)teamDAO.getTeamIds().get(x), 
							STUDY_TEAM_MODULE_NAME);
				
				
				Code statusCode =
					new Code(
							CodeCache.CODE_TYPE_TEAM_STATUS,
							latestStatusCode);
					
				Code roleCode = 
					codeCache.getCodeSubTypeByPK(
							CodeCache.CODE_TYPE_ROLE, 
							roleCodePK,
							userAccountPK);

				UserIdentifier teamUser = 
					getUserIdFromPK(
							teamMemberUserPK, 
							objectMapService,
							userAgent);

				StudyTeamMember.UserType teamUserType = null;
				if (teamUserTypeStr != null){
                    teamUserType = StudyTeamMember.UserType.DEFAULT;
				}

				StudyTeamMember studyTeamMember = new StudyTeamMember();
			
				
				studyTeamMember.setStatus(statusCode);
				studyTeamMember.setTeamRole(roleCode);
				studyTeamMember.setUserId(teamUser);
				studyTeamMember.setUserType(teamUserType);
				teamMembers.add(studyTeamMember);
			}
		}
		
		StudyTeamMembers studyTeamMembers = new StudyTeamMembers(); 
		studyTeamMembers.setStudyTeamMember(teamMembers); 
		
		//Virendra:Fixed #6125
		StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
		StudyIdentifier studyId = marshallStudyIdentifier(studyBean);
		studyTeamMembers.setParentIdentifier(studyId);
	
		return studyTeamMembers;
	}

	//start virendra
	
	/**
	 * Returns a list of study Status that the user has access to.
	 * If the user does not have team access to view study statuses,
	 * returns an empty list.
	 * @param studyPK
	 * @param teamAuth
	 * @return
	 */
	
	private StudyStatuses marshallStudyStatusList(
			int studyPK, 
			TeamAuthModule teamAuth) throws OperationException{

		if (logger.isDebugEnabled()) logger.debug("marshalling studyStatus");
		
		int studyStatusPrivileges = teamAuth.getStudyStatusPrivileges();
		
		if (!TeamAuthModule.hasViewPermission(studyStatusPrivileges)){
			if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view study Status");
			//DRM for 5540
			addIssue(
					new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
							"User does not have view permission to view Study Status"));
			throw new AuthorizationException("User does not have view permission to view Study Status");
		}
		//Virendra:#6125
		List<StudyStatus> lstStudyStatus =  StudyStatusDAO.getStudyStatuses(
				studyPK, 
				EJBUtil.stringToNum(callingUser.getUserAccountId()));
	
		for(StudyStatus studyStatus:lstStudyStatus){
			
			//Virendra: #6237
			UserIdentifier userAssignedTo = studyStatus.getAssignedTo();
			UserIdentifier userDocumentedBy = studyStatus.getDocumentedBy();
			try {
				ObjectLocator.userBeanFromIdentifier(callingUser, userAssignedTo, userAgent, sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				e.printStackTrace();
			}
			try {
				ObjectLocator.userBeanFromIdentifier(callingUser, userDocumentedBy, userAgent, sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				e.printStackTrace();
			}
			
		}
		
		StudyStatuses studyStatus = new StudyStatuses(); 
		StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
		StudyIdentifier studyId = marshallStudyIdentifier(studyBean);
		studyStatus.setParentIdentifier(studyId);
		studyStatus.setStudyStatus(lstStudyStatus); 
		return studyStatus;
	}
	
	private StudyStatuses marshallStudyStatuses(
	        StudyBean studyBean, 
            TeamAuthModule teamAuth) throws OperationException{

        if (logger.isDebugEnabled()) logger.debug("marshalling studyStatus");
        
        int studyStatusPrivileges = teamAuth.getStudyStatusPrivileges();
        
        if (!TeamAuthModule.hasViewPermission(studyStatusPrivileges)){
            if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view study Status");
            //DRM for 5540
            addIssue(
                    new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
                            "User does not have view permission to view Study Status"));
            throw new AuthorizationException("User does not have view permission to view Study Status");
        }
        //Virendra:#6125
        List<StudyStatus> lstStudyStatus =  StudyStatusDAO.getStudyStatuses(
                studyBean.getId(), 
                EJBUtil.stringToNum(callingUser.getUserAccountId()));       
        StudyIdentifier studyId = marshallStudyIdentifier(studyBean);
	    StudyStatuses myStudyStatuses = new StudyStatuses();
	    myStudyStatuses.setStudyStatus(lstStudyStatus);
	    myStudyStatuses.setParentIdentifier(studyId);
	    return myStudyStatuses;
	}

	
	//ennd virendra
	
	public StudyStatuses getStudyStatuses(
			StudyIdentifier studyIdentifier) 
	
	throws OperationException{
		try{			
			//DRM - 5539 - check group rights to view studies
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
	
			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();
	
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view studies"));
				throw new AuthorizationException("User Not Authorized to view studies");
			}
			
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
			//Kanwal - fixed bug 6246
			if (studyBean == null || (! studyBean.getAccount().equals(callingUser.getUserAccountId()))){
				StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
				if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0 )
				{
					errorMessage.append(" OID:" + studyIdentifier.getOID()); 
				}
				if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0)
				{
					errorMessage.append( " Study Number:" + studyIdentifier.getStudyNumber()); 
				}
				if(!StringUtil.isEmpty(StringUtil.integerToString(studyIdentifier.getPK())))
				{
					errorMessage.append( " Study Pk: " + studyIdentifier.getPK());
				}
				Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(); 
			}

			TeamAuthModule teamAuthModule = getTeamAuthModule(studyPK);
			return marshallStudyStatuses(studyBean, teamAuthModule);
		
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudyStatuses", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl getStudyStatuses", t);
			throw new OperationException(response.getIssues());
		}
	}
	
	/**
	 * Returns a list of study organizations that the user has access to.
	 * If the user does not have team access to view study organizations,
	 * returns an empty list.
	 * @param studyPK
	 * @param userPK
	 * @param userAccountPK
	 * @param teamAuth
	 * @return
	 */
	private StudyOrganizations marshalStudyOrganizations(
			int studyPK, 
			int userPK, 
			int userAccountPK,
			TeamAuthModule teamAuth){

		if (logger.isDebugEnabled()) logger.debug("marshaling study organizations");

		Integer studyOrgPrivileges = teamAuth.getStudyTeamPrivileges();

		if (!TeamAuthModule.hasViewPermission(studyOrgPrivileges)){
			if (logger.isDebugEnabled()) logger.debug("user does not have permission to view study organizations");
			return new StudyOrganizations();
		}
		if (logger.isDebugEnabled()) logger.debug("user has permission to view study organizations");

		List<StudyOrganization> studyOrgs = 
			StudyOrganizationDAO.getStudySiteOrganziations(studyPK, userPK, userAccountPK);
		
		StudyOrganizations studyOrg = new StudyOrganizations(); 
		studyOrg.setStudyOrganizaton(studyOrgs); 
		//Virendra:Fixed #6125
//		for(StudyOrganization studyOrg:studyOrgs){
			StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
			StudyIdentifier studyId = marshallStudyIdentifier(studyBean);
			studyOrg.setParentIdentifier(studyId);
//		}
		return studyOrg;
	}

	/**
	 * marshals the study summary into a StudySummary object.
	 * If the user does not have team privilage to view a study summary,
	 * return an empty StudySummary instance.
	 * @param studyBean
	 * @param teamAuth
	 * @return
	 * @throws AuthorizationException
	 */
	private StudySummary marshalStudySummary(
			StudyBean studyBean, 
			TeamAuthModule teamAuth)
	throws AuthorizationException, OperationException{
		StudySummary returnSummary = new StudySummary();
		if (logger.isDebugEnabled()) logger.debug("marshaling study summary");

		Integer studySummaryAuth = teamAuth.getStudySummaryPrivileges();

		if (!TeamAuthModule.hasViewPermission(studySummaryAuth)){
			//DRM for 5540
			addIssue(
					new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
							"User does not have view permission to view Study Summary"));
			throw new AuthorizationException("User does not have view permission to view Study Summary");
		}
		if (logger.isDebugEnabled()) logger.debug("user has permission to view study organizasummarytions");

		//virendra;Fixed#6121, setting parentIdentifier for study summary
		Integer parentStudyPK = studyBean.getId();
		ObjectMap objParentStudySummary = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, parentStudyPK);
		
		StudyIdentifier studyId = new StudyIdentifier();
		studyId.setOID(objParentStudySummary.getOID());
		studyId.setPK(parentStudyPK);
		studyId.setStudyNumber(studyBean.getStudyNumber());
		returnSummary.setParentIdentifier(studyId);
		
		
		returnSummary.setStudyTitle(studyBean.getStudyTitle());
		Integer callingUserAccountId = Integer.valueOf(callingUser.getUserAccountId());
		CodeCache codeCache = CodeCache.getInstance();
		Code theraputicArea = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_THERAPUTIC_AREA, 
					studyBean.getStudyTArea(),
					callingUserAccountId);
		returnSummary.setTherapeuticArea(theraputicArea);

		returnSummary.setStudyAuthor(
				getUserIdFromPKStr(studyBean.getStudyAuthor(), objectMapService, userAgent));

		returnSummary.setPrincipalInvestigator(
				getUserIdFromPKStr(studyBean.getStudyPrimInv(), objectMapService, userAgent));

		returnSummary.setStudyContact(
				getUserIdFromPKStr(studyBean.getStudyCoordinator(), objectMapService, userAgent));

		returnSummary.setStudyNumber(studyBean.getStudyNumber());
		returnSummary.setStudyTitle(studyBean.getStudyTitle());
		returnSummary.setObjective(studyBean.getStudyObjective());
		returnSummary.setAgentDevice(studyBean.getStudyProduct());

		Code division = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_DIVISION,
					studyBean.getStudyDivision(),
					callingUserAccountId);

		returnSummary.setDivision(division);

		if (studyBean.getDisSite() != null && studyBean.getDisSite().length() > 0){
			String[] diseaseSites = studyBean.getDisSite().split(",");
			List<Code> diseaseSiteCodes = 
				codeCache.getCodeSubTypeByMultiPKs(
						CodeCache.CODE_TYPE_DISEASE_SITE, 
						diseaseSites,
						callingUserAccountId);
			returnSummary.getDiseaseSites().addAll(diseaseSiteCodes);
		}



		returnSummary.setSpecificSite1(studyBean.getStudyICDCode1());
		returnSummary.setSpecificSite2(studyBean.getStudyICDCode2());
		returnSummary.setSpecificSite3(studyBean.getStudyICDCode3());
		
		//added null check for 5782
		if (studyBean.getStudyNSamplSize() != null && 
				studyBean.getStudyNSamplSize().length() > 0){
			returnSummary.setNationalSampleSize(
					Integer.valueOf(studyBean.getStudyNSamplSize()));
		}
		
		String studyDurationUnitStr = studyBean.getStudyDurationUnit();
		if (studyDurationUnitStr != null){
			Duration.TimeUnits studyDurationUnit = null;
			if (studyDurationUnitStr.equalsIgnoreCase("days")){
				studyDurationUnit = Duration.TimeUnits.DAY;
			}
			if (studyDurationUnitStr.equalsIgnoreCase("weeks")){
				studyDurationUnit = Duration.TimeUnits.WEEK;
			}
			if (studyDurationUnitStr.equalsIgnoreCase("months")){
				studyDurationUnit = Duration.TimeUnits.MONTH;
			}
			if (studyDurationUnitStr.equalsIgnoreCase("years")){
				studyDurationUnit = Duration.TimeUnits.YEAR;
			}

			Duration studyDuration = 
				new Duration(new Integer(studyBean.getStudyDuration()),
						studyDurationUnit);
			returnSummary.setStudyDuration(studyDuration);
		}
		returnSummary.setEstimatedBeginDate(studyBean.getStudyEstBeginDt());

		Code phaseCd = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_PHASE, 
					studyBean.getStudyPhase(),
					callingUserAccountId);
		returnSummary.setPhase(phaseCd);

		Code researchCd = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_RESEARCH_TYPE, 
					studyBean.getStudyResType(),
					callingUserAccountId);
		returnSummary.setResearchType(researchCd);

		Code studyScopeCd = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_STUDY_SCOPE, 
					studyBean.getStudyScope(),
					callingUserAccountId);

		returnSummary.setStudyScope(studyScopeCd);

		Code studyCd = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_STUDY_TYPE, 
					studyBean.getStudyType(),
					callingUserAccountId);

		returnSummary.setStudyType(studyCd);
		returnSummary.setSummaryText(studyBean.getStudySummary());
		//returnStudy.setStudyLinkedTo(studyBean.getStudyNumber());

		//KM-#5383
		String associatedStudyPK = studyBean.getStudyAssoc();
		//if the study has an associated study, marshal the associated study
		//and pass back the StudyIdentifier for it.
		if (associatedStudyPK != null){
			//get the associated study's study number to put into a StudyIdentifier object
			StudyBean assocStudyBean =
				studyAgent.getStudyDetails(EJBUtil.stringToNum(associatedStudyPK));
			StudyIdentifier assocStudyId = new StudyIdentifier(assocStudyBean.getStudyNumber());
			returnSummary.setStudyLinkedTo(assocStudyId);
		}

		Code blindingCd = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_BLINDING, 
					studyBean.getStudyBlinding(),
					callingUserAccountId);
		returnSummary.setBlinding(blindingCd);

		Code randomizationCd = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_RANDOMIZATION, 
					studyBean.getStudyRandom(),
					callingUserAccountId);
		returnSummary.setRandomization(randomizationCd);

		Code sponsorNameCd = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_SPONSOR_NAME, 
					studyBean.getStudySponsorName(),
					callingUserAccountId);
		returnSummary.setSponsorName(sponsorNameCd);

		returnSummary.setSponsorNameOther(studyBean.getStudySponsorIdInfo());
		//Virendra:Fixed Bug No:5413
		returnSummary.setPrincipalInvestigatorOther(studyBean.getStudyOtherPrinv());
		//KM-#5384
		returnSummary.setSponsorID(studyBean.getStudySponsorIdInfo() );
		returnSummary.setSponsorContact(studyBean.getStudyContact());
		returnSummary.setSponsorNameOther(studyBean.getStudySponsor());
		returnSummary.setSponsorOtherInfo(studyBean.getStudyInfo());
		returnSummary.setKeywords(studyBean.getStudyKeywords());
		if(studyBean.getMajAuthor() != null){
			returnSummary.setPIMajorAuthor(
					studyBean.getMajAuthor().equalsIgnoreCase("Y") ? 
							true : false);
		}
		returnSummary.setStudyIndIdeNum(studyBean.getStudyIndIdeNum());
		
		//virendra:for #5246 start
		boolean isInvestigatorHeldIndIde = false;
		if(studyBean.getStudyInvIndIdeFlag() != null){
			isInvestigatorHeldIndIde = studyBean.getStudyInvIndIdeFlag().equals("1") ? true : false;
		}
		returnSummary.setIsInvestigatorHeldIndIde(isInvestigatorHeldIndIde);
		//virendra:for #5246 end
		
		// Begin More Study Details
		//get study ids

		
		
		Collection<NVPair> moreStudyDetailsCollection =
			MoreStudyDetailsDAO.getStudyMoreStudyDetails(
					studyBean.getId(),
					EJBUtil.stringToInteger(callingUser.getUserGrpDefault())).values();
		
		if (moreStudyDetailsCollection != null){
			List<NVPair> moreStudyDetailsList = new ArrayList<NVPair>();
			
			moreStudyDetailsList.addAll(moreStudyDetailsCollection);

			returnSummary.setMoreStudyDetails(moreStudyDetailsList);

			//virendra- setting public flags for sections of study summary
			////Fixed Bug No. 5248
			returnSummary.setIsStdDefPublic(false);
			returnSummary.setIsStdDesignPublic(false);
			returnSummary.setIsStdDetailPublic(false);
			returnSummary.setIsStdSponsorInfoPublic(false);
			
			String strStdSummaryPublicFlagCombo = studyBean.getStudyPubCol();

			if(strStdSummaryPublicFlagCombo != null && strStdSummaryPublicFlagCombo.length() == 4){
				returnSummary.setIsStdDefPublic(strStdSummaryPublicFlagCombo.charAt(0)== '1' ? true : false);
				returnSummary.setIsStdDesignPublic(strStdSummaryPublicFlagCombo.charAt(1)== '1' ? true : false);
				returnSummary.setIsStdDetailPublic(strStdSummaryPublicFlagCombo.charAt(2)== '1' ? true : false);
				returnSummary.setIsStdSponsorInfoPublic(strStdSummaryPublicFlagCombo.charAt(3)== '1' ? true : false);
			
			
			}
			
		}
		
		return returnSummary;
	}

	


	private TeamBean locateStudyTeamMember(Integer studyPK, UserIdentifier userIdentifier)
	throws OperationException{
		UserBean userBean =  null;
		try {
			userBean = ObjectLocator.userBeanFromIdentifier(
					callingUser, 
					userIdentifier, 
					userAgent, 
					sessionContext, objectMapService);
		} catch (MultipleObjectsFoundException e) {
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple user for team members " + userIdentifier.toString()));
			throw new OperationException();
		}
		
		//fix for 5634 - #SM-F-1- 'createStudy' method creates a study, with studyAuthor 
		//if a user is not found, added a check to avoid a nullpointer
		if (userBean == null){
			if (logger.isDebugEnabled()) logger.debug("locateStudyTeamMember: User not found. " + userIdentifier);
			addIssue(
					new Issue(
							IssueTypes.USER_NOT_FOUND, 
							"StudyTeamMember User Not Found: " + userIdentifier));
			throw new OperationException();
		}
		Integer studyTeamPK = null;
		
		//switch to different call in teamAgent to populate DAO
		//Integer studyTeamPK = null;
		
		//call teamAgent.getTeamValues(...) which will return a DAO. Then,
		//within that DAO, find your primary key for the study team member based
		//on the userBean.getUserId(). 
		//TeamDao teamDAO = teamAgent.findUserInTeam(studyPK, userBean.getUserId());
		TeamDao teamDAO = 
			teamAgent.getTeamValuesBySite(
					studyPK, 
					EJBUtil.stringToNum(callingUser.getUserAccountId()),0);
		ArrayList teamPKs = teamDAO.getTeamIds();
		ArrayList userIds = teamDAO.getUserIds();
		int teamMemberIndex = userIds.indexOf(userBean.getUserId());
		
		Integer teamMemberPK = null;
		if (teamMemberIndex > -1){
			teamMemberPK = (Integer)teamPKs.get(teamMemberIndex);
			}
		else{
			addIssue(new Issue(IssueTypes.STUDY_ERROR_REMOVE_TEAM_MEMBER, "Could not find team member to remove " + userIdentifier.toString()));
				throw new OperationException();
			}

		if (teamMemberPK != null){
			return this.teamAgent.getTeamDetails(teamMemberPK);
		}
		return null;
	}
	
	private StudySiteBean locateStudyOrganizationPK(
			Integer studyPK, 
			OrganizationIdentifier organizationId)
	throws OperationException{
		Integer sitePK;
		try {
			sitePK = ObjectLocator.sitePKFromIdentifier(
					callingUser, 
					organizationId, 
					sessionContext, 
					objectMapService);
			//virendra: #5687
			if(sitePK == null){
				addIssue(new Issue(IssueTypes.STUDY_ORG_NOT_FOUND, "The Study Organization to update not found :::" + sitePK));
				throw new OperationException();
			}
			//virendra: #5687 end	
		} catch (MultipleObjectsFoundException e) {
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for " + organizationId.toString()));
			throw new OperationException();
		}
		
		return studySiteAgent.findBeanByStudySite(studyPK, sitePK);
	}
	
	private StudyStatusBean locateStudyStatus(
			Integer studyPK,
			Code studyStatusCode)
	throws OperationException{
		
		Integer studyStatusCodePK = 
			dereferenceCode(
					studyStatusCode, 
					CodeCache.CODE_TYPE_STATUS, 
					callingUser);
		
		return studyStatusAgent.findByAtLeastOneStatus(
				studyPK, 
				EJBUtil.integerToString(studyStatusCodePK));
	}
	
	private Integer locateStudyPK(StudyIdentifier studyIdentifier) 
	throws OperationException{
		//Virendra:#6123,added OID in if clause
		if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
				&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
				&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
		{
		addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
		throw new OperationException();
		}
		
		Integer studyPK =
				ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
		if (studyPK == null || studyPK == 0){
			StringBuffer errorMessage = new StringBuffer("Study not found for:"); 
			if(studyIdentifier.getOID() != null && studyIdentifier.getOID().length() > 0 )
			{
				errorMessage.append(" OID: " + studyIdentifier.getOID()); 
			}
			if(studyIdentifier.getStudyNumber() != null && studyIdentifier.getStudyNumber().length() > 0)
			{
				errorMessage.append( " Study Number: " + studyIdentifier.getStudyNumber()); 
			}
			addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, errorMessage.toString()));
			throw new OperationException();
		}
		return studyPK;
	}

	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}


	public ResponseHolder updateStudySummary(
			StudyIdentifier studyId,
			StudySummary summary,
			boolean createNonSystemUsers) 
	throws OperationException {

		try{
			//check for validation issues...will throw validation exception
			//and add issues to response.
			
			if(studyId==null || (StringUtil.isEmpty(studyId.getOID()) 
					&& (studyId.getPK()==null || studyId.getPK()<=0) 
					&& StringUtil.isEmpty(studyId.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			validate(summary);
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			
			//Instantiate a StudyBean object. We will populate this 
			//with information from our study then persist it.
			Integer studyPK = locateStudyPK(studyId);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyId.getOID() + " StudyNumber " + studyId.getStudyNumber()));
				throw new OperationException();
			}
			TeamAuthModule teamAuthModule = getTeamAuthModule(studyPK);
			StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
			
			updateStudySummary(
					studyPK, 
					summary, 
					studyBean, 
					createNonSystemUsers, 
					teamAuthModule);
			if (response.getIssues().getIssue().size() > 0){
				throw new OperationException();
			}
			//VIrendra:#Fixed 6238			
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			studyId.setOID(map.getOID());
			studyId.setPK(studyPK);
			studyId.setStudyNumber(summary.getStudyNumber());
		
			response.addAction(
					new CompletedAction(studyId, CRUDAction.UPDATE));
			
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudySummmary", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudySummmary", t);
			throw new OperationRolledBackException(response.getIssues());
		}
		return response;
	}

	

	
	private Integer createStudyOrganization(
			Integer studyPK,
			StudyOrganization studyOrganization,
			TeamAuthModule teamAuthModule,
			List<Integer> addedOrganizations)
	throws OperationException{
		
		StudySiteBean legacyBean = new StudySiteBean();
		
		if (!TeamAuthModule.hasNewPermission(teamAuthModule.getStudyTeamPrivileges())){
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have study team new privileges"));
			throw new AuthorizationException("User not authorized to add study team member");
		}
		
		Integer sitePK = null;
		try {
			sitePK = ObjectLocator.sitePKFromIdentifier(
					callingUser, 
					studyOrganization.getOrganizationIdentifier(), 
					sessionContext, 
					objectMapService);
		
		} catch (MultipleObjectsFoundException e) {
			addIssue(new Issue(IssueTypes.MULTIPLE_OBJECTS_FOUND, "Found multiple organizations for " + studyOrganization.getOrganizationIdentifier()));
			throw new OperationException();
		}
		
		if (sitePK == null || sitePK == 0){
			addIssue(new Issue(IssueTypes.STUDY_ORG_NOT_FOUND, "Could not find organization for: " + studyOrganization.getOrganizationIdentifier()));
			throw new OperationException();
		}
		
		//Virendra:Fixed Bug No.:5473
		StudySiteDao studySiteDao =studySiteAgent.getCntForSiteInStudy(studyPK, sitePK);//getAllStudyTeamSites
		if(studySiteDao.getCRows() >= 1){
			addIssue(
					new Issue(
							IssueTypes.STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION, 
							"Study Organization already exists in study: " + studyOrganization.getOrganizationIdentifier().getSiteName()));
			throw new OperationException();
		}
		
		if(addedOrganizations.contains(sitePK))
		{
			addIssue(
					new Issue(
							IssueTypes.STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION, 
							"Study Organization already exists in study: " + studyOrganization.getOrganizationIdentifier().getSiteName()));
			throw new OperationException();
		}
		
		addedOrganizations.add(sitePK); 

		studySiteIntoBean(
				studyOrganization, 
				legacyBean,
				studyPK,
				sitePK);
		
		Integer studySitePK = studySiteAgent.setStudySiteDetails(legacyBean);
		if (studySitePK == -2){
			Issue issue = new Issue(IssueTypes.STUDY_ERROR_CREATE_ORGANIZATION, 
					studyOrganization.getOrganizationIdentifier().toString());
			addIssue(issue);
			throw new OperationException();
		}
		return studySitePK;
	} 

	
	public ResponseHolder addStudyOrganization(
			StudyIdentifier studyIdentifier,
			StudyOrganization studyOrganization) throws OperationException {
		try{
            int studySitePK;//Bug Fix : 7978 : Tarandeep Singh Bali
			
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			validate(studyOrganization);
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User does not have group new manage protocol rights"));
				throw new AuthorizationException();
			}

			
			if (logger.isDebugEnabled()) logger.debug("Creating study team organization: " + studyOrganization.toString());
			Integer studyPK = locateStudyPK(studyIdentifier);
			
			TeamAuthModule teamAuth = getTeamAuthModule(studyPK);
						//KM-#5474
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			else{	
			studySitePK = 	createStudyOrganization(studyPK, studyOrganization, teamAuth, new ArrayList<Integer>());
			}
			if (response.getIssues().getIssue().size() > 0){
				throw new OperationException();
			}
			//Begin Bug Fix : 7978 : Tarandeep Singh Bali
			ObjectMap mapStudy = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			studyIdentifier.setOID(mapStudy.getOID());
			studyIdentifier.setPK(studyPK);
			StudyOrganizationIdentifier studyOrganizationIdentifier = new StudyOrganizationIdentifier();
			ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_ORGANIZATION_ID, studySitePK);
			studyOrganizationIdentifier.setOID(map.getOID());
			studyOrganizationIdentifier.setPK(studySitePK);
			response.addAction(new CompletedAction(studyIdentifier, CRUDAction.UPDATE));
			response.addObjectCreatedAction(studyOrganizationIdentifier);
			//End Bug Fix : 7978 : Tarandeep Singh Bali
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl addStudyOrganization", e);
			throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl addStudyOrganization", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}

	
	/**
	 * Adds a new study status to the list of study statuses
	 * 
	 * @param studyIdentifier Identifier for the study to add a status to
	 * @param createStudyStatus new study status to be added to the studies list of statuses 
	 */
	public ResponseHolder addStudyStatus(
			StudyIdentifier studyIdentifier,
			StudyStatus newStudyStatus) throws OperationException {
		try{
			
			
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			validate(newStudyStatus);
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			
			
			if (logger.isDebugEnabled()) logger.debug("Creating study status: " + newStudyStatus.toString());
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			//5642 and 5643
			TeamAuthModule teamAuthModule = getTeamAuthModule(studyPK);
		
			createStudyStatus(studyPK, newStudyStatus, teamAuthModule);
			//Begin Bug Fix : 7979 : Tarandeep Singh Bali
			ObjectMap mapStudy = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			studyIdentifier.setOID(mapStudy.getOID());
			studyIdentifier.setPK(studyPK);
			//End Bug Fix : 7979 : Tarandeep Singh Bali
			response.addAction(new CompletedAction(studyIdentifier, CRUDAction.UPDATE));
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl addStudyStatus", e);
			throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl addStudyStatus", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
		
		
	}

	private Integer createStudyTeamMember(
			Integer studyPK,
			StudyTeamMember studyTeamMember,
			TeamAuthModule teamAuth,
			boolean createNonSystemUsers, 
			List<Integer> addedTeamMembers) throws OperationException{
		
		//For 5443 - Dylan
		//Check the teamAuth module for add study team rights
		if (!TeamAuthModule.hasNewPermission(teamAuth.getStudyTeamPrivileges())){
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have study team new privilages"));
			throw new AuthorizationException("User not authorized to add study team member");
		}
		//end 5443 addition
		
		//For 5402 - Dylan
		//validate that the user does not appear twice AND add user's organization
		//if it's not there already.
		//first, get the user bean
		UserBean studyTeamUserB = null;
		try {
			studyTeamUserB = ObjectLocator.userBeanFromIdentifier(
					callingUser,
					studyTeamMember.getUserId(), 
					userAgent, 
					sessionContext, 
					objectMapService);
		} catch (MultipleObjectsFoundException e) {
			if (logger.isDebugEnabled()) logger.debug("Found multiple objects that matched study team by user");
			addIssue(
					new Issue(
							IssueTypes.MULTIPLE_OBJECTS_FOUND, 
							"Found multiple objects that matched study team by user"));
		}
		
        // At this time the new non-system user has not been committed to DB until transaction is complete.
        // So studyTeamUserB is still null; thus we need to use the bean in memory
		if (createNonSystemUsers && studyTeamUserB == null) {
		    studyTeamUserB = getFromHashOrCreateNonSystemUser(userAgent, objectMapService, 
                    studyTeamMember.getUserId(), callingUser);
		}
		
		//fix for 5634 - #SM-F-1- 'createStudy' method creates a study, with studyAuthor 
		//if a user is not found, added a check to avoid a nullpointer
		if (studyTeamUserB == null){
			if (logger.isDebugEnabled()) logger.debug("addStudyTeamMember: User not found. " + studyTeamMember.getUserId());
			addIssue(
					new Issue(
							IssueTypes.USER_NOT_FOUND, 
							"StudyTeamMember User Not Found: " + studyTeamMember.getUserId()));
			throw new OperationException();
		}
		
		if(addedTeamMembers.contains(studyTeamUserB.getUserId()))
		{
			addIssue(
					new Issue(
							IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, 
							"User already exists in study team: " + studyTeamMember.getUserId()));
			throw new OperationException();
		}
		
		addedTeamMembers.add(studyTeamUserB.getUserId()); 
		
		//Now check with the DAO that the user is already there
		//KM-#5531
		TeamDao teamDao = teamAgent.findUserInTeam(studyPK, studyTeamUserB.getUserId());
		if (teamDao.getCRows() > 0){
			addIssue(
					new Issue(
							IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, 
							"User already exists in study team: " + studyTeamMember.getUserId()));
			throw new OperationException();
		}
		if (logger.isDebugEnabled()) logger.debug("User is not already in study site");
		
		//Now, add the user's organization if it's not already in the study
		//first, try and find the study site bean for this user's side and the study
		//virendra: #5620
		//if user is hidden do not add it to the study
		if(studyTeamUserB.getUserHidden().equals("1")){
			StringBuffer errorObject = new StringBuffer(); 
			if(studyTeamMember.getUserId().getOID() != null) errorObject.append(" OID::" + studyTeamMember.getUserId().getOID()); 
			if(studyTeamMember.getUserId().getFirstName() != null) errorObject.append(" FirstName::" + studyTeamMember.getUserId().getFirstName());
			if(studyTeamMember.getUserId().getLastName() != null) errorObject.append(" LastName::"+studyTeamMember.getUserId().getLastName()); 
			if(studyTeamMember.getUserId().getUserLoginName() != null) errorObject.append(" UserLoginName::"+studyTeamMember.getUserId().getUserLoginName()); 
			addIssue( 
					new Issue(
							IssueTypes.USER_HIDDEN, 
							"User is hidden, Cannot be added to Study, USER_HIDDEN= " + studyTeamUserB.getUserHidden() + errorObject.toString() ));
			throw new OperationException();
		}
		//virendra: #5620 end
		
		Integer userSitePK = EJBUtil.stringToInteger(studyTeamUserB.getUserSiteId());
		StudySiteBean studySiteBean = studySiteAgent.findBeanByStudySite(
				studyPK, 
				userSitePK);
		if (studySiteBean == null){
			logger.info("User organization not found, adding " + studyPK + " ," + studyTeamUserB.getUserSiteId());
			studySiteBean = new StudySiteBean();
			studySiteBean.setStudyId(EJBUtil.integerToString(studyPK));
			studySiteBean.setCreator(getCreatorString());
			studySiteBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
			studySiteBean.setSiteId(EJBUtil.integerToString(userSitePK));
			studySiteBean.setRepInclude("1");
			int studySitePK = studySiteAgent.setStudySiteDetails(studySiteBean);
			if (studySitePK == -2){
				logger.info("Error adding organization " + studyPK + " ," + studyTeamUserB.getUserSiteId());
				addIssue(
						new Issue(
								IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, 
								"Study team member auto-creation of oganization failed " + studyTeamMember.getUserId()));
				throw new OperationException();
			}
		}
		
		TeamBean legacyBean = new TeamBean();
		
		studyTeamMemberIntoBean(
				studyTeamMember, 
				legacyBean,
				studyPK,
				createNonSystemUsers);
		
		Integer studySitePK = teamAgent.setTeamDetails(legacyBean);
		
		//Now that we have a new study team member, we need to add a 
		//row to er_status_history to reflect new status
		String activeStatusCode = null;
		try {
			activeStatusCode = dereferenceCodeStr(
					studyTeamMember.getStatus(), 
					CodeCache.CODE_TYPE_TEAM_STATUS, 
					callingUser);
		} catch (CodeNotFoundException e) {
			addIssue(
					new Issue(
						IssueTypes.DATA_VALIDATION, 
						"Team Status Code Not Found: " + studyTeamMember.getStatus()));
			throw new OperationException();
		}
		if (studySitePK == 0){
			Issue issue = new Issue(
							IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, 
							studyTeamMember.getUserId().toString());
			addIssue(issue);
			throw new OperationException();
		}
	
		
		//DRM 5442 - We need to call stdSiteRightsAgentRObj.createStudySiteRightsData
		//to add the user's site rights
		int ret = studySiteRightsAgent.
			createStudySiteRightsData(
					 studyTeamUserB.getUserId(), 
					 EJBUtil.stringToInteger(callingUser.getUserAccountId()), 
					 studyPK, 
					 callingUser.getUserId(), 
					 IP_ADDRESS_FIELD_VALUE);
		//for this call, -1 is failure
		if (ret == -1){
			logger.error("error adding studysiterights data for while creating study team member: " + studyTeamMember);
		}
		
		//end 5442
		
		StatusHistoryBean statusHistoryBean = new StatusHistoryBean();
		statusHistoryBean.setCreator(getCreatorString());
		statusHistoryBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		statusHistoryBean.setIsCurrentStat("1");
		statusHistoryBean.setRecordType("N"); //????
		statusHistoryBean.setStatusCodelstId(activeStatusCode);
		statusHistoryBean.setStatusModuleTable(STUDY_TEAM_MODULE_NAME);
		statusHistoryBean.setStatusModuleId(EJBUtil.integerToString(studySitePK));
		statusHistoryBean.setStatusStartDate(new Date());
		int statusHistoryPK = statusHistoryAgent.setStatusHistoryDetails(statusHistoryBean);
		if (statusHistoryPK == 0){
			Issue issue = new Issue(
					IssueTypes.STUDY_ERROR_CREATE_TEAM_MEMBER, 
					"Error setting team memeber status" + 
					studyTeamMember.getUserId().toString());
			addIssue(issue);
			throw new OperationException();

		}

		return studySitePK;
	}
	
	public ResponseHolder addStudyTeamMember(
			StudyIdentifier studyIdentifier,
			StudyTeamMember studyTeamMember,
			boolean createNonSystemUsers) throws OperationException {
		try{
 			if (logger.isDebugEnabled()) logger.debug("Creating study team member: " + studyTeamMember.toString());
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			validate(studyTeamMember);
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			TeamAuthModule teamAuthModule = getTeamAuthModule(studyPK);
			
			createStudyTeamMember(studyPK, studyTeamMember, teamAuthModule, createNonSystemUsers, new ArrayList<Integer>()); 
			response.addAction(new CompletedAction(studyTeamMember.getUserId(), CRUDAction.CREATE));
			
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl addStudyOrganization", e);
			throw new OperationRolledBackException(response.getIssues());
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl addStudyOrganization", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}

public ResponseHolder removeStudyOrganization(
			StudyIdentifier studyIdentifier,
			OrganizationIdentifier organizationId) 
	throws OperationException {
		try{
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			if (logger.isDebugEnabled()) logger.debug("Removing study organization: ");
			
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			//DRM - 5491 - Add study team permissions check
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			if (!TeamAuthModule.hasEditPermission(teamAuthModule.getStudyTeamPrivileges())){
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have study team edit privilages"));
				throw new AuthorizationException("User not authorized to remove study team member");
			}
			//end 5491

			//KM-5488
			StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
			String StudyAuthor = studyBean.getStudyAuthor();
			UserBean userBean = userAgent.getUserDetails(EJBUtil.stringToNum(StudyAuthor));
			String authorSiteId = userBean.getUserSiteId();
			Integer selOrgId = ObjectLocator.sitePKFromIdentifier(callingUser, organizationId, sessionContext, objectMapService);
			
			if(selOrgId == null)
			{
				Issue issue = new Issue(IssueTypes.ORGANIZATION_NOT_FOUND, 
				"Organization not found");
				addIssue(issue);
				throw new OperationException();
			}
			
			if (EJBUtil.stringToInteger(authorSiteId).equals(selOrgId)){
				Issue issue = new Issue(IssueTypes.STUDY_REMOVE_DATA_MANAGER, 
				"Data Manager can not be removed from Study Team.");
				addIssue(issue);
				throw new OperationException();
			}
			
			//KM-5487(Should not delete organization that has current study status)
			
			StudySiteDao studySiteDao = studySiteAgent.getStudySiteTeamValues(studyPK.intValue(), selOrgId, callingUser.getUserId(), EJBUtil.stringToNum(callingUser.getUserAccountId()));
			ArrayList siteIds = studySiteDao.getSiteIds();
			ArrayList currStats = studySiteDao.getCurrStats();

			int currStat = 0;
			int siteId = 0;
			for(int i=0; i<siteIds.size(); i++) {
				siteId = (Integer)siteIds.get(i);
				if (siteId == selOrgId) {
					 currStat = (Integer) currStats.get(i);
				}
			}
			
			if (currStat == 1){
				Issue issue = new Issue(IssueTypes.STUDY_ERROR_REMOVE_ORGANIZATION_CURRENT_STUDYSTATUS, 
						"You cannot delete the organization that has current study status");
				addIssue(issue);
				throw new OperationException();
			}
			
			//KM-5486(Should not remove organization if it is linked with Active/Enrolling status and Patient)
			int enrPatCount  = 0;
			int lenStudy =0;
			String subType = "";
			int activeCount = 0;

			StudyStatusDao studyStatDao = 
				studyStatusAgent.getStudyStatusDesc(
						studyPK.intValue(), 
						selOrgId, 
						callingUser.getUserId(), 
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
			
			ArrayList subTypeStatusLst = studyStatDao.getSubTypeStudyStats();
			ArrayList studyStatusIdLst = studyStatDao.getIds();
			ArrayList studyStatusDescs = studyStatDao.getDescStudyStats();
			lenStudy = studyStatusIdLst.size();
			enrPatCount = studyStatDao.getEnrPatCnt();
			String activeStatusDesc = null;
			for(int tmpCnt = 0;tmpCnt < lenStudy; tmpCnt++)
			{
				subType =(((subTypeStatusLst.get(tmpCnt)) == null)?"":(subTypeStatusLst.get(tmpCnt)).toString()).trim();

				if (subType.equals(ACTIVE_STUDY_STATUS_CODE)){
						activeCount++;
						activeStatusDesc = (String)studyStatusDescs.get(tmpCnt);
						break;
				}

			} 
			
			if (activeCount >= 1 && enrPatCount > 0 )
			{
				Issue issue = new Issue(IssueTypes.STUDY_ERROR_REMOVE_ORGANIZATION_ENROLLED_PATIENT, 
				"A Patient is currently enrolled to the Study and '"+activeStatusDesc+"' status is linked to this Organization");
				addIssue(issue);
				throw new OperationException();
			}
					
			StudySiteBean legacyBean =
				locateStudyOrganizationPK(
						studyPK, 
						organizationId);
			
            //virendra - 5521
            if(legacyBean == null){
                    Issue issue = new Issue(
                                    IssueTypes.STUDY_ORG_NOT_FOUND,
                                    "Study Org to delete not found: " +
                                    legacyBean);
                    addIssue(issue);
                    throw new OperationException();
            }
            //virendra end

            //KM-#5489
            Integer returnCode = studySiteAgent.deleteOrgFromStudyTeam(
            		studyPK, 
            		EJBUtil.stringToNum(callingUser.getUserAccountId()), 
            		selOrgId,
            		callingUser.getUserId());
            
			if (returnCode == -1){
				Issue issue = new Issue(IssueTypes.STUDY_ERROR_REMOVE_ORGANIZATION, 
						"Error removing study site, pk=" + studyPK);
				addIssue(issue);
				throw new OperationException();
			}
			else{
				response.addAction(new CompletedAction(organizationId, CRUDAction.REMOVE));
			}
			if (response.getIssues().getIssue().size() > 0){
				throw new OperationException();
			}
		} 
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyOrganization", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyOrganization", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}

	public ResponseHolder removeStudyTeamMember(
			StudyIdentifier studyIdentifier,
			UserIdentifier teamUserId) 
	throws OperationException {
		try{
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			if (logger.isDebugEnabled()) logger.debug("Removing study team member: ");
			
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			
			
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			//DRM - 5446 - Add study team permissions check
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			if (!TeamAuthModule.hasEditPermission(teamAuthModule.getStudyTeamPrivileges())){
				addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User does not have study team edit privilages"));
				throw new AuthorizationException("User not authorized to remove study team member");
			}
			//end 5446
			
			TeamBean legacyBean= locateStudyTeamMember(studyPK, teamUserId);
			//KM-#5494- User linked to DataManager should not be deleted.
			StudyBean studyBean = studyAgent.getStudyDetails(studyPK);
			String StudyAuthor = studyBean.getStudyAuthor();
			
			if((legacyBean.getTeamUser()).equals(StudyAuthor)) {
				Issue issue = new Issue(IssueTypes.STUDY_REMOVE_DATA_MANAGER, 
				"Data Manager can not be removed from Study Team.");
				addIssue(issue);
				throw new OperationException();
			}
		
			//oddly, the deleteFromTeam call requires a full bean, but only uses
			//the id.
			Integer returnCode = teamAgent.deleteFromTeam(legacyBean);
			if (returnCode == -2){ //-2 means error for this call
				Issue issue = new Issue(IssueTypes.STUDY_ERROR_REMOVE_ORGANIZATION, 
						"Error removing study team member, pk=" + studyPK);
				addIssue(issue);
				throw new OperationException();
			}
			
			//As part of 5447, we found that we need to remove the user from 
			//need to remove study site rights
			int rightsResponse = 
				studySiteRightsAgent.deleteStdSiteRights(
						studyPK, 
						EJBUtil.stringToInteger(legacyBean.getTeamUser()));
			
			if (rightsResponse < 0){
				Issue issue = new Issue(IssueTypes.STUDY_ERROR_REMOVE_ORGANIZATION, 
						"Error removing study site rights for user " + EJBUtil.stringToInteger(legacyBean.getTeamUser()));
				addIssue(issue);
				throw new OperationException();
			}
			
			if (response.getIssues().getIssue().size() > 0){
				throw new OperationException();
			}
			
			//DRM 12/14/10 5626
			response.addAction(
					new CompletedAction(teamUserId, CRUDAction.REMOVE));
		} 
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyTeamMember", e);
			throw new OperationRolledBackException(response.getIssues()); 
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl removeStudyTeamMember", t);
			throw new OperationRolledBackException(response.getIssues());
		}
		return response;
	}

	public ResponseHolder updateStudyOrganization(
			StudyIdentifier studyIdentifier,
			StudyOrganization studyOrganization) throws OperationException {
		try{
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			if (logger.isDebugEnabled()) logger.debug("Updating study organization: " + studyOrganization.toString());
			validate(studyOrganization);
			
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			TeamAuthModule teamAuthModule = getTeamAuthModule(studyPK);

			updateStudyOrganization(studyPK, studyOrganization, teamAuthModule);
			if (response.getIssues().getIssue().size() > 0){
				throw new OperationException();
			}
			response.addAction(
					new CompletedAction(studyOrganization.getOrganizationIdentifier(), CRUDAction.UPDATE));
		} 
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudyOrganization", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudyOrganization", t);
			throw new OperationRolledBackException(response.getIssues());
		}
		return response;
	}

//	public ResponseHolder updateStudyStatus(
//			StudyIdentifier studyId,
//			StudyStatus studyStatus) throws OperationException {
//		try{
//			if (logger.isDebugEnabled()) logger.debug("Updating study status: ");
//			validate(studyStatus);
//			
//			//Get the user's permission for managing protocols
//			GroupAuthModule groupAuth = 
//				new GroupAuthModule(callingUser, groupRightsAgent);
//
//			Integer manageProtocolPriv = 
//				groupAuth.getAppManageProtocolsPrivileges();
//
//			boolean hasNewManageProt = 
//				GroupAuthModule.hasEditPermission(manageProtocolPriv);
//			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
//			if (!hasNewManageProt){
//				throw new AuthorizationException("User Not Authorized to edit studies");
//			}
//			Integer studyPK = locateStudyPK(studyId);
//			TeamAuthModule teamAuthModule = getTeamAuthModule(studyPK);
//			updateStudyStatus(studyPK, studyStatus, teamAuthModule);
//	
//		} 
//		catch(OperationException e){
//			sessionContext.setRollbackOnly();
//			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudyStatus", e);
//			throw new OperationRolledBackException(response.getIssues());
//	
//		}
//		catch(Throwable t){
//			sessionContext.setRollbackOnly();
//			addUnknownThrowableIssue(t);
//			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudyStatus", t);
//			throw new OperationRolledBackException(response.getIssues());
//		}
//		return response;
//	}

	public ResponseHolder updateStudyTeamMember(
			StudyIdentifier studyIdentifier,
			StudyTeamMember studyTeamMember) 
	throws OperationException {
		try{
			if (logger.isDebugEnabled()) logger.debug("Updating study organization: " + studyTeamMember);
			if(studyIdentifier==null || (StringUtil.isEmpty(studyIdentifier.getOID()) 
					&& (studyIdentifier.getPK()==null || studyIdentifier.getPK()<=0) 
					&& StringUtil.isEmpty(studyIdentifier.getStudyNumber())))
			{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "valid studyIdentifier is required to addStudyPatientStatus"));
			throw new OperationException();
			}
			validate(studyTeamMember);
			
			//Get the user's permission for managing protocols
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);

			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();

			boolean hasNewManageProt = 
				GroupAuthModule.hasEditPermission(manageProtocolPriv);
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasNewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to edit studies"));
				throw new AuthorizationException("User Not Authorized to edit studies");
			}
			Integer studyPK = locateStudyPK(studyIdentifier);
			if(studyPK == null || studyPK == 0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for StudyIdentifier OID:" + studyIdentifier.getOID() + " StudyNumber " + studyIdentifier.getStudyNumber()));
				throw new OperationException();
			}
			TeamAuthModule teamAuthModule = getTeamAuthModule(studyPK);
			updateStudyTeamMember(studyPK, studyTeamMember, teamAuthModule);
			if (response.getIssues().getIssue().size() > 0){
				throw new OperationException();
			}
			
			response.addAction(
					new CompletedAction(studyTeamMember.getUserId(), CRUDAction.UPDATE));
		} 
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudyTeamMember", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl updateStudyTeamMember", t);
			throw new OperationRolledBackException(response.getIssues());
		}
		return response;
	}

	/**
	 * Returns a TeamAuthModule constructed for the
	 * calling user and study.
	 * 
	 * Use this method to get a TeamAuthModule as we may
	 * refactor this some day to get cache instances for module.
	 * @return
	 */
	protected TeamAuthModule getTeamAuthModule(Integer studyPK){
		return new TeamAuthModule(callingUser.getUserId(), studyPK);
	}
	
	//virendra:5400
	private void createStudyStatuses(
			Integer studyPK,
			StudyStatuses studyStatuses,
			TeamAuthModule teamAuthModule)
			throws 
			OperationException{
		
		//capture whether an exception occured creating one
		//of the items from the list. This lets us report exceptions
		//for all items, rather than just one.
		boolean exceptionOccured = false;
		
		for (StudyStatus studyStatus : studyStatuses.getStudyStatus()){
			try{//5642 and 5643
				createStudyStatus(
						studyPK, 
						studyStatus, 
						teamAuthModule);
			}
			catch(OperationException e){
				if (logger.isDebugEnabled()) logger.debug(e);
				addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE, e.getMessage()));
				exceptionOccured = true;
				continue;
			}
		}
		
		if (exceptionOccured){
			//if at least one exception occured, throw so that 
			//the calling method can roll back.
			throw new OperationException();
		}

		 
	}
	
	//virendra: #5686
	/**
	 * @return 
	 *  
	 */
	private void persistsStudySummaryMoreStudyDetails(
			StudySummary studySummary, 
			Integer studyPK, 
			UserBean callingUser) 
	throws OperationException{
		
		//12/08/2010 DRM - major changes below for 
		//Bug 5430
		//We now use a different method to get the list of more study details for this study,
		//and added the ability to remove un-sent MSD.
		//virendra: 5686
		List<NVPair> moreStudyDetails = 
			studySummary.getMoreStudyDetails();
		
		StudyIdDao studyIdDao = new StudyIdDao();
		studyIdDao.getStudyIds(
				studyPK, 
				callingUser.getUserGrpDefault());
		
		//find existing rows so we can calculate removes.
		//this list will get paired down as items are modified.
		//then, anything still on the list will be deleted.
		HashSet<Integer> existingMSDPKList = new HashSet<Integer>();
		for (int x=0; x< studyIdDao.getId().size(); x++){
			if ("M".equalsIgnoreCase((String)studyIdDao.getRecordType().get(x))){
				existingMSDPKList.add((Integer)studyIdDao.getId().get(x));
			}
		}
		
		
		if (moreStudyDetails != null){
			CodeCache codes = CodeCache.getInstance();
			//loop through each MSD in the provided MSD List, looking
			//too see if an action needs to be taken (add, update)
			for (NVPair nvPair : moreStudyDetails ){
				boolean isModify = false;
				StudyIdBean studyIdBean = null;
				//in nvPair.getType(), we have a code that maps
				//to er_codelist. But the api for studyidbean wants
				//the er_codelst.pk_codelst, so we need to derefence it.
				Code tempCode = new Code(CodeCache.CODE_TYPE_STUDY_ID_TYPE, nvPair.getKey());
				Integer codeListPK = null;
				try{
					codeListPK = codes.dereferenceCode(
						tempCode, 
						CodeCache.CODE_TYPE_STUDY_ID_TYPE, 
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
				}
				catch(CodeNotFoundException e){
					logger.error("Could not find code: " + nvPair.getKey() , e);
					addIssue(new Issue(IssueTypes.CODE_NOT_FOUND, "Code not found for more study details: " + nvPair.getKey()));
					throw new OperationException();
				}

				for (int x=0; x< studyIdDao.getId().size(); x++){
					Integer msdPK = (Integer)studyIdDao.getId().get(x);
					Integer msdCodePK = (Integer)studyIdDao.getStudyIdType().get(x);

					
					if (msdCodePK.equals(codeListPK)){
						//if msdPK is is null, we know we have a new entry
						if (msdPK.equals(0)){
							studyIdBean = new StudyIdBean();
							isModify = false;
							break;
						}
						//get the legacy bean so we can update it
						studyIdBean =
							studyIdAgent.getStudyIdDetails(msdPK);
						isModify = true;
						//remove from the existing list...anything not removed
						//will be assumed to need deletion.
						existingMSDPKList.remove(msdPK);
						break;
					}
				
				}
				
				//now set the details
				studyIdBean.setAlternateStudyId((String)nvPair.getValue());
				studyIdBean.setStudyIdType(EJBUtil.integerToString(codeListPK));
				studyIdBean.setCreator(getCreatorString());//creator should be set by a trigger
				studyIdBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
				studyIdBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
				studyIdBean.setStudyId(EJBUtil.integerToString(studyPK));
				if (isModify){
					int returnValue = studyIdAgent.updateStudyId(studyIdBean);
					if (returnValue == -2){
						addIssue(new Issue(IssueTypes.STUDY_ERROR_CREATE_MSD));
						throw new OperationException();
					}
				} 
				else{
					int returnValue = studyIdAgent.setStudyIdDetails(studyIdBean);
					if (returnValue == -2){
						addIssue(new Issue(IssueTypes.STUDY_ERROR_CREATE_MSD));
						throw new OperationException();
					}
					
				}
			}
		
		}
		//remove all MSD entries that existed going into this 
		//method but weren't sent in by the call. 
		for (Integer existingMSDPK : existingMSDPKList){
			studyIdAgent.removeStudyId(existingMSDPK);
		}
	}
	
	/**	virendra for get study status
	 * 
	 * @param StudyStatusIdentifier
	 * @return StudyStatus
	 * @throws OperationException
	 */
	public StudyStatus getStudyStatus(StudyStatusIdentifier studyStatusIdentifier) throws OperationException {
		//CodeCache codes = CodeCache.getInstance(); 
		//Team Auth
		try {
//			callingUser = 
//				getLoggedInUser(
//						sessionContext,
//						userAgent);
		
		if((studyStatusIdentifier==null) || ((StringUtil.isEmpty(studyStatusIdentifier.getOID())) && (studyStatusIdentifier.getPK()==null || studyStatusIdentifier.getPK()==0)))
		{
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Valid PatientStudyStatusIdentifier with OID or PK is required.")); 
			throw new OperationException();
		}
		GroupAuthModule groupAuth = 
		new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();

		boolean hasViewManageProt = 
			GroupAuthModule.hasViewPermission(manageProtocolPriv);
		
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasViewManageProt){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view study status"));
			throw new AuthorizationException("User Not Authorized to view study status");
		}
		ObjectMap objectMap =null;
		Integer studyStatusPk=0;
		if(!StringUtil.isEmpty(studyStatusIdentifier.getOID()))
		{
			objectMap = objectMapService.getObjectMapFromId(studyStatusIdentifier.getOID());
			if(objectMap == null || !(objectMap.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATSTUDYSTAT))|| (!(objectMap.getTableName().equals(ObjectMapService.PERSISTENCE_UNIT_STUDY_STATUS))))
			{
				addIssue(new Issue(IssueTypes.STUDY_PATIENT_STATUS_NOT_FOUND, "Please send valid PatientStudyStatusIdentifier"));
				throw new OperationException();
			}
			studyStatusPk=objectMap.getTablePK();
		}
		
		if(studyStatusIdentifier.getPK()!=null && studyStatusIdentifier.getPK()!=0)
		{
			studyStatusPk = studyStatusIdentifier.getPK();
		}
		if (studyStatusPk == null 
				|| studyStatusPk == 0 ){
			addIssue(new Issue(IssueTypes.STUDY_STATUS_NOT_FOUND, "Study status not found"));
			throw new OperationException("Study status not found");
		}
		StudyStatusBean studyStatusBean  = studyStatusAgent.getStudyStatusDetails(studyStatusPk);
		if (studyStatusBean == null){
			addIssue(new Issue(IssueTypes.STUDY_STATUS_NOT_FOUND, "Study status bean not found"));
			throw new OperationException("Study status not found");
		}
		//Kanwal - fix for Bug 6149
		StudyBean sb = studyAgent.getStudyDetails(EJBUtil.stringToInteger(studyStatusBean.getStatStudy()));
		if(sb == null || (! sb.getAccount().equals(callingUser.getUserAccountId())))
		{
			Issue issue = new Issue(IssueTypes.STUDY_STATUS_NOT_FOUND, "Study status not found for OID "+ studyStatusIdentifier.getOID() );
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(); 
		}//Bug fix 6149 ends
		return marshalStudyStatus( studyStatusBean);
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyStatusServiceImpl get", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyStatusServiceImpl get", t);
			throw new OperationException(t);
		}
	}
	
	/**
	 * 
	 * @param studyStatusBean
	 * @return StudyStatus
	 * @throws OperationException
	 */
	private StudyStatus marshalStudyStatus(
			StudyStatusBean studyStatusBean) throws OperationException{
		
		Integer studyPk = EJBUtil.stringToInteger(studyStatusBean.getStatStudy());
		TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPk);
		//Virendra: #6199
		int studyStatusPrivileges = teamAuthModule.getStudyStatusPrivileges();//getStudyStatusPrivileges();
		//Kanwal - no need to check right for Study Team tab
		//int studyTeamPrivileges = teamAuthModule.getStudyTeamPrivileges();
		
		if (!TeamAuthModule.hasViewPermission(studyStatusPrivileges)
				//|| !TeamAuthModule.hasViewPermission(studyTeamPrivileges) - commented by Kanwal
				){
			if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view study Status");
			addIssue(
					new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
							"User does not have view permission to view Study Status"));
			throw new AuthorizationException("User does not have view permission to view Study Status");
		}
		CodeCache codeCache = CodeCache.getInstance();
		StudyStatus studyStatus = new StudyStatus();
		StudyStatusIdentifier studyStatusIdentifier = new StudyStatusIdentifier();
		Integer studyStatusPk = studyStatusBean.getId();
		
		ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_STATUS, studyStatusPk);
		String OID = obj.getOID();
		studyStatusIdentifier.setOID(OID);
		studyStatusIdentifier.setPK(studyStatusPk);
				
		studyStatus.setStudyStatusId(studyStatusIdentifier);
		
		//set study status site
		Integer studyStatusSitePk = EJBUtil.stringToInteger(studyStatusBean.getSiteId());
		SiteBean siteBean = siteAgent.getSiteDetails(studyStatusSitePk);
		ObjectMap objSite = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, studyStatusSitePk);
		OrganizationIdentifier orgId = new OrganizationIdentifier();
		orgId.setSiteName(siteBean.getSiteName());
		orgId.setOID(objSite.getOID());
		orgId.setPK(studyStatusSitePk);
		studyStatus.setOrganizationId(orgId);
		
		Integer studyStatusTypePK = EJBUtil.stringToInteger(studyStatusBean.getStudyStatusType());
		if(studyStatusTypePK != null){
			Code studyStatusTypeCode = 
				codeCache.getCodeSubTypeByPK(
						CodeCache.CODE_TYPE_STATUS_TYPE, 
						studyStatusTypePK,
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
			studyStatus.setStatusType(studyStatusTypeCode);
		}
		else{
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Study Status Type Code not found:::" +studyStatusTypePK));
			throw new OperationException();
			
		}
		Integer studyStatusPK = EJBUtil.stringToInteger(studyStatusBean.getStudyStatus());
		if(studyStatusPK != null){
			Code studyStatusCode = 
				codeCache.getCodeSubTypeByPK(
						CodeCache.CODE_TYPE_STATUS, 
						studyStatusPK,
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
			studyStatus.setStatus(studyStatusCode);
		}
		else{
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Study Status Code not found:::" +studyStatusPK));
			throw new OperationException();
			
		}
		
		Integer userDocByPK = EJBUtil.stringToInteger(studyStatusBean.getStatDocUser());
		UserIdentifier userDocBy = getUserIdFromPK(userDocByPK, objectMapService, userAgent);
		ObjectMap objUserDocBy = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, userDocByPK);
		userDocBy.setOID(objUserDocBy.getOID());
		studyStatus.setDocumentedBy(userDocBy);

		//set studystatus assigned to
		Integer studyStatusAssignedToUserPk = EJBUtil.stringToInteger(studyStatusBean.getStudyStatusAssignedTo());
		if(studyStatusAssignedToUserPk != null){
		UserIdentifier studyStatusAssignedToUserIdentifier = getUserIdFromPK(studyStatusAssignedToUserPk, objectMapService, userAgent);
		ObjectMap objUserAssignedTo = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, studyStatusAssignedToUserPk);
		userDocBy.setOID(objUserAssignedTo.getOID());
		studyStatus.setAssignedTo(studyStatusAssignedToUserIdentifier);
		}
		else{
			addIssue(
					new Issue(
							IssueTypes.GET_DOCUMENTED_BY_USERID_NOT_FOUND, 
							"Get Documented by User Id not found:::" +studyStatusAssignedToUserPk));
		}
		
		Date validFromDate = studyStatusBean.getStatStartDate();
		studyStatus.setValidFromDate(validFromDate);
		
		Date validUntilDate = studyStatusBean.getStatValidDt();
		studyStatus.setValidUntilDate(validUntilDate);
		
		Date meetingDate = studyStatusBean.getStatMeetingDt();
		studyStatus.setMeetingDate(meetingDate);
		
		Integer reviewBoardCodePK = EJBUtil.stringToInteger(studyStatusBean.getStudyStatusReviewBoard());
		if(reviewBoardCodePK != null){
			Code studyStatusReviewBoardCode = 
				codeCache.getCodeSubTypeByPK(
						CodeCache.CODE_TYPE_REVIEW_BOARD, 
						reviewBoardCodePK,
						EJBUtil.stringToNum(callingUser.getUserAccountId()));
			studyStatus.setReviewBoard(studyStatusReviewBoardCode);
		}
		else{
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Review Board Code not found:::" +reviewBoardCodePK));
			
		}
		
		Integer outcomeCodePK = EJBUtil.stringToInteger(studyStatusBean.getStudyStatusOutcome()); 
		if(outcomeCodePK != null){
		Code studyStatusOutcomeCode = 
			codeCache.getCodeSubTypeByPK(
					CodeCache.CODE_TYPE_OUTCOME, 
					outcomeCodePK,
					EJBUtil.stringToNum(callingUser.getUserAccountId()));
		studyStatus.setOutcome(studyStatusOutcomeCode);
		}
		else{
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Status Outcome Code not found Code not found:::" +outcomeCodePK));
			
		}
		String studyStatusNotes = studyStatusBean.getStatNotes();
		studyStatus.setNotes(studyStatusNotes);
		
		String currentStatus = studyStatusBean.getCurrentStat();
		if(currentStatus != null){
			Boolean isCurrentStatus = currentStatus.equals("1") ? true : false;
			studyStatus.setCurrentForStudy(isCurrentStatus);
		}
		//virendra;Fixed#6124, overloading setParentId for studyIdentifier
		Integer parentStudyPK = EJBUtil.stringToInteger(studyStatusBean.getStatStudy());
		StudyBean studyBean = studyAgent.getStudyDetails(parentStudyPK);
		ObjectMap objStatStudy = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, parentStudyPK);
		
		
		StudyIdentifier studyId = new StudyIdentifier();
		studyId.setOID(objStatStudy.getOID());
		studyId.setStudyNumber(studyBean.getStudyNumber());
		studyStatus.setParentIdentifier(studyId);
		
	return 	studyStatus;
	}
	
	//virendra
//	private Integer locateStudyStatusPK(StudyStatusIdentifier studyStatusIdentifier) 
//	throws OperationException{
//		if  (studyStatusIdentifier == null || 
////				(studyIdentifier.getSystemId() == null && 
//				studyStatusIdentifier.getOID() == null){
//			
//			addIssue(new Issue(IssueTypes.DATA_VALIDATION, "Study Status Identifier is required"));
//			throw new ValidationException();
//		}
//		Integer studyStatusPK =
//			ObjectLocator.studyStatusPKFromIdentifier(callingUser, studyStatusIdentifier,objectMapService);
//		
//		if (studyStatusPK == null | studyStatusPK == 0){
//			addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for: " + studyStatusIdentifier));
//			throw new OperationException();
//		}
//		return studyStatusPK;
//	}
}