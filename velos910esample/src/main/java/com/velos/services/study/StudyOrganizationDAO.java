/**
 * 
 */
package com.velos.services.study;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.StudyOrganization;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ServicesUtil;

/**
 * @author dylan
 *
 */
public class StudyOrganizationDAO extends CommonDAO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -167530389376670749L;
	private static Logger logger = Logger.getLogger(StudyOrganizationDAO.class);
	static ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
	
	private static String orgsSQL = " select PK_STUDYSITES, s.FK_SITE, s.ENR_NOTIFYTO, s.ENR_STAT_NOTIFYTO, studysite_enrcount," 
		+ " s.studysite_public, s.studysite_repinclude, s.is_reviewbased_enr, "
		+ " (select SITE_NAME from er_site where  PK_SITE  = s.FK_SITE) as site_name, "
		+ " (select SITE_ALTID from er_site where  PK_SITE  = s.FK_SITE) as site_altid, "
		+ " (select codelst_subtyp from er_codelst where  PK_CODELST = FK_CODELST_STUDYSITETYPE) as type,  "
		+ " STUDYSITE_LSAMPLESIZE, case when ( pkg_user.f_chk_right_for_studysite(s.fk_study,?,s.fk_site) > 0   )  "
		+ " then 1  else 0  end as site_access,  "
		+ " case when 1 in (select current_stat from er_studystat where fk_site = s.fk_site and fk_study = s.fk_study ) then 1 else 0 end as current_stat" //KM
		+ " from er_studysites s  where s.fk_study = ? ";
	
	private static String usersSQL = "select usr_firstname, usr_lastname, usr_logname from er_user " +
										"where fk_account = ? and " +
										"pk_user in ?";
	
	/**
	 * Returns a map of site/studysite primary keys for a study, that a user has access to
	 * @param studyPK study to get the study/studysite pks for
	 * @param userPK user requesting the information
	 * @return map of site/studysite primary keys. key is studysite, value is site
	 */
	public static Map<Integer, Integer> getStudySiteOrganziationPKs(
			int studyPK, 
			int userPK){

		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			if (logger.isDebugEnabled()) logger.debug("StudySiteDao.getStudySiteOrganziationPKs sql:" + orgsSQL);
			
			pstmt = conn.prepareStatement(orgsSQL);
			pstmt.setInt(1, userPK);
			pstmt.setInt(2, studyPK);


			ResultSet rs = pstmt.executeQuery();
			Map<Integer, Integer> returnStudySitePKMap= new HashMap<Integer, Integer>();
			while (rs.next()) {
				Integer studySitePK = rs.getInt("PK_STUDYSITES");
				Integer sitePK = rs.getInt("fk_site");
				
				returnStudySitePKMap.put(studySitePK, sitePK);
			}
			return returnStudySitePKMap;

		} catch (SQLException ex) {
			logger.fatal("StudyOrganizationDAO.getStudySiteOrganziationPKs EXCEPTION IN FETCHING FROM er_studysites"
					+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return null;
	}
	
	public static List<StudyOrganization> getStudySiteOrganziations(
			int studyPK, 
			int userPK,
			int userAccountPK){

		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();

			if (logger.isDebugEnabled()) logger.debug("StudySiteDao.getStudySiteTeamValues sql:" + orgsSQL);
			
			pstmt = conn.prepareStatement(orgsSQL);
			pstmt.setInt(1, userPK);
			pstmt.setInt(2, studyPK);


			ResultSet rs = pstmt.executeQuery();
				
			List<StudyOrganization> returnOrgs = new ArrayList<StudyOrganization>();
			while (rs.next()) {
				StudyOrganization studyOrg = new StudyOrganization();
				OrganizationIdentifier orgId = 
					new OrganizationIdentifier(
							rs.getString("site_name"), 
							rs.getString("site_altid"));
				//ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
				
				ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, rs.getInt("fk_site")); 
				orgId.setOID(map.getOID()); 
				orgId.setPK(rs.getInt("fk_site"));
				String orgType = rs.getString("type");
				if (orgType != null && orgType.length() > 0){
					Code studyType = 
						new Code(
								CodeCache.CODE_TYPE_STUDY_ORG_TYPE, 
								rs.getString("type"));
					studyOrg.setType(studyType);

				}
				//virendra: fixed #5558, changed data type of localSampleSize to String
				Integer localSampleSize = null;
				if (rs.getString("STUDYSITE_LSAMPLESIZE") != null){
					try{
						localSampleSize=EJBUtil.stringToInteger(rs.getString("STUDYSITE_LSAMPLESIZE"));
					}
					catch(NumberFormatException e){
						if (logger.isDebugEnabled()) logger.debug("local sample size for " + orgId, e);
					}
				}
				
				int studySiteEnrCnt = rs.getInt("studysite_enrcount");
				boolean studySitePublic = (rs.getInt("studysite_public") == 1) ? true :false;
				boolean studySiteRepInclude = (rs.getInt("studysite_repinclude") == 1) ? true :false;
				
				//isReviewBasedEnroll is technically a boolean, but for some reason stored
				//as a string...so we take extra precautions against null pointer.
				String isReviewBasedEnrString = rs.getString("is_reviewbased_enr");
				boolean isReviewBasedEnroll = false;
				if (isReviewBasedEnrString != null){
					if (isReviewBasedEnrString.equals("1")){
						isReviewBasedEnroll = true;
					}				
				}
				//enroll notify users comes in as comma-separated list of primary keys.
				//parse the string and turn each into a userIdentifer.
				String enrNotifyPKList = rs.getString("ENR_NOTIFYTO");
				//List<UserIdentifier> enrollNotifyUserIds = new ArrayList<UserIdentifier>();
				if (enrNotifyPKList != null){
					List<UserIdentifier> users = getUsers(enrNotifyPKList, userAccountPK);
					studyOrg.setNewEnrollNotificationTo(users);
				}
				
				
				String statChangeNotifyPKList = rs.getString("ENR_STAT_NOTIFYTO");
				//List<UserIdentifier> changeStatusNotifyUserIds = new ArrayList<UserIdentifier>();
				if (statChangeNotifyPKList != null){
					List<UserIdentifier> users = getUsers(statChangeNotifyPKList, userAccountPK);
					studyOrg.setApproveEnrollNotificationTo(users);
				}
				//setSiteAccess(rs.getString("site_access"));
				//setCurrStats(new Integer(rs.getInt("current_stat")));//KM
			
				//setStudySiteIds(new Integer(rs.getInt("PK_STUDYSITES")));

//				studyOrg.setAttachment(attachment)
				
				//Virendra: Commented to fix #5397
//				studyOrg.setCurrentSitePatientCount(studySiteEnrCnt);
				studyOrg.setLocalSampleSize(localSampleSize);
				studyOrg.setOrganizationIdentifier(orgId);
				studyOrg.setPublicInformationFlag(studySitePublic);
				studyOrg.setReportableFlag(studySiteRepInclude);
				studyOrg.setReviewBasedEnrollmentFlag(isReviewBasedEnroll);
				


				returnOrgs.add(studyOrg);
			}

			return returnOrgs;

		} catch (SQLException ex) {
			logger.fatal("StudyOrganizationDAO.getStudySiteOrganziations EXCEPTION IN FETCHING FROM er_studysites"
					+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return null;
	}
	private static List<UserIdentifier> getUsers(
			String userPKList, 
			int accId){
	

		PreparedStatement pstmt = null;
		Connection conn = null;
		try {
			conn = getConnection();
			
			if (logger.isDebugEnabled()) logger.debug("StudyOrganizationDAO.getStudySiteTeamValues sql:" + usersSQL);
			//Virendra: Fixed Bug No.: 5452
			List<UserIdentifier> users = new ArrayList<UserIdentifier>();	;
			String[] userPKListArray= commaSeparatedStringToStringArray(userPKList);
			for(int i=0; i< userPKListArray.length; i++ ){
			
			pstmt = conn.prepareStatement(usersSQL);
			pstmt.setInt(1, accId);
			pstmt.setString(2, userPKListArray[i]);


			ResultSet rs = pstmt.executeQuery();
						
			while (rs.next()) {
				String firstName = rs.getString("usr_firstname");
				String lastName = rs.getString("usr_lastname");
				String loginName = rs.getString("usr_logname");
				UserIdentifier user = 
					new UserIdentifier(
							loginName, 
							firstName, 
							lastName);
				//Virendra: Fixed#6237
				ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_USER, EJBUtil.stringToInteger(userPKListArray[i]));
				String strUserOID = obj.getOID();
				user.setOID(strUserOID);
				user.setPK(StringUtil.stringToInteger(userPKListArray[i]));
				
				users.add(user);
				}
			}
			return users;

		} catch (SQLException ex) {
			logger.fatal("StudyOrganizationDAO.getStudySiteTeamValues EXCEPTION IN FETCHING FROM User table"
					+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return null;
	}
	
	private static String[] commaSeparatedStringToStringArray(String aString){
	    String[] splittArray = null;
	    if (aString != null && !aString.equalsIgnoreCase("")){
	         splittArray = aString.split(",");
	        }
	    return splittArray;
	}

}
