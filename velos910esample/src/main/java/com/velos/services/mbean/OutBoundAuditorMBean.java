/**
 * Created On Mar 31, 2011
 */
package com.velos.services.mbean;

import org.jboss.system.ServiceMBean;

/**
 * @author Kanwaldeep
 *
 */
public interface OutBoundAuditorMBean extends ServiceMBean {

}
