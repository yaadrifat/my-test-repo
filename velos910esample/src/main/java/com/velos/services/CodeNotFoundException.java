/**
 * 
 */
package com.velos.services;

import com.velos.services.model.Code;

/**
 * Exception indicating that a Code object was defined, but 
 * not found, usually when derefencing.
 * @author dylan
 *
 */
public class CodeNotFoundException extends ValidationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5848961051706950094L;

	private Code code;

	public CodeNotFoundException(Code code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public CodeNotFoundException(Code code, String message) {
		super(message);
		this.code = code;
	}

	public CodeNotFoundException(Code code) {
		this.code = code;
	}
	
	@Override
	public String getMessage() {
		
		return CodeNotFoundException.class.getName() + " Code: " + code.getCode() + "type: " + code.getType() + super.getMessage();
	}

	
}
