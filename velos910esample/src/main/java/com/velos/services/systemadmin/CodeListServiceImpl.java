/**
 * Created On Nov 5, 2012
 */
package com.velos.services.systemadmin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.AbstractService;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Code;
import com.velos.services.model.CodeType;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;
import com.velos.services.util.CodeCache;

/**
 * @author Kanwaldeep
 *
 */
@Stateless
public class CodeListServiceImpl extends AbstractService implements CodeListService{

	@Resource 
	private SessionContext sessionContext;
	
	@EJB
	private UserAgentRObj userAgent;
	
	
	private static Logger logger = Logger.getLogger(CodeListServiceImpl.class.getName());

	
	@Override
	public Codes getCodeList(String codeListType) throws OperationException {
		
		Codes codes = new Codes(); 
		
		try{
			
			
			//TZ code list check for 		
			if(codeListType.equalsIgnoreCase(CodeCache.CODE_TYPE_TIMEZONE))
			{

				CodeDao cd2 = new CodeDao(); 
				cd2.getCodeValuesForTimeZone(codeListType); 
				for(int i=0; i < cd2.getCId().size(); i++) {
					Code code = new Code();
					code.setType(cd2.getCType());
					code.setCode((String)cd2.getCSubType().get(i));
					code.setDescription((String)cd2.getCDesc().get(i));
					codes.add(code);
				}	
			}else{

				// now translate service_type code type to real code type
				
				CodeServiceDAO codeDao = new CodeServiceDAO(); 
				codes = codeDao.getCodes(codeListType, StringUtil.stringToNum(callingUser.getUserAccountId()));
				
			}
			
			if(codes.getCodes().isEmpty())
			{
				addIssue(new Issue(IssueTypes.CODE_LIST_TYPE_NOT_FOUND, "Code list for type " + codeListType + " not found")); 
				throw new OperationException(); 
			}

		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", t);
			throw new OperationException(t);
		}
		return codes; 
	}

@AroundInvoke
public Object myInterceptor(InvocationContext ctx) throws Exception {
	response = new ResponseHolder();
	callingUser = 
		getLoggedInUser(
				sessionContext,
				userAgent);
	return ctx.proceed();

}

public CodeTypes getCodeTypes() throws OperationException {
	CodeServiceDAO serviceDAO = new CodeServiceDAO(); 
	CodeTypes types; 
	try{
		types = serviceDAO.getCodeTypes(); 
		CodeType timezoneCode = new CodeType();
		timezoneCode.setCodeType(CodeCache.CODE_TYPE_TIMEZONE);
		timezoneCode.setDescription("Code For Timezones");
		types.add(timezoneCode); 
	}catch(Throwable t){
		this.addUnknownThrowableIssue(t);
		if (logger.isDebugEnabled()) logger.debug("StudyServiceImpl create", t);
		throw new OperationException(t);
	}
	return types;
}

}
