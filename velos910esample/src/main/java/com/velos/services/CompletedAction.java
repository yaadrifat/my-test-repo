/**
 * 
 */
package com.velos.services;


import java.io.Serializable;

import com.velos.services.model.SimpleIdentifier;

/**
 * @author  dylan
 */
public class CompletedAction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -908944906416237032L;

	/**
	 * 
	 */
	private SimpleIdentifier objectId;
	
	/**
	 * 
	 */
	private CRUDAction action;

	private String objectName;
	
	/**
	 * 
	 */
	public CompletedAction() {
	}
	
	public CompletedAction( String objectName, CRUDAction action) {
		super();
		this.action = action;
		this.objectName = objectName;
	}

	public CompletedAction(SimpleIdentifier objectId ,CRUDAction action) {
		super();
		this.objectId = objectId;
		this.action = action;
	}

	// new Constructor
	// if identifier Object is not available in WSDL, xsi:type will not get populated
	public CompletedAction(SimpleIdentifier objectId, String objectName, CRUDAction action)
	{
		super();
		this.objectId = objectId;
		this.action = action;
		this.objectName = objectName;		
	}
	
	public SimpleIdentifier getObjectId() {
		return objectId;
	}


	public CRUDAction getAction() {
		return action;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public void setObjectId(SimpleIdentifier objectId) {
		this.objectId = objectId;
	}

	public void setAction(CRUDAction action) {
		this.action = action;
	}




	
}