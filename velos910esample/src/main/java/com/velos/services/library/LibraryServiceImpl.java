package com.velos.services.library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.web.eventdef.EventdefJB;
import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Cost;
import com.velos.services.model.Event;
import com.velos.services.model.EventSearch;
import com.velos.services.model.Events;
import com.velos.services.model.LibraryEvent;
import com.velos.services.model.LibraryEvents;
import com.velos.services.patientschedule.PatientScheduleServiceImpl;
import com.velos.services.util.CodeCache;


@Stateless
@Remote(LibraryService.class)
public class LibraryServiceImpl extends AbstractService 
implements LibraryService{
	
	@EJB
	private UserAgentRObj userAgent;
	
	@Resource 
	private SessionContext sessionContext;
	
	private static Logger logger = Logger.getLogger(LibraryServiceImpl.class.getName());

	@Override
	public LibraryEvents searchEvent(EventSearch eventSearch) throws OperationException {
		
		EventdefJB eventdefB = new EventdefJB();
		EventdefDao eventdefDAO1 = new EventdefDao();
		EventdefDao eventdefDAO = new EventdefDao();
		Events events = new Events();
		LibraryEvents libraryEvents = new LibraryEvents();
		//LibraryEvent libraryEvent = new LibraryEvent();
		List<Event> eventList = new ArrayList<Event>();
		int eventLibraryType = 0;
	
		
		try{
			
			Map parameters = new HashMap();
			parameters.put("callingUser", this.callingUser);


			
			eventLibraryType = dereferenceSchCode(eventSearch.getLibraryType(), CodeCache.CODE_EVENT_LIBRARY_TYPE, callingUser);
            
			libraryEvents = LibraryServiceDAO.searchEvent(eventSearch, eventLibraryType, parameters);

			
			return libraryEvents;
			
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("LibraryServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("LibraryServiceImpl create", t);
			throw new OperationException(t);
		}
	

	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}


}
