/**
 * 
 */
package com.velos.services.monitoring;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.velos.eres.service.util.DateUtil;
import com.velos.services.AbstractService;
import com.velos.services.CRUDAction;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.MessageTraffic;

/**
 * @author dylan
 * Implementation class for Monitoring Services operations.
 *
 */

@Stateless
public class MessageServiceImpl extends AbstractService implements MessageService{

	private static Logger logger = Logger.getLogger(MessageServiceImpl.class.getName());
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
	

	public List<MessageLog> getMessages(Date fromDate, Date toDate, String module) throws OperationException {
		return MonitorDAO.fetchMessageLogsByDateRange(fromDate, toDate, module);
	}


	public MessageLog setMessageDetails(MessageLog message) throws OperationException {
		em.persist(message);
		return message;
		
	}


	/* (non-Javadoc)
	 * @see com.velos.services.monitoring.MessageService#updateMessage(com.velos.services.monitoring.MessageLog)
	 */
	public void updateMessage(MessageLog message) throws OperationException {
		em.merge(message);
		
	}
	/**
	 * Gets the message traffic which eSP experiences between a date range
	 * and returns count of All messages, Succeeded messages and failed messages
	 * between the date range.
	 * virendra
	 * @param fromDate
	 * @param toDate
	 * @param module
	 * @return
	 * @throws OperationException
	 */
	
	public MessageTraffic getMessageTraffic(Date fromDate, Date toDate) throws OperationException{
		Long messageCountAll = null;
		Long messageCountSuccess=null;
		Long MessageCountFailure = null;
		
		Date myDate = new Date();
	    //Bug Fix for #9115: Tarandeep Singh Bali	
		if (toDate.compareTo(myDate) > 0 || fromDate.compareTo(myDate) > 0)
    	{
	       	response.addIssue(
					new Issue(
							IssueTypes.DATA_VALIDATION, 
							"fromDate or toDate cannot be a future date"));
			throw new OperationException(response.getIssues());
    	}
		//End of Bug Fix #9115
		
		//Bug Fix for #9159: Tarandeep Singh Bali
		if (fromDate.compareTo(toDate) > 0)
		{
	       	response.addIssue(
					new Issue(
							IssueTypes.DATA_VALIDATION, 
							"fromDate cannot be greater than the toDate"));
			throw new OperationException(response.getIssues());
		}
    	//End Of Bug Fix #9159
	    
		try{
			MessageTraffic messageTraffic = new MessageTraffic();
			//Commented as Arraylist not required as of now. May be needed in future with new set of requirements.
			//ArrayList<MessageTraffic> lstHeartBeatCount= new ArrayList<MessageTraffic>();
			
			Query queryAllMsg = em.createNamedQuery("findMessageCountAll");
			//Commented as module parameter not required as of now. May be needed in future with new set of requirements.
			//queryAllMsg.setParameter("module", module);
			queryAllMsg.setParameter("fromDate", fromDate);
			queryAllMsg.setParameter("toDate", toDate);
			messageCountAll = (Long) queryAllMsg.getSingleResult();
			messageTraffic.setMessageCountAll(messageCountAll);
			
			Query querySuccessMsg = em.createNamedQuery("findMessageCountSuccess");
			//querySuccessMsg.setParameter("module", module);
			querySuccessMsg.setParameter("fromDate", fromDate);
			querySuccessMsg.setParameter("toDate", toDate);
			messageCountSuccess = (Long)querySuccessMsg.getSingleResult();
			messageTraffic.setMessageCountSuccess(messageCountSuccess);
			
			Query queryFailureMsg = em.createNamedQuery("findMessageCountFailure");
			//queryFailureMsg.setParameter("module", module);
			queryFailureMsg.setParameter("fromDate", fromDate);
			queryFailureMsg.setParameter("toDate", toDate);
			MessageCountFailure = (Long)queryFailureMsg.getSingleResult();
			messageTraffic.setMessageCountFailure(MessageCountFailure);
			
			return messageTraffic;
		}
		catch(Throwable t){
			
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("MessageServiceImpl Get", t);
			throw new OperationException(t);
		}
	}
	/**
	 * Gets runtime status of database server and returns response
	 * wrapped in ResponseHolder object along with response(if running)
	 *  and issues(if any error). 
	 */
	
	public ResponseHolder getHeartBeat() throws OperationException{
		try{
			if(MonitorDAO.getRuntimeStatus()){
				response.addAction(new CompletedAction("Getting Heartbeats",CRUDAction.RETRIEVE));
			}
			else{
				response.addIssue(
				new Issue(
						IssueTypes.DATABASE_CONNECTION_NOT_FOUND, 
						"Database connection not found"));
			}
		}
		catch(Throwable t){
			
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("MessageServiceImpl Get", t);
			throw new OperationException(t);
		}
		return response;
	}
	
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		return ctx.proceed();

	}
	
}
