package com.velos.services.outbound;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import com.sun.org.apache.xerces.internal.dom.DocumentImpl;
import com.velos.eres.business.common.CommonDAO;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.UserIdentifier;

/**
 * @author Kanwaldeep
 *
 */
public class MessagingDAO extends CommonDAO{
	
	private static Logger logger = Logger.getLogger(MessagingDAO.class); 
	
	private static String getDataForProcessingSQL ="select pk_message, tablename, table_pk, module, action, root_pk, fk_account, processed_flag, root_tablename, to_char(created_on, PKG_DATEUTIL.f_get_dateformat||' '||PKG_DATEUTIL.f_get_timeformat) create_on_string from er_msg_queue where (processed_flag = ? or processed_flag = ?) and fk_account = ? order by root_pk, table_pk, action, create_on_string desc";
	
	private static String insertIntoHistoryTableSQL = "insert into er_msg_queue_history(PK_MESSAGE, TABLENAME, TABLE_PK, MODULE, FK_ACCOUNT, PROCESSED_FLAG, ACTION, ROOT_PK, ROOT_TABLENAME, CREATED_ON, ERROR_MESSAGE)" +
													"  select pk_message, tablename, table_pk, module, fk_account, ?, action, root_pk, root_tablename, created_on, ? FROM ER_MSG_QUEUE WHERE PK_MESSAGE = ?";
	private static String updateStatusSQL = "update er_msg_queue set processed_flag = ? where pk_message = ?"; 
	
	private static String deleteFromTableSQL = "delete from er_msg_queue where pk_message = ?";  
	
	private static String getAdditionalInformationSQL = "select ELEMENT_NAME, ELEMENT_VALUE from er_msg_addinfo where FK_MSG_QUEUE = ?"; 
	
	private static String insertIntoAddInfoHistoryTableSQL = "insert into er_msg_addinfo_history(PK_MSG_ADDINFO, FK_MSG_QUEUE, ELEMENT_NAME, ELEMENT_VALUE, COL_NAME)" +
														" select PK_MSG_ADDINFO, FK_MSG_QUEUE, ELEMENT_NAME, ELEMENT_VALUE, COL_NAME from er_msg_addinfo where FK_MSG_QUEUE = ?"; 
	private static String deleteFromAddInfoSQL = "delete from er_msg_addinfo where FK_MSG_QUEUE = ?";  
	
	public List<MessageQueueBean> getDataForProcessing(int accountFK)
	{
		 List<MessageQueueBean> list = new ArrayList<MessageQueueBean>(); 
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		
		try
		{
			conn = getConnection(); 
			
			stmt = conn.prepareStatement(getDataForProcessingSQL); 
			stmt.setInt(1, MessagingConstants.MESSAGE_STATUS_NEW); 
			stmt.setInt(2, MessagingConstants.MESSAGE_STATUS_RE_PROCESS);
			stmt.setInt(3, accountFK); 
			
			
			rs = stmt.executeQuery(); 
		
			while(rs.next())
			{
				MessageQueueBean bean  = new MessageQueueBean(); 
				bean.setMessagePK(rs.getInt("pk_message"));
				bean.setTableName(rs.getString("tablename")); 
				bean.setTablePK(rs.getInt("table_pk")); 
				bean.setModule(rs.getString("module")); 
				bean.setAction(rs.getString("action")); 
			//	bean.setFieldsUpdated(rs.getString("fields_updated")); 
				bean.setRootFK(rs.getString("root_pk")); 
				bean.setAccountFK(rs.getInt("fk_account")); 
				bean.setProcessedFlag(rs.getInt("processed_flag"));
				bean.setRootTableName(rs.getString("root_tablename"));
				bean.setCreatedOn(rs.getString("create_on_string"));
				list.add(bean); 
				
			}
			
			
		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
			logger.log(Level.ERROR, sqe);
		}
		finally
		{			
			closeRS(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
		return list; 		
	}
	 
	
	public void updateStatus(int status, List<Integer> messagePK)
	{
		PreparedStatement stmt = null; 
		int updated = 0;
		Connection conn = null; 
		try
		{
			conn  = getConnection (); 
			stmt =  conn.prepareStatement(updateStatusSQL); 
			
			Iterator<Integer> pkIterator = messagePK.iterator(); 
			while(pkIterator.hasNext())
			{
				stmt.setInt(1, status); 
				stmt.setInt(2, pkIterator.next()); 
				stmt.addBatch(); 
			}
			
			 stmt.executeBatch();
			conn.commit(); 
		}catch(SQLException sqe)
		{
			logger.log(Level.ERROR, sqe); 
		}finally
		{
			
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
		
	}
	
	public void closeRS(ResultSet rs)
	{
		try {
			if(rs != null) rs.close(); 			
		} catch (SQLException e) {
			logger.error("Unable to close ResultSet", e); 
			e.printStackTrace();
		} 
	}
	
	public void closeStatement(PreparedStatement stmt)
	{
		try{
			if(stmt !=null) stmt.close();			 
		}catch(SQLException e){
			logger.error("Unable to close PreparedStatement", e); 
		}
		
	}
	
	public void closeConnection(Connection conn)
	{
		try{
			if(conn != null) conn.close(); 
		}catch(SQLException e)
		{
			logger.error("Unable to close Connection", e); 
		}
	}


	/**
	 * @param pkList
	 */
	public void moveToHistoryTable(List<Integer> pkList,
			String errorMessage,
			int processedFlag) {
		
		Connection conn = null ;
		try
		{
			conn = getConnection();
			conn.setAutoCommit(false); 
			//insertIntoHistoryTable; 
			insertIntoHistoyTable(pkList, errorMessage, processedFlag, conn); 
			insertAddInfoIntoHistoryTable(pkList, conn); 
			
			//deleteFromTable
			deleteFromAddInfoTable(pkList, conn); 
			deleteFromQueueTable(pkList, conn); 
			
			
			conn.commit(); 
			
			
		} catch (SQLException e) {
			logger.error("Error occured during Outbound Queue Table clean up ", e); 
			try {
				conn.rollback();
			} catch (SQLException e1) {
				logger.error("Error occured during rollback of Outbound Queue Table clean up ", e); 
			} 
			e.printStackTrace();
		}finally
		{
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			returnConnection(conn); 
		}
		
	}




	/**
	 * @param pkList
	 * @param conn
	 */
	private void deleteFromAddInfoTable(List<Integer> pkList, Connection conn) throws SQLException{
		PreparedStatement stmt = null ;
		try
		{
			stmt = conn.prepareStatement(deleteFromAddInfoSQL); 
			for(int pkmessage: pkList)
			{
				stmt.setInt(1, pkmessage); 
				stmt.addBatch(); 
			}
			
			stmt.executeBatch(); 
			
		}finally
		{
			closeStatement(stmt);
		}
		
	}


	/**
	 * @param pkList
	 * @param conn
	 */
	private void insertAddInfoIntoHistoryTable(List<Integer> pkList,
			Connection conn) throws SQLException{

		PreparedStatement stmt = null ; 
		
		try
		{
			stmt = conn.prepareStatement(insertIntoAddInfoHistoryTableSQL); 
			for(int pkmessage: pkList)
			{
				stmt.setInt(1, pkmessage); 				
				stmt.addBatch(); 
			}
			
			stmt.executeBatch(); 
			
		}finally
		{
			closeStatement(stmt);
		}
		
	}


	/**
	 * @param pkList
	 * @param conn
	 */
	private void deleteFromQueueTable(List<Integer> pkList,
			Connection conn) throws SQLException{
		
		PreparedStatement stmt = null ;
		try
		{
			stmt = conn.prepareStatement(deleteFromTableSQL); 
			for(int pkmessage: pkList)
			{
				stmt.setInt(1, pkmessage); 
				stmt.addBatch(); 
			}
			
			stmt.executeBatch(); 
			
		}finally
		{
			closeStatement(stmt);
		}
		
	}


	/**
	 * @param pkList
	 * @param errorMessage
	 * @param processedFlag
	 */
	private void insertIntoHistoyTable(List<Integer> pkList,
			String errorMessage, int processedFlag, Connection conn) throws SQLException{
		
		PreparedStatement stmt = null ; 
		
		try
		{
			stmt = conn.prepareStatement(insertIntoHistoryTableSQL); 
			for(int pkmessage: pkList)
			{
				stmt.setInt(1, processedFlag);
				stmt.setString(2, errorMessage); 
				stmt.setInt(3, pkmessage); 
				
				stmt.addBatch(); 
			}
			
			stmt.executeBatch(); 
			
		}finally
		{
			closeStatement(stmt);
		}
		
	}


	/**
	 * @return
	 */
	public List<JAXBElement> getAddtionalInformation(int messagePK) {		

		
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null ;
		List<JAXBElement> elements = new ArrayList<JAXBElement>(); 
	//	StringBuffer xmlElements = new StringBuffer(); 
		
		try
		{
			conn = getConnection(); 
			
			stmt = conn.prepareStatement(getAdditionalInformationSQL); 
			stmt.setInt(1, messagePK); 	
			
			rs = stmt.executeQuery(); 
		
			while(rs.next())
			{
				if(rs.getString(1).equalsIgnoreCase("UserIdentifier"))
				{
					elements.add(new JAXBElement(new QName(rs.getString(1)), UserIdentifier.class, MessageProcessor.getIdentifierObject(Integer.parseInt(rs.getString(2)), ObjectMapService.PERSISTENCE_UNIT_USER))); 
				}else
				{
					elements.add(new JAXBElement(new QName(rs.getString(1)), String.class, rs.getString(2))); 
				}
			}
			
			
		}catch(SQLException sqe)
		{
			sqe.printStackTrace(); 
			logger.log(Level.ERROR, sqe);
		}
		finally
		{			
			closeRS(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		
		return elements	; 
		
	}
		

}
