package com.velos.services.outbound;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;


import com.velos.eres.business.common.CommonDAO;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.AccountFormResponseIdentifier;
import com.velos.services.model.BudgetIdentifier;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarStatusIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.MoreStudyDetailsIdentifier;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.ParentIdentifier;
import com.velos.services.model.PatientFormResponseIdentifier;
import com.velos.services.model.SimpleIdentifier;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudyTeamIdentifier;
import com.velos.services.model.UserIdentifier;
import com.velos.services.util.ServicesUtil;

/**
 * @author Kanwaldeep
 * 
 * 
 * 
 */
public class MessageProcessor {

	private static Logger logger = Logger.getLogger(MessageProcessor.class);

	/**
	 * 
	 */
	/**This method processes all the messages available in Queue for given Account Number.This method serves as main handler for outbound messaging.
	 * This is entry point for TimerTask.
	 * @param accountNumber
	 */
	public static void processMessages(int accountNumber) {
		// Get all the records which are not processed yet for this account
		
		
		try{	
		 logger.info("Started Batch for Outbound messaging for Account Number " + accountNumber); 
		//
			MessagingDAO dao = new MessagingDAO();
			MessagingAccountDAO accountdao = new MessagingAccountDAO(); 
			// Module Group and Topic setting 
			Map<String,MessagePublisher> moduleGroupPublisherMap = accountdao.getTopicInformation(accountNumber); 
			List<MessageQueueBean> messageQueueList = dao.getDataForProcessing(accountNumber);			
			Set<Class> identifierClasses = new HashSet<Class>();
			identifierClasses.add(UserIdentifier.class); 
			identifierClasses.add(Changes.class); 
			Class[] array = new Class[0]; 
			String currentRootFK ="";
			String currentRootTableName = "";
			
			//currentTableName and currentTablePK are have SimpleIdentifier initialized only one for each tablePk and tableName			
			String currentTableName = ""; 
			int currentTablePK = 0;  
			SimpleIdentifier childIdentifier = null; 
			Changes changes = null;		 
			List<Integer> pkList = new ArrayList<Integer>();
			Map<Thread, MessageSender> threadList = new HashMap<Thread, MessageSender>(); 
			for (MessageQueueBean bean : messageQueueList) {
				if(! (bean.getRootFK().equalsIgnoreCase(currentRootFK)
						 && bean.getRootTableName().equalsIgnoreCase(
							currentRootTableName))) {
					if (changes != null)
						  processMessage(pkList, changes, moduleGroupPublisherMap.get(currentRootTableName),identifierClasses.toArray(array), threadList); // process the previous
														// Changes Message
					currentRootFK = bean.getRootFK();
					currentRootTableName = bean.getRootTableName();
					identifierClasses = new HashSet<Class>(); 
					identifierClasses.add(Changes.class); 
					identifierClasses.add(UserIdentifier.class); 
					changes = new Changes();
					// Composite key 
					String[] currentRootFKArray = bean.getRootFK().split(","); 
					String[] currentRootTableNameArray = bean.getRootTableName().split(","); 
					List<SimpleIdentifier> parentIDList = new ArrayList<SimpleIdentifier>(); 
					// get OID for parent
					for(int i = 0; i < currentRootFKArray.length ; i++)
					{
						SimpleIdentifier parentID = getIdentifierObject(Integer.parseInt(currentRootFKArray[i]),
								currentRootTableNameArray[i]); 
						identifierClasses.add(parentID.getClass()); 
						parentIDList.add(parentID); 
					}
					changes.setParentIdentifier(new ParentIdentifier(parentIDList)); 
				}

				pkList.add(bean.getMessagePK());
				Change change = new Change();
				change.setAction(bean.getAction());
				
				if(!currentTableName.equalsIgnoreCase(bean.getTableName()) || currentTablePK != bean.getTablePK() )
				{
						childIdentifier = getIdentifierObject(bean.getTablePK(), bean.getTableName());
						currentTableName = bean.getTableName();
						currentTablePK = bean.getTablePK();
						identifierClasses.add(childIdentifier.getClass()); 
				}
				
				change.setIdentifier(childIdentifier);
				change.setModule(bean.getModule());
				change.setTimeStamp(bean.getCreatedOn()); 
				if(changes.getChange().contains(change))
				{
					
				}else{
					change.setAdditionalInformation(dao.getAddtionalInformation(bean.getMessagePK())); 
					changes.getChange().add(change); 
					
				}
			
		}

		// process last Changes Message

			if (changes != null)
				processMessage(pkList, changes, moduleGroupPublisherMap.get(currentRootTableName), identifierClasses.toArray(array), threadList);
			
			
			Iterator<Thread> itr = threadList.keySet().iterator(); 		
			int numberOfSuccess = 0; 
			int numberOfFailure = 0; 
			while(itr.hasNext())
			{
				Thread t = itr.next(); 
			//wait for all the threads to complete 
				try {
					t.join();
					MessageSender sender = threadList.get(t); 
					if(sender.isMessageSentSuccessfully())
					{
						numberOfSuccess++;
					}else
					{
						numberOfFailure++; 
					}
				
				} catch (InterruptedException e) {
					logger.error("", e); 
				} 
			}
			//close all the publishers connection and session
			Iterator<String> connectionItr =  moduleGroupPublisherMap.keySet().iterator(); 
			
			while(connectionItr.hasNext())
			{
				MessagePublisher publisher = moduleGroupPublisherMap.get(connectionItr.next()); 
				publisher.closeSession(); 
				publisher.closeConnection(); 
			}
			
			logger.info("Number of messages sent in this Batch : " + threadList.size() + " Total success : "+ numberOfSuccess + " Total Failed: " + numberOfFailure);
		
		} catch (Exception e) {
			logger.error("Failed to run OutBoundMessaging Batch for account " + accountNumber + " items will be processed in next Batch"); 
			e.printStackTrace(); 
		}finally
		{
			
		}
	}

	/**
	 * This method creates separate Thread for sending message to Topic and updating Database entries related to that message. 
	 * @param messageIdentifier - list the identifier objects needed for JAXB context to bound
	 * @param message - instance of {@link com.velos.services.outbound.Changes} which represents the message need to put on Topic
	 * @param publisher - instance of {@link com.velos.services.outbound.MessagePublisher} which handles the process of putting message on specific Topic
	 * @param classesToLoad - list the identifier objects needed for JAXB context to bound
	 * @param threadMap
	 */
	private static void processMessage(List<Integer> messageIdentifier,
			Changes message, MessagePublisher publisher, Class[] classesToLoad, Map<Thread, MessageSender> threadMap) {

		// Start New thread for each Changes Message
		MessageSender sender = new MessageSender(message,messageIdentifier, publisher, classesToLoad); 
		Thread thread = new Thread(sender); 
		thread.start(); 
		
		threadMap.put(thread, sender); 

	}

	/**
	 * Returns the {@link com.velos.services.model.SimpleIdentifier} object for given tablePK and tableName
	 * @param tablePK
	 * @param tableName
	 * @return SimpleIdentifier
	 */
	public static SimpleIdentifier getIdentifierObject(int tablePK,
			String tableName) {
			ObjectMapService objectMapService = ServicesUtil.getObjectMapService();
			ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(
				tableName, tablePK);
			
			// Get Identifier for Study
		if (tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY)) {

			StudyIdentifier studyID = new StudyIdentifier();
			studyID.setOID(objectMap.getOID());
			// TODO get Study Number for ID
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();			
			studyID.setStudyNumber(helper.getStudyNumber(tablePK));
			return studyID;
		}
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_USER))
		{
			UserIdentifier userIdentifier = new UserIdentifier(); 
			userIdentifier.setOID(objectMap.getOID()); 
			MessagingIdentifierHelperDAO helper = new MessagingIdentifierHelperDAO();
			Map<String, String> userMap = helper.getUserInformation(tablePK); 
			
			userIdentifier.setFirstName(userMap.get("firstname")); 
			userIdentifier.setLastName(userMap.get("lastname")); 
			userIdentifier.setUserLoginName(userMap.get("logname")); 
			
			return userIdentifier; 
			
		}
		
		// Get Identifier for study status
		if (tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_STATUS)) {
			StudyStatusIdentifier statusIdentifier = new StudyStatusIdentifier();
			statusIdentifier.setOID(objectMap.getOID());
			return statusIdentifier;
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION))
		{
			OrganizationIdentifier oid = new OrganizationIdentifier(); 
		//	ogid.setOID(objectMap.getOID()); 
			//ogid.setSiteName(""); 
		}
		//Get Identifier for Study Calendar
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR))
		{
			CalendarIdentifier calendarIdentifier = new CalendarIdentifier(); 
			calendarIdentifier.setOID(objectMap.getOID()); 
			return calendarIdentifier; 
		}
		//Get Identifier for Calendar Status
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_CALENDAR_STATUS))
		{
			CalendarStatusIdentifier calendarStatusIdentifier = new CalendarStatusIdentifier(); 
			calendarStatusIdentifier.setOID(objectMap.getOID()); 
			return calendarStatusIdentifier; 
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_TEAM))
		{
			StudyTeamIdentifier studyTeamIdentifier = new StudyTeamIdentifier(); 
			studyTeamIdentifier.setOID(objectMap.getOID()); 
			return studyTeamIdentifier; 
			
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_MORESTUDY_DETAILS))
		{
			MoreStudyDetailsIdentifier moreStudyDetailsIdentifier = new MoreStudyDetailsIdentifier(); 
			moreStudyDetailsIdentifier.setOID(objectMap.getOID()); 
			return moreStudyDetailsIdentifier; 
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_BUDGET))
		{
			BudgetIdentifier budgetIdentifier = new BudgetIdentifier(); 
			budgetIdentifier.setOID(objectMap.getOID()); 
			return budgetIdentifier; 
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE))
		{
			PatientFormResponseIdentifier patientFormResponseIdentifier = new PatientFormResponseIdentifier(); 
			patientFormResponseIdentifier.setOID(objectMap.getOID());
			return patientFormResponseIdentifier;
			
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_STUDY_FORM_RESPONSE))
		{
			StudyFormResponseIdentifier studyFormResponseIdentifier=new StudyFormResponseIdentifier();
			studyFormResponseIdentifier.setOID(objectMap.getOID());
			return studyFormResponseIdentifier;
			
		}
		
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_ACCOUNT_FORM_RESPONSE))
		{
			AccountFormResponseIdentifier accountFormResponseIdentifier=new AccountFormResponseIdentifier();
			accountFormResponseIdentifier.setOID(objectMap.getOID());
			return accountFormResponseIdentifier;
			
		}
	
		if(tableName.equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY))
		{
			FormIdentifier formIdentifier=new FormIdentifier();
			formIdentifier.setOID(objectMap.getOID()); 
			return formIdentifier;
			
		}
		
		SimpleIdentifier simpleIdentifier = new SimpleIdentifier(); 
		simpleIdentifier.setOID(objectMap.getOID()); 
		return simpleIdentifier;
		
	}

}
