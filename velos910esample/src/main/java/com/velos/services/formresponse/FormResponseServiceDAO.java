/**
 * Created On Jan 16, 2012
 */
package com.velos.services.formresponse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.services.OperationException;
import com.velos.services.form.FormDesignDAO;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormField;
import com.velos.services.model.FormFieldIdentifier;
import com.velos.services.model.FormFieldResponse;
import com.velos.services.model.FormFieldResponses;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormSection;
import com.velos.services.model.FormSections;
import com.velos.services.model.FormType;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientFormResponse;
import com.velos.services.model.PatientFormResponseIdentifier;
import com.velos.services.model.PatientFormResponses;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.StudyPatientFormResponseIdentifier;
import com.velos.services.model.LinkedFormDesign.DisplayType;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.ScheduleEventIdentifier;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientFormResponse;
import com.velos.services.model.StudyPatientFormResponses;
import com.velos.services.model.StudyPatientScheduleFormResponse;
import com.velos.services.util.CodeCache;

/**
 * @author Kanwaldeep
 *
 */
public class FormResponseServiceDAO extends CommonDAO{
	
	
	private static Logger logger = Logger.getLogger(FormResponseServiceDAO.class); 
	
	private Integer formLibPK; 
	
	private Integer studyPK; 
	
	private Integer personPK; 
	
	private Integer patprotPK; 
	
	private Integer schEventPK; 
	
	public Integer getFormResponseCount(Integer formLibPK, Integer personPK, Integer schEventID)
	{
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Integer count = 0; 
		
		try
		{
			StringBuffer sql = new StringBuffer("select count(*) from er_patforms where fk_formlib = ? and FK_PER = ? and RECORD_TYPE <> 'D'");
			if(schEventID != null && schEventID > 0) sql.append(" and fk_sch_events1 = ? "); 
			conn = getConnection(); 			
			stmt = conn.prepareStatement(sql.toString()); 
			stmt.setInt(1, formLibPK); 
			stmt.setInt(2, personPK); 
			if(schEventID != null && schEventID > 0) stmt.setInt(3, schEventID); 
			
			rs = stmt.executeQuery(); 
			
			if(rs.next()){
				
				count = rs.getInt(1); 
			}			
		}catch(SQLException sqe)
		{
			logger.error(sqe.getStackTrace());  
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}
		
		return count; 
		
	}
	
	
	public Integer getStudyFormResponseCount(Integer formLibPK, Integer studyPK)
	{
		Connection conn = null; 
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Integer count = 0; 
		
		try
		{
			StringBuffer sql = new StringBuffer("select count(*) from er_studyforms where fk_formlib = ? and RECORD_TYPE <> 'D' and fk_study = ? ");
		
			conn = getConnection(); 			
			stmt = conn.prepareStatement(sql.toString()); 
			stmt.setInt(1, formLibPK); 
			stmt.setInt(2, studyPK); 
						
			rs = stmt.executeQuery(); 
			
			if(rs.next()){
				
				count = rs.getInt(1); 
			}			
		}catch(SQLException sqe)
		{
			logger.error(sqe.getStackTrace());  
		}finally
		{
			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
		}
		
		return count; 
		
	}
	
    public String getFormResponseSQL(int filledFormID, int loggedInUserID, DisplayType formType,ArrayList formColumns, ArrayList formColumnsDataTypes,ArrayList formColumnsSystemIDs)
	{
    	Calendar formFillDtCal = Calendar.getInstance();
    	
    	int schIndex = -1;
    	String linkFrom = formType.toString(); 
     	String v_sch_eventpk = "";
    	int v_index_velsep = -1;
    	String v_patprot = "";
    
    	
    	String studyNumberSQL= " (select s1.study_number  from er_study s1 where s1.pk_study="+studyPK+" ) as studyNumber ";
       	String patientSiteSQL = "(select site_name from epat.person, eres.er_site  where pk_site = fk_site and pk_person = "+personPK+") as sitecode";
     	String patientIDSQL = " (select person_code from person where pk_person = "+personPK+") as personCode"; 
       	//String studyNumberSQLForPat = "( SELECT s1.study_number FROM ER_STUDY s1 WHERE s1.pk_study=(SELECT pp1.fk_study FROM ER_PATPROT pp1 WHERE pp1.pk_patprot= fk_patprot))  AS studyNumber "; //not sure why we need it
    	String additionalFields= "";
    
    	String respAccessRightWhereClause = " and ( pkg_filledform.fn_getUserAccess (fk_form," + loggedInUserID +",creator) > 0 )"	;
    	
    
    	//String orderByColumnName = "";
        String browserColName= "";
        String orderByColumnDesc = "";
        //KM-#5942
        String orderByDateDesc = "";
        
        String colDisplayName = "";
        StringBuffer sbColumnList = new StringBuffer();
        String colDataType = "";
        int vbrowsercount = 0;
        
        String formLinkCol = "";
        String responseId = "";
        
 
    	
    	StringBuffer whereClause=new StringBuffer();
    
    	StringBuffer selectSQL= new StringBuffer();
        
    	StringBuffer sbSQL= new StringBuffer();
    	
    	String versionPKColumnSQL = "";
    	//append patient/study/account ID
    	
    	//append  filled form ID
    	
    //	whereClause.append( " Where fk_form = " + formLibPK);
    	whereClause.append(" where fk_filledform = " + filledFormID); 
    	//append ID - module info 
  //  	whereClause.append( " and id = " + p_id);
    	
    	//append form response access SQL
    	whereClause.append(respAccessRightWhereClause);
 
    	schIndex = linkFrom.indexOf("[VELSCHPK]"); 
    	
    	if (schIndex > 0)
    	{
    		v_sch_eventpk = linkFrom.substring(schIndex+10);
    		linkFrom = linkFrom.substring(0, schIndex);
    		
    	}
        
    	v_index_velsep = linkFrom.indexOf("[VELSEP]");
    	
    	if (v_index_velsep > 0)
    	{
    		v_patprot = linkFrom.substring(v_index_velsep+8);
    		linkFrom = linkFrom.substring(0, v_index_velsep);
    		
    	}
         
    if (linkFrom.equals("A")) 
    {
        additionalFields = "'' as studyNumber, ''  as personCode," ;
       // versionPKColumnSQL = getAccountFormsVersionPKSQL();
        whereClause.append(" and form_type = 'A'"); 
    }
    
    else if(linkFrom.equals("PA"))
    {
    	additionalFields = patientIDSQL+ ","+ patientSiteSQL +",";
    	whereClause.append(" and form_type = 'P'");
    }	
    else if (linkFrom.equals("S") || linkFrom.equals("SA"))
    {
    	additionalFields =  studyNumberSQL + " , ''  as personCode," ;
   // 	versionPKColumnSQL = getStudyFormsVersionPKSQL();
    	  whereClause.append(" and form_type = 'S'"); 
    }
    else if (linkFrom.equals("SP") )
    {
    	additionalFields =  studyNumberSQL + ", " + patientIDSQL + ", ";

    	  	
    	if (patprotPK != null)
    	{
    		whereClause.append( " and fk_patprot in (select pk_patprot from er_patprot pp where pp.fk_study = "+studyPK+" and " +
    				" pp.fk_per = "+ personPK+")"); 
    	}
    	else
    	{
    		whereClause.append( " and fk_patprot is not null"); 

    	}
    	
    	  whereClause.append(" and form_type = 'P'"); 
    }
     
    if (linkFrom.equals("SP") || linkFrom.equals("PR") || linkFrom.equals("PA") || linkFrom.equals("PS"))
    {
    	if (! StringUtil.isEmpty(v_sch_eventpk))
    	{
    		whereClause.append( " and fk_sch_events1 = " + v_sch_eventpk);
    	}
    }
    
    if (linkFrom.equals("PR") || linkFrom.equals("PA") )
    {
    	whereClause.append( " and ( ( fk_patprot is null and exists (select * from er_linkedforms f where " +
    			"f.fk_formlib = fk_form and nvl(LF_DISPLAY_INPAT,0)=1 ) ) or exists ( select * from er_linkedforms f where " +
    			"f.fk_formlib = fk_form and nvl(LF_DISPLAY_INPAT,0)=0 ) ) " );
    	
    }

    //Prepare Select String 
    
     
    // check formLinkCol 
    
    if (! StringUtil.isEmpty(formLinkCol))
    {
    	whereClause.append( " and EXISTS (SELECT * FROM ER_FORMSLINEAR l WHERE l.fk_form="+ formLibPK +
    			"  AND l.fk_filledform=ot.fk_filledform AND " + formLinkCol.trim() +"="+responseId +") "); 
    	
    }
    
//    if ( !v_dt_whereclause.equals("ALL")) {
//    	
//    	whereClause.append( " and filldate  " + EJBUtil.getRelativeTime(formFillDtCal, v_dt_whereclause) + " ");
//     
//    }
 
     // iterate through the loaded SaveFormDao object to get the details of browser columns
     String systemID; 
    
          
     if (formColumns != null)
     {
	     for (int k = 0; k< formColumns.size() ; k++)
	    	 {
	    	 	vbrowsercount = vbrowsercount+1;
	    	 	
	    	 	browserColName = (String)formColumns.get(k);

	    	 	colDataType  = (String)formColumnsDataTypes.get(k); 
	    	 	
	    	 	systemID = (String) formColumnsSystemIDs.get(k); 
	    	 	
	    	 	
	    	 	if (colDataType.equals("MD") || colDataType.equals("MC") || colDataType.equals("MR"))
    	 		{
	    	 		sbColumnList.append("F_GET_MCFIELDVAL ( "+  browserColName + " ) "); //add column with header name
    	 		}
	    	 	else
	    	 	{
	    	 		sbColumnList.append( browserColName ); //add column with header name
	    	 	}
	    	 	
	    	 	sbColumnList.append(" as "+systemID+" , "); //one comma added for data colum name before
	    	 	
	    	 	
	    	
	    	 	
	    	      	 	
	    	 	
	    	 }
	   }   
	 
     selectSQL.append(" SELECT fk_form,"+versionPKColumnSQL);
    
    
     selectSQL.append(" form_version, filldate hidden_filldate, pk_formslinear as rowcount, id hidden_id,");
     
     selectSQL.append(additionalFields);
   
     if (! StringUtil.isEmpty(sbColumnList.toString()))
     {
    	 selectSQL.append(sbColumnList.toString());
    	 
     }
   
     selectSQL.append(" FORM_COMPLETED");
     
     
     
     //From Clause 
     selectSQL.append(" from er_formslinear ot ");
     
     
     //append select clause to main SQL
     sbSQL.append(selectSQL.toString());
     
     //Where clause
     sbSQL.append(whereClause.toString());
  
//     else
//     {
//    	 orderByColumnDesc  = orderByColumnDesc  +  p_ordertype;
//    	 
//    	 //KM-#5942
//    	 if (!StringUtil.isEmpty(orderByDateDesc))
//    	 {
//    		orderByColumnDesc = orderByColumnDesc + orderByDateDesc + p_ordertype; 
//    	 }
//     }
//     
//     sbSQL.append(" Order By "+  orderByColumnDesc );
     
       
     return sbSQL.toString();   
    	
    	
	}
    
    
    
    public void populateStudyPatFormID(Integer filledFormID)
    {
    	Integer formLibID = 0; 
    	PreparedStatement stmt = null ; 
    	ResultSet rs = null; 
    	Connection conn = null; 
    	
    	try
    	{
    		String sql = "select a.fk_formlib, a.fk_per, a.fk_patprot, b.fk_study, a.fk_sch_events1 from er_patforms a, er_patprot b where pk_patforms = ? and a.fk_patprot = b.PK_PATPROT"; 
    		
    		conn = getConnection(); 
    		
    		stmt = conn.prepareStatement(sql); 
    		stmt.setInt(1, filledFormID); 
    		
    	   rs = stmt.executeQuery(); 
    	   
    	   while(rs.next())
    	   {
    		   formLibPK = rs.getInt(1); 
    		   personPK = rs.getInt(2); 
    		   patprotPK = rs.getInt(3); 
    		   studyPK = rs.getInt(4); 
    		   schEventPK = rs.getInt(5); 
    		   
    	   }
    		
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe.getStackTrace()); 
    	}finally
    	{

			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    }
    
    
    /**Call populateStudyPatFormID first to populate values needed for this method
     * @param filledFormID
     * @param loggedInUserID
     * @param objectMapService
     * @param studyPatientFormDesign
     * @return
     * @throws OperationException
     */
    public StudyPatientFormResponse getStudyPatientFormResponse(Integer filledFormID, UserBean loggedInUserID, ObjectMapService objectMapService, StudyPatientFormDesign studyPatientFormDesign) throws OperationException
    {
    	StudyPatientFormResponse formResponse = new StudyPatientFormResponse(); 
    	
    //	getStudyPatFormID(filledFormID); 
    	FormFieldResponses formFieldResponses = new FormFieldResponses();
    	Connection conn = null; 
    	PreparedStatement stmt = null; 
    	ResultSet rs = null ;
    	ArrayList formColumns = new ArrayList();
    	ArrayList formColumnsDataTypes = new ArrayList();
    	ArrayList formColumnsSystemIDs = new ArrayList();
    	SaveFormDao sfDao = new SaveFormDao(); 
    	sfDao.setFormId(formLibPK); 
        sfDao.getFormDetails(); 
        
//        StudyPatientFormDesign studyPatientFormDesign  = new StudyPatientFormDesign(); 
//              
//        FormDesignDAO  formDesignDAO = new FormDesignDAO(); 
//        try {
//			formDesignDAO.getStudyPatientFormDesign(formLibPK, loggedInUserID, objectMapService, studyPatientFormDesign, studyPK, false);
//		} catch (OperationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
        

		FormSections formSections = studyPatientFormDesign.getSections(); 
		
		List<FormSection> formSectionsList = formSections.getSection(); 
		List<FormField> formfieldList = new ArrayList<FormField>(); 
		
		for(FormSection formSection: formSectionsList)
		{
			formfieldList.addAll(formSection.getFields().getField()); 		
		}

		//Map of SystemIDs 
		Map<String, FormField> formFieldValueMap = new HashMap<String, FormField>();
		
		for(FormField formField: formfieldList)
		{
			formFieldValueMap.put(formField.getSystemID(), formField); 
		}
					
        formColumns = sfDao.getColNames();
        formColumnsDataTypes = sfDao.getColTypes();
        formColumnsSystemIDs = sfDao.getColSysIds(); 
    	
    	
    	try{
    		
    		conn = getConnection(); 
    		stmt = conn.prepareStatement(getFormResponseSQL(filledFormID, loggedInUserID.getUserId(), DisplayType.PATIENT_ENROLLED_TO_SPECIFIC_STUDY, formColumns, formColumnsDataTypes, formColumnsSystemIDs)); 
    		
    		rs = stmt.executeQuery(); 
    		
    		if(rs.next())
    		{

    			formResponse.setFormFillDt(rs.getDate("hidden_filldate"));

    			FormIdentifier formIdentifier  = new FormIdentifier(); 
    			ObjectMap formObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, formLibPK); 
    			formIdentifier.setOID(formObjectMap.getOID()); 
    			formResponse.setFormId(formIdentifier); 

    			CodeCache codeCache = CodeCache.getInstance(); 
    			Code formStatus  = 	codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_FILLED_FORM_STATUS,rs.getString("FORM_COMPLETED"),EJBUtil.stringToInteger(loggedInUserID.getUserAccountId()));
    			formResponse.setFormStatus(formStatus); 
    			formResponse.setFormVersion(rs.getInt("form_version")); 

    			PatientIdentifier patientIdentifier = new PatientIdentifier(); 
    			ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPK); 
    			patientIdentifier.setOID(objectMap.getOID());
    			patientIdentifier.setPatientId(rs.getString("personCode")); 
    			formResponse.setPatientIdentifier(patientIdentifier); 

    			PatientProtocolIdentifier patProtID = new PatientProtocolIdentifier(); 
    			ObjectMap objectMap2 = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATPROT, patprotPK); 
    			patProtID.setOID(objectMap2.getOID()); 
    			formResponse.setPatientProtocolIdentifier(patProtID); 

    			StudyIdentifier studyID = new StudyIdentifier(); 
    			ObjectMap studyMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
    			studyID.setOID(studyMap.getOID()); 
    			studyID.setStudyNumber(rs.getString("studyNumber")); 
    			formResponse.setStudyIdentifier(studyID); 

    			Iterator formColSysIDItr = formColumnsSystemIDs.iterator(); 
    			while(formColSysIDItr.hasNext())
    			{
    				String systemID = formColSysIDItr.next().toString(); 
    				FormFieldResponse formFieldResponse = new FormFieldResponse(); 
    				formFieldResponse.setFieldIdentifier(formFieldValueMap.get(systemID).getFieldIdentifier()); 
    				formFieldResponse.setValue(rs.getString(systemID)== null?"":rs.getString(systemID));  
    				formFieldResponses.addField(formFieldResponse); 
    			}

    		}else{
    			throw new OperationException(); 
    		}
    			
    			
//    			
//    		}
    		
    		formResponse.setFormFieldResponses(formFieldResponses); 
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe); 
    		sqe.printStackTrace(); 
    	}
    	
    	return formResponse;     	
    	
    }
    
    /**Call populateStudyPatFormID first to populate values needed for this method. 
     * @param filledFormID
     * @param loggedInUserID
     * @param objectMapService
     * @param studyPatientFormDesign
     * @return
     * @throws OperationException
     */
    public StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse(Integer filledFormID,
    		UserBean loggedInUserID, 
    		ObjectMapService objectMapService, 
    		StudyPatientFormDesign studyPatientFormDesign)
    throws OperationException{
    	
    	StudyPatientScheduleFormResponse studyPatientScheduleFormResponse = new StudyPatientScheduleFormResponse(); 
    	
    	populateDataforResponse(studyPatientScheduleFormResponse, filledFormID, loggedInUserID, objectMapService, studyPatientFormDesign); 
    	
    	return studyPatientScheduleFormResponse; 
    }
    
    
    private void populateDataforResponse(StudyPatientFormResponse formResponse,
    		Integer filledFormID, 
    		UserBean loggedInUserID, 
    		ObjectMapService objectMapService, 
    		StudyPatientFormDesign studyPatientFormDesign) throws OperationException{
    	
    	FormFieldResponses formFieldResponses = new FormFieldResponses();
    	Connection conn = null; 
    	PreparedStatement stmt = null; 
    	ResultSet rs = null ;
    	ArrayList formColumns = new ArrayList();
    	ArrayList formColumnsDataTypes = new ArrayList();
    	ArrayList formColumnsSystemIDs = new ArrayList();
    	SaveFormDao sfDao = new SaveFormDao(); 
    	sfDao.setFormId(formLibPK); 
        sfDao.getFormDetails(); 

		FormSections formSections = studyPatientFormDesign.getSections(); 
		
		List<FormSection> formSectionsList = formSections.getSection(); 
		List<FormField> formfieldList = new ArrayList<FormField>(); 
		
		for(FormSection formSection: formSectionsList)
		{
			formfieldList.addAll(formSection.getFields().getField()); 		
		}

		//Map of SystemIDs 
		Map<String, FormField> formFieldValueMap = new HashMap<String, FormField>();
		
		for(FormField formField: formfieldList)
		{
			formFieldValueMap.put(formField.getSystemID(), formField); 
		}
					
        formColumns = sfDao.getColNames();
        formColumnsDataTypes = sfDao.getColTypes();
        formColumnsSystemIDs = sfDao.getColSysIds(); 
        
        
    	
    	
    	try{
    		
    		conn = getConnection(); 
    		stmt = conn.prepareStatement(getFormResponseSQL(filledFormID, loggedInUserID.getUserId(), DisplayType.PATIENT_ENROLLED_TO_SPECIFIC_STUDY, formColumns, formColumnsDataTypes, formColumnsSystemIDs)); 
    		
    		rs = stmt.executeQuery(); 
    		
    		if(rs.next())
    		{

    			formResponse.setFormFillDt(rs.getDate("hidden_filldate"));

    			FormIdentifier formIdentifier  = new FormIdentifier(); 
    			ObjectMap formObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, formLibPK); 
    			formIdentifier.setOID(formObjectMap.getOID()); 
    			formResponse.setFormId(formIdentifier); 

    			CodeCache codeCache = CodeCache.getInstance(); 
    			Code formStatus  = 	codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_FILLED_FORM_STATUS,rs.getString("FORM_COMPLETED"),EJBUtil.stringToInteger(loggedInUserID.getUserAccountId()));
    			formResponse.setFormStatus(formStatus); 
    			formResponse.setFormVersion(rs.getInt("form_version")); 

    			PatientIdentifier patientIdentifier = new PatientIdentifier(); 
    			ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPK); 
    			patientIdentifier.setPatientId(rs.getString("personCode")); 
    			patientIdentifier.setOID(objectMap.getOID());     			
    			formResponse.setPatientIdentifier(patientIdentifier); 

    			PatientProtocolIdentifier patProtID = new PatientProtocolIdentifier(); 
    			ObjectMap objectMap2 = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATPROT, patprotPK); 
    			patProtID.setOID(objectMap2.getOID()); 
    			formResponse.setPatientProtocolIdentifier(patProtID); 

    			StudyIdentifier studyID = new StudyIdentifier(); 
    			ObjectMap studyMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
    			studyID.setOID(studyMap.getOID()); 
    			studyID.setStudyNumber(rs.getString("studyNumber")); 
    			formResponse.setStudyIdentifier(studyID); 
    			
    			if(formResponse instanceof StudyPatientScheduleFormResponse)
    			{
    				
    				ScheduleEventIdentifier scheduleEventIdentifier = new ScheduleEventIdentifier(); 
    				ObjectMap scheduleEventMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT, schEventPK); 
    				scheduleEventIdentifier.setOID(scheduleEventMap.getOID()); 
    				((StudyPatientScheduleFormResponse) formResponse).setScheduleEventIdentifier(scheduleEventIdentifier); 
    			}

    			Iterator formColSysIDItr = formColumnsSystemIDs.iterator(); 
    			while(formColSysIDItr.hasNext())
    			{
    				String systemID = formColSysIDItr.next().toString(); 
    				FormFieldResponse formFieldResponse = new FormFieldResponse(); 
    				formFieldResponse.setFieldIdentifier(formFieldValueMap.get(systemID).getFieldIdentifier()); 
    				formFieldResponse.setValue(rs.getString(systemID)== null?"":rs.getString(systemID));  
    				formFieldResponses.addField(formFieldResponse); 
    			}

    		}else{
    			throw new OperationException(); 
    		}
    			
    			
//    			
//    		}
    		
    		formResponse.setFormFieldResponses(formFieldResponses); 
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe); 
    		sqe.printStackTrace(); 
    	}
    	
    	//return formResponse;     	
    	
    }
    
    
    
    public Integer getPatProtIDforFilledForm()
    {
    	return patprotPK; 
    }
    
    public Integer getPerIDForFilledForm()
    {
    	return personPK; 
    }
    
    public Integer getStudyPKForFilledForm()
    {
    	return studyPK; 
    }
    
    public Integer getFormLibIDforFilledForm()
    {
    	return formLibPK; 
    }

	public Integer getSchEventforFilledForm() {
		
		return schEventPK;
	}
    
    public boolean isStudyPatientFormResponseDeleted(Integer formResponseIdentifier)
    {
    	boolean formResponseDeleted = false; 
    	PreparedStatement stmt = null; 
    	Connection conn = null; 
    	ResultSet rs = null ;
    	try
    	{
    		conn = getConnection(); 
    		stmt = conn.prepareStatement("select RECORD_TYPE from er_patforms where pk_patforms = ?"); 
    		
    		stmt.setInt(1, formResponseIdentifier); 
    		rs = stmt.executeQuery(); 
    		
    		while(rs.next())
    		{
    			if(rs.getString(1).equals("D")) formResponseDeleted = true; 
    			break; 
    		}    		
    		
    	}catch(SQLException sqe)
    	{
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return formResponseDeleted; 
    }
    
    public boolean isStudyFormResponseDeleted(Integer formResponseIdentifier)
    {
    	boolean formResponseDeleted = false; 
    	PreparedStatement stmt = null; 
    	Connection conn = null; 
    	ResultSet rs = null ;
    	try
    	{
    		conn = getConnection(); 
    		stmt = conn.prepareStatement("select RECORD_TYPE from er_studyforms where pk_studyforms = ?"); 
    		
    		stmt.setInt(1, formResponseIdentifier); 
    		rs = stmt.executeQuery(); 
    		
    		while(rs.next())
    		{
    			if(rs.getString(1).equals("D")) formResponseDeleted = true; 
    			break; 
    		}    		
    		
    	}catch(SQLException sqe)
    	{
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return formResponseDeleted; 
    }
    
    
    public Integer getActivePatientScheduleEventID(Integer calendarEventID, Integer patProtID){
    	Integer schEventID = null; 
    	PreparedStatement stmt = null; 
    	Connection conn = null; 
    	ResultSet rs = null ;
    	try
    	{
    		conn = getConnection(); 
    		stmt = conn.prepareStatement("select event_id from sch_events1 where fk_assoc = ?  and fk_patprot = ?"); 
    	//and status = ?
    		stmt.setInt(1, calendarEventID); 
    	//	stmt.setInt(3, 0);
    		stmt.setInt(2, patProtID);
    		rs = stmt.executeQuery(); 
    		
    		while(rs.next())
    		{
    			schEventID = rs.getInt(1); 
    		 
    		}    		
    		
    	}catch(SQLException sqe)
    	{
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return schEventID; 
    	
    }
    
    
    public Integer getCalendarEventID(Integer scheduleEventID)
    {
    	
    	String sql = "select fk_assoc, fk_patprot from sch_events1 where event_id = ?"; 
    	PreparedStatement stmt = null; 
    	Connection conn = null; 
    	ResultSet rs = null ; 
    	Integer calendarEventID = null; 
    	try
    	{
    		conn = getConnection(); 
    		stmt = conn.prepareStatement(sql); 
    		stmt.setInt(1, scheduleEventID); 
    		rs = stmt.executeQuery(); 
    		if(rs.next())
    		{
    			calendarEventID = rs.getInt("fk_assoc"); 
    			patprotPK = rs.getInt("fk_patprot"); 
    		}
    	}catch(SQLException sqe)
    	{
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return calendarEventID; 
    	
    }
    
    public List<Integer> getEventLinkedForms(Integer eventID){
    	List<Integer> linkedForms = new ArrayList<Integer>(); 
    	PreparedStatement stmt = null; 
    	Connection conn = null; 
    	ResultSet rs = null ;
    	try
    	{
    		conn = getConnection(); 
    		stmt = conn.prepareStatement("select fk_form from esch.sch_event_crf where fk_event = ?"); 
    		
    		stmt.setInt(1, eventID); 
    		
    		rs = stmt.executeQuery(); 
    		
    		while(rs.next())
    		{
    			linkedForms.add(rs.getInt(1)); 
    		 
    		}    		
    		
    	}catch(SQLException sqe)
    	{
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return linkedForms; 
    	
    }
    
    
    public boolean isStudyPatientOnScheduleEvent(Integer schEventID, Integer studyID, Integer patientID)
    {
    	boolean studyPatientOnScheduleEvent = false; 
    	PreparedStatement stmt = null; 
    	Connection conn = null; 
    	ResultSet rs = null ;
    	try
    	{
    		conn = getConnection(); 
    		stmt = conn.prepareStatement("select a.event_id from sch_events1 a, er_patprot b where a.fk_patprot = b.pk_patprot and a.event_id = ? and b.fk_study = ? and fk_per = ?"); 
    		
    		stmt.setInt(1, schEventID); 
    		stmt.setInt(2, studyID); 
    		stmt.setInt(3, patientID); 
    		
    		rs = stmt.executeQuery(); 
    		
    		if(rs.next())
    		{
    			studyPatientOnScheduleEvent = true; 
    		}    		
    		
    	}catch(SQLException sqe)
    	{
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return studyPatientOnScheduleEvent; 
    }

    public StudyFormResponse getStudyFormResponse(Integer filledFormID, UserBean loggedInUserID, ObjectMapService objectMapService, StudyFormDesign studyFormDesign) throws OperationException
    {
    	StudyFormResponse formResponse = new StudyFormResponse();
    	FormFieldResponses formFieldResponses = new FormFieldResponses();
    	Connection conn = null; 
    	PreparedStatement stmt = null; 
    	ResultSet rs = null ;
    	ArrayList formColumns = new ArrayList();
    	ArrayList formColumnsDataTypes = new ArrayList();
    	ArrayList formColumnsSystemIDs = new ArrayList();
    	SaveFormDao sfDao = new SaveFormDao(); 
    	sfDao.setFormId(formLibPK); 
        sfDao.getFormDetails();         

		FormSections formSections = studyFormDesign.getSections(); 
		
		List<FormSection> formSectionsList = formSections.getSection(); 
		List<FormField> formfieldList = new ArrayList<FormField>(); 
		
		for(FormSection formSection: formSectionsList)
		{
			formfieldList.addAll(formSection.getFields().getField()); 		
		}

		//Map of SystemIDs 
		Map<String, FormField> formFieldValueMap = new HashMap<String, FormField>();
		
		for(FormField formField: formfieldList)
		{
			formFieldValueMap.put(formField.getSystemID(), formField); 
		}
					
        formColumns = sfDao.getColNames();
        formColumnsDataTypes = sfDao.getColTypes();
        formColumnsSystemIDs = sfDao.getColSysIds(); 
    	
    	
    	try{
    		
    		conn = getConnection(); 
    		stmt = conn.prepareStatement(getFormResponseSQL(filledFormID, loggedInUserID.getUserId(), DisplayType.STUDY, formColumns, formColumnsDataTypes, formColumnsSystemIDs)); 
    		
    		rs = stmt.executeQuery(); 
    		
    		if(rs.next())
    		{

    			formResponse.setFormFillDt(rs.getDate("hidden_filldate"));

    			FormIdentifier formIdentifier  = new FormIdentifier(); 
    			ObjectMap formObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, formLibPK); 
    			formIdentifier.setOID(formObjectMap.getOID()); 
    			formResponse.setFormId(formIdentifier); 

    			CodeCache codeCache = CodeCache.getInstance(); 
    			Code formStatus  = 	codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_FILLED_FORM_STATUS,rs.getString("FORM_COMPLETED"),EJBUtil.stringToInteger(loggedInUserID.getUserAccountId()));
    			formResponse.setFormStatus(formStatus); 
    			formResponse.setFormVersion(rs.getInt("form_version")); 

    			StudyIdentifier studyID = new StudyIdentifier(); 
    			ObjectMap studyMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
    			studyID.setOID(studyMap.getOID()); 
    			studyID.setStudyNumber(rs.getString("studyNumber")); 
    			formResponse.setStudyIdentifier(studyID); 

    			Iterator formColSysIDItr = formColumnsSystemIDs.iterator(); 
    			while(formColSysIDItr.hasNext())
    			{
    				String systemID = formColSysIDItr.next().toString(); 
    				FormFieldResponse formFieldResponse = new FormFieldResponse(); 
    				formFieldResponse.setFieldIdentifier(formFieldValueMap.get(systemID).getFieldIdentifier()); 
    				formFieldResponse.setValue(rs.getString(systemID)== null?"":rs.getString(systemID));  
    				formFieldResponses.addField(formFieldResponse); 
    			}

    		}else{
    			throw new OperationException(); 
    		}
    			
    			
//    			
//    		}
    		
    		formResponse.setFormFieldResponses(formFieldResponses); 
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe); 
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return formResponse;     	
    	
    }
    
    
    public void populateFormIDForStudyFilledFormID(Integer filledFormID)
    {
    	Integer formLibID = 0; 
    	PreparedStatement stmt = null ; 
    	ResultSet rs = null; 
    	Connection conn = null; 
    	
    	try
    	{
    		String sql = "select a.fk_formlib, a.fk_study from er_studyforms a where pk_studyforms = ?"; 
    		
    		conn = getConnection(); 
    		
    		stmt = conn.prepareStatement(sql); 
    		stmt.setInt(1, filledFormID); 
    		
    	   rs = stmt.executeQuery(); 
    	   
    	   while(rs.next())
    	   {
    		   formLibPK = rs.getInt(1);     		   
    		   studyPK = rs.getInt(2); 
    	    		   
    	   }
    		
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe.getStackTrace()); 
    	}finally
    	{

			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    }
    
    public StudyPatientFormResponses getListOfStudyPatientFormResponses(int personPk,int formPk,String studyPK, UserBean callingUser,ObjectMapService objectMapService,StudyPatientFormDesign studyPatientFormDesign,int pageNumber,int pageSize) throws OperationException
    {
    	SaveFormDao sfDao = new SaveFormDao(); 
    	sfDao.getFormResponseBrowserColumns(""+formPk);
    	ArrayList formColumns = new ArrayList();
    	ArrayList formColumnsSystemIDs = new ArrayList();
    	
    	formColumns = sfDao.getColNames();
        formColumnsSystemIDs = sfDao.getColSysIds(); 
        
        FormSections formSections = studyPatientFormDesign.getSections(); 
		
		List<FormSection> formSectionsList = formSections.getSection(); 
		List<FormField> formfieldList = new ArrayList<FormField>(); 
		
		for(FormSection formSection: formSectionsList)
		{
			formfieldList.addAll(formSection.getFields().getField()); 		
		}

		//Map of SystemIDs 
		Map<String, FormField> formFieldValueMap = new HashMap<String, FormField>();
		for(FormField formField: formfieldList)
		{
			if(formColumnsSystemIDs.contains(formField.getSystemID().toString()))
				{
					formFieldValueMap.put(formField.getSystemID(), formField); 
				}
		}
		StringBuffer sqlWithPagination=new StringBuffer();
    	StringBuffer sqlFormResponse=new StringBuffer();
    	
    	sqlFormResponse.append("SELECT fk_filledform, filldate,FORM_COMPLETED ");
    	for(int i=0;i<formColumns.size();i++)
    	{
    		sqlFormResponse.append(" ,"+formColumns.get(i) +" as "+formColumnsSystemIDs.get(i));
    	}
    	sqlFormResponse.append(" from er_formslinear ");
    	sqlFormResponse.append(" where fk_form ="+formPk+" and id = "+personPk+" and ( pkg_filledform.fn_getUserAccess (fk_form,"+callingUser.getUserId().intValue()+",creator) > 0 ) and fk_patprot in (select pk_patprot from er_patprot pp where pp.fk_study = "+studyPK+" and  pp.fk_per = "+personPk+") and form_type = 'P' order by filldate ");
    	
    	int uperLimit=pageSize*pageNumber;
    	int lowerLimit=(uperLimit-pageSize)+1;
    	
    	sqlWithPagination.append(" select * from (select x.*, rownum row_num from (" +
    			sqlFormResponse.toString() +
    			") x ) where row_num between "+lowerLimit+" and " + uperLimit);
    	if(logger.isDebugEnabled()){logger.debug("FormResponseServiceDAO.getListOfStudyPatientFormResponses sqlWithPagination generated is " +sqlWithPagination);}
    	
    	StudyPatientFormResponses studyPatientFormResponses=new StudyPatientFormResponses();
    	Connection conn = null; 
    	PreparedStatement stmt = null; 
    	ResultSet rs = null ;
    	try{
    		
    		StudyPatientFormResponse studyPatientFormResponse=null;
    		FormFieldResponses formFieldResponses=null;
    		StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier=null;
    		conn = getConnection(); 
    		stmt = conn.prepareStatement(sqlWithPagination.toString()); 
    		rs = stmt.executeQuery();
    		
    		while(rs.next())
    		{
    			studyPatientFormResponse=new StudyPatientFormResponse();
    			formFieldResponses=new FormFieldResponses();
    			
    			studyPatientFormResponse.setFormFillDt(rs.getDate("filldate"));
    			CodeCache codeCache = CodeCache.getInstance(); 
    			Code formStatus  = 	codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_FILLED_FORM_STATUS,rs.getString("FORM_COMPLETED"),StringUtil.stringToInteger(callingUser.getUserAccountId()));
    			studyPatientFormResponse.setFormStatus(formStatus);
    			
    			Iterator formColSysIDItr = formColumnsSystemIDs.iterator(); 
    			while(formColSysIDItr.hasNext())
    			{
    				String systemID = formColSysIDItr.next().toString(); 
    				FormFieldResponse formFieldResponse = new FormFieldResponse(); 
    				formFieldResponse.setFieldIdentifier(formFieldValueMap.get(systemID).getFieldIdentifier()); 
    				formFieldResponse.setValue(rs.getString(systemID)== null?"":rs.getString(systemID)); 
    				formFieldResponses.addField(formFieldResponse); 
    			}
    			studyPatientFormResponse.setFormFieldResponses(formFieldResponses);
    			
    			
    			ObjectMap formObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE, rs.getInt("FK_FILLEDFORM"));
    			studyPatientFormResponseIdentifier=new StudyPatientFormResponseIdentifier();
    			studyPatientFormResponseIdentifier.setOID(formObjectMap.getOID());
    			studyPatientFormResponse.setSystemID(studyPatientFormResponseIdentifier);
    			
    			studyPatientFormResponses.addStudyPatientFormResponse(studyPatientFormResponse); 
    			
    		}
    		
    		studyPatientFormResponses.setRecordCount(getFormResponseCount(formPk, personPk, 0));
    		
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe); 
    		sqe.printStackTrace(); 
    	}
		return studyPatientFormResponses;
    	
    }
    
    
    public PatientFormResponses getListOfPatientFormResponses(int personPk,int formPk, UserBean callingUser,ObjectMapService objectMapService,FormDesign formDesign,int pageNumber,int pageSize) 
    		throws OperationException
    {
    	SaveFormDao sfDao = new SaveFormDao(); 
    	sfDao.getFormResponseBrowserColumns(""+formPk);
    	ArrayList formColumns = new ArrayList();
    	ArrayList formColumnsSystemIDs = new ArrayList();
    	
    	formColumns = sfDao.getColNames();
        formColumnsSystemIDs = sfDao.getColSysIds(); 
        
        FormSections formSections = formDesign.getSections(); 
		List<FormSection> formSectionsList = formSections.getSection(); 
		List<FormField> formfieldList = new ArrayList<FormField>(); 
		
		for(FormSection formSection: formSectionsList)
		{
			formfieldList.addAll(formSection.getFields().getField()); 		
		}

		//Map of SystemIDs 
		Map<String, FormField> formFieldValueMap = new HashMap<String, FormField>();
		for(FormField formField: formfieldList)
		{
			if(formColumnsSystemIDs.contains(formField.getSystemID().toString()))
				{
					formFieldValueMap.put(formField.getSystemID(), formField); 
				}
		}
		StringBuffer sqlWithPagination=new StringBuffer();
    	StringBuffer sqlFormResponse=new StringBuffer();
    	
    	sqlFormResponse.append("SELECT fk_filledform, filldate,FORM_COMPLETED ");
    	for(int i=0;i<formColumns.size();i++)
    	{
    		sqlFormResponse.append(" ,"+formColumns.get(i) +" as "+formColumnsSystemIDs.get(i));
    	}
    	sqlFormResponse.append(" from er_formslinear ");
    	sqlFormResponse.append(" where fk_form ="+formPk+" and id = "+personPk+" and ( pkg_filledform.fn_getUserAccess (fk_form,"+callingUser.getUserId().intValue()+",creator) > 0 ) and form_type = 'P' order by filldate ");
    	
    	int uperLimit=pageSize*pageNumber;
    	int lowerLimit=(uperLimit-pageSize)+1;
    	
    	sqlWithPagination.append(" select * from (select x.*, rownum row_num from (" +
    			sqlFormResponse.toString() +
    			") x ) where row_num between "+lowerLimit+" and " + uperLimit);
    	if(logger.isDebugEnabled()){logger.debug("FormResponseServiceDAO.getListOfStudyPatientFormResponses sqlWithPagination generated is " +sqlWithPagination);}
    	PatientFormResponses patientFormResponses=new PatientFormResponses();
    	Connection conn = null; 
    	PreparedStatement stmt = null; 
    	ResultSet rs = null ;
    	try{
    		
    		PatientFormResponse patientFormResponse=null;
    		FormFieldResponses formFieldResponses=null;
    		PatientFormResponseIdentifier patientFormResponseIdentifier=null;
    		conn = getConnection(); 
    		stmt = conn.prepareStatement(sqlWithPagination.toString()); 
    		rs = stmt.executeQuery();
    		
    		while(rs.next())
    		{
    			patientFormResponse=new PatientFormResponse();
    			formFieldResponses=new FormFieldResponses();
    			
    			patientFormResponse.setFormFillDt(rs.getDate("filldate"));
    			CodeCache codeCache = CodeCache.getInstance(); 
    			Code formStatus  = 	codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_FILLED_FORM_STATUS,rs.getString("FORM_COMPLETED"),StringUtil.stringToInteger(callingUser.getUserAccountId()));
    			patientFormResponse.setFormStatus(formStatus);
    			
    			Iterator formColSysIDItr = formColumnsSystemIDs.iterator(); 
    			while(formColSysIDItr.hasNext())
    			{
    				String systemID = formColSysIDItr.next().toString(); 
    				FormFieldResponse formFieldResponse = new FormFieldResponse(); 
    				formFieldResponse.setFieldIdentifier(formFieldValueMap.get(systemID).getFieldIdentifier()); 
    				formFieldResponse.setValue(rs.getString(systemID)== null?"":rs.getString(systemID)); 
    				formFieldResponses.addField(formFieldResponse); 
    			}
    			patientFormResponse.setFormFieldResponses(formFieldResponses);
    			
    			
    			ObjectMap formObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PATIENT_FORM_RESPONSE, rs.getInt("FK_FILLEDFORM"));
    			patientFormResponseIdentifier=new PatientFormResponseIdentifier();
    			patientFormResponseIdentifier.setOID(formObjectMap.getOID());
    			patientFormResponse.setFormFilledFormId(patientFormResponseIdentifier);
    			
    			patientFormResponses.addPatientFormResponse(patientFormResponse); 
    			
    		}
    		
    		patientFormResponses.setRecordCount(getFormResponseCount(formPk, personPk, 0));
    		
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe); 
    		sqe.printStackTrace(); 
    	}
		return patientFormResponses;
    	
    }


	public boolean getPatientFormResponseDetails(Integer formResponseIdentifier) 
	{

    	boolean formResponseDeleted = false; 
    	PreparedStatement stmt = null; 
    	Connection conn = null; 
    	ResultSet rs = null ;
    	try
    	{
    		conn = getConnection(); 
    		stmt = conn.prepareStatement("select a.fk_formlib, a.fk_per,a.RECORD_TYPE from er_patforms a where pk_patforms = ?"); 
    		
    		stmt.setInt(1, formResponseIdentifier); 
    		rs = stmt.executeQuery(); 
    		
    		while(rs.next())
    		{
    			formLibPK = rs.getInt(1);     		   
     		    personPK = rs.getInt(2); 
    			if(rs.getString(3).equals("D")) formResponseDeleted = true; 
    			break; 
    		}    		
    		
    	}catch(SQLException sqe)
    	{
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	
    	return formResponseDeleted; 
    
	}


	/*public void populateFormIDForPatientFilledFormID1(Integer filledFormID) {

    	Integer formLibID = 0; 
    	PreparedStatement stmt = null ; 
    	ResultSet rs = null; 
    	Connection conn = null; 
    	
    	try
    	{
    		String sql = "select a.fk_formlib, a.fk_per from er_patforms a where pk_patforms = ?"; 
    		
    		conn = getConnection(); 
    		
    		stmt = conn.prepareStatement(sql); 
    		stmt.setInt(1, filledFormID); 
    		
    	   rs = stmt.executeQuery(); 
    	   
    	   while(rs.next())
    	   {
    		   formLibPK = rs.getInt(1);     		   
    		   personPK = rs.getInt(2); 
    	    		   
    	   }
    		
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe.getStackTrace()); 
    	}finally
    	{

			if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    
		
	}*/
 
	public PatientFormResponse getPatientFormResponse(Integer filledFormID,UserBean loggedInUserID, ObjectMapService objectMapService,
			LinkedFormDesign linkedFormDesign) throws OperationException
	{

		PatientFormResponse formResponse = new PatientFormResponse();
    	FormFieldResponses formFieldResponses = new FormFieldResponses();
    	Connection conn = null; 
    	PreparedStatement stmt = null; 
    	ResultSet rs = null ;
    	ArrayList formColumns = new ArrayList();
    	ArrayList formColumnsDataTypes = new ArrayList();
    	ArrayList formColumnsSystemIDs = new ArrayList();
    	SaveFormDao sfDao = new SaveFormDao(); 
    	sfDao.setFormId(formLibPK); 
        sfDao.getFormDetails();         

		FormSections formSections = linkedFormDesign.getSections(); 
			
		List<FormSection> formSectionsList = formSections.getSection(); 
		List<FormField> formfieldList = new ArrayList<FormField>(); 
				
		for(FormSection formSection: formSectionsList)
		{
			formfieldList.addAll(formSection.getFields().getField()); 		
		}

		//Map of SystemIDs 
		Map<String, FormField> formFieldValueMap = new HashMap<String, FormField>();
		
		for(FormField formField: formfieldList)
		{
			formFieldValueMap.put(formField.getSystemID(), formField); 
		}
					
        formColumns = sfDao.getColNames();
        formColumnsDataTypes = sfDao.getColTypes();
        formColumnsSystemIDs = sfDao.getColSysIds(); 
    	
    	
    	try{
    		
    		conn = getConnection(); 
    		stmt = conn.prepareStatement(getFormResponseSQL(filledFormID, loggedInUserID.getUserId(), DisplayType.ALL_PATIENTS, formColumns, formColumnsDataTypes, formColumnsSystemIDs)); 
    		rs = stmt.executeQuery(); 
    		
    		if(rs.next())
    		{

    			formResponse.setFormFillDt(rs.getDate("hidden_filldate"));

    			FormIdentifier formIdentifier  = new FormIdentifier(); 
    			ObjectMap formObjectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_FORM_LIBRARY, formLibPK);
    			formIdentifier.setPK(formLibPK);
    			formIdentifier.setOID(formObjectMap.getOID()); 
    			formResponse.setFormId(formIdentifier); 

    			CodeCache codeCache = CodeCache.getInstance(); 
    			Code formStatus  = 	codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_FILLED_FORM_STATUS,rs.getString("FORM_COMPLETED"),StringUtil.stringToInteger(loggedInUserID.getUserAccountId()));
    			formResponse.setFormStatus(formStatus); 
    			formResponse.setFormVersion(rs.getInt("form_version")); 
    			
    			
    			PatientIdentifier patientIdentifier = new PatientIdentifier(); 
    			ObjectMap objectMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPK); 
    			patientIdentifier.setOID(objectMap.getOID());
    			patientIdentifier.setPK(personPK);
    			patientIdentifier.setPatientId(rs.getString("personCode")); 
    			OrganizationIdentifier orgId = new OrganizationIdentifier();
    			orgId.setSiteName(rs.getString("sitecode"));
    			patientIdentifier.setOrganizationId(orgId);
    			formResponse.setPatientID(patientIdentifier); 
    			
    			/*StudyIdentifier studyID = new StudyIdentifier(); 
    			ObjectMap studyMap = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK); 
    			studyID.setOID(studyMap.getOID()); 
    			studyID.setStudyNumber(rs.getString("studyNumber")); 
    			formResponse.setStudyIdentifier(studyID); */

    			Iterator formColSysIDItr = formColumnsSystemIDs.iterator(); 
    			while(formColSysIDItr.hasNext())
    			{
    				String systemID = formColSysIDItr.next().toString(); 
    				FormFieldResponse formFieldResponse = new FormFieldResponse(); 
    				formFieldResponse.setFieldIdentifier(formFieldValueMap.get(systemID).getFieldIdentifier()); 
    				formFieldResponse.setValue(rs.getString(systemID)== null?"":rs.getString(systemID));  
    				formFieldResponses.addField(formFieldResponse); 
    			}

    		}else{
    			throw new OperationException(); 
    		}
    			
    			
//    			
//    		}
    		
    		formResponse.setFormFieldResponses(formFieldResponses); 
    		
    	}catch(SQLException sqe)
    	{
    		logger.error(sqe); 
    		sqe.printStackTrace(); 
    	}finally
    	{
    		if(rs != null)
			{
				try
				{
					rs.close(); 
				}catch(SQLException sqe)
				{}
			}

			if(stmt != null)
			{
				try{
					stmt.close(); 
				}catch(SQLException sqe){}
			}

			if(conn != null)
			{
				returnConnection(conn); 
			} 
    	}
    	return formResponse;     	
    
	}

}
