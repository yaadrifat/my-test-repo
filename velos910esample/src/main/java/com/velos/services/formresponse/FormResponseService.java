/**
 * Created On Sep 2, 2011
 */
package com.velos.services.formresponse;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.AccountFormResponseIdentifier;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormResponse;
import com.velos.services.model.PatientFormResponse;
import com.velos.services.model.PatientFormResponseIdentifier;
import com.velos.services.model.PatientFormResponses;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormResponse;
import com.velos.services.model.StudyFormResponseIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormResponse;
import com.velos.services.model.StudyPatientFormResponseIdentifier;
import com.velos.services.model.StudyPatientFormResponses;
import com.velos.services.model.StudyPatientScheduleFormResponse;
import com.velos.services.model.VisitIdentifier;

/**
 * @author Kanwaldeep
 *
 */
@Remote
public interface FormResponseService {
	
	public ResponseHolder createStudyFormResponse(StudyFormResponse studyFormResponse)
	throws OperationRolledBackException, OperationException; 
	
	public ResponseHolder removeStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
	throws OperationRolledBackException, OperationException; 

	public StudyFormResponse getStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier)
	throws OperationException; 
	public PatientFormResponse getPatientFormResponse(PatientFormResponseIdentifier formResponseIdentifier)
	throws OperationException;
	
//       getStudyPatientFormResponse
	public ResponseHolder createStudyPatientFormResponse(StudyPatientFormResponse studyPatientFormResponse)
	throws OperationException; 
	
	public StudyPatientFormResponse getStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier)
	throws OperationException;
	
	public ResponseHolder updateStudyPatientFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier, 
			FormResponse studyPatientFormResponse)
	throws OperationException; 
			
	
    public ResponseHolder removeStudyPatientFormResponse(
    		StudyPatientFormResponseIdentifier formResponseIdentifier)
    throws OperationException; 
//	createAccountFormResponse
//	removeAccountFormResponse
//	getAccountFormResponse
//	createPatientFormResponse
//	removePatientFormResponse
//	getPatientFormResponse
//	updatePatientFormResponse
//	updateStudyPatientFormResponse
//	updateStudyFormResponse
//	updateAccountFormResponse

	/**
	 * @param studyPatientScheduleFormResponse
	 * @param calendarIdentifier
	 * @param calendarName
	 * @param visitIdentifier
	 * @param visitName
	 * @param eventIdentifier
	 * @param eventName
	 * @return
	 * @throws OperationException 
	 */
	public ResponseHolder createStudyPatientScheduleFormResponse(
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse,
			CalendarIdentifier calendarIdentifier, String calendarName,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName) throws OperationException;
	
	public StudyPatientScheduleFormResponse getStudyPatientScheduleFormResponse(StudyPatientFormResponseIdentifier
			studyPatientFormResponseIdentifier) throws OperationException; 
	
	public ResponseHolder updateStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier formResponseIdentifier,
			StudyPatientScheduleFormResponse studyPatientScheduleFormResponse) throws OperationException;

	/**
	 * @param studyPatientFormResponseIdentifier
	 * @return
	 */
	public ResponseHolder removeStudyPatientScheduleFormResponse(
			StudyPatientFormResponseIdentifier studyPatientFormResponseIdentifier) throws OperationException;

	/**
	 * @param formResponseIdentifier
	 * @param studyFormResponse
	 * @return
	 */
	public ResponseHolder updateStudyFormResponse(
			StudyFormResponseIdentifier formResponseIdentifier,
			StudyFormResponse studyFormResponse) throws OperationException; 
	
	/**
	 * @param FormIdentifier
	 * @param PatientIdentifier
	 * @param StudyIdentifier
	 * @return StudyPatientFormResponses
	 */
	public StudyPatientFormResponses getListOfStudyPatientFormResponses(
			FormIdentifier formIdentifier,
			PatientIdentifier patientIdentifier,
			StudyIdentifier studyIdentifier,
			int pageNumber,
			int pageSize) throws OperationException;
	
	/**
	 * @param FormIdentifier
	 * @param PatientIdentifier
	 * @return PatientFormResponses
	 * @throws OperationException 
	 */
	public PatientFormResponses  getListOfPatientFormResponses(
			FormIdentifier formIdentifier,
			PatientIdentifier patientIdentifier,
			int pageNumber,
			int pageSize
			) throws OperationException;
	
	  public ResponseHolder createPatientFormResponse(PatientFormResponse patientFormResponse) 
	    		throws OperationException;
	  
	  /**
		 * @param patientFormResponseIdentifier
		 * @param patientFormResponse
		 * @return
		 */
		public ResponseHolder updatePatientFormResponse(
				PatientFormResponseIdentifier patientFormResponseIdentifier,
				PatientFormResponse patientFormResponse) throws OperationException; 
		
		public ResponseHolder removePatientFormResponse(
				PatientFormResponseIdentifier formResponseIdentifier)
		throws OperationRolledBackException, OperationException; 
	  


}
