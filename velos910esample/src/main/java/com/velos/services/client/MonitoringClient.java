package com.velos.services.client;

import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.MessageTraffic;
import com.velos.services.monitoring.MessageService;
import com.velos.services.util.JNDINames;
/***
 * Client Class/Remote object for Monitoring Services 
 * @author Virendra
 *
 */
public class MonitoringClient{
	/**
	 * Invokes the remote object.
	 * @return remote monitoring service
	 * @throws OperationException
	 */
	
	private static MessageService getMonitoringRemote()
	throws OperationException{
		
	MessageService monitoringRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		monitoringRemote =
			(MessageService) ic.lookup(JNDINames.MessageServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return monitoringRemote;
	}
	/**
	 * Client Method call retrieves the message traffic in MessageTraffic object.
	 * @param fromDate
	 * @param toDate
	 * @return MessageTraffic
	 * @throws OperationException
	 */
	
	public static MessageTraffic getMessageTraffic(Date fromDate, Date toDate) 
	throws OperationException{
		MessageService monitoringService = getMonitoringRemote();
		return monitoringService.getMessageTraffic(fromDate, toDate);
	}
	/**
	 * Client method call retrieves the status of Database connection in
	 * ResponseHolder object
	 * @return response
	 * @throws OperationException
	 */

	public static ResponseHolder getHeartBeat() 
	throws OperationException{
		ResponseHolder response = new ResponseHolder();
		MessageService monitoringService = getMonitoringRemote();
		if(monitoringService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_MONITORING_SERVICES, 
					"Exception getting remote monitoring service:"+monitoringService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return monitoringService.getHeartBeat();
	}
	
}