/**
 * Created On May 16, 2011
 */
package com.velos.services.client;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.calendar.StudyCalendarService;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEventSummary;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisitSummary;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Cost;
import com.velos.services.model.EventCostIdentifier;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventIdentifiers;
import com.velos.services.model.EventNames;
import com.velos.services.model.StudyCalendar;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.VisitIdentifiers;
import com.velos.services.model.VisitNames;
import com.velos.services.util.JNDINames;

/**
 * @author Kanwaldeep
 *
 */
public class StudyCalendarClient {
	
	private static StudyCalendarService getStudyCalendarRemote() throws OperationException
	{
		StudyCalendarService studyCalendarRemote = null;
		InitialContext ic = null ; 
		try
		{
			ic = new InitialContext(); 
			studyCalendarRemote = (StudyCalendarService) ic.lookup(JNDINames.StudyCalendarServiceImpl);
						
		}catch(NamingException e){
				throw new OperationException(e);
		}
		
		return studyCalendarRemote; 
	}
	
	public static ResponseHolder createStudyCalendar(StudyCalendar study) throws OperationException
	{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.createStudyCalendar(study); 
		
	}
	
	public static ResponseHolder updateStudyCalendarSummary(
			CalendarIdentifier calendarIdentifier, 
			StudyIdentifier studyIdentifier, 
			String calendarName,
			CalendarSummary calendarSummary) 
	throws OperationException
	{
		StudyCalendarService service = getStudyCalendarRemote();
		return service.updateStudyCalendarSummary(calendarIdentifier, studyIdentifier, calendarName, calendarSummary); 
	}
	
	public static StudyCalendar getStudyCalendar(CalendarIdentifier calendarIdentifier, StudyIdentifier studyIdentifier, String calendarName) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.getStudyCalendar(calendarIdentifier, studyIdentifier, calendarName); 
	}
	
	//Visit Methods
	public static ResponseHolder updateStudyCalendarVisitDetails(			
			CalendarIdentifier studyCalendarIdentifier,
			String visitName,
			VisitIdentifier visitIdentifier,
			CalendarVisitSummary studyCalendarVisit) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.updateStudyCalendarVisit(studyCalendarIdentifier, visitIdentifier, visitName, studyCalendarVisit); 
	}
	
	public static CalendarVisit getStudyCalendarVisit(			
			CalendarIdentifier calendarIdentifier,			
			VisitIdentifier visitIdentifier,
			String visitName) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.getStudyCalendarVisit(calendarIdentifier, visitIdentifier, visitName); 
	}
	
	
	public static ResponseHolder addVisitsToStudyCalendar(			
			CalendarIdentifier calendarIdentifier,			
			CalendarVisits newVisit) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.addVisitsToStudyCalendar(calendarIdentifier, newVisit); 
	}
	

	public static ResponseHolder removeVisitsFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifiers visitIdentifier,
			VisitNames visitName) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.removeVisitsFromStudyCalendar(calendarIdentifier, visitIdentifier, visitName); 
	}
	
	
	//Event Methods
	
	public  static ResponseHolder updateStudyCalendarEventDetails(
			CalendarIdentifier studyCalendarIdentifier,
			VisitIdentifier visitIdentifier, 
			String visitName,
			EventIdentifier eventIdentifier, 
			String eventName,
			CalendarEventSummary studyCalendarEvent) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.updateStudyCalendarEvent(studyCalendarIdentifier, visitIdentifier, visitName, eventIdentifier, eventName, studyCalendarEvent); 
	}
	
	public static CalendarEvent getStudyCalendarEvent(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, 
			String visitName,
			EventIdentifier eventIdentifier, 
			String eventName) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.getStudyCalendarEvent(calendarIdentifier, visitIdentifier, visitName, eventIdentifier, eventName); 
	}
	
	
	public static ResponseHolder addEventsToStudyCalendarVisit(
			CalendarIdentifier calendarIdentifier,
			String visitName,
			VisitIdentifier visitIdentifier, 
			CalendarEvents studyCalendarEvent) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.addEventsToStudyCalendarVisit(calendarIdentifier, visitName, visitIdentifier, studyCalendarEvent); 
	}
	
	public static ResponseHolder removeEventsFromStudyCalendarVisit(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier,
			String visitName,
			EventIdentifiers eventIdentifier,
			EventNames eventName) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.removeEventsFromStudyCalendarVisit(calendarIdentifier, visitIdentifier, visitName, eventIdentifier, eventName); 
	}
	
	public static CalendarSummary getStudyCalendarSummary(
			CalendarIdentifier calendarIdentifier, 
			StudyIdentifier studyIdentifier, 
			String calendarName) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.getStudyCalendarSummary(calendarIdentifier, studyIdentifier, calendarName); 
	}
	
	public static ResponseHolder removeStudyCalendar(CalendarIdentifier calendarIdentifier, StudyIdentifier studyIdentifier, String calendarName)
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.removeStudyCalendar(calendarIdentifier, studyIdentifier, calendarName); 
	}
	//Event Cost methods
	//Added by Virendra as Event Cost services of CalndarService
	public static ResponseHolder removeEventCost(
			EventCostIdentifier eventCostIdentifier) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.removeEventCost(eventCostIdentifier);
		
	};
	
	public static ResponseHolder addEventCost(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName,
			 Cost cost) throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		
		return service.addEventCost(calendarIdentifier, 
				visitIdentifier, visitName, eventIdentifier, eventName,
				 cost);
		
	};
	
	public static ResponseHolder updateEventCost(
			EventCostIdentifier eventCostIdentifier,
			Cost cost) 
	throws OperationException{
		StudyCalendarService service = getStudyCalendarRemote(); 
		return service.updateEventCost(eventCostIdentifier, cost);
		
	};

}
