package com.velos.services.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Visit;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.patientschedule.PatientScheduleService;
import com.velos.services.util.JNDINames;
/***
 * Client Class/Remote object for Patient Schedule Services 
 * @author Virendra
 *
 */
public class PatientScheduleClient{
	/**
	 * Invokes the remote object.
	 * @return remote PtientDemo service
	 * @throws OperationException
	 */
	
	private static PatientScheduleService getPatScheduleRemote()
	throws OperationException{
		
	PatientScheduleService PatScheduleRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		PatScheduleRemote =
			(PatientScheduleService) ic.lookup(JNDINames.PatientScheduleServiceImpl);
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return PatScheduleRemote;
	}
	/**
	 * Calls getPatientSchedule on getPatientSchedule Remote object
	 * to return PatientSchedule 
	 * @param patientId
	 * @param studyIdentifier
	 * @return PatSchedule
	 * @throws OperationException
	 */

	
	public static PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier) 
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getPatientScheduleList(patientId, studyIdentifier);
	}
	
	public static PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleOID)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getPatientSchedule(scheduleOID);
	}
	
	public static PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientId, StudyIdentifier studyIdentifier,Date startDate,Date endDate)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getCurrentPatientSchedule(patientId, studyIdentifier, startDate, endDate);
	}
	
	public static ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus)
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.addScheduleEventStatus(eventIdentifier, eventStatus);
	}
	
	public static SitesOfService getSitesOfService()
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getSitesOfService();
	}
	
}