/**
 * Created On Sep 12, 2011
 */
package com.velos.services.client;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.form.FormDesignService;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.FormList;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.PatientForms;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientForms;
import com.velos.services.model.StudyPatientScheduleFormDesign;
import com.velos.services.util.JNDINames;


/**
 * @author Kanwaldeep
 *
 */
public class FormRemoteObj {
	
	private static FormDesignService getFormRemote() throws OperationException
	{
		FormDesignService formRemote = null; 
		InitialContext ic; 
		
		try
		{
			ic = new InitialContext(); 
			formRemote = (FormDesignService) ic.lookup(JNDINames.FormDesignServiceImpl);
			
			
		}catch(NamingException e){
				throw new OperationException(e);
		}
		
		return formRemote; 
	}
	
	public static FormDesign getLibraryFormDesign(FormIdentifier formIdentifier, boolean includeFormatting, String formName) throws OperationException
	{
		FormDesignService formService = getFormRemote(); 
		return formService.getLibraryFormDesign(formIdentifier, includeFormatting, formName); 
	}
	

	public  static StudyFormDesign getStudyFormDesign(FormIdentifier formIdentifier, StudyIdentifier studyIdentifier, String formName, boolean includeFormatting)
	throws OperationException{
		FormDesignService formService = getFormRemote(); 
		 return formService.getStudyFormDesign(formIdentifier, studyIdentifier, formName, includeFormatting); 
	}
	
	public static LinkedFormDesign getAccountFormDesign(FormIdentifier formIdentifier, String formName, boolean includeFormatting)
	throws OperationException{
		FormDesignService formService = getFormRemote(); 
		return formService.getAccountFormDesign(formIdentifier, formName, includeFormatting); 
		
	}
	public static LinkedFormDesign getPatientFormDesign(FormIdentifier formIdentifier, String formName,boolean includeFormatting)
	throws OperationException{
		FormDesignService formService = getFormRemote(); 
		return formService.getPatientFormDesign(formIdentifier, formName,includeFormatting); 
	}
	
	public static StudyPatientFormDesign getStudyPatientFormDesign(FormIdentifier formIdentifier, StudyIdentifier studyIdentifier, String formName, boolean includeFormatting)
	throws OperationException{
		FormDesignService formService = getFormRemote(); 
		return formService.getStudyPatientFormDesign(formIdentifier, studyIdentifier, formName, includeFormatting); 
	}
	
	public static FormList getAllFormsForStudy(StudyIdentifier studyIdentifier) throws OperationException
	{
		FormDesignService formService = getFormRemote(); 
		return formService.getAllFormsForStudy(studyIdentifier); 
	}

	public static StudyPatientScheduleFormDesign getStudyPatientScheduleFormDesign(
			FormIdentifier formIdentifier, StudyIdentifier studyIdentifier,
			String formName, boolean includeFormatting) throws OperationException {
		FormDesignService formService = getFormRemote(); 
		return formService.getStudyPatientScheduleFormDesign(formIdentifier, studyIdentifier, formName, includeFormatting);
	}
	
	public static StudyPatientForms getListOfStudyPatientForms(PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier, int maxNumberOfResults, boolean formHasResponses) throws OperationException
	{
		FormDesignService formService= getFormRemote();
		return formService.getListOfStudyPatientForms(patientIdentifier, studyIdentifier, maxNumberOfResults, formHasResponses);
	}
	
	public static PatientForms getListOfPatientForms(PatientIdentifier patientIdentifier, boolean formHasResponses,int pageNumber, int pageSize) throws OperationException
	{
		FormDesignService formService= getFormRemote();
		return formService.getListOfPatientForms(patientIdentifier, formHasResponses, pageNumber, pageSize);
	}

}
