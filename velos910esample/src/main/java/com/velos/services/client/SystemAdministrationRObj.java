/**
 * Created On Nov 6, 2012
 */
package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.model.CodeTypes;
import com.velos.services.model.Codes;
import com.velos.services.systemadmin.CodeListService;
import com.velos.services.util.JNDINames;

/**
 * @author Kanwaldeep
 *
 */
public class SystemAdministrationRObj {
	
	private static CodeListService getCodeListRemote() throws OperationException
	{
		CodeListService codeListService = null; 
		InitialContext ic = null; 
		try
		{
			ic = new InitialContext();
			codeListService = (CodeListService) ic.lookup(JNDINames.CodeListServiceImpl); 
			
		}catch(NamingException ne)
		{
			throw new OperationException(ne); 
		}
		
		return codeListService; 
	}
	
	public static Codes getCodeList(String codeListType) throws OperationException
	{
		return getCodeListRemote().getCodeList(codeListType); 
	}

	/**
	 * @return
	 */
	public static CodeTypes getCodeTypes() throws OperationException{
			return getCodeListRemote().getCodeTypes();
	}

}
