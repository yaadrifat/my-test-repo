package com.velos.services.util;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.eres.service.util.Rlog;
import com.velos.services.map.ObjectMapService;

public class ServicesUtil {
	
	 public static ObjectMapService getObjectMapService() {
		 InitialContext initial = null; 
	        try {
	            initial = new InitialContext();
	            return (ObjectMapService) initial.lookup(
	            		JNDINames.ObjectMapServiceImpl);

	        } catch (Exception e) {
	            Rlog.fatal("common", "object Map" + e);
	            return null;
	        }finally
	        {
	        	try {
					initial.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	        }
	    }

}
