package com.velos.services.util;

public class JNDINames {
	public final static String prefix = "velos/";
	public final static String suffix = "/remote";
	
	public final static String MessageServiceImpl = prefix+"MessageServiceImpl"+suffix;
	public final static String ObjectMapServiceImpl = prefix+"ObjectMapServiceImpl"+suffix;
	public final static String SessionServiceImpl = prefix+"SessionServiceImpl"+suffix;
	public final static String StudyServiceImpl = prefix+"StudyServiceImpl"+suffix;
	public final static String StudyCalendarServiceImpl = prefix+"StudyCalendarServiceImpl"+suffix; 
	public final static String PatientDemographicsServiceImpl = prefix+"PatientDemographicsServiceImpl"+suffix;
	public final static String PatientStudyServiceImpl = prefix+"PatientStudyServiceImpl" + suffix; 
	public final static String StudyPatientServiceImpl = prefix+ "StudyPatientServiceImpl" + suffix; 
	public final static String PatientScheduleServiceImpl= prefix+ "PatientScheduleServiceImpl" + suffix; 
	public final static String FormDesignServiceImpl= prefix+ "FormDesignServiceImpl" + suffix;
	public final static String FormResponseServiceImpl = prefix + "FormResponseServiceImpl" + suffix; 
	public final static String BudgetServiceImpl = prefix + "BudgetServiceImpl" + suffix;
	public final static String UserServiceImpl = prefix + "UserServiceImpl" + suffix;
	public final static String SearchPatientServiceImpl = prefix +"SearchPatientServiceImpl" +suffix; 
	public final static String UpdatePatientDemographicsServiceImpl = prefix+"UpdatePatientDemographicsServiceImpl"+suffix; 
	public final static String LibraryServiceImpl = prefix + "LibraryServiceImpl" + suffix;
	public final static String PatientFacilityServiceImpl = prefix+"PatientFacilityServiceImpl"+suffix;
	public final static String CodeListServiceImpl = prefix + "CodeListServiceImpl" + suffix; 
	
}
