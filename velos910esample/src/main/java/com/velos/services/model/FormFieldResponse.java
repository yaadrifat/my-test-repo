/**
 * Created On Nov 9, 2011
 */
package com.velos.services.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="FieldResponse")
public class FormFieldResponse extends ServiceObject{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3510472588381084523L;

	protected String value;
	protected FieldIdentifier fieldIdentifier; 
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public FieldIdentifier getFieldIdentifier() {
		return fieldIdentifier;
	}
	public void setFieldIdentifier(FieldIdentifier fieldIdentifier) {
		this.fieldIdentifier = fieldIdentifier;
	} 
	
}
