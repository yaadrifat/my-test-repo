package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.velos.services.form.FormDesignDAO;

public class StudyPatientForms implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -528778118234310698L;

	protected List<StudyPatientFormDesign> studyPatientForms = new ArrayList<StudyPatientFormDesign>();

	protected List<FormDesignDAO> frmDesignDao= new ArrayList<FormDesignDAO>();
	
	protected int formCount;
	
	/**
	 * @return the formCount
	 */
	public int getFormCount() {
		return formCount;
	}

	/**
	 * @param formCount the formCount to set
	 */
	public void setFormCount(int formCount) {
		this.formCount = formCount;
	}

	public List<StudyPatientFormDesign> getStudyPatientForms() {
		return studyPatientForms;
	}

	public void setStudyPatientForms(List<StudyPatientFormDesign> studyPatientForms) {
		this.studyPatientForms = studyPatientForms;
	}
	

	public void addAll(List<StudyPatientFormDesign> studyPatientForms) {
		this.studyPatientForms = studyPatientForms;
	}
	
	public void add(StudyPatientFormDesign studyPatientForms) {
		this.studyPatientForms.add(studyPatientForms);
	}
	
	public void add(FormDesignDAO frmDesignDao) {
		this.frmDesignDao.add(frmDesignDao);
	}


}
