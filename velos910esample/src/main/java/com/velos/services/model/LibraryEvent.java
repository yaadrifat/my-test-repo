package com.velos.services.model;
/*
 * Author: Tarandeep Singh Bali
 */

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="LibraryEvent")
public class LibraryEvent extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9077481369174034593L;
	
	protected String eventCategory;
	protected String eventName;
	protected String description;
	protected List<Cost> costs;
	
	public LibraryEvent(){
		
	}

	public String getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Cost> getCosts() {
		return costs;
	}

	public void setCosts(List<Cost> costs) {
		this.costs = costs;
	}
	
	

}
