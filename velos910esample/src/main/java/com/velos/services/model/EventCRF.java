package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="EventCRF")
public class EventCRF extends ServiceObject {
	
	protected FormIdentifier formIdentifier;
	protected String formName;
	public FormIdentifier getFormIdentifier() {
		return formIdentifier;
	}
	public void setFormIdentifier(FormIdentifier formIdentifier) {
		this.formIdentifier = formIdentifier;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}

}
