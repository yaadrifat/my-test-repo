/**
 * Created On Oct 20, 2011
 */
package com.velos.services.model;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kanwaldeep
 *
 */
public enum FormType {
	LIBRARY("L"),
	STUDY("S"),
	PATIENT("P"),
	ACCOUNT("A"),
	STUDYPATIENT("SP"); 
	private static final Map<String,FormType> lookup 
	= new HashMap<String,FormType>();
	static {
		for(FormType s : EnumSet.allOf(FormType.class))
			lookup.put(s.getValue(), s);
	}

	private FormType(String value)
	{
		this.value = value; 
	}

	private final String value; 

	public String getValue(){
		return value;
	}

	public static FormType get(String value)
	{
		return lookup.get(value); 
	}

}
