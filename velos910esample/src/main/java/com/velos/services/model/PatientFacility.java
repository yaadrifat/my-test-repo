package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientFacility")
	public class PatientFacility extends ServiceObject implements Serializable
	{
	private static final long serialVersionUID = -6507444023773849440L;
	
	protected PatientOrganization patientOrganization;
	protected PatientOrganizationIdentifier patientOrganizationIdentifier;
	
	public PatientOrganizationIdentifier getPatientOrganizationIdentifier() {
		return patientOrganizationIdentifier;
	}
	public void setPatientOrganizationIdentifier(
			PatientOrganizationIdentifier patientOrganizationIdentifier) {
		this.patientOrganizationIdentifier = patientOrganizationIdentifier;
	}
	public PatientOrganization getPatientOrganization() {
		return patientOrganization;
	}
	public void setPatientOrganization(PatientOrganization patientOrganization) {
		this.patientOrganization = patientOrganization;
	}

}
