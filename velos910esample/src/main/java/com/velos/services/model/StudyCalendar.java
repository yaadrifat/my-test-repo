/**
 * 
 */
package com.velos.services.model;

import java.util.List;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyCalendar")
public class StudyCalendar extends ServiceObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5149425039581664209L;

	protected CalendarIdentifier calendarIdentifier;
	
	protected StudyIdentifier studyIdentifier; 

	protected CalendarSummary calendarSummary;
	
	protected CalendarVisits visits;
	
	public StudyCalendar(){
		
	}

	public CalendarIdentifier getCalendarIdentifier() {
		return calendarIdentifier;
	}

	public void setCalendarIdentifier(CalendarIdentifier calendarIdentifier) {
		this.calendarIdentifier = calendarIdentifier;
	}
	
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}

	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	
	@Valid
	public CalendarVisits getVisits() {
		return visits;
	}

	public void setVisits(CalendarVisits visits) {
		this.visits = visits;
	}
	@Valid
	public CalendarSummary getCalendarSummary() {
		return calendarSummary;
	}

	public void setCalendarSummary(CalendarSummary calendarSummary) {
		this.calendarSummary = calendarSummary;
	}
	
	
}
