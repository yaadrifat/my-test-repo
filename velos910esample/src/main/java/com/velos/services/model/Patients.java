/**
 * Created On January 2, 2013
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Raman
 *
 */

@XmlRootElement(name="Patients")
public class Patients extends ServiceObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4573949502407701466L;
	
	private List<UpdatePatientDemographics> patients = new ArrayList<UpdatePatientDemographics>();

	
	/**
	 * @return the patients
	 */
	public List<UpdatePatientDemographics> getPatients() {
		return patients;
	}
	/**
	 * @param patients the patients to set
	 */
	public void setPatients(List<UpdatePatientDemographics> patients) {
		this.patients = patients;
	}


	public void addPatient(UpdatePatientDemographics patientDemographics)
	{
		this.patients.add(patientDemographics); 
	}

}
