package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyIdentifier")

/**
 * Subclass of {@link SimpleIdentifier} used to identify a {@link Study}
 */
public class StudyIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5570649506761170709L;
	protected String studyNumber;
	
	public StudyIdentifier(){
		super();
	}

	public StudyIdentifier(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	/**
	 * Gets the study number, which uniquely identifies a study for a particular account.
	 * @return
	 */
	public String getStudyNumber() {
		return studyNumber;
	}

	/**
	 * Sets the study number, which uniquely identifies a study for a particular account.
	 * @return
	 */
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	@Override
	public String toString(){
		return studyNumber;
	}
	
}
