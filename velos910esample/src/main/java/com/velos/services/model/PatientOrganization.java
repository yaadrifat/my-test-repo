/**
 * Created On Jun 17, 2011
 */
package com.velos.services.model;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author Kanwaldeep
 *
 */
public class PatientOrganization extends ServiceObject{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7730822434538150780L;

	protected OrganizationIdentifier OrganizationID; 
	
	protected String facilityID; 
	
	protected Date registrationDate; 
	//Raman bug#11974
	protected boolean emptyRegistrationDate;
	
	protected UserIdentifier provider; 
	
	protected String otherProvider; 
	
	protected SpecialityAccess specialtyAccess;
	
	protected AccessRights access; 
	
	protected boolean isDefault;

	
	public boolean isEmptyRegistrationDate() {
		return emptyRegistrationDate;
	}

	public void setEmptyRegistrationDate(boolean emptyRegistrationDate) {
		this.emptyRegistrationDate = emptyRegistrationDate;
	}

	@NotNull
	public OrganizationIdentifier getOrganizationID() {
		return OrganizationID;
	}

	public void setOrganizationID(OrganizationIdentifier organizationID) {
		OrganizationID = organizationID;
	}

	@NotNull
	public String getFacilityID() {
		return facilityID;
	}

	public void setFacilityID(String facilityID) {
		this.facilityID = facilityID;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public UserIdentifier getProvider() {
		return provider;
	}

	public void setProvider(UserIdentifier provider) {
		this.provider = provider;
	}

	public String getOtherProvider() {
		return otherProvider;
	}

	public void setOtherProvider(String otherProvider) {
		this.otherProvider = otherProvider;
	}

	public SpecialityAccess getSpecialtyAccess() {
		return specialtyAccess;
	}

	public void setSpecialtyAccess(SpecialityAccess specialtyAccess) {
		this.specialtyAccess = specialtyAccess;
	}
	
	public AccessRights getAccess() {
		return access;
	}

	public void setAccess(AccessRights access) {
		this.access = access;
	}

	@NotNull
	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	} 
	
	
	

}
