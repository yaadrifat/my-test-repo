/**
 * Created On Sep 30, 2011
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Kanwaldeep
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FieldValidations")
public class FieldValidations extends ServiceObject{
	/**
	 * 
	 */		
	private static final long serialVersionUID = 1769502725967211486L;
	private boolean mandatory; 
	private boolean overrideMandatory;
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	public boolean isOverrideMandatory() {
		return overrideMandatory;
	}
	public void setOverrideMandatory(boolean overrideMandatory) {
		this.overrideMandatory = overrideMandatory;
	}

}
