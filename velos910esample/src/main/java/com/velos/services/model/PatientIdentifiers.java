/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/** Model class representing a list of PatientIdentifier Object
 * @author Raman
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientIdentifiers")
public class PatientIdentifiers implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 169553995649914323L;
	protected List<PatientIdentifier> patientIdentifiers;
	
	/**
	 * @return the patientIdentifiers
	 */
	@NotNull 
    @Size(min=1)
    @Valid
	public List<PatientIdentifier> getPatientIdentifiers() {
		return patientIdentifiers;
	}
	/**
	 * @param patientIdentifiers the patientIdentifiers to set
	 */
	public void setPatientIdentifiers(List<PatientIdentifier> patientIdentifiers) {
		this.patientIdentifiers = patientIdentifiers;
	}
	
	
	
	
}
