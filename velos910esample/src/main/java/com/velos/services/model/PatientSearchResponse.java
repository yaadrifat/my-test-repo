package com.velos.services.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSearchResponse")
public class PatientSearchResponse extends ServiceObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4248454119997827485L;
	/**
	 * 
	 */
	List<PatientDataBean> patDataBean = null;
	int patRecordCount = 0;
	public List<PatientDataBean> getPatDataBean() {
		return patDataBean;
	}
	public void setPatDataBean(List<PatientDataBean> patDataBean) {
		this.patDataBean = patDataBean;
	}
	public int getPatRecordCount() {
		return patRecordCount;
	}
	public void setPatRecordCount(int patRecordCount) {
		this.patRecordCount = patRecordCount;
	}
}
