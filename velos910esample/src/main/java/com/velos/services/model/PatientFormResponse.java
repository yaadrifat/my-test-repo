/**
 * Created On Nov 9, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class PatientFormResponse extends FormResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8967158498667906191L;	
	
	protected PatientFormResponseIdentifier formFilledFormId; 

	protected PatientIdentifier patientID;
	
//	protected PatientProtocolIdentifier patprotID;

	public PatientIdentifier getPatientID() {
		return patientID;
	}

	public void setPatientID(PatientIdentifier patientID) {
		this.patientID = patientID;
	}

	public PatientFormResponseIdentifier getFormFilledFormId() {
		return formFilledFormId;
	}

	public void setFormFilledFormId(PatientFormResponseIdentifier formFilledFormId) {
		this.formFilledFormId = formFilledFormId;
	}


}
