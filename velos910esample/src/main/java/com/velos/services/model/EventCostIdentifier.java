package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="EventCostIdentifier")
@XmlAccessorType(XmlAccessType.FIELD)
public class EventCostIdentifier extends SimpleIdentifier{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8996403275260133153L;

}
