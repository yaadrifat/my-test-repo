package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientFacilities")
public class PatientFacilities extends ServiceObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7069807246862011586L;
	protected List<PatientFacility> patientFacility = new ArrayList<PatientFacility>();
	protected PatientIdentifier patientIdentifier;
	
	public List<PatientFacility> getPatientFacility() {
		return patientFacility;
	}
	public void setPatientFacility(List<PatientFacility> patientFacility) {
		this.patientFacility = patientFacility;
	}
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
		
	public void add(PatientFacility patfac) {
		this.patientFacility.add(patfac);
	}

	public void addAll(List<PatientFacility> patfac) {
		this.patientFacility = patfac;
	}
	
}
