package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="HeartBeatCount")


public class MessageTraffic implements Serializable{
	/**
     * 
     */
    private static final long serialVersionUID = 5175773824153262803L;
    
    /**
	 * Object for Message Traffic with properties as All Messages Count,
	 * Success Messages Count, Failed Message Count.
	 */
	protected Long messageCountAll;
	protected Long messageCountSuccess;
	protected Long messageCountFailure;
	
	public Long getMessageCountAll() {
		return messageCountAll;
	}
	public void setMessageCountAll(Long messageCountAll) {
		this.messageCountAll = messageCountAll;
	}
	public Long getMessageCountSuccess() {
		return messageCountSuccess;
	}
	public void setMessageCountSuccess(Long messageCountSuccess) {
		this.messageCountSuccess = messageCountSuccess;
	}
	public Long getMessageCountFailure() {
		return messageCountFailure;
	}
	public void setMessageCountFailure(Long messageCountFailure) {
		this.messageCountFailure = messageCountFailure;
	}
	
}