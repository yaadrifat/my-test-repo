	/**
 * 
 */
package com.velos.services.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Transfer object for storing cost information. Typically, event costs are
 * associated with {@link Event} objects.
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CostInfo")

public class Cost extends ServiceObject {
	/** Cost type **/
	
	protected EventCostIdentifier eventCostIdentifier; 
	protected Code costType;
	protected Code currency;
	protected BigDecimal cost;
	public Cost() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * get cost type
	 * @return
	 */
	public Code getCostType() {
		return costType;
	}

	/**
	 * set cost type
	 * @param costType
	 */
	public void setCostType(Code costType) {
		this.costType = costType;
	}


	public Code getCurrency() {
		return currency;
	}


	public void setCurrency(Code currency) {
		this.currency = currency;
	}


	public BigDecimal getCost() {
		return cost;
	}


	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}


	public EventCostIdentifier getEventCostIdentifier() {
		return eventCostIdentifier;
	}


	public void setEventCostIdentifier(EventCostIdentifier eventCostIdentifier) {
		this.eventCostIdentifier = eventCostIdentifier;
	}	

}
