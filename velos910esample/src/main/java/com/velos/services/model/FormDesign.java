/**
 * Created On Aug 25, 2011
 */
package com.velos.services.model;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Kanwaldeep
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Form")
public class FormDesign extends ServiceObject{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2429146316345517936L;
	private String formName; 
	private String formDesc; 
	private SharedWith formSharedWith; 
	private Code formStatus; 
	private LinkedTo formLinkedTo; 
	private FormSections sections;	
	private boolean isESignRequired; 
	private FormIdentifier formIdentifier; 
	
	public enum SharedWith
	{
		 PRIVATE("P"), 
		 USERS("A"),
		 GROUPS("G"),
		 STUDYTEAM("S"),
		 ORGANIZATION("O"); 
		
		private SharedWith(String value)
		{
			this.value = value; 
		}
		
		private final String value; 
		
		public String toString()
		{
			return value; 		
		}
		
	}
	
	public enum LinkedTo
	{
		LIBRARY("L"), 
		STUDY("S"),
		ACCOUNT("A"); 
		
		private LinkedTo(String value)
		{
			this.value = value; 
		}
		
		private String value; 
		public String toString()
		{
			return value; 
		}	
	}
	
	
	public FormSections getSections() {
		return sections;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormDesc() {
		return formDesc;
	}
	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}
	public SharedWith getFormSharedWith() {
		return formSharedWith;
	}
	public void setFormSharedWith(SharedWith formSharedWith) {
		this.formSharedWith = formSharedWith;
	}	
	public Code getFormStatus() {
		return formStatus;
	}
	public void setFormStatus(Code formStatus) {
		this.formStatus = formStatus;
	}
	public LinkedTo getFormLinkedTo() {
		return formLinkedTo;
	}
	public void setFormLinkedTo(LinkedTo formLinkedTo) {
		this.formLinkedTo = formLinkedTo;
	}
	public boolean isESignRequired() {
		return isESignRequired;
	}
	public void setESignRequired(boolean isESignRequired) {
		this.isESignRequired = isESignRequired;
	}
	public void setSections(FormSections sections) {
		this.sections = sections;
	}
	
	public void setFormIdentifier(FormIdentifier formIdentifier)
	{
		this.formIdentifier = formIdentifier; 
	}
	
	public FormIdentifier getFormIdentifier()
	{
		return formIdentifier; 
	}
	

}
