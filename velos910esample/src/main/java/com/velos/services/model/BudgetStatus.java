/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Virendra
 *
 */
public class BudgetStatus extends ServiceObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected BudgetIdentifier budgetIdentifier;
	protected Code budgetStatus;
	protected Date statusDate;
	protected UserIdentifier statusChangedBy;
	
	public BudgetIdentifier getBudgetIdentifier() {
		return budgetIdentifier;
	}
	public void setBudgetIdentifier(BudgetIdentifier budgetIdentifier) {
		this.budgetIdentifier = budgetIdentifier;
	}
	public Code getBudgetStatus() {
		return budgetStatus;
	}
	public void setBudgetStatus(Code budgetStatus) {
		this.budgetStatus = budgetStatus;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	public UserIdentifier getStatusChangedBy() {
		return statusChangedBy;
	}
	public void setStatusChangedBy(UserIdentifier statusChangedBy) {
		this.statusChangedBy = statusChangedBy;
	}
	
	
	

}
