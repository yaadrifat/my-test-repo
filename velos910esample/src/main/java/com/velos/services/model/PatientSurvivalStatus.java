
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model class representing a patient survival status.
 * @author Raman
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSurvivalStatus")
public class PatientSurvivalStatus extends ServiceObject
{

	/**
     * 
     */
    private static final long serialVersionUID = 2564377396563561079L;
    protected PatientIdentifier patientIdentifier;
    protected Code survivalStatus;
	
    
    /**
	 * @return the patientIdentifier
	 */
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	/**
	 * @param patientIdentifier the patientIdentifier to set
	 */
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	/**
	 * @return the survivalStatus
	 */
	public Code getSurvivalStatus() {
		return survivalStatus;
	}
	/**
	 * @param survivalStatus the survivalStatus to set
	 */
	public void setSurvivalStatus(Code survivalStatus) {
		this.survivalStatus = survivalStatus;
	}
    
    
    
}
