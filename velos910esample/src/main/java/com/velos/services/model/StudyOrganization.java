
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Transfer object representing a Study Organization. Study Organizations are related to Study Team Members,
 * though stored separately.
 * 
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyOrganization")
public class StudyOrganization
    extends ServiceObject
{

    /**
     * 
     */
    private static final long serialVersionUID = 3577717517772640191L;

	protected OrganizationIdentifier organizationIdentifier;

    protected Code type;
    //virendra: fixed #5558, changed data type to String
    protected Integer localSampleSize;

    protected boolean publicInformationFlag;

    protected boolean reportableFlag;

    protected Boolean reviewBasedEnrollmentFlag;

    //5253 DRM 12/14/10 - removed for now
//    protected List<QName> attachment;
    
    //Virendra: Commented to fix #5397
    //protected int currentSitePatientCount;
    
    protected List<UserIdentifier> newEnrollNotificationTo;
    
    protected List<UserIdentifier> approveEnrollNotificationTo;
    
    
	public StudyOrganization(){
    	
    }
	
	/**
	 * Returns the {@link OrganizationIdentifier} for this Study Organization.
	 * @return
	 */
    @NotNull
    public OrganizationIdentifier getOrganizationIdentifier() {
		return organizationIdentifier;
	}

    /**
     * Sets the {@link OrganizationIdentifier} for this Study Organization.
     * @param organizationIdentifier
     */
	public void setOrganizationIdentifier(
			OrganizationIdentifier organizationIdentifier) {
		this.organizationIdentifier = organizationIdentifier;
	}

	//Virendra: Commented to fix #5397
//	public int getCurrentSitePatientCount() {
//		return currentSitePatientCount;
//	}
//
//
//	public void setCurrentSitePatientCount(int currentSitePatientCount) {
//		this.currentSitePatientCount = currentSitePatientCount;
//	}

	/**
	 * @deprecated User the method {@link #isReviewBasedEnrollmentFlag()} This field
	 * will be removed in a future version.
	 */
	public Boolean getReviewBasedEnrollmentFlag() {
		return reviewBasedEnrollmentFlag;
	}

	 //5253 DRM 12/14/10 - removed for now
//	public void setAttachment(List<QName> attachment) {
//		this.attachment = attachment;
//	}


	/**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link Code }
     *     
     */
	@Valid
    public Code getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link Code }
     *     
     */
    public void setType(Code value) {
        this.type = value;
    }
    //virendra: commented to fix #5558
    /**
     * Gets the value of the localSampleSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Max(2147483647)
    public Integer getLocalSampleSize() {
        return localSampleSize;
    }

    /**
     * Sets the value of the localSampleSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLocalSampleSize(Integer value) {
        this.localSampleSize = value;
    }

    /**
     * Gets the value of the publicInformationFlag property.
     * 
     */
    public boolean isPublicInformationFlag() {
        return publicInformationFlag;
    }

    /**
     * Sets the value of the publicInformationFlag property.
     * 
     */
    public void setPublicInformationFlag(boolean value) {
        this.publicInformationFlag = value;
    }

    /**
     * Gets the value of the reportableFlag property.
     * 
     */
    public boolean isReportableFlag() {
        return reportableFlag;
    }

    /**
     * Sets the value of the reportableFlag property.
     * 
     */
    public void setReportableFlag(boolean value) {
        this.reportableFlag = value;
    }

    /**
     * Gets the value of the reviewBasedEnrollmentFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReviewBasedEnrollmentFlag() {
        return reviewBasedEnrollmentFlag;
    }

    /**
     * Sets the value of the reviewBasedEnrollmentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReviewBasedEnrollmentFlag(Boolean value) {
        this.reviewBasedEnrollmentFlag = value;
    }

   //5253 DRM 12/14/10 - removed for now
//    public List<QName> getAttachment() {
//        if (attachment == null) {
//            attachment = new ArrayList<QName>();
//        }
//        return this.attachment;
//    }

	@Override
	public String toString() {
		//DRM Removed references to system ID and object map service for 2.0 build 100
//		return "External ID: " + organizationIdentifier.getSystemId() + 
		return 	"Site Name: " + organizationIdentifier.getSiteName();
	}
	public List<UserIdentifier> getNewEnrollNotificationTo() {
		return newEnrollNotificationTo;
	}
	public void setNewEnrollNotificationTo(
			List<UserIdentifier> newEnrollNotificationTo) {
		this.newEnrollNotificationTo = newEnrollNotificationTo;
	}
	public List<UserIdentifier> getApproveEnrollNotificationTo() {
		return approveEnrollNotificationTo;
	}
	public void setApproveEnrollNotificationTo(
			List<UserIdentifier> approveEnrollNotificationTo) {
		this.approveEnrollNotificationTo = approveEnrollNotificationTo;
	}
//	//virendra: fixed #5558, changed data type to String
//	/**
//	 * 
//	 * @return
//	 */
//	public String getLocalSampleSize() {
//		return localSampleSize;
//	}
//	public void setLocalSampleSize(String localSampleSize) {
//		this.localSampleSize = localSampleSize;
//	}
	//virendra;Fixed#6121, overloading setParentId for studyIdentifier
    public StudyIdentifier getParentIdentifier() {
        if (super.parentIdentifier == null || 
                super.parentIdentifier.getId().size() < 1) { return null; }
        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
            if (myId instanceof StudyIdentifier) {
                return (StudyIdentifier)myId;
            }
        }
        return null;
    }
    
    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
        idList.add(studyIdentifer);
        super.parentIdentifier = new ParentIdentifier();
        super.parentIdentifier.setId(idList);
    }

}
