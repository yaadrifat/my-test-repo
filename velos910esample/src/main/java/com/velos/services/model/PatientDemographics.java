
package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientDemographics")

/**
 * Model class for fields in the Patient Demographics
 */

public class PatientDemographics
    extends ServiceObject 
    implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1519268508848509517L;
	/** constant defining the name of the velos tabespace for more patient details */
	public static QName MORE_PATIENT_DETAILS_NS = 
			new QName(
					"www.velos.com",
					"MorePatientDetails",
			"Map");


	/**
	 * 
	 */


	public PatientDemographics(){
		
	}
	
	protected PatientIdentifier patientIdentifier;
	protected String patientCode; 
	protected String firstName;
	protected String middleName;
	protected String lastName;
    protected Date dateOfBirth;
    protected Code gender;
    protected Code ethnicity;
    protected Code race;
    
    protected Code survivalStatus;
    protected Date deathDate;
    protected String eMail;
    protected String address1;
    protected String address2;
    protected String city;
    protected String state;
    protected String county;
    protected String zipCode;
    protected String country;
    protected String homePhone;
    protected String workPhone;
    protected OrganizationIdentifier organization;
    protected String patFacilityId;
    protected Date registrationDate;
    //Virendra:Fixed 6023
    protected List<NVPair> morePatientDetails;
    //Raman
    protected boolean emptyDeathDate;
    
    
	protected Code deathCause;
	protected String deathCauseOther;
	protected Codes additionalEthnicity ;
	protected Codes additionalRace;
	protected Code bloodGroup;
	protected Code maritalStatus;
	protected String notes;
	protected Code timeZone;
	protected Code employment;
	protected Code education;
	protected String SSN;
	public enum enumSSNNotAppl{
		 YES,
		 NO;
	}
	private enumSSNNotAppl ssn;    

	public enumSSNNotAppl getSSNNot(){
		return ssn; 
	}
	public void setSSNNot(enumSSNNotAppl ssn){
     this.ssn = ssn;
	}   
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public Code getDeathCause() {
		return deathCause;
	}
	public void setDeathCause(Code deathCause) {
		this.deathCause = deathCause;
	}
	public String getDeathCauseOther() {
		return deathCauseOther;
	}
	public void setDeathCauseOther(String deathCauseOther) {
		this.deathCauseOther = deathCauseOther;
	}
	public Codes getAdditionalEthnicity() {
		return additionalEthnicity;
	}
	public void setAdditionalEthnicity(Codes additionalEthnicity) {
		this.additionalEthnicity = additionalEthnicity;
	}
	public Codes getAdditionalRace() {
		return additionalRace;
	}
	public void setAdditionalRace(Codes additionalRace) {
		this.additionalRace = additionalRace;
	}
	public Code getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(Code bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public Code getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(Code maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Code getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(Code timeZone) {
		this.timeZone = timeZone;
	}
	public Code getEmployment() {
		return employment;
	}
	public void setEmployment(Code employment) {
		this.employment = employment;
	}
	public Code getEducation() {
		return education;
	}
	public void setEducation(Code education) {
		this.education = education;
	}
  
	public boolean isEmptyDeathDate() {
		return emptyDeathDate;
	}
	public void setEmptyDeathDate(boolean emptyDeathDate) {
		this.emptyDeathDate = emptyDeathDate;
	}
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	public String getPatientCode() {
		return patientCode;
	}
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Code getGender() {
		return gender;
	}
	public void setGender(Code gender) {
		this.gender = gender;
	}
	public Code getEthnicity() {
		return ethnicity;
	}
	public void setEthnicity(Code ethnicity) {
		this.ethnicity = ethnicity;
	}
	public Code getRace() {
		return race;
	}
	public void setRace(Code race) {
		this.race = race;
	}
	public Code getSurvivalStatus() {
		return survivalStatus;
	}
	public void setSurvivalStatus(Code survivalStatus) {
		this.survivalStatus = survivalStatus;
	}
	public Date getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	public OrganizationIdentifier getOrganization() {
		return organization;
	}
	public void setOrganization(OrganizationIdentifier organization) {
		this.organization = organization;
	}
	public String getPatFacilityId() {
		return patFacilityId;
	}
	public void setPatFacilityId(String patFacilityId) {
		this.patFacilityId = patFacilityId;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public List<NVPair> getMorePatientDetails() {
		return morePatientDetails;
	}
	public void setMorePatientDetails(List<NVPair> morePatientDetails) {
		this.morePatientDetails = morePatientDetails;
	}

   }
