/**
 * Created On Jan 3, 2013
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class CodeType extends ServiceObject {

	private static final long serialVersionUID = -3689889589748579429L;
	protected String codeType;
	protected String description;
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
