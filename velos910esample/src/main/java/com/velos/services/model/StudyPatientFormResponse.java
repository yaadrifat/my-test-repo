/**
 * Created On Nov 9, 2011
 */
package com.velos.services.model;

/**
 * @author Kanwaldeep
 *
 */
public class StudyPatientFormResponse extends FormResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9022733846807064205L;
	
	protected StudyIdentifier studyIdentifier; 
	protected PatientIdentifier patientIdentifier;
	protected PatientProtocolIdentifier patientProtocolIdentifier;
	
	public StudyIdentifier getStudyIdentifier() {
		return studyIdentifier;
	}
	public void setStudyIdentifier(StudyIdentifier studyIdentifier) {
		this.studyIdentifier = studyIdentifier;
	}
	public PatientIdentifier getPatientIdentifier() {
		return patientIdentifier;
	}
	public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifier = patientIdentifier;
	}
	public PatientProtocolIdentifier getPatientProtocolIdentifier() {
		return patientProtocolIdentifier;
	}
	public void setPatientProtocolIdentifier(
			PatientProtocolIdentifier patientProtocolIdentifier) {
		this.patientProtocolIdentifier = patientProtocolIdentifier;
	}
	public SimpleIdentifier getSystemID() {
		return systemId;
	}
	public void setSystemID(SimpleIdentifier systemID) {
		this.systemId = systemID;
	}
	
	

}
