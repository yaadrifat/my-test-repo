/**
 * Created On Apr 16, 2012
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kanwaldeep
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ScheduleEventIdentifier")
public class ScheduleEventIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7075031938865984744L;



}
