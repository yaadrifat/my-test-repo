package com.velos.services.authorization;


import javax.ejb.Remote;
import com.velos.services.OperationException;

/**
 * 
 * @author Raman
 *
 */

@Remote
public interface PatientAuthAgentRObj {
	
	  public boolean checkPatientPermission(Integer mainPK,Privilege privilege)
		throws OperationException;
}
