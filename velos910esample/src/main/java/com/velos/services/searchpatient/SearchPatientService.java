package com.velos.services.searchpatient;


import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.PatientSearch;
import com.velos.services.model.PatientSearchResponse;

@Remote
public abstract interface SearchPatientService {
	
	  public abstract PatientSearchResponse searchPatient(PatientSearch paramPatientIdentifier)
	    throws OperationException;	
}
