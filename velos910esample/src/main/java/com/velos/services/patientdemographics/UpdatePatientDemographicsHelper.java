package com.velos.services.patientdemographics;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.SessionContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.velos.epat.audit.service.AuditRowEpatAgent;
import com.velos.epat.business.common.AuditRowEpatDao;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.perId.impl.PerIdBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.perIdAgent.PerIdAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.account.AccountJB;
import com.velos.eres.web.user.ConfigDetailsObject;
import com.velos.eres.web.user.ConfigFacade;
import com.velos.services.AbstractService;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.ValidationException;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.NVPair;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.UpdatePatientDemographics;
import com.velos.services.patientdemographics.MorePatientDetailsDAO;
import com.velos.services.util.CodeCache;

public class UpdatePatientDemographicsHelper extends CommonDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8892283816684237784L;
	private static Logger logger = Logger.getLogger(UpdatePatientDemographicsHelper.class);
	private static Integer personPk=null; 
	
	private boolean isFieldMandatory(ConfigDetailsObject configDetails, String field)
	{
		PatientDemographicsHelper patientDemographicHelper = new PatientDemographicsHelper(); 
		return patientDemographicHelper.isMandatory(configDetails, field);
	}
	
	private PersonBean getPersonBeanForUpdate(UpdatePatientDemographics updatePatDemo,PersonBean perDbBean, UserBean callingUser, Map<String, Object> parameters)
		    throws OperationException
		  {	 
		
			PatientDao pdao = new PatientDao();
			boolean hasMinPHIRight = false;
			int userId = callingUser.getUserId();
			int grpId = EJBUtil.stringToNum(callingUser.getUserGrpDefault());
		    int minPHIRight = pdao.getPatientCompleteDetailsAccessRight(userId, grpId, perDbBean.getPersonPKId());
			System.out.println("--- minPHIRight ---"+minPHIRight);
			if(minPHIRight >= 4)
			{    
				hasMinPHIRight = true;		    
			}
		    
			PersonBean perBean=new PersonBean();
			
			ConfigDetailsObject configDetails = 
		      ConfigFacade.getConfigFacade().populateObject(EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue(), "patient");
		    AccountJB acctJB = new AccountJB();
		    acctJB.setAccId(EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue());
		    acctJB.getAccountDetails();

		    List validationIssues = new ArrayList();
		    if (configDetails != null)
		    {		    			
		    			if(updatePatDemo.getPatientCode() == null)
				        {
				        	perBean.setPersonPId(perDbBean.getPersonPId());
				        }
				        else
				        {
				        	
				        	if(updatePatDemo.getPatientCode().trim().length() == 0 && (isFieldMandatory(configDetails, "patid")))
				        	{
				        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Patient Id may not be null"));
				        	}
				 		    else
				 		    {
				 		    	if(!(updatePatDemo.getPatientCode().equalsIgnoreCase(perDbBean.getPersonPId())))
				 		    	{
								   Integer defOrgPk=null;
								   //UpdatePatientDemographicsDao updatePatientDemographicsDao=new UpdatePatientDemographicsDao();
								   //defOrgPk=updatePatientDemographicsDao.getDefOrgPkfromPersonPk(personPk);
								   defOrgPk= EJBUtil.stringToNum(perDbBean.getPersonLocation());
								   if(defOrgPk.intValue()==0 || defOrgPk==null)
									{
									 	((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.ORGANIZATION_NOT_FOUND,"No Default Organization Found"));
										OperationException oe=new OperationException();
										oe.setIssues(((ResponseHolder) parameters.get("ResponseHolder")).getIssues());
										throw oe;								 	
									}
								   
								   PatientDao patDao=new PatientDao();				 		        
					 		       boolean isPatientIDExists=patDao.patientCodeExists(updatePatDemo.getPatientCode(), EJBUtil.integerToString(defOrgPk));
								   //boolean isPatientIDExists=updatePatientDemographicsDao.isPatientIdExists(personPk, updatePatDemo.getPatientCode(), defOrgPk);
					 		       System.out.println(defOrgPk);
					 		      if (isPatientIDExists)
						    	    {
						    	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						    	        new Issue(
						    	        IssueTypes.PATIENT_ID_ALREADY_EXISTS, "Patient ID already exists: " + updatePatDemo.getPatientCode()));
						    	      throw new OperationException();
						    	    }
				 		    	}
				 		    	perBean.setPersonPId(updatePatDemo.getPatientCode());
				 		    }
				        }
				 
		        
		    
		    	if(updatePatDemo.getFirstName() == null)
		        {
		        	perBean.setPersonFname(perDbBean.getPersonFname());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getFirstName().trim().length() == 0 && (isFieldMandatory(configDetails, "fname")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "First Name may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonFname(updatePatDemo.getFirstName());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, First Name = " + updatePatDemo.getFirstName()));
		        	               throw new OperationException();		        	     
		        	}		        	
		        }
		      
		      
		    	if(updatePatDemo.getMiddleName() == null)
		        {
		        	perBean.setPersonMname(perDbBean.getPersonMname());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getMiddleName().trim().length() == 0 && (isFieldMandatory(configDetails, "midname")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Middle Name may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonMname(updatePatDemo.getMiddleName());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Middle Name = " + updatePatDemo.getMiddleName()));
		        	               throw new OperationException();
		        	     
		        	}		
		        }
		    	
		     
		    	if(updatePatDemo.getLastName() == null)
		        {
		        	perBean.setPersonLname(perDbBean.getPersonLname());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getLastName().trim().length() == 0 && (isFieldMandatory(configDetails, "lname")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Last Name may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonLname(updatePatDemo.getLastName());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Last Name = " + updatePatDemo.getLastName()));
		        	               throw new OperationException();
		        	}
		        }

		     
		    	if(updatePatDemo.getDateOfBirth() == null)
		        {
		        	perBean.setPersonDb(perDbBean.getPersonDb());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		Date date = new Date();
		        	    if (updatePatDemo.getDateOfBirth().after(date))
		        	    {
		        	    	Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Date of birth should not be after present day");
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(issue);
		        	      throw new OperationException();
		        	    }
		        	    if(updatePatDemo.getDeathDate() == null && perDbBean.getPersonDeathDt() != null){
					    	  if (perDbBean.getPersonDeathDt().before(updatePatDemo.getDateOfBirth()))
						      {
						        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						          new Issue(IssueTypes.DATA_VALIDATION, "Date of Birth cannot be after DeathDate"));
						        throw new OperationException();
						      }
				    	  }
		        	
		        	    perBean.setPersonDb(updatePatDemo.getDateOfBirth());
		  		      
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Date of Birth : " + updatePatDemo.getDateOfBirth()));
		        	               throw new OperationException();
		        	} 
		        }
		    	
		    	if(updatePatDemo.getGender() == null)
		        {
		        	perBean.setPersonGender(perDbBean.getPersonGender());
		        }
		        else
		        {
		        	if((updatePatDemo.getGender().getCode() == null || updatePatDemo.getGender().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "gender")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Gender code may not be null"));
		        	}
		 		    else
		 		    {
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getGender().getCode() != null && updatePatDemo.getGender().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "gender"))) {
		 		    			perBean.setPersonGender("");
		 		    		} else {
		 		    			String genderCode=AbstractService.dereferenceCodeStr(updatePatDemo.getGender(), "gender", callingUser);		 		    		
		 		    			perBean.setPersonGender(genderCode);
		 		    		}
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Gender Code Not Found: " + updatePatDemo.getGender().getCode()));
			 		                throw new OperationException();
			 		    }
		 		    }
		        }
		    	
		      
		    	
		    	if(updatePatDemo.getEthnicity() == null)
		        {
		        	perBean.setPersonEthnicity(perDbBean.getPersonEthnicity());
		        }
		        else
		        {
		        	if((updatePatDemo.getEthnicity().getCode() ==null || updatePatDemo.getEthnicity().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "ethnicity")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Ethnicity code may not be null"));
		        	}
		 		    else
		 		    {
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getEthnicity().getCode() !=null && updatePatDemo.getEthnicity().getCode().trim().length()==0 )&& !(isFieldMandatory(configDetails, "ethnicity"))){
		 		    			perBean.setPersonEthnicity("");
		 		    		} else {
		 		    			String ethinicityCode=AbstractService.dereferenceCodeStr(updatePatDemo.getEthnicity(), "ethnicity", callingUser);		 		    		
		 		    			perBean.setPersonEthnicity(ethinicityCode);
		 		    		}
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Ethnicity Code Not Found: " + updatePatDemo.getEthnicity().getCode()));
			 		                throw new OperationException();
			 		    }
		 		    }
		        }
		      
		    	
		    	if(updatePatDemo.getRace() == null)
		        {
		        	perBean.setPersonRace(perDbBean.getPersonRace());
		        }
		        else
		        {
		        	if((updatePatDemo.getRace().getCode() == null || updatePatDemo.getRace().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "race")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Race code may not be null"));
		        	}
		 		    else
		 		    {
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getRace().getCode() != null && updatePatDemo.getRace().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "race"))){
		 		    			perBean.setPersonRace("");	
		 		    		} else {
		 		    			String raceCode=AbstractService.dereferenceCodeStr(updatePatDemo.getRace(), "race", callingUser);
		 		    		
		 		    			perBean.setPersonRace(raceCode);		 		    	
		 		    		}
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Race Code Not Found: " +updatePatDemo.getRace().getCode() ));
			 		                throw new OperationException();
			 		    }
		 		    }
		        }
		     	
		    	
		    	//Survival Status
		    	
		    	if(updatePatDemo.getSurvivalStatus()== null)
				{
		    		perBean.setPersonStatus(perDbBean.getPersonStatus());
				}
				else
				{
						if((updatePatDemo.getSurvivalStatus().getCode()==null || updatePatDemo.getSurvivalStatus().getCode().trim().length()==0)&& (isFieldMandatory(configDetails, "survivestat")))
						{
							validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Survival Status code may not be null"));
						}
						else
						{
							try
							{
								String survivalStatusCode = AbstractService.dereferenceCodeStr(updatePatDemo.getSurvivalStatus(), "patient_status", callingUser);
								perBean.setPersonStatus(survivalStatusCode);
								CodeCache codeCache = CodeCache.getInstance();
								updatePatDemo.setSurvivalStatus(codeCache.getCodeSubTypeByPK("patient_status", survivalStatusCode, EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue()));
							} 
							catch (CodeNotFoundException e) {
					        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					          new Issue(
					          IssueTypes.DATA_VALIDATION, 
					          "Survival Status Code Not Found: " + updatePatDemo.getSurvivalStatus().getCode()));
					        throw new OperationException();
					      }
						if (updatePatDemo.getSurvivalStatus().getCode().equals("A"))
					    {
					      if (updatePatDemo.getDeathDate() != null)
					      {
					        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					          new Issue(IssueTypes.DATA_VALIDATION, "Patient's survival status is given as \"" + updatePatDemo.getSurvivalStatus().getCode()+ "\" along with Date of Death"));
					        throw new OperationException();
					      }
					      
					      if(hasMinPHIRight && (perDbBean.getPersonDeathDate() != null && perDbBean.getPersonDeathDate().length() > 0) && !updatePatDemo.isEmptyDeathDate())
					      {
					    	  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
							          new Issue(IssueTypes.DATA_VALIDATION, "Patient's survival status is given as \"" + updatePatDemo.getSurvivalStatus().getCode()+ "\" without isEmptyDeathDate as true"));
							        throw new OperationException();
					      }
					    
					      if(updatePatDemo.getDeathCause() != null)
					      {
					    	  if(updatePatDemo.getDeathCause().getCode() != null && updatePatDemo.getDeathCause().getCode().length() > 0)
					    	  {	  
					    		  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
							          new Issue(IssueTypes.DATA_VALIDATION, "Patient's death cause is given as \"" + updatePatDemo.getDeathCause().getCode()+ "\" along with Patient status " +updatePatDemo.getSurvivalStatus().getCode()));
							        throw new OperationException();
					    	  }					    	
					      }
					      
					      if(perDbBean.getPatDthCause()!= null  && (updatePatDemo.getDeathCause() == null || (updatePatDemo.getDeathCause().getCode()!=null && updatePatDemo.getDeathCause().getCode().length() > 0 )))
				    	  {
				    		  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
							          new Issue(IssueTypes.DATA_VALIDATION, "Please update Patient's death cause to \"\" along with Patient status " +updatePatDemo.getSurvivalStatus().getCode()));
							        throw new OperationException();
				    	  }  

					    }
							
					
						if(hasMinPHIRight)
						{	
						if (updatePatDemo.getSurvivalStatus().getCode().equals("dead"))
						    {
						      if (updatePatDemo.getDeathDate() == null)
						      {
						        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						          new Issue(IssueTypes.DATA_VALIDATION, "DeathDate is required when Patient's survival status is given as \" " + updatePatDemo.getSurvivalStatus().getCode() + " \""));
						        throw new OperationException();
						      }
						     				      				      
						      if(updatePatDemo.getDateOfBirth()!=null)
						      {	  
						    	  if (updatePatDemo.getDeathDate().before(updatePatDemo.getDateOfBirth()))
						    	  {
						    		  ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						    				  new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth"));
						    		  throw new OperationException();
						    	  }
						      }
						      else
						      {
						    	  if (updatePatDemo.getDeathDate().before(perDbBean.getPersonDb()))
							      {
							        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
							          new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth"));
							        throw new OperationException();
							      }
						    	  Date date = new Date();
						    	    if (updatePatDemo.getDeathDate().after(date))
						    	    {
						    	      Issue issue = new Issue(IssueTypes.DATA_VALIDATION, "Date of death should not be after present day");
						    	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(issue);
						    	      throw new OperationException();
						    	    }
						    	  
						      }	
						    
						    }
						}
						
					}	
				
		        }
		      
		    	//Date of Death
		    	if(updatePatDemo.getDeathDate() == null && hasMinPHIRight)
		        {		        	
		    		if (updatePatDemo.getSurvivalStatus()!=null && updatePatDemo.getSurvivalStatus().getCode()!=null)
		    	    {
		    			if(updatePatDemo.getSurvivalStatus().getCode().toLowerCase().equals("dead")){
		    				((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		  		    	          new Issue(IssueTypes.DATA_VALIDATION, "DeathDate is required when Patient's survival status is given as \" " + updatePatDemo.getSurvivalStatus().getCode() + " \""));
		  		    	        throw new OperationException();
		    			} 
//		    			else if(updatePatDemo.getSurvivalStatus().getCode().toString().toUpperCase().equals("A"))
//			    		{
//			    			perBean.setPersonDeathDt(null);
//			    		}
		    			else {
			    			perBean.setPersonDeathDt(perDbBean.getPersonDeathDt());
			    		}
		    	        
		    	    } else {		    		
		    	    	perBean.setPersonDeathDt(perDbBean.getPersonDeathDt());
		    	    }
		        }
		        else
		        {			        	
		        	if(hasMinPHIRight)
		        	{
		        		if ((isFieldMandatory(configDetails, "dod")) && (updatePatDemo.getDeathDate() == null))
			  		      {
			  		        validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Date of Death may not be null"));
			  		      } else {
			        		if ( updatePatDemo.getSurvivalStatus()!=null && updatePatDemo.getSurvivalStatus().getCode()!=null && updatePatDemo.getSurvivalStatus().getCode().toUpperCase().equals("A"))
				            {
				        		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				                  new Issue(IssueTypes.DATA_VALIDATION, "Patient's survival status is given as Alive along with Date of Death"));
				                throw new OperationException();
				            }			        	 			        	
				        	if((updatePatDemo.getSurvivalStatus()==null || updatePatDemo.getSurvivalStatus().getCode()==null) &&(CodeCache.getInstance()).getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS,EJBUtil.stringToInteger(perDbBean.getPersonStatus()),EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue()).getCode().toString().equals("A") )
				        		{
				        		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						                  new Issue(IssueTypes.DATA_VALIDATION, "Death Date may not be provided as Survival status code is already provided as Alive"));
						                throw new OperationException();
				        		}
	
				        	 if (updatePatDemo.getDateOfBirth()!=null && updatePatDemo.getDeathDate().before(updatePatDemo.getDateOfBirth()))
				             {
				        		 
				        		 ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						                  new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth"));
						                throw new OperationException();
	
				             
				             }
				        	 else if(updatePatDemo.getDateOfBirth()==null && updatePatDemo.getDeathDate().before(perDbBean.getPersonDb()))
				        	 {
				        		
				        		 ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
						                  new Issue(IssueTypes.DATA_VALIDATION, "DeathDate cannot be before Date of Birth " + perDbBean.getPersonDb()));
						                throw new OperationException();
	
				        	 }
				        	
				        	perBean.setPersonDeathDt(updatePatDemo.getDeathDate());
			        	}
		        	}else
		        	{
		        		perBean.setPersonDeathDt(perDbBean.getPersonDeathDt());
		        	}
		        	
		        }
		      
		      
		    	
			    //cause of death
		    	if(updatePatDemo.getDeathCause() == null)
		        {
		    			perBean.setPatDthCause(perDbBean.getPatDthCause());

		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if((updatePatDemo.getDeathCause().getCode() == null || updatePatDemo.getDeathCause().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "cod")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Death Cause code may not be null"));
			        	}
			 		    else
			 		    {
			 		    	if(updatePatDemo.getDeathCause().getCode() != null && updatePatDemo.getDeathCause().getCode().toString().trim().length() > 0 ){
				 		    	if(updatePatDemo.getSurvivalStatus() != null && updatePatDemo.getSurvivalStatus().getCode() != null && updatePatDemo.getSurvivalStatus().getCode().toString().toUpperCase().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Survival Status code may not be Alive along with Death Cause" ));
					 		                throw new OperationException();
				 		    	}
				 		    	if((updatePatDemo.getSurvivalStatus() == null || updatePatDemo.getSurvivalStatus().getCode() == null) && (CodeCache.getInstance()).getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS,EJBUtil.stringToInteger(perDbBean.getPersonStatus()),EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue()).getCode().toString().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Death Cause may not be given as survival status is already provided as Alive" ));
					 		                throw new OperationException();
				 		    	}
				 		    	if(updatePatDemo.getDeathDate()==null && perDbBean.getPersonDeathDt()==null)
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Death Date is needed when specifying Death Cause" ));
					 		                throw new OperationException();
				 		    	}
				 		    	try
				 		    	{
				 		    		if((updatePatDemo.getDeathCause().getCode() != null && updatePatDemo.getDeathCause().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "cod"))){
				 		    			perBean.setPatDthCause("");	 	
				 		    		} else {
					 		    		String deathCauseCode = AbstractService.dereferenceCodeStr(updatePatDemo.getDeathCause(), "pat_dth_cause", callingUser);
						 		    	
					 		    		perBean.setPatDthCause(deathCauseCode);	 		    	
				 		    		}
				 		    	}
				 		    	catch (CodeNotFoundException e)
				 		    	{
			 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Death Cause code Not Found: " + updatePatDemo.getDeathCause().getCode()));
					 		                throw new OperationException();
					 		    }	
			 		    	} else if (updatePatDemo.getDeathCause().getCode() == null){
			 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				 		                  new Issue(
				 		                  IssueTypes.DATA_VALIDATION, 
				 		                  "Death Cause code Not Found: " + updatePatDemo.getDeathCause().getCode()));
				 		                throw new OperationException();
			 		    	} else {
			 		    		perBean.setPatDthCause("");	
			 		    		
			 		    	}			 		    	
			 		    					 		    
			 		    }
		        	}
		        	else{
		        		
		        	
		        		if((updatePatDemo.getDeathCause().getCode() == null || updatePatDemo.getDeathCause().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "cod")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Death Cause code may not be null"));
			        	}
			 		    else
			 		    {
			 		    	if(updatePatDemo.getDeathCause().getCode() != null && updatePatDemo.getDeathCause().getCode().toString().trim().length() > 0 ){
				 		    	if(updatePatDemo.getSurvivalStatus() != null && updatePatDemo.getSurvivalStatus().getCode() != null && updatePatDemo.getSurvivalStatus().getCode().toString().toUpperCase().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Survival Status code may not be Alive along with Death Cause" ));
					 		                throw new OperationException();
				 		    	}
				 		    	if((updatePatDemo.getSurvivalStatus() == null || updatePatDemo.getSurvivalStatus().getCode() == null) && (CodeCache.getInstance()).getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS,EJBUtil.stringToInteger(perDbBean.getPersonStatus()),EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue()).getCode().toString().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Death Cause may not be given as survival status is already provided as Alive" ));
					 		                throw new OperationException();
				 		    	}
				 		    	
				 		    	try
				 		    	{
				 		    		if((updatePatDemo.getDeathCause().getCode() != null && updatePatDemo.getDeathCause().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "cod"))){
				 		    			perBean.setPatDthCause("");	 	
				 		    		} else {
					 		    		String deathCauseCode1 = AbstractService.dereferenceCodeStr(updatePatDemo.getDeathCause(), "pat_dth_cause", callingUser);
						 		    	
					 		    		perBean.setPatDthCause(deathCauseCode1);	 		    	
				 		    		}
				 		    	}
				 		    	catch (CodeNotFoundException e)
				 		    	{
			 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Death Cause code Not Found: " + updatePatDemo.getDeathCause().getCode()));
					 		                throw new OperationException();
					 		    }	
			 		    	} else if (updatePatDemo.getDeathCause().getCode() == null){
			 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				 		                  new Issue(
				 		                  IssueTypes.DATA_VALIDATION, 
				 		                  "Death Cause code Not Found: " + updatePatDemo.getDeathCause().getCode()));
				 		                throw new OperationException();
			 		    	} else {
			 		    		perBean.setPatDthCause("");	
			 		    		
			 		    	}			 		    	
			 		    					 		    
			 		    }
		        	
		        	}
		        	
		        }
		    	
		    	
		    	//DeathCauseOther
		    	if(updatePatDemo.getDeathCauseOther() == null)
		        {
		        	perBean.setDthCauseOther(perDbBean.getDthCauseOther());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getDeathCauseOther().length() == 0 && (isFieldMandatory(configDetails, "speccause")))
			        	{
			        		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Specify Other Death Cause may not be null"));		  		    }
			 		    else
			 		    {
			 		    	if(updatePatDemo.getDeathCauseOther().trim().length()>0){
				 		        if(updatePatDemo.getSurvivalStatus() != null && updatePatDemo.getSurvivalStatus().getCode().toString().toUpperCase().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Survival Status code may not be Alive along with Other Death Cause" ));
					 		                throw new OperationException();
				 		    	}
				 		    	if(updatePatDemo.getSurvivalStatus() == null && (CodeCache.getInstance()).getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS,EJBUtil.stringToInteger(perDbBean.getPersonStatus()),EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue()).getCode().toString().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Other Death Cause may not be given as survival status is already provided as Alive" ));
					 		                throw new OperationException();
				 		    	}
				 		    	if(updatePatDemo.getDeathDate()==null && perDbBean.getPersonDeathDt()==null)
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Death Date is needed when specifying Other Death Cause" ));
					 		                throw new OperationException();
				 		    	}
			 		    	}
			 		    	perBean.setDthCauseOther(updatePatDemo.getDeathCauseOther().trim());
				 		    
			 		    }
		        	
		        	}
		        	
		        	else{
		        	
		        		if(updatePatDemo.getDeathCauseOther().length() == 0 && (isFieldMandatory(configDetails, "speccause")))
			        	{
			        		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Specify Other Death Cause may not be null"));		  		    }
			 		    else
			 		    {
			 		    	if(updatePatDemo.getDeathCauseOther().trim().length()>0){
				 		        if(updatePatDemo.getSurvivalStatus() != null && updatePatDemo.getSurvivalStatus().getCode().toString().toUpperCase().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Survival Status code may not be Alive along with Other Death Cause" ));
					 		                throw new OperationException();
				 		    	}
				 		    	if(updatePatDemo.getSurvivalStatus() == null && (CodeCache.getInstance()).getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS,EJBUtil.stringToInteger(perDbBean.getPersonStatus()),EJBUtil.stringToInteger(callingUser.getUserAccountId()).intValue()).getCode().toString().equals("A"))
				 		    	{
				 		    		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
					 		                  new Issue(
					 		                  IssueTypes.DATA_VALIDATION, 
					 		                  "Other Death Cause may not be given as survival status is already provided as Alive" ));
					 		                throw new OperationException();
				 		    	}
				 		    	
			 		    	}
			 		    	perBean.setDthCauseOther(updatePatDemo.getDeathCauseOther().trim());
				 		    
			 		    }
		        		
		        		
		        	}
		        	
		        }
		    	
		      
		      //BloodGroup
		    	if(updatePatDemo.getBloodGroup() == null)
		        {
		        	perBean.setPersonBloodGr(perDbBean.getPersonBloodGr());
		        	System.out.println("--- blood group value fron data base ---- 111"+perBean.getPersonBloodGr());
		        }
		        else
		        {
		        	if((updatePatDemo.getBloodGroup().getCode() == null || updatePatDemo.getBloodGroup().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "bloodgrp")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Blood Group code may not be null"));
		        	}
		 		    else
		 		    {		 		         
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getBloodGroup().getCode() != null && updatePatDemo.getBloodGroup().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "bloodgrp")))
		 		    		{
		 		    			perBean.setPersonBloodGr("");
		 		    			
		 		    		} else {
		 		    			
		 		    			String bloodGroupCode = AbstractService.dereferenceCodeStr(updatePatDemo.getBloodGroup(), "bloodgr", callingUser);	
		 		    			perBean.setPersonBloodGr(bloodGroupCode);
		 		    			
		 		    		}
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Blood Group code Not Found: " ));
			 		                throw new OperationException();
			 		    }
		 		    }
		        }
		      
		        //Marital status
		    	if(updatePatDemo.getMaritalStatus() == null)
		        {
		        	perBean.setPersonMarital(perDbBean.getPersonMarital());
		        }
		        else
		        {
		        	if((updatePatDemo.getMaritalStatus().getCode() == null || updatePatDemo.getMaritalStatus().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "maritalstat")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Marital Status code may not be null"));
		        	}
		 		    else
		 		    {
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getMaritalStatus().getCode() != null && updatePatDemo.getMaritalStatus().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "maritalstat"))){
		 		    			perBean.setPersonMarital("");
		 		    		} else {
		 		    			String maritalStatusCode = AbstractService.dereferenceCodeStr(updatePatDemo.getMaritalStatus(), "marital_st", callingUser);			 		    	
			 		    		perBean.setPersonMarital(maritalStatusCode);
		 		    		}		 		    			    	
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Marital Status code Not Found: " ));
			 		                throw new OperationException();
			 		    }
		 		    }
		        }
		    	
		      
		      //Employment employment
		    	if(updatePatDemo.getEmployment() == null)
		        {
		        	perBean.setPersonEmployment(perDbBean.getPersonEmployment());
		        }
		        else
		        {
		        	if((updatePatDemo.getEmployment().getCode() == null || updatePatDemo.getEmployment().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "employment")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Employment code may not be null"));
		        	}
		 		    else
		 		    {		 		         
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getEmployment().getCode() != null && updatePatDemo.getEmployment().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "employment"))){
		 		    			perBean.setPersonEmployment("");
		 		    		} else {
		 		    			String employmentCode = AbstractService.dereferenceCodeStr(updatePatDemo.getEmployment(), "employment", callingUser);				 		    	
			 		    		perBean.setPersonEmployment(employmentCode);
		 		    		}
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Employment code Not Found: " ));
			 		                throw new OperationException();
			 		    }
		 		    }
		        }
		    	
		    			       
		      //Education
		    	if(updatePatDemo.getEducation() == null)
		        {
		        	perBean.setPersonEducation(perDbBean.getPersonEducation());
		        }
		        else
		        {
		        	if((updatePatDemo.getEducation().getCode() == null || updatePatDemo.getEducation().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "education")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Education code may not be null"));
		        	}
		 		    else
		 		    {		 		    	
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getEducation().getCode() != null && updatePatDemo.getEducation().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "education"))){
		 		    			perBean.setPersonEducation("");
		 		    		} else {
		 		    			String educationCode = AbstractService.dereferenceCodeStr(updatePatDemo.getEducation(), "education", callingUser);
		 		    			perBean.setPersonEducation(educationCode);	    	
		 		    		}
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Education code Not Found: " ));
			 		                throw new OperationException();
			 		    }		 		         
		 		    }
		        }
		    			       
		    	
		      

		     
		    	if(updatePatDemo.geteMail() == null)
		        {
		        	perBean.setPersonEmail(perDbBean.getPersonEmail());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.geteMail().length() == 0 && (isFieldMandatory(configDetails, "email")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Email may not be null"));
			        	}
		        		//Raman Bug#11897
		        		else if(updatePatDemo.geteMail().length() == 0 && (!isFieldMandatory(configDetails, "email")))
			        	{
		        			perBean.setPersonEmail("");
			        	}
			 		    else
			 		    {
			 		    	String email = updatePatDemo.geteMail();
			 		    	try {
			 				      InternetAddress emailAddr = new InternetAddress(email);
			 				      emailAddr.validate();
			 				   } catch (AddressException ex) {
			 				      ex.printStackTrace();
			 				     ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			        	                 new Issue(
			        	                 IssueTypes.DATA_VALIDATION, "Invalid Email Address : " + updatePatDemo.geteMail()));
			        	               throw new OperationException();	
			 				   }
			 		         perBean.setPersonEmail(email);
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Email : " + updatePatDemo.geteMail()));
		        	               throw new OperationException();		        	     
		        	}		        	
		        }
		      
		     
		    	if(updatePatDemo.getAddress1() == null)
		        {
		        	perBean.setPersonAddress1(perDbBean.getPersonAddress1());
		        }
		        else
		        {		        	
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getAddress1().length() == 0 && (isFieldMandatory(configDetails, "address1")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Address1 may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonAddress1(updatePatDemo.getAddress1());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Address1 : " + updatePatDemo.getAddress1()));
		        	               throw new OperationException();		        	     
		        	}
		        }

		     

		    	if(updatePatDemo.getAddress2() == null)
		        {
		        	perBean.setPersonAddress2(perDbBean.getPersonAddress2());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getAddress2().length() == 0 && (isFieldMandatory(configDetails, "address2")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Address2 may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonAddress2(updatePatDemo.getAddress2());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Address2 : " + updatePatDemo.getAddress2()));
		        	               throw new OperationException();		        	     
		        	}
		        }
		      
		    	if(updatePatDemo.getCity() == null)
		        {
		        	perBean.setPersonCity(perDbBean.getPersonCity());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getCity().length() == 0 && (isFieldMandatory(configDetails, "city")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "City may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonCity(updatePatDemo.getCity());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, City : " + updatePatDemo.getCity()));
		        	               throw new OperationException();		        	     
		        	}
		        }

		     
		    	if(updatePatDemo.getState() == null)
		        {
		        	perBean.setPersonState(perDbBean.getPersonState());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getState().length() == 0 && (isFieldMandatory(configDetails, "state")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "State may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonState(updatePatDemo.getState());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, State : " + updatePatDemo.getState()));
		        	               throw new OperationException();
		        	     
		        	}
		        }
		     
		    	
		    	if(updatePatDemo.getCounty() == null)
		        {
		        	perBean.setPersonCounty(perDbBean.getPersonCounty());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getCounty().length() == 0 && (isFieldMandatory(configDetails, "county")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "County may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonCounty(updatePatDemo.getCounty());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, County " + updatePatDemo.getCounty()));
		        	               throw new OperationException();
		        	     
		        	}
		        }
		    	
		      
		    	if(updatePatDemo.getZipCode() == null)
		        {
		        	perBean.setPersonZip(perDbBean.getPersonZip());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getZipCode().length() == 0 && (isFieldMandatory(configDetails, "zipcode")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "ZipCode may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonZip(updatePatDemo.getZipCode());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Zipcode : " + updatePatDemo.getZipCode()));
		        	               throw new OperationException();		        	     
		        	}
		        }
		    	
		    	
		    	if(updatePatDemo.getCountry() == null)
		        {
		        	perBean.setPersonCountry(perDbBean.getPersonCountry());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getCountry().length() == 0 && (isFieldMandatory(configDetails, "country")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Country may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonCountry(updatePatDemo.getCountry());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Counrty : " + updatePatDemo.getCountry()));
		        	               throw new OperationException();		        	     
		        	}
		        }
		    	
		      

		    	if(updatePatDemo.getHomePhone() == null)
		        {
		        	perBean.setPersonHphone(perDbBean.getPersonHphone());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getHomePhone().length() == 0 && (isFieldMandatory(configDetails, "hphone")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Home Phone may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonHphone(updatePatDemo.getHomePhone());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Home Phone : " + updatePatDemo.getHomePhone()));
		        	               throw new OperationException();		        	     
		        	}
		        }
		      

		    	if(updatePatDemo.getWorkPhone() == null)
		        {
		        	perBean.setPersonBphone(perDbBean.getPersonBphone());
		        }
		        else
		        {
		        	if(hasMinPHIRight)
		        	{
		        		if(updatePatDemo.getWorkPhone().length() == 0 && (isFieldMandatory(configDetails, "wphone")))
			        	{
			        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Work Phone may not be null"));
			        	}
			 		    else
			 		    {
			 		         perBean.setPersonBphone(updatePatDemo.getWorkPhone());
			 		    }
		        	}
		        	else
		        	{    
		        	      ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		        	                 new Issue(
		        	                 IssueTypes.GROUP_AUTHORIZATION, " User not authorized to edit PHI data, Work Phone : " + updatePatDemo.getWorkPhone()));
		        	               throw new OperationException();
		        	     
		        	}	
		        }
		      

		      //AdditionalRace
		    	
		    	if(updatePatDemo.getAdditionalRace() == null)
		        {
		    		if(isFieldMandatory(configDetails, "additional2")){
	        			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Additional Race may not be null"));
	        		} else {
	        			perBean.setPersonAddRace(perDbBean.getPersonAddRace());
	        		}
		        } 
		    	else if(updatePatDemo.getAdditionalRace().getCodes() == null){
		    		if(isFieldMandatory(configDetails, "additional2")){
	        			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Additional Race may not be null"));
	        		} else {
	        			perBean.setPersonAddRace("");
	        		}
		        }
		        else
		        {
		        	System.out.println("Race List size: " + updatePatDemo.getAdditionalRace().getCodes().size());
		        	
		 		    	 try
		 		        {
		 		         List<Code> additionalrace = updatePatDemo.getAdditionalRace().getCodes();
		 		         		 		        
		 		         String finalPK = null;
		 		          
		 		         for(int i=0;i<additionalrace.size();i++)
		 		         {
		 		            //Raman Bug#11900   
		 		        	String addPK = null;
		 		        	 if (!StringUtil.isEmpty(additionalrace.get(i).getCode().trim())) {
		 		        		 addPK = AbstractService.dereferenceCodeStr(additionalrace.get(i), "race", callingUser);
		 		        		 }
		 		          if(addPK != null)
		 		          { 
		 		           if(finalPK != null)
		 		           { 
		 		            finalPK = finalPK+","+addPK;
		 		           }
		 		           else
		 		            finalPK = addPK;
		 		          }
		 		        
		 		         }	 		         
		 		         perBean.setPersonAddRace(finalPK);		 		        
		 		        }
		 		         catch (CodeNotFoundException e) {
		 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		 		                  new Issue(
		 		                  IssueTypes.DATA_VALIDATION, 
		 		                  "Additional Race Code Not Found: " ));
		 		                throw new OperationException();
		 		         }
		 		   // }
		        }
		      
		      //AdditionalEthnicity
		    	if(updatePatDemo.getAdditionalEthnicity() == null)
		        {
		    		if(isFieldMandatory(configDetails, "additional1")){
	        			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Additional Ethnicity may not be null"));
	        		} else {
	        			perBean.setPersonAddEthnicity(perDbBean.getPersonAddEthnicity());
	        		}
		        } else if (updatePatDemo.getAdditionalEthnicity().getCodes() == null)
		        {
		        	if(isFieldMandatory(configDetails, "additional1")){
	        			validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Additional Ethnicity may not be null"));
	        		} else {
	        			perBean.setPersonAddEthnicity("");
	        		}
		        }
		        else
		        {
		        	   	 try
			 		        {
			 		         List<Code> additionalethnicity = updatePatDemo.getAdditionalEthnicity().getCodes();			 		         
			 		        
			 		         String finalPK = null;
			 		          
			 		         for(int i=0;i<additionalethnicity.size();i++)
			 		         {
			 		        	String  addPK =null;
			 		           if(!StringUtil.isEmpty(additionalethnicity.get(i).getCode().trim()))
			 		           {
			 		        	  addPK= AbstractService.dereferenceCodeStr(additionalethnicity.get(i), "ethnicity", callingUser);
			 		           }
			 		           
			 		          
			 		          
			 		          if(addPK != null)
			 		          { 
			 		           if(finalPK != null)
			 		           { 
			 		            finalPK = finalPK+","+addPK;
			 		           }
			 		           else
			 		            finalPK = addPK;
			 		          }
			 		         
			 		         }
			 		        perBean.setPersonAddEthnicity(finalPK);
			 		       	 		        
			 		        }
			 		         catch (CodeNotFoundException e) {
			 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "Additional Ethnicity Code Not Found: " ));
			 		                throw new OperationException();
			 		        }
		 		   // }
		        }
		    	
		      
		      //Notes
		    	if(updatePatDemo.getNotes() == null)
		        {
//		    	   	CommonDAO cDao = new CommonDAO();
//	 		        int res=cDao.updateClob(perDbBean.getPersonNotes(), "person", "person_notes_clob", " where pk_person  = " + perDbBean.getPersonPKId());
		    		perBean.setPersonNotes(perDbBean.getPersonNotes());
		        }
		        else
		        {
		        	if(updatePatDemo.getNotes().length() == 0 && (isFieldMandatory(configDetails, "pnotes")))
		        	{
		        		validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "Notes may not be null"));
		  		    }
		 		    else
		 		    {
		 		    	perBean.setPersonNotes(updatePatDemo.getNotes());		 		    
		 		    }
		        }
		    	
		    	//TimeZone
		    	if(updatePatDemo.getTimeZone() == null)
		        {
		    		perBean.setTimeZoneId(perDbBean.getTimeZoneId());
		        }
		        else
		        {
		        	if((updatePatDemo.getTimeZone().getCode() == null || updatePatDemo.getTimeZone().getCode().trim().length()==0) && (isFieldMandatory(configDetails, "timezone")))
		        	{
		        		 validationIssues.add(new Issue(IssueTypes.DATA_VALIDATION, "TimeZone code may not be null"));
		        	}
		 		    else
		 		    {
		 		    	try
		 		    	{
		 		    		if((updatePatDemo.getTimeZone().getCode() != null && updatePatDemo.getTimeZone().getCode().trim().length()==0) && !(isFieldMandatory(configDetails, "timezone"))){
		 		    			perBean.setTimeZoneId("");	
		 		    		} else {
		 		    			String timeZoneCode=AbstractService.dereferenceTzCodeStr(updatePatDemo.getTimeZone(), "timezone", callingUser);
		 		    		
		 		    			perBean.setTimeZoneId(timeZoneCode);		 		    	
		 		    		}
		 		    	}
		 		    	catch (CodeNotFoundException e)
		 		    	{
	 		                ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
			 		                  new Issue(
			 		                  IssueTypes.DATA_VALIDATION, 
			 		                  "TimeZone Code Not Found: " +updatePatDemo.getTimeZone().getCode() ));
			 		                throw new OperationException();
			 		    }
		 		    }
		        }
		    	
		    	//emptyDeathDate
		    	if(updatePatDemo.isEmptyDeathDate())
		    	{
		    		if ( updatePatDemo.getSurvivalStatus()!=null && updatePatDemo.getSurvivalStatus().getCode()!=null && updatePatDemo.getSurvivalStatus().getCode().toUpperCase().equals("DEAD"))
		            {
		        		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
		                  new Issue(IssueTypes.DATA_VALIDATION, "Death Date can not be cleared as Survival status code is provided as Dead"));
		                throw new OperationException();
		            }			        	 			        	
		        	if((updatePatDemo.getSurvivalStatus()==null || updatePatDemo.getSurvivalStatus().getCode()==null) &&(CodeCache.getInstance()).getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS,StringUtil.stringToInteger(perDbBean.getPersonStatus()),StringUtil.stringToInteger(callingUser.getUserAccountId()).intValue()).getCode().toString().equalsIgnoreCase("DEAD") )
		        		{
		        		((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(
				                  new Issue(IssueTypes.DATA_VALIDATION, "Death Date can not be cleared as Survival status code is already provided as Dead"));
				                throw new OperationException();
		        		}
		    		perBean.setPersonDeathDt(null);
		    	}
		      
		        
		    	//morePatientDetails
		    	if (updatePatDemo.getMorePatientDetails()!=null && updatePatDemo.getMorePatientDetails().size()>0) {
		    		
		    		updateMorePatientDetails(updatePatDemo.getMorePatientDetails(),parameters);
		    		
				}
		      

		      if (validationIssues.size() > 0) {
		    	 
		        ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().addAll(validationIssues);

		        if (logger.isDebugEnabled()) logger.debug("Validation Issues found for PatientDemographics");
		        throw new ValidationException();
		      }
		      
		      
		      //setting fields that are not present in UpdatePatientDemographicsBean
		      
		      
		      perBean.setPersonPKId(perDbBean.getPersonPKId());
		      perBean.setPersonAccount(perDbBean.getPersonAccount());
		      perBean.setPersonLocation(perDbBean.getPersonLocation());
		      perBean.setPersonAltId(perDbBean.getPersonAltId());
		      perBean.setPersonSuffix(perDbBean.getPersonSuffix());
		      perBean.setPersonPrefix(perDbBean.getPersonPrefix());
		      perBean.setPersonDegree(perDbBean.getPersonDegree());
		      perBean.setPersonMotherName(perDbBean.getPersonMotherName());
		      perBean.setPersonAlias(perDbBean.getPersonAlias());
		      perBean.setPersonPrimaryLang(perDbBean.getPersonPrimaryLang());
		      perBean.setPersonRelegion(perDbBean.getPersonRelegion());
		      perBean.setPersonSSN(perDbBean.getPersonSSN());
		      perBean.setPersonDrivLic(perDbBean.getPersonDrivLic());
		      perBean.setPersonMotherId(perDbBean.getPersonMotherId());
		      perBean.setPersonEthGroup(perDbBean.getPersonEthGroup());
		      perBean.setPersonBirthPlace(perDbBean.getPersonBirthPlace());
		      perBean.setPersonMultiBirth(perDbBean.getPersonMultiBirth());
		      perBean.setPersonBirthOrder(perDbBean.getPersonBirthOrder());
		      perBean.setPersonCitizen(perDbBean.getPersonCitizen());
		      perBean.setPersonVetMilStatus(perDbBean.getPersonVetMilStatus());
		      perBean.setPersonNationality(perDbBean.getPersonNationality());
		      perBean.setPersonDeathIndicator(perDbBean.getPersonDeathIndicator());
		      perBean.setPersonRegDate(perDbBean.getPersonRegDate());
		      perBean.setPersonRegBy(perDbBean.getPersonRegBy());
		      perBean.setCreator(perDbBean.getCreator());
		      perBean.setModifiedBy(EJBUtil.integerToString(userId));
		      perBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		      perBean.setPhyOther(perDbBean.getPhyOther());
		      perBean.setOrgOther(perDbBean.getOrgOther());
		      perBean.setPersonSplAccess(perDbBean.getPersonSplAccess());
		      perBean.setPatientFacilityId(perDbBean.getPatientFacilityId());
		      //perBean.setTimeZoneId(perDbBean.getTimeZoneId());		      		      
		    }
			return perBean;
		  }
 
	
		public int updatePatientDemographics(UpdatePatientDemographics updatePatDemo,PatientIdentifier patId,Map<String, Object> parameters) throws OperationException
		{
			 
			 UserBean callingUser = (UserBean)parameters.get("callingUser");
			 ResponseHolder response=(ResponseHolder) parameters.get("ResponseHolder");
			 SessionContext sessionContext = (SessionContext)parameters.get("sessionContext");
			 ObjectMapService objectMapService = (ObjectMapService)parameters.get("objectMapService");
			 UserSiteAgentRObj userSiteAgent = (UserSiteAgentRObj)parameters.get("userSiteAgent");
			 AuditRowEpatAgent auditRowEpatAgent = (AuditRowEpatAgent) parameters.get("auditRowEpatAgent");
			 personPk=(Integer) parameters.get("personPk");
			 
			 PersonAgentRObj perAgent=(PersonAgentRObj) parameters.get("personAgent");
			 PersonBean perDbBean=perAgent.getPersonDetails(personPk.intValue());
			 PersonBean perBean=null;
			 
			 perBean=getPersonBeanForUpdate(updatePatDemo,perDbBean, callingUser, parameters);
			 
			 if(perBean!=null)
			 {
				 	UserTransaction utx = sessionContext.getUserTransaction();
				 	try {
						utx.begin();
					} catch (Exception e) {
						throw new OperationException(e);
					} 
				 //Kanwal - added code to Reason for Change 
				 	int rid = auditRowEpatAgent.getRID(personPk, "PERSON", "PK_PERSON");
					int lastRaid = auditRowEpatAgent.findLatestRaid(rid);
					
					int result= perAgent.updatePerson(perBean);
					
					try {
						utx.commit();
					} catch (Exception e) {
						throw new OperationException(e);
					} 
					
					if(perBean.getPersonNotes() != null && perBean.getPersonNotes().length() > 0)
					 {						
		 		       	CommonDAO cDao = new CommonDAO();
		 		        Rlog.debug("person", "person this.getPersonNotes():" + perBean.getPersonNotes());
		 		        int res=cDao.updateClob(perBean.getPersonNotes(), "person", "person_notes_clob", " where pk_person  = " + perDbBean.getPersonPKId());
		 		    }
					
					String remarks = updatePatDemo.getReasonForChange();
					if (!StringUtil.isEmpty(remarks)){
						AuditRowEpatDao auditDao = new AuditRowEpatDao();
						ArrayList listRaids = auditDao.getAllLatestRaids(rid, callingUser.getUserId(), lastRaid);
						
						if(listRaids != null)
						{
							for(int i = 0; i < listRaids.size(); i++)
							{
								int raid = ((Integer)listRaids.get(i)).intValue();
								auditRowEpatAgent.setReasonForChange(raid, remarks);
							}
						}			
					}
					
				
					return result;
			 }			 
			 return -1; 
		}
		
		public static void updateMorePatientDetails(List<NVPair> morePatDetailsList,Map<String, Object> parameters)
			    throws OperationException
			  {
			
			UserBean callingUser=(UserBean) parameters.get("callingUser");
			MorePatientDetailsDAO morePatientDetailsDAO=new MorePatientDetailsDAO();
			Map<Integer, NVPair> map=morePatientDetailsDAO.getMorePatientDetails(personPk,EJBUtil.stringToInteger(callingUser.getUserGrpDefault()));
			PerIdAgentRObj perIdAgentRObj=(PerIdAgentRObj) parameters.get("perIdAgentRObj");
			
			Set<Integer> keys=map.keySet();
			NVPair nvPair=null;
	
			boolean isDetailExist=false;
			int pk_perId=0;
			
			ArrayList<PerIdBean> multiPerIdBeanList=new ArrayList<PerIdBean>();
			PerIdBean perIdBean=null;
			
			
				for (int i = 0; i < morePatDetailsList.size(); i++)
			  	{
					for (Iterator localIterator = keys.iterator(); localIterator.hasNext(); )
					{ 
						pk_perId=((Integer)localIterator.next()).intValue();
						nvPair=map.get(pk_perId);
						
						if(nvPair.getKey().equalsIgnoreCase(morePatDetailsList.get(i).getKey().toString()))
						{
							isDetailExist=true;
							break;
						}
					}
					System.out.println("Iteration : " + i + "  ---personPk: " + personPk );
					System.out.println("morePatDetailsList.get(i).getValue(): " + morePatDetailsList.get(i).getValue() + " ---morePatDetailsList.get(i).getKey(): " + morePatDetailsList.get(i).getKey());
					
					System.out.println("morePatDetailsList.get(i).getType(): " + morePatDetailsList.get(i).getType() );
					System.out.println("nvPair.getKey(): " + nvPair.getKey() + " --- nvPair.getType()" + nvPair.getType() + " ---nvPair.getVAlue(): " + nvPair.getValue() ); 
					
					perIdBean=new PerIdBean();
					perIdBean.setPerId(EJBUtil.integerToString(personPk));
					try
					{
						perIdBean.setPerIdType(AbstractService.dereferenceCodeStr((new Code("peridtype",morePatDetailsList.get(i).getKey())), "peridtype", callingUser));
						
					} catch (CodeNotFoundException cnfe) {
						// TODO: handle exception
						 ((ResponseHolder)parameters.get("ResponseHolder")).getIssues().add(new Issue(IssueTypes.CODE_NOT_FOUND, "More Patient Details code not found"));

					        if (logger.isDebugEnabled()) logger.debug("Validation Issues found for PatientDemographics");
					        throw new ValidationException();
					}
					
					perIdBean.setAlternatePerId(morePatDetailsList.get(i).getValue().toString());
					
					
					if (isDetailExist) 
					{
						perIdBean.setId(pk_perId);
						perIdBean.setRecordType("M");
						perIdBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
						multiPerIdBeanList.add(perIdBean);
					}
					else
					{
						perIdBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
						multiPerIdBeanList.add(perIdBean);
					} 
					
				isDetailExist=false;
				pk_perId=0;
			}
							
			PerIdBean multiPerIdBean=new PerIdBean();
			multiPerIdBean.setPerIdStateKeepers(multiPerIdBeanList);
			int res=perIdAgentRObj.createMultiplePerIds(multiPerIdBean);
			System.out.println("result of update MPD "+res);
				
	}
		
		public ResponseHolder UpdateMPatientDemographics(List<UpdatePatientDemographics> patientDemographicsList, Map<String, Object> parameters) throws OperationException
		{
			UserBean callingUser = (UserBean)parameters.get("callingUser");
			SessionContext sessionContext = (SessionContext)parameters.get("sessionContext");
			AuditRowEpatAgent auditRowEpatAgent = (AuditRowEpatAgent) parameters.get("auditRowEpatAgent");
			PersonAgentRObj personAgent=(PersonAgentRObj) parameters.get("personAgent");
			ResponseHolder response=(ResponseHolder) parameters.get("ResponseHolder");
			PersonBean perDbBean=null;
			PersonBean perBean=null;
			UserTransaction utx = sessionContext.getUserTransaction();
		 	try {
				utx.begin();
			} catch (Exception e) {
				throw new OperationException(e);
			}
		 	int rid=0;int lastRaid =0;int result=0;
		 	PatientIdentifier patId=null;
			for(UpdatePatientDemographics updatePatDemo:patientDemographicsList)
			{
				patId=null;
				patId=updatePatDemo.getPatientIdentifier();
				personPk=patId.getPK();
				perDbBean=null;
				perDbBean=personAgent.getPersonDetails(personPk.intValue());
				perBean=null;
				perBean=getPersonBeanForUpdate(updatePatDemo,perDbBean, callingUser, parameters);
				if(perBean!=null)
				 {
					 rid = auditRowEpatAgent.getRID(personPk, "PERSON", "PK_PERSON");
					 lastRaid = auditRowEpatAgent.findLatestRaid(rid);
					 result= personAgent.updatePerson(perBean);
					 if(perBean.getPersonNotes() != null && perBean.getPersonNotes().length() > 0)
					 {						
			 		       	CommonDAO cDao = new CommonDAO();
			 		        Rlog.debug("person", "person this.getPersonNotes():" + perBean.getPersonNotes());
			 		        int res=cDao.updateClob(perBean.getPersonNotes(), "person", "person_notes_clob", " where pk_person  = " + perDbBean.getPersonPKId());
			 		 }
					 String remarks = updatePatDemo.getReasonForChange();
					 if (!StringUtil.isEmpty(remarks))
					 {
						 AuditRowEpatDao auditDao = new AuditRowEpatDao();
						 ArrayList listRaids = auditDao.getAllLatestRaids(rid, callingUser.getUserId(), lastRaid);
						 if(listRaids != null)
						 {
							for(int i = 0; i < listRaids.size(); i++)
							{
								int raid = ((Integer)listRaids.get(i)).intValue();
								auditRowEpatAgent.setReasonForChange(raid, remarks);
							}
						 }
					 }
				}
				if (result==0)
			    {
			    	response.addAction(new CompletedAction("Patient with pk : "+patId.getPK()
			    				 +" OID : "+patId.getOID()
			    				 +" patientId : "+patId.getPatientId()
			    				 +" siteName"+patId.getOrganizationId().getSiteName()+ "  updated successfully", CRUDAction.UPDATE));
			    }
			    else
			    {
			    	response.addIssue(new Issue(IssueTypes.PATIENT_ERROR_UPDATE_DEMOGRAPHICS));
			    }
			}
			try {
				utx.commit();
			} catch (Exception e) {
				throw new OperationException(e);
			}
			return response;
		}
}
