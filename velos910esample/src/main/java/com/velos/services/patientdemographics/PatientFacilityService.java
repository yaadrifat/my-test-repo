package com.velos.services.patientdemographics;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientFacilities;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;

@Remote
public interface PatientFacilityService
{
			ResponseHolder addPatientFacility(PatientIdentifier patId, PatientOrganization patOrg) throws OperationException;
			ResponseHolder updatePatientFacility(PatientIdentifier patIdentifier,
			OrganizationIdentifier orgID,
			PatientOrganizationIdentifier paramOrganizationIdentifier,
			PatientOrganization paramPatientOrganization) throws OperationException;
			PatientFacilities getPatientFacilities(PatientIdentifier patientIdentifier) throws OperationException;
}
