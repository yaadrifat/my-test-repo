/**
 * 
 */
package com.velos.services;

import com.velos.services.model.ServiceObject;

/**
 * @author dylan
 *
 */
public class PersistAction {
	protected CRUDAction actionType;
	
	protected ServiceObject serviceObject;
	
	protected Integer primaryKey;

	public PersistAction(
			CRUDAction actionType, 
			ServiceObject serviceObject,
			Integer primaryKey) {
		super();
		this.actionType = actionType;
		this.serviceObject = serviceObject;
		this.primaryKey = primaryKey;
	}

	public PersistAction(CRUDAction actionType, Integer primaryKey) {
		super();
		this.actionType = actionType;
		this.primaryKey = primaryKey;
	}

	public PersistAction(CRUDAction actionType, ServiceObject serviceObject) {
		super();
		this.actionType = actionType;
		this.serviceObject = serviceObject;
	}

	public CRUDAction getActionType() {
		return actionType;
	}

	public void setActionType(CRUDAction actionType) {
		this.actionType = actionType;
	}

	public ServiceObject getServiceObject() {
		return serviceObject;
	}

	public void setServiceObject(ServiceObject serviceObject) {
		this.serviceObject = serviceObject;
	}

	public Integer getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(Integer primaryKey) {
		this.primaryKey = primaryKey;
	}
	
	
	
}
