package com.velos.services.budget;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.common.BudgetUsersDao;
import com.velos.services.map.MultipleObjectsFoundException;

public class BudgetServiceDAO extends CommonDAO{
	
	/**
	 * 
	 */
	

	//By Virendra for budget status
	/**
	 * 
	 * @param budgetName
	 * @return
	 */
	public Integer locateBudgetPK(String budgetName, String versionNumber, String accountNumber) throws MultipleObjectsFoundException{
		Integer budgetPK = 0;
		Connection conn = null; 
		PreparedStatement stmt = null ; 
		ResultSet rs = null; 
		
		StringBuffer sql = new StringBuffer(); 
		sql.append("select pk_budget from sch_budget where budget_name = ? ");  
		sql.append(" and FK_ACCOUNT = ?"); 
		if(versionNumber != null && versionNumber.length() > 0){
			sql.append(" and  BUDGET_VERSION = ?"); 
		}else
		{
			sql.append(" and BUDGET_VERSION is null"); 
		}
		
		sql.append(" and (BUDGET_DELFLAG is null or BUDGET_DELFLAG = 'N')"); 
				
		
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(sql.toString()); 
			stmt.setString(1, budgetName); 
			stmt.setString(2, accountNumber); 
			if(versionNumber != null && versionNumber.length() > 0) stmt.setString(3, versionNumber);
						
			rs = stmt.executeQuery(); 
			
			int ct = 0; 
			while(rs.next())
			{
				budgetPK = rs.getInt(1); 
				ct++; 
			}
			
			if(ct > 1) throw new MultipleObjectsFoundException("Found multiple Objects for BudgetName "+ budgetName + " and BudgetVersion "+ versionNumber);
					
		
		}catch(SQLException sqe)
		{
			// TODO Auto-generated catch block
			sqe.printStackTrace();
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}
		return budgetPK;
	}
	
	// Check these rights after checking Group rights for non-combined budgets and study team rights for combined Budgets . 
	public String getAccessRightsForBudget(BudgetBean budgetBean, UserBean user)
	{
		String budgetRights = "000"; 		
		PreparedStatement stmt = null; 
		Connection conn = null ; 
		ResultSet rs = null; 
		
		try
		{
			String sql = "select grp_SUPBUD_rights from er_grps where pk_grp=? and grp_supbud_flag = 1" ;
			
			conn = getConnection(); 

			stmt = conn.prepareStatement(sql); 

			stmt.setString(1, user.getUserGrpDefault()); 		

			rs = stmt.executeQuery();

			if(rs.next())
			{				
				budgetRights = rs.getString(1);				
			}else
			{
				if(budgetBean.getBudgetCombinedFlag() == null)
				{
					String scope = budgetBean.getBudgetRScope(); 
					if(!StringUtil.isEmpty(scope))
					{
						if(scope.equalsIgnoreCase("A"))
						{
							if(budgetBean.getBudgetAccountId().equalsIgnoreCase(user.getUserAccountId()))
							{
								budgetRights = budgetBean.getBudgetRights();								
							}
						}else if(scope.equalsIgnoreCase("O"))
						{
							if(budgetBean.getBudgetSiteId().equalsIgnoreCase(user.getUserSiteId()))
							{
								budgetRights = budgetBean.getBudgetRights(); 							
							}
						}else if(scope.equalsIgnoreCase("S"))
						{
							// check if user is in studyTeam. 
							String studyID = budgetBean.getBudgetStudyId(); 
							TeamDao teamDao = new TeamDao(); 
							teamDao.findUserInTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId()),user.getUserId());
							if (teamDao.getCRows() > 0) {
								budgetRights = budgetBean.getBudgetRights(); 							
							}else
							{
								teamDao.getSuperUserTeam(EJBUtil.stringToNum(budgetBean.getBudgetStudyId())); 
								if(teamDao.getUserIds().contains(user.getUserId()))
								{
									
									budgetRights = budgetBean.getBudgetRights(); 									
								}
							}

						}
					}	
				}
			}
			//individual user access right overrides grpRight and scopeRight - not applicable to combined Budgets
			if(budgetBean.getBudgetCombinedFlag() == null)
			{
				BudgetUsersDao bgtUsersDao = new BudgetUsersDao();	            
	            String userRightStr = bgtUsersDao.getBudgetUserRight(budgetBean.getBudgetId(), user.getUserId());
			    if (!StringUtil.isEmpty(userRightStr)) {
			    	budgetRights = userRightStr; 			    
			    }	           
			}			
		}catch(SQLException sqe)
		{
			
		}finally
		{
			closeResultSet(rs); 
			closeStatement(stmt); 
			returnConnection(conn); 
		}		
		return budgetRights;
	}
	
	private void closeStatement(PreparedStatement stmt)
	{
		if(stmt != null)
		{
			try{
				stmt.close(); 
			}catch(SQLException sqe)
			{
				
			}
		}
	}
	
	private void closeResultSet(ResultSet rs)
	{
		if(rs != null)
		{
			try
			{
				rs.close(); 
			}catch(SQLException sqe)
			{
				
			}
		}
		
	}

}
