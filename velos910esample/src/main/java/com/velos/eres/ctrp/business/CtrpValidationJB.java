package com.velos.eres.ctrp.business;

import java.io.Serializable;

public class CtrpValidationJB implements Serializable {

	private String label = null;
	private String value = null;
	private String type = null;
	private String validationMsg = null;
	
	CtrpValidationJB(String label,String value,String type,String validationMsg)
	{
		this.label = label;
		this.value = value;
		this.type = type;
		this.validationMsg = validationMsg;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValidationMsg() {
		return validationMsg;
	}
	public void setValidationMsg(String validationMsg) {
		this.validationMsg = validationMsg;
	}
	
	
	

}
