package com.velos.eres.ctrp.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.naming.InitialContext;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.service.CtrpDraftAgent;
import com.velos.eres.ctrp.web.CtrpDraftValidationPojo;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VelosResourceBundle;

public class CtrpDraftValidationJB {
	private Integer draftId;
	private Integer loggedUser;
	private LinkedHashMap<String,ArrayList<CtrpDraftValidationPojo>> validationMsgMap = null;
	private CtrpDraftBean draftBean = null;
	private StudyBean studyBean = null;
	private CtrpDraftJB ctrpDraftJB = null;
	private CtrpDraftSectSponsRespJB ctrpDraftSectSponsRespJB = null;
	
	public static final String EMPTY_STRING = "";
	public static final String LETTER_A = "A";
	public static final String LETTER_O = "O";
	public static final String LETTER_U = "U";
	public static final String LETTER_NA = "NA";
	public static final String STRING_OTHER = "other";
	
	@SuppressWarnings("unused")
	private CtrpDraftValidationJB() {}
	
	public CtrpDraftValidationJB(Integer draftId) {
		this.draftId = draftId;
		this.validationMsgMap = new LinkedHashMap<String,ArrayList<CtrpDraftValidationPojo>> ();
		this.ctrpDraftJB = new CtrpDraftJB();
		this.ctrpDraftSectSponsRespJB = new CtrpDraftSectSponsRespJB();
		ctrpDraftJB.setCtrpDraftSectSponsorRespJB(ctrpDraftSectSponsRespJB);
	}
	
	public LinkedHashMap<String,ArrayList<CtrpDraftValidationPojo>> getValidationMsgMap() {
		return validationMsgMap;
	}
	
	private String getCodelstDescriptionByPk(Integer codePk) {
		if (codePk == null) { return EMPTY_STRING; }
		CodeDao codeDao = new CodeDao();
		String desc = codeDao.getCodeDescription(codePk);
		if (desc == null) { return EMPTY_STRING; }
		return desc;
	}
	
	private String getCodelstSubtypeByPk(Integer codePk) {
		if (codePk == null) { return EMPTY_STRING; }
		CodeDao codeDao = new CodeDao();
		String desc = codeDao.getCodeSubtype(codePk);
		if (desc == null) { return EMPTY_STRING; }
		return desc;
	}
	
	private SiteBean getSiteBean(int fkSite) throws Exception {
		InitialContext ic = new InitialContext();
		SiteAgentRObj siteAgent = (SiteAgentRObj)ic.lookup(JNDINames.SiteAgentHome);
		return siteAgent.getSiteDetails(fkSite);
	}
	
	private UserBean getUserBean(int fkUser) throws Exception {
		InitialContext ic = new InitialContext();
		UserAgentRObj userAgent = (UserAgentRObj)ic.lookup(JNDINames.UserAgentHome);
		return userAgent.getUserDetails(fkUser);
	}
	
	private AddressBean getAddressBean(int fkAdd) throws Exception {
		InitialContext ic = new InitialContext();
		AddressAgentRObj addressAgent = (AddressAgentRObj)ic.lookup(JNDINames.AddressAgentHome);
		return addressAgent.getAddressDetails(fkAdd);
	}
	
	private void fillSiteDetails(int fkSite, int fkAdd,
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		SiteBean siteBean = this.getSiteBean(fkSite);
		validationList.add(new CtrpDraftValidationPojo(LC.L_Name, true,
				StringUtil.trueValue(siteBean.getSiteName()), 
				siteBean.getSiteName() == null ? MC.M_FieldRequired : null));
		this.fillAddDetails(fkAdd, validationList);
	}
	
	private void fillUserDetails(int fkUser, int fkAdd,
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		UserBean userBean = this.getUserBean(fkUser);
		validationList.add(new CtrpDraftValidationPojo(LC.L_First_Name, true,
				StringUtil.trueValue(userBean.getUserFirstName()),
				userBean.getUserFirstName() == null ? MC.M_FieldRequired : null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Middle_Name, false,
				StringUtil.trueValue(userBean.getUserMidName()),
				null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Last_Name, true,
				StringUtil.trueValue(userBean.getUserLastName()),
				userBean.getUserLastName() == null ? MC.M_FieldRequired : null));
		this.fillAddDetails(fkAdd, validationList);
	}
	
	private void fillAddDetails(int fkAdd,
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		AddressBean addressBean = this.getAddressBean(fkAdd);
		validationList.add(new CtrpDraftValidationPojo(LC.L_Street_Address, true,
				StringUtil.trueValue(addressBean.getAddPri()),
				addressBean.getAddPri() == null ? MC.M_FieldRequired : null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_City, true,
				StringUtil.trueValue(addressBean.getAddCity()),
				addressBean.getAddCity() == null ? MC.M_FieldRequired : null));
		validationList.add(new CtrpDraftValidationPojo(LC.CTRP_DraftStateProvince, true,
				StringUtil.trueValue(addressBean.getAddState()),
				addressBean.getAddState() == null ? MC.M_FieldRequired : null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Zip_Code, true,
				StringUtil.trueValue(addressBean.getAddZip()),
				addressBean.getAddZip() == null ? MC.M_FieldRequired : null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Country, true,
				StringUtil.trueValue(addressBean.getAddCountry()),
				addressBean.getAddCountry() == null ? MC.M_FieldRequired : null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Email_Addr, true,
				StringUtil.trueValue(addressBean.getAddEmail()),
				addressBean.getAddEmail() == null ? MC.M_FieldRequired : null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Phone, false,
				StringUtil.trueValue(addressBean.getAddPhone()),
				null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_TTY, false,
				StringUtil.trueValue(addressBean.getAddTty()),
				null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Fax, false,
				StringUtil.trueValue(addressBean.getAddFax()),
				null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Url_Upper, false,
				StringUtil.trueValue(addressBean.getAddUrl()),
				null));
	}
	
	private void fillOptionalSiteDetails(int fkSite, int fkAdd,
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		SiteBean siteBean = getSiteBean(fkSite);
		validationList.add(new CtrpDraftValidationPojo(LC.L_Name, false,
				StringUtil.trueValue(siteBean.getSiteName()), null));
		this.fillOptionalAddDetails(fkAdd, validationList);
	}
	
	private void fillOptionalUserDetails(int fkUser, int fkAdd,
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		UserBean userBean = this.getUserBean(fkUser);
		validationList.add(new CtrpDraftValidationPojo(LC.L_First_Name, false,
				StringUtil.trueValue(userBean.getUserFirstName()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Middle_Name, false,
				StringUtil.trueValue(userBean.getUserMidName()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Last_Name, false,
				StringUtil.trueValue(userBean.getUserLastName()), null));
		this.fillOptionalAddDetails(fkAdd, validationList);
	}
	
	private void fillOptionalAddDetails(int fkAdd,
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		AddressBean addressBean = this.getAddressBean(fkAdd);
		validationList.add(new CtrpDraftValidationPojo(LC.L_Street_Address, false,
				StringUtil.trueValue(addressBean.getAddPri()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_City, false,
				StringUtil.trueValue(addressBean.getAddCity()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.CTRP_DraftStateProvince, false,
				StringUtil.trueValue(addressBean.getAddState()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Zip_Code, false,
				StringUtil.trueValue(addressBean.getAddZip()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Country, false,
				StringUtil.trueValue(addressBean.getAddCountry()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Email_Addr, false,
				StringUtil.trueValue(addressBean.getAddEmail()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Phone, false,
				StringUtil.trueValue(addressBean.getAddPhone()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_TTY, false,
				StringUtil.trueValue(addressBean.getAddTty()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Fax, false,
				StringUtil.trueValue(addressBean.getAddFax()), null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Url_Upper, false,
				StringUtil.trueValue(addressBean.getAddUrl()), null));
	}
	
	private void fillEmptySiteDetails(
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		validationList.add(new CtrpDraftValidationPojo(LC.L_Name, false,
				EMPTY_STRING, null));
		this.fillEmptyAddDetails(validationList);
	}
	
	private void fillEmptyUserDetails(
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		validationList.add(new CtrpDraftValidationPojo(LC.L_First_Name, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Middle_Name, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Last_Name, false,
				EMPTY_STRING, null));
		this.fillEmptyAddDetails(validationList);
	}
	
	private void fillEmptyAddDetails(
			ArrayList<CtrpDraftValidationPojo> validationList) throws Exception {
		validationList.add(new CtrpDraftValidationPojo(LC.L_Street_Address, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_City, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.CTRP_DraftStateProvince, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Zip_Code, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Country, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Email_Addr, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Phone, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_TTY, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Fax, false,
				EMPTY_STRING, null));
		validationList.add(new CtrpDraftValidationPojo(LC.L_Url_Upper, false,
				EMPTY_STRING, null));
	}
	
	/**
	 * Run validation.
	 * @return -1 if fail; 0 if success.
	 */
	public Integer runValidation(CtrpDraftValidationJB validationJB) {
		int out = -1;
		if (draftId == null || draftId < 1) {
			return out;
		}
		try {
			ctrpDraftJB.setLoggedUser(validationJB.getLoggedUser());
			ctrpDraftJB.retrieveCtrpDraft(this.draftId);

			CtrpDraftAgent ctrpDraftAgent = EJBUtil.getCtrpDraftAgentHome();
			this.draftBean = ctrpDraftAgent.findByDraftId(this.draftId);
			if (draftBean.getId() < 1) {
				throw new Exception("No draft present");	
			}
			if (draftBean.getFkStudy() < 1) {
				throw new Exception("No study info present for draft ID="+draftBean.getId());
			}
			if (ctrpDraftJB.getId() < 1) {
				throw new Exception("No draft present");	
			}
			if (StringUtil.stringToNum(ctrpDraftJB.getFkStudy()) < 1) {
				throw new Exception("No study info present for draft ID="+draftBean.getId());
			}
			ctrpDraftJB.retreiveCtrpDocumentInfo(this.draftId);
			ctrpDraftJB.retrieveStudy(StringUtil.stringToNum(ctrpDraftJB.getFkStudy()));
			
			Integer draftType = this.draftBean.getCtrpDraftType(); // 1-Industrial,0-Non-Industrial
			if (draftType == null) {
				throw new Exception("No draft type present");
			}
			
			Integer studyId = draftBean.getFkStudy();
			StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
			this.studyBean = studyAgent.getStudyDetails(studyId);
			
			// Figure out industrial vs. non-industrial and delegate the rest to the respective runValidation method
			if (draftType == 1) {
				out = this.runValidationIndustrial();
			} else if (draftType == 0) {
				out = this.runValidationNonIndustrial();
			}
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftValidationJB.runValidation():"+e);
			out = -1;
		}
		return out;
	}
	
	private int runValidationIndustrial() {
		int out = -1;
		try {
			this.validateIndusTrialSubmType();
			this.validateIndusTrialIds();
			this.validateIndusTrialDetails();
			this.validateIndusLeadOrgPi();
			this.validateSectSumm4();
			this.validateIndusStatusDates();
			this.validateIndusTrialDocs();
			out = 0;
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftValidationJB.runValidationIndustrial() for draftId="+draftId+" :"+e);
			e.printStackTrace();
			out = -1;
		}
		return out;
	}

	private int runValidationNonIndustrial() throws Exception {
		int out = -1;
		try {
			this.validateSectTrialSubmType();
			this.validateSectTrialIds();
			this.validateSectTrialDetails();
			this.validateSectLeadOrgPi();
			this.validateSectSponsResp();
			this.validateSectSumm4();
			this.validateSectNihGrants();
			this.validateSectStatusDates();
			this.validateSectFdaIndIde();
			this.validateSectRegulatoryInfo();
			this.validateSectTrialDocs();
			out = 0;
		} catch(Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftValidationJB.runValidationNonIndustrial() for draftId="+draftId+" :"+e);
			e.printStackTrace();
			out = -1;
		}
		return out;
	}
	
	private void validateIndusTrialSubmType() {
		ArrayList<CtrpDraftValidationPojo> validationMsgSubmType = new ArrayList<CtrpDraftValidationPojo>();

		if (ctrpDraftJB.getTrialSubmTypeInt() == null || ctrpDraftJB.getTrialSubmTypeInt() < 1) {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSectSubmType, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSectSubmType, true, 
					this.getCodelstDescriptionByPk(ctrpDraftJB.getTrialSubmTypeInt()), null));
		}
		
		validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftTrialOwner,true, false, 
				EMPTY_STRING, null));
		
		if (StringUtil.isEmpty(ctrpDraftJB.getTrialFirstName())) {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.L_First_Name, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.L_First_Name, true, 
					ctrpDraftJB.getTrialFirstName(), null));
		}
		if (StringUtil.isEmpty(ctrpDraftJB.getTrialLastName())) {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.L_Last_Name, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.L_Last_Name, true, 
					ctrpDraftJB.getTrialLastName(), null));
		}
		if (StringUtil.isEmpty(ctrpDraftJB.getTrialEmailId())) {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.L_Email_Addr, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.L_Email_Addr, true, 
					ctrpDraftJB.getTrialEmailId(), null));
		}
		
		validationMsgMap.put(LC.CTRP_DraftTrialSubmSec, validationMsgSubmType);
	}
	
	private void validateIndusTrialIds() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgTrialIds = 
			new ArrayList<CtrpDraftValidationPojo>();
		String submTypeSubtyp = this.getCodelstSubtypeByPk(ctrpDraftJB.getTrialSubmTypeInt());
		if (!LETTER_O.equals(submTypeSubtyp)) {
			if (ctrpDraftJB.getNciTrialId() == null) {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, true, 
						StringUtil.trueValue(ctrpDraftJB.getNciTrialId()), null));
			}
		} else {
			validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, false, 
					StringUtil.trueValue(ctrpDraftJB.getNciTrialId()), null));
		}
		if (ctrpDraftJB.getLeadOrgTrialId() == null) {
			validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, true, 
					StringUtil.trueValue(ctrpDraftJB.getLeadOrgTrialId()), null));
		}
		validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSubOrgLclIdentfy, false, 
				StringUtil.trueValue(ctrpDraftJB.getSubOrgTrialId()), null));
		validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCTNumber, false, 
				StringUtil.trueValue(ctrpDraftJB.getNctNumber()), null));
		
		validationMsgMap.put(LC.CTRP_DraftTrialIdenti, validationMsgTrialIds);
	}
	
	private void validateIndusTrialDetails() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgTrialDetails = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.L_Title, false, 
				StringUtil.trueValue(ctrpDraftJB.getStudyTitle()), null));
		
		// Bug #8624
		if(ctrpDraftJB.getNctNumber()!= null && !ctrpDraftJB.getNctNumber().isEmpty()){
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.L_Phase, true, 
					this.getCodelstDescriptionByPk(ctrpDraftJB.getDraftStudyPhase()), null));
			
		}else{
			if (ctrpDraftJB.getDraftStudyPhase() == null || ctrpDraftJB.getDraftStudyPhase() < 1) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.L_Phase, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			}else {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.L_Phase, true, 
						this.getCodelstDescriptionByPk(ctrpDraftJB.getDraftStudyPhase()), null));
			}	
		}
		 
		validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPhaseColon, false, 
				StringUtil.trueValue(ctrpDraftJB.getStudyPhase()), null));
		if (String.valueOf(ctrpDraftJB.getDraftStudyPhase()).equals(ctrpDraftJB.getDraftStudyPhaseNA())) {
			if (ctrpDraftJB.getDraftPilot() == null) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPilotTrial,
						true, EMPTY_STRING, MC.M_FieldRequired));
			} else {
				ArrayList<RadioOption> draftPilotList = 
					(ArrayList<RadioOption>)ctrpDraftJB.getDraftPilotList();
				String draftPilotText = null;
				for (RadioOption option : draftPilotList) {
					if (option.getRadioCode() != null 
							&& option.getRadioCode().equals(ctrpDraftJB.getDraftPilot())) {
						draftPilotText = option.getRadioDisplay();
						break;
					}
				}
				draftPilotText = draftPilotText == null ? EMPTY_STRING : draftPilotText;
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPilotTrial,
						true, draftPilotText, null));
			}
		}
		Integer draftStudyType = ctrpDraftJB.getDraftStudyType();
		if (draftStudyType == null) {
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftTrialType, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			ArrayList<RadioOption> draftStudyTypeList = 
				(ArrayList<RadioOption>)ctrpDraftJB.getDraftStudyTypeList();
			String studyTypeText = null;
			for (RadioOption option : draftStudyTypeList) {
				if (option.getRadioCode() != null && option.getRadioCode().equals(draftStudyType)) {
					studyTypeText = option.getRadioDisplay();
					break;
				}
			}
			studyTypeText = studyTypeText == null ? EMPTY_STRING : studyTypeText;
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftTrialType, true, 
					studyTypeText, null));
		}
		validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftTrialTypeColon, false, 
				StringUtil.trueValue(ctrpDraftJB.getStudyType()), null));
		if (StringUtil.isEmpty(ctrpDraftJB.getNctNumber())) {
			if (ctrpDraftJB.getCtrpIntvnTypeInt() == null || ctrpDraftJB.getCtrpIntvnTypeInt() < 1) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIntervenType, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIntervenType, true, 
						this.getCodelstDescriptionByPk(ctrpDraftJB.getCtrpIntvnTypeInt()), null));
			}
			if (StringUtil.isEmpty(ctrpDraftJB.getInterventionName())) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIntervenName, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIntervenName, true, 
						ctrpDraftJB.getInterventionName(), null));
			}
			if (StringUtil.isEmpty(ctrpDraftJB.getStudyDiseaseName())) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftDiseaseName, true, 
						EMPTY_STRING, MC.M_FieldRequired+" "+MC.CTRP_DraftPlsUpdtStudy));
			} else {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftDiseaseName, true, 
						ctrpDraftJB.getStudyDiseaseName(), null));
			}
			
			// Bug #8624
			if(ctrpDraftJB.getNctNumber()!= null && !ctrpDraftJB.getNctNumber().isEmpty()){
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurpose, true, 
						this.getCodelstDescriptionByPk(ctrpDraftJB.getStudyPurpose()), null));
				if (STRING_OTHER.equals(this.getCodelstSubtypeByPk(ctrpDraftJB.getStudyPurpose()))) {
					if (StringUtil.isEmpty(ctrpDraftJB.getStudyPurposeOther())) {
						validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurposeOther, true, 
								EMPTY_STRING, MC.M_FieldRequired));
					} else {
						validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurposeOther, true, 
								ctrpDraftJB.getStudyPurposeOther(), null));
					}
				}
			} else {
				if (ctrpDraftJB.getStudyPurpose() == null
						|| ctrpDraftJB.getStudyPurpose() < 1) {
					validationMsgTrialDetails.add(new CtrpDraftValidationPojo(
							LC.CTRP_DraftPurpose, true, EMPTY_STRING,
							MC.M_FieldRequired));
				} else {
					validationMsgTrialDetails.add(new CtrpDraftValidationPojo(
							LC.CTRP_DraftPurpose, true, this
									.getCodelstDescriptionByPk(ctrpDraftJB
											.getStudyPurpose()), null));
					if (STRING_OTHER.equals(this
							.getCodelstSubtypeByPk(ctrpDraftJB
									.getStudyPurpose()))) {
						if (StringUtil.isEmpty(ctrpDraftJB
								.getStudyPurposeOther())) {
							validationMsgTrialDetails
									.add(new CtrpDraftValidationPojo(
											LC.CTRP_DraftPurposeOther, true,
											EMPTY_STRING, MC.M_FieldRequired));
						} else {
							validationMsgTrialDetails
									.add(new CtrpDraftValidationPojo(
											LC.CTRP_DraftPurposeOther, true,
											ctrpDraftJB.getStudyPurposeOther(),
											null));
						}
					}
				}
			}
		} else {
			if (ctrpDraftJB.getCtrpIntvnTypeInt() == null || ctrpDraftJB.getCtrpIntvnTypeInt() < 1) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIntervenType, false, 
						EMPTY_STRING, null));
			} else {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIntervenType, false, 
						this.getCodelstDescriptionByPk(ctrpDraftJB.getCtrpIntvnTypeInt()), null));
			}
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIntervenName, false, 
					StringUtil.trueValue(ctrpDraftJB.getInterventionName()), null));
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftDiseaseName, false, 
					StringUtil.trueValue(ctrpDraftJB.getStudyDiseaseName()), null));
			if (ctrpDraftJB.getStudyPurpose() == null || ctrpDraftJB.getStudyPurpose() < 1) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurpose, false, 
						EMPTY_STRING, null));
			} else {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurpose, false, 
						this.getCodelstDescriptionByPk(ctrpDraftJB.getStudyPurpose()), null));
				if (STRING_OTHER.equals(this.getCodelstSubtypeByPk(ctrpDraftJB.getStudyPurpose()))) {
					if (StringUtil.isEmpty(ctrpDraftJB.getStudyPurposeOther())) {
						validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurposeOther, true, 
								EMPTY_STRING, MC.M_FieldRequired));
					} else {
						validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurposeOther, true, 
								ctrpDraftJB.getStudyPurposeOther(), null));
					}
				}
			}
		}
		
		validationMsgMap.put(LC.CTRP_DraftSectTrialDetails, validationMsgTrialDetails);
	}
	
	private void validateIndusLeadOrgPi() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgLeadOrgPi = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		if (ctrpDraftJB.getLeadOrgCtrpId() == null && ctrpDraftJB.getLeadOrgFkSite() == null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
					"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics",LC.L_Id_Upper)+"</b>",
					false, EMPTY_STRING, null));
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(EMPTY_STRING, true,
					EMPTY_STRING, MC.CTRP_LeadOrgEnterOrSelect.replaceAll("<br/>", EMPTY_STRING).replaceAll("<br>", EMPTY_STRING)));
			this.fillEmptySiteDetails(validationMsgLeadOrgPi);
		} else {
			if (ctrpDraftJB.getLeadOrgCtrpId() != null) {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics",LC.L_Id_Upper)+"</b>", false,
						ctrpDraftJB.getLeadOrgCtrpId(), null));
				if (ctrpDraftJB.getLeadOrgFkSite() != null && ctrpDraftJB.getLeadOrgFkAdd() != null) {
					this.fillOptionalSiteDetails(Integer.parseInt(ctrpDraftJB.getLeadOrgFkSite()), 
							Integer.parseInt(ctrpDraftJB.getLeadOrgFkAdd()), validationMsgLeadOrgPi);
				} else {
					this.fillEmptySiteDetails(validationMsgLeadOrgPi);
				}
			} else {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics",LC.L_Id_Upper)+"</b>", false,
						EMPTY_STRING, null));
				this.fillSiteDetails(Integer.parseInt(ctrpDraftJB.getLeadOrgFkSite()), 
						Integer.parseInt(ctrpDraftJB.getLeadOrgFkAdd()), validationMsgLeadOrgPi);
			}
		}
		if (ctrpDraftJB.getLeadOrgTypeInt() != null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
					VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics",LC.L_Type), false,
					this.getCodelstDescriptionByPk(ctrpDraftJB.getLeadOrgTypeInt()), null));
		} else {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
					VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics",LC.L_Type), false,
					EMPTY_STRING, MC.M_FieldRequired));
		}
		
		if (ctrpDraftJB.getSubOrgCtrpId() == null && ctrpDraftJB.getSubOrgFkSite() == null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
					"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSubmitOrgSpecifics",LC.L_Id_Upper)+"</b>",
					false, EMPTY_STRING, null));
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(EMPTY_STRING, true,
					EMPTY_STRING, MC.CTRP_LeadOrgEnterOrSelect.replaceAll("<br/>", EMPTY_STRING).replaceAll("<br>", EMPTY_STRING)));
			this.fillEmptySiteDetails(validationMsgLeadOrgPi);
		} else {
			if (ctrpDraftJB.getSubOrgCtrpId() != null) {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSubmitOrgSpecifics",LC.L_Id_Upper)+"</b>",
						false, ctrpDraftJB.getSubOrgCtrpId(), null));
				if (ctrpDraftJB.getSubOrgFkSite() != null && ctrpDraftJB.getSubOrgFkAdd() != null) {
					this.fillOptionalSiteDetails(Integer.parseInt(ctrpDraftJB.getSubOrgFkSite()), 
							Integer.parseInt(ctrpDraftJB.getSubOrgFkAdd()), validationMsgLeadOrgPi);
				} else {
					this.fillEmptySiteDetails(validationMsgLeadOrgPi);
				}
			} else {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSubmitOrgSpecifics",LC.L_Id_Upper)+"</b>", 
						false, EMPTY_STRING, null));
				this.fillSiteDetails(Integer.parseInt(ctrpDraftJB.getSubOrgFkSite()), 
						Integer.parseInt(ctrpDraftJB.getSubOrgFkAdd()), validationMsgLeadOrgPi);
			}
		}
		if (ctrpDraftJB.getSubOrgTypeInt() != null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
					VelosResourceBundle.getLabelString("CTRP_DraftSubmitOrgSpecifics",LC.L_Type), false,
					this.getCodelstDescriptionByPk(ctrpDraftJB.getSubOrgTypeInt()), null));
		} else {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
					VelosResourceBundle.getLabelString("CTRP_DraftSubmitOrgSpecifics",LC.L_Type), false,
					EMPTY_STRING, MC.M_FieldRequired));
		}
		ArrayList<RadioOption> nciDesignatedList = 
			(ArrayList<RadioOption>)ctrpDraftJB.getIsNCIDesignatedList();
		String nciDesignatedText = null;
		for (RadioOption option : nciDesignatedList) {
			if (option.getRadioCode() != null 
					&& option.getRadioCode().equals(ctrpDraftJB.getIsNCIDesignated())) {
				nciDesignatedText = option.getRadioDisplay();
				break;
			}
		}
		nciDesignatedText = nciDesignatedText == null ? EMPTY_STRING : nciDesignatedText;
		validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(LC.CTRP_DraftIsSubmOrgNCIDesignated,
				false, nciDesignatedText, null));
		if (ctrpDraftJB.getIsNCIDesignated() != null && ctrpDraftJB.getIsNCIDesignated() == 1) {
			if (StringUtil.isEmpty(ctrpDraftJB.getSubOrgProgCode())) {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSiteProgramCode,
						true, EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSiteProgramCode,
						true, StringUtil.trueValue(ctrpDraftJB.getSubOrgProgCode()), null));
			}
		}
		
		if (ctrpDraftJB.getPiId() == null && ctrpDraftJB.getPiFkUser() == null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
					"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSitePiSpecifics",LC.L_Id_Upper)+"</b>",
					false, EMPTY_STRING, null));
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(EMPTY_STRING, true, EMPTY_STRING,
					MC.CTRP_PiEnterOrSelect.replaceAll("<br/>", EMPTY_STRING).replaceAll("<br>", EMPTY_STRING)));
			this.fillEmptyUserDetails(validationMsgLeadOrgPi);
		} else {
			if (ctrpDraftJB.getPiId() != null) {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSitePiSpecifics",LC.L_Id_Upper)+"</b>",
						false, ctrpDraftJB.getPiId(), null));
				if (ctrpDraftJB.getPiFkUser() != null && ctrpDraftJB.getPiFkAdd() != null) {
					this.fillOptionalUserDetails(Integer.parseInt(ctrpDraftJB.getPiFkUser()), 
							Integer.parseInt(ctrpDraftJB.getPiFkAdd()), validationMsgLeadOrgPi);
				} else {
					this.fillEmptyUserDetails(validationMsgLeadOrgPi);
				}
			} else {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSitePiSpecifics",LC.L_Id_Upper)+"</b>",
						false, EMPTY_STRING, null));
				this.fillUserDetails(
						ctrpDraftJB.getPiFkUser() == null ? null : Integer.parseInt(ctrpDraftJB.getPiFkUser()), 
								ctrpDraftJB.getPiFkAdd() == null ? null : Integer.parseInt(ctrpDraftJB.getPiFkAdd()),
										validationMsgLeadOrgPi);
			}
		}
		
		validationMsgMap.put(MC.CTRP_DraftLeadSubOrgorPiInvst, validationMsgLeadOrgPi);
	}
	
	private void validateIndusStatusDates() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgStatusDates = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		Integer siteRecruitTypeInt = ctrpDraftJB.getSiteRecruitTypeInt();
		if (siteRecruitTypeInt == null || siteRecruitTypeInt < 1) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftSiteRecruitStatus, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftSiteRecruitStatus, true,
					this.getCodelstDescriptionByPk(siteRecruitTypeInt), null));
		}
		if (ctrpDraftJB.getRecStatusDate() == null) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftSiteRecruitStatusDate, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftSiteRecruitStatusDate, true,
					ctrpDraftJB.getRecStatusDate(), null));
		}
		boolean isOpenAccrualDateRequired = StringUtil.isEmpty(ctrpDraftJB.getClosedAccrualDate());
		if (isOpenAccrualDateRequired && StringUtil.isEmpty(ctrpDraftJB.getOpenAccrualDate())) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftOpenAccrualDate, isOpenAccrualDateRequired,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftOpenAccrualDate, isOpenAccrualDateRequired,
					ctrpDraftJB.getOpenAccrualDate(), null));
		}
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				LC.CTRP_DraftCloseAccrualDate, false,
				StringUtil.trueValue(ctrpDraftJB.getClosedAccrualDate()), null));
		if (ctrpDraftJB.getIsNCIDesignated() != null && ctrpDraftJB.getIsNCIDesignated() == 1) {
			if (StringUtil.isEmpty(ctrpDraftJB.getSiteTrgtAccrual())) {
				validationMsgStatusDates.add(new CtrpDraftValidationPojo(
						LC.CTRP_DraftSiteTargetAccrual, true,
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgStatusDates.add(new CtrpDraftValidationPojo(
						LC.CTRP_DraftSiteTargetAccrual, true,
						StringUtil.trueValue(ctrpDraftJB.getSiteTrgtAccrual()), null));
			}
		}
		
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				LC.L_CurStd_Status, false, ctrpDraftJB.getCurrStudyStatus(), null));
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				LC.L_CurStat_Date, false, ctrpDraftJB.getStatusValidFrm(), null));
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				LC.CTRP_DraftActStatusValidfrm, false, ctrpDraftJB.getStudyActiveDate(), null));
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				MC.CTRP_DraftClsdAccStatsVldFrm, false, ctrpDraftJB.getStudyEnrollClosedDt(), null));
		
		validationMsgMap.put(LC.CTRP_DraftSectStatusDates, validationMsgStatusDates);
	}
	
	private void validateIndusTrialDocs() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgTrialDocs = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		if (StringUtil.isEmpty(ctrpDraftJB.getNctNumber())) {
			if (StringUtil.isEmpty(ctrpDraftJB.getAbbrTrialTempDesc())) {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.CTRP_DraftAbbTrialTmplt, true, EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.CTRP_DraftAbbTrialTmplt, true, StringUtil.trueValue(ctrpDraftJB.getAbbrTrialTempDesc()), null));
			}
		} else {
			validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftAbbTrialTmplt, false, StringUtil.trueValue(ctrpDraftJB.getAbbrTrialTempDesc()), null));
		}
		validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
				LC.L_Other, false, StringUtil.trueValue(ctrpDraftJB.getOtherDesc()), null));
		
		validationMsgMap.put(LC.CTRP_DraftSectTrialDocs, validationMsgTrialDocs);
	}
	
	private void validateSectTrialSubmType() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgSubmType = new ArrayList<CtrpDraftValidationPojo>();
		
		String submTypeSubtyp = this.getCodelstSubtypeByPk(ctrpDraftJB.getTrialSubmTypeInt());
		if (ctrpDraftJB.getTrialSubmTypeInt() == null || ctrpDraftJB.getTrialSubmTypeInt() < 1) {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSectSubmType, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSectSubmType, true, 
					this.getCodelstDescriptionByPk(ctrpDraftJB.getTrialSubmTypeInt()), null));
		}
		Integer xmlReqd = draftBean.getDraftXMLReqd();
		ArrayList<RadioOption> xmlReqdList = (ArrayList<RadioOption>)ctrpDraftJB.getDraftXmlReqList();
		String xmlReqdText = null;
		for (RadioOption option : xmlReqdList) {
			if (option.getRadioCode() != null && option.getRadioCode().equals(xmlReqd)) {
				xmlReqdText = option.getRadioDisplay();
				break;
			}
		}
		xmlReqdText = xmlReqdText == null ? "" : xmlReqdText;
		validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftXMLRequired, false, 
				xmlReqdText, null));
		
		if (LETTER_A.equals(submTypeSubtyp)) {
			if (StringUtil.isEmpty(draftBean.getAmendNumber())) {
				validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftAmendmentNumber, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftAmendmentNumber, true, 
						StringUtil.trueValue(draftBean.getAmendNumber()), null));
			}
			if (draftBean.getAmendDate() == null) {
				validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftAmendmentDate, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgSubmType.add(new CtrpDraftValidationPojo(LC.CTRP_DraftAmendmentDate, true, 
						DateUtil.dateToString(draftBean.getAmendDate()), null));
			}
		}
		
		validationMsgMap.put(LC.CTRP_DraftSectSubmType, validationMsgSubmType);
	}
	
	private void validateSectTrialIds() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgTrialIds = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		String submTypeSubtyp = this.getCodelstSubtypeByPk(ctrpDraftJB.getTrialSubmTypeInt());
		validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftUniqTrialId, false, 
				studyBean.getStudyNumber(), null));
		if (LETTER_A.equals(submTypeSubtyp)) {
			if (StringUtil.isEmpty(draftBean.getNciStudyId())) {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, true, 
						draftBean.getNciStudyId(), null));
			}
			if (StringUtil.isEmpty(draftBean.getLeadOrgStudyId())) {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, true, 
						draftBean.getLeadOrgStudyId(), null));
			}
		} else if (LETTER_O.equals(submTypeSubtyp)) {
			validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, false, 
					StringUtil.trueValue(draftBean.getNciStudyId()), null));
			if (StringUtil.isEmpty(draftBean.getLeadOrgStudyId())) {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, true, 
						draftBean.getLeadOrgStudyId(), null));
			}
		} else if (LETTER_U.equals(submTypeSubtyp)) {
			if (StringUtil.isEmpty(draftBean.getNciStudyId())) {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, true, 
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, true, 
						draftBean.getNciStudyId(), null));
			}
			validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, false, 
					StringUtil.trueValue(draftBean.getLeadOrgStudyId()), null));
		}else{
			// 21-Mar-2012 else block added for Bug#8962
			validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCITrialId, false, 
					draftBean.getNciStudyId(), null));
			validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftLeadOrgTrialId, false, 
					StringUtil.trueValue(draftBean.getLeadOrgStudyId()), null));
		}
		validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftNCTNumber, false, 
				StringUtil.trueValue(draftBean.getNctNumber()), null));
		validationMsgTrialIds.add(new CtrpDraftValidationPojo(LC.CTRP_DraftOtherTrialId, false, 
				StringUtil.trueValue(draftBean.getOtherNumber()), null));
		
		validationMsgMap.put(LC.CTRP_DraftSectTrialIdentfiers, validationMsgTrialIds);
	}
	
	private void validateSectTrialDetails() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgTrialDetails = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.L_Title, false, 
				studyBean.getStudyTitle(), null));
		Integer fkCodelstPhase = draftBean.getFkCodelstPhase();
		if (fkCodelstPhase == null || fkCodelstPhase < 1) {
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.L_Phase, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.L_Phase, true, 
					getCodelstDescriptionByPk(fkCodelstPhase), null));
		}
		validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPhaseColon, false, 
				getCodelstDescriptionByPk(StringUtil.stringToNum(studyBean.getStudyPhase())),
				null));
		if (fkCodelstPhase != null && fkCodelstPhase > 0 
				&& LETTER_NA.equals(getCodelstSubtypeByPk(fkCodelstPhase))) {
			if (draftBean.getDraftPilot() == null) {
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPilotTrial,
						true, EMPTY_STRING, MC.M_FieldRequired));
			} else {
				ArrayList<RadioOption> draftPilotList = 
					(ArrayList<RadioOption>)ctrpDraftJB.getDraftPilotList();
				String draftPilotText = null;
				for (RadioOption option : draftPilotList) {
					if (option.getRadioCode() != null 
							&& option.getRadioCode().equals(draftBean.getDraftPilot())) {
						draftPilotText = option.getRadioDisplay();
						break;
					}
				}
				draftPilotText = draftPilotText == null ? EMPTY_STRING : draftPilotText;
				validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPilotTrial,
						true, draftPilotText, null));
			}
		}
		
		Integer draftStudyType = draftBean.getStudyType();
		if (draftStudyType == null) {
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftTrialType, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			ArrayList<RadioOption> draftStudyTypeList = 
				(ArrayList<RadioOption>)ctrpDraftJB.getDraftStudyTypeList();
			String studyTypeText = null;
			for (RadioOption option : draftStudyTypeList) {
				if (option.getRadioCode() != null && option.getRadioCode().equals(draftStudyType)) {
					studyTypeText = option.getRadioDisplay();
					break;
				}
			}
			studyTypeText = studyTypeText == null ? EMPTY_STRING : studyTypeText;
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftTrialType, true, 
					studyTypeText, null));
		}
		validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftTrialTypeColon, false, 
				getCodelstDescriptionByPk(StringUtil.stringToNum(studyBean.getStudyType())),
				null));
		Integer studyPurpose = draftBean.getStudyPurpose();
		if (studyPurpose == null || studyPurpose < 1) {
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurpose, true, 
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurpose, true, 
					getCodelstDescriptionByPk(studyPurpose), null));
			// Other purpose
			String purposeSubtype = getCodelstSubtypeByPk(studyPurpose);
			if (STRING_OTHER.equals(purposeSubtype)) {
				if (draftBean.getStudyPurposeOther() == null ||
						draftBean.getStudyPurposeOther().length() < 1) {
					validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurposeOther, true, 
							EMPTY_STRING, MC.M_FieldRequired));
				} else {
					validationMsgTrialDetails.add(new CtrpDraftValidationPojo(LC.CTRP_DraftPurposeOther, true, 
							draftBean.getStudyPurposeOther(), null));
				}
			}
		}
		
		validationMsgMap.put(LC.CTRP_DraftSectTrialDetails, validationMsgTrialDetails);
	}
	
	private void validateSectLeadOrgPi() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgLeadOrgPi = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		if (ctrpDraftJB.getLeadOrgCtrpId() == null && ctrpDraftJB.getLeadOrgFkSite() == null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftLeadOrgId+"</b>", false,
					EMPTY_STRING, null));
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(EMPTY_STRING, true,
					EMPTY_STRING, MC.CTRP_LeadOrgEnterOrSelect.replaceAll("<br/>", EMPTY_STRING)));
			this.fillEmptySiteDetails(validationMsgLeadOrgPi);
		} else {
			if (ctrpDraftJB.getLeadOrgCtrpId() != null) {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftLeadOrgId+"</b>", false,
						ctrpDraftJB.getLeadOrgCtrpId(), null));
				if (ctrpDraftJB.getLeadOrgFkSite() != null && ctrpDraftJB.getLeadOrgFkAdd() != null) {
					this.fillOptionalSiteDetails(Integer.parseInt(ctrpDraftJB.getLeadOrgFkSite()), 
							Integer.parseInt(ctrpDraftJB.getLeadOrgFkAdd()), validationMsgLeadOrgPi);
				} else {
					this.fillEmptySiteDetails(validationMsgLeadOrgPi);
				}
			} else {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftLeadOrgId+"</b>", false,
						EMPTY_STRING, null));
				this.fillSiteDetails(Integer.parseInt(ctrpDraftJB.getLeadOrgFkSite()), 
						Integer.parseInt(ctrpDraftJB.getLeadOrgFkAdd()), validationMsgLeadOrgPi);
			}
		}
		
		if (ctrpDraftJB.getLeadOrgTypeInt() != null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(LC.L_Type, false,
					this.getCodelstDescriptionByPk(ctrpDraftJB.getLeadOrgTypeInt()), null));
		} else {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(LC.L_Type, false,
					EMPTY_STRING, MC.M_FieldRequired));
		}
		
		if (ctrpDraftJB.getPiId() == null && ctrpDraftJB.getPiFkUser() == null) {
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_PrinInvID+"</b>", false,
					EMPTY_STRING, null));
			validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo(EMPTY_STRING, true,
					EMPTY_STRING, MC.CTRP_PiEnterOrSelect.replaceAll("<br/>", EMPTY_STRING)));
			this.fillEmptyUserDetails(validationMsgLeadOrgPi);
		} else {
			if (ctrpDraftJB.getPiId() != null) {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_PrinInvID+"</b>", false,
						ctrpDraftJB.getPiId(), null));
				if (ctrpDraftJB.getPiFkUser() != null && ctrpDraftJB.getPiFkAdd() != null) {
					this.fillOptionalUserDetails(Integer.parseInt(ctrpDraftJB.getPiFkUser()), 
							Integer.parseInt(ctrpDraftJB.getPiFkAdd()), validationMsgLeadOrgPi);
				} else {
					this.fillEmptyUserDetails(validationMsgLeadOrgPi);
				}
			} else {
				validationMsgLeadOrgPi.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_PrinInvID+"</b>", false,
						EMPTY_STRING, null));
				this.fillUserDetails(
						ctrpDraftJB.getPiFkUser() == null ? null : Integer.parseInt(ctrpDraftJB.getPiFkUser()), 
								ctrpDraftJB.getPiFkAdd() == null ? null : Integer.parseInt(ctrpDraftJB.getPiFkAdd()),
										validationMsgLeadOrgPi);
			}
		}
		
		validationMsgMap.put(LC.CTRP_DraftSectLeadOrgPI, validationMsgLeadOrgPi);
	}

	private void validateSectSponsResp() throws Exception {
		if (ctrpDraftJB.getDraftXmlRequired() == null || ctrpDraftJB.getDraftXmlRequired() == 0) {
			return;
		}
		ArrayList<CtrpDraftValidationPojo> validationMsgSponsResp = 
			new ArrayList<CtrpDraftValidationPojo>();
		if (draftBean.getSponsorCtrpId() == null && draftBean.getFkSiteSponsor() == null) {
			validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.L_Sponsor_Id+"</b>", false,
					EMPTY_STRING, null));
			validationMsgSponsResp.add(new CtrpDraftValidationPojo(EMPTY_STRING, true,
					EMPTY_STRING, MC.CTRP_SponsRespEnterOrSelect.replaceAll("<br/>", EMPTY_STRING)));
			this.fillEmptySiteDetails(validationMsgSponsResp);
		} else {
			if (draftBean.getSponsorCtrpId() != null) {
				validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.L_Sponsor_Id+"</b>", false,
						draftBean.getSponsorCtrpId(), null));
				if (draftBean.getFkSiteSponsor() != null && draftBean.getFkAddSponsor() != null) {
					this.fillOptionalSiteDetails(draftBean.getFkSiteSponsor(), 
							draftBean.getFkAddSponsor(), validationMsgSponsResp);
				} else {
					this.fillEmptySiteDetails(validationMsgSponsResp);
				}
			} else {
				validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.L_Sponsor_Id+"</b>", false,
						EMPTY_STRING, null));
				this.fillSiteDetails(draftBean.getFkSiteSponsor(), 
						draftBean.getFkAddSponsor(), validationMsgSponsResp);
			}
		}
		
		if (draftBean.getResponsibleParty() == null) {
			validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftResponsibleParty+"</b>", true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			CtrpDraftSectSponsRespJB ctrpDraftSectSponsRespJB = new CtrpDraftSectSponsRespJB();
			ArrayList<RadioOption> respPartyList = 
				(ArrayList<RadioOption>)ctrpDraftSectSponsRespJB.getResponsiblePartyList();
			String respPartyText = null;
			for (RadioOption option : respPartyList) {
				if (option.getRadioCode() != null 
						&& option.getRadioCode().equals(draftBean.getResponsibleParty())) {
					respPartyText = option.getRadioDisplay();
					break;
				}
			}
			respPartyText = respPartyText == null ? EMPTY_STRING : respPartyText;
			validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftResponsibleParty+"</b>", true,
					respPartyText, null));
			if (draftBean.getResponsibleParty() == 0) { // Sponsor
				validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSponsorContactFromStudy, false,
						StringUtil.trueValue(studyBean.getStudyContact()), null));
				ArrayList<RadioOption> sponsContactTypeList = 
					(ArrayList<RadioOption>)ctrpDraftSectSponsRespJB.getSponsorContactTypeList();
				String sponsContactTypeText = null;
				for (RadioOption option : sponsContactTypeList) {
					if (option.getRadioCode() != null 
							&& option.getRadioCode().equals(draftBean.getRpSponsorContactType())) {
						sponsContactTypeText = option.getRadioDisplay();
						break;
					}
				}
				validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.CTRP_DraftSponsorContactType, false,
						StringUtil.trueValue(sponsContactTypeText), null));
				if (draftBean.getRpSponsorContactType() != null) {
					if (draftBean.getRpSponsorContactType() == 0) { // generic type
						if (draftBean.getRpSponsorContactId() == null || draftBean.getRpSponsorContactId().length() < 1) {
							validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.CTRP_DraftGenericContactId, true,
									EMPTY_STRING, MC.M_FieldRequired));
						} else {
							validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.CTRP_DraftGenericContactId, true,
									draftBean.getRpSponsorContactId(), null));
						}
						validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.CTRP_DraftGenericContactTitle, false,
								StringUtil.trueValue(draftBean.getSponsorGenericTitle()), null));
					} else { // personal type
						if (draftBean.getRpSponsorContactId() == null 
								&& draftBean.getFkUserSponsorPersonal() == null) {
							validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftPersonalContactId+"</b>", false,
									EMPTY_STRING, null));
							validationMsgSponsResp.add(new CtrpDraftValidationPojo(EMPTY_STRING, true,
									EMPTY_STRING, MC.CTRP_PersonalContactEnterOrSelect.replaceAll("<br/>", EMPTY_STRING)));
							this.fillEmptyUserDetails(validationMsgSponsResp);
						} else {
							if (draftBean.getRpSponsorContactId() != null) {
								validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftPersonalContactId+"</b>", false,
									draftBean.getRpSponsorContactId(), null));
								if (draftBean.getFkUserSponsorPersonal() != null && draftBean.getFkAddSponsorPersonal() != null) {
									this.fillOptionalUserDetails(draftBean.getFkUserSponsorPersonal(), 
											draftBean.getFkAddSponsorPersonal(), validationMsgSponsResp);
								} else {
									this.fillEmptyUserDetails(validationMsgSponsResp);
								}
							} else {
								validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftPersonalContactId+"</b>", false,
										EMPTY_STRING, null));
								this.fillUserDetails(draftBean.getFkUserSponsorPersonal(), 
										draftBean.getFkAddSponsorPersonal(), validationMsgSponsResp);
							}
						}
					}
				}
			}
		}
		
		validationMsgSponsResp.add(new CtrpDraftValidationPojo("<b>"+LC.CTRP_DraftResponsiblePartyContact+"</b>", false,
				EMPTY_STRING, null));
		
		if (draftBean.getRpEmail() == null || draftBean.getRpEmail().length() < 1) {
			validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.L_Email_Addr, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.L_Email_Addr, true,
					draftBean.getRpEmail(), null));
		}

		if (draftBean.getRpPhone() == null || draftBean.getRpPhone().length() < 1) {
			validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.L_Phone, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.L_Phone, true,
					draftBean.getRpPhone(), null));
		}
		validationMsgSponsResp.add(new CtrpDraftValidationPojo(LC.L_Ext, false,
				draftBean.getRpExtn() == null ? EMPTY_STRING : String.valueOf(draftBean.getRpExtn()), null));
		
		validationMsgMap.put(LC.CTRP_DraftSectSponsorRespParty, validationMsgSponsResp);
	}
	
	private void validateSectSumm4() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgSumm4 = 
			new ArrayList<CtrpDraftValidationPojo>();

		validationMsgSumm4.add(new CtrpDraftValidationPojo(
				VelosResourceBundle.getLabelString("CTRP_DraftSumm4SponsorSpecifics", LC.L_Type), false,
				this.getCodelstDescriptionByPk(draftBean.getSumm4SponsorType()), null));
		if (draftBean.getSumm4SponsorId() == null && draftBean.getFkSiteSumm4Sponsor() == null) {
			validationMsgSumm4.add(new CtrpDraftValidationPojo(
					"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSumm4SponsorSpecifics",LC.L_Id_Upper)+"</b>",
					false, EMPTY_STRING, null));
			validationMsgSumm4.add(new CtrpDraftValidationPojo(EMPTY_STRING, true,
					EMPTY_STRING, 
					MC.CTRP_DraftSumm4OrgEnterOrSelect.replaceAll("<br/>", EMPTY_STRING).replaceAll("<br>", EMPTY_STRING)));
			this.fillEmptySiteDetails(validationMsgSumm4);
		} else {
			if (draftBean.getSumm4SponsorId() != null) {
				validationMsgSumm4.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSumm4SponsorSpecifics",LC.L_Id_Upper)+"</b>",
						false, draftBean.getSumm4SponsorId(), null));
				if (draftBean.getFkSiteSumm4Sponsor() != null && draftBean.getfkAddSumm4Sponsor() != null) {
					this.fillOptionalSiteDetails(draftBean.getFkSiteSumm4Sponsor(), 
							draftBean.getfkAddSumm4Sponsor(), validationMsgSumm4);
				} else {
					this.fillEmptySiteDetails(validationMsgSumm4);
				}
			} else {
				validationMsgSumm4.add(new CtrpDraftValidationPojo(
						"<b>"+VelosResourceBundle.getLabelString("CTRP_DraftSumm4SponsorSpecifics",LC.L_Id_Upper)+"</b>",
						false, EMPTY_STRING, null));
				this.fillSiteDetails(draftBean.getFkSiteSumm4Sponsor(), 
						draftBean.getfkAddSumm4Sponsor(), validationMsgSumm4);
			}
		}
		
		validationMsgMap.put(LC.CTRP_DraftSectSumm4Info, validationMsgSumm4);
	}
	
	private void validateSectNihGrants() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgNihGrants = 
			new ArrayList<CtrpDraftValidationPojo>();
		StudyNIHGrantDao nihGrantDao = new StudyNIHGrantDao();
		nihGrantDao.getStudyNIHGrantRecords(studyBean.getId());
		ArrayList pkStudyNIhGrants = nihGrantDao.getPkStudyNIhGrants();
		validationMsgNihGrants.add(new CtrpDraftValidationPojo(
		VelosResourceBundle.getLabelString("CTRP_DraftNihGrantSpecifics", LC.L_Count),
		false, String.valueOf(pkStudyNIhGrants.size()), null));
		validationMsgMap.put(LC.L_NIH_Grant_Information, validationMsgNihGrants);
	}
	
	private void validateSectStatusDates() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgStatusDates = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		Integer fkCodelstCtrpStudyStatus = draftBean.getFkCodelstCtrpStudyStatus();
		if (fkCodelstCtrpStudyStatus == null || fkCodelstCtrpStudyStatus < 1) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftCurrTrialStatus, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftCurrTrialStatus, true,
					this.getCodelstDescriptionByPk(fkCodelstCtrpStudyStatus), null));
		}
		if (draftBean.getCtrpStudyStatusDate() == null) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftCurrTrialStatusDate, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftCurrTrialStatusDate, true,
					DateUtil.dateToString(draftBean.getCtrpStudyStatusDate()), null));
		}
		if (CtrpDraftJB.ADMIN_COMP.equals(this.getCodelstSubtypeByPk(fkCodelstCtrpStudyStatus))
				|| CtrpDraftJB.WITHDRAWN.equals(this.getCodelstSubtypeByPk(fkCodelstCtrpStudyStatus))
				|| CtrpDraftJB.TEMPCLOSEDACC.equals(this.getCodelstSubtypeByPk(fkCodelstCtrpStudyStatus))) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftWhyTrialStopped, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftWhyTrialStopped, false,
					StringUtil.trueValue(draftBean.getCtrpStudyStopReason()), null));
		}
		if (draftBean.getCtrpStudyStartDate() == null) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftTrialStartDate, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftTrialStartDate, true,
					DateUtil.dateToString(draftBean.getCtrpStudyStartDate()), null));
		}
		Integer trialStartTypeInt = draftBean.getIsStartActual();
		ArrayList<RadioOption> trialStartTypeList = (ArrayList<RadioOption>)ctrpDraftJB.getTrialStartTypeList();
		String trialStartTypeText = null;
		StringBuffer optionDescSB = new StringBuffer();
		for (RadioOption option : trialStartTypeList) {
			optionDescSB.append(option.getRadioDisplay()).append("/");
			if (option.getRadioCode() != null && option.getRadioCode().equals(trialStartTypeInt)) {
				trialStartTypeText = option.getRadioDisplay();
			}
		}
		if (optionDescSB.length() > 0) { optionDescSB.deleteCharAt(optionDescSB.length()-1); }
		trialStartTypeText = trialStartTypeText == null ? "" : trialStartTypeText;
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				optionDescSB.toString(), false, trialStartTypeText, null));

		
		if (draftBean.getCtrpStudyCompDate() == null) {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftPrimaryCompletionDate, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgStatusDates.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftPrimaryCompletionDate, true,
					DateUtil.dateToString(draftBean.getCtrpStudyCompDate()), null));
		}
		Integer compTypeInt = draftBean.getIsCompActual();
		ArrayList<RadioOption> compTypeList = (ArrayList<RadioOption>)ctrpDraftJB.getPrimaryCompletionTypeList();
		String compTypeText = null;
		StringBuffer optionDescCompSB = new StringBuffer();
		
		for (RadioOption option : compTypeList) {
			optionDescCompSB.append(option.getRadioDisplay()).append("/");
			if (option.getRadioCode() != null && option.getRadioCode().equals(compTypeInt)) {
				compTypeText = option.getRadioDisplay();
			}
		}
		if (optionDescCompSB.length() > 0) { optionDescCompSB.deleteCharAt(optionDescCompSB.length()-1); }
		compTypeText = compTypeText == null ? "" : compTypeText;
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				optionDescCompSB.toString(), false, compTypeText, null));
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				LC.L_CurStd_Status, false, ctrpDraftJB.getCurrStudyStatus(), null));
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				LC.L_CurStat_Date, false, ctrpDraftJB.getStatusValidFrm(), null));
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				MC.M_DtStd_Active, false, ctrpDraftJB.getStudyActiveDate(), null));
		validationMsgStatusDates.add(new CtrpDraftValidationPojo(
				MC.M_DtStdClosed_Perm, false, ctrpDraftJB.getStudyEndDate(), null));
		
		validationMsgMap.put(LC.CTRP_DraftSectStatusDates, validationMsgStatusDates);
	}
	
	private void validateSectFdaIndIde() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgFdaIndIde = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		StudyINDIDEDAO studyIndIdeDAO = new StudyINDIDEDAO();
		studyIndIdeDAO.getStudyIndIdeInfo(StringUtil.stringToNum(ctrpDraftJB.getFkStudy()));
		ArrayList pkStudyIndIdeList = studyIndIdeDAO.getIdIndIdeList();
		validationMsgFdaIndIde.add(new CtrpDraftValidationPojo(
					VelosResourceBundle.getLabelString("CTRP_DraftIndIdeSpecifics", LC.L_Count),
					false, String.valueOf(pkStudyIndIdeList.size()), null));
		validationMsgMap.put(LC.CTRP_DraftSectFdaIndIde, validationMsgFdaIndIde);
	}
	
	private void validateSectRegulatoryInfo() throws Exception {
		if (ctrpDraftJB.getDraftXmlRequired() == null || ctrpDraftJB.getDraftXmlRequired() == 0) {
			return;
		}
		ArrayList<CtrpDraftValidationPojo> validationMsgRegulatoryInfo = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		if (ctrpDraftJB.getOversightCountry() == null || ctrpDraftJB.getOversightCountry() < 1) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftTrialAuthorityCountry, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftTrialAuthorityCountry, true,
					this.getCodelstDescriptionByPk(ctrpDraftJB.getOversightCountry()), 
					null));
		}
		
		if (ctrpDraftJB.getOversightOrganization() == null 
				|| ctrpDraftJB.getOversightOrganization() < 1) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftTrialAuthorityOrgName, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftTrialAuthorityOrgName, true,
					this.getCodelstDescriptionByPk(ctrpDraftJB.getOversightOrganization()), 
					null));
		}
		
		if (ctrpDraftJB.getFdaIntvenIndicator() == null ||
				ctrpDraftJB.getFdaIntvenIndicator() == -1) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftFdaRegIntvnIndicator, true,
					EMPTY_STRING, MC.M_FieldRequired));
		} else if (ctrpDraftJB.getFdaIntvenIndicator() == 0) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftFdaRegIntvnIndicator, true,
					LC.L_No, null));
		} else if (ctrpDraftJB.getFdaIntvenIndicator() == 1) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftFdaRegIntvnIndicator, true,
					LC.L_Yes, null));
			if (ctrpDraftJB.getSection801Indicator() == null ||
					ctrpDraftJB.getSection801Indicator() == -1) {
				validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
						LC.CTRP_DraftSection801Indicator, true,
						EMPTY_STRING, MC.M_FieldRequired));
			} else if (ctrpDraftJB.getSection801Indicator() == 0) {
				validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
						LC.CTRP_DraftSection801Indicator, true,
						LC.L_No, null));
			} else if (ctrpDraftJB.getSection801Indicator() == 1) {
				validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
						LC.CTRP_DraftSection801Indicator, true,
						LC.L_Yes, null));
				if (ctrpDraftJB.getDelayPostIndicator() == null ||
						ctrpDraftJB.getDelayPostIndicator() == -1) {
					validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
							LC.CTRP_DraftDelayPostIndicator, true,
							EMPTY_STRING, MC.M_FieldRequired));
				} else if (ctrpDraftJB.getDelayPostIndicator() == 0) {
					validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
							LC.CTRP_DraftDelayPostIndicator, true,
							LC.L_No, null));
				} else if (ctrpDraftJB.getDelayPostIndicator() == 1) {
					validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
							LC.CTRP_DraftDelayPostIndicator, true,
							LC.L_Yes, null));
				}
			}
		}
		if (ctrpDraftJB.getDmAppointIndicator() == null ||
				ctrpDraftJB.getDmAppointIndicator() == -1) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftDMAppointIndicator, false,
					EMPTY_STRING, null));
		} else if (ctrpDraftJB.getDmAppointIndicator() == 0) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftDMAppointIndicator, true,
					LC.L_No, null));
		} else if (ctrpDraftJB.getDmAppointIndicator() == 1) {
			validationMsgRegulatoryInfo.add(new CtrpDraftValidationPojo(
					LC.CTRP_DraftDMAppointIndicator, true,
					LC.L_Yes, null));
		}
		
		validationMsgMap.put(LC.CTRP_DraftSectRegInfo, validationMsgRegulatoryInfo);
	}
	
	private void validateSectTrialDocs() throws Exception {
		ArrayList<CtrpDraftValidationPojo> validationMsgTrialDocs = 
			new ArrayList<CtrpDraftValidationPojo>();
		
		String trialSubmTypeStr = String.valueOf(ctrpDraftJB.getTrialSubmTypeInt());
		
		if (ctrpDraftJB.getTrialSubmTypeA().equals(trialSubmTypeStr) 
				|| ctrpDraftJB.getTrialSubmTypeO().equals(trialSubmTypeStr)) {
			if (StringUtil.isEmpty(ctrpDraftJB.getProtocolDocDesc())) {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.L_Protocol_Docu, true,
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.L_Protocol_Docu, true,
						ctrpDraftJB.getProtocolDocDesc(), null));
			}
		} else {
			validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
					LC.L_Protocol_Docu, false,
					StringUtil.trueValue(ctrpDraftJB.getProtocolDocDesc()), null));
		}

		if (ctrpDraftJB.getTrialSubmTypeA().equals(trialSubmTypeStr)
				|| ctrpDraftJB.getTrialSubmTypeO().equals(trialSubmTypeStr)) {
			if (StringUtil.isEmpty(ctrpDraftJB.getIRBApprovalDesc())) {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.L_IRB_Approval, true,
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.L_IRB_Approval, true,
						ctrpDraftJB.getIRBApprovalDesc(), null));
			}
		} else {
			validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
					LC.L_IRB_Approval, false,
					StringUtil.trueValue(ctrpDraftJB.getIRBApprovalDesc()), null));
		}
		
		validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
				LC.L_ListOf_Part_Sites, false,
				StringUtil.trueValue(ctrpDraftJB.getPartSitesListDesc()), null));
		validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
				LC.L_Inform_Cons_Docu, false,
				StringUtil.trueValue(ctrpDraftJB.getInformedConsentDesc()), null));
		validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
				LC.L_Other, false,
				StringUtil.trueValue(ctrpDraftJB.getOtherDesc()), null));
		if (ctrpDraftJB.getTrialSubmTypeA().equals(trialSubmTypeStr)) {
			if (StringUtil.isEmpty(ctrpDraftJB.getChangeMemoDocDesc())) {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.L_Change_Memo_Docu, true,
						EMPTY_STRING, MC.M_FieldRequired));
			} else {
				validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
						LC.L_Change_Memo_Docu, true,
						ctrpDraftJB.getChangeMemoDocDesc(), null));
			}
		} else {
			validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
					LC.L_Change_Memo_Docu, false,
					StringUtil.trueValue(ctrpDraftJB.getChangeMemoDocDesc()), null));
		}
		validationMsgTrialDocs.add(new CtrpDraftValidationPojo(
				LC.L_Prot_HighLight_Docu, false,
				StringUtil.trueValue(ctrpDraftJB.getProtHighlightDesc()), null));
		
		validationMsgMap.put(LC.CTRP_DraftSectTrialDocs, validationMsgTrialDocs);
	}
	public Integer getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(Integer loggedUser) {
		this.loggedUser = loggedUser;
	}
}
