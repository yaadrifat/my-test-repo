/**
 * __astcwz_cmpnt_det: Author: Konesa CodeWizard Date and Time Created: Wed Jul
 * 09 14:41:50 IST 2003 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(1587)
// /__astcwz_packg_idt#(1580,1582,1583,1584~,,,)
package com.velos.eres.tags;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

// /__astcwz_class_idt#(1449!n)
public class VTextArea extends VTag {
    // /__astcwz_attrb_idt#(1455)
    private int columns;

    // /__astcwz_attrb_idt#(1456)
    private int rows;

    // /__astcwz_attrb_idt#(1458)
    private boolean readOnly;

    // /__astcwz_attrb_idt#(1459)
    private String wrap;

    // /__astcwz_attrb_idt#(1528)
    private String name;

    // /__astcwz_attrb_idt#(1529)
    private boolean disabled;

    // /__astcwz_default_constructor
    private String value;

    private String mandatoryOverRide;

    private VLabel taLabel;

    private String align;

    private int maxLength;

    /**
     * flag to hide field label in preview, Possible values - 0:No, 1:Yes
     */
    protected String hideLabel;

    /** The actual field name specified by the user without any formatting */
    protected String fldNameForMessages;

    public VTextArea() {
    }

    public VTextArea(String name, int rows, int cols, VLabel taLabel) {
        this.setName(name);
        this.setRows(rows);
        this.setColumns(cols);
        this.setTaLabel(taLabel);
        this.mandatoryOverRide = "";

    }

    // /__astcwz_opern_idt#()
    public int getColumns() {
        return columns;
    }

    // /__astcwz_opern_idt#()
    public int getRows() {
        return rows;
    }

    // /__astcwz_opern_idt#()
    public boolean getReadOnly() {
        return readOnly;
    }

    // /__astcwz_opern_idt#()
    public String getWrap() {
        return wrap;
    }

    // /__astcwz_opern_idt#()
    public String getName() {
        return name;
    }

    // /__astcwz_opern_idt#()
    public boolean getDisabled() {
        return disabled;
    }

    // /__astcwz_opern_idt#()
    public void setColumns(int acolumns) {
        columns = acolumns;
    }

    // /__astcwz_opern_idt#()
    public void setRows(int arows) {
        rows = arows;
    }

    // /__astcwz_opern_idt#()
    public void setReadOnly(boolean areadOnly) {
        readOnly = areadOnly;
    }

    // /__astcwz_opern_idt#()
    public void setWrap(String awrap) {
        wrap = awrap;
    }

    // /__astcwz_opern_idt#()
    public void setName(String aname) {
        name = aname;
    }

    // /__astcwz_opern_idt#()
    public void setDisabled(boolean adisabled) {
        disabled = adisabled;
    }

    public VLabel getTaLabel() {
        return this.taLabel;
    }

    public void setTaLabel(VLabel taLabel) {
        this.taLabel = taLabel;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAlign() {
        return this.align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    /**
     * Returns the value of hideLabel.
     */
    public String getHideLabel() {
        return hideLabel;
    }

    /**
     * Sets the value of hideLabel.
     * 
     * @param hideLabel
     *            The value to assign hideLabel.
     */
    public void setHideLabel(String hideLabel) {
        this.hideLabel = hideLabel;
    }

    public String getMandatoryOverRide() {
        return this.mandatoryOverRide;
    }

    public void setMandatoryOverRide(String mandatoryOverRide) {
        this.mandatoryOverRide = mandatoryOverRide;
    }

    /**
     * Returns the value of fldNameForMessages.
     */
    public String getFldNameForMessages() {
        if (!StringUtil.isEmpty(fldNameForMessages)) {
            return fldNameForMessages;
        } else {

            return (this.getTaLabel().getText());
        }
    }

    /**
     * Sets the value of fldNameForMessages.
     * 
     * @param fldNameForMessages
     *            The value to assign fldNameForMessages.
     */
    public void setFldNameForMessages(String fldNameForMessages) {
        this.fldNameForMessages = fldNameForMessages;
    }

    public String getTaLabelXSL() {
        String hideLabelStr = "";
        hideLabelStr = getHideLabel();
        // //////////////////
        VLabel vl = getTaLabel();
        String xslStr = "";

        if (StringUtil.isEmpty(hideLabelStr)) {
            hideLabelStr = "0";
        }

        if (hideLabelStr.equals("0")) {
            if (vl != null) {
                if (!this.align.equals("top")) {
                    xslStr = vl.drawXSL(true);
                } else {
                    xslStr = vl.drawXSL(false);
                    xslStr = xslStr + " <BR/> ";
                }

            }
        } else {
            vl.setText("&#xa0;");
            xslStr = vl.drawXSL(true);
        }

        return xslStr;

    }

    // NEW CHANGES TO GET THE MAX-LENGTH
    // /__astcwz_opern_idt#()
    public int getMaxLength() {
        return maxLength;
    }

    // /__astcwz_opern_idt#()
    public void setMaxLength(int amaxLength) {
        maxLength = amaxLength;
    }

    // /////////////////////

    public String getReadOnlyAttribute() {

        // get readOnly
        if (this.readOnly) {
            return " readonly = \"true\"";
        } else {
            return "";

        }

    }

    public String getDisabledAttribute() {

        // get disabled
        if (this.disabled) {
            return " disabled = \"true\"";
        } else {
            return "";

        }

    }

    public String getNameAttribute() {

        // get name
        if (!EJBUtil.isEmpty(this.name)) {
            return " name = \"" + this.name + "\"";
        } else {
            return "";

        }

    }

    public String getWrapAttribute() {

        // get wrap
        if (!EJBUtil.isEmpty(this.wrap)) {
            return " wrap = \"" + this.wrap + "\"";
        } else {
            return "";

        }

    }

    public String getColumnsAttribute() {

        // get columns
        if (this.columns > 0) {
            return " cols = \"" + this.columns + "\"";
        } else {
            return "";

        }

    }

    public String getRowsAttribute() {

        // get columns
        if (this.rows > 0) {
            return " rows = \"" + this.rows + "\"";
        } else {
            return "";

        }

    }

    public String getAlignAttribute() {

        if (!EJBUtil.isEmpty(this.align))

            // changed by Sonika
            // this indicates alignment of text which is achieved thru style

            return " style=\"text-align:" + this.align + "\"";

        // return "align = \"" + this.align + "\"";
        else
            return "";

    }

    public String getValueText() {

        // get value text

        if (!EJBUtil.isEmpty(this.name)) {
            return "<xsl:value-of select=\"" + this.name + "\" />";

        } else {
            return "";

        }

    }

    public String getValueVariableTag() {

        // get value variable
        if (!EJBUtil.isEmpty(this.name)) {
            return " <xsl:variable name =  \"" + this.name
                    + "\" select = \"rowset/" + this.name + "\" />";
        } else {
            return "";

        }

    }

    /**
     * Method to draw XSL for Text Area Field *
     * 
     * @return xslString
     */
    /*
     * *************************History*********************************
     * Modified by : Anu/sonika Modified On:06/11/2004 Comment: Changes remove
     * the help icon after the text area as now we display help icon with the
     * field label
     * 
     * *************************END****************************************
     */

    public String drawXSL() {

        StringBuffer xslStr = new StringBuffer();
        String str = "";

        if (StringUtil.isEmpty(align)) {
            align = "";
        }

        // if field label is hidden, alignment=top is not applicable

        String hideLabelStr = "";
        hideLabelStr = getHideLabel();
        if (StringUtil.isEmpty(hideLabelStr)) {
            hideLabelStr = "0";
        }

        if (hideLabelStr.equals("1") && align.equals("top")) {
            align = "left";
        }

        // get input label XSL
        if (!align.equals("top")) {
            xslStr.append(getTaLabelXSL());
        }

        xslStr.append("<td>");

        if (align.equals("top")) {
            xslStr.append(getTaLabelXSL());
        }

        xslStr.append("<textarea ");
        xslStr.append(getTagClass());
        xslStr.append(getStyle());
        // xslStr.append(getTitle());
        xslStr.append(getToolTip());
        xslStr.append(getId());
        xslStr.append(getDisabledAttribute());
        xslStr.append(getNameAttribute());
        xslStr.append(getReadOnlyAttribute());
        xslStr.append(getRowsAttribute());
        xslStr.append(getColumnsAttribute());
        xslStr.append(getWrapAttribute());
        xslStr.append(getFieldActionJavaScript());
        // xslStr.append(getAlignAttribute());
        xslStr.append(" >");
        xslStr.append(getValueText());
        xslStr.append("</textarea>");
        // xslStr.append(getHelpIcon());
        VInputBase v = new VInputBase();
        v.name = this.name;
        if (mandatoryOverRide == null)
            mandatoryOverRide = "0";
        if (mandatoryOverRide.equals("1")) {
            Rlog.debug("common", "in textarea mandatory");
            str = "mnd";
            xslStr.append("<input ");

            xslStr.append(v.getHiddenId(str));

            xslStr.append(v.getHiddenNameAttribute(str));

            xslStr.append(v.getHiddenType());
            xslStr.append(" >");

            xslStr.append(v.getHiddenValueAttribute(str));
            xslStr.append("</input>");

            xslStr.append("<input ");
            xslStr.append(v.getHiddenReasonId(str));
            xslStr.append(v.getHiddenReasonNameAttr(str));
            xslStr.append(v.getHiddenType());
            xslStr.append(" >");
            xslStr.append("</input>");

        }
        xslStr.append("</td>");

        return xslStr.toString();

    }

    public String getJavaScript(boolean isRequired) {
        String js = "";

        js = "  if  ( formobj." + this.name + ".value.length  > "
                + this.maxLength + ") " + "{  alert(\"Please enter only "
                + this.maxLength + "  characters in "
                + (getFldNameForMessages()) + ". \"); " + "formobj."
                + this.name + ".focus();" + " return false; }";

        if (isRequired) {
            js = js + "  if (!(validate_col(\"" + (getFldNameForMessages())
                    + "\",formobj." + this.name + "))) return false; ";
        }

        return js;

    }

    public String getHiddenFldName(String str) {
        String fldName = "hdn_" + str + "_" + this.name;
        // get name
        if (!EJBUtil.isEmpty(this.name)) {
            return fldName;

        } else {
            return "";

        }

    }

    public String getJavaScript(boolean isRequired, String mandatoryOverRide) {
        String js = "";
        String str = "";
        String validateStr = "";

        if (mandatoryOverRide == null)
            mandatoryOverRide = "0";
        Rlog.debug("common", "inside TA mandatoryOverRide" + mandatoryOverRide);
        if (isRequired && mandatoryOverRide.equals("1")) {
            Rlog.debug("common", "inside TA :::");
            str = "mnd";
            validateStr = "\"Please enter data in all mandatory fields.[VELSEPLABEL]"
                    + (getFldNameForMessages()) + "\"";
            js = "if (!(validate_col_no_alert(\"" + (getFldNameForMessages())
                    + "\",formobj." + this.name + "))) formobj."
                    + getHiddenFldName(str) + ".value = " + validateStr
                    + " ;else formobj." + getHiddenFldName(str)
                    + ".value = '' ; ";
            Rlog.debug("common", "inside TA js::" + js);

        }

        if (isRequired && mandatoryOverRide.equals("0")) {
            js = " if (!(validate_col(\"" + (getFldNameForMessages())
                    + "\",formobj." + this.name + "))) return false ;  ";

        }

        return js;

    }

    public String getFieldActionJavaScript() {
        /*
         * 05/11/04 - by Sonia Sahni Implemented for Field Actions; returns
         * default onChange() script for fields
         */
        String strJS = " onChange=\"performAction(document.er_fillform1,document.er_fillform1."
                + this.name + ")\" ";
        return strJS;

    }

}
