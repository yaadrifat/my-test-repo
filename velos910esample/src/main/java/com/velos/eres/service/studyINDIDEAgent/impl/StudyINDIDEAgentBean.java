/**
 * The stateless session EJB 
 * Class name : StudyIndIdeAgentBean<br>
 * @version 1.0 11/21/2011
 * Copyright notice: Velos, Inc
 * @ejbRemote StudyIndIdeAgentRObj
 * Author: Ashwani Godara
 */
package com.velos.eres.service.studyINDIDEAgent.impl;




/* Import Statements */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.SpecimenDao;
import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.specimen.impl.SpecimenBean;
import com.velos.eres.business.studyINDIDE.impl.StudyINDIDEBean;
import com.velos.eres.service.studyINDIDEAgent.StudyINDIDEAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;


/* End of Import Statements */

@Stateless
public class StudyINDIDEAgentBean implements StudyINDIDEAgentRObj {
	@PersistenceContext(unitName = "eres")
    protected EntityManager em;
	
	@Resource
	private SessionContext context;

	
	
	public int setStudyINDIDEInfo(int studyId){
		return 0;
	}
	
	public StudyINDIDEDAO getStudyINDIDEInfo(int studyId) {
		
	    	 try {
	             
	    		 StudyINDIDEDAO studyINDIDEDAO = new StudyINDIDEDAO();
	    		 studyINDIDEDAO.getStudyIndIdeInfo(studyId);
	             return studyINDIDEDAO;
	             
	         } catch (Exception e) {
	             Rlog.fatal("study", "Exception In getStudyINDIDEInfo in StudyINDIDEAgentBean " + e);
	         }
	         return null;
   	
	    
	}

	public int deleteIndIdeInfo(int indIdeId,Hashtable<String, String> auditInfo) {
		
		StudyINDIDEBean studyIndIdeBean= new StudyINDIDEBean();
        
		try {
			AuditBean audit=null;
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_STUDY_INDIDE","eres","PK_STUDY_INDIIDE="+indIdeId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_STUDY_INDIDE",String.valueOf(indIdeId),rid,userID,currdate,app_module,ipAdd,reason);
            studyIndIdeBean = (StudyINDIDEBean) em.find(StudyINDIDEBean .class, new Integer(indIdeId));
        	
            if (studyIndIdeBean == null) {
                return -2;
            }
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(studyIndIdeBean);
            Rlog.debug("address", "removed address");
           
        } catch (Exception e) {
        	context.setRollbackOnly();
            e.printStackTrace();
            Rlog.fatal("study", "EXCEPTION IN DELETING indIdeInfo" + e);
            return -2;
        }
        return 0;
	}

	

	public int setIndIdeInfo(StudyINDIDEBean indIdeBean) {
		
		int output=-1;
		StudyINDIDEBean studyIndIdeBean= new StudyINDIDEBean();
	    		        
	       try {
	    	   studyIndIdeBean.updateIndIdeBean(indIdeBean);
	    	   em.persist(studyIndIdeBean);
	            
	        } catch (Exception e) {
	        	Rlog.fatal("Study",  "Error in updateIndIdeInfo() in StudyINDIDEBean" + e);
	            return -2;
	        }
	       return output;
	    
	  }



	public int updateIndIdeInfo(StudyINDIDEBean indIdeBean) {
		int output=-1;
		StudyINDIDEBean studyIndIdeBean= null;
	    		        
	       try {
	        	studyIndIdeBean = (StudyINDIDEBean) em.find(StudyINDIDEBean .class, new Integer(indIdeBean.getIdINDIDE()));
	        	if (studyIndIdeBean == null) {
	                return -2;
	            }            
      
	            output = studyIndIdeBean.updateIndIdeBean(indIdeBean);            
	            if(output==0)
	            em.merge(studyIndIdeBean);
	            
	        } catch (Exception e) {
	        	Rlog.fatal("Study",  "Error in updateIndIdeInfo() in StudyINDIDEBean" + e);
	            return -2;
	        }
	       return output;
	    
	  }

	
	

}
