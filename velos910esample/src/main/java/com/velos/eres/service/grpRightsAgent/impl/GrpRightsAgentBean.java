/*
 * Classname			GrpRightsAgentBean.class
 * 
 * Version information  1.0
 *
 * Date					03/02/2001
 * 
 * Copyright notice		Velos inc.
 */

package com.velos.eres.service.grpRightsAgent.impl;

import java.util.ArrayList;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.grpRights.impl.GrpRightsBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Deepali
 * @version 1.0 03/02/2001
 * @ejbHome GrpRightsAgentHome
 * @ejbremote GrpRightsAgentRObj
 */
@Stateless
@Remote( { GrpRightsAgentRObj.class })
public class GrpRightsAgentBean implements GrpRightsAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     */
    private static final long serialVersionUID = 3905520514607757113L;

    /**
     * Looks up on the id parameter passed in and constructs and returns a state
     * keeper. containing the details of the Rights<br>
     * 
     * @param id
     *            the Group Rights id
     * @return Group Rights State Keeper Object which contains all rights for a
     *         group
     */

    public GrpRightsBean getGrpRightsDetails(int id) {

        // Object
        GrpRightsBean gsk = new GrpRightsBean();

        String grpRight = null;
        CtrlDao cntrl;
        ArrayList cVal, cDesc, cSeq;
        cVal = new ArrayList();
        cVal = new ArrayList();
        cSeq = new ArrayList();
        int sLength = 0;
        int counter = 0;

        cntrl = new CtrlDao();
        try {

            // pkGrpRights = new GrpRightsPK(id);
            // Rlog.debug("grprights","Primary Key For Group Righs" +
            // pkGrpRights);

            gsk = (GrpRightsBean) em.find(GrpRightsBean.class, new Integer(id));
            // Rlog.debug("grprights","remote Object For Group Righs" +
            // retrieved);
            grpRight = gsk.getGrpRights(); // Getting the Rights
            // assigned to the group on
            // each feature
            // from Entity Bean
            // gsk = retrieved.getGrpRightsStateKeeper();// Sets Group Id and
            // Rlog.debug("grprights","State Keeper for Group Rights" + gsk);

            if (!(StringUtil.isEmpty(grpRight))) {
                sLength = grpRight.length();
            }
            // Rlog.debug("grprights","Length Of the Group Rights" + sLength);

            for (counter = 0; counter <= (sLength - 1); counter++) {
                // Rlog.debug("grprights","Group Right at the current Counter" +
                // String.valueOf(grpRight.charAt(counter)));
                gsk.setFtrRights(String.valueOf(grpRight.charAt(counter)));
                gsk.setGrSeq(Integer.toString(counter));
            }

            cntrl.getControlValues("app_rights");
            cVal = cntrl.getCValue();
            cDesc = cntrl.getCDesc();
            gsk.setGrValue(cVal);
            gsk.setGrDesc(cDesc);
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("grprights", "EXCEPTION" + e);
        }
        Rlog.debug("grprights", "RETURNING FROM SESSION BEAN GROUP RIGHTS"
                + gsk);
        return gsk;
    }

    /**
     * Sets Rights for a group.
     * 
     * @param Group
     *            Rights state holder containing Rights attributes to be set.
     * @return 0 when updation is successful and -2 when some exception occurs
     */

    public int setGrpRightsDetails(GrpRightsBean gsk) {
        int grpId = 0;
        int counter = 0;
        int sLength = 0;
        ArrayList grpRht = new ArrayList();
        String rights = "";
        try {
            Rlog.debug("grprights", "in try bloc of Set Rights ");
            // GrpRightsHome grpRightsHome = EJBUtil.getGrpRightsHome();

            grpRht = gsk.getFtrRights();
            // Rlog.debug("grprights","Group Rights from State Keeper "
            // +grpRht);
            sLength = grpRht.size();
            // Rlog.debug("grprights","Size of Array List Rights " +sLength);
            for (counter = 0; counter <= (sLength - 1); counter++) {
                rights += grpRht.get(counter).toString();
            }
            // Rlog.debug("grprights","String of Rights " +rights);
            // GrpRightsRObj grp = grpRightsHome.create(gsk, rights);
            gsk.setGrpRights(rights);
            em.merge(gsk);
            // Rlog.debug("grprights","Creation of Group Rights Home " +grp);
        } catch (Exception e) {
            Rlog.fatal("grprights", "EXCEPTION" + e);
            e.printStackTrace();
            return -2; // Exception
        }
        return 0; // successful
    }

    /**
     * Updates Group Rights. Concatenate all the Feature rights into single
     * string of rights.
     * 
     * @param Group
     *            Rights state holder containing Rights attributes to be set.
     * @return 0 when updation is successful and -2 when some exception occurs
     */

    public int updateGrpRights(GrpRightsBean gsk) {
        int grpId = 0;
        int counter = 0;
        int sLength = 0;
        int result = 0;
        ArrayList grpRht = new ArrayList();
        String rights = "";
        GrpRightsBean grpBean = null;
        try {
            /*
             * GrpRightsRObj grpRights; GrpRightsPK pkGrpRights; pkGrpRights =
             * new GrpRightsPK(gsk.getId());
             */

            Rlog.debug("grprights", "in try bloc of Set Rights " + gsk.getId());
            grpBean = em.find(GrpRightsBean.class, new Integer(gsk.getId()));

            grpRht = gsk.getFtrRights();
            // Rlog.debug("grprights","Group Rights from State Keeper "
            // +grpRht);
            sLength = grpRht.size();
            // Rlog.debug("grprights","Size of Array List Rights " +sLength);
            for (counter = 0; counter <= (sLength - 1); counter++) {
                rights += grpRht.get(counter).toString();
            }
            // Rlog.debug("grprights","String of Rights " +rights);
            // grpBean.setFtrRights(rights);
            grpBean.updateGrpRights(gsk, rights);
            result = 0;
            Rlog.debug("grprights", "Updation of Group Rights " + result);
        } catch (Exception e) {
            Rlog.fatal("grprights", "EXCEPTION" + e);
            e.printStackTrace();
            return -2;
        }
        return 0;
    }

}// end of class

