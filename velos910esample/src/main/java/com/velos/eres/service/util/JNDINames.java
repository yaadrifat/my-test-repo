package com.velos.eres.service.util;



/**
 *
 *
 * This class is to be used as a central repository to store the
 *
 *
 * Internal JNDI names of various components. Any sort of change
 *
 *
 * at this location should also reflect in the Deployment Descriptor
 *
 *
 */

public class JNDINames {

    /* JNDI names of EJB home objects */

	public final static String prefix = "velos/";
	public final static String suffix = "/remote";
	
	public final static String AccountAgentHome = prefix+"AccountAgentBean"+suffix;
	public final static String AccountWrapperAgentHome = prefix+"AccountWrapperAgentBean"+suffix;
	public final static String AddressAgentHome = prefix+"AddressAgentBean"+suffix;
	public final static String AppendixAgentHome = prefix+"AppendixAgentBean"+suffix;
	public final static String BrowserRowsAgentHome = prefix+"BrowserRowsAgentBean"+suffix;
	public final static String CatLibAgentHome = prefix+"CatLibAgentBean"+suffix;
	public final static String CodelstAgentHome = prefix+"CodelstAgentBean"+suffix;
	public final static String CommonAgentHome = prefix+"CommonAgentBean"+suffix;
	public final static String CtrltabAgentHome = prefix+"CtrltabAgentBean"+suffix;
	public final static String DynRepAgentHome = prefix+"DynRepAgentBean"+suffix;
	public final static String DynRepDtAgentHome = prefix+"DynRepDtAgentBean"+suffix;
	public final static String DynRepFltrAgentHome = prefix+"DynRepFltrAgentBean"+suffix;
	public final static String EschReportsAgentHome = prefix+"EschReportsAgentBean"+suffix;
	public final static String ExtURightsAgentHome = prefix+"ExtURightsAgentBean"+suffix;
	public final static String FieldLibAgentHome = prefix+"FieldLibAgentBean"+suffix;
	public final static String FldRespAgentHome = prefix+"FldRespAgentBean"+suffix;
	public final static String FldValidateAgentHome = prefix+"FldValidateAgentBean"+suffix;
	public final static String FormActionAgentHome = prefix+"FormActionAgentBean"+suffix;
	public final static String FormFieldAgentHome = prefix+"FormFieldAgentBean"+suffix;
	public final static String FormLibAgentHome = prefix+"FormLibAgentBean"+suffix;
	public final static String FormNotifyAgentHome = prefix+"FormNotifyAgentBean"+suffix;
	public final static String FormQueryAgentHome = prefix+"FormQueryAgentBean"+suffix;
	public final static String FormQueryStatusAgentHome = prefix+"FormQueryStatusAgentBean"+suffix;
	public final static String FormSecAgentHome = prefix+"FormSecAgentBean"+suffix;
	public final static String FormStatAgentHome = prefix+"FormStatAgentBean"+suffix;
	public final static String GroupAgentHome = prefix+"GroupAgentBean"+suffix;
	public final static String GrpRightsAgentHome = prefix+"GrpRightsAgentBean"+suffix;
	public final static String GrpSupUserRigthtsAgentHome = prefix+"GrpSupUserRightsAgentBean"+suffix;
	public final static String InvoiceAgentHome = prefix+"InvoiceAgentBean"+suffix;
	public final static String InvoiceDetailAgentHome = prefix+"InvoiceDetailAgentBean"+suffix;
	public final static String LabAgentHome = prefix+"LabAgentBean"+suffix;
	public final static String LinkedFormsAgentHome = prefix+"LinkedFormsAgentBean"+suffix;
	public final static String MileApndxAgentHome = prefix+"MileApndxAgentBean"+suffix;
	public final static String MilepaymentAgentHome = prefix+"MilepaymentAgentBean"+suffix;
	public final static String MilestoneAgentHome = prefix+"MilestoneAgentBean"+suffix;
	public final static String MoreDetailsAgentHome = prefix+"MoreDetailsAgentBean"+suffix;
	public final static String MsgcntrAgentHome = prefix+"MsgcntrAgentBean"+suffix;
	public final static String PatFacilityAgentHome = prefix+"PatFacilityAgentBean"+suffix;
	public final static String PatLoginAgentHome = prefix+"PatLoginAgentBean"+suffix;
	public final static String PatProtAgentHome = prefix+"PatProtAgentBean"+suffix;
	public final static String PatStudyStatAgentHome = prefix+"PatStudyStatAgentBean"+suffix;
	public final static String PatTXArmAgentHome = prefix+"PatTXArmAgentBean"+suffix;
	public final static String PaymentDetailAgentHome = prefix+"PaymentDetailAgentBean"+suffix;
	public final static String PerApndxAgentHome = prefix+"PerApndxAgentBean"+suffix;
	public final static String PerIdAgentHome = prefix+"PerIdAgentBean"+suffix;
	public final static String PersonAgentHome = prefix+"PersonAgentBean"+suffix;
	public final static String PortalAgentHome = prefix+"PortalAgentBean"+suffix;
	public final static String PortalDesignAgentHome = prefix+"PortalDesignAgentBean"+suffix;
	public final static String PrefAgentHome = prefix+"PrefAgentBean"+suffix;
	public final static String ReportAgentHome = prefix+"ReportAgentBean"+suffix;
	public final static String ReportsAgentHome = prefix+"ReportsAgentBean"+suffix;
	public final static String ReviewMeetingTopicAgentHome = prefix+"ReviewMeetingTopicAgentBean"+suffix;
	public final static String SavedRepAgentHome = prefix+"SavedRepAgentBean"+suffix;
	public final static String SectionAgentHome = prefix+"SectionAgentBean"+suffix;
	public final static String SiteAgentHome = prefix+"SiteAgentBean"+suffix;
	public final static String SpecimenAgentHome = prefix+"SpecimenAgentBean"+suffix;
	// Enhancement INV-22518: Pritam Singh
	public final static String BulkUploadAgentHome = prefix+"BulkUploadAgentBean"+suffix;
	public final static String BulkUploadErrAgentHome = prefix+"BulkErrAgentBean"+suffix;
	public final static String BulkUploadErrLogAgentHome = prefix+"BulkErrLogAgentBean"+suffix;
	public final static String BulkTemplateAgentHome = prefix+"BulkTemplateAgentBean"+suffix;
	public final static String BulkTemplateDetAgentHome = prefix+"BulkTempateDetailAgentBean"+suffix;
	public final static String SpecimenApndxAgentHome = prefix+"SpecimenApndxAgentBean"+suffix;
	public final static String SpecimenStatusAgentHome = prefix+"SpecimenStatusAgentBean"+suffix;
	public final static String StatusHistoryAgentHome = prefix+"StatusHistoryAgentBean"+suffix;
	public final static String StdSiteAddInfoAgentHome = prefix+"StdSiteAddInfoAgentBean"+suffix;
	public final static String StdSiteRightsAgentHome = prefix+"StdSiteRightsAgentBean"+suffix;
	public final static String StorageAgentHome = prefix+"StorageAgentBean"+suffix;
	public final static String StorageAllowedItemsAgentHome = prefix+"StorageAllowedItemsAgentBean"+suffix;
	public final static String StorageStatusAgentHome = prefix+"StorageStatusAgentBean"+suffix;
	public final static String StorageKitAgentHome = prefix+"StorageKitAgentBean"+suffix;
	public final static String StudyAgentHome = prefix+"StudyAgentBean"+suffix;
	public final static String StudyIdAgentHome = prefix+"StudyIdAgentBean"+suffix;
	// Enhancement CTRP-20527 : AGodara
	public final static String StudyINDIDEAgentHome = prefix+"StudyINDIDEAgentBean"+suffix;
	public final static String StudyNotifyAgentHome = prefix+"StudyNotifyAgentBean"+suffix;
	public final static String StudyRightsAgentHome = prefix+"StudyRightsAgentBean"+suffix;
	public final static String StudySiteAgentHome = prefix+"StudySiteAgentBean"+suffix;
	public final static String StudySiteApndxAgentHome = prefix+"StudySiteApndxAgentBean"+suffix;
	public final static String StudyStatusAgentHome = prefix+"StudyStatusAgentBean"+suffix;
	public final static String StudyTXArmAgentHome = prefix+"StudyTXArmAgentBean"+suffix;
	public final static String StudyVerAgentHome = prefix+"StudyVerAgentBean"+suffix;
	public final static String StudyViewAgentHome = prefix+"StudyViewAgentBean"+suffix;
	//Ak:Added for enhancement CTRP-20527_1
	public final static String StudyNIHGrantAgentHome = prefix+"StudyNIHGrantAgentBean"+suffix;
	public final static String SubmissionAgentHome = prefix+"SubmissionAgentBean"+suffix;
	public final static String SubmissionBoardAgentHome = prefix+"SubmissionBoardAgentBean"+suffix;
	public final static String SubmissionProvisoAgentHome = prefix+"SubmissionProvisoAgentBean"+suffix;
	public final static String SubmissionStatusAgentHome = prefix+"SubmissionStatusAgentBean"+suffix;
	public final static String TeamAgentHome = prefix+"TeamAgentBean"+suffix;
	public final static String ULinkAgentHome = prefix+"ULinkAgentBean"+suffix;
	public final static String UpdateSchedulesAgentHome = prefix+"UpdateSchedulesAgentBean"+suffix;
	public final static String UserAgentHome = prefix+"UserAgentBean"+suffix;
	public final static String UserSessionAgentHome = prefix+"UserSessionAgentBean"+suffix;
	public final static String UserSessionLogAgentHome = prefix+"UserSessionLogAgentBean"+suffix;
	public final static String UserSiteAgentHome = prefix+"UserSiteAgentBean"+suffix;
	public final static String UsrGrpAgentHome = prefix+"UsrGrpAgentBean"+suffix;
	public final static String CtrpDraftAgentHome = prefix+"CtrpDraftAgentBean"+suffix;
	public final static String CtrpDraftDocAgentHome = prefix+"CTRPDraftDocAgentBean"+suffix;
	public final static String AuditRowEresAgentHome = prefix+"AuditRowEresAgentBean"+suffix;
	public final static String AuditRowEpatAgentHome = prefix+"AuditRowEpatAgentBean"+suffix;
	
	public final static String UsrSiteGrpAgentBean = prefix+"UsrSiteGrpAgentBean"+suffix;
	
	
	public static final String SPECIMEN_EJBHOME  = "SpecimenAgentBean";

	public static final String SPECIMENSTATUS_EJBHOME = "SpecimenStatusAgentBean";//JM

    public static final String STUDY_EJBHOME = "StudyBean";

    public static final String STUDYAGENT_EJBHOME = "StudyAgentBean";

    public static final String STUDYSECTION_EJBHOME = "SectionBean";

    public static final String STUDYSECTIONAGENT_EJBHOME = "SectionAgentBean";

    public static final String STUDYSTATUS_EJBHOME = "StudyStatusBean";

    public static final String STUDYSTATUSAGENT_EJBHOME = "StudyStatusAgentBean";

    public static final String TEAM_EJBHOME = "TeamBean";

    public static final String TEAMAGENT_EJBHOME = "TeamAgentBean";

    public static final String ACCOUNT_EJBHOME = "AccountBean";

    public static final String ACCOUNTAGENT_EJBHOME = "AccountAgentBean";

    public static final String GROUP_EJBHOME = "GroupBean";

    public static final String GROUPAGENT_EJBHOME = "GroupAgentBean";

    public static final String ADDRESS_EJBHOME = "AddressBean";

    public static final String ADDRESSAGENT_EJBHOME = "AddressAgentBean";

    public static final String MSGCNTR_EJBHOME = "MsgcntrBean";

    public static final String MSGCNTRAGENT_EJBHOME = "MsgcntrAgentBean";

    public static final String USER_EJBHOME = "UserEntityBean";

    public static final String USERAGENT_EJBHOME = "UserAgentBean";

    public static final String SITE_EJBHOME = "SiteBean";

    public static final String SITEAGENT_EJBHOME = "SiteAgentBean";

    public static final String ULINK_EJBHOME = "ULinkBean";

    public static final String ULINKAGENT_EJBHOME = "ULinkAgentBean";

    public static final String GRPRIGHTSAGENT_EJBHOME = "GrpRightsAgentBean";

    public static final String GRPRIGHTS_EJBHOME = "GrpRightsBean";

    public static final String REPORTSAGENT_EJBHOME = "ReportsAgentBean";

    public static final String USRGRPAGENT_EJBHOME = "UsrGrpAgentBean";

    public static final String USRGRP_EJBHOME = "UsrGrpBean";

    public static final String APPENDIXAGENT_EJBHOME = "AppendixAgentBean";

    public static final String APPENDIX_EJBHOME = "AppendixBean";

    public static final String REPORTAGENT_EJBHOME = "ReportAgentBean";

    public static final String ACCOUNTWRAPPERAGENT_EJBHOME = "AccountWrapperAgentBean";

    public static final String STUDYRIGHTSAGENT_EJBHOME = "StudyRightsAgentBean";

    public static final String STUDYRIGHTS_EJBHOME = "StudyRightsBean";

    public static final String STUDYNOTIFYAGENT_EJBHOME = "StudyNotifyAgentBean";

    public static final String STUDYNOTIFY_EJBHOME = "StudyNotifyBean";

    public static final String EXTURIGHTSAGENT_EJBHOME = "ExtURightsAgentBean";

    public static final String EXTURIGHTS_EJBHOME = "ExtURightsBean";

    public static final String CODELSTAGENT_EJBHOME = "CodelstAgentBean";

    public static final String CODELST_EJBHOME = "CodelstBean";


    //KM:20,May2006-Newly added for adding data in the ctrl tab table for tech. support work requirement
    public static final String CTRLTABAGENT_EJBHOME = "CtrltabAgentBean";

    public static final String CTRLTAB_EJBHOME = "CtrltabBean";

    public static final String PREFAGENT_EJBHOME = "PrefAgentBean";

    public static final String PREF_EJBHOME = "PrefBean";

    public static final String PERSONAGENT_EJBHOME = "PersonAgentBean";

    public static final String PERSON_EJBHOME = "PersonBean";

    public static final String ERESEARCH_DATASOURCE = "java:comp/env/eres";

    public static final String ERESEARCH_PAT_DATASOURCE = "java:comp/env/perjunk";

    public static final String PATPROT_EJBHOME = "PatProtBean";

    public static final String PATPROTAGENT_EJBHOME = "PatProtAgentBean";

    public static final String PATSTUDYSTAT_EJBHOME = "PatStudyStatBean";

    public static final String PATSTUDYSTATAGENT_EJBHOME = "PatStudyStatAgentBean";

    public static final String ESCHREPORTSAGENT_EJBHOME = "EschReportsAgentBean";

    public static final String STUDYVIEW_EJBHOME = "StudyViewBean";

    public static final String STUDYVIEWAGENT_EJBHOME = "StudyViewAgentBean";

    public static final String MILESTONE_EJBHOME = "MilestoneBean";

    public static final String MILESTONEAGENT_EJBHOME = "MilestoneAgentBean";

    public static final String MILEPAYMENT_EJBHOME = "MilepaymentBean";

    public static final String MILEPAYMENTAGENT_EJBHOME = "MilepaymentAgentBean";

    public static final String SAVEDREP_EJBHOME = "SavedRepBean";

    public static final String SAVEDREPAGENT_EJBHOME = "SavedRepAgentBean";

    public static final String STUDYVER_EJBHOME = "StudyVerBean";

    public static final String STUDYVERAGENT_EJBHOME = "StudyVerAgentBean";

    public static final String MILEAPNDXAGENTBEAN_EJBHOME = "MileApndxAgentBean";

    public static final String MILEAPNDXBEAN_EJBHOME = "MileApndxBean";

    public static final String PERAPNDXAGENTBEAN_EJBHOME = "PerApndxAgentBean";

    public static final String PERAPNDXBEAN_EJBHOME = "PerApndxBean";

    public static final String USERSESSIONLOGAGENTBEAN_EJBHOME = "UserSessionLogAgentBean";

    public static final String USERSESSIONLOGBEAN_EJBHOME = "UserSessionLogBean";

    public static final String USERSESSIONAGENTBEAN_EJBHOME = "UserSessionAgentBean";

    public static final String USERSESSIONBEAN_EJBHOME = "UserSessionBean";

    public static final String USERSITE_EJBHOME = "UserSiteBean";

    public static final String USERSITEAGENT_EJBHOME = "UserSiteAgentBean";

    public static final String FORMNOTIFY_EJBHOME = "FormNotifyBean";

    public static final String FORMNOTIFYAGENT_EJBHOME = "FormNotifyAgentBean";

    public static final String CATLIB_EJBHOME = "CatLibBean";

    public static final String CATLIBAGENT_EJBHOME = "CatLibAgentBean";

    public static final String FLDRESP_EJBHOME = "FldRespBean";

    public static final String FLDRESPAGENT_EJBHOME = "FldRespAgentBean";

    public static final String FIELDLIB_EJBHOME = "FieldLibBean";

    public static final String FIELDLIBAGENT_EJBHOME = "FieldLibAgentBean";

    public static final String FORMLIB_EJBHOME = "FormLibBean";

    public static final String FORMLIBAGENT_EJBHOME = "FormLibAgentBean";

    public static final String FORMFIELD_EJBHOME = "FormFieldBean";

    public static final String FORMFIELDAGENT_EJBHOME = "FormFieldAgentBean";

    public static final String LINKEDFORMS_EJBHOME = "LinkedFormsBean";

    public static final String LINKEDFORMSAGENT_EJBHOME = "LinkedFormsAgentBean";

    public static final String FORMSEC_EJBHOME = "FormSecBean";

    public static final String FORMSECAGENT_EJBHOME = "FormSecAgentBean";

    public static final String FORMSTAT_EJBHOME = "FormStatBean";

    public static final String FORMSTATAGENT_EJBHOME = "FormStatAgentBean";

    public static final String GRPSUPUSER_RIGHTSAGENT_EJBHOME = "GrpSupUserRightsAgentBean";

    public static final String GRPSUPUSER_RIGHTS_EJBHOME = "GrpSupUserRightsBean";

    public static final String STUDYIDAGENT_EJBHOME = "StudyIdAgentBean";

    public static final String STUDYID_EJBHOME = "StudyIdBean";

    public static final String STUDYSITEAGENT_EJBHOME = "StudySiteAgentBean";

    public static final String STUDYSITE_EJBHOME = "StudySiteBean";

    public static final String PERIDAGENT_EJBHOME = "PerIdAgentBean";

    public static final String PERID_EJBHOME = "PerIdBean";

    public static final String DYNREPAGENT_EJBHOME = "DynRepAgentBean";

    public static final String DYNREP_EJBHOME = "DynRepBean";

    public static final String DYNREPDTAGENT_EJBHOME = "DynRepDtAgentBean";

    public static final String DYNREPDT_EJBHOME = "DynRepDtBean";

    public static final String DYNREPFLTRAGENT_EJBHOME = "DynRepFltrAgentBean";

    public static final String DYNREPFLTR_EJBHOME = "DynRepFltrBean";

    public static final String FLDVALIDATE_EJBHOME = "FldValidateBean";

    public static final String FLDVALIDATEAGENT_EJBHOME = "FldValidateAgentBean";

    public static final String BROWSERROWSAGENT_EJBHOME = "BrowserRowsAgentBean";

    public static final String LABAGENT_EJBHOME = "LabAgentBean";

    public static final String LAB_EJBHOME = "LabBean";

    public static final String STATUSHISTORY_EJBHOME = "StatusHistoryBean";

    public static final String STATUSHISTORYAGENT_EJBHOME = "StatusHistoryAgentBean";

    public static final String STUDYSITEAPNDX_EJBHOME = "StudySiteApndxBean";

    public static final String STUDYSITEAPNDXAGENT_EJBHOME = "StudySiteApndxAgentBean";

    public static final String STDSITEADDINFO_EJBHOME = "StdSiteAddInfoBean";

    public static final String STDSITEADDINFOAGENT_EJBHOME = "StdSiteAddInfoAgentBean";

    public static final String STDSITERIGHTS_EJBHOME = "StdSiteRightsBean";

    public static final String STDSITERIGHTSAGENT_EJBHOME = "StdSiteRightsAgentBean";

    public static final String FORMQUERY_EJBHOME = "FormQueryBean";

    public static final String FORMQUERYAGENT_EJBHOME = "FormQueryAgentBean";

    public static final String FORMQUERYSTATUS_EJBHOME = "FormQueryStatusBean";

    public static final String FORMQUERYSTATUSAGENT_EJBHOME = "FormQueryStatusAgentBean";

    public static final String STUDYTXARM_EJBHOME = "StudyTXArmBean";

    public static final String STUDYTXARMAGENT_EJBHOME = "StudyTXArmAgentBean";

    public static final String PATTXARM_EJBHOME = "PatTXArmBean";

    public static final String PATTXARMAGENT_EJBHOME = "PatTXArmAgentBean";

    public static final String COMMONAGENT_EJBHOME = "CommonAgentBean";

    public static final String PORTAL_EJBHOME ="PortalAgentBean";//KM

    //JM: 040407
    public static final String PATLOGIN_EJBHOME = "PatLoginBean";

    public static final String PATLOGINAGENT_EJBHOME = "PatLoginAgentBean";

    public static final String PORTALDESIGN_EJBHOME = "PortalDesignAgentBean";

    public static final String STORAGE_EJBHOME = "StorageAgentBean";//KM

    public static final String STORAGESTATUS_EJBHOME = "StorageStatusAgentBean";//KM

    public static final String STORAGEALLOWEDITEMS_EJBHOME = "StorageAllowedItemsAgentBean";//KM

    public static final String SPECIMENAPPENDX_EJBHOME = "SpecimenApndxAgentBean";//KN

    public static final String UPDATESCHEDULESAGENT_EJBHOME = "UpdateSchedulesAgentBean";//KM

    public static final String FORMACTION_EJBHOME = "FormActionAgentBean";

    public static final String STORAGEKIT_EJBHOME = "StorageKitBean";

    public static final String STORAGEKITAGENT_EJBHOME = "StorageKitAgentBean";


}
