package com.velos.eres.service.util;

import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.common.FormCompareUtilityDao;

/**
 * 
 *  Utility Class to compare Form Response values in XML with values stored in er_formslinear 
 */
public class CompareFormResponses extends Thread  {

    public String formType;
    public int formId;
    public Hashtable htParam;
    
    public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	
	
	public CompareFormResponses() {
		htParam = new Hashtable();
         
    }

	
    

	public Hashtable getHtParam() {
		return htParam;
	}

	public void setHtParam(Hashtable htParam) {
		this.htParam = htParam;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}
    
     
    /**
     * Overrides Thread's run method.
     */
    public void run() {
    	CompareFormResponses.startComparing(this);
    }
    
    /**
     * A Wrapper method to call appropriate form comparison method
     * @param compObj
     *            CompareFormResponses Object
     * 
     */
    public static synchronized void startComparing(CompareFormResponses compObj) {
    
    		if (!StringUtil.isEmpty(compObj.getFormType()) && compObj.getFormType().equals("P"))
    		{
    			FormCompareUtilityDao fCDao = new FormCompareUtilityDao();
    			
    			fCDao.setFormId(compObj.getFormId());
    			
    			fCDao.comparePatientFormResponses(compObj.getHtParam());
    			
    		}
    
    }
    
    
}
