package com.velos.eres.service.util;

import java.lang.reflect.Field;

/**
 * ES stands for "eSample". This class holds in memory all the value strings
 * in esampleBundle.properties file. Make all strings public static.
 */

public class ES {
	public static String getLabelByKey(String key) {
		String labelText ="" ;
		synchronized(ES.class) {
			try {
				Field field = ES.class.getField(key);
				try {
					labelText = (String)field.get(null);
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			} catch (SecurityException e1) {
				e1.printStackTrace();
			} catch (NoSuchFieldException e1) {
				e1.printStackTrace();
			}
		}
		return labelText;
	}
		
	public static void reload() {
		synchronized(ES.class) {
			Field[] fields = ES.class.getFields();
			for (Field field : fields) {
				try {
					if (field.getName() != null && field.getName().endsWith("_Upper")) {
						field.set(null, VelosResourceBundle.geteSampleString(
								field.getName().replaceAll("_Upper", "")).toUpperCase());
					} else if (field.getName() != null && field.getName().endsWith("_Lower")) {
						field.set(null, VelosResourceBundle.geteSampleString(
								field.getName().replaceAll("_Lower", "")).toLowerCase());
					} else {
						field.set(null, VelosResourceBundle.geteSampleString(field.getName()));
					}
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
	   
    public static String ES_StrtNew_Upload = VelosResourceBundle.geteSampleString("ES_StrtNew_Upload");
    public static String ES_Selc_File = VelosResourceBundle.geteSampleString("ES_Selc_File");
    public static String ES_PlsSelc_DotCSVFile = VelosResourceBundle.geteSampleString("ES_PlsSelc_DotCSVFile");
    public static String ES_Selc_Mapp = VelosResourceBundle.geteSampleString("ES_Selc_Mapp");
    public static String ES_Create_NewMAppt = VelosResourceBundle.geteSampleString("ES_Create_NewMAppt");
    public static String ES_ToRow_InFile = VelosResourceBundle.geteSampleString("ES_ToRow_InFile");
    public static String ES_UpldInPrgess = VelosResourceBundle.geteSampleString("ES_UpldInPrgess");
    public static String ES_ViewBlk_FldMapp = VelosResourceBundle.geteSampleString("ES_ViewBlk_FldMapp");
    public static String ES_InstBulk_UpldData = VelosResourceBundle.geteSampleString("ES_InstBulk_UpldData");
    public static String ES_BulkUpld_Det = VelosResourceBundle.geteSampleString("ES_BulkUpld_Det");
    public static String ES_View_Mapp = VelosResourceBundle.geteSampleString("ES_View_Mapp");
    public static String ES_VelosEsam_FldName = VelosResourceBundle.geteSampleString("ES_VelosEsam_FldName");
    public static String ES_File_FldNAme = VelosResourceBundle.geteSampleString("ES_File_FldNAme");
    public static String ES_Uplod = VelosResourceBundle.geteSampleString("ES_Uplod");
    public static String ES_Uplod_Data = VelosResourceBundle.geteSampleString("ES_Uplod_Data");
    public static String ES_Manage_MApp = VelosResourceBundle.geteSampleString("ES_Manage_MApp");
    public static String ES_PrevSaved_Mapp = VelosResourceBundle.geteSampleString("ES_PrevSaved_Mapp");
    public static String ES_MApp_NAme = VelosResourceBundle.geteSampleString("ES_MApp_NAme");
    public static String ES_Crted_By = VelosResourceBundle.geteSampleString("ES_Crted_By");
    public static String ES_BulkUpld_Area = VelosResourceBundle.geteSampleString("ES_BulkUpld_Area");
    public static String ES_StatNew_Uplod = VelosResourceBundle.geteSampleString("ES_StatNew_Uplod");
    public static String ES_Mang_MApp = VelosResourceBundle.geteSampleString("ES_Mang_MApp");
    public static String ES_Upld_Histy = VelosResourceBundle.geteSampleString("ES_Upld_Histy");
    public static String ES_DateOf_Upld = VelosResourceBundle.geteSampleString("ES_DateOf_Upld");
    public static String ES_Upld_By = VelosResourceBundle.geteSampleString("ES_Upld_By");
    public static String ES_Upld_Log = VelosResourceBundle.geteSampleString("ES_Upld_Log");
    public static String ES_Upld_Num = VelosResourceBundle.geteSampleString("ES_Upld_Num");
    public static String ES_TotFile_Rec = VelosResourceBundle.geteSampleString("ES_TotFile_Rec");
    public static String ES_RecUpld_Succ = VelosResourceBundle.geteSampleString("ES_RecUpld_Succ");
    public static String ES_RecNot_UpldSucc = VelosResourceBundle.geteSampleString("ES_RecNot_UpldSucc");
    public static String ES_Row_Num = VelosResourceBundle.geteSampleString("ES_Row_Num");
    public static String ES_Faiure_Reason = VelosResourceBundle.geteSampleString("ES_Faiure_Reason");
    public static String ES_BulkUpld_Mapp = VelosResourceBundle.geteSampleString("ES_BulkUpld_Mapp");
    public static String ES_MappName_AllExst = VelosResourceBundle.geteSampleString("ES_MappName_AllExst");
    public static String ES_Pls_Sel = VelosResourceBundle.geteSampleString("ES_Pls_Sel");
    public static String ES_EitherSelc_AutoSpec_OrMap = VelosResourceBundle.geteSampleString("ES_EitherSelc_AutoSpec_OrMap");
    public static String ES_PlsSelc_FileHeader = VelosResourceBundle.geteSampleString("ES_PlsSelc_FileHeader");
    public static String ES_FileHeaderUnq_ForSuppFld = VelosResourceBundle.geteSampleString("ES_FileHeaderUnq_ForSuppFld");
    public static String ES_PlsSelc_SuppFld = VelosResourceBundle.geteSampleString("ES_PlsSelc_SuppFld");
    public static String ES_PlsEntr_MappName = VelosResourceBundle.geteSampleString("ES_PlsEntr_MappName");
    public static String ES_WantToSave_Mapp = VelosResourceBundle.geteSampleString("ES_WantToSave_Mapp");
    public static String ES_Strt_NewUpld = VelosResourceBundle.geteSampleString("ES_Strt_NewUpld");
    public static String ES_FldMapp = VelosResourceBundle.geteSampleString("ES_FldMapp");
    public static String ES_FileFld_Name = VelosResourceBundle.geteSampleString("ES_FileFld_Name");
    public static String ES_SaveMApp_As = VelosResourceBundle.geteSampleString("ES_SaveMApp_As");
    public static String ES_AutoSpecId_NotProv_InFileOrMapp = VelosResourceBundle.geteSampleString("ES_AutoSpecId_NotProv_InFileOrMapp");
    public static String ES_SelcFile_IsEmpt = VelosResourceBundle.geteSampleString("ES_SelcFile_IsEmpt");
    public static String ES_PlsEntVald_CsvFile = VelosResourceBundle.geteSampleString("ES_PlsEntVald_CsvFile");
    public static String ES_PlsEnter_DotCsvFile = VelosResourceBundle.geteSampleString("ES_PlsEnter_DotCsvFile");
    public static String ES_Rows_Imported = VelosResourceBundle.geteSampleString("ES_Rows_Imported");
    public static String ES_Issue_Desc = VelosResourceBundle.geteSampleString("ES_Issue_Desc");
    public static String ES_BlkUpld_Prev = VelosResourceBundle.geteSampleString("ES_BlkUpld_Prev");
    public static String ES_ErrProc_TryAgn = VelosResourceBundle.geteSampleString("ES_ErrProc_TryAgn");
}
