package com.velos.eres.service.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class VelosResourceBundle {
    
    /**
     * List of available resource bundles in properties files. 
     * For each type of bundle, there is a custom bundle.
     *   (e.g. labelBundle_custom.properties for labelBundle.properties)
     * Any key-value pair in the custom bundle will override the corresponding
     * pair in the default bundle.
     */
    public static final String LABEL_BUNDLE   = "labelBundle";
    public static final String MESSAGE_BUNDLE = "messageBundle";
    public static final String ESAMPLE_BUNDLE = "eSampleBundle";
    public static final String[] AVAILABLE_BUNDLES = {LABEL_BUNDLE, MESSAGE_BUNDLE, ESAMPLE_BUNDLE};
    public static final String CUSTOM_SUFFIX = "_custom";
    public static final String CHARSET = "UTF-8";
    
    private static Hashtable<String, ResourceBundle> hash = null;
    private static String eHome = "";
    
    /**
     * Private constructor to keep people from creating multiple instances of this class
     */
    private VelosResourceBundle() {}
    
    /**
     * The static block is called at initial load time and only once.
     * It loads all the available bundles plus the custom bundles.
     */
    static {
        synchronized(VelosResourceBundle.class) {
            // Get ERES_HOME where the properties files are stored
            try {
                eHome = EnvUtil.getEnvVariable("ERES_HOME");
            } catch(Exception e) {
                eHome = "";
            }
            if (eHome == null || eHome.trim().equals("%ERES_HOME%")) {
                eHome = StringUtil.trueValue(System.getProperty("ERES_HOME"));
            }
            eHome = eHome.trim();
            if (hash == null) {
                hash = new Hashtable<String, ResourceBundle>();
            }
            // Read the default bundles and the respective custom bundles (if exist)
            FileInputStream stream = null;
            InputStreamReader reader = null;
            for(int iX=0; iX<AVAILABLE_BUNDLES.length; iX++) {
                try {
                    stream = new FileInputStream(eHome+AVAILABLE_BUNDLES[iX]+".properties");
                    reader = new InputStreamReader(stream, CHARSET);
                    hash.put(AVAILABLE_BUNDLES[iX], new PropertyResourceBundle(reader));
                } catch(IOException e) {
                    Rlog.fatal("resourceBundle", "VelosResourceBundle could not read "+AVAILABLE_BUNDLES[iX]+" "+e);
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                            reader = null;
                        } catch(IOException e) {}
                    }
                    if (stream != null) {
                        try {
                            stream.close();
                            stream = null;
                        } catch(IOException e) {}
                    }
                }
                try {
                    stream = new FileInputStream(eHome+AVAILABLE_BUNDLES[iX]+CUSTOM_SUFFIX+".properties");
                    reader = new InputStreamReader(stream, CHARSET);
                    hash.put(AVAILABLE_BUNDLES[iX]+CUSTOM_SUFFIX, new PropertyResourceBundle(reader));
                } catch(IOException e) {
                    Rlog.info("resourceBundle", "Nonexisting custom bundle: "+AVAILABLE_BUNDLES[iX]+CUSTOM_SUFFIX+" "+e);
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                            reader = null;
                        } catch(IOException e) {}
                    }
                    if (stream != null) {
                        try {
                            stream.close();
                            stream = null;
                        } catch(IOException e) {}
                    }
                }
            }
        }
    }
    
    /**
     * This method reloads all the listed bundles and their custom bundles.
     * 
     * This is a convenience method for developers during development time only. The use of this is 
     * not intended to be left in the eResearch application.
     */
    public static void reloadProperties() {
        synchronized(VelosResourceBundle.class) {
            Enumeration<String> keyEnum = hash.keys();
            FileInputStream stream = null;
            InputStreamReader reader = null;
            while(keyEnum.hasMoreElements()) {
                String key = keyEnum.nextElement();
                try {
                    stream = new FileInputStream(eHome+key+".properties");
                    reader = new InputStreamReader(stream, CHARSET);
                    hash.remove(key);
                    hash.put(key, new PropertyResourceBundle(reader));
                } catch(IOException e) {
                    Rlog.fatal("resourceBundle", "VelosResourceBundle could not read "+key+" "+e);
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                            reader = null;
                        } catch(IOException e) {}
                    }
                    if (stream != null) {
                        try {
                            stream.close();
                            stream = null;
                        } catch(IOException e) {}
                    }
                }
            }
        }
    }
    
    /**
     * This method loads a properties file that has an extension to its name.
     *   e.g. labelBundle_custom.properties for labelBundle.properties
     *   
     * If the extension is empty, it will not append an underscore.
     * 
     * @param bundleName - base name of the bundle
     * @param extension - extension without the first underscore
     */
    private static void loadExtendedProperties(String bundleName, String extension) {
        synchronized(VelosResourceBundle.class) {
            if (bundleName == null) { return; }
            String ext = StringUtil.isEmpty(extension) ? "" : "_" + extension;
            String fullName = bundleName + ext;
            FileInputStream stream = null;
            InputStreamReader reader = null;
            try {
                stream = new FileInputStream(eHome+fullName+".properties");
                reader = new InputStreamReader(stream, CHARSET);
                hash.remove(fullName);
                hash.put(fullName, new PropertyResourceBundle(reader));
            } catch(IOException e) {
                Rlog.fatal("resourceBundle", "VelosResourceBundle could not read "+fullName+" "+e);
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                        reader = null;
                    } catch(IOException e) {}
                }
                if (stream != null) {
                    try {
                        stream.close();
                        stream = null;
                    } catch(IOException e) {}
                }
            }
        }
    }
    
    /**
     * Use this only to access the bundle itself. But preferably use one of getXxxString() methods 
     * instead. This method does not handle the overriding with custom bundle.
     * 
     * @param bundleName
     * @return ResourceBundle
     */
    public static ResourceBundle getBundle(String bundleName) {
        return hash.get(bundleName);
    }

    /**
     * Use this only to access the bundle itself. But preferably use one of getXxxString() methods 
     * instead. This method does not handle the overriding with custom bundle. This looks for the bundle 
     * with the locale suffix first. That does not exist, it returns the default bundle.
     * 
     * @param bundleName
     * @param locale
     * @return ResourceBundle
     */
    public static ResourceBundle getBundle(String bundleName, Locale locale) {
        if (bundleName == null || locale == null) { return null; }
        String fullName = bundleName+"_"+locale.toString();
        ResourceBundle rb = hash.get(fullName);
        if (rb != null) { return rb; }
        loadExtendedProperties(bundleName, locale.toString());
        if (hash.get(fullName) != null) { return hash.get(fullName); }
        return hash.get(bundleName);
    }
    
    /**
     * This gets the value of the string based on the bundle name and the key. It first looks for
     * the key-value pair in the custom bundle. If it does not exist there, it looks for the same in
     * the default bundle. 
     * 
     * @param bundleName - e.g. VelosResourceBundle.LABEL_BUNDLE
     * @param key - as defined in the properties of the bundle
     * @param arguments - any number of arguments that can be passed in to replace the
     *        parameters ({0}, {1},...) in the value string
     * @return - value for the specified key. If either the bundle or key-value does not exist,
     *        it returns a "" string. It does not throw an exception. 
     */
    public static String getString(String bundleName, String key, Object... arguments) {
        if (bundleName == null || key == null) { return ""; }
        // Try to get string from custom bundle
        String pattern = null;
        if (hash.get(bundleName+CUSTOM_SUFFIX) != null) {
            try { pattern = hash.get(bundleName+CUSTOM_SUFFIX).getString(key); } catch(Exception e) {}
        }
        String value = null;
        if (pattern != null && pattern.length() > 0) {
            try {
                value = MessageFormat.format(pattern, arguments);
            } catch(Exception e) {}
            return value == null ? "" : value;
        }
        if (hash.get(bundleName) == null) { return ""; }
        // String not customized; get string from default bundle
        try { pattern = hash.get(bundleName).getString(key); } catch(Exception e) {}
        if (pattern == null || pattern.length() == 0) { return ""; }
        try {
            value = MessageFormat.format(pattern, arguments);
        } catch(Exception e) {}
        return value == null ? "" : value;
    }
    
    /**
     * This gets the value of the string from the label bundle based on the key. It first looks for
     * the key-value pair in the custom bundle. If it does not exist there, it looks for the same in
     * the default bundle. 
     * 
     * @param key - as defined in the properties of the label bundle
     * @param arguments - any number of arguments that can be passed in to replace the
     *        parameters ({0}, {1},...) in the value string
     * @return - value for the specified key. If either the bundle or key-value does not exist,
     *        it returns a "" string. It does not throw an exception.
     */
    public static String getLabelString(String key, Object... arguments) {
        if (key == null) { return ""; }
        // Try to get string from custom bundle
        String pattern = null;
        if (hash.get(LABEL_BUNDLE+CUSTOM_SUFFIX) != null) {
            try { pattern = hash.get(LABEL_BUNDLE+CUSTOM_SUFFIX).getString(key); } catch(Exception e) {}
        }
        String value = null;
        if (pattern != null && pattern.length() > 0) {
            try {
                value = MessageFormat.format(pattern, arguments);
            } catch(Exception e) {}
            return value == null ? "" : value;
        }

        // String not customized; get string from default bundle
        if (hash.get(LABEL_BUNDLE) == null) {
            // This should never happen
            Rlog.fatal("resourceBundle", "FATAL - missing bundle: "+LABEL_BUNDLE);
            return "";
        }
        try { pattern = hash.get(LABEL_BUNDLE).getString(key); } catch(Exception e) {}
        if (pattern == null || pattern.length() == 0) { return ""; }
        try {
            value = MessageFormat.format(pattern, arguments);
        } catch(Exception e) {}
        return value == null ? "" : value;
    }
    
    /**
     * @param key - as defined in the properties of the label bundle
     * @return - returns Upper Case value for the specified key. 
     */
    public static String getLabelStringUpper(String key){
    	Object[] dummy = {}; 
    	String valUpper = getLabelString(key, dummy);
    	return valUpper.toUpperCase();
    }
    
    /**
     * @param key - as defined in the properties of the label bundle
     * @return - returns Lower Case value for the specified key. 
     */
    public static String getLabelStringLower(String key){
    	Object[] dummy = {}; 
    	String valLower = getLabelString(key, dummy);
    	return valLower.toLowerCase();
    }
    
    /**
     * This is a wrapper method for getLabelString(String key, Object... arguments).
     * This will be removed when the JSP compiler is made JDK5,6-compatible.
     * 
     * @param key - as defined in the properties of the label bundle
     * @return - value for the specified key. If either the bundle or key-value does not exist,
     *        it returns a "" string. It does not throw an exception.
     */
    public static String getLabelString(String key) {
        Object[] dummy = {}; 
        return getLabelString(key, dummy);
    }

    /**
     * This gets the value of the string from the message bundle based on the key. It first looks for
     * the key-value pair in the custom bundle. If it does not exist there, it looks for the same in
     * the default bundle. 
     * 
     * @param key - as defined in the properties of the message bundle
     * @param arguments - any number of arguments that can be passed in to replace the
     *        parameters ({0}, {1},...) in the value string
     * @return - value for the specified key. If either the bundle or key-value does not exist,
     *        it returns a "" string. It does not throw an exception.
     */
    public static String getMessageString(String key, Object... arguments) {
        if (key == null) { return ""; }
        // Try to get string from custom bundle
        String pattern = null;
        if (hash.get(MESSAGE_BUNDLE+CUSTOM_SUFFIX) != null) {
            try { pattern = hash.get(MESSAGE_BUNDLE+CUSTOM_SUFFIX).getString(key); } catch(Exception e) {}
        }
        String value = null;
        if (pattern != null && pattern.length() > 0) {
            try {
                value = MessageFormat.format(pattern, arguments);
            } catch(Exception e) {}
            return value == null ? "" : value;
        }

        // String not customized; get string from default bundle
        if (hash.get(MESSAGE_BUNDLE) == null) {
            // This should never happen
            Rlog.fatal("resourceBundle", "FATAL - missing bundle: "+MESSAGE_BUNDLE);
            return "";
        }
        try { pattern = hash.get(MESSAGE_BUNDLE).getString(key); } catch(Exception e) {}
        if (pattern == null || pattern.length() == 0) { return ""; }
        try {
            value = MessageFormat.format(pattern, arguments);
        } catch(Exception e) {}
        return value == null ? "" : value;
    }
    
    /**
     * This is a wrapper method for getMessageString(String key, Object... arguments).
     * This will be removed when the JSP compiler is made JDK5,6-compatible.
     * 
     * @param key - as defined in the properties of the message bundle
     * @return - value for the specified key. If either the bundle or key-value does not exist,
     *        it returns a "" string. It does not throw an exception.
     */
    public static String getMessageString(String key) {
        Object[] dummy = {}; 
        return getMessageString(key, dummy);
    }
    
    /**
     * This is a wrapper method for getMessageStringUpper(String key, Object... arguments).
     * This will be removed when the JSP compiler is made JDK5,6-compatible.
     * 
     * @param key - as defined in the properties of the message bundle
     * @return - value for the specified key. If either the bundle or key-value does not exist,
     *        it returns a "" string. It does not throw an exception.
     */
    public static String getMessageStringUpper(String key) {
        Object[] dummy = {}; 
        return getMessageString(key, dummy);
    }
    
    /*BayaTree ES - eSample specific code below - Pritam Singh*/

    public static String geteSampleString(String key, Object... arguments) {
        if (key == null) { return ""; }
        // Try to get string from eSample bundle
        String pattern = null;
        if (hash.get(ESAMPLE_BUNDLE+CUSTOM_SUFFIX) != null) {
            try { pattern = hash.get(ESAMPLE_BUNDLE+CUSTOM_SUFFIX).getString(key); } catch(Exception e) {}
        }
        String value = null;
        if (pattern != null && pattern.length() > 0) {
            try {
                value = MessageFormat.format(pattern, arguments);
            } catch(Exception e) {}
            return value == null ? "" : value;
        }

        // String not customized; get string from default bundle
        if (hash.get(ESAMPLE_BUNDLE) == null) {
            // This should never happen
            Rlog.fatal("resourceBundle", "FATAL - missing bundle: "+ESAMPLE_BUNDLE);
            return "";
        }
        try { pattern = hash.get(ESAMPLE_BUNDLE).getString(key); } catch(Exception e) {}
        if (pattern == null || pattern.length() == 0) { return ""; }
        try {
            value = MessageFormat.format(pattern, arguments);
        } catch(Exception e) {}
        return value == null ? "" : value;
    }


    /**
     * @param key - as defined in the properties of the eSample bundle
     * @return - returns Upper Case value for the specified key. 
     */
    public static String geteSampleStringUpper(String key){
    	Object[] dummy = {}; 
    	String valUpper = geteSampleString(key, dummy);
    	return valUpper.toUpperCase();
    }

    /**
     * @param key - as defined in the properties of the eSample bundle
     * @return - returns Lower Case value for the specified key. 
     */
    public static String geteSampleStringLower(String key){
    	Object[] dummy = {}; 
    	String valLower = geteSampleString(key, dummy);
    	return valLower.toLowerCase();
    }

    /**
     * This is a wrapper method for geteSampleString(String key, Object... arguments).
     * This will be removed when the JSP compiler is made JDK5,6-compatible.
     * 
     * @param key - as defined in the properties of the eSample bundle
     * @return - value for the specified key. If either the bundle or key-value does not exist,
     *        it returns a "" string. It does not throw an exception.
     */
    public static String geteSampleString(String key) {
        Object[] dummy = {}; 
        return geteSampleString(key, dummy);
    }    
}
