/*
 * Classname			CodelstAgent.class
 * 
 * Version information   
 *
 * Date					03/19/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.codelstAgent;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.velos.eres.business.codelst.impl.CodelstBean;
import com.velos.eres.business.common.CodeDao;

/**
 * Remote interface for CodelstAgent session EJB
 * 
 * @author dinesh
 */
@Remote
public interface CodelstAgentRObj {
    /**
     * gets the codelst details
     */
    CodelstBean getCodelstDetails(int codelstId);

    /**
     * sets the codelst details
     */
    public int setCodelstDetails(CodelstBean codelst);

    /**
     * updates the codelst
     */

    public int updateCodelst(CodelstBean clsk);

    public CodeDao getCodelstData(int accId, String codelstType);

    public String getCodeDescription(int codeId);
    
    public String getCodeCustomCol(int codeId);

    public CodeDao getRolesForEvent(int eventId);

    public CodeDao getDescForEventUsers(ArrayList eventUserIds,
            ArrayList userTypes);

    public CodeDao getCodeDaoInstance();
    
    /**
     * Gets the codeId attribute of the CodeDao object
     * 
     * @param type
     *            codelist type
     * @param subType
     *            codelst subtype
     * @return The codeId value
     */
    public int getCodeId(String type, String subType); 
    
}