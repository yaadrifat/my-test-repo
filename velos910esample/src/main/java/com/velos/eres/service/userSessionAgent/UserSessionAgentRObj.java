/*
 * Classname			UserSessionAgentRObj
 * 
 * Version information 	1.0
 *
 * Date					03/04/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.userSessionAgent;

import javax.ejb.Remote;

import com.velos.eres.business.userSession.impl.UserSessionBean;

// import com.velos.eres.business.common.MilepaymentDao;

/**
 * Remote interface for UserSessionAgent session EJB
 * 
 * @author Sonia Sahni
 */
@Remote
public interface UserSessionAgentRObj {
    /**
     * gets the userSession details
     */
    public UserSessionBean getUserSessionDetails(int userSessionId);

    /**
     * sets the UserSession details
     */
    public int setUserSessionDetails(UserSessionBean usk);

    public int updateUserSession(UserSessionBean usk);

}
