/*
 * Classname			TeamAgentRObj.class
 * 
 * Version information   
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */
package com.velos.eres.service.teamAgent;
import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.TeamDao;
import com.velos.eres.business.team.impl.TeamBean;

/**
 * 
 * 
 * Remote interface for TeamAgentRObj session EJB
 * 
 * 
 * @author
 * 
 * 
 */
@Remote
public interface TeamAgentRObj
{
    /**
     * 
     * 
     * gets the study details
     * 
     * 
     */
    public TeamBean getTeamDetails(int id);
    /**
     * 
     * 
     * sets the team details
     * 
     * 
     */
    public int setTeamDetails(TeamBean study);
    public int updateTeam(TeamBean tsk);
    public TeamDao getTeamValues(int studyId, int accId);
    public TeamDao getSuperUserTeam(int studyId);
    public TeamDao getBgtSuperUserTeam();
    public TeamDao getTeamRights(int studyId, int userId);
    public int deleteFromTeam(TeamBean tsk);
    // Overloaded for INF-18183 ::: AGodara
    public int deleteFromTeam(TeamBean tsk,Hashtable<String, String> auditInfo);
    public TeamDao getTeamValuesBySite(int studyId, int accId, int orgId);
    public TeamDao findUserInTeam(int studyId, int userId);
    
    public int addUsersToMultipleStudies(String[] userIds, String[] studyIds, String[] roles, int user, String ipAdd);	//JM:
}
