/**
 * Classname			StdSiteRightsAgentRObj
 * 
 * Version information 	1.0
 *
 * Date				11/17/2004
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.stdSiteRightsAgent;

import javax.ejb.Remote;

import com.velos.eres.business.common.StdSiteRightsDao;
import com.velos.eres.business.stdSiteRights.impl.StdSiteRightsBean;

/**
 * Remote interface for StdSiteRightsAgent session EJB
 * 
 * @author Anu Khanna
 */
@Remote
public interface StdSiteRightsAgentRObj {
    /**
     * gets the study site rights details
     */
    StdSiteRightsBean getStdSiteRightsDetails(int stdSiteRightsId);

    /**
     * sets the site details
     */
    public int setStdSiteRightsDetails(StdSiteRightsBean ssk);

    public int updateStdSiteRights(StdSiteRightsBean ssk);

    public int updateStudySite(String[] pkStudySites, String[] siteIds,
            String[] rights, int studyId, int userId, String ipAdd,String creator);

    public StdSiteRightsDao getStudySiteTree(int accountId, int loginUser,
            int studyId);

    public StdSiteRightsDao getStudySitesWithViewRight(int accId, int userId,
            int studyId);

    // public UserSiteDao getSitesWithNewRight(int accId, int userId) throws
    // RemoteException;

    public int createStudySiteRightsData(int user, int account, int studyId,
            int creator, String ipAdd);

    // public int getRightForUserSite(int userId, int siteId) throws
    // RemoteException;

    public int deleteStdSiteRights(int studyId, int userId);

}
