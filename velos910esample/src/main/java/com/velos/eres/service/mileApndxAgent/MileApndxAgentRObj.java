/* class MileApndxAgentRObj.java
 * version 4.0
 * date 04.05.02
 * Author Dinesh Khurana
 * Copy right notice Velos Inc,
 */

package com.velos.eres.service.mileApndxAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.mileApndx.impl.MileApndxBean;

@Remote
public interface MileApndxAgentRObj {
    int setMileApndxDetails(MileApndxBean mask);

    MileApndxBean getMileApndxDetails(int id);

    int updateMileApndx(MileApndxBean mask);

    int removeMileApndx(int id);
    // Overloaded for INF-18183 ::: AGodara 
    int removeMileApndx(int id,Hashtable<String, String> auditInfo);

}
