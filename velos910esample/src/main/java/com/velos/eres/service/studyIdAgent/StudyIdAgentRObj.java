/*
 * Classname : StudyIdAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 09/23/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.service.studyIdAgent;

/* Import Statements */

import javax.ejb.Remote;

import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.studyId.impl.StudyIdBean;

/* End of Import Statements */

/**
 * Remote interface for StudyIdAgent session EJB
 * 
 * @author Sonia Sahni
 * @version : 1.0 09/23/2003
 */
@Remote
public interface StudyIdAgentRObj {
    /**
     * gets the StudyId record
     */
    StudyIdBean getStudyIdDetails(int id);

    /**
     * creates a new StudyId Record
     */
    public int setStudyIdDetails(StudyIdBean ssk);

    /**
     * updates the StudyId Record
     */
    public int updateStudyId(StudyIdBean ssk);

    /**
     * remove StudyId Record
     */
    public int removeStudyId(int id);

    /**
     * create multiple study ids
     * 
     */

    public int createMultipleStudyIds(StudyIdBean ssk,String defuserGroup);

    /**
     * updates multiple study ids
     * 
     */

    public int updateMultipleStudyIds(StudyIdBean ssk);

    /**
     * gets study ids
     * 
     */

    public StudyIdDao getStudyIds(int studyId,String defUserGroup);

}
