/*
 * Classname			SpecimenAgentBean
 * 
 * Version information : 
 *
 * Date					07/09/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.specimenAgent.impl;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.*;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.SpecimenDao;
import com.velos.eres.business.specimen.impl.SpecimenBean;
import com.velos.eres.service.specimenAgent.SpecimenAgentRObj;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;


/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 * 
 * @author Jnanamay Majumdar
 * @version 1.0, 07/09/2007
 * @ejbHome SpecimenAgentHome
 * @ejbRemote SpecimenAgentRObj
 */

@Stateless
public class SpecimenAgentBean implements SpecimenAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    public SpecimenBean getSpecimenDetails(int pkSpec) {
        if (pkSpec == 0) {
            return null;
        }
    	SpecimenBean spec = null;
        try {

        	spec = (SpecimenBean) em.find(SpecimenBean .class, new Integer(pkSpec));
        	
        	CommonDAO cdao = new CommonDAO();
            cdao.populateClob("ER_SPECIMEN","SPEC_NOTES"," where PK_SPECIMEN = " + pkSpec );
            spec.setSpecNote(cdao.getClobData());
            
        } catch (Exception e) {
            Rlog.fatal("specimen",
                    "Error in getSpecimenDetails() in SpecimenAgentBean" + e);
        }

        return spec;
    }
    
    public int setSpecimenDetails(SpecimenBean spsk) {    
    	
        try {
        	
        	SpecimenBean  spec = new SpecimenBean(); 
        	int rowfound = 0;
        	
        	////
        	try {

                Query queryIdentifier = em.createNamedQuery("findBySpecimenIdentifier");
                queryIdentifier.setParameter("specimenId", (spsk.getSpecimenId()).toUpperCase());
                
                //JM: 19Sep2007:
                queryIdentifier.setParameter("fkAccount", spsk.getFkAccount());
                
                ArrayList list = (ArrayList) queryIdentifier.getResultList();
                if (list == null) list = new ArrayList();
                if (list.size() > 0) {
                    rowfound = 1;
                    return -3; // specimen name exists
                }

            } catch (Exception ex) {
                Rlog.fatal("specimen", "In setSpecimenDetails call to find specimen name exception" + ex);
                rowfound = 0; // specimen name does not exist
            }
        	
        	
        	//////
        	
        	 spec.updateSpecimen(spsk);        	 
            em.persist(spec);
            return spec.getPkSpecimen();
        } catch (Exception e) {
        	  Rlog.fatal("specimen",  "Error in setSpecimenDetails() in SpecimenAgentBean" + e);
        }
        return 0;
    }
    
    public int updateSpecimen(SpecimenBean spsk) {
    	SpecimenBean appSpec= null;
    	int rowfound = 0;
        int output;
        try {
        	appSpec = (SpecimenBean) em.find(SpecimenBean .class, new Integer(spsk.getPkSpecimen()));
        	if (appSpec == null) {
                return -2;
            }
            /////////////////

            if (!((appSpec.getSpecimenId()).toUpperCase()).equals((spsk.getSpecimenId()).toUpperCase())) {
                // specimen changed

            	try {

                    Query queryIdentifier = em.createNamedQuery("findBySpecimenIdentifier");
                    queryIdentifier.setParameter("specimenId", (spsk.getSpecimenId()).toUpperCase());
                    
                    //JM: 19Sep2007:
                    queryIdentifier.setParameter("fkAccount", spsk.getFkAccount());
                    
                    ArrayList list = (ArrayList) queryIdentifier.getResultList();
                    if (list == null) list = new ArrayList();
                    if (list.size() > 0) {
                        rowfound = 1;
                        return -3; // specimen name exists
                    }

                } catch (Exception ex) {
                    Rlog.fatal("specimen", "In updateSpecimen call to updateSpecimen ..." + ex);
                    rowfound = 0; // specimen name does not exist
                }

            } // end check if specimen name is changed

            ////////////////
            
            
            output = appSpec.updateSpecimen(spsk);            
            em.merge(appSpec);
        } catch (Exception e) {
        	Rlog.fatal("specimen",  "Error in updateSpecimen() in SpecimenAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    public int deleteSpecimens(String[] deleteIds) {
        SpecimenDao specimenDao = new SpecimenDao();
        int ret = 0;
        try {
            Rlog.debug("specimen", "SpecimenAgentBean:deleteSpecimen- 0");
            ret = specimenDao.deleteSpecimens(deleteIds);

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenAgentBean:deleteSpecimen-Exception In deleteSpecimen "
                    + e);
            return -1;
        }

        return ret;

    }
    
    // for INF-21676 ::: Panjvir
   public HashSet<String> getChildSpecimenId(int Parent_SpecId) 
   { 	
    	SpecimenDao specimenDao = new SpecimenDao();
    	HashSet SpecId =null;
        try {
            Rlog.debug("specimen", "SpecimenAgentBean:getChildSpecimenId ...");
            SpecId = specimenDao.getChildSpecimenId(Parent_SpecId);            

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenAgentBean:getChildSpecimenId-Exception In getSpecimenIdAuto "+ e);
            return SpecId;
        }        
        return SpecId;
    }
   
    // Overloaded for INF-18183 ::: AGodara
    public int deleteSpecimens(String[] deleteIds,Hashtable<String, String> auditInfo){
        SpecimenDao specimenDao = new SpecimenDao();
        int ret = 0;
        Connection conn = null;
        try {
           	StringBuffer buf = new StringBuffer(); 
        	for (int i = 0; i < deleteIds.length; i++) {
    			if (i > 0) {
    				buf.append(",");
    			}
    			if (deleteIds[i] != null) {
    				buf.append(deleteIds[i]);
    			}
    		}
        	String condition ="PK_SPECIMEN IN (" + buf.toString()+ ")";
        	Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("ER_SPECIMEN", condition, "eres");
            conn = AuditUtils.insertAuditRow("ER_SPECIMEN", rowValues, LC.L_Manage_Invent, auditInfo, "eres");
            ret = specimenDao.deleteSpecimens(deleteIds);
            if(ret!=-1){
            	conn.commit();
            }
        } catch (Exception e) {
           	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
            Rlog.fatal("specimen", "SpecimenAgentBean:deleteSpecimen-Exception In deleteSpecimen "
                    + e);
            return -1;
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

        return ret;

    }
    
    
    public String getSpecimenIdAuto() {
    	
    	SpecimenDao specimenDao = new SpecimenDao();
        String SpecId = "";
        try {
            Rlog.debug("specimen", "SpecimenAgentBean:getSpecimenIdAuto ...");
            SpecId = specimenDao.getSpecimenIdAuto();            

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenAgentBean:getSpecimenIdAuto-Exception In getSpecimenIdAuto "
                    + e);
            return SpecId;
        }        
        return SpecId;

    }
    
    public String getSpecimenIdAuto(String studyId, String patientId, int accId) {
        
        SpecimenDao specimenDao = new SpecimenDao();
        String SpecId = "";
        try {
            Rlog.debug("specimen", "SpecimenAgentBean:getSpecimenIdAuto(studyId, patientId) ...");
            SpecId = specimenDao.getSpecimenIdAuto(studyId, patientId, accId);            

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenAgentBean:getSpecimenIdAuto-Exception In getSpecimenIdAuto "
                    + e);
            return SpecId;
        }        
        return SpecId;

    }
    
    public SpecimenDao getSpecimenChilds(int specId, int accId){
    	 try {
             
    		 SpecimenDao specDao = new SpecimenDao();
    		 specDao.getSpecimenChilds(specId, accId);
             return specDao;
             
         } catch (Exception e) {
             Rlog.fatal("specimen", "Exception In getSpecimenChilds in SpecimenAgentBean " + e);
         }
         return null;
    	
    }
    
    public String getParentSpecimenId(int specId) {
    	
    	
    	SpecimenDao specimenDao = new SpecimenDao();
        String parentSpecId = "";
        try {
            Rlog.debug("specimen", "SpecimenAgentBean:getParentSpecimenId ...");
            parentSpecId = specimenDao.getParentSpecimenId(specId);            

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenAgentBean:getParentSpecimenId-Exception In getParentSpecimenId "
                    + e);
            return parentSpecId;
        }        
        return parentSpecId;
    	
    }
  
       
}
   