/**
 * Classname : FormQueryAgentBean
 *
 * Version information: 1.0
 *
 * Date: 01/03/2005
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.formQueryAgent.impl;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.FormQueryDao;
import com.velos.eres.business.formQuery.impl.FormQueryBean;
import com.velos.eres.service.formQueryAgent.FormQueryAgentRObj;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FormQueryAgentBean.<br>
 * <br>
 *
 * @author Anu Khanna
 * @see FormQueryAgentBean
 * @ejbHome formQueryAgentHome
 * @ejbRemote formQueryAgentRObj
 */

@Stateless
public class FormQueryAgentBean implements FormQueryAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     *
     * Looks up on the id parameter passed in and constructs and returns a
     * FormQuery state keeper. containing all the details of the FormQuery <br>
     *
     * @param Form
     *            Query Id
     */

    public FormQueryBean getFormQueryDetails(int formQueryId) {
        FormQueryBean retrieved = null;

        try {
            retrieved = (FormQueryBean) em.find((FormQueryBean.class),
                    new Integer(formQueryId));

            return retrieved;

        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Exception in FormQueryStateKeeper() in FormQueryAgentBean"
                            + e);
        }

        return retrieved;

    }

    /**
     * Creates FormQuery record.
     *
     * @param a
     *            State Keeper containing the formQueryId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setFormQueryDetails(FormQueryBean fqk) {
        try {
            FormQueryBean fb = new FormQueryBean();
            fb.updateFormQuery(fqk);
            em.persist(fb);
            return fb.getFormQueryId();
        } catch (Exception e) {
            Rlog
                    .fatal("formQuery",
                            "Error in setFormQueryDetails() in FormQueryAgentBean "
                                    + e);
            return -2;
        }

    }

    /**
     * Updates a FormQuery record.
     *
     * @param a
     *            FormQuery state keeper containing studySite attributes to be
     *            set.
     * @return int for successful:0 ; e\Exception : -2
     */

    public int updateFormQuery(FormQueryBean fqk) {

        FormQueryBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = (FormQueryBean) em.find((FormQueryBean.class),
                    new Integer(fqk.getFormQueryId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFormQuery(fqk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Error in updateFormQuery in FormQueryAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a FormQuery record.
     *
     * @param a
     *            FormQuery Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeFormQuery(int formQueryId) {
        int output;
        FormQueryBean retrieved = null;

        try {
            retrieved = (FormQueryBean) em.find((FormQueryBean.class),
                    new Integer(formQueryId));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formQuery", "Exception in removeFormQuery" + e);
            return -1;

        }

    }

    //KM-#4016
    public FormQueryDao insertIntoFormQuery(int filledFormId, int formId, int calledFrom,
            int queryType, String[] queryTypeIds, String[] fldSysIds,
            String[] notes, String[] status, String[] comments,
            String[] modes, String[] pkQueryStats, int creator,
            String ipAdd) {

        try {

            FormQueryDao formQueryDao = new FormQueryDao();
            formQueryDao.insertIntoFormQuery(filledFormId, formId, calledFrom,
                    queryType, queryTypeIds, fldSysIds, notes, status, comments, 
                    modes, pkQueryStats, creator, ipAdd);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Exception In insertIntoFormQuery in FormQueryAgentBean "
                            + e);
        }
        return null;
    }

    public FormQueryDao getQueriesForForm(int formId, int queryModuleId, int moduleLinkedTo) {

        try {

            FormQueryDao formQueryDao = new FormQueryDao();
            formQueryDao.getQueriesForForm(formId, queryModuleId, moduleLinkedTo);
            return formQueryDao;
        } catch (Exception e) {
            Rlog
                    .fatal("formQuery",
                            "Exception In getQueriesForForm in FormQueryAgentBean "
                                    + e);
        }
        return null;
    }

    public FormQueryDao getQueryHistoryForField(int formQueryId) {

        try {

            FormQueryDao formQueryDao = new FormQueryDao();
            formQueryDao.getQueryHistoryForField(formQueryId);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Exception In getQueryHistoryForField in FormQueryAgentBean "
                            + e);
        }
        return null;
    }

    public int insertQueryResponse(int filledformId, int calledFrom, int fldId,
            int queryType, int queryTypeId, int queryStatusId, String comments,
            String enteredOn, int enteredBy, int creator, String ipAdd) {
        int ret = 0;

        try {

            FormQueryDao formQueryDao = new FormQueryDao();
            ret = formQueryDao.insertQueryResponse(filledformId, calledFrom,
                    fldId, queryType, queryTypeId, queryStatusId, comments,
                    enteredOn, enteredBy, creator, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Exception In insertQueryResponse in FormQueryAgentBean "
                            + e);
        }
        return ret;
    }

    public FormQueryDao getSystemFormQueryStat(int filledFormId,
            String fldSysIds) {

        try {

            FormQueryDao formQueryDao = new FormQueryDao();
            formQueryDao.getSystemFormQueryStat(filledFormId, fldSysIds);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Exception In getSystemFormQueryStat in FormQueryAgentBean "
                            + e);
        }
        return null;
    }
    
    public FormQueryDao getSystemFormQueryStat(int filledFormId,
			ArrayList fldSysIds, ArrayList fldReasons) {
    	try {

            FormQueryDao formQueryDao = new FormQueryDao();
            formQueryDao.getSystemFormQueryStat(filledFormId, fldSysIds, fldReasons);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Exception In getSystemFormQueryStat in FormQueryAgentBean "
                            + e);
        }
        return null;
	}

    public FormQueryDao getSystemQueryNotes(int formQueryId) {
        FormQueryDao formQueryDao = null;
        try {
            formQueryDao = new FormQueryDao();
            formQueryDao.getSystemQueryNotes(formQueryId);
        } catch (Exception e) {
            Rlog.fatal("formQuery", "In getSystemQueryNotes in FormQueryAgentBean "+e);
            formQueryDao = null;
        }
        return formQueryDao;
    }
}
