/*
 * Classname			MsgcntrAgentBean
 * 
 * Version information	1.0	
 *
 * Date					02/27/2001
 * 
 * Copyright notice		Velos Inc.
 */

package com.velos.eres.service.msgcntrAgent.impl;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.MsgCntrDao;
import com.velos.eres.business.msgcntr.MsgsStateKeeper;
import com.velos.eres.business.msgcntr.impl.MsgcntrBean;
import com.velos.eres.service.msgcntrAgent.MsgcntrAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB acting as a facade for the entity BMP.<br>
 * <br>
 * 
 * @author sajal
 * @version 1.0 02/27/2001
 * @ejbHome MsgcntrAgentHome
 * @ejbremote MsgcntrAgentRObj
 */
@Stateless
public class MsgcntrAgentBean implements MsgcntrAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     */
    private static final long serialVersionUID = 3256723961743094582L;

    /**
     * Looks up on the Msgcntr id parameter passed in and constructs and returns
     * a Msgcntr state keeper. containing all the summary details of the msgcntr<br>
     * 
     * @param msgcntrId
     *            the Msgcntr id
     */

    public MsgcntrBean getMsgcntrDetails(int msgcntrId) {

        try {
            /*
             * MsgcntrPK pkMsgcntr; pkMsgcntr = new MsgcntrPK(msgcntrId);
             * MsgcntrHome msgcntrHome = EJBUtil.getMsgcntrHome(); retrieved =
             * msgcntrHome.findByPrimaryKey(pkMsgcntr); toreturn =
             * retrieved.getMsgcntrStateKeeper();
             */
            return (MsgcntrBean) em.find(MsgcntrBean.class, new Integer(
                    msgcntrId));
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Error in getMsgcntrDetails() in MsgcntrAgentBean" + e);
            return null;
        }

    }

    /**
     * Calls setMsgcntrDetails() of Entity Bean: MsgcntrBean
     * 
     * @param an
     *            msgcntr state keeper containing msgcntr attributes to be set.
     * @return 0 when updation is successful and -2 when exception occurs
     * @see MsgcntrBean
     */

    public int setMsgcntrDetails(MsgcntrBean msgcntr) {
        try {
            em.merge(msgcntr);
            return msgcntr.getMsgcntrId();
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Error in setMsgcntrDetails() in MsgcntrAgentBean " + e);
        }
        return 0;
    }

    /**
     * Calls updateMsgcntr() of Entity Bean: MsgcntrBean
     * 
     * @param an
     *            msgcntr state keeper containing msgcntr attributes to be set.
     * @return 0 when updation is successful and -2 when exception occurs
     * @see MsgcntrBean
     */

    public int updateMsgcntr(MsgcntrBean msgsk) {

        MsgcntrBean msgcntrBean = null;
        int output;

        try {
            msgcntrBean = (MsgcntrBean) em.find(MsgcntrBean.class, new Integer(
                    msgsk.getMsgcntrId()));

            if (msgcntrBean == null) {
                return -2;
            }
            output = msgcntrBean.updateMsgcntr(msgsk);

        } catch (Exception e) {
            Rlog.debug("msgcntr", "Error in updateMsgcntr in MsgcntrAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    public MsgCntrDao updateMsgs(MsgsStateKeeper msgsk) {

        int studyId;
        int xUser;
        int author;
        String permission;
        int msgId;

        try {
            Rlog.debug("msgcntr", "In updateMsgs in MsgCntrAgentBean 1");

            ArrayList msgIds = msgsk.getMsgIds();
            ArrayList msgStudyIds = msgsk.getMsgStudyIds();
            ArrayList msgUserXs = msgsk.getMsgUserXs();
            ArrayList msgGrants = msgsk.getMsgGrants();
            Rlog.debug("msgcntr", "In updateMsgs in MsgCntrAgentBean 2");

            MsgCntrDao msgcntrDao = new MsgCntrDao();
            // here call a procedure

            Rlog.debug("msgcntr",
                    "In updateMsgs in MsgCntrAgentBean 3 msgsk.getAuthorId() "
                            + msgsk.getAuthorId());
            author = StringUtil.stringToNum(msgsk.getAuthorId());
            Rlog.debug("msgcntr", "In updateMsgs in MsgCntrAgentBean 4 author "
                    + author);
            for (int i = 0; i < msgIds.size(); i++) {
                Rlog.debug("msgcntr", "In updateMsgs in MsgCntrAgentBean 5 i "
                        + i);
                String test = (String) msgStudyIds.get(i);
                Rlog.debug("msgcntr",
                        "In updateMsgs in MsgCntrAgentBean 6 test " + test);
                studyId = StringUtil.stringToNum(test);
                Rlog.debug("msgcntr",
                        "In updateMsgs in MsgCntrAgentBean 7 study Id "
                                + studyId);

                test = (String) (msgUserXs.get(i));
                Rlog.debug("msgcntr",
                        "In updateMsgs in MsgCntrAgentBean 8 test " + test);
                xUser = StringUtil.stringToNum(test);
                Rlog.debug("msgcntr",
                        "In updateMsgs in MsgCntrAgentBean 9 xUser " + xUser);

                permission = (String) (msgGrants.get(i));
                Rlog.debug("msgcntr",
                        "In updateMsgs in MsgCntrAgentBean 10 permission "
                                + permission);

                test = (String) (msgIds.get(i));
                Rlog.debug("msgcntr",
                        "In updateMsgs in MsgCntrAgentBean 11 test " + test);
                msgId = StringUtil.stringToNum(test);
                Rlog.debug("msgcntr",
                        "In updateMsgs in MsgCntrAgentBean 12 msgId " + msgId);

                msgcntrDao
                        .updateMsgs(studyId, xUser, author, permission, msgId);
                Rlog.debug("msgcntr", "In updateMsgs in MsgCntrAgentBean 13");

            }

            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getMsgCntrUserTo in MsgCntrAgentBean " + e);
        }
        return null;

    }

    /**
     * Returns Message Center Data Access Object containing all the messages for
     * the user
     * 
     * @param userTo
     *            user for whom avialable messages are required
     * @return Dao of Message center
     */
    public MsgCntrDao getMsgCntrUserTo(int userTo) {
        Rlog.debug("msgcntr", "session bean: outside try block");
        try {
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean line number 1");
            msgcntrDao.getMsgCntrUserTo(userTo);
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getMsgCntrUserTo in MsgCntrAgentBean " + e);
        }
        return null;

    }

    /**
     * Returns Message Center Data Access Object containing all the read
     * messages for the user
     * 
     * @param userTo
     *            user for whom already read messages are required
     * @return Dao of Message center
     */
    public MsgCntrDao getMsgCntrUserToRead(int userTo) {
        try {
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean line number 1");
            msgcntrDao.getMsgCntrUserToRead(userTo);
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getMsgCntrUserTo in MsgCntrAgentBean " + e);
        }
        return null;
    }

    /**
     * Returns Message Center Data Access Object containing all the unread
     * messages for the user
     * 
     * @param userTo
     *            user for which unread messages are required
     * @return Dao of Message center
     */

    public MsgCntrDao getMsgCntrUserToUnRead(int userTo) {
        try {
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean line number 1");
            msgcntrDao.getMsgCntrUserToUnRead(userTo);
            Rlog.debug("msgcntr",
                    "In getByGroupId in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getMsgCntrUserTo in MsgCntrAgentBean " + e);
        }
        return null;
    }

    public MsgCntrDao getMsgCntrVelosUser(int userTo) {
        try {
            Rlog.debug("msgcntr",
                    "In getMsgCntrVelosUser in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getMsgCntrVelosUser in MsgCntrAgentBean line number 1");
            msgcntrDao.getMsgCntrVelosUser(userTo);
            Rlog
                    .debug("msgcntr",
                            "In getMsgCntrVelosUser in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog
                    .fatal("msgcntr",
                            "Exception In getMsgCntrVelosUser in MsgCntrAgentBean "
                                    + e);
        }
        return null;
    }

    public MsgCntrDao getUserMsgs(int userTo) {
        try {
            Rlog.debug("msgcntr",
                    "In getUserMsgs in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getUserMsgs in MsgCntrAgentBean line number 1");
            msgcntrDao.getUserMsgs(userTo);
            Rlog.debug("msgcntr",
                    "In getUserMsgs in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getUserMsgs in MsgCntrAgentBean " + e);
        }
        return null;
    }

    public MsgCntrDao getUserMsgsWithSearch(int userTo, String lname,
            String fname, String userStatus) {
        try {
            Rlog
                    .debug("msgcntr",
                            "In getUserMsgsWithSearch in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog
                    .debug("msgcntr",
                            "In getUserMsgsWithSearch in MsgCntrAgentBean line number 1");
            msgcntrDao.getUserMsgsWithSearch(userTo, lname, fname, userStatus);
            Rlog
                    .debug("msgcntr",
                            "In getUserMsgsWithSearch in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getUserMsgsWithSearch in MsgCntrAgentBean "
                            + e);
        }
        return null;
    }

    public MsgCntrDao getMsgCntrVelosUser(int userTo, String status) {
        try {
            Rlog.debug("msgcntr",
                    "In getMsgCntrVelosUser in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getMsgCntrVelosUser in MsgCntrAgentBean line number 1");
            msgcntrDao.getMsgCntrVelosUser(userTo, status);
            Rlog
                    .debug("msgcntr",
                            "In getMsgCntrVelosUser in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog
                    .fatal("msgcntr",
                            "Exception In getMsgCntrVelosUser in MsgCntrAgentBean "
                                    + e);
        }
        return null;
    }

    public MsgCntrDao getMsgCntrVelosUserAcc(int userTo) {
        try {
            Rlog.debug("msgcntr",
                    "In getMsgCntrVelosUser in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getMsgCntrVelosUser in MsgCntrAgentBean line number 1");
            msgcntrDao.getMsgCntrVelosUserAcc(userTo);
            Rlog
                    .debug("msgcntr",
                            "In getMsgCntrVelosUser in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog
                    .fatal("msgcntr",
                            "Exception In getMsgCntrVelosUser in MsgCntrAgentBean "
                                    + e);
        }
        return null;
    }

    /**
     * Returns Message Center Data Access Object containing all the messages for
     * the study
     * 
     * @param study
     *            study for which messages are required
     * @return Dao of Message center
     */

    public MsgCntrDao getMsgCntrStudy(int study) {
        Rlog.debug("msgcntr", "session bean: outside try block");
        try {
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            msgcntrDao.getMsgCntrStudy(study);
            Rlog.debug("msgcntr",
                    "In getByStudyId in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getMsgCntrStudy To in MsgCntrAgentBean " + e);
        }
        return null;

    }

    public MsgCntrDao getMsgCount(int userTo) {
        try {
            Rlog.debug("msgcntr",
                    "In getMsgCount in MsgCntrAgentBean line number 0");
            MsgCntrDao msgcntrDao = new MsgCntrDao();
            Rlog.debug("msgcntr",
                    "In getMsgCount in MsgCntrAgentBean line number 1");
            msgcntrDao.getMsgCount(userTo);
            Rlog.debug("msgcntr",
                    "In getMsgCount in MsgCntrAgentBean  line number 2");
            return msgcntrDao;
        } catch (Exception e) {
            Rlog.fatal("msgcntr",
                    "Exception In getMsgCount in MsgCntrAgentBean " + e);
        }
        return null;
    }

    /**
     * Returns the State keeper which holds all the messages/requests that are
     * not yet approved
     * 
     * @return all the pending requests
     */
    // public MsgsPendingValidationStateKeeper findByMsgsPendingValidation() {
    // MsgsPendingValidationStateKeeper msgsk = new
    // MsgsPendingValidationStateKeeper();
    // MsgcntrRObj msgcntrRObj;
    // UserRObj userRObj;
    // AccountRObj accountRObj;
    // try {
    // MsgcntrHome msgcntrHome = EJBUtil.getMsgcntrHome();
    // Enumeration enum_velos = msgcntrHome.findByMsgsPendingValidation();
    // while (enum_velos.hasMoreElements()) {
    // int usrId = 0;
    // String usrLastName = null;
    // String usrFirstName = null;
    // String usrMiddleName = null;
    // String accName = null;
    // String msgStat = null;
    // String usrStat = null;
    // UserPK usrPK = null;
    // AccountPK accountPK = null;
    // msgcntrRObj = (MsgcntrRObj) enum_velos.nextElement();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line a **** msgcntrRObj " +
    // msgcntrRObj);
    // msgStat = msgcntrRObj.getMsgcntrStatus();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line b **** msgStat " + msgStat);
    // usrId = EJBUtil.stringToNum(msgcntrRObj.getMsgcntrFromUserId());
    // Rlog.debug("msgcntr","MsgcntrAgentBean line c **** usrId " + usrId);
    // if (usrId != 0)
    // {
    // usrPK = new UserPK(usrId);
    // UserHome userHome = EJBUtil.getUserHome();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line d **** ");
    // userRObj = userHome.findByPrimaryKey(usrPK);
    // Rlog.debug("msgcntr","MsgcntrAgentBean line e **** userRObj "+ userRObj);
    // usrLastName = userRObj.getUserLastName();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line f **** usrLastName " +
    // usrLastName);
    // usrFirstName = userRObj.getUserFirstName();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line g **** usrFirstName " +
    // usrFirstName);
    // usrMiddleName = userRObj.getUserMidName();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line h **** usrMiddleName " +
    // usrMiddleName);
    // usrStat = userRObj.getUserStatus();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line i **** usrStat " + usrStat);
    // accountPK = new
    // AccountPK(EJBUtil.stringToNum(userRObj.getUserAccountId()));
    // Rlog.debug("msgcntr","MsgcntrAgentBean line j **** accountPK " +
    // accountPK);
    // if (accountPK != null)
    // {
    // AccountHome accountHome = EJBUtil.getAccountHome();
    // Rlog.debug("msgcntr","MsgcntrAgentBean line a **** ");
    // accountRObj = accountHome.findByPrimaryKey(accountPK);
    // Rlog.debug("msgcntr","MsgcntrAgentBean line a **** ");
    // accName = accountRObj.getAccName();
    // }
    // }
    // msgsk.setMsgsPendingValidationDetails(msgStat, usrId, usrLastName,
    // usrFirstName,
    // usrMiddleName, usrStat, accName);
    // }
    // } catch(Exception e) {
    // Rlog.fatal("msgcntr","Error in findByMsgsPendingValidation in
    // MsgcntrAgentBean" + e);
    // }
    // return msgsk;
    // }
}// end of class
