/*
 * Classname : commonAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 09/28/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol		
 */

package com.velos.eres.service.commonAgent;

/* Import Statements */
import java.util.ArrayList;

import javax.ejb.Remote;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.SasExportDao;
import com.velos.eres.business.common.SettingsDao;

/* End of Import Statements */

/**
 * Remote interface for comomnAgent session EJB
 * 
 * @author Vishal Abrol
 * @version : 1.0 09/28/2003
 */
@Remote
public interface CommonAgentRObj {
    public SasExportDao getSasHandle(String type, int id);
    
    public CommonDAO getCommonHandle() ;

    public SettingsDao getSettingsInstance();

    public SettingsDao retrieveSettings(int modnum, int modname, String keyword);

    public SettingsDao retrieveSettings(int modnum, int modname,
            ArrayList keyword);

    public SettingsDao retrieveSettings(int modnum, int modname);

    public int purgeSettings(String modnum, String modname, String keyword);

    public int purgeSettings(String modnum, String modname,
            ArrayList keywordList);
	//Added by Gopu for July-August Enhancement (#U2)
    public int updateSettingsWithoutPK(SettingsDao settingsDao);

    public int insertSettings(SettingsDao settingsDao);

    /**
     * Returns the Account Primary key
     * 
     * @param siteCode
     *            The unique site code for an account
     * @return the Account Primary key
     */

    public int getAccountPK(String siteCode);

    /**
     * Gets the site PK. If you are on ASP type deployment, it is advisable to
     * use getSitePK(String siteId, int pkAccount )
     * 
     * @param siteId
     *            Unique Site Id
     * @return Site Primary Key
     */
    public int getSitePK(String siteId);

    /**
     * Gets the site PK
     * 
     * @param siteId
     *            Unique Site Id
     * @param pkAccount
     *            Primary Key of site account
     * @return Site Primary Key
     */
    public int getSitePK(String siteId, int pkAccount);

    /**
     * Gets the user PK. Please note: For Non System users, it is advisable to
     * store their email addresses in eResearch. User email is a good parameter
     * to find a user.
     * 
     * @param firstName
     *            User's first Name
     * @param lastName
     *            User's last Name
     * @param userEmail
     *            User's email address
     * @return user Primary Key
     */
    public int getUserPK(String firstName, String lastName, String userEmail);

    /**
     * Gets the user PK on the basis of first name and last name. It is
     * advisable to use getUserPK(String firstName, String lastName, String
     * userEmail ) as there is a chance that system may have more than one user
     * with same first name and last name
     * 
     * @param firstName
     *            User's first Name
     * @param lastName
     *            User's last Name
     * @return user Primary Key
     */
    public int getUserPK(String firstName, String lastName);

    /**
     * Returns the internal Code List Primary key corresponding to specific code
     * type,code subtype,and code description combination
     * 
     * @param type
     *            maps to Code_type in er_codelst
     * @param subType
     *            maps to Code_subtyp in er_codelst
     * @param description
     *            maps to code_Desc in er_codelst
     * @return Primary Key of the code list value
     */
    public int getCodeListPK(String type, String subType, String description);

    /**
     * Returns the internal Code List Primary key corresponding to specific code
     * type and code subtype combination
     * 
     * @param type
     *            maps to Code_type in er_codelst
     * @param subType
     *            maps to Code_subtyp in er_codelst
     * @return Primary Key of the code list value
     */
    public int getCodeListPK(String type, String subType);

    /**
     * Gets Study PK
     * 
     * @param studyNum
     *            The Study Number
     * @param pkAccount
     *            Primary Key of study account
     * @return Study Primary Key
     */
    public int getStudyPK(String studyNum, int pkAccount);

    /**
     * Gets study Protocol PK
     * 
     * @param studyPK
     *            The Study Primary Key
     * @param CalendarName
     *            The Calendar Name
     * @return Protocol Primary Key
     */
    public int getProtocolPK(int studyPK, String CalendarName);

    /**
     * Returns the internal Code List Primary key corresponding to specific code
     * type and code description combination
     * 
     * @param type
     *            Code Type, maps to CodeLst_type in er_codelst
     * @param desc
     *            Code Description, maps to CodeLst_desc in er_codelst
     * @return Primary Key of the code list value
     */
    public int getCodeListPKFromDesc(String type, String desc);
}
