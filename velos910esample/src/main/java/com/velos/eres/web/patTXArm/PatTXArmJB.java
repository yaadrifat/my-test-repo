/**
 * Classname : PatTXArmJB
 * 
 * Version information: 1.0
 *
 * Date: 05/03/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.patTXArm;

/* IMPORT STATEMENTS */

import com.velos.eres.business.common.PatTXArmDao;
import java.util.Hashtable;
import com.velos.eres.business.patTXArm.impl.PatTXArmBean;
import com.velos.eres.service.patTXArmAgent.PatTXArmAgentRObj;
import com.velos.eres.service.patTXArmAgent.impl.PatTXArmAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for PatTXArm
 * 
 * @author Anu Khanna
 * @version 1.0 05/03/2005
 */

public class PatTXArmJB {

    /**
     * The primary key:
     */

    private int patTXArmId;

    /**
     * patPort id
     */
    private String patProtId;

    /**
     * study treatment arm id
     */
    private String studyTXArmId;

    /**
     * treatment drug info
     */

    private String txDrugInfo;

    /**
     * treatment start date
     */
    private String startDate;

    /**
     * treatment end date
     */
    private String endDate;

    /**
     * treatment notes
     */
    private String notes;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    /**
     * Get the PatTXArm ID
     * 
     * @return int PatTXArm id
     */
    public int getPatTXArmId() {
        return this.patTXArmId;
    }

    /**
     * sets PatTXArm ID
     * 
     * @param patTXArmId
     */
    public void setPatTXArmId(int patTXArmId) {
        this.patTXArmId = patTXArmId;
    }

    /**
     * Get patProt id
     * 
     * @return String patProtId
     */

    public String getPatProtId() {
        return this.patProtId;
    }

    /**
     * sets patProtId
     * 
     * @param patProtId
     */
    public void setPatProtId(String patProtId) {
        this.patProtId = patProtId;
    }

    /**
     * Get the StudyTXArm ID
     * 
     * @return int StudyTXArm id
     */
    public String getStudyTXArmId() {
        return this.studyTXArmId;
    }

    /**
     * sets StudyTXArm ID
     * 
     * @param studyTXArmId
     */
    public void setStudyTXArmId(String studyTXArmId) {
        this.studyTXArmId = studyTXArmId;
    }

    /**
     * Get treatment drug info
     * 
     * @return String txDrugInfo
     */

    public String getTXDrugInfo() {
        return this.txDrugInfo;
    }

    /**
     * sets treatment drug info
     * 
     * @param txDrugInfo
     */
    public void setTXDrugInfo(String txDrugInfo) {
        this.txDrugInfo = txDrugInfo;
    }

    /**
     * Get treatment start date
     * 
     * @return String startDate
     */

    public String getStartDate() {
        return this.startDate;
    }

    /**
     * sets treatment start date
     * 
     * @param startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * Get treatment end date
     * 
     * @return String startDate
     */

    public String getEndDate() {
        return this.endDate;
    }

    /**
     * sets treatment end date
     * 
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * Get treatment notes
     * 
     * @return String notes
     */

    public String getNotes() {
        return this.notes;
    }

    /**
     * sets treatment notes
     * 
     * @param notes
     */

    public void setNotes(String notes) {
        this.notes = notes;

    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param patTXArmId:
     *            Unique Id constructor
     * 
     */

    public PatTXArmJB(int patTXArmId) {
        setPatTXArmId(patTXArmId);
    }

    /**
     * Default Constructor
     */

    public PatTXArmJB() {
        Rlog.debug("patTXArm", "PatTXArmJB.PatTXArmJB() ");
    }

    public PatTXArmJB(int patTXArmId, String patProtId, String studyTXArmId,
            String txDrugInfo, String startDate, String endDate,
            String creator, String modifiedBy) {
        setPatTXArmId(patTXArmId);
        setPatProtId(patProtId);
        setStudyTXArmId(studyTXArmId);
        setTXDrugInfo(txDrugInfo);
        setStartDate(startDate);
        setEndDate(endDate);
        setNotes(notes);
        setCreator(creator);
        setModifiedBy(modifiedBy);

    }

    /**
     * Calls getPatTXArmDetails of PatTXArmId Session Bean: PatTXArmAgentBean
     * 
     * @return PatTXArmStatKeeper
     * @see PatTXArmAgentBean
     */
    public PatTXArmBean getPatTXArmDetails() {
        PatTXArmBean patTXArmsk = null;
        try {

            PatTXArmAgentRObj patTXArmAgentRObj = EJBUtil
                    .getPatTXArmAgentHome();
            patTXArmsk = patTXArmAgentRObj.getPatTXArmDetails(this.patTXArmId);
            Rlog.debug("PatTXArm",
                    "PatTXArmJB.getPatTXArmDetails() PatTXArmStateKeeper "
                            + patTXArmsk);
        } catch (Exception e) {
            Rlog.fatal("patTXArm", "Error in PatTXArm getPatTXArmDetails" + e);
        }

        if (patTXArmsk != null) {
            setPatTXArmId(patTXArmsk.getPatTXArmId());
            setPatProtId(patTXArmsk.getPatProtId());
            setStudyTXArmId(patTXArmsk.getStudyTXArmId());
            setTXDrugInfo(patTXArmsk.getTXDrugInfo());
            setStartDate(patTXArmsk.getStartDate());
            setEndDate(patTXArmsk.getEndDate());
            setNotes(patTXArmsk.getNotes());
            setCreator(patTXArmsk.getCreator());
            setModifiedBy(patTXArmsk.getModifiedBy());

        }

        return patTXArmsk;

    }

    /**
     * Calls setPatTXArmDetails() of PatTXArm Session Bean: PatTXArmAgentBean
     * 
     */

    public int setPatTXArmDetails() {
        int toReturn;
        try {

            PatTXArmAgentRObj patTXArmAgentRObj = EJBUtil
                    .getPatTXArmAgentHome();

            PatTXArmBean tempStateKeeper = new PatTXArmBean();

            tempStateKeeper = this.createPatTXArmStateKeeper();

            toReturn = patTXArmAgentRObj.setPatTXArmDetails(tempStateKeeper);
            this.setPatTXArmId(toReturn);

            Rlog.debug("patTXArm", "PatTXArmJB.setPatTXArmDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("patTXArm",
                    "Error in setPatTXArmDetails() in PatTXArmJB " + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the PatTXArm Record with the current
     *         values of the bean
     */
    public PatTXArmBean createPatTXArmStateKeeper() {

        return new PatTXArmBean(patTXArmId, patProtId, studyTXArmId,
                txDrugInfo, startDate, endDate, notes, creator, modifiedBy);

    }

    /**
     * Calls updatePatTXArm() of PatTXArm Session Bean: PatTXArmAgentBean
     * 
     * @return The status as an integer
     */
    public int updatePatTXArm() {
        int output;
        try {

            PatTXArmAgentRObj patTXArmAgentRObj = EJBUtil
                    .getPatTXArmAgentHome();
            output = patTXArmAgentRObj.updatePatTXArm(this
                    .createPatTXArmStateKeeper());
        } catch (Exception e) {
            Rlog.debug("patTXArm",
                    "EXCEPTION IN SETTING PatTXArm DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    
    
    
  // Overloaded for INF-18183 ::: Akshi
    public int removePatTXArm(Hashtable<String, String> args) {

        int output;

        try {

            PatTXArmAgentRObj patTXArmAgentRObj = EJBUtil
                    .getPatTXArmAgentHome();
            output = patTXArmAgentRObj.removePatTXArm(this.patTXArmId,args);
            return output;

        } catch (Exception e) {
            Rlog.fatal("PatTXArm", "in PatTXArmJB removePatTXArm() method" + e);
            return -1;
        }

    }

    public int removePatTXArm() {

        int output;

        try {

            PatTXArmAgentRObj patTXArmAgentRObj = EJBUtil
                    .getPatTXArmAgentHome();
            output = patTXArmAgentRObj.removePatTXArm(this.patTXArmId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("PatTXArm", "in PatTXArmJB removePatTXArm() method" + e);
            return -1;
        }

    }

    public PatTXArmDao getPatTrtmtArms(int patProtId) {
        PatTXArmDao patTXArmDao = new PatTXArmDao();
        try {

            PatTXArmAgentRObj patTXArmAgentRObj = EJBUtil
                    .getPatTXArmAgentHome();
            patTXArmDao = patTXArmAgentRObj.getPatTrtmtArms(patProtId);
            return patTXArmDao;
        } catch (Exception e) {
            Rlog.fatal("patTXArm", "getPatTrtmtArms in PatTXArmJB " + e);
        }
        return patTXArmDao;
    }
}
