/*

 * Classname : UserJB

 * 

 * Version information: 1.0

 *

 * Date: 03/12/2001

 * 

 * Copyright notice: Velos, Inc

 *

 * Author: Sajal

 */

package com.velos.eres.web.usrGrp;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.eres.business.usrGrp.impl.UsrGrpBean;
import com.velos.eres.service.usrGrpAgent.UsrGrpAgentRObj;
import com.velos.eres.service.usrGrpAgent.impl.UsrGrpAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * 
 * Client side bean for UsrGrp (User Groups)
 * 
 * @author Sajal
 * 
 * @version 1.0 03/12/2001
 * 
 */

public class UsrGrpJB {

    /**
     * 
     * the usergroup Id
     * 
     */

    private int usrGrpId;

    /**
     * 
     * the usergroup user id
     * 
     */

    private String usrGrpUserId;

    /**
     * 
     * the usergroup group id
     * 
     */

    private String usrGrpGroupId;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * 
     * 
     * 
     * @return unique Id for one User-Group combination
     * 
     */

    public int getUsrGrpId() {

        return this.usrGrpId;

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @param usrGrpId
     *            unique Id for one User-Group combination
     * 
     */

    public void setUsrGrpId(int usrGrpId) {

        this.usrGrpId = usrGrpId;
    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @return User Id
     * 
     */

    public String getUsrGrpUserId() {

        return this.usrGrpUserId;

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @param usrGrpUserId
     *            User Id
     * 
     */

    public void setUsrGrpUserId(String usrGrpUserId) {

        this.usrGrpUserId = usrGrpUserId;

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @return Group Id
     * 
     */

    public String getUsrGrpGroupId() {

        return this.usrGrpGroupId;

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @param usrGrpGroupId
     *            Group Id
     * 
     */

    public void setUsrGrpGroupId(String usrGrpGroupId) {

        this.usrGrpGroupId = usrGrpGroupId;
    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    // end of getter setter methods here

    /**
     * 
     * Constructor
     * 
     * 
     * 
     * @param usrGrpId
     *            unique Id for one User-Group combination
     * 
     */

    public UsrGrpJB(int usrGrpId)

    {

        setUsrGrpId(usrGrpId);

    }

    /**
     * 
     * Default Constructor
     * 
     * 
     * 
     */

    public UsrGrpJB()

    {

        Rlog.debug("usrgrp", "UsrGrpJB.UsrGrpJB() ");

    };

    /**
     * 
     * Full arguments constructor.
     * 
     * 
     * 
     * @param usrGrpId
     * 
     * @param usrGrpUserId
     * 
     * @param usrGrpGroupId
     * 
     */

    public UsrGrpJB(int usrGrpId, String usrGrpUserId, String usrGrpGroupId,
            String creator,

            String modifiedBy, String ipAdd) {

        setUsrGrpId(usrGrpId);

        setUsrGrpUserId(usrGrpUserId);

        setUsrGrpGroupId(usrGrpGroupId);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);

        Rlog.debug("usrgrp", "UsrGrpJB.UsrGrpJB(all parameters)");

    }

    /**
     * 
     * Calls getUsrGrpDetails of User Groups Session Bean: UsrGrpAgentBean
     * 
     * 
     * 
     * @return
     * 
     * @see UsrGrpAgentBean
     * 
     */

    public UsrGrpBean getUsrGrpDetails()

    {

        UsrGrpBean ugsk = null;

        try {

            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();

            ugsk = usrGrpAgentRObj.getUsrGrpDetails(this.usrGrpId);

            Rlog.debug("usrGrp",
                    "UsrGrpJB.getUsrGrpDetails() UsrGrpStateKeeper " + ugsk);

        } catch (Exception e) {

            Rlog
                    .fatal("usrGrp", "Error in getUsrGrpDetails() in UsrGrpJB "
                            + e);

        }

        if (ugsk != null) {

            this.usrGrpId = ugsk.getUsrGrpId();

            this.usrGrpUserId = ugsk.getUsrGrpUserId();

            this.usrGrpGroupId = ugsk.getUsrGrpGroupId();

            this.creator = ugsk.getCreator();

            this.modifiedBy = ugsk.getModifiedBy();

            this.ipAdd = ugsk.getIpAdd();

        }

        return ugsk;

    }

    /**
     * 
     * Calls setUsrGrpDetails() of User Groups Session Bean: UsrGrpAgentBean
     * 
     * 
     * 
     */

    public void setUsrGrpDetails() {

        try {

            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();

            this.setUsrGrpId(usrGrpAgentRObj.setUsrGrpDetails(this
                    .createUsrGrpStateKeeper()));

            Rlog.debug("usrGrp", "UsrGrpJB.setUsrGrpDetails()");

        } catch (Exception e) {

            Rlog
                    .fatal("usrGrp", "Error in setUsrGrpDetails() in UsrGrpJB "
                            + e);

        }

    }

    /**
     * 
     * Calls updateUsrGrp() of User Groups Session Bean: UsrGrpAgentBean
     * 
     * 
     * 
     * @return
     * 
     */

    public int updateUsrGrp()

    {

        int output;

        try {

            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();

            output = usrGrpAgentRObj.updateUsrGrp(this
                    .createUsrGrpStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("usrGrp",
                    "EXCEPTION IN SETTING USRGRP DETAILS TO  SESSION BEAN" + e);

            return -2;

        }

        return output;

    }

    public int removeUsrFromGrp() {

        int output;

        try

        {

            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();

            output = usrGrpAgentRObj.removeUsrFromGrp(this.usrGrpId);

            return output;

        }

        catch (Exception e)

        {

            Rlog.debug("usrGrp", "in JB - remove user from group");

            return -1;

        }

    }
    
    // Overloaded for INF-18183 -- AGodara
    public int removeUsrFromGrp(Hashtable<String, String> userInfo) {

        int output;
        try{
            UsrGrpAgentRObj usrGrpAgentRObj = EJBUtil.getUsrGrpAgentHome();
            output = usrGrpAgentRObj.removeUsrFromGrp(this.usrGrpId,userInfo);
            return output;
        }catch (Exception e){
            Rlog.debug("usrGrp", "in JB method - removeUsrFromGrp(Hashtable<String, String> userInfo)");
            return -1;
        }
    }
    

    /**
     * 
     * 
     * 
     * 
     * 
     * @return a statekeeper object for the User Group Record with the current
     *         values of the bean
     * 
     */

    public UsrGrpBean createUsrGrpStateKeeper() {

        Rlog.debug("usrGrp", "UsrGrpJB.createUsrGrpStateKeeper ");

        return new UsrGrpBean(usrGrpId, usrGrpUserId, usrGrpGroupId, creator,
                modifiedBy, ipAdd);

    }

    /**
     * 
     * NOT IN USE
     * 
     * 
     * 
     * @return
     * 
     */

    public String toXML()

    {

        Rlog.debug("usrGrp", "UsrGrpJB.toXML()");

        return new String(
                "<?xml version=\"1.0\"?>"
                        +

                        "<?cocoon-process type=\"xslt\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +

                        // to be done "<form action=\"usrGrpsummary.jsp\">" +

                        "<head>" +

                        // to be done "<title>UsrGrp Summary </title>" +

                        "</head>" +

                        "<input type=\"hidden\" name=\"usrGrpId\" value=\""
                        + this.getUsrGrpId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\"usrGrpUserId\" value=\""
                        + this.getUsrGrpUserId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\"usrGrpGroupId\" value=\""
                        + this.getUsrGrpGroupId() + "\" size=\"10\"/>" +

                        "</form>"

        );

    }// end of method

}// end of class

