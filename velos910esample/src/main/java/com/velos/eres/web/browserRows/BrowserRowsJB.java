/*
 * Classname : 				BrowserRowsJB
 * 
 * Version information		1.0
 *
 * Date 					12/23/2003	
 * 
 * Copyright notice: 		Velos Inc
 *
 * Author 					Sonia Sahni
 */

package com.velos.eres.web.browserRows;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import com.velos.eres.service.browserRowsAgent.BrowserRowsAgentRObj;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for BrowserRows.
 * 
 * @author Sonia Sahni
 * 
 * @version 1.0, 12/23/2003
 */

public class BrowserRowsJB {

    public BrowserRowsJB() {
    }

    /**
     * Creates an account using the values in this object
     */
    public BrowserRows getPageRows(long page, long rowsRequired, String bSql,
            long totalPages, String countSql, String orderBy, String order) {
        BrowserRows br = new BrowserRows();
        try {

            BrowserRowsAgentRObj browserRowsAgentRObj = EJBUtil
                    .getBrowserRowsAgentHome();
            br = browserRowsAgentRObj.getPageRows(page, rowsRequired, bSql,
                    totalPages, countSql, orderBy, order);
            Rlog.debug("browserRows", "BrowserRowsJB.getPageRows()");
        } catch (Exception e) {
            Rlog.fatal("browserRows",
                    "Exception in BrowserRowsJB.getPageRows() " + e);
        }
        return br;
    }

    public BrowserRows getSchPageRows(long page, long rowsRequired,
            String bSql, long totalPages, String countSql, String orderBy,
            String order) {
        BrowserRows br = new BrowserRows();
        try {

            BrowserRowsAgentRObj browserRowsAgentRObj = EJBUtil
                    .getBrowserRowsAgentHome();
            br = browserRowsAgentRObj.getSchPageRows(page, rowsRequired, bSql,
                    totalPages, countSql, orderBy, order);
            Rlog.debug("browserRows", "BrowserRowsJB.getPageRows()");
        } catch (Exception e) {
            Rlog.fatal("browserRows",
                    "Exception in BrowserRowsJB.getPageRows() " + e);
        }
        return br;
    }

    public Hashtable getSchedulePageRows(long page, long rowsRequired,
            String bSql, long totalPages, String countSql, String orderBy,
            String order, String schWhere) {
        Hashtable htReturn = new Hashtable();
        try {

            BrowserRowsAgentRObj browserRowsAgentRObj = EJBUtil
                    .getBrowserRowsAgentHome();

            htReturn = browserRowsAgentRObj.getSchedulePageRows(page,
                    rowsRequired, bSql, totalPages, countSql, orderBy, order,
                    schWhere);

            Rlog.debug("browserRows", "BrowserRowsJB.getSchedulePageRows()");

        } catch (Exception e) {
            Rlog.fatal("browserRows",
                    "Exception in BrowserRowsJB.getSchedulePageRows() " + e);
        }
        return htReturn;
    }

}// end of class

