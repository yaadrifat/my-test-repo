/*

 * Classname : MsgcntrJB

 * 

 * Version information 1.0

 *

 * Date 02/27/2001

 * 

 * Copyright notice: Velos Inc

 */

package com.velos.eres.web.msgcntr;

import java.util.ArrayList;

import com.velos.eres.business.common.MsgCntrDao;
import com.velos.eres.business.msgcntr.MsgsStateKeeper;
import com.velos.eres.business.msgcntr.impl.MsgcntrBean;
import com.velos.eres.service.msgcntrAgent.MsgcntrAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * 
 * Client side bean for Msgcntr
 * 
 * @author Sajal
 * 
 * @version 1.0 02/27/2001
 * 
 */

public class MsgcntrJB {

    /**
     * 
     * the msgcntr Id
     * 
     */

    private int msgcntrId;

    /**
     * 
     * the msgcntr study Id
     * 
     */

    private String msgcntrStudyId;

    /**
     * 
     * the msgcntr from userId
     * 
     */

    private String msgcntrFromUserId;

    /**
     * 
     * the msgcntr to userId
     * 
     */

    private String msgcntrToUserId;

    /**
     * 
     * the msgcntr text
     * 
     */

    private String msgcntrText;

    /**
     * 
     * the msgcntr required type
     * 
     */

    private String msgcntrReqType;

    /**
     * 
     * the msgcntr status
     * 
     */

    private String msgcntrStatus;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    // /fields for the multiple ids

    /**
     * 
     * the msgcntr multiple ids
     * 
     */

    private ArrayList msgIds;

    /**
     * 
     * the msgcntr multiple Study ids
     * 
     */

    private ArrayList msgStudyIds;

    /**
     * 
     * the msgcntr External user ids
     * 
     */

    private ArrayList msgUserXs;

    /**
     * 
     * the msgcntr multiple UserFrom ids
     * 
     */

    private ArrayList msgGrants;

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * 
     * Returns Message Center Id
     * 
     * 
     * 
     * @return message center id
     * 
     */

    public int getMsgcntrId() {

        return this.msgcntrId;

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * @param msgcntrId
     * 
     */

    public void setMsgcntrId(int msgcntrId)

    {

        this.msgcntrId = msgcntrId;

    }

    /**
     * 
     * Returns Study id
     * 
     * 
     * 
     * @return study id
     * 
     */

    public String getMsgcntrStudyId() {

        return this.msgcntrStudyId;

    }

    /**
     * 
     * Sets study id
     * 
     * 
     * 
     * @param msgcntrStudyId
     *            study id
     * 
     */

    public void setMsgcntrStudyId(String msgcntrStudyId) {

        this.msgcntrStudyId = msgcntrStudyId;

    }

    /**
     * 
     * Returns Id of the user from whom message is received
     * 
     * 
     * 
     * @return user id of the user who has sent the message
     * 
     */

    public String getMsgcntrFromUserId() {

        return this.msgcntrFromUserId;

    }

    /**
     * 
     * Set Id of the user who has sent the message
     * 
     * 
     * 
     * @param msgcntrFromUserId
     *            User id of the user who has sent the message
     * 
     */

    public void setMsgcntrFromUserId(String msgcntrFromUserId) {

        this.msgcntrFromUserId = msgcntrFromUserId;

    }

    /**
     * 
     * Returns User Id of the user to whom message has been sent
     * 
     * 
     * 
     * @return Id of the user to whom message has been sent
     * 
     */

    public String getMsgcntrToUserId() {

        return this.msgcntrToUserId;

    }

    /**
     * 
     * Sets the user id of the user to whom message has been sent
     * 
     * 
     * 
     * @param msgcntrToUserId
     *            id of the user to whom message has been sent
     * 
     */

    public void setMsgcntrToUserId(String msgcntrToUserId) {

        this.msgcntrToUserId = msgcntrToUserId;

    }

    /**
     * 
     * Returns the message Text
     * 
     * 
     * 
     * @return text of the message
     * 
     */

    public String getMsgcntrText() {

        return this.msgcntrText;

    }

    /**
     * 
     * Sets the text of the message
     * 
     * 
     * 
     * @param msgcntrText
     *            Text of the message
     * 
     */

    public void setMsgcntrText(String msgcntrText) {

        this.msgcntrText = msgcntrText;

    }

    /**
     * 
     * Returns the Type of Request
     * 
     * 
     * 
     * @return 'S' for messages related to study, 'E' for messages
     * 
     * related to external users, 'A' for messages related to users signup
     * 
     */

    /**
     * 
     * Sets the type of Request
     * 
     * 
     * 
     * @param msgcntrReqType
     *            Request Type. 'S' for messages related to study, 'E' for
     *            messages
     * 
     * related to external users, 'A' for messages related to users signup
     * 
     */

    public void setMsgcntrReqType(String msgcntrReqType) {

        this.msgcntrReqType = msgcntrReqType;

    }

    /**
     * 
     * Returns Message status
     * 
     * 
     * 
     * @return Status of messages. 'R' for Unread messages, 'U' for UnRead
     *         messages
     * 
     */

    public String getMsgcntrStatus() {

        return this.msgcntrStatus;

    }

    /**
     * 
     * Sets the message status
     * 
     * 
     * 
     * @param msgcntrStatus
     *            'R' for Unread messages, 'U' for UnRead messages
     * 
     */

    public void setMsgcntrStatus(String msgcntrStatus) {

        this.msgcntrStatus = msgcntrStatus;

    }

    public String getMsgcntrReqType() {

        return this.msgcntrReqType;

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    // new functions added to update multiple messages

    public ArrayList getMsgIds() {

        return this.msgIds;

    }

    public void setMsgIds(ArrayList msgIds) {

        this.msgIds = msgIds;

    }

    public ArrayList getMsgStudyIds() {

        return this.msgStudyIds;

    }

    public void setMsgStudyIds(ArrayList msgStudyIds) {

        this.msgStudyIds = msgStudyIds;

    }

    public ArrayList getMsgUserXs() {

        return this.msgUserXs;

    }

    public void setMsgUserXs(ArrayList msgUserXs) {

        this.msgUserXs = msgUserXs;

    }

    public ArrayList getMsgGrants() {

        return this.msgGrants;

    }

    public void setMsgGrants(ArrayList msgGrants) {

        this.msgGrants = msgGrants;

    }

    // END OF GETTER SETTER METHODS

    /**
     * 
     * Sets Message center Id in the constructor
     * 
     * 
     * 
     * @param msgcntrId
     * 
     */

    public MsgcntrJB(int msgcntrId)

    {

        setMsgcntrId(msgcntrId);

    }

    /**
     * 
     * default Constructor
     * 
     * 
     * 
     */

    public MsgcntrJB()

    {

    };

    /**
     * 
     * Full arguments constructor.
     * 
     * 
     * 
     * @param msgcntrId
     * 
     * @param msgcntrStudyId
     * 
     * @param msgcntrFromUserId
     * 
     * @param msgcntrToUserId
     * 
     * @param msgcntrText
     * 
     * @param msgcntrReqType
     * 
     * @param msgcntrStatus
     * 
     */

    public MsgcntrJB(int msgcntrId, String msgcntrStudyId,
            String msgcntrFromUserId,

            String msgcntrToUserId, String msgcntrText, String msgcntrReqType,

            String msgcntrStatus, String creator, String modifiedBy,
            String ipAdd) {

        setMsgcntrId(msgcntrId);

        setMsgcntrStudyId(msgcntrStudyId);

        setMsgcntrFromUserId(msgcntrFromUserId);

        setMsgcntrToUserId(msgcntrToUserId);

        setMsgcntrText(msgcntrText);

        setMsgcntrReqType(msgcntrReqType);

        setMsgcntrStatus(msgcntrStatus);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);

    }

    public MsgcntrJB(ArrayList msgIds, ArrayList msgStudyIds,

    ArrayList msgUserXs, ArrayList msgGrants) {

        setMsgIds(msgIds);

        setMsgStudyIds(msgStudyIds);

        setMsgUserXs(msgUserXs);

        setMsgGrants(msgGrants);

    }

    /**
     * 
     * Returns all the details of Message Center in the Message center State
     * Keeper
     * 
     * 
     * 
     * @return Message Center State Keeper
     * 
     */

    public MsgcntrBean getMsgcntrDetails()

    {

        MsgcntrBean msgsk = null;

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            msgsk = msgcntrAgentRObj.getMsgcntrDetails(this.msgcntrId);

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in getMsgcntrDetails() in MsgcntrJB "
                    + e);

        }

        if (msgsk != null)

        {

            this.msgcntrId = msgsk.getMsgcntrId();

            this.msgcntrStudyId = msgsk.getMsgcntrStudyId();

            this.msgcntrFromUserId = msgsk.getMsgcntrFromUserId();

            this.msgcntrToUserId = msgsk.getMsgcntrToUserId();

            this.msgcntrText = msgsk.getMsgcntrText();

            this.msgcntrReqType = msgsk.getMsgcntrReqType();

            this.msgcntrStatus = msgsk.getMsgcntrStatus();

            this.creator = msgsk.getCreator();

            this.modifiedBy = msgsk.getModifiedBy();

            this.ipAdd = msgsk.getIpAdd();

        }

        return msgsk;

    }

    /**
     * 
     * Sets Message Center Details. Calls setMsgcntrDetails() of the
     * 
     * session Bean: MsgCntrAgentBean
     * 
     * 
     * 
     * @see MsgCntrAgentBean
     * 
     */

    public void setMsgcntrDetails()

    {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            this.setMsgcntrId(msgcntrAgentRObj.setMsgcntrDetails(this
                    .createMsgcntrStateKeeper()));

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in setMsgcntrDetails() in MsgcntrJB "
                    + e);

        }

    }

    /**
     * 
     * Creates a State Keeper of Message Center
     * 
     * 
     * 
     * @return State Keeper of Message Center
     * 
     */

    public MsgcntrBean createMsgcntrStateKeeper()

    {

        return new MsgcntrBean(msgcntrId, msgcntrStudyId, msgcntrFromUserId,
                msgcntrToUserId, msgcntrText, msgcntrReqType, msgcntrStatus,
                creator, modifiedBy, ipAdd);

    }

    public MsgsStateKeeper createMsgsStateKeeper()

    {

        Rlog.debug("msgcntr", "MsgcntrJB.createMsgsStateKeeper");

        return new MsgsStateKeeper(this.msgcntrToUserId, this.msgIds,
                this.msgStudyIds,

                this.msgUserXs, this.msgGrants);

    }

    /**
     * 
     * Updates the deatils of Messages
     * 
     * 
     * 
     * @return 0 if updation is successful and -2 when exception occurs
     * 
     */

    public int updateMsgcntr()

    {

        int output;

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            Rlog.debug("msgcntr", "before calling the update");

            output = msgcntrAgentRObj.updateMsgcntr(this
                    .createMsgcntrStateKeeper());

        }

        catch (Exception e) {
            Rlog
                    .fatal("msgcntr",
                            "EXCEPTION IN SETTING MSGCNTR DETAILS TO  SESSION BEAN"
                                    + e);
            return -2;

        }

        return output;

    }

    // added to update multiple messages simultaneously dinesh

    // using a stored procedure

    public MsgCntrDao updateMsgs()

    {

        try {

            Rlog.debug("msgcntr", "In MsgcntrJB.updateMsgs line 1");

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            Rlog.debug("msgcntr", "In MsgcntrJB.updateMsgs line 3");

            return msgcntrAgentRObj.updateMsgs(this.createMsgsStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("msgcntr", "Exception in MsgcntrJB.updateMsgs " + e);

        }

        return null;

    }

    /**
     * 
     * Returns the State keeper which holds all the messages/requests
     * 
     * that are not yet approved
     * 
     * 
     * 
     * @return all the pending requests
     * 
     */

    // public MsgsPendingValidationStateKeeper findByMsgsPendingValidation() {
    // try{
    // MsgcntrAgentHome msgcntrAgentHome = EJBUtil.getMsgcntrAgentHome();
    // MsgcntrAgentRObj msgcntrAgentRObj = msgcntrAgentHome.create();
    // return msgcntrAgentRObj.findByMsgsPendingValidation();
    // } catch(Exception e) {
    // Rlog.fatal("msgcntr","Error in findByMsgsPendingValidation() in MsgcntrJB
    // " + e);
    // }
    // return null;
    // }
    /**
     * 
     * this method returns all the details from message center corrosponding to
     * 
     * a particular user
     * 
     * 
     * 
     * @param userTo
     * 
     * @return
     * 
     */

    public MsgCntrDao getMsgCntrUserTo(int userTo) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCntrUserTo(userTo);

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in getMsgCntrUserTo() in MsgcntrJB "
                    + e);

        }

        return null;

    }

    /**
     * 
     * Returns Message Center Data Access Object containing all the
     * 
     * read messages for the user
     * 
     * 
     * 
     * @param userTo
     *            user for whom already read messages are required
     * 
     * @return Dao of Message center
     * 
     */

    public MsgCntrDao getMsgCntrUserToRead(int userTo) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCntrUserToRead(userTo);

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in getMsgCntrUserTo() in MsgcntrJB "
                    + e);

        }

        return null;

    }

    /**
     * 
     * Returns Message Center Data Access Object containing all the
     * 
     * unread messages for the user
     * 
     * 
     * 
     * @param userTo
     *            user for which unread messages are required
     * 
     * @return Dao of Message center
     * 
     */

    public MsgCntrDao getMsgCntrUserToUnRead(int userTo) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCntrUserToUnRead(userTo);

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in getMsgCntrUserTo() in MsgcntrJB "
                    + e);

        }

        return null;

    }

    public MsgCntrDao getMsgCntrVelosUser(int userTo) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCntrVelosUser(userTo);

        } catch (Exception e) {

            Rlog.fatal("msgcntr",
                    "Error in getMsgCntrVelosUser()  in MsgcntrJB " + e);

        }

        return null;

    }

    public MsgCntrDao getUserMsgs(int userTo) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getUserMsgs(userTo);

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in getUserMsgs()  in MsgcntrJB " + e);

        }

        return null;

    }

    public MsgCntrDao getUserMsgsWithSearch(int userTo, String lname,
            String fname, String userStatus) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getUserMsgsWithSearch(userTo, lname, fname,
                    userStatus);

        } catch (Exception e) {

            Rlog.fatal("msgcntr",
                    "Error in getUserMsgsWithSearch()  in MsgcntrJB " + e);

        }

        return null;

    }

    public MsgCntrDao getMsgCount(int userTo) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCount(userTo);

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in getMsgCount()  in MsgcntrJB " + e);

        }

        return null;

    }

    public MsgCntrDao getMsgCntrVelosUser(int userTo, String status) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCntrVelosUser(userTo, status);

        } catch (Exception e) {

            Rlog.fatal("msgcntr",
                    "Error in getMsgCntrVelosUser()  in MsgcntrJB " + e);

        }

        return null;

    }

    public MsgCntrDao getMsgCntrVelosUserAcc(int userTo) {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCntrVelosUserAcc(userTo);

        } catch (Exception e) {

            Rlog.fatal("msgcntr",
                    "Error in getMsgCntrVelosUser()  in MsgcntrJB " + e);

        }

        return null;

    }

    /**
     * 
     * Returns Message Center Data Access Object containing all the
     * 
     * messages for the study
     * 
     * 
     * 
     * @param study
     *            study for which messages are required
     * 
     * @return Dao of Message center
     * 
     */

    public MsgCntrDao getMsgCntrStudy(int study)

    {

        try {

            MsgcntrAgentRObj msgcntrAgentRObj = EJBUtil.getMsgcntrAgentHome();

            return msgcntrAgentRObj.getMsgCntrStudy(study);

        } catch (Exception e) {

            Rlog.fatal("msgcntr", "Error in getMsgCntrStudy() in MsgcntrJB "
                    + e);

        }

        return null;

    }

    /**
     * 
     * generates a String of XML for the msgcntr details .<br>
     * <br>
     * 
     * Not yet in Use
     * 
     * @return
     * 
     */

    public String toXML()

    {

        return new String(
                "<?xml version=\"1.0\"?>"
                        +

                        "<?cocoon-process type=\"xslt\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +

                        // to be done "<form action=\"msgcntrsummary.jsp\">" +

                        "<head>" +

                        // to be done "<title>Message center Summary </title>" +

                        "</head>" +

                        "<input type=\"hidden\" name=\"msgcntrId\" value=\""
                        + this.getMsgcntrId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\"msgcntrStudyId\" value=\""
                        + this.getMsgcntrStudyId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\" msgcntrFromUserId\" value=\""
                        + this.getMsgcntrFromUserId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\" msgcntrToUserId\" value=\""
                        + this.getMsgcntrToUserId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\" msgcntrText\" value=\""
                        + this.getMsgcntrText() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\" msgcntrReqType\" value=\""
                        + this.getMsgcntrReqType() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\" msgcntrStatus\" value=\""
                        + this.getMsgcntrStatus() + "\" size=\"10\"/>" +

                        "</form>"

        );

    }// end of method

}// end of class
