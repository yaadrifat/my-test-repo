/**
 * Classname : StudyTXArmJB
 * 
 * Version information: 1.0
 *
 * Date: 05/02/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.studyTXArm;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import com.velos.eres.business.common.StudyTXArmDao;
import com.velos.eres.business.studyTXArm.impl.StudyTXArmBean;
import com.velos.eres.service.studyTXArmAgent.StudyTXArmAgentRObj;
import com.velos.eres.service.studyTXArmAgent.impl.StudyTXArmAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for StudyTXArm
 * 
 * @author Anu Khanna
 * @version 1.0 05/02/2005
 */

public class StudyTXArmJB {

    /**
     * The primary key:
     */

    private int studyTXArmId;

    /**
     * study id
     */
    private String studyId;

    /**
     * treatment name
     */
    private String txName;

    /**
     * treatment drug info
     */

    private String txDrugInfo;

    /**
     * treatment description
     */
    private String txDesc;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    /**
     * Get the StudyTXArm ID
     * 
     * @return int StudyTXArm id
     */
    public int getStudyTXArmId() {
        return this.studyTXArmId;
    }

    /**
     * sets StudyTXArm ID
     * 
     * @param studyTXArmId
     */
    public void setStudyTXArmId(int studyTXArmId) {
        this.studyTXArmId = studyTXArmId;
    }

    /**
     * Get study id
     * 
     * @return String studyId
     */

    public String getStudyId() {
        return this.studyId;
    }

    /**
     * sets studyId
     * 
     * @param studyId
     */
    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    /**
     * Get treatment name
     * 
     * @return String txName
     */

    public String getTXName() {
        return this.txName;
    }

    /**
     * sets treatment name
     * 
     * @param txName
     */
    public void setTXName(String txName) {
        this.txName = txName;
    }

    /**
     * Get treatment drug info
     * 
     * @return String txDrugInfo
     */

    public String getTXDrugInfo() {
        return this.txDrugInfo;
    }

    /**
     * sets treatment drug info
     * 
     * @param txDrugInfo
     */
    public void setTXDrugInfo(String txDrugInfo) {
        this.txDrugInfo = txDrugInfo;
    }

    /**
     * Get treatment description
     * 
     * @return String txDesc
     */

    public String getTXDesc() {
        return this.txDesc;
    }

    /**
     * sets treatment description
     * 
     * @param txDesc
     */

    public void setTXDesc(String txDesc) {
        this.txDesc = txDesc;

    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param studyTXArmId:
     *            Unique Id constructor
     * 
     */

    public StudyTXArmJB(int studyTXArmId) {
        setStudyTXArmId(studyTXArmId);
    }

    /**
     * Default Constructor
     */

    public StudyTXArmJB() {
        Rlog.debug("studyTXArm", "StudyTXArmJB.StudyTXArmJB() ");
    }

    public StudyTXArmJB(int studyTXArmId, String studyId, String txName,
            String txDrugInfo, String txDesc, String creator,
            String modifiedBy, String ipAdd) {
        setStudyTXArmId(studyTXArmId);
        setStudyId(studyId);
        setTXName(txName);
        setTXDrugInfo(txDrugInfo);
        setTXDesc(txDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getStudyTXArmDetails of StudyTXArmId Session Bean:
     * StudyTXArmAgentBean
     * 
     * @return StudyTXArmStatKeeper
     * @see StudyTXArmAgentBean
     */
    public StudyTXArmBean getStudyTXArmDetails() {
        StudyTXArmBean studyTXArmsk = null;
        try {
            StudyTXArmAgentRObj studyTXArmAgentRObj = EJBUtil
                    .getStudyTXArmAgentHome();

            studyTXArmsk = studyTXArmAgentRObj
                    .getStudyTXArmDetails(this.studyTXArmId);

            Rlog.debug("StudyTXArm",
                    "StudyTXArmJB.getStudyTXArmDetails() StudyTXArmStateKeeper "
                            + studyTXArmsk);
        } catch (Exception e) {
            Rlog.fatal("studyTXArm", "Error in StudyTXArm getStudyTXArmDetails"
                    + e);
        }

        if (studyTXArmsk != null) {
            setStudyTXArmId(studyTXArmsk.getStudyTXArmId());
            setStudyId(studyTXArmsk.getStudyId());
            setTXName(studyTXArmsk.getTXName());
            setTXDrugInfo(studyTXArmsk.getTXDrugInfo());
            setTXDesc(studyTXArmsk.getTXDesc());
            setCreator(studyTXArmsk.getCreator());
            setModifiedBy(studyTXArmsk.getModifiedBy());
            setIpAdd(studyTXArmsk.getIpAdd());

        }

        return studyTXArmsk;

    }

    /**
     * Calls setStudyTXArmDetails() of StudyTXArm Session Bean:
     * StudyTXArmAgentBean
     * 
     */

    public int setStudyTXArmDetails() {
        int toReturn;
        try {

            StudyTXArmAgentRObj studyTXArmAgentRObj = EJBUtil
                    .getStudyTXArmAgentHome();

            StudyTXArmBean tempStateKeeper = new StudyTXArmBean();

            tempStateKeeper = this.createStudyTXArmStateKeeper();

            toReturn = studyTXArmAgentRObj
                    .setStudyTXArmDetails(tempStateKeeper);
            this.setStudyTXArmId(toReturn);

            Rlog.debug("studyTXArm", "StudyTXArmJB.setStudyTXArmDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "Error in setStudyTXArmDetails() in StudyTXArmJB " + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the StudyTXArm Record with the current
     *         values of the bean
     */
    public StudyTXArmBean createStudyTXArmStateKeeper() {

        return new StudyTXArmBean(studyTXArmId, studyId, txName, txDrugInfo,
                txDesc, creator, modifiedBy, ipAdd);

    }

    /**
     * Calls updateStudyTXArm() of StudyTXArm Session Bean: StudyTXArmAgentBean
     * 
     * @return The status as an integer
     */
    public int updateStudyTXArm() {
        int output;
        try {
            StudyTXArmAgentRObj studyTXArmAgentRObj = EJBUtil
                    .getStudyTXArmAgentHome();

            output = studyTXArmAgentRObj.updateStudyTXArm(this
                    .createStudyTXArmStateKeeper());
        } catch (Exception e) {
            Rlog.debug("studyTXArm",
                    "EXCEPTION IN SETTING StudyTXArm DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

 // Overloaded for INF-18183 ::: Akshi
    public int removeStudyTXArm( Hashtable<String, String> args) {

        int output;

        try {

            StudyTXArmAgentRObj studyTXArmAgentRObj = EJBUtil
                    .getStudyTXArmAgentHome();
            output = studyTXArmAgentRObj.removeStudyTXArm(this.studyTXArmId,args);
            
            return output;

        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "in StudyTXArmJB removeStudyTXArm() method" + e);
            return -1;
        }

    }

    public int removeStudyTXArm() {

        int output;

        try {

            StudyTXArmAgentRObj studyTXArmAgentRObj = EJBUtil
                    .getStudyTXArmAgentHome();

            output = studyTXArmAgentRObj.removeStudyTXArm(this.studyTXArmId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "in StudyTXArmJB removeStudyTXArm() method" + e);
            return -1;
        }

    }

    public StudyTXArmDao getStudyTrtmtArms(int studyId) {
        StudyTXArmDao studyTXArmDao = new StudyTXArmDao();
        try {

            StudyTXArmAgentRObj studyTXArmAgentRObj = EJBUtil
                    .getStudyTXArmAgentHome();

            studyTXArmDao = studyTXArmAgentRObj.getStudyTrtmtArms(studyId);
            return studyTXArmDao;
        } catch (Exception e) {
            Rlog.fatal("studyTXArm", "getStudyTrtmtArms in StudyTXArmJB " + e);
        }
        return studyTXArmDao;
    }

    public int getCntTXForPat(int studyId, int studyTXArmId) {
        int ret = 0;
        try {

            StudyTXArmAgentRObj studyTXArmAgentRObj = EJBUtil
                    .getStudyTXArmAgentHome();

            ret = studyTXArmAgentRObj.getCntTXForPat(studyId, studyTXArmId);

        } catch (Exception e) {
            Rlog.fatal("studyTXArm", "getCntTXForPat in StudyTXArmJB " + e);
        }
        return ret;
    }

}
