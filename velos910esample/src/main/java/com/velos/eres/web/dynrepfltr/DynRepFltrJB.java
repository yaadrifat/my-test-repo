/*
 *  Classname : DynRepFltrJB
 *
 *  Version information: 1.0
 *
 *  Date: 09/28/2003
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */
package com.velos.eres.web.dynrepfltr;

/*
 * IMPORT STATEMENTS
 */
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.dynrepfltr.impl.DynRepFltrBean;
import com.velos.eres.service.dynrepfltrAgent.DynRepFltrAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/*
 * END OF IMPORT STATEMENTS
 */

/**
 * Client side bean for dynrep
 * 
 * @author Vishal Abrol
 * @created October 25, 2004
 * @version 1.0 11/05/2003
 */

/*
 * Modified by Sonia Abrol, 01/13/05, added a new attribute parentFilter- Parent
 * Filter Id associated with the filter. Used for Multiform reports
 */

/*
 * Modified by Sonia Abrol. 01/26/05, added a new attribute formType. This
 * attribute is added to distinguish between application forms and core table
 * lookup forms
 */

/*
 * Modified by Sonia Abrol , 05/02/05, for enhancement default date range
 * filters for each filter, added new
 * attributes:flrDateRangeType,fltrDateRangeFrom,fltrDateRangeTo
 */

public class DynRepFltrJB {
    /**
     * The primary key:pk_dynrepview
     */

    private int id;

    /**
     * The foreign key reference to er_dynrep table.
     */
    private String repId;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    private String fltrName;

    private String fltrStr;

    /**
     * FormId associated with the filter
     */
    private String formId;

    /**
     * Parent Filter Id associated with the filter. Used for Multiform reports
     */
    private String parentFilter;

    /**
     * Distinguishes between application forms and core table lookup forms <br>
     * Possible Values: <br>
     * 'F' - Application forms <br>
     * 'C' - Core table lookup forms
     */
    private String formType;

    /**
     * the type of date range applied on the filter. Possible values: A:All,
     * M:Month,Y:Year,DR:given range, PS:patient study status
     */
    private String flrDateRangeType;

    /**
     * the 'from' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    private String fltrDateRangeFrom;

    /**
     * the 'to' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    private String fltrDateRangeTo;

    // /////////////////////////////////////////////////////////////////////////////////
    /**
     * Sets the id attribute of the DynRepFltrJB object
     * 
     * @param id
     *            The new id value
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the id attribute of the DynRepFltrJB object
     * 
     * @return The id value
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the repId attribute of the DynRepFltrJB object
     * 
     * @param repId
     *            The new repId value
     */
    public void setRepId(String repId) {
        this.repId = repId;
    }

    /**
     * Gets the repId attribute of the DynRepFltrJB object
     * 
     * @return The repId value
     */
    public String getRepId() {
        return repId;
    }

    /**
     * @return the Creator of the Form Field is returned
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The new creator value
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The new modifiedBy value
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The new ipAdd value
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Sets the fltrName attribute of the DynRepFltrJB object
     * 
     * @param fltrName
     *            The new fltrName value
     */
    public void setFltrName(String fltrName) {
        this.fltrName = fltrName;
    }

    /**
     * Gets the fltrName attribute of the DynRepFltrJB object
     * 
     * @return The fltrName value
     */
    public String getFltrName() {
        return fltrName;
    }

    /**
     * Sets the fltrStr attribute of the DynRepFltrJB object
     * 
     * @param fltrStr
     *            The new fltrStr value
     */
    public void setFltrStr(String fltrStr) {
        this.fltrStr = fltrStr;
    }

    /**
     * Gets the fltrStr attribute of the DynRepFltrJB object
     * 
     * @return The fltrStr value
     */
    public String getFltrStr() {
        return fltrStr;
    }

    /**
     * Returns the value of formId.
     */
    public String getFormId() {
        return formId;
    }

    /**
     * Sets the value of formId.
     * 
     * @param formId
     *            The value to assign formId.
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * Returns the value of parentFilter.
     */
    public String getParentFilter() {
        return parentFilter;
    }

    /**
     * Sets the value of parentFilter.
     * 
     * @param parentFilter
     *            Parent Filter Id, used in multiform reports
     */
    public void setParentFilter(String parentFilter) {
        this.parentFilter = parentFilter;
    }

    /**
     * Returns the value of formType.
     */
    public String getFormType() {
        return formType;
    }

    /**
     * Sets the value of formType.
     * 
     * @param formType
     *            The value to assign formType.
     */
    public void setFormType(String formType) {
        this.formType = formType;
    }

    /**
     * Returns the value of flrDateRangeType.
     */
    public String getFlrDateRangeType() {
        return flrDateRangeType;
    }

    /**
     * Sets the value of flrDateRangeType.
     * 
     * @param flrDateRangeType
     *            The value to assign flrDateRangeType.
     */
    public void setFlrDateRangeType(String flrDateRangeType) {
        this.flrDateRangeType = flrDateRangeType;
    }

    /**
     * Returns the value of fltrDateRangeFrom.
     */
    public String getFltrDateRangeFrom() {
        return fltrDateRangeFrom;
    }

    /**
     * Sets the value of fltrDateRangeFrom.
     * 
     * @param fltrDateRangeFrom
     *            The value to assign fltrDateRangeFrom.
     */
    public void setFltrDateRangeFrom(String fltrDateRangeFrom) {
        this.fltrDateRangeFrom = fltrDateRangeFrom;
    }

    /**
     * Returns the value of fltrDateRangeTo.
     */
    public String getFltrDateRangeTo() {
        return fltrDateRangeTo;
    }

    /**
     * Sets the value of fltrDateRangeTo.
     * 
     * @param fltrDateRangeTo
     *            The value to assign fltrDateRangeTo.
     */
    public void setFltrDateRangeTo(String fltrDateRangeTo) {
        this.fltrDateRangeTo = fltrDateRangeTo;
    }

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor for the DynRepFltrJB object
     */
    public DynRepFltrJB() {
        Rlog.debug("dynrep", "DynRepFltrJB.DynRepFltrJB() ");
    }

    /**
     * Constructor for the DynRepFltrJB object
     * 
     * @param id
     *            Description of the Parameter
     * @param repId
     *            Description of the Parameter
     * @param creator
     *            Description of the Parameter
     * @param modifiedBy
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @param fltrName
     *            Description of the Parameter
     * @param fltrStr
     *            Description of the Parameter
     * @param formId
     *            form with which the filter is associated
     * @param parentFilter
     *            Parent Filter Id, used in multiform reports
     * @param flrDateRangeType
     *            the type of date range applied on the filter. Possible values:
     *            A:All, M:Month,Y:Year,DR:given range, PS:patient study status
     * @param fltrDateRangeFrom
     *            the 'from' date for the date range filter. In case of 'patient
     *            study status', the attribute stores the code subtype of
     *            patient study status
     * @param fltrDateRangeTo
     *            the 'to' date for the date range filter. In case of 'patient
     *            study status', the attribute stores the code subtype of
     *            patient study status
     */
    public DynRepFltrJB(int id, String repId, String creator,
            String modifiedBy, String ipAdd, String fltrName, String fltrStr,
            String formId, String parentFilter, String formType,
            String flrDateRangeType, String fltrDateRangeFrom,
            String fltrDateRangeTo) {
        setId(id);
        setRepId(repId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setFltrName(fltrName);
        setFltrStr(fltrStr);
        setFormId(formId);
        setParentFilter(parentFilter);
        setFormType(formType);
        setFlrDateRangeType(flrDateRangeType);
        setFltrDateRangeFrom(fltrDateRangeFrom);
        setFltrDateRangeTo(fltrDateRangeTo);

    }

    /**
     * Gets the dynRepFltrDetails attribute of the DynRepFltrJB object
     * 
     * @return The dynRepFltrDetails value
     */
    public DynRepFltrBean getDynRepFltrDetails() {
        DynRepFltrBean dynrepsk = null;
        try {
            DynRepFltrAgentRObj dynrepAgentRObj = EJBUtil
                    .getDynRepFltrAgentHome();

            dynrepsk = dynrepAgentRObj.getDynRepFltrDetails(this.id);
            Rlog.debug("dynrep",
                    "DynRepJB.getDynRepFltrDetails() DynRepFltrStateKeeper "
                            + dynrepsk);
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynrepFltrJB getDynRepFltrDetails"
                    + e);
        }

        if (dynrepsk != null) {
            setId(dynrepsk.getId());
            setRepId(dynrepsk.getRepId());
            setCreator(dynrepsk.getCreator());
            setModifiedBy(dynrepsk.getModifiedBy());
            setIpAdd(dynrepsk.getIpAdd());
            setFltrName(dynrepsk.getFltrName());
            setFltrStr(dynrepsk.getFltrStr());
            setFormId(dynrepsk.getFormId());
            setParentFilter(dynrepsk.getParentFilter());
            setFormType(dynrepsk.getFormType());
            setFlrDateRangeType(dynrepsk.getFlrDateRangeType());
            setFltrDateRangeFrom(dynrepsk.getFltrDateRangeFrom());
            setFltrDateRangeTo(dynrepsk.getFltrDateRangeTo());

        }

        return dynrepsk;

    }

    /**
     * Sets the dynRepFltrDetails attribute of the DynRepFltrJB object
     * 
     * @return Description of the Return Value
     */
    public int setDynRepFltrDetails() {
        int toReturn;
        try {

            DynRepFltrAgentRObj dynrepAgentRObj = EJBUtil
                    .getDynRepFltrAgentHome();

            DynRepFltrBean tempStateKeeper = new DynRepFltrBean();

            tempStateKeeper = this.createDynRepFltrStateKeeper();

            toReturn = dynrepAgentRObj.setDynRepFltrDetails(tempStateKeeper);
            Rlog
                    .debug("dynrep",
                            "DynRepFltrJB.setDynRepFltrDetails() Outta thewre2"
                                    + dynrepAgentRObj + "statekeeper"
                                    + tempStateKeeper);
            this.setId(toReturn);
            Rlog.debug("dynrep", "DynRepDtJB.setDynRepFltrDetails()");
            return toReturn;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in setDynRepFltrDetails() in dynrepJB "
                    + e);
            return -2;
        }
    }

    /**
     * @return a statekeeper object for the dynrepId Record with the current
     *         values of the bean
     */
    public DynRepFltrBean createDynRepFltrStateKeeper() {

        return new DynRepFltrBean(id, repId, creator, modifiedBy, ipAdd,
                fltrName, fltrStr, formId, parentFilter, formType,
                flrDateRangeType, fltrDateRangeFrom, fltrDateRangeTo);
    }

    /**
     * Calls updateDynRep() of Lab Session Bean: LabAgentBean
     * 
     * @return The status as an integer
     */
    public int updateDynRepFltr() {
        int output;
        try {
            DynRepFltrAgentRObj dynrepAgentRObj = EJBUtil
                    .getDynRepFltrAgentHome();

            output = dynrepAgentRObj.updateDynRepFltr(this
                    .createDynRepFltrStateKeeper());
        } catch (Exception e) {
            Rlog.debug("dynrep",
                    "EXCEPTION IN SETTING dynrepFltr DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public int removeDynRepFltr() {

        int output;

        try {

            DynRepFltrAgentRObj dynrepAgentRObj = EJBUtil
                    .getDynRepFltrAgentHome();

            output = dynrepAgentRObj.removeDynRepFltr(this.id);
            return output;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "in DynRepFltrJB -removeDynRepFltr() method"
                    + e);
            return -1;
        }

    }
    // removeDynRepFltr() Overloaded for INF-18183 ::: Raviesh
    public int removeDynRepFltr(Hashtable<String, String> args) {

        int output;

        try {

            DynRepFltrAgentRObj dynrepAgentRObj = EJBUtil
                    .getDynRepFltrAgentHome();

            output = dynrepAgentRObj.removeDynRepFltr(this.id,args);
            return output;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "in DynRepFltrJB -removeDynRepFltr() method"
                    + e);
            return -1;
        }

    }
    
    /**
     * Description of the Method
     * 
     * @param fltrIds
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int removeDynFilters(ArrayList fltrIds) {
        int ret = -1;
        try {

            DynRepFltrAgentRObj dynrepAgentRObj = EJBUtil
                    .getDynRepFltrAgentHome();

            Rlog.debug("dynrep", "DynrepJB.removeDynFilters() after remote");
            ret = dynrepAgentRObj.removeDynFilters(fltrIds);
            Rlog.debug("dynrep", "DynRepJB.removeDynFilters after Dao");

        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:removeDynFilters) " + e);
            return -2;
        }
        return ret;
    }

    //Overloaded for INF-18183 ::: Ankit
    public int removeDynFilters(ArrayList fltrIds, Hashtable<String, String> auditInfo) {
        int ret = -1;
        try {

            DynRepFltrAgentRObj dynrepAgentRObj = EJBUtil
                    .getDynRepFltrAgentHome();

            Rlog.debug("dynrep", "DynrepJB.removeDynFilters() after remote");
            ret = dynrepAgentRObj.removeDynFilters(fltrIds, auditInfo);
            Rlog.debug("dynrep", "DynRepJB.removeDynFilters after Dao");

        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:removeDynFilters) " + e);
            return -2;
        }
        return ret;
    }

}
