/*
 * Classname : ConfigDetailsObject
 * 
 * Version information: 1.0
 *
 * Date: 02/17/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Gopu
 */

package com.velos.eres.web.user;

/**
 * Client Side Object for ConfigDetailsObject
 */
import java.util.ArrayList;

public class ConfigDetailsObject {

    /**
     * Stores the pagecustomfield Id value to ER_PAGECUSTOMFLDS
     */

    public ArrayList pcfId;

    /**
     * Stores the pagecustomfields value to ER_PAGECUSTOMFLDS
     */
    public ArrayList pcfField;

    /**
     * Stores the pagecustomfieldsAttribute value to ER_PAGECUSTOMFLDS
     */
    public ArrayList pcfAttribute;

    /**
     * Stores the pagecustomfields Mandatory field value to ER_PAGECUSTOMFLDS
     */
    public ArrayList pcfMandatory;

    /**
     * Stores the pagecustomfields Mandatory Message value to ER_PAGECUSTOMFLDS
     */
    
    
    //Added by Manimaran for Enh.#GL9
    
    /**
     * Stores the pagecustomfields Label value to ER_PAGECUSTOMFLDS
     */
    
    public ArrayList pcfLabel;
    
    

    public ArrayList getPcfId() {
        return this.pcfId;
    }

    public void setPcfId(ArrayList pcfId) {
        this.pcfId = pcfId;
    }

    public ArrayList getPcfField() {
        return this.pcfField;
    }

    public void setPcfField(ArrayList pcfField) {
        this.pcfField = pcfField;
    }

    public ArrayList getPcfAttribute() {
        return this.pcfAttribute;
    }

    public void setPcfAttribute(ArrayList pcfAttribute) {
        this.pcfAttribute = pcfAttribute;
    }

    public ArrayList getPcfMandatory() {
        return this.pcfMandatory;
    }

    public void setPcfMandatory(ArrayList pcfMandatory) {
        this.pcfMandatory = pcfMandatory;
    }
    
    //Added by Manimaran for #GL9
    public ArrayList getPcfLabel() {
        return this.pcfLabel;
    }

    public void setPcfLabel(ArrayList pcfLabel) {
        this.pcfLabel = pcfLabel;
    }

}
