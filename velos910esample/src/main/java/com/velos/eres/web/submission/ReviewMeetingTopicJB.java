package com.velos.eres.web.submission;

import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.submission.impl.ReviewMeetingTopicBean;
import com.velos.eres.service.submissionAgent.ReviewMeetingTopicAgent;
import com.velos.eres.service.submissionAgent.SubmissionProvisoAgent;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

public class ReviewMeetingTopicJB {

    private int id = 0;
    private Integer fkReviewMeeting;
    private String  topicNumber;
    private String  meetingTopic;
    private Integer creator;
    private Integer lastModifiedBy;
    private String  ipAdd;

    public ArrayList findMeetingTopicsByFkReviewMeeting(String reviewMeeting) {
        ArrayList output = null;
        try {
            ReviewMeetingTopicAgent topicAgent = EJBUtil.getReviewMeetingTopicAgentHome();
            output = 
                topicAgent.findMeetingTopicsByFkReviewMeeting(EJBUtil.stringToInteger(reviewMeeting));
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in ReviewMeetingTopicJB.findMeetingTopicsByFkReviewMeeting "
                    +e);
            output = null;
        }
        return output;
    }
    
    public int findByFkReviewMeetingAndTopicNumber() {
        int output = 0;
        try {
            ReviewMeetingTopicAgent topicAgent = EJBUtil.getReviewMeetingTopicAgentHome();
            ReviewMeetingTopicBean bean = 
                topicAgent.findByFkReviewMeetingAndNumber(
                        this.fkReviewMeeting,
                        this.topicNumber==null?"":this.topicNumber.trim());
            if (bean == null) { return 0; } 
            output = bean.getId();
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in ReviewMeetingTopicJB.findMeetingTopicsByFkReviewMeeting "
                    +e);
            output = -1;
        }
        return output;
    }
    
    public int updateMeetingTopic() {
        int output = 0;
        try {
            ReviewMeetingTopicAgent topicAgent = EJBUtil.getReviewMeetingTopicAgentHome();
            output = topicAgent.updateMeetingTopic(createEntityBean());
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in ReviewMeetingTopicJB.updateMeetingTopic "+e);
            output = -1;
        }
        return output;
    }

    public int createMeetingTopic() {
        int output = 0;
        try {
            ReviewMeetingTopicAgent topicAgent = EJBUtil.getReviewMeetingTopicAgentHome();
            output = topicAgent.createMeetingTopic(createEntityBean());
        } catch(Exception e) {
            Rlog.fatal("submission", "Error in ReviewMeetingTopicJB.createMeetingTopic "+e);
            output = -1;
        }
        return output;
    }

    public int loadMeetingTopic() {
        ReviewMeetingTopicBean bean = null;
        try {
            ReviewMeetingTopicAgent topicAgent = EJBUtil.getReviewMeetingTopicAgentHome();
            bean = topicAgent.getMeetingTopic(this.id);
        } catch (Exception e) {
            Rlog.fatal("submission", "Error in ReviewMeetingTopicJB.loadMeetingTopic " + e);
        }
        if (bean == null) { return 0; }
        
        // Pull the details from Entity bean
        this.id = bean.getId();
        this.fkReviewMeeting = bean.getFkReviewMeeting();
        this.topicNumber = bean.getTopicNumber();
        this.meetingTopic = bean.getMeetingTopic();
        this.creator = bean.getCreator();
        this.lastModifiedBy = bean.getLastModifiedBy();
        return bean.getId();
    }
    
    private ReviewMeetingTopicBean createEntityBean() {
        return new ReviewMeetingTopicBean(this.id, this.fkReviewMeeting, 
                this.topicNumber, this.meetingTopic, 
                this.creator, this.lastModifiedBy, this.ipAdd);
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getReviewMeeting() {
        return String.valueOf(fkReviewMeeting);
    }
    public void setReviewMeeting(String reviewMeeting) {
        this.fkReviewMeeting = EJBUtil.stringToInteger(reviewMeeting);
    }
    public String getTopicNumber() {
        return topicNumber;
    }
    public void setTopicNumber(String topicNumber) {
        if (topicNumber != null) { topicNumber = topicNumber.trim(); }
        this.topicNumber = topicNumber;
    }
    public String getMeetingTopic() {
        return meetingTopic;
    }
    public void setMeetingTopic(String meetingTopic) {
        this.meetingTopic = meetingTopic;
    }
    public String getLastModifiedBy() {
        return String.valueOf(lastModifiedBy);
    }
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = EJBUtil.stringToInteger(lastModifiedBy);
    }
    public String getCreator() {
        return String.valueOf(creator);
    }
    public void setCreator(String creator) {
        this.creator = EJBUtil.stringToInteger(creator);
    }
    public String getIpAdd() {
        return ipAdd;
    }
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public int removeReviewMeetingTopic() {

        int output;

        try {

        	ReviewMeetingTopicAgent topicAgent = EJBUtil.getReviewMeetingTopicAgentHome();
        	this.meetingTopic = null;
        	topicAgent.updateMeetingTopic(createEntityBean());
            output = topicAgent.removeReviewMeetingTopic(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("submisison", "in ReviewMeetingTopicJB removeReviewMeetingTopic() method" + e);
            return -1;
        }

    }
 // Overloaded for INF-18183 ::: AGodara
    public int removeReviewMeetingTopic(Hashtable<String, String> auditInfo) {

        int output;

        try {

        	ReviewMeetingTopicAgent topicAgent = EJBUtil.getReviewMeetingTopicAgentHome();
        	this.meetingTopic = null;
        	topicAgent.updateMeetingTopic(createEntityBean());
            output = topicAgent.removeReviewMeetingTopic(this.id,auditInfo);
            return output;

        } catch (Exception e) {
            Rlog.fatal("submisison", "in ReviewMeetingTopicJB removeReviewMeetingTopic(Hashtable<String, String> auditInfo)" + e);
            return -1;
        }

    }
    
    
}
