/*
 *  Classname : CommonJB
 *
 *  Version information: 1.0
 *
 *  Date: 09/28/2003
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */
package com.velos.eres.web.common;

/*
 * IMPORT STATEMENTS
 */
import java.util.ArrayList;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.SasExportDao;
import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.service.commonAgent.CommonAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/*
 * END OF IMPORT STATEMENTS
 */

/**
 * Client side bean for common
 * 
 * @author Vishal Abrol
 * @created May 10, 2005
 * @version 1.0 11/05/2003
 */

public class CommonJB {

    /**
     * Gets the sasHandle attribute of the CommonJB object
     * 
     * @param type
     *            Description of the Parameter
     * @param id
     *            Description of the Parameter
     * @return The sasHandle value
     */
    public SasExportDao getSasHandle(String type, int id) {
        SasExportDao sasDao = new SasExportDao();
        try {

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            Rlog.debug("common", "commonJB.getSsasHandle() after remote");
            sasDao = commonAgentRObj.getSasHandle(type, id);
            Rlog.debug("common", "commonJB.getSasHandle()() after Dao");
            return sasDao;
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:getSsasHandle()() " + e);
            e.printStackTrace();
        }
        return sasDao;
    }
    
    public CommonDAO getCommonHandle() {
        CommonDAO commonDao = new CommonDAO();
        try {

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            commonDao = commonAgentRObj.getCommonHandle();
            return commonDao;
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:getCommonHandle()() " + e);
            e.printStackTrace();
        }
        return commonDao;
    }

    /**
     * Gets the settingsInstance attribute of the CommonJB object
     * 
     * @return The settingsInstance value
     */
    public SettingsDao getSettingsInstance() {
        try {
            Rlog.debug("common", "commonJB.getSettingsInstance() starting");

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            Rlog.debug("common", "commonJB.getSettingsInstance() after remote");
            return commonAgentRObj.getSettingsInstance();
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:getSsasHandle()() " + e);
            return null;
        }

    }

    /**
     * Description of the Method
     * 
     * @param modnum
     *            Description of the Parameter
     * @param modname
     *            Description of the Parameter
     * @param keyword
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public SettingsDao retrieveSettings(int modnum, int modname, String keyword) {

        try {

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            Rlog.debug("common", "commonJB.retrieveSettings after remote");
            return commonAgentRObj.retrieveSettings(modnum, modname, keyword);
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:getSsasHandle()() " + e);
            return null;
        }

    }

    /**
     * Description of the Method
     * 
     * @param modnum
     *            Module number
     * @param modname
     *            module name
     * @param keyword
     *            Keyword List
     * @return Setting DAO object
     */
    public SettingsDao retrieveSettings(int modnum, int modname,
            ArrayList keyword) {

        try {

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            Rlog.debug("common", "commonJB.retrieveSettings after remote");
            return commonAgentRObj.retrieveSettings(modnum, modname, keyword);
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:RetrieveSettings() " + e);
            return null;
        }

    }

    /**
     * Description of the Method
     * 
     * @param modnum
     *            Description of the Parameter
     * @param modname
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public SettingsDao retrieveSettings(int modnum, int modname) {

        try {

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            Rlog.debug("common", "commonJB.retrieveSettings after remote");
            return commonAgentRObj.retrieveSettings(modnum, modname);
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:getSsasHandle()() " + e);
            return null;
        }
    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name and keyword
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keyword
     *            Keyword
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname, String keyword) {

        try {

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            return commonAgentRObj.purgeSettings(modnum, modname, keyword);
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:purgeSettings() " + e);
            return -1;
        }
    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name and keyword
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keywordList
     *            ArrayList of keywords
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname,
            ArrayList keywordList) {
        try {

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            return commonAgentRObj.purgeSettings(modnum, modname, keywordList);
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:purgeSettings() " + e);
            return -1;
        }
    }

	//Added by Gopu for July-August Enhancement (#U2)
	/**
     * Gets the settingsInstance attribute of the CommonJB object
     * 
     * @return The settingsDao value
     */
    public int updateSettingsWithoutPK(SettingsDao settingsDao) {
        try {
            Rlog.debug("common", "commonJB.updateSettingsWithoutPK() starting");

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            Rlog.debug("common", "commonJB.updateSettingsWithoutPK() after remote");
            return commonAgentRObj.updateSettingsWithoutPK(settingsDao);
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:updateSettingsWithoutPK() " + e);
            return -1;
        }

    }

	 public int insertSettings(SettingsDao settingsDao) {
        try {
            Rlog.debug("common", "commonJB.insertSettings() starting");

            CommonAgentRObj commonAgentRObj = EJBUtil.getCommonAgentHome();
            Rlog.debug("common", "commonJB.insertSettings() after remote");
            return commonAgentRObj.insertSettings(settingsDao);
        } catch (Exception e) {
            Rlog.fatal("common", "Error in commonJB:insertSettings() " + e);
            return -1;
        }

    }

}
