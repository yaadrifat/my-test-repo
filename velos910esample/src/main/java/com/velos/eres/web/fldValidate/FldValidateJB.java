/*
 * Classname : FldValidateJB
 * 
 * Version information: 1.0
 *
 * Date: 11/05/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Kaura
 */

package com.velos.eres.web.fldValidate;

/* IMPORT STATEMENTS */
import com.velos.eres.business.fldValidate.impl.FldValidateBean;
import com.velos.eres.service.fldValidateAgent.FldValidateAgentRObj;
import com.velos.eres.service.fldValidateAgent.impl.FldValidateAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Form Notification
 * 
 * @author Sonia Kaura
 * @version 1.0 11/05/2003
 */

public class FldValidateJB {
    /**
     * The form primary key:pk_formsec
     */

    private int fldValidateId;

    /**
     * The foreign key reference from the Field : FK_FLDLIB
     */
    private String fldLibId;

    /**
     * The First Operator in the Validation : FLD_VALIDATE_OP1
     */

    private String fldValidateOp1;

    /**
     * The First Value in the Validation : FLD_VALIDATE_Val1
     */
    private String fldValidateVal1;

    /**
     * The First Boolean Operator in the Validation : FLD_VALIDATE_LOGOP1
     * 
     */

    private String fldValidateLogOp1;

    /**
     * The Second Operator in the Validation : FLD_VALIDATE_OP2
     * 
     */
    private String fldValidateOp2;

    /**
     * The second Value in the Validation : FLD_VALIDATE_Val2
     */
    private String fldValidateVal2;

    /**
     * The Boolean Connected Operator in Validation : FLD_VALIDATE_LOGOP2
     * 
     */

    private String fldValidateLogOp2;

    /**
     * The Java Script of the Validation : FLD_VALIDATE_JAVASCR
     * 
     */

    private String fldValidateJavaScr;

    private String recordType;

    /**
     * 
     * The ID of the Creator is sotred : CREATOR
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // START OF GETTER AND SETTERS

    /**
     * 
     * 
     * @return fldValidateId
     */
    public int getFldValidateId() {

        return fldValidateId;
    }

    /**
     * 
     * 
     * @param fldValidateId
     */
    public void setFldValidateId(int fldValidateId) {
        this.fldValidateId = fldValidateId;
    }

    /**
     * 
     * 
     * @return fldLibId
     */
    public String getFldLibId() {
        return fldLibId;
    }

    /**
     * 
     * 
     * @param fldLibId
     */
    public void setFldLibId(String fldLibId) {

        this.fldLibId = fldLibId;
    }

    /**
     * 
     * 
     * @return fldValidateOp1
     */
    public String getFldValidateOp1() {
        return fldValidateOp1;

    }

    /**
     * 
     * 
     * @param fldValidateOp1
     */
    public void setFldValidateOp1(String fldValidateOp1) {

        this.fldValidateOp1 = fldValidateOp1;
    }

    /**
     * 
     * @return fldValidateVal1
     */
    public String getFldValidateVal1() {

        return fldValidateVal1;
    }

    /**
     * 
     * @param fldValidateVal1
     */
    public void setFldValidateVal1(String fldValidateVal1) {

        this.fldValidateVal1 = fldValidateVal1;
    }

    /**
     * 
     * 
     * @return fldValidateLogOp1
     */
    public String getFldValidateLogOp1() {

        return fldValidateLogOp1;
    }

    /**
     * 
     * 
     * @param fldValidateLogOp1
     */
    public void setFldValidateLogOp1(String fldValidateLogOp1) {
        this.fldValidateLogOp1 = fldValidateLogOp1;
    }

    /**
     * 
     * 
     * @return fldValidateOp2
     */
    public String getFldValidateOp2() {
        return fldValidateOp2;
    }

    /**
     * 
     * 
     * @param fldValidateOp2
     */
    public void setFldValidateOp2(String fldValidateOp2) {

        this.fldValidateOp2 = fldValidateOp2;
    }

    /**
     * 
     * @return fldValidateVal2
     */
    public String getFldValidateVal2() {

        return fldValidateVal2;
    }

    /**
     * 
     * @param fldValidateVal2
     */
    public void setFldValidateVal2(String fldValidateVal2) {

        this.fldValidateVal2 = fldValidateVal2;
    }

    /**
     * 
     * @return fldValidateLogOp2
     */
    public String getFldValidateLogOp2() {
        return fldValidateLogOp2;

    }

    /**
     * 
     * 
     * @param fldValidateLogOp2
     */
    public void setFldValidateLogOp2(String fldValidateLogOp2) {

        this.fldValidateLogOp2 = fldValidateLogOp2;
    }

    /**
     * 
     * @return fldValidateJavaScr
     */
    public String getFldValidateJavaScr() {

        return fldValidateJavaScr;
    }

    /**
     * 
     * 
     * @param fldValidateJavaScr
     */
    public void setFldValidateJavaScr(String fldValidateJavaScr) {

        this.fldValidateJavaScr = fldValidateJavaScr;
    }

    /**
     * @return the Creator of is returned
     * 
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     * 
     * 
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF THE GETTER AND SETTER METHODS
    // //////////////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param frmNotifyId:
     *            Unique Id constructor
     * 
     */

    public FldValidateJB(int fldValidateId) {
        setFldValidateId(fldValidateId);
    }

    /**
     * Default Constructor
     */

    public FldValidateJB() {
        Rlog.debug("fldvalidate", "FldValidateJB.FldValidateJB() ");
    }

    public FldValidateJB(int fldValidateId, String fldLibId,
            String fldValidateOp1, String fldValidateVal1,
            String fldValidateLogOp1, String fldValidateOp2,
            String fldValidateVal2, String fldValidateLogOp2,
            String fldValidateJavaScr, String recordType, String creator,
            String modifiedBy, String ipAdd) {

        setFldValidateId(fldValidateId);
        setFldLibId(fldLibId);
        setFldValidateOp1(fldValidateOp1);
        setFldValidateVal1(fldValidateVal1);
        setFldValidateLogOp1(fldValidateLogOp1);
        setFldValidateOp2(fldValidateOp2);
        setFldValidateVal2(fldValidateVal2);
        setFldValidateLogOp2(fldValidateLogOp2);
        setFldValidateJavaScr(fldValidateJavaScr);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("fldvalidate", "FldValidateJB.FldValidate(all parameters)");
    }

    /**
     * Calls getFldValidateDetails of Form Notify Session Bean:
     * FldValidateAgentBean
     * 
     * @return FldValidateStatKeeper
     * @see FldValidateAgentBean
     */

    public FldValidateBean getFldValidateDetails() {
        FldValidateBean fldValidatesk = null;
        try {
            FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                    .getFldValidateAgentHome();

            fldValidatesk = fldValidateAgentRObj
                    .getFldValidateDetails(this.fldValidateId);
            Rlog.debug("fldvalidate",
                    "FldValidateJB.getFldValidateDetails() FldValidateStateKeeper "
                            + fldValidatesk);
        } catch (Exception e) {
            Rlog.fatal("fldvalidate",
                    "Error in FldValidate getFldValidateDetails" + e);
        }

        if (fldValidatesk != null) {
            this.fldValidateId = fldValidatesk.getFldValidateId();
            this.fldLibId = fldValidatesk.getFldLibId();
            this.fldValidateOp1 = fldValidatesk.getFldValidateOp1();
            this.fldValidateVal1 = fldValidatesk.getFldValidateVal1();
            this.fldValidateLogOp1 = fldValidatesk.getFldValidateLogOp1();
            this.fldValidateOp2 = fldValidatesk.getFldValidateOp2();
            this.fldValidateVal2 = fldValidatesk.getFldValidateVal2();
            this.fldValidateLogOp2 = fldValidatesk.getFldValidateLogOp2();
            this.fldValidateJavaScr = fldValidatesk.getFldValidateJavaScr();
            this.creator = fldValidatesk.getCreator();
            this.modifiedBy = fldValidatesk.getModifiedBy();
            this.ipAdd = fldValidatesk.getIpAdd();
            this.recordType = fldValidatesk.getRecordType();

        }

        return fldValidatesk;

    }

    /**
     * Calls setFldValidateDetails() of Notify Form Session Bean:
     * FldValidateAgentBean
     * 
     */

    public int setFldValidateDetails() {
        int toReturn;
        try {

            FldValidateAgentRObj FldValidateAgentRObj = EJBUtil
                    .getFldValidateAgentHome();

            FldValidateBean tempStateKeeper = new FldValidateBean();
            tempStateKeeper = this.createFldValidateStateKeeper();
            toReturn = FldValidateAgentRObj
                    .setFldValidateDetails(tempStateKeeper);
            this.setFldValidateId(toReturn);

            Rlog.debug("fldvalidate", "FldValidateJB.setFldValidateDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("fldvalidate",
                    "Error in setFldValidateDetails() in FldValidateJB " + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the Form Notify Record with the current
     *         values of the bean
     */
    public FldValidateBean createFldValidateStateKeeper() {
        Rlog
                .debug("fldvalidate",
                        "FldValidateJB.createFldValidateStateKeeper ");

        return new FldValidateBean(fldValidateId, fldLibId, fldValidateOp1,
                fldValidateVal1, fldValidateLogOp1, fldValidateOp2,
                fldValidateVal2, fldValidateLogOp2, fldValidateJavaScr,
                recordType, creator, modifiedBy, ipAdd);

    }

    /**
     * Calls updateFldValidate() of FldValidate Session Bean:
     * FldValidateAgentBean
     * 
     * @return
     */
    public int updateFldValidate() {
        int output;
        try {
            FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                    .getFldValidateAgentHome();

            output = fldValidateAgentRObj.updateFldValidate(this
                    .createFldValidateStateKeeper());
        } catch (Exception e) {
            Rlog.debug("fldvalidate",
                    "EXCEPTION IN SETTING FORM NOTIFY DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeFldValidate() {

        int output;

        try {

            FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                    .getFldValidateAgentHome();

            output = fldValidateAgentRObj.removeFldValidate(this.fldValidateId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("fldvalidate",
                    "in FldValidateJB -removeFldValidate() method");
            return -1;
        }

    }

    public FldValidateBean findByFieldId(int fldlibId) {
        FldValidateBean fldValidatesk = null;
        try {

            FldValidateAgentRObj fldValidateAgentRObj = EJBUtil
                    .getFldValidateAgentHome();

            fldValidatesk = fldValidateAgentRObj.findByFieldId(fldlibId);
            Rlog.debug("fldvalidate",
                    "FldValidateJB.findByFieldId() FldValidateStateKeeper "
                            + fldValidatesk);
        } catch (Exception e) {
            Rlog.fatal("fldvalidate", "Error in FldValidate findByFieldId" + e);
        }

        if (fldValidatesk != null) {
            this.fldValidateId = fldValidatesk.getFldValidateId();
            // Rlog.debug("fldvalidate","this.fldValidateId"+this.fldValidateId);
            this.fldLibId = fldValidatesk.getFldLibId();
            // Rlog.debug("fldvalidate","this.fldLibId"+this.fldLibId);
            this.fldValidateOp1 = fldValidatesk.getFldValidateOp1();
            // Rlog.debug("fldvalidate","this.fldValidateOp1"+this.fldValidateOp1);
            this.fldValidateVal1 = fldValidatesk.getFldValidateVal1();
            // Rlog.debug("fldvalidate","this.fldValidateVal1"+this.fldValidateVal1);
            this.fldValidateLogOp1 = fldValidatesk.getFldValidateLogOp1();
            // Rlog.debug("fldvalidate","this.fldValidateLogOp1"+this.fldValidateLogOp1);
            this.fldValidateOp2 = fldValidatesk.getFldValidateOp2();
            // Rlog.debug("fldValidateOp2","this.fldLibId"+this.fldValidateOp2);
            this.fldValidateVal2 = fldValidatesk.getFldValidateVal2();
            // Rlog.debug("fldvalidate","this.fldValidateVal2"+this.fldValidateVal2);
            this.fldValidateLogOp2 = fldValidatesk.getFldValidateLogOp2();
            this.fldValidateJavaScr = fldValidatesk.getFldValidateJavaScr();
            // Rlog.debug("fldvalidate","this.fldValidateJavaScr"+this.fldValidateJavaScr);
            this.creator = fldValidatesk.getCreator();
            this.modifiedBy = fldValidatesk.getModifiedBy();
            this.ipAdd = fldValidatesk.getIpAdd();
            this.recordType = fldValidatesk.getRecordType();

        }

        return fldValidatesk;

    }

}
