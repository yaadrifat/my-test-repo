/*
 * Classname : ObjectShareJB
 * 
 * Version information: 
 *
 * Date: 29/04/04
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.objectShare;

/* IMPORT STATEMENTS */

import java.util.ArrayList;

import com.velos.eres.business.common.ObjectShareDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for object share
 * 
 * @author Anu Khanna
 * @version Date: 29/04/04
 */

public class ObjectShareJB {
    /**
     * The form primary key:object Share
     */

    private int objId;

    /**
     * Object Number : 1- Forms sharewith, 2- Ad-hoc query sharewith, 3 -
     * Calendar sharewith
     */
    private String objNumber;

    /**
     * The foreign key reference :Stores the PK of the respective module like
     * form_id incase of forms, report_id or calendar_id
     */

    private String fkObj;

    /**
     * Stores the user id for user module access relationship. Also stores
     * sharewith option : stores the group ids, study ids and the site ids to
     * which the form is shared with
     */
    private String objSharedId;

    /**
     * Flag to distinguish the records of module access users and module shared
     * with option.U - user id, S - study id, G - group id, O - organization id
     * 
     */

    private String objSharedType;

    private String recordType;

    /**
     * 
     * The ID of the Creator is sotred : CREATOR
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    private String shrdWithIds;

    private String shrdWithNames;

    // /////////////////////////////////////////////////////////////////////////////////

    // START OF GETTER AND SETTERS

    /**
     * 
     * 
     * @return objId
     */
    public int getObjId() {

        return objId;
    }

    /**
     * 
     * 
     * @param objId
     */
    public void setObjId(int objId) {
        this.objId = objId;
    }

    /**
     * 
     * 
     * @return objNumber
     */
    public String getObjNumber() {
        return objNumber;
    }

    /**
     * 
     * 
     * @param objNumber
     */
    public void setObjNumber(String objNumber) {

        this.objNumber = objNumber;
    }

    /**
     * 
     * 
     * @return fkObj
     */
    public String getFkObj() {
        return fkObj;

    }

    /**
     * 
     * 
     * @param fkObj
     */
    public void setFkObj(String fkObj) {

        this.fkObj = fkObj;
    }

    /**
     * 
     * @return objSharedId
     */
    public String getObjSharedId() {

        return objSharedId;
    }

    /**
     * 
     * @param objSharedId
     */
    public void setObjSharedId(String objSharedId) {

        this.objSharedId = objSharedId;
    }

    /**
     * 
     * 
     * @return objSharedType
     */
    public String getObjSharedType() {

        return objSharedType;
    }

    /**
     * 
     * 
     * @param objSharedType
     */
    public void setObjSharedType(String objSharedType) {
        this.objSharedType = objSharedType;
    }

    /**
     * @return the Creator of is returned
     * 
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     * 
     * 
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getShrdWithIds() {
        return shrdWithIds;

    }

    public void setShrdWithIds(String shrdWithIds) {
        this.shrdWithIds = shrdWithIds;
    }

    public String getShrdWithNames() {
        return this.shrdWithNames;
    }

    public void setShrdWithNames(String shrdWithNames) {
        this.shrdWithNames = shrdWithNames;
    }

    // END OF THE GETTER AND SETTER METHODS
    // //////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     * 
     * @param objId:
     *            Unique Id constructor
     * 
     */

    public ObjectShareJB(int objId) {
        setObjId(objId);
    }

    /**
     * Default Constructor
     */

    public ObjectShareJB() {

    }

    public ObjectShareJB(int objId, String objNumber, String fkObj,
            String objSharedId, String objSharedType, String recordType,
            String creator, String modifiedBy, String ipAdd) {

        setObjId(objId);
        setObjNumber(objNumber);
        setFkObj(fkObj);
        setObjSharedId(objSharedId);
        setObjSharedType(objSharedType);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);

    }
    
    public int insertIntoObjectShareByType() {
    	int ret = -1;
    	ObjectShareDao objShareDao = new ObjectShareDao();
    	ret = objShareDao.insertIntoObjectShareByType(this.fkObj, this.objNumber,
    			this.objSharedId, this.objSharedType, this.creator, this.ipAdd);
    	return ret;
    }
    
    public int insertIntoObjectShareForRespAuthors(String lfDisplayType) {
    	int ret = -1;
    	ObjectShareDao objShareDao = new ObjectShareDao();
    	ret = objShareDao.insertIntoObjectShareForRespAuthors(this.fkObj, 
    			lfDisplayType, this.creator, this.ipAdd);
    	return ret;
    }

    public int setObjectShareDetails() {

        Rlog.debug("common", "inside setObjectShareDetails()");
        int ret = 0;
        try {
            ObjectShareDao objShare = new ObjectShareDao();
            ret = objShare
                    .insertIntoObjectShare(
                            EJBUtil.stringToNum(this.getFkObj()), this
                                    .getObjNumber(), this.getObjSharedId(),
                            this.getObjSharedType(), this.getCreator(), this
                                    .getIpAdd());
            Rlog.debug("common", "in setObjectShareDetails() return val" + ret);

        } catch (Exception e) {
            Rlog.fatal("common", "error in setObjectShareDetails:" + e);
            return -1;
        }
        return ret;

    }

    /**
     * Retrievs the sharing details for an object using DAO
     * 
     * @param fkObj -
     *            eresearch interal id of the object
     * @param objShareType -
     *            level of Share type e.g U-USer,g-Group..
     * @param module -
     *            module for which the data is retrieved.
     * @param usr -
     *            user Id
     * @param accId -
     *            account Id for which data is being retrieved.
     */

    public void getObjectShareDetails(int fkObj, String objShareType,
            String module, String usr, String accId) {
        int len = 0;

        if (objShareType.equals("G") || objShareType.equals("S")
                || objShareType.equals("O") || (objShareType.equals("U"))
                || (objShareType.equals("P"))) {
            String selNames = "";
            String selIds = "";
            String names = "";
            ArrayList arrIds = new ArrayList();
            ArrayList arrNames = new ArrayList();
            ObjectShareDao objShare = new ObjectShareDao();
            objShare.getObjSharedWithInfo(fkObj, objShareType, module);
            arrNames = objShare.getSharedWithNames();
            arrIds = objShare.getSharedWithIds();

            len = arrNames.size();

            if (len == 1) {
                selNames = (String) arrNames.get(0);
                selIds = EJBUtil.integerToString((Integer) arrIds.get(0));
            } else {
                if (len > 0) {
                    for (int count = 0; count < len; count++) {
                        selNames = selNames + (String) arrNames.get(count);
                        selIds = selIds
                                + EJBUtil.integerToString((Integer) arrIds
                                        .get(count));

                        selNames = selNames + ",";
                        selIds = selIds + ",";

                    }

                    int pos = 0;

                    pos = selNames.lastIndexOf(",");
                    selNames = selNames.substring(0, pos);

                    pos = selIds.lastIndexOf(",");
                    selIds = selIds.substring(0, pos);

                }

            }

            setShrdWithNames(selNames);
            setShrdWithIds(selIds);

        }
        /*
         * else if (objShareType.equals("P")) { setShrdWithIds(arrIds); }
         */
        else if (objShareType.equals("A")) {

            setShrdWithIds(accId);
        }

    }

}
