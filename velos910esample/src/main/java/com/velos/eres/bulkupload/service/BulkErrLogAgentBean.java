package com.velos.eres.bulkupload.service;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.bulkupload.business.BulkErrLogBean;
import com.velos.eres.service.util.Rlog;
 
@Stateless
@Remote( { BulkErrLogAgent.class } )
public class BulkErrLogAgentBean implements BulkErrLogAgent{
	  @PersistenceContext(unitName = "eres")
	    protected EntityManager em;
	  public BulkErrLogBean getBulkErrLogDetails(int pkSpec) {
	        if (pkSpec == 0) {
	            return null;
	        } 
	        BulkErrLogBean spec = null;
	        try {
	        	spec = (BulkErrLogBean) em.find(BulkErrLogBean .class, new Integer(pkSpec));	            
	        } catch (Exception e) {
	            Rlog.fatal("bulkupload",
	                    "Error in getBulkErrLogDetails() in BulkErrLogAgentBean" + e);
	        }
	        return spec;
	    }
	 
	  public int setBulkErrLogDetails(BulkErrLogBean spsk) {    
	        try {
	        	
	        	BulkErrLogBean  spec = new BulkErrLogBean(); 
	        	spec.updateBulkErrLog(spsk);        	 
	            em.persist(spec);
	            int tst=spec.getPkErrLog();
	           
	            return spec.getPkErrLog();
	        } catch (Exception e) {
	        	  Rlog.fatal("bulkupload",  "Error in setBulkErrLogDetails() in BulkErrLogAgentBean" + e);
	        }
	        return 0;
	    }
	    public int updateBulkErrLog(BulkErrLogBean spsk) {
	    	BulkErrLogBean appSpec= null;
	        int output;
	        try {	            
	            output = appSpec.updateBulkErrLog(spsk);            
	            em.merge(appSpec);
	        } catch (Exception e) {
	        	Rlog.fatal("bulkupload",  "Error in updateBulkErrLog() in BulkErrLogAgentBean" + e);
	            return -2;
	        }
	        return output;
	    }
}
