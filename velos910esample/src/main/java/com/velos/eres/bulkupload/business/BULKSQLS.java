package com.velos.eres.bulkupload.business;

public class BULKSQLS {
	 public static final String erbulkupld = "select PK_BULK_ENTITY_MASTER from ER_BULK_ENTITY_MASTER where rtrim(ENTITY_NAME) = ?"; 
	 public static final String erbulksurfld ="select PK_BULK_ENTITY_DETAIL,VELOS_FIELD_NAME,DATA_TYPE,MANDATORY_FLAG from ER_BULK_ENTITY_DETAIL where FK_BULK_ENTITY_MASTER = ?";
	 public static final String erbulkpatprotdet="select pk_patprot,fk_study,fk_per,fk_site_enrolling from er_patprot where patprot_stat = 1 and patprot_patstdid=?";
	 public static final String erbulkpatprotfk="select fk_protocol from er_patprot where pk_patprot=?";
	 public static final String erbulkpatprotName="select name from event_assoc where event_id=?";
	 public static final String erbulkgetparentSpec="select pk_specimen from er_specimen where fk_account=? and spec_id=?";
	 public static final String erbulkgetpatient="select PK_PER,FK_SITE from er_per where PER_CODE=?";
	 public static final String erbulkgetuseracc="SELECT distinct u.pk_user,u.USR_LASTNAME,(select SITE_NAME from er_site where PK_SITE  = FK_SITEID) as site_name,u.USR_FIRSTNAME, u.USR_MIDNAME, u.USR_LOGNAME, u.USR_STAT, u.USR_TYPE,a.ADDRESS, a.ADD_CITY, a.ADD_STATE, a.ADD_ZIPCODE, a.ADD_EMAIL, a.ADD_PHONE from  ER_USER u, ER_ADD a Where u.fk_peradd = a.pk_add and u.USR_STAT in('A','B','D') and (u.USR_TYPE <>'X' and u.USR_TYPE <> 'P' ) and u.USER_HIDDEN <> 1 and u.fk_account =?";
}
