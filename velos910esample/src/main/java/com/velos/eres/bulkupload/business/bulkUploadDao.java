package com.velos.eres.bulkupload.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.Rlog;

public class bulkUploadDao extends CommonDAO implements java.io.Serializable{

	
	
	private ArrayList pkBulkUpload;
    private ArrayList fkBulkTamplate;
    private ArrayList fileName;
    private ArrayList totRecord;
    private ArrayList sucRecord;
    private ArrayList unsucRecord;
    private ArrayList usrFirstName;
    private ArrayList usrMidName;
    private ArrayList usrLastName;
	private ArrayList createdOn;
	private ArrayList errorMsg;
	private ArrayList rowNum;
    
    public ArrayList getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(ArrayList errorMsg) {
		this.errorMsg = errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg.add(errorMsg);
	}

	public ArrayList getRowNum() {
		return rowNum;
	}

	public void setRowNum(ArrayList rowNum) {
		this.rowNum = rowNum;
	}
	public void setRowNum(String rowNum) {
		this.rowNum.add(rowNum);
	}
	public bulkUploadDao(){
    	pkBulkUpload=new ArrayList();
    	fkBulkTamplate=new ArrayList();
    	fileName=new ArrayList();
    	totRecord=new ArrayList();
    	sucRecord=new ArrayList();
    	unsucRecord=new ArrayList();
    	createdOn=new ArrayList();
    	usrFirstName=new ArrayList();
    	usrMidName=new ArrayList();
    	usrLastName=new ArrayList();
    	errorMsg=new ArrayList();
    	rowNum=new ArrayList();
    }

    public ArrayList getUsrFirstName() {
		return usrFirstName;
	}


	public void setUsrFirstName(ArrayList usrFirstName) {
		this.usrFirstName = usrFirstName;
	}

	public void setUsrFirstName(String usrFirstName) {
		this.usrFirstName.add(usrFirstName);
	}

	public ArrayList getUsrMidName() {
		return usrMidName;
	}


	public void setUsrMidName(ArrayList usrMidName) {
		this.usrMidName = usrMidName;
	}
	public void setUsrMidName(String usrMidName) {
		this.usrMidName.add(usrMidName);
	}


	public ArrayList getUsrLastName() {
		return usrLastName;
	}


	public void setUsrLastName(ArrayList usrLastName) {
		this.usrLastName = usrLastName;
	}
	public void setUsrLastName(String usrLastName) {
		this.usrLastName.add(usrLastName);
	}
	public ArrayList getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(ArrayList createdOn) {
		this.createdOn = createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn.add(createdOn);
	}
	public ArrayList getPkBulkUpload() {
		return pkBulkUpload;
	}

	public void setPkBulkUpload(ArrayList pkBulkUpload) {
		this.pkBulkUpload = pkBulkUpload;
	}
	public void setPkBulkUpload(String pkBulkUpload) {
		this.pkBulkUpload.add(pkBulkUpload);
	}


	
	public ArrayList getFkBulkTamplate() {
		return fkBulkTamplate;
	}


	public void setFkBulkTamplate(ArrayList fkBulkTamplate) {
		this.fkBulkTamplate = fkBulkTamplate;
	}
	public void setFkBulkTamplate(String fkBulkTamplate) {
		this.fkBulkTamplate.add(fkBulkTamplate);
	}


	public ArrayList getFileName() {
		return fileName;
	}


	public void setFileName(ArrayList fileName) {
		this.fileName = fileName;
	}
	public void setFileName(String fileName) {
		this.fileName.add(fileName);
	}

	public ArrayList getTotRecord() {
		return totRecord;
	}


	public void setTotRecord(ArrayList totRecord) {
		this.totRecord = totRecord;
	}
	public void setTotRecord(String totRecord) {
		this.totRecord.add(totRecord);
	}


	public ArrayList getSucRecord() {
		return sucRecord;
	}


	public void setSucRecord(ArrayList sucRecord) {
		this.sucRecord = sucRecord;
	}

	public void setSucRecord(String sucRecord) {
		this.sucRecord.add(sucRecord);
	}
	public ArrayList getUnsucRecord() {
		return unsucRecord;
	}


	public void setUnsucRecord(ArrayList unsucRecord) {
		this.unsucRecord = unsucRecord;
	}
	public void setUnsucRecord(String unsucRecord) {
		this.unsucRecord.add(unsucRecord);
	}
	public void getUploadHistory(String fkUser)
	{
		PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("SELECT u.pk_bulk_upload,u.fk_bulk_template_master,u.file_name,u.total_records,u.successful_records,u.unsuccessful_records,u.created_on,n.usr_firstname,n.usr_midname,n.usr_lastname from er_bulk_upload u,er_user n where  u.fk_user=? and u.fk_user=n.pk_user order by u.pk_bulk_upload desc");
            pstmt.setString(1, fkUser);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setPkBulkUpload(rs.getString("pk_bulk_upload"));
            	setFkBulkTamplate(rs.getString("fk_bulk_template_master"));
            	setFileName(rs.getString("file_name"));
            	setTotRecord(rs.getString("total_records"));
            	setSucRecord(rs.getString("successful_records"));
            	setUnsucRecord(rs.getString("unsuccessful_records"));
            	setCreatedOn(rs.getString("created_on"));
            	setUsrFirstName(rs.getString("usr_firstname"));
            	setUsrMidName(rs.getString("usr_midname"));
            	setUsrLastName(rs.getString("usr_lastname"));
            }

       } catch (SQLException ex) {
            Rlog.fatal("bulkUploadDao",
                    "bulkUploadDao.getUploadHistory EXCEPTION IN FETCHING FROM ER_BULK_UPLOAD table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
	}
	public void getUploadLogs(String fkUser,int pkbulk)
	{
		PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("select l.ERROR_MESSAGE,R.FILE_ROW_NUMBER from er_bulk_upload m,ER_BULK_UPLOAD_ERRORLOG l,ER_BULK_UPLOAD_ERRORROW r where R.FK_BULK_UPLOAD=M.PK_BULK_UPLOAD and L.FK_BULK_UPLOAD_ERRORROW=R.PK_BULK_UPLOAD_ERRORROW and M.PK_BULK_UPLOAD=? and M.FK_USER=? order by R.FILE_ROW_NUMBER");
            pstmt.setInt(1, pkbulk);
            pstmt.setString(2,fkUser);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	setErrorMsg(rs.getString("error_message"));
            	setRowNum(rs.getString("file_row_number"));
            }

       } catch (SQLException ex) {
            Rlog.fatal("bulkUploadDao",
                    "bulkUploadDao.getUploadLogs EXCEPTION IN FETCHING FROM ER_BULK_UPLOAD table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
	}
}
