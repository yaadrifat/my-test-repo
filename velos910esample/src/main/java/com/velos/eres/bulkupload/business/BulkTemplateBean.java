package com.velos.eres.bulkupload.business;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.velos.eres.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;
@Entity
@Table(name = "ER_BULK_TEMPLATE_MASTER")
@NamedQuery(name = "findByTemplateNameIdentifier", query = "SELECT OBJECT(template) FROM BulkTemplateBean template where FK_USER = :fkUser AND UPPER(trim(TEMPLATE_NAME)) = UPPER(trim(:templateName))")
public class BulkTemplateBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  int pkTemplate;
	private  Integer fkBulkEntity;
    private  String templateName;
    private  Integer fkUser;
    private  String ipAdd;
    private  String creator;
    
    @Column(name = "creator")
    public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_bulk_template_master", allocationSize=1)
    @Column(name = "PK_BULK_TEMPLATE_MASTER")
    public int getPkTemplate() {
		return this.pkTemplate;
	}
	public void setPkTemplate(int pkTemplate) {
		this.pkTemplate = pkTemplate;
	}
	@Column(name = "FK_BULK_ENTITY_MASTER")
	public String getFkBulkEntity() {
		return StringUtil.integerToString(this.fkBulkEntity);
	}
	public void setFkBulkEntity(String fkBulkEntity) {
		this.fkBulkEntity = StringUtil.stringToInteger(fkBulkEntity);
	}
	@Column(name = "TEMPLATE_NAME")
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	@Column(name = "FK_USER")
	public String getFkUser() {
		return StringUtil.integerToString(this.fkUser);
	}
	public void setFkUser(String fkUser) {
		this.fkUser = StringUtil.stringToInteger(fkUser);
	}
	@Column(name = "IP_ADD")
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
	 public BulkTemplateBean() {
	    }
	 public BulkTemplateBean( int  pkTemplate,String fkBulkEntity,String templateName,String fkUser,String creator,String ipAdd) {
		 		setPkTemplate(pkTemplate);
		 		setFkBulkEntity(fkBulkEntity);
		 		setTemplateName(templateName);
		 		setFkUser(fkUser);
		 		setIpAdd(ipAdd);
		 		setCreator(creator);
	}
		    public int updateBulktemplate(BulkTemplateBean spsk) {
		        try {
		        	setPkTemplate(spsk.getPkTemplate());
		        	setFkBulkEntity(spsk.getFkBulkEntity());
		        	setTemplateName(spsk.getTemplateName());
		        	setFkUser(spsk.getFkUser());
		        	setIpAdd(spsk.getIpAdd());
		        	setCreator(spsk.getCreator());
		            return spsk.getPkTemplate();
		        } catch (Exception e) {
		            Rlog.fatal("bulkuploadtempalte", " error in BulkTemplateBean.updateBulktemplate" + e);
		            e.printStackTrace();
		            return -2;
		        }
		    }
}
