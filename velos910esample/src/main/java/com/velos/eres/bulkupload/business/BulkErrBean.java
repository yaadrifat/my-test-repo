package com.velos.eres.bulkupload.business;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
@Entity
@Table(name = "ER_BULK_UPLOAD_ERRORROW")
public class BulkErrBean implements Serializable{
	private  int pkErr;
    private  String fkBulkUpload;
    private  String fileRowNum;
    private  String fileHeaderRowData;
    private  String fileErrRowData;
    private  String creator;
    private  String ipAdd;
	@Column(name = "IP_ADD")
	public String getIpAdd() {
		return ipAdd;
	}
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
    @Column(name = "creator")
    public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_bulk_upload", allocationSize=1)
    @Column(name = "PK_BULK_UPLOAD_ERRORROW")
	public int getPkErr() {
		return this.pkErr;
	}
	public void setPkErr(int pkErr) {
		this.pkErr = pkErr;
	}
	 @Column(name = "FK_BULK_UPLOAD")
	public String getFkBulkUpload() {
		return this.fkBulkUpload;
	}
	public void setFkBulkUpload(String fkBulkUpload) {
		this.fkBulkUpload = fkBulkUpload;
	}
	@Column(name = "FILE_ROW_NUMBER")
	public String getFileRowNum() {
		return this.fileRowNum;
	}
	public void setFileRowNum(String fileRowNum) {
		this.fileRowNum = fileRowNum;
	}
	@Column(name = "FILE_HEADER_ROWDATA")
	public String getFileHeaderRowData() {
		return this.fileHeaderRowData;
	}
	public void setFileHeaderRowData(String fileHeaderRowData) {
		this.fileHeaderRowData = fileHeaderRowData;
	}
	@Column(name = "FILE_ERROR_ROWDATA")
	public String getFileErrRowData() {
		return this.fileErrRowData;
	}
	public void setFileErrRowData(String fileErrRowData) {
		this.fileErrRowData = fileErrRowData;
	}
    public BulkErrBean() {

    }
	 public BulkErrBean( int  pkErr,String fkBulkUpload,String fileRowNum,String fileHeaderRowData,String fileErrRowData,String Creator,String ipAdd) {
		 	setPkErr(pkErr);
		 	setFkBulkUpload(fkBulkUpload);
		 	setFileRowNum(fileRowNum);
		 	setFileHeaderRowData(fileHeaderRowData);
		 	setFileErrRowData(fileErrRowData);
		 	setCreator(Creator);
		 	setIpAdd(ipAdd);
}
	    public int updateBulkErr(BulkErrBean spsk) {
	        try {
	        	setPkErr(spsk.getPkErr());
	        	setFkBulkUpload(spsk.getFkBulkUpload());
	        	setFileRowNum(spsk.getFileRowNum());
	        	setFileHeaderRowData(spsk.getFileHeaderRowData());
	        	setFileErrRowData(spsk.getFileErrRowData());
	        	setCreator(spsk.getCreator());
			 	setIpAdd(spsk.getIpAdd());
	            return spsk.getPkErr();
	        } catch (Exception e) {
	            Rlog.fatal("bulkupload", " error in BulkErrBean.updateBulkErr" + e);
	            e.printStackTrace();
	            return -2;
	        }
	    }
}
