package com.velos.eres.bulkupload.service;
import javax.ejb.Remote;
import com.velos.eres.bulkupload.business.BulkTemplateDetailBean;
@Remote
public interface BulkTempateDetailAgent {
 	public BulkTemplateDetailBean getBulkTemplateDetDetails(int pkSpec);    
    public int setBulkTemplateDetDetails(BulkTemplateDetailBean specimen);    
    public int updateBulktemplateDet(BulkTemplateDetailBean specimen);
}
