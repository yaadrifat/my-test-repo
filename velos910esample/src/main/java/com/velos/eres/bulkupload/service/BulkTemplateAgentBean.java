package com.velos.eres.bulkupload.service;
import java.util.ArrayList;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.bulkupload.business.BulkTemplateBean;
import com.velos.eres.service.util.Rlog;
@Stateless
@Remote( { BulkTemplateAgent.class } )
public class BulkTemplateAgentBean implements BulkTemplateAgent{
	  @PersistenceContext(unitName = "eres")
	  protected EntityManager em;
	  public BulkTemplateBean getBulkTemplateDetails(int pkSpec) {
	        if (pkSpec == 0) {
	            return null;
	        } 
	        BulkTemplateBean spec = null;
	        try {
	        	spec = (BulkTemplateBean) em.find(BulkTemplateBean .class, new Integer(pkSpec));	            
	        } catch (Exception e) {
	            Rlog.fatal("bulktemplate",
	                    "Error in getBulkTemplateDetails() in BulkTemplateAgentBean" + e);
	        }
	        return spec;
	    }
	
	  public int setBulkTemplateDetails(BulkTemplateBean spsk) {    
	        try {
	        	BulkTemplateBean  spec = new BulkTemplateBean(); 
	        	int rowfound = 0;
	        	try {

	                Query queryIdentifier = em.createNamedQuery("findByTemplateNameIdentifier");
	                queryIdentifier.setParameter("templateName", (spsk.getTemplateName()).toUpperCase());
	                //JM: 19Sep2007:
	               
	                queryIdentifier.setParameter("fkUser", spsk.getFkUser());
	                 
	                ArrayList list = (ArrayList) queryIdentifier.getResultList();
	                if (list == null) list = new ArrayList();
	                if (list.size() > 0) {
	                    rowfound = 1;
	                    return -3; // template name exists
	                }

	            } catch (Exception ex) {
	                Rlog.fatal("specimen", "In findByTemplateNameIdentifier call to find template name exception" + ex);
	                rowfound = 0; // template name does not exist
	            }
	        	spec.updateBulktemplate(spsk);        	 
	            em.persist(spec);
	            return spec.getPkTemplate();
	        } catch (Exception e) {
	        	  Rlog.fatal("bulktemplate",  "Error in setBulkTemplateDetails() in BulkTemplateAgentBean" + e);
	        }
	        return 0;
	    }
	    public int updateBulkTemplate(BulkTemplateBean spsk) {
	    	BulkTemplateBean appSpec= null;
	        int output;
	        try {	            
	            output = appSpec.updateBulktemplate(spsk);            
	            em.merge(appSpec);
	        } catch (Exception e) {
	        	Rlog.fatal("bulktemplate",  "Error in updateBulkTemplate() in BulkTemplateAgentBean" + e);
	            return -2;
	        }
	        return output;
	    }
}
