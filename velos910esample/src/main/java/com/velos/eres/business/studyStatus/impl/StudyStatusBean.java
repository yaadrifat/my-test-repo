/* * Classname			StudyStatusBean.class
 * 
 * Version information
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */

package com.velos.eres.business.studyStatus.impl;

/**
 * 
 */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "er_studystat")
@NamedQueries( {
        @NamedQuery(name = "findByPreviousCurrentStatusProt", query = "SELECT OBJECT(stat) FROM StudyStatusBean stat  where to_number(stat.statStudy) = :fk_study and  stat.statEndDate  is null and trunc(stat.statStartDate) <= to_date(:begindate,:date_format) and stat.id <> :pk_studystat "),
        @NamedQuery(name = "findByIsCurrentFlag", query = "SELECT OBJECT(stat) FROM StudyStatusBean stat  where to_number(stat.statStudy) = :FK_STUDY and  stat.currentStat =:IS_CURRENT_FLAG "),
        @NamedQuery(name = "findByNextStatusProt", query = "SELECT OBJECT(stat) FROM StudyStatusBean stat "
                + " where stat.id = (select max(i.id) from StudyStatusBean i where "
                + " to_number(i.statStudy) = :fk_study and trunc(i.statStartDate) =  "
                + " (select min (trunc(ii.statStartDate)) from StudyStatusBean ii where to_number(ii.statStudy) = :fk_study and  trunc(ii.statStartDate) >= to_date(:begindate,:date_format) and "
                + " ii.id <> :pk_studystat ) and  i.id <> :pk_studystat ) "),
        @NamedQuery(name = "findByPreviousStatusProt", query = "SELECT OBJECT(stat) FROM StudyStatusBean stat "
                + " where stat.id = (select max(i.id) from StudyStatusBean i where "
                + "	to_number(i.statStudy) = :fk_study and trunc(i.statStartDate) = "
                + "	(select max (trunc(ii.statStartDate)) from StudyStatusBean ii where to_number(ii.statStudy) = :fk_study and  trunc(ii.statStartDate) <= to_date(:begindate,:date_format) and "
                + " ii.id <> :pk_studystat ) and  i.id <> :pk_studystat )"),
        @NamedQuery(name = "findByLatestStatusProt", query = "SELECT OBJECT(stat) FROM StudyStatusBean stat "
                + " where stat.id = (select max(i.id) from StudyStatusBean i where "
                + " to_number(i.statStudy) = :fk_study and trunc(i.statStartDate) = "
                + " (select max (trunc(ii.statStartDate)) from StudyStatusBean ii where to_number(ii.statStudy) = :fk_study ))"),
        @NamedQuery(name = "findByAtLeastOneStatusProt", query = "SELECT OBJECT(stat) FROM StudyStatusBean stat where to_number(stat.statStudy) = :fk_study and FK_CODELST_STUDYSTAT = :pk_codelst and rownum < 2 ") })
public class StudyStatusBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3617297804989378608L;

    /**
     * the Study Status Id
     */
    public int id;

    /**
     * the User who documented the status
     */
    public Integer statDocUser;

    /**
     * study status
     */
    public Integer studyStatus;

    /**
     * the Study id
     */

    public Integer statStudy;

    /**
     * the status start date
     */

    public Date statStartDate;

    /**
     * the HSPN number
     */
    public String statHSPN;

    /**
     * the associated Notes
     */
    public String statNotes;

    /**
     * the status end date
     */

    public Date statEndDate;

    /**
     * the status valid unit date
     */

    public Date statValidDate;
        
    /**
     * the Approval Status
     */

    public Integer statApproval;

    /**
     * the Approval Renewal Number
     */

    public Integer statAppRenNo;

    /**
     * site Id
     */
    public Integer siteId;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;
    
    /**
     * the Current Status
     */
    public String currentStat;//KM
    
    //Added By Amarnadh
    
    /**
     * This field represent meeting date
     */
    public Date statMeetingDate;
    
    /**
     * This field represent study status outcome
     */
    public Integer studyStatusOutcome;
    
    /**
     * This field represent study status type
     */
    public Integer studyStatusType;
    
    /**
     * the review board
     */
    public String studyStatusReviewBoard; 
    
    /**
     * the user tstaus assigned to
     */
    public String studyStatusAssignedTo; 
    
    

    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYSTAT", allocationSize=1)
    @Column(name = "PK_STUDYSTAT")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "fk_codelst_aprstat")
    public String getStatApproval() {
        return StringUtil.integerToString(this.statApproval);

    }

    public void setStatApproval(String statApproval) {
        if (statApproval != null) {
            this.statApproval = Integer.valueOf(statApproval);
        } else {
            this.statApproval = null;
        }
    }

    @Column(name = "fk_user_docby")
    public String getStatDocUser() {

        return StringUtil.integerToString(this.statDocUser);

    }

    public void setStatDocUser(String statDocUser) {
        Rlog.debug("studystatus", "statdocuser *" + statDocUser + "*");
        this.statDocUser = StringUtil.stringToInteger(statDocUser);

    }

    /*
     * public Date getStatEndDt() { return this.statEndDate; }
     * 
     * public void setStatEndDt(Date statEndDate) { this.statEndDate =
     * statEndDate; }
     */

    // @Transient
    @Column(name = "STUDYSTAT_ENDT")
    public Date getStatEndDate() {
        // return DateUtil.dateToString(getStatEndDt());
        return this.statEndDate;

    }

    public void setStatEndDate(Date statEndDate) {
         
        this.statEndDate = statEndDate;

    }

    public String returnStatEndDate() {
        // return DateUtil.dateToString(getStatEndDt());
        return DateUtil.dateToString(this.statEndDate);

    }

    public void updateStatEndDate(String statEndDate) {
         
        this.statEndDate = DateUtil.stringToDate(statEndDate, null);

    }

    @Column(name = "studystat_validt")
    public Date getStatValidDt() {

        return this.statValidDate;
    }

    public void setStatValidDt(Date statValidDate) {

        this.statValidDate = statValidDate;

    }

    @Transient
    public String getStatValidDate() {

        return DateUtil.dateToString(getStatValidDt());
    }

    public void setStatValidDate(String statValidDate) {

        DateUtil bDate = null;
        setStatValidDt(DateUtil.stringToDate(statValidDate, null));

    }
    
    @Column(name = "studystat_hspn")
    public String getStatHSPN() {
        return this.statHSPN;
    }

    public void setStatHSPN(String statHSPN) {
        this.statHSPN = statHSPN;
    }

    @Column(name = "studystat_note")
    public String getStatNotes() {
        return this.statNotes;
    }

    public void setStatNotes(String statNotes) {
        this.statNotes = statNotes;
    }

    /*
     * public Date getStatStartDt() { return this.statStartDate; }
     * 
     * public void setStatStartDt(Date statStartDate) {
     * 
     * this.statStartDate = statStartDate; }
     */

    // @Transient
    @Column(name = "studystat_date")
    public Date getStatStartDate() {
        // return DateUtil.dateToString(getStatStartDt());
        return this.statStartDate;

    }

    public void setStatStartDate(Date statStartDate) {
 
        this.statStartDate = statStartDate;

    }

    public String returnStatStartDate() {
        // return DateUtil.dateToString(getStatStartDt());
        return DateUtil.dateToString(this.statStartDate);

    }

    public void updateStatStartDate(String statStartDate) {

       
        this.statStartDate = DateUtil.stringToDate(statStartDate, null);

    }

    @Column(name = "fk_study")
    public String getStatStudy() {
        return StringUtil.integerToString(this.statStudy);

    }

    public void setStatStudy(String statStudy) {
        this.statStudy = StringUtil.stringToInteger(statStudy);
    }

    @Column(name = "fk_codelst_studystat")
    public String getStudyStatus() {
        return StringUtil.integerToString(this.studyStatus);

    }

    public void setStudyStatus(String studyStatus) {
        this.studyStatus = StringUtil.stringToInteger(studyStatus);
    }

    @Column(name = "fk_codelst_aprno")
    public String getStatAppRenNo() {
        return StringUtil.integerToString(this.statAppRenNo);
    }

    public void setStatAppRenNo(String statAppRenNo) {
        Rlog.debug("studystatus", "statAppRenNo *" + statAppRenNo + "*");
        if (statAppRenNo != null) {
            this.statAppRenNo = Integer.valueOf(statAppRenNo);
        } else {
            this.statAppRenNo = null;
        }

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "fk_site")
    public String getSiteId() {
        return StringUtil.integerToString(this.siteId);

    }

    public void setSiteId(String siteId) {
        this.siteId = StringUtil.stringToInteger(siteId);
    }
    
    //Added by Manimaran for the September Enhancement(S8)
    @Column(name="current_stat")
    public String getCurrentStat() {
    	return this.currentStat;
    }
    
    public void setCurrentStat(String currentStat) {
    	this.currentStat = currentStat;
    }
    
// Added by Amarnadh 
    
    @Column(name = "studystat_meetdt")
    public Date getStatMeetingDt() {

        return this.statMeetingDate;
    }

    public void setStatMeetingDt(Date statMeetingDate) {

        this.statMeetingDate = statMeetingDate;

    }

    @Transient
    public String getStatMeetingDate() {

        return DateUtil.dateToString(getStatMeetingDt());
    }

    public void setStatMeetingDate(String statMeetingDate) {

        DateUtil bDate = null;
        setStatMeetingDt(DateUtil.stringToDate(statMeetingDate,null));

    }
       
    @Column(name = "OUTCOME")
    public String getStudyStatusOutcome() {
        return StringUtil.integerToString(this.studyStatusOutcome);

    }

    public void setStudyStatusOutcome(String studyStatusOutcome) {
        this.studyStatusOutcome = StringUtil.stringToInteger(studyStatusOutcome);
    }
    
    @Column(name = "STATUS_TYPE")
    public String getStudyStatusType() {
        return StringUtil.integerToString(this.studyStatusType);

    }

    public void setStudyStatusType(String studyStatusType) {
        this.studyStatusType = StringUtil.stringToInteger(studyStatusType);
    }
    
   // END OF GETTER SETTER METHODS

     /**
     * sets up a state holder containing details of the study status
     */
 

    // to update study status
    public int updateStudyStatus(StudyStatusBean ssk) {
        Rlog.debug("studystatus","IN ENTITY BEAN - UPDATE STATUS STATE HOLDER");

        try {

             
            setStatDocUser(ssk.getStatDocUser());
            setStatApproval(ssk.getStatApproval());
            setStatHSPN(ssk.getStatHSPN());
            setStatNotes(ssk.getStatNotes());
            updateStatStartDate(ssk.returnStatStartDate());
            setStatValidDate(ssk.getStatValidDate());
            updateStatEndDate(ssk.returnStatEndDate());
            setStatStudy(ssk.getStatStudy());
            setStudyStatus(ssk.getStudyStatus());
            setStatAppRenNo(ssk.getStatAppRenNo());
            setSiteId(ssk.getSiteId());
            setCreator(ssk.getCreator());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            setCurrentStat(ssk.getCurrentStat());//KM
            setStatMeetingDate(ssk.getStatMeetingDate());  //Amar
            setStudyStatusOutcome(ssk.getStudyStatusOutcome());//Amar
            setStudyStatusType(ssk.getStudyStatusType());//Amar
            
            setStudyStatusAssignedTo(ssk.getStudyStatusAssignedTo());
            setStudyStatusReviewBoard(ssk.getStudyStatusReviewBoard());
            
            Rlog.debug("studystatus", "saved");
            return 0;
        } catch (Exception e) {
            // System.out.println("EXCEPTION " + e);
            Rlog.fatal("studystatus", "EXCEPTION IN CREATING NEW STUDY STATUS "
                    + e);
            return -2;
        }
    }

    public StudyStatusBean() {

    }

    public StudyStatusBean(int id, String statDocUser, String studyStatus,
            String statStudy, String statStartDate, String statHSPN,
            String statNotes, String statEndDate, String statValidDate, String statMeetingDate,String studyStatusOutcome,
            String studyStatusType,String statApproval, String statAppRenNo, String siteId,
            String creator, String modifiedBy, String ipAdd, String currentStat, String assignedTo,String revboard) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setStatDocUser(statDocUser);
        setStudyStatus(studyStatus);
        setStatStudy(statStudy);
        updateStatStartDate(statStartDate);
        setStatHSPN(statHSPN);
        setStatNotes(statNotes);
        updateStatEndDate(statEndDate);
        setStatValidDate(statValidDate);
        setStatMeetingDate(statMeetingDate);  
        setStudyStatusOutcome(studyStatusOutcome);
        setStudyStatusType(studyStatusType);
        setStatApproval(statApproval);
        setStatAppRenNo(statAppRenNo);
        setSiteId(siteId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setCurrentStat(currentStat);
        setStudyStatusAssignedTo(assignedTo);
        setStudyStatusReviewBoard(revboard);
        
    }

    @Column(name = "fk_codelst_revboard")
	public String getStudyStatusReviewBoard() {
		return studyStatusReviewBoard;
	}

	public void setStudyStatusReviewBoard(String studyStatusReviewBoard) {
		this.studyStatusReviewBoard = studyStatusReviewBoard;
	}

	@Column(name = "studystat_assignedto")
	public String getStudyStatusAssignedTo() {
		return studyStatusAssignedTo;
	}

	public void setStudyStatusAssignedTo(String studyStatusAssignedTo) {
		this.studyStatusAssignedTo = studyStatusAssignedTo;
	}

}// end of class

