/*
 * Classname			EschReportsDao
 * 
 * Version information 	1.0
 *
 * Date					07/14/2001
 * 
 * Copyright notice		Velos, Inc.
 *
 * Author 				dinesh
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * EschReportsDao for fetching EschReports data from the database for various
 * eschreports
 * 
 * @author Dinesh
 * @version : 1.0 05/12/2001
 */

public class EschReportsDao extends CommonDAO implements java.io.Serializable {

    /*
     * CLASS METHODS
     * 
     * 
     * END OF CLASS METHODS
     */

    /*
     * Arraylist to save Event's Id
     */
    private ArrayList eventIds;

    /*
     * Arraylist to save Event's Name
     */
    private ArrayList names;

    /*
     * Arraylist to save Event's duration
     */
    private ArrayList durations;

    /*
     * Arraylist to save fuzzy periods
     */
    private ArrayList fuzzyPeriods;

    /*
     * Arraylist to save displacements
     */
    private ArrayList displacements;

    /*
     * Arraylist to save costs
     */
    private ArrayList costs;

    /*
     * Arraylist to save docNames
     */
    private ArrayList docNames;

    /*
     * Arraylist to save docNames
     */
    private ArrayList users;

    /*
     * Arraylist to save Resources
     */
    private ArrayList resources;

    private String protName;

    private String protDuration;

    private int cRows;

    /**
     * Defines a EschReportsDao object with empty array lists
     */
    public EschReportsDao() {
        eventIds = new ArrayList();
        names = new ArrayList();
        durations = new ArrayList();
        fuzzyPeriods = new ArrayList();
        displacements = new ArrayList();
        costs = new ArrayList();
        docNames = new ArrayList();
        users = new ArrayList();
        durations = new ArrayList();
        resources = new ArrayList();
        protName = new String();
        protDuration = new String();
    }

    // Getter and Setter methods

    /**
     * Returns an Arraylist containing String objects with Event Ids
     * 
     * @return an Arraylist containing the String objects with Event Names
     */

    public ArrayList getEventIds() {
        return this.eventIds;
    }

    /**
     * Returns an Arraylist containing String objects with Event Ids
     * 
     * @return an Arraylist containing the String objects with Event Names
     */

    public void setEventIds(ArrayList eventIds) {
        this.eventIds = eventIds;
    }

    /**
     * Returns an Arraylist containing String objects with Event Names
     * 
     * @return an Arraylist containing the String objects with Event Names
     */

    public ArrayList getNames() {
        return this.names;
    }

    /**
     * Sets the Arraylist containing the Event Names with the ArrayList passed
     * as the parameter
     * 
     * @param userLastNames
     *            An ArrayList with String objects having Event Names
     */

    public void setNames(ArrayList names) {
        this.names = names;
    }

    /**
     * Returns an Arraylist containing String objects with Event durations
     * 
     * @return an Arraylist containing the String objects with Event durations
     */

    public ArrayList getDurations() {
        return this.durations;
    }

    /**
     * Sets the Arraylist containing the durations with the ArrayList passed as
     * the parameter
     * 
     * @param userLastNames
     *            An ArrayList with String objects having Event durations
     */
    public void setDurations(ArrayList durations) {
        this.durations = durations;
    }

    /**
     * Returns an Arraylist containing String objects with Event fuzzyPeriods
     * 
     * @return an Arraylist containing the String objects with Event
     *         fuzzyPeriods
     */

    public ArrayList getFuzzyPeriods() {
        return this.fuzzyPeriods;
    }

    /**
     * Sets the Arraylist containing the Event fuzzyPeriods with the ArrayList
     * passed as the parameter
     * 
     * @param userLastNames
     *            An ArrayList with String objects having Event fuzzyPeriods
     */
    public void setFuzzyPeriods(ArrayList fuzzyPeriods) {
        this.fuzzyPeriods = fuzzyPeriods;
    }

    /**
     * Returns an Arraylist containing String objects with Event displacements
     * 
     * @return an Arraylist containing the String objects with Event
     *         displacements
     */

    public ArrayList getDisplacements() {
        return this.displacements;
    }

    /**
     * Sets the Arraylist containing the Event displacements with the ArrayList
     * passed as the parameter
     * 
     * @param userLastNames
     *            An ArrayList with String objects having Event displacements
     */

    public void setDisplacements(ArrayList displacements) {
        this.displacements = displacements;
    }

    /**
     * Returns an Arraylist containing String objects with Event displacements
     * 
     * @return an Arraylist containing the String objects with Event
     *         displacements
     */

    public ArrayList getCosts() {
        return this.costs;
    }

    /**
     * Sets the Arraylist containing the Event fuzzyPeriods with the ArrayList
     * passed as the parameter
     * 
     * @param userLastNames
     *            An ArrayList with String objects having Event fuzzyPeriods
     */

    public void setCosts(ArrayList costs) {
        this.costs = costs;
    }

    /**
     * Returns an Arraylist containing String objects with Event useruments
     * 
     * @return an Arraylist containing the String objects with Event documents
     */

    public ArrayList getDocNames() {
        return this.docNames;
    }

    /**
     * Sets the Arraylist containing the Event fuzzyPeriods with the ArrayList
     * passed as the parameter
     * 
     * @param userLastNames
     *            An ArrayList with String objects having Event document
     */

    public void setDocNames(ArrayList docNames) {
        this.docNames = docNames;
    }

    public ArrayList getUsers() {
        return this.users;
    }

    public void setUsers(ArrayList users) {
        this.users = users;
    }

    public ArrayList getResources() {
        return this.resources;
    }

    public void setResources(ArrayList resources) {
        this.resources = resources;
    }

    public String getProtName() {
        return this.protName;
    }

    public void setProtName(String protName) {
        this.protName = protName;
    }

    public String getProtDuration() {
        return this.protDuration;
    }

    public void setProtDuration(String protDuration) {
        this.protDuration = protDuration;
    }

    /**
     * Sets the number of elements in the ArrayLists
     * 
     * @param cRows
     *            the number of elements in the ArrayLists
     */
    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * Adds the object to the Arraylist containing Event Id
     * 
     * @param name
     *            A String object that needs to be added to the Arraylist
     *            containing Event Id
     */
    public void setEventIds(String eventId) {
        eventIds.add(eventId);
    }

    /**
     * Adds the object to the Arraylist containing Event name
     * 
     * @param name
     *            A String object that needs to be added to the Arraylist
     *            containing Event name
     */
    public void setNames(String name) {
        names.add(name);
    }

    /**
     * Adds the object to the Arraylist containing Event duration
     * 
     * @param duration
     *            A String object that needs to be added to the Arraylist
     *            containing Event name
     */
    public void setDurations(String duration) {
        durations.add(duration);
    }

    /**
     * Adds the object to the Arraylist containing Event fuzzyPeriod
     * 
     * @param fuzzyPeriod
     *            A String object that needs to be added to the Arraylist
     *            containing Event name
     */
    public void setFuzzyPeriods(String fuzzyPeriod) {
        fuzzyPeriods.add(fuzzyPeriod);
    }

    /**
     * Adds the object to the Arraylist containing Event name
     * 
     * @param name
     *            A String object that needs to be added to the Arraylist
     *            containing Event name
     */
    public void setDisplacements(String displacement) {
        displacements.add(displacement);
    }

    /**
     * Adds the object to the Arraylist containing Event cost
     * 
     * @param name
     *            A String object that needs to be added to the Arraylist
     *            containing Event name
     */

    public void setCosts(String cost) {
        costs.add(cost);
    }

    /**
     * Adds the object to the Arraylist containing Event document
     * 
     * @param name
     *            A String object that needs to be added to the Arraylist
     *            containing Event document
     */

    public void setDocNames(String docName) {
        docNames.add(docName);
    }

    public void setUsers(String user) {
        users.add(user);
    }

    public void setResources(String resource) {
        resources.add(resource);
    }

    // end of getter and setter methods

    /**
     * Fetches the data for the eschreports
     * 
     * @param
     * 
     */
    public void fetchEschReportsData(int reportNumber, String filterType,
            String year, String month, String dateFrom, String dateTo,
            String id, String accId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String sql = null;
        Rlog.debug("eschreports", "line 1");
        try {
            conn = getConnection();
            Rlog.debug("eschreports", "line 2");
            String mysql = "select event_id,name, " + "duration, "
                    + "fuzzy_period, " + "displacement "
                    + "from event_assoc " + "where chain_id = ? "
                    + "order by displacement,org_id";
            Rlog.debug("eschreports", "value is" + mysql);

            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("eschreports", "line 3");
            pstmt.setInt(1, StringUtil.stringToNum(id));

            rs = pstmt.executeQuery();
            Rlog.debug("eschreports", "line 4");
            Rlog.debug("eschreports", "EschReportsDao.fetchEschReportsData  ");
            while (rs.next()) {
                Rlog.debug("eschreports", "inside loop");
                setEventIds(rs.getString("event_id"));
                setNames(rs.getString("name"));
                setDurations(rs.getString("duration"));
                setFuzzyPeriods(rs.getString("fuzzy_period"));
                setDisplacements(rs.getString("displacement"));
            }

            Rlog.debug("eschreports", "line 5");
            // ////////////////retrieving the costs
            String costeventId = "";
            String costValue = "";
            String costeventbakId = "";
            String costValueBak = "";
            HashMap eventcost = new HashMap();
            Rlog.debug("eschreports", "line 6");

            /*
             * mysql= "select a.eventcost_value as cost, " + "a.fk_event as
             * costeventId " + "from sch_eventcost a,event_assoc
             * b" + " where b.chain_id= ? and " + "b.event_id=a.fk_event order
             * by b.displacement,b.org_id";
             */

            mysql = "select substr(c.codelst_desc,1,10)||' '|| a.eventcost_value   as cost , "
                    + " a.fk_event as costeventId "
                    + " from sch_eventcost a,event_assoc b ,sch_codelst c "
                    + " where b.chain_id= ? "
                    + " and a.FK_CURRENCY=c.PK_CODELST and "
                    + " b.event_id=a.fk_event order by b.displacement,b.org_id ";

            Rlog.debug("eschreports", "second sql is " + mysql);

            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("eschreports", "line 7");

            pstmt.setInt(1, StringUtil.stringToNum(id));

            rs = pstmt.executeQuery();
            Rlog.debug("eschreports", "EschReportsDao.fetchEschReportsData  ");
            int count = 0;
            Rlog.debug("eschreports", "line 8");
            while (rs.next()) {
                Rlog.debug("eschreports", "line 9");
                costValue = rs.getString("cost");
                costeventId = rs.getString("costeventId");

                if (count == 0) {
                    costValueBak = costValueBak + costValue + ",";
                    costeventbakId = costeventId;
                    Rlog.debug("eschreports", "line 10");
                    count++;
                    continue;
                }

                if (costeventId.equals(costeventbakId)) {
                    costValueBak = costValueBak + costValue + ",";
                    Rlog.debug("eschreports", "line 11");
                } else {
                    costValueBak = costValueBak.substring(0, (costValueBak
                            .length() - 1));
                    eventcost.put(costeventbakId, costValueBak);
                    costeventbakId = costeventId;
                    costValueBak = costValue + ",";
                    Rlog.debug("eschreports", "line 12");
                }

            }

            if (costValueBak != "") {
                costValueBak = costValueBak.substring(0,
                        (costValueBak.length() - 1));
                eventcost.put(costeventbakId, costValueBak);
            }

            // for(int i=0;i<eventIds.size();i++)
            // {
            // String cost=(String) eventcost.get((eventIds.get(i)));
            // if (cost=="null")
            // {
            // setCosts("");
            // }
            // else
            // {
            // setCosts(cost);
            // }
            // }

            Rlog.debug("eschreports", "line 9");

            // ////////////////////////////////////////////////retrieving the
            // docs
            String doceventId = "";
            String docValue = "";
            String doceventbakId = "";
            String docValueBak = "";
            HashMap eventdoc = new HashMap();
            Rlog.debug("eschreports", "line 6");

            mysql = "select a.doc_name as docname, "
                    + "c.event_id as doceventId"
                    + " from sch_docs a, sch_eventdoc b,event_assoc c "
                    + "where c.chain_id= ? and "
                    + "a.pk_docs=b.pk_docs and b.fk_event=c.event_id order by c.displacement,c.org_id";

            Rlog.debug("eschreports", "second sql is " + mysql);

            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("eschreports", "line 7");

            pstmt.setInt(1, StringUtil.stringToNum(id));

            rs = pstmt.executeQuery();
            Rlog.debug("eschreports", "EschReportsDao.fetchEschReportsData  ");
            count = 0;
            Rlog.debug("eschreports", "line 8");
            while (rs.next()) {
                Rlog.debug("eschreports", "line 9");
                docValue = rs.getString("docname");
                doceventId = rs.getString("doceventId");

                if (count == 0) {
                    docValueBak = docValueBak + docValue;
                    doceventbakId = doceventId;
                    Rlog.debug("eschreports", "line 10");
                    count++;
                    continue;
                }

                if (doceventId.equals(doceventbakId)) {
                    docValueBak = docValueBak + ", " + docValue;
                    Rlog.debug("eschreports", "line 11");
                } else {
                    eventdoc.put(doceventbakId, docValueBak);
                    doceventbakId = doceventId;
                    docValueBak = docValue;
                    Rlog.debug("eschreports", "line 12");
                }

            }

            if (docValueBak != "") {
                eventdoc.put(doceventbakId, docValueBak);
            }

            // for(int i=0;i<eventIds.size();i++)
            // {
            // String doc=(String) eventdoc.get((eventIds.get(i)));
            // if (doc=="null")
            // {
            // setDocNames("");
            // }
            // else
            // {
            // setDocNames(doc);
            // }
            // }
            //	
            Rlog.debug("eschreports", "line 9");

            // ///////// ///////////////retrieving the users

            String usereventId = "";
            String userValue = "";
            String usereventbakId = "";
            String userValueBak = "";
            HashMap eventuser = new HashMap();
            Rlog.debug("eschreports", "line 6");

            mysql = "select a.usr_lastname||','||a.usr_firstname as username, "
                    + "c.event_id as usereventId "
                    + "from er_user a, sch_eventusr b,event_assoc c "
                    + "where c.chain_id=  "
                    + StringUtil.stringToNum(id)
                    + " and c.event_id=b.fk_event and b.eventusr= a.pk_user and b.eventusr_type='U' order by c.displacement,c.org_id";

            Rlog.debug("eschreports", "second sql is " + mysql);

            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("eschreports", "line 7");

            // pstmt.setInt(1,StringUtil.stringToNum(id));

            rs = pstmt.executeQuery();
            Rlog.debug("eschreports", "EschReportsDao.fetchEschReportsData  ");
            count = 0;
            Rlog.debug("eschreports", "line 8");
            while (rs.next()) {
                Rlog.debug("eschreports", "line 9");
                userValue = rs.getString("username");
                usereventId = rs.getString("usereventId");

                if (count == 0) {
                    userValueBak = userValueBak + userValue;
                    usereventbakId = usereventId;
                    Rlog.debug("eschreports", "line 10");
                    count++;
                    continue;
                }

                if (usereventId.equals(usereventbakId)) {
                    userValueBak = userValueBak + ", " + userValue;
                    Rlog.debug("eschreports", "line 11");
                } else {
                    eventuser.put(usereventbakId, userValueBak);
                    usereventbakId = usereventId;
                    userValueBak = userValue;
                    Rlog.debug("eschreports", "line 12");
                }

            }
            if (userValueBak != "") {
                eventuser.put(usereventbakId, userValueBak);
            }
            // for(int i=0;i<eventIds.size();i++)
            // {
            // String user=(String) eventuser.get((eventIds.get(i)));
            // if (user=="null")
            // {
            // setUsers("");
            // }
            // else
            // {
            // setUsers(user);
            // }
            // }

            Rlog.debug("eschreports", "line 9");

            // ///////// ///////////////retrieving the resources

            String reseventId = "";
            String resValue = "";
            String reseventbakId = "";
            String resValueBak = "";
            HashMap eventres = new HashMap();
            Rlog.debug("eschreports", "line 6");

            mysql = "select a.codelst_desc  as resname,"
                    + "c.event_id as reseventId "
                    + "from er_codelst a, sch_eventusr b,event_assoc c "
                    + " where c.chain_id= "
                    + StringUtil.stringToNum(id)
                    + " and  c.event_id=b.fk_event and b.eventusr= a.PK_CODELST and b.eventusr_type='R' order by c.displacement,c.org_id";

            Rlog.debug("eschreports", "second sql is " + mysql);

            pstmt = conn.prepareStatement(mysql);
            Rlog.debug("eschreports", "line 7");

            // pstmt.setInt(1,StringUtil.stringToNum(id));

            rs = pstmt.executeQuery();
            Rlog.debug("eschreports", "EschReportsDao.fetchEschReportsData  ");
            count = 0;
            Rlog.debug("eschreports", "line 8");
            while (rs.next()) {
                Rlog.debug("eschreports", "line 9");
                resValue = rs.getString("resname");
                reseventId = rs.getString("reseventId");

                if (count == 0) {
                    resValueBak = resValueBak + resValue;
                    reseventbakId = reseventId;
                    Rlog.debug("eschreports", "line 10");
                    count++;
                    continue;
                }

                if (reseventId.equals(reseventbakId)) {
                    resValueBak = resValueBak + ", " + resValue;
                    Rlog.debug("eschreports", "line 11");
                } else {
                    eventres.put(reseventbakId, resValueBak);
                    reseventbakId = reseventId;
                    resValueBak = resValue;
                    Rlog.debug("eschreports", "line 12");
                }

            }
            Rlog.debug("eschreports", "line 13");

            if (resValueBak != "") {
                eventres.put(reseventbakId, resValueBak);
            }
            Rlog.debug("eschreports", "line 14");
            for (int i = 0; i < eventIds.size(); i++) {
                Rlog.debug("eschreports", "line 15");

                // ////fetching docs

                String cost = (String) eventcost.get((eventIds.get(i)));
                if (cost == "null") {
                    setCosts("");
                } else {
                    setCosts(cost);
                }

                // ////fetching docs

                String doc = (String) eventdoc.get((eventIds.get(i)));
                if (doc == "null") {
                    setDocNames("");
                } else {
                    setDocNames(doc);
                }

                // /////fetching users
                String user = (String) eventuser.get((eventIds.get(i)));
                if (user == "null") {
                    setUsers("");
                } else {
                    setUsers(user);
                }

                // /////fetching resources

                String res = (String) eventres.get((eventIds.get(i)));
                if (res == "null") {
                    setResources("");
                    Rlog.debug("eschreports", "line 16");
                } else {
                    setResources(res);
                    Rlog.debug("eschreports", "line 17");
                }
            }

            Rlog.debug("eschreports", "line 9");

            // ///////// ///////////////retrieving the duration

            // /till here

            mysql = "select name,duration from event_assoc where event_id=?";
            pstmt = conn.prepareStatement(mysql);

            pstmt.setInt(1, StringUtil.stringToNum(id));

            rs = pstmt.executeQuery();
            Rlog.debug("eschreports", "EschReportsDao.fetchEschReportsData  ");
            while (rs.next()) {
                setProtName(rs.getString("name"));
                setProtDuration(rs.getString("duration"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("eschreports",
                    "EschReportsDao.fetchEschReportsData EXCEPTION IN FETCHING "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

}