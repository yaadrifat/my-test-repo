 
/*
 * Classname			PortalDao.class
 * 
 * Version information 	1.0
 *
 * Date					04/05/2007
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.GenerateId;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.patLogin.PatLoginJB;
import com.velos.eres.web.portal.PortalJB;
import com.velos.eres.web.user.UserJB;

/** 
 * PortalDao for getting portal records
 * 
 * @author Manimaran
 * @version : 1.0 04/05/2007
 */


public class PortalDao extends CommonDAO implements java.io.Serializable {
    
	private ArrayList portalIds;

    private ArrayList portalNames;
    
    private ArrayList accountIds;

    private ArrayList createdBy;
    
    private ArrayList portalDescs;

    private ArrayList portalLevels;
    
    private ArrayList notificationFlags;
    
    private ArrayList portalStats;

    private ArrayList dispTypes;

    private ArrayList portalBgColors;

    private ArrayList portalTxtColors;

    private ArrayList portalHeaders;

    private ArrayList portalFooters;
    
    private ArrayList auditUsers;
    
    private ArrayList creator;
    
    private ArrayList lastModifiedBy;
       
    private ArrayList ipAdds;
    
    private ArrayList studyNumbers;
    
    private ArrayList siteNames;
    
    private ArrayList perCodes;
    
    private ArrayList objectIds;
    
    private ArrayList firstNames;
    
    private ArrayList lastNames;
    
    private ArrayList createdOns;
    
    private ArrayList lastModifiedDates;
    
    private ArrayList histStatIds;
    
    //for portal design
    private ArrayList linkedModuleIds;
    
    private ArrayList linkedModuleNames;
    
    private ArrayList linkedModuleTypes;
    
    private ArrayList linkedModuleAlignment;
    
    private ArrayList linkedModuleRangeFrom;
    
    private ArrayList linkedModuleRangeTo;
    
    private ArrayList linkedModuleRangeFromUnit;
    
    private ArrayList linkedModuleRangeToUnit;
    
    private ArrayList linkedModuleFormAfter;
    
    private ArrayList fkStudyIds;//JM;
    
    private ArrayList arPortalSelfLogout;
            
    private int cRows;

    public PortalDao() {
    	portalIds = new ArrayList();
    	portalNames = new ArrayList();
    	accountIds = new ArrayList();
    	createdBy = new ArrayList();
    	portalDescs = new ArrayList();
    	portalLevels = new ArrayList();
    	dispTypes = new ArrayList();
    	portalStats = new ArrayList();
    	notificationFlags = new ArrayList();
    	portalBgColors = new ArrayList();
    	portalTxtColors = new ArrayList();
    	portalHeaders = new ArrayList();
    	portalFooters = new ArrayList();
    	auditUsers = new ArrayList();
    	creator = new ArrayList();
    	lastModifiedBy = new ArrayList();
    	ipAdds = new ArrayList();
    	studyNumbers = new ArrayList();
    	siteNames = new ArrayList();
    	perCodes = new ArrayList();
    	objectIds = new ArrayList();
    	firstNames = new ArrayList();
    	lastNames = new ArrayList();
    	createdOns = new ArrayList();
    	lastModifiedDates = new ArrayList();
    	histStatIds = new ArrayList();
    	
    	linkedModuleIds = new ArrayList();
        
    	linkedModuleNames = new ArrayList();
    	
        linkedModuleTypes = new ArrayList();
        
        linkedModuleAlignment = new ArrayList();
        
        linkedModuleRangeFrom = new ArrayList();
        
        linkedModuleRangeTo = new ArrayList();
        
        linkedModuleRangeFromUnit = new ArrayList();
        
        linkedModuleRangeToUnit = new ArrayList();
        
        linkedModuleFormAfter = new ArrayList();        

        fkStudyIds = new ArrayList();//JM:
        arPortalSelfLogout = new ArrayList();
    	
   }

    // Getter and Setter methods

    public ArrayList getPortalIds() {
        return this.portalIds;
    }

    public void setPortalIds(ArrayList portalIds) {
        this.portalIds = portalIds;
    }
    
    public void setPortalIds(Integer portalId) {
        this.portalIds.add(portalId);
    }
    public ArrayList getPortalNames() {
        return this.portalNames;
    }

    public void setPortalNames(ArrayList portalNames) {
        this.portalNames = portalNames;
    }
    
    public void setPortalNames(String portalName) {
        this.portalNames.add(portalName);
    }

    public ArrayList getAccountIds() {
        return this.accountIds;
    }

    public void setAccountIds(ArrayList accountIds) {
        this.accountIds = accountIds;
    }
    
    public void setAccountIds(Integer accountId) {
        this.accountIds.add(accountId);
    }

    public ArrayList getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(ArrayList createdBy) {
        this.createdBy = createdBy;
    }
    
    public void setCreatedBy(Integer createBy) {
        this.createdBy.add(createBy);
    }

    public ArrayList getPortalDescs() {
        return this.portalDescs;
    }

    public void setPortalDescs(ArrayList portalDescs) {
        this.portalDescs = portalDescs;
    }
    
    public void setPortalDescs(String portalDesc) {
        this.portalDescs.add(portalDesc);
    }

    public ArrayList getPortalLevels() {
        return this.portalLevels;
    }
    
    

    public void setPortalLevels(ArrayList portalLevels) {
        this.portalLevels = portalLevels;
    }
    
    public void setPortalLevels(String portalLevel) {
        this.portalLevels.add(portalLevel);
    }

    public ArrayList getNotificationFlags() {
        return this.notificationFlags;
    }

    public void setNotificationFlags(ArrayList notificationFlags) {
        this.notificationFlags = notificationFlags;
    }
////
   
    public void setNotificationFlags(Integer notificationFlag) {
        this.notificationFlags.add(notificationFlag);
    }
    
    public ArrayList getPortalStats() {
    	return this.portalStats;
    }
    
    public void setPortalStats(ArrayList portalStats) {
        this.portalStats = portalStats;	
    }
    
    public void setPortalStats(String portalStat) {
        this.portalStats.add(portalStat);	
    }
    
    public ArrayList getDispTypes() {
    	return this.dispTypes;
    }
    
    public void setDispTypes(ArrayList dispTypes) {
        this.dispTypes = dispTypes;	
    }
    
    public void setDispTypes(String dispType) {
        this.dispTypes.add(dispType);	
    }
    
    public ArrayList getPortalBgColors() {
        return this.portalBgColors;
    }

    public void setPortalBgColors(ArrayList portalBgColors) {
        this.portalBgColors = portalBgColors;
    }    
    
    public void setPortalBgColors(String portalBgColor) {
        this.portalBgColors.add(portalBgColor);
    }  
    
    public ArrayList getPortalTxtColors() {
        return this.portalTxtColors;
    }

    public void setPortalTxtColors(ArrayList portalTxtColors) {
        this.portalTxtColors = portalTxtColors;
    }
    
    public void setPortalTxtColors(String portalTxtColor) {
        this.portalTxtColors.add(portalTxtColor);
    }
    
    public ArrayList getPortalHeaders() {
        return this.portalHeaders;
    }

    public void setPortalHeaders(ArrayList portalHeaders) {
        this.portalHeaders = portalHeaders;
    }
    
    public void setPortalHeaders(String portalHeader) {
        this.portalHeaders.add(portalHeader);
    }
    
    public ArrayList getPortalFooters() {
        return this.portalFooters;
    }

    public void setPortalFooters(ArrayList portalFooters) {
        this.portalFooters = portalFooters;
    }
    
    public void setPortalFooters(String portalFooter) {
        this.portalFooters.add(portalFooter);
    }
    ///
    public ArrayList getAuditUsers() {
        return this.auditUsers;
    }

    public void setAuditUsers(ArrayList auditUsers) {
        this.auditUsers = auditUsers;
    }
    
    public void setAuditUsers(Integer auditUser) {
        this.auditUsers.add(auditUser);
    }
    
    public ArrayList getCreator() {
        return this.creator;
    }

    public void setCreator(ArrayList creator) {
        this.creator = creator;
    }
    
    public void setCreator(String creatr) {
        this.creator.add(creatr);
    }
    
    
    public ArrayList getLastModifiedBy() {
        return this.lastModifiedBy;
    }

        
    public void setLastModifiedBy(ArrayList lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
    
    public void setLastModifiedBy(String lastModiBy) {
        this.lastModifiedBy.add(lastModiBy);
    }
    
    public ArrayList getIpAdd() {
        return this.ipAdds;
    }
    
    public void setIpAdds(ArrayList ipAdds) {
        this.ipAdds = ipAdds;
    }
    
    public void setIpAdds(String ipAdd) {
        this.ipAdds.add(ipAdd);
    }
    
    
    public ArrayList getObjectId() {
        return this.objectIds;
    }

    public void setObjectId(ArrayList objectIds) {
        this.objectIds = objectIds;
    }
    
    public void setObjectId(Integer objectId) {
        this.objectIds.add(objectId);
    }
  
    public ArrayList getStudyNumber() {
        return this.studyNumbers;
    }

    public void setStudyNumber(ArrayList studyNumbers) {
        this.studyNumbers = studyNumbers;
    }
    
    public void setStudyNumber(String studyNumber) {
        this.studyNumbers.add(studyNumber);
    }
    
    public ArrayList getSiteName() {
        return this.siteNames;
    }

    public void setSiteName(ArrayList siteNames) {
        this.siteNames = siteNames;
    }
    
    public void setSiteName(String siteName) {
        this.siteNames.add(siteName);
    }
    
    public ArrayList getPerCode() {
        return this.perCodes;
    }

    public void setPerCode(ArrayList perCodes) {
        this.perCodes = perCodes;
    }
    
    public void setPerCode(String perCode) {
        this.perCodes.add(perCode);
    }
    
    
    public ArrayList getLastNames() {
        return this.lastNames;
    }

    public void setLastNames(ArrayList lastNames) {
        this.lastNames = lastNames;
    }
    
    public void setLastNames(String lastName) {
        this.lastNames.add(lastName);
    }
    
    public ArrayList getFirstNames() {
        return this.firstNames;
    }

    public void setFirstNames(ArrayList firstNames) {
        this.firstNames = firstNames;
    }
    
    public void setFirstNames(String firstName) {
        this.firstNames.add(firstName);
    }
    
    public ArrayList getCreatedOns() {
        return this.createdOns;
    }

    public void setCreatedOns(ArrayList createdOns) {
        this.createdOns = createdOns;
    }
    
    public void setCreatedOns(String createdOn) {
        this.createdOns.add(createdOn);
    }
     
    public ArrayList getLastModifiedDates() {
        return this.lastModifiedDates;
    }

    public void setLastModifiedDates(ArrayList lastModifiedDates) {
        this.lastModifiedDates = lastModifiedDates;
    }
    
    public void setLastModifiedDates(String lastModifiedDate) {
        this.lastModifiedDates.add(lastModifiedDate);
    }
    
    public ArrayList getHistStatIds() {
        return this.histStatIds;
    }

    public void setHistStatIds(ArrayList histStatIds) {
        this.histStatIds = histStatIds;
    }
    
    public void setHistStatIds(Integer histStatId) {
        this.histStatIds.add(histStatId);
    }
    
    
    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }  
   
//  Query modified by Manimaran to fix the Bug2972
//JM: 03JAN2010: Added the userId parameter: #Enh- PP2.1 
        
    public void getPortalValues(int accountId, int userId) {      	
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
            .prepareStatement("select PK_PORTAL, fk_study, PORTAL_NAME, PORTAL_CREATED_BY, PORTAL_DESC, PORTAL_LEVEL, "
                    + "PORTAL_NOTIFICATION_FLAG, (select codelst_desc from er_codelst where pk_codelst = fk_codelst_stat) as PORTAL_STATUS, PORTAL_DISP_TYPE, PORTAL_BGCOLOR, PORTAL_TEXTCOLOR,  "
                    + "PORTAL_HEADER, PORTAL_FOOTER, PORTAL_AUDITUSER, (select usr_firstname || ' ' || usr_lastname from er_user where pk_user=P1.creator) as creator, "
                    + "(select usr_firstname || ' ' || usr_lastname from er_user where pk_user=P1.last_modified_by) as last_modified_by, P1.IP_ADD, TO_CHAR(P1.CREATED_ON ) AS CREATED_ON, " 
                    + "TO_CHAR(P1.LAST_MODIFIED_DATE) AS LAST_MODIFIED_DATE,PK_STATUS,portal_selflogout FROM ER_PORTAL P1 ,er_status_history WHERE FK_ACCOUNT = ? and p1.pk_portal =status_modpk "
                    + "and STATUS_MODTABLE = 'er_portal' and status_iscurrent =1 " 
                    + "and (p1.fk_study is null or p1.fk_study in (select fk_study from ER_STUDYTEAM where fk_study=p1.fk_study AND FK_USER=? and STUDYTEAM_STATUS='Active') "
                    + "or pkg_superuser.F_Is_Superuser(?,p1.fk_study) > 0) "
                    + "order by lower(PORTAL_NAME) ");
            
                        
            pstmt.setInt(1, accountId);  
            pstmt.setInt(2, userId);  
            pstmt.setInt(3, userId);  
            ResultSet rs = pstmt.executeQuery();            	
            while (rs.next()) {            	
            	setPortalIds(new Integer(rs.getInt("PK_PORTAL"))); 
            	setFkStudyIds(new Integer(rs.getInt("fk_study")));
            	setPortalNames(rs.getString("PORTAL_NAME"));            	
            	setCreatedBy(new Integer(rs.getInt("PORTAL_CREATED_BY")));            	
            	setPortalDescs(rs.getString("PORTAL_DESC"));            	
            	setPortalLevels(rs.getString("PORTAL_LEVEL"));            	
            	setNotificationFlags(new Integer(rs.getInt("PORTAL_NOTIFICATION_FLAG")));
            	setPortalStats(rs.getString("PORTAL_STATUS"));            	
            	setDispTypes(rs.getString("PORTAL_DISP_TYPE"));
            	setPortalBgColors(rs.getString("PORTAL_BGCOLOR"));            	
            	setPortalTxtColors(rs.getString("PORTAL_TEXTCOLOR"));            	
            	setPortalHeaders(rs.getString("PORTAL_HEADER"));            	
            	setPortalFooters(rs.getString("PORTAL_FOOTER"));            	
            	setAuditUsers(new Integer(rs.getInt("PORTAL_AUDITUSER")));
            	setCreator(rs.getString("CREATOR"));
            	setLastModifiedBy(rs.getString("LAST_MODIFIED_BY"));
            	setIpAdds(rs.getString("IP_ADD"));
            	setCreatedOns(rs.getString("CREATED_ON"));
            	setLastModifiedDates(rs.getString("LAST_MODIFIED_DATE"));
            	setHistStatIds(new Integer(rs.getInt("PK_STATUS")));
            	setArPortalSelfLogout(rs.getString("portal_selflogout"));
                rows++;
                Rlog.debug("portal", "PortalDao.getPortalValues rows " + rows);
            }

            setCRows(rows);
            
       } catch (SQLException ex) {
            Rlog.fatal("portal",
                    "PortalDao.getPortalValues EXCEPTION IN FETCHING FROM portal table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    
    
    public void getPortalValue(int portalId) {    
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String portalLvl ="";
        String sqlstmt = "";
        String sqlstmtlvl = "";
        try {
            conn = getConnection();
            sqlstmt = "select PORTAL_NAME, PORTAL_CREATED_BY, PORTAL_DESC, PORTAL_LEVEL, "
                    + "PORTAL_NOTIFICATION_FLAG, PORTAL_STATUS,usr_firstname,usr_lastname from er_portal left outer join er_user on (nvl(portal_created_by,0)= pk_user) where pk_portal = ? ";
                 
            pstmt = conn.prepareStatement( sqlstmt);
            
            pstmt.setInt(1, portalId);
            ResultSet rs = pstmt.executeQuery();            	
            while (rs.next()) {            	
            	
            	setPortalNames(rs.getString("PORTAL_NAME"));
            	
            	setCreatedBy(new Integer(rs.getInt("PORTAL_CREATED_BY")));            	
            	setPortalDescs(rs.getString("PORTAL_DESC"));            	
            	setPortalLevels(rs.getString("PORTAL_LEVEL"));
            	
            	setNotificationFlags(new Integer(rs.getInt("PORTAL_NOTIFICATION_FLAG")));
            	setPortalStats(rs.getString("PORTAL_STATUS"));  
            	setFirstNames(rs.getString("usr_firstname"));
            	setLastNames(rs.getString("usr_lastname"));
            	portalLvl = rs.getString("PORTAL_LEVEL"); 
            } 
            
            pstmt =null;
            rs =null;
            
            if (portalLvl.equals("S")) {
            	
            	sqlstmtlvl = "SELECT PP_OBJECT_ID, STUDY_NUMBER "
                    + "FROM ER_STUDY, ER_PORTAL_POPLEVEL "
                    + "WHERE FK_PORTAL = ?  AND PP_OBJECT_ID = PK_STUDY ";
            	
            } else if (portalLvl.equals("O")) {
            	
            	sqlstmtlvl = "SELECT PP_OBJECT_ID, SITE_NAME "
                    + "FROM ER_SITE , ER_PORTAL_POPLEVEL "
                    + "WHERE FK_PORTAL = ?  AND PP_OBJECT_ID = PK_SITE ";
            	
            } else if (portalLvl.equals("P")) {
            	
        	sqlstmtlvl = "SELECT PP_OBJECT_ID, PER_CODE "
                + "FROM ER_PER, ER_PORTAL_POPLEVEL "
                    + "WHERE FK_PORTAL = ?  AND PP_OBJECT_ID = PK_PER ";
            }
            
           
            pstmt = conn.prepareStatement( sqlstmtlvl);
            
            pstmt.setInt(1, portalId);
            rs = pstmt.executeQuery();            	
            while (rs.next()) {   
            	
            		setObjectId(rs.getInt("PP_OBJECT_ID"));
            	if( portalLvl.equals("S"))
            	    setStudyNumber(rs.getString("STUDY_NUMBER"));
            	if( portalLvl.equals("O"))
            		setSiteName(rs.getString("SITE_NAME"));
            	if( portalLvl.equals("P"))
            		setPerCode(rs.getString("PER_CODE"));
            }

            
       } catch (SQLException ex) {
            Rlog.fatal("portal",
                    "PortalDao.getPortalValues EXCEPTION IN FETCHING FROM portal table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public void insertPortalValues(int pid,String portalPatPop,String selIds,int usr,int user,String ipAdd) {
    	
    	int rows = 0;
    	int id = -1;
    	String selectedId="";
        PreparedStatement pstmt = null;
        Connection conn = null;
              
        
        try {
            conn = getConnection();
           //code modified by KN for 2965    
            pstmt = conn
                    .prepareStatement("insert into er_portal_poplevel(pk_portal_poplevel,fk_portal,pp_object_type,pp_object_id,creator,ip_add) values (?,?,?,?,?,?)");
           
            String[] strValues = selIds.split(",");
            for(int i=0;i<strValues.length;i++){
            	selectedId=strValues[i];
				id = GenerateId.getId("seq_er_portal_poplevel", conn);
				pstmt.setInt(1,id);
				pstmt.setInt(2, pid);
		        pstmt.setString(3,portalPatPop);
		        pstmt.setInt(4,EJBUtil.stringToNum(selectedId));
		        pstmt.setInt(5,usr);
		        pstmt.setString(6,ipAdd);
		        pstmt.execute();
		                   	
            }
              
        } catch (SQLException ex) {
            Rlog.fatal("portal",
                    "PortalDao.insertPortalValues"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    
    public void deletePortalPop(int portalId) {    
       
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("delete  from er_portal_poplevel where fk_portal = ?");
                            
                            
            pstmt.setInt(1, portalId);            
            pstmt.execute();            	
                        
       } catch (SQLException ex) {
            Rlog.fatal("portal",
                    "PortalDao.deletePortalPop EXCEPTION IN deletion from er_portal_poplevel table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public int deletePortal(int portalId) {   // Modified for INF-18183 ::: Akshi
    	int ret=0;
        CallableStatement cstmt = null;
        Rlog.debug("portal", "In deletePortal in PortalDao line number 0");
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_delete_portal(?)}");

            cstmt.setInt(1, portalId);
            cstmt.execute();
        } catch (SQLException ex) {
        	ret = -1;
            Rlog.fatal("portal",
                    "In deletePortal in PortalDao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;
    }
    
	// Modified for PP-18306 AGodara
	public void getDesignPreview(int portalId, int patProtProtocolId, boolean flag) {

		int rows = 0;
		PreparedStatement pstmt = null;
		Connection conn = null;
		StringBuffer sbSQL = new StringBuffer();

		try {
			conn = getConnection();

			if (flag) {
				sbSQL
						.append(" select fk_id,pm_type,pm_align,pm_from,pm_to,pm_from_unit,pm_to_unit, pm_form_after_resp,");
				sbSQL
						.append(" decode(pm_type,'EF',' ','LF',' ','S',(select name from event_assoc where event_id = ? and event_type = 'P'),'SF',(select name from event_assoc where event_id = ? and event_type = 'P') ) module_name");
				sbSQL
						.append(" from er_portal_modules where fk_portal = ? order by pm_align ");
				pstmt = conn.prepareStatement(sbSQL.toString());
				pstmt.setInt(1, patProtProtocolId);
				pstmt.setInt(2, patProtProtocolId);
				pstmt.setInt(3, portalId);
			} else {
				sbSQL
						.append(" select fk_id,pm_type,pm_align,pm_from,pm_to,pm_from_unit,pm_to_unit, pm_form_after_resp,");
				sbSQL.append(" decode(pm_type,'EF',' ','LF',' ') module_name");
				sbSQL
						.append(" from er_portal_modules where fk_portal = ? AND pm_type NOT IN ('S','SF')  order by pm_align ");
				pstmt = conn.prepareStatement(sbSQL.toString());
				pstmt.setInt(1, portalId);
			}

			Rlog.info("portal", "PortalDao.getDesignPreview sbSQL.toString()"
					+ sbSQL.toString());

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				
				if(rs.getString("pm_type").equalsIgnoreCase("S") || rs.getString("pm_type").equalsIgnoreCase("SF")){
					setLinkedModuleIds(String.valueOf(patProtProtocolId));
				}else{
					setLinkedModuleIds(rs.getString("fk_id"));
				}
				setLinkedModuleNames(rs.getString("module_name"));
				setLinkedModuleTypes(rs.getString("pm_type"));
				setLinkedModuleAlignment(rs.getString("pm_align"));
				setLinkedModuleRangeFrom(rs.getString("pm_from"));
				setLinkedModuleRangeTo(rs.getString("pm_to"));
				setLinkedModuleRangeFromUnit(rs.getString("pm_from_unit"));
				setLinkedModuleRangeToUnit(rs.getString("pm_to_unit"));
				setLinkedModuleFormAfter(rs.getString("pm_form_after_resp"));

				rows++;
				Rlog.debug("portal", "PortalDao.getPortalValues rows " + rows);
			}

			setCRows(rows);

		} catch (SQLException ex) {
			Rlog.fatal("portal",
					"PortalDao.getDesignPreview EXCEPTION IN FETCHING FROM er_portal_modules table"
							+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
	}
  
	// PP-18306 AGodara
	public String getPortalCalIds(int portalId) {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		String calIds = "";
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement("select fk_id from er_portal_modules where fk_portal = ? AND pm_type IN ('S','SF') ");

			pstmt.setInt(1, portalId);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				calIds = rs.getString(1);
			}
		} catch (SQLException ex) {
			Rlog
					.fatal(
							"portal",
							"PortalDao.getPortalCalIds EXCEPTION IN FETCHING calIds FROM er_portal_modules table"
									+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return calIds;
	}

	// GETTER SETTERS FOR PRORTAL DESIGN

	public ArrayList getLinkedModuleAlignment() {
		return linkedModuleAlignment;
	}

	public void setLinkedModuleAlignment(ArrayList linkedModuleAlignment) {
		this.linkedModuleAlignment = linkedModuleAlignment;
	}

	public void setLinkedModuleAlignment(String alignment) {
		this.linkedModuleAlignment.add(alignment);
	}

	
	public ArrayList getLinkedModuleFormAfter() {
		return linkedModuleFormAfter;
	}

	public void setLinkedModuleFormAfter(ArrayList linkedModuleFormAfter) {
		this.linkedModuleFormAfter = linkedModuleFormAfter;
	}
	
	public void setLinkedModuleFormAfter(String formAfter) {
		this.linkedModuleFormAfter.add(formAfter);
	}
	

	public ArrayList getLinkedModuleIds() {
		return linkedModuleIds;
	}

	public void setLinkedModuleIds(ArrayList linkedModuleIds) {
		this.linkedModuleIds = linkedModuleIds;
	}

	public void setLinkedModuleIds(String linkedModuleId) {
		this.linkedModuleIds.add(linkedModuleId);
	}
	
	
	public ArrayList getLinkedModuleNames() {
		return linkedModuleNames;
	}

	public void setLinkedModuleNames(ArrayList linkedModuleNames) {
		this.linkedModuleNames = linkedModuleNames;
	}
	
	public void setLinkedModuleNames(String linkedModuleName) {
		this.linkedModuleNames.add(linkedModuleName);
	}
	

	public ArrayList getLinkedModuleRangeFrom() {
		return linkedModuleRangeFrom;
	}

	public void setLinkedModuleRangeFrom(ArrayList linkedModuleRangeFrom) {
		this.linkedModuleRangeFrom = linkedModuleRangeFrom;
	}
	
	public void setLinkedModuleRangeFrom(String rangeFrom) {
		this.linkedModuleRangeFrom.add(rangeFrom);
	}

	

	public ArrayList getLinkedModuleRangeFromUnit() {
		return linkedModuleRangeFromUnit;
	}

	public void setLinkedModuleRangeFromUnit(ArrayList linkedModuleRangeFromUnit) {
		this.linkedModuleRangeFromUnit = linkedModuleRangeFromUnit;
	}
	
	public void setLinkedModuleRangeFromUnit(String rangeFromUnit) {
		this.linkedModuleRangeFromUnit.add(rangeFromUnit);
	}

	public ArrayList getLinkedModuleRangeTo() {
		return linkedModuleRangeTo;
	}

	public void setLinkedModuleRangeTo(ArrayList linkedModuleRangeTo) {
		this.linkedModuleRangeTo = linkedModuleRangeTo;
	}
	
	public void setLinkedModuleRangeTo(String rangeTo) {
		this.linkedModuleRangeTo.add(rangeTo);
	}


	public ArrayList getLinkedModuleRangeToUnit() {
		return linkedModuleRangeToUnit;
	}

	public void setLinkedModuleRangeToUnit(ArrayList linkedModuleRangeToUnit) {
		this.linkedModuleRangeToUnit = linkedModuleRangeToUnit;
	}

	public void setLinkedModuleRangeToUnit(String rangeToUnit) {
		this.linkedModuleRangeToUnit.add(rangeToUnit);
	}

	
	public ArrayList getLinkedModuleTypes() {
		return linkedModuleTypes;
	}

	public void setLinkedModuleTypes(ArrayList linkedModuleTypes) {
		this.linkedModuleTypes = linkedModuleTypes;
	}
	
	public void setLinkedModuleTypes(String linkedModuleType) {
		this.linkedModuleTypes.add(linkedModuleType);
	}
	
    //JM:
    public ArrayList getFkStudyIds() {
        return this.fkStudyIds;
    }

    public void setFkStudyIds(ArrayList fkStudyIds) {
        this.fkStudyIds = fkStudyIds;
    }   
    
     
    public void setFkStudyIds(Integer fkStudyId) {
        this.fkStudyIds.add(fkStudyId);
    }
          
    
//  JM: 07May07 to find out the existing total count of portals

    public int getPortalsCount(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("SELECT count(*) portalcount "
                    + "FROM ER_PORTAl Where fk_account = ? ");

            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();
            if (rs != null) {
                rs.next();
                rows = rs.getInt("portalcount");
            }
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "AccountDao.getPortalsCount EXCEPTION IN FETCHING FROM ER_PORTAL table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return rows;
    }
    
    
    public int notifyPatient(int patient, String portalUserName, String portalPassword,int creator) {
        CallableStatement cstmt = null;
        
        Connection conn = null;
        String err = "";
        int returnVal = 0;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call pkg_patient.sp_notify_portal_patient(?,?,?,?,?)}");

            cstmt.setInt(1, patient);
            cstmt.setString(2, portalUserName);
            cstmt.setString(3, portalPassword);
            cstmt.setInt(4, creator);
            
            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            cstmt.execute();
            err = cstmt.getString(5);
            
            if (! StringUtil.isEmpty(err))
            {
            	returnVal  = -1;
            	
            }
            else
            {
            	Rlog.fatal("portal",
                        "In notifyPatient in PortalDao line EXCEPTION IN calling the procedure"
                                + err);
            	
            }
            return returnVal;
            
        } catch (SQLException ex) {
            Rlog.fatal("portal",
                    "In notifyPatient in PortalDao line EXCEPTION IN calling the procedure"
                            + ex);
            return returnVal;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            
        }
    }

	public ArrayList getArPortalSelfLogout() {
		return arPortalSelfLogout;
	}

	public void setArPortalSelfLogout(ArrayList arPortalSelfLogout) {
		this.arPortalSelfLogout = arPortalSelfLogout;
	}
	
	public void setArPortalSelfLogout(String portalSelfLogout) {
		this.arPortalSelfLogout.add(portalSelfLogout);
	}
	
	/* create default logins for the portal according to patient population option*/
    public int createPatientLogins(int portalId, int creator, String ipAdd) {
        CallableStatement cstmt = null;
        
        Connection conn = null;
        String err = "";
        int returnVal = 0;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call pkg_portal.SP_CREATE_DEFLOGINS(?,?,?,?) }");

            cstmt.setInt(1, portalId);
            cstmt.setInt(2, creator);
            cstmt.setString(3, ipAdd);
           
            
            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();
            returnVal = cstmt.getInt(4);
            
            if (returnVal == -1)
            {
            	Rlog.fatal("portal",
                        "In createPatientLogins in PortalDao line EXCEPTION IN calling the procedure"
                                );
            	
            }
            return returnVal;
            
        } catch (SQLException ex) {
            Rlog.fatal("portal",
                    "In createPatientLogins in PortalDao line EXCEPTION IN calling the procedure"
                            + ex);
            return returnVal;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            
        }
    }

    //create login if not found
    
    /** creates default login using the patient MRN if the account is set for automatic login facility
     * @return Hasthtable A Hashtable object with following information:
     * Key:password ,  Value:the password for the patient login created
     * Key:loginFlag, value: 1:new login created, 0 : new login not created
     *  
     * */
    public Hashtable createAutoPatientLoginIfEnabled(String login,String logpass,String ipAdd) {
        CallableStatement cstmt = null;
        
        Connection conn = null;
        String err = "";
        String loginFlag = "0";
        String password = "";
        Hashtable htData = new Hashtable();
        
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call pkg_portal.SP_CREATE_AUTOLOGIN(?,?,?,?,?,?) }");

            cstmt.setString(1, login);
             
            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(4, java.sql.Types.VARCHAR);
            
            cstmt.setString(5, logpass);
            cstmt.setString(6, ipAdd);
            
            cstmt.execute();
            
            
            password =  cstmt.getString(2);
            loginFlag = cstmt.getString(3);
            
            err = cstmt.getString(4);
            
            
            if (!StringUtil.isEmpty(err))
            {
            	Rlog.fatal("portal",
                        "In createAutoPatientLoginIfEnabled in PortalDao line EXCEPTION IN calling the procedure"
                        + err        );
            	htData.put("loginFlag","0" );
            	
            }
            else
            {
            	if (! StringUtil.isEmpty(password))
            	{
            		htData.put("password",password );
            	}	
            	htData.put("loginFlag", loginFlag );
            	
            }
            
            
            
            return htData;
            
        } catch (SQLException ex) {
            Rlog.fatal("portal",
                    "In createAutoPatientLoginIfEnabledin PortalDao line EXCEPTION IN calling the procedure"
                            + ex);
            return htData;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            
        }
    }

    public int migratePass(String mod)
    {
   	 int count = 0;
   	 PortalJB pJB=new PortalJB();
   	PatLoginJB plJB=new PatLoginJB();
        String tempPass="";     
        PreparedStatement pstmt = null;
        Connection conn = null;
        int id=0;
        
        try {
            conn = getConnection();
            if (mod.equals("portal")) {	
            pstmt = conn.prepareStatement("select pk_portal from er_portal where portal_defaultpass is not null order by pk_portal asc");
                    ResultSet rs = pstmt.executeQuery();
   
           while (rs.next())
           {
           	 id=rs.getInt("PK_PORTAL");
            	 pJB.setPortalId(id);
            	 
            	 //System.out.println("Processing Portal with Id=" +id );
            	 
            	 pJB.getPortalDetails();
            	 tempPass=Security.encryptSHA(Security.decrypt(pJB.getPortalDefaultPassword()));
            	 pJB.setPortalDefaultPassword(tempPass);
            	 pJB.updatePortal();
            	 count++;
           }
           
          }
           if (mod.equals("portL")) {	
               pstmt = conn.prepareStatement("select pk_portal_login from er_portal_logins where pl_password is not null order by pk_portal_login asc");
                        ResultSet rs = pstmt.executeQuery();
       
               while (rs.next())
               {
               	 id=rs.getInt("PK_PORTAL_LOGIN");
                 plJB.setId(id);
                	 //System.out.println("Processing Portal Login with Id=" +id );
                 
                	 plJB.getPatLoginDetails();
                	 tempPass=Security.encryptSHA(Security.decrypt(plJB.getPlPassword()));
                	 plJB.setPlPassword(tempPass);
                	 plJB.updatePatLogin();
                	 count++;
               }
               
              }
   
   
        } catch (SQLException ex) {
            Rlog.fatal("user",
                    "PortalDao.migratePass EXCEPTION IN FETCHING FROM Portal table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();               
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();                
            } catch (Exception e) {
            }
    
        }
        return count;
    }
        

    
	//end of class
}
