/*
 * Classname:	StorageKitBean.class
 *
 * Version information: 1.0
 *
 * Date: 05Aug2009
 *
 * Copyright notice: Velos Inc
 *
 * Author: Jnanamay
 *
 */

package com.velos.eres.business.storageKit.impl;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.sql.Clob;
import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The StorageKit BMP entity bean.
 *
 *
 * @author Jnanamay
 * @version 1.0 06/26/2007
 */
@Entity
@Table(name = "er_storage_kit")

/*@NamedQueries({
	@NamedQuery(name = "findStorageKitName", queryString = "SELECT OBJECT(storage) FROM StorageBean storage where UPPER(trim(STORAGE_NAME)) = UPPER(trim(:storageName)) and UPPER(trim(STORAGE_ID)) = UPPER(trim(:storageId))"),
	@NamedQuery(name = "findStorageKitId", queryString = "SELECT OBJECT(storage) FROM StorageBean storage where UPPER(trim(STORAGE_ID)) = UPPER(trim(:storageId)) and FK_ACCOUNT = :accountId")
})*/

public class StorageKitBean implements Serializable {

    private static final long serialVersionUID = 3761410811205333553L;


    /**
     * PK_STORAGE_KIT
     */
    public int pkStorageKit;

    /**
     * FK_STORAGE
     */
    public Integer fkStorage;

    /**
     * DEF_SPECIMEN_TYPE
     */
    public Integer defSpecimenType;

    /**
     * DEF_PROCESSING_TYPE
     */
    public String defProcType;

    /**
     * DEF_SPEC_PROCESSING_SEQ
     */
    public String defProcSeq;

    /**
     * DEF_SPEC_QUANTITY
     */
    public float defSpecQuant;

    /**
     * DEF_SPEC_QUANTITY_UNIT
     */
    public Integer defSpecQuantUnit;

    /**
     * KIT_SPEC_DISPOSITION
     */
    public Integer kitSpecDisposition;

    /**
     * KIT_SPEC_ENVT_CONS
     */
    public String kitSpecEnvtCons;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;









 // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_storage_kit", allocationSize=1)

    @Column(name = "PK_STORAGE_KIT")
    public int getPkStorageKit() {
        return this.pkStorageKit;
    }

    public void setPkStorageKit(int pkStorageKit) {
        this.pkStorageKit = pkStorageKit;
    }

    @Column(name = "FK_STORAGE")
    public String getFkStorage() {
    	return StringUtil.integerToString(this.fkStorage);
    }

    public void setFkStorage(String fkStorage) {
        this.fkStorage = StringUtil.stringToInteger(fkStorage);
    }


    @Column(name = "DEF_SPECIMEN_TYPE")
    public String getDefSpecimenType() {
    	return StringUtil.integerToString(this.defSpecimenType);
    }

    public void setDefSpecimenType(String defSpecimenType) {
        this.defSpecimenType = StringUtil.stringToInteger(defSpecimenType);
    }

    @Column(name = "DEF_PROCESSING_TYPE")
    public String getDefProcType() {
    	return this.defProcType;
    }

    public void setDefProcType(String defProcType) {
        this.defProcType = defProcType;
    }

    @Column(name = "DEF_SPEC_PROCESSING_SEQ")
    public String getDefProcSeq() {
    	return this.defProcSeq;
    }

    public void setDefProcSeq(String defProcSeq) {
        this.defProcSeq = defProcSeq;
    }

    @Column(name = "DEF_SPEC_QUANTITY")
    public String getDefSpecQuant() {
    	return StringUtil.floatToString(this.defSpecQuant);
    }

    public void setDefSpecQuant(String defSpecQuant) {
    	this.defSpecQuant = StringUtil.stringToFloat(defSpecQuant);
    }

    @Column(name = "DEF_SPEC_QUANTITY_UNIT")
    public String getDefSpecQuantUnit() {
    	return StringUtil.integerToString(this.defSpecQuantUnit);
    }

    public void setDefSpecQuantUnit(String defSpecQuantUnit) {
        this.defSpecQuantUnit = StringUtil.stringToInteger(defSpecQuantUnit);
    }

    @Column(name = "KIT_SPEC_DISPOSITION")
    public String getKitSpecDisposition() {
    	return StringUtil.integerToString(this.kitSpecDisposition);
    }

    public void setKitSpecDisposition(String kitSpecDisposition) {
        this.kitSpecDisposition = StringUtil.stringToInteger(kitSpecDisposition);
    }

    @Column(name = "KIT_SPEC_ENVT_CONS")
    public String getKitSpecEnvtCons() {
    	return this.kitSpecEnvtCons;
    }

    public void setKitSpecEnvtCons(String kitSpecEnvtCons) {
        this.kitSpecEnvtCons = kitSpecEnvtCons;
    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS


    public int updateStorageKit(StorageKitBean ssk) {
        try {

        	setPkStorageKit(ssk.getPkStorageKit());
		    setFkStorage(ssk.getFkStorage());
		    setDefSpecimenType(ssk.getDefSpecimenType());
		    setDefProcType(ssk.getDefProcType());
		    setDefProcSeq(ssk.getDefProcSeq());
		    setDefSpecQuant(ssk.getDefSpecQuant());
		    setDefSpecQuantUnit(ssk.getDefSpecQuantUnit());
		    setKitSpecDisposition(ssk.getKitSpecDisposition());
		    setKitSpecEnvtCons(ssk.getKitSpecEnvtCons());
		    setCreator(ssk.getCreator());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());

            Rlog.debug("storagekit", "StorageBean.updateStorageKit");
            return 0;
        } catch (Exception e) {

            Rlog.fatal("storagekit", " error in StorageBean.updateStorageKit");
            return -2;
        }
    }

    public StorageKitBean() {

    }

    public StorageKitBean(int pkStorageKit,
    	    String fkStorage,
    	    String defSpecimenType,
    	    String defProcType,
    	    String defProcSeq,
    	    String defSpecQuant,
    	    String defSpecQuantUnit,
    	    String kitSpecDisposition,
    	    String kitSpecEnvtCons,
    	    String creator,
    	    String modifiedBy,
    	    String ipAdd) {
        super();
        // TODO Auto-generated constructor stub

        setPkStorageKit(pkStorageKit);
        setFkStorage(fkStorage);
        setDefSpecimenType(defSpecimenType);
        setDefProcType(defProcType);
        setDefProcSeq(defProcSeq);
        setDefSpecQuant(defSpecQuant);
        setDefSpecQuantUnit(defSpecQuantUnit);
        setKitSpecDisposition(kitSpecDisposition);
        setKitSpecEnvtCons(kitSpecEnvtCons);
        setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);



    }

}


