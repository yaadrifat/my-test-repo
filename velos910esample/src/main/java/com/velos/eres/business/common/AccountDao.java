/*
 * Classname			AccountDao
 * 
 * Version information 	1.0
 *
 * Date					05/09/2001
 * 
 * Copyright notice		Velos, Inc.
 *
 * Author 				Sajal
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * AccountDao for getting Account information based on some retrieval criteria
 * 
 * @author Sajal
 * @version : 1.0 05/09/2001
 */

public class AccountDao extends CommonDAO implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5416619346886894222L;

    private ArrayList accIds;

    private ArrayList accTypes;

    private ArrayList accRoleTypes;

    private ArrayList accNames;

    private ArrayList accStartDates;

    private ArrayList accEndDates;

    private ArrayList accStats;

    private ArrayList accMailFlags;

    private ArrayList accPubFlags;

    private ArrayList accNotes;

    private ArrayList accCreators;

    private ArrayList accMaxUsrs;

    private ArrayList accMaxStorages;

    private ArrayList accModRights;
    
    private ArrayList accLoginModuleDNList;
    
    private ArrayList accLoginModuleIdList;
    
    private ArrayList accLoginModuleServerList;
    
    private ArrayList accLoginModulePortList;
    
    private ArrayList accLoginModuleProtocolList;
    

    private int cRows;

   
	public AccountDao() {
        accIds = new ArrayList();
        accTypes = new ArrayList();
        accRoleTypes = new ArrayList();
        accNames = new ArrayList();
        accStartDates = new ArrayList();
        accEndDates = new ArrayList();
        accStats = new ArrayList();
        accMailFlags = new ArrayList();
        accPubFlags = new ArrayList();
        accNotes = new ArrayList();
        accCreators = new ArrayList();
        accMaxUsrs = new ArrayList();
        accMaxStorages = new ArrayList();
        accModRights = new ArrayList();
        accLoginModuleDNList=new ArrayList();
        accLoginModuleIdList=new ArrayList();
        accLoginModuleServerList=new ArrayList();;
        accLoginModulePortList=new ArrayList();
        accLoginModuleProtocolList=new ArrayList();;
        
    }

    // GETTER SETTER METHIODS

    public ArrayList getAccIds() {
        return this.accIds;
    }

    public void setAccIds(ArrayList accIds) {
        this.accIds = accIds;
    }

    public ArrayList getAccTypes() {
        return this.accTypes;
    }

    public void setAccTypes(ArrayList accTypes) {
        this.accTypes = accTypes;
    }

    public ArrayList getAccRoleTypes() {
        return this.accRoleTypes;
    }

    public void setAccRoleTypes(ArrayList accRoleTypes) {
        this.accRoleTypes = accRoleTypes;
    }

    public ArrayList getAccNames() {
        return this.accNames;
    }

    public void setAccNames(ArrayList accNames) {
        this.accNames = accNames;
    }

    public ArrayList getAccStartDates() {
        return this.accStartDates;
    }

    public void setAccStartDates(ArrayList accStartDates) {
        this.accStartDates = accStartDates;
    }

    public ArrayList getAccEndDates() {
        return this.accEndDates;
    }

    public void setAccEndDates(ArrayList accEndDates) {
        this.accEndDates = accEndDates;
    }

    public ArrayList getAccStats() {
        return this.accStats;
    }

    public void setAccStats(ArrayList accStats) {
        this.accStats = accStats;
    }

    public ArrayList getAccMailFlags() {
        return this.accMailFlags;
    }

    public void setAccMailFlags(ArrayList accMailFlags) {
        this.accMailFlags = accMailFlags;
    }

    public ArrayList getAccPubFlags() {
        return this.accPubFlags;
    }

    public void setAccPubFlags(ArrayList accPubFlags) {
        this.accPubFlags = accPubFlags;
    }

    public ArrayList getAccNotes() {
        return this.accNotes;
    }

    public void setAccNotes(ArrayList accNotes) {
        this.accNotes = accNotes;
    }

    public ArrayList getAccCreators() {
        return this.accCreators;
    }

    public void setAccCreators(ArrayList accCreators) {
        this.accCreators = accCreators;
    }

    public ArrayList getAccMaxUsrs() {
        return this.accMaxUsrs;
    }

    public void setAccMaxUsrs(ArrayList accMaxUsrs) {
        this.accMaxUsrs = accMaxUsrs;
    }

    public ArrayList getAccMaxStorages() {
        return this.accMaxStorages;
    }

    public void setAccMaxStorages(ArrayList accMaxStorages) {
        this.accMaxStorages = accMaxStorages;
    }

    public ArrayList getAccModRights() {
        return this.accModRights;
    }

    public void setAccModRights(ArrayList accModRights) {
        this.accModRights = accModRights;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setAccIds(Integer accId) {
        this.accIds.add(accId);
    }

    public void setAccTypes(String accType) {
        this.accTypes.add(accType);
    }

    public void setAccRoleTypes(String accRoleType) {
        this.accRoleTypes.add(accRoleType);
    }

    public void setAccNames(String accName) {
        this.accNames.add(accName);
    }

    public void setAccStartDates(String accStartDate) {
        this.accStartDates.add(accStartDate);
    }

    public void setAccEndDates(String accEndDate) {
        this.accEndDates.add(accEndDate);
    }

    public void setAccStats(String accStat) {
        this.accStats.add(accStat);
    }

    public void setAccMailFlags(String accMailFlag) {
        this.accMailFlags.add(accMailFlag);
    }

    public void setAccPubFlags(String accPubFlag) {
        this.accPubFlags.add(accPubFlag);
    }

    public void setAccNotes(String accNote) {
        this.accNotes.add(accNote);
    }

    public void setAccCreators(String accCreator) {
        this.accCreators.add(accCreator);
    }

    public void setAccMaxUsrs(String accMaxUsr) {
        this.accMaxUsrs.add(accMaxUsr);
    }

    public void setAccMaxStorages(String accMaxStorage) {
        this.accMaxStorages.add(accMaxStorage);
    }

    public void setAccModRights(String accModRight) {
        this.accModRights.add(accModRight);
    }
    /**
	 * @return the accLoginModuleDNList
	 */
	public ArrayList getAccLoginModuleDNList() {
		return accLoginModuleDNList;
	}

	/**
	 * @param accLoginModuleDNList the accLoginModuleDNList to set
	 */
	public void setAccLoginModuleDNList(ArrayList accLoginModuleDNList) {
		this.accLoginModuleDNList = accLoginModuleDNList;
	}
	/**
	 * @param accLoginModuleDN the accLoginModuleDN to set
	 */
	public void setAccLoginModuleDNList(String accLoginModuleDN) {
		this.accLoginModuleDNList.add(accLoginModuleDN);
	}

	/**
	 * @return the accLoginModuleIdList
	 */
	public ArrayList getAccLoginModuleIdList() {
		return accLoginModuleIdList;
	}

	/**
	 * @param accLoginModuleIdList the accLoginModuleIdList to set
	 */
	public void setAccLoginModuleIdList(ArrayList accLoginModuleIdList) {
		this.accLoginModuleIdList = accLoginModuleIdList;
	}
	/**
	 * @param accLoginModuleIdList the accLoginModuleIdList to set
	 */
	public void setAccLoginModuleIdList(Integer accLoginModuleId) {
		this.accLoginModuleIdList.add(accLoginModuleId);
	}
	
    /**
	 * @return the accLoginModulePortList
	 */
	public ArrayList getAccLoginModulePortList() {
		return accLoginModulePortList;
	}

	/**
	 * @param accLoginModulePortList the accLoginModulePortList to set
	 */
	public void setAccLoginModulePortList(ArrayList accLoginModulePortList) {
		this.accLoginModulePortList = accLoginModulePortList;
	}
	/**
	 * @param accLoginModulePortList the accLoginModulePortList to set
	 */
	public void setAccLoginModulePortList(Integer accLoginModulePort) {
		this.accLoginModulePortList.add(accLoginModulePort);
	}

	/**
	 * @return the accLoginModuleProtocolList
	 */
	public ArrayList getAccLoginModuleProtocolList() {
		return accLoginModuleProtocolList;
	}

	/**
	 * @param accLoginModuleProtocolList the accLoginModuleProtocolList to set
	 */
	public void setAccLoginModuleProtocolList(ArrayList accLoginModuleProtocolList) {
		this.accLoginModuleProtocolList = accLoginModuleProtocolList;
	}
	/**
	 * @param accLoginModuleProtocolList the accLoginModuleProtocolList to set
	 */
	public void setAccLoginModuleProtocolList(String accLoginModuleProtocol) {
		this.accLoginModuleProtocolList.add(accLoginModuleProtocol);
	}

	/**
	 * @return the accLoginModuleServerList
	 */
	public ArrayList getAccLoginModuleServerList() {
		return accLoginModuleServerList;
	}

	/**
	 * @param accLoginModuleServerList the accLoginModuleServerList to set
	 */
	public void setAccLoginModuleServerList(ArrayList accLoginModuleServerList) {
		this.accLoginModuleServerList = accLoginModuleServerList;
	}
	/**
	 * @param accLoginModuleServerList the accLoginModuleServerList to set
	 */
	public void setAccLoginModuleServerList(String accLoginModuleServer) {
		this.accLoginModuleServerList.add(accLoginModuleServer);
	}

	/**
     * Sets this classes variables with the account information
     * 
     */

    public void getAccounts() {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select pk_account, USR_LOGNAME "
                    + "from er_account, er_user "
                    + "where AC_USRCREATOR = pk_user "
                    + "order by USR_LOGNAME ");

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("account", "AccountDao.getAccounts  ");
            while (rs.next()) {
                // Rlog.debug("account","AccountDao.getAccounts ACCID " +
                // rs.getInt("pk_account"));
                // Rlog.debug("account","AccountDao.getAccounts USR_LOGNAME " +
                // rs.getString("USR_LOGNAME"));

                // setAccIds(new Integer(23));
                // setAccNames("er");

                setAccIds(new Integer(rs.getInt("pk_account")));
                setAccNames(rs.getString("USR_LOGNAME"));
                rows++;
                Rlog.debug("account",
                        "AccountDao.getAccounts  ************rows " + rows);
            }
            Rlog.debug("account", "AccountDao.getAccounts  rows " + rows);
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("account",
                    "AccountDao.getCreationDate EXCEPTION IN FETCHING FROM Account table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Returns the Creation date corresponding to some Account Id
     * 
     * @param accId
     *            The accId for which the Creation date is to be fetched
     * @return The creation date of the account
     */

    public String getCreationDate(int accId) {
        String creation_date = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select created_on "
                    + "from	er_account " + "where pk_account = ? ");
            pstmt.setInt(1, accId);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("account", "AccountDao.getCreationDate  ");
            rs.next();
            creation_date = rs.getString("created_on");
        } catch (SQLException ex) {
            Rlog.fatal("account",
                    "AccountDao.getCreationDate EXCEPTION IN FETCHING FROM Account table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return creation_date;
    }

    public int copyAccount(int accId) {
        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            cstmt = conn.prepareCall("{call sp_cpaccount(?,?,?)}");

            Configuration conf = new Configuration();
            int velosAdminAccountId = StringUtil.stringToNum(conf
                    .getVelosAdminAccountId(Configuration.ERES_HOME
                            + "eresearch.xml"));

            Rlog.debug("account", "AccountDao.copyAccount accId = " + accId);

            Rlog.debug("account",
                    "AccountDao.copyAccount velosAdminAccountId = "
                            + velosAdminAccountId);

            cstmt.setInt(1, velosAdminAccountId);
            cstmt.setInt(2, accId);

            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);

            cstmt.execute();
            Rlog.debug("account", "AccountDao.copyAccount ret = " + ret);
            ret = cstmt.getInt(3);

            Rlog.debug("account", "AccountDao.copyAccount ret = " + ret);

        } catch (SQLException ex) {
            Rlog.fatal("account",
                    "AccountDao.copyAccount EXCEPTION IN FETCHING FROM Account table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;
    }

    /**
     * Returns the Account Primary key
     * 
     * @param siteCode
     *            The unique site code for an account
     * @return the Account Primary key
     */

    public int getAccountPK(String siteCode) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int accountPk = 0;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select pk_account from er_account where lower(trim(SITE_CODE)) = lower(trim(?)) ");
            pstmt.setString(1, siteCode);

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("account", "AccountDao.getAccountPK ");

            if (rs != null) {
                rs.next();
                accountPk = rs.getInt("pk_account");
            }
        } catch (SQLException ex) {
            Rlog.fatal("account",
                    "AccountDao.getAccountPK  EXCEPTION IN FETCHING FROM Account table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return accountPk;
    }
    
    public void getLoginModuleDetails(int accountId)
    {
         PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select pk_loginmodule, loginmodule_dn "
                    + "from er_loginmodule "
                    + "where fk_account = ? "
                    + "order by pk_loginmodule ");

            pstmt.setInt(1,accountId);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("account", "AccountDao.getLoginModuleDetails()  ");
            while (rs.next()) {
                
                this.setAccLoginModuleDNList(rs.getString("loginmodule_dn"));
                this.setAccLoginModuleIdList(rs.getInt("pk_loginmodule"));
            	               
                
            }
            
            
        } catch (SQLException ex) {
            Rlog.fatal("account",
                    "AccountDao.getLoginModuleDetails EXCEPTION IN FETCHING FROM Account Login Module table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
}
    public void getLoginModule(int loginModuleId)
    {
         PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select loginmodule_dn,loginmodule_server,loginmodule_port,loginmodule_protocol "
                    + "from er_loginmodule "
                    + "where pk_loginmodule = ? ");

            pstmt.setInt(1,loginModuleId);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("account", "AccountDao.getLoginModule()  ");
            while (rs.next()) {
                
                this.setAccLoginModuleDNList(rs.getString("loginmodule_dn"));
                this.setAccLoginModulePortList(new Integer(rs.getString("loginmodule_port")));
                this.setAccLoginModuleServerList((rs.getString("loginmodule_server")));
                this.setAccLoginModuleProtocolList((rs.getString("loginmodule_protocol")));
                
            }
            
            
        } catch (SQLException ex) {
            Rlog.fatal("account",
                    "AccountDao.getLoginModule EXCEPTION IN FETCHING FROM Account Login Module table"
                            + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
}
    }