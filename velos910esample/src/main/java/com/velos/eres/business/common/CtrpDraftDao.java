//sajal dt 03/29/2001

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.velos.eres.service.util.Rlog;

public class CtrpDraftDao extends CommonDAO implements java.io.Serializable {

	private static final long serialVersionUID = 7158101413194792668L;
	
	public CtrpDraftDao() {    	
    
    }
    public int unmarkStudyCtrpReportable(String studyIds) {
    	 PreparedStatement pstmt = null;
         Connection conn = null;
         int ret=0;
         String sql="";
         try{
        	 conn = getConnection();
        	 sql="UPDATE ER_STUDY SET STUDY_CTRP_REPORTABLE=0 WHERE PK_STUDY IN "+studyIds;
        	 pstmt = conn.prepareStatement(sql);
       		 ret=pstmt.executeUpdate();
       		 conn.commit();
       	 return ret;
         }catch (SQLException ex) {
             Rlog.fatal("studyCtrp",
                     "CtrpDraftDao.unmarkStudyCtrpReportable EXCEPTION IN UPDATING Study table "
                             + ex);
             return ret;
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();
             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();
             } catch (Exception e) {
             }

         }
    	
    }
    public int deleteStudyDrafts(String studyIds) {
    	PreparedStatement pstmt = null;
    	Connection conn = null;
    	int ret=0;
    	String sql="";
    	try{
    		conn = getConnection();
    		sql="UPDATE ER_CTRP_DRAFT SET DELETED_FLAG=1 WHERE FK_STUDY IN "+studyIds;
    		pstmt = conn.prepareStatement(sql);
    		ret=pstmt.executeUpdate();
    		conn.commit();
    		return ret;
    	}catch (SQLException ex) {
    		Rlog.fatal("studyCtrp",
    				"CtrpDraftDao.deleteStudyDrafts EXCEPTION IN Deleting Draft from ER_CTRP_DRAFT table "
    				+ ex);
    		return ret;
    	} finally {
    		try {
    			if (pstmt != null)
    				pstmt.close();
    		} catch (Exception e) {
    		}
    		try {
    			if (conn != null)
    				conn.close();
    		} catch (Exception e) {
    		}
    		
    	}
    	
    }

   


   

}  
