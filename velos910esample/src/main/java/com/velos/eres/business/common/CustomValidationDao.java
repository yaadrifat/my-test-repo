/*
 * Classname			CustomValidationDao
 * 
 * Version information 	1.0
 *
 * Date					02/17/2005
 * 
 * Copyright notice		Velos, Inc.
 *
 * Author 				Gopu
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

public class CustomValidationDao extends CommonDAO implements
        java.io.Serializable {

    /**
     * CustomValidationDao for getting Custom Fields Information
     */

    private ArrayList pkPageCustom;

    private ArrayList fkAccount;

    private ArrayList pageCustomPage;

    private ArrayList pkpagecustomflds;

    private ArrayList pagecustomfldsfield;

    private ArrayList pagecustomfldsattribute;

    private ArrayList pagecustomfldsmandatory;
    
    private ArrayList pagecustomfldslabel;//KM-042308

    public CustomValidationDao() {
        pkPageCustom = new ArrayList();
        fkAccount = new ArrayList();
        pageCustomPage = new ArrayList();
        pkpagecustomflds = new ArrayList();
        pagecustomfldsfield = new ArrayList();
        pagecustomfldsattribute = new ArrayList();
        pagecustomfldsmandatory = new ArrayList();
        pagecustomfldslabel = new ArrayList();

    }

    // GETTER SETTER METHODS

    public ArrayList getPkPageCustom() {
        return this.pkPageCustom;
    }

    public void setPkPageCustom(String pkPageCustom) {
        this.pkPageCustom.add(pkPageCustom);
    }

    public ArrayList getFkAccount() {
        return this.fkAccount;
    }

    public void setFkAccount(String fkAccount) {
        this.fkAccount.add(fkAccount);
    }

    public ArrayList getPageCustomPage() {
        return this.pageCustomPage;
    }

    public void setPageCustomPage(String pageCustomPage) {
        this.pageCustomPage.add(pageCustomPage);
    }

    public ArrayList getPkpagecustomflds() {
        return this.pkpagecustomflds;
    }

    public void setPkpagecustomflds(String pkpagecustomflds) {
        this.pkpagecustomflds.add(pkpagecustomflds);
    }

    public ArrayList getPagecustomfldsfield() {
        return this.pagecustomfldsfield;
    }

    public void setPagecustomfldsfield(String pagecustomfldsfield) {
        this.pagecustomfldsfield.add(pagecustomfldsfield);
    }

    public ArrayList getPagecustomfldsattribute() {
        return this.pagecustomfldsattribute;
    }

    public void setPagecustomfldsattribute(String pagecustomfldsattribute) {
        this.pagecustomfldsattribute.add(pagecustomfldsattribute);
    }

    public ArrayList getPagecustomfldsmandatory() {
        return this.pagecustomfldsmandatory;
    }

    public void setPagecustomfldsmandatory(String pagecustomfldsmandatory) {
        this.pagecustomfldsmandatory.add(pagecustomfldsmandatory);
    }
    
   
    //Added by Manimaran for Enh.#GL9
    public ArrayList getPagecustomfldslabel() {
        return this.pagecustomfldslabel;
    }

    public void setPagecustomfldslabel(String pagecustomfldslabel) {
    	this.pagecustomfldslabel.add(pagecustomfldslabel);
    }

    

    public void getPageCustomFields() {

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select "
                    + "pk_pagecustom,fk_account,pagecustom_page "
                    + "from	er_pagecustom order by fk_account");
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("customAccount",
                    "CustomValidationDao.getPageCustomFields ");
            while (rs.next()) {
                String strArray[] = new String[3];
                setPkPageCustom(String.valueOf(rs.getInt("pk_pagecustom")));
                setFkAccount(String.valueOf(rs.getInt("fk_account")));
                setPageCustomPage(rs.getString("pagecustom_page"));
            }
            rs.close();
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "getPageCustomFields",
                            "CustomValidationDao.getPageCustomFields EXCEPTION IN FETCHING FROM er_pagecustom"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * CustomValidationDao for getting Custom Page Information based on some
     * retrieval criteria @ PrimaryKey which the custom page fields fetched @ Return
     * the Custom page fields
     */

    //KM-Label field added to change the name in GUI for #GL9
    public void getPageCustomDetails(int primaryKey) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select "
                            + "pk_pagecustomflds,pagecustomflds_field,pagecustomflds_attribute,pagecustomflds_mandatory,pagecustomflds_label "
                            + "from	er_pagecustomflds where fk_pagecustom ="
                            + primaryKey);
            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("customAccount",
                    "CustomValidationDao.getPageCustomDetails ");

            while (rs.next()) {
                setPkpagecustomflds(String.valueOf(rs
                        .getInt("pk_pagecustomflds")));
                setPagecustomfldsfield(rs.getString("pagecustomflds_field"));
                setPagecustomfldsattribute(rs
                        .getString("pagecustomflds_attribute"));
                setPagecustomfldsmandatory(rs
                        .getString("pagecustomflds_mandatory"));
                setPagecustomfldslabel(rs.getString("pagecustomflds_label"));//KM
            }
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "getPageCustomDetails",
                            "CustomValidationDao.getPageCustomDetails EXCEPTION IN FETCHING FROM er_pagecustom"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
}