/*
 * Classname			StdSiteRightsDao
 * 
 * Version information	1.0
 *
 * Date				11/17/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.Rlog;


/** 
 * StorageDao for getting storage records
 * 
 * @author Khader
 * @version : 1.0 07/17/2007
 */


public class StorageAllowedItemsDao extends CommonDAO implements java.io.Serializable {

	private ArrayList pkAllowedItems;
    private ArrayList fkStorages;
    private ArrayList<Integer> fkCodelstStorageTypes;
    private ArrayList<Integer> fkCodelstSpecimenTypes;
    private ArrayList allowedItemTypes;

    public StorageAllowedItemsDao() {
    	pkAllowedItems = new ArrayList();
    	fkStorages = new ArrayList();
    	fkCodelstStorageTypes = new ArrayList();
    	fkCodelstSpecimenTypes = new ArrayList();
    	allowedItemTypes = new ArrayList();
    	
   }

	public ArrayList getAllowedItemTypes() {
		return allowedItemTypes;
	}

	public void setAllowedItemTypes(Integer allowedItemTypes) {
		
		this.allowedItemTypes.add(allowedItemTypes);
	}

	public ArrayList<Integer> getFkCodelstSpecimenTypes() {
		return fkCodelstSpecimenTypes;
	}

	public void setFkCodelstSpecimenTypes(Integer fkCodelstSpecimenTypes) {
		this.fkCodelstSpecimenTypes.add(fkCodelstSpecimenTypes);
	}

	public ArrayList<Integer> getFkCodelstStorageTypes() {
		return fkCodelstStorageTypes;
	}

	public void setFkCodelstStorageTypes(Integer fkCodelstStorageTypes) {
		this.fkCodelstStorageTypes.add(fkCodelstStorageTypes) ;
	}

	public ArrayList getFkStorages() {
		return fkStorages;
	}

	public void setFkStorages(Integer fkStorages) {
		this.fkStorages.add(fkStorages); 
	}

	public ArrayList getPkAllowedItems() {
		return pkAllowedItems;
	}

	public void setPkAllowedItems(Integer pkAllowedItems) {
		this.pkAllowedItems.add(pkAllowedItems) ;
	}
	public void deleteStorageAllowedItems(int fkStorage) {    
		
	    PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("delete  from er_allowed_items where FK_STORAGE = ?");
            pstmt.setInt(1,fkStorage);            
            pstmt.execute();            	
                        
       } catch (SQLException ex) {
            Rlog.fatal("storageAllowedItems",
                    "StorageAllowedItemsDao.delete Allowed Conntents EXCEPTION IN deletion from er_allowed_items table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            	Rlog.fatal("storageAllowedItems",
                    "StorageAllowedItemsDao.delete Allowed Conntents EXCEPTION IN deletion from er_allowed_items table"
                                + e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

	// Bug #7267  AGodara
	public void deleteStorageAllowedItems(int fkStorage,ArrayList<Integer> oldSpecimenChckBoxList,String[] newSpecimenChckBoxArray,ArrayList<Integer> oldStorageChckBoxList,String[] newStorageChckBoxArray,Hashtable<String,String> auditInfo) {

		PreparedStatement pstmt = null;
		Connection conn = null;
		ArrayList<Integer> newSpecimenChckBoxArrayList = new ArrayList<Integer>();
		ArrayList<Integer> newStorageChckBoxArrayList = new ArrayList<Integer>();
		
		if (newSpecimenChckBoxArray != null) {
				for (String s : newSpecimenChckBoxArray) {
					newSpecimenChckBoxArrayList.add(Integer.parseInt(s));
			}
		}
		if (newStorageChckBoxArray != null) {
			for (String s : newStorageChckBoxArray) {
				newStorageChckBoxArrayList.add(Integer.parseInt(s));
			}
		}
		newSpecimenChckBoxArrayList.add(0);
		newStorageChckBoxArrayList.add(0);
		oldSpecimenChckBoxList.removeAll(newSpecimenChckBoxArrayList);
		oldStorageChckBoxList.removeAll(newStorageChckBoxArrayList);
			
	
		StringBuffer condition=new StringBuffer();
		condition.append("FK_STORAGE="+fkStorage);
		
		if (!oldSpecimenChckBoxList.isEmpty()) {
			condition.append(" AND (");
			condition.append("FK_CODELST_SPECIMEN_TYPE IN (");
			for (Integer i : oldSpecimenChckBoxList) {
				condition.append(i.toString() + ",");
			}
			condition.deleteCharAt(condition.length() - 1);
			condition.append(")");
			if(!oldStorageChckBoxList.isEmpty())
				condition.append(" OR ");
		}
		
		if(!oldStorageChckBoxList.isEmpty()){
			if (oldSpecimenChckBoxList.isEmpty())
				condition.append(" AND (");
		condition.append( " FK_CODELST_STORAGE_TYPE IN (" );
	
		for(Integer i:oldStorageChckBoxList){
			condition.append(i.toString()+",");
		}
		condition.deleteCharAt(condition.length()-1);
		condition.append( ")" );
		}
		condition.append( ")" );
		String tableName ="er_allowed_items";
		String schema ="eres";
		
		try{
			if(!oldSpecimenChckBoxList.isEmpty()||!oldStorageChckBoxList.isEmpty()){
			String app_module = (String)auditInfo.get(AuditUtils.APP_MODULE);
			Hashtable<String, ArrayList<String>> a = AuditUtils.getRowValues(tableName, condition.toString(), schema);
			conn = AuditUtils.insertAuditRow(tableName, a, app_module, auditInfo, schema);
			}else{
				conn = getConnection();
			}
			pstmt = conn
					.prepareStatement("delete from er_allowed_items where FK_STORAGE = ?");
			pstmt.setInt(1, fkStorage);
			pstmt.execute();
			conn.commit();

		} catch (Exception ex) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Rlog
					.fatal(
							"storageAllowedItems",
							"StorageAllowedItemsDao.delete Allowed Conntents EXCEPTION IN deletion from er_allowed_items table"
									+ ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
				Rlog
						.fatal(
								"storageAllowedItems",
								"StorageAllowedItemsDao.delete Allowed Conntents EXCEPTION IN deletion from er_allowed_items table"
										+ e);
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}

	}

	public void getStorageAllowedItemValue(int pkStorage) {
		PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String sql = " select PK_AI,FK_STORAGE,FK_CODELST_STORAGE_TYPE,FK_CODELST_SPECIMEN_TYPE,AI_ITEM_TYPE "
	  			+" from ER_ALLOWED_ITEMS where FK_STORAGE = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, pkStorage);
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {            	
               	setPkAllowedItems(new Integer(rs.getInt("PK_AI")));
               	setFkStorages(new Integer(rs.getInt("FK_STORAGE")));
               	setFkCodelstSpecimenTypes(new Integer(rs.getInt("FK_CODELST_SPECIMEN_TYPE")));
               	setFkCodelstStorageTypes(new Integer(rs.getInt("FK_CODELST_STORAGE_TYPE")));
               	setAllowedItemTypes(new Integer(rs.getInt("AI_ITEM_TYPE")));
            	
            }
            
       } catch (SQLException ex) {
            Rlog.fatal("storageAllowedItems",
                    "StorageAllowedItemsDao.getStorageAllowedItemValue EXCEPTION IN FETCHING FROM allowed items table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
        Rlog.fatal("storageAllowedItems","StorageAllowedItemsDao.getStorageAllowedItemValue EXCEPTION IN FETCHING FROM allowed items table"
                            + e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            	 Rlog.fatal("storageAllowedItems",
                         "StorageAllowedItemsDao.getStorageAllowedItemValue EXCEPTION IN FETCHING FROM allowed items table"
                                 + e);
            }

        }

    }
}