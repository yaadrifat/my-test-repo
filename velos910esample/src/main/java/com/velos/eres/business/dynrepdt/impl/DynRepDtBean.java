/*
 * Classname : DynRepDtBean
 * 
 * Version information: 1.0
 *
 * Date: 09/28/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol
 */

/*Modified by Sonia Abrol. 01/26/05, added a new attribute formType. 
 This attribute is added to distinguish between application forms and core table lookup forms*/

/*Modified by Sonia Abrol, 04/14/05, added new attributes, useUniqueId, useDataValue */

package com.velos.eres.business.dynrepdt.impl;

/* Import Statements */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The DynRepDtBean CMP entity bean.<br>
 * <br>
 * 
 * @author vishal abrol
 * @vesion 1.0 12/15/2003
 * @ejbHome DynRepDtHome
 * @ejbRemote DynRepDtRObj
 */

@Entity
@Table(name = "ER_DYNREPVIEW")
public class DynRepDtBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3904681587252672560L;

    /**
     * The primary key:pk_dynrepview
     */

    public int id;

    /**
     * The foreign key reference to er_dynrep table.
     */
    public Integer repId;

    /**
     * report name
     */
    public String repCol;

    /**
     * report column sequence
     */
    public Integer colSeq;

    /**
     * report col width
     */

    public String colWidth;

    /**
     * report Format
     */
    public String colFormat;

    /**
     * report dispname
     */
    public String colDisp;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    public String colName;

    public String colType;

    /**
     * The FormId with which report columns linked to
     */
    public String formId;

    /**
     * Distinguishes between application forms and core table lookup forms <br>
     * Possible Values: <br>
     * 'F' - Application forms <br>
     * 'C' - COre table lookup forms
     */
    public String formType;

    /**
     * Flag to Use 'Unique Field ID' as Display Name. possible values:
     * 0:true,1:false
     */
    public String useUniqueId;

    /**
     * Flag to Display Data value of multiple choice fields. possible values:
     * 0:true,1:false (default, display value will be shown)
     */

    public String useDataValue;
    
    /**
     *Flag to calculate summary information for this field - min/max/median/avg. Possible values-0:do not calculate, 1:calculate
     */
    public String calcSum;


    /**
     *Flag to calculate count/percentage information. Possible values-0:do not calculate,1:calculate
     */
    public String calcPer;
    

    /**
     *the repeat number count for the selected field
     */
    public String repeatNumber;
    
    
    
    @Column(name = "CALCPER")
    public String getCalcPer() {
		return calcPer;
	}

	public void setCalcPer(String calcPer) {
		 if (StringUtil.isEmpty(calcPer)) {
	            this.calcPer = "0";
	        }
		else
		{
			this.calcPer = calcPer;
		}
	}

	@Column(name = "CALCSUM")
	public String getCalcSum() {
		return calcSum;
	}

	public void setCalcSum(String calcSum) {
		 if (StringUtil.isEmpty(calcSum)) {
	            this.calcSum = "0";
	        }
		 else
		 {
			 this.calcSum = calcSum;
		 }
	}

	public DynRepDtBean() {

    }

    public DynRepDtBean(int id, String repId, String repCol, String colSeq,
            String colWidth, String colFormat, String colDisp, String creator,
            String modifiedBy, String ipAdd, String colName, String colType,
            String formId, String formType, String useUniqueId,
            String useDataValue,String calcSum, String calcPer,String repeat_Number) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setRepId(repId);
        setRepCol(repCol);
        setColSeq(colSeq);
        setColWidth(colWidth);
        setColFormat(colFormat);
        setColDisp(colDisp);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setColName(colName);
        setColType(colType);
        setFormId(formId);
        setFormType(formType);
        setUseUniqueId(useUniqueId);
        setUseDataValue(useDataValue);
        setCalcSum(calcSum);
        setCalcPer(calcPer);
        setRepeatNumber(repeat_Number);
    }

    // /////////////////////////////////////////////////////////////////////////////////
    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_DYNREPVIEW", allocationSize=1)
    @Column(name = "PK_DYNREPVIEW")
    public int getId() {
        return id;
    }

    public void setRepId(String repId) {
        if (!StringUtil.isEmpty(repId))
            this.repId = Integer.valueOf(repId);
    }

    @Column(name = "FK_DYNREP")
    public String getRepId() {
        return StringUtil.integerToString(repId);
    }

    public void setRepCol(String repCol) {
        this.repCol = repCol;
    }

    @Column(name = "DYNREPVIEW_COL")
    public String getRepCol() {
        return repCol;
    }

    public void setColSeq(String colSeq) {
        if (!StringUtil.isEmpty(colSeq))
            this.colSeq = Integer.valueOf(colSeq);
    }

    @Column(name = "DYNREPVIEW_SEQ")
    public String getColSeq() {
        return StringUtil.integerToString(colSeq);
    }

    public void setColWidth(String colWidth) {
        this.colWidth = colWidth;
    }

    @Column(name = "DYNREPVIEW_WIDTH")
    public String getColWidth() {
        return colWidth;
    }

    public void setColFormat(String colFormat) {
        this.colFormat = colFormat;
    }

    @Column(name = "DYNREPVIEW_FORMAT")
    public String getColFormat() {
        return colFormat;
    }

    public void setColDisp(String colDisp) {
        this.colDisp = colDisp;
    }

    @Column(name = "DYNREPVIEW_DISPNAME")
    public String getColDisp() {
        return colDisp;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    @Column(name = "DYNREPVIEW_COLNAME")
    public String getColName() {
        return colName;
    }

    @Column(name = "DYNREPVIEW_COLDATATYPE")
    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    /**
     * @return the Creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Returns the value of formId.
     */
    @Column(name = "FK_FORM")
    public String getFormId() {
        return formId;
    }

    /**
     * Sets the value of formId.
     * 
     * @param formId
     *            The value to assign formId.
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * Returns the value of formType.
     */
    @Column(name = "FORM_TYPE")
    public String getFormType() {
        return formType;
    }

    /**
     * Sets the value of formType.
     * 
     * @param formType
     *            The value to assign formType.
     */
    public void setFormType(String formType) {
        this.formType = formType;
    }

    /**
     * Returns the value of useUniqueId.
     */
    @Column(name = "USEUNIQUEID")
    public String getUseUniqueId() {
        return useUniqueId;
    }

    /**
     * Sets the value of useUniqueId.
     * 
     * @param useUniqueId
     *            The value to assign useUniqueId.
     */
    public void setUseUniqueId(String useUniqueId) {
        if (StringUtil.isEmpty(useUniqueId)) {
            useUniqueId = "0";
        }
        this.useUniqueId = useUniqueId;
    }

    /**
     * Returns the value of useDataValue.
     */
    @Column(name = "USEDATAVAL")
    public String getUseDataValue() {
        return useDataValue;
    }

    /**
     * Sets the value of useDataValue.
     * 
     * @param useDataValue
     *            The value to assign useDataValue.
     */
    public void setUseDataValue(String useDataValue) {
        if (StringUtil.isEmpty(useDataValue)) {
            useDataValue = "0";
        }

        this.useDataValue = useDataValue;
    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////




    /**
     * updates the Record
     * 
     * @param dsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateDynRepDt(DynRepDtBean dsk) {
        try {
            setRepId(dsk.getRepId());
            setRepCol(dsk.getRepCol());
            setColSeq(dsk.getColSeq());
            setColWidth(dsk.getColWidth());
            setColFormat(dsk.getColFormat());
            setColDisp(dsk.getColDisp());
            setCreator(dsk.getCreator());
            setModifiedBy(dsk.getModifiedBy());
            setIpAdd(dsk.getIpAdd());
            setColName(dsk.getColName());
            setColType(dsk.getColType());
            setFormId(dsk.getFormId());
            setFormType(dsk.getFormType());
            setUseUniqueId(dsk.getUseUniqueId());
            setUseDataValue(dsk.getUseDataValue());
            setCalcSum(dsk.getCalcSum());
            setCalcPer(dsk.getCalcPer());
            setRepeatNumber(dsk.getRepeatNumber());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("dynrep", " error in DynRepDtBean.updateDynRepDt" + e);
            return -2;
        }
    }

    @Column(name = "REPEAT_NUMBER")
    public String getRepeatNumber() {
		return repeatNumber;
	}

	public void setRepeatNumber(String repeatNumber) {
		this.repeatNumber = repeatNumber;
	}

}
