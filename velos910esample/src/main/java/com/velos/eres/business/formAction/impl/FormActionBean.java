package com.velos.eres.business.formAction.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

@Entity
@Table(name = "er_formaction")
public class FormActionBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public int formActionId;

	public String formActionName;

	public int formActionSourceFormId;

	public int formActionTargetFormId;

	public String formActionSourceFld;

	public String formActionTargetFld;

	public String formActionOperator;

	public int formActionFilledFormId;
	
	public String formActionMsg;

	// GETTER SETTER METHODS

	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FORMACTION", allocationSize=1)
	@Column(name = "PK_FORMACTION")
	public int getFormActionId() {
		return formActionId;
	}

	public void setFormActionId(int formActionId) {
		this.formActionId = formActionId;
	}

	@Column(name = "FORMACTION_FK_FILLEDFORM")
	public int getFormActionFilledFormId() {
		return formActionFilledFormId;
	}

	public void setFormActionFilledFormId(int formActionFilledFormId) {
		this.formActionFilledFormId = formActionFilledFormId;
	}

	@Column(name = "FORMACTION_NAME")
	public String getFormActionName() {
		return formActionName;
	}

	public void setFormActionName(String formActionName) {
		this.formActionName = formActionName;
	}

	@Column(name = "FORMACTION_OPERATOR")
	public String getFormActionOperator() {
		return formActionOperator;
	}

	public void setFormActionOperator(String formActionOperator) {
		this.formActionOperator = formActionOperator;
	}

	@Column(name = "FORMACTION_SOURCEFLD")
	public String getFormActionSourceFld() {
		return formActionSourceFld;
	}

	public void setFormActionSourceFld(String formActionSourceFld) {
		this.formActionSourceFld = formActionSourceFld;
	}

	@Column(name = "FORMACTION_SOURCEFORM")
	public int getFormActionSourceFormId() {
		return formActionSourceFormId;
	}

	public void setFormActionSourceFormId(int formActionSourceFormId) {
		this.formActionSourceFormId = formActionSourceFormId;
	}

	@Column(name = "FORMACTION_TARGETFLD")
	public String getFormActionTargetFld() {
		return formActionTargetFld;
	}

	public void setFormActionTargetFld(String formActionTargetFld) {
		this.formActionTargetFld = formActionTargetFld;
	}

	@Column(name = "FORMACTION_TARGETFORM")
	public int getFormActionTargetFormId() {
		return formActionTargetFormId;
	}

	public void setFormActionTargetFormId(int formActionTargetFormId) {
		this.formActionTargetFormId = formActionTargetFormId;
	}
	@Column(name = "FORMACTION_MSG")
	public String getFormActionMsg() {
		return formActionMsg;
	}

	public void setFormActionMsg(String formActionMsg) {
		this.formActionMsg = formActionMsg;
	}


	public FormActionBean(int formActionId, String formActionName,
			String formActionOperator, String formActionSourceFld,
			int formActionSourceFormId, String formActionTargetFld,
			int formActionTargetFormId, int formActionFilledFormId,String formActionMsg) {

		setFormActionFilledFormId(formActionFilledFormId);
		setFormActionId(formActionId);
		setFormActionName(formActionName);
		setFormActionOperator(formActionOperator);
		setFormActionSourceFld(formActionSourceFld);
		setFormActionSourceFormId(formActionSourceFormId);
		setFormActionTargetFld(formActionTargetFld);
		setFormActionTargetFormId(formActionTargetFormId);
		setFormActionMsg(formActionMsg);

	} 

	public FormActionBean() {
	}

	public int updateFormAction(FormActionBean fask) {
		try {

			setFormActionFilledFormId(fask.getFormActionFilledFormId());
			setFormActionId(fask.getFormActionId());
			setFormActionName(fask.getFormActionName());
			setFormActionOperator(fask.getFormActionOperator());
			setFormActionSourceFld(fask.getFormActionSourceFld());
			setFormActionSourceFormId(fask.getFormActionSourceFormId());
			setFormActionTargetFld(fask.getFormActionTargetFld());
			setFormActionTargetFormId(fask.getFormActionTargetFormId());
			setFormActionMsg(fask.getFormActionMsg());

			Rlog.debug("formaction", "FormActionBean.updateFormAction");
			return 0;
		} catch (Exception e) {
			Rlog.fatal("formaction",
					" error in FormActionBean.updateFormAction");
			return -2;
		}
	}

}// end of class
