/*
 * Classname : FormSecBean
 * 
 * Version information: 1.0
 *
 * Date: 17/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.business.formSec.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The FormSec CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Kaura
 * @vesion 1.0 17/07/2003
 * @ejbHome FormSecHome
 * @ejbRemote FormSecRObj
 */
@Entity
@Table(name = "er_formsec")
@NamedQuery(name = "findByFormIdAndSeq", query = "SELECT OBJECT(sec) FROM FormSecBean sec where fk_formlib = :fk_formlib  AND formsec_seq = :formsec_seq")
public class FormSecBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3760565308103931448L;

    /**
     * The form primary key:pk_formsec
     */

    public int formSecId;

    /**
     * The foreign key reference from the Field : FK_FORMLIB
     */
    public Integer formLibId;

    /**
     * The Msg Type, Pop or Notification : FORMSEC_NAME
     */

    public String formSecName;

    /**
     * The Sequence number of the Field: FORMSEC_SEQ
     */
    public Integer formSecSeq;

    /**
     * Stores section format - tabular / non-tabular fields: FORMSEC_FMT
     * 
     */

    public String formSecFmt;

    /**
     * No. of section times fields will be repeated within the section:
     * FORMSEC_REPNO
     * 
     */
    public Integer formSecRepNo;

    /**
     * The Record Type : Record_Type
     */

    public String recordType;

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    public Integer offlineFlag;

    // //////////////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     * @return formSecId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_FORMSEC", allocationSize=1)
    @Column(name = "PK_FORMSEC")
    public int getFormSecId() {
        return formSecId;
    }

    /**
     * 
     * 
     * @param formSecId
     */
    public void setFormSecId(int formSecId) {
        this.formSecId = formSecId;
    }

    /**
     * 
     * 
     * @return formLibId
     */
    @Column(name = "FK_FORMLIB")
    public String getFormLibId() {
        return StringUtil.integerToString(formLibId);

    }

    /**
     * 
     * 
     * @param formLibId
     */
    public void setFormLibId(String formLibId) {

        if (!StringUtil.isEmpty(formLibId))
            this.formLibId = Integer.valueOf(formLibId);
    }

    /**
     * 
     * 
     * @return formSecName
     */
    @Column(name = "FORMSEC_NAME")
    public String getFormSecName() {

        return formSecName;

    }

    /**
     * 
     * 
     * @param formSecName
     */
    public void setFormSecName(String formSecName) {
        this.formSecName = formSecName;

    }

    /**
     * 
     * 
     * @return formSecSeq
     */
    @Column(name = "FORMSEC_SEQ")
    public String getFormSecSeq() {

        return StringUtil.integerToString(formSecSeq);

    }

    /**
     * 
     * 
     * @param formSecSeq
     */
    public void setFormSecSeq(String formSecSeq) {

        if (!StringUtil.isEmpty(formSecSeq))
            this.formSecSeq = Integer.valueOf(formSecSeq);

    }

    /**
     * 
     * 
     * @return formSecFmt
     */
    @Column(name = "FORMSEC_FMT")
    public String getFormSecFmt() {

        return formSecFmt;
    }

    /**
     * 
     * 
     * @param formSecFmt
     */
    public void setFormSecFmt(String formSecFmt) {

        this.formSecFmt = formSecFmt;

    }

    /**
     * 
     * 
     * @return formSecRepNo
     */
    @Column(name = "FORMSEC_REPNO")
    public String getFormSecRepNo() {

        return StringUtil.integerToString(formSecRepNo);

    }

    /**
     * 
     * 
     * @param formSecRepNo
     */
    public void setFormSecRepNo(String formSecRepNo) {
        if (!StringUtil.isEmpty(formSecRepNo))
            this.formSecRepNo = Integer.valueOf(formSecRepNo);

    }

    /**
     * @return the Record Type : m: modified n:new d: delet
     */
    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    /**
     * @param set
     *            the Record Type to be modified,new or delete
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;

    }

    /**
     * @return the Creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the unique ID of the User
     */

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return the offline Flag
     */
    @Column(name = "FORMSEC_OFFLINE")
    public String getOfflineFlag() {
        return StringUtil.integerToString(this.offlineFlag);
    }

    /**
     * @param set
     *            the offline Flag
     */

    public void setOfflineFlag(String offlineFlag) {
        if (offlineFlag != null) {
            this.offlineFlag = Integer.valueOf(offlineFlag);
        }

    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////

    /*
     * public FormSecStateKeeper getFormSecStateKeeper() {
     * Rlog.debug("formsection", "FormSecBean.getFormSecStateKeeper"); return
     * (new FormSecStateKeeper(getFormSecId(), getFormLibId(), getFormSecName(),
     * getFormSecSeq(), getFormSecFmt(), getFormSecRepNo(), getRecordType(),
     * getCreator(), getModifiedBy(), getIpAdd(), getOfflineFlag())); }
     */

    /**
     * sets up a state keeper containing details of the form field
     */

    /*
     * public void setFormSecStateKeeper(FormSecStateKeeper formSecsk) {
     * GenerateId formSId = null; try { Connection conn = null; conn =
     * getConnection(); Rlog.debug("formsection",
     * "FormSecBean.setFormSecStateKeeper() Connection :" + conn);
     * this.formSecId = formSId.getId("SEQ_ER_FORMSEC", conn);
     * Rlog.debug("formsection", "FormSecBean.setFormSecStateKeeper() formSecId :" +
     * this.formSecId); conn.close(); //
     * Rlog.debug("formsection","formSecsk.getFormLibId() :"+ //
     * formSecsk.getFormLibId()); setFormLibId(formSecsk.getFormLibId()); //
     * Rlog.debug("formsection","formSecsk.getFormSecName() :"+ //
     * formSecsk.getFormSecName()); setFormSecName(formSecsk.getFormSecName()); //
     * Rlog.debug("formsection","formSecsk.getFormSecSeq() :"+ //
     * formSecsk.getFormSecSeq()); setFormSecSeq(formSecsk.getFormSecSeq()); //
     * Rlog.debug("formsection","formSecsk.getFormSecFmt() :"+ //
     * formSecsk.getFormSecFmt()); setFormSecFmt(formSecsk.getFormSecFmt()); //
     * Rlog.debug("formfield","formSecsk.getFormSecRepNo():"+ //
     * formSecsk.getFormSecRepNo());
     * setFormSecRepNo(formSecsk.getFormSecRepNo()); //
     * Rlog.debug("formsection","formSecsk.getRecordType() :"+ //
     * formSecsk.getRecordType()); setRecordType(formSecsk.getRecordType()); //
     * Rlog.debug("formsection","formSecsk.getCreator() :"+ //
     * formSecsk.getCreator()); setCreator(formSecsk.getCreator()); //
     * Rlog.debug("formsection","formSecsk.getIpAdd() :"+ //
     * formSecsk.getIpAdd()); setIpAdd(formSecsk.getIpAdd());
     * 
     * setOfflineFlag(formSecsk.getOfflineFlag()); } catch (Exception e) {
     * Rlog.fatal("formsection", "Error in setFormSecStateKeeper() in
     * FormSecBean " + e); } }
     */

    /**
     * updates the Form Sec Record
     * 
     * @param formSecsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateFormSec(FormSecBean formSecsk) {
        try {

            setFormSecId(formSecsk.getFormSecId());
            setFormLibId(formSecsk.getFormLibId());
            setFormSecName(formSecsk.getFormSecName());
            setFormSecSeq(formSecsk.getFormSecSeq());
            setFormSecFmt(formSecsk.getFormSecFmt());
            setFormSecRepNo(formSecsk.getFormSecRepNo());
            setRecordType(formSecsk.getRecordType());
            setCreator(formSecsk.getCreator());
            setModifiedBy(formSecsk.getModifiedBy());
            setIpAdd(formSecsk.getIpAdd());
            setOfflineFlag(formSecsk.getOfflineFlag());

            return 0;
        } catch (Exception e) {
            Rlog
                    .fatal("formsection",
                            " error in FormFieldBean.updateFormField");
            return -2;
        }
    }

    public FormSecBean() {

    }

    public FormSecBean(int formSecId, String formLibId, String formSecName,
            String formSecSeq, String formSecFmt, String formSecRepNo,
            String recordType, String creator, String modifiedBy, String ipAdd,
            String offlineFlag) {
        super();
        // TODO Auto-generated constructor stub
        setFormSecId(formSecId);
        setFormLibId(formLibId);
        setFormSecName(formSecName);
        setFormSecSeq(formSecSeq);
        setFormSecFmt(formSecFmt);
        setFormSecRepNo(formSecRepNo);
        setRecordType(recordType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setOfflineFlag(offlineFlag);
    }

}
