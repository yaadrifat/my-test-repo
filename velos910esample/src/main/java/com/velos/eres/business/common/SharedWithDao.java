/*
 * Classname : 		SharedWithDao
 * 
 * Version information: 1.0 
 *
 * Date: 			07/04/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: 			Anu Khanna
 */

package com.velos.eres.business.common;

/* Import Statements */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The SharedWith Dao.
 * 
 * @author Anu Khanna
 * @vesion 1.0 07/04/2003
 */

public class SharedWithDao extends CommonDAO implements java.io.Serializable {

    private ArrayList arrFormLibIds;

    private ArrayList arrSharedWithIds;

    private ArrayList sharedWithNames;

    private ArrayList sharedWithIds;

    private String[] saFormLibIds;

    private String[] saSharedWithIds;

    private String creator;

    private String ipAdd;

    private String lastModifiedBy;

    private String recordType;

    private String sharedWith;

    private String fswType;

    private int cRows;

    private int formLibId;

    private int fsw_id;

    private String fsw_type;

    public SharedWithDao() {
        sharedWithNames = new ArrayList();
        arrSharedWithIds = new ArrayList();
        arrFormLibIds = new ArrayList();
        sharedWithIds = new ArrayList();

        int creator;
        String fswType;

        String ipAdd;
        int lastModifiedBy;
        int field;
        String recordType;
        int cRows;

    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public int getFormLibId() {
        return this.formLibId;
    }

    public ArrayList getArrFormLibIds() {
        return this.arrFormLibIds;
    }

    public void setArrFormLibIds(ArrayList arrFormLibIds) {
        this.arrFormLibIds = arrFormLibIds;
    }

    public ArrayList getSharedWithIds() {
        return this.sharedWithIds;
    }

    public void setSharedWithIds(ArrayList sharedWithIds) {
        this.sharedWithIds = sharedWithIds;
    }

    public ArrayList getSharedWithNames() {
        return this.sharedWithNames;
    }

    public void setSharedWithNames(ArrayList sharedWithNames) {
        this.sharedWithNames = sharedWithNames;
    }

    /* getter setter for string type */

    public String[] getFormLibIds() {
        return this.saFormLibIds;
    }

    public void setFormLibIds(String[] saFormLibIds) {
        this.saFormLibIds = saFormLibIds;
    }

    public void setFswType(String fwdType) {
        this.fswType = fswType;
    }

    public String getFswType() {
        return this.fswType;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setSharedWithIds(Integer sharedWithId) {
        sharedWithIds.add(sharedWithId);
    }

    public void setSharedWithNames(String sharedWithName) {
        sharedWithNames.add(sharedWithName);
    }

    /**
     * TO GET THE SHAREDWITH INFORMATION DEPENDING WHETHER IT IS A GROUP , STUDY
     * OR ORGANIZATION.
     */

    public void getSharedWithInfo(int formId, String type) {

        Rlog.debug("formlib", "anuform inside getSharedWithIds");
        int ret = 1;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            Rlog.debug("formlib", "In getSharedWithIds () ");
            if (type.equals("G")) {
                sql = " select grp_name , pk_grp from er_grps, er_formsharewith "
                        + " where er_grps.pk_grp=er_formsharewith.fsw_id "
                        + " and er_formsharewith.fk_formlib= ? and "
                        + " er_formsharewith.fsw_type= ?  ";

                Rlog.debug("formlib", "anu1 sql" + sql);

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, formId);
                pstmt.setString(2, type);

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("GRP_NAME"));
                    setSharedWithIds(new Integer(rs.getInt("PK_GRP")));
                    rows++;
                }
                setCRows(rows);
                Rlog.debug("formlib", "number of rows" + rows);
            }

            else if (type.equals("S")) {
                sql = " select study_number , pk_study from er_study, er_formsharewith "
                        + " where er_study.pk_study=er_formsharewith.fsw_id "
                        + " and er_formsharewith.fk_formlib= ? and "
                        + " er_formsharewith.fsw_type= ? ";

                Rlog.debug("formlib", "anu1 sql" + sql);

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, formId);
                pstmt.setString(2, type);

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("STUDY_NUMBER"));
                    setSharedWithIds(new Integer(rs.getInt("PK_STUDY")));
                    rows++;
                }

                setCRows(rows);
                Rlog.debug("formlib", "number of rows" + rows);
            }

            else if (type.equals("O")) {
                sql = " select site_name , pk_site from er_site, er_formsharewith "
                        + " where er_site.pk_site=er_formsharewith.fsw_id "
                        + " and er_formsharewith.fk_formlib= ? and "
                        + " er_formsharewith.fsw_type= ? ";

                Rlog.debug("formlib", "anu1 sql" + sql);

                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, formId);
                pstmt.setString(2, type);

                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    setSharedWithNames(rs.getString("SITE_NAME"));
                    setSharedWithIds(new Integer(rs.getInt("PK_SITE")));
                    rows++;
                }
                setCRows(rows);
                Rlog.debug("formlib", "number of rows" + rows);
            }

        } catch (SQLException ex) {
            Rlog.fatal("formlib",
                    "SharedWithDao.getSharedWithIds EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
