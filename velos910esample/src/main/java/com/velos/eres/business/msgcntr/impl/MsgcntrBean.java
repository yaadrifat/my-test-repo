/*
 * Classname			MsgcntrBean
 * 
 * Version information	1.0
 *
 * Date					02/27/2001
 * 
 * Copyright notice		Velos Inc.
 */

package com.velos.eres.business.msgcntr.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Msgcntr CMP entity bean.<br>
 * <br>
 * 
 * @author Sajal
 * @version 1.0 02/27/2001
 * @ejbHome msgcntrHome
 * @ejbRemote msgcntrRemote
 */
@Entity
@Table(name = "er_msgcntr")
// @NamedQuery(name = "findByMsgsPendingValidation", queryString = "SELECT
// OBJECT(msgcntr) FROM MsgcntrBean msgcntr where MSGCNTR_STAT IS NOT NULL")
public class MsgcntrBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3258695390302974519L;

    /**
     * the msgcntr Id
     */
    public int msgcntrId;

    /**
     * msgcntr studyId
     */
    public Integer msgcntrStudyId;

    /**
     * msgcntr from userId
     */
    public Integer msgcntrFromUserId;

    /**
     * msgcntr to userId
     */
    public Integer msgcntrToUserId;

    /**
     * msgcntr text
     */
    public String msgcntrText;

    /**
     * msgcntr request type
     */
    public String msgcntrReqType;

    /**
     * msgcntr Status
     */
    public String msgcntrStatus;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    /**
     * Returns Message Center Id
     * 
     * @return message center id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_MSGCNTR", allocationSize=1)
    @Column(name = "PK_MSGCNTR")
    public int getMsgcntrId() {
        return this.msgcntrId;
    }

    public void setMsgcntrId(int msgcntrId) {
        this.msgcntrId = msgcntrId;
    }

    /**
     * Returns Study id
     * 
     * @return study id
     */
    @Column(name = "fk_study")
    public String getMsgcntrStudyId() {
        return StringUtil.integerToString(this.msgcntrStudyId);
    }

    /**
     * Sets study id
     * 
     * @param msgcntrStudyId
     *            study id
     */
    public void setMsgcntrStudyId(String msgcntrStudyId) {
        if (msgcntrStudyId != null) {
            this.msgcntrStudyId = Integer.valueOf(msgcntrStudyId);
        }
    }

    /**
     * Returns Id of the user from whom message is received
     * 
     * @return user id of the user who has sent the message
     */
    @Column(name = "fk_userx")
    public String getMsgcntrFromUserId() {
        return StringUtil.integerToString(this.msgcntrFromUserId);
    }

    /**
     * Set Id of the user who has sent the message
     * 
     * @param msgcntrFromUserId
     *            User id of the user who has sent the message
     */
    public void setMsgcntrFromUserId(String msgcntrFromUserId) {
        if (msgcntrFromUserId != null) {
            this.msgcntrFromUserId = Integer.valueOf(msgcntrFromUserId);
        }
    }

    /**
     * Returns User Id of the user to whom message has been sent
     * 
     * @return Id of the user to whom message has been sent
     */
    @Column(name = "fk_userto")
    public String getMsgcntrToUserId() {
        return StringUtil.integerToString(this.msgcntrToUserId);
    }

    /**
     * Sets the user id of the user to whom message has been sent
     * 
     * @param msgcntrToUserId
     *            id of the user to whom message has been sent
     */
    public void setMsgcntrToUserId(String msgcntrToUserId) {
        if (msgcntrToUserId != null) {
            this.msgcntrToUserId = Integer.valueOf(msgcntrToUserId);
        }
    }

    /**
     * Returns the message Text
     * 
     * @return text of the message
     */
    @Column(name = "msgcntr_text")
    public String getMsgcntrText() {
        return this.msgcntrText;
    }

    /**
     * Sets the text of the message
     * 
     * @param msgcntrText
     *            Text of the message
     */
    public void setMsgcntrText(String msgcntrText) {
        this.msgcntrText = msgcntrText;
    }

    /**
     * Returns the Type of Request
     * 
     * @return 'S' for messages related to study, 'E' for messages related to
     *         external users, 'A' for messages related to users signup
     */
    @Column(name = "msgcntr_reqtype")
    public String getMsgcntrReqType() {
        return this.msgcntrReqType;
    }

    /**
     * Sets the type of Request
     * 
     * @param msgcntrReqType
     *            Request Type. 'S' for messages related to study, 'E' for
     *            messages related to external users, 'A' for messages related
     *            to users signup
     */
    public void setMsgcntrReqType(String msgcntrReqType) {
        this.msgcntrReqType = msgcntrReqType;
    }

    /**
     * Returns Message status
     * 
     * @return Status of messages. 'R' for Unread messages, 'U' for UnRead
     *         messages
     */
    @Column(name = "msgcntr_stat")
    public String getMsgcntrStatus() {
        return this.msgcntrStatus;
    }

    /**
     * Sets the message status
     * 
     * @param msgcntrStatus
     *            'R' for Unread messages, 'U' for UnRead messages
     */
    public void setMsgcntrStatus(String msgcntrStatus) {
        this.msgcntrStatus = msgcntrStatus;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this msgcntr
     * 
     * @return Message Center State Keeper which holds the details of the
     *         messages
     */
    /*
     * public MsgcntrStateKeeper getMsgcntrStateKeeper() { Rlog.debug("msgcntr",
     * "MsgcntrBean.getMsgcntrStateKeeper"); return (new
     * MsgcntrStateKeeper(getMsgcntrId(), getMsgcntrStudyId(),
     * getMsgcntrFromUserId(), getMsgcntrToUserId(), getMsgcntrText(),
     * getMsgcntrReqType(), getMsgcntrStatus(), getCreator(), getModifiedBy(),
     * getIpAdd())); }
     */

    /**
     * sets up a state keeper containing details of the msgcntr
     * 
     * @param msgsk
     */
    /*
     * public void setMsgcntrStateKeeper(MsgcntrStateKeeper msgsk) { GenerateId
     * msgcntrId = null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("msgcntr", "MsgcntrBean.setMsgcntrStateKeeper() Connection :" +
     * conn); this.msgcntrId = msgcntrId.getId("SEQ_ER_MSGCNTR", conn);
     * Rlog.debug("msgcntr", "MsgcntrBean.setMsgcntrStateKeeper() msgcntrId :" +
     * this.msgcntrId); conn.close();
     * setMsgcntrStudyId(msgsk.getMsgcntrStudyId()); Rlog.debug("msgcntr",
     * "MsgcntrBean.setMsgcntrStateKeeper() msgcntrStudyId :" + msgcntrStudyId);
     * setMsgcntrFromUserId(msgsk.getMsgcntrFromUserId()); Rlog.debug("msgcntr",
     * "MsgcntrBean.setMsgcntrStateKeeper() msgcntrFromUserId :" +
     * msgcntrFromUserId); setMsgcntrToUserId(msgsk.getMsgcntrToUserId());
     * Rlog.debug("msgcntr", "MsgcntrBean.setMsgcntrStateKeeper()
     * msgcntrToUserId :" + msgcntrToUserId);
     * setMsgcntrText(msgsk.getMsgcntrText()); Rlog.debug("msgcntr",
     * "MsgcntrBean.setMsgcntrStateKeeper() msgcntrText :" + msgcntrText);
     * setMsgcntrReqType(msgsk.getMsgcntrReqType()); Rlog.debug("msgcntr",
     * "MsgcntrBean.setMsgcntrStateKeeper() msgcntrReqType :" + msgcntrReqType);
     * setMsgcntrStatus(msgsk.getMsgcntrStatus()); Rlog.debug("msgcntr",
     * "MsgcntrBean.setMsgcntrStatus() msgcntrStatus :" + msgcntrStatus);
     * setCreator(msgsk.getCreator()); setModifiedBy(msgsk.getModifiedBy());
     * setIpAdd(msgsk.getIpAdd()); } catch (Exception e) { Rlog.fatal("msgcntr",
     * "Error in setMsgcntrStateKeeper() in MsgcntrBean " + e); } }
     */

    /**
     * Updates details of the msgcntr
     * 
     * @param msgsk
     *            Message Center containing the modified details of the message
     *            Center
     * @return 0 when updation fails and -2 when exception occurs
     */
    public int updateMsgcntr(MsgcntrBean msgsk) {
        try {
            setMsgcntrStudyId(msgsk.getMsgcntrStudyId());
            setMsgcntrFromUserId(msgsk.getMsgcntrFromUserId());
            setMsgcntrToUserId(msgsk.getMsgcntrToUserId());
            setMsgcntrText(msgsk.getMsgcntrText());
            setMsgcntrReqType(msgsk.getMsgcntrReqType());
            setMsgcntrStatus(msgsk.getMsgcntrStatus());
            setCreator(msgsk.getCreator());
            setModifiedBy(msgsk.getModifiedBy());
            setIpAdd(msgsk.getIpAdd());
            Rlog.debug("msgcntr", "MsgcntrBean.updateMsgcntr");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("msgcntr", " error in MsgcntrBean.updateMsgcntr");
            return -2;
        }
    }

    public MsgcntrBean() {
    }

    public MsgcntrBean(int msgcntrId, String msgcntrStudyId,
            String msgcntrFromUserId, String msgcntrToUserId,
            String msgcntrText, String msgcntrReqType, String msgcntrStatus,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setMsgcntrId(msgcntrId);
        setMsgcntrStudyId(msgcntrStudyId);
        setMsgcntrFromUserId(msgcntrFromUserId);
        setMsgcntrToUserId(msgcntrToUserId);
        setMsgcntrText(msgcntrText);
        setMsgcntrReqType(msgcntrReqType);
        setMsgcntrStatus(msgcntrStatus);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
