/*
 * Classname : studyNIHGrantBean
 * 
 * Version information: 1.0
 *
 * Date: 11/21/2010
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.business.studyNIHGrant.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
@Entity
@Table(name = "ER_STUDY_NIH_GRANT")
@NamedQuery(name = "findStudyNIHGrantId", query = "SELECT OBJECT(studyNIHGrant) FROM StudyNIHGrantBean studyNIHGrant where UPPER(trim(STORAGE_ID)) = UPPER(trim(:storageId)) and FK_ACCOUNT = :accountId")
public class StudyNIHGrantBean implements Serializable {
	
	 /**
     * the studyNIHGrantId
     */
	public Integer studyNIHGrantId;
	
	 /**
     * the study Id
     */
	
	public Integer studyId;
	
	 /**
     * the Funding Mechanism Type
     */
	
	
	public Integer fundMech;
	/**
     * the Institude Code
     */
	
	public Integer instCode;
	
	/**
     * the NCI PRogram Code
     */
	
	public Integer programCode;
	
	/**
     * the NIH grant Serial Number
     */
	
	public String   nihGrantSerial;
	 /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;
    
    public StudyNIHGrantBean() {

    }

    public StudyNIHGrantBean(Integer studyNIHGrantId, String studyId, String fundMech,
            String instCode, String programCode, String nihGrantSerial,
             String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setStudyNIHGrantId(studyNIHGrantId);
        setStudyId(studyId);
        setFundMech(fundMech);
        setInstCode(instCode);
        setProgramCode(programCode);
        setNihGrantSerial(nihGrantSerial);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
	@SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDY_NIH_GRANT", allocationSize=1)
	@Column(name = "PK_STUDY_NIH_GRANT")
	
	 /**
     * Get studyNIHGrantId
     * 
     * @return studyNIHGrantId
     */
	public Integer getStudyNIHGrantId() {
		return studyNIHGrantId;
	}

	public void setStudyNIHGrantId(Integer studyNIHGrantId) {
		this.studyNIHGrantId = studyNIHGrantId;
	}
	
	 /**
     * Get studyNIHGrantId 
     * 
     * @return studyNIHGrantId
     */
	 @Column(name = "FK_STUDY")
	 public String getStudyId() {
	        return StringUtil.integerToString(this.studyId);
	 }

	    /**
	     * sets Study Id
	     * 
	     * @param studyId
	     */
	 public void setStudyId(String studyId) {
	        this.studyId = StringUtil.stringToInteger(studyId);
	 }
	 
	  /**
	     * sets Funding mechanism
	     * 
	     * @param fundMech
	     */
	 
	  @Column(name = "FK_CODELST_FUNDMECH")
	    public String getFundMech() {
	        return StringUtil.integerToString(this.fundMech);
	    }

	    public void setFundMech(String fundMech) {

	        this.fundMech = StringUtil.stringToInteger(fundMech);
	    }
	    
	    
	    /**
	     * sets Institude Code
	     * 
	     * @param instCode
	     */
	    
	    @Column(name = "FK_CODELST_INSTCODE")
	    public String getInstCode() {
	        return StringUtil.integerToString(this.instCode);
	    }

	    public void setInstCode(String instCode) {

	        this.instCode = StringUtil.stringToInteger(instCode);
	    }
	    
	    /**
	     * sets NCI division Program Code
	     * 
	     * @param programCode
	     */
	    
	    
	    @Column(name = "FK_CODELST_PROGRAM_CODE")
	    public String getProgramCode() {
	        return StringUtil.integerToString(this.programCode);
	    }

	    public void setProgramCode(String programCode) {

	        this.programCode = StringUtil.stringToInteger(programCode);
	    }

	    /**
	     * sets NIH Serial Number
	     * 
	     * @param nihGrantSerial
	     */
	    
	    @Column(name = "NIH_GRANT_SERIAL")
	    public String getNihGrantSerial() {
	        return this.nihGrantSerial;
	    }

	    public void setNihGrantSerial(String nihGrantSerial) {
	        this.nihGrantSerial = nihGrantSerial;
	    }
	    
	    @Column(name = "IP_ADD")
	    public String getIpAdd() {
	        return this.ipAdd;
	    }

	    public void setIpAdd(String ipAdd) {
	        this.ipAdd = ipAdd;
	    }
	    
	    
	    @Column(name = "CREATOR")
	    public String getCreator() {
	        return StringUtil.integerToString(this.creator);
	    }

	    public void setCreator(String creator) {
	        if (creator != null) {
	            this.creator = Integer.valueOf(creator);
	        }
	    }

	    @Column(name = "LAST_MODIFIED_BY")
	    public String getModifiedBy() {
	        return StringUtil.integerToString(this.modifiedBy);
	    }

	    public void setModifiedBy(String modifiedBy) {
	        if (modifiedBy != null) {
	            this.modifiedBy = Integer.valueOf(modifiedBy);
	        }
	    }
	

		public int updateStudyNIHGrant(StudyNIHGrantBean ssk) {
				try{
					
					if(ssk.getStudyNIHGrantId()>0){
					setStudyNIHGrantId(ssk.getStudyNIHGrantId());}
					setStudyId(ssk.getStudyId());
					setFundMech(ssk.getFundMech());
					setInstCode(ssk.getInstCode());
					setProgramCode(ssk.getProgramCode());
					setNihGrantSerial(ssk.getNihGrantSerial());
					setCreator(ssk.getCreator());
			        setModifiedBy(ssk.getModifiedBy());
			        setIpAdd(ssk.getIpAdd());
					return 0;
				}catch(Exception e){
			           Rlog.fatal("studyNIHGrant", " error in StudyNIHGrantBean.updateStudyNIHGrantt");
			           return -2;				
				}
				
		}
	   

}
