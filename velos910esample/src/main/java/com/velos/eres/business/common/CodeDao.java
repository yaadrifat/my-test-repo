/*
 *  Classname : CodeDao
 *
 *  Version information: 1.0
 *
 *  Copyright notice: Velos, Inc
 *  date: 03/08/2001
 *
 *  Author: sonia sahni
 *  Data Access class for codelist data
 */

package com.velos.eres.business.common;

/*
 * import statements
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/*
 * import statements
 */
/**
 * The CodeDao.<br>
 * <br>
 * data Access Class for all CodeList Data
 *
 * @author Sonia Sahni
 * @created May 10, 2005
 * @vesion 1.0 03/08/2001
 */
/*
 * Modified by Sonia Sahni on 11/01/04 , overloaded method getCodeValues() :
 * getCodeValues(String codeType, String customColValue) , to get codelst data
 * for codelst_type and codelst_custom_col
 */
public class CodeDao extends CommonDAO implements java.io.Serializable {
    private ArrayList cId;

    private ArrayList cDesc;

    private ArrayList cSeq;

    private ArrayList cSubType;

    private ArrayList codeCustom;

    private ArrayList codeCustom1;

    private String cType;

    private int cRows;

    private String codeDescription;

    private ArrayList codeHide;

    private ArrayList   mileStatusIds;
	private ArrayList   mileStatusIdescs;
	
    private String forGroup;

    private int hiddenRows;
    // GETTER SETTER METHIODS


    public ArrayList getMileStatusIds() {
		return this.mileStatusIds;
	}

	public void setMileStatusIds(int mileIdsAchvied) {
		this.mileStatusIds.add(mileIdsAchvied);
	}

	public ArrayList getMileStatusIdescs() {
		return this.mileStatusIdescs;
	}

	public void setMileStatusIdescs(String mileIdescsAchvied) {
		this.mileStatusIdescs.add(mileIdescsAchvied);
	}

    
    /**
     * Gets the cId attribute of the CodeDao object
     *
     * @return The cId value
     */
    public ArrayList getCId() {
        return this.cId;
    }


	/**
     * Sets the cId attribute of the CodeDao object
     *
     * @param cId
     *            The new cId value
     */
    public void setCId(ArrayList cId) {
        this.cId = cId;
    }

    /**
     * Gets the cDesc attribute of the CodeDao object
     *
     * @return The cDesc value
     */
    public ArrayList getCDesc() {
        return this.cDesc;
    }

    /**
     * Sets the cDesc attribute of the CodeDao object
     *
     * @param cDesc
     *            The new cDesc value
     */
    public void setCDesc(ArrayList cDesc) {
        this.cDesc = cDesc;
    }

    /**
     * Gets the cSeq attribute of the CodeDao object
     *
     * @return The cSeq value
     */
    public ArrayList getCSeq() {
        return this.cSeq;
    }

    /**
     * Sets the cSeq attribute of the CodeDao object
     *
     * @param cSeq
     *            The new cSeq value
     */
    public void setCSeq(ArrayList cSeq) {
        this.cSeq = cSeq;
    }

    /**
     * Gets the cSubType attribute of the CodeDao object
     *
     * @return The cSubType value
     */
    public ArrayList getCSubType() {
        return this.cSubType;
    }

    /**
     * Sets the cSubType attribute of the CodeDao object
     *
     * @param cSubType
     *            The new cSubType value
     */
    public void setCSubType(ArrayList cSubType) {
        this.cSubType = cSubType;
    }

    /**
     * Gets the cRows attribute of the CodeDao object
     *
     * @return The cRows value
     */
    public int getCRows() {
        return this.cRows;
    }

    /**
     * Sets the cRows attribute of the CodeDao object
     *
     * @param cRows
     *            The new cRows value
     */
    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * Gets the cType attribute of the CodeDao object
     *
     * @return The cType value
     */
    public String getCType() {
        return this.cType;
    }

    /**
     * Sets the cType attribute of the CodeDao object
     *
     * @param cType
     *            The new cType value
     */
    public void setCType(String cType) {
        this.cType = cType;
    }

    /**
     * Sets the cDesc attribute of the CodeDao object
     *
     * @param sDesc
     *            The new cDesc value
     */
    public void setCDesc(String sDesc) {
        this.cDesc.add(sDesc);
    }

    /**
     * Sets the cId attribute of the CodeDao object
     *
     * @param sCId
     *            The new cId value
     */
    public void setCId(Integer sCId) {
        this.cId.add(sCId);
    }

    /**
     * Sets the cSeq attribute of the CodeDao object
     *
     * @param sSeq
     *            The new cSeq value
     */
    public void setCSeq(Integer sSeq) {
        this.cSeq.add(sSeq);
    }

    /**
     * Sets the cSubType attribute of the CodeDao object
     *
     * @param cSubType
     *            The new cSubType value
     */
    public void setCSubType(String cSubType) {
        this.cSubType.add(cSubType);
    }

    /**
     * Gets the codeDescription attribute of the CodeDao object
     *
     * @return The codeDescription value
     */
    public String getCodeDescription() {
        return this.codeDescription;
    }

    /**
     * Sets the codeDescription attribute of the CodeDao object
     *
     * @param codeDescription
     *            The new codeDescription value
     */
    public void setCodeDescription(String codeDescription) {
        this.codeDescription = codeDescription;
    }

    /**
     * Returns the value of codeCustom.
     *
     * @return The codeCustom value
     */
    public ArrayList getCodeCustom() {
        return codeCustom;
    }

    /**
     * Sets the value of codeCustom.
     *
     * @param codeCustom
     *            The value to assign codeCustom.
     */
    public void setCodeCustom(ArrayList codeCustom) {
        this.codeCustom = codeCustom;
    }

    /**
     * Returns the value of codeCustom1.
     *
     * @return The codeCustom1 value
     */
    public ArrayList getCodeCustom1() {
        return codeCustom1;
    }

    /**
     * Sets the value of codeCustom1.
     *
     * @param codeCustom1
     *            The value to assign codeCustom1.
     */
    public void setCodeCustom1(ArrayList codeCustom1) {
        this.codeCustom1 = codeCustom1;
    }

    /**
     * Sets the value of codeCustom1.
     *
     * @param cdCustom1
     *            The value to add to codeCustom1.
     */
    public void setCodeCustom1(String cdCustom1) {
        this.codeCustom1.add(cdCustom1);
    }

    /**
     * Sets the value of codeCustom.
     *
     * @param cdCustom
     *            The value to add to codeCustom.
     */
    public void setCodeCustom(String cdCustom) {
        this.codeCustom.add(cdCustom);
    }

    // End of Getter Setter methods

    /**
     * Default Constructor
     */

    public CodeDao() {
        cId = new ArrayList();
        cDesc = new ArrayList();
        cSeq = new ArrayList();
        cSubType = new ArrayList();
        codeCustom1 = new ArrayList();
        codeCustom = new ArrayList();
        codeHide= new ArrayList();
        mileStatusIds = new ArrayList();
        mileStatusIdescs = new ArrayList();
    }


    /**
     * Gets code list data for a code type and populates the class attributes
     *
     * @param c
     *            Code Type
     * @return The codeValues value
     */

    public boolean getCodeValues(String c) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        setCType(c);
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.erCodeLst);
            pstmt.setString(1, getCType());
            ResultSet rs = pstmt.executeQuery();
            String rowHide="";

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                setCodeCustom(rs.getString("CODELST_CUSTOM_COL"));
                setCodeCustom1(rs.getString("CODELST_CUSTOM_COL1"));

                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Gets all sites for an account and populates the class attributes
     *
     * @param accId
     *            Description of the Parameter
     * @return The accountSites value
     */
    public boolean getAccountSites(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(ERSQLS.accSites);
            pstmt.setInt(1, accId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("SITE_NAME"));
                setCId(new Integer(rs.getInt("PK_SITE")));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }


    //KM-Added on 22Aug08
    /**
     * Gets all sites for an account and populates the class attributes with no hidden organizations
     *
     * @param accId
     *            Description of the Parameter
     * @return The accountSites value
     */
    public boolean getAccountSitesNoHidden(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(ERSQLS.accSitesNoHidden);
            pstmt.setInt(1, accId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("SITE_NAME"));
                setCId(new Integer(rs.getInt("PK_SITE")));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }




    /**
     * Gets all groups for an account and populates the class attributes
     *
     * @param accId
     *            Description of the Parameter
     * @return The accountGroups value
     */
    public boolean getAccountGroups(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.accGroups);
            pstmt.setInt(1, accId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("GRP_NAME"));
                setCId(new Integer(rs.getInt("PK_GRP")));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }



    //KM-Added on 22Aug08
    /**
     * Gets all groups for an account and populates the class attributes with no hidden groups
     *
     * @param accId
     *            Description of the Parameter
     * @return The accountGroups value
     */
    public boolean getAccountGroupsNoHidden(int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.accGroupsNoHidden);
            pstmt.setInt(1, accId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("GRP_NAME"));
                setCId(new Integer(rs.getInt("PK_GRP")));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }


    /**
     * Gets all Users for a Group and populates the class attributes
     *
     * @param usrId
     * @param accId
     * @return The userGroups value
     */

    public boolean getUserGroups(int usrId, int accId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.usrGroups);
            pstmt.setInt(1, usrId);
            pstmt.setInt(2, accId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("GRP_NAME"));
                setCId(new Integer(rs.getInt("PK_GRP")));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * returns HTML string of dropdown with the values populated in the class
     * attributes
     *
     * @param dName -
     *            name of the select tag
     * @return Description of the Return Value
     */

    public String toPullDown(String dName) {
        StringBuffer mainStr = new StringBuffer();
        try {
            int counter = 0;
            String hideStr = "";

            getHiddenCodelstDataForUser();

            mainStr.append("<SELECT NAME=" + dName + ">");

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {

            	 hideStr = getCodeHide(counter) ;


            	if (hideStr.equals("N"))
            	{
            		mainStr.append("<OPTION value = " + cId.get(counter) + ">"
                        + cDesc.get(counter) + "</OPTION>");
            	}

            }

            mainStr.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }



    /**
     * returns HTML string of dropdown with the values populated in the class
     * attributes
     *
     * @param dName -
     *            name of the select tag
     * @param prop
     *            Description of the Parameter
     *
     * @return Description of the Return Value
     */


    //Added by Manimaran to add a property for a dropdown
    public String toPullDown(String dName, String prop) {
        StringBuffer mainStr = new StringBuffer();
        try {
            int counter = 0;
            String hideStr = "";

            getHiddenCodelstDataForUser();

            mainStr.append("<SELECT NAME=" + dName + " " + prop + ">");

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {

            	 hideStr = getCodeHide(counter) ;


            	if (hideStr.equals("N"))
            	{
            		mainStr.append("<OPTION value = " + cId.get(counter) + ">"
                        + cDesc.get(counter) + "</OPTION>");
            	}

            }

            mainStr.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }



    /**
     * returns HTML string of dropdown with the values populated in the class
     * attributes
     *
     * @param dName -
     *            name of the select tag
     * @param selValue -
     *            id of selected value
     * @return Description of the Return Value
     */

    public String toPullDown(String dName, int selValue) {
        Integer codeId = null;
        StringBuffer mainStr = new StringBuffer();

        try {

        	getHiddenCodelstDataForUser();
            int counter = 0;
            String hideStr = "";

            mainStr.append("<SELECT NAME='" + dName + "' id='" + dName + "'>");
            Integer val = new java.lang.Integer(selValue);

            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0")) {
                mainStr
                        .append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            }

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);



                if (selValue == codeId.intValue())
                {


                    mainStr.append("<OPTION value = " + codeId + " SELECTED>"
                            + cDesc.get(counter) + "</OPTION>");
                } else
                {

                	hideStr = getCodeHide(counter) ;



                	if (hideStr.equals("N"))
                	{

                    	mainStr.append("<OPTION value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}

                    /////////////
                }
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    /**
     * @param id
     *            Description of the Parameter
     * @return The codeValuesById value
     */
    // by sonia sahni - populates arralists against code data for code PK
    public boolean getCodeValuesById(int id) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.erCodeLstById);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            String rowHide = "";

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * @param c
     *            Description of the Parameter
     * @param userId
     *            Description of the Parameter
     * @return The codeValuesNotify value
     */

    /**
     * @param c
     *            Description of the Parameter
     * @param userId
     *            Description of the Parameter
     * @return The codeValuesNotify value
     */

    /**
     * Gets code list data for a code type which does not exist in
     * er_studynotify for a user
     *
     * @param c
     *            Code Type
     * @param userId
     *            Description of the Parameter
     * @return The codeValuesNotify value
     */

    public boolean getCodeValuesNotify(String c, int userId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        setCType(c);
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.erCodeLstAvTAreas);
            pstmt.setString(1, getCType());
            pstmt.setInt(2, userId);
            ResultSet rs = pstmt.executeQuery();

            String rowHide="";

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROMCONTROL TABLE " + ex);
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROMCONTROL TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Gets code list data for a codelst type and account
     *
     * @param accId
     *            Description of the Parameter
     * @param codelstType
     *            Description of the Parameter
     */
    /*
     * Modified by Sonia Abrol 03/18/05, to populate codelist custom cols
     */
    public void getCodelstData(int accId, String codelstType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select codelst_desc, pk_codelst, codelst_seq, "
                    + "codelst_subtyp , CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,codelst_hide"
                    + " from er_codelst "
                    + " where fk_account = ?  and codelst_type = ? order by codelst_seq";
            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, accId);
            pstmt.setString(2, codelstType);

            ResultSet rs = pstmt.executeQuery();
            String rowHide="";

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeCustom(rs.getString("CODELST_CUSTOM_COL"));
                setCodeCustom1(rs.getString("CODELST_CUSTOM_COL1"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }


                rows++;
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CONTROL TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * @param codeId
     *            Description of the Parameter
     * @return The codeDescription value
     */

    /**
     * @param codeId
     *            Description of the Parameter
     * @return The codeDescription value
     */

    /**
     * public static String getCodeDescription(int codeId) Static
     * function,returns code description for a codeid
     *
     * @param codeId
     * @return The codeDescription value
     */

    public String getCodeDescription(int codeId) {
        int rows = 0;
        String desc = null;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select codelst_desc " + "from er_codelst "
                    + "where pk_codelst = " + codeId;

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                desc = rs.getString("CODELST_DESC");
            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

        return desc;
    }

    /**
     * public static String getRolesForEvent(int eventId) Static function
     *
     * @param eventId
     */

    public void getRolesForEvent(int eventId) {
        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            String rowHide="";

            // modified by sonia, get code list data on the basis of study team
            // role
            String sql = "select pk_codelst, codelst_hide" + "codelst_desc, "
                    + "1 as selected " + "from 	er_codelst "
                    + "where 	codelst_type = 'role' "
                    + "and 	pk_codelst in 	(select eventusr "
                    + "from	sch_eventusr  " + "where 	fk_event = "
                    + eventId + " " + "and 	eventusr_type = 'R') " + "union "
                    + "select pk_codelst, codelst_hide " + "codelst_desc, "
                    + "0 as selected " + "from 	er_codelst "
                    + "where 	codelst_type = 'role' "
                    + "and 	pk_codelst not in	(select	eventusr "
                    + "from	sch_eventusr  " + "where 	fk_event = "
                    + eventId + " " + "and 	eventusr_type = 'R')";

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("codelst_desc"));
                setCId(new Integer(rs.getInt("pk_codelst")));
                setCSeq(new Integer(rs.getInt("selected")));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }


                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * @param eventUserIds
     *            User Ids or codelst Ids
     * @param userTypes
     *            User Types U for User, R for role and S for mail users
     */

    public void getDescForEventUsers(ArrayList eventUserIds, ArrayList userTypes) {
        int rows = eventUserIds.size();
        int i = 0;
        String sql = "";

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            for (i = 0; i < rows; i++) {
                if (userTypes.get(i).equals("R")) {
                    sql = "select codelst_desc as description "
                            + "from 	er_codelst " + "where 	pk_codelst = "
                            + eventUserIds.get(i);
                } else {
                    sql = "select usr_firstname || ' ' || usr_lastname  as description "
                            + "from 	er_user "
                            + "where 	pk_user = "
                            + eventUserIds.get(i);
                }
                pstmt = conn.prepareStatement(sql);

                ResultSet rs = pstmt.executeQuery();

                rs.next();
                setCDesc(rs.getString("description"));
            }
        } catch (SQLException ex) {
            Rlog.fatal("codelst",
                    "EXCEPTION In CodeDao.getDescForEventUsers IN FETCHING FROM CODE LST TABLE "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Description of the Method
     *
     * @param name
     *            Description of the Parameter
     * @param accountId
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int validateAccSection(String name, String accountId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int ret = 0;
        try {
            conn = getConnection();

            String mysql = "SELECT COUNT(PK_CODELST) AS SECCOUNT "
                    + "FROM ER_CODELST WHERE CODELST_DESC= ? AND "
                    + " FK_ACCOUNT = ? AND UPPER(CODELST_TYPE)='SECTION'";

            pstmt = conn.prepareStatement(mysql);

            pstmt.setString(1, name);
            pstmt.setInt(2, StringUtil.stringToNum(accountId));
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                ret = rs.getInt("SECCOUNT");
            }

            return ret;
        } catch (SQLException ex) {
            Rlog.fatal("codelst",
                    "CodeDao.validateAccSection EXCEPTION IN validating in ER_CODELST table"
                            + ex);
            return -2;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }

    // Method to get the Time Zones
    // Arvind
    // 12-27-2001

    /**
     * Gets the timeZones attribute of the CodeDao object
     *
     * @return The timeZones value
     */
    public boolean getTimeZones() {
        int rows = 0;
        String sql = "";
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            sql = "Select TZ_VALUE, TZ_DESCRIPTION, TZ_NAME, PK_TZ from sch_timezones ";
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("TZ_DESCRIPTION"));
                setCId(new Integer(rs.getInt("PK_TZ")));
                setCSubType(rs.getString("TZ_NAME"));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            Rlog.fatal("codelst",
                    "EXCEPTION IN FETCHING FROM TIME ZONES TABLE " + ex);
            return false;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    // ////////////////////////////

    /**
     * Description of the Method
     *
     * @param dName
     *            Description of the Parameter
     * @param selValue
     *            Description of the Parameter
     * @param withSelect
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public String toPullDown(String dName, int selValue, boolean withSelect) {
        Integer codeId = null;
        StringBuffer mainStr = new StringBuffer();
        boolean valueFound=false;

        // Debug.println("SIZE IS" +cDesc.size());

        try {

        	getHiddenCodelstDataForUser();

            int counter = 0;

            String hideStr = "";

            mainStr.append("<SELECT NAME=" + dName + ">");
            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= cDesc.size() - 1; counter++)
            {
                codeId = (Integer) cId.get(counter);


                if (selValue == codeId.intValue()) {
                    mainStr.append("<OPTION value = " + codeId + " SELECTED>"
                            + cDesc.get(counter) + "</OPTION>");

                    valueFound = true;
                } else {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                    	mainStr.append("<OPTION value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}


                }
            }

            if (withSelect){
	            if (val.toString().equals("") || val.toString().equals(null)
	                    || val.toString().equals("0") || valueFound==false) {
	                mainStr
	                	.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
	            } else {
	                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");
	            }
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    /**
     * Description of the Method
     *
     * @param dName
     *            Description of the Parameter
     * @param selValue
     *            Description of the Parameter
     * @param prop
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public String toPullDown(String dName, int selValue, String prop) {
        Integer codeId = null;
        String hideStr = "";
        boolean selectedFlag = false;


        StringBuffer mainStr = new StringBuffer();
        // Debug.println("SIZE IS" +cDesc.size());

        try {

        	getHiddenCodelstDataForUser();

            int counter = 0;
            mainStr.append("<SELECT NAME=" + dName + " " + prop + ">");
            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);

                if (selValue == codeId.intValue())
                {
                    mainStr.append("<OPTION value = " + codeId + " SELECTED>" + cDesc.get(counter) + "</OPTION>");
                    selectedFlag = true;
                }
                else
                {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                		mainStr.append("<OPTION value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}

                }
            }
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0") || selectedFlag == false ) {
                mainStr
                	.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else {
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }
    
    /**
     * Gets the codeId attribute of the CodeDao object
     *
     * @param type
     *            Description of the Parameter
     * @param subType
     *            Description of the Parameter
     * @return The codeId value
     */
    public int getCodeId(String type, String subType) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int codeId = 0;
        int rows = 0;
        try {
            conn = getConnection();
            String sql = "select PK_CODELST, CODELST_DESC ,codelst_hide from er_codelst where CODELST_TYPE='"
                    + type + "' " + " and CODELST_SUBTYP='" + subType + "'";
            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();
            String rowHide="";

            while (rs.next()) {
                codeId = rs.getInt("PK_CODELST");
                setCodeDescription(rs.getString("CODELST_DESC"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }


                rows++;
            }
            return codeId;
        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODELST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return codeId;
    }

    /**
     * Returns the internal code Id corresponding to specific
     * type,subtype,description combination
     *
     * @param type
     *            Code type
     * @param subType
     *            Code subtype
     * @param description
     *            Description of the item
     * @return internal code Id retrieved based on parameter
     */
    public int getCodeId(String type, String subType, String description) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int codeId = 0;
        int rows = 0;
        String rowHide="";

        try {
            conn = getConnection();
            String sql = "select PK_CODELST, CODELST_DESC,codelst_hide from er_codelst where CODELST_TYPE='"
                    + type
                    + "' "
                    + " and CODELST_SUBTYP='"
                    + subType
                    + "' and lower(codelst_desc)= '"
                    + description.toLowerCase() + "'";
            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                codeId = rs.getInt("PK_CODELST");
                setCodeDescription(rs.getString("CODELST_DESC"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }


                rows++;
            }
            return codeId;
        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODELST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return codeId;
    }

    /*
     * Method to get the codelst names corresponding to the comma separated
     * codeLst ids
     */

    /**
     * Gets the codeLstNames attribute of the CodeDao object
     *
     * @param codeLstIds
     *            Description of the Parameter
     * @return The codeLstNames value
     */
    public String getCodeLstNames(String codeLstIds) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";

        try {
            conn = getConnection();
            String sql = "select code_lst_names('" + codeLstIds
                    + "') as codeLstNames from dual";
            Rlog.debug("codelst", "sql:: " + sql);

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                retStr = rs.getString("codeLstNames");

            }

            return retStr;
        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODELST TABLE "
                    + ex);
            return "error";
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }

    /**
     * This method searches and returns the description for the given code id
     * from the cDesc ArrayList.
     *
     * @param codeId
     * @return The description for the codeId. If none found, returns ""
     */

    public String getCodeDesc(int codeId) {
        String retString = "";
        Integer lCode;
        Integer pCode;
        try {
            int counter = 0;

            counter = cId.indexOf(new Integer(codeId));
            if (counter >= 0) {
                retString = (String) cDesc.get(counter);
            }
            return retString;
        } catch (Exception e) {
            Rlog.fatal("codelst", "Exception thrown in getCodeDesc"
                    + e.getMessage());
            return "";
        }

    }

    /**
     * Populates the object with code list data for a code_type and custom_col
     * value
     *
     * @param codeType
     *            Code Type
     * @param customColValue
     *            A custom col value to identify codelst records
     */
    public void getCodeValues(String codeType, String customColValue) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String rowHide="";
        // set code type
        setCType(codeType);
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.erCodeLstWithCustom);
            pstmt.setString(1, getCType());
            pstmt.setString(2, customColValue);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }


                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM ER_CODELST" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Populates the object with code list data for a code_type and custom_col
     * value
     *
     * @param codeType
     *            Code Type
     * @param customColValue
     *            A custom col value to identify codelst records
     */
    public void getCodeValues(String codeType, String customColValue,String orderBy) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        setCType(codeType);

        String rowHide="";

        try {
            conn = getConnection();

            String sql="select PK_CODELST, CODELST_SUBTYP, CODELST_DESC , CODELST_SEQ ,codelst_hide from er_codelst where rtrim(CODELST_TYPE) = ? and codelst_custom_col = ? and (codelst_hide='N' or codelst_hide is null) ";

            if (orderBy.length()>0)
            {
               sql= sql + " order by " + orderBy;
            }
            else
            {

            	sql= sql + " order by CODELST_SEQ" ;
            }

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, getCType());
            pstmt.setString(2, customColValue);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM ER_CODELST" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }


    /**
     * Populates the object with code list data for a code_type and account
     * value
     *
     * @param codeType
     *            Code Type
     * @param accId
     *            Description of the Parameter
     */
    public void getCodeValues(String codeType, int accId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String rowHide="";

        // set code type
        setCType(codeType);
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.erCodeLstByAccount);
            pstmt.setString(1, getCType());
            pstmt.setInt(2, accId);
            pstmt.setString(3, getCType());
            pstmt.setString(4, getCType());
            pstmt.setInt(5, accId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));
                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }
                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM ER_CODELST" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Find code values for sub types as: Active/Enrolling, Non Active, Study
     * completed
     *
     * @return The valForSpecificSubType value
     */
    public String getValForSpecificSubType() {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String codeIds = "";

        int rows = 0;
        try {
            int counter = 0;
            conn = getConnection();
            String sql = "select pk_codelst " + " from er_codelst "
                    + " where codelst_type='studystat' "
                    + " and (trim(codelst_custom_col)='browser')";

            // + " and (codelst_subtyp='active' or codelst_subtyp='not_active'
            // or codelst_subtyp='prmnt_cls')";

            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCId(new Integer(rs.getInt("pk_codelst")));
                rows++;
            }
            // codeIds = "(";
            for (counter = 0; counter <= cId.size() - 1; counter++) {
                if (counter == 0) {
                    codeIds = cId.get(counter).toString();
                } else {
                    codeIds = codeIds + "," + cId.get(counter).toString();
                }
                // codeIds = ")";

            }
            return codeIds;
        } catch (Exception e) {
            Rlog.fatal("codelst",
                    "Exception thrown in getValForSpecificSubType"
                            + e.getMessage());
            return "";
        }finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the codeValuesByIds attribute of the CodeDao object
     *
     * @param CodeIds
     *            Comma seperated ID for which code values are retrieved.
     * @return Return comma seperated list of code values
     */
    public String getCodeValuesByIds(String codeIds, String sep, String respSep) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        String sql = "select pkg_util.f_getCodeValues(?,?,?) as codeValue  from dual";
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, codeIds);
            pstmt.setString(2, sep);
            pstmt.setString(3, respSep);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                retStr = rs.getString("codeValue");

            }

            return retStr;
        } catch (SQLException ex) {
            Rlog.fatal("codelst",
                    "CodeDao.getCodeValuesById EXCEPTION IN FETCHING FROM codelst table"
                            + ex);
            return "error";
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the codeId attribute of the CodeDao object
     *
     * @param type
     *            Code Type , maps to codelst_type in er_codelst
     * @param desc
     *            Code Description , maps to codelst_desc in er_codelst
     * @return The codeId value
     */
    public int getCodeIdFromDesc(String type, String desc) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int codeId = 0;
        try {
            conn = getConnection();
            String sql = " select PK_CODELST from er_codelst where lower(trim(CODELST_TYPE)) = lower(trim(?)) "
                    + " and lower(trim(CODELST_DESC)) = lower(trim(?))";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, type);
            pstmt.setString(2, desc);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                codeId = rs.getInt("PK_CODELST");
            }
            return codeId;
        } catch (SQLException ex) {
            Rlog.fatal("codelst",
                    "getCodeIdfromDesc :EXCEPTION IN FETCHING FROM CODELST TABLE "
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return codeId;
    }

    public void getCodeCustomCol(String type) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int codeId = 0;
        int rows = 0;
        try {
            conn = getConnection();
            String sql = "SELECT DISTINCT INITCAP(codelst_custom_col) as custom_col   FROM ER_CODELST  "
                    + "WHERE codelst_type=? AND codelst_custom_col IS NOT NULL and (upper(codelst_hide)<>'Y' or codelst_hide is null ) order by custom_col";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, type);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                this.setCodeCustom(rs.getString("custom_col"));
                setCodeHide("N"); //because distinct is used

            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODELST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }
  ///
    public String getCodeCustomCol(int codeId) {
        int rows = 0;
        String customcol = null;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select codelst_custom_col " + "from er_codelst "
                    + "where pk_codelst = " + codeId;

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	customcol = rs.getString("codelst_custom_col");
            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

        return customcol;
    }



    public void resetDao()
    {
       cId=new ArrayList();

        cDesc=new ArrayList();

        cSeq=new ArrayList();

        cSubType=new ArrayList();

        codeCustom=new ArrayList();
        codeCustom1=new ArrayList();
        codeHide=new ArrayList();

    }


    /**
     * Populates the object with code list data for a code_type and its parent code
     * Method to be used to establist parent child relationships between two codes
     * Supports multiple values in codelst_custom_col1 (values must be comma separated)
     *
     * @param codeType
     *            Code Type of the child code
     * @param parentCodePK
     *            PK of the code whose 'code_subtype' will be used to check custom_col1 for a given child codeType
         *
     */
    public void getCodeValuesForCustom1(String childCodeType, String parentCodePK) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String parentSubType = "";

        String rowHide="";

        // set code type
        setCType(childCodeType);
        try {

        	parentSubType = this.getCodeSubtype(StringUtil.stringToNum(parentCodePK));

        	if (StringUtil.isEmpty(parentSubType))
        	{
        		parentSubType = "";


        	}


        	parentSubType = "%," + parentSubType +",%";

            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.erCodeLstWithCustom_1);
            pstmt.setString(1, getCType());
            pstmt.setString(2, parentSubType);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM ER_CODELST-erCodeLstWithCustom_1" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Populates the object with code list data for a code_type and its parent code
     * Method to be used to establist parent child relationships between two codes
     * Supports multiple values in codelst_custom_col1 (values must be comma separated)
     *
     * @param codeType
     *            Code Type of the child code
     * @param customCol1
     *            will be matched with custom_col1
         *
     */
    public void getCodeValuesFilterCustom1(String childCodeType, String customCol1) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String rowHide="";
        // set code type
        setCType(childCodeType);
        try {
        	if (StringUtil.isEmpty(customCol1))
        	{
        		customCol1="";
        	}

        	customCol1 = "%," + customCol1 +",%";

            conn = getConnection();
            pstmt = conn.prepareStatement(ERSQLS.erCodeLstWithCustom_1);
            pstmt.setString(1, getCType());
            pstmt.setString(2, customCol1);
            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM ER_CODELST-erCodeLstWithCustom_1" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }


    /**
    * public String getCodeSubType (int codeId)  - returns code_subtyp for a codeid
    *
    * @param codeId
    * @return The codeSubtype value
    */

   public String getCodeSubtype(int codeId) {
       String subtyp = null;

       PreparedStatement pstmt = null;
       Connection conn = null;

       try {
           conn = getConnection();

           String sql = "select codelst_subtyp from er_codelst  where pk_codelst = ?";

           pstmt = conn.prepareStatement(sql);
           pstmt.setInt(1, codeId);

           ResultSet rs = pstmt.executeQuery();

           while (rs.next()) {
        	   subtyp = rs.getString("codelst_subtyp");
           }

       } catch (SQLException ex) {
           Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LIST TABLE:getCodeSubtype: "
                   + ex);
       } finally {
           try {
               if (pstmt != null) {
                   pstmt.close();
               }
           } catch (Exception e) {
           }
           try {
               if (conn != null) {
                   conn.close();
               }
           } catch (Exception e) {
           }
       }

       return subtyp;
   }

	public ArrayList getCodeHide() {
		return codeHide;
	}

	public void setCodeHide(ArrayList codeHide) {
		this.codeHide = codeHide;
	}

	public void setCodeHide(String cHide) {
		String hide = "";

		if (StringUtil.isEmpty(cHide))
		{
			hide= "N";

		}else
		{
			hide = cHide;
		}
		this.codeHide.add(hide);
	}

	public String getCodeHide(int index) {

		String hideStr = "";

		if (codeHide != null && index < codeHide.size() )
    	{
    		hideStr = (String )codeHide.get(index);

    	}
    	else
    	{
    		hideStr = "N";
    	}

		return hideStr ;
	}


	/**
     * returns HTML string of dropdown with the values populated in the class
     * attributes. the pull down option is selected using the descripton passed inthe parameter
     *
     * @param dName -
     *            name of the select tag
     * @param selValue -
     *            description of selected value
     * @return HTML string
     */

    public String toPullDownUsingDescription(String dName, String selValue) {
        Integer codeId = null;
        String codeDesc = "";


        StringBuffer mainStr = new StringBuffer();
        try {
            int counter = 0;
            String hideStr = "";
            getHiddenCodelstDataForUser();

            mainStr.append("<SELECT NAME=" + dName + ">");


            if (StringUtil.isEmpty(selValue)) {
                mainStr.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
                selValue = "";
            }

            for (counter = 0; counter <= cDesc.size() - 1; counter++) {
                codeId = (Integer) cId.get(counter);

                codeDesc = (String) cDesc.get(counter);



                if (codeDesc.equals(selValue))
                {
                	mainStr.append("<OPTION value = " + codeId + " SELECTED>"
                            + cDesc.get(counter) + "</OPTION>");
                } else
                {

                	hideStr = getCodeHide(counter) ;



                	if (hideStr.equals("N"))
                	{

                    	mainStr.append("<OPTION value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                	}

                    /////////////
                }
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDownUsingDescription" + e;
        }
        return mainStr.toString();
    }
     
    //KM-05May10-#D-FIN7
    public String toPullDownMilestone(String dName, int selValue, String milestoneAchievedCount) {
        Integer codeId = null;
        StringBuffer mainStr = new StringBuffer();
        boolean valueFound=false;
        try {

        	getHiddenCodelstDataForUser();

            int counter = 0;

            String hideStr = "";

            mainStr.append("<SELECT NAME=" + dName + ">");
            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= cDesc.size() - 1; counter++)
            {
                codeId = (Integer) cId.get(counter);
                
                if (selValue == codeId.intValue()) {
                    mainStr.append("<OPTION value = " + codeId + " SELECTED>"
                            + cDesc.get(counter) + "</OPTION>");
                    valueFound = true;
                } else {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                		int wipCodePK = getCodeId("milestone_stat","WIP");
                		int actCodePK = getCodeId("milestone_stat","A");
                		int inactCodePK = getCodeId("milestone_stat","IA");
                	
                		if (actCodePK != codeId && inactCodePK != codeId) { 
                			if ((selValue != actCodePK && selValue !=inactCodePK) || ( milestoneAchievedCount.equals("0") && (selValue == actCodePK) )) {	
                				mainStr.append("<OPTION value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");	
                			}
                		} else {
                			mainStr.append("<OPTION value = " + codeId + ">"+ cDesc.get(counter) + "</OPTION>");
                		}
                	}
                }
            }
            
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0") || valueFound==false) {
                mainStr
                	.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else {
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");
            }
            
            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }
    
    public void toPullDownMilestoneCodeLst(int selValue, String milestoneAchievedCount) {
        Integer codeId = null;
        StringBuffer mainStr = new StringBuffer();
        boolean valueFound=false;
        try {

        	getHiddenCodelstDataForUser();

            int counter = 0;

            String hideStr = "";

            Integer val = new java.lang.Integer(selValue);
            this.mileStatusIds = new ArrayList();
            this.mileStatusIdescs = new ArrayList();
            for (counter = 0; counter <= cDesc.size() - 1; counter++)
            {
                codeId = (Integer) cId.get(counter);
                
                if (selValue == codeId.intValue()) {
                   this.setMileStatusIds(codeId);
                   this.setMileStatusIdescs(cDesc.get(counter).toString());
                    valueFound = true;
                } else {

                	hideStr = getCodeHide(counter) ;

                	if (hideStr.equals("N"))
                	{
                		int wipCodePK = getCodeId("milestone_stat","WIP");
                		int actCodePK = getCodeId("milestone_stat","A");
                		int inactCodePK = getCodeId("milestone_stat","IA");
                	
                		if (actCodePK != codeId && inactCodePK != codeId) { 
                			if ((selValue != actCodePK && selValue !=inactCodePK) || ( milestoneAchievedCount.equals("0") && (selValue == actCodePK) )) {	
                				this.setMileStatusIds(codeId);
                                this.setMileStatusIdescs(cDesc.get(counter).toString());	
                			}
                		} else {
                			this.setMileStatusIds(codeId);
                            this.setMileStatusIdescs(cDesc.get(counter).toString());
                		}
                	}
                }
            }
            this.setMileStatusIds(0);
            this.setMileStatusIdescs(LC.L_Select_AnOption);

        } catch (Exception e) {
            
        }

    }
    
    
    /** Works when following CodeList attributes are set: forGroup and cType
     *
     *
     * forGroup - the user's group for which hidden codelist PKs should be retrieved
     * cType - codelist type key
     * */
    public void getHiddenCodelstDataForUser() {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int pos = -1;



        if ((! StringUtil.isEmpty( this.cType)) && (!StringUtil.isEmpty( this.forGroup)) && this.cId != null && this.cId.size() > 0 )
        {
        try {
            conn = getConnection();
            String sql = "select FK_CODELST from ER_CODELST_HIDE where CODELST_TYPE = ? and  fk_grp = ? and CODELST_HIDE_TABLE = ?";

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, this.cType);
            pstmt.setString(2, this.forGroup);
            pstmt.setInt(3, 1);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                 Integer hId = null;

                 hId = new Integer(rs.getInt("FK_CODELST"));
                 pos = this.cId.indexOf(hId);

                 if (pos >= 0) //the hidden code exists
                 {
                	 //replace the 'hidden' code for this codelist item
                	 if (pos < this.codeHide.size())
                	 {
                		 //System.out.println("Hiding:..........." +hId.intValue() );
                		 this.codeHide.set(pos,"Y");
                		 this.incrementHiddenRows();
                	 }
                 }

            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN getHiddenCodelstDataForUser"
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        }

    }

    /** Works when following CodeList attributes are set: forGroup and cType and where getCodeCustomCol is used
     *
     *
     * forGroup - the user's group for which hidden codelist PKs should be retrieved
     * cType - codelist type key
     * */
    public void getHiddenCodelstDataForUserCustom() {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int pos = -1;



        if ((! StringUtil.isEmpty( this.cType)) && (!StringUtil.isEmpty( this.forGroup)) && this.codeCustom != null && this.codeCustom.size() > 0 )
        {
        try {
            conn = getConnection();
            String sql = "select distinct INITCAP(codelst_custom_col)  codelst_custom_col  from ER_CODELST_HIDE ,er_codelst where ER_CODELST_HIDE.CODELST_TYPE = ? and  fk_grp = ? and CODELST_HIDE_TABLE = ? and pk_codelst = fk_codelst";

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, this.cType);
            pstmt.setString(2, this.forGroup);
            pstmt.setInt(3, 1);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                 String hId = null;

                 hId =  rs.getString("codelst_custom_col");
                 pos = this.codeCustom.indexOf(hId);

                 if (pos >= 0) //the hidden code exists
                 {
                	 //replace the 'hidden' code for this codelist item
                	 if (pos < this.codeHide.size())
                	 {

                		 this.codeHide.set(pos,"Y");
                		 this.incrementHiddenRows();
                	 }
                 }

            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN getHiddenCodelstDataForUserCustom"
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        }

    }


	public String getForGroup() {
		return forGroup;
	}

	public void setForGroup(String forGroup) {
		this.forGroup = forGroup;
	}


	//JM: 13Jul2009: #4098
	public String getCodeCustomCol1(int codeId) {
        int rows = 0;
        String customcol = null;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            String sql = "select codelst_custom_col1 " + "from er_codelst "
                    + "where pk_codelst = " + codeId;

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	customcol = rs.getString("codelst_custom_col1");
            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", " getCodeCustomCol1 EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

        return customcol;
    }


    // END OF CLASS

	//JM: 28Sep2009: #4302
	 /**
     * Populates the object with code list data for a code_type
     *
     * @param codeType
     *            Code Type
     *
     */


	public boolean getCodeValuesSaveKit(String c) {
    int rows = 0;
    PreparedStatement pstmt = null;
    Connection conn = null;
    String rowHide="";

    // set code type
    setCType(c);
    try {
        conn = getConnection();
        pstmt = conn.prepareStatement(ERSQLS.erCodeLstSaveKit);
        pstmt.setString(1, getCType());
        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            setCDesc(rs.getString("CODELST_DESC"));
            setCId(new Integer(rs.getInt("PK_CODELST")));
            setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
            setCSubType(rs.getString("CODELST_SUBTYP"));

            setCodeHide(rs.getString("CODELST_HIDE"));

            rowHide= rs.getString("CODELST_HIDE");

            if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
            {
            	incrementHiddenRows();
            }

            setCodeCustom(rs.getString("CODELST_CUSTOM_COL"));
            setCodeCustom1(rs.getString("CODELST_CUSTOM_COL1"));

            rows++;
        }
        setCRows(rows);
        return true;
    } catch (SQLException ex) {

    	Rlog.fatal("codelst", " getCodeValuesSaveKit EXCEPTION IN FETCHING FROM eres.CODELST TABLE "
                + ex);
        return false;
    } finally {
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }
}

    /**
     * Gets code list data for a code type and study role . populates the class attributes.
     * if role is empty, gets values without role specified - custimcol=default_data
     *
     * @param codeType
     * @param roleCodelstPK Primary key of codelist record for the role. Where study role is not available, it is blank
     * @return boolean
     */

    public boolean getCodeValuesForStudyRole(String codeType,String roleCodelstPK)
    {
    	String roleSubtype = "";
    	int codePk = 0;
    	int rows = 0;


    	codePk = StringUtil.stringToNum(roleCodelstPK);

    	if (codePk >0 )
    	{
    		roleSubtype  = getCodeSubtype(codePk );

    		if (! StringUtil.isEmpty(roleSubtype))
    		{

    			//getCodeValuesFilterCustom1(codeType, roleSubtype) ;
    			getCodeValuesFilterStudyRole(codeType, roleSubtype);

    			rows = getCRows() ;

    			if (getCRows() <= 0 || getCRows() == getHiddenRows() )
    			{

    				rows = 0;
    			}
    		}
    		else
    		{
    			rows = 0;
    		}
    	}
    	else
    	{

    		rows = 0;
    	}

    	if (rows ==0) //get Default Data where custom col is null
    	{
    		//getCodeValuesFilterCustom1(codeType, "default_data") ;
    		getCodeValuesFilterStudyRole(codeType, "default_data") ;

    	}

    	return true;
    }


    /************************************************************************************************/
    //JM: 30Apr2010: #enh-SW-FIN4
    /**
     * Populates the object with code list data for a code_type and its parent code
     * Method to be used to establist parent child relationships between two codes
     * Supports multiple values in codelst_study_role (values must be comma separated)
     *
     * @param codeType
     *            Code Type of the child code
     * @param customCol1
     *            will be matched with codelst_study_role
     *
     */
    public void getCodeValuesFilterStudyRole(String childCodeType, String customCol1) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String rowHide="";
        // set code type
        setCType(childCodeType);
        try {
        	if (StringUtil.isEmpty(customCol1))
        	{
        		customCol1="";
        	}

        	customCol1 = "%," + customCol1 +",%";



            conn = getConnection();



            pstmt = conn.prepareStatement(ERSQLS.erCodeLstWithStudyRole);

            pstmt.setString(1, getCType());
            pstmt.setString(2, customCol1);
            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM ER_CODELST-erCodeLstWithStudyRole" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }


    /**
     * Populates the object with code list data for a code_type and its parent code and role
     * Method to be used to establist parent child relationships between two codes
     * Supports multiple values in codelst_study_role (values must be comma separated)
     *
     * @param codeType
     *            Code Type of the child code
     * @param customCol1
     *            will be matched with codelst_study_role
         *
     */
    public void getCodeValuesFilterStudyRoleAndCustom1(String childCodeType, String stStTypSubType, String customCol1) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String rowHide="";
        // set code type
        setCType(childCodeType);
        try {
        	if (StringUtil.isEmpty(customCol1))
        	{
        		customCol1="";
        	}

        	if (StringUtil.isEmpty(stStTypSubType))
        	{
        		stStTypSubType="";
        	}

        	customCol1 = "%," + customCol1 +",%";

        	stStTypSubType = "%," + stStTypSubType +",%";



            conn = getConnection();



            pstmt = conn.prepareStatement(ERSQLS.erCodeLstWith2Filter);

            pstmt.setString(1, getCType());
            pstmt.setString(2, stStTypSubType);
            pstmt.setString(3, getCType());
            pstmt.setString(4, customCol1);
            ResultSet rs = pstmt.executeQuery();


            while (rs.next()) {
                setCDesc(rs.getString("CODELST_DESC"));
                setCId(new Integer(rs.getInt("PK_CODELST")));
                setCSeq(new Integer(rs.getInt("CODELST_SEQ")));
                setCSubType(rs.getString("CODELST_SUBTYP"));

                setCodeHide(rs.getString("CODELST_HIDE"));

                rowHide= rs.getString("CODELST_HIDE");

                if (! StringUtil.isEmpty(rowHide) && rowHide.equals("Y"))
                {
                	incrementHiddenRows();
                }

                rows++;
            }
            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM ER_CODELST-erCodeLstWith2Filter" + ex);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Gets code list data for a code type and study role and study status type's subtype.
     * if role is empty, gets values without role specified - custimcol=default_data
     *
     * @param codeType
     * @param roleCodelstPK Primary key of codelist record for the role. Where study role is not available, it is blank
     * @return boolean
     */

    public boolean getCodeValuesForStudyRoleAndCustom1(String codeType,String stStatTypePK, String roleCodelstPK)
    {
    	String roleSubtype = "";
    	int codePk = 0;
    	int rows = 0;

    	String stStTypeSubType = "";
    	int stStatusPk = 0;



    	stStatusPk = StringUtil.stringToNum(stStatTypePK);//subtype of study status type
    	codePk = StringUtil.stringToNum(roleCodelstPK);//subtype of the role


    	stStTypeSubType  = getCodeSubtype(stStatusPk );
		stStTypeSubType = (stStTypeSubType==null)?"":stStTypeSubType;


    	if (codePk >0 )
    	{
    		roleSubtype  = getCodeSubtype(codePk );

    		if (! StringUtil.isEmpty(roleSubtype))
    		{

    			getCodeValuesFilterStudyRoleAndCustom1(codeType, stStTypeSubType, roleSubtype) ;

    			rows = getCRows() ;

    			if (getCRows() <= 0 || getCRows() == getHiddenRows() )
    			{

    				rows = 0;
    			}
    		}
    		else
    		{
    			rows = 0;
    		}
    	}
    	else
    	{

    		rows = 0;
    	}

    	if (rows ==0) //get Default Data where custom col is null
    	{
    		getCodeValuesFilterStudyRoleAndCustom1(codeType, stStTypeSubType, "default_data") ;

    	}

    	return true;
    }


    /************************************************************************************************/





	public int getHiddenRows() {
		return hiddenRows;
	}

	public void setHiddenRows(int hiddenRows) {
		this.hiddenRows = hiddenRows;
	}

	public void incrementHiddenRows() {
		this.hiddenRows = this.hiddenRows+1;
	}
	
	//Raman
	public boolean getCodeValuesForTimeZone(String codeType) {
		int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        setCType(codeType);
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select PK_TZ, trim(TZ_SUBTYPE) TZ_SUBTYPE, TZ_NAME from sch_timezones");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	setCDesc(rs.getString("TZ_NAME"));
                setCId(new Integer(rs.getInt("PK_TZ")));
                setCSubType(rs.getString("TZ_SUBTYPE"));
                rows++;
            }
            setCRows(rows);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            Rlog.fatal("common", "EXCEPTION IN FETCHING FROM SCH_TIMEZONES TABLE "
                    + ex);
            return false;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
		
	

}
