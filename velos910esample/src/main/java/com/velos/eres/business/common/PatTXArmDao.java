/**
 * Classname	PatTXArmDao.class
 * 
 * Version information 	1.0
 *
 * Date	05/05/2005
 * 
 * Author -- Anu
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class PatTXArmDao extends CommonDAO implements java.io.Serializable {

    private ArrayList patTXArmIds;

    private ArrayList studyTXArmIds;

    private ArrayList txNames;

    private ArrayList drugsInfo;

    private ArrayList startDates;

    private ArrayList endDates;

    private ArrayList notes;

    public PatTXArmDao() {

        patTXArmIds = new ArrayList();
        studyTXArmIds = new ArrayList();
        txNames = new ArrayList();
        drugsInfo = new ArrayList();
        startDates = new ArrayList();
        endDates = new ArrayList();
        notes = new ArrayList();

    }

    // Getter and Setter methods

    public ArrayList getPatTXArmIds() {
        return this.patTXArmIds;
    }

    public void setPatTXArmIds(ArrayList patTXArmIds) {
        this.patTXArmIds = patTXArmIds;
    }

    public ArrayList getStudyTXArmIds() {
        return this.studyTXArmIds;
    }

    public void setStudyTXArmIds(ArrayList studyTXArmIds) {
        this.studyTXArmIds = studyTXArmIds;
    }

    public ArrayList getTXNames() {
        return this.txNames;
    }

    public void setTXNames(ArrayList txNames) {
        this.txNames = txNames;
    }

    public ArrayList getDrugsInfo() {
        return this.drugsInfo;
    }

    public void setDrugsInfo(ArrayList drugsInfo) {
        this.drugsInfo = drugsInfo;
    }

    public ArrayList getStartDates() {
        return this.startDates;
    }

    public void setStartDates(ArrayList startDates) {
        this.startDates = startDates;
    }

    public ArrayList getEndDates() {
        return this.endDates;
    }

    public void setEndDates(ArrayList endDates) {
        this.endDates = endDates;
    }

    public ArrayList getNotes() {
        return this.notes;
    }

    public void setNotes(ArrayList notes) {
        this.notes = notes;
    }

    // //

    public void setPatTXArmIds(Integer patTXArmId) {
        patTXArmIds.add(patTXArmId);
    }

    public void setStudyTXArmIds(Integer studyTXArmId) {
        studyTXArmIds.add(studyTXArmId);
    }

    public void setTXNames(String txName) {
        txNames.add(txName);
    }

    public void setDrugsInfo(String drugInfo) {
        drugsInfo.add(drugInfo);
    }

    public void setStartDates(String startDate) {
        startDates.add(startDate);
    }

    public void setEndDates(String endDate) {
        endDates.add(endDate);
    }

    public void setNotes(String note) {
        notes.add(note);
    }

    // end of getter and setter methods

    // /////////
    /**
     * Retreives all treatment arms for a patient
     * 
     * @param patProtId -
     *            patProt Id
     */

    public void getPatTrtmtArms(int patProtId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {
            conn = getConnection();
            sql = "SELECT PK_PATTXARM, "
                    + " FK_STUDYTXARM, "
                    + "(select tx_name from er_studytxarm where pk_studytxarm = FK_STUDYTXARM) txname, "
                    + " TX_DRUG_INFO, " + " TX_START_DATE, " + " TX_END_DATE, "
                    + " NOTES " + " from er_pattxarm where fk_patprot in " 
                    + " (select pk_patprot from er_patprot where fk_per in (select fk_per from er_patprot where pk_patprot=?) " 
                    + " and fk_study in (select fk_study from er_patprot where pk_patprot=?) ) "
                   	+ "order by TX_START_DATE DESC";
            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, patProtId);
            pstmt.setInt(2, patProtId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setPatTXArmIds(new Integer(rs.getInt("PK_PATTXARM")));

                setStudyTXArmIds(new Integer(rs.getInt("FK_STUDYTXARM")));

                setTXNames(rs.getString("txname"));

                setDrugsInfo(rs.getString("TX_DRUG_INFO"));

                setStartDates(rs.getString("TX_START_DATE"));

                setEndDates(rs.getString("TX_END_DATE"));

                setNotes(rs.getString("NOTES"));

                rows++;

            }

        } catch (SQLException ex) {
            Rlog.fatal("patTXArm",
                    ".getPatTrtmtArms EXCEPTION IN FETCHING FROM TABLE" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

}
