//sajal dt 03/29/2001

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class StudyDao extends CommonDAO implements java.io.Serializable {
    private String studyDropDown = null;

    private ArrayList studyIds;

    private ArrayList studyAccountIds;

    private ArrayList studySponsors;

    private ArrayList studyContacts;

    private ArrayList studyPubFlags;

    private ArrayList studyTitles;

    private ArrayList studyObjectives;

    private ArrayList studySummarys;

    private ArrayList studyProducts;

    private ArrayList studySizes;

    private ArrayList studyDurations;

    private ArrayList studyDurationUnits;

    private ArrayList studyEstBeginDates;

    private ArrayList studyActBeginDates;

    private ArrayList studyPrimInvs;

    private ArrayList studyPartCenters;

    private ArrayList studyKeywords;

    private ArrayList studyPubCols;

    private ArrayList studyParents;

    private ArrayList studyAuthors;

    private ArrayList studyTAreas;

    private ArrayList studyPhases;

    private ArrayList studyBlindings;

    private ArrayList studyRandoms;

    private ArrayList studyVersions;

    private ArrayList studyCurrents;

    private ArrayList studyNumbers;

    private ArrayList studyResTypes;
    // Added By Amarnadh for Bugzilla issue #3208 28th Nov '07
    private ArrayList studyScopes; 

    private ArrayList studyTypes;

    private ArrayList studyInfos;

    private ArrayList studyStatuses;

    private ArrayList studySubtypes;

    private ArrayList studyStatNotes;

    private ArrayList studyStatDates;
    
    private ArrayList studySites;//JM
    
    private ArrayList studyTeamRights;

    private int verifyValue;

    private int cRows;
    //AK:Added for INVP.1 (BUG#6714)
    private ArrayList studyAutoNames;
	private ArrayList studyAutoIds;


    public StudyDao() {    	
        studyIds = new ArrayList();
        studyAccountIds = new ArrayList();
        studySponsors = new ArrayList();
        studyContacts = new ArrayList();
        studyPubFlags = new ArrayList();
        studyTitles = new ArrayList();
        studyObjectives = new ArrayList();
        studySummarys = new ArrayList();
        studyProducts = new ArrayList();
        studySizes = new ArrayList();
        studyDurations = new ArrayList();
        studyDurationUnits = new ArrayList();
        studyEstBeginDates = new ArrayList();
        studyActBeginDates = new ArrayList();
        studyPrimInvs = new ArrayList();
        studyPartCenters = new ArrayList();
        studyKeywords = new ArrayList();
        studyPubCols = new ArrayList();
        studyParents = new ArrayList();
        studyAuthors = new ArrayList();
        studyTAreas = new ArrayList();
        studyPhases = new ArrayList();
        studyBlindings = new ArrayList();
        studyRandoms = new ArrayList();
        studyVersions = new ArrayList();
        studyCurrents = new ArrayList();
        studyNumbers = new ArrayList();
        studyResTypes = new ArrayList();
        studyScopes = new ArrayList(); // Amar
        studyTypes = new ArrayList();
        studyInfos = new ArrayList();
        studyStatuses = new ArrayList();
        studySubtypes = new ArrayList();
        studyStatNotes = new ArrayList();
        studyStatDates = new ArrayList();
        studySites	= new ArrayList();//JM        
        studyTeamRights = new ArrayList();
        //AK:Added for INVP.1 (BUG#6714)
        studyAutoNames= new ArrayList();
        studyAutoIds=new ArrayList();
    }

    // Getter and Setter methods

    public String getStudyDropDown() {
        return this.studyDropDown;
    }

    public void setStudyDropDown(String studyDropDown) {
        this.studyDropDown = studyDropDown;
    }

    public ArrayList getStudyIds() {
        return this.studyIds;
    }

    public void setStudyIds(ArrayList studyIds) {
        this.studyIds = studyIds;
    }

    public ArrayList getStudyAccountIds() {
        return this.studyAccountIds;
    }

    public void setStudyAccountIds(ArrayList studyAccountIds) {
        this.studyAccountIds = studyAccountIds;
    }

    public ArrayList getStudySponsors() {
        return this.studySponsors;
    }

    public void setStudySponsors(ArrayList studySponsors) {
        this.studySponsors = studySponsors;
    }
    
    public ArrayList getStudyContacts() {
        return this.studyContacts;
    }

    public void setStudyContacts(ArrayList studyContacts) {
        this.studyContacts = studyContacts;
    }

    public ArrayList getStudyPubFlags() {
        return this.studyPubFlags;
    }

    public void setStudyPubFlags(ArrayList studyPubFlags) {
        this.studyPubFlags = studyPubFlags;
    }

    public ArrayList getStudyTitles() {
        return this.studyTitles;
    }

    public void setStudyTitles(ArrayList studyTitles) {
        this.studyTitles = studyTitles;
    }

    public ArrayList getStudyObjectives() {
        return this.studyObjectives;
    }

    public void setStudyObjectives(ArrayList studyObjectives) {
        this.studyObjectives = studyObjectives;
    }

    public ArrayList getStudySummarys() {
        return this.studySummarys;
    }

    public void setStudySummarys(ArrayList studySummarys) {
        this.studySummarys = studySummarys;
    }

    public ArrayList getStudyProducts() {
        return this.studyProducts;
    }

    public void setStudyProducts(ArrayList studyProducts) {
        this.studyProducts = studyProducts;
    }

    public ArrayList getStudySizes() {
        return this.studySizes;
    }

    public void setStudySizes(ArrayList studySizes) {
        this.studySizes = studySizes;
    }

    public ArrayList getStudyDurations() {
        return this.studyDurations;
    }

    public void setStudyDurations(ArrayList studyDurations) {
        this.studyDurations = studyDurations;
    }

    public ArrayList getStudyDurationUnits() {
        return this.studyDurationUnits;
    }

    public void setStudyDurationUnits(ArrayList studyDurationUnits) {
        this.studyDurationUnits = studyDurationUnits;
    }

    public ArrayList getStudyEstBeginDates() {
        return this.studyEstBeginDates;
    }

    public void setStudyEstBeginDates(ArrayList studyEstBeginDates) {
        this.studyEstBeginDates = studyEstBeginDates;
    }

    public ArrayList getStudyActBeginDates() {
        return this.studyActBeginDates;
    }

    public void setStudyActBeginDates(ArrayList studyActBeginDates) {
        this.studyActBeginDates = studyActBeginDates;
    }

    public ArrayList getStudyPrimInvs() {
        return this.studyPrimInvs;
    }

    public void setStudyPrimInvs(ArrayList studyPrimInvs) {
        this.studyPrimInvs = studyPrimInvs;
    }

    public ArrayList getStudyPartCenters() {
        return this.studyPartCenters;
    }

    public void setStudyPartCenters(ArrayList studyPartCenters) {
        this.studyPartCenters = studyPartCenters;
    }

    public ArrayList getStudyKeywords() {
        return this.studyKeywords;
    }

    public void setStudyKeywords(ArrayList studyKeywords) {
        this.studyKeywords = studyKeywords;
    }

    public ArrayList getStudyPubCols() {
        return this.studyPubCols;
    }

    public void setStudyPubCols(ArrayList studyPubCols) {
        this.studyPubCols = studyPubCols;
    }

    public ArrayList getStudyParents() {
        return this.studyParents;
    }

    public void setStudyParents(ArrayList studyParents) {
        this.studyParents = studyParents;
    }

    public ArrayList getStudyAuthors() {
        return this.studyAuthors;
    }

    public void setStudyAuthors(ArrayList studyAuthors) {
        this.studyAuthors = studyAuthors;
    }

    public ArrayList getStudyTAreas() {
        return this.studyTAreas;
    }

    public void setStudyTAreas(ArrayList studyTAreas) {
        this.studyTAreas = studyTAreas;
    }

    public ArrayList getStudyPhases() {
        return this.studyPhases;
    }

    public void setStudyPhases(ArrayList studyPhases) {
        this.studyPhases = studyPhases;
    }

    public ArrayList getStudyBlindings() {
        return this.studyBlindings;
    }

    public void setStudyBlindings(ArrayList studyBlindings) {
        this.studyBlindings = studyBlindings;
    }

    public ArrayList getStudyRandoms() {
        return this.studyRandoms;
    }

    public void setStudyRandoms(ArrayList studyRandoms) {
        this.studyRandoms = studyRandoms;
    }

    public ArrayList getStudyVersions() {
        return this.studyVersions;
    }

    public void setStudyVersions(ArrayList studyVersions) {
        this.studyVersions = studyVersions;
    }

    public ArrayList getStudyCurrents() {
        return this.studyCurrents;
    }

    public void setStudyCurrents(ArrayList studyCurrents) {
        this.studyCurrents = studyCurrents;
    }

    public ArrayList getStudyNumbers() {
        return this.studyNumbers;
    }

    public void setStudyNumbers(ArrayList studyNumbers) {
        this.studyNumbers = studyNumbers;
    }

    public ArrayList getStudyStatNotes() {
        return this.studyStatNotes;
    }

    public void setStudyStatNotes(ArrayList studyStatNotes) {
        this.studyStatNotes = studyStatNotes;
    }

    public ArrayList getStudyStatDates() {
        return this.studyStatDates;
    }

    public void setStudyStatDates(ArrayList studyStatDates) {
        this.studyStatDates = studyStatDates;
    }
    
    //JM: 01.25.2006
    
    public ArrayList getStudySites() {
        return this.studySites;
    }

    public void setStudySites(ArrayList studySites) {
        this.studySites = studySites;
    }
    
    public void setStudySites(String studySite) {
    	studySites.add(studySite);
    }    
    
    public ArrayList getStudyResTypes() {
        return this.studyResTypes;
    }

    public void setStudyResTypes(ArrayList studyResTypes) {
        this.studyResTypes = studyResTypes;
    }
    // Amarnadh :: 28th Nov '07
    public ArrayList getStudyScopes() {
    	   	return this.studyScopes;
    }
    
    public void setStudyScopes(ArrayList studyScopes){
    	 	this.studyScopes = studyScopes;
    }

    public ArrayList getStudyTypes() {
        return this.studyTypes;
    }

    public void setStudyTypes(ArrayList studyTypes) {
        this.studyTypes = studyTypes;
    }

    public ArrayList getStudyStatuses() {
        return this.studyStatuses;
    }

    public void setStudyStatuses(ArrayList studyStatuses) {
        this.studyStatuses = studyStatuses;
    }

    public ArrayList getStudySubtypes() {
        return this.studySubtypes;
    }

    public void setStudySubtypes(ArrayList studySubtypes) {
        this.studySubtypes = studySubtypes;
    }

    public ArrayList getStudyInfos() {
        return this.studyInfos;
    }

    public void setStudyInfos(ArrayList studyInfos) {
        this.studyInfos = studyInfos;
    }

    public int getVerifyValue() {
        return this.verifyValue;
    }

    public void setVerifyValue(int verifyValue) {
        this.verifyValue = verifyValue;
    }

    public ArrayList getStudyTeamRights() {
        return this.studyTeamRights;
    }

    public void setStudyTeamRights(ArrayList studyTeamRights) {
        this.studyTeamRights = studyTeamRights;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setStudyIds(Integer studyId) {
        studyIds.add(studyId);
    }

    public void setStudyAccountIds(String studyAccountId) {
        studyAccountIds.add(studyAccountId);
    }

    public void setStudySponsors(String studySponsor) {
        studySponsors.add(studySponsor);
    }
    
    public void setStudyContacts(String studyContact) {
        studyContacts.add(studyContact);
    }

    public void setStudyPubFlags(String studyPubFlag) {
        studyPubFlags.add(studyPubFlag);
    }

    public void setStudyTitles(String studyTitle) {
        studyTitles.add(studyTitle);
    }

    public void setStudyObjectives(String studyObjective) {
        studyObjectives.add(studyObjective);
    }

    public void setStudySummarys(String studySummary) {
        studySummarys.add(studySummary);
    }

    public void setStudyProducts(String studyProduct) {
        studyProducts.add(studyProduct);
    }

    public void setStudySizes(String studySize) {
        studySizes.add(studySize);
    }

    public void setStudyDurations(String studyDuration) {
        studyDurations.add(studyDuration);
    }

    public void setStudyDurationUnits(String studyDurationUnit) {
        studyDurationUnits.add(studyDurationUnit);
    }

    public void setStudyEstBeginDates(String studyEstBeginDate) {
        studyEstBeginDates.add(studyEstBeginDate);
    }

    public void setStudyActBeginDates(String studyActBeginDate) {
        studyActBeginDates.add(studyActBeginDate);
    }

    public void setStudyPrimInvs(String studyPrimInv) {
        studyPrimInvs.add(studyPrimInv);
    }

    public void setStudyPartCenters(String studyPartCenter) {
        studyPartCenters.add(studyPartCenter);
    }

    public void setStudyKeywords(String studyKeyword) {
        studyKeywords.add(studyKeyword);
    }

    public void setStudyPubCols(String studyPubCol) {
        studyPubCols.add(studyPubCol);
    }

    public void setStudyParents(String studyParent) {
        studyParents.add(studyParent);
    }

    public void setStudyAuthors(String studyAuthor) {
        studyAuthors.add(studyAuthor);
    }

    public void setStudyTAreas(String studyTArea) {
        studyTAreas.add(studyTArea);
    }

    public void setStudyPhases(String studyPhase) {
        studyPhases.add(studyPhase);
    }

    public void setStudyBlindings(String studyBlinding) {
        studyBlindings.add(studyBlinding);
    }

    public void setStudyRandoms(String studyRandom) {
        studyRandoms.add(studyRandom);
    }

    public void setStudyVersions(String studyVersion) {
        studyVersions.add(studyVersion);
    }

    public void setStudyCurrents(String studyCurrent) {
        studyCurrents.add(studyCurrent);
    }

    public void setStudyNumbers(String studyNumber) {
        studyNumbers.add(studyNumber);
    }

    public void setStudyResTypes(String studyResType) {
        studyResTypes.add(studyResType);
    }
    // Amarnadh :: 28th Nov '07 
    public void setStudyScopes(String studyScope){
    	  	studyScopes.add(studyScope);
    }

    public void setStudyTypes(String studyType) {
        studyTypes.add(studyType);
    }

    public void setStudyInfos(String studyInfo) {
        studyInfos.add(studyInfo);
    }

    public void setStudyStatuses(String studyStatus) {
        studyStatuses.add(studyStatus);
    }

    public void setStudySubtypes(String studySubtype) {
        studySubtypes.add(studySubtype);
    }

    public void setStudyStatNotes(String notes) {
        studyStatNotes.add(notes);
    }

    public void setStudyStatDates(String statDate) {
        studyStatDates.add(statDate);
    }

    public void setStudyTeamRights(String stdTeamRights) {
        studyTeamRights.add(stdTeamRights);
    }
    
  //AK:Added for enhancement INVP9.1 autocomplete (BUG#6714)
    public ArrayList getStudyAutoNames() {
		return this.studyAutoNames;
	}

	public void setStudyAutoNames(String studyAutoNames) {
		this.studyAutoNames.add(studyAutoNames);
	}

	public ArrayList getStudyAutoIds() {
		return this.studyAutoIds;
	}

	public void setStudyAutoIds(String studyAutoIds) {
		this.studyAutoIds.add(studyAutoIds);
	}

    
    // end of getter and setter methods

    public void getStudyValuesKeyword(String search) {
    	
    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "STUDY_SPONSOR, "
                            + "FK_ACCOUNT, "
                            + "STUDY_CONTACT, "
                            + "STUDY_PUBFLAG, "
                            + "STUDY_TITLE, "
                            + "STUDY_OBJ_CLOB, "
                            + "STUDY_SUM_CLOB, "
                            + "STUDY_PRODNAME, "
                            + "STUDY_SAMPLSIZE, "
                            + "STUDY_DUR, "
                            + "STUDY_DURUNIT, "
                            + "STUDY_ESTBEGINDT, "
                            + "STUDY_ACTUALDT, "
                            + "STUDY_PRINV, "
                            + "STUDY_PARTCNTR, "
                            + "STUDY_KEYWRDS, "
                            + "STUDY_PUBCOLLST, "
                            + "STUDY_PARENTID, "
                            + "FK_AUTHOR, "
                            + "FK_CODELST_TAREA, "
                            + "FK_CODELST_PHASE, "
                            + "FK_CODELST_BLIND, "
                            + "FK_CODELST_RANDOM, "
                            + "STUDY_CURRENT, "
                            + "STUDY_NUMBER, "
                            + "FK_CODELST_RESTYPE, "
                            + "FK_CODELST_SCOPE, "
                            + "FK_CODELST_TYPE, "
                            + "STUDY_INFO "
                            + "from er_study "
                            + "where (STUDY_PARENTID is null) "
                            + "and STUDY_VERPARENT is null "
                            + "and (STUDY_PUBFLAG = 'Y')"
                            + "and ((select instr((select upper(STUDY_KEYWRDS) from er_study a where  a.pk_study = er_study.pk_study), upper(?),1,1) \"Instring\" from Dual) > 0)");
            // In the last line of the above query the \"Instring\" from Dual is
            // unnecessary it can be removed -- Sajal
            pstmt.setString(1, search);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                setStudyAccountIds(rs.getString("FK_ACCOUNT"));
                setStudySponsors(rs.getString("STUDY_SPONSOR"));
                setStudyContacts(rs.getString("STUDY_CONTACT"));
                setStudyPubFlags(rs.getString("STUDY_PUBFLAG"));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                
                //JM
                //setStudyObjectives(rs.getString("STUDY_OBJ"));
                //setStudySummarys(rs.getString("STUDY_SUM"));
                
                objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                if (!(objCLob == null)) {
                	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesKeyword rows finalStr" + objFinalStr);
                setStudyObjectives(objFinalStr);
                
                
                sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                if (!(sumCLob == null)) {
                	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesKeyword rows finalStr" + sumFinalStr);
                setStudySummarys(sumFinalStr);
                ///////////              
                
                
                setStudyProducts(rs.getString("STUDY_PRODNAME"));
                setStudySizes(rs.getString("STUDY_SAMPLSIZE"));
                setStudyDurations(rs.getString("STUDY_DUR"));
                setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyPrimInvs(rs.getString("STUDY_PRINV"));
                setStudyPartCenters(rs.getString("STUDY_PARTCNTR"));
                setStudyKeywords(rs.getString("STUDY_KEYWRDS"));
                setStudyPubCols(rs.getString("STUDY_PUBCOLLST"));
                setStudyParents(rs.getString("STUDY_PARENTID"));
                setStudyAuthors(rs.getString("FK_AUTHOR"));
                setStudyTAreas(rs.getString("FK_CODELST_TAREA"));
                setStudyPhases(rs.getString("FK_CODELST_PHASE"));
                setStudyBlindings(rs.getString("FK_CODELST_BLIND"));
                setStudyRandoms(rs.getString("FK_CODELST_RANDOM"));
                setStudyCurrents(rs.getString("STUDY_CURRENT"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                setStudyResTypes(rs.getString("FK_CODELST_RESTYPE"));
                setStudyScopes(rs.getString("FK_CODELST_SCOPE")); //Amar
                setStudyTypes(rs.getString("FK_CODELST_TYPE"));
                setStudyInfos(rs.getString("STUDY_INFO"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesKeyword EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getStudyValuesKeywordAll(ArrayList search) {
        
    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
    	
    	int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int length = 0;
        String nullStr = "";
        String finalStr = "";
        String kwd = "";
        int tempVal = 0;
        
       String sql = "";
       
        try {

            length = search.size();
            nullStr = " upper(STUDY_KEYWRDS) like '%%'";

            if (length != 0) {
                for (int i = 0; i < length; i++) {
                    tempVal = i + 1;
                    kwd = (String) search.get(i);
                    kwd = kwd.toUpperCase();

                    if (tempVal != length && tempVal == 1) {
                        finalStr = finalStr + "( upper(STUDY_KEYWRDS) like"
                                + " " + "'%" + kwd + "%' or";

                    } else if (tempVal != length && tempVal != 1) {
                        finalStr = finalStr + " upper(STUDY_KEYWRDS) like"
                                + " " + "'%" + kwd + "%' or";
                    } else if (tempVal == length) {
                        finalStr = finalStr + " upper(STUDY_KEYWRDS) like"
                                + " " + "'%" + kwd + "%')";
                    }

                }
                if (tempVal == 1) {
                    finalStr = " upper(STUDY_KEYWRDS) like" + " " + "'%" + kwd
                            + "%'";
                }
            } else {
                finalStr = nullStr;
            }

            conn = getConnection();
            
            sql = "select PK_STUDY, "
                + "STUDY_SPONSOR, " + "FK_ACCOUNT, " + "STUDY_CONTACT, "
                + "STUDY_PUBFLAG, " + "STUDY_TITLE, " + "STUDY_OBJ_CLOB, "
                + "STUDY_SUM_CLOB, " + "STUDY_PRODNAME, " + "STUDY_SAMPLSIZE, "
                + "STUDY_DUR, " + "STUDY_DURUNIT, " + "STUDY_ESTBEGINDT, "
                + "STUDY_ACTUALDT, " + "STUDY_PRINV, " + "STUDY_PARTCNTR, "
                + "STUDY_KEYWRDS, " + "STUDY_PUBCOLLST, "
                + "STUDY_PARENTID, " + "FK_AUTHOR, " + "FK_CODELST_TAREA, "
                + "FK_CODELST_PHASE, " + "FK_CODELST_BLIND, "
                + "FK_CODELST_RANDOM, " + "STUDY_CURRENT, "
                + "STUDY_NUMBER, " + "FK_CODELST_RESTYPE, " + "FK_CODELST_SCOPE, "
                + "FK_CODELST_TYPE, " + "STUDY_INFO " + "from er_study "
                + "where (STUDY_PARENTID is null) "
                + "and STUDY_VERPARENT is null "
                + "and (STUDY_PUBFLAG = 'Y')" + "and" + finalStr;
            
            Rlog.debug("STUDY", "StudyDao.getStudyValuesKeywordAll sql" + sql);

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                setStudyAccountIds(rs.getString("FK_ACCOUNT"));
                setStudySponsors(rs.getString("STUDY_SPONSOR"));               
                setStudyContacts(rs.getString("STUDY_CONTACT"));
                setStudyPubFlags(rs.getString("STUDY_PUBFLAG"));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                    
                //JM:
                //setStudyObjectives(rs.getString("STUDY_OBJ"));
                //setStudySummarys(rs.getString("STUDY_SUM"));
                
                
                objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                if (!(objCLob == null)) {
                	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesKeywordAll rows finalStr" + objFinalStr);
                setStudyObjectives(objFinalStr);
                
                
                sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                if (!(sumCLob == null)) {
                	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesKeywordAll rows finalStr" + sumFinalStr);
                setStudySummarys(sumFinalStr);
                        
                //////              
                
                
                
                setStudyProducts(rs.getString("STUDY_PRODNAME"));
                setStudySizes(rs.getString("STUDY_SAMPLSIZE"));
                setStudyDurations(rs.getString("STUDY_DUR"));
                setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyPrimInvs(rs.getString("STUDY_PRINV"));
                setStudyPartCenters(rs.getString("STUDY_PARTCNTR"));
                setStudyKeywords(rs.getString("STUDY_KEYWRDS"));
                setStudyPubCols(rs.getString("STUDY_PUBCOLLST"));
                setStudyParents(rs.getString("STUDY_PARENTID"));
                setStudyAuthors(rs.getString("FK_AUTHOR"));
                setStudyTAreas(rs.getString("FK_CODELST_TAREA"));
                setStudyPhases(rs.getString("FK_CODELST_PHASE"));
                setStudyBlindings(rs.getString("FK_CODELST_BLIND"));
                setStudyRandoms(rs.getString("FK_CODELST_RANDOM"));
                setStudyCurrents(rs.getString("STUDY_CURRENT"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                setStudyResTypes(rs.getString("FK_CODELST_RESTYPE"));
                setStudyScopes(rs.getString("FK_CODELST_SCOPE"));//Amar
                setStudyTypes(rs.getString("FK_CODELST_TYPE"));
                setStudyInfos(rs.getString("STUDY_INFO"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesKeywordAll EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getStudyValuesTitle(String search) {
        
    	
    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
    	
    	
    	
    	int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "STUDY_SPONSOR, "
                            + "FK_ACCOUNT, "
                            + "STUDY_CONTACT, "
                            + "STUDY_PUBFLAG, "
                            + "STUDY_TITLE, "
                            + "STUDY_OBJ_CLOB, "
                            + "STUDY_SUM_CLOB, "
                            + "STUDY_PRODNAME, "
                            + "STUDY_SAMPLSIZE, "
                            + "STUDY_DUR, "
                            + "STUDY_DURUNIT, "
                            + "STUDY_ESTBEGINDT, "
                            + "STUDY_ACTUALDT, "
                            + "STUDY_PRINV, "
                            + "STUDY_PARTCNTR, "
                            + "STUDY_KEYWRDS, "
                            + "STUDY_PUBCOLLST, "
                            + "STUDY_PARENTID, "
                            + "FK_AUTHOR, "
                            + "FK_CODELST_TAREA, "
                            + "FK_CODELST_PHASE, "
                            + "FK_CODELST_BLIND, "
                            + "FK_CODELST_RANDOM, "
                            + "STUDY_CURRENT, "
                            + "STUDY_NUMBER, "
                            + "FK_CODELST_RESTYPE, "
                            + "FK_CODELST_SCOPE, "
                            + "FK_CODELST_TYPE, "
                            + "STUDY_INFO "                            
                            + "from er_study "
                            + "where (STUDY_PARENTID is null) "
                            + "and STUDY_VERPARENT is null "
                            + "and (STUDY_PUBFLAG = 'Y')"
                            + "and ((select instr((select upper(STUDY_TITLE) from er_study a where  a.pk_study = er_study.pk_study), upper(?),1,1) \"Instring\" from Dual) > 0)");
            pstmt.setString(1, search);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                setStudyAccountIds(rs.getString("FK_ACCOUNT"));
                setStudySponsors(rs.getString("STUDY_SPONSOR"));
                setStudyContacts(rs.getString("STUDY_CONTACT"));
                setStudyPubFlags(rs.getString("STUDY_PUBFLAG"));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                
                //JM:
                //setStudyObjectives(rs.getString("STUDY_OBJ"));
                //setStudySummarys(rs.getString("STUDY_SUM"));
                

                objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                if (!(objCLob == null)) {
                	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesTitle rows finalStr" + objFinalStr);
                setStudyObjectives(objFinalStr);
                
                
                sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                if (!(sumCLob == null)) {
                	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesTitle rows finalStr" + sumFinalStr);
                setStudySummarys(sumFinalStr);
                        
                //////
                
                setStudyProducts(rs.getString("STUDY_PRODNAME"));
                setStudySizes(rs.getString("STUDY_SAMPLSIZE"));
                setStudyDurations(rs.getString("STUDY_DUR"));
                setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyPrimInvs(rs.getString("STUDY_PRINV"));
                setStudyPartCenters(rs.getString("STUDY_PARTCNTR"));
                setStudyKeywords(rs.getString("STUDY_KEYWRDS"));
                setStudyPubCols(rs.getString("STUDY_PUBCOLLST"));
                setStudyParents(rs.getString("STUDY_PARENTID"));
                setStudyAuthors(rs.getString("FK_AUTHOR"));
                setStudyTAreas(rs.getString("FK_CODELST_TAREA"));
                setStudyPhases(rs.getString("FK_CODELST_PHASE"));
                setStudyBlindings(rs.getString("FK_CODELST_BLIND"));
                setStudyRandoms(rs.getString("FK_CODELST_RANDOM"));
                setStudyCurrents(rs.getString("STUDY_CURRENT"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                setStudyResTypes(rs.getString("FK_CODELST_RESTYPE"));
                setStudyScopes(rs.getString("FK_CODELST_SCOPE"));//Amar
                setStudyTypes(rs.getString("FK_CODELST_TYPE"));
                setStudyInfos(rs.getString("STUDY_INFO"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesTitle EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getStudyValuesAuthor(String search) {
        

    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
    	int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "STUDY_SPONSOR, "                            
                            + "FK_ACCOUNT, "
                            + "STUDY_CONTACT, "
                            + "STUDY_PUBFLAG, "
                            + "STUDY_TITLE, "
                            + "STUDY_OBJ_CLOB, "
                            + "STUDY_SUM_CLOB, "
                            + "STUDY_PRODNAME, "
                            + "STUDY_SAMPLSIZE, "
                            + "STUDY_DUR, "
                            + "STUDY_DURUNIT, "
                            + "STUDY_ESTBEGINDT, "
                            + "STUDY_ACTUALDT, "
                            + "STUDY_PRINV, "
                            + "STUDY_PARTCNTR, "
                            + "STUDY_KEYWRDS, "
                            + "STUDY_PUBCOLLST, "
                            + "STUDY_PARENTID, "
                            + "FK_AUTHOR, "
                            + "FK_CODELST_TAREA, "
                            + "FK_CODELST_PHASE, "
                            + "FK_CODELST_BLIND, "
                            + "FK_CODELST_RANDOM, "
                            + "STUDY_CURRENT, "
                            + "STUDY_NUMBER, "
                            + "FK_CODELST_RESTYPE, "
                            + "FK_CODELST_SCOPE, "
                            + "FK_CODELST_TYPE, "
                            + "STUDY_INFO "
                            + "from er_study "
                            + "where (STUDY_PARENTID is null) "
                            + "and STUDY_VERPARENT is null "
                            + "and (STUDY_PUBFLAG = 'Y')"
                            + "and ((select instr((select upper(USR_LASTNAME) from er_user where  pk_user = fk_author), upper(?),1,1) "
                            + " + instr((select upper(USR_FIRSTNAME) from er_user where  pk_user = fk_author), upper(?),1,1) \"Instring\" from Dual) > 0)");
            pstmt.setString(1, search);
            pstmt.setString(2, search);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                setStudyAccountIds(rs.getString("FK_ACCOUNT"));
                setStudySponsors(rs.getString("STUDY_SPONSOR"));                
                setStudyContacts(rs.getString("STUDY_CONTACT"));
                setStudyPubFlags(rs.getString("STUDY_PUBFLAG"));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                
                //JM:
                //setStudyObjectives(rs.getString("STUDY_OBJ"));
                //setStudySummarys(rs.getString("STUDY_SUM"));
                
                
                objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                if (!(objCLob == null)) {
                	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesAuthor rows finalStr" + objFinalStr);
                setStudyObjectives(objFinalStr);
                
                
                sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                if (!(sumCLob == null)) {
                	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesAuthor rows finalStr" + sumFinalStr);
                setStudySummarys(sumFinalStr);
                        
                //////
                
                setStudyProducts(rs.getString("STUDY_PRODNAME"));
                setStudySizes(rs.getString("STUDY_SAMPLSIZE"));
                setStudyDurations(rs.getString("STUDY_DUR"));
                setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyPrimInvs(rs.getString("STUDY_PRINV"));
                setStudyPartCenters(rs.getString("STUDY_PARTCNTR"));
                setStudyKeywords(rs.getString("STUDY_KEYWRDS"));
                setStudyPubCols(rs.getString("STUDY_PUBCOLLST"));
                setStudyParents(rs.getString("STUDY_PARENTID"));
                setStudyAuthors(rs.getString("FK_AUTHOR"));
                setStudyTAreas(rs.getString("FK_CODELST_TAREA"));
                setStudyPhases(rs.getString("FK_CODELST_PHASE"));
                setStudyBlindings(rs.getString("FK_CODELST_BLIND"));
                setStudyRandoms(rs.getString("FK_CODELST_RANDOM"));
                setStudyCurrents(rs.getString("STUDY_CURRENT"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                setStudyResTypes(rs.getString("FK_CODELST_RESTYPE"));
                setStudyScopes(rs.getString("FK_CODELST_SCOPE"));
                setStudyTypes(rs.getString("FK_CODELST_TYPE"));
                setStudyInfos(rs.getString("STUDY_INFO"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesAuthor EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getStudyValuesTarea(String search) {
        

    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
    	int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "STUDY_SPONSOR, "
                            + "FK_ACCOUNT, "
                            + "STUDY_CONTACT, "
                            + "STUDY_PUBFLAG, "
                            + "STUDY_TITLE, "
                            + "STUDY_OBJ_CLOB, "
                            + "STUDY_SUM_CLOB, "
                            + "STUDY_PRODNAME, "
                            + "STUDY_SAMPLSIZE, "
                            + "STUDY_DUR, "
                            + "STUDY_DURUNIT, "
                            + "STUDY_ESTBEGINDT, "
                            + "STUDY_ACTUALDT, "
                            + "STUDY_PRINV, "
                            + "STUDY_PARTCNTR, "
                            + "STUDY_KEYWRDS, "
                            + "STUDY_PUBCOLLST, "
                            + "STUDY_PARENTID, "
                            + "FK_AUTHOR, "
                            + "FK_CODELST_TAREA, "
                            + "FK_CODELST_PHASE, "
                            + "FK_CODELST_BLIND, "
                            + "FK_CODELST_RANDOM, "
                            + "STUDY_CURRENT, "
                            + "STUDY_NUMBER, "
                            + "FK_CODELST_RESTYPE, "
                            + "FK_CODELST_SCOPE, "
                            + "FK_CODELST_TYPE, "
                            + "STUDY_INFO "
                            + "from er_study "
                            + "where (STUDY_PARENTID is null) "
                            + "and STUDY_VERPARENT is null "
                            + "and (STUDY_PUBFLAG = 'Y')"
                            + " and ((select instr((select upper(CODELST_DESC) from er_CODELST where  PK_CODELST = FK_CODELST_TAREA), upper(?),1,1) \"Instring\" from Dual) > 0)");
            pstmt.setString(1, search);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                setStudyAccountIds(rs.getString("FK_ACCOUNT"));
                setStudySponsors(rs.getString("STUDY_SPONSOR"));
                setStudyContacts(rs.getString("STUDY_CONTACT"));
                setStudyPubFlags(rs.getString("STUDY_PUBFLAG"));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                
                //JM
                //setStudyObjectives(rs.getString("STUDY_OBJ"));
                //setStudySummarys(rs.getString("STUDY_SUM"));
                
                objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                if (!(objCLob == null)) {
                	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesTarea rows finalStr" + objFinalStr);
                setStudyObjectives(objFinalStr);
                
                
                sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                if (!(sumCLob == null)) {
                	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesTarea rows finalStr" + sumFinalStr);
                setStudySummarys(sumFinalStr);
                        
                //////
                
                setStudyProducts(rs.getString("STUDY_PRODNAME"));
                setStudySizes(rs.getString("STUDY_SAMPLSIZE"));
                setStudyDurations(rs.getString("STUDY_DUR"));
                setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyPrimInvs(rs.getString("STUDY_PRINV"));
                setStudyPartCenters(rs.getString("STUDY_PARTCNTR"));
                setStudyKeywords(rs.getString("STUDY_KEYWRDS"));
                setStudyPubCols(rs.getString("STUDY_PUBCOLLST"));
                setStudyParents(rs.getString("STUDY_PARENTID"));
                setStudyAuthors(rs.getString("FK_AUTHOR"));
                setStudyTAreas(rs.getString("FK_CODELST_TAREA"));
                setStudyPhases(rs.getString("FK_CODELST_PHASE"));
                setStudyBlindings(rs.getString("FK_CODELST_BLIND"));
                setStudyRandoms(rs.getString("FK_CODELST_RANDOM"));
                setStudyCurrents(rs.getString("STUDY_CURRENT"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                setStudyResTypes(rs.getString("FK_CODELST_RESTYPE"));
                setStudyScopes(rs.getString("FK_CODELST_SCOPE"));
                setStudyTypes(rs.getString("FK_CODELST_TYPE"));
                setStudyInfos(rs.getString("STUDY_INFO"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesTarea EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /* Modified by Sonia Abrol, 06/03/05 made order by case insensitive */
    /*Modified by Sonia Abrol, 10/23/07 changed the sql for revised super user implementation*/
    public void getStudyValuesForUsers(String userId) {
        
    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
    	UserDao usrDao = new UserDao();
    	usrDao.setUsrIds(StringUtil.stringToNum(userId));
    	usrDao.getUsersDetails(userId);
    	
    	String accId ="0";  		
    	ArrayList accIdList = new ArrayList();
    	accIdList= usrDao.getUsrAccIds();
    	accId = ""+accIdList.get(0);
    	
    	int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
 

            // modified by sonia - the user should see only those studies if he
            // belongs to the study team - as all tab pages need studyteam
            // rights
            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "STUDY_TITLE, "
                            + "STUDY_OBJ_CLOB, "
                            + "STUDY_SUM_CLOB, "
                            + "STUDY_DUR, "
                            + "STUDY_DURUNIT, "
                            + "STUDY_ESTBEGINDT, "
                            + "STUDY_ACTUALDT, "
                            + "STUDY_PARENTID, "
                            + "FK_AUTHOR, "
                            + "STUDY_NUMBER  "
                            + "from er_study "
                            + "where fk_account = ?"
                            + " AND ( exists (select * from er_studyteam where pk_study = fk_study and  fk_user = ? and study_team_usr_type = 'D'  ) "
                            + " or pkg_superuser.F_Is_Superuser(?, pk_study) = 1 ) and STUDY_VERPARENT is null "
                            + "order by lower(STUDY_NUMBER)  ");

            pstmt.setInt(1, StringUtil.stringToNum(accId));
            pstmt.setInt(2, StringUtil.stringToNum(userId));
            pstmt.setInt(3, StringUtil.stringToNum(userId));
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                
                //JM:
                //setStudyObjectives(rs.getString("STUDY_OBJ"));
                //setStudySummarys(rs.getString("STUDY_SUM"));
                
                objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                if (!(objCLob == null)) {
                	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesForUsers rows finalStr" + objFinalStr);
                setStudyObjectives(objFinalStr);
                
                
                sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                if (!(sumCLob == null)) {
                	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getStudyValuesForUsers rows finalStr" + sumFinalStr);
                setStudySummarys(sumFinalStr);
                        
                //////
                
                setStudyDurations(rs.getString("STUDY_DUR"));
                setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyParents(rs.getString("STUDY_PARENTID"));
                setStudyAuthors(rs.getString("FK_AUTHOR"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesForUsers EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*Modified by Sonia Abrol, 10/23/07 changed the sql for revised super user implementation*/
    //check bugs Bug2537, 2536, 2528,Bug2493,2528
    public void getStudyValuesForUsers(String userId, String studycount) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
 
        String superuserRights = "";
        int pkstudystat = 0;
        String sqlstr = "";
        try {
            conn = getConnection();

            //get default super user rights , returns null if user does not belong to a super user group
            superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);
            
            
            //modified by sonia - we will not use comma separated string of statuses for browser. use query instead
            
             
            //get all status details, whether it will be shown in the browser or not will be decided in the JSP,
            //no point making a very complicated sql just for few records
             
             sqlstr = " select d.* from ( "
                    + " select a.pk_study, "
                    + " a.study_title, "
                    + " a.STUDY_ACTUALDT, "
                    + "  NVL(a.last_modified_date,a.created_on)   last_modified_date, "
                    + " a.STUDY_NUMBER, "
                    + " (nvl((select study_team_rights from er_studyteam where fk_user = ? and " 
            		+ " fk_study = pk_study  and study_team_usr_type = 'D'), ? ) ) as	study_team_rights, "
                    + " nvl( (SELECT MAX(pk_studystat) FROM ER_STUDYSTAT WHERE " 
                    + " fk_study = pk_study and fk_site=(select fk_siteid from er_user where pk_user=?) " 
                    + " AND fk_codelst_studystat IN ( select pk_codelst "
                    + " from er_codelst  where codelst_type='studystat'   and (trim(codelst_custom_col)='browser') "
                    + " )  AND studystat_date = (SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE " 
                    + " fk_codelst_studystat IN ( select pk_codelst from er_codelst "   
                    + " where codelst_type='studystat'   and (trim(codelst_custom_col)='browser') "
                    + ") AND fk_study = pk_study and fk_site=(select fk_siteid from er_user where pk_user=?))),0)as pk_studystat"
                    + " from er_study a"
                    + " where a.STUDY_VERPARENT is null and fk_account = (select fk_account from er_user where pk_user = ?) "
                    + " and ( exists ( select * from er_studyteam where fk_study = pk_study and " +
                     " fk_user = ? and  nvl(study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser(?, pk_study) = 1 )  "
                    + " order by last_modified_date desc ) d  "
                    + " where   rownum <=? ";

            Rlog.fatal("study", "sql for getStudyValuesForUsers:: " + sqlstr);
            pstmt = conn.prepareStatement(sqlstr);
            pstmt.setInt(1, StringUtil.stringToNum(userId));
            pstmt.setString(2, superuserRights);
            pstmt.setInt(3, StringUtil.stringToNum(userId));
            pstmt.setInt(4, StringUtil.stringToNum(userId));
            pstmt.setInt(5, StringUtil.stringToNum(userId));
            pstmt.setInt(6, StringUtil.stringToNum(userId));
            pstmt.setInt(7, StringUtil.stringToNum(userId)); 
            pstmt.setInt(8, StringUtil.stringToNum(studycount));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                //setStudyTitles(rs.getString("STUDY_TITLE"));
                
                //Modified By Parminder Singh Bug10300#
                setStudyTitles(StringUtil.stripScript(rs.getString("STUDY_TITLE")));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                setStudyTeamRights(rs.getString("STUDY_TEAM_RIGHTS"));
                
                pkstudystat = rs.getInt("PK_STUDYSTAT");
                
                if (pkstudystat  <= 0)
                {
                	setStudyStatuses("...");
                    setStudySubtypes("-");
                    setStudyStatNotes("-");
                    setStudyStatDates("-");
                    setStudySites("-");
                	
                }
                else
                {
                	//get the status information using the PK_STUDYSTAT
                	StudyStatusDao sd = new StudyStatusDao();
                	
                	sd.getStudyStatusDetails(pkstudystat);

                    setStudyStatuses(sd.getDescStudyStats(0));
                    setStudySubtypes(sd.getSubTypeStudyStats(0));
                    setStudyStatNotes(sd.getStatNotes(0));
                    setStudyStatDates(sd.getStatStartDates(0));
                    setStudySites(sd.getSiteNames(0));
                	
                }	
                

                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesForUsers EXCEPTION IN FETCHING FROM Study table"
                            + ex);
            Rlog.fatal("study",
                    "StudyDao.getStudyValuesForUsers sql:"
                            + sqlstr);
            
            
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void verifyStudyRequest(String studyId, String userId) {
        CallableStatement cstmt = null;
        Connection conn = null;
        Rlog.debug("study", "In verifyStudyRequest in StudyDao line number 1");
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_verifyExUser(?,?,?)}");
            cstmt.setString(1, studyId);
            cstmt.setString(2, userId);
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
            cstmt.execute();
            this.setVerifyValue(cstmt.getInt(3));
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.verifyStudyRequest EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int getStudyCount(String accountId) {
        int count = 0;
        String sql = "select count(pk_study) count from er_study where fk_account = "
                + StringUtil.stringToNum(accountId);
        Rlog.debug("study", "StudyDao.getStudyCount prepared statement is ***"
                + sql);
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");
        } catch (SQLException ex) {

            Rlog.fatal("study",
                    "StudyDao.getStudyCount EXCEPTION IN FETCHING FROM Study table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return count;
    }

    public void copyStudy(int studyId, int xUser, int author,
            String permission, int msgId) {

        CallableStatement cstmt = null;
        Rlog.debug("study", "In copyStudy in StduyDao line number 0");
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_copystudy(?,?,?,?,?,?,?)}");

            cstmt.setInt(1, studyId);

            cstmt.setInt(2, xUser);

            cstmt.setInt(3, author);

            cstmt.setString(4, permission);

            cstmt.setInt(5, msgId);

            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.registerOutParameter(7, OracleTypes.CURSOR);

            cstmt.execute();

        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "In copyStudy in StudyDao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    // to delete study.
    // km
    public void deleteStudy(int studyId) {
        CallableStatement cstmt = null;
        Rlog.debug("study", "In deleteStudy in StduyDao line number 0");
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call delete_study(?)}");

            cstmt.setInt(1, studyId);
            cstmt.execute();
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "In deleteStudy in StudyDao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    // km

    // to delete a patient from a particular study
    // km
    // modified for INF-18183 ::: AGodara
    public int deletePatStudy(int studyId, int patId) {
        CallableStatement cstmt = null;
        Rlog.debug("study", "In deletePatStudy in StduyDao line number 0");
        Connection conn = null;
        
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_delete_patstudy(?,?)}");

            cstmt.setInt(1, studyId);
            cstmt.setInt(2, patId);
            cstmt.execute();
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "In deletePatStudy in StudyDao line EXCEPTION IN calling the procedure"
                            + ex);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return 1;
    }
    
    

    // km
    // createVersion

    public int createVersion(int studyId, int xUser) {

        CallableStatement cstmt = null;
        boolean success = false;

        Rlog.debug("study", "In createVersion in StduyDao line number 0");
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_studyver(?,?,?)}");

            cstmt.setInt(1, studyId);

            cstmt.setInt(2, xUser);
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);

            cstmt.execute();

            success = true;
            return 0;

        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "In createVersion in StudyDao line EXCEPTION IN calling the procedure"
                            + ex);
            success = false;
            return -1;

        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
    }

    // /////////////

    /* Modified by sOnia Abrol 06/03/05, made order by case insensitive */
    public void getUserStudies(String userId, String dName, int selVal,
            String flag) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        PreparedStatement pseq = null;
        int rightSeq = 0;
        String right = "";
        String superuserRights = "";
        
        UserDao usrDao = new UserDao();
        usrDao.setUsrIds(StringUtil.stringToNum(userId));
        usrDao.getUsersDetails(userId);
    	
    	String accId ="0";  		
    	ArrayList accIdList = new ArrayList();
    	accIdList= usrDao.getUsrAccIds();
    	accId = ""+accIdList.get(0);
        
        

        try {
            conn = getConnection();
            
            //get default super user rights , returns null if user does not belong to a super user group
            superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);
            
            CtrlDao ctrlRight = new CtrlDao();
	   		 
	   		ArrayList arSeq = new ArrayList();
	   		
	   		ctrlRight.getControlValues("study_rights", "STUDYMPAT");
	   		arSeq = ctrlRight.getCSeq();
	   		
	   		if (arSeq.size() > 0)
	   		{
	   			rightSeq =  ((Integer) arSeq.get(0)).intValue(); 
	   		}	
	   		

            // modified by sonia - the user should see only those studies if he
            // belongs to the study team - as all tab pages need studyteam
            // rights

           //JM: 14/09/05 modified, blocked the studies with status as 'permanent closure/Study completed or Retired' ....	
		if (flag.equals("active")) {
				pstmt = conn.prepareStatement(" SELECT * FROM ( select PK_STUDY, "
				+ "STUDY_NUMBER,(nvl((select study_team_rights from er_studyteam where fk_user = ? and " 
            	+ " fk_study = pk_study  and study_team_usr_type = 'D'), ? ) ) STUDY_TEAM_RIGHTS,lower(study_number) as snLower, "
            	+ " '' as closedStudy "
				+ "from er_study "
				+ "where fk_account = ? " 
				+ " and ( exists (select * from er_studyteam where fk_user = ? and study_team_usr_type <> 'X' and fk_study = pk_study  ) "
				+ " or  pkg_superuser.F_Is_Superuser(? , pk_study) = 1 ) "
				+ " and study_actualdt is not null  "
				+ "and er_study.STUDY_VERPARENT is null and not exists ( select * from er_studystat st, er_codelst ec "
				+ " where st.fk_study = pk_study and st.FK_CODELST_STUDYSTAT=ec.PK_CODELST and ec.codelst_subtyp = 'prmnt_cls'  ) "
				+ " ) M WHERE pkg_util.f_getStudyRight(m.study_team_rights , ? ) > 0  order by lower(STUDY_NUMBER) ");

		} 
		//to get the study status 'permanent closure/Study completed or Retired'
		else if (flag.equals("activeForEnrl")) {
			pstmt = conn.prepareStatement("SELECT * FROM ( select PK_STUDY, "
				+ "STUDY_NUMBER,(nvl((select study_team_rights from er_studyteam where fk_user = ? and " 
            	+ " fk_study = pk_study  and study_team_usr_type = 'D'), ? ) ) STUDY_TEAM_RIGHTS,lower(study_number) as snLower, "
            	+ " (select  min(STUDYSTAT_DATE) from er_studystat "
                + " where  fk_study=pk_study and FK_CODELST_STUDYSTAT = "
                + " (select pk_codelst from er_codelst where codelst_type ='studystat' and codelst_subtyp='prmnt_cls')) as closedStudy "
				+ "from er_study "
				+ "where fk_account = ? " 
				+ " and ( exists (select * from er_studyteam where fk_user = ? and study_team_usr_type <> 'X' and fk_study = pk_study  ) "
				+ " or  pkg_superuser.F_Is_Superuser(? , pk_study) = 1 ) "
				+ " and study_actualdt is not null  "
				+ "and er_study.STUDY_VERPARENT is null ) M WHERE pkg_util.f_getStudyRight(m.study_team_rights , ? ) > 0  order by lower(STUDY_NUMBER) ");

		} else {
			pstmt = conn.prepareStatement(" SELECT * FROM ( select PK_STUDY, "
				+ "STUDY_NUMBER,(nvl((select study_team_rights from er_studyteam where fk_user = ? and " 
            	+ " fk_study = pk_study  and study_team_usr_type = 'D'), ? ) ) STUDY_TEAM_RIGHTS,lower(study_number) as snLower, "
            	+ " (select  min(STUDYSTAT_DATE) from er_studystat "
                + " where  fk_study=pk_study and FK_CODELST_STUDYSTAT = "
                + " (select pk_codelst from er_codelst where codelst_type ='studystat' and codelst_subtyp='prmnt_cls')) as closedStudy "
				+ "from er_study "
				+ "where fk_account = ? " 
				+ " and ( exists (select * from er_studyteam where fk_user = ? and study_team_usr_type <> 'X' and fk_study = pk_study  ) "
				+ " or  pkg_superuser.F_Is_Superuser(? , pk_study) = 1 ) "
				+ "and er_study.STUDY_VERPARENT is null  ) M WHERE pkg_util.f_getStudyRight(m.study_team_rights , ? ) > 0  order by lower(STUDY_NUMBER) ");
		}

            pstmt.setInt(1, StringUtil.stringToNum(userId));
            pstmt.setString(2, superuserRights);
            pstmt.setInt(3, StringUtil.stringToNum(accId));
            
            pstmt.setInt(4, StringUtil.stringToNum(userId));
            pstmt.setInt(5, StringUtil.stringToNum(userId));
            pstmt.setInt(6, rightSeq);           

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
   
                    setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                    setStudyNumbers(rs.getString("STUDY_NUMBER"));
                    String closedStudy = rs.getString("closedStudy"); 
                    if (StringUtil.isEmpty(closedStudy)){
                    	setStudyStatDates("");
                    } else {
                    	setStudyStatDates(closedStudy);
                    }
                    rows++;
            }

            setCRows(rows);

            retStr = toPullDown(dName, selVal);

        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getuserstudies EXCEPTION IN FETCHING FROM Study table"
                            + ex);
            retStr = null;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            setStudyDropDown(retStr);

        }

    }

    // /////////////////////

    // /////////////////////////////
// Modified by Gopu to fix the bugzilla issue #2818 (Resize and make size static for the Study Number dropdown.)
    public String toPullDown(String dName, int selValue) {
        Integer studyId = null;
        String studyNum = "";
        String disp = "";

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            mainStr.append("<SELECT NAME=" + dName + "   >");
            Integer val = new java.lang.Integer(selValue);

            mainStr.append("<OPTION value = 0 SELECTED>"+LC.L_All+"</OPTION>");

            for (counter = 0; counter <= studyIds.size() - 1; counter++) {
                studyId = (Integer) studyIds.get(counter);
                studyNum = (String) studyNumbers.get(counter);

                disp = studyNum;

                if (selValue == studyId.intValue()) {
                    mainStr.append("<OPTION value = " + studyId + " SELECTED>"
                            + disp + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = " + studyId + ">" + disp
                            + "</OPTION>");
                }
            }
            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }

    // ///////////////////////////////////
    /* modified by sonia abrol for new super user implementation*/
    
    public void getReportStudyValuesForUsers(String userId) {
        
    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
    	UserDao usrDao = new UserDao();
    	usrDao.setUsrIds(StringUtil.stringToNum(userId));
    	usrDao.getUsersDetails(userId);
    	
    	String accId ="0";  		
    	ArrayList accIdList = new ArrayList();
    	accIdList= usrDao.getUsrAccIds();
    	accId = ""+accIdList.get(0);
    	
    	int rows = 0;
        PreparedStatement pstmt = null;
                  Connection conn = null;
        try {
            conn = getConnection();

          
            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "STUDY_TITLE, "
                            + "STUDY_OBJ_CLOB, "
                            + "STUDY_SUM_CLOB, "
                            + "STUDY_DUR, "
                            + "STUDY_DURUNIT, "
                            + "STUDY_ESTBEGINDT, "
                            + "STUDY_ACTUALDT, "
                            + "STUDY_PARENTID, "
                            + "FK_AUTHOR, "
                            + "STUDY_NUMBER "
                            + "from er_study "
                            + "where  er_study.STUDY_VERPARENT is null "
                            + " and fk_account = ?"
                        	+ " and 	(exists  (select * from ER_STUDYTEAM T "
                      		+ "	where  T.fk_user = ? and "
                     		+ "	T.fk_study = pk_study  and nvl(T.study_team_usr_type,'D')='D' )"
                       	    + "	    or pkg_superuser.F_Is_Superuser(?, pk_study) = 1 )" 
                            + "order by lower(STUDY_NUMBER)  ");

            pstmt.setInt(1, StringUtil.stringToNum(accId));
            pstmt.setString(2, userId);
            pstmt.setString(3, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
              
                        setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                        setStudyTitles(rs.getString("STUDY_TITLE"));
                        
                         
                        objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                        if (!(objCLob == null)) {
                        	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                        }
                         
                        setStudyObjectives(objFinalStr);
                         
                        sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                        if (!(sumCLob == null)) {
                        	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                        }
                         
                        setStudySummarys(sumFinalStr);
                          
                        setStudyDurations(rs.getString("STUDY_DUR"));
                        setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                        setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                        setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                        setStudyParents(rs.getString("STUDY_PARENTID"));
                        setStudyAuthors(rs.getString("FK_AUTHOR"));
                        setStudyNumbers(rs.getString("STUDY_NUMBER"));
                        rows++;
             }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getReportStudyValuesForUsers EXCEPTION IN FETCHING FROM Study table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ////////////////////////////////////////////

    /* Modified by Sonia Abrol, 06/03/05, added order by */
    public void getBudgetStudyValuesForAccount(String accountId) {
        
    	//JM
    	String objFinalStr = "";
    	String sumFinalStr = "";
    	Clob objCLob = null;
    	Clob sumCLob = null;
    	
    	int rows = 0;
        PreparedStatement pstmt = null;
        PreparedStatement pseq = null;

        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "STUDY_TITLE, "
                            + "STUDY_OBJ_CLOB, "
                            + "STUDY_SUM_CLOB, "
                            + "STUDY_DUR, "
                            + "STUDY_DURUNIT, "
                            + "STUDY_ESTBEGINDT, "
                            + "STUDY_ACTUALDT, "
                            + "STUDY_PARENTID, "
                            + "FK_AUTHOR, "
                            + "STUDY_NUMBER "
                            + " from er_study "
                            + " where "
                            + " FK_ACCOUNT = ? "
                            + "and STUDY_VERPARENT is null order by lower(STUDY_NUMBER)");

            pstmt.setInt(1, StringUtil.stringToNum(accountId));
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                
                //JM
                //setStudyObjectives(rs.getString("STUDY_OBJ"));
                //setStudySummarys(rs.getString("STUDY_SUM"));
                
                objCLob  = rs.getClob("STUDY_OBJ_CLOB");                
                if (!(objCLob == null)) {
                	objFinalStr= objCLob.getSubString(1, (int) objCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getBudgetStudyValuesForAccount rows finalStr" + objFinalStr);
                setStudyObjectives(objFinalStr);
                
                
                sumCLob  = rs.getClob("STUDY_SUM_CLOB");                
                if (!(sumCLob == null)) {
                	sumFinalStr= sumCLob.getSubString(1, (int) sumCLob.length());
                }
                Rlog.debug("STUDY", "StudyDao.getBudgetStudyValuesForAccount rows finalStr" + sumFinalStr);
                setStudySummarys(sumFinalStr);
                        
                //////
                
                setStudyDurations(rs.getString("STUDY_DUR"));
                setStudyDurationUnits(rs.getString("STUDY_DURUNIT"));
                setStudyEstBeginDates(rs.getString("STUDY_ESTBEGINDT"));
                setStudyActBeginDates(rs.getString("STUDY_ACTUALDT"));
                setStudyParents(rs.getString("STUDY_PARENTID"));
                setStudyAuthors(rs.getString("FK_AUTHOR"));
                setStudyNumbers(rs.getString("STUDY_NUMBER"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "study",
                            "StudyDao.getBudgetStudyValuesForAccount EXCEPTION IN FETCHING FROM Study table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public String getDiseaseSite(String studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select code_lst_names(er_study.STUDY_DISEASE_SITE) as sites  from er_study where pk_study=?");
            pstmt.setInt(1, StringUtil.stringToNum(studyId));
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                retStr = rs.getString("sites");

            }

            return retStr;
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getDiseaseSites EXCEPTION IN FETCHING FROM Study table"
                            + ex);
            return "error";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Calls New procedure to copy study. This implements selective copy of
     * versions and calendars
     * 
     * @param original
     *            study id
     * @param flag
     *            to copy study status records
     * @param flag
     *            to copy study team records
     * @param array
     *            of study version ids
     * @param array
     *            of study calendar ids
     * @param study
     *            number for new study
     * @param study
     *            data manager id for new study
     * @param study
     *            title for new study
     * @param Logged
     *            in user Id
     * @param IP
     *            of logged in user
     * @returns 0 for successful copy and -1 for error
     */
    
    /*
    public int copyStudySelective(int origstudy, int statflag, int teamflag,
            String[] verarr, String[] calarr, String studynum, int studydm,
            String title, int userId, String ipAdd) {
    */   

    
//JM: 31July2006: Modified    
  
public int copyStudySelective(int origstudy, int statflag, int teamflag,int dictflag, 
        String[] verarr, String[] calarr, String[] txarr, String[] formsarr, String studynum, int studydm,
        String title, int userId, String ipAdd) {
    int ret = 1;
    CallableStatement cstmt = null;
    Connection conn = null;

    try {
        conn = getConnection();

        ArrayDescriptor adVerArr = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY verArray = new ARRAY(adVerArr, conn, verarr);

        ArrayDescriptor adCalArr = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY calArray = new ARRAY(adCalArr, conn, calarr);
        
        ArrayDescriptor stdTxArr = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
        ARRAY txArray = new ARRAY(stdTxArr, conn, txarr );//JM
        
        
        ArrayDescriptor formsArr = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn) ;
        ARRAY formsArray = new ARRAY(formsArr, conn, formsarr );//JM
        
        

        // cstmt = conn.prepareCall("{call PKG_STUDYSTAT.SP_COPYSTUDY_SELECTIVE(?,?,?,?,?,?,?,?,?,?,?)}");
        cstmt = conn
                .prepareCall("{call PKG_STUDYSTAT.SP_COPYSTUDY_SELECTIVE(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

        cstmt.setInt(1, origstudy);
        cstmt.setInt(2, statflag);
        cstmt.setInt(3, teamflag);
        cstmt.setInt(4, dictflag);//
        cstmt.setArray(5, verArray);
        cstmt.setArray(6, calArray);
        
        cstmt.setArray(7, txArray);//        
        cstmt.setArray(8, formsArray);//
        
        cstmt.setString(9, studynum);
        cstmt.setInt(10, studydm);
        cstmt.setString(11, title);
        cstmt.setInt(12, userId);
        cstmt.setString(13, ipAdd);
        cstmt.registerOutParameter(14, java.sql.Types.INTEGER);
        cstmt.execute();

        ret = cstmt.getInt(14);

        return ret;
    } catch (Exception e) {
        Rlog.fatal("study",
                "EXCEPTION in copy_new_study, excecuting Stored Procedure "
                        + e);
        return -1;
    } finally {
        try {
            if (cstmt != null)
                cstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
    }

}
    /**
     * Gets studies which has been given rights for an organization through user
     * deatils as well as from study team access rights.
     * 
     * @param user
     *            Id
     * @param dName
     *            for dropdown
     * @param selVal
     *            for dropdown
     * @param site
     *            id
     *            @deprecated
     */

    public void getUserStudiesWithRights(String userId, String dName,
            int selVal, String siteId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String retStr = "";
        PreparedStatement pseq = null;
        int rightSeq = 0;
        String right = "";

        try {
            conn = getConnection();

            pseq = conn
                    .prepareStatement("select ctrl_seq from er_ctrltab where ctrl_key = 'study_rights' and upper(ctrl_value) = 'STUDYMPAT'");

            ResultSet rsSeq = pseq.executeQuery();

            while (rsSeq.next()) {
                rightSeq = rsSeq.getInt("ctrl_seq");
            }

            pseq.close();

            /*
             * pstmt = conn.prepareStatement("select PK_STUDY, " + "
             * STUDY_NUMBER, PK_STUDYTEAM, STUDY_TEAM_RIGHTS " + " from er_study
             * s, er_studyteam t, er_study_site_rights r, er_usersite u " + "
             * where " + " s.pk_study = t.FK_STUDY and study_actualdt is not
             * null " + " and r.fk_site = u.fk_site " + " and u.fk_user =
             * t.fk_user " + " and r.fk_user = u.fk_user " + " and r.fk_study =
             * t.fk_study " + " and s.pk_study = r.fk_study " + " and t.fk_user = ? " + "
             * and r.fk_user = ? " + " and u.fk_site = ? and s.STUDY_VERPARENT
             * is null " + " and nvl(study_team_usr_type,'D')='D' " + " and
             * r.fk_site= ? " + " and r.USER_STUDY_SITE_RIGHTS = 1 " + " and
             * u.fk_user = ? " + " and u.fk_site = ? " + " and u.usersite_right >
             * 0 ");
             */

            pstmt = conn
                    .prepareStatement("select PK_STUDY, "
                            + "  STUDY_NUMBER, PK_STUDYTEAM, STUDY_TEAM_RIGHTS "
                            + " from er_study s, er_studyteam t, er_study_site_rights r, er_usersite u , er_studysites d "
                            + " where  "
                            + " s.pk_study = t.FK_STUDY and study_actualdt is not null  "
                            + " and r.fk_site = u.fk_site "
                            + " and u.fk_user = t.fk_user "
                            + " and r.fk_user = u.fk_user "
                            + " and r.fk_study = t.fk_study "
                            + " and s.pk_study = r.fk_study "
                            + " and d.fk_study = s.pk_study "
                            + " and d.fk_study = t.fk_study "
                            + " and d.fk_study = r.fk_study "
                            + " and d.fk_site = r.fk_site "
                            + " and d.fk_site = u.fk_site "
                            + " and d.fk_site = ? "
                            + " and t.fk_user = ? "
                            + " and r.fk_user = ? "
                            + " and u.fk_site = ? and s.STUDY_VERPARENT is null  "
                            + " and nvl(study_team_usr_type,'D')='D' "
                            + " and r.fk_site= ? "
                            + " and r.USER_STUDY_SITE_RIGHTS = 1   "
                            + " and u.fk_user = ? "
                            + " and u.fk_site = ? "
                            + " and u.usersite_right > 0 "			    
			    //JM:modified
			    + " and s.pk_study  not in(select st.fk_study from er_codelst ec, er_studystat st "
			    + " where st.FK_CODELST_STUDYSTAT=ec.PK_CODELST and ec.codelst_subtyp='prmnt_cls' ) "
                            /*
                             * + " union " + " select PK_STUDY, " + "
                             * STUDY_NUMBER, PK_STUDYTEAM, STUDY_TEAM_RIGHTS " + "
                             * from er_study o, er_studyteam " + " where " + "
                             * o.pk_study = er_studyteam.FK_STUDY and
                             * study_actualdt is not null and
                             * er_studyteam.fk_user = ? " + " and
                             * o.STUDY_VERPARENT is null " + " and
                             * study_team_usr_type ='S' and 0 = ( Select
                             * count(*) from er_studyteam i where i.fk_study =
                             * o.pk_study and i.fk_user = ? and
                             * i.study_team_usr_type ='D')" + " order by
                             * STUDY_NUMBER ");
                             */
                            + " union "
                            + " select PK_STUDY, "
                            + " STUDY_NUMBER, PK_STUDYTEAM, STUDY_TEAM_RIGHTS "
                            + " from er_study o, er_studyteam t, er_studysites s  "
                            + " where "
                            + " o.pk_study = t.FK_STUDY"
                            + " and t.FK_STUDY = s.fk_study"
                            + " and s.fk_study = o.pk_study "
                            + " and s.fk_site = ? "
                            + " and study_actualdt is not null and t.fk_user = ? "
                            + " and o.STUDY_VERPARENT is null "
                            + " and study_team_usr_type ='S' "
                            + " and  0 = ( Select count(*) from er_studyteam i where "
                            + " i.fk_study = o.pk_study and i.fk_user = ? and i.study_team_usr_type ='D')"
			    //JM:modified
			    + " and o.pk_study  not in(select st.fk_study from er_codelst ec, er_studystat st "
			    + " where st.FK_CODELST_STUDYSTAT=ec.PK_CODELST and ec.codelst_subtyp='prmnt_cls' ) "
                            
			    + " order by STUDY_NUMBER");

            pstmt.setInt(1, StringUtil.stringToNum(siteId));
            pstmt.setInt(2, StringUtil.stringToNum(userId));
            pstmt.setInt(3, StringUtil.stringToNum(userId));
            pstmt.setInt(4, StringUtil.stringToNum(siteId));
            pstmt.setInt(5, StringUtil.stringToNum(siteId));
            pstmt.setInt(6, StringUtil.stringToNum(userId));
            pstmt.setInt(7, StringUtil.stringToNum(siteId));
            pstmt.setInt(8, StringUtil.stringToNum(siteId));
            pstmt.setInt(9, StringUtil.stringToNum(userId));
            pstmt.setInt(10, StringUtil.stringToNum(userId));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                right = rs.getString("STUDY_TEAM_RIGHTS");

                if (right.length() >= 11) {
                    right = right.substring(rightSeq - 1, rightSeq);
                } else {
                    right = "0";
                }

                Rlog.debug("dummy", "*" + right.length() + "*");

                if (Integer.parseInt(right) > 0) {

                    setStudyIds(new Integer(rs.getInt("PK_STUDY")));
                    setStudyNumbers(rs.getString("STUDY_NUMBER"));
                    rows++;
                }
            }

            setCRows(rows);

            retStr = toPullDown(dName, selVal);

        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getUserStudiesWithRights EXCEPTION IN FETCHING FROM Study table"
                            + ex);
            retStr = null;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            setStudyDropDown(retStr);

        }

    }

    /**
     * Gets studies which has been given rights for an organization through user
     * deatils as well as from study team access rights.
     * 
     * @param user
     *            Id
     * @param dName
     *            for dropdown
     * @param selVal
     *            for dropdown
     * @param site
     *            id
     * @returns count as 0- if study does not have access to org else 1
     */
  /* modified by sonia abrol 11/9/07 to change the super user implementation*/
    
    public int getOrgAccessForStudy(int userId, int studyId, int orgId) {
        int count = 0;
        String sql = " select pkg_user.f_chk_right_for_studysite(?,?,?) from dual";      	
          
         
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1,studyId);
            pstmt.setInt(2,userId);
            pstmt.setInt(3,orgId);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt(1);
            
        } catch (SQLException ex) {

            Rlog.fatal("study",
                    "StudyDao.getOrgAccessForStudy EXCEPTION IN FETCHING FROM Study table"
                            + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return count;
    }

    /**
     * This method gets access right for complete patient data depending on
     * study and user's group <br>
     * parameter
     * 
     * @param userId
     * @param groupId
     * @param StudyId
     */
    public int getStudyCompleteDetailsAccessRight(int userId, int groupId,
            int studyId) {

        int retRight = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_STUDYSTAT.SP_GET_STUDY_RIGHT(?,?,?,?)}");
            cstmt.setInt(1, userId);
            cstmt.setInt(2, groupId);
            cstmt.setInt(3, studyId);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);

            cstmt.execute();
            retRight = cstmt.getInt(4);
            return retRight;

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "study",
                            "EXCEPTION in getStudyCompleteDetailsAccessRight, excecuting Stored Procedure PKG_STUDYSTAT.SP_GET_STUDY_RIGHT"
                                    + e);
            return 0;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    // JM: 25/08/05 get study number from study drop down value, which is
    // selected
    /**
     * @param studyId
     *            Description of the Parameter
     * @return The study number
     */

    public String getStudy(int studyId) {

        int rows = 0;
        String studyno = null;

        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            setStudyIds(studyId);
            conn = getConnection();
            String sql = "select fk_account, study_number,STUDY_TITLE, STUDY_PRINV "
            		+ "from er_study "
                    + "where pk_study = " + studyId;
            pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                studyno = rs.getString("STUDY_NUMBER");
                setStudyNumbers(studyno);
                setStudyAccountIds(rs.getString("fk_account"));
                setStudyTitles(rs.getString("STUDY_TITLE"));
                setStudyPrimInvs(rs.getString("STUDY_PRINV"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("codelst", "EXCEPTION IN FETCHING FROM CODE LST TABLE "
                    + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

        return studyno;
    }

    /**
     * Gets Study PK
     * 
     * @param study
     *            Number The Study Number
     * @param Primary
     *            Key of study account
     * @return Study Primary Key
     */

    public static int getStudyPK(String studyNum, int pkAccount) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int studyPk = 0;
        try {
            conn = EnvUtil.getConnection();

            pstmt = conn
                    .prepareStatement("select pk_study from er_study where lower(trim(STUDY_NUMBER)) = lower(trim(?)) and fk_account = ? ");
            pstmt.setString(1, studyNum);
            pstmt.setInt(2, pkAccount);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                studyPk = rs.getInt(1);
                Rlog.debug("study",
                        "StudyDao.getStudyPK(String studyNum, int pkAccount ) studyPk"
                                + studyPk);
            }
            return studyPk;
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getStudyPK(String studyNum, int pkAccount ) EXCEPTION "
                            + ex);
            return studyPk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ////////

    // ///////
    /**
     * Gets study Protocol PK
     * 
     * @param studyPK
     *            The Study Primary Key
     * @param CalendarName
     *            The Calendar Name
     * @return Protocol Primary Key
     */

    public int getProtocolPK(int studyPK, String CalendarName) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int protocolPk = 0;
        try {
            conn = getSchConnection();

            pstmt = conn
                    .prepareStatement("select event_id from event_assoc where chain_id = ? and lower(trim(name)) = lower(trim(?)) ");
            pstmt.setInt(1, studyPK);
            pstmt.setString(2, CalendarName);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                protocolPk = rs.getInt(1);
                Rlog.debug("study",
                        "StudyDao.getProtocolPK(int studyPK, String CalendarName ) studyPk"
                                + protocolPk);
            }
            return protocolPk;
        } catch (SQLException ex) {
            Rlog.fatal("study",
                    "StudyDao.getProtocolPK(int studyPK, String CalendarName ) EXCEPTION "
                            + ex);
            return protocolPk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }   
    
    //AK:BUG#6714 Added method for Study Autocomplete filter
    
    public void getStudyAutoRows(String userID ,String actId) {
    	PreparedStatement stmt = null;
    	ResultSet rs;
    	String sql="";
    	Connection conn = null;
    	try {
    		conn = getConnection();
    		sql="select pk_study ,study_number from erv_study_dyn WHERE erv_study_dyn.fk_account= ? and(pk_study IN"+
    		"(select fk_study FROM er_studyteam WHERE fk_user =? and study_team_usr_type <> 'X')"+
    		"OR pkg_superuser.F_Is_Superuser(?, pk_study) = 1 )";
    		stmt = conn.prepareStatement(sql);
    		stmt.setString(1, actId);
    		stmt.setString(2, userID);   
    		stmt.setString(3, userID);   
    		rs = stmt.executeQuery();    
    		while(rs.next())
    		{				  	
    			setStudyAutoIds(rs.getString(1));
    			setStudyAutoNames(rs.getString(2));	 				  
    		}
    	}
    	catch (SQLException ex) {
    		Rlog.fatal("common",
    				"Study:Exception in calling getStudyAutoRows:forAutoComplete Search "
    				+ ex);
    	} finally {
    		try {
    			if (stmt != null)
    				stmt.close();
    		} catch (Exception e) {
    		}
    		try {
    			if (conn != null)
    				conn.close();
    		} catch (Exception e) {
    		}

    	}

    }
   


   

}  
