/*
 * Classname			   StorageAllowedItemsBean.class
 * 
 * Version information
 *
 * Date					09/05/2007 
 * 
 * Copyright notice
 */

package com.velos.eres.business.storageAllowedItems.impl;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.business.common.StorageAllowedItemsDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The StorageAllowedItems BMP entity bean.<br>
 * <br>
 * @author Khader
 * @version 1.0 09/06/2007 
 */
@Entity
@Table(name = "er_allowed_items")

public class StorageAllowedItemsBean implements Serializable {
   
    private static final long serialVersionUID = 3761410811205333553L;

    /**
     * pkAllowedItems
     */
    public int pkAllowedItems;

    /**
     * fkStorage
     */
    public Integer fkStorage;
    

    /**
     * fkCodelstStorageType
     */
    public Integer fkCodelstStorageType;

    /**
     * fkCodelstSpecimenType
    */
    public Integer fkCodelstSpecimenType;
    
    /**
     * allowedItemType
    */
    public Integer allowedItemType;
    
    
    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */	
    public String ipAdd;

        
    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_er_allowed_items", allocationSize=1)
    @Column(name = "PK_AI")
    public int getPkAllowedItem() {
        return this.pkAllowedItems;
    }

    public void setPkAllowedItem(int pkAllowedItems) {
        this.pkAllowedItems= pkAllowedItems;
    }

    @Column(name = "FK_STORAGE")
    public String getFkStorage() {
        return StringUtil.integerToString(this.fkStorage);
    }
    public void setFkStorage(String fkStorage) {
        this.fkStorage = StringUtil.stringToInteger(fkStorage);
    }

    @Column(name = "FK_CODELST_STORAGE_TYPE")
    public String getFkCodelstStorageType() {
    	
       return  StringUtil.integerToString(fkCodelstStorageType);
    } 
     
    public void setFkCodelstStorageType(String fkCodelstStorageType) {
    	this.fkCodelstStorageType = StringUtil.stringToInteger(fkCodelstStorageType);
    }

    @Column(name = "FK_CODELST_SPECIMEN_TYPE")
    public String getFkCodelstSpecimenType() {
       return  StringUtil.integerToString(fkCodelstSpecimenType);
    } 
    public void setFkCodelstSpecimenType(String fkCodelstSpecimenType) {
        this.fkCodelstSpecimenType = StringUtil.stringToInteger(fkCodelstSpecimenType);
    }
    
    @Column(name = "AI_ITEM_TYPE")
    public String getAllowedItemType() {
       return StringUtil.integerToString(allowedItemType);
    } 
     
    public void setAllowedItemType(String allowedItemType) {
        this.allowedItemType = StringUtil.stringToInteger(allowedItemType);
    }

   @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
       }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    // END OF GETTER SETTER METHODS
    public StorageAllowedItemsBean() {

    }

    public StorageAllowedItemsBean(int pkAllowedItem, String fkStorage, String fkCodelstStorageType, 
    		String fkCodelstSpecimenType,String allowedItemType, String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setPkAllowedItem(pkAllowedItem);
		setFkStorage(fkStorage);
		setFkCodelstStorageType(fkCodelstStorageType);
		setFkCodelstSpecimenType(fkCodelstSpecimenType);
		setAllowedItemType(allowedItemType);
        setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
    }
    public int updateStorageAllowedItems(StorageAllowedItemsBean ssk) {
        try {
           	setPkAllowedItem(ssk.getPkAllowedItem());
        	setFkStorage(ssk.getFkStorage());
        	setFkCodelstStorageType(ssk.getFkCodelstStorageType());
        	setFkCodelstSpecimenType(ssk.getFkCodelstSpecimenType());
        	setAllowedItemType(ssk.getAllowedItemType());
        	setCreator(ssk.getCreator());
        	setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            Rlog.debug("storageAllowedItem", "StorageAllowedItemsBean.updateStorageAllowedItems");
            return 0;
        } catch (Exception e) {
        	
            Rlog.fatal("storageAllowedItem", " error in StorageAllowedItemsBean.updateStorageAllowedItems()");
            return -2;
        }
    }
   

}


