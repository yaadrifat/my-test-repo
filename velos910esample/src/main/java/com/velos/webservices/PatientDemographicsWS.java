package com.velos.webservices;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.PatientDemographicsClient;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientFacilities;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientIdentifiers;
import com.velos.services.model.PatientOrganization;
import com.velos.services.model.PatientOrganizationIdentifier;
import com.velos.services.model.PatientSearch;
import com.velos.services.model.PatientSearchResponse;
import com.velos.services.model.PatientSurvivalStatuses;
import com.velos.services.model.Patients;
import com.velos.services.model.UpdatePatientDemographics;

/**
 * WebServices class dealing with all PatientDemographics Services operations.
 * @author virendra
 *
 */
@WebService(
		serviceName="PatientDemographicsService",
		endpointInterface="com.velos.webservices.PatientDemographicsSEI",
		targetNamespace="http://velos.com/services/"
		)
public class PatientDemographicsWS implements PatientDemographicsSEI{
	
	private static Logger logger = Logger.getLogger(PatientDemographicsWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public PatientDemographicsWS(){
		
	}
	/***getPatientDemographics method in WebServicesClass
	 * calls Client class with PatientIdentifier object and
	 * returns PatientDemographics object
	 * @return
	 */
	public PatientDemographics getPatientDemographics(PatientIdentifier patientId)
			throws OperationException {
		PatientDemographics patientDemographics = PatientDemographicsClient.getPatientDemographics(patientId);
		return patientDemographics;
	}

	
	public ResponseHolder createPatient(Patient patient)
	throws OperationException, OperationRolledBackException {

		ResponseHolder response = new ResponseHolder();
		try {
			response = PatientDemographicsClient.createPatient(patient);

		} catch (OperationException e) {
			logger.error("create Patient", e);
			throw e;
		} catch (Throwable t) {
			logger.error("create Patient", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}		return response;
	}
	
	public ResponseHolder updatePatient(
			PatientIdentifier paramPatientIdentifier,
			UpdatePatientDemographics paramPatientUpdateDemographics)
			throws OperationException, OperationRolledBackException {
		// TODO Auto-generated method stub
		return PatientDemographicsClient.updatePatient(paramPatientIdentifier, paramPatientUpdateDemographics);
	}

	public ResponseHolder addPatientFacility(PatientIdentifier patId,
			PatientOrganization patOrg) throws OperationException,
			OperationRolledBackException {	
		return PatientDemographicsClient.addPatientFacility(patId, patOrg);
	}
	
	public ResponseHolder updatePatientFacility(PatientIdentifier patIdentifier,
			OrganizationIdentifier orgID,
			PatientOrganizationIdentifier pat, PatientOrganization patOrgIdentifier)
			throws OperationException, OperationRolledBackException{
		return PatientDemographicsClient.updatePatientFacility(patIdentifier, orgID, pat, patOrgIdentifier);
	}

	public PatientSearchResponse searchPatient(
			PatientSearch paramPatientIdentifier) throws OperationException {		
		return PatientDemographicsClient.searchPatient(paramPatientIdentifier);
	}
	@Override
	public PatientSurvivalStatuses getMPatientSurvivalStatus(
			PatientIdentifiers patientIdentifiers) throws OperationException {
		return PatientDemographicsClient.getMPatientSurvivalStatus(patientIdentifiers);
	}
	
	@Override
	public PatientFacilities getPatientFacilities(PatientIdentifier patId)
			throws OperationException, OperationRolledBackException {
			return PatientDemographicsClient.getPatientFacilities(patId);
	}

	@Override
	public ResponseHolder UpdateMPatientDemographics(
			Patients patients)
			throws OperationException {
		return PatientDemographicsClient.UpdateMPatientDemographics(patients);
	}

}