package com.velos.webservices;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.PatientScheduleClient;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Visit;
import com.velos.services.model.VisitIdentifier;
/**
 * WebService class for PatientSchedule
 * @author Virendra
 *
 */
@WebService(
		serviceName="PatientScheduleService",
		endpointInterface="com.velos.webservices.PatientScheduleSEI",
		targetNamespace="http://velos.com/services/")
		
public class PatientScheduleWS implements PatientScheduleSEI{
	
	private static Logger logger = Logger.getLogger(PatientScheduleWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public PatientScheduleWS(){
	}
	/**
	 * Calls getPatientSchedule of Patient Schedule Client with PatientIdentifier
	 * and StudyIdentifier objects and returns List of PatientSchedule for a patient in a study
	 */

	
	public PatientSchedules getPatientScheduleList(PatientIdentifier patientId, StudyIdentifier studyIdentifier)
			throws OperationException {
		PatientSchedules lstPatSchedule = PatientScheduleClient.getPatientScheduleList(patientId, studyIdentifier);
		return lstPatSchedule;
	}
	
	public PatientSchedule getPatientSchedule(PatientProtocolIdentifier scheduleOID)
			throws OperationException {
		PatientSchedule patientSchedule = PatientScheduleClient.getPatientSchedule(scheduleOID);
		return patientSchedule;
	}
	
	public PatientSchedule getCurrentPatientSchedule(PatientIdentifier patientId, StudyIdentifier studyIdentifier,Date startDate,Date endDate)
			throws OperationException {
		PatientSchedule currentPatientSchedule = PatientScheduleClient.getCurrentPatientSchedule(patientId, studyIdentifier, startDate, endDate);
		return currentPatientSchedule;
	}
	
	public ResponseHolder addScheduleEventStatus(EventIdentifier eventIdentifier, EventStatus eventStatus)
			throws OperationException {
		ResponseHolder response = PatientScheduleClient.addScheduleEventStatus(eventIdentifier, eventStatus);
		return response;
	}
	
	public SitesOfService getSitesOfService()
			throws OperationException {
		SitesOfService sitesOfService = PatientScheduleClient.getSitesOfService();
		return sitesOfService;
	}
	
	
}