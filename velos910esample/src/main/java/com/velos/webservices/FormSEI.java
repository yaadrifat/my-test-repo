/**
 * Created On Sep 1, 2011
 */
package com.velos.webservices;
//
//import java.util.List;

import javax.jws.WebService;
import javax.jws.WebResult;
import javax.jws.WebParam;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.velos.services.OperationException;
import com.velos.services.model.DateFieldValidations;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.LinkedFormDesign;
import com.velos.services.model.PatientForms;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyFormDesign;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.StudyPatientForms;
import com.velos.services.model.StudyPatientScheduleFormDesign;
//import com.velos.services.model.FormInfo;
//import com.velos.services.model.FormList;
import com.velos.services.model.NumberFieldValidations;
//import com.velos.services.model.StudyFormDesign;
//import com.velos.services.model.StudyIdentifier;
//import com.velos.services.model.StudyPatientFormDesign;
import com.velos.services.model.TextFieldValidations;

/**
 * @author Kanwaldeep
 *
 */

@WebService(
		 targetNamespace="http://velos.com/services/")
@XmlSeeAlso({TextFieldValidations.class,
	DateFieldValidations.class,
	NumberFieldValidations.class
	})
public interface FormSEI {
	
	@WebResult(name="FormDesign")
	public FormDesign getLibraryFormDesign(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier,
			@WebParam(name="IncludeFormatting")
			boolean includeFormatting, 
			@WebParam(name="FormName")
			String formName)
	throws OperationException; 
	
	@WebResult(name="FormDesign")
	public StudyFormDesign getStudyFormDesign(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier,
			@WebParam(name="FormName")
			String formName,
			@WebParam(name="IncludeFormatting")
			boolean includeFormatting
			)
	throws OperationException; 
	
//	@WebResult(name="FormDesign")
//	public FormDesign getAccountFormDesign(
//			@WebParam(name="FormIdentifier")
//			FormIdentifier formIdentifier, 
//			@WebParam(name="IncludeFormatting")
//			boolean includeFormatting,
//			@WebParam(name="FormName")
//			String formName)
//	throws OperationException; 
//	
//	
//	
/*	@WebResult(name="FormDesign")
	public FormDesign getPatientFormDesign(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier, 
			@WebParam(name="FormName")
			String formName,
			@WebParam(name="IncludeFormatting")
			boolean includeFormatting) throws OperationException;*/
	
	@WebResult(name="FormDesign")
	public LinkedFormDesign getPatientFormDesign(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier, 
			@WebParam(name="FormName")
			String formName,
			@WebParam(name="IncludeFormatting")
			boolean includeFormatting) throws OperationException;	
	
	@WebResult(name="FormDesign")
	public StudyPatientFormDesign getStudyPatientFormDesign(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier,
			@WebParam(name="FormName")
			String formName,
			@WebParam(name="IncludeFormatting")
			boolean includeFormatting) throws OperationException;  
	
	@WebResult(name="FormDesign")
	public StudyPatientScheduleFormDesign getStudyPatientScheduleFormDesign(
			@WebParam(name="FormIdentifier")
			FormIdentifier formIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier,
			@WebParam(name="FormName")
			String formName,
			@WebParam(name="IncludeFormatting")
			boolean includeFormatting) throws OperationException; 
	
	@WebResult(name="FormDesign")
	public StudyPatientForms getListOfStudyPatientForms(
			@WebParam(name="PatientIdentifier")
			PatientIdentifier patientIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier,
			@WebParam(name="maxNumberOfResults")
			int maxNumberOfResults,
			@WebParam(name="formHasResponses")
			boolean formHasResponses) throws OperationException; 
	
	@WebResult(name="FormDesign")
	public PatientForms getListOfPatientForms(
			@WebParam(name="PatientIdentifier")
			PatientIdentifier patientIdentifier,
			@WebParam(name="formHasResponses")
			boolean formHasResponses,
			@WebParam(name="pageNumber")
			int pageNumber,
			@WebParam(name="pageSize")
			int pageSize) throws OperationException;
	
//	
//	@WebResult(name="FormList")
//	public FormList getAllFormsForStudy(
//			@WebParam(name="StudyIdentifier")
//			StudyIdentifier studyIdentifier) throws OperationException; 

}
