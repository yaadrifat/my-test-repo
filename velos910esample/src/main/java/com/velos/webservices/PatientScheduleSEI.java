package com.velos.webservices;

import java.util.ArrayList;
import java.util.Date;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientProtocolIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedules;
import com.velos.services.model.SitesOfService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Visit;
import com.velos.services.model.VisitIdentifier;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of Patient Schedule Web Services
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
	/**
	 * 
	 */
	public interface PatientScheduleSEI {
		/**
		 * public method of Patient Schedule Service Endpoint Interface
		 * which calls getPatientSchedule method of PatientSchedule webservice
		 * and returns Patient Schedule
		 * @param patientId
		 * @param studyId
		 * @return PatSchedule
		 * @throws OperationException
		 */
	@WebResult(name="PatientSchedules")
	    public PatientSchedules getPatientScheduleList(
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId,
		@WebParam(name = "StudyIdentifier")		
		StudyIdentifier studyId
		)
		throws OperationException;
	@WebResult(name="PatientSchedule")
	    public PatientSchedule getPatientSchedule(
		@WebParam(name = "ScheduleIdentifier")		
		PatientProtocolIdentifier scheduleOID
		)
		throws OperationException;
	@WebResult(name="CurrentPatientSchedule")
        public PatientSchedule getCurrentPatientSchedule(
	    @WebParam(name = "PatientIdentifier")		
	    PatientIdentifier patientId,
	    @WebParam(name= "StudyIdentifier")
	    StudyIdentifier studyId,
	    @WebParam(name = "startDate")		
	    Date startDate,
	    @WebParam(name = "endDate")		
	    Date endDate
	    )
	    throws OperationException;
	@WebResult(name="addScheduleEventStatus")
        public ResponseHolder addScheduleEventStatus(
        @WebParam(name= "EventIdentifier")
        EventIdentifier eventIdentifier,
        @WebParam(name= "EventStatus")
        EventStatus eventStatus
        )
        throws OperationException;
	@WebResult(name="getSitesOfService")
    public SitesOfService getSitesOfService(

    )
    throws OperationException;

}