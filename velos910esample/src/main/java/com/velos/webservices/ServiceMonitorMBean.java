/**
 * 
 */
package com.velos.webservices;

import java.io.IOException;
import java.util.Date;
import javax.management.openmbean.TabularDataSupport
;
/**
 * @author dylan
 *
 */
public interface ServiceMonitorMBean {
	public enum ServiceState{
		ACTIVE,
		INACTIVE
	}
	/**
	 * Returns the number of messages processed without error
	 * in the date range defined in fromDate and toDate
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public TabularDataSupport  currentSuccessfulMessages(String fromDate, String toDate)  throws IOException;
	
	/**
	 * Returns the number of messages processed with error
	 * in the date range defined in fromDate and toDate
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public Integer getErroredMessageCount(String fromDate, String toDate)  throws IOException;
	
	public String getServiceType() throws IOException;
	
	public String getVersion() throws IOException;
	
	public ServiceState getServiceState() throws IOException;
	
	
}
