/**
 * @author Tarandeep Singh Bali
 */
package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.velos.services.OperationException;
import com.velos.services.model.DateFieldValidations;
import com.velos.services.model.EventSearch;
import com.velos.services.model.Events;
import com.velos.services.model.FormDesign;
import com.velos.services.model.FormIdentifier;
import com.velos.services.model.LibraryEvents;
import com.velos.services.model.NumberFieldValidations;
import com.velos.services.model.TextFieldValidations;


@WebService(
		 targetNamespace="http://velos.com/services/")
@XmlSeeAlso({TextFieldValidations.class,
	DateFieldValidations.class,
	NumberFieldValidations.class
	})
public interface LibrarySEI {
	
	@WebResult(name="Library")
	public LibraryEvents searchEvent(
			@WebParam(name="EventSearch")
			EventSearch eventSearch)
	throws OperationException; 

}
