package com.velos.webservices;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.StudyPatientClient;
import com.velos.services.model.Code;
import com.velos.services.model.Patient;
import com.velos.services.model.PatientEnrollmentDetails;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientStudyStatusIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.StudyPatientStatuses;
/**
 * Webservice class for StudyPatient.
 * @author Virendra
 *
 */
@WebService(
		serviceName="StudyPatientService",
		endpointInterface="com.velos.webservices.StudyPatientSEI",
		targetNamespace="http://velos.com/services/")
		
public class StudyPatientWS implements StudyPatientSEI{
	
	private static Logger logger = Logger.getLogger(StudyPatientWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public StudyPatientWS(){
		
	}
	/***
	 * Calls getStudyPatient methiod of StudyPatient client with StudyIdentifier
	 * and returns List of SudyPatient object.
	 */
	public List<StudyPatient> getStudyPatients(StudyIdentifier StudyId)
			throws OperationException {
		List<StudyPatient> studyPatients = StudyPatientClient.getStudyPatients(StudyId);
		return studyPatients;
	}
	/* (non-Javadoc)
	 * @see com.velos.webservices.StudyPatientSEI#enrollPatientToStudy(com.velos.services.model.PatientEnrollmentDetails)
	 */
	public ResponseHolder enrollPatientToStudy(PatientIdentifier patientIdentifier, 
			StudyIdentifier studyIdentifier,
			PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException, OperationRolledBackException {
		return StudyPatientClient.enrollPatientToStudy(patientIdentifier, studyIdentifier,patientEnrollmentDetails);
	}
	
	public ResponseHolder createAndEnrollPatient(Patient patient,
			StudyIdentifier studyIdentifier,
			PatientEnrollmentDetails patientEnrollmentDetails)
	throws OperationException, OperationRolledBackException{
		return StudyPatientClient.createAndEnrollPatient(patient, studyIdentifier, patientEnrollmentDetails); 
	}
	public ResponseHolder deleteStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier,String reasonForDelete)
			throws OperationException {
		return StudyPatientClient.deleteStudyPatientStatus(patientStudyStatusIdentifier,reasonForDelete);
	}
	@Override
	public ResponseHolder updateStudyPatientStatus(PatientStudyStatusIdentifier patientStudyStatusIdentifier,PatientEnrollmentDetails patientEnrollmentDetails)
			throws OperationException {
		return StudyPatientClient.updateStudyPatientStatus(patientStudyStatusIdentifier,patientEnrollmentDetails);
	}
	/* (non-Javadoc)
	 * @see com.velos.webservices.StudyPatientSEI#addStudyPatientStatus(com.velos.services.model.PatientEnrollmentDetails, com.velos.services.model.PatientIdentifier, com.velos.services.model.StudyIdentifier)
	 */
	@Override
	public ResponseHolder addStudyPatientStatus(
			PatientEnrollmentDetails patientStudyStatus,
			PatientIdentifier patientIdentifier, StudyIdentifier studyIdentifier)
			throws OperationException {
		
		return StudyPatientClient.addStudyPatientStatus(studyIdentifier, patientIdentifier, patientStudyStatus);
	}
	/* (non-Javadoc)
	 * @see com.velos.webservices.StudyPatientSEI#getStudyPatientStatusHistory(com.velos.services.model.StudyIdentifier, com.velos.services.model.PatientIdentifier)
	 */
	@Override
	public StudyPatientStatuses getStudyPatientStatusHistory(
			StudyIdentifier studyIdentifier, PatientIdentifier patientIdentifier)
			throws OperationException {
		return StudyPatientClient.getStudyPatientStatusHistory(studyIdentifier, patientIdentifier);
	}
	@Override
	public PatientEnrollmentDetails getStudyPatientStatus(
			PatientStudyStatusIdentifier patientStudyStatusIdentifier)
			throws OperationException {
		return StudyPatientClient.getStudyPatientStatus(patientStudyStatusIdentifier);
	}

}