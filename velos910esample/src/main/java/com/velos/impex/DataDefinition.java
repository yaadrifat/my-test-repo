/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Mon Feb 02
 * 11:53:21 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2063)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.io.Serializable;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

// /__astcwz_class_idt#(2039!n)
public class DataDefinition implements Serializable {

    private String tableName;

    private ArrayList tableColumns;

    private int columnCount;

    private String tableLocation;

    // /__astcwz_opern_idt#(2052)
    public String getCreateDDL() {
        /** Returns DDL for this DataDefinition */

        StringBuffer sbDDL = new StringBuffer();

        sbDDL.append("CREATE TABLE " + tableName + " ( ");

        // get DDL for each column

        int colCount = tableColumns.size();

        for (int ctr = 0; ctr < colCount; ctr++) {
            DataColumn dc = (DataColumn) tableColumns.get(ctr);
            sbDDL.append(dc.getColumnDDL());

            if (colCount != (ctr + 1)) {
                sbDDL.append(",\n");
            }

        }

        sbDDL.append(")");

        return sbDDL.toString();
    }

    public void initDataDefinition() {
        tableColumns = new ArrayList();

    }

    // /__astcwz_opern_idt#(2054|2055)
    public DataDefinition(String pTableName) {
        initDataDefinition();
        setTableName(pTableName);

    }

    // /__astcwz_opern_idt#(2057|2058,2059)
    public DataDefinition(String pTableName, ArrayList pTableColumns) {
        initDataDefinition();

        setTableName(pTableName);
        setTableColumns(pTableColumns);

    }

    public String getTableLocation() {
        return this.tableLocation;
    }

    public void setTableLocation(String tableLocation) {
        this.tableLocation = tableLocation;
    }

    // /__astcwz_opern_idt#(2063)
    public static DataDefinition getDataDefinition(ResultSetMetaData rsMD) {
        DataDefinition ddObj = new DataDefinition();
        int nColCount = 0;
        String colName = "";
        String colType = "";
        String rsTableName = "";
        int colSize = 0;
        int colPrecision = 0;
        int colScale = 0;

        try {

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> DataDefinition.getDataDefinition...");

            if (rsMD != null) {
                nColCount = rsMD.getColumnCount();
                ddObj.setColumnCount(nColCount);

                if (nColCount > 0) {

                    rsTableName = rsMD.getTableName(1);

                    // by default set the table name as table name of the first
                    // column

                    ddObj.setTableName(rsTableName);

                    DBLogger.log2DB("debug", "MSG",
                            "IMPEX --> DataDefinition.getDataDefinition..nColCount"
                                    + nColCount);

                    for (int j = 0; j < nColCount; j++) {
                        DataColumn dColumn = new DataColumn();

                        colSize = 0;
                        colPrecision = 0;
                        colScale = 0;

                        colName = rsMD.getColumnName(j + 1);
                        colType = rsMD.getColumnTypeName(j + 1);

                        if ((!colType.equalsIgnoreCase("blob"))
                                && (!colType.equalsIgnoreCase("clob"))) {
                            colSize = rsMD.getColumnDisplaySize(j + 1);
                            colPrecision = rsMD.getPrecision(j + 1);
                            colScale = rsMD.getScale(j + 1);
                        }

                        // DBLogger.log2DB ("debug", "MSG", "IMPEX -->
                        // DataDefinition.getDataDefinition..colvals , colSize"
                        // + colSize + ",colPrecision " + colPrecision +
                        // ",colscale" + colScale);

                        dColumn.setColumnName(colName);
                        dColumn.setColumnType(colType);
                        dColumn.setColumnDisplaySize(colSize);
                        dColumn.setColumnPrecision(colPrecision);
                        dColumn.setColumnScale(colScale);

                        ddObj.setTableColumns(dColumn);
                    }

                }

            }
            DBLogger
                    .log2DB("debug", "MSG",
                            "IMPEX --> DataDefinition.getDataDefinition..Object created");
            return ddObj;
        } catch (Exception e) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in DataDefinition.getDataDefinition.."
                            + e.toString());
            System.out.println("Exception in getDataDefinition " + e);
            return ddObj;
        }

    }

    // /__astcwz_default_constructor
    public DataDefinition() {
        initDataDefinition();
    }

    // /__astcwz_opern_idt#()
    public String getTableName() {
        return tableName;
    }

    // /__astcwz_opern_idt#()
    public ArrayList getTableColumns() {
        return tableColumns;
    }

    // /__astcwz_opern_idt#()
    public int getColumnCount() {
        return columnCount;
    }

    // /__astcwz_opern_idt#()
    public void setTableName(String atableName) {
        tableName = atableName;
    }

    // /__astcwz_opern_idt#()
    public void setTableColumns(ArrayList acolumns) {
        tableColumns = acolumns;
    }

    public void setTableColumns(DataColumn aColumn) {
        tableColumns.add(aColumn);
    }

    public DataColumn getTableColumns(int indx) {
        return (DataColumn) tableColumns.get(indx);
    }

    // /__astcwz_opern_idt#()
    public void setColumnCount(int acolumnCount) {
        columnCount = acolumnCount;
    }

    public String getDynamicInsertSQL() {
        /**
         * Returns a dynamic Insert SQL for this DataDefinition This will be
         * used in a PrepardStatement
         */

        StringBuffer sbIns = new StringBuffer();
        StringBuffer placeHolder = new StringBuffer();

        sbIns.append("INSERT INTO " + tableName + " ( ");

        // get DDL for each column

        int colCount = tableColumns.size();

        for (int ctr = 0; ctr < colCount; ctr++) {
            DataColumn dc = (DataColumn) tableColumns.get(ctr);
            sbIns.append(dc.getColumnName());
            placeHolder.append("?");

            if (colCount != (ctr + 1)) {
                sbIns.append(", ");
                placeHolder.append(", ");
            }

        }

        sbIns.append(") VALUES (" + placeHolder.toString() + " )");

        DBLogger.log2DB("debug", "MSG",
                "IMPEX --> DataDefinition.getDynamicInsertSQL() : "
                        + sbIns.toString());

        return sbIns.toString();
    }

    public ArrayList getDataTypes() {
        /** Returns an ArrayList of datatypes of all Column objects */

        ArrayList arColTypes = new ArrayList();

        // get datatype for each column

        int colCount = tableColumns.size();

        for (int ctr = 0; ctr < colCount; ctr++) {
            DataColumn dc = (DataColumn) tableColumns.get(ctr);
            arColTypes.add(dc.getColumnType());
        }

        DBLogger.log2DB("debug", "MSG",
                "IMPEX --> DataDefinition.getDataTypes() : Got columns "
                        + arColTypes.size());

        return arColTypes;
    }

}
