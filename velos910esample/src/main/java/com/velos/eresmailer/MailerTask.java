package com.velos.eresmailer;


import java.util.Timer;
import java.util.TimerTask;


import org.apache.log4j.Logger;



//APR-04-2011,MAILER TASK INTRODUCED FOR ALL NOTIFICATION MAILS,EARLIER THIS TASK WAS ACCOMPLISHED
//BY AWT TIMER.
/**
 * This class provides the functionality to send mails based on information provided in message template.
 * 
 * @author Bikash
 *
 */

public class MailerTask extends TimerTask {
	
	private static  Logger logger = Logger.getLogger(MailerTask.class);
	private static Timer timer = new Timer(); 
	 vcDispatcher dispatcher = new vcDispatcher();
	 
	 /**
	  * runs continuously after the specified time interval. 
	  */
	@Override
	public void run() {	
		logger.info("Entered run method of Mailer, runs after every 60 seconds");		
		dispatcher.InitDB();        
        dispatcher.FetchActions();        
        dispatcher.CloseDB();
        
	}	
	/**
	 * Set up the time interval for service and start the timer.
	 */
public static void startMailer() {
		
		try{
			timer = new Timer();
			
			
				MailerTask aMailerTask = new MailerTask();				
				long scheduleInterval = 60000;				
				logger.info("Scheduling timer is started with the interval of "+ scheduleInterval+" milisecond");
				try{					
					timer.schedule(aMailerTask,60000, scheduleInterval);
				}catch(Exception e)
				{
					logger.info(e); 
					e.printStackTrace(); 
				}
			
		
		}catch(Exception e)
		{
			logger.fatal("Unable to send mail. ", e); 
		}

		
	}

/**
 * Stops the timer.
 */
	public static void stopMailer()
	{
		timer.cancel(); 
	}
	
	
}
