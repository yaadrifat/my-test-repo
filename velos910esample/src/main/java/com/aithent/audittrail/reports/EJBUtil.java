package com.aithent.audittrail.reports;

/* Import all the exceptions can be thrown */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.velos.esch.service.util.Rlog;

public final class EJBUtil {

    static int cReturn;
    
    static float cFloatReturn;

    public static String integerToString(Integer i) {

        String returnString = null;
        try {
            Rlog.debug("common", "{Integer VALUE:}" + i);
            if (i == null) {
                Rlog.debug("common", "{Integer is null}");
                return returnString;
            }
            returnString = i.toString();
            Rlog.debug("common", "{String VALUE:}" + returnString);
            return returnString;
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
            return returnString;
        }
    }

    public static int stringToNum(String str) {

        int iReturn = 0;
        try {
            Rlog.debug("common", "{STRING VALUE:}" + str);
            if (str == null) {
                return cReturn;
            } else if (str == "") {
                return cReturn;
            } else {
                iReturn = Integer.parseInt(str);
                Rlog.debug("common", "{INT VALUE:}" + iReturn);
                return iReturn;
            }
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO NUMBER:}" + e);
            return iReturn;
        }
    }

    public static float stringToFloat(String str) {
        float iReturn = 0;
        try {
            Rlog.debug("common", "{STRING VALUE:}" + str);
            if (str == null) {
                return cFloatReturn;
            } else if (str == "") {
                return cFloatReturn;
            } else {
                iReturn = Float.parseFloat(str);
                Rlog.debug("common", "{Float VALUE:}" + iReturn);
                return iReturn;
            }
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO FLOAT:}" + e);
            return iReturn;
        }
    }

    /*
     * if the appServerParam are set in configuration class then return it,
     * otherwise read it from xml file and store in appServerParam then return.
     */
    /*
     * public static Hashtable getContextProp(){
     * 
     * try{ Rlog.debug("common","Conf param = "+Configuration.getServerParam());
     * if(Configuration.getServerParam() == null) Configuration.readSettings();
     * Rlog.debug("common","Conf param = "+Configuration.getServerParam());
     * }catch(Exception e){ Rlog.fatal("common","EJBUtil:getContextProp:general
     * ex"+e); } return Configuration.getServerParam(); }
     */

    public static String getActualPath(String dir, String fileName) {

        String path;
        dir = dir.trim();
        fileName = fileName.trim();
        File f = new File(dir, fileName);

        try {
            path = f.getAbsolutePath();
        } catch (Exception e) {
            path = null;
        }
        return path;
    }

    /** *********GET CONNECTION FROM RESOURCE********* */

    public static Connection getConnection() {

        // InitialContext iCtx=null;
        Connection conn = null;
        Configuration conf = null;
        Rlog.debug("common", "GetConnection Function Call");

        try {
            if (conf.DBUrl == null) {
                conf.readSettings();
            }
            Class.forName(Configuration.DBDriverName);
            conn = DriverManager.getConnection(conf.DBUrl, conf.DBUser,
                    conf.DBPwd);

            /*
             * iCtx=new InitialContext(EJBUtil.getContextProp()); if
             * (conf.dsJndiName == null){ conf.readSettings(); }
             * Rlog.debug("common","JNDI NAME DS NAME" + conf.dsJndiName);
             * DataSource ds=(DataSource)iCtx.lookup(conf.dsJndiName);
             */
        } catch (Exception se) {
            Rlog.fatal("common", ":getConnection():Sql exception:" + se);
        }
        return conn;
    }

    /** **********READ ENV VARIABLES************ */

    public static String getEnvVariable(String Name) throws Exception {

        String iline = "";
        String envValue = "";
        int i = 0;
        int sType = 0;
        System.out.println("***************EJBUtil.getEnvVariable line 1");
        Process theProcess = null;
        BufferedReader stdInput = null;
        System.out.println("***************EJBUtil.getEnvVariable line 2");
        try {

            // Figure out what kind of system we are running on
            // 0 = windows
            // 1 = unix

            sType = getSystemType();
            System.out.println("***************EJBUtil.getEnvVariable line 3");
            if (sType == 0) {
                String envPath = "cmd /C echo %" + Name + "%";
                System.out
                        .println("***************EJBUtil.getEnvVariable line 4 envPath "
                                + envPath);
                theProcess = Runtime.getRuntime().exec(envPath);
            } else {
                theProcess = Runtime.getRuntime().exec("printenv " + Name);
            }
            System.out.println("***************EJBUtil.getEnvVariable line 5");
            stdInput = new BufferedReader(new InputStreamReader(theProcess
                    .getInputStream()));
            System.out.println("***************EJBUtil.getEnvVariable line 6");
            envValue = stdInput.readLine();
            stdInput.close();
            theProcess.destroy();
            System.out
                    .println("***************EJBUtil.getEnvVariable line 7 envValue******"
                            + envValue);
            return envValue;

        } catch (IOException ioe) {

            throw new Exception(ioe.getMessage());
        }
    }

    public static int getSystemType() {
        Properties p = System.getProperties();
        String os = p.getProperty("os.name");
        int i = 0;
        int osType = 0;

        if ((i = os.indexOf("Win")) != -1) {
            osType = 0;
        } else {
            osType = 1;
        }
        return osType;
    }
    // END OF CLASS

}