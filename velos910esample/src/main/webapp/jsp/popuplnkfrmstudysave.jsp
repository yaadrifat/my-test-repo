<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Crf_Dets%><%--CRF Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>


<jsp:useBean id="linkedformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = " com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.web.crflib.CrflibJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<body>
<DIV> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>	
	<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
			int errorCode=0;
			int ret=0;
			
			String eventId = request.getParameter("eventId");
			
			String accId=(String)tSession.getAttribute("accountId");
			
			String usrId = (String) tSession.getValue("userId");
			
			String ipAdd=(String)tSession.getAttribute("ipAdd");
						
			String[] crfNames= request.getParameterValues("chkcrf");
			
			String[] formIds= request.getParameterValues("formIds");
			
			String[] cNames= request.getParameterValues("crfNames");
			
			String[] crfNumbers= request.getParameterValues("crfNumbers");

			String src = request.getParameter("srcmenu");

			String duration = request.getParameter("duration");

			String protocolId = request.getParameter("protocolId");

			String calledFrom = request.getParameter("calledFrom");

			String calStatus = request.getParameter("calStatus");

			String fromPage = request.getParameter("fromPage");

			
			int cnt=crfNames.length;
			int i=0;
			int len=0;
			int j=0;
			
			String[] crfNums= new String[cnt];
			String[] cN= new String[cnt];
			String[] fIds= new String[cnt];
			len=crfNumbers.length;
			
			int count=0;
			for(i=0;i<len;i++)
		   {
			
			 if(!crfNumbers[i].equals("")) 
			   {
				 for(int k=0;k<cnt;k++)
				   {
				   cN[k]=crfNames[k];				   
				   }
				 crfNums[j]=crfNumbers[i];
				 fIds[j]=formIds[i];
			     j++;
				 count++;
			   }
		   
		   }


		ret=linkedformsB.insertCrfForms(fIds,accId,cN,crfNums,protocolId,eventId,usrId,ipAdd );
		
	%>
		<br>
 <p class = "sectionHeadings" align = center><%=MC.M_Data_SavedSucc%><%--Data saved successfully.*****--%></p>
		<% if((request.getParameter("fromPage")).equals("fetchProt"))
		   {%>
		    <META HTTP-EQUIV=Refresh CONTENT="1;
		URL=crflibbrowser.jsp?eventmode=M&mode=M&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=6&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>" >  
		   <%}
		   else{%>

		 <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>  
		   
		<%
		   }%>
	
<br><br><br><br><br>


<%
}//end of if for eSign check
else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>
