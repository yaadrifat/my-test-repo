<%@page language = "java" import="com.aithent.audittrail.reports.EJBUtil,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE><%=LC.L_Del_Portal%><%--Delete Portal*****--%></TITLE>
</HEAD>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>


<% String src;

src= request.getParameter("srcmenu");


%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*, com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id ="portalB" scope="request" class="com.velos.eres.web.portal.PortalJB"/> 

<body>

<br>

<DIV class="formDefault" id="div1">

<% 

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))	{

//	JM: 07Aug2007 
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));
	
	
	String portalId=request.getParameter("portalId");
	String portalName=request.getParameter("portalName");
	portalName = StringUtil.decodeString(portalName);
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
	
	
%>

	<FORM name="deletePortal" id="delPortalFrm" method="post" action="" onSubmit="if (validate(document.deletePortal)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	<% if ( pageRight>=6 ) {%>
		<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_AboutTo_DelPortal%><%--You are about to delete Portal*****--%>&nbsp;<font color="red"><B><%=portalName%></B></font>.&nbsp;<%=MC.M_OnceDel_CntRetrv_EtrEsign%><%--Once deleted, the data cannot be retrieved.  If you are sure that you would like to delete this portal, enter your e-signature below and click 'Submit'.*****--%>	</b>
	
	</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
		<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="delPortalFrm"/>
		<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

	
	<input type="hidden" name="delMode" value="<%=delMode%>">
	 <input type ="hidden" name="portalId" value="<%=portalId%>">
	
	<%}else {%>
	 <P class="defComments">	<%=MC.M_NoRgtTo_DelPortal%><%--You do not have appropriate rights to delete this portal.*****--%></P>	
	
	</FORM>
	<% }
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>

<%
				} else {
			//Modified for INF-18183 ::: Akshi		
			portalB.deletePortal(EJBUtil.stringToNum(portalId),AuditUtils.createArgs(session,"",LC.L_Manage_Acc));
					
%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "successfulmsg" align = center> <%=MC.M_Portal_DelSucc%><%--The Portal was deleted successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="3; URL=portal.jsp?srcmenu=<%=src%>">
<%
}
}
}//end of if body for session	

  

else

{

%>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</HTML>

