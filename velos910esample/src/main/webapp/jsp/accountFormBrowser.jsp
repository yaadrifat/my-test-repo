<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%--<title>Account Forms</title>*****--%>
<title><%=LC.L_Acc_Forms%></title>
</HEAD>
<%@ page import="com.velos.eres.service.util.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.user.UserJB"
%>
<body>
<br>

<DIV  class="browserDefault" id="div1">  

<%
 HttpSession tSession = request.getSession(true); 
 
 
  if (sessionmaint.isValidSession(tSession))
	{		 
		int pageRight = 0;	
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ACTFRMSACC"));
		String modRight = (String) tSession.getValue("modRight");
		int patProfileSeq = 0, formLibSeq = 0;
		
		 ///////////
		 	// To check for the account level rights
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProfileAppRight = '0';
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);
		 
		
	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
	{
		 ArrayList arrFrmNames = null;
		 ArrayList arrDesc = null;
		 ArrayList arrLastEntryDate = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 //by salil
		 ArrayList arrEntryChar=null;
		 int frmId=0;
		 String frmName = "";
		 String entryChar="";
		 String desc = "";
		 String lastEntryDate = "";
		 String numEntries = "";
		 String url="";

	 	 String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
		 
		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();

		 lnkFrmDao = lnkformB.getAccountForms(iaccId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
		 
		 arrFrmIds = lnkFrmDao.getFormId();		 
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrDesc = lnkFrmDao.getFormDescription();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrLastEntryDate = lnkFrmDao.getFormLastEntryDate();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
		 int len = arrFrmNames.size();
 %>
 <BR>
		<P class="sectionHeadings"> <%--Account Forms*****--%><%=LC.L_Acc_Forms%>
 </P>
		<br>
 <Form name="formlib" action="formLibraryBrowser.jsp"  method="post"> 

 <table width="100%" cellspacing="1" cellpadding="0" >
					<tr> 
				        <th width="20%"><%--Form Name*****--%><%=LC.L_Frm_Name%></th>
				        <th width="25%"><%--Description*****--%><%=LC.L_Description%></th>
						<th width="15%"><%--Last Entry On*****--%><%=LC.L_Last_EntryOn%></th>
						<th width="20%"><%--Number of Entries*****--%><%=LC.L_Number_OfEntries%></th>
					</tr>
	 <%
	 int counter=0;
	 int i=0;
	for(counter=0;counter<len;counter++)
		{
		  frmId = ((Integer)arrFrmIds.get(counter)).intValue();
		  entryChar=(String)arrEntryChar.get(counter);
		  frmName=((arrFrmNames.get(counter)) == null)?"-":(arrFrmNames.get(counter)).toString();
		  desc=((arrDesc.get(counter)) == null)?"-":(arrDesc.get(counter)).toString();
		  lastEntryDate=((arrLastEntryDate.get(counter))== null)?"-":(arrLastEntryDate.get(counter)).toString();
		  numEntries=((arrNumEntries.get(counter))== null)?"-":(arrNumEntries.get(counter)).toString();
			int lk = Integer.parseInt(numEntries);
			if ((i%2)==0) 
				{i++;%>
				<tr class="browserEvenRow"> 
				<%}else
				 {i++;%>
				 <tr class="browserOddRow"> 
					<%}
					
				 if( lk==0){%>
					<td width="10%"><A href="acctformdetails.jsp?srcmenu=tdMenuBarItem1&formId=<%=frmId%>&mode=N&formDispLocation=A&entryChar=<%=entryChar%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=frmName%></A></td>
				<%}
				 else
				 {%>
					<td width="10%"><A href="formfilledaccountbrowser.jsp?formId=<%=frmId%>&srcmenu=<%=src%>&entryChar=<%=entryChar%>" ><%=frmName%></A></td>
				<%}%>							
				
				<td width="8%"><%=desc%></td>	
				<%if(lastEntryDate!="-"){%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}else{%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}%>
				<td width="13%"><%=numEntries%></td>
		
		</tr>
		
		<%}%>

					
	</table>
	 <%
		
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>

</body>

</html>


			
