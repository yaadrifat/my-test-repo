<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_Bgt_AddLineItem%><%--Budget >> Add/Edit Line Items*****--%></title>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*,java.util.*,java.io.*"%>

</head>

<SCRIPT Language="javascript">

 function  validate(formobj){  
  var len = formobj.type.length;
  len = len - 1;

   if(isNaN(len) == true)
   {
   	   if (!(validate_col('Budget Name',formobj.type))) return false;
	    if (!(checkquote(formobj.description.value)))
	    {
	    	formobj.description.focus();
		    return false;	
	    } 
	    	
   }else
   {   
	  	for(i=0;i<=len ;i++){
		if (!(checkquote(formobj.description[i].value))) 
		{
			formobj.description[i].focus();
			return false;
		}		
	  }
	  
	  for(i=0;i<=len ;i++){

		var blank = formobj.type[i].value;

		if (!(checkquote(formobj.description[i].value))) 
		{
			formobj.description[i].focus();
			return false;
		}		
			
		if(blank == null || blank == "")
			{
			if (!(validate_col('Budget Name',formobj.type[i]))) return false;
		    break;
			}
		else
		{
			break;
		}
	  }
  }
    

	if (!(validate_col('e-Signature',formobj.eSign))) return false
	

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
			 return false;

   }

   }
   
  
</SCRIPT>

<body>
<jsp:useBean id="budgetB" scope="session" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

 <% String src = request.getParameter("srcmenu");
	String mode = request.getParameter("mode");
	String lineitemMode = request.getParameter("lineitemMode");	 
	String defaultCategory = "";
	String ctgryPullDn = "";
	SchCodeDao schDao = new SchCodeDao();
	schDao.getCodeValues("category");
	
	defaultCategory = EJBUtil.integerToString(new Integer (schDao.getCodeId("category", "ctgry_patcare")) );

	if (defaultCategory == null){
		
		ctgryPullDn = schDao.toPullDown("cmbCtgry");
	}
	else{
		
		ctgryPullDn = schDao.toPullDown("cmbCtgry", EJBUtil.stringToNum(defaultCategory));
			
	}
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%
	String budgetTemplate = request.getParameter("budgetTemplate");

%>
<br>

	<DIV class="tabDefTopN" id="div1">
  <%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
		{	
		
			if(lineitemMode.equals("M")) {
	%>
	
	 
	 
	 <%
	 }else{
	 
	 %>
	  
	  
	<%}%>
	
	
	
	<jsp:include page="budgettabs.jsp" flush="true"> 
	<jsp:param name="budgetTemplate" value="<%=budgetTemplate%>"/>
	<jsp:param name="mode" value="<%=mode%>"/>		
	</jsp:include>

	</DIV>
			<div class="tabDefBotN" id="div2">	
		<%
		
	String selectedTab = request.getParameter("selectedTab");
	String bgtId=request.getParameter("budgetId");
	String bgtcalId = request.getParameter("bgtcalId");
	String bgtSectionId=request.getParameter("bgtSectionId");
	String bgtStat = request.getParameter("bgtStat");
	
	ArrayList type = new ArrayList();
	ArrayList description = new ArrayList();
	int budgetId=EJBUtil.stringToNum(bgtId);	
	String budgetname=""; 
	budgetB.setBudgetId(budgetId);
	budgetB.getBudgetDetails();
	budgetname=budgetB.getBudgetName();
	String bgtSectionName = request.getParameter("bgtSectionName");

	bgtSectionName=bgtSectionName.replace('~',' ');		
	
	bgtSectionName= StringUtil.decodeString(bgtSectionName);
	
	String sectionName = bgtSectionName;		
		
	BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);
	ArrayList bgtStats = budgetDao.getBgtStats();
	ArrayList bgtSites = budgetDao.getBgtSites();
	ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
	String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
	String bgtSite = (String) bgtSites.get(0);
	int count = 4;
	String lineitemName = "";
	String lineitemDesc = "";
	String lineitemId = "";
	//Rohit CCF-FIN21
	//String tmid = "";
	//String cdm = "";
	
	budgetB.setBudgetId(budgetId);
	budgetB.getBudgetDetails();
	budgetname=budgetB.getBudgetName();
	
	budgetname = (budgetname == null || budgetname.equals(""))?"-":budgetname;
	bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
	bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;
	
	if(lineitemMode.equals("M")) {
		count = 0;
		lineitemName = request.getParameter("lineitemName");
		lineitemName=lineitemName.replace('~',' ');

        
        
		System.out.println("****************"+lineitemName+"******************");
		lineitemDesc = request.getParameter("lineitemDesc");
        lineitemDesc=lineitemDesc.replace('~',' ');
        
		lineitemId = request.getParameter("lineitemId");
		
		
		lineitemName= StringUtil.decodeString(lineitemName);
		lineitemDesc= StringUtil.decodeString(lineitemDesc);
		lineitemB.setLineitemId(EJBUtil.stringToNum(lineitemId));
		lineitemB.getLineitemDetails();	
		//Rohit CCF-FIN21
		//tmid= lineitemB.getLineitemTMID();	
		
		//cdm= lineitemB.getLineitemCDM();
		/*
		if(tmid == null)
			tmid = "";

			if(cdm == null)
		cdm = "";
			*/
		ctgryPullDn = schDao.toPullDown("cmbCtgry",EJBUtil.stringToNum(lineitemB.getLineitemCategory()));
		//ctgryPullDn = schDao.toPullDown("cmbCtgry",EJBUtil.stringToNum(lineitemB.getLineitemCategory()));
		
	}

  %> 
	<Form name="budget" id="budgetadditem" method="post" action="updateadditemstudy.jsp" onSubmit="if (validate(document.budget)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		
<table width=100%>
<tr>
<td class=tdDefault width = 20% ><%Object[] arguments0 = {budgetname}; %> <%=VelosResourceBundle.getMessageString("M_Bgt_Nme",arguments0)%><%--<B>Budget Name: </B>  <%=budgetname%>*****--%></td>
</tr>

<tr>
<td class=tdDefault width = 20% ><%Object[] arguments1 = {bgtStudyNumber}; %> <%=VelosResourceBundle.getMessageString("M_Std_Num",arguments1)%><%--<B>Study Number:  </B>  <%=bgtStudyNumber%>*****--%></td>
</tr>


<tr>
<td class=tdDefault width = 20% ><%Object[] arguments2 = {bgtSite}; %> <%=VelosResourceBundle.getMessageString("M_Org",arguments2)%><%--<B>Organization: </B> <%=bgtSite%>*****--%></td>
</tr>
</table>
<br>


<%
if(lineitemMode.equals("M")){


if ((!(bgtStat ==null)) && bgtStat.equals("Template") ) 
		{ Object[] arguments = {bgtStat};
		%>

			 	<tr>
					<td>
					    	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=bgtStat%>'. You cannot make any changes to the budget.*****--%></Font></P>
					</td>
				</tr>

		<%
		}
%>



<P class = "defComments"> <%Object[] arguments = {sectionName}; %> <%=VelosResourceBundle.getMessageString("M_EdtTypeHere_BgtFlwgSec",arguments)%><%--These Edit / Modified line items 'Types' specified here will be displayed in the budget in the following section : <B><%=sectionName%></B>*****--%></P>
<%}else{%>
<P class = "defComments"> <%Object[] arguments5 = {sectionName}; %> <%=VelosResourceBundle.getMessageString("M_NewType_DispBgt",arguments5)%><%--The new line items 'Types' specified here will be displayed in the budget in the following section : <B><%=sectionName%></B>*****--%></P>
<%}%>

<TABLE border=0 cellPadding=1 cellSpacing=1 width="100%">
  <TR>
    <TH><B><%=LC.L_Type%><%--Type*****--%></B> <FONT class="Mandatory">* </FONT> </TH>
    <TH><%=LC.L_Description%><%--Description*****--%></TH>
	<TH><%=LC.L_Category%><%--Category*****--%></TH>
<!--
	<TH>TMID</TH>
	<TH>CDM</TH>
-->
  </TR>	
<%
for (int i=0;i<=count;i++)
{
%>

<tr>

   <td align=center>
  <input type=text name="type" maxlength=100 size=25  value="<%=lineitemName%>">
  </td>
  <td align=center>
  <input type=text name="description" maxlength=100 size=30 value="<%=lineitemDesc%>">
  </td>
 <td align=center><%=ctgryPullDn%></td>
<%--
 <td align=center><input type=text name="tmid" maxlength=15 size=15 value='<%=tmid%>'> </td> 
 <td align=center><input type=text name="cdm" maxlength=15 size=15 value='<%=cdm%>'></td> 
--%>
  
  
  
</tr>
<%
}
%>

</table>
<input type="hidden" name="src" MAXLENGTH = 15 value="<%=src%>">
<input type="hidden" name="bgtSectionId" MAXLENGTH = 15 value="<%=bgtSectionId%>">
<input type="hidden" name="selectedTab" MAXLENGTH = 15 value="<%=selectedTab%>">
<input type="hidden" name="bgtcalId" MAXLENGTH = 15 value="<%=bgtcalId%>">
<input type="hidden" name="budgetId" MAXLENGTH = 15 value="<%=budgetId%>">
<input type="hidden" name="mode" value="<%=mode%>">
<input type="hidden" name="lineitemMode" value="<%=lineitemMode%>">
<input type="hidden" name="lineitemId" value="<%=lineitemId%>">


<br>
<%	if ((!(bgtStat ==null)) && !bgtStat.equals("Template") && !bgtStat.equals("Freeze")) {%>

<BR>
	
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="budgetadditem"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<%}%>
	</Form>
	<%
	
}//end of if body for session

else

{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>
