<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Lookup_Type%><%--Lookup Type*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script>
checkQuote="N";
</script>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>




<SCRIPT Language="javascript">



function changeCount(formobj, row)

	{

	 var_no_of_checked_fields = 0;


	  selrow = row ;
	  checkedrows = formobj.checkedrows.value;
	  totflds = formobj.totalrows.value;

	  rows = parseInt(checkedrows);

	  fldnum = parseInt(totflds);



	if (fldnum > 1)

	{

	  if (formobj.assignLkpFld[selrow].checked)

		{

			rows = parseInt(rows) + 1;

		}

	  else

		{

			rows = parseInt(rows) - 1;

		}

	}else{

	  if (formobj.assignLkpFld.checked)

		{

			rows = parseInt(rows) + 1;

		}

	  else

		{

			rows = parseInt(rows) - 1;

		}



	}

	formobj.checkedrows.value = rows;


//JM: 12Aug2008 #3136
	var_no_of_checked_fields = rows;

	}
	function setLookup(formobj,mode){

	if (mode=="L"){
	formobj.dynrep.value="";
	formobj.dynrepId.value="";
	formobj.prevId.value="0";
	} else if (mode=="A"){
	formobj.lkpview.value="";
	formobj.prevId.value="0";
	selectView1(document.lookupType,"L")
	}
	}

 function  validate(formobj , pagemode)
 {

 	 if (formobj.lkptype[0].checked==true){
	 formobj.dynrep.value="";
	 formobj.dynrepId.value="";
	 }
	if (formobj.lkptype[1].checked==true){
	 formobj.lkpview.value="";
	}
	if (pagemode == 'final')
	{
	ls_tot =formobj.totalrows.value;



	 checkedrows = formobj.checkedrows.value;

//JM: 12Aug2008 #3136
	if ( (checkedrows || var_no_of_checked_fields) >25){
			var paramArray = [checkedrows];
			alert(getLocalizedMessageString("M_NoFldSel_25ModSel",paramArray));/*alert("No. of fields selected is " +checkedrows+ " exceeding maximum permitted no. 25, please modify your selection.")*****/

			return false;

	}



   if (ls_tot == 0  || checkedrows == 0 )
     {
	     alert("<%=MC.M_LkUpFldMissing_FrmFld%>");/*alert("Lookup Field is missing a corresponding Form Field");*****/
	     return false;
	 }

     //whether a lookup field is selected if the checkbox is checked
	 if (ls_tot == 1)
     {
         if (formobj.assignLkpFld != '' && formobj.assignLkpFld.value != null)
         {
            if (!(validate_col('viewfld',formobj.viewfld)))   return false;
         }
     } else
     {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)  {
			 if ( formobj.assignLkpFld[ll_cnt].checked)
             {
			   if (!(validate_col('viewfld',formobj.viewfld[ll_cnt]))) return false;

             }
         }
     }

	 //whether checkbox is checked if viewfld is selected
     if (ls_tot == 1)
     {
         if (formobj.viewfld.value != '' && formobj.viewfld.value != null)
         {
            if (formobj.assignLkpFld.checked==false){
				alert("<%=MC.M_PlsChk_FrmFld%>");/*alert("Please check the Form's Field.");*****/
				return false;
			}
         }
     } else   {
         for (ll_cnt = 0;ll_cnt < ls_tot;ll_cnt ++)  {
	         if (formobj.viewfld[ll_cnt].value != '' && formobj.viewfld[ll_cnt].value != null)
             {
			   if (formobj.assignLkpFld[ll_cnt].checked==false){
				   alert("<%=MC.M_PlsChk_FrmFld%>");/*alert("Please check the Form's Field.");*****/
					return false;
				}

             }
         }
     }
	} //if page mode == final

 	if (   !(validate_col ('  Section', formobj.section)  )  )  return false
 	if (   !(validate_col ('Sequence', formobj.sequence)  )  )  return false

	if(isNaN(formobj.sequence.value) == true)
	{
		alert("<%=MC.M_SecSeqNum_EtrAgn%>");/*alert("Section Sequence has to be a number. Please enter again");*****/
		formobj.sequence.focus();
		 return false;
   	}

 	if (   !(validate_col ('  Name', formobj.name)  )  )  return false
 	//if (   !(validate_col ('  LKP', formobj.lkpview)  )  &&  !(validate_col ('  DynRep', formobj.dynrepId) ) )  return false

	if (! (splcharcheck(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}
	if (! (splcharcheckForXSL(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}


	if ((formobj.lkpview.value.length==0) && (formobj.dynrepId.value.length==0)){
	alert("<%=MC.M_PlsSel_View%>");/*alert("Please select a view.");*****/
	if (formobj.lkptype[0].checked==true){ formobj.lkpview.focus();}
	if (formobj.lkptype[1].checked==true){formobj.dynrep.focus(); }
	return false;
	}

	if (formobj.lkptype[1].checked==true){
	//if (formobj.prevId.value>0){
	if (formobj.prevId.value!=formobj.dynrepId.value){
	alert("<%=MC.M_SelFlds_ForSelFrm%>");/*alert("Please select fields for selected form.");*****/
	formobj.dynrep.focus();
	return false;
	}
	//}
	} else if (formobj.lkptype[0].checked==true){
	//if (formobj.prevId.value>0){
	if (formobj.prevId.value!=formobj.lkpview.value){
	alert("<%=MC.M_SelFlds_ForLkUp%>");/*alert("Please select fields for selected lookup.");*****/
	formobj.lkpview.focus();
	return false;
	}

	//}
}


	if (pagemode == 'initial')
	{
		alert("<%=MC.M_MapOneFld_WithLkUp%>");/*alert("Please map atleast one field with this lookup");*****/
	    return false;
	}

    if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false

	if(isNaN(formobj.eSign.value) == true)
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		 return false;
   	}
  }
  function openlookup(formobj,accId){
  formobj.lkptype[1].checked=true;

  var userIdFromSession = formobj.userIdFromSession.value;

  url="getlookup.jsp?viewId=&viewName=Ad-Hoc Query&form=lookupType&keyword=dynrep|VELDYNNAME~dynrepId|VELDYNPK&dfilter=lookupType";
  windowname=window.open(url ,'Lookup',"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
  windowname.focus();
  }

function selectView1(formobj,calledFrom){
	// codeed by Chennai Team
	if(formobj.lkptype[1].checked==true){
	   formobj.action = "lookupType.jsp?calledFrom="+calledFrom+"&flagA=SS";
	   formobj.submit();
    }
// end
}
function selectView(formobj,calledFrom)
{
	if(formobj.section.value == '' ){
       alert("<%=MC.M_PlsSel_Section%>")/*alert("Please select a Section")*****/
       formobj.section.focus()
       return false;
	}


		if ((formobj.lkpview.value.length == 0 ) && (formobj.dynrepId.value==0)){
		alert("<%=MC.M_PlsSel_View%> ");/*alert("Please select a View ");*****/
		if (formobj.lkptype[0].checked==true){ formobj.lkpview.focus();}
		if (formobj.lkptype[1].checked==true){formobj.dynrep.focus(); }
		 return false;
		}
		if ((formobj.lkptype[1].checked==true) && (formobj.dynrepId.value==0)){
			alert("<%=MC.M_PlsSel_View%> ");/*alert("Please select a View ");*****/
			 formobj.dynrep.focus();
			 return false;
		 }

		if ((formobj.lkptype[0].checked==true) && (formobj.lkpview.value.length==0))
		{
			alert("<%=MC.M_PlsSel_View%> ");/*alert("Please select a View ");*****/
		    formobj.lkpview.focus();
		    return false;
		}


   formobj.action = "lookupType.jsp?calledFrom="+calledFrom;
   formobj.submit();
}

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<DIV class="popDefault" id="div1">
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;

   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	 String calledFrom=request.getParameter("calledFrom");

	 String lnkFrom=request.getParameter("lnkFrom");
	 String prevIdStr=request.getParameter("prevId");
	 int prevId=(prevIdStr==null)?0:EJBUtil.stringToNum(prevIdStr);

	 if(lnkFrom==null)
		{lnkFrom="-";}

	 String stdId=request.getParameter("studyId");
	 int studyId= EJBUtil.stringToNum(stdId);

		String userIdFromSession = (String) tSession.getValue("userId");
		//calledFrom="L";
		if( calledFrom.equals("A")){ //from account form management
	 	    if (lnkFrom.equals("S"))
	 	      {
		 		 ArrayList tId ;
				 tId = new ArrayList();
				 if(studyId >0)
				 {
				 	TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();
					if (tId.size() <=0)
					 {
						pageRight  = 0;
					  } else
					  {
					  	StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
							ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();


						if ((stdRights.getFtrRights().size()) == 0)
						{
						  pageRight= 0;
						} else
						{
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					  }
			    }
			 }
    	 	else
    	    {
    	  		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
    		 }
	  	}

	 	if( calledFrom.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}

		if( calledFrom.equals("St")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}


	if (pageRight >= 4)

	{

		String mode=request.getParameter("mode");
		String pagemode =request.getParameter("pagemode");

		if (pagemode == null)
			pagemode = "initial";

		String fieldLibId="";
		String fldInstructions="";

		// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD
		// LIKE BOLD, SAMELINE, ALIGN , ITALICS

		Style aStyle= new Style();
		String formId="";
		String lkpDispString = "";
		String formSecId="";
		String fldSeq="";
		String fldAlign="";
		String samLinePrevFld="";
		String fldBold="";
		String fldItalics="";
		String formFldId="";
		String fldType="";
		String codeStatus="";
		String fldName = "";
		String repName="";
		String pullDownViews = "";
		String pullDownField = "";
		int lkpSelView = 0,totCount=0,index=-1;
		ArrayList viewIds = new ArrayList();
		ArrayList viewNames = new ArrayList();
		ArrayList lkpFieldIds = new ArrayList();
		ArrayList lkpFieldNames = new ArrayList();
		ArrayList lkpFieldSystemIds = new ArrayList();


		ArrayList viewFieldNames = new ArrayList();
		ArrayList viewFieldKeyords = new ArrayList();
		ArrayList fldNames = new ArrayList();
		ArrayList fldCols = new ArrayList();
		String lkpFieldId ="";
		String lkpFieldName ="";

		String viewFieldKeyord = "";
		String selViewKeyword = "";
		String pullDownViewFields = "";
		StringBuffer sbLkView = new StringBuffer();
		String lkpType="";
		String stFld = "";
		String stKwrd = "";
		String stSet = "";
		String isChecked = "";
		String pullDownViewFieldsModify = "";
		int checkedrows = 0;

		HashMap hm = new HashMap();


		FieldLibDao fieldLibDao = new FieldLibDao();
		DynRepDao dynDao = new DynRepDao();
		LookupDao lkpDao = new LookupDao();


		formId=request.getParameter("formId");
		String account=(String)tSession.getAttribute("accountId");

		int secfldCount = 0;




		ArrayList idSec= new ArrayList();
		ArrayList descSec= new ArrayList();

		String pullDownSection = "";;
		FormSecDao formSecDao= new FormSecDao();
		formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));
		idSec= formSecDao.getFormSecId();
		descSec=formSecDao.getFormSecName();


		String dfldsys = "";
		String selsecfld = "";
		StringBuffer sbLkpFld = new StringBuffer();

		String lkpFilter = "";
		String offlineFlag = "0";


		if (pagemode.equals("initial"))
		{
		pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);
		}



		if ( mode.equals("M") && (pagemode.equals("initial")) )
		{

				formId=request.getParameter("formId");
				formFldId=request.getParameter("formFldId");
				codeStatus = request.getParameter("codeStatus");


				formFieldJB.setFormFieldId(Integer.parseInt(formFldId) );
				formFieldJB.getFormFieldDetails();
				fieldLibId=formFieldJB.getFieldId();
				offlineFlag = formFieldJB.getOfflineFlag();

				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
				fieldLibJB.getFieldLibDetails();

				formSecId=formFieldJB.getFormSecId();


				//TO GIVE A SINGLE SECTION IN EDIT MODE
				ArrayList idSecNew = new ArrayList();
				ArrayList descSecNew = new ArrayList();
				int secId =EJBUtil.stringToNum(formSecId) ;
				 for(int j = 0 ; j < idSec.size() ; j++)
				 {
				 	int  secIdNew =(   (Integer) idSec.get(j)  ).intValue();
					if ( secIdNew  ==   secId )
					{
				 		idSecNew.add(   (  Integer )idSec.get(j) );
						descSecNew.add( (String )descSec.get(j) ) ;
						break ;
					}
				 }

			// TO MAKE A READONLY BOX FOR NOT LETTING THE EDITING OF THE SECTION FOR FIELD IN EDIT MODE
				String sectionN = (String)descSecNew.get(0) ;
				pullDownSection = "<label>"+sectionN+"</label>"
				 							+ "<Input type=\"hidden\" name=\"section\" value="+(Integer)idSecNew.get(0)+" >  " ;


			//	pullDownSection=EJBUtil.createPullDown("section", secId,idSecNew, descSecNew   );

				fldSeq=((formFieldJB.getFormFldSeq()) == null)?"":(formFieldJB.getFormFldSeq()) ;


				//  WHILE GETTING THE STYLE WE CHECK ITS VALUES TO DISPLAY APPROPRIATELY
				aStyle=fieldLibJB.getAStyle( );
				if ( aStyle != null )
				{

					fldAlign = (  (   aStyle.getAlign()   ) == null)?"-":(   aStyle.getAlign()   ) ;
					fldBold = (  (   aStyle.getBold()   ) == null)?"-1":(   aStyle.getBold()   ) ;
					fldItalics = (  (   aStyle.getItalics()   ) == null)?"-1":(   aStyle.getItalics()   ) ;
					samLinePrevFld = (  (   aStyle.getSameLine()   ) == null)?"-1":(   aStyle.getSameLine()   ) ;
					}
				fldType=fieldLibJB.getFldType();
				fldInstructions = ((fieldLibJB.getFldInstructions()) == null)?"":(fieldLibJB.getFldInstructions()) ;
				fldName = fieldLibJB.getFldName();
				lkpType = ((fieldLibJB.getFldLkpType()) == null)? "" :(fieldLibJB.getFldLkpType()) ;
				lkpSelView = ((fieldLibJB.getLookup()) == null)? 0 :(EJBUtil.stringToNum(fieldLibJB.getLookup())) ;
				lkpDispString = ((fieldLibJB.getLkpDisplayVal()) == null)? "" :(fieldLibJB.getLkpDisplayVal()) ;


				lkpFilter = ((fieldLibJB.getLkpDataVal()) == null)? "" :(fieldLibJB.getLkpDataVal()) ;
			}

		if (pagemode.equals("final"))
		{
			codeStatus = request.getParameter("codeStatus");
			mode = request.getParameter("mode");
			fldInstructions = request.getParameter("instructions");
			fldName =  request.getParameter("name");
			lkpType=request.getParameter("lkptype");
			lkpType=(lkpType==null)?"":lkpType;

			if (lkpType.equals("L")) lkpSelView = EJBUtil.stringToNum(request.getParameter("lkpview"));
			if (lkpType.equals("A")) lkpSelView = EJBUtil.stringToNum(request.getParameter("dynrepId"));

			lkpDispString=request.getParameter("lkpDispString");
			lkpDispString=(lkpDispString==null)?"":lkpDispString;

			if (prevId>0){
			if (prevId==lkpSelView){}else{
			lkpDispString="";
			}
			}

			samLinePrevFld 	= request.getParameter("sameLine");
			if (samLinePrevFld 	 == null)
				samLinePrevFld 	= "-1";
			fldAlign = request.getParameter("align");
			fldSeq = request.getParameter("sequence");
			formSecId = request.getParameter("section");
			pullDownSection=EJBUtil.createPullDown("section", EJBUtil.stringToNum(formSecId), idSec, descSec);
			formId = request.getParameter("formId");

			if (mode.equals("M"))
				formFldId=request.getParameter("formFldId");
		}
		//////////
		//create dropdown for lookup views
		lkpDao = fieldLibJB.getAllLookpUpViews(account);
		viewIds = lkpDao.getLViewIds();
		viewNames = lkpDao.getLViewNames();
		if (lkpType.equals("A")) {pullDownViews = EJBUtil.createPullDown("lkpview", 0, viewIds , viewNames );}
		else if (lkpType.equals("L")) pullDownViews = EJBUtil.createPullDown("lkpview", lkpSelView, viewIds , viewNames );
		else pullDownViews = EJBUtil.createPullDown("lkpview", lkpSelView, viewIds , viewNames );
		///////////
		//retrieve the name of the form
		if (lkpType.equals("A")){
		dynrepB.setId(lkpSelView);
		dynrepB.getDynRepDetails();
		repName=dynrepB.getRepName();
		repName=(repName==null)?"":repName;
		}

		if (lkpSelView > 0 )
		{
			pagemode = "final";
			//get fields

			fieldLibDao = fieldLibJB.getFieldNamesForSection(EJBUtil.stringToNum(formId), EJBUtil.stringToNum(formSecId) , "E");
			lkpFieldIds = fieldLibDao.getFieldLibId();
			lkpFieldNames = fieldLibDao.getFldName();
			lkpFieldSystemIds = fieldLibDao.getFldSysID();
			//get lookup fields
			if (lkpType.equals("L")){
		   	lkpDao = fieldLibJB.getLookpUpViewColumns(String.valueOf(lkpSelView)) ;
		   	viewFieldNames = lkpDao.getLViewRetColumns();
			viewFieldKeyords = lkpDao.getLViewRetKeywords();
			pullDownViewFields = EJBUtil.createPullDownWithStr("viewfld", selViewKeyword, viewFieldKeyords , viewFieldNames );
			} else if (lkpType.equals("A")) {
			dynDao=dynrepB.getReportDetails(String.valueOf(lkpSelView));
			fldCols=dynDao.getRepCol();
			fldNames=dynDao.getRepColName();
			//remove the repeated fields from dropdown
			ArrayList tempCount=new ArrayList();
			for(int counter=0;counter<fldCols.size();counter++){
			index=((String)fldCols.get(counter)).indexOf("|");
			if (index >=0){
			tempCount.add(new Integer(counter));
			}
			}

			//go through tempCount loop toremove those elements
			for(int counter=0;counter<tempCount.size();counter++){

			fldCols.remove(((Integer)tempCount.get(counter)).intValue() - counter );
			fldNames.remove(((Integer)tempCount.get(counter)).intValue() - counter);

			}

			//end remving

			pullDownViewFields = EJBUtil.createPullDownWithStr("viewfld", selViewKeyword, fldCols, fldNames );
			}

	}

		////
%>

    <Form name="lookupType" id="lkpTypeFrmId" method="post" action="lookupTypeSubmit.jsp" onsubmit="if (validate(document.lookupType,'<%=pagemode%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<P class="sectionHeadings"> <%=LC.L_FldType_Lookup%><%--Field Type: Lookup*****--%> </P> </td>

 	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="prevId" value=<%=lkpSelView%> >
	<Input type="hidden" name="pagemode" value="final">
	<Input type="hidden" name="codeStatus" value=<%=codeStatus%>>
	<Input type="hidden" name="lkpDispString" value=<%=lkpDispString%>>
	<Input type="hidden" name="userIdFromSession" value=<%=userIdFromSession%>>


	<Input type="hidden" name="formId" value=<%=formId%> >
	<Input type="hidden" name="formFldId" value=<%=formFldId%> >
	<table width="80%" cellspacing="0" cellpadding="0" border="0">
		<tr>
      <td width="20%"><%=LC.L_Section%><%--Section*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=pullDownSection%> </td>
	  </tr>
	<tr class="browserEvenRow">
      <td width="20%"><%=LC.L_Sequence%><%--Sequence*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><input type="text" name="sequence" size = 10 MAXLENGTH = 10 value="<%=fldSeq%>"> </td>
	  </tr>

		<tr>
			<td >
			<%=LC.L_Fld_Name%><%--Field Name*****--%> <FONT class="Mandatory">* </FONT>
			</td>
			<td width="30%">
			<!-- Commented and Modified by Gopu to fix the bugzilla Issues #2595 -->
			<!--input type="text" name="name" size = 40 MAXLENGTH = 50 value="<%=fldName%>"-->
			<input type="text" name="name" size = 50 MAXLENGTH = 1000 value="<%=fldName%>">
			</td>
			</tr>
			<tr class="browserEvenRow"><td ><label><%=MC.M_FldHelp_DispOnMouse%><%--Field Help (Displayed on Mouse Over)*****--%></I></label></td>
			<td>
			<textarea name="instructions" cols="40" rows="3"  MAXLENGTH=500 ><%=fldInstructions%></textarea>
			</td>
		</tr>

		<%//Commented as per UCSF requirements*/%>
	<!-- <tr><td width="20%" > Align -->
	<% //if  (  (fldAlign.equals("left")  )  || (  fldAlign.equals("") )   )
		//{ %>
		<!-- <input type="radio" name="align" value="left"  onclick=  " " CHECKED> Left
		<input type="radio" name="align" value="right"  onclick=  " "> Right -->
		<%//}   if (   fldAlign.equals("right")   )
		//{%>
		<!-- <input type="radio" name="align" value="left"  onclick=  " " > Left
		<input type="radio" name="align" value="right"  onclick=  " " CHECKED> Right -->
		<%//}   if (  fldAlign.equals("-")  )
		 //{%>
		<!-- <input type="radio" name="align" value="left"  onclick=  " " > Left
		<input type="radio" name="align" value="right"  onclick=  " "> Right -->
		<%//}%></td></tr>
		<!-- <tr height"5"><td> &nbsp; </td></tr> -->
		<tr><td><%=MC.M_AlignFld_DispName%><%--Align (Field Display Name)*****--%></td>
		<%if (  ( samLinePrevFld.equals("-1")  ) || ( samLinePrevFld.equals("0" ) ) ||  (samLinePrevFld.equals("") ) )
		   {%>
			<td width="25%">	<input type="checkbox" name="sameLine" value="1" ><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%><br></td>
		 <%}%>
		 <% if (   samLinePrevFld.equals("1")  )
		   {%>
			<td width="25%">	<input type="checkbox" name="sameLine" value="1" CHECKED><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%><br></td>
		 <%}%>

		</tr>
	</table>


  <table width="70%" cellspacing="0" cellpadding="0" border="0">
	  	<tr>
	       <tr>
	  			<td colspan="3" >
	  <br>
	  			<P class="defComments">
	  				<%=MC.M_Sel_LookupView%><%--Select a Lookup View and click on the 'Select' link. Map fields with lookup data.*****--%>
	  			</P>
				  <br>
	  			</td>
	  		</tr>
			<tr>
	  		<td>
	  			<%=LC.L_Select_LookupView%><%--Select Lookup View*****--%> <font class="mandatory">*</font>
		  	</td>
			<td>
			<%if (lkpType.equals("L") || lkpType.length()==0){ %>
			<input type="radio" name="lkptype" value="L" checked onClick="setLookup(document.lookupType,'L')">
			<%} else{ %>
			<input type="radio" name="lkptype" value="L" onClick="setLookup(document.lookupType,'L')">
			<%}%>
			</td>
			 <td>
				<%=pullDownViews%>
		  	<A href=# onclick="selectView(document.lookupType,'<%=calledFrom%>')"><%=LC.L_Select%><%--Select*****--%></A>
			</td>

		</tr>
		<tr><td></td>
			<td>
			<%if (lkpType.equals("A")){%>
			<input type="radio" name="lkptype" value="A" checked onClick="setLookup(document.lookupType,'A')">
			<%}else{%>
			<input type="radio" name="lkptype" value="A" onClick="setLookup(document.lookupType,'A')">
			<%}%>
			</td>

			 <td>
				<input type="text" name="dynrep" value="<%=repName%>" readonly>
				<%if (lkpType.equals("A")){%>
				<input type="hidden" name="dynrepId" value="<%=lkpSelView%>">
				<%}else{%>
				<input type="hidden" name="dynrepId" value="">
				<%}%>
				<A href=# onclick="openlookup(document.lookupType,<%=account%>)"><%=LC.L_Lookup%><%--Lookup*****--%></A>&nbsp;&nbsp;
				<A href=# onclick="selectView(document.lookupType,'<%=calledFrom%>')"><%=LC.L_Get_LookupFlds%><%--Get Lookup Fields*****--%></A>
		  	</td>



		</tr>

	</table>

		<%
	if ( ( pagemode.equals("final") && mode.equals("N")) ||  ( pagemode.equals("final") && mode.equals("M") && EJBUtil.isEmpty(lkpDispString)) )
		{

		%>
	<BR>
	  <table width="100%" cellspacing="0" cellpadding="0" border="0">
	  <% if(request.getParameter("flagA")==null){ %>
		<tr><td><b><%=LC.L_Form_Flds%><%--Form Fields*****--%> </b></td>	 <td><b><%=LC.L_Lookup_Flds%><%--Lookup Fields*****--%></td></b> </tr>
	  <% }%>
	<%
		for (int counter = 0; counter <= lkpFieldSystemIds.size() -1 ; counter++)
		{

	   if (! ((String)lkpFieldSystemIds.get(counter)).equals("er_def_date_01")  )
		   {
		   	     secfldCount++;
	%>
		<tr>
			<td>
				<input type="checkbox" name="assignLkpFld" value= <%= lkpFieldSystemIds.get(counter)%> onclick="changeCount(document.lookupType, <%=secfldCount-1%>)" ><%= lkpFieldNames.get(counter)%>
				<Input type="hidden" name="selLkpFld" value= <%= lkpFieldSystemIds.get(counter)%>>
			</td>
			 <td>
				<%=pullDownViewFields%>
		  	</td>

		</tr>
	<%
		}
	}

	%>
    <Input type="hidden" name="checkedrows" value=0>
    <Input type="hidden" name="totalrows" value=<%=secfldCount%>  >
	</table>
	<%
	}
	//////////////////////////////////////////////////////////// when page mode is final and we have mode = "M"
	else if (pagemode.equals("final") && mode.equals("M" ) && (! EJBUtil.isEmpty(lkpDispString)))
	{

		%>
	<BR>

	<%
		StringTokenizer st = new StringTokenizer(lkpDispString,"~",false);


		checkedrows = st.countTokens();

	    while (st.hasMoreTokens())
	    {
			stSet = st.nextToken();

			StringTokenizer stMap = new StringTokenizer(stSet,"|",false);

			//add it to hashmap
			stFld = stMap.nextToken();
			stKwrd = stMap.nextToken();

			hm.put(stFld,stKwrd);

	     }

	 %>

	  <table width="100%" cellspacing="0" cellpadding="0" border="0">
	  <tr><td><b><%=LC.L_Form_Flds%><%--Form Fields*****--%></b></td>	 <td><b><%=LC.L_Lookup_Flds%><%--Lookup Fields*****--%></td></b> </tr>
	<%
		for (int counter = 0; counter <= lkpFieldSystemIds.size() -1 ; counter++)
		{
		   if (! ((String)lkpFieldSystemIds.get(counter)).equals("er_def_date_01")  )
		   {

	   	    secfldCount++;
			isChecked = "";
			if (hm.containsKey(lkpFieldSystemIds.get(counter)))
			{

				isChecked = "CHECKED";
				selViewKeyword = (String) hm.get(lkpFieldSystemIds.get(counter));
				if (lkpType.equals("A"))
				pullDownViewFieldsModify = EJBUtil.createPullDownWithStr("viewfld", selViewKeyword, fldCols , fldNames );
				if (lkpType.equals("L"))
				pullDownViewFieldsModify = EJBUtil.createPullDownWithStr("viewfld", selViewKeyword, viewFieldKeyords , viewFieldNames );
			}
			else
			{
				pullDownViewFieldsModify = pullDownViewFields;
			}

	%>
		<tr>
			<td>
				<input type="checkbox" name="assignLkpFld" <%=isChecked%> value= <%= lkpFieldSystemIds.get(counter)%> onclick="changeCount(document.lookupType, <%=secfldCount-1%>)" ><%= lkpFieldNames.get(counter)%>
				<Input type="hidden" name="selLkpFld" value= <%= lkpFieldSystemIds.get(counter)%>>
			</td>
			 <td>
				<%=pullDownViewFieldsModify%>
		  	</td>

		</tr>

	<%
			}
	}

	%>


	</table>
	<Input type="hidden" name="checkedrows" value=<%=checkedrows%>>
    <Input type="hidden" name="totalrows" value=<%=secfldCount%>  >
	<%
	}

	////////////////////////////////////////////////////////////
	%>
		<br> <br>

	<br>

	<%
		if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && !codeStatus.equals("Deactivated") && !codeStatus.equals("Lockdown") )|| mode.equals("N")){
			if ( (calledFrom.equals("St") || calledFrom.equals("A")) && codeStatus.equals("Active"))  {}
			else{%>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="lkpTypeFrmId"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
		<%}
		}%>
     <br>
   </Form>
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>
















