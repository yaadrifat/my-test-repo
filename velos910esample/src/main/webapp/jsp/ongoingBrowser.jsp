<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/>
<%
String moduleName  = "irbOngoing";
%>

<script>
var paginate_study;
window.onload = initEvents;
function initEvents() {
	document.getElementById('searchCriteria').onkeypress = readCharacter;
}
function readCharacter(evt) {
	evt = (evt) || window.event;
	if (evt) {
	    var code = evt.charCode || evt.keyCode;
	    if (code == 13 || code == 10) {
	        paginate_study.runFilter();
	    }
	}
}
function checkSearch(formobj)
{
 	paginate_study.runFilter();
}



 $E.addListener(window, "load", function() {

    paginate_study=new VELOS.Paginator('<%=moduleName%>',{
 				sortDir:"asc",
				sortKey:"STUDY_NUMBER",
				defaultParam:"userId,accountId,grpId,tabsubtype",
				filterParam:"searchCriteria",
				dataTable:'serverpagination_study',
				saveEnable:false,
				exportEnable:false,
				rowSelection:[5,10,15]
				});
	paginate_study.render();
		elem = document.getElementById("yui-gen2")


			 }
				 )

		titleLink=function(elCell, oRecord, oColumn, oData)
		{
		var studyTitle=oRecord.getData("STUDY_TITLE");
		var reg_exp = /\'/g;
		studyTitle = studyTitle.replace(reg_exp, "\\'");
		reg_exp = /[\r\n|\n]/g;
		studyTitle = studyTitle.replace(reg_exp, "<br/>");
 		elCell.innerHTML="<a href=\"#\" onmouseover=\"return overlib('"+studyTitle+"',CAPTION,'<%=LC.L_Study_Title%><%--Study Title*****--%>');\" onmouseout=\"return nd();\"><img border=\"0\" src=\"./images/View.gif\" title=\"<%=LC.L_View%><%--View*****--%>\"/></a>";
		}

		studyNumber=function(elCell, oRecord, oColumn, oData)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		var studyNumber=oRecord.getData("STUDY_NUMBER");

		if (!study) study="";
		if (!studyNumber) studyNumber="";

 		htmlStr="<A HREF=\"study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=M&studyId="+study+"\">"
		+studyNumber+"</A> ";
 		elCell.innerHTML=htmlStr;
		}

		formsLink=function(elCell, oRecord, oColumn, oData, appSubmissionType)
		{
		var htmlStr="";
		var study=oRecord.getData("PK_STUDY");
		if (!study) study="";
		//Modified for Bug#8650 | Date 28 Feb 2012 | By: YPS
		if(appSubmissionType=='study_amend'){
	 		htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+",'"+appSubmissionType+"')\">"+
			"<img src=\"./images/Amendments.gif\" border=\"0\" align=\"left\"/></A> ";
	 		elCell.innerHTML=htmlStr;
		}
		if(appSubmissionType=='prob_rpt'){
			htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+",'"+appSubmissionType+"')\">"+
			"<img src=\"./images/Report.gif\" border=\"0\" align=\"left\"/></A> ";
	 		elCell.innerHTML=htmlStr;
			
		}
		if(appSubmissionType=='cont_rev'){
			htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+",'"+appSubmissionType+"')\">"+
			"<img src=\"./images/ContinuingReview.gif\" border=\"0\" align=\"left\"/></A> ";
	 		elCell.innerHTML=htmlStr;
			
		}
		if(appSubmissionType=='closure'){
			htmlStr="<A title=\"<%=LC.L_Action%><%--Action*****--%>\" HREF=\"javascript:void(0);\" onclick=\"openFormsWin('"+ $('tabsubtype').value+"', "+study+",'"+appSubmissionType+"')\">"+
			"<img src=\"./images/StudyClosure.gif\" border=\"0\" align=\"left\"/></A> ";
	 		elCell.innerHTML=htmlStr;
			
		}
		}

		amdLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'study_amend');
		}
		probLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'prob_rpt');
		}
		contLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'cont_rev');
		}
		closLink=function(elCell, oRecord, oColumn, oData) {
			formsLink(elCell, oRecord, oColumn, oData, 'closure');
		}

		function openFormsWin(tabsubtype,study,appSubmissionType)
		{

			   windowName = window.open("irbForms.jsp?appSubmissionType="+appSubmissionType+"&tabsubtype="+tabsubtype+"&studyId="+study,"actionWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=800,left=400,top=400");
			   windowName.focus();
  		}

 </script>

<body class="yui-skin-sam">

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<%@ page import="com.velos.eres.service.util.*" %>

   <%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,java.sql.*,com.velos.eres.business.common.*"%>




<%

 HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    String userId ="";

    userId = (String) tSession.getValue("userId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");


    String accountId = (String) tSession.getValue("accountId");
    String tabsubtype  = "";

    tabsubtype = request.getParameter("tabsubtype");

    //Added by Manimaran to give access right for default admin group to the delete link
	int usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();

	 int pageRight = 0;
     pageRight = 7;

    if (pageRight > 0 )    {

    %>

  <Form name="subcommon" METHOD="POST" onsubmit="return false;">

    <input type="hidden" id="accountId" value="<%=accountId%>">
    <input type="hidden" id="userId" value="<%=userId%>">
    <input type="hidden" id="grpId" value="<%=defUserGroup%>">
    <input type="hidden" id="tabsubtype" value="<%=tabsubtype%>">

    <p class="sectionHeadings">
    <tr height="10"><td></td></tr>
    <table border="0" cellspacing="0" cellpadding="0" class="outline midAlign" width="99%">
    	<tr height="40" >
    		<td>
    			&nbsp;&nbsp;&nbsp;<%=LC.L_Search_AStd %><%-- Search a <%=LC.Std_Study%>*****--%>
    		</td>
    		<td>
    		    &nbsp;&nbsp;&nbsp;<input type="text" id="searchCriteria" size="50" value="">
      	    </td>
      	    <td>&nbsp;&nbsp;&nbsp;<button type="submit" onClick="paginate_study.runFilter()"><%=LC.L_Search%></button></td>
    	</tr>
    	</table>
    </p>
    			<br>

        <div >
    <div id="serverpagination_study" ></div>
    </div>





  </Form>

<script>
	 //	alert(paginate_study);
		//if (paginate_study) paginate_study.runFilter();

	</script>
    <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
}//end of if body for session
else {
%>

    <jsp:include page="timeout.html" flush="true"/>
<%}
%>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>




