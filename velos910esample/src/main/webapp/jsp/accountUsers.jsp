<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%--Account Users*****--%><%=LC.L_Acc_Users%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<script>
function fopen(link)
{
	window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=400,height=280')
	return true;
}
</script>


<body>

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.*,java.text.*"%>



<%
	int accId = EJBUtil.stringToNum(request.getParameter("accId"));

   UserDao userDao = new UserDao();

   ArrayList userLastNames;
   
   ArrayList userFirstNames;   

   ArrayList userStats;

   ArrayList userCodes;
   
   ArrayList siteNames;
   
   ArrayList userIds;

   userIds = new ArrayList();

   userFirstNames = new ArrayList();
   
   userLastNames = new ArrayList();   

   userStats = new ArrayList();

   userCodes = new ArrayList();
   
   siteNames = new ArrayList();

   userDao = userB.getAccountUsersDetails(accId);

   userIds = userDao.getUsrIds();

   userLastNames = userDao.getUsrLastNames();
   
   userFirstNames = userDao.getUsrFirstNames();   

   userStats = userDao.getUsrStats();

   userCodes = userDao.getUsrCodes();
   
   siteNames  = userDao.getUsrSiteNames();

   int len = userIds.size();

   int counter = 0;
   
   String userId = "";

   String userLastName = "";
   
   String userFirstName = "";
   
   String userName = "";   

   String userStatus = "";

   String userCode = "";
   
   String siteName = "";

   String pagename = "accountcreation.jsp";

%>

<br>
<DIV class="browserDefault_veloshome" id="div1">
<%
HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

		{
		
%>

<Form name="userbrowser" method="post" action="" onsubmit="">



<P class = "defComments">

<%--Following are the users in the selected account*****--%><%=MC.M_FlwUsr_InSelAcc%>

</P>



<table width="100%" >
 <tr>
   <th> 
	<%--User Name*****--%><%=LC.L_User_Name%>
   </th>
   
   <th>
	<%--User ID*****--%><%=LC.L_User_Id%>
   </th>
   
   <th>
	<%--User Status*****--%><%=LC.L_User_Status%>
   </th>
   
   <th>
	<%--Organization*****--%><%=LC.L_Organization%>
   </th>

   <th> 
	<%--Reset Password*****--%><%=LC.L_Reset_Pwd%>
   </th>   
  </tr>

 <%

	for(counter = 0;counter<len;counter++)
	{	

	userId=((userIds.get(counter)) == null)?"-":(userIds.get(counter)).toString();
 	userLastName=((userLastNames.get(counter))==null)?"-":(userLastNames.get(counter)).toString();
 	userFirstName=((userFirstNames.get(counter))==null)?"-":(userFirstNames.get(counter)).toString();
	userName = userLastName + ", " + userFirstName; 
    userStatus=((userStats.get(counter)) == null)?"-":(userStats.get(counter)).toString();

	if (userStatus.equals("A")) {

		/*userStatus="Activated";*****/
		userStatus=LC.L_Activated;

	}

	else if (userStatus.equals("D"))  {

		/*userStatus="Deactivated";*****/
		userStatus=LC.L_Deactivated;

	}

	else if (userStatus.equals("R"))  {

		/*userStatus="Reactivated";*****/
		userStatus=LC.L_Reactivated;

	}

	else if (userStatus.equals("P"))  {

		/*userStatus="Pending";*****/
		userStatus=LC.L_Pending;

	}
	else if (userStatus.equals("J"))  {

		/*userStatus="Rejected";*****/
		userStatus=LC.L_Rejected;

	}
	
	userCode = ((userCodes.get(counter)) == null)?"-":(userCodes.get(counter)).toString();
	
	siteName = ((siteNames.get(counter)) == null)?"-":(siteNames.get(counter)).toString();
	
	
%>

<%
	if ((counter%2)==0) {
%>
		<tr class="browserEvenRow">	
  <%
	}
	else{
  %>
			<tr class="browserOddRow">	
  <%
	}
  %>
		<td>

	<A HREF="<%=pagename%>?userId=<%=userId%>&mode=M&from=accountUsers"><%=userName%></A> 

		</td>
		
		<td>
		<%=userCode%>
		</td>


		<td>
		 <%= userStatus%>
		</td>

		<td>
			<%=siteName%>
		</td>

		<td align="center">
			<%String link ="resetpass.jsp?userId=" + userId + "&fromPage=Velos";  %>
			<A href=# title="<%=LC.L_Change_Pwd%><%--Change Password*****--%>"  onClick='return fopen("<%=link%>")'><%--Reset*****--%><%=LC.L_Reset%></A>
		</td>
		
		</tr>
<%
		}
%>

</table>

</Form>

<%
}//end of if body for session

else

{
%>
  <jsp:include page="timeout_admin.html" flush="true"/>
<%
}
%>



<div>
<jsp:include page="bottompanel.jsp" flush="true"/> 
</div>

</div>

	<jsp:include page="velosmenus.htm" flush="true"/> 

</body>

</html>
