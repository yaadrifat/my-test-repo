<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Alert_Notification%><%--Alert Notification*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="JavaScript1.1">

function setVals(str,formobj)
{
		formobj.action = formobj.action + "?addFlag=" + str;
		return true;
}

function setParam(str,formobj){
		formobj.subVal.value=str;

		if (!(validate(formobj))) {
			 return false  }
//		 setVals(formobj.subVal.value,formobj);
		formobj.submit();
	}


function openwin1(formobj){
var names = formobj.alertNotifyToNames.value;
var ids = formobj.alertNotifyToId.value;
var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=notification&mode=initial&ids=" + ids + "&names=" + names ;

window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
}



function  validate(formobj,str)
{


     if (!(validate_col('Event-Status',formobj.eventstatus))) {
     	  	  formobj.subVal.value="Submit";
     	  return false
     	  }
     if (!(validate_col('e-Signature',formobj.eSign))) {
     	 	  formobj.subVal.value="Submit";
     return false}

	if(isNaN(formobj.eSign.value) == true)
	{
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
		  formobj.subVal.value="Submit";
	return false;
       }

    //if   (!(setVals(str,formobj))) return false ;
    //formobj.submit();
    	return setVals(formobj.subVal.value,formobj);
	return true ;

}

</SCRIPT>
</head>

<% String src="";
src= request.getParameter("srcmenu");
%>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<jsp:useBean id="alertNotifyB" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="eventInfo" scope="request" class="com.velos.esch.business.common.EventInfoDao"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page import="com.velos.eres.service.util.*" %>
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*"%>

<body>

<br>

<DIV class="formDefault" id="div1">

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {

	String enrollId =(String) tSession.getValue("enrollId");
	String study = (String) tSession.getValue("studyId");
	String pkey = request.getParameter("pkey");
	String protocolId = request.getParameter("protocolId");

	ArrayList eventNames = null;
	String eventName="";
	int personPK = 0;
	String patientId = "";
	String dob = "";
	String gender = "";
	String genderId = "";
	String yob = "";
	int age = 0;
	String alertNotifyToName="";
	String adverseToId="";
	String alertNotifyToId = "";
	String str = "";
	Calendar cal1 = new GregorianCalendar();
	String alertNotifyId="";
	String mode = request.getParameter("mode");


	String studyId = (String) tSession.getValue("studyId");
	String statDesc=request.getParameter("statDesc");
	String statid=request.getParameter("statid");
	String studyVer = request.getParameter("studyVer");

	studyB.setId(EJBUtil.stringToNum(studyId));

	studyB.getStudyDetails();
    String studyTitle = studyB.getStudyTitle();
	String studyNumber = studyB.getStudyNumber();

	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	} else {
		protocolId = "";
	}


	EventInfoDao eventInf = new EventInfoDao();
	eventInf = adveventB.getAdverseEventNames(EJBUtil.stringToNum(enrollId));
	eventNames =eventInf.getadvEventNames();
	String fromPage = request.getParameter("fromPage");

	SchCodeDao cd = new SchCodeDao();
	String dCur = "";
	cd.getCodeValues("eventstatus",0);
	dCur =  cd.toPullDown("eventstatus");

	person.setPersonPKId(EJBUtil.stringToNum(pkey));
	person.getPersonDetails();
	patientId = person.getPersonPId();
	genderId = person.getPersonGender();
	dob = person.getPersonDob();
	gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
	if (gender==null){ gender=""; }
	yob = dob.substring(6,10);
	age = cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob);

	//if global setting=G or GR, user cannot enter data
	String globalSetting = alertNotifyB.getANGlobalFlag(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId));

%>
<P class="sectionHeadings"> <%=MC.M_MngProd_EvtNotify%><%--Manage <%=LC.Pat_Patients%> >> Event Notifications*****--%> </P>
<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="pkey" value="<%=pkey%>"/>
<jsp:param name="patientCode" value="<%=patientId%>"/>
<jsp:param name="patProtId" value="<%=enrollId%>"/>
<jsp:param name="studyVer" value="<%=studyVer%>"/>
</jsp:include>

 <br>




<%

if(mode.equals("M"))
{

// To open the notification in the modified mode.
	alertNotifyId = request.getParameter("alertNotifyId");
	alertNotifyB.setAlNotId(EJBUtil.stringToNum(alertNotifyId));
	alertNotifyB.getAlertNotifyDetails();

	alertNotifyToId = alertNotifyB.getAlertNotifyAlNotUsers();
	alertNotifyToId = (alertNotifyToId==null)?"":alertNotifyToId;
	UserDao userDao = userB.getUsersDetails(alertNotifyToId);
	ArrayList fname = userDao.getUsrFirstNames();
	ArrayList lname = userDao.getUsrLastNames();

	for(int i=0;i<fname.size();i++)
	{

	alertNotifyToName = alertNotifyToName + ((String)lname.get(i)).trim() + "," + ((String)fname.get(i)).trim() + ";";

	}
	if(alertNotifyToName.length() > 0)
	{
	alertNotifyToName = alertNotifyToName.substring(0,alertNotifyToName.length()-1);
	}

// To open the notification in the modified mode.
	//alertNotifyToName=alertNotifyB.getAlertNotifyAlNotUsers();
	eventName=alertNotifyB.getAlertNotifyAlNotMobEve();
	dCur = alertNotifyB.getAlertNotifyCodelstAnId();
	dCur =  cd.toPullDown("eventstatus",EJBUtil.stringToNum(dCur));

}

%>


<%--
<Form name="adverse" method=post action="updatenotification.jsp" onsubmit="return validate(document.adverse)">
--%>

<Form name="adverse" method=post action="updatenotification.jsp" onsubmit="return validate(document.adverse)">
<!-- <Form name="adverse" method=post action="updatenotification.jsp">-->


<table width=100% class="patHeader">
<tr>
	<td class=tdDefault width = 20% ><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:</td><td class=tdDefault><%=studyNumber%></td>
</tr>
<tr>
	<td class=tdDefault><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:</td><td class=tdDefault><%=studyTitle%></td>
</tr>
 <tr>
 	<td class=tdDefault ><%=LC.L_Pcol_Cal%><%--Protocol Calendar*****--%>:</td><td class=tdDefault><%=protName%></td>
 </tr>
</table>
<br>
<p class = "sectionHeadings" ><%=LC.L_Notification%><%--Notification*****--%></p>

<table width="700" cellspacing="0" cellpadding="0" >
  <tr>
    <td class=tdDefault ><%=LC.L_Send_MsgTo%><%--Send Message To*****--%>:</td>
   <td width='65%'>

   <input type=text name="alertNotifyToNames" maxlength=100 size=20 readonly  value="<%=alertNotifyToName%>" >
   <A HREF=# onClick="openwin1(document.adverse)">  <%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%>
   </A>
   </td>
  </tr>
  <input type=hidden name=alertNotifyToId	 value='<%=alertNotifyToId%>'>
  <tr>
    <td class=tdDefault ><%=MC.M_WhenEvtStatus_ChgTo%><%--When Event Status Changed To*****--%>:
        <FONT class="Mandatory">* </FONT>
    </td>
    <td><%=dCur%> </td>
  </tr>
  <tr>
    <td class=tdDefault ><%=LC.L_For_Evt%><%--For Event*****--%>:

    </td>

<td>

<%


	int length=eventNames.size();
	str = "<select name=eventName>";
	str  = str + "<option value = 'All'> "+LC.L_All/*All*****/+"</option>";
for(int i=0 ;i<length; i++)
{
	if(eventNames.get(i).equals(eventName))
	{
		str = str + "<option selected value='" + eventNames.get(i) + "'>" +  eventNames.get(i) + "</option>";
	}
	else
	{
		str = str + "<option value='" + eventNames.get(i) + "'>" +  eventNames.get(i) + "</option>";
	}
}
	str = str + "</select>";

//	else
//	{
//	str = "<select name=eventName>";
//	str = str + "<option value='" + eventName + "'>" +  eventName + "</option>";
//	str = str + "</select>";
//	}

%>


<%=str%>
</td>

  </tr>
</table>

<% if ((globalSetting.equals("N")) || (globalSetting.equals("NR"))) {%>
<table width=100% >
	<tr>


	<td class=tdDefault>
	<%=LC.L_EHypenSign%><%--E-Signature*****--%> <FONT class="Mandatory">* </FONT>
	</td >
	<td class=tdDefault>
		<input type="password" name="eSign" maxlength="8">

<%// changes by Vishal on 09/19/2002 to fix bug SCR#50%>

				<input type="image" src="../images/jpg/Submit.gif" align="absmiddle"  border="0" onClick="return setVals('Submit',document.adverse);">
	<!--	<input type="image" src="../images/jpg/Add.gif" align="absmiddle" border="0" onClick="return setVals('Add',document.adverse);">-->
	<!--<A href='#' onClick="return validate(document.adverse,'');"><img src="../images/jpg/Submit.gif" border="0" align="absmiddle"></img></A>      -->
    <button onClick="return setParam('Add',document.adverse);"><%=LC.L_Submit_AddAnother%></button>
    <%//End Vishal Change%>
	</td>
	</tr>
</table>
<%}%>

<input type=hidden name=mode value=<%=mode%>>
<input type=hidden name=alNotId value=<%=alertNotifyId%>>
<input type=hidden name=fromPage value=<%=fromPage%>>
<input type=hidden name=pkey value=<%=pkey%>>
<input type=hidden name=alertNotifyToName value=<%=alertNotifyToName%>/>

<input type="hidden" name=studyId value=<%=studyId%>>
<input type="hidden" name=statDesc value=<%=statDesc%>>
<input type="hidden" name=statid value=<%=statid%>>
<input type=hidden name=studyVer value=<%=studyVer%>>
<input type="hidden" name=patientCode value=<%=patientId%>>
<input type="hidden" name=protocolId value=<%=protocolId%>>
   <input type="hidden" name="subVal" value="Sumbit">
</form>


<%
} //end of if session times out
else
{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
   <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

</body>
</html>
