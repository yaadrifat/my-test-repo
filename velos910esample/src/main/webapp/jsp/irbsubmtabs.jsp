<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.TeamDao"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
 
<%!
  private String getHrefBySubtype(String subtype, String mode) {
      if (subtype == null) { return "#"; }
      if ("manage_menu".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_summary_tab&mode=N";
      }
      if ("irb_summary_tab".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_summary_tab&mode=N";
      }
      if ("irb_new_tab".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_new_tab&mode=N";
      }
      if ("irb_assigned_tab".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_assigned_tab&mode=N";
      }
      
      if ("irb_pend_rev".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_pend_rev&mode=N";
      }
      if ("irb_compl_tab".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_compl_tab&mode=N";
      }
      if ("irb_post_tab".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_post_tab&mode=N";
      }
      if ("irb_pend_tab".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_pend_tab&mode=N";
      }
        if ("irb_appr_tab".equals(subtype)) {
          return "irbsubmissions.jsp?selectedTab=irb_appr_tab&mode=N";
      }
      return "#";
  }
%>

<%
String mode="N";
String selclass;
String verNumber = ""  ; 
String userId = "";
 
String userIdFromSession = "";
String accId = "";

String tab= request.getParameter("selectedTab");
String from= request.getParameter("from");
mode= request.getParameter("mode");
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
 	userIdFromSession= (String) tSession.getValue("userId");
	accId = (String) tSession.getValue("accountId");
	verNumber = request.getParameter("verNumber");
	userId = (String) tSession.getValue("userId");

	int subSumRight= 0;
	int subNewGroupRight=  0;
	int subComplGroupRight=  0;
	int subAssignedGroupRight  = 0;
	int subPRGroupRight  = 0;
	int subPIGroupRight  = 0;
	     	   	   	
    int subPendingReviewsRight = 0;
    int subFinalOutcomeRight = 0;
    
    
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	
	subSumRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SSUM"));

	subNewGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SNS"));
	
	subComplGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SCS"));

    subAssignedGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SAS"));
    
    subPRGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPRS"));
    
    subPIGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPI"));  
     
    subPendingReviewsRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_PREV"));  
    
    subFinalOutcomeRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_OUT"));  
      
if (mode == null){
 	mode = "N";
} 

out.print("<Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\">");
String uName = (String) tSession.getValue("userName");
%>
<!-- <P class = "userName"><%= uName %></p> -->
<%
 
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_subm_tab");

%>
<DIV>
<!-- <table  cellspacing="0" cellpadding="0" border="0">   --> 
<table  cellspacing="0" cellpadding="0" border="0"> 
	<tr>
    <%
    // check the rights
 	// To check for the account level rights
	 
	 for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings)tabList.get(iX);
	    // If this tab is configured as invisible in DB, skip it
	    if ("0".equals(settings.getObjVisible())) {
	        continue;
	    }
	    // Figure out the access rights
        boolean showThisTab = false;
	 
			
		if ("irb_summary_tab".equals(settings.getObjSubType()) && (subSumRight >= 4))
		{
			showThisTab = true;
		}
		else if ("irb_new_tab".equals(settings.getObjSubType()) && (subNewGroupRight >=6 || subNewGroupRight == 4))
		{
			showThisTab = true;
		}	 
	 	else if ("irb_assigned_tab".equals(settings.getObjSubType()) && (subAssignedGroupRight >=6 || subAssignedGroupRight == 4))
		{
			showThisTab = true;
		}	 
		else if ("irb_pend_rev".equals(settings.getObjSubType()) && (subPendingReviewsRight  >=6 || subPendingReviewsRight  == 4))
		{
			showThisTab = true;
		}	 
		else if ("irb_compl_tab".equals(settings.getObjSubType()) && (subComplGroupRight >=6 || subComplGroupRight == 4))
		{
			showThisTab = true;
		}
		else if ("irb_post_tab".equals(settings.getObjSubType()) && (subPRGroupRight >=6 || subPRGroupRight == 4))
		{
			showThisTab = true;
		}
		else if ("irb_pend_tab".equals(settings.getObjSubType()) && (subPIGroupRight >=6 || subPIGroupRight == 4))
		{
			showThisTab = true;
		}	 
		
		else if ("irb_appr_tab".equals(settings.getObjSubType()) && (subFinalOutcomeRight >=6 || subFinalOutcomeRight == 4)) 
		{
			showThisTab = true;
		}
		
	    if (!showThisTab) { continue; } // no access right; skip this tab
	    // Figure out the selected tab position
        if (tab == null) { 
            selclass = "unselectedTab";
	    } else if (tab.equals(settings.getObjSubType())) {
            selclass = "selectedTab";
        } else {
            selclass = "unselectedTab";
        }
    %>
		<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				 <!--  	<td class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td>  --> 
					<td class="<%=selclass%>">
						<a href="<%=getHrefBySubtype(settings.getObjSubType(),mode )%>"><%=settings.getObjDispTxt()%></a>
					</td> 
				  <!--  <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td>  --> 
			  	</tr> 
		   	</table> 
        </td>
    <%
     } // End of tabList loop
    %>
		
   	</tr>
 <!--   <tr>
     <td colspan=10 height=10></td>
  </tr>  --> 
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
<%		
} //session time out.
%>
</DIV> 
