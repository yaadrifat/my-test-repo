<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Manage_Org%><%--Manage Organizations*****--%></title>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="userSiteJB" scope="request" class="com.velos.eres.web.usrSiteGrp.UsrSiteGrpJB"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<body>
<div class="popDefault" style="width:480px">
<%		
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
	
String pkUserSIte []= request.getParameterValues("pkUserSite");	
String pkUserSiteGrp []= request.getParameterValues("userSiteGrpPk");	
String groupPk []= request.getParameterValues("groupPk");	
String groupOldPk []= request.getParameterValues("groupOldPk");	
String ipAdd = (String) tSession.getValue("ipAdd");
int usr = EJBUtil.stringToNum(request.getParameter("userId"));
String userIdSess = (String) tSession.getValue("userId");
String eSign = request.getParameter("eSign");
String oldESign = (String) tSession.getValue("eSign");
if(!oldESign.equals(eSign)) {
	%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
		} else {
int loopLenth= pkUserSIte.length;
ArrayList delPkValues= new ArrayList();
int counter=0;
for (int i=0;i<loopLenth;i++)
{

	  if(groupPk[i]=="" && groupOldPk[i]!="")
  	  {
	    String [] tablePkArray =  pkUserSiteGrp[i].split(",");
		  for(int x=0;x<tablePkArray.length;x++)
		    {
		      delPkValues.add(tablePkArray[x]);
		    }
	  }else if(groupPk[i]!="" && groupOldPk[i]!="")
	  {	
	   String [] newArray =  groupPk[i].split(",");	  
	   String [] oldArray =  groupOldPk[i].split(",");
	   String [] tablePkArray =  pkUserSiteGrp[i].split(",");
	   int lenOld=oldArray.length;
	    for(int j=0;j<lenOld;j++)
	    {
	         	if(!(Arrays.asList(newArray).contains(oldArray[j])))
	    	{
	    	  delPkValues.add(tablePkArray[j]); 
	    	}
	    }
		  
	  }

  counter++;	   
}

String [] pkDelUserSiteGrp = (String [])delPkValues.toArray(new String[delPkValues.size()]);
int ret = -1;
ret = userSiteJB.updateUserSiteGroup(usr,pkUserSIte,groupPk,pkDelUserSiteGrp,ipAdd,userIdSess);


%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 
<jsp:include page="include.jsp" flush="true"/>

<%if(ret==0){%>
<div class="defcomments">
<%=MC.M_Changes_SavedSucc %>
</div>
<script>
setTimeout("self.close()",1000); //wait for sometime for showing  msg then close
</script>	
<%-- <META HTTP-EQUIV=Refresh CONTENT="2; URL=manageUserSitesGrps.jsp?userId=<%=usr%>" >--%>
<%}else{%>
<div class=error>
<%=MC.M_Changes_CntSvd %>
</div>	
<%}%>




<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

<%}
} else {  //else of if body for session

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>

</div>
</body>
</html>
