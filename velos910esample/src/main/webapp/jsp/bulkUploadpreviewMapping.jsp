<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title>View Bulk Field Mapping</title>
<script>
</script>
</head>
    <%@ page autoFlush="true" buffer="1094kb"%>   
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="com.velos.eres.bulkupload.business.*,java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page  language = "java" import="com.velos.eres.bulkupload.business.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<body onload="clearcon();">
  <%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
String mapPk="";
String temName="";
temName=request.getParameter("temName");
temName=temName.replace("*****"," ").trim();
mapPk=request.getParameter("mapPk");
ArrayList velosFieldName=null;
ArrayList fileFieldName=null;
String userId="";
userId = (String) tSession.getValue("userId");
BulkTemplateDao bulkTemDao=new BulkTemplateDao();
bulkTemDao.getMappingNames(userId,StringUtil.stringToNum(mapPk));
velosFieldName=bulkTemDao.getVelosFieldName();
fileFieldName=bulkTemDao.getFileFldName();	
%> 
	 <table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0">
	 
	 <%if(!velosFieldName.isEmpty()){
	 %>
	 <tr class="tmpHeight"><td><font size="3"><b>View Mapping:<%=temName%></b></font><br/></td> </tr>
		  <tr class="tmpHeight">
		<table class="basetbl outline midAlign lhsFont" width="50%" cellspacing="0" cellpadding="0" border="1" >
	 		<tr>
		<th>Velos eSample Field Name</th>
		<th>File Field Name</th>
		</tr>
	   <%for (int errSize=0;errSize<velosFieldName.size();errSize++)  
	   {
	   %>
		<tr class="tmpHeight">
	     <td><%=velosFieldName.get(errSize)%><FONT class="Mandatory">*</FONT> &nbsp;</td>
		 <td><%=fileFieldName.get(errSize)%></td>
         </tr>
	  <%}%>
	  </table>
	   <%}%>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <% 
}
%>

</body>
</html>