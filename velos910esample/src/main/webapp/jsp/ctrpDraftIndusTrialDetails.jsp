<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<script>
$j(document).ready(function(){
	studyPurposeAction();
	studyPhaseAction();
	studyTypeAction();
});
var studyPhaseNA = <s:property escape="false" value="ctrpDraftJB.draftStudyPhaseNA" />;
var studyPurposeOther =<s:property escape="false" value="ctrpDraftJB.studyPurposeOtherPk" />;
var studyType = '<s:property escape="false" value="ctrpDraftJB.draftStudyType" />';
function studyPhaseAction() {
	if (!$('ctrpDraftJB.ctrpStudyPhase')) { return; }
	if (!$('ctrp_draft_pilot_row')) { return; }
	if ($('ctrpDraftJB.ctrpStudyPhase').options[$('ctrpDraftJB.ctrpStudyPhase').selectedIndex].value == studyPhaseNA) {
		$('ctrp_draft_pilot_row').style.visibility = 'visible';
	} else {
		$('ctrp_draft_pilot_row').style.visibility = 'hidden';
	}
}
function studyPurposeAction() {
	if (!$('ctrpDraftJB.studyPurpose')) { return; }
	if (!$('ctrp_draft_purpose_other_row')) { return; }
	if ($('ctrpDraftJB.studyPurpose').options[$('ctrpDraftJB.studyPurpose').selectedIndex].value == studyPurposeOther) {
		$('ctrp_draft_purpose_other_row').style.visibility = 'visible';
		$('study_purpose_other_asterisk').style.visibility = 'visible';
	} else {
		$('ctrp_draft_purpose_other_row').style.visibility = 'hidden';
		$('study_purpose_other_asterisk').style.visibility = 'hidden';
	}
}
function studyTypeAction(){
	if(studyType=='0')
	{
		$j('#ctrpDraftIndustrialForm_ctrpDraftJB_draftStudyType0').attr('checked',true);
	}else{
		$j('#ctrpDraftIndustrialForm_ctrpDraftJB_draftStudyType1').attr('checked',true);
	}
}
</script>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td width="50%">
    	<table border="0" width="50%">
    		<tr>
    			<td align="right" valign="top" width="30%" ><%=LC.L_Title%></td>
    			<td width="2%"></td>
    			<td valign="top" align="left" height="22px"> 
					<s:textfield name="ctrpDraftJB.studyTitle" id="ctrpDraftJB.studyTitle" style="width:100%; background-color:#F7F7F7; border:0px;" readonly="true" size="70" /> 
				</td>
    		</tr>
    		<tr>
    			<td colspan="3" align="center" valign="middle" height="22px"><div class="defComments"><%=MC.CTRP_DraftSelectValFrmVelosEres %></div></td>
    		</tr>
    		<tr>
    			<td align="right" width="30%"  height="22px">
    				<%=LC.L_Phase%>&nbsp;<span id="ctrpDraftJB.ctrpStudyPhase_error" class="errorMessage" style="display:none;"></span>
    			</td>
    			<td width="2%"><FONT id="draft_study_phase_menu_asterisk" style="visibility:visible" class="Mandatory">* </FONT></td>
    			<td align="left" height="22px"> 
					<s:property escape="false" value="ctrpDraftJB.draftStudyPhaseMenu" />
				</td>
    		</tr>
    		<tr id="ctrp_draft_pilot_row" style="visibility:hidden">
				<td align="right"   height="22px"><%=LC.CTRP_DraftPilotTrial %></td>
				<td> <FONT id="draft_pilot_asterisk" class="Mandatory">*</FONT></td>
				<td align="left" height="22px">
					<s:select name="ctrpDraftJB.draftPilot" id="ctrpDraftJB.draftPilot" list="ctrpDraftJB.draftPilotList" 
						listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.draftPilot" />
				</td>
			</tr>
			<tr>
				<td align="right"   height="22px"><%=LC.CTRP_DraftTrialType %><span id="ctrpDraftJB.draftStudyType_error" class="errorMessage" style="display:none;"></span></td>
				<td ><FONT id="draft_study_type_asterisk" style="visibility:visible" class="Mandatory">*</FONT></td>
				<td align="left" height="22px">
					<s:radio name="ctrpDraftJB.draftStudyType" list="ctrpDraftJB.draftStudyTypeList" listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.draftStudyType" />
				</td>
			</tr>
			<tr>
				<td align="right"   height="22px"><%=LC.CTRP_DraftIntervenType %><span id="ctrpDraftJB.ctrpIntvnType_error" class="errorMessage" style="display:none;"></span></td>
				<td ></td>
				<td align="left" height="22px">
				 <s:property escape="false" value="ctrpDraftJB.ctrpIntvnTypeMenu" ></s:property>  </td>
			</tr>
			<tr>
				<td align="right"   height="22px"><%=LC.CTRP_DraftIntervenName %><span id="ctrpDraftJB.interventionName_error" class="errorMessage" style="display:none;"></span></td>
				<td></td>
				<td align="left" height="22px"> <s:textfield name="ctrpDraftJB.interventionName" size="50"></s:textfield>  </td>
			</tr>
			<tr>
				<td align="right"  valign="top"  height="22px"><%=LC.CTRP_DraftDiseaseName %><span id="ctrpDraftJB.studyDiseaseSite_error" class="errorMessage" style="display:none;"></span></td>
				<td></td>
				<td align="left"  valign="top" height="22px">
				<s:textfield name="ctrpDraftJB.studyDiseaseName" readonly="true" size="50"></s:textfield>
				<s:hidden  name="ctrpDraftJB.studyDiseaseSite" id="ctrpDraftJB.studyDiseaseSite"/></td>
			</tr>
			<tr>
				<td align="right"   height="22px"><%=LC.CTRP_DraftPurpose %><span id="ctrpDraftJB.studyPurpose_error" class="errorMessage" style="display:none;"></span></td>
				<td ><FONT id="study_purpose_menu_asterisk" style="visibility:visible" class="Mandatory">* </FONT></td>
				<td align="left" height="22px"><s:property escape="false" value="ctrpDraftJB.studyPurposeMenu" /></td>
			</tr>
			<tr id="ctrp_draft_purpose_other_row" style="visibility:hidden">
				<td align="right"   height="22px" valign="top">
					<%=LC.CTRP_DraftPurposeOther %>&nbsp;
				<span id="ctrpDraftJB.studyPurposeOther_error" class="errorMessage" style="display:none;"></span>
				</td>
				<td  valign="top"><FONT id="study_purpose_other_asterisk" style="visibility:hidden" class="Mandatory">* </FONT> </td>
				<td align="left" height="22px">
				<s:textarea name="ctrpDraftJB.studyPurposeOther" id="ctrpDraftJB.studyPurposeOther" cols="30" rows="3" />	
				</td>
			</tr>
    	</table>
    </td>
    <td width="50%">
    	<table border="0" style="vertical-align: top;" >
    	
    	<tr>
    		<td height="22px" align="right"><%=LC.CTRP_DraftPhaseColon %></td>
    		<td width="2%" height="22px"></td>
    		<td align="left" height="22px"><s:textfield name="ctrpDraftJB.studyPhase" style="background-color:#F7F7F7; border:0px;" readonly="true" /></td>
    	</tr>
    	<tr>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    	</tr>
    	<tr>
    		<td height="22px" align="right"><%=LC.CTRP_DraftTrialTypeColon %>&nbsp;</td>
			<td height="22px"></td>
			<td align="left" height="22px"><s:textfield name="ctrpDraftJB.studyType" style="background-color:#F7F7F7; border:0px;" readonly="true" /></td>
    	</tr>
    	<tr>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    	</tr>
    	<tr>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    	</tr>
    	<tr>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    	</tr>
    	<tr>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    	</tr>
    	<tr>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    		<td height="22px">&nbsp;</td>
    	</tr>
    	</table>
    </td>
  </tr>
</table>