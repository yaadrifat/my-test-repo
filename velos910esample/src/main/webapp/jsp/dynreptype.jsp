<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=LC.L_Adhoc_QueryCreation%><%--Ad-Hoc Query Creation*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
	<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>
window.name = "dynrep";
var formWin = null;
var formLkp="lookup";  
 function  validate(formobj,act){
	formId=formobj.formId.value;
	prevId=formobj.prevId.value;
	if (act!="dynreptype.jsp"){
	if (formId.length==0){
	alert("<%=MC.M_PlsSel_LineProc%>");/*alert("Please select a form to proceed.");*****/
	return false;
	}
	if (formobj.dynType.value=="P"){
	 if (formobj.patselect[0].checked){
	 formobj.patId.value="";
	 formobj.patPk.value="";
	 formobj.study1.value="";
	 } else if (formobj.patselect[1].checked){
	 formobj.patId.value="";
	 formobj.patPk.value="";
	 } else if (formobj.patselect[2].checked){
	 formobj.study1.value="";
	 }
	 } 
	 else if (formobj.dynType.value=="S"){
	  if (formobj.studyselect[0].checked){
	  formobj.study1.value="";
	 }  
	}
	
	
	} else {
	
	if (formobj.dynType.value=="S")
	{
	
	formobj.study1.value="";
	formobj.patId.value="";
	formobj.patPk.value="";
	formobj.formId.value="";
	formobj.prevId.value="";
	formobj.formName.value="";
	}else if (formobj.dynType.value=="P"){
	formobj.study1.value="";
	formobj.formId.value="";
	formobj.prevId.value="";
	formobj.formName.value="";
	
	 }
	}
	/*if (formId==prevId)
	{}
	else
	{
	alert("Please Select the Fields for Selected Form.");
	return false;
	}*/
     /*if (!(validate_col('eSign',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {

	alert("Incorrect E-Signature. Please enter again");

	formobj.eSign.focus();
	
	return false;
   }*/
formobj.action=act;
formobj.submit();
//formobj.target="";
}
function openFormWin(formobj,act) {
formId=formobj.formId.value;
prevId=formobj.prevId.value;
//act="dynpreview.jsp";

if (formId==prevId)
{}
else
{
alert("<%=MC.M_SelFlds_ForFrm%>");/*alert("Please Select the Fields for Selected Form.");*****/
return false;
}
formobj.target="formWin";
formobj.action=act;
formWin = open('','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynreport.submit();
void(0);
}
function resetVals(formobj,mode){
if (formobj.repMode.value=="N"){
prevSelect=formobj.select.value;
if (formobj.dynType.value=="P"){
if (formobj.patselect[0].checked) selected="0";
if (formobj.patselect[1].checked) selected="1";
if (formobj.patselect[2].checked) selected="2";
if (mode=="All"){
if ((formobj.patId.value.length>0) || (formobj.study1.value.length>0)){
if (confirm("<%=MC.M_OptWillReset_WantCont%>")){/*if (confirm("Other options will be reset. Do you want to continue?")){*****/
formobj.patId.value="";
formobj.patPk.value="";
formobj.study1.value="";
formobj.formId.value="";
formobj.formName.value="";
formobj.prevId.value="";
} else {
if (prevSelect=="0") formobj.patselect[0].checked=true;
if (prevSelect=="1") formobj.patselect[1].checked=true;
if (prevSelect=="2") formobj.patselect[2].checked=true;

return false;
}  
}
}else if (mode=="pat"){
if ((formobj.study1.value.length>0) || (formobj.formId.value.length>0) ){
	if (confirm("<%=MC.M_OptWillReset_WantCont%>")){/*if (confirm("Other options will be reset. Do you want to continue?")){*****/
formobj.study1.value="";
formobj.formId.value="";
formobj.formName.value="";
formobj.prevId.value="";
} else {
if (prevSelect=="0") formobj.patselect[0].checked=true;
if (prevSelect=="1") formobj.patselect[1].checked=true;
if (prevSelect=="2") formobj.patselect[2].checked=true;
return false;
}  
}
} else if (mode=="study"){
if ((formobj.patId.value.length>0) ||  (formobj.formId.value.length>0)){
	if (confirm("<%=MC.M_OptWillReset_WantCont%>")){/*if (confirm("Other options will be reset. Do you want to continue?")){*****/
formobj.patId.value="";
formobj.patPk.value="";
formobj.formId.value="";
formobj.formName.value="";
formobj.prevId.value="";

} else {
if (prevSelect=="0") formobj.patselect[0].checked=true;
if (prevSelect=="1") formobj.patselect[1].checked=true;
if (prevSelect=="2") formobj.patselect[2].checked=true;

return false;
}  
}

}
} else if (formobj.dynType.value=="S"){

if (formobj.studyselect[0].checked) selected="0";
if (formobj.studyselect[1].checked) selected="1";

if (mode=="study"){
if (formobj.formId.value.length>0){
	if (confirm("<%=MC.M_OptWillReset_WantCont%>")){/*if (confirm("Other options will be reset. Do you want to continue?")){*****/
formobj.formId.value="";
formobj.formName.value="";
formobj.prevId.value="";
}else {
if (prevSelect=="0") formobj.studyselect[0].checked=true;
if (prevSelect=="1") formobj.studyselect[1].checked=true;
return false;
}
}
} if (mode=="All"){
if (formobj.study1.value.length>0){
	if (confirm("<%=MC.M_OptWillReset_WantCont%>")){/*if (confirm("Other options will be reset. Do you want to continue?")){*****/
formobj.formId.value="";
formobj.study1.value="";
formobj.formName.value="";
formobj.prevId.value="";
}else{
if (prevSelect=="0") formobj.studyselect[0].checked=true;
if (prevSelect=="1") formobj.studyselect[1].checked=true;
return false;
}
}
}


}
formobj.select.value=selected;
}
}
function openLookup(formobj){
accId=formobj.accId.value;
userId=formobj.userId.value;
studyId=formobj.study1.value;
type=formobj.dynType.value;
var filter="";
//studyflg=formobj.studyselect[0].checked;
if (type=="S"){
	if (formobj.studyselect[0].checked){	
	studyList=formobj.studyList.value;	
/*   filter=" (b.Fk_study in ( "+ studyList + ") or (b.fk_account="+accId+" and b.lf_displaytype='SA'))  and  b.FK_FORMLIB=a.pk_formlib and c.FK_FORMLIB=a.pk_formlib and " +  
 	 "  b.record_type <> 'D'  and c.formstat_enddate IS NULL and c.fk_codelst_stat in " + 
	 "  (select pk_codelst from er_codelst where lower(codelst_subtyp)='a' and lower(codelst_type)='frmstat') ";*/
//********************************start here*********************************************//
filter=  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
"((fk_study is null) or (fk_study in (select fk_study from er_studyteam where fk_user="+userId+"))) and "+
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
"b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
" AND ((LF_DISPLAYTYPE = 'SA' )) union " +
"select PK_FORMLIB,FORM_NAME,FORM_DESC "+
" from er_formlib a,er_linkedforms b,er_formstat c " +
 "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
" AND ((LF_DISPLAYTYPE = 'SA' )) " ; 
//******************************endhere**************************************************//
	 
}
else{
if (studyId.length==0){
alert("<%=MC.M_PlsSel_StdFirst%>");/*alert("Please Select a Study First");*****/
formobj.study1.focus();
return;
 }
 /*filter=" b.Fk_study= "+ studyId + "  and  b.FK_FORMLIB=a.pk_formlib and c.FK_FORMLIB=a.pk_formlib and " +  
 	 "  b.record_type <> 'D'  and c.formstat_enddate IS NULL and c.fk_codelst_stat in " + 
	 " (select pk_codelst from er_codelst where lower(codelst_subtyp)='a' and lower(codelst_type)='frmstat') ";*/
	 //********************************start here*********************************************//
filter=  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
"((fk_study="+ studyId + " and LF_DISPLAYTYPE = 'S') or (LF_DISPLAYTYPE = 'SA' ) ) and "+
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
"b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
" union "+
"select PK_FORMLIB,FORM_NAME,FORM_DESC "+
" from er_formlib a,er_linkedforms b,er_formstat c " +
 "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
"((fk_study="+ studyId + " and LF_DISPLAYTYPE = 'S') or (LF_DISPLAYTYPE = 'SA' ) ) and "+ 
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
" AND ((LF_DISPLAYTYPE = 'S' ) or (LF_DISPLAYTYPE = 'SA' )) " ; 
//******************************endhere**************************************************//
}
} else if (type=="P"){
if (formobj.patselect[0].checked){
	studyList=formobj.studyList.value;	
  /* filter=" ((b.fk_account="+accId+" and b.lf_displaytype='PA'))  and  b.FK_FORMLIB=a.pk_formlib and c.FK_FORMLIB=a.pk_formlib and " +  
 	 "  b.record_type <> 'D'  and c.formstat_enddate IS NULL and c.fk_codelst_stat in " + 
	 "  (select pk_codelst from er_codelst where lower(codelst_subtyp)='a' and lower(codelst_type)='frmstat') ";*/
//********************************start here*********************************************//
filter=  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
"((fk_study is null) or (fk_study in (select fk_study from er_studyteam where fk_user="+userId+"))) and "+
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
" b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
" AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS' )) union " +
"select PK_FORMLIB,FORM_NAME,FORM_DESC "+
" from er_formlib a,er_linkedforms b,er_formstat c " +
 "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
" AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS' )) " ; 
//******************************endhere**************************************************//	 

}
if (formobj.patselect[1].checked){
if (studyId.length==0){
	alert("<%=MC.M_PlsSel_StdFirst%>");/*alert("Please Select a Study First");*****/
formobj.study1.focus();
return;
 }
 /*filter=" (b.Fk_study= "+ studyId + "and (b.fk_account="+accId+" and b.lf_displaytype='SP')) and  b.FK_FORMLIB=a.pk_formlib and c.FK_FORMLIB=a.pk_formlib and " +  
 	 "  b.record_type <> 'D'  and c.formstat_enddate IS NULL and c.fk_codelst_stat in " + 
	 " (select pk_codelst from er_codelst where lower(codelst_subtyp)='a' and lower(codelst_type)='frmstat') ";*/
//********************************start here*********************************************//
filter=  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
"((fk_study="+ studyId + " and LF_DISPLAYTYPE = 'SP' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PA' ) ) and "+
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
" b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
"   union "+
"select PK_FORMLIB,FORM_NAME,FORM_DESC "+
" from er_formlib a,er_linkedforms b,er_formstat c " +
 "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
"((fk_study="+ studyId + " and LF_DISPLAYTYPE = 'SP' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PA' ) ) and "+ 
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) " ; 
//******************************endhere**************************************************//	 
 
}
if (formobj.patselect[2].checked){
if (formobj.patId.value.length==0){
alert("<%=MC.M_PlsSel_PatProc%>");/*alert("Please Select a <%=LC.Pat_Patient%> to proceed");*****/
formobj.patId.focus();
return;
 }
 patId=formobj.patPk.value;
 //********************************start here*********************************************//
filter=  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
"((fk_study is null) or (fk_study in (select fk_study from er_patprot where fk_per="+patId+" and patprot_stat=1))) and "+
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
"b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
"b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
" AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS') or (LF_DISPLAYTYPE = 'SP') ) " +
"  union " +
" select PK_FORMLIB,FORM_NAME,FORM_DESC "+
" from er_formlib a,er_linkedforms b,er_formstat c " +
 "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D' and "+
"((fk_study is null) or (fk_study in (select fk_study from er_patprot where fk_per="+patId+" and patprot_stat=1))) and "+ 
"((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
"   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
" and (c.formstat_enddate is null) and  "+
" (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
" or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
" AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS') or (LF_DISPLAYTYPE = 'SP')) "  ; 
//******************************endhere**************************************************//
}

}
//filter1=" F2.FK_ACCOUNT ="+ accId + " AND F2.FK_FORMLIB = F1.PK_FORMLIB and " +   
//" F2.record_type <> 'D'   AND F4.FK_FORMLIB = F1.PK_FORMLIB   and F4.formstat_enddate IS NULL " +  
// "  and lf_displaytype IN ( 'S','SP','A','SA','PA' )  and F4.fk_codelst_stat in (select pk_codelst from er_codelst where lower(codelst_desc)='active') " ;
    //alert(filter);
    formobj.dfilter.value=filter;
 formobj.target='formLkp';   
formobj.action="getlookup.jsp?viewId=&viewName=Form Browser&form=dynreport&keyword=formName|VELFORMNAME~formId|VELFORMPK&filter=[VELGET:dfilter]"; 
windowname=window.open('' ,'formLkp',"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
formobj.submit();
formobj.action="dynreptype.jsp";
formobj.target="";
windowname.focus();	

}
function selectFlds(formobj){
formId=formobj.formId.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
from=formobj.from.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter+"&from="+from ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, "); 
windowname.focus();
}
function setSeqFlag(formobj){
formobj.seqChg.value="Y";

}
function openpatwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=&form=dynreport&dispFld=patId&dataFld=patPk","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
   windowName.focus();
}

</SCRIPT>
<% String src,tempStr="",fldName="",fldCol="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");


%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>
<DIV class="formDefault" id="div1"> 
<%
        String formId="",formName="",formType="",fldType="",repIdStr="",fldDisp="",fldWidth="",fldId="",select="0";
	String repName="",repHdr="",repFtr="",repType="",repFilter="",selStudy="",selPat="",dynType="",repMode="",patCode="";
	String dynTypeSess="",dynTypeReq="";
	HashMap TypeAttr=new HashMap();
	int seq=0,repId=0,pkey=0;
	ArrayList repColId=new ArrayList();
	ArrayList repCol=new ArrayList();
	ArrayList repColSeq=new ArrayList();
	ArrayList repColWidth=new ArrayList();
	ArrayList repColFormat=new ArrayList();
	ArrayList repColDispName=new ArrayList();
	ArrayList repColName=new ArrayList();
	ArrayList repColType=new ArrayList();
	DynRepDao dyndao=new DynRepDao();
	HashMap attributes = new HashMap();
	HashMap RepAttr = new HashMap();
	
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	String mode = request.getParameter("mode");
	mode=(mode==null)?"":mode;
	 if (mode.equals("M")){
	repIdStr=request.getParameter("repId");
	if (repIdStr==null) repIdStr="";
	if (repIdStr.length() > 0)
	repId=(new Integer(repIdStr)).intValue();
	dyndao=dynrepB.populateHash(repIdStr);
	attributes=dyndao.getAttributes();
	
	tSession.setAttribute("attributes",attributes);
	
	if (attributes.containsKey("RepAttr")){
	RepAttr=(HashMap)attributes.get("RepAttr");
	RepAttr.put("repId",repIdStr);
	attributes.put("RepAttr",RepAttr);
	}
	} else if (mode.equals("N")){
	if (tSession.getAttribute("attributes")!=null) tSession.removeAttribute("attributes");
	TypeAttr.put("repMode","N");
	attributes.put("TypeAttr",TypeAttr);
	}
	if ((tSession.getAttribute("attributes"))==null)
	{
	tSession.setAttribute("attributes",attributes);
	
	}
	attributes=(HashMap)tSession.getAttribute("attributes");
	if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	
	//dynType
	dynType=request.getParameter("dynType");
	dynTypeReq=dynType;
	dynTypeReq=(dynTypeReq==null)?"":dynTypeReq.trim();
	if (TypeAttr.containsKey("dynType")) dynTypeSess=(String)TypeAttr.get("dynType");
	dynTypeSess=(dynTypeSess==null)?"":dynTypeSess.trim();
	
	if (dynType==null) dynType="";
	if (dynType.length()==0) {
	if (TypeAttr.containsKey("dynType")) dynType=(String)TypeAttr.get("dynType");
	if (dynType==null) dynType="";
	}
	if (dynType.length()==0) dynType="P";
	
	//study
	if (dynTypeSess.equals(dynTypeReq)){
	selStudy=request.getParameter("study1");
	if (selStudy==null) selStudy="";
	if (selStudy.length()==0) {
	if (TypeAttr.containsKey("studyId")) selStudy=(String)TypeAttr.get("studyId");
	if (selStudy==null) selStudy="";
	}
	} else 
	{
	selStudy="";
	}
	//Patient
	selPat=request.getParameter("patId");
	if (selPat==null) selPat="";
	if (selPat.length()==0) {
	if (TypeAttr.containsKey("perId")) selPat=(String)TypeAttr.get("perId");
	if (selPat==null) selPat="";
	if (selPat.length()>0){
	pkey=EJBUtil.stringToNum(selPat);
	person.setPersonPKId(pkey);
	person.getPersonDetails();
	patCode = person.getPersonPId();
	}
	}
	
	//form
	if (dynTypeSess.equals(dynTypeReq)){
	formId=request.getParameter("formId");
	if (formId==null) formId="";
	if (formId.length()==0) {
	if (TypeAttr.containsKey("formId")) formId= (String)TypeAttr.get("formId");
	if (formId==null) formId="";
	}
	} else {
	formId="";
	}
	//retrieve the formName
	formlibB.setFormLibId(EJBUtil.stringToNum(formId));
	formlibB.getFormLibDetails();
	formName=formlibB.getFormLibName();
	TypeAttr.put("formName",formName);
	
	//Rep
	repMode=request.getParameter("mode");
	if (repMode==null) repMode="";
	if (repMode.length()==0) {
	if (TypeAttr.containsKey("repMode")) repMode=(String)TypeAttr.get("repMode");
	}
	 if ((TypeAttr.containsKey("repMode")) && (TypeAttr.get("repMode")!=null)){
	 }else {	 
	  TypeAttr.put("repMode",repMode);
	  }
	repMode=(String)TypeAttr.get("repMode");
	repMode=(repMode==null)?"":repMode;
	String accId = (String) tSession.getValue("accountId");
	String userId=	(String) tSession.getValue("userId");
	String userIdFromSession = (String) tSession.getValue("userId");
	 if( formName==null) formName="";
	 if( formId==null) formId="";
	 int counter=0;
	int studyId=0;
	String studyList="";		
	StringBuffer study=new StringBuffer();	
	//StudyDao studyDao = new StudyDao();	
	//studyDao.getReportStudyValuesForUsers(userId);
	// this is to reset all the values in case user comes back from previous page. since values are already in session and populates from sess
	//to control that user does not select wrong combination for forms and study/patient selection
	
	 /*if ((dynTypeSess.trim()).equals(dynTypeReq.trim())){}else{
	 System.out.println("FynTypeBOTH"+dynTypeSess.trim()+" ADD "+dynTypeReq.trim());
	   formId="";
	     formName="";
	     selStudy="";
	   
	 }*/
	 StudyDao studyDao = studyB.getUserStudies(userId, "dummy",0,"");
	 study.append("<SELECT NAME=study1>") ;
	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();
		if (studyList.length()==0) 
		   studyList=studyList+studyId;
		   else 
		   studyList=studyList+","+studyId;
		 if (selStudy.length()>0){
		   if (studyId==((new Integer(selStudy)).intValue()))
		study.append("<OPTION value = "+ studyId +" selected>" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
		else 	
		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
	} else{
	study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
	}
	}
	study.append("</SELECT>");
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FldAttr")) System.out.println("I am here-FldAttr"+attributes.get("FldAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("TypeAttr")) System.out.println("I am here-TypeAttr"+attributes.get("TypeAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FltrAttr")) System.out.println("I am here-FltrAttr"+attributes.get("FltrAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("SortAttr")) System.out.println("I am here-SortAttr"+attributes.get("SortAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("RepAttr")) System.out.println("I am here-RepAttr"+attributes.get("RepAttr"));
	System.out.println("*****************************************************************************************");
	
	 
%>

<P class="sectionHeadings"> <%=MC.M_AdhocQry_Creat%><%--*Ad-Hoc Query >> Create****--%> </P>
<br><br>
<P  class="redMessage"><%=MC.M_RptCreatedPrev_ChgCntSave%><%--This report has been created in a previous version of Ad-Hoc Query. 
	You can make changes to 'Preview' but can not save them.*****--%> </P>
<%
%>

<form name="dynreport"  METHOD="POST">
<table><tr>
<%if (attributes.get("TypeAttr")!=null){%>
<td><A href="dynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><font color="red"><b><%=LC.L_Select_RptType%><%--Select Report Type*****--%></b></font></A></td>
<%}else{%>
<td><p class="sectionHeadings"><font color="red"><b><%=LC.L_Select_RptType%><%--Select Report Type*****--%></b></font></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FldAttr")!=null){%>
<td><A href="dynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_Flds%><%--Select Fields*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FltrAttr")!=null){%>
<td><A href="dynfilter.jsp?srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("SortAttr")!=null){%>
<td><A href="dynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("RepAttr")!=null){%>
<td><A href="dynrep.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></p></td>
<%}%>
</tr></table>
<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="repMode" value=<%=repMode%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formId" readonly value="<%=formId%>">
<input type="hidden" name="studyList" readonly value="<%=studyList%>">
<input type="hidden" name="dfilter" readonly value="">
<%if (mode.equals("M")){%>
<input type="hidden" name="formType" readonly value="<%=repType%>">
<%}else{%> 
<input type="hidden" name="formType" readonly value="<%=formType%>">
<%}%>
<input type="hidden" name="prevId" readonly value="<%=formId%>">
<input type="hidden" name="seqChg" readonly value="">
<input type="hidden" name="from" readonly value="pat">
<input type="hidden" name="repId" readonly value="<%=repId%>">
<P class="sectionHeadings"> <%=LC.L_Select_RptType%><%--Select Report Type*****--%> </P>
<table width="100%"><tr><td width="15%"><%=LC.L_Rpt_Type%><%--Report Type*****--%></td>
<td width="50%">
<%if (repMode.equals("M")){%>
<select size="1" name="dynType" disabled onChange="return validate(document.dynreport,'dynreptype.jsp');">
<%}else{%>
<select size="1" name="dynType" onChange="return validate(document.dynreport,'dynreptype.jsp');">
<%}%>
<option selected value="S" <%if (dynType.equals("S")){%>selected<%}%> ><%=LC.L_Study%><%--Study*****--%></option>
<option value="P" <%if (dynType.equals("P")){%>selected<%}%>><%=LC.L_Patient%><%--<%=LC.Pat_Patient%>*****--%></option>
</select></td>
<%if (repMode.equals("N")){%>
<td></td>
<!--<td><A href="#" onClick="return validate(document.dynreport,'dynreptype.jsp');"><img src="../images/jpg/Go.gif" align="absmiddle" border="0"></A></td>-->
<%}else{%>
<td></td>
<%}%>
</tr></table>
 <%if (dynType.equals("P")){%>
<P class="sectionHeadings"> <%=LC.L_SelPat_Popu%><%--Select <%=LC.Pat_Patient%> Population*****--%> </P>
<table width="100%">
  <tr><td><input name="patselect" type="radio"  value="0" checked onClick="return resetVals(document.dynreport,'All')" <%if (repMode.equals("M")){%>disabled<%}%>><%=LC.L_All_Patients%><%--All <%=LC.Pat_Patients%>*****--%></td></tr>
   <tr><td width="15%">
   <%if (selStudy.length()>0){
   select="1";
   
   %>
     <input name="patselect" type="radio"  value="1" checked onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
   <%}else{%>
   <input name="patselect" type="radio"  value="1" onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
   <%}%>
   <%=MC.M_PatOn_Std%><%--<%=LC.Pat_Patients%> on a Study*****--%></td><td width="50%"><%=LC.L_Select_Study%><%--Select Study*****--%>&nbsp;<%=study%></td></tr>
  <tr><td width="15%">
  <%if (selPat.length()>0){
  select="2";
  
  %>
  <input name="patselect" type="radio"  value="2" checked onClick="return resetVals(document.dynreport,'pat')" <%if (repMode.equals("M")){%>disabled<%}%>>
  <%}else {%>
  <input name="patselect" type="radio"  value="2" onClick="return resetVals(document.dynreport,'pat')" <%if (repMode.equals("M")){%>disabled<%}%>>
  <%}%>
  <%=LC.L_Specific_Patient%><%--Specific <%=LC.Pat_Patient%>*****--%></td><td width="50%">
  <%if (repMode.equals("N")){%>
  <A href="#" onClick="openpatwindow()"><%=LC.L_Select_APat%><%--Select a <%=LC.Pat_Patient%>*****--%></A>&nbsp;
  <input type="text" name="patId" readonly value=<%=patCode%>></td></tr>
  <%}else{%>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
  <input type="text" name="patId" readonly value=<%=patCode%>></td></tr>
  <%}%>
  
  <input type="hidden" name="patPk" readonly value=<%=selPat%>>
    </table>
    <%}else if (dynType.equals("S")){ %>
    <P class="sectionHeadings"> <%=LC.L_Select_StdGrp%><%--Select Study Group*****--%> </P>
<table width="100%" >
  <tr><td><input name="studyselect" type="radio"  value="1" checked onClick="return resetVals(document.dynreport,'All')" <%if (repMode.equals("M")){%>disabled<%}%>><%=LC.L_All_Studies%><%--All Studies*****--%></td></tr>	
    <tr><td width="15%">
    <%if (selStudy.length()>0){
    select="1";
    
    %>
    <input name="studyselect" type="radio"  value="2" checked onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
    <%}else{%>
    <input name="studyselect" type="radio" value="2" onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
    <%}%>
    <%=LC.L_Specific_Study%><%--Specific Study*****--%></td><td width="50%"><%=study%></td></tr>
    </table>
    <%}%>
    <br>
    <P class="sectionHeadings"> <%=LC.L_Select_Form%><%--Select Form*****--%> </P>
    <table width="100%">
    <tr><td width="15%"><%=LC.L_Frm_Name%><%--Form Name*****--%><FONT class="Mandatory">* </FONT></td><td width="50%"><input type="text" name="formName" readonly value="<%=formName%>" size="35" >
    <%if (repMode.equals("N")){%>
    <A href="#" onClick="openLookup(document.dynreport)"><%=LC.L_Select%><%--Select*****--%></A>
    <%}%>
    </td><td></td></tr>
    </table>
     <input type="hidden" name="select" value="<%=select%>">  
    <br><br>
    <table width="100%" cellspacing="0" cellpadding="0">
      <td align=center> 
	   
	<!--	<A href="" type="submit"><%=LC.L_Back%></A>-->
	  	  	
      </td> 

      <td width="40%"></td><td> 
		<button type="submit" onClick ="return validate(document.dynreport,'dynselectflds.jsp?mode=next');"><%=LC.L_Next%></button>
		<!--<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>-->
      </td> 
      </tr>
  </table>
    
    
    <input type="hidden" name="sess" value="keep">
    

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
  </div>


</body>
</html>
