<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>	

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.account.AccountJB,com.velos.esch.business.common.*"%>
<%

 HttpSession tSession = request.getSession(true); 
 String accId = (String) tSession.getValue("accountId");
 int iaccId = EJBUtil.stringToNum(accId);
 String studyId = (String) tSession.getValue("studyId");
 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");
 int iusr = EJBUtil.stringToNum(usr);

 String src= request.getParameter("srcmenu");

 String selectedTab = request.getParameter("selectedTab");
 String eSign = request.getParameter("eSign");
 int success = 0;
 if (sessionmaint.isValidSession(tSession))
 {	
	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
    String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	    System.out.println("tot="+request.getParameter("totalBoardCount"));
	    System.out.println("To0="+request.getParameter("submitTo0"));
	    System.out.println("To1="+request.getParameter("submitTo1"));
        if(success >=0 )
        {
%>
  <br>
  <Br>
  <br>
  <Br>
  <p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
  <%
  boolean isIrb = "irb_init_tab".equals(selectedTab) ? true : false; 
  String nextUrl = isIrb ? "irbnewinit.jsp" : "study.jsp";
  %>
  
  <META HTTP-EQUIV=Refresh CONTENT="1; URL=<%=nextUrl%>?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>">
<%
        }// end of if body for success
    }// end of if body for e-sign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>
</HTML>
