<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=LC.L_MngInv_Spmen%><%--Manage Inventory >> Specimens*****--%></title>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT language="javascript">

var sCode = "";
var timeOfLastKeyPress ;
var interval = 100;

function fchange(intKeyCode,formobj)
{
	sCode = sCode + String.fromCharCode(intKeyCode);
    //timeOfLastKeyPress = now();
    setTimeout("onTick()", 1000);
 }


function onTick() {

  	  if (sCode!= null && sCode.length > 0)
  	  {
  	  	  sCode="";
    	document.specimen.submit();
      }
}

//JM: 14Nov2007: added study look up
function openStudyWindow(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6013&form=specimen&seperator=,"+
                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="specimenbrowser.jsp";
	void(0);
}

function openUserWindow(frm) {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
}


function openwindow(formobj,pageRight ){

     if (! f_check_perm(pageRight,'N') )
     {
      return false;
     }

	  windowName =window.open('addmultiplespecimens.jsp?srcmenu=tdmenubaritem6&selectedTab=1' , 'Information', 'toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=450, top=100, left=90');
	  windowName.focus();

}

function setOrder(formObj,orderBy,pgRight)
{
	var lorderType;
	if (formObj.orderType.value=="asc" || formObj.orderType.value=="asc, pk_specimen asc") {
		formObj.orderType.value= "desc, pk_specimen desc";
		lorderType="desc, pk_specimen desc";

	} else 	if (formObj.orderType.value=="desc" || formObj.orderType.value=="desc, pk_specimen desc") {
		formObj.orderType.value= "asc, pk_specimen asc";
		lorderType="asc, pk_specimen asc";

	}


	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="specimenbrowser.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	setFilterText(formObj)
	formObj.submit();
}

function setFilterText(form){

	  form.ptxt.value=form.patIdText.value;
 	  form.sptxt.value=form.specimenId.value;
  	  form.otxt.value=form.createdBy.value;
   	  form.ltxt.value=form.storageLoc.value;
<%--  Fixed Bug #6844 agodara --%>
  	  if(!(validate_date(form.collDate))){
	    return false;
	  }else{
		form.dtxt.value=form.collDate.value;
		   }
	  form.scannerRead.value="0";	 //YK 25MAY2011 Bug#4340

 	  if (form.specStat[form.specStat.selectedIndex].value != "")
 	  form.dsttxt.value=form.specStat[form.specStat.selectedIndex].text ;

 	  form.dstudytxt.value=form.selStudy.value;

 	  if (form.specStatType[form.specStatType.selectedIndex].value != "")
 	  form.dtypetxt.value=form.specStatType[form.specStatType.selectedIndex].text ;

 	  if (form.dPatSite[form.dPatSite.selectedIndex].value != "")
 	  form.dorgtxt.value=form.dPatSite[form.dPatSite.selectedIndex].text ;


 }


 function deleteSpecimens(formobj,pageRight){
       if (! f_check_perm(pageRight,'E') )
     {
      return false;
     }
	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu  = formobj.srcmenu.value;

	 submit="no";
	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelSpmen_ToDel%>");/*alert("Please select Specimen(s) to be deleted");*****/
	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			   msg= "<%=MC.M_ActDelSpmen_SelWantCont%>"/*"This action will also " +
				  "delete all the child Specimens linked with the selected specimen(s). Are you " +
				  "sure you want to continue? "*****/ ;
			 if (confirm(msg))
			{

			cnt++;


  	   window.open("deletespecimens.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+formobj.Del.value,"_self");

		 }
		 else
				{
					return false;
				}
		 }

	 }else{
	  for(i=0;i<totcount;i++){

	  if (formobj.Del[i].checked){
		  selStrs[j] = formobj.Del[i].value;
		  j++;
		  cnt++;
			}
		}
		if(cnt>0){
			  msg="<%=MC.M_ActDelSpmen_SelWantCont%>"/*"This action will also " +
				  "delete all the child Specimens linked with the selected specimen(s). Are you " +
				  "sure you want to continue? "*****/ ;
			  if(confirm(msg)) {

		window.open("deletespecimens.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+selStrs,"_self");

	    }
			  else
				{
				  return false;
				}
		}

	  }
	  if(cnt==0)
		 {
		   alert("<%=MC.M_SelSpmen_ToDel%>");/*alert("Please select Specimen(s) to be deleted");*****/
		   return false;
		 }


 }

//JM: added: 29Oct2007:
 function editStatusWindow(formobj,pageRight){

      if (! f_check_perm(pageRight,'E') )
     {
      return false;
     }

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu  = formobj.srcmenu.value;

	 submit="no";
	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelSpmen_ToUpdt%>");/*alert("Please select Specimen(s) to be updated");*****/
	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			cnt++;
			windowName =window.open("editmultiplespecimenstatus.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+formobj.Del.value, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1100,height=450, top=100, left=90");
	  	 }
	  	 else{
	  		alert("<%=MC.M_SelSpmen_ToUpdt%>");/*alert("Please select Specimen(s) to be updated");*****/
	   	  return false;
	  	 }

	 }else{

	  for(i=0;i<totcount;i++){

	 	if (formobj.Del[i].checked){

			selStrs[j] = formobj.Del[i].value;
		  	j++;
		  	cnt++;
		 }
		}
		if(cnt>0){

	  windowName =window.open("editmultiplespecimenstatus.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+selStrs, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1100,height=450, top=100, left=90");
	  windowName.focus();

	  }
	  if(cnt==0)
		 {
		  alert("<%=MC.M_SelSpmen_ToUpdt%>");/*alert("Please select Specimen(s) to be updated");*****/
		   return false;
		 }

 }

 }

function printLabelWin (formobj,pageRight)
  {

      if (! f_check_perm(pageRight,'V') )
     {
      return false;
     }

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	  var selPks = "";

	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelSpmen_ToPrint%>");/*alert("Please select Specimen(s) to be Printed");*****/
	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			cnt++;
			selPks = "," +  formobj.Del.value;
	  	 }


	 }else{

	  for(i=0;i<totcount;i++)
	  {

	 	if (formobj.Del[i].checked)
	 	{

			//selStrs[j] = formobj.Del[i].value;
			selPks = selPks + "," + formobj.Del[i].value;

		  	j++;
		  	cnt++;
		 }	//if
	 	} //for

	 } 	 //else
	if(cnt>0){

		selPks = selPks + ",";

	  windowName =window.open("printMultiLabel.jsp?&selPks="+selPks, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=450, top=100, left=90");
	  if (windowName != null) { windowName.focus(); }

	  }
//JM: 14Dec2009: #4343
	  if(cnt==0)
		 {
		  alert("<%=MC.M_SelSpmen_ToPrint%>");/*alert("Please select Specimen(s) to be Printed");*****/
		   return false;
		 }


 }

function checkAll(formobj){
    act="check";
 	totcount=formobj.totcount.value;
    if (formobj.chkAll.value=="checked")
		act="uncheck" ;
    if (totcount==1){
       if (act=="uncheck")
		   formobj.Del.checked =false ;
       else
		   formobj.Del.checked =true ;
	}
    else {
         for (i=0;i<totcount;i++){
             if (act=="uncheck")
				 formobj.Del[i].checked=false;
             else
				 formobj.Del[i].checked=true;
         }
    }


    if (act=="uncheck")
		formobj.chkAll.value="unchecked";
    else
		formobj.chkAll.value="checked";
}


function openLocLookup(formobj) {

	/*formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6050&form=specimen&seperator=,"+
                  "&keyword=storageLoc|storage_name~mainFKStorage|pk_storage|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="specimenbrowser.jsp?page=1";
	void(0);*/

	//JM: 31Dec2009 #4576
	windowName= window.open("searchLocation.jsp?srchLocFlag=Y&gridFor=SP&form=specimen&locationName=storageLoc&storagepk=mainFKStorage&coordx=strCoordinateX&coordy=strCoordinateY","Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=650");
	windowName.focus();
}

function initEvents() {
	if (!document || !document.forms[0]) { return; }
	if (document.forms[0].barcode) {
        document.forms[0].barcode.value = "";
	}
    document.onkeypress = readScanner;
}
function readScanner(evt) {
	evt = (evt) || window.event;
    if (evt) {
        var code = evt.charCode || evt.keyCode;
        var elem = (evt.target) ? evt.target :
            ((evt.srcElement) ? evt.srcElement : null);
        var target = "";
        if (elem) {
        	if (elem.nodeType == 1) {
                // for W3C DOM property
                if (evt.relatedTarget) {
                    if (evt.relatedTarget != elem.firstChild) {
                    	target = (evt.relatedTarget.firstChild) ?
                            evt.relatedTarget.firstChild.nodeValue : null;
                    }
                // for IE DOM property
                } else if (elem.name) {
                	target = elem.name;
                }
        	}
        }
        if (!target) {
            document.specimen.barcode.value = document.specimen.barcode.value + String.fromCharCode(code);
            document.specimen.specimenId.value = document.specimen.barcode.value;
            if (document.specimen.specimenId.value != '') {
            	setFilterText(document.specimen);
            	document.specimen.scannerRead.value="1";//YK 25MAY2011 Bug#4340
                setTimeout("document.specimen.submit()", 1000);
            }
        }
    }
}
function checkBarcode() {
	if (document.specimen.barcode.value != '') {
		document.specimen.specimenId.value = document.specimen.barcode.value;
	}
}

function removeLocation(formobj, turn) {
	if (turn == 1){
		formobj.storageLoc.value = "";
		formobj.mainFKStorage.value ="";
	}else if(turn==2){
		formobj.storageLocation.value = "";
		formobj.mainFKStorageId.value ="";
	}else if(turn==3){
		formobj.eStorageLocation.value = "";
		formobj.eMainFKStorageId.value ="";
	}
}
//YK 25MAY2011 Bug#4340
function goToStorageDetail(pkId,personPk,studyPk) {
	location.href = 'specimendetails.jsp?mode=M&pkId='+pkId+'&pkey='+personPk+'&studyId='+studyPk;
}

//PS 02Aug2012 INV 21677
function callOverlib(fullpath) 
{
	return overlib('<table border=0><tr><td valign=top><font size=2><%=LC.L_Location%>:<br>'+fullpath+'</font></td></tr></table>');
}

function ClearDynaDiv(cdiv_id)
{
	document.getElementById(cdiv_id).innerHTML = "";
}
//  AJAX function to fetch specimen location - Requirement No: INV 21677
function processAjaxCall(pk_storage_id) 
{
	xmlHttp = getXmlHttpObject();
	var urlParameters = "ajaxGetSpecimenLocation.jsp?strg_pk="+pk_storage_id;
	xmlHttp.onreadystatechange = stateChanged;
	xmlHttp.open("GET", urlParameters, true);
	xmlHttp.send(null);
}	

function getXmlHttpObject()
{
	var xmlHttp = null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) { //Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function stateChanged() 
{
	var resultArray;
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		if (xmlHttp.status == 200) 
		{
			showDataChild = xmlHttp.responseText;
			callOverlib(showDataChild);
		}
		else 
		{
			alert("Error occured in processing the data, Please try again.");
		}
	}
}
</SCRIPT>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="tdmenubaritem6"/>
</jsp:include>
<script>
window.onload = initEvents;
</script>
<body>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>


<%
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))
{
  String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	String userId="";
	String acc="";

	userId = (String) tSession.getValue("userId");

	String fkStorage  = "";

	int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSPEC"));


String accId = (String) tSession.getValue("accountId");

String ptxt=LC.L_All/*"All"*****/,sptxt=LC.L_All/*"All"*****/,otxt=LC.L_All/*"All"*****/,ltxt=LC.L_All/*"All"*****/,dtxt=LC.L_All/*"All"*****/ ;
String dsttxt = LC.L_All/*"All"*****/, dstudytxt = LC.L_All/*"All"*****/, dtypetxt = LC.L_All/*"All"*****/, dorgtxt = LC.L_All/*"All"*****/;



 String selectedTab = "" ;
 selectedTab=request.getParameter("selectedTab");

 String patIdRec = request.getParameter("patIdText");
 if (patIdRec==null) patIdRec="";

 String specimnId = request.getParameter("specimenId");
 if (specimnId==null) specimnId="";

 String storageLocation = request.getParameter("storageLoc");
 if (storageLocation==null) storageLocation="";


 String storageOwnr = request.getParameter("createdBy");
 if (storageOwnr==null) storageOwnr="";

 String collDt = request.getParameter("collDate");
 if (collDt==null) collDt="";


 String creatorid = request.getParameter("creatorId");
 if (creatorid==null) creatorid="";

 String studyId = request.getParameter("selStudyIds");
 if (studyId==null) studyId="";

 String studyNumber = request.getParameter("selStudy");
 if (studyNumber==null) studyNumber="";




 fkStorage = request.getParameter("mainFKStorage");
 if (StringUtil.isEmpty(fkStorage))
 {
 	fkStorage = "";
 }

//JM: 25Jun2009: #INVP2.39(a)
String isCheckedStudy="";
String isCheckedPat="";

String specSearchStudy =request.getParameter("chkSpecStudy");
String specSearchPat =request.getParameter("chkSpecPat");

if (specSearchStudy == null || specSearchStudy =="")	{specSearchStudy ="";} else {specSearchStudy ="1";}
if (specSearchPat == null || specSearchPat =="")	{specSearchPat ="";} else {specSearchPat ="1";}

if (specSearchStudy.equals("1")){isCheckedStudy ="checked";}else {isCheckedStudy ="";}
if (specSearchPat.equals("1")){isCheckedPat ="checked";}else {isCheckedPat="";}


 if (( request.getParameter("ptxt"))!= null &&  (request.getParameter("ptxt").length() > 0)  )		     ptxt= request.getParameter("ptxt");
 if (( request.getParameter("sptxt"))!= null &&  (request.getParameter("sptxt").length() > 0)  )		     sptxt= request.getParameter("sptxt");
 if (( request.getParameter("otxt"))!= null &&  (request.getParameter("otxt").length() > 0)  )		     otxt= request.getParameter("otxt");
 if (( request.getParameter("ltxt"))!= null &&  (request.getParameter("ltxt").length() > 0)  )		     ltxt= request.getParameter("ltxt");
 if (( request.getParameter("dtxt"))!= null &&  (request.getParameter("dtxt").length() > 0)  )		     dtxt= request.getParameter("dtxt");

 if (( request.getParameter("dsttxt"))!= null &&  (request.getParameter("dsttxt").length() > 0)  )		dsttxt= request.getParameter("dsttxt");
 if (( request.getParameter("dstudytxt"))!= null &&  (request.getParameter("dstudytxt").length() > 0)  )		dstudytxt= request.getParameter("dstudytxt");
 if (( request.getParameter("dtypetxt"))!= null &&  (request.getParameter("dtypetxt").length() > 0)  )		dtypetxt= request.getParameter("dtypetxt");
 if (( request.getParameter("dorgtxt"))!= null &&  (request.getParameter("dorgtxt").length() > 0)  )		dorgtxt= request.getParameter("dorgtxt");








	CodeDao cdSpecStatTyp = new CodeDao();
	cdSpecStatTyp.getCodeValues("specimen_type");

	cdSpecStatTyp.setCType("specimen_type");
	cdSpecStatTyp.setForGroup(defUserGroup);

	String dSpecStatType = "";


	String specStatTyp =	request.getParameter("specStatType");
	if (specStatTyp ==null) specStatTyp ="";

	if (specStatTyp.equals("")){
 	   dSpecStatType=cdSpecStatTyp.toPullDown("specStatType");
	}
	else{
       dSpecStatType=cdSpecStatTyp.toPullDown("specStatType",EJBUtil.stringToNum(specStatTyp),true);
	}



	CodeDao cdSpecStat = new CodeDao();
	cdSpecStat.getCodeValues("specimen_stat");

	cdSpecStat.setCType("specimen_stat");
	cdSpecStat.setForGroup(defUserGroup);


	String dSpecStat = "";


	String specimenStat =	request.getParameter("specStat");
	if (specimenStat ==null) specimenStat ="";

	if (specimenStat.equals("")){
 	   dSpecStat=cdSpecStat.toPullDown("specStat");
	}
	else{
           dSpecStat=cdSpecStat.toPullDown("specStat",EJBUtil.stringToNum(specimenStat),true);
	}




	int inSite = 0;
	String selSite = "";
	selSite =  request.getParameter("dPatSite");
	if (selSite==null) selSite = "";

	UserJB user = (UserJB) tSession.getValue("currentUser");

	String siteId = user.getUserSiteId();
   	String accountId = user.getUserAccountId();

	UserSiteDao usd = new UserSiteDao ();
	ArrayList arSiteid = new ArrayList();
	ArrayList arSitedesc = new ArrayList();
	String ddSite = "";

	usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));

	arSiteid = usd.getUserSiteSiteIds();
	arSitedesc = usd.getUserSiteNames();

	StringBuffer sbSite = new StringBuffer();

	//JM: 22DEC2009: #4567: Found that when the Org. Name is too long (50)/captures more pixels then FF browser GUI misalignment takes place
	String tempDesc = "";
	String sqlSiteAll = "";

	sbSite.append("<SELECT NAME='dPatSite' STYLE='WIDTH:250 px'> ") ;
	sbSite.append("<option selected value='0' >"+LC.L_All/*All*****/+"</option>") ;
		if (arSiteid.size() > 0)
		{
			for (int counter = 0; counter < arSiteid.size()  ; counter++)
			{
				inSite = Integer.parseInt((String)arSiteid.get(counter));
				tempDesc = ((String)arSitedesc.get(counter));
				if (tempDesc.length()>25)
				tempDesc = tempDesc.substring(0,22) + "...";

				if(inSite == EJBUtil.stringToNum(selSite)){

					sbSite.append("<OPTION value = "+ inSite+" selected>" + tempDesc+ "</OPTION>");
				}else{
				sbSite.append("<OPTION value = "+ inSite+">" + tempDesc+ "</OPTION>");
				}
				sqlSiteAll = inSite +","+ sqlSiteAll;
			}
		}

	sbSite.append("</SELECT>");
	ddSite  = sbSite.toString();
	
	if(sqlSiteAll.length() > 0)
	{
		sqlSiteAll = sqlSiteAll.substring(0, (sqlSiteAll.length()-1));
	}

	//JM: 14Nov2007
	String studyIdStr = "";
	studyIdStr = request.getParameter("selStudyIds");
	studyIdStr=(studyIdStr==null)?"":studyIdStr;


	String mainsql = "";
	String countsql = "";

	StringBuffer sbSelect = new StringBuffer();
	StringBuffer sbFrom = new StringBuffer();
	StringBuffer sbWhere = new StringBuffer();



	sbSelect.append(" select  PK_SPECIMEN, SPEC_ID, ") ;
	sbSelect.append(" (select per_code from er_per where pk_per=fk_per) person,  ") ;
	sbSelect.append(" (select PK_STORAGE from ER_STORAGE WHERE PK_STORAGE = ER_SPECIMEN.FK_STORAGE) pk_storage,  ") ;
	sbSelect.append(" (select SITE_NAME from ER_SITE WHERE PK_SITE = ER_SPECIMEN.FK_SITE) site_name,  ") ;
	sbSelect.append(" (select study_number from er_study where pk_study=fk_study) studynum, fk_per, fk_Study,") ;
	sbSelect.append(" SPEC_COLLECTION_DATE, ") ;
	sbSelect.append(" (select visit_name from SCH_PROTOCOL_VISIT,sch_events1 schev where schev.event_id = FK_SCH_EVENTS1 and  PK_PROTOCOL_VISIT= schev.FK_VISIT) visit,  ") ;
	sbSelect.append(" (select codelst_desc from er_codelst where PK_codelst=SPEC_TYPE) spec_type , ");

	sbSelect.append(" (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = ( select  fk_codelst_status from ER_SPECIMEN_STATUS where  fk_specimen = pk_specimen ");
	sbSelect.append(" and pk_specimen_status=(select max(PK_SPECIMEN_STATUS)from ER_SPECIMEN_STATUS where fk_specimen=pk_specimen ");
    sbSelect.append(" and SS_DATE =(select max(SS_DATE) from ER_SPECIMEN_STATUS where fk_specimen=pk_specimen )))) specimen_status ") ;



	sbFrom.append(" from ER_SPECIMEN ");

//JM: 19Sep2007: Modified for the issue #3159
	sbWhere.append(" where  fk_account =" + accId +" and SPEC_ID is not null");

//JM: 22Aug2007: Modified for the issue #3122
	StringBuffer sbWhereSearch = new StringBuffer();
	String strSearch = "";

	sbWhereSearch.append("  exists ( select * from epat.person a, ER_PATFACILITY fac, er_usersite usr ");
	sbWhereSearch.append(" WHERE pk_person = er_specimen.fk_per and a.fk_account =" + accountId + " and usr.fk_user =" + userId + " AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site ");
	sbWhereSearch.append(" AND fac.patfacility_accessright > 0 ");

	strSearch = " and " + sbWhereSearch.toString();

	if (!patIdRec.equals("")){

		sbWhere.append(strSearch);
		sbWhere.append(" and (lower(a.person_code) like lower('%"+ patIdRec.trim() + "%')  or" );
		sbWhere.append(" lower(fac.pat_facilityid) like lower('%"+patIdRec.trim()+"%')) and a.pk_person = fac.fk_per )" ) ;
	}


	if (!specimnId.equals("")){

		sbWhere.append(" and ( lower(spec_id) like lower('%"+ specimnId.trim() + "%') ");
		sbWhere.append("    or lower(spec_alternate_id) like lower('%"+ specimnId.trim() + "%') ) ");

	}

	if (!specimenStat.equals("")){

	 sbWhere.append(" and " + specimenStat + "=(SELECT fk_codelst_status FROM er_specimen_status WHERE fk_specimen = pk_specimen ");
     sbWhere.append(" AND pk_specimen_status = (SELECT MAX(pk_specimen_status) FROM er_specimen_status WHERE fk_specimen = pk_specimen) ");
     sbWhere.append(" AND ss_date = (SELECT MAX(ss_date) FROM er_specimen_status WHERE fk_specimen = pk_specimen)) ");

	}

	if (! StringUtil.isEmpty(fkStorage))
	{
		sbWhere.append(" and fk_storage = "  + fkStorage );
	}

	if (!studyIdStr.equals("")){

		sbWhere.append(" and fk_study  = " + studyIdStr );
	}


	if (!specStatTyp.equals("") ){

		sbWhere.append(" and SPEC_TYPE   = " + specStatTyp );


	}


	if (!creatorid.equals("") ){

		sbWhere.append(" and SPEC_OWNER_FK_USER = " + creatorid );
	}



	if (!selSite.equals("0") && !selSite.equals("")){

		//Panjvir: INV 21679
		sbWhere.append(" and FK_SITE = " + selSite );
		//sbWhere.append(strSearch);
		//sbWhere.append(" and fac.fk_site = " + selSite + " and a.pk_person = fac.fk_per )" );

	}
	else if(selSite.equals("0"))
	{
		if(sqlSiteAll.length() > 0)
		{
			sbWhere.append(" AND (FK_SITE IN ("+ sqlSiteAll +") OR FK_SITE IS NULL)");
		}
		else
		{
			sbWhere.append(" AND FK_SITE IS NULL");
		}
	}

	//JM: 28Aug2007: added for the issue #3122
	//Modified for Req:21679 
/* 	if (!patIdRec.equals("")&& !selSite.equals("0")){


		sbWhere.append(strSearch);
		sbWhere.append(" and (lower(a.person_code) like lower('%"+ patIdRec.trim() + "%')  or" );
		sbWhere.append(" lower(fac.pat_facilityid) like lower('%"+patIdRec.trim()+"%'))" ) ;
		sbWhere.append(" and fac.fk_site = " + selSite + " and a.pk_person = fac.fk_per )" );

	} */



	if ( !EJBUtil.isEmpty(collDt)){
		sbWhere.append(" and spec_collection_date like to_date('" +
		        collDt + "', PKG_DATEUTIL.f_get_dateformat)" );
	}

//append study access rights check

 if (specSearchStudy.equals("1")){
	sbWhere.append("  and fk_study is null ");
 }else{
	sbWhere.append("  and (fk_study is null or ( exists ( select * from er_studyteam t where t.fk_study = er_specimen.fk_study and ");
	sbWhere.append(" fk_user = "+userId+" and  nvl(study_team_usr_type,'D')='D') or pkg_superuser.F_Is_Superuser("+userId+", er_specimen.fk_study) = 1 ) )");
 }

//append patient access rights check

 if (specSearchPat.equals("1")){
	sbWhere.append(" and fk_per is null "); //one extra bracket as sbWheerSearch has one less bracket
 }else{
	sbWhere.append(" and ( fk_per is null or " + sbWhereSearch.toString() + " and a.pk_person = fac.fk_per ) )" ); //one extra bracket as sbWheerSearch has one less bracket
 }
 	 mainsql = sbSelect.toString() + sbFrom.toString() + sbWhere.toString();
 	 //System.out.println("mainsql=====>"+mainsql);
 	 countsql = " Select count(*) from ( " + mainsql + " )";

			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;


			pagenum = request.getParameter("page");

			curPage = EJBUtil.stringToNum(pagenum == null ? "1" : pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");

			if (EJBUtil.isEmpty(orderBy)){
			orderBy = "SPEC_COLLECTION_DATE";
			}

			String orderType = "";

			orderType = request.getParameter("orderType");

			if (EJBUtil.isEmpty(orderType))
			{
			orderType = "desc, pk_specimen desc";
			//orderType = "desc";

			}


			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;
			long firstRec = 0;
			long lastRec = 0;
			boolean hasMore = false;
			boolean hasPrevious = false;


			rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
			totalPages =Configuration.PAGEPERBROWSER ;

			BrowserRows br = new BrowserRows();

			//JM: 19Nov2009: #4344
			if (pagenum != null) {
				br.getPageRows(curPage,rowsPerPage,mainsql,totalPages,countsql,orderBy,orderType);
			}

	    	rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
			startPage = br.getStartPage();
			hasMore = br.getHasMore();
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();


String id = "", patId = "", studynum = "", collectDate ="";
String visit = "",  type = "", status = "";
String pkId = "", speclocation = "";

String personPk ="", studyPk="", siteName="";

%>

<!--JM: 17Sep2007: Moved segement here from above-->
<DIV class="tabDefTopN" id="div1">
		<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		</jsp:include>

<!--JM: 17Sep2007: -->

<Form name="specimen" method="post" action="specimenbrowser.jsp?page=1" onSubmit="checkBarcode();setFilterText(document.specimen);">

<input type="hidden" name="selectedTab" Value=<%=selectedTab%> >
<Input type="hidden" name="orderBy" value="<%=orderBy%>">
<Input type="hidden" name="page" value="<%=curPage%>">
<Input type="hidden" name="orderType" value="<%=orderType%>">
<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="ptxt">
<input type="hidden" name="sptxt">
<input type="hidden" name="otxt">
<input type="hidden" name="ltxt">
<input type="hidden" name="dtxt">
<input type="hidden" name="dsttxt">
<input type="hidden" name="dstudytxt">
<input type="hidden" name="dtypetxt">
<input type="hidden" name="dorgtxt">
<input type="hidden" name="scannerRead" value="0"> <!-- /* YK 25MAY2011 Bug#4340 -->

</DIV>

<DIV class="tabFormTopN tabFormTopN_PAS"  id="div2" style="overflow:visible">
<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0" border="0" >
<tr class="tmpHeight"></tr>
<tr >
	<td><%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>:</td>
	<td><Input type = "text" name="patIdText"  value="<%=patIdRec%>" size=15 align = right>
	</td>

	<td><%=LC.L_Type%><%--Type*****--%>:</td>
	<td><%=dSpecStatType%></td>

	<td><%=LC.L_Organization%><%--Organization*****--%>:</td>
	<td><%=ddSite%></td>

</tr>

<tr >
	<td><%=LC.L_Specimen_Id%><%--Specimen ID*****--%>:</td>
	<td><Input type = "text" name="specimenId" value="<%=specimnId%>" size=15>
	</td>

	<td ><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:</td>
	<input type="hidden" name="selStudyIds" value="<%=studyId%>">
	<td ><Input type = "text" name="selStudy"  value="<%=studyNumber%>" size=15 readonly >
	<A href="#" onClick="openStudyWindow(document.specimen)" ><%=LC.L_Select%><%--Select*****--%></A>
	</td>

    	<td><%=LC.L_Status%><%--Status*****--%>:</td>
	<td><%=dSpecStat%></td>
</tr>
<tr >

	<td><%=LC.L_Location%><%--Location*****--%>:</td>
	<td><Input readonly type = "text" name="storageLoc" value="<%=storageLocation%>" size=15 align = right>
	<Input type = "hidden" name="mainFKStorage" value = "<%=fkStorage%>" size=25 align = right/>
	<Input type = "hidden" name="strCoordinateX" value = "" size=25 align = right/>
	<Input type = "hidden" name="strCoordinateY" value = "" size=25 align = right/>
	<A  href="javascript:void(0);" onClick="return openLocLookup(document.specimen)" ><%=LC.L_Select%><%--Select*****--%></A>&nbsp;
	<A href="#" onClick="return removeLocation(document.specimen, 1)" ><%=LC.L_Remove%><%--Remove*****--%></A>&nbsp;
	</td>

	<input type="hidden" name="creatorId" value="<%=creatorid%>">
    	<td><%=LC.L_Owner%><%--Owner*****--%>: </td>
	<td><Input type = "text" name="createdBy" readonly value="<%=storageOwnr%>" size=15 align = right>
	<A href="#" onClick="return openUserWindow('specimenUser')"><%=LC.L_Select%><%--Select*****--%></A></td>
	
<%-- INF-20084 Datepicker-- AGodara --%>
	<td><%=LC.L_Collection_Date%><%--Collection Date*****--%>:</td>
    	<td><Input type = "text" name="collDate" value="<%=collDt%>" size=15 align = right class="datefield">
	</td>
	
</tr>
<tr  >
<td colspan="2">
<input type="checkbox" name="chkSpecStudy" <%=isCheckedStudy%> >
	<%=MC.M_SpmenNtLkd_Std%><%--Specimen not linked with <%=LC.Std_Study%>*****--%>
</td><td>&nbsp;</td>
<td colspan="2">
<input type="checkbox" name="chkSpecPat" <%=isCheckedPat%> >
	<%=MC.M_SpmenNtLkd_Pat%><%--Specimen not linked with <%=LC.Pat_Patient%>*****--%>
</td>
<td>
   	&nbsp;&nbsp;&nbsp;<button type="submit"><%=LC.L_Search%></button>
</td>
</tr>
</table>
<div class="tmpHeight"></div>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="searchBg">
	<tr height="35">
		<td width=30% class="lhsFont">
			<%=LC.L_Barcode%><%--Barcode*****--%>:&nbsp;<input id="barcode" name="barcode" type="text" size=12>
		</td>
		<td width=70% align="right">
			<A href="bulkupload.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=N" onClick="return f_check_perm(<%=pageRight%>,'N') ;"><%=ES.ES_BulkUpld_Area%><%--Bulk Upload Area *****--%></A>&nbsp;&nbsp;&nbsp;
			<A href="specimendetails.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mode=N" onClick="return f_check_perm(<%=pageRight%>,'N') ;"><%=LC.L_Add_New%><%--ADD NEW*****--%></A>&nbsp;&nbsp;&nbsp;
			<A href="#" onClick="return openwindow(document.specimen,<%=pageRight%>);" ><%=LC.L_Add_Multi%><%--ADD MULTIPLE*****--%></A>&nbsp;&nbsp;&nbsp;
			<A href="#" onClick=" return editStatusWindow(document.specimen ,<%=pageRight%>);" ><%=LC.L_Update_Status%><%--UPDATE STATUS--%></A>&nbsp;&nbsp;&nbsp;
			<A href="#" onClick=" return printLabelWin(document.specimen ,<%=pageRight%>);"><%=LC.L_Print_Label%><%--PRINT LABEL*****--%></A>&nbsp;&nbsp;&nbsp;
			<A href="#"onClick =" return deleteSpecimens(document.specimen,<%=pageRight%>);" ><%=LC.L_Delete%><%--DELETE*****--%></A>
		</td>
	</tr>
</table>

</DIV>


<DIV class="tabFormBotNInv" id="div3">
<%
/*//YK 25MAY2011 Bug#4340*/
String scannerReader = request.getParameter("scannerRead");
scannerReader=(scannerReader==null)?"0":scannerReader;
if ( scannerReader.equalsIgnoreCase("0") || (scannerReader.equalsIgnoreCase("1") && rowsReturned>1)){
%>

<table class="basetbl outline" width="100%" cellspacing="0" cellpadding="0" border="0">
<TR>


     <th width=14% onClick="setOrder(document.specimen,'lower(SPEC_ID)')" align =center><%=LC.L_Specimen_Id%><%--Specimen ID*****--%> &loz;</th>
     <th width=12% onClick="setOrder(document.specimen,'lower(person)')" align =center><%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%> &loz;</th>
     <th width=10% onClick="setOrder(document.specimen,'lower(studynum)')" align =center><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> &loz;</th>
     <th width=12% onClick="setOrder(document.specimen,'SPEC_COLLECTION_DATE')" align =center><%=LC.L_Collected_On%><%--Collected On*****--%> &loz;</th>
     <th width=10% onClick="setOrder(document.specimen,'lower(visit)')" align =center><%=LC.L_Visit%><%--Visit*****--%> &loz;</th>
     <th width=8%  onClick="setOrder(document.specimen,'lower(spec_type)')" align =center ><%=LC.L_Type%><%--Type*****--%> &loz;</th>
     <th width=10% onClick="setOrder(document.specimen,'lower(specimen_status)')" align =center ><%=LC.L_Status%><%--Status*****--%> &loz;</th>
	 <th width=13% align =center ><%=LC.L_Location%></th>
     <!-- <th width=7%  align =center >Print</th> -->
     <th width=11%  align =center ><%=LC.L_Select%><%--Select*****--%><input type="checkbox" name="chkAll" value="" onClick="checkAll(document.specimen)"></th>

</TR>

<%
			int specTotal=0;
			for(int i = 1 ; i <=rowsReturned ; i++){
				siteName = br.getBValues(i,"site_name");
				siteName=(siteName==null)?"":siteName;
				if((arSitedesc.contains(siteName))||(siteName.isEmpty())){
				specTotal++;
			 pkId = br.getBValues(i,"PK_SPECIMEN");

			 id = br.getBValues(i,"SPEC_ID");
			 id=(id==null)?"":id;

			 if ((id.trim()).length()>50) {
			 id = (id.trim()).substring(0, 50) + "...";

			 }

			 patId = br.getBValues(i,"person");
			 patId=(patId==null)?"":patId;

			 studynum = br.getBValues(i,"studynum");
			 studynum=(studynum==null)?"":studynum;

 			 collectDate = br.getBValues(i,"SPEC_COLLECTION_DATE");
 			 collectDate=(collectDate==null)?"":collectDate;



 			 int dateLen = collectDate.length();
			 if(dateLen > 1){
				collectDate = DateUtil.dateToString(java.sql.Date.valueOf(collectDate.toString().substring(0,10)));
			 }

 			 visit = br.getBValues(i,"visit");
 			 visit=(visit==null)?"":visit;

 			 type = br.getBValues(i,"spec_type");
 			 type=(type==null)?"":type;
	
		//PS: 02Aug2012: #INV 21677
			 speclocation = br.getBValues(i,"pk_storage");
 			 speclocation=(speclocation==null)?"":speclocation;
			 
 			 status = br.getBValues(i,"specimen_status");
 			 status=(status==null)?"":status;


 		//JM: 09Jul2009: #INVP2.11
 			 personPk = br.getBValues(i,"fk_per");
 			 personPk=(personPk==null)?"":personPk;

 			 studyPk = br.getBValues(i,"fk_Study");
 			 studyPk=(studyPk==null)?"":studyPk;

 
	if ((i%2)!=0) {

	%>

      <tr class="browserEvenRow">

	<%

	}else{

	%>

      <tr class="browserOddRow">

       <%
 	}

       %>


	 <td width =14%><A href="specimendetails.jsp?mode=M&pkId=<%=pkId%>&pkey=<%=personPk%>&studyId=<%=studyPk%>"><%=id%></A></td>
	 <td width =12%><%=patId %></td>
   	 <td width =10%><%=studynum%></td>
	 <td width =12%><%=collectDate%></td>
	 <td width =10%><%=visit%></td>
	 <td width =8%><%=type%></td>
	 <td width =10%><%=status%></td>
	 <td width =13%>
	 <% if(!speclocation.isEmpty()){ %><a href="#" onmouseover="processAjaxCall('<%=speclocation%>');" onmouseout="return nd();" style="text-decoration:none;"><%=LC.L_View_Location%></a><% } %>
	 </td>
 	<!-- <td width =7% align="center"><A href="#"> <img src="./images/printer.gif" border="0" align="center"/></A></td> -->

     <td width =11% align="center"><input type="checkbox" name="Del" value="<%=pkId%>"></td>


</tr>

<%
				}	//if condition
	}    //for loop
	rowsReturned=specTotal;
%>
<input type="hidden" name="totcount" Value="<%=rowsReturned%>">

</table>
<%}else if (rowsReturned==1){ /*//YK 25MAY2011 Bug#4340*/
	pkId = br.getBValues(1,"PK_SPECIMEN");
	personPk = br.getBValues(1,"fk_per");
	personPk=(personPk==null)?"''":personPk;

	 studyPk = br.getBValues(1,"fk_Study");
	 studyPk=(studyPk==null)?"''":studyPk;
	%>
	<script type="text/javascript">
	goToStorageDetail(<%=pkId%>,<%=personPk%>,<%=studyPk%>);
	</script>
	<%
	
} %>
<P class="defComments_txt"><%=MC.M_YourFilter_PatID%><%--Your selected filters are: <%=LC.Pat_Patient%> ID*****--%> :&nbsp;<B><I><u><%=ptxt%></u></I>
	</B>&nbsp;&nbsp;<%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> :&nbsp;<B><I><u><%=dstudytxt%></u></I>
	</B>&nbsp;&nbsp;<%=LC.L_Organization%><%--Organization*****--%>:&nbsp;<B><I><u><%=dorgtxt%></u></I></B>&nbsp;&nbsp;<%=LC.L_Specimen_Id%><%--Specimen ID*****--%> :&nbsp;<B><I><u><%=sptxt%></u></I>
	</B>&nbsp;&nbsp;<%=LC.L_Type%><%--Type*****--%>:&nbsp;<B><I><u><%=dtypetxt%></u></I></B>&nbsp;&nbsp;<%=LC.L_Location%><%--Location*****--%>:&nbsp;<B><I><u><%=ltxt%></u></I></B>
    &nbsp;&nbsp;<%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=dsttxt%></u></I>
	</B>&nbsp;&nbsp;<%=LC.L_Owner%><%--Owner*****--%>:&nbsp;<B><I><u><%=otxt%></u></I></B>&nbsp;&nbsp;<%=LC.L_Collection_Date%><%--Collection Date*****--%>:&nbsp;
	<B><I><u><%=dtxt%></u></I></B>
	</P>

<% if (rowsReturned==0){%>
<P class="defComments_txt" align="center"><%=MC.M_NoRec_MdfCrit%><%--No matching records found. Please modify your criteria and try again.*****--%></P>
<%}%>
<% /*//YK 25MAY2011 Bug#4340*/
if ( scannerReader.equalsIgnoreCase("0") || (scannerReader.equalsIgnoreCase("1") && rowsReturned>1)){
%>
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
		<tr valign="top"><td>
		<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} %>
	   </td></tr>
	</table>
			
	<div align="center" class="midalign">
	<%
		if (curPage==1) startPage=1;
	
	    for (int count = 1; count <= showPages;count++)
		{
	   cntr = (startPage - 1) + count;
	
		if ((count == 1) && (hasPrevious))
		{
    %>

  		<A href="specimenbrowser.jsp?selectedTab=1&srcmenu=<%=src%>&patIdText=<%=patIdRec%>&selStudyIds=<%=studyIdStr%>&selStudy=<%=studyNumber%>&specimenId=<%=specimnId%>&specStat=<%=specimenStat%>&creatorId=<%=creatorid%>&createdBy=<%=storageOwnr%>&storageLoc=<%=storageLocation%>&specStatType=<%=specStatTyp%>&dPatSite=<%=selSite%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&ptxt=<%=ptxt%>&sptxt=<%=sptxt%>&otxt=<%=otxt%>&ltxt=<%=ltxt%>&dtxt=<%=dtxt%>&dsttxt=<%=dsttxt%>&dstudytxt=<%=dstudytxt%>&dtypetxt=<%=dtypetxt%>&dorgtxt=<%=dorgtxt%>&chkSpecStudy=<%=specSearchStudy%>&chkSpecPat=<%=specSearchPat%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;

	<%
  		}
	%>

	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="specimenbrowser.jsp?selectedTab=1&srcmenu=<%=src%>&patIdText=<%=patIdRec%>&selStudyIds=<%=studyIdStr%>&selStudy=<%=studyNumber%>&specimenId=<%=specimnId%>&specStat=<%=specimenStat%>&creatorId=<%=creatorid%>&createdBy=<%=storageOwnr%>&storageLoc=<%=storageLocation%>&specStatType=<%=specStatTyp%>&dPatSite=<%=selSite%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&ptxt=<%=ptxt%>&sptxt=<%=sptxt%>&otxt=<%=otxt%>&ltxt=<%=ltxt%>&dtxt=<%=dtxt%>&dsttxt=<%=dsttxt%>&dstudytxt=<%=dstudytxt%>&dtypetxt=<%=dtypetxt%>&dorgtxt=<%=dorgtxt%>&chkSpecStudy=<%=specSearchStudy%>&chkSpecPat=<%=specSearchPat%>"><%= cntr%></A>
       <%
    	}
	 %>

	<%
	  }

	if (hasMore)
	{
   %>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="specimenbrowser.jsp?selectedTab=1&srcmenu=<%=src%>&patIdText=<%=patIdRec%>&selStudyIds=<%=studyIdStr%>&selStudy=<%=studyNumber%>&specimenId=<%=specimnId%>&specStat=<%=specimenStat%>&creatorId=<%=creatorid%>&createdBy=<%=storageOwnr%>&storageLoc=<%=storageLocation%>&specStatType=<%=specStatTyp%>&dPatSite=<%=selSite%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&ptxt=<%=ptxt%>&sptxt=<%=sptxt%>&otxt=<%=otxt%>&ltxt=<%=ltxt%>&dtxt=<%=dtxt%>&dsttxt=<%=dsttxt%>&dstudytxt=<%=dstudytxt%>&dtypetxt=<%=dtypetxt%>&dorgtxt=<%=dorgtxt%>&chkSpecStudy=<%=specSearchStudy%>&chkSpecPat=<%=specSearchPat%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	<%
  	}
	%>
	</div>
<%} %>


	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>

</form>
<%
} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for session time out

%>
	</DIV>

</body>

</html>
