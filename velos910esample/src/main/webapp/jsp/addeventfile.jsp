<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%
	String fromPage = request.getParameter("fromPage");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {		
	%>
		<title> <%=MC.M_EvtLib_ApdxEvtDets%><%--Event Library >> Event Appendix >> Event File Details*****--%> </title>
	<%
	} else {%>
		<title><%=LC.L_Evt_FileDets%><%--Event File Details*****--%></title>	
	<%
	}
%>


<%@ page import="java.util.*,com.velos.eres.service.util.*,java.io.*,org.w3c.dom.*" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>

<%

//SV, 11/05, fix for bug 1827, allow appendix from eventbrowser: part of cal-enh
//		if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))
if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") || (request.getParameter("fromPage")).equals("eventbrowser") )


{ %>



<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->




 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))



	%>



<SCRIPT Language="javascript">



 function  validate(formobj){



     if (!(validate_col('File',formobj.name))) return false

     if (!(validate_col('File',formobj.eSign))) return false

     if (!(validate_col('Description',formobj.desc))) return false



	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   }





 }

</SCRIPT>





<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<!--Virendra: Fixed Bug no. 4734, added com.velos.eres.service.util.StringUtil  -->
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao,com.velos.eres.service.util.StringUtil"%>

<jsp:useBean id="eventdocB" scope="request" class="com.velos.esch.web.eventdoc.EventdocJB"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>



<% String src;



src= request.getParameter("srcmenu");

//String eventName = request.getParameter("eventName");
String eventName = "";//KM



%>



<%

//SV, 11/05, enable upload from eventbrowser - Select Events screen if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}
//if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") ||(request.getParameter("fromPage")).equals("eventbrowser") ){}
if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") ){
	%>
<jsp:include page="include.jsp" flush="true"/>
<%
}
 else{

%>

<jsp:include page="panel.jsp" flush="true">



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>

<%}%>

<body>

<%
	//SV, 11/05, allow upload from eventbrowser. if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
//if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") || (request.getParameter("fromPage")).equals("eventbrowser")){
    if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
%>
		<DIV  class="popDefault" id="div1">
		<br>
	<%	}

else { %>

<DIV class="formDefault" id="div1">

<%}%>

<%

	String duration = request.getParameter("duration");

	String protocolId = request.getParameter("protocolId");

	String calledFrom = request.getParameter("calledFrom");
	String eventId = request.getParameter("eventId");

	String docmode = request.getParameter("docmode");

	String mode = request.getParameter("mode");

	String calStatus = request.getParameter("calStatus");

	String eventmode = request.getParameter("eventmode");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");


	String docname = "";

	String docDesc = "";

	String docId = "";
	//Virendra: Fixed Bug no. 4734, added int addAnotherWidth = 0;
	 int addAnotherWidth = 0;


	HttpSession tSession = request.getSession(true);



	if (sessionmaint.isValidSession(tSession))

	{

	   String uName = (String) tSession.getValue("userName");

	   String ipAdd = (String) tSession.getValue("ipAdd");

         String usr = (String) tSession.getValue("userId");

	   String accountId=(String) tSession.getValue("accountId");

	   String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");

	   String calAssoc = request.getParameter("calassoc");
	   calAssoc = (calAssoc == null) ? "" : calAssoc;


	CtrlDao ctrlDao = new CtrlDao();
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accountId));


	if (calledFrom.equals("P")||calledFrom.equals("L"))
    {
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }

    }else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }


     }

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary")  )
	{
	%>
	<!-- P class="sectionHeadings"> Event Library >> Event Appendix >> Upload Document </P-->
	<%
	}else{
		String calName = (String) tSession.getValue("protocolname");
	%>
	<P class="sectionHeadings"> <%Object[] arguments = {calName}; %><%=VelosResourceBundle.getMessageString("M_PcolCalEvt_UploadDocu",arguments)%><%--Protocol Calendar [ <%=calName%> ] >> Event Appendix >> Upload Document*****--%> </P>
	<%}%>


<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="displayDur" value="<%=displayDur%>"/>
<jsp:param name="displayType" value="<%=displayType%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
<jsp:param name="calassoc" value="<%=calAssoc%>"/>
</jsp:include>

<%
	//KM-#3207
  if (docmode.equals("N"))
 {
	com.aithent.file.uploadDownload.Configuration.readSettings("sch");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "eventapndx");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;
	%>
  <form name=upload id="uploadevtfile" action=<%=upld%> METHOD=POST ENCTYPE=multipart/form-data onSubmit="if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
%>
<%--
  else
  {
	mileApndxId = (String) request.getParameter("mileApndxId");
	mileApndx.setMileApndxId(com.velos.eres.service.util.EJBUtil.stringToNum(mileApndxId));
	mileApndx.getMileApndxDetails();
	mileApndxDesc = mileApndx.getDesc();

  %>
  <form name="upload" action="milefileupdate.jsp" METHOD=POST onSubmit=" return validate(document.upload)">
  <%
  }
--%>

<TABLE width="90%">

      <tr>

        <td>

          <P class = "defComments"> <%=MC.M_AddDocOrFrm_PcolEvt%><%--Add documents/forms to your Protocol's Event.*****--%> </P>

        </td>

      </tr>

    </table>

    <table width="100%" >

      <tr>

        <td class=tdDefault width="35%"> <%=LC.L_File%><%--File*****--%> <FONT class="Mandatory">* </FONT> </td>

        <td width="65%">

          <input type=file name=name MAXLENGTH=255 size=50>
		  <input type=hidden name=db value='sch'>
		  <input type=hidden name=module value='eventapndx'>
		  <input type=hidden name=tableName value='SCH_DOCS'>
		  <input type=hidden name=columnName value='DOC'>
		  <input type=hidden name=pkValue value='0'>
		  <input type=hidden name=pkColumnName value='PK_DOCS'>
  		  <input type=hidden name=pkColumnName value='PK_DOCS'>


        </td>

      </tr>

      <tr>

        <td width="35%"> </td>

        <td width="65%">

          <P class="defComments"> <%=MC.M_Specify_FullFilePath%><%--Specify full path of the file.*****--%>

        </td>

      </tr>

      <tr>

        <td class=tdDefault width="35%"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT>

        </td>

        <td width="65%">

          <input type=text name=desc MAXLENGTH=100 size=40>

	 <input type=hidden name=userId value=<%=usr%>>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
 	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>>
	 <input type=hidden name=ipAdd  value=<%=ipAdd%>>
    <input type=hidden name=type value=F>
	<input type=hidden name=eventId value='<%=eventId%>'>
	  <input type=hidden name=calassoc value=<%=calAssoc%>>
  <input type=hidden name=propagateMode value='N'>

   <input type=hidden name=protocolId value=<%=protocolId%>>

	<input type=hidden name=nextPage value='../../velos/jsp/eventappendix.jsp?srcmenu=tdmenubaritem3&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>'>



        </td>

      </tr>

      <tr>

        <td width="35%"> </td>

        <td width="65%">

          <P class="defComments">  <%=MC.M_ShortDescFile_100CharMax%><%--Give a short description of your file (100 char max.)*****--%>

 </P>

        </td>

      </tr>

</table>

<jsp:include page="propagateEventUpdate.jsp" flush="true">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="upload"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>

<table width="100%" cellspacing="0" cellpadding="0">
	<tr bgcolor="#cccccc" align="center">
		<%if ((request.getParameter("fromPage")).equals("selectEvent")){ %>
		<td>
			<button onclick="window.history.back();"><%=LC.L_Back%></button>
		</td>
		<%}%>
		<!-- Virendra: Fixed Bug no. 4734, added td with addAnotherWidth  -->
		<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
	 			<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="uploadevtfile"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="noBR" value="Y"/>
			</jsp:include>
		</td>
	</tr>
</table>


</form>



<%



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%
//SV, 11/05, upload from eventbrowser as well.  if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){}
if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt") || (request.getParameter("fromPage")).equals("eventbrowser")){}
else {

%>





</div>

<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>
<div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>
</html>





