<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<script>
</script>



<BODY>
<jsp:useBean id="alnot" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,java.util.*,com.velos.eres.service.util.MC"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%
 HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))
 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String eSign = request.getParameter("eSign");
	String usr;
	int successVal = -1;
	usr = (String) tSession.getValue("userId");

	String oldESign = (String) tSession.getValue("eSign");
	String ipAdd = (String) tSession.getValue("ipAdd");


	String studyId = request.getParameter("studyId");
	String protocolId = request.getParameter("protocolId");
	String src = request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");
	String from = request.getParameter("from");

	successVal = alnot.updateStudyAlertNotify(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId),EJBUtil.stringToNum(usr),ipAdd,"G",1);

	if (successVal > 0)
	{
%>
<br><br><br><br>
<p class="sectionHeadings" align=center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=studynotification.jsp?srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&from=<%=from%>">

<%}
	else
	{
	%>
		<br><br><br><br>
		<p class="sectionHeadings" align=center> <%=MC.M_DataCnt_SvdSucc%><%--Data could not be saved successfully*****--%> </p>
	<%
	} // end of data not save else


} //end of if for session
else{%>
<jsp:include page="timeout.html" flush="true"/>
<%
}%>





</BODY>
</HTML>
