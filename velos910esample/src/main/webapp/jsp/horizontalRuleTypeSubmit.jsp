<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Horizontal_RuleSubmit%><%-- Horizontal Rule Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>


<body><%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.FieldLibBean,com.velos.eres.service.util.*"%>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
	
		
			int errorCode=0;
			String mode=request.getParameter("mode");
			String fieldLibId="";
			
			String fldInstructions="";
			String fldType="H";
		
			String accountId="";
			String fldLibFlag="F";
			String fldBrowserFlg="0" ;
			
			
			String formId="";
			String formFldId="";
			String formSecId="";
			String fldSeq="";
			String fldName="";
		
			String creator="";
			String ipAdd="";
		
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");
			
			mode=request.getParameter("mode");
			String recordType=mode;  
			
			
						
			
	
			formId=request.getParameter("formId");
			fldInstructions=request.getParameter("instructions");
			fldType="H"; //edit box type to be entered by us 
			formSecId=request.getParameter("section");
			fldSeq=request.getParameter("sequence");
			
			FieldLibBean fieldLsk = new FieldLibBean();
			
			fieldLsk.setFormId(formId );
			
			
			fieldLsk.setFldInstructions(fldInstructions);
			fieldLsk.setFldName("Line Break");
			fieldLsk.setFldType(fldType);
			fieldLsk.setIpAdd(ipAdd);
			fieldLsk.setRecordType(recordType);
			fieldLsk.setAccountId(accountId);
			fieldLsk.setLibFlag(fldLibFlag);
			fieldLsk.setFormFldBrowserFlg(fldBrowserFlg);
			fieldLsk.setFormSecId(formSecId );
			fieldLsk.setFormFldSeq( fldSeq );
			
			// ///////////
			String fldAlign="";
			String samLinePrevFld="";
			String fldBold="";
			String fldItalics="";
			Style aStyle = new Style(); 

			if (fldAlign==null)
			aStyle.setAlign("0");

			if (fldBold==null)
			aStyle.setBold("0");
			else if (fldBold.equals("1"))
			aStyle.setBold("1");

			if (fldItalics==null)  
			aStyle.setItalics("0");
			else if (fldItalics.equals("1"))
			aStyle.setItalics("1");
			
			
			if (samLinePrevFld==null)
			aStyle.setSameLine("0");
			else if (samLinePrevFld.equals("1"))
			aStyle.setSameLine("1");
			////////////////////////
			fieldLsk.setAStyle(aStyle);
			
			
			if (mode.equals("N"))
			{
				
				fieldLsk.setCreator(creator);
				errorCode=fieldLibJB.insertToFormField(fieldLsk);
				
			}

			else if (mode.equals("M"))
			{
			
		
				
				formFldId=request.getParameter("formFldId");
				formFieldJB.setFormFieldId(EJBUtil.stringToNum(formFldId));
				formFieldJB.getFormFieldDetails();
				fieldLibId=formFieldJB.getFieldId();
				
			
				fieldLibJB.setFieldLibId(EJBUtil.stringToNum(fieldLibId) );
				fieldLibJB.getFieldLibDetails();
				fieldLsk  = fieldLibJB.createFieldLibStateKeeper();
				
				
				fieldLsk.setFormId(formId );
				fieldLsk.setFormFldId(formFldId );
				fieldLsk.setFldType(fldType);
				fieldLsk.setIpAdd(ipAdd);
				fieldLsk.setRecordType(recordType);
				fieldLsk.setAccountId(accountId);
				fieldLsk.setLibFlag(fldLibFlag);
			
				fieldLsk.setModifiedBy(creator);
				fieldLsk.setFormSecId(formSecId );
				fieldLsk.setFormFldSeq( fldSeq );
				
				errorCode=fieldLibJB.updateToFormField(fieldLsk);
				
						
		 }
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>
  	
<%             if (errorCode == -3 )
				{
%>
				
				<br><br><br><br><br>
				<p class = "successfulmsg" > <%=MC.M_FldIdExst_EtrDiff%><%-- The Field ID already exists. Please enter a different Field ID*****--%></p>
				<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
				
				
<%
				} else 
%>				
				<br><br><br><br><br>
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%-- Data not saved successfully*****--%> </p>

<%
		
		} //end of if for Error Code

		else

		{
%>	
		
<%
		
%>		
		<br><br><br><br><br>
		<p class = "successfulmsg"><%=MC.M_Data_SavedSucc%><%-- Data Saved Successfully*****--%> </p>
		<script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
		</script>		
			
<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
