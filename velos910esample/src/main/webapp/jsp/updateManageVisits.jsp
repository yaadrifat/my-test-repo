<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil,com.velos.esch.web.protvisit.ProtVisitJB,com.velos.esch.business.common.*" %>
<%@page import="org.json.*"%>
<%@ page import = "com.velos.eres.service.util.MC,com.aithent.audittrail.reports.AuditUtils,com.velos.esch.web.protvisit.ProtVisitResponse,com.velos.eres.service.util.LC"%>
<%@ page import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.VelosResourceBundle"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<%
	response.setContentType("application/json");
	String visitJSONString = request.getParameter("myVisitGrid");
	String updateString = request.getParameter("updateInfo");
	String deleteString = request.getParameter("deleteInfo");
	String src = request.getParameter("srcmenu");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
	String calStatus = request.getParameter("calstatus");

	String visitId = "";
	String from = request.getParameter("from");
	int new_protocol_duration = 0;
	String duration = request.getParameter("duration");
	String calAssoc = request.getParameter("calassoc");
	request.setCharacterEncoding("UTF-8");

	calAssoc = (calAssoc == null) ? "" : calAssoc;
	int ret = 0;
	JSONObject jsObj = new JSONObject();
	HttpSession tSession = request.getSession(true);

	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn/*"User is not logged in."*****/);
		out.println(jsObj.toString());
		return;
	}

	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -9);
		jsObj
				.put("resultMsg",
						MC.M_EtrWrgEsign_Svg/*"You entered a wrong e-signature. Please try saving again."*****/);
		out.println(jsObj.toString());
		return;
	}

	JSONArray visitArray = new JSONArray(visitJSONString);
	JSONArray updateArray = new JSONArray(updateString);
	String ip_add = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String accountId = (String) tSession.getValue("accountId");
	protVisitB.setProtocol_id(EJBUtil.stringToNum(protocolId));
	protVisitB.setCreator(usr);
	protVisitB.setip_add(ip_add);
	JSONObject gridRecord = null;
	ArrayList wrongVisitArray = new ArrayList();
	ArrayList wrongVisitNameArray = new ArrayList();
	
	//Modified for INF-18183 and BUG#7224 : Raviesh
	ProtVisitResponse visitResponse = protVisitB.manageVisits(
			visitJSONString, updateString, StringUtil.stringToNum(duration), calledFrom,calStatus,deleteString,AuditUtils.createArgs(session,"",LC.L_Cal_MngVsts));					
	ret = visitResponse.getVisitId();
	visitId = (new Integer(ret)).toString();
	if ((calledFrom.equals("P")) || (calledFrom.equals("S"))) {
		if (ret == -7) {
			jsObj.put("result", -7);
			jsObj.put("resultMsg",LC.L_Row/*"Row"*****/+" #"+ visitResponse.getRowNumber()+ ":"+MC.M_CalcIntervalExceedsDuration);
			out.println(jsObj.toString());
			return;
		}
		else if (ret == -9) {
			jsObj.put("result", -9);
			jsObj.put("resultMsg",LC.L_Row/*"Row"*****/+" #"+ visitResponse.getRowNumber()+ ":"+MC.M_MaxIntervalExceedsDuration);
			out.println(jsObj.toString());
			return;
		}else if (ret == -8) {
			jsObj.put("result", -8);
			jsObj.put("resultMsg", LC.L_Row/*"Row"*****/+" #"
					+ visitResponse.getRowNumber() + ":"
					+ MC.M_DayZeroExceedsDuration);
			out.println(jsObj.toString());
			return;
		}else if (ret == -12) { //Ak:Fixed BUG#6995
			SchCodeDao cd = new SchCodeDao();
			String calStatusDesc = cd.getCodeDescription(cd.getCodeId("calStatStd","O"));
			jsObj.put("result", -12);
			jsObj.put("resultMsg", LC.L_Row/*"Row"*****/+" #"
					+ visitResponse.getRowNumber() + ":"
					+ VelosResourceBundle.getMessageString("M_OfflineCalculatedVisitDayZero",calStatusDesc));
			out.println(jsObj.toString());
			return;
		}
		else if (ret == -13) { 
			SchCodeDao cd = new SchCodeDao();
			String calStatusDesc = cd.getCodeDescription(cd.getCodeId("calStatStd","O"));
			jsObj.put("result", -13);
			jsObj.put("resultMsg", LC.L_Row/*"Row"*****/+" #"
					+ visitResponse.getRowNumber() + ":"
					+ VelosResourceBundle.getMessageString("M_OfflineVisitDayZero",calStatusDesc));
			out.println(jsObj.toString());
			return;
		}
		else if(ret == -5) { //Ak:Fixed BUG#7085
	    	jsObj.put("result", -5);
			jsObj.put("resultMsg", LC.L_Row +" #"
					+ visitResponse.getRowNumber() + ":"
					+ MC.M_VstNoIntvlDef_CantSet);
			out.println(jsObj.toString());
			return;
        }
		//D-FIN-25-DAY0
		else if (ret == -1) {
			jsObj.put("result", -1);
			jsObj.put("resultMsg",LC.L_Row/*"Row"*****/+" #"+ visitResponse.getRowNumber()+ ":"+MC.M_VstNameDupli_ChgName/*"Visit Name is duplicate,Change the Name."*****/);
			out.println(jsObj.toString());
			return;
		} 
		else if (ret == -2) {
			jsObj.put("result", -2);
			jsObj.put("resultMsg",LC.L_Row/*"Row"*****/+" #"+ visitResponse.getRowNumber()+ ":"+MC.M_VisitInterval_BackBtn/*"Visit intervals Month1,week1,day7 and day0 can not coexist."*****/);
			out.println(jsObj.toString());
			return;
		}else {

			//tSession.putValue("visitname",visit_name);
			if (ret >= 0) {
				new_protocol_duration = protVisitB.generateRipple(
						new Integer(visitId).intValue(), calledFrom);
				if (new_protocol_duration > 0) //if ripple succeeds
				{
					tSession.setAttribute("newduration", String
							.valueOf(new_protocol_duration));
				} else {
					tSession.setAttribute("newduration", duration);
				}
				//Ak:Added the code for PCAL-20801 enhancement
				HashMap copyingVistMap=visitResponse.getCopyVisitMap();
			    Iterator it = copyingVistMap.entrySet().iterator();
			    while (it.hasNext()) {
				        Map.Entry visitlst = (Map.Entry)it.next();
				        protVisitB.copyVisit(EJBUtil.stringToNum(protocolId),(Integer)visitlst.getValue(),(Integer)visitlst.getKey(),calledFrom,usr,ip_add);
				}
			    //Added By Yogendra For Bug# 10053
			    session.removeAttribute("wrongVisitCalender");
				session.removeAttribute("wrongVisitData");
				session.removeAttribute("wrongVisitArray");
				session.removeAttribute("wrongVisitNameArray");
				JSONArray visitData = new JSONArray();
				JSONObject jsObjTemp1=null;
				for(int iX=0;iX<visitArray.length();iX++)
				{
					int wrngVisit;
					String visitName;
					String insertAfterVisit;
					String noVisitList="";
					String insertAfterInterval="";
					String intervalUnit="";
					gridRecord = visitArray.getJSONObject(iX);
					noVisitList = gridRecord.getString("insertAfterId");
					if(noVisitList.equalsIgnoreCase("-1/-1"))
					{	wrngVisit = gridRecord.getInt("visitId");
						visitName = gridRecord.getString("visitName");
						insertAfterVisit = gridRecord.getString("insertAfter");
						System.out.print(insertAfterVisit.indexOf("</FONT>"));
						if(insertAfterVisit.indexOf("</FONT>")>0){
							insertAfterVisit=insertAfterVisit.substring((insertAfterVisit.indexOf(">")+1),insertAfterVisit.indexOf("</FONT>"));
						}
						insertAfterInterval = gridRecord.getString("insertAfterInterval");
						intervalUnit = gridRecord.getString("intervalUnit");
						      if(!wrongVisitArray.contains(wrngVisit))
						      {
						    	  wrongVisitArray.add(wrngVisit);
						      }
						      if(!wrongVisitNameArray.contains(visitName))
						      {
						    	wrongVisitNameArray.add(visitName);
						      }
					  jsObjTemp1 = new JSONObject();
					  jsObjTemp1.put("visitId", wrngVisit);
					  jsObjTemp1.put("visitName", visitName);
					  jsObjTemp1.put("insertAfterVisit", insertAfterVisit);
					  jsObjTemp1.put("insertAfterInterval", insertAfterInterval);
					  jsObjTemp1.put("intervalUnit", intervalUnit);
					  visitData.put(jsObjTemp1);
					}
				
				}
				if(wrongVisitArray.size()>0 || wrongVisitNameArray.size()>0 )
				{ 
					session.setAttribute("wrongVisitCalender",protocolId);
					session.setAttribute("wrongVisitData",visitData);
					session.setAttribute("wrongVisitArray",wrongVisitArray);
					session.setAttribute("wrongVisitNameArray",wrongVisitNameArray);
				}

			}

		}
	}
	jsObj.put("result", 0);
	jsObj.put("resultMsg", MC.M_Changes_SavedSucc/*"Changes saved successfully"*****/);
	out.println(jsObj.toString());
%>





