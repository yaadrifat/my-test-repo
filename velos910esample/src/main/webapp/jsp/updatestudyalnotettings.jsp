<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>  
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="alertnotifyB" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.esch.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
<%

int ret=2;
String src =request.getParameter("srcmenu");   
String eSign = request.getParameter("eSign");
String studyId=request.getParameter("studyId");
String from = request.getParameter("from");
String selectedTab = request.getParameter("selectedTab");



HttpSession tSession = request.getSession(true); 


 if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
	} else {
	
	String[] enteredByIds = request.getParameterValues("enteredByIds");
	String[] userMobs =request.getParameterValues("userMobs");
	String[] anIds=request.getParameterValues("anIds");
	String[] alertTypes= new String[anIds.length];
	
	String enteredBy= request.getParameter("enteredBy");
	String protocolId= request.getParameter("protocolId");  
	String globalFlag = request.getParameter("globalFlag");
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");
	String msg="";
	
//	out.println(alertTypes.length);
//	out.println(anIds.length);

	for(int i=0;i<anIds.length;i++)
	{
	String alertType="alertTypes" + (i+1);	
	alertType=request.getParameter(alertType);
	if(alertType==null)
	{
	alertTypes[i]="0";	
	}else{
	alertTypes[i]="1";
	}

//	alertTypes[i]="0"; 
//	out.println(anIds[i]);
//	out.println(alertTypes[i]);
//	out.println(enteredByIds [i]+ "*");
//	out.println(userMobs[i]+"*");
//	out.println(usr);
//	out.println(ipAdd);
//	out.println("anIds" + anIds.length);
//	out.println("alertTypes" +alertTypes.length);
//	out.println("enteredByIds" +enteredByIds.length);
//	out.println("userMobs" +userMobs.length);
//	out.println(usr);
//	out.println(ipAdd); 

	}

	//out.println("STUDY ID"+studyId);
	//out.println("PROTOCOL ID"+protocolId);

	ret=alertnotifyB.updateAlertNotifyList(anIds,alertTypes ,enteredByIds ,userMobs, usr ,ipAdd,studyId,protocolId );

   	if (ret >= 0) {
	%>
		
		<br>
		<br>
		<br>
		<br>
		<br>
		<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%-- Data was Saved successfully.*****--%> </p>
	
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyalnotsettings.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=M&globalFlag=<%=globalFlag%>&studyId=<%=studyId%>&protocolId=<%=protocolId%>&from=<%=from%>">
		
	<%
	}
	else
	{
	%>		
		<br>
		<br>
		<br>
		<br>
		<br>
		<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_SvdSucc%><%-- Data could not be saved successfully.*****--%> </p>
	<%
		}		
	%>		

<%
} //end of if for eSign check

} else {   

%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%> 
</BODY>
</HTML>
