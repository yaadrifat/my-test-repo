<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	
<BODY>
   
  <%@ page language = "java" import = " oracle.sql.CLOB,java.sql.*,java.util.*,com.velos.eres.service.util.*, com.velos.eres.business.common.*"%>
  <%

	// System.out.println("Hello I M In updateReasonModule");
	String pkcolumnid[] = request.getParameterValues("PK_COLUMN_ID");
	String remarks[] = request.getParameterValues("remarks");
	String fkcodelstreason[] = request.getParameterValues("fk_codelst_reason");
	
	String eSign = request.getParameter("eSign");	
 
	//System.out.println("pkcolumnid:- "+ pkcolumnid);
	//System.out.println("remarks:- "+ remarks);
	//System.out.println("fkcodelstreason:- "+ fkcodelstreason);
	
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

%>

<%
	int length = 0;
	
	if (pkcolumnid != null)
	{
		length = pkcolumnid.length;
	}	
		Connection conn = CommonDAO.getConnection();		 
	    conn.setAutoCommit(false);	              
      	CallableStatement cs = null;    	
        cs = (CallableStatement) conn.prepareCall("begin esch.PKG_AUDIT_TRAIL_MODULE.SP_UPDATECOLUMN(?,?,?,?,?); end;");
		   
	for (int ctr = 0; ctr <	length ; ctr++)
	{
		        

        try {
		    cs.setObject(1, remarks[ctr]);
			cs.setObject(2, "STRING");
            cs.setObject(3, "esch.audit_column_module");
            cs.setObject(4, "remarks");
	        cs.setObject(5, " where PK_COLUMN_ID = " + pkcolumnid[ctr] );
    		cs.execute();
        
        } catch (Exception e) {
            Rlog.fatal("common",
                    "EJBUtil:Exception thrown in updating form auditcol remarks "
                            + e.getMessage());
            System.out.println("message:"+e);
             
        }
		
		//-----Added by Rakesh To update the "Reason" column in the Audit Table
        try {
			cs.setObject(1, fkcodelstreason[ctr]);
			cs.setObject(2, "INTEGER");
            cs.setObject(3, "esch.audit_column_module");
            cs.setObject(4, "FK_CODELST_REASON");
	        cs.setObject(5, " where PK_COLUMN_ID = " + pkcolumnid[ctr] );
    		cs.execute();
        
        } catch (Exception e) {
            Rlog.fatal("common",
                    "EJBUtil:Exception thrown in updating form auditcol Fk Codelst Reason "
                            + e.getMessage());
            System.out.println("message:"+e);
             
        }
		
	}
	
	try
	{
			conn.commit();
            // Close the Statement object
            cs.close();
            conn.close();
	} catch(Exception ex)
				{
					Rlog.fatal("common",
                    "EJBUtil:Exception thrown in closing connections"
                            + ex.getMessage());
            	System.out.println("message:"+ex);
            	
				}
	 
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	
 <p align="center"><A href="#" onClick="window.self.close();"><img src="../images/jpg/close.jpg" border=0></A></p>

	  
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>