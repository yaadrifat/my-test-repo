<%@page import="com.velos.eres.business.common.MileAchievedDao"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@page import="com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil,
com.velos.eres.business.common.InvoiceDetailDao,com.velos.eres.business.common.CodeDao"%>
<%@page import="java.util.ArrayList,java.util.Enumeration,java.util.Date"%>
<%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
<jsp:useBean id="InvDetB" scope="request" class="com.velos.eres.web.invoice.InvoiceDetailJB"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD></HEAD>
<BODY>
<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}

	Enumeration params = request.getParameterNames();
	ArrayList mileAchiveList = new ArrayList();
	ArrayList milestoneList = new ArrayList();
	ArrayList milestoneAmtList = new ArrayList();
	ArrayList prevInvoiceAmtList = new ArrayList();
	ArrayList invoiceAmtList = new ArrayList();
	ArrayList doneList = new ArrayList();
	
	while (params.hasMoreElements()) {
	    String param = (String)params.nextElement();
	    if (param.startsWith("invAmt")) {
	    	String mileAchieveId = "";
	    	String milestoneId = "";
	    	mileAchieveId = param.substring(6, param.indexOf("_mile")-1);
	    	milestoneId = param.substring(param.indexOf("_mile")+5);
	    	
	    	String paramVal = (String)request.getParameter(param);
	    	JSONObject jsObjAmount = new JSONObject((String)request.getParameter(param));
	    	double invoiceAmount = Double.parseDouble(""+jsObjAmount.get("invoiceAmount"));
	    	if (invoiceAmount > 0){
	        	mileAchiveList.add(jsObjAmount.get("mileAchieveId"));
	        	milestoneList.add(jsObjAmount.get("milestoneId"));
	        	milestoneAmtList.add(jsObjAmount.get("mileAmount"));
	        	prevInvoiceAmtList.add(jsObjAmount.get("prevInvoicedAmount"));
	        	invoiceAmtList.add(jsObjAmount.get("invoiceAmount"));
	        	doneList.add("0");
	    	}
	    }
	}
	
	String ipAdd = (String) tSession.getValue("ipAdd");
    String userId = (String) tSession.getAttribute("userId");
	String accountId = (String) tSession.getAttribute("accountId");
	String studyId = request.getParameter("studyId");
	
	String invNumber = request.getParameter("invNumber");
	String paymentDueIn = request.getParameter("paymentDueIn");
	String paymentDueUnit = request.getParameter("paymentDueUnit");
	String addressedTo = request.getParameter("addressedTo");
	String sentFrom = request.getParameter("sentFrom");
	String invDate = request.getParameter("invDate");
	String milestoneReceivableStatus = request.getParameter("milestoneReceivableStatus");
	String dateRangeType = request.getParameter("dateRangeType");
	String invNotes = request.getParameter("invNotes");
	String dtFilterDateFrom = request.getParameter("dtFilterDateFrom");
	String dtFilterDateTo = request.getParameter("dtFilterDateTo");
	String dSites = request.getParameter("dSites");
	String coverType = request.getParameter("coverType");
	if (StringUtil.isEmpty(coverType) || "null".equals(coverType)){
		coverType = null;
	}
	
	int invoicePK = 0;
	int ret = 0;
	
	if (mileAchiveList.size() == 0) {
		jsObj.put("result", 0);
		jsObj.put("resultMsg", MC.M_No_OperPerformed);/*jsObj.put("resultMsg", "No operations were performed");*****/
		out.println(jsObj.toString());
		return;
	}
	
    try {
    	if(invNumber==null || invNumber.equals("")){
			invNumber = InvB.getInvoiceNumber();
		}
		InvB.setInvAddressedto(addressedTo);
		InvB.setInvDate(invDate);
		InvB.setInvIntervalFrom(dtFilterDateFrom) ;
		InvB.setInvIntervalTo(dtFilterDateTo);
		InvB.setInvIntervalType(dateRangeType) ;
		InvB.setInvMileStatusType(milestoneReceivableStatus) ;
		InvB.setInvNotes(invNotes);
		InvB.setInvNumber(invNumber) ;
		//setInvOtherUser(iBean.getInvOtherUser()) ;
		InvB.setInvSentFrom(sentFrom) ;
		InvB.setInvPayDueBy(paymentDueIn);
		InvB.setInvPayUnit(paymentDueUnit);
		InvB.setCreator(userId);
		InvB.setIPAdd(ipAdd);
		InvB.setStudy(studyId);
		InvB.setSite(dSites);
		InvB.setInvStat("W");
		InvB.setCovTyp(coverType);
		InvB.setInvoiceDetails();
		invoicePK = InvB.getId();


		//---------------JM: 21Feb2008 added for #FIN11: Feb2008 enhancements-----------
		String moduleId="";
        CodeDao codedao = new CodeDao();
	    int codeLstId = 0;
	    String codeLstStatId="";

	    Date dt = new java.util.Date();
		String stDate = DateUtil.dateToString(dt);
		codeLstId = codedao.getCodeId("invStatus","W");
		codeLstStatId= ""+ codeLstId;
		moduleId=invoicePK+"";

		statusB.setStatusModuleId(moduleId);
		statusB.setStatusModuleTable("er_invoice");
		statusB.setStatusCodelstId(codeLstStatId);
		statusB.setStatusStartDate(stDate);
		statusB.setIpAdd(ipAdd);

		statusB.setCreator(userId);
		statusB.setRecordType("N");
		statusB.setStatusHistoryDetailsWithEndDate();

		//---------------JM: 21Feb2008-----------
	
		ret = InvDetB.generateInvoiceDetails(invoicePK, StringUtil.stringToNum(studyId), StringUtil.stringToNum(userId), ipAdd, 
	    		mileAchiveList,  milestoneList, milestoneAmtList, invoiceAmtList);
		
	    jsObj.put("result", 0);
	    jsObj.put("resultMsg", MC.M_Changes_SavedSucc);/*jsObj.put("resultMsg", "Changes saved successfully");*****/
		//out.println(jsObj.toString());
	    
	} catch(Exception e) {
		jsObj.put("result", -3);
		jsObj.put("resultMsg", e);
		//out.println(jsObj.toString());
		return;
	}

%>
	<%if (ret > 0){%>
		<BR><BR><BR><BR>
		<p class = "sectionHeadings" align = center> <%=jsObj.get("resultMsg")%> </p>
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>">
		
		<script>
			windowName = window.open("viewInvoice.jsp?invPk=<%=invoicePK%>&CoverType=<%=coverType%>", "Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=530,left=125,top=200");
			windowName.focus();
		</script>
	<%} %>

	</BODY>
</HTML>