<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<P>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY > 
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.*"%> 
<%
 int iret = -1;
 HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
		   String userId = (String) tSession.getValue("userId");		   
		   String ipAdd =  (String) tSession.getValue("ipAdd");	
		   String calledFrom = request.getParameter("calledFrom");
		   if (calledFrom==null) calledFrom = "-";		   	  
		   
		   String cmbView =request.getParameter("cmbView");		
		   
		   String[] statusId =request.getParameterValues("eventstatus");
		   String[] oldStatusId= request.getParameterValues("oldStatusId");
		   String[] note=request.getParameterValues("note");
		   String[] exeon=request.getParameterValues("exeon");
		   String enrollId = request.getParameter("patProtId");
		   String studyId=request.getParameter("studyId");
	       String arrCheck= request.getParameter("arrCheck");

		   int len = oldStatusId.length;
		  
		   int ret=0;
		   int j=0;
			String[] strChk = new String[len];
		   StringTokenizer saCheck = new StringTokenizer(arrCheck,",");
			int chkNum = saCheck.countTokens();
			 for(int count=0;count<chkNum;count++)
			 {
				  strChk[j]= saCheck.nextToken();
				  j++;
			 }



		  if(cmbView.equals("visit") || cmbView.equals("")  ){
			String[]  chkVst = request.getParameterValues("chkVst");
			String[] eveIds = request.getParameterValues("eveIds");
			 
			int viewLen = chkVst.length;	
			String[] strEventIds = new String[viewLen];
			int chkIds=0;
			int totalIds=0; 
		    int index=0;
			int m=0;
			for(int i=0 ; i<chkNum ; i++)
			 {
				if(strChk[i].equals("1"))
				 {
					strEventIds[index] = eveIds[i];
					StringTokenizer saIds = new StringTokenizer(strEventIds[index],":");
					chkIds = saIds.countTokens();
					for(m=0;m<chkIds;m++)
					 {
						totalIds++;					 
					 }				
			
				}
			 }
		 String[] chkStatusId =new String[totalIds];
		 String[] chkNote=new String[totalIds];
		 String[] chkExeon=new String[totalIds];
		 String[] chkOldStatus = new String[totalIds];
		 String[] chkEventIds = new String[totalIds];
		 int cnt=0;
	    int indx=0;
	 for(int i=0 ; i<chkNum ; i++)
	 {
		if(strChk[i].equals("1"))
		 {
		
			strEventIds[cnt] = eveIds[i];
			StringTokenizer saIds = new StringTokenizer(strEventIds[cnt],":");
			chkIds = saIds.countTokens();
			for(m=0;m<chkIds;m++)
			 {
				chkEventIds[indx] = saIds.nextToken();
				chkStatusId[indx] = statusId[i];
				chkExeon[indx]= exeon[i];
				chkNote[indx] = note[i];
			    chkOldStatus[indx] = oldStatusId[i];
				indx++;
			 }
			cnt++;		
		}
	 }
	
		
		ret = eventdefB.saveMultipleEventsVisits(EJBUtil.stringToNum(enrollId),EJBUtil.stringToNum(userId),chkEventIds,chkStatusId,chkOldStatus,chkExeon,chkNote,ipAdd); 
	}

	else if( cmbView.equals("event")){
		String[] chkEvent =request.getParameterValues("chkEvent");		
		int viewLen = chkEvent.length;		
		 String[] strStatusId =new String[viewLen];
		 String[] strNote=new String[viewLen];
		 String[] strExeon=new String[viewLen];
		 String[] strOldStatus = new String[viewLen];
		 String[] strEventIds = new String[viewLen];
		
	
	int cnt=0;
	 for(int i=0 ; i<chkNum ; i++)
	 {
		if(strChk[i].equals("1"))
		 {
			strStatusId[cnt] = statusId[i];
			strExeon[cnt] = exeon[i];
			strNote[cnt] = note[i];
		    strOldStatus[cnt] = oldStatusId[i];
		  
			cnt++;
		
		}
	 }
	
	
	
		ret = eventdefB.saveMultipleEventsVisits(EJBUtil.stringToNum(enrollId),EJBUtil.stringToNum(userId),chkEvent,strStatusId,strOldStatus,strExeon,strNote,ipAdd); 
	
	
	}
	if ( ret!= 0)  {
			
				%> 
				<br><br><br><br><br>
					<p class = "sectionHeadings" align = center> <%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%>
					</p>
					<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
				<% return;
				} 
			
			else {%>
				<br><br><br><br><br><p class = "sectionHeadings" align = center>
				<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>	
				<script>
			 		window.opener.location.reload();
					setTimeout("self.close()",1000);
				</script>			
						
			<%}


	 
	   }
		else {

	%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
		} 
	}
	
	else
	{
	%>
	  <jsp:include page="timeout.html" flush="true"/>
	  <%
	}
	%>

	</BODY>

	</HTML>

