<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--File Modified For :Bug#: 9525 :  By YPS --%>
<script type="text/javascript" src="js/calendar/calendar.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss_skin.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<!--RK <SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT> -->

<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/prototype.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/dojo/dojo.js"></SCRIPT>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
<jsp:include page="jqueryUtils.jsp"></jsp:include> 
<jsp:include page="sessionwarning.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/fixedHeaders.js"></SCRIPT>

<link rel="shortcut icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" media="all" href="js/calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />
 <!-- main calendar program -->


  <!-- language for the calendar -->
  <script type="text/javascript" src="js/calendar/lang/calendar-en.js"></script>

  <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
  <script type="text/javascript" src="js/calendar/calendar-setup.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<div id="notification" style="visibility:hidden"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<script>toggleNotification('on',"");</script>
<%

HttpSession tSession = request.getSession(true);
tSession = request.getSession(true);

String skin = "default";
if (sessionmaint.isValidSession(tSession))
	{
	 UserJB userB1 = (UserJB) tSession.getValue("currentUser");
	String accName = (String) tSession.getValue("accName");
	String accSkinId = "";
	String usersSkinId = "";
	String userSkin = "";
	String accSkin = "";
	
	accSkinId = 	(String) tSession.getValue("accSkin");
	usersSkinId = userB1.getUserSkin();
	userSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(usersSkinId) );


	accSkin= codeLst.getCodeCustomCol(EJBUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){

		accSkin = "accSkin";

	}
	else{


		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){

		skin = userSkin;

	}

	//	System.out.println("*******skinname...."+skin);
	}

%>
<script> whichcss_skin("<%=skin%>");</script>
<input type="hidden" name="accSkin" id="accSkin" value="<%=skin%>">
</body>
</html>