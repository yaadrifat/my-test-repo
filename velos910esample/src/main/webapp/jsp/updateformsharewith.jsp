<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>

<jsp:useBean id="objShareB" scope="request" class="com.velos.eres.web.objectShare.ObjectShareJB"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import ="com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*, com.velos.eres.service.util.EJBUtil" %>
<%
 int objShareId = 0;
 
 String mode =request.getParameter("mode");
 
 String eSign = request.getParameter("eSign");
 
 HttpSession tSession = request.getSession(true); 
 
 if (sessionmaint.isValidSession(tSession)) {

%>
	
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {

%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
else
	{

	String accId = (String) tSession.getValue("accountId");
    String ipAdd = (String) tSession.getValue("ipAdd");
    String usr = (String) tSession.getValue("userId");
    String src;
    src= request.getParameter("srcmenu");
    
    String formId = request.getParameter("formId");
    String codeStatus =request.getParameter("codeStatus");
    String calledFrom=request.getParameter("calledFrom");
	String lnkFrom=request.getParameter("lnkFrom");
	String selectedTab = request.getParameter("selectedTab");
	
 	if(lnkFrom==null) 
	 {lnkFrom="-";}
	String studyId=request.getParameter("studyId");

    String sharedWith="";
    String sharedWithIds="";
    int ret=0;

	sharedWith=request.getParameter("rbSharedWith");
	formlibB.setFormLibId(EJBUtil.stringToNum(formId));
	formlibB.getFormLibDetails();
	formlibB.setFormLibSharedWith(sharedWith);		
	 ret=formlibB.updateFormLib();
	
	if (ret>=0) {
		
		sharedWithIds="";
		String objNumber =request.getParameter("objNumber");
 	    String fkObj = request.getParameter("fkObj");


		if(sharedWith.equals("P"))
			{
			sharedWithIds=request.getParameter("selUsrId");
			}
	   else if(sharedWith.equals("A"))
			{
			sharedWithIds=request.getParameter("selAccId");
			}
		else if(sharedWith.equals("G"))
			{
			sharedWithIds=request.getParameter("selGrpIds");
			}
		else if(sharedWith.equals("S"))
			{
			sharedWithIds=request.getParameter("selStudyIds");
			}
		else if(sharedWith.equals("O"))
			{
			sharedWithIds=request.getParameter("selOrgIds");
			}
	
		    fkObj=formId;
		    objShareB.setIpAdd(ipAdd);
         	objShareB.setCreator(usr);
         	objShareB.setObjNumber(objNumber);//number of module
         	objShareB.setFkObj(fkObj);// formId, CalId, reportId
         	objShareB.setObjSharedId(sharedWithIds);// shared ids
         	objShareB.setObjSharedType(sharedWith);// U-user id,A -account id,S - study id,G - group id,O - organization id 
         	objShareB.setRecordType(mode);
         	objShareB.setModifiedBy(usr);	
         	ret = objShareB.setObjectShareDetails();
	}	
	
	if (ret==0 ) {%>
		<br><br><br><br><br><p class = "successfulmsg" align = center>
		<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>	
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=formsettings.jsp?calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&mode=<%=mode%>&codeStatus=<%=codeStatus%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>">		
	<%} else
      {
      %>
      <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd%><%--Data could not be saved.*****--%></p>
      <%
      }
	}
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>

