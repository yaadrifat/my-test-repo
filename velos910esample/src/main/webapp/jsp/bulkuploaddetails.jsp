<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title>Bulk Upload Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
</head>
<jsp:include page="ui-include.jsp" flush="true"/>
<script>
var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 650 : 500;
jQuery.noConflict();
function openSequenceDialog() //YK: Added for PCAL-20461 - Opens the Dialog Window
{
		jQuery("#embedDataHere").dialog({ 
		height: defaultHeight,width: 700,position: 'right' ,modal: true,
		closeText:'',
		buttons: [
		          {
		              text: L_Close,
		              click: function() { 
		        	  jQuery(this).dialog("close");
		              }
		          }
		      ],
		close: function() { $j("#embedDataHere").dialog("destroy"); },
		beforeClose: function(event, ui) { if(!checkBeforeClose()){ return false;} }
        });
  
}
function loadModifySequenceDialog(mapPk,temName) {
			openSequenceDialog(); 
		    jQuery('#embedDataHere').load("bulkUploadpreviewMapping.jsp?mapPk="+mapPk+"&temName="+temName);	  
	}
function checkBeforeClose()
{
//alert("");
//var i=0;
//for(i=0;i<10;i++)
//document.getElementById('embedDataHere').innerHTML = i ;
	return true;
}
function checkAll(formobj){
    act="check";
 	totcount=formobj.totcount.value;
    if (formobj.chkAll.value=="checked")
		act="uncheck" ;
    if (totcount==1){
       if (act=="uncheck")
		   formobj.delMap.checked =false ;
       else
		   formobj.delMap.checked =true ;
	}
    else {
         for (i=0;i<totcount;i++){
             if (act=="uncheck")
				 formobj.delMap[i].checked=false;
             else
				 formobj.delMap[i].checked=true;
         }
    }
    if (act=="uncheck")
		formobj.chkAll.value="unchecked";
    else
		formobj.chkAll.value="checked";
}
</script>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="com.velos.eres.bulkupload.business.*,java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page  language = "java" import="com.velos.eres.bulkupload.business.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<br>
<DIV class="tabDefTopN" id="div1">
		<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		</jsp:include>
</DIV>
  <%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{

ArrayList pkTemplate=null;
ArrayList fkBulkEntity=null;
ArrayList<String> templateName=null;
ArrayList createdOn=null;
ArrayList fkUser=null;
String userName="";
String userId="";
long rowsReturned = 0;
ArrayList<String> usrFirstName=null;
ArrayList<String> usrMidName=null;
ArrayList<String> usrLastName=null;
userId = (String) tSession.getValue("userId");
BulkTemplateDao bulkTemDao=new BulkTemplateDao();
bulkTemDao.getUserTemplates(userId);
pkTemplate=bulkTemDao.getPkTemplate();
fkBulkEntity=bulkTemDao.getFkBulkEntity();
templateName=bulkTemDao.getTemplateName();
createdOn=bulkTemDao.getCreatedOn();
fkUser=bulkTemDao.getFkUser();
usrFirstName=bulkTemDao.getUsrFirstName();
usrMidName=bulkTemDao.getUsrMidName();
usrLastName=bulkTemDao.getUsrLastName();
   	%>
<DIV class="tabFormTopN tabFormTopN_PAS"  id="div2" style="overflow:visible">
<s:form action="bulkMappingDel" method="post" name="bulkMappingDel">    
	<table   cellspacing="0" cellpadding="0" border="0" >
    
      
		 
			 <tr >
	 <td><font class="Mandatory"><s:if test="hasActionErrors()">
   <div class="errors">
      <s:actionerror/>
   </div>
</s:if></font></td>
       
		    <tr>
<%if(!pkTemplate.isEmpty()){
	 %>
	 	 
		 </table>
		 
		<table    cellspacing="0" cellpadding="0" border="0" >
		<tr >
		<td colspan="2"><font class="sectionHeadingsFrm"> Saved Mappings </font></td>	
			<td> <input type="submit" name="delMapping" value="Delete Selected"/><BR/><BR/></td>
        </tr>
	 	<tr>
		<th>Mapping Name</th>
		<th>Created By</th>
		<th align="center"><input type="checkbox" name="chkAll" value="" onClick="checkAll(document.bulkMappingDel)"></th></td>
		</tr>
	   <%for (int errSize=0;errSize<pkTemplate.size();errSize++)  
	   {
	    rowsReturned++;
	   if(!usrFirstName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrFirstName.get(errSize)))
    		userName=usrFirstName.get(errSize);
		}
		if(!usrMidName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrMidName.get(errSize)))
    		userName=userName+" "+usrMidName.get(errSize);
		}
		if(!usrLastName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrLastName.get(errSize)))
    		userName=userName+" "+usrLastName.get(errSize);
		}
	   %>
	   <%String temNm=(String)templateName.get(errSize);
	   temNm=temNm.replace(" ","*****").trim();
	   %>
		<tr >
	     <td><a href="#" onClick="return loadModifySequenceDialog('<%=pkTemplate.get(errSize)%>','<%=temNm%>')"><%=templateName.get(errSize)%></a></td>
		 
		 <td><%=userName%></td>
		 <td align="center"><input type="checkbox" name="delMap" value="<%=pkTemplate.get(errSize)%>"></td>
         </tr>
	  <%}%>
	  </table>
	   <%}%>
		 <input type="hidden" name="totcount" Value="<%=rowsReturned%>">
			</tr>
       
      </table>
	  </div>
	  <div id="editVisitDiv" title="Upload Data" style="display:none">
<div id='embedDataHere'>

</div>
</div>
</s:form>
	
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <% 
}
%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>
</html>