<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>

<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>

 <%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%

	HttpSession tSession = request.getSession(true); 

    if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String src = request.getParameter("srcmenu");
	String formId = request.getParameter("formId");
	String selectedTab = request.getParameter("selectedTab");
	String mode = request.getParameter("mode");		
	
	String studyId=request.getParameter("studyId");	
	String formFldId=request.getParameter("formFldId");
	String otherFldId= request.getParameter("otherFormFldId");
	if (otherFldId==null) otherFldId="0";
	int otherFormFldId= EJBUtil.stringToNum(otherFldId);
	
	String formFldSeq = request.getParameter("formFldSeq");
	String otherSeq = request.getParameter("otherSeq");	
	String calledFrom = request.getParameter("calledFrom");
	String codeStatus = request.getParameter("codeStatus");
	String lnkFrom = request.getParameter("lnkFrom");
	
	calledFrom = calledFrom.trim();
	
	
	//swap the sequences
	if (otherFormFldId > 0) {
	
    	formFieldJB.setFormFieldId(EJBUtil.stringToNum(formFldId));
    	formFieldJB.getFormFieldDetails();
        formFieldJB.setFormFldSeq(otherSeq);
    	int ret = formFieldJB.updateFormField();

    
    	formFieldJB.setFormFieldId(otherFormFldId);
    	formFieldJB.getFormFieldDetails();
        formFieldJB.setFormFldSeq(formFldSeq);
    	ret = formFieldJB.updateFormField();

	}
%>	
<META HTTP-EQUIV=Refresh CONTENT="0; URL=formfldbrowser.jsp?codeStatus=<%=codeStatus%>&lnkFrom=<%=lnkFrom%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&mode=<%=mode%>&studyId=<%=studyId%>">
<%
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





