<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<%@ page import="com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,java.util.*,com.velos.eres.business.common.TeamDao" %>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings,com.velos.eres.service.util.*"%>




    
<%
String mode="N";
String study=""; 
int studyId = 0;	
String selclass="";
String tab= request.getParameter("selectedTab");
study=request.getParameter("studyId");
studyId = EJBUtil.stringToNum(study);
String uName="";
String studyNumber = "";
String studyTitle ="";
 
HttpSession tSession = request.getSession(true); 

String acc = (String) tSession.getValue("accountId");
 int mileRight= 0;
 int pageRight= 0;
if (sessionmaint.isValidSession(tSession))
{
	uName = (String) tSession.getValue("userName");
	
	studyB.setId(studyId);
	studyB.getStudyDetails();
	studyNumber = studyB.getStudyNumber();
	studyTitle = studyB.getStudyTitle();	

	if (studyTitle==null) studyTitle="-";
   
	ArrayList teamId ;

	String userIdFromSession = (String) tSession.getValue("userId");
	teamId = new ArrayList();
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();

	if (teamId.size() <=0)
	{
		mileRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	if (teamId.size() == 0) {
		mileRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
	 	ArrayList teamRights ;
					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRightsB.loadStudyRights();


		if ((stdRightsB.getFtrRights().size()) == 0){
			mileRight = 0;
		}else{
			mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
		}
	}

}

	ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "milestone_tab");

out.print("<Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\">");

%>

<DIV>
<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">  --> 
<table cellspacing="0" cellpadding="0" border="0"> 
	<tr>

<%
	int mileGrpRight =0;
	
	GrpRightsJB grprightsB = (GrpRightsJB) tSession.getValue("GRights");
	
	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		mileGrpRight = Integer.parseInt(grprightsB.getFtrRightsByValue("MILEST"));
	 if (mileRight > 0 && mileGrpRight >= 4)
	 {		 showThisTab = true; 	}
	}
	else if ("2".equals(settings.getObjSubType())) {
		if (mileRight > 0)
				showThisTab = true; 
	}
	else if ("3".equals(settings.getObjSubType())) {
		if (mileRight > 0)
				showThisTab = true;  
	}
	else if ("4".equals(settings.getObjSubType())) {
		if (mileRight > 0)
				showThisTab = true; 
	} 
	else if ("7".equals(settings.getObjSubType())) {
			if (mileRight > 0)
				showThisTab = true; 
   }
	else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; } 


	if (tab == null) { 
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType())) {
		selclass = "selectedTab";
	} else {
		selclass = "unselectedTab";
	}
	 %>	

		<td  valign="TOP">
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>
				<!--    	<td class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td>  -->
					<td class="<%=selclass%>">
					<%
					if ("1".equals(settings.getObjSubType())) {
					%>
						<a href="milestone.jsp?srcmenu=tdmenubaritem7&selectedTab=1&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("2".equals(settings.getObjSubType())) {  %>
						<a href="milestonenotifications.jsp?studyId=<%=study%>&selectedTab=2&srcmenu=tdmenubaritem7"><%=settings.getObjDispTxt()%></a>

					<%} else if ("3".equals(settings.getObjSubType())) {%>
						<a href="mileapndx.jsp?srcmenu=tdMenuBarItem7&selectedTab=3&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%} else if ("4".equals(settings.getObjSubType())) { %>
						<a href="milepaymentbrowser.jsp?srcmenu=tdMenuBarItem7&selectedTab=4&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%>	</a>
					<%} else if ("7".equals(settings.getObjSubType())) { %>
						<a href="invoicebrowser.jsp?srcmenu=tdmenubaritem7&selectedTab=7&studyId=<%=studyId%>"><%=settings.getObjDispTxt()%></a>
					<%}%>
					
					</td> 
				<!--     <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td> -->
			  	</tr> 
		   	</table> 
        </td> 
		


	
			
			<!--commented by sonia abrol, 01/04/2006, milestone reports will be accessed from reports central --> 
			<!--<td valign="TOP"> 
			<% if (tab.equals("5"))
	    	{
	     		selclass= "selectedTab";
		    } 
		    else
		    {
				selclass = "unselectedTab";
			}%>
			
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>  
				   	<td class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td> 
					<td class="<%=selclass%>">
						<a href="repmilestone.jsp?srcmenu=tdMenuBarItem7&selectedTab=5&studyId=<%=studyId%>">Reports</a>
					</td> 
			        <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td>
			   	</tr> 
		   	</table> 
		</td> 

		<td valign="TOP"> 
			<% if (tab.equals("6"))
	    	{
	     		selclass= "selectedTab";
		    } 
		    else
		    {
				selclass = "unselectedTab";
			}%>
			
			<table class="<%=selclass%>"  cellspacing="0" cellpadding="0" border="0">
				<tr>  
				   	<td class="<%=selclass%>" rowspan="3" valign="top" >
						<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
					</td> 
					<td class="<%=selclass%>">
						<a href="savedrepbrowser.jsp?srcmenu=tdMenuBarItem7&selectedTab=6&studyId=<%=studyId%>">Saved Reports</a>
					</td> 
			        <td class="<%=selclass%>" rowspan="3" valign="top">
						<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
			        </td>
			   	</tr> 
		   	</table> 
		</td>  -->

<%}%>
	</tr>
 <!--   <tr>
     <td colspan=5 height=10></td>
  </tr>   -->
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>

 <%	
 //Modified by Manimaran to display full title in mouseover.
   studyTitle = StringUtil.escapeSpecialCharJS(studyTitle); 
 //Modified By Parminder Singh Bug  #10388
   studyTitle=StringUtil.stripScript(studyTitle);
 %>
 
 <script language="javascript">
	 var varViewTitle = htmlEncode('<%=studyTitle%>');
 </script>
		
    <table width="100%" cellspacing="0" cellpadding="0" class="patHeader">
	<tr>
		<td> <font size="1"><B><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:<%=studyNumber%>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:  <a href="#"  onmouseover="return overlib(varViewTitle,CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><img border="0" src="./images/View.gif" title="<%=LC.L_View%><%--View*****--%>"/></a>  </B></font></td>
	</tr>
</table>

</DIV>

