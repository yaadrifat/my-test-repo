<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Form Filled Browser</title>

<script>

//function openWin(formId,studyId)
//{
//	windowName= window.open("studyformdetails.jsp?formId="+formId+"&mode=N&studyId="+studyId+"&formDispLocation=S","information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=500");
	//windowName.focus();
//}

function checkPerm(pageRight)
{

	if (f_check_perm(pageRight,'N') == true)
	{
		return true ;

	}
	else
	{
		return false;
	}
}


</script>

<%
String src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBeanid="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="lnkformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration"%>
<%@ page import="com.velos.eres.service.util.LC"%>
<br>

<div class="browserDefault" id="div1">

<%
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {

    int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATFRMSACC"));
	String modRight = (String) tSession.getValue("modRight");
	int patProfileSeq = 0, formLibSeq = 0;

	// To check for the account level rights
	acmod.getControlValues("module");
	 ArrayList acmodfeature =  acmod.getCValue();
	 ArrayList acmodftrSeq = acmod.getCSeq();
	 char formLibAppRight = '0';
	 char patProfileAppRight = '0';
	 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
	 formLibSeq = acmodfeature.indexOf("MODFORMS");
	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
	 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
	 formLibAppRight = modRight.charAt(formLibSeq - 1);
	 patProfileAppRight = modRight.charAt(patProfileSeq - 1);

	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
	{
	    String studyId=request.getParameter("studyId");
        String statDesc=request.getParameter("statDesc");
        String statid=request.getParameter("statid");
        String patProtId=request.getParameter("patProtId");
        String patientCode=request.getParameter("patientCode");
        String mode = request.getParameter("mode");
        int patId=EJBUtil.stringToNum(request.getParameter("pkey"));
		String selectedTab = request.getParameter("selectedTab");

		String linkedFormId = request.getParameter("linkedFormId");
		String formId=request.getParameter("formId");;

		 //date filter
	  	 String formFillDt = "";
	 	 formFillDt = request.getParameter("formFillDt") ;
 	 	 if (formFillDt == null) formFillDt = "ALL";
		 String dDateFilter = EJBUtil.getRelativeTimeDD("formFillDt",formFillDt);
	%>
	 <P class="sectionHeadings"> Manage <%=LC.Pat_Patients%> >> Schedule >> Linked Forms</P>

     <jsp:include page="patienttabs.jsp" flush="true">
     <jsp:param name="pkey" value="<%=patId%>"/>
     <jsp:param name="statDesc" value="<%=statDesc%>"/>
     <jsp:param name="statid" value="<%=statid%>"/>
     <jsp:param name="patProtId" value="<%=patProtId%>"/>
     <jsp:param name="patientCode" value="<%=patientCode%>"/>
     <jsp:param name="fromPage" value=""/>
     <jsp:param name="page" value="patientEnroll"/>
     </jsp:include>

	<%

		 if (formId==null) {
		 	 lnkformsB.setLinkedFormId(EJBUtil.stringToNum(linkedFormId));
		  	 lnkformsB.getLinkedFormDetails();
		 	 formId = lnkformsB.getFormLibId();
		 }
	   formLibB.setFormLibId(EJBUtil.stringToNum(formId));
	   formLibB.getFormLibDetails();
	   String formName = formLibB.getFormLibName();
	   String entryChar= request.getParameter("entryChar");
 	   String fkcrf = request.getParameter("fkcrf");
       String schevent = request.getParameter("schevent");
	   String linkFrom = "C";
	   String hrefName = "formfilledcrfbrowser.jsp?srcmenu="+src;
	   String modifyPageName = "crfformdetails.jsp?srcmenu="+src;
	%>
	<br>
	<Form method="post" action="formfilledcrfbrowser.jsp" onsubmit="">
		<input type="hidden" name="srcmenu" value=<%=src%>>
		<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
		<input type="hidden" name="studyId" value=<%=studyId%>>
		<input type="hidden" name="statDesc" value=<%=statDesc%>>
		<input type="hidden" name="statid" value=<%=statid%>>
		<input type="hidden" name="patProtId" value=<%=patProtId%>>
		<input type="hidden" name="patientCode" value=<%=patientCode%>>
		<input type="hidden" name="mode" value=<%=mode%>>
		<input type="hidden" name="pkey" value=<%=patId%>>
		<input type="hidden" name="linkedFormId" value=<%=linkedFormId%>>
		<input type="hidden" name="fkcrf" value=<%=fkcrf%>>
		<input type="hidden" name="schevent" value=<%=schevent%>>
		<input type="hidden" name="entryChar" value=<%=entryChar%>>

	  <table width="100%">
	  <tr>
	  <td width="70%">
  	    <P class="blackComments"><b> Form Name: </b><%=formName%></P>
	  </td>
	  <td width="25%"><P class="blackComments">Filter By Date &nbsp;  <%=dDateFilter%> </P></td>
	  <td width="5%"><button type="submit"><%=LC.L_Search%></button></td>
	  </tr>
	  </table>
	  </Form>


	<% if(entryChar.equals("M")){%>
	    <A href="crfformdetails.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&linkedFormId=<%=linkedFormId%>&formId=<%=formId%>&mode=N&formDispLocation=C&entryChar=<%=entryChar%>&schevent=<%=schevent%>&fkcrf=<%=fkcrf%>&pkey=<%=patId%>&patProtId=<%=patProtId%>&studyId=<%=studyId%>&calledFrom=S&statDesc=<%=statDesc%>&statid=<%=statid%>&patientCode=<%=patientCode%>&formFillDt=<%=formFillDt%>" onClick ="return checkPerm(<%=pageRight%>) ">Add New</A>
	<%}%>

	<!--include the jsp for showing the records-->
	 <jsp:include page="formdatarecords.jsp" flush="true">
	 <jsp:param name="linkFrom" value="<%=linkFrom%>"/>
	 <jsp:param name="hrefName" value="<%=hrefName%>"/>
	 <jsp:param name="entryChar" value="<%=entryChar%>"/>
	 <jsp:param name="modifyPageName" value="<%=modifyPageName%>"/>
	 <jsp:param name="fkcrf" value="<%=fkcrf%>"/>
	 <jsp:param name="schevent" value="<%=schevent%>"/>
 	 <jsp:param name="formId" value="<%=formId%>"/>
 	 <jsp:param name="formFillDt" value="<%=formFillDt%>"/>
   	 <jsp:param name="srcmenu" value="<%=src%>"/>
   	 <jsp:param name="pageRight" value="<%=pageRight%>"/>
	 </jsp:include>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

</body>
</html>

