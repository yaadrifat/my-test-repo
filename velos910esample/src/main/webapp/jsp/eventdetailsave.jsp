<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>

<Link Rel="STYLESHEET" HREF="common.css" type="text/css">
<%response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>
<head></head>

<jsp:include page="include.jsp" flush="true"/>
<BODY>
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="eventdefB" scope="request"
	class="com.velos.esch.web.eventdef.EventdefJB" />
<jsp:useBean id="eventassocB" scope="request"
	class="com.velos.esch.web.eventassoc.EventAssocJB" />
<%@ page language="java" import="com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB"%>

	<div class = "myHomebottomPanel"> 
		<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
<%String src = request.getParameter("srcmenu");
            String duration = request.getParameter("duration");
            String protocolId = request.getParameter("protocolId");
            String calledFrom = request.getParameter("calledFrom");
            String eventmode = request.getParameter("eventmode");
            String fromPage = request.getParameter("fromPage");
            String mode = request.getParameter("mode");
            String calStatus = request.getParameter("calStatus");
            String displayDur = request.getParameter("displayDur");
            String displayType = request.getParameter("displayType");

            String propagateInVisitFlag = request
                    .getParameter("propagateInVisitFlag"); //SV, 9/16/04, cal-enh-05
            String propagateInEventFlag = request
                    .getParameter("propagateInEventFlag");

            String categoryId = "";
            String eventId = "";
            String eSign = request.getParameter("eSign");

            String ename = "";
            int ret = 0;
            ename = request.getParameter("eventName");

            String eventDesc = "";
            String eventNotes = "";
            eventDesc = request.getParameter("eventDesc");
            eventDesc = (eventDesc == null) ? "" : (eventDesc);

            eventNotes = request.getParameter("eventNotes");
            eventNotes = (eventNotes == null) ? "" : (eventNotes);

            String eventSequence= request.getParameter("eventSeq");
            eventSequence = (eventSequence==null) ? "" : (eventSequence);

            String offlineFalg ="";//JM: 28Apr2008, added for, Enh. #C9

            String oldOfflineFlag= request.getParameter("offLineFlag");
            oldOfflineFlag = (oldOfflineFlag==null) ? "" : (oldOfflineFlag);


			//JM: 25Jun2008, added for, #3531
    		String calledFromPg = request.getParameter("calledFromPage");
    		calledFromPg = (calledFromPg==null)?"":calledFromPg;

			String patientId = request.getParameter("patId");
			patientId = (patientId==null)?"":patientId;


			String patProtKey = request.getParameter("patProtId");
			patProtKey = (patProtKey==null)?"":patProtKey;

			String dsplcmnt = request.getParameter("displacement");
			dsplcmnt = (dsplcmnt==null)?"0":dsplcmnt;

			String visitNumber = request.getParameter("visitId");
    		visitNumber = (visitNumber==null)?"":visitNumber;

			String eventLibType = "";
			eventLibType = request.getParameter("cmbLibType");
			
			int eventSOSId = 0;
			eventSOSId = EJBUtil.stringToNum(request.getParameter("ddSos"));
			String strEventSOSId = "" + eventSOSId;
			
			int eventFacilityId = 0;
			eventFacilityId = EJBUtil.stringToNum(request.getParameter("ddFacility"));
			String strEventFacilityId = "" + eventFacilityId;			
			
			int eventCoverType = 0;
			eventCoverType = EJBUtil.stringToNum(request.getParameter("ddCoverage"));
			String strEventCoverType = "" + eventCoverType;
			
            %>
<%HttpSession tSession = request.getSession(true);

            if (sessionmaint.isValidSession(tSession)) {

                %>
<jsp:include page="sessionlogging.jsp" flush="true" />
<%String oldESign = (String) tSession.getValue("eSign");

                if (!oldESign.equals(eSign)) {

                %>
<jsp:include page="incorrectesign.jsp" flush="true" />
<%} else {

                    String ipAdd = (String) tSession.getValue("ipAdd");
                    String usr = (String) tSession.getValue("userId");

                    String accountId = (String) tSession.getValue("accountId");
                    categoryId = request.getParameter("categoryId");
					String evtLineCat ="";
                    if (eventmode.equals("N")) {
                        categoryId = request.getParameter("categoryId");
						eventdefB.setEvent_id(EJBUtil.stringToNum(categoryId));
                        eventdefB.getEventdefDetails();
						evtLineCat = eventdefB.getEventLineCategory();

                        eventdefB.setChain_id(categoryId);
						eventdefB.setEventLineCategory(evtLineCat);
                    } else {
                        eventId = request.getParameter("eventId");
                        if ((calledFrom.equals("P")) || (calledFrom.equals("L"))
                                || (calledFrom.equals("S") && fromPage.equals("selectEvent"))) {
                            eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
                            eventdefB.getEventdefDetails();
                        } else {
                            eventassocB.setEvent_id(EJBUtil
                                    .stringToNum(eventId));
                            eventassocB.getEventAssocDetails();
                        }
                    } //mode = 'N' .. else

                    int eventDuration = EJBUtil.stringToNum(request
                            .getParameter("eventDuration"));
                    String eventDurDays = request.getParameter("eventDurDays");
                    /*	if(request.getParameter("eventDurDays").equals("Weeks"))
                     {
                     eventDuration = eventDuration*7;
                     }*/

                    String eventFuzzyPerDays = request
                            .getParameter("eventFuzzyPerDays");
                    eventFuzzyPerDays = (eventFuzzyPerDays == null) ? ""
                            : eventFuzzyPerDays;
                    if (eventFuzzyPerDays.length() == 0) {
                        eventFuzzyPerDays = "0";
                    }
                    String eventFuzzyPerDaysA = request
                            .getParameter("eventFuzzyPerDaysA");
                    eventFuzzyPerDaysA = (eventFuzzyPerDaysA == null) ? ""
                            : eventFuzzyPerDaysA;
                    if (eventFuzzyPerDaysA.length() == 0) {
                        eventFuzzyPerDaysA = "0";
                    }

                    String durUnitB = request.getParameter("durUnitB");
                    durUnitB = (durUnitB == null) ? "" : durUnitB;

                    String durUnitA = request.getParameter("durUnitA");
                    durUnitA = (durUnitA == null) ? "" : durUnitA;

                    String cptCode = request.getParameter("cptcode");
                    cptCode = (cptCode == null) ? "" : cptCode;

                    String calAssoc = request.getParameter("calassoc");
                    calAssoc = (calAssoc == null) ? "" : calAssoc;

					String evtName = "";
					if (eventmode.equals("N")) {


                    	eventdefB.setEventSequence(eventSequence); //JM:03Apr2008
                        evtName = request.getParameter("eventName");  //KM:06Aug09
						if (evtName.length() > 4000)
						{
						   	evtName = evtName.substring(0,4000);
						}
						eventdefB.setName(evtName);
                        eventdefB.setDescription(eventDesc);
                        eventdefB.setDuration((new Integer(eventDuration))
                                .toString());
                        eventdefB.setDurationUnit(eventDurDays);
                        eventdefB.setFuzzy_period(eventFuzzyPerDays);
                        eventdefB.setFuzzyAfter(eventFuzzyPerDaysA);
                        eventdefB.setDurationUnitAfter(durUnitA);
                        eventdefB.setDurationUnitBefore(durUnitB);
                        eventdefB.setCptCode(cptCode);
                        eventdefB.setNotes(eventNotes);
                        eventdefB.setUser_id(accountId);
                        eventdefB.setEvent_type("E");
						eventdefB.setEventLibType(eventLibType);
						
                        eventdefB.setEventSOSId(strEventSOSId);
                        eventdefB.setEventFacilityId(strEventFacilityId);
                        eventdefB.setEventCoverageType(strEventCoverType);
                        
                        eventdefB.setCreator(usr);
                        eventdefB.setIpAdd(ipAdd);

                        eventdefB.setEventdefDetails();

                        ret = eventdefB.getEvent_id();
                        eventId = (new Integer(eventdefB.getEvent_id()))
                                .toString();
                        eventmode = "M";

                    }

                    else { //event mode = 'M'
                        if (calledFrom.equals("P") || calledFrom.equals("L")
                                ||(calledFrom.equals("S") && fromPage.equals("selectEvent"))) {

                            eventdefB.setEventSequence(eventSequence); //JM:03Apr2008
							evtName = request.getParameter("eventName");
							if (evtName.length() > 4000)
							{
						    	evtName = evtName.substring(0,4000);
							}
                            eventdefB.setName(evtName);
                            eventdefB.setDescription(eventDesc);
                            eventdefB.setDuration((new Integer(eventDuration)).toString());
                            eventdefB.setDurationUnit(eventDurDays);
                            eventdefB.setFuzzy_period(eventFuzzyPerDays);
                            eventdefB.setFuzzyAfter(eventFuzzyPerDaysA);
                            eventdefB.setDurationUnitAfter(durUnitA);
                            eventdefB.setDurationUnitBefore(durUnitB);
                            eventdefB.setCptCode(cptCode);
							eventdefB.setEventLibType(eventLibType);
                            
                            eventdefB.setEventSOSId(strEventSOSId);
                            eventdefB.setEventFacilityId(strEventFacilityId);
                            eventdefB.setEventCoverageType(strEventCoverType);
                            
                            eventdefB.setNotes(eventNotes);
                            eventdefB.setModifiedBy(usr);
                            eventdefB.setIpAdd(ipAdd);
                            
                            System.out.println("fromPage=" + fromPage);
                            ret = eventdefB.updateEventdef();

                        } else {
                        	eventassocB.setEventSequence(eventSequence); //JM:03Apr2008
                            evtName = request.getParameter("eventName");
							if (evtName.length() > 4000)
							{
						    	evtName = evtName.substring(0,4000);
							}
							eventassocB.setName(evtName);
                            eventassocB.setDescription(eventDesc);
                            eventassocB
                                    .setDuration((new Integer(eventDuration))
                                            .toString());
                            eventassocB.setDurationUnit(eventDurDays);
                            eventassocB.setFuzzy_period(eventFuzzyPerDays);
                            eventassocB.setFuzzyAfter(eventFuzzyPerDaysA);
                            eventassocB.setDurationUnitAfter(durUnitA);
                            eventassocB.setDurationUnitBefore(durUnitB);
							
                            StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
							int finDetRight=0;
							
							if ((stdRights.getFtrRights().size()) == 0){
								finDetRight=0;
							}else{
								finDetRight=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFIN"));;
							}
							if (StringUtil.isAccessibleFor(finDetRight,'V')){        	  	   
                            	eventassocB.setCptCode(cptCode);
							}

                            eventassocB.setEventSOSId(strEventSOSId);
                            eventassocB.setEventFacilityId(strEventFacilityId);
                            eventassocB.setEventCoverageType(strEventCoverType);
                            
                            eventassocB.setNotes(eventNotes);
                            eventassocB.setModifiedBy(usr);
                            eventassocB.setIpAdd(ipAdd);
                            eventassocB.updateEventAssoc();

				            //JM: 28Apr2008, added for, Enh. #C9
							//if Calendar status is made Offline for editing then if events attributes change make offlineFalg="2" else "1"
							//provided that previous offline flag value =1
				         	if(calStatus.equals("O") && (oldOfflineFlag.equals("1") || oldOfflineFlag.equals("2"))){
				         	 offlineFalg="2";
				         	 eventassocB.updateOfflnFlgForEventsVisits(EJBUtil.stringToNum(eventId), offlineFalg);
				         	}


                        }

                        //SV, 10/12/04, I think, new mode is only relevant when new events are created and thus propagate only in "M" mode.
                        if (fromPage.equals("eventbrowser")
                                || fromPage.equals("fetchProt")) {
                            System.out.println("prop:" + propagateInVisitFlag
                                    + ":" + propagateInVisitFlag);
                            if ((propagateInVisitFlag == null)
                                    || (propagateInVisitFlag.equals("")))
                                propagateInVisitFlag = "N";

                            if ((propagateInEventFlag == null)
                                    || (propagateInEventFlag.equals("")))
                                propagateInEventFlag = "N";

                            System.out.println("prop S:" + propagateInVisitFlag
                                    + ":" + propagateInVisitFlag);

                           System.out.println("prop S:" + propagateInEventFlag
                                    + ":" + propagateInEventFlag);


                            if ((propagateInVisitFlag.equals("Y"))
                                    || (propagateInEventFlag.equals("Y"))) {
                                if (calledFrom.equals("P")) {
                                    ret = eventdefB.propagateEventdef(
                                            propagateInVisitFlag,
                                            propagateInEventFlag);
                                } else if (calledFrom.equals("S")) {
                                    ret = eventassocB.propagateEventUpdates(
                                            propagateInVisitFlag,
                                            propagateInEventFlag);
                                }
                            }
                        }

                    } //mode

                    if (((calledFrom.equals("P")) || (calledFrom.equals("L")) ||  (calledFrom.equals("S")) )   && (ret == -1)) {%>
<br>
<br>
<br>
<br>
<br>
<p class="sectionHeadings" align="center"><%=MC.M_EvtNameDupli_ClkBack%><%-- Event Name is duplicate. Click
on "Back" Button to change the name*****--%> <Br>
<Br>
<button type="Button" name="back" onclick="window.history.back()"><%=LC.L_Back%><%--Back*****--%></button></p>
<%return;
                    }

                    //out.println(request.getParameter("updateFlag"));
                    tSession.putValue("eventname", ename);
                    if (request.getParameter("updateFlag").equals("Add")) {
                        eventmode = "N";
                    }

if (request.getParameter("updateFlag").equals("Submit")) {
%>
<META HTTP-EQUIV="Refresh"
	CONTENT="1; URL=eventdetails.jsp?eventmode=<%=eventmode%>&amp;srcmenu=<%=src%>&amp;selectedTab=1&amp;duration=<%=duration%>&amp;eventId=<%=eventId%>&amp;protocolId=<%=protocolId%>&amp;calledFrom=<%=calledFrom%>&amp;mode=<%=mode%>&amp;fromPage=<%=fromPage%>&amp;calStatus=<%=calStatus%>&amp;eventId=<%=eventId%>&amp;categoryId=<%=categoryId%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>&amp;offLineFlag=<%=oldOfflineFlag%>&calledFromPage=<%=calledFromPg%>&visitId=<%=visitNumber%>&patId=<%=patientId%>&patProtId=<%=patProtKey%>&displacement=<%=dsplcmnt%>">

<%}else if (request.getParameter("updateFlag").equals("Add")) {

//JM: added if else conditions: 02NOV2009: Issue No: 3581

%>

<META HTTP-EQUIV="Refresh"
	CONTENT="1; URL=eventdetails.jsp?srcmenu=<%=src%>&amp;eventmode=N&amp;categoryId=<%=categoryId%>&amp;duration=<%=duration%>&amp;protocolId=<%=protocolId%>&amp;calledFrom=<%=calledFrom%>&amp;mode=<%=mode%>&amp;calStatus=<%=calStatus%>&amp;fromPage=<%=fromPage%>&amp;selectedTab=1">

<%
}
}// end of if body for esign
            }//end of if body for session

            else {

            %>
<jsp:include page="timeout.html" flush="true" />
<%}

        %>


</BODY>
</HTML>


