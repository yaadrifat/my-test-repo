<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="MsB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="MsDao" scope="request" class="com.velos.eres.business.common.MilestoneDao"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*"%>

<%

String protocolId = request.getParameter("protocolId");
String studyId  = request.getParameter("studyId");
String totRows = request.getParameter("ecount");

String eSign = request.getParameter("eSign");	



String rules[] = null;
String events[] = null;
String amounts[] = null;




String count="";
String limit = "";
String patStatus = "";
String paymentType = "";
String paymentFor = "";
String milestoneStat = "";//KM
String eventStatus = "";

String limits[] = null;
String patStatuses[] = null;
String paymentTypes[] = null;
String paymentFors[] = null;
String milestoneStats[] = null;
String counts[] = null;      
String eventStatuses[] = null;  
   	

String msType = "EM";
String delFlag = "N";

int cnt=0;
int ret = 0;

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 		
//	String oldESign = (String) tSession.getValue("eSign");
//	if(!oldESign.equals(eSign)) {
%>
  
<%
//	} else {
	
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

	String amount="";


	String event = "";
	String rule = "";
	int starPos = 0;
	int arrayCounter = 0;
	String eventVisit = "";
		


if (totRows.equals("1")){
     event = request.getParameter("eventid");
	 amount = request.getParameter("amount");

	  

	if(amount.equals(".")){
		amount="0";
	}

	rule = request.getParameter("rule");
	
	count = request.getParameter("count");
	limit = request.getParameter("mLimit");
    patStatus = request.getParameter("dStatus");
    paymentType = request.getParameter("dpayType");
    paymentFor = request.getParameter("dpayFor");
	milestoneStat = request.getParameter("milestoneStat");
    eventStatus = request.getParameter("dEventStatus");
         
     if (StringUtil.isEmpty(count))    
     {
     	count = "-1";
     }
     
     eventassocB.setEvent_id(EJBUtil.stringToNum(event));
	 eventassocB.getEventAssocDetails();
	 eventVisit = eventassocB.getEventVisit();

     
   	MsB.setMilestoneType(msType);
	MsB.setMilestoneStudyId(studyId);				
	MsB.setMilestoneCalId(protocolId);			
	MsB.setMilestoneRuleId(rule);	
	MsB.setMilestoneAmount(amount);	
	MsB.setMilestoneDelFlag("N");	

	MsB.setMilestoneEvent(event);
	MsB.setCreator(usr);
    MsB.setIpAdd(ipAdd);
	MsB.setMilestoneStatFK(milestoneStat);
    MsB.setMilestoneLimit( limit) ;
    MsB.setMilestonePayFor( paymentFor) ;
    MsB.setMilestonePayType( paymentType) ;
    MsB.setMilestoneStatus( patStatus );
    MsB.setMilestoneCount(count);
    
    MsB.setMilestoneEventStatus(eventStatus);
    MsB.setMilestoneVisitFK(eventVisit);


	ret = MsB.setMilestoneDetails();
   
}else {

  	events = request.getParameterValues("eventid");
  	amounts = request.getParameterValues("amount");
    rules = request.getParameterValues("rule");
     counts = request.getParameterValues("count");
    limits = request.getParameterValues("mLimit");
	patStatuses = request.getParameterValues("dStatus");
    paymentTypes = request.getParameterValues("dpayType");
    paymentFors = request.getParameterValues("dpayFor");
	milestoneStats = request.getParameterValues("milestoneStat");
    eventStatuses = request.getParameterValues("dEventStatus");
  	
  
	for (cnt =0;cnt< EJBUtil.stringToNum(totRows); cnt++ )
	{
	
		amount = null;
		event = events[cnt];
		
		if (EJBUtil.stringToNum(event) > 0 )
		{
				
				amount = amounts[cnt];
				if(amount.equals(".")){
					amount="0";
				}
		
				rule = rules[cnt];
				count = counts[cnt];
				limit = limits[cnt];
			    patStatus = patStatuses[cnt];
			    paymentType = paymentTypes[cnt];
			    paymentFor = paymentFors[cnt];
				milestoneStat = milestoneStats[cnt];
				eventStatus = eventStatuses[cnt] ;

			     if (StringUtil.isEmpty(count))    
				     {
				     	count = "-1";
				     }
								
				 
			     eventassocB.setEvent_id(EJBUtil.stringToNum(event));
				 eventassocB.getEventAssocDetails();
				 eventVisit = eventassocB.getEventVisit();
				
				
			   	MsB.setMilestoneType(msType);
				MsB.setMilestoneStudyId(studyId);				
				MsB.setMilestoneCalId(protocolId);			
				MsB.setMilestoneRuleId(rule);	
				MsB.setMilestoneAmount(amount);	
				MsB.setMilestoneDelFlag("N");	
			
				MsB.setMilestoneEvent(event);
				MsB.setCreator(usr);
			    MsB.setIpAdd(ipAdd);
				MsB.setMilestoneStatFK(milestoneStat);//KM

			    MsB.setMilestoneLimit( limit) ;
			    MsB.setMilestonePayFor( paymentFor) ;
			    MsB.setMilestonePayType( paymentType) ; 
			    MsB.setMilestoneStatus( patStatus );
			    MsB.setMilestoneCount(count);
			    
			    MsB.setMilestoneEventStatus(eventStatus);
			    MsB.setMilestoneVisitFK(eventVisit);
			
			
				ret = MsB.setMilestoneDetails();		
		}				
	}
	
} 
%>

<SCRIPT>
   	window.opener.location.reload();
	self.close();
</SCRIPT>	

<br><br><br><br><br>

	 
<p class = "sectionHeadings" align = center><%=MC.M_Data_SavedSucc%><%--Data saved successfully.*****--%></p>

<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


