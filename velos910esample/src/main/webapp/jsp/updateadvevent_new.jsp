<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<jsp:useBean id="advinfoB" scope="request" class="com.velos.esch.web.advInfo.AdvInfoJB"/>
<jsp:useBean id="advnotifyB" scope="request" class="com.velos.esch.web.advNotify.AdvNotifyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*"%>
<%@ page language="java" import="com.velos.esch.audit.web.AuditRowEschJB"%>
<%

   int ret=2;
   int cnt = 0;
   String src =request.getParameter("srcmenu");
   String mode =request.getParameter("mode");
   String eventId= request.getParameter("eventId");
   String adve_type = request.getParameter("adve_type");


   //String adve_severity = request.getParameter("adve_severity");

/*if(adve_severity.equals("")){
out.println("a" +adve_severity + "b");
} */

   //String adve_bdsystem = request.getParameter("adve_bdsystem");

   String adve_relation = request.getParameter("adve_relation");
   if (adve_relation == null  || adve_relation.equals("null") ) adve_relation ="";
   String adve_recovery = request.getParameter("adve_recovery");
   if (adve_recovery == null || adve_recovery.equals("null") ) adve_recovery ="";


   //KM
   String meddracode=request.getParameter("MedDRAcode");
   String advdictionary=request.getParameter("advDictionary");

   String descripton = request.getParameter("descripton");
   //JM: 26Oct2009: 4144
   if (descripton.length()>2000){
 	descripton=descripton.substring(0,2000);

   }
   String treatment = request.getParameter("treatment");
   String startDt = request.getParameter("startDt");
   String stopDt = request.getParameter("stopDt");
   String enteredBy= request.getParameter("enteredBy");
   String reportedBy=request.getParameter("reportedBy");
   reportedBy = (reportedBy==null)?"":reportedBy;

   String discoveryDt = request.getParameter("aediscoveryDt");
   String loggedDt    = request.getParameter("aeloggedDt");
   String linkedToId  = request.getParameter("linkedToName");
   String outcomeString = request.getParameter("outcomeString");
   String outcomeDt     = request.getParameter("outcomeDt");
   String addInfoString = request.getParameter("addInfoString");
   String advNotifyString = request.getParameter("advNotifyString");


   String notes = request.getParameter("notes");
    //JM: 26Oct2009: 4144
   if (notes.length()>4000){
 	notes=notes.substring(0,4000);

   }
   String adveventId=request.getParameter("adveventId");
   String patProtId= request.getParameter("patProtId");
   int advId=0;
   String advNotifyDate = "";
   String advNotifyCodeId = "";
   String advNotifyId = "";
   String studyNum = request.getParameter("studyNum");


   char strIndex='0';
   char strAddInfo = '0';
   String outcomeLen = request.getParameter("outcomeLen");
   String addInfoLen = request.getParameter("addInfoLen");
   String advNotifyLen = request.getParameter("advNotifyLen");
   String outcome= request.getParameter("outaction");
   if (outcome == null || outcome.equals("null")) outcome ="";

   String outcomeIds[] = null;
   String advNotifyCodeIds[] = null;
   String advNotifyIds[] = null;
   String addInfoIds[] = null;
   String advInfoOutIds[] = null;
   String advInfoAddIds[] = null;

   String advInfoOutcomeType = "O";
   String advInfoAddInfo = "A";
   String outcomeId = "";
   String addInfoId = "";
   String advInfoOutId = "";
   String advInfoAddId = "";
   String formStatus = "";




   String death = request.getParameter("death");
   String studyId =request.getParameter("studyId");

   String statDesc=request.getParameter("statDesc");
   String statid=request.getParameter("statid");
   String studyVer=request.getParameter("studyVer");
   String patientCode =request.getParameter("patientCode");

   String advName    =request.getParameter("advName");
   String grade      =request.getParameter("grade");
   String gradeText  =request.getParameter("advGradeText");
   formStatus        =request.getParameter("formStatus");
   if( formStatus == null || formStatus.equals("null")) formStatus = "";



  if (mode.equals("M"))
 {
    advId=EJBUtil.stringToNum(request.getParameter("adveventId"));
  }

//out.println("adveventId" +adveventId);

   String eSign = request.getParameter("eSign");
   String outcomeNotes=request.getParameter("outcomeNotes");

   String msg="";
   String pkey = request.getParameter("pkey");
   String eventName=request.getParameter("eventName");
   String visit=request.getParameter("visit");



HttpSession tSession = request.getSession(true);


 if (sessionmaint.isValidSession(tSession))
   {

 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} else {

 String ipAdd = (String) tSession.getValue("ipAdd");
 String usr = null;
 usr = (String) tSession.getValue("userId");

   	outcomeIds = request.getParameterValues("outcomeId");
	addInfoIds = request.getParameterValues("addInfoId");

	advNotifyCodeIds = request.getParameterValues("advNotifyCodeId");
	advNotifyIds = request.getParameterValues("advNotifyId");

 if (mode.equals("M"))

 {

   adveventB.setAdvEveId(advId);

 }


   adveventB.setAdvEveEvents1Id(eventId);
   adveventB.setAdvEveCodelstAeTypeId(adve_type);

   //adveventB.setAdvEveSeverity(adve_severity);

   //adveventB.setAdvEveBdsystemAff(adve_bdsystem);

   adveventB.setAdvEveRelationship(adve_relation);
   adveventB.setAdvEveRecoveryDesc(adve_recovery);



   //KM
   adveventB.setAdvEveMedDRA(meddracode);
   adveventB.setAdvEveDictionary(advdictionary);

   adveventB.setAdvEveDesc(descripton);
   adveventB.setAdvEveTreatment(treatment);
   adveventB.setAdvEveStDate(startDt);
   adveventB.setAdvEveEndDate(stopDt);

   adveventB.setFkStudy(studyId);
   adveventB.setFkPer(pkey);
   adveventB.setAdvEveEnterBy(enteredBy);

   adveventB.setAdvEveReportedBy(reportedBy);
   adveventB.setAdvEveLinkedTo(linkedToId);
   adveventB.setAdvEveDiscvryDate(discoveryDt);
   adveventB.setAdvEveLoggedDate(loggedDt);


   adveventB.setAdvEveOutType(outcomeString);
   adveventB.setAdvEveOutDate(outcomeDt);
   adveventB.setAdvEveAddInfo(addInfoString);
   adveventB.setAdvEveNotes(notes);
   adveventB.setAdvEveOutNotes(outcomeNotes);
   adveventB.setFormStatus(formStatus);
   adveventB.setFkOutcomeAction(outcome);
   if (gradeText == null)
   {
	   	grade = null ;
   		advName = "";
   } else if (gradeText.trim().equals(""))
   {
	   	grade = null;
   		advName = "";
   }


   adveventB.setAdvEveGrade(grade);
   adveventB.setAdvEveName(advName);


//out.println("mode is "+ mode);


   if (mode.equals("M")) {

	 adveventB.setModifiedBy(usr);
	 adveventB.setIpAdd(ipAdd);
	 ret =adveventB.updateAdvEve();

	String remarks = request.getParameter("remarks");
	if (!StringUtil.isEmpty(remarks)){
		AuditRowEschJB auditJB = new AuditRowEschJB();
		auditJB.setReasonForChangeOfAdverseEvent(advId,StringUtil.stringToNum(usr),remarks);
	}

		advInfoOutIds = request.getParameterValues("advInfoOutId");

	 for(cnt =0;cnt< EJBUtil.stringToNum(outcomeLen); cnt++ ){

		outcomeId = outcomeIds[cnt];
		advInfoOutId = advInfoOutIds[cnt];
		strIndex=(StringUtil.isEmpty(outcomeString))?'0':outcomeString.charAt(cnt);

		advinfoB.setAdvInfoId(EJBUtil.stringToNum(advInfoOutId));
		advinfoB.setAdvInfoAdverseId(request.getParameter("adveventId"));
		advinfoB.setAdvInfoCodelstInfoId(outcomeId);
		advinfoB.setAdvInfoType(advInfoOutcomeType);
		advinfoB.setAdvInfoValue(""+strIndex);
		advinfoB.setCreator(usr);
		advinfoB.setModifiedBy(usr);
		advinfoB.setIpAdd(ipAdd);

		advinfoB.updateAdvInfo();

		if (!StringUtil.isEmpty(remarks)){
			AuditRowEschJB auditJB = new AuditRowEschJB();
			auditJB.setReasonForChangeOfAdverseEventInfo(StringUtil.stringToNum(advInfoOutId),StringUtil.stringToNum(usr),remarks);
		}

	}


		advInfoAddIds = request.getParameterValues("advInfoAddId");
	for(cnt =0;cnt< EJBUtil.stringToNum(addInfoLen); cnt++ ){

		addInfoId = addInfoIds[cnt];
		advInfoAddId = advInfoAddIds[cnt];
		strIndex=(StringUtil.isEmpty(addInfoString))?'0':addInfoString.charAt(cnt);

		advinfoB.setAdvInfoId(EJBUtil.stringToNum(advInfoAddId));
		advinfoB.setAdvInfoAdverseId(request.getParameter("adveventId"));
		advinfoB.setAdvInfoCodelstInfoId(addInfoId);
		advinfoB.setAdvInfoType(advInfoAddInfo);
		advinfoB.setAdvInfoValue(""+strIndex);
		advinfoB.setCreator(usr);
		advinfoB.setModifiedBy(usr);
		advinfoB.setIpAdd(ipAdd);

		advinfoB.updateAdvInfo();

		if (!StringUtil.isEmpty(remarks)){
			AuditRowEschJB auditJB = new AuditRowEschJB();
			auditJB.setReasonForChangeOfAdverseEventInfo(StringUtil.stringToNum(advInfoAddId),StringUtil.stringToNum(usr),remarks);
		}
	}



	for(cnt =0;cnt< EJBUtil.stringToNum(advNotifyLen); cnt++ ){
		advNotifyCodeId = advNotifyCodeIds[cnt];
		advNotifyId = advNotifyIds[cnt];
		strIndex=(StringUtil.isEmpty(advNotifyString))?'0':advNotifyString.charAt(cnt);

		advNotifyDate = "notifyDate"+cnt;
		advNotifyDate = request.getParameter(advNotifyDate);


		advnotifyB.setAdvNotifyId(EJBUtil.stringToNum(advNotifyId));
		advnotifyB.setAdvNotifyAdverseId(request.getParameter("adveventId"));
		advnotifyB.setAdvNotifyCodelstNotTypeId(advNotifyCodeId);
		advnotifyB.setAdvNotifyDate(advNotifyDate);
		//advnotifyB.setAdvNotifyNotes(notes);
		advnotifyB.setAdvNotifyValue(""+strIndex);
		advnotifyB.setCreator(usr);
		advnotifyB.setModifiedBy(usr);
		advnotifyB.setIpAdd(ipAdd);

		advnotifyB.updateAdvNotify();

		if (!StringUtil.isEmpty(remarks)){
			AuditRowEschJB auditJB = new AuditRowEschJB();
			auditJB.setReasonForChangeOfAdverseEventNotify(StringUtil.stringToNum(advNotifyId),StringUtil.stringToNum(usr),remarks);
		}
	}




   }  else {




	adveventB.setCreator(usr);
	adveventB.setIpAdd(ipAdd);
	int advEventId = adveventB.setAdvEveDetails();


	for(cnt =0;cnt< EJBUtil.stringToNum(outcomeLen); cnt++ ){

		outcomeId = outcomeIds[cnt];
		strIndex=(StringUtil.isEmpty(outcomeString))?'0':outcomeString.charAt(cnt);

		advinfoB.setAdvInfoAdverseId((new Integer(advEventId)).toString());
		advinfoB.setAdvInfoCodelstInfoId(outcomeId);
		advinfoB.setAdvInfoType(advInfoOutcomeType);
		advinfoB.setAdvInfoValue(""+strIndex);
		advinfoB.setCreator(usr);
		advinfoB.setIpAdd(ipAdd);

		advinfoB.setAdvInfoDetails();


	}


	for(cnt =0;cnt< EJBUtil.stringToNum(addInfoLen); cnt++ ){
		addInfoId = addInfoIds[cnt];
		strIndex=(StringUtil.isEmpty(addInfoString))?'0':addInfoString.charAt(cnt);

		advinfoB.setAdvInfoAdverseId((new Integer(advEventId)).toString());
		advinfoB.setAdvInfoCodelstInfoId(addInfoId);
		advinfoB.setAdvInfoType(advInfoAddInfo);
		advinfoB.setAdvInfoValue(""+strIndex);
		advinfoB.setCreator(usr);
		advinfoB.setIpAdd(ipAdd);

		advinfoB.setAdvInfoDetails();

	}



	for(cnt =0;cnt< EJBUtil.stringToNum(advNotifyLen); cnt++ ){
		advNotifyCodeId = advNotifyCodeIds[cnt];
		strIndex=(StringUtil.isEmpty(advNotifyString))?'0':advNotifyString.charAt(cnt);
		advNotifyDate = "notifyDate"+cnt;
		advNotifyDate = request.getParameter(advNotifyDate);

		advnotifyB.setAdvNotifyAdverseId((new Integer(advEventId)).toString());
		advnotifyB.setAdvNotifyCodelstNotTypeId(advNotifyCodeId);
		advnotifyB.setAdvNotifyDate(advNotifyDate);
		//advnotifyB.setAdvNotifyNotes(notes);
		advnotifyB.setAdvNotifyValue(""+strIndex);
		advnotifyB.setCreator(usr);
		advnotifyB.setIpAdd(ipAdd);

		advnotifyB.setAdvNotifyDetails();

	}


   }

   if (ret == 0) {

	msg = MC.M_AdvEvtDet_SvdSucc;/*msg = "Adverse Event details saved successfully";*****/
   }
   else {
	msg = MC.M_AdvEvtDet_NotSvd;/*msg = "Adverse Event details not saved";*****/
   }


%>
<br>
<br>
<br>
<br>
<br>
<%//out.println(patProtId);%>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>


<%	if(death.equals("Yes"))	{ 		%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=enrollpatient.jsp?srcmenu=<%=src%>&selectedTab=2&studyId=<%=studyId%>&pkey=<%=pkey%>&patientCode=<%=patientCode%>">

	<%	}else{	%>

	<META HTTP-EQUIV=Refresh CONTENT="1; URL=adveventbrowser.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=6&pkey=<%=pkey%>&visit=<%=visit%>&eventname=<%=eventName%>&eventId=<%=eventId%>&generate=N&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>">

	<% }  }


} //end of if for eSign check


else {

%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>


</BODY>
</HTML>
