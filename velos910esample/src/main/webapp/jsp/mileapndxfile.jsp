<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Mstone_MstoneFile%><%-- Milestone >> Milestone File*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>


<% String src;
src= request.getParameter("srcmenu");

%>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   



<SCRIPT Language="javascript">

 function  validate(formobj){
	 mode = formobj.mode.value;
	 if (mode == 'N')
	 {
	     if (!(validate_col('File',formobj.name))) return false
	 }
     if (!(validate_col('Description',formobj.desc))) return false
    if (!(validate_col('e-Sign',formobj.eSign))) return false
	 if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
		}
 }


function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}

</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="mileApndx" scope="page" class="com.velos.eres.web.mileApndx.MileApndxJB"/>

<%@ page  language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<br>
<DIV class="tabDefTopN" id="div1"> 

  <%

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

{
	String accId = (String) tSession.getAttribute("accountId"); 
	String userId = (String) tSession.getAttribute("userId");
	String uName = (String) tSession.getAttribute("userName");
    String studyId = request.getParameter("studyId");
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String mode = (String) request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	String accMaxStorage = (String) tSession.getAttribute("accMaxStorage");
		String mileApndxDesc = "";
		String mileApndxId = "";
		String milestoneId="";
		
	CtrlDao ctrlDao = new CtrlDao();
	long freeSpace = ctrlDao.getFreeSpace(EJBUtil.stringToNum(accId));

%>
  
  <%
  if (mode.equals("N"))
 {
	com.aithent.file.uploadDownload.Configuration.readSettings("eres");
	com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "milestone");
	String upld=com.aithent.file.uploadDownload.Configuration.UPLOADSERVLET;
	%>

  <form name=upload id="mileapndxfile" action=<%=upld%> METHOD=POST ENCTYPE=multipart/form-data onSubmit="if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
  else
  {
	mileApndxId = (String) request.getParameter("mileApndxId");
	mileApndx.setMileApndxId(com.velos.eres.service.util.EJBUtil.stringToNum(mileApndxId));  
	mileApndx.getMileApndxDetails();
	mileApndxDesc = mileApndx.getDesc(); 
  
  %>
  <form name="upload" id="mileapndxfile" action="milefileupdate.jsp" METHOD=POST onSubmit="if (validate(document.upload)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
  <%
  }
   %>
    </DIV>
	<DIV class="tabDefBotN" id="div2">
    <table width="98%" >
      <tr> 
        <td width="98%" align="center">
		<%
		if (mode.equals("N"))
		  {
	   %> 
	    <!-- Modified By parminder singh Bug#10127-->
          <P class="sectionHeadingsFrm"> <%=MC.M_UploadDocs_ToMstoneAppx%><%-- Upload documents to your Milestone's Appendix*****--%>
		  </P>
		 <%
			}
		 else
		  {
		%>
	      <P class = "defComments"> <%=LC.L_Edit_ShortDesc%><%-- Edit Short Description*****--%>
		  </P>
	
		<%
		  }
		%>   
		    
        </td>
      </tr>
    </table>
    <table width="98%" >
	  <%
	  if (mode.equals("N"))
	  {
	   %>
      <tr><td>&nbsp;</td></tr>
      <tr> 
        <td width="20%" align="right">  <%=LC.L_File%><%-- File*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=file name=name size=40 >
		  <input type=hidden name=db value='eres'>
		  <input type=hidden name=module value='milestone'>		  
		  <input type=hidden name=tableName value='ER_MILEAPNDX'>
		  <input type=hidden name=columnName value='MILEAPNDX_FILEOBJ'>
		  <input type=hidden name=pkValue value='0'>
		  <input type=hidden name=pkColumnName value='PK_MILEAPNDX'>
		  

		  <input type=hidden name=nextPage value='../../velos/jsp/mileapndx.jsp?srcmenu=tdMenuBarItem7&selectedTab=3&studyId=<%=studyId%>'>
		  
        </td>
      </tr>
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_SpecifyFile_FullPath%><%-- Specify the file's full path*****--%> </P>
        </td>
      </tr>
	  <%
	  }
	  %>
      <tr><td>&nbsp;</td></tr>
      <tr> 
        <td align="right">  <%=LC.L_Short_Desc%><%-- Short Description*****--%> <FONT class="Mandatory" >* </FONT> 
        </td>
        <td >
		<%
		if (mode.equals("N"))
		  {
	   %>
		   <input type=text name=desc MAXLENGTH=250 size=40>
		<%
			}
		 else
		  {
		%>
		    <input type=text name=desc MAXLENGTH=250 size=40 value='<%=mileApndxDesc%>'>
		<%
		  }
		%>   
		   
        </td>
      </tr>
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_ShortDescFile_250CharMax%><%-- Give a short description of your file (250 char  max.)*****--%> </P>
        </td>
      </tr>
    </table>
   
  	<input type=hidden name=accId value=<%=accId%>>
  	<input type=hidden name=userId value=<%=userId%>>
	<input type=hidden name=studyId value=<%=studyId%>>
	<input type=hidden name=ipAdd value=<%=ipAdd%>>
	<input type=hidden name=accountId value=<%=accId%>>
	<input type=hidden name=maxFileSize value=<%=freeSpace%>>
	<input type=hidden name=accMaxStorage value=<%=accMaxStorage%>> 
	
	
    <input type="hidden" name="type" value='F'>
    <input type="hidden" name="mode" value=<%=mode%>>
	<input type="hidden" name="srcmenu" value=<%=src%>>

  	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">	
  
    <%
	  if (mode.equals("M"))
	  {
    %>
	   <input type="hidden" name="mileApndxId" value=<%=mileApndxId%>>
 	<%
	   }
	%>
	  
	  
 <BR>
	
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="mileapndxfile"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
	 
  </form>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
