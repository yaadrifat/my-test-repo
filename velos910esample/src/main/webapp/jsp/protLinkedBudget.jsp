<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head> 
<title><%=LC.L_Manage_Bgt%><%--Manage Budgets*****--%></title> 

<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.appendix.AppendixJB,com.velos.eres.web.user.UserJB" %>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>			 
<script type="text/javascript"> 
  
</script> 
</head> 
   	 
<%
String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
	
<body style="overflow:hidden;"> 
	<div id="overDiv"
	style="position:absolute; visibility:hidden; z-index:1000;"></div>
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
	
					String defUserGroup = (String) tSession.getAttribute("defUserGroup");

					String studyId = request.getParameter("studyId");
					studyId = (studyId == null)? (String)tSession.getAttribute("studyId"):studyId;
					tSession.setAttribute("studyId",studyId);
					
					String accId=(String)tSession.getAttribute("accountId");
					 
			 	    String userId = (String) tSession.getValue("userId");
				    UserJB userB = (UserJB) tSession.getValue("currentUser");
			   	    String siteId = userB.getUserSiteId();
					int protocolPK= EJBUtil.stringToNum(request.getParameter("protocolId"));
					
					int budgetPK = 0;
					String delFlag="";
					
					budgetPK = budgetB.getDefaultBudgetForCalendar(protocolPK);
					
					budgetB.setBudgetId(budgetPK);
					budgetB.getBudgetDetails();
					String protocolName="",protocolStatus ="";
					String calStatusPk = "";
					SchCodeDao scho = new SchCodeDao();
					
					//BK 06/24/2020 #5041
					String calledFrom = request.getParameter("calledFrom");
					 if (calledFrom.equals("P")){
						eventdefB.setEvent_id(protocolPK);
						eventdefB.getEventdefDetails();				
						protocolName = eventdefB.getName();
						//protocolStatus = eventdefB.getStatus();
						//KM-DFin9
						calStatusPk = eventdefB.getStatCode();
						protocolStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();

					 }					 
					else{
						
						eventassocB.setEvent_id(protocolPK);
						eventassocB.getEventAssocDetails();
						protocolName = eventassocB.getName();						
						//protocolStatus = eventassocB.getStatus();
						//KM-DFin9
						calStatusPk = eventassocB.getStatCode();
						protocolStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
					}

						tSession.putValue("protocolname", protocolName);
						tSession.putValue("calStatus", protocolStatus);
					if (budgetPK > 0)
					{
						delFlag = budgetB.getBudgetDelFlag();
						
						if (StringUtil.isEmpty(delFlag))
						{
							delFlag="N";
						}
						
					} else
						{
							delFlag="Y";
						}					

					%>
 	<DIV class="BrowserTopn" id="div1">
	 				<jsp:include page="protocoltabs.jsp" flush="true"/>
	</div>
	<DIV class="BgtTabFormN BgtTabFormN_Managebudgets" id="div2">

				<table border="1" width="100%" height="100%" cellpadding="3px" cellspacing="3px"> 
				<tr> 
				 
					<td valign="top" width="100%"> 
	                <% if (budgetPK > 0) { %>
		                <jsp:include page="patientbudget.jsp" flush="true">
			                <jsp:param name="mode" value="M"/>
			                <jsp:param name="budgetId" value="<%=budgetPK%>" />
			                <jsp:param name="budgetTemplate" value="P" />
			                <jsp:param name="includedIn" value="P" />
		                </jsp:include>
					<% } else { %>
							<p class="sectionHeadings"><%=MC.M_NoDefBgt_OrDel%><%--There is no default Budget linked with this Calendar OR it has been deleted.*****--%></p>
						<% }%>	
					</td> 
					 	
				</tr> 
				</table> 

 	<% }
		else
		{
		%>
			<jsp:include page="timeout.html" flush="true"/>
		<%
		}
		%>
		

	  <div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	
	</div>
			  
	<div class ="mainMenu" id = "emenu">
	  <jsp:include page="getmenu.jsp" flush="true"/>
	</div>
		
</body> 
</html>