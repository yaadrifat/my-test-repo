<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_SpecimenStatus%><%--Delete Specimen Status*****--%></title>
<jsp:include page="popupJS.js" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>")/*;alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="specStatB" scope="request" class="com.velos.eres.web.specimenStatus.SpecimenStatusJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:include page="include.jsp" flush="true"/>
<BODY> 
<br>

<DIV class="popDefault" id="div1"> 
<% 
	String pkSpecimenStat= "";
	

HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		pkSpecimenStat= request.getParameter("pkSpecimenStat");
		int ret=0;
		String delMode=request.getParameter("delMode");
	
		if (EJBUtil.isEmpty(delMode)) {
			delMode="final";
%>
  
	<FORM name="specstatdel" id="specstatdelfrm" method="post" action="specimenstatusdelete.jsp" onSubmit="if (validate(document.specstatdel)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>
		
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="specstatdelfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="pkSpecimenStat" value="<%=pkSpecimenStat%>">
   	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
				// Modified for INF-18183 ::: AGodara 
				ret = specStatB.delSpecimenStatus(EJBUtil.stringToNum(pkSpecimenStat),AuditUtils.createArgs(tSession,"",LC.L_Manage_Invent));  
			
			%>
				<br><br><br><br><br><br><br>
			<TABLE width="550" border = "0">
			 <tr>
				<td align="center">
				<%
			
			if (ret == -1) {%>
			 <p class = "successfulmsg"> <%=MC.M_DataCnt_DelSucc%><%--Data could not be deleted successfully*****--%> </p>			
			<%}
			else { %>
			 <p class = "successfulmsg"> <%=MC.M_Data_DelSucc%><%--Data deleted successfully*****--%> </p>
			<%}
			%>
			
				</td>
			   </tr>
			 </table>
				<% if (ret >= 0)
					{%>
				  <script>
						window.opener.location.reload();
						setTimeout("self.close()",1000);
				  </script>	  				
					<%
					} // end of if status got deleted 
				  else
					 {
					 	%>
					 	<TABLE width="550" border = "0">
			 			<tr>
							<td align="center">
								<button onClick="window.self.close()"><%=LC.L_Close%></button>	 
							</td>
			   			</tr>
			 			</table>	
						<%
					 
					 }  
						
			
			} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/> 
 <% } %>
 
 
</DIV>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</HTML>


