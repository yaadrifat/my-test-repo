<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_Multi_EvtDets%><%--Multiple Event Details*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>

/*function changeView(formobj,cmbView)
{


if(cmbView == "visit")
	view = "event";
if(cmbView == "event")
	view = "visit";
formobj.action= "editMulEventDetails.jsp"+"?view="+ view;
formobj.submit();


}*/

function setFlag(formobj)
{
  formobj.buttonFlag.value = "S";
}

function selectDeSelectAll(formobj)
{

    cmbView = formobj.cmbView.value;

	if(formobj.selDeSelAll.checked)
	{
			if(cmbView == "visit")
			{

			   countVisit =formobj.countVisit.value;

			   if(countVisit==1)
				{
				    formobj.chkVst.checked=true;
				}
				else{
					   for(cntVst=0;cntVst<countVisit;cntVst++)
						{
						  formobj.chkVst[cntVst].checked=true;
						}
				}

			}
			if(cmbView == "event")
			{
			  countEvent =formobj.countEvent.value;

				if(countEvent==1)
				{
				    formobj.chkEvent.checked=true;
				}
				else{
					  for(cntEvnt=0;cntEvnt<countEvent;cntEvnt++)
						{
						  formobj.chkEvent[cntEvnt].checked=true;
						}
				    }
			}
	}
	else
	{
		if(cmbView == "visit")
				{

				  countVisit =formobj.countVisit.value;
				   if(countVisit==1)
					{
						formobj.chkVst.checked=false;
					}
				else{
					  for(cntVst=0;cntVst<countVisit;cntVst++)
						{
						  formobj.chkVst[cntVst].checked=false;
						}
				  }

				}
				if(cmbView == "event")
				{
				  countEvent =formobj.countEvent.value;

				  if(countEvent==1)
					{
						formobj.chkEvent.checked=false;
					}
				else{
					  for(cntEvnt=0;cntEvnt<countEvent;cntEvnt++)
						{
						  formobj.chkEvent[cntEvnt].checked=false;
						}
					}
				}

	}

}

function updateAll(formobj)
{
cmbView = formobj.cmbView.value;
//status = formobj.status.value;

var idx = formobj.status.options.selectedIndex ;
var status = formobj.status.options[idx].value;

idx = formobj.ddSos.options.selectedIndex ;
var sos = formobj.ddSos.options[idx].value;

idx = formobj.ddCoverage.options.selectedIndex ;
var coverage = formobj.ddCoverage.options[idx].value;

caldate = formobj.caldate.value;


var cntVst=0;
var cntEvt=0;

if(status == "" && caldate == "" ){
	alert("<%=MC.M_PlsStatOrStat_Frm%>");/*alert("Please Select Status/Status Valid From");*****/
	return false;

}

if(cmbView == "visit")
	{

	  countVisit =formobj.countVisit.value;

			if(countVisit > 1){
				 for(i=0;i<countVisit;i++)
					{
					if(formobj.chkVst[i].checked)
						{
							if(status != "")
							formobj.eventstatus[i].value = status;
							if(caldate != "" )
							formobj.exeon[i].value = caldate;

							if(sos != "" )
								formobj.ddSiteService[i].value = sos;
							if(coverage != "" )
								formobj.ddCoverageType[i].value = coverage;
							cntVst++;

							//JM: 10Aug2010, #5062
							formobj.reasonForCoverageChange[i].disabled=false;
						 }

					}
			}
			else if(countVisit == 1)
			{
				if(formobj.chkVst.checked)
				{
						if(status != "")
						 formobj.eventstatus.value = status;
						if(caldate != "" )
						  formobj.exeon.value = caldate;

						if(sos != "" )
							formobj.ddSiteService.value = sos;
						if(coverage != "" )
							formobj.ddCoverageType.value = coverage;
						cntVst++;

						//JM: 10Aug2010, #5062
						formobj.reasonForCoverageChange.disabled=false;
				}

			}



	if(cntVst==0)
		{
			alert("<%=MC.M_PlsSelectVisit%>");/*alert("Please Select Visit");*****/
		}
	}
	if(cmbView == "event")
		{

				countEvent =formobj.countEvent.value;

				if(countEvent > 1){
					 for(i=0;i<countEvent;i++)
						{
						if(formobj.chkEvent[i].checked)
							{
								if(status != "")
								formobj.eventstatus[i].value = status;
								if(caldate != "" )
								formobj.exeon[i].value = caldate;

								if(sos != "" )
									formobj.ddSiteService[i].value = sos;
								if(coverage != "" )
									formobj.ddCoverageType[i].value = coverage;
								cntEvt++;

								//JM: 10Aug2010, #5062
								formobj.reasonForCoverageChange[i].disabled=false;
							 }

						}
				}
				else if(countEvent == 1)
				{
					if(formobj.chkEvent.checked)
							{
							    if(status != "")
								formobj.eventstatus.value = status;
								if(caldate != "" )
								formobj.exeon.value = caldate;

								if(sos != "" )
									formobj.ddSiteService[i].value = sos;
								if(coverage != "" )
									formobj.ddCoverageType[i].value = coverage;
								cntEvt++;

								//JM: 10Aug2010, #5062
								formobj.reasonForCoverageChange.disabled=false;
							 }
				}

		if(cntEvt==0)
			{
				alert("<%=MC.M_PlsSelectEvt%>");/*alert("Please Select Event");*****/
			}

		}
}



function setVals(formobj,enrollId,len){
   if(formobj.buttonFlag.value != 'S') {
	if(len == 0)
	{
		alert("<%=MC.M_NoRec_ToSave%>");/*alert("No Records to be saved");*****/
		return false;
	}
	else{
			cmbView = formobj.cmbView.value;

			arrCheck = new Array();
			var i=0;
			var j=0;
			var k=0;
			var chk=0;
			if(cmbView == "visit" || cmbView == "")
			{
					countVisit =formobj.countVisit.value;

					if(countVisit > 1){
						 for(i=0;i<countVisit;i++)
							{
							if(formobj.chkVst[i].checked)
								{

									if(formobj.exeon[i].value=="")
									{
										alert("<%=MC.M_StatValidFor_Visits%>");/*alert("Please Enter Status Valid From for Checked Visits");*****/
										return false;
									}
									arrCheck[k] ="1";
									k++;
									chk++;

									if ( formobj.ddCoverageType[i].value !='' && formobj.reasonForCoverageChange[i].value==''){

										alert("<%=MC.M_EtrReason_ForChg%>");/*alert("Please enter the Reason for Change in Coverage Type");*****/
										formobj.reasonForCoverageChange[i].focus();
										return false;
									}


								 }
								 else{
										arrCheck[k] ="0";
										k++;
								}
							}

					}
					else if(countVisit == 1)
					{
						if(formobj.chkVst.checked)
								{
								 if(formobj.exeon.value=="")
									{
										alert("<%=MC.M_StatValidFor_Visits%>");/*alert("Please Enter Status Valid From for Checked Visits");*****/
										return false;
									}
								  arrCheck[k]="1";
								  chk++;

									if ( formobj.ddCoverageType.value !='' && formobj.reasonForCoverageChange.value==''){

										alert("<%=MC.M_EtrReason_ForChg%>");/*alert("Please enter the Reason for Change in Coverage Type");*****/
										formobj.reasonForCoverageChange.focus();
										return false;
									}

								 }
								 else{
									 arrCheck[k]="0";

								 }
							}



							if(chk==0)
							{
								alert("<%=MC.M_PlsSelectVisit%>");/*alert("Please Select Visit");*****/
							return false;
							}
							var fdaVal = document.getElementById("fdaStudy").value;
							if (fdaVal == "1"){
								if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
							}
							if(formobj.eSign.value== "")
								{
									alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
									formobj.eSign.focus();
									return false;
								}


								 if (!(validate_col('e-Signature',formobj.eSign))) return false

								 if(isNaN(formobj.eSign.value) == true) {
									alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
									formobj.eSign.focus();
									return false;
								   }


								//JM: 19Aug2010, #5062
								if(countVisit > 1){

									for(i=0;i<countVisit;i++){

										formobj.reasonForCoverageChange[i].disabled=false;

									}

								}else{

									formobj.reasonForCoverageChange.disabled=false;
								}


						formobj.action = "mulEventsSave.jsp" + "?patProtId="+enrollId+"&arrCheck="+arrCheck;
						return true;

			}
			if(cmbView == "event")
			{

					countEvent =formobj.countEvent.value;

					if(countEvent > 1){
						 for(i=0;i<countEvent;i++)
							{
							if(formobj.chkEvent[i].checked)
								{
									if(formobj.exeon[i].value=="")
									{
										alert("<%=MC.M_StatValidFor_Evt%>");/*alert("Please Enter Status Valid From for Checked Events");*****/
										return false;
									}
									arrCheck[k] ="1";
									k++;
									chk++;
									//YK 10Mar2011 Bug#5644
									if ( (formobj.eventCoverageType[i].value != formobj.ddCoverageType[i].value) && formobj.ddCoverageType[i].value !=''&& formobj.reasonForCoverageChange[i].value==''){

										alert("<%=MC.M_EtrReason_ForChg%>");/*alert("Please enter the Reason for Change in Coverage Type");*****/
										formobj.reasonForCoverageChange[i].focus();
										return false;
									}



								 }
								 else{
									 arrCheck[k] ="0";
									  k++;
								}
							}
					}
					else if(countEvent == 1)
					{
						if(formobj.chkEvent.checked)
								{
								 if(formobj.exeon.value=="")
									{
										alert("<%=MC.M_StatValidFor_Evt%>");/*alert("Please Enter Status Valid From for Checked Events");*****/
										return false;
									}
								 arrCheck[k]="1";
								 chk++;
								//YK 10Mar2011 Bug#5644
									if ( (formobj.eventCoverageType.value != formobj.ddCoverageType.value) && formobj.ddCoverageType.value !='' && formobj.reasonForCoverageChange.value==''){

										alert("<%=MC.M_EtrReason_ForChg%>");/*alert("Please enter the Reason for Change in Coverage Type");*****/
										formobj.reasonForCoverageChange.focus();
										return false;
									}

								 }
								 else{
									 arrCheck[k]="0";

								 }


					}


						if(chk==0)
							{
							alert("<%=MC.M_PlsSelectEvt%>");/*alert("Please Select Event");*****/
							return false;
							}


						 	var fdaVal = document.getElementById("fdaStudy").value;
							if (fdaVal == "1"){
								if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
							}

							if(formobj.eSign.value== "")
							{
								alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
								formobj.eSign.focus();
								return false;
							}


							 if (!(validate_col('e-Signature',formobj.eSign))) return false

							 if(isNaN(formobj.eSign.value) == true) {
								 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
								formobj.eSign.focus();
								return false;
							   }


						//JM: 19Aug2010, #5062
								if(countEvent > 1){

									 for(i=0;i<countEvent;i++){

										formobj.reasonForCoverageChange[i].disabled=false;
									}

								}else{

								 	formobj.reasonForCoverageChange.disabled=false;

								}




						formobj.action = "mulEventsSave.jsp" + "?patProtId="+enrollId+"&arrCheck="+arrCheck;
						return true;

			}

	}
}
}


//JM: 05Aug2010, #5062, function to selectively enable/disable the reason for change in coverage type ..

function callChangeInCovergeType(formobj, turn){

cmbView = formobj.cmbView.value;


	if(cmbView == "visit" || cmbView == ""){

	countVisit =formobj.countVisit.value;

		if(countVisit > 1){

			for(i=0;i<countVisit;i++){
					//KM-#5859
					if (formobj.eventCoverageType[turn-1].value == '0' && formobj.ddCoverageType[turn-1].value == '') {
						 formobj.reasonForCoverageChange[turn-1].value = '';
						 formobj.reasonForCoverageChange[turn-1].disabled=true;
					}
					else if ((formobj.eventCoverageType[turn-1].value != formobj.ddCoverageType[turn-1].value) && formobj.ddCoverageType[turn-1].value !=''){

					 	formobj.reasonForCoverageChange[turn-1].disabled=false;

					}else if ((formobj.eventCoverageType[turn-1].value != formobj.ddCoverageType[turn-1].value) && formobj.ddCoverageType[turn-1].value ==''){

						formobj.reasonForCoverageChange[turn-1].disabled=true;

					}
			}
		}
		else if(countVisit == 1)
		{
			//KM-#5859
			if (formobj.eventCoverageType.value == '0' && formobj.ddCoverageType.value == '') {
				 formobj.reasonForCoverageChange.value = '';
				 formobj.reasonForCoverageChange.disabled=true;
			}
			else if ((formobj.eventCoverageType.value != formobj.ddCoverageType.value) && formobj.ddCoverageType.value !=''){

			 	formobj.reasonForCoverageChange.disabled=false;

			}else if ((formobj.eventCoverageType.value != formobj.ddCoverageType.value) && formobj.ddCoverageType.value ==''){

				formobj.reasonForCoverageChange.disabled=true;

			}

		}

	}

	if(cmbView == "event"){


	countEvent =formobj.countEvent.value;


		if(countEvent > 1){

			 for(i=0;i<countEvent;i++){
				
				//KM-#5859
				if(formobj.eventCoverageType[turn-1].value == '0' && formobj.ddCoverageType[turn-1].value == '') {
					formobj.reasonForCoverageChange[turn-1].value = '';
					formobj.reasonForCoverageChange[turn-1].disabled=true;
				}
				else 
				if ((formobj.eventCoverageType[turn-1].value != formobj.ddCoverageType[turn-1].value) && formobj.ddCoverageType[turn-1].value !=''){

			 		formobj.reasonForCoverageChange[turn-1].disabled=false;
			 	
				}else if ((formobj.eventCoverageType[turn-1].value != formobj.ddCoverageType[turn-1].value) && formobj.eventCoverageType[turn-1].value =='0'){

					formobj.reasonForCoverageChange[turn-1].disabled=true;
					
				}else if ((formobj.eventCoverageType[turn-1].value != formobj.ddCoverageType[turn-1].value) && formobj.ddCoverageType[turn-1].value ==''){

					formobj.reasonForCoverageChange[turn-1].disabled=false;
		
				}else{

					formobj.reasonForCoverageChange[turn-1].disabled=true;
				}

			 }

		}
		else if(countEvent == 1){

			//KM-#5859
			if(formobj.eventCoverageType.value == '0' && formobj.ddCoverageType.value == '') {
					formobj.reasonForCoverageChange.value = '';
					formobj.reasonForCoverageChange.disabled=true;
			}
			else if ((formobj.eventCoverageType.value != formobj.ddCoverageType.value) && formobj.ddCoverageType.value !=''){

			 	formobj.reasonForCoverageChange.disabled=false;
		
			}else if ((formobj.eventCoverageType.value != formobj.ddCoverageType.value) && formobj.eventCoverageType.value =='0'){

				formobj.reasonForCoverageChange.disabled=true;
		
			}else if ((formobj.eventCoverageType.value != formobj.ddCoverageType.value) && formobj.ddCoverageType.value ==''){

				formobj.reasonForCoverageChange.disabled=false;
	
			}else{
				formobj.reasonForCoverageChange.disabled=true;

			}

		}


	}


}
function shwNdHdeCols(formobj){		
	try{		
		var selvalue = formobj.view.value;		
		var count = formobj.count.value;		
		if(selvalue=="event"){			
			for(var i=0;i<=count;i++){
			var showTD=document.getElementById('hideCols'+Number(i));			
			if( showTD!=null){			
				showTD.style.display = 'block'; 
				showTD.removeAttribute("style");
			}
			}
		}
		if(selvalue=="visit"){
			for(var i=0;i<count;i++){
				var showTD=document.getElementById('hideCols'+Number(i));			
				if(showTD!=null )
					showTD.setAttribute("style","display:none");
				}
		}
	}catch(e){}
}
</script>

<%int count=0;%>
<BODY onload="shwNdHdeCols(document.MultipleEvent);">

<div class="popDefault">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<jsp:include page="include.jsp" flush="true"/>

<%
	HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
	{
	    String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		String enrollId = request.getParameter("patProtId");
		String calledFrom = request.getParameter("calledFrom");
		int countVisit = 0;
		int countEvent = 0;
		String patStudyId="";
		patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
		patEnrollB.getPatProtDetails();
		patStudyId = patEnrollB.getPatStudyId();
		int totalNumberOfVisits = 0;

		 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

	<%

	String userIdFromSession = (String) tSession.getValue("userId");
	int pageRight = 0;
	String studyId = request.getParameter("studyId");
	String studyNumber = "";

	    studyB.setId(EJBUtil.stringToNum(studyId));
		studyB.getStudyDetails();
		studyNumber = studyB.getStudyNumber();
		int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;


	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
	ArrayList tId = teamDao.getTeamIds();
	int patdetright = 0;
	if (tId.size() == 0) {
		pageRight=0 ;
	}else
	{

		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

		ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();

				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();


		if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
		}else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));

		}
	}
	int orgRight=0;


		int personPK = EJBUtil.stringToNum(request.getParameter("pkey"));

    	personB.setPersonPKId(personPK);
     	personB.getPersonDetails();
    	String patientId = personB.getPersonPId();

    	int siteId = EJBUtil.stringToNum(personB.getPersonLocation());

    	orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), personPK , EJBUtil.stringToNum(studyId) );

		if (orgRight > 0)
		{
			System.out.println("patient schedule edit mul orgRight" + orgRight);
			orgRight = 7;
		}


	if ( ( pageRight >= 4 ) && ( orgRight >= 4) )
	{
%>
<Form class=formDefault id="editEvtFrm" name="MultipleEvent" action="" method="post" onSubmit="if (setVals(document.MultipleEvent,<%=enrollId%>,document.MultipleEvent.totEvt.value)== false) { setValidateFlag('false'); return false; } else { if(document.MultipleEvent.buttonFlag.value == 'S') setValidateFlag('false'); else setValidateFlag('true'); return true;}" >
	<input type="hidden" name="calledFrom" value="<%=calledFrom%>">
<%

	SchCodeDao cd = new SchCodeDao();
	cd.getCodeValues("eventstatus",0);

	cd.setCType("eventstatus");
	cd.setForGroup(defUserGroup);

	String dEvStat = "";
	int stTempPk;


	String cmbView = request.getParameter("view");

	if(cmbView == null)
		{
		cmbView ="visit";
		}
	String evParmStatus = request.getParameter("dstatus");


	String cmbVisit = request.getParameter("visit");

	int tempVisit =EJBUtil.stringToNum(cmbVisit);
	int tempStat = EJBUtil.stringToNum(evParmStatus);
	if(tempStat==0 && evParmStatus==null )
		{
		  tempStat=cd.getCodeId("eventstatus","ev_notdone");
		}

	//StringBuffer mainStr = new StringBuffer();
	//int cdcounter = 0;
	//int val=41;
	//if(cmbView.equals("visit"))
		/*mainStr.append("<SELECT NAME='dstatus'>") ;
		for (cdcounter = 0; cdcounter <= cd.getCDesc().size() -1 ; cdcounter++)
	{
		stTempPk = ((Integer) cd.getCId().get(cdcounter)).intValue() ;
		if (tempStat == stTempPk )
		{
		mainStr.append("<OPTION SELECTED value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}
		else
		{
		mainStr.append("<OPTION value = "+ cd.getCId().get(cdcounter)+">" + cd.getCDesc().get(cdcounter)+ "</OPTION>");
		}
	}

	mainStr.append("</SELECT>");
	dEvStat = mainStr.toString(); */
	dEvStat = cd.toPullDown("dstatus",tempStat);


    EventdefDao eventdao = new EventdefDao();
	eventdao = eventdefB.getScheduleEventsVisits(EJBUtil.stringToNum(enrollId));

			java.util.ArrayList visits = eventdao.getVisit();

			if (tempVisit > 0 )
			{
				totalNumberOfVisits = 1;
			}
			else
			{
				totalNumberOfVisits = visits.size();
			}



			//System.out.println("visits sonia "+ visits.size());

			java.util.ArrayList visitnames = eventdao.getVisitName();

			String visitDD="<select name='visit'><option value=\"0\" Selected>"+LC.L_All/*All*****/+"</option>"  ;

			int visitNo = 0;

		 	for(int cnt=1; cnt<=visits.size();cnt++)
			{
				visitNo = EJBUtil.stringToNum(visits.get(cnt - 1).toString()) ;

				Integer integerVisit = new Integer(0);
				integerVisit = (Integer) visits.get(cnt-1) ;

				int intVisit = 0;
				intVisit = integerVisit.intValue();


				if ( tempVisit == intVisit  )
				{
/* SV, 10/15/04, show visit name instead	"Visit"+ cnt  to eventdao.getDefinedVisitNameFromVisitNo(visitNo)*/
					if (visitnames.size() > 0) {
						visitDD = visitDD +"<option value=" + intVisit +" Selected> " + visitnames.get(cnt-1)  +"</option>";
}
					else {
						visitDD = visitDD +"<option value=" +intVisit +" Selected> " + "Visit "+visits.get(cnt-1)  +"</option>";

					}


				}
				else
				{
/* SV, 10/15/04, show visit name instead	"Visit"+ cnt  to eventdao.getDefinedVisitNameFromVisitNo(visitNo)*/

					if (visitnames.size() > 0) {
						visitDD = visitDD +"<option value=" + intVisit +">" + visitnames.get(cnt-1)  +"</option>";
}
					else {
						visitDD = visitDD +"<option value=" + intVisit +"> " + "Visit "+visits.get(cnt-1)  +"</option>";

					}
				}
			}
			visitDD = visitDD+"</select>";

%>  <table width="100%" cellspacing="0" cellpadding="0" class= "patHeader" border="0">
      <tr>

    		<td><%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>: <%=patientId%></td>
    		<td><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%>: <%=patStudyId%></td>
		<td><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <%=studyNumber%></td>
	</tr>
	</table>
	<%if(orgRight > 5 && pageRight > 5 ){%>
	<hr/>
	<table width=100% border="0">
		<tr>
		    <td width="10%"><%=LC.L_ReasonForChangeFDA%>
		    	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
		    </td>
			<td><textarea id="remarks" name="remarks" rows="3" cols="30" MAXLENGTH="4000"></textarea></td>
			<td>
				<jsp:include page="submitBar.jsp" flush="true">
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="editEvtFrm"/>
					<jsp:param name="showDiscard" value="N"/>
				</jsp:include>
			</td>
		</tr>
	</table>
	<%}%>
	<hr/>
	<P class=defcomments><%=MC.M_VisitsEvts_AvalForSel%><%--Following are the Visit(s)/Event(s) available for the selection*****--%> </p>
	<%
	ArrayList eventIds = null;
	ArrayList descs = null;
	ArrayList vsts = null;
	ArrayList status = null;
	ArrayList notes = null;
	ArrayList exeons = null;
	ArrayList SOSIds = null;
	ArrayList CoverageIds = null;
	ArrayList reasonForCoverageChanges =null;

	int eventSOSId =0;
	int eventCoverType =0;

	String ddSOS ="";
	String ddCoverage ="";
	String reasonForCoverageCng = "";

	SchCodeDao schDao1 = new SchCodeDao();
	schDao1.getCodeValues("coverage_type");

	int accountId=0;
	accountId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));

	CodeDao codeDao = new CodeDao();
	codeDao.getCodeValues("site_type","service");

	ArrayList siteType = new ArrayList();
	siteType = codeDao.getCId();

	String strSiteType = EJBUtil.ArrayListToString(siteType);
	SiteDao siteDao = siteB.getBySiteTypeCodeId(accountId,strSiteType);
	ArrayList siteIds = new ArrayList();
	siteIds = siteDao.getSiteIds();

	ArrayList siteNames = new ArrayList();
	siteNames= siteDao.getSiteNames();

	EventdefDao eventD = new EventdefDao();

	int visit=0;
	eventD = eventdefB.getVisitEvents(EJBUtil.stringToNum(enrollId),tempStat,tempVisit,null);
	descs = eventD.getDescriptions();
	eventIds = eventD.getEvent_ids();
	SOSIds = eventD.getEventSOSIds();
	CoverageIds = eventD.getEventCoverageTypes();
	vsts = eventD.getVisit();
	status = eventD.getStatus();
	notes = eventD.getNotes();
	exeons = eventD.getEvent_exeons();
	reasonForCoverageChanges = eventD.getReasonForCoverageChange();


	String vst = "";
	String firstVst = "";
	String desc = "";
	String firstStatus = "";
	int uniqueVisit = 0;
	Integer visitToCompare = null;
	int ctrVst=0;

	int len= eventIds.size();

	%>
	<input type="hidden" name="totEvt" value=<%=len%>>
	<%

	//SV, iterate through the visit ArrayList and see if the events belong to only one visit or theer ar e more than one visists

	if (totalNumberOfVisits > 1 && len > 0)
	{
		for ( ctrVst=0; ctrVst < visits.size()  ; ctrVst++)
	 	{

	 		visitToCompare = (Integer) visits.get(ctrVst);

	 		// see if this visit exists in the retrieved events's visit
	 		if (vsts.indexOf(visitToCompare) > -1)
	 		{
	 			uniqueVisit = uniqueVisit +1;
	 		}

	 	}
	}
	else
	{
		uniqueVisit = 1;
	}

	totalNumberOfVisits = uniqueVisit;

	// SA, 07/27/05
	//	check the number of events, if number of events ==1, totalNumberOfVisits also becomes 1

	if (len == 1)
	{
		totalNumberOfVisits = 1;
	}




	String oldStatusId ="";
	String eveId = "";
	%>

	   <table width=100%>
		   <tr>
		   <td class=tdDefault width=10% align="center"><%=LC.L_Select%><%--Select*****--%></td>
		   <td width=10% align=left>
		  <select name="view">

		   <% if(cmbView.equals("visit") || cmbView.equals("")){%>
			   <option value="visit" selected><%=LC.L_Visit_View%><%--Visit View*****--%></option>
			   <option value="event"><%=LC.L_Evt_View%><%--Event View*****--%></option>
			   <%}
			else if(cmbView.equals("event")) {%>
			   <option value="visit" ><%=LC.L_Visit_View%><%--Visit View*****--%></option>
			   <option value="event" selected><%=LC.L_Evt_View%><%--Event View*****--%></option>
				<%}%>
		   </select>
		   </td>
		   <td class="tdDefault" width=15% align="center"><%=LC.L_Visit%><%--Visit*****--%>&nbsp;&nbsp;&nbsp;&nbsp;<%=visitDD%></td>

		   <td class=tdDefault width=40% align=left><%=MC.M_FilterVisits_EvtStat%><%--Filter Visits/Events with Status*****--%>&nbsp;&nbsp;&nbsp;&nbsp;<%=dEvStat%></td>
		   	<td width=15% align=left><button type="submit" onclick="setFlag(document.MultipleEvent)"><%=LC.L_Search%></button></td>
		   </tr>
		   </table>
            <input type="hidden" name="buttonFlag" value ="">
			<br>
<%

	if(len >= 1){

			 firstVst = vsts.get(0).toString();

			 desc = descs.get(0).toString();
			 eveId= eventIds.get(0).toString();

			String exeon = "";
			String statusId = "";
			String note= "";
			String eventId = "";
			int iEventId =0;

			SchCodeDao schDao = new SchCodeDao();
			int k=0;
			String[] arr = new String[len];
			String[] arrIds = new String[len];


			   String dCur = "";

			%>
				<table width="100%" cellspacing="1" cellpadding="0" border="0">
				<tr>
					<th width=30% bgcolor="#9999ff"><%=LC.L_SelVisit_Evt%><%--Select Visit/Events*****--%></th>
					<th width=25% bgcolor="#9999ff"><%=LC.L_Status%><%--Status*****--%></th>
<%-- INF-20084 Datepicker-- AGodara --%>					
					<th width=18% bgcolor="#9999ff"><%=LC.L_Status_ValidFrom%><%--Status Valid From*****--%></th>
					<th width=27% bgcolor="#9999ff" id="hideCols<%=(count++)%>" style="display:none"><%=LC.L_Site_OfService%><%--Site of Service*****--%></th>
					<th width=30% bgcolor="#9999ff" id="hideCols<%=(count++)%>" style="display:none"><%=VelosResourceBundle.getLabelString("L_Coverage_Type")%></th><%--*<th width=30% bgcolor="#9999ff"><b><%=VelosResourceBundle.getLabelString("Evt_CoverageType")%></b></th>****--%>
					<th width=20% bgcolor="#9999ff" id="hideCols<%=(count++)%>" style="display:none"><%=VelosResourceBundle.getMessageString("M_ReasonChg_CvgType")%></th><%--<th width=20% bgcolor="#9999ff"><b><%=VelosResourceBundle.getLabelString("Reason_For_Change_CT")%></b></th>*****--%>
					<th width=20% bgcolor="#9999ff" id="hideCols<%=(count++)%>" style="display:none"><%=LC.L_Notes%><%--Notes*****--%></th>
				</tr>
<!--				<tr>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				</tr>
-->
				<tr>
					<%

					 schDao.getCodeValues("eventstatus",0);

					 schDao.setCType("eventstatus");
	                 schDao.setForGroup(defUserGroup);


					 String dStatus =  schDao.toPullDown("status");

					 ddSOS=siteDao.toPullDown("ddSos",0, " ");
					 ddCoverage = schDao1.toPullDown("ddCoverage",0," ");

					  %>

					<td width=30%><input type="checkbox" name="selDeSelAll" onclick="selectDeSelectAll(document.MultipleEvent)"><%=LC.L_Select_All%><%--Select All*****--%></td>
<%-- INF-20084 Datepicker-- AGodara --%>					
					<td width=25% align="center"><%=dStatus%></td>
				    <td width=20% align="center"><input type="text" name="caldate" class="datefield" READONLY size=10 ></td>
					<td id="hideCols<%=(count++)%>" style="display:none"><%=ddSOS %></td>
					<td id="hideCols<%=(count++)%>" style="display:none"><%=ddCoverage %></td>
					<td width=20%><A href=# onclick="updateAll(document.MultipleEvent)"><%=MC.M_Updt_AllSelcRows%><%--Update All Selected Rows*****--%></A></td>
				</tr>

			<%

				ddSOS=siteDao.toPullDown("ddSiteService",0, " ");
				ddCoverage = schDao1.toPullDown("ddCoverageType",0," ");

				for(int i=0,m=0;i<len;i++)
				{
				  vst =  vsts.get(i).toString();
				  visitNo = EJBUtil.stringToNum(vst) ;

				  statusId= status.get(i).toString();
				  oldStatusId = statusId;

   				  note = ((notes.get(i)) == null)?" ":(notes.get(i)).toString();
   				  reasonForCoverageCng  = ((reasonForCoverageChanges.get(i)) == null)?"":(reasonForCoverageChanges.get(i)).toString();

				  eventId = eventIds.get(i).toString();
				  iEventId =0;
				  iEventId = EJBUtil.stringToNum(eventId);

				  if(statusId!=null){
				  dCur =  schDao.toPullDown("eventstatus",EJBUtil.stringToNum(statusId));
				  }

				  if(exeons.get(i) !=null){
						exeon= exeons.get(i).toString();
						java.sql.Date exeondate = java.sql.Date.valueOf(exeon);
				    	//exeon= exeon.substring(5,7)+"/"+exeon.substring(8,10)+"/"+exeon.substring(0,4);
						exeon = DateUtil.dateToString(exeondate);
				  }else{exeon="";}

		/* When user selects Visit View */
		if(cmbView.equals("visit") || cmbView.equals("")){
			note="";
			exeon="";

			reasonForCoverageCng = "";

			if(i==0){

				%>
			<tr>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
				<td><hr class="thinLine"></td>
			</tr>
   				 <tr>
				     <td width=30%><b><input type="checkBox" name="chkVst" value="<%=vst%>*<%=eveId%>"><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td>
					<%countVisit++;%>
<%-- INF-20084 Datepicker-- AGodara --%>					
					<td width=25% align="center">
					<input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
					<td class=tdDefault width=20% align="center"><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
					<td id="hideCols<%=(count++)%>" style="display:none"><%=ddSOS %></td>
				<%
				//JM: 21Jul2010: #5062

					StringBuffer ddCoverageBufferVisit = new StringBuffer();

					ddCoverage = schDao1.toPullDown("ddCoverageType",0," ");

					int lenVisit = ddCoverage.length();

					int strIndexVisit = ddCoverage.indexOf(">");

					String strSelVisit =  ddCoverage.substring(0, strIndexVisit);

					strSelVisit = strSelVisit + " "+ "onchange=callChangeInCovergeType(document.MultipleEvent,'"+countVisit+"'); >";
					ddCoverageBufferVisit.append(strSelVisit);
					String strAppVisit = ddCoverage.substring(strIndexVisit+1, lenVisit);
					ddCoverageBufferVisit.append(strAppVisit);
					ddCoverage = ddCoverageBufferVisit.toString();

				%>
					<td id="hideCols<%=(count++)%>" style="display:none"><%=ddCoverage %></td>

					<INPUT NAME="eventCoverageType" TYPE="hidden"  value=<%=eventCoverType%>>
			
					<td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="reasonForCoverageChange" value="<%=reasonForCoverageCng%>" disabled></td>
					<input type="hidden" name="reasonForCoverageChangeTxt" value="<%=reasonForCoverageCng%>">
					<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
					 <td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="note" value="<%=note%>"></td>
					<%desc="";
					 eveId="";%>

				 </tr>
				 <%
			 		}
					if(!firstVst.equals(vst)){

						arr[m] = desc;
						arrIds[m] = eveId;
						//fVst = vsts.get(i).toString();
						desc =(String) descs.get(i);
						eveId = eventIds.get(i).toString();
						%>
						<tr><td></td></tr>
					 <tr>
						<td width=30%> <b><input type="checkBox" name="chkVst" value="<%=vst%>*<%=eveId%> "><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td>
								<%countVisit++;%>
<%-- INF-20084 Datepicker-- AGodara --%>
						<td width=25% align="center" ><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
						<td class=tdDefault width=20% align="center"><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
						<td id="hideCols<%=(count++)%>" style="display:none"><%=ddSOS %></td>
					<%
					//JM: 21Jul2010: #5062

						StringBuffer ddCoverageBufferVisit = new StringBuffer();

						ddCoverage = schDao1.toPullDown("ddCoverageType",0," ");

						int lenVisit = ddCoverage.length();

						int strIndexVisit = ddCoverage.indexOf(">");

						String strSelVisit =  ddCoverage.substring(0, strIndexVisit);

						strSelVisit = strSelVisit + " "+ "onchange=callChangeInCovergeType(document.MultipleEvent,'"+countVisit+"'); >";
						ddCoverageBufferVisit.append(strSelVisit);
						String strAppVisit = ddCoverage.substring(strIndexVisit+1, lenVisit);
						ddCoverageBufferVisit.append(strAppVisit);
						ddCoverage = ddCoverageBufferVisit.toString();
					%>
						<td id="hideCols<%=(count++)%>" style="display:none"><%=ddCoverage %></td>
						<INPUT NAME="eventCoverageType" TYPE="hidden"  value=<%=eventCoverType%>>
						<td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="reasonForCoverageChange" value="<%=reasonForCoverageCng%>" disabled></td>
						<input type="hidden" name="reasonForCoverageChangeTxt" value="<%=reasonForCoverageCng%>">
						<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
						 <td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="note" value="<%=note%>"></td>

						 <td id="hideCols<%=(count++)%>" style="display:none"><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td>


					</tr>
						<%if(i<len-1){
							if(!(vsts.get(i+1).toString().equals(vst)) ){
							desc =(String) descs.get(i);
							if(!eveId.equals(eventIds.get(i).toString())){
							eveId = eveId+":"+eventIds.get(i).toString();
							}
							arr[m] = desc;
							arrIds[m] = eveId;

							%> <tr>
								<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
							  </tr>
							<%
							}

						}

						if(i==len-1){
							if(!(vsts.get(i-1).toString().equals(vst)) ){
								desc =(String) descs.get(i);
								if(!eveId.equals(eventIds.get(i).toString())){
								eveId = eveId+":"+eventIds.get(i).toString();
								}
								arr[m] = desc;
								arrIds[m] = eveId;

								%> <tr>
									<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
								  </tr>
								<%
								}

							}

						}

						else {
							if(i==0){%>

							<%	desc = (String) descs.get(i);
								if(!eveId.equals(eventIds.get(i).toString())){
								eveId = eveId+":"+eventIds.get(i).toString();
								  }
								}
								else{
								 desc = desc + ","+(String) descs.get(i);
								 if(!eveId.equals(eventIds.get(i).toString())){
								 eveId = eveId+":"+eventIds.get(i).toString();
						  	}
						}

						 arr[m] = desc;
						 arrIds[m] = eveId;
						if(i<len-1){
							if(!(vsts.get(i+1).toString().equals(vst)) ){
					%>  <tr>
						<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
						</tr><%
							}
						}
						else if(i == len-1){%>
						 <tr >
						<td ><input type="hidden" name="eveIds" value=<%=arrIds[m]%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=arr[m]%></td>
						</tr>
						<%}%>
					 <td><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td>

					<%}
					m++;
				}

				/* When user selects Event View */
				else{

					eventSOSId =0;
	   			 	if (SOSIds.get(i) != null){
				  		eventSOSId = EJBUtil.stringToNum(SOSIds.get(i).toString());
	   			 	}
				  	ddSOS= EJBUtil.createPullDown("ddSiteService",eventSOSId, siteIds, siteNames);

				  	eventCoverType =0;
				  	if (CoverageIds.get(i) != null){
				  		eventCoverType = EJBUtil.stringToNum(CoverageIds.get(i).toString());
	   			 	}
	   				//ddCoverage = schDao1.toPullDown("ddCoverageType",eventCoverType," ");



					if(i==0){%>
					<tr>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
						<td><hr class="thinLine"></td>
					</tr>
					<%--SV, 10/15/04, changed to show visit name					<tr><td width=30%><b>Visit <%=vst%></b></td></tr> --%>
					<tr><td width=30%><b><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td></tr>

					<%desc =(String) descs.get(i);%>

					<tr><td>

						<input type="checkBox" name="chkEvent" value=<%=eventId%>><%=desc%></td>
<%-- INF-20084 Datepicker-- AGodara --%>						
						<td width=25% align="center" ><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
						<td class=tdDefault width=20% align="center"><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
						<td id="hideCols<%=(count++)%>" style="display:none"><%=ddSOS %></td>
					<%

					//JM: 20Jul2010: #5062

						countEvent++; //brought here from the lower areas

						StringBuffer ddCoverageBufferEvt = new StringBuffer();

						ddCoverage = schDao1.toPullDown("ddCoverageType",eventCoverType," ");

						int lenEvt = ddCoverage.length();

						int strIndexEvt = ddCoverage.indexOf(">");

						String strSelEvt =  ddCoverage.substring(0, strIndexEvt);

						strSelEvt = strSelEvt + " "+ "onchange=callChangeInCovergeType(document.MultipleEvent,'"+countEvent+"'); >";

						ddCoverageBufferEvt.append(strSelEvt);
						String strAppEvt = ddCoverage.substring(strIndexEvt+1, lenEvt);
						ddCoverageBufferEvt.append(strAppEvt);
						ddCoverage = ddCoverageBufferEvt.toString();

					%>
						<td id="hideCols<%=(count++)%>" style="display:none"><%=ddCoverage %></td>
						<INPUT NAME="eventCoverageType" TYPE="hidden"  value=<%=eventCoverType%>>
						<td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="reasonForCoverageChange" value="<%=reasonForCoverageCng%>" disabled></td>
						<input type="hidden" name="reasonForCoverageChangeTxt" value="<%=reasonForCoverageCng%>">
						<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
						<td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="note" value="<%=note%>"></td>
						<td id="hideCols<%=(count++)%>" style="display:none"><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td></tr>

						<%//countEvent++;
					}
					else if(!firstVst.equals(vst)){%>
						<tr><td></td></tr>
						<tr><td></td></tr><tr><td></td></tr><tr><td></td></tr>
						<%--SV, 10/15/04, changed t show visit name.						<tr><td width=30%><b>Visit <%=vst%></b></td></tr> --%>

						<tr><td width=30%><b><%=eventdao.getDefinedVisitNameFromVisitNo(visitNo)%></b></td></tr>

							<%desc =(String) descs.get(i);%>
						<tr>
							<td><input type="checkBox" name="chkEvent" value=<%=eventId%>><%=desc%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
							<td width=25% align="center" ><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
							<td class=tdDefault width=20% align="center"><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
							<td id="hideCols<%=(count++)%>" style="display:none"><%=ddSOS %></td>
							<%
							//JM: 20Jul2010: #5062

								countEvent++; //brought here from the lower areas

								StringBuffer ddCoverageBufferEvt = new StringBuffer();

								ddCoverage = schDao1.toPullDown("ddCoverageType",eventCoverType," ");

								int lenEvt = ddCoverage.length();

								int strIndexEvt = ddCoverage.indexOf(">");

								String strSelEvt =  ddCoverage.substring(0, strIndexEvt);

								strSelEvt = strSelEvt + " "+ "onchange=callChangeInCovergeType(document.MultipleEvent,'"+countEvent+"'); >";

								ddCoverageBufferEvt.append(strSelEvt);
								String strAppEvt = ddCoverage.substring(strIndexEvt+1, lenEvt);
								ddCoverageBufferEvt.append(strAppEvt);
								ddCoverage = ddCoverageBufferEvt.toString();
							%>
								<td id="hideCols<%=(count++)%>" style="display:none"><%=ddCoverage %></td>
								<INPUT NAME="eventCoverageType" TYPE="hidden"  value=<%=eventCoverType%>>
								<td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="reasonForCoverageChange" value="<%=reasonForCoverageCng%>" disabled></td>
								<input type="hidden" name="reasonForCoverageChangeTxt" value="<%=reasonForCoverageCng%>">
								<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
								 <td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="note" value="<%=note%>"></td>
								<td id="hideCols<%=(count++)%>" style="display:none"><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td></tr>

								<% //countEvent++;
								}
								else{
									desc =(String) descs.get(i);%>
								<tr>
									<td><input type="checkBox" name="chkEvent" value=<%=eventId%>><%=desc%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
									<td width=25% align="center"><input type="hidden" name="statusId" value=<%=statusId%>><%=dCur%></td>
									<td class=tdDefault width=20% align="center"><INPUT type="text" name="exeon" class="datefield" READONLY size=10 value=<%=exeon%>></td>
									<td id="hideCols<%=(count++)%>" style="display:none"><%=ddSOS %></td>
								<%
								//JM: 20Jul2010: #5062

									countEvent++; //brought here from the lower areas

									StringBuffer ddCoverageBufferEvt = new StringBuffer();

									ddCoverage = schDao1.toPullDown("ddCoverageType",eventCoverType," ");

									int lenEvt = ddCoverage.length();

									int strIndexEvt = ddCoverage.indexOf(">");

									String strSelEvt =  ddCoverage.substring(0, strIndexEvt);

									strSelEvt = strSelEvt + " "+ "onchange=callChangeInCovergeType(document.MultipleEvent,'"+countEvent+"'); >";

									ddCoverageBufferEvt.append(strSelEvt);
									String strAppEvt = ddCoverage.substring(strIndexEvt+1, lenEvt);
									ddCoverageBufferEvt.append(strAppEvt);
									ddCoverage = ddCoverageBufferEvt.toString();

								%>
									<td id="hideCols<%=(count++)%>" style="display:none"><%=ddCoverage %></td>
									<INPUT NAME="eventCoverageType" TYPE="hidden"  value=<%=eventCoverType%>>
									<td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="reasonForCoverageChange" value="<%=reasonForCoverageCng%>" disabled></td>
									<input type="hidden" name="reasonForCoverageChangeTxt" value="<%=reasonForCoverageCng%>">
									<!-- value=<%=note%> has been changed as value="<%=note%>" - eRes Chennai Team-->
									 <td width=20% id="hideCols<%=(count++)%>" style="display:none"><input type="text" name="note" value="<%=note%>"></td>
									<td><input type="hidden" name="oldStatusId" value=<%=oldStatusId%>></td></tr>
									<%//countEvent++;
								}



							}
							firstVst = vst;
							count++;
						}//end of for loop%>
			<Input type="hidden" name="patProtId" value="<%=enrollId%>">
				<input type="hidden" name="countVisit" value="<%=countVisit%>">
				<input type="hidden" name="cmbView" value="<%=cmbView%>">
				<input type="hidden" name="countEvent" value="<%=countEvent%>">
				<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">

	</table>
				<%



			}//end of if length
			else{	%>
			<font class="recNumber" align="center"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
			<%}%>
				<input type="hidden" name="patProtId" value=<%=enrollId%>>
				<input type="hidden" name="len" value=<%=len%>>
				<input type="hidden" name="count" value=<%=count%>>


		</form><%

} //end of if body for page right
else
{
%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%

}
	}//end of else body for page right


		else{



				%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
</body>
