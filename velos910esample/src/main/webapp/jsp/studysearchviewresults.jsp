<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="JavaScript1.1">
	
function openStudyView(id)
{
     windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
}
	
	
</SCRIPT> 

</HEAD>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*"%>

<%
HttpSession tSession = request.getSession(true); 
String userId = null;
if (sessionmaint.isValidSession(tSession)) {
	 userId = (String) tSession.getValue("userId");
}

String resultSet=request.getParameter("resultSet");
String searchCriteria = request.getParameter("searchCriteria");
if (searchCriteria==null) {searchCriteria="";}

Vector vec = new Vector();
			
Column studyId = new Column(LC.L_Std_Id/*Study Id*****/);
studyId.passAsParameter = true;
studyId.hidden = true;
			
vec.add(studyId);	
			
Column studyNumber = new Column(LC.L_Study_Number/*Study Number*****/);
studyNumber.funcName = "openStudyView";
studyNumber.width = "20%"	;
studyNumber.align = "left"	;		
vec.add(studyNumber);

Column studyVersion = new Column(LC.L_Version/*Version*****/);
studyVersion.width = "10%"	;
studyVersion.align = "left"	;		
vec.add(studyVersion);
			
Column studyTitle = new Column(LC.L_Study_Title/*Study Title*****/);
studyTitle.width = "40%" ;
studyTitle.align = "left"	;
vec.add(studyTitle);
			
String sSQL = "select pk_studyview, STUDYVIEW_NUM, studyview_verno, case when length(STUDYVIEW_TITLE) > 80 then substr(STUDYVIEW_TITLE,1,80) || '...'  else STUDYVIEW_TITLE  end from er_studyview where fk_user = " + userId +" and (lower(studyview_num) like lower('%"+searchCriteria+"%') or lower(studyview_title) like lower('%"+searchCriteria+"%'))";

String title =MC.M_StdsMatch_SrchCriteria/*The following studies match your search criteria*****/+": ";

String srchType=request.getParameter("srchType");
String sortType=request.getParameter("sortType");
String sortOrder=request.getParameter("sortOrder");

request.setAttribute("sSQL",sSQL);
request.setAttribute("srchType",srchType);
request.setAttribute("vec",vec);
request.setAttribute("sTitle",title);
			
//This parameter is recieved on clicking back or next button
String s_scroll=request.getParameter("scroll"+resultSet);
request.setAttribute("scroll"+resultSet,s_scroll);
request.setAttribute("sortType",sortType);
request.setAttribute("sortOrder",sortOrder);


%>

<Form name="studysearchviewresults" method="post" action="studysearchbrowser.jsp">
  <P class = "defComments"> <%=MC.M_StdPcols_ViewAces%><%--Study Protocols with View access*****--%> </P>		
</Form>

<jsp:include page="getresults.jsp?" flush="true">
<jsp:param name="resultSet" value="<%=resultSet%>"/>
</jsp:include>
 
</body>

</html>



