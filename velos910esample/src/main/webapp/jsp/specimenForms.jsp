<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC. L_Specimen_Forms%><%--Specimen Forms*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<SCRIPT language="javascript">

			function nextPage(formobj)
			{
				var pageNum = 1;

			   	pageNum = parseInt(formobj.pageNum.value) ;
			   	pageNum = pageNum+1;

			   	obj = document.getElementById('rowsPerPage');
				if(!obj){return false;} // YK19APR Bug#3337
			   	rowsPerPage = obj.value;

			   	obj = document.getElementById('totalRows');
			   	if(!obj){return false;} // YK19APR Bug#3337
			   	totalRows = obj.value;


			   	if (totalRows > 0)
			   	{

			   		if ( ((pageNum * rowsPerPage)-totalRows )  <= (rowsPerPage-1) )
			   		{
			   				formobj.pageNum.value = pageNum;
						   	callAjaxGetFormBrowser(formobj);

			   		}
			   		else
			   		{
			   			alert("<%=MC.M_NoMoreData_ForFrm%>");/*alert("There is no more data for this form");*****/
			   			return false;
			   		}
			   	}


			}

			function prevPage(formobj)
			{
				var pageNum = 1;

			   	pageNum = parseInt(formobj.pageNum.value) ;

			   	if (pageNum == 1)
			   	{
			   		alert("<%=MC.M_This_FirstPage%>");/*alert("This is the first page");*****/
			   		return false;
			   	}

			   	pageNum = pageNum-1;

			   	obj = document.getElementById('rowsPerPage');

			   	rowsPerPage = obj.value;

			   	obj = document.getElementById('totalRows');
			   	totalRows = obj.value;


			   	if (totalRows > 0)
			   	{
			  			formobj.pageNum.value = pageNum;
					   	callAjaxGetFormBrowser(formobj);
			   	}


			}


			function callAjaxGetFormBrowser(formobj){


			   var browserurl = "";
			   var filename="";
			   var params = "";
			   var pos = 0;
			   var pagenum = 1;
			   var specimenPk;

			   selval = formobj.formIds.value;

			   pagenum = parseInt(formobj.pageNum.value);
			   specimenPk = formobj.specimenPk.value;


			   //extract data from selval

			   var formid;
			   var entryChar;
			   var entryCount;
			   var disptype ;

			   formid = Nth_Tab(selval, 1, "*");
			   entryChar = Nth_Tab(selval, 2, "*");
			   entryCount = Nth_Tab(selval, 3, "*");
			   disptype = Nth_Tab(selval, 4, "*");

			   if (parseInt(formid) <= 0)
			   {
			    document.getElementById("dynbrowser").innerHTML = "<br><p class='defComments'><%=MC.M_Selc_Frm%><%--Please Select a Form*****--%></p>";
			   }
			   else
			   {
					   browserurl = getFormBrowserURL(formid,entryChar,disptype,formobj);

					   //extract file name and parameters
					   pos = browserurl.indexOf('?');
					   filename=browserurl.substring(0,pos);
					   params = browserurl.substring(pos+1);



					   //attach the target for other pages

					   params = "specimenPk="+specimenPk+"&outputTarget=specFormIds&page="+pagenum + "&"+params;


					   new VELOS.ajaxObject(filename, {
				   			urlData:params ,
						   reqType:"POST",
						   outputElement: "dynbrowser" }

				   ).startRequest();
              }
		}



	</SCRIPT>

</head>

<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="specJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:include page="include.jsp" flush="true"></jsp:include>
<br>
<body>

  <%
	int pageRight = 7;

	HttpSession tSession = request.getSession(true);
	String specimenPk = request.getParameter("specimenPk");
	String study = "";
	String patientpk = "";
	int studyIdInt = 0;
	int personPKInt = 0;
	String patprotpk = "";

	specJB.setPkSpecimen(EJBUtil.stringToNum(specimenPk));
	specJB.getSpecimenDetails();

	study = specJB.getFkStudy();
	patientpk = specJB.getFkPer();

	if (StringUtil.isEmpty(study))
	{
		study = "";
	}
	studyIdInt = EJBUtil.stringToNum(study);

	if (StringUtil.isEmpty(patientpk))
	{
		patientpk = "";
	}
	personPKInt = EJBUtil.stringToNum(patientpk);


	//get patient and study id linked with the specimen



	if (sessionmaint.isValidSession(tSession))
	{

		String userIdFromSession = (String) tSession.getValue("userId");


		String calledFrom = request.getParameter("calledFrom");
		if (calledFrom == null || calledFrom.equals(""))
		{
			calledFrom = "M";
		}

		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

		int  userId = EJBUtil.stringToNum((String) tSession.getValue("userId"));
		UserJB userB = (UserJB) tSession.getValue("currentUser");
   	    int siteId = EJBUtil.stringToNum(userB.getUserSiteId());
		int accId= EJBUtil.stringToNum((String)tSession.getAttribute("accountId"));

		ArrayList arFormLocationTypes = new ArrayList();
		ArrayList arFormLocationDesc = new ArrayList();

		arFormLocationTypes.add("A");
		arFormLocationDesc.add("----------"+LC.L_Acc_Forms/*Account Forms*****/+"----------");

		if (! StringUtil.isEmpty(study) )
		{
			arFormLocationTypes.add("S");
			arFormLocationDesc.add("----------"+LC.L_Std_Forms/*+LC.Std_Study+ Forms*****/+"----------");
		}

	 	if ((! StringUtil.isEmpty(patientpk))  &&  (! StringUtil.isEmpty(study)) )
	 	{
	 		arFormLocationTypes.add("SP");
	 		arFormLocationDesc.add("----------"+LC.L_PatStd_Frms/*+LC.Pat_Patient+" "+LC.Std_Study+" Forms*****/+"----------");

	 		patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(study),EJBUtil.stringToNum(patientpk));
			patprotpk = String.valueOf(patEnrollB.getPatProtId());

		}

	 	if (! StringUtil.isEmpty(patientpk) )
		{
			arFormLocationTypes.add("P");
			arFormLocationDesc.add("----------"+LC.L_PatProfile_Frms/*+LC.Pat_Patient+" Profile Forms*****/+"----------");
		}


		//get patprot


		String frmInfo= "";
		int formId = 0;
		String strFormId = "";
		ArrayList finalFormIds = new ArrayList();
		ArrayList finalFormNames = new ArrayList();
		String frmLocType = "";


		//prepare forms dropdown data
		for (int j=0; j<arFormLocationTypes.size();j++)
		{
				LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
				ArrayList arrFrmNames = null;
		 		ArrayList arrNumEntries = null;
 		 		ArrayList arrFrmIds = null;
		 		ArrayList arrEntryChar = null;
		 		ArrayList arrDispType = null;


		 		 frmLocType = (String)	arFormLocationTypes.get(j);

		 		 if (frmLocType.equals("A"))
		 		 {
				 	lnkFrmDao = lnkformB.getAccountFormsForSpecimen(accId, userId, siteId);
				 }
				 else if (frmLocType.equals("S"))
		 		 {
				 	lnkFrmDao = lnkformB.getStudyFormsForSpecimen(accId,studyIdInt, userId, siteId, 7,  7);
				 }
				 else if (frmLocType.equals("SP"))
		 		 {
				 	lnkFrmDao = lnkformB.getPatientStudyFormsForSpecimen(accId,personPKInt,studyIdInt, userId, siteId);
				 }
				 else if (frmLocType.equals("P"))
		 		 {
				 	lnkFrmDao = lnkformB.getPatientFormsForSpecimen(accId,personPKInt, userId, siteId);
				 	ArrayList dispTypeArray1 = lnkFrmDao.getFormDisplayType();
				 	if (dispTypeArray1 != null) {
				 		for (int iX=0; iX< dispTypeArray1.size(); iX++) {
				 			dispTypeArray1.set(iX, "PA");
				 		}
				 	}
				 }

				 finalFormIds.add("0");
	    		 finalFormNames.add(arFormLocationDesc.get(j).toString());

				 arrFrmIds = lnkFrmDao.getFormId();
				 arrFrmNames = lnkFrmDao.getFormName();
				 arrEntryChar = lnkFrmDao.getFormEntryChar();
				 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
				 arrDispType = lnkFrmDao.getFormDisplayType();


				for (int i=0;i<arrFrmIds.size();i++)
	    	 {
	    		 	//store the formId, entryChar and num Entries separated with a *
	    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString()+ "*" + arrDispType.get(i).toString();
	    		 	 finalFormIds.add(frmInfo);
	    		 	 finalFormNames.add(arrFrmNames.get(i).toString());

	    		 }

		}

	String dformPullDown = "";

	dformPullDown  = EJBUtil.createPullDownWithStrNoSelect("formIds","0", finalFormIds , finalFormNames);

	%>

	<DIV class="browserDefault" id="div1">


	<table width="99%" cellspacing="0" cellpadding="0" border="0" >

<!-- removed some part of code from here  -->
		<tr><td colspan = "4">
		<jsp:include page="inventorytabs.jsp" flush="true">
			<jsp:param name="selectedTab" value="1"/>
			<jsp:param name="specimenPk" value="<%=specimenPk%>" />
			</jsp:include>
		</td>
		</tr>
	</table>



	 <BR>
		    <table width="95%" cellspacing="0" cellpadding="0">
		    <tr>
				<td class=tdDefault width="50%"><B> <%=LC.L_Specimen_Id%><%--Specimen ID*****--%>:</B>&nbsp;&nbsp;<%=specJB.getSpecimenId()%></td>
			</tr>

			</table>

	 <form name="spec" method="POST">

	<%

		if ( pageRight > 0 )
		{
			%>
				 <!-- <select name="formIds" >
				 	<option value="0">-------------- Account Forms-------------</option>
				 	<option value="390">abc form 2</option>
				 	<option value="481">form 3-single</option>
				 	<option value="0">-------------- <%=LC.Std_Study%> Forms-------------</option>
				 	<option value="448">form 3..</option>
				 	<option value="0">-------------- <%=LC.Pat_Patient%> Forms-------------</option>
				 	<option value="395">def form-single-entry-allpat</option>
				 	<option value="389">abc form only <%=LC.Pat_Patient%></option>
				 	<option value="0">-------------- <%=LC.Pat_Patient%> <%=LC.Std_Study%> Forms-------------</option>
				 	<option value="400">27jul-form1</option>
				 	<option value="453">27jul-form2-single</option>

				 </select> -->

				 <%= dformPullDown %>


				 <button type="button"  id = "specFormIds" name="Go" onClick="callAjaxGetFormBrowser(document.spec);"><%=LC.L_Go_Upper%></button>
				 <A href="#" onClick="prevPage(document.spec);"><%=LC.L_Prev_Page%><%--Previous Page*****--%></A>&nbsp;&nbsp;
				 <A href="#" onClick="nextPage(document.spec);"><%=LC.L_Next_Page%><%--Next Page*****--%></A>

			<%

		}
	%>
	<input type=hidden name="specimenPk" value=<%=specimenPk%>>

	<input type=hidden name="pageNum" value=1>

	<!--following hidden fields are needed for forms.

	-->

	<input type=hidden name="formStudy" value=<%=study%> >
	<input type=hidden name="patStudyId" value=<%=study%> >

	<input type=hidden name="patProtId" value="<%=patprotpk%>">
	<input type=hidden name="formPatprot" value="<%=patprotpk%>">

	<input type=hidden name="formPatient" value=<%=patientpk%> >

	</form>
	
	<table width="95%" cellspacing="0" cellpadding="0">
		<tr>
			<td><span id="dynbrowser"></span></td>
		</tr>
	</table>

	  <div>
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	  </div>

	</div>

<div class ="mainMenu" id = "emenu">
	<jsp:include page="getmenu.jsp" flush="true"/>
</div>


<%
	}
%>
</body>

</html>