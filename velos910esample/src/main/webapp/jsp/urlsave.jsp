<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*, com.aithent.audittrail.reports.AuditUtils" %>

</head>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdocB" scope="request" class="com.velos.esch.web.eventdoc.EventdocJB"/>

<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<SCRIPT>

function  validate(formobj){

	if (!(validate_col('e-Signature',formobj.eSign))) return false



	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   }

}

</SCRIPT>

<%

if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))

{ %>



<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))
	%>

<% String src;

	src= request.getParameter("srcmenu");
	String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 10/12/04, cal-enh-05
	String propagateInEventFlag = request.getParameter("propagateInEventFlag");
	//String eventName = request.getParameter("eventName");
	String eventName = "";//KM

	String calAssoc = request.getParameter("calassoc");
    calAssoc = (calAssoc == null) ? "" : calAssoc;



%>

<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){

%>
<jsp:include page="include.jsp" flush="true"/>

<%
}
 else{

%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>

<%}%>



<BODY>

<br>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>

		<DIV class="formDefaultpopup" id="div1">

	<%	}

else { %>

<DIV class="formDefault" id="div1">

<%}%>

<%

String duration = request.getParameter("duration");

String protocolId = request.getParameter("protocolId");

String calledFrom = request.getParameter("calledFrom");

String eventId = request.getParameter("eventId");

String docmode = request.getParameter("docmode");

String mode = request.getParameter("mode");

String fromPage = request.getParameter("fromPage");

String calStatus = request.getParameter("calStatus");

String eventmode = request.getParameter("eventmode");

String displayDur=request.getParameter("displayDur");

String displayType=request.getParameter("displayType");



String eSign = request.getParameter("eSign");

String eventdocId = "";



HttpSession tSession = request.getSession(true);

String ipAdd = (String) tSession.getValue("ipAdd");

String usr = null;

usr = (String) tSession.getValue("userId");




if (calledFrom.equals("P")||calledFrom.equals("L"))
{
	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventdefB.getEventdefDetails();
	  eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
}else{
	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventassocB.getEventAssocDetails();
	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
 }





if (sessionmaint.isValidSession(tSession)) {

	String oldESign = (String) tSession.getValue("eSign");



	if((!docmode.equals("D")) && (!oldESign.equals(eSign)) ) {

%>

  <jsp:include page="incorrectesign.jsp" flush="true"/>

<%

	} else {

   String name = request.getParameter("name");

   String desc = request.getParameter("desc");



   if(docmode.equals("D")) {

		eventdocId = request.getParameter("docId");

		String docType=request.getParameter("docType");



		String delMode=request.getParameter("delMode");

		if (delMode==null) {

			delMode="final";

%>

	<FORM name="deleteFile" id="deleventurl" method="post" action="urlsave.jsp" onSubmit="if (validate(document.deleteFile)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<br><br>

	<jsp:include page="propagateEventUpdate.jsp" flush="true">

		<jsp:param name="fromPage" value="<%=fromPage%>"/>

		<jsp:param name="formName" value="deleteFile"/>

		<jsp:param name="eventName" value="<%=eventName%>"/>

		</jsp:include>

	<P class="defComments"><%=MC.M_PlsEtrEsign_EvtApdxDel %><%-- Please enter e-Signature to proceed with Event Appendix File/Link Delete.*****--%></P>

	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="deleventurl"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>



 	 <input type="hidden" name="delMode" value="<%=delMode%>">

  	 <input type="hidden" name="srcmenu" value="<%=src%>">

  	 <input type="hidden" name="duration" value="<%=duration%>">

  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">

  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">

  	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">

  	 <input type="hidden" name="eventId" value="<%=eventId%>">

  	 <input type="hidden" name="docmode" value="<%=docmode%>">

  	 <input type="hidden" name="mode" value="<%=mode%>">

  	 <input type="hidden" name="fromPage" value="<%=fromPage%>">

  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">

   	 <input type="hidden" name="eventmode" value="<%=eventmode%>">

   	 <input type="hidden" name="displayDur" value="<%=displayDur%>">

   	 <input type="hidden" name="displayType" value="<%=displayType%>">

   	 <input type="hidden" name="docId" value="<%=eventdocId%>">

   	 <input type="hidden" name="docType" value="<%=docType%>">

 	 <input type="hidden" name="eventName" value="<%=eventName%>">



	</FORM>

<%

		} else {

			if(!oldESign.equals(eSign)) {

%>

	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>

<%

			} else {

				if (calledFrom.equals("P") || calledFrom.equals("S")) {

				// SV, Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
				if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
					propagateInVisitFlag = "N";

				if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
					propagateInEventFlag = "N";

				if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y")))
				{
					eventdocB.setEventId(eventId);
					eventdocB.setDocId(EJBUtil.stringToNum(eventdocId));
			   		eventdocB.propagateEventdoc(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "D", calledFrom);
				}
				else
					{
						eventdocB.removeEventdoc(EJBUtil.stringToNum(eventdocId),docType, AuditUtils.createArgs(session,"",LC.L_Evt_Lib));
					}

			}
			else
			{
				eventdocB.removeEventdoc(EJBUtil.stringToNum(eventdocId),docType, AuditUtils.createArgs(session,"",LC.L_Evt_Lib));
			}

%>

<br><br><br><br><br> <p class = "sectionHeadings" align = center> <%=MC.M_Data_DelSucc%><%-- Data deleted successfully.*****--%> </p>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventappendix.jsp?srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" >

<%

			}

		}

   } else {

		if(docmode.equals("M")) {

		   eventdocId = request.getParameter("eventdocId");

		   eventdocB.setDocId(EJBUtil.stringToNum(eventdocId));

		   eventdocB.getEventdocDetails();

	   	}



   		eventdocB.setDocName(name);

		eventdocB.setDocDesc(desc);

   		eventdocB.setEventId(eventId);

		eventdocB.setDocType("U");

	   	if(docmode.equals("M")) {

		   eventdocB.setModifiedBy(usr);

		   eventdocB.setIpAdd(ipAdd);

		   eventdocB.updateEventdoc();

	   	} else {

		   eventdocB.setCreator(usr);

		   eventdocB.setIpAdd(ipAdd);

		   eventdocB.setEventdocDetails();

	   	}

		if (calledFrom.equals("P") || calledFrom.equals("S")) {

			// SV, Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
				if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
					propagateInVisitFlag = "N";

				if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
					propagateInEventFlag = "N";

				if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
					//docmode = "N" - New/Add, "M"- modify/update, and calType = "P" - protocol/library calendar.
			   		eventdocB.propagateEventdoc(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, docmode, calledFrom);
				}
		}
%>

<br><br><br><br><br> <p class = "sectionHeadings" align = center> <%=MC.M_Data_SavedSucc%><%-- Data saved successfully.*****--%> </p>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventappendix.jsp?srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&selectedTab=4&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" >

<%   }

   %>

<%

}//end of if for eSign check

}//end of if body for session



else

{

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}

%>

  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){

  %>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

<%


  }

else {

%>

  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>



</div>



<div class ="mainMenu" id = "emenu">

<jsp:include page="getmenu.jsp" flush="true"/>

</div>

	<% }%>



</BODY>

</HTML>





