<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%@ page language = "java" import="com.velos.eres.service.util.*,velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<jsp:useBean id="eventassocB" scope="request"	class="com.velos.esch.web.eventassoc.EventAssocJB" />

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))	{
	String selStrs =request.getParameter("selStrs");
	//String tableName =request.getParameter("tableName");
	String hideFlagVal =request.getParameter("hideFlagVal");

	String selectedTab=request.getParameter("selectedTab");
	String duration=request.getParameter("duration");
	String protocolId=request.getParameter("protocolId");
	String calledFrom=request.getParameter("calledFrom");
	String mode=request.getParameter("mode");
	String calStatus=request.getParameter("calStatus");

	String calAssoc=request.getParameter("calassoc");
    calAssoc=(calAssoc==null)?"":calAssoc;


    //to check if the id is to delete the visit(s), not the events
    String visitFlags = request.getParameter("visitFlags");



	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";




%>
	<FORM name="hideunhideform" method="post" action="hideunhidecalendar.jsp" onSubmit="return validate(document.hideunhideform)">
	<br><br>

	<%if (hideFlagVal.equals("1")){%>
		<P class="defComments"><%=MC.M_EsignToHide_EvtFromPcol%><%--Please enter e-Signature to hide the Visit(s)/Event(s) from the Protocol.*****--%></P>
	<%}else{ %>
		<P class="defComments"><%=MC.M_EsignToUnhide_EvtFromPcol%><%--Please enter e-Signature to unhide the Visit(s)/Event(s) from the Protocol.*****--%></P>
	<%}%>

	<TABLE width="100%" cellspacing="0" cellpadding="0" >
	<tr>
	   <td width="40%">
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 >
	   </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		 <button type="submit"><%=LC.L_Submit%></button>
		</td>
	</tr>
	</table>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">

  	 <input type="hidden" name="hideFlagVal" value="<%=hideFlagVal%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
  	 <input type="hidden" name="duration" value="<%=duration%>">
  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">
  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">
   	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">
   	 <input type="hidden" name="selStrs" value="<%=selStrs%>" size="150">
   	 <input type="hidden" name="visitFlags" value="<%=visitFlags%>" size="150">


	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {

				StringTokenizer idStrs=new StringTokenizer(selStrs,",");
				int numIds=idStrs.countTokens();
				String[] strArrStrs = new String[numIds] ;



				StringTokenizer flagStrs=new StringTokenizer(visitFlags,",");
				int numFlags=flagStrs.countTokens();
				String[] strArrFlags = new String[numFlags] ;


				for(int cnt=0;cnt<numIds;cnt++)
				{
					strArrStrs[cnt] = idStrs.nextToken();
					strArrFlags[cnt] = flagStrs.nextToken();

					System.out.println("strArrFlags[cnt]==="+ strArrStrs[cnt] + "-"+strArrFlags[cnt]);

				}

				int i=eventassocB.hideUnhideEventsVisits(strArrStrs,hideFlagVal, strArrFlags);


%>
<br><br><br><br><br> <p class = "sectionHeadings" align = center> <%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=fetchProt.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>">

<%
	} //end esign
	} //end of delMode
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</BODY>
</HTML>

