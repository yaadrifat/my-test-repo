<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.service.util.*"%>
<html>
<head>
<title><%=LC.L_About_Us%><%--About Us*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
<!--
a:link {  text-decoration: none; color: #000000}
a:hover {  color: #CC0000; text-decoration: none}
a:active {  color: #000000; text-decoration: none}
a:visited {  text-decoration: none}
-->
</style>
</head>
<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="homepanel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="formDefault" id=div1>
            <table width="592" border="0" cellspacing="0" cellpadding="0" height="43">
              <tr> 
                <td width="269" align="left" valign="top"> 
                  <div align="justify">
                    <p style="margin-left: 0"><b><font face="Lucida Handwriting" size="4"><%=MC.M_VelosUsing_NewTech%><%--V</font></b><font face="Arial" size="2">elos
                    is ushering 
                    in a new generation of Internet-based research capabilities 
                    that fundamentally improves the pervasiveness and efficiency 
                    of clinical research, both for the research team and trial 
                    sponsors. Velos eResearch enables better patient enrollment 
                    and communication, efficient transfer of protocol and data 
                    between investigators, the IRB and the participating sites, 
                    and streamlined task/time management, thereby reducing the 
                    elapsed time and effort required to execute clinical trials.*****--%></font></div>
                </td>
                <td align="center" valign="middle" width="323">
                  <p align="center"><img src="../images/assets/aboutus.gif" width="275" height="177"></p>
                </td>
              </tr>
            </table>
            <table width="560" border="0" cellspacing="0" cellpadding="0" height="75">
              <tr>
                <td width="172" align="left" valign="middle"><font size="5"><b><font face="Arial"><i><font color="#0000CC">&nbsp; 
                  <font size="4"><%=LC.L_Easeofuse%><%--Ease-of-Use*****--%></font></font></i></font> </b> <br>
                  </font></td>
                <td width="388" align="left" valign="top"> 
                  <div align="justify"><font face="Arial" size="2"> 
                    <%=MC.M_VM_SimpleToUse%><%--Velos eResearch is simple and intuitive to use. With basic 
                    software application experience, users and clinical coordinators 
                    can quickly become proficient using Velos eResearch. Our application 
                    is built with an open architecture for ASP platforms in Java 
                    and Oracle. eResearch also provides easy connectivity to 
                    legacy systems through XML-based system integration. 128-bit 
                    encryption and firewall protection ensure system security 
                    and incoming and outgoing data protection.*****--%> </font> </div>
                </td>
              </tr>
            </table>
            <table width="589" border="0" cellspacing="0" cellpadding="0" height="31">
              <tr>
                <td width="420" align="left" valign="top">
                  <div align="justify"><font size="2" face="Arial"><br>
                    <%=MC.M_WorkInClinical_SolveProb%><%--We started in the clinical application domain. By working 
                    with healthcares front-line professionals, we learned how 
                    to solve the unique, complex clinical, administrative, and 
                    financial challenges investigators deal with on a day-to-day 
                    basis. Velos mastery of clinical applications led to the development 
                    of Velos eResearch: investigator-centered, clinically oriented 
                    and web-enabled.*****--%> </font></div>
                </td>
                <td align="left" valign="middle" width="169">
                  <div align="center"><font size="6"><b><font size="5" color="#0000CC"><i><font face="Arial" size="4"><%=LC.L_Why_Velos%><%--Why 
                    Velos?*****--%></font></i></font> </b></font></div>
                </td>
              </tr>
            </table>
            <div align="justify"><br>
              <font size="2" face="Arial">
              <% String Hyper_Link = "<a href=\"http://www.velos.com\"><font color=\"#000099\">"+LC.L_WwwVelos_Com+"</font></a>";
              Object[] arguments = {Hyper_Link}; %><%=VelosResourceBundle.getMessageString("M_ToLrnVelos_VisitWeb",arguments) %>
              <%--To learn more about Velos, its management and other products and services, visit our corporate web site at <a href="http://www.velos.com"><font color="#000099">www.velos.com</font></a> *****--%>
              </font></div>
			  
</div>
</body>
</html>
