<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<%@page import="com.velos.eres.service.util.*"%>
<H2><%=LC.L_Evt_DetPage%><%--Event Detail Page*****--%></H>
<BODY>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil"%>

<% String event_id =request.getParameter("event_id"); 
event_id="2832";
Integer test=new Integer(event_id);

 %>

<% 	com.velos.esch.business.common.EventInfoDao eventinfo=eventdefB.getEventDefInfo(test.intValue());
%>

<% java.util.ArrayList costIds=eventinfo.getCostIds() ; %>
<% java.util.ArrayList costDescriptions=eventinfo.getCostDescriptions() ; %>
<% java.util.ArrayList costValues= eventinfo.getCostValues(); %>
<% java.util.ArrayList eventUserIds= eventinfo.getEventUserIds(); %>
<% java.util.ArrayList userTypes= eventinfo.getUserTypes(); %>
<% java.util.ArrayList userIds= eventinfo.getUserIds(); %>
<% String costvalue="";
   String costdesc="";
   String userId="";
   String userType="";
   String eventuserId="";		
  %>


<%String chain_id=request.getParameter("chain_id");
//out.println(chain_id);
 %>

<form METHOD=POST action="../EventdefLib.jsp">
<TABLE width="90%" BGCOLOR=beige >

<tr><td>
<%=MC.M_FrmLnkTo_ThisEvt%><%--Forms linked to this event*****--%></td>
<td>
<INPUT NAME="form_linked" TYPE= radio><%=LC.L_No%><%--No*****--%> 
<INPUT NAME="form_linked" TYPE= radio><%=LC.L_Yes%><%--Yes*****--%>
</td>
</tr>


<tr><td>
<%=MC.M_IfYes_LnkFrm%><%--If yes link form*****--%> </td>
<td>
 <A href="www.yahoo.com"><%=LC.L_Link_Uri%><%--Link URI*****--%></A> 


 &nbsp&nbsp&nbsp&nbsp    <A href="www.yahoo.com"><%=LC.L_Upload_Docu%><%--Upload Document*****--%></A> </td>
</tr>

<tr><td>
<%=MC.M_Cur_LnkFrm%><%--Currently linked forms are*****--%> <br><br></td>
<td></td>

</tr>

<tr><td>
<%=MC.M_CostAssoc_WiithEvt%><%--Cost associated with this event*****--%>
</td>
<td>
 <A href="AddCost.jsp?event_id=<%= event_id %>"><%=LC.L_Specify_Cost%><%--Specify Cost*****--%></A> </td>
</tr>

<tr><td>
<%=LC.L_Assoc_Costs%><%--Associated Costs are*****--%></td>
</tr>
<% for(int i=0;i<costIds.size(); i++){
costvalue= (String)costValues.get(i);
costdesc= (String)costDescriptions.get(i); %>

<tr>
<td><%=costvalue %> </td>
<td> <%=costdesc %> </td>
</tr>
<%}%>


<tr>
<td>
<%=MC.M_ResrcAccoc_Evt%><%--Resources associated with this event*****--%>
</td>

<td>
 <A href="SelectRoles.jsp?event_id=<%=event_id%>"><%=LC.L_Select_RoleType%><%--Select Role type*****--%></A> 

&nbsp&nbsp&nbsp&nbsp&nbsp <A href="SelectResource.jsp?event_id=<%=event_id%>"><%=LC.L_Select_User%><%--Select user*****--%></A> </td>
</td>


<tr><td>
<%=LC.L_Assoc_Resources%><%--Associated resources are*****--%><br><br></td>
</tr>

<tr> <td>
<% for(int i=0;i<userIds.size(); i++){
userId= (String)userIds.get(i);
userType= (String)userTypes.get(i); 
%>
<%  if(userType.equals("U"))
userId=userId+"(U)";

if(userType.equals("R"))
userId=userId+"(R)";   %>


<%if(userType.equals("U")||userType.equals("R")) out.println(userId +"," ); %> 

<%}%>
</td></tr>


<tr>
</tr><td>
<A href="SelectUser.jsp?event_id=<%=event_id%>"><%=LC.L_Select_User_S%><%--Select User(S)*****--%></A> </td>
</tr>

<tr><td>

<%=MC.M_UsrSel_ToReceieveMsg%><%--Users selected to receieve this message are*****--%><br><br>

</td>
</tr>
<tr> <td>
<% for(int i=0;i<userIds.size(); i++){
userId= (String)userIds.get(i);
userType= (String)userTypes.get(i); 
%>
<%  if(userType.equals("S"))
userId=userId+"(S)"; %>


<% if(userType.equals("S")) out.println(userId+ ","); %> 
<%}%>
</td></tr>


<tr>
<td></td><td>
<INPUT colspan=20 TYPE=submit name="submit" value="submit">
</td>
</tr>

</table>
</form>
       
</body>
</HTML>
