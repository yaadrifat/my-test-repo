<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title></title>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
</head>
<jsp:include page="ui-include.jsp" flush="true"/>
 <script type="text/javascript">
 var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 650 : 500;
jQuery.noConflict();
function openSequenceDialog() //YK: Added for PCAL-20461 - Opens the Dialog Window
{
		jQuery("#embedDataHere").dialog({ 
		height: defaultHeight,width: 700,position: 'right' ,modal: true,
		closeText:'',
		buttons: [
		          {
		              text: L_Close,
		              click: function() { 
		        	  jQuery(this).dialog("close");
		              }
		          }
		      ],
		close: function() { $j("#embedDataHere").dialog("destroy"); },
		beforeClose: function(event, ui) { if(!checkBeforeClose()){ return false;} }
        });
  
}
function loadModifySequenceDialog(logPk,totReds,succRec,unSuccRec) {
			openSequenceDialog(); 
		    jQuery('#embedDataHere').load("bulkUpladViewLogs.jsp?logPk="+logPk+"&totReds="+totReds+"&succRec="+succRec+"&unSuccRec="+unSuccRec);
	}
function checkBeforeClose()
{
	return true;
}
        function doSubmit(csvFile){
		if(csvFile=="New")
		{
		winOpen =window.open("bulkfileuploaddetail.jsp",'_self',false) 
		winOpen.focus();
		}
		else if(csvFile=="Map")
		{
		winOpen =window.open("bulkuploaddetails.jsp",'_self',false) 
		winOpen.focus();
		}
        }
        </script>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="tdmenubaritem6"/>
</jsp:include>
<%@page import="com.velos.eres.bulkupload.business.*,java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.LC"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
String userId="";
ArrayList<String>pkBulkUpload=null;
ArrayList<String> fileName=null;
ArrayList<String> totRecord=null;
ArrayList<String> sucRecord=null;
ArrayList<String> unsucRecord=null;
ArrayList<String> createdOn=null;
ArrayList<String> usrFirstName=null;
ArrayList<String> usrMidName=null;
ArrayList<String> usrLastName=null;
userId = (String) tSession.getValue("userId");
String userName="";
bulkUploadDao bukUpldDao=new bulkUploadDao();
bukUpldDao.getUploadHistory(userId);
pkBulkUpload=bukUpldDao.getPkBulkUpload();
fileName=bukUpldDao.getFileName();
totRecord=bukUpldDao.getTotRecord();
sucRecord=bukUpldDao.getSucRecord();
unsucRecord=bukUpldDao.getUnsucRecord();
createdOn=bukUpldDao.getCreatedOn();
usrFirstName=bukUpldDao.getUsrFirstName();
usrMidName=bukUpldDao.getUsrMidName();
usrLastName=bukUpldDao.getUsrLastName();
%>
<DIV class="tabDefTopN" id="div1">
		<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		</jsp:include>
</DIV>
<DIV class="tabFormTopN tabFormTopN_PAS"  id="div2" style="overflow:visible">
<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0" border="0" >
      
      <tr >
		<td ><b><font size="3"> Bulk Upload Area</font></b></td>
        
        <td colspan="2" ><button type="submit" onclick="doSubmit('New');"  >Start a new upload</button><button type="submit" onclick="doSubmit('Map');">Manage mapping</button></td>
        </tr>
		   
<table>
<BR>
		
	 <%if(!pkBulkUpload.isEmpty()){
	 %>
<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0" border="0" >	
	<tr  >
		<td ><b><font size="3"> Upload History</font></b></td>		
        
		<tr>
		<th>#</th>
		<th>Date of Upload</th>
		<th>Uploaded By</th>
		<th>Upload Log</th>
		</tr>
	   <%for (int errSize=0;errSize<pkBulkUpload.size();errSize++)  
	   {
	   if(!usrFirstName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrFirstName.get(errSize)))
    		userName=usrFirstName.get(errSize);
		}
		if(!usrMidName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrMidName.get(errSize)))
    		userName=userName+" "+usrMidName.get(errSize);
		}
		if(!usrLastName.isEmpty())
		{
    	if(!StringUtil.isEmpty(usrLastName.get(errSize)))
    		userName=userName+" "+usrLastName.get(errSize);
		}
	   %>
		<tr  >
	     <td><%=pkBulkUpload.get(errSize)%></td>
		 <td><%=createdOn.get(errSize)%></td>
		 <td><%=userName%></td>
		 <%
		 String pkblk=pkBulkUpload.get(errSize);
		 String crted=createdOn.get(errSize);
		 String toRec=totRecord.get(errSize);
		 String sucRec=sucRecord.get(errSize);
		 String unSuccRec=unsucRecord.get(errSize);
		 %>
		 <td  align="center"><A href="#" onClick="loadModifySequenceDialog('<%=pkblk%>','<%=toRec%>','<%=sucRec%>','<%=unSuccRec%>')">
		 <img src="../images/jpg/preview.gif" title="<%=LC.L_Preview%>" border="0"></A></td>
         </tr>
	  <%}%>
	  </table>
	   <%}%>
		 
		  
		      
      
	  
	  </div>
	   <div id="editVisitDiv" title="Upload Data" style="display:none">
<div id='embedDataHere'>

</div>
</div>
	  <div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	
<%
} //end of if session times out
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for session time out
%>
</body>

</html>
