<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id="studySiteApndxB" scope="request" class="com.velos.eres.web.studySiteApndx.StudySiteApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.MC,com.velos.eres.service.util.EJBUtil" %>
  
<%
 
 
 String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))
{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
	String oldESign = (String) tSession.getValue("eSign");
	String mode = request.getParameter("mode");
	int iupdate = -1;


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
		int iret = -1;
 	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId"); 

	String type= request.getParameter("type");
	
	
	String url= request.getParameter("name");

	String desc = request.getParameter("desc");
	
	String studyId = request.getParameter("studyId");

	String studySiteId=request.getParameter("studySiteId");
	
	String studySiteApndxId = "";
if(mode.equals("N")){

	studySiteApndxB.setAppendixType(type);

	studySiteApndxB.setAppendixName(url);

	studySiteApndxB.setAppendixDescription(desc);

	studySiteApndxB.setStudySiteId(studySiteId);
	
	studySiteApndxB.setCreator(usr); 
	studySiteApndxB.setIpAdd(ipAdd);
 	iret = studySiteApndxB.setStudySiteApndxDetails();
 	
}
if(mode.equals("M")){
studySiteApndxId = request.getParameter("studySiteApndxId");
studySiteApndxB.setStudySiteApndxId(EJBUtil.stringToNum(studySiteApndxId));
studySiteApndxB.getStudySiteApndxDetails();
studySiteApndxB.setAppendixName(url);
studySiteApndxB.setAppendixDescription(desc);
//KM-#3634
studySiteApndxB.setModifiedBy(usr);
studySiteApndxB.setIpAdd(ipAdd);
iupdate = studySiteApndxB.updateStudySiteApndx();

}
%>
	<br>
	<br>
	<br>
	<br>
	<%if(iupdate == 0 || iret >= 0){%>
	<br><p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=newOrganization.jsp?mode=M&studyId=<%=studyId%>&studySiteId=<%=studySiteId%>">
		<%}%>
	

<%	
}//end of if for eSign check
}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>

</BODY>

</HTML>





