<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.MC,com.velos.eres.service.util.LC,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%
  String selclass;

  String studyId="";	
  String eventName="";
  String protocolId="";
  String study="";	
 
  String studyNo="";

  String tab= request.getParameter("selectedTab");

  HttpSession tSession = request.getSession(true); 

  if (sessionmaint.isValidSession(tSession))

  {
  	String eventmode = request.getParameter("eventmode");   
	String duration = request.getParameter("duration");   
	protocolId = request.getParameter("protocolId");   
	String calledFrom = request.getParameter("calledFrom");   
	String fromPage =request.getParameter("fromPage");
	String mode = request.getParameter("mode");   
	String calStatus= request.getParameter("calStatus");

  	String eventId=request.getParameter("eventId");

	String displayDur=request.getParameter("displayDur");
	String displayType=request.getParameter("displayType");
	String src = request.getParameter("src");	   
	String crflibId = request.getParameter("crflibId");

  	if (mode == null){
	 	mode = "N";
	}else if (mode.equals("M")){
  		 eventId= request.getParameter("eventId");
    }


   if (calledFrom.equals("P")||calledFrom.equals("L"))
    {	  
		  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));		
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
		 

    }else{	
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();     
       	  eventName = eventassocB.getName(); 
		
     }		 

	out.print("<Form method=\"POST\"><Input type=\"hidden\" name=\"mode\" value=\"" +mode+"\"> </Form>");
	String uName = (String) tSession.getValue("userName");
	String modRight = (String) tSession.getValue("modRight");
	int formLibSeq = 0;
	acmod.getControlValues("module");
	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();
	char formLibAppRight = '0';
	formLibSeq = acmodfeature.indexOf("MODFORMS");
	formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
	formLibAppRight = modRight.charAt(formLibSeq - 1);
	if (String.valueOf(formLibAppRight).compareTo("1") == 0)
	  {
%>


<table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0"> 
<tr>
	<td  valign="TOP">
	<% if (tab.equals("1"))
    	{
     		selclass= "selectedTab";
	    } 
	    else
	    {
		selclass = "unselectedTab";
	}%>
		<table class="<%=selclass%>"  cellspacing="0" cellpadding="0"  border="0">
			<tr>
			   	<td rowspan="3" valign="top" >
					<img src="../images/leftgreytab.gif" height=20 border=0 alt=""></td> 
				<td>
					<a href="popuplnkfrmstudy.jsp?selectedTab=1&mode=M&eventId=<%=eventId%>&calStatus=<%=calStatus%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>"><%=MC.M_LnkFrm_StdLib%><%--Link Forms From Study Library*****--%>
					</a>
				</td> 
				<td rowspan="3" valign="top" >
					<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
		        </td>
		   	</tr> 
	   	</table> 
	</td> 
    <td  valign="TOP"> 
		<% if (tab.equals("2"))
    	{
     		selclass= "selectedTab";
		} 
		else
		{
			selclass = "unselectedTab";
		}%>
        <table class="<%=selclass%>" border="0" cellspacing="0" cellpadding="0">
			<tr>
			   	<td rowspan="3" valign="top" >
					<img src="../images/leftgreytab.gif" height=20 border=0 alt=""></td> 
				<td>
					
			<a href="popupaddcrf.jsp?mode=N&crflibmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>selectedTab=2&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&selectedTab=2&displayType=<%=displayType%>&displayDur=<%=displayDur%>&crflibId=<%=crflibId%>"><%=MC.M_FreeTxt_FrmNtStdLib%><%--Free Text Entry(Forms Not In Study Library)*****--%>
					</a>
				</td> 
				<td rowspan="3" valign="top" >
					<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
		        </td>
		   	</tr> 
	   	</table> 
	

	</tr> 
    <tr>
    <td colspan=5 height=10></td>
    </tr> 
</table>
<P class="sectionHeadings"><%=LC.L_Event_Name%><%--Event Name*****--%> : <%=eventName%></P>
	 <%}
  }%>
