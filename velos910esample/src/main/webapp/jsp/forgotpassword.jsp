<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<LINK REL="SHORTCUT ICON" HREF="./favicon.ico">
	<title><%=MC.M_VelosEres_FgetPwd %><%-- Velos eResearch : Forgot Password*****--%></title>
</head>
<script language="JavaScript1.2" src="validations.js"></script>
<script language="JavaScript">
function  validate(formobj){
	
	if (!(validate_col('Login ID',formobj.loginName))){
				 return false
				
				 }
	if (!(validate_col('Email Address',formobj.email))) {
				
				 return false
				 }
   
}
</SCRIPT>

<link rel="stylesheet" href="styles/velos_style.css" type="text/css">
<jsp:useBean id="signupB" scope="request" class="com.velos.eres.web.accountWrapper.AccountWrapperJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,javax.mail.*, javax.mail.internet.*, javax.activation.*,java.lang.*,java.util.*,java.text.*"%>

<%
	String loginName = "";
	String email = "";
	String notes = "";
	String messageTo = "";
	String userEmail = "";
	String messageFrom = "";
	String messageText = "";
	String messageSubject = "";
	String messageHeader = "";
	String messageFooter = "";		
	String completeMessage = "";
	String smtpHost = "";
	String domain = "";

%>


<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff">


<!--  LIBRARY JAVASCRIPT START -->
<script language="JavaScript">
<!--
function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",92,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  fw_menu_0.addMenuItem("<%=MC.M_At_AGlance%>","location='ataglance.html'");/*fw_menu_0.addMenuItem("at a glance","location='ataglance.html'");*****/
  fw_menu_0.addMenuItem("<%=LC.L_InThe_News%>","location='news.html'");/*fw_menu_0.addMenuItem("in the news","location='news.html'");*****/
  fw_menu_0.addMenuItem("<%=LC.L_Partners%>","location='partners.html'");/*fw_menu_0.addMenuItem("partners","location='partners.html'");*****/
  fw_menu_0.addMenuItem("<%=LC.L_Faqs%>","location='faqs.html'");/*fw_menu_0.addMenuItem("FAQs","location='faqs.html'");*****/
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",137,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  fw_menu_1.addMenuItem("<%=LC.L_Prod_Serv%>","location='products.html'");/*fw_menu_1.addMenuItem("products &amp; services","location='products.html'");*****/
  fw_menu_1.addMenuItem("<%=LC.L_Acc_Pricing%>","location='pricing.html'");/*fw_menu_1.addMenuItem("accounts &amp; pricing","location='pricing.html'");*****/
  fw_menu_1.addMenuItem("<%=LC.L_Sec_Hipaa%>","location='security.html'");/*fw_menu_1.addMenuItem("security &amp; HIPAA","location='security.html'");*****/
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  fw_menu_2.addMenuItem("<%=LC.L_Demo%>","location='demo.html'");/*fw_menu_2.addMenuItem("demo","location='demo.html'");*****/
   fw_menu_2.hideOnMouseOut=true;
  window.fw_menu_3 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  fw_menu_3.addMenuItem("<%=LC.L_Contact%>","location='contact.html'");/*fw_menu_3.addMenuItem("contact","location='contact.html'");*****/
   fw_menu_3.hideOnMouseOut=true;
  window.fw_menu_4 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  fw_menu_4.addMenuItem("<%=LC.L_Register%>","location='register.jsp'");/*fw_menu_4.addMenuItem("register","location='register.jsp'");*****/
   fw_menu_4.hideOnMouseOut=true;
  window.fw_menu_5 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#ffffff","#ffffff","#9999ff","#000084");
  fw_menu_5.addMenuItem("<%=LC.L_Home%>","location='index.html'");/*fw_menu_5.addMenuItem("home","location='index.html'");*****/
   fw_menu_5.hideOnMouseOut=true;

  fw_menu_3.writeMenus();
} // fwLoadMenus()


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


//-->
</script>
<SCRIPT LANGUAGE="JavaScript">
function Openflashtour()
	{
	thewindow = window.open('http://www.veloseresearch.com/eres/jsp/producttour.html', 'anew', config='height=430,width=570,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,directories=no,status=no');
	}
</SCRIPT>


<script language="JavaScript1.2" src="fw_menu.js"></script>

<script language="JavaScript1.2">fwLoadMenus();</script>

<!--  LIBRARY JAVASCRIPT END -->


    <table border="0" cellspacing="0" cellpadding="0" width="770">

	 <!-- HEADER OBJECT START-->
	<tr>
	  <td colspan="14"><a href="http://www.velos.com"><img src="./images/toplogo.jpg" width="770" height="60" alt="<%=LC.L_Velos_Eres %><%-- Velos eResearch*****--%>" border="0"></a></td>
	</tr>
	<tr>
	  	  <td align="right"><img src="./images/toplefcurve.gif" width="18" height="16"></td> 
          <td><img src="./images/vspacer.gif" width="201" height="16" border="0"></td> 
          <td><img src="./images/topmidcurve.gif" width="30" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="index.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_5,250,77);" ><img name="Home" src="./images/home.gif" height="16" border="0" alt="<%=LC.L_Home %><%-- Home*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="ataglance.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,335,77);" ><img name="About" src="./images/about.gif" height="16" border="0" alt="<%=LC.L_About %><%-- About*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="products.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,420,77);" ><img name="vpopmenu_r2_c3" src="./images/solutions.gif" height="16" border="0" alt="<%=LC.L_Solutions %><%-- Solutions*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="demo.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,515,77);" ><img name="vpopmenu_r2_c4" src="./images/demo.gif" height="16" border="0" alt="<%=LC.L_Demo %><%-- Demo*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="contact.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,597,77);" ><img name="vpopmenu_r2_c6" src="./images/contact.gif" height="16" border="0" alt="<%=LC.L_Contact %><%-- Contact*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="register.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_4,683,77);" ><img name="vpopmenu_r2_c7" src="./images/register.gif" height="16" border="0" alt="<%=LC.L_Register %><%-- Register*****--%>"></td>
     </tr>
	 </table>
	 <!-- HEADER OBJECT END -->


	 
	   <table width="770" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">


	   		  <tr>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  	  <td width="125" height="450" valign="top" background="./images/leftsidebg.gif">
				  <BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="./images/loginsm.gif" width="68" height="45" border="0" alt="<%=LC.L_Contact %><%-- contact*****--%>">
				  </td>
				  <td valign="top">
				  	  <table cellpadding="10" border="0">
					  <Form name="forgotmail" method="post" onSubmit="return validate(document.forgotmail)" action="forgotmail.jsp" >
					  <tr>
							<td valign="top" colspan="2">
								<img src="images/forgotpwd.gif" width="150" height="21" border="0" alt="<%=LC.L_Forgot_Pwd %><%-- fogot password*****--%>"><BR><BR>
							  	<%=MC.M_EtrInfo_PwdEmailedToYou%><%-- Please enter information as indicated below and click on Submit. Your password will be emailed to you after verification.*****--%>
							</td>

					  </tr>
					  <tr>
    				  <td>
					  <%=LC.L_Login_Id%><%-- Login ID*****--%> <FONT class="Mandatory">* </FONT>
    				  </td>
    				  <td>
					  <input type="text" name="loginName" size = 15 MAXLENGTH = 50></input>
					  </td>
					  </tr>

					  <tr>
    				  <td>
					  <%=LC.L_Email_Addr%><%--Email Address*****--%> <FONT class="Mandatory">* </FONT>
    				  </td>
    				  <td>
					  <input type="text" name="email" size = 15 MAXLENGTH = 50></input>
					  </td>
					  </tr>

					  <tr>
    				  <td>
					  <%=LC.L_Notes%><%--Notes*****--%> 
    				  </td>
    				  <td>

					        <TextArea name="notes" rows=3 cols=50 MAXLENGTH = 2000 ></TextArea>

					  </td>
					  </tr>
  					  <tr>
					  <td align="right">
					  <input type="image" src="./images/submit.gif"  align="absmiddle"  border="0">
					  </td>
  					  </tr>

					  </Form>
	

	
	
					  </table>
				  </td>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  </tr>

      </table>
	  <!-- FOOTER OBJECT START-->
	   <table width="770" cellspacing="0" cellpadding="0" border="0">
	   		  <tr>
			  	  <td rowspan="2"><img src="images/copyright.gif" width="307" height="32" border="0" alt="<%=LC.L_Velos_Copyright %><%-- Velos copyright*****--%>"></td>
          		  <td colspan="6" bgcolor="#FFFFFF"><img src="./images/spacer.gif" width="308" height="16" border="0"></td>
	  	  		  <td align="right"><img src="./images/botrigcurve.gif" width="18" height="16"></td> 
			  </tr>

		<tr>
          <td bgcolor="9999ff"><img src="./images/sspacer.gif" width="8" height="16" border="0"></td>
		  <td><a href="termsofservice.html"><img name="Terms of service" src="./images/termsofservice.gif" width="150" height="16" border="0" alt="<%=LC.L_Terms_OfService %><%-- Terms of Service*****--%>"></a></td>
          <td bgcolor="9999ff"><img src="./images/sspacer.gif" width="1" height="16" border="0"></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td><a href="privacypolicy.html"><img name="Privacy policy" src="./images/privacypolicy.gif" width="150" height="16" border="0" alt="<%=LC.L_Privacy_Policy%><%--Privacy policy*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td><a href="sitemap.html"><img name="Site map" src="./images/sitemap.gif" width="150" height="16" border="0" alt="<%=LC.L_Sitemap %><%-- Sitemap*****--%>"></a></td>
		</tr>
	   </table>
	   <table width="770" cellspacing="0" cellpadding="0" border="0">
        <tr>
          <td colspan="7"><img src="./images/botbar.gif" width="770" height="8" border="0"></td>
        </tr>
	   </table>
	 <!-- FOOTER OBJECT END-->

</table>
</body>
</html>
