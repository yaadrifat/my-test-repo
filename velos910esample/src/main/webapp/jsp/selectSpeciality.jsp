<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Select_Speciality%><%--Select Speciality*****--%></title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" >

	function getSpecialityTitles(formobj)
	{
	mode = formobj.mode.value;
	selIds = new Array();
	selSpeciality = new Array(); //array of selected speciality
	// if(!(validate_chk_radio("Speciality",formobj.chkSpeciality))) return false;
	totrows = formobj.totalrows.value;
	checked = false;
	var k=0;
	/*if (totrows==1) {
		if (formobj.chkSpeciality.checked) {
			checked=true;
		}else {
			checked=false;
		}
	} else {
		for (i=0;i<totrows;i++) {
			if (formobj.chkSpeciality[i].checked) {
				checked=true;
				break;
			} else {
				checked=false;
			}
		}
	}*/

	/*if (!checked) {
		alert("Please select a Speciality.");
		return false;
	}	*/

	if (totrows==1) {
		if (formobj.chkSpeciality.checked) {
			selValue = formobj.chkSpeciality.value;
			pos = selValue.indexOf("*");
			selIds[0] = selValue.substring(0,pos);
			selSpeciality[0] = selValue.substring(pos+1);

		}
	} else {
		j=0;
		for (i=0;i<totrows;i++) {
			if (formobj.chkSpeciality[i].checked) {

				selValue = formobj.chkSpeciality[i].value;
				pos = selValue.indexOf("*");
				selIds[j] = selValue.substring(0,pos);
				selSpeciality[j] = selValue.substring(pos+1);
				j++;
				var len=j;

			}
		}
	}

if (document.layers) {
	if(mode="M")
	{
      window.opener.document.div1.document.patient.selSpecialityIds.value = selIds;
	  window.opener.document.div1.document.patient.txtSpeciality.value = selSpeciality;
	}
} else {
	if(mode="M")
	{
	  window.opener.document.patient.selSpecialityIds.value=selIds;
	  window.opener.document.patient.txtSpeciality.value=selSpeciality;
	}
}

self.close();
	}



	</SCRIPT>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<div class="popDefault" >
<P class="sectionHeadings"> <%=MC.M_MngPat_SelSpcl%><%--Manage <%=LC.Pat_Patients%> >> Select Speciality*****--%> </P>
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
		//String study="";
		String acc = (String) tSession.getValue("accountId");

		String mode=request.getParameter("mode");
		String splAccessRight="";
		int numIds = 0;
		String specialityIds="";
		String specialityNames="";
		specialityIds = request.getParameter("specialityIds");

		/*if(mode.equals("M"))
		{

		specialityNames =  request.getParameter("specialityNames");
		}*/


		String desc="";
		String id="";
		int codeInd=0;
		ArrayList codeLstDesc=null;
		ArrayList codeLstId= null;

		CodeDao cd = new CodeDao();
		cd.getCodeValues("prim_sp");

		cd.getCodelstData(EJBUtil.stringToNum(acc),"prim_sp");
		codeLstDesc = cd.getCDesc();
		codeLstId = cd.getCId();

		int codeLstLen=codeLstId.size();
		String[] j = new String[codeLstLen];

		//if(mode.equals("M"))
		//{
		StringTokenizer idsSpl=new StringTokenizer(specialityIds,",");
		numIds=idsSpl.countTokens();

		for(int h=0;h<codeLstLen;h++)
		{
			j[h]="";
		}

		int index=0;
		String strId= "";
		int  ind=0;
		int[] selIds=null;
		ArrayList arr =null;
		int cnt=0;
		for(cnt=0;cnt<numIds;cnt++)
			{
					strId=idsSpl.nextToken();
					//j[ind] = Integer.toString(
					codeInd = codeLstId.indexOf(Integer.valueOf(strId));

					j[codeInd]=strId;
					ind++;

			}
		//}



		%>
<Form  name="speciality" method="post" action="" onsubmit="return getSpecialityTitles(document.speciality)">
    <table width="40%" cellspacing="2" cellpadding="0" border=0 >
      <tr>

			<th width="50%"><%=LC.L_Speciality%><%--Speciality*****--%></th>
        <th width="10%"> &nbsp;&nbsp;</th>
      </tr>
		<%if(codeLstLen==0)
		{
		}
		else
		{
		for(int i=0;i<codeLstLen;i++)
			{
			String checked= "0";
			desc = codeLstDesc.get(i).toString();
			id=codeLstId.get(i).toString();

			if(i%2==0)
			{%>

          <tr class=browserEvenRow>
			<%}
			   else
			{%>
				<tr class=browserOddRow>
			<%}



			   %>
			<td><%=desc%></td>
				<%

			  if( !j[i].equals(""))
				{ %>
			<td><input type="checkbox" name="chkSpeciality" value="<%=id%>*<%=desc%>" checked></td>

				<%}
			else{%>
			<td><input type="checkbox" name="chkSpeciality" value="<%=id%>*<%=desc%>" ></td>
				<%}%>


		   </tr>
	<%}
		}%>
			<tr>

	<td colspan=2 align="center"><button type="submit"><%=LC.L_Submit%></button></td>
	</tr>

    </table>
	<Input type="hidden" name="checkedrows" value=0>
	 <Input type="hidden" name="totalrows" value=<%=codeLstLen%> >
    <input type="hidden" name="mode" value=<%=mode%>>
</Form>

<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</div>
</body>

</html>

