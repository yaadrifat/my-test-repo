<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*"%>

<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrldao1" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%

   HttpSession tSession2 = request.getSession(true);
   if (sessionmaint2.isValidSession(tSession2))
	{
	String codeType = request.getParameter("codeType");
	String ddName = request.getParameter("ddName");
	String parentCodePK = request.getParameter("selectedVal");
	String ddType = request.getParameter("ddType");
	String opt = request.getParameter("from");
	String prop = request.getParameter("prop");

	String userRole = request.getParameter("role");
	userRole = (userRole==null)?"":userRole;


	if(prop == null) prop ="";

	if(opt == null)
		opt ="";


	String defUserGroup = (String) tSession2.getAttribute("defUserGroup");
	//KM-01Sep
	String accId = (String)tSession2.getValue("accountId");


	 String ddString = "";
	 int length= 0;
	 ArrayList catIds = null;
	 ArrayList catNames= null;

	 if (StringUtil.isEmpty(ddType))
	 {
	 	ddType = "";
	 }

	CodeDao cdDD = new CodeDao();

	if (ddType.equals("child"))
	{
		if (userRole.equals("")){
			cdDD.getCodeValuesForCustom1(codeType,parentCodePK);
		}else{
			cdDD.getCodeValuesForStudyRoleAndCustom1(codeType,parentCodePK, userRole);
		}
	}

	else if (ddType.equals("category"))
	{
			ctrldao1= eventdefB.getHeaderList(EJBUtil.stringToNum(accId),"L",EJBUtil.stringToNum(parentCodePK));
			catIds = ctrldao1.getEvent_ids();
			catNames = ctrldao1.getNames();

			length = catIds.size();

	}

	else
	{
		if (userRole.equals("")){
			cdDD.getCodeValues(codeType);
		}else{
			cdDD.getCodeValuesForStudyRoleAndCustom1(codeType,parentCodePK, userRole);
		}
	}


	if((!ddType.equals("category"))) {

		cdDD.setCType(codeType);
		cdDD.setForGroup(defUserGroup);


		ddString = cdDD.toPullDown(ddName, prop);

	}
	else
	{
		//Added by Manimaran for dynamic dropdown changes for category
		StringBuffer mainStr = new StringBuffer();

		mainStr.append("<select  name="+ddName+" "+prop+">");

		if(opt.equals("All"))
			mainStr.append("<option value='' Selected>"+LC.L_All/*All*****/+" </option>");
		else if (opt.equals("valAll"))
			mainStr.append("<option value='All'>"+LC.L_All/*All*****/+" </option>");
		else if (opt.equals("valAllCat"))
			mainStr.append("<option value='All'>"+LC.L_All/*All*****/+" </option>");

		else
			mainStr.append("<option value='' Selected> "+LC.L_Select_AnOption/*Select an Option*****/+"</option>");

		for(int i = 0; i< length;i++)
		{
				if(opt.equals("valAllCat")) {
					mainStr.append("<option value="+catIds.get(i)+"%"+catNames.get(i)+">"+catNames.get(i)+" </option>");
				}
				else {
					mainStr.append("<option value="+catIds.get(i)+">" +catNames.get(i)+" </option>");
				}



		}
		mainStr.append("</select>");

		ddString = mainStr.toString();

	}
%>
<%= ddString %>

<% } %>