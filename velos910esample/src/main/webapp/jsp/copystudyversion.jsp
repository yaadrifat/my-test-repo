<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) { 
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=LC.L_StdVer_Copy%><%--<%=LC.Std_Study%> Version >> Copy*****--%></title>
<% } %>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.StringUtil"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT Language="javascript">

 function  validate(formobj){
 	if (!(validate_col('new Version Number',formobj.newVerNum))) return false
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
   }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>
<br>
<DIV id="div1"> 
  <%
    String from="version";
    String selectedTab = "";
	String studyVer="";
	int studyVerId =0;
	String newVerNum ="";
	String verDate = "";
	String verCat = "";
	String verType = "";

	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		selectedTab=request.getParameter("selectedTab");
		String study = (String) tSession.getValue("studyId");
		int studyId = EJBUtil.stringToNum(study);
		String usrId = (String) tSession.getValue("userId");
		int usr = EJBUtil.stringToNum(usrId);
		String accId = (String) tSession.getValue("accountId");	
		int accountId = EJBUtil.stringToNum(accId);
		studyVer = request.getParameter("studyVerId");
		String copyMode=request.getParameter("copyMode");
%>
<jsp:include page="<%=includeTabsJsp%>" flush="true">
<jsp:param name="from" value="<%=from%>"/> 
</jsp:include>
<%
if(copyMode.equals("")) {
	copyMode="final";
%>
<Form name="copyversion" id ="copyverfrm" method="post" action="copystudyversion.jsp" onsubmit="if (validate(document.copyversion)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<P class = "defComments"> <%=MC.M_Etr_NewVerNum%><%--Please enter your new Version number*****--%> </P>
	<TABLE width="60%" cellspacing="0" cellpadding="0" >	
	<tr>
	   <td width="30%" align="right">
		<%=LC.L_New_VersionNumber%><%--New Version Number*****--%>&nbsp; <FONT class="Mandatory">* </FONT> 
	   </td>
	   <td width="30%">
	   <!-- modified by Ganapathy on 05/24/05 -->
		<input type="text" name="newVerNum" size="28" maxlength=100>
	   </td>
	</tr>
	</table>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="copyverfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>



  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="studyVer" value="<%=studyVer%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
   	 <input type="hidden" name="copyMode" value="<%=copyMode%>">	 
</form>

<%
} //end of if for checking whether copyMode.equals("")
 else if(copyMode.equals("final")) {
 		String eSign = request.getParameter("eSign");	
		String oldESign = (String) tSession.getValue("eSign");
		if(!oldESign.equals(eSign)) {
%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
		} else {
%>

<br><br><br><br><br><br>		
<% 
   studyVer = request.getParameter("studyVer");
   studyVerId =  EJBUtil.stringToNum(studyVer);
   newVerNum = request.getParameter("newVerNum");
   if (newVerNum == null) { newVerNum = ""; }
   newVerNum = StringUtil.htmlEncodeXss(newVerNum.trim());
     
   verDate = request.getParameter("studyVerDate");
   verCat =  request.getParameter("studyVerCat");
   verType =  request.getParameter("studyVerType");
//JM: 11/18/2005: Modified: verDate,verCat, verType parameters added
   int ret = studyVerB.copyStudyVersion(studyVerId,newVerNum,verDate,verCat, verType, usr);
   if (ret == -3) { //duplicate version number
%>
 <table>
 <tr>
 <td>
   <P class="sectionHeadings" align = "center"><%=MC.M_VerNumExst_EtrAgain%><%--The Version Number you have entered already exists. Please click on Back button and enter it again.*****--%> </P>
 </td>
 </tr>
  <tr height=20></tr>
 <tr>
  <td align=center>
		<button type="button" onclick="window.history.back();"><%=LC.L_Back%></button>
 </td>		
 </tr>
 </table>		
<%  
  } else if (ret == -1) {
%>
<P class="sectionHeadings" align = "center"><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%>  </P>
<%
   } else {
%>
<P class="sectionHeadings" align = "center"><%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%> </P>

<META HTTP-EQUIV=Refresh CONTENT="2; URL=studyVerBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mode=N">

<%
	}
	} //esign

}

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>