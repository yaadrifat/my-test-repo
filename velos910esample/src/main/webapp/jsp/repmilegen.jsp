<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<TITLE><%=LC.L_Rpt_Output%><%--Report Output*****--%></TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="savedRepB" scope="request" class="com.velos.eres.web.savedRep.SavedRepJB"/>


<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,java.io.*,java.text.*,java.net.URL,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.SavedRepDao,com.velos.eres.business.common.MileRepGen"%>

<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>

<%
  HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))
  {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String src="";
	src= request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");

	String studyPk=request.getParameter("studyPk");
	int studyId = EJBUtil.stringToNum(studyPk);

	String genRepName = request.getParameter("genRepName"); //user entered report name
	//validate report name. It should be unique in a study
	SavedRepDao savedRepDao = savedRepB.validateReportName(studyId,genRepName);
	int cnt = savedRepDao.validateReportName(studyId,genRepName);
	if (cnt > 0) {
%>
	<table width=100%>
	<tr>
	<td align=center>
	<p class = "sectionHeadings"><%=MC.M_Rpt_WithName%><%--Report with the name*****--%> '<%=genRepName%>' <%=MC.M_AldyExstInStd_EtrDiffName%><%--already exists in this <%=LC.Std_Study_Lower%>. Please click on Back button to enter a different name.*****--%>
	</p>
	</td>
	</tr>
	<tr height=20></tr>
	<tr>
	<td align=center>
		<button onclick="window.history.back();"><%=LC.L_Back%></button>
	</td>
	</tr>
	</table>
<%
	return;
	}
	String intervalType = "";
	String repName=request.getParameter("repName");
	String dtFrom=request.getParameter("dateFrom");
	String dtTo=request.getParameter("dateTo");

	String repArgsDisplay = "";
	String monthString = null;
	String filterType = request.getParameter("filterType");

	String month = request.getParameter("month");
	String year = null;
	String format = "";

	if (filterType == null) {
		filterType = request.getParameter("filterType2"); ///for account reports
	}

	if(filterType == null) filterType = "";
	if(filterType.equals("2")) { //Year
		year = request.getParameter("year");
		dtFrom = "01/01/"+year;
		dtTo = "12/31/"+year;
	}

	if(filterType.equals("3")) { //Month
		year = request.getParameter("year1");
		dtFrom = month+"/01/"+year;
		String lastDate="";
		switch(EJBUtil.stringToNum(month)) {
			case(1):
				monthString = LC.L_January/*"January"*****/;
				lastDate="31";
				break;
			case(2):
				monthString = LC.L_February/*"February"*****/;
				int yrNum=0;
				yrNum = EJBUtil.stringToNum(year);
				if (((yrNum) % (4)) == 0) {
					lastDate="29";
				}else {
					lastDate="28";
				}
				break;
			case(3):
				monthString = LC.L_March/*"March"*****/;
				lastDate="31";
				break;
			case(4):
				monthString = LC.L_April/*"April"*****/;
				lastDate="30";
				break;
			case(5):
				monthString = LC.L_May/*"May"*****/;
				lastDate="31";
				break;
			case(6):
				monthString = LC.L_June/*"June"*****/;
				lastDate="30";
				break;
			case(7):
				monthString = LC.L_July/*"July"*****/;
				lastDate="31";
				break;
			case(8):
				monthString = LC.L_August/*"August"*****/;
				lastDate="31";
				break;
			case(9):
				monthString = LC.L_September/*"September"*****/;
				lastDate="30";
				break;
			case(10):
				monthString = LC.L_October/*"October"*****/;
				lastDate="31";
				break;
			case(11):
				monthString = LC.L_November/*"November"*****/;
				lastDate="30";
				break;
			case(12):
				monthString = LC.L_December/*"December"*****/;
				lastDate="31";
				break;
		}
		dtTo = month+"/"+lastDate+"/"+year;
	}

	if(filterType.equals("1")) { //All Dates
		dtFrom = "01/01/1900";
		dtTo = "12/31/3000";
	}

	if(dtFrom == null) dtFrom = "";
	dtFrom = dtFrom.trim();

	if (!(dtFrom.equals(""))) {
		//converting dates to the required format, it should always be mm/dd/yyyy and not m/dd/yyyy or mm/d/yyyy

		format="MM/dd/yyyy";
		SimpleDateFormat dformat=new SimpleDateFormat(format);

		Calendar cal=  Calendar.getInstance();

		cal.setTime(dformat.parse(dtFrom));

		format="MM/dd/yyyy";
		SimpleDateFormat FromFormat=new SimpleDateFormat(format);
		dtFrom = FromFormat.format(cal.getTime());

		cal.setTime(dformat.parse(dtTo));
		format="MM/dd/yyyy";
		SimpleDateFormat ToFormat=new SimpleDateFormat(format);
		dtTo = ToFormat.format(cal.getTime());

		if(filterType.equals("1")) { //All Dates
			repArgsDisplay = LC.L_All/*"ALL"*****/;
		}
		else {
			repArgsDisplay = dtFrom + " "+LC.L_To/*To*****/+" " + dtTo ;
		}

	}

	int repId = EJBUtil.stringToNum(request.getParameter("repId"));
	int repXslId = 0;

	String dispStyle=request.getParameter("dispStyle");

	String orgId = request.getParameter("orgId");
	if (orgId ==null) orgId="0";

	String patientId = request.getParameter("id");
	int patId=EJBUtil.stringToNum(patientId);

   	String uName =(String) tSession.getValue("userName");
 	int userId = EJBUtil.stringToNum((String) (tSession.getValue("userId")));
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String ipAdd = (String) tSession.getValue("ipAdd");
	%>
	<p class = "sectionHeadings" align = center> <%=MC.M_YourReq_ForRpt%><%--Your request for the report*****--%> "<%=repName%>" <%=MC.M_EmailSent_AfterRptGenr%><%--has been accepted. An email will be sent to you after report generation.*****--%></p>
	<%
	MileRepGen mRepGen = new MileRepGen();
	mRepGen.callGenRep(userId,ipAdd,acc,repId,repName,genRepName,dtFrom,dtTo,studyPk,patientId,uName,repArgsDisplay,orgId);
	%>
	<META HTTP-EQUIV=Refresh CONTENT="2; URL=repmilestone.jsp?srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>">
<%
} //end of if session times out

else

{

%>

<jsp:include page="timeout.hmtl" flush="true"/>


<%

}

%>


</body>
</html>
