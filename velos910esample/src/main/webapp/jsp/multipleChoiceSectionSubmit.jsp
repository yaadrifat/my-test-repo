<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Fld_MultiChoice%><%--Field Multiple Choice*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<jsp:useBean id="fieldRespB" scope="request" class="com.velos.eres.web.fldResp.FldRespJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.business.fieldLib.impl.FieldLibBean"%>
<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
			int errorCode=0;
			String mode=request.getParameter("mode");
			String calledFrom = request.getParameter("calledFrom");
			String lnkFrom = request.getParameter("lnkFrom");			
			String refresh=request.getParameter("refresh");
			String studyId = request.getParameter("studyId");
			//new responses made through refresh link
			String respFromRefresh=request.getParameter("respFromRefresh");
			if (respFromRefresh==null) respFromRefresh="false";
									
			String moreRespNo=request.getParameter("moreRespNo");
			String fldLibId = "" ;
			int fieldLibId = 0;
			int formFieldId = 0;   
			
			// for default response 
			//String fldDefResp = request.getParameter(rdDefault);
			//if  ( fldDefResp == null )
			//fldDefResp = "0" ;
			
			
			
			
			//String fldCategory="";
			String fldName="";
			String fldNameFormat="";
			String fldExpLabel = "";
			String fldDesc="";
			String fldUniqId="";
			String fldKeyword="";
			String fldInstructions="";
			String fldType="";
			String fldDataType="";
			String fldColCount="";
		
		
		// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
			// LIKE BOLD, SAMELINE, ALIGN , ITALICS
			Style aStyle = new Style();
			String formId="";
			String formFldId="";
			String formSecId="";
			String fldMandatory="";
			String fldSeq="";
			String fldAlign="";
			String samLinePrevFld="";
			String fldBold="";
			String fldItalics="";
		
				
			String accountId="";
			String fldLibFlag="F"; 
			String fldBrowserFlg="0" ;
			String fldIsVisible ="";
			String codeStatus = request.getParameter("codeStatus");
			
			String hideLabel = "";
			String labelDisplayWidth= "";
			String hideResponseLabel = "";
			String responseAlign = "";
			String sortOrder="";
		
			
			
			String creator="";
			String ipAdd="";
			int countResp = 0;
			
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");

			String recordType=mode;  
			formId=request.getParameter("formId");
			//fldCategory=request.getParameter("category");
			fldName=request.getParameter("name");
			
			fldNameFormat=request.getParameter("nameta");
			fldNameFormat=(fldNameFormat==null)?"":fldNameFormat;
			
			fldDesc=request.getParameter("description");
			fldUniqId=request.getParameter("uniqueId");
			fldKeyword=request.getParameter("keyword");
			fldInstructions=request.getParameter("instructions");
			fldType="M"; //mulitple choice box type to be entered by us 
			
			fldColCount=request.getParameter("colcount");

			formSecId=request.getParameter("section");
			fldSeq=request.getParameter("sequence");
			fldMandatory=request.getParameter("mandatory");
			String mandatoryChk = request.getParameter("overRideMandatory");
			if(mandatoryChk == null || mandatoryChk.equals(""))
				mandatoryChk = "0";
			/*Commented as per UCSF requirements*/
			fldAlign=request.getParameter("align");
			//fldAlign = "right";
			samLinePrevFld=request.getParameter("sameLine");
			fldBold=request.getParameter("bold");
			fldItalics=request.getParameter("italics");
			fldExpLabel = request.getParameter("expLabel");
			fldIsVisible = request.getParameter("isvisible");
			
				fldDataType=request.getParameter("editBoxType");
		 
			//  We SET THE STYLE WE CHECK ITS VALUES TO BEFORE SENDING TO THE DB
			aStyle.setAlign(fldAlign);
			if(fldMandatory==null){
			fldMandatory="0";
			}
			if (fldBold==null)
			aStyle.setBold("0");
			else if (fldBold.equals("1"))
			aStyle.setBold("1");

			if (fldItalics==null)  
			aStyle.setItalics("0");
			else if (fldItalics.equals("1"))
			aStyle.setItalics("1");
			
			
			if (samLinePrevFld==null)
			aStyle.setSameLine("0");
			else if (samLinePrevFld.equals("1"))
			aStyle.setSameLine("1");


			if(fldExpLabel==null){
				aStyle.setExpLabel("0");
				fldExpLabel="0";
			}
			else
		   {
				aStyle.setExpLabel("1");
				fldExpLabel="1";
		   }
		   if(fldIsVisible.equals("2"))
		   {
							
				aStyle.setColor("lightgrey");

		   }
		   
		   	hideLabel = request.getParameter("hideLabel");
			if(StringUtil.isEmpty(hideLabel)){
				hideLabel="0";
			}
			labelDisplayWidth = request.getParameter("labelDisplayWidth");
			
			hideResponseLabel = request.getParameter("hideResponseLabel");
			if(StringUtil.isEmpty(hideResponseLabel)){
				hideResponseLabel="0";
			}
			
			responseAlign = request.getParameter("responseAlign");
			if(StringUtil.isEmpty(responseAlign)){
				responseAlign="";
			}
			
			sortOrder = request.getParameter("sortOrder");
			if(StringUtil.isEmpty(sortOrder)){
				sortOrder="";
			}
			
	
			FieldLibBean fieldLsk = new FieldLibBean();
			
			fieldLsk.setFormId(formId );
			//fieldLsk.setCatLibId(fldCategory );
			fieldLsk.setFldName(fldName);
			fieldLsk.setFldNameFormat(fldNameFormat);
			
			fieldLsk.setFldUniqId(fldUniqId);
			fieldLsk.setFldKeyword(fldKeyword);
			fieldLsk.setFldInstructions(fldInstructions);
			fieldLsk.setFldDesc(fldDesc);
			fieldLsk.setFldType(fldType);
			fieldLsk.setFldDataType(fldDataType);
			//KM-#3835
			fieldLsk.setCreator(creator);
			fieldLsk.setIpAdd(ipAdd);
			fieldLsk.setRecordType(recordType);
			fieldLsk.setAccountId(accountId);
			fieldLsk.setLibFlag(fldLibFlag);
			fieldLsk.setFldColCount(fldColCount);
			fieldLsk.setFormFldBrowserFlg(fldBrowserFlg);
			fieldLsk.setFldIsVisible(fldIsVisible);
			
			fieldLsk.setFormSecId(formSecId );
			fieldLsk.setFormFldSeq( fldSeq );
			fieldLsk.setFormFldMandatory( fldMandatory );
			
			fieldLsk.setExpLabel(fldExpLabel);
			fieldLsk.setAStyle(aStyle);
			fieldLsk.setFldHideLabel(hideLabel);
			fieldLsk.setFldDisplayWidth(labelDisplayWidth);
			
			fieldLsk.setFldResponseAlign(responseAlign);
			fieldLsk.setFldHideResponseLabel(hideResponseLabel);
			fieldLsk.setOverRideMandatory(mandatoryChk);
			
			fieldLsk.setOverRideFormat("0");
			fieldLsk.setOverRideRange("0");
			fieldLsk.setOverRideDate("0");
			
			fieldLsk.setFldSortOrder(sortOrder);
			
			if (mode.equals("M"))
			{
				formFldId=request.getParameter("formFldId");
				formFieldId = EJBUtil.stringToNum(formFldId);
				formFieldJB.setFormFieldId(formFieldId);
				formFieldJB.getFormFieldDetails();
				fldLibId=formFieldJB.getFieldId();
				fieldLibId=EJBUtil.stringToNum(fldLibId);
				
				
				fieldLibJB.setFieldLibId(fieldLibId );
				fieldLibJB.getFieldLibDetails();
				fieldLsk  = fieldLibJB.createFieldLibStateKeeper();
	
				aStyle.setSameLine(samLinePrevFld);		
				fieldLsk.setFormId(formId );
				fieldLsk.setFormFldId(formFldId );
				//fieldLsk.setCatLibId(fldCategory );
				fieldLsk.setFldName(fldName);
				fieldLsk.setFldNameFormat(fldNameFormat);
				fieldLsk.setFldUniqId(fldUniqId);
				fieldLsk.setFldKeyword(fldKeyword);
				fieldLsk.setFldInstructions(fldInstructions);
				fieldLsk.setFldDesc(fldDesc);
				fieldLsk.setFldType(fldType);
				fieldLsk.setFldDataType(fldDataType);
				fieldLsk.setIpAdd(ipAdd);
				fieldLsk.setRecordType(recordType);
				fieldLsk.setAccountId(accountId);
				fieldLsk.setLibFlag(fldLibFlag);
				fieldLsk.setFldColCount(fldColCount);
				fieldLsk.setFldIsVisible(fldIsVisible);
			
			
				fieldLsk.setModifiedBy(creator);
				fieldLsk.setFormSecId(formSecId );
				fieldLsk.setFormFldSeq( fldSeq );
				fieldLsk.setFormFldMandatory( fldMandatory );
				fieldLsk.setExpLabel(fldExpLabel);
				
				fieldLsk.setAStyle(aStyle);
				fieldLsk.setFldHideLabel(hideLabel);
				fieldLsk.setFldDisplayWidth(labelDisplayWidth);
				fieldLsk.setFldResponseAlign(responseAlign);
				fieldLsk.setFldHideResponseLabel(hideResponseLabel);		
				fieldLsk.setOverRideMandatory(mandatoryChk);		
				fieldLsk.setFldSortOrder(sortOrder);
				
				fieldLsk.setOverRideFormat("0");
				fieldLsk.setOverRideRange("0");
				fieldLsk.setOverRideDate("0");
			
			}
			
			
			

			//this indicates new responses have been entered 
			//either through new mode or Refresh link
           	FldRespDao fldRespDao=new FldRespDao();			
			if ((mode.equals("N")) || respFromRefresh.equals("true")) 
			{
     			String[] saSeq = request.getParameterValues("newTxtSeq");            			
             	String[] saDispVal=request.getParameterValues("newTxtDispVal");
             	String[] saDataVal=request.getParameterValues("newTxtDataVal");
             	String[] saScore=request.getParameterValues("newTxtScore");
             	String[] saDefault=request.getParameterValues("newHdDefault");
             	String[] saRecordType=request.getParameterValues("newHdRecordType"); 
				          				
						
				for(int cnt=0 ;	cnt<saDispVal.length ; cnt++)
				{
					
					if(!saDispVal[cnt].equals("")){
						countResp++;
					}
				}
				
				String[] seq = new String[countResp];
				String[] dispVal = new String[countResp];
				String[] dataVal = new String[countResp];
				String[] score = new String[countResp];
				String[] defaultVal = new String[countResp];
				String[] rcrdType = new String[countResp];
				int i=0;
				for(int count=0 ;	count<saDispVal.length ; count++)
				{
					if(!saDispVal[count].equals("")){
						seq[i] = saSeq[count];
						dispVal[i] = saDispVal[count].trim();
						dataVal[i] = saDataVal[count].trim();
						score[i] = saScore[count];
						defaultVal[i] = saDefault[count];
						rcrdType[i] = saRecordType[count];
						i++;
					}
				}
				
				fldRespDao.setSequences(seq);
             	fldRespDao.setScores(score);
             	fldRespDao.setDispVals(dispVal);
             	fldRespDao.setDataVals(dataVal);
             	fldRespDao.setDefaultValues(defaultVal);
             	fldRespDao.setLastModifiedBy(creator);
             	fldRespDao.setCreator(creator);
             	fldRespDao.setRecordTypes(rcrdType);
             	fldRespDao.setIPAdd(ipAdd);
			
			}
			
			
			// Set the field Lib State Keeper
			if (mode.equals("N"))
			{				
//				fieldLsk.setCreator(creator);
				//pk of new form field is returned if successful
			errorCode=fieldLibJB.insertToFormField( fieldLsk, fldRespDao)		;	
				formFieldId = errorCode;
			}					

			if (mode.equals("M"))
			{
				aStyle= new Style();	
				

				//Anu
				String[] saRespId = request.getParameterValues("fldRespId"); 
     			String[] saSeq = request.getParameterValues("txtSeq");            			
             	String[] saDispVal=request.getParameterValues("txtDispVal");
             	String[] saDataVal=request.getParameterValues("txtDataVal");
             	String[] saScore=request.getParameterValues("txtScore");
             	String[] saDefault=request.getParameterValues("hdDefault");
             	String[] saRecordType=request.getParameterValues("hdRecordType");
				
				
	
				
            	FldRespDao fldRespDaoModify=new FldRespDao();
				fldRespDaoModify.setFldRespIds(saRespId);          		
             	fldRespDaoModify.setSequences(saSeq);

             	fldRespDaoModify.setScores(saScore);



				for(int cnt=0 ;	cnt<saDispVal.length ; cnt++)
				{
					
					if(!saDispVal[cnt].equals("")){
						countResp++;
					}
				}
				String[] dispVal = new String[countResp];
				String[] dataVal = new String[countResp];

				int i=0;
				for(int count=0 ;	count<saDispVal.length ; count++)
				{
					if(!saDispVal[count].equals("")){             	
						dispVal[i] = saDispVal[count].trim();
						dataVal[i] = saDataVal[count].trim();
						
						i++;
					}
				}
				fldRespDaoModify.setDispVals(dispVal);
             	fldRespDaoModify.setDataVals(dataVal);


				

				
             	fldRespDaoModify.setDefaultValues(saDefault);
             	fldRespDaoModify.setLastModifiedBy(creator);
             	fldRespDaoModify.setRecordTypes(saRecordType);
             	fldRespDaoModify.setIPAdd(ipAdd);

				//this implies that new responses have also been added
				if (respFromRefresh.equals("true")) 
				{
				   fldRespDao.setField(fldLibId);
					errorCode = fieldLibJB.insertFieldLibResponses(fldRespDao);
				}				
								
				// UPDATE CALLED HERE 
				errorCode=fieldLibJB.updateToFormField( fieldLsk,	fldRespDaoModify)	;
		}
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>

<%             if (errorCode == -3 )
				{
%>
				<br><br><br><br><br><p class = "successfulmsg" align = center>
				 <%=MC.M_UnqFldName_EtrFldId%><%--The unique field name already exists. Please enter a different Field ID.*****--%></P>
				<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
				
				
<%
				} else 
%>				
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<%=MC.M_Not_Succ%><%--Not Successful*****--%></p>
<%	
		} //end of if for Error Code

		else

		{
%>		<br><br><br><br><br><p class = "successfulmsg" align = center>	
		<%=MC.M_Data_SavedSucc%><%--Data saved Successfully*****--%></p>
		<%if (refresh.equals("true")) 
		{ %>
   <META HTTP-EQUIV=Refresh CONTENT="1; URL=multipleChoiceSection.jsp?mode=M&formFldId=<%=formFieldId%>&moreRespNo=<%=moreRespNo%>&refresh=true&formId=<%=formId%>&codeStatus=<%=codeStatus%>&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>">  
		<%} else {%>
			 <script>
				window.opener.location.reload();
				setTimeout("self.close()",1000);
			</script>		

		 <%}%> 
		
	<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
