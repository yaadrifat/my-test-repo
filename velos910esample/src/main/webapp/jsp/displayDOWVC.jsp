<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<BODY>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = 
"com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>


<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>


<DIV class="browserDefault" id="div1"> 

	<jsp:include page="protocoltabs.jsp" flush="true"> 
	</jsp:include>

<%

	int pageRight=0;
	String eventId="";
	String protocolId=request.getParameter("protocolId");
	String eventType="";	 
	String name="";
	String duration= request.getParameter("duration");
	String eventDur = "";
	String description="";
	String displacement="";
	String orgId="";
	Integer Id=new Integer(protocolId);
	int weeks = 0;
	Integer dur= new Integer("2");
	int temp = 1;
	ArrayList disps = new ArrayList();

	HttpSession tSession = request.getSession(true); 

    if (sessionmaint.isValidSession(tSession)) {
	
	String userId = (String) (tSession.getValue("userId"));
	String accId = (String) (tSession.getValue("accountId"));
	
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
   	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));   


	pageRight = 7;
	if (pageRight > 0 )
	{
	
	eventType = "P";

	String mode = request.getParameter("mode");
	String calledFrom = request.getParameter("calledFrom");

	ArrayList neweventIds= new ArrayList(); 
	ArrayList newnames= new ArrayList(); 
	ArrayList eventIds=null; 
	ArrayList protocolIds=null; 
	ArrayList names=null ; 
	ArrayList descriptions= null; 
	ArrayList eventTypes=null; 
	ArrayList eventDurs= null; 
	ArrayList displacements= null; 
	ArrayList orgIds= null; 
	ArrayList costs= null; 


	if (calledFrom.equals("P")) { 

		ctrldao= eventdefB.getAllProtAndEvents(Id.intValue()); 
		eventIds=ctrldao.getEvent_ids() ; 
		protocolIds=ctrldao.getChain_ids() ; 
		names= ctrldao.getNames(); 
		descriptions= ctrldao.getDescriptions(); 
		eventTypes= ctrldao.getEvent_types(); 
		eventDurs= ctrldao.getDurations(); 
		displacements= ctrldao.getDisplacements(); 
		orgIds= ctrldao.getOrg_ids(); 
		costs = ctrldao.getCosts();
		
	}
	else if (calledFrom.equals("S")) { //called From Study
		assocdao= eventassocB.getAllProtAndEvents(Id.intValue()); 
		eventIds=assocdao.getEvent_ids() ; 
		protocolIds=assocdao.getChain_ids() ; 
		names= assocdao.getNames(); 
		descriptions= assocdao.getDescriptions(); 
		eventTypes= assocdao.getEvent_types(); 
		eventDurs= assocdao.getDurations(); 
		displacements= assocdao.getDisplacements(); 
		orgIds= assocdao.getOrg_ids(); 
		costs = ctrldao.getCosts();

//out.println(eventIds.size());
	}

	if(request.getParameterValues("eventcheckbox")!= null)
	{
		String[] ev_ids = request.getParameterValues("eventcheckbox");
		for(int y = 0;y<ev_ids.length;y++)
		{
			Integer tmp = new Integer(ev_ids[y]);

			neweventIds.add((Integer)tmp);
			String ev_name = request.getParameter("event_name" + ev_ids[y]);
			newnames.add(ev_name);
		}
	} //end of getParameterValues

	for(int i=0;i<eventIds.size();i++)
	{
	  if(((String)eventTypes.get(i)).equals( "P"))
	  {

		eventDur=(String)eventDurs.get(i);
		dur = new Integer(eventDur);
		float flt=dur.intValue();
		flt = (float)java.lang.Math.ceil(flt/7);		
		weeks=(int)flt;
		break;
	
	//	if (calledFrom.equals("P")) { //called From Protocol
	//		break;
	//	}		
	  }
	} //loop of events

	String table_width = weeks*33+"%";
%>

  <table width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr > 
      <td width = "70%"> 
        <P class = "defComments">The following are the Scheduled Events in your protocol calendar</P>
      </td>
      <td width = "30%" align="right"> 
        <p> <A href="eventbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=M">Add More Events</A> </p>
      </td>
    </tr>
  </table>

<Form name="displayDOW" method="post" action="saveEventToProto.jsp">

	<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>>
	<INPUT NAME="srcmenu" TYPE=hidden value=<%=src%>>
	<INPUT NAME="calledFrom" TYPE=hidden  value=<%=calledFrom%>>
	<INPUT NAME="duration" TYPE=hidden  value=<%=duration%>>
	<INPUT NAME="protocolId" TYPE=hidden  value=<%=protocolId%>>

<P>
<TABLE border=1 cellPadding=1 cellSpacing=1 width=<%=table_width%>>
  
 <TR>
 <TH>Event</TH>

	<%
	for(int j=1;j<=weeks;j++)
	{%>

	 <TH>Week<%=j%></TH>
	<%}%>

  </TR>

<%

for(int n=1;n<eventIds.size();n++)
{
%>
<INPUT NAME="eventIds" TYPE=hidden  value=<%=eventIds.get(n)%>>
 <%

if (n%2 == 0) 
{ 
%>
<tr class="browserEvenRow"> 
<%
}
else {
%>
<tr class="browserOddRow"> 
<%
}
%> 

<TD><%=names.get(n)%></TD>
<TD>

<%for(int m=1 ;m<=(weeks*7); m++)
{%>

    <INPUT id=checkbox1 name=checkbox<%=(Integer)eventIds.get(n)%>  type=checkbox value=<%=m%><%if(m >dur.intValue()){%> disabled = "true"
<%}
for(int p=0;p<disps.size();p++)
{disp = new Integer((String)disps.get(p)); if(m == disp.intValue() ) {%>  checked="true" <%}}%>>
   <%if((m%7 == 0) && (m < (weeks*7))) {%>
    </TD>
    <TD>
<%}%>	 

<%}}%>		

</TR>


<%for(int y = 0;y< neweventIds.size();y++)
{%>
<INPUT NAME="eventIds" TYPE=hidden  value=<%=neweventIds.get(y)%>> 

<%if (y%2 == 0) 
{ 
%>
<tr class="browserEvenRow"> 
<%
}
else {
%>
<tr class="browserOddRow"> 
<%
}
%> 

<td><%=newnames.get(y)%></td>
<td>
<%for(int m=1 ;m<=(weeks*7); m++)
{%>
 <INPUT id=checkbox1 name=checkbox<%=(Integer)neweventIds.get(y)%>  type=checkbox  value=<%=m%><%if(m >dur.intValue()){%> disabled = "true"
<%}%> >
<%if(m%7 == 0){%>
    </TD>
    <TD>
<%}%>	 
<%}%>
</tr>

<%}%>
</TABLE>

<BR>
  <table width="100%" cellspacing="0" cellpadding="0">
      <td align=center> 
        <button onclick="window.history.back();"><%=LC.L_Back%></button>
      </td>
      <td> 
        <input type="Submit" name="next" value="Next">
      </td> 
      </tr>
  </table>
</Form>

<%

 	} //end of if body for page right
else
{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%
 } //end of else body for page right
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
 
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <jsp:include page="menus.htm" flush="true"/>
</div>

</BODY>

</HTML>





