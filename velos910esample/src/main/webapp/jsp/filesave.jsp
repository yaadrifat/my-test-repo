<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdocB" scope="request" class="com.velos.esch.web.eventdoc.EventdocJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.MC"%>

<%



HttpSession tSession = request.getSession(true); 

String docId=request.getParameter("docId");
String fileName="";
if (sessionmaint.isValidSession(tSession)) {	

eventdocB.setDocId(EJBUtil.stringToNum(docId));

eventdocB.getEventdocDetails();
byte docval[]=eventdocB.getDocValue();
ByteArrayInputStream fin1=new ByteArrayInputStream(docval);
BufferedInputStream fbin1=new BufferedInputStream(fin1);
fileName=eventdocB.getDocName();

com.velos.eres.service.util.Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
String path="";
path=EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, fileName);



File fo1=new File(path);
FileOutputStream fout1 = new FileOutputStream(fo1);
int c1 ;
while ((c1 = fbin1.read()) != -1){
fout1.write(c1);
 }
fbin1.close();
fout1.close();

}//end of if body for session


else

{

%>
<br><br><br><br><br>

<p class = "sectionHeadings" align = center>

<%=MC.M_SessTimeOut_LoginAgain%><%-- Session timed out. Please login again.*****--%>

</p>

<%} %>
<%
String urlpath= "URL=/appendix/servlet/Download/"+ fileName; 

 out.println("<META HTTP-EQUIV=Refresh CONTENT=\"0;"+urlpath+"\">");
%>


</BODY>
</HTML>


