<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT  Language="JavaScript1.2">

	function validate(formobj)
	{

		if (!(validate_col('Invoice Number',formobj.invNumber))) return false;
		if (!(validate_col('Payment Due In',formobj.paymentDueIn))) return false;

		if (!(validate_col('Invoice Date',formobj.invDate))) return false;


	 //added for date field validation
	 if (!(validate_date(formobj.invDate))) return false

		if (!(validate_col('e-Signature',formobj.eSign))) return false;
	    if(isNaN(formobj.eSign.value) == true) {
	    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();

			return false;
		   }

		var detailct = formobj.totalDetailCount.value;


		if (detailct > 1)
		{
				 for (c=0;c<detailct;c++)
					 {

					    eval("isDec = isDecimalAndNotBlank(replaceValues(formobj.amountInvoiced[" + c + "].value,',','' ))");
						if (!isDec)
						{
						  eval("isNumber = isIntegerAndNotBlank(replaceValues(formobj.amountInvoiced[" + c + "].value,',','' ))");
						  if (!isNumber)
							{
							  alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
								eval("formobj.amountInvoiced[" + c + "].focus()");
								return false;
							}
						}
					}
		}
		else if (detailct == 1)
		{
					eval("isDec = isDecimalAndNotBlank(replaceValues(formobj.amountInvoiced.value,',','' ))");
						if (!isDec)
						{
						  eval("isNumber = isIntegerAndNotBlank(replaceValues(formobj.amountInvoiced.value,',','' ))");
						  if (!isNumber)
							{
							  alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
								eval("formobj.amountInvoiced.focus()");
								return false;
							}
						}
		}

		return true;

	}
	//Yogendra Pratap : Bug# 8764 :Date 28 Mar 2012
	function replaceValues(value, replace, replaceWithStr) {
		  return value.replace(new RegExp(replace, 'g'),replaceWithStr);
	}	
</SCRIPT>

<script type="text/javascript">
//JM: 04Jun2010: #3904
if (typeof window.event != 'undefined')
	document.onkeydown = function()
	{
		if (event.srcElement.tagName.toLowerCase() != 'input')
			return (event.keyCode != 8);
	}
else
	document.onkeypress = function(e)
	{
		if (e.target.nodeName.toLowerCase() != 'input')
			return (e.keyCode != 8);
	}

</script>


</head>
</HEAD>



<DIV class="popDefault" id="div1">
<jsp:useBean id ="reportIO" scope="session" class="com.velos.eres.service.util.ReportIO"/>
  <jsp:useBean id="InvDetB" scope="request" class="com.velos.eres.web.invoice.InvoiceDetailJB"/>
   <jsp:useBean id="userInv" scope="request" class="com.velos.eres.web.user.UserJB"/>
  <jsp:useBean id="AddInv" scope="request" class="com.velos.eres.web.address.AddressJB"/>
  <jsp:useBean id="SiteInv" scope="request" class="com.velos.eres.web.site.SiteJB"/>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
   <jsp:useBean id="InvStudyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,java.io.*,java.net.URL,com.velos.eres.web.grpRights.*,java.text.DecimalFormat"%>
  <%@page import="com.velos.esch.business.common.SchCodeDao"%>
  <jsp:include page="include.jsp" flush="true"/>
  <%


	String studyId = request.getParameter("studyId");
	String invPkStr = request.getParameter("invPk");
	String mode = request.getParameter("mode");

	if (StringUtil.isEmpty(mode))
	{
		mode= "V";
	}


HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%


int totalDetailCount = 0;

	String usr = (String) tSession.getAttribute("userId");

	int invPk = EJBUtil.stringToNum(invPkStr);
	 DecimalFormat decFormatter = new DecimalFormat("###,##0.00");

	StringBuffer sbMain = new StringBuffer();
			double totalAmountInvoicedForAll = 0.0;

			String invHeader = "";
	 		String invFooter = "";

	 String paymentDueIn = "";
	 String paymentDueInUnit = "";
	 String invNotes = "";

	sbMain.append("<BODY class=\"repBody\"> ");

	if(mode.equals("E"))
	{
		sbMain.append("<FORM name= \"inv\" id=\"viewInvFrm\" method=\"POST\" action=\"updateInvDetails.jsp\" onSubmit=\"if (validate(document.inv)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}\">");

 	}

 if (invPk > 0)
 {
	 	String[] arMilestoneType =  new String[5];
 		arMilestoneType[0] = "PM";
 		arMilestoneType[1] = "VM";
 		arMilestoneType[2] = "EM";
 		arMilestoneType[3] = "SM";
		arMilestoneType[4] = "AM"; //KM

 		Hashtable htParam = new Hashtable();

 		htParam.put("calledFrom","INV");

 		String[] arMilestoneTypeHeader =  new String[5];
 		arMilestoneTypeHeader[0] = LC.L_Pat_StatusMstones;/*arMilestoneTypeHeader[0] = LC.Pat_Patient +" Status Milestones";*****/
 		arMilestoneTypeHeader[1] = LC.L_Visit_Mstones;/*arMilestoneTypeHeader[1] = "Visit Milestones";*****/
 		arMilestoneTypeHeader[2] = LC.L_Evt_Mstones;/*arMilestoneTypeHeader[2] = "Event Milestones";*****/
 		arMilestoneTypeHeader[3] = LC.L_Std_Mstones;/*arMilestoneTypeHeader[3] = LC.Std_Study+" Milestones";*****/
 		arMilestoneTypeHeader[4] = LC.L_Addl_Mstones;/*arMilestoneTypeHeader[4] = "Additional Milestones";*****/

 		int addTo = 0;  int addFrom = 0; String userName = ""; int toAddress = 0; int toOrg = 0;
 		String siteName = "";
 		StringBuffer toAddStr = new StringBuffer();
 		String add1 = ""; String city = ""; String state = ""; String zip = ""; String country="";
 		String studyNumber = "";
 		String studyTitle = "";

 		String dtFrom = "";
 		String dtTo = "";
 		String invDate = "";
 		String paymentDueDate = "";
 		String invNumber = "";
 		String dateRange = "";
 		String invFKSite = "";
 		String invSiteName = "";




 		InvB.setId(invPk);
 		InvB.getInvoiceDetails();

 		invFKSite = InvB.getSite();

 		if (! StringUtil.isEmpty(invFKSite))
 		{
	 		SiteInv.setSiteId(EJBUtil.stringToNum(invFKSite));
	 		SiteInv.getSiteDetails();
	 		invSiteName = SiteInv.getSiteName();
	 	}
	 	else
	 	{
	 		invSiteName = LC.L_All;/*invSiteName = "All";*****/
	 	}

 		if (StringUtil.isEmpty(invSiteName))
 		{
 			invSiteName = LC.L_All;/*invSiteName = "All";*****/
 		}

 		InvStudyB.setId(EJBUtil.stringToNum(InvB.getStudy()));
 		InvStudyB.getStudyDetails();
 		studyNumber = InvStudyB.getStudyNumber();
 		studyTitle = InvStudyB.getStudyTitle();
 		studyTitle = StringUtil.stripScript(StringUtil.trueValue(studyTitle));
 		studyTitle = (studyTitle.length()>50)? studyTitle.substring(0,47)+"...":studyTitle;
 		
 		String CoverType = request.getParameter("CoverType");
 	 		
 		if((mode.equals("E") || mode.equals("V")) && CoverType!=null && !CoverType.trim().equalsIgnoreCase("") && !CoverType.equalsIgnoreCase("null") && !CoverType.equalsIgnoreCase("All")){
 			CoverType = CoverType.replaceAll(",","','");
 		}
 		dtFrom  = InvB.getInvIntervalFrom();
 		dtTo  = InvB.getInvIntervalTo();
 		paymentDueIn = InvB.getInvPayDueBy();
 		paymentDueInUnit = InvB.getInvPayUnit();
 		invNotes= InvB.getInvNotes();

 		if (StringUtil.isEmpty(invNotes))
 		{
 			invNotes = "";
 		}

 		if (StringUtil.isEmpty(dtFrom))
 		{
 			dateRange = LC.L_All;/*dateRange = "All";*****/
 		}
 		else
 		{
 			dateRange = dtFrom + " "+LC.L_To+" " + dtTo;/*dateRange = dtFrom + " To " + dtTo;*****/

 		}

 		//JM: 111306
 		String intAccNum = InvB.getIntnlAccNum();
 		if (StringUtil.isEmpty(intAccNum))
 		{
 			intAccNum = "";
 		}

 		invDate = InvB.getInvDate();
 		paymentDueDate = InvB.getCalculatedInvPaymentDate();
 		invNumber = InvB.getInvNumber();
 		invHeader = InvB.getInvHeader();

 		if (StringUtil.isEmpty(invHeader))
 		{
 			invHeader = "";
 		}

 		invFooter = InvB.getInvFooter();

 		if (StringUtil.isEmpty(invFooter))
 		{
 			invFooter = "";
 		}


 		addTo = EJBUtil.stringToNum(InvB.getInvAddressedto());
 		addFrom = EJBUtil.stringToNum(InvB.getInvSentFrom());

///////////////////////

		if(mode.equals("V"))
		{
			sbMain.append(invHeader);

		}
		else
		{
			sbMain.append("<table width=\"100%\" style=\"border: none;border-collapse: separate ;\"><tr><td style=\"border: solid black 1px;\" class=tdDefault><P class=\"sectionHeadings\">"+LC.L_Header/*Header*****/+"</P></td></tr><tr><td><Textarea name=invHeader rows=4 cols=100>"+invHeader+"</textarea> </td></tr></table>");
		}


		toAddStr.append("<TABLE    width=\"100%\" style=\"border: none;border-collapse: separate ;\">");
	 	toAddStr.append("<TR style=\"border: none;\">");
	 	toAddStr.append("<TD style=\"border: solid black 1px;\" colspan='2'>");
	 	toAddStr.append("<P class=\"sectionHeadings\">"+LC.L_Addressed_To/*Addressed To*****/+":</P>");


	 	if (addTo > 0)
	 	{
		 	userInv.setUserId(addTo);
 			userInv.getUserDetails();

 			userName = userInv.getUserFirstName() + " "  + userInv.getUserLastName();

 			toOrg = EJBUtil.stringToNum(userInv.getUserSiteId());

 			SiteInv.setSiteId(toOrg);
 			SiteInv.getSiteDetails();

 			siteName = SiteInv.getSiteName();

 			toAddress = EJBUtil.stringToNum( SiteInv.getSitePerAdd() );
 			// get address details

 			AddInv.setAddId(toAddress);
 			AddInv.getAddressDetails();

	 		add1 = AddInv.getAddPri();
			if (StringUtil.isEmpty(add1))
		 		add1 = "-";


	 		city = 	AddInv.getAddCity();
	 		if (StringUtil.isEmpty(city))
		 		city = "-";

	 		state = AddInv.getAddState();
	 		if (StringUtil.isEmpty(state))
		 		state = "-";

	 		zip = AddInv.getAddZip();
	 		if (StringUtil.isEmpty(zip))
		 		zip = "-";

	 		country = AddInv.getAddCountry();
	 		if (StringUtil.isEmpty(country))
		 		country = "";


	 		if (mode.equals("V"))
			{
				toAddStr.append(userName + "<BR>" + siteName + " <BR> " + add1 + "<BR> " + city + " " + state + " " + zip + " <BR> " + country );
			}


 		}

 		if (! mode.equals("V"))
			{
				toAddStr.append("<input type=hidden name=addressedTo value="+addTo+">");
          		toAddStr.append("<input type=text name=addressedToName size=30 value='"+userName+"' readonly>");
          		toAddStr.append("<A HREF=# onClick=openCommonUserSearchWindow(\"inv\",\"addressedTo\",\"addressedToName\") >"+LC.L_Select_User/*Select User*****/+"</A>");

			}


	 	toAddStr.append("</TD>");
	 	//toAddStr.append("<TD style=\"border: none;\" width=\"20%\">&nbsp;</TD>");
	 	toAddStr.append("<TD style=\"border: solid black 1px;\" colspan='2'>");
	 	toAddStr.append("<P class=\"sectionHeadings\">"+LC.L_Sent_From/*Sent From*****/+":</P>");

 		userName = "";
	 	if (addFrom  > 0)
	 	{
		 	userInv.setUserId(addFrom);
 			userInv.getUserDetails();

 			userName  = userInv.getUserFirstName() + " "  + userInv.getUserLastName();

 			toOrg = EJBUtil.stringToNum(userInv.getUserSiteId());

 			SiteInv.setSiteId(toOrg);
 			SiteInv.getSiteDetails();

 			siteName = SiteInv.getSiteName();

 			toAddress = EJBUtil.stringToNum( SiteInv.getSitePerAdd() );
 			// get address details

 			AddInv.setAddId(toAddress);
 			AddInv.getAddressDetails();

	 		add1 = AddInv.getAddPri();
			if (StringUtil.isEmpty(add1))
		 		add1 = "-";


	 		city = 	AddInv.getAddCity();
	 		if (StringUtil.isEmpty(city))
		 		city = "-";

	 		state = AddInv.getAddState();
	 		if (StringUtil.isEmpty(state))
		 		state = "-";

	 		zip = AddInv.getAddZip();
	 		if (StringUtil.isEmpty(zip))
		 		zip = "-";

	 		country = AddInv.getAddCountry();
	 		if (StringUtil.isEmpty(country))
		 		country = "";


			if (mode.equals("V"))
			{
				toAddStr.append(userName + "<BR>" + siteName + " <BR> " + add1 + "<BR> " + city + " " + state + " " + zip + " <BR> " + country );
			}

 		}


 		if (!mode.equals("V"))
			{
				toAddStr.append("<input type=hidden name=sentFrom value="+addFrom+">");
          		toAddStr.append("<input type=text name=sentFromName size=30 value='"+userName+"' readonly>");
          		toAddStr.append("<A HREF=# onClick=openCommonUserSearchWindow(\"inv\",\"sentFrom\",\"sentFromName\") >"+LC.L_Select_User/*Select User*****/+"</A>");

			}

	 	toAddStr.append("</TD></TR>");

	sbMain.append(toAddStr);
	sbMain.append("<tr><td width=\"20%\">"+ LC.L_Study_Number +":</td><td width=\"30%\">" + studyNumber + "</td>");/*sbMain.append("<tr><td width=\"20%\">"+ LC.Std_Study +" Number:</td><td width=\"30%\">" + studyNumber + "</td>");*****/
	
	sbMain.append("<td  width=\"20%\">"+LC.L_Inv_Number/*Invoice Number*****/+":");

	if (! mode.equals("V"))
	{
		sbMain.append("<FONT class=\"Mandatory\">* </FONT>");
	}

	sbMain.append("</td><td width=\"30%\">");

	if (mode.equals("V"))
	{
		sbMain.append(invNumber );
	}
	else
	{

		sbMain.append("<input type=text name=\"invNumber\" value='"+invNumber+"' size=10 maxlength=50  > ");
	}

	sbMain.append("</td></tr>");
	//FIX #6145
	sbMain.append("<tr><td width=\"20%\">"+ LC.L_Study_Title +":</td><td width=\"30%\">" + studyTitle + "</td>");/*sbMain.append("<tr><td width=\"20%\">"+ LC.Std_Study +" Title:</td><td width=\"30%\">" + studyTitle + "</td>");*****/
	
	sbMain.append("<td  width=\"20%\">"+LC.L_Invoice_Date/*Invoice Date*****/+": ");

	if (! mode.equals("V"))
	{
		sbMain.append("<FONT class=\"Mandatory\">* </FONT>");
	}

	sbMain.append("</td><td>");

	if (mode.equals("V"))
	{
		sbMain.append(invDate );
	}
	else
	{
		//INF-20084 Datepicker-- AGodara
		sbMain.append("<input type=\"text\" name=\"invDate\" class=\"datefield\" size = 15 MAXLENGTH = 50 value="+invDate+">");
	}



	sbMain.append("</td></tr>");

	sbMain.append("<tr><td width=\"22%\">"+MC.M_Selected_DtRangeFilter/*Selected Date Range Filter*****/+":</td><td width=\"30%\">" + dateRange + "</td><td width=\"20%\">"+LC.L_Payment_DueDt/*Payment Due Date*****/+": &nbsp;</td><td>"+paymentDueDate +"</td>");



	//JM: 111306
	sbMain.append("</tr><tr><td width=\"20%\">"+LC.L_InetAcc_Num/*Internal Account Number*****/+":" );

	if (mode.equals("V"))
	{
		sbMain.append(intAccNum  );
	}
	else
	{
		sbMain.append("<input type=text name=\"intAccNum\" value='"+intAccNum+"' size=15 maxlength=60  > ");
	}

	sbMain.append("</td>");

	if (!mode.equals("V"))
	{

		sbMain.append("<td  width=\"20%\">"+LC.L_Pyment_DueIn/*Payment Due In*****/+":<FONT class=\"Mandatory\">* </FONT>");


		sbMain.append("</td><td><input type=text name=\"paymentDueIn\" size=5 maxlength=10 class=\"leftAlign\" value="+paymentDueIn +">");

		sbMain.append("<Select name=\"paymentDueUnit\">");

		String DSelected = "";
		String WSelected = "";
		String MSelected = "";
		String YSelected = "";

		paymentDueInUnit= paymentDueInUnit.trim();

		if (paymentDueInUnit.equals("D"))
		{
			DSelected = " SELECTED ";
		}
		else if (paymentDueInUnit.equals("W"))
		{
			WSelected = " SELECTED ";
		}else if (paymentDueInUnit.equals("M"))
		{
			MSelected = " SELECTED ";
		} else if (paymentDueInUnit.equals("Y"))
		{
			YSelected = " SELECTED ";
		}


		sbMain.append("	<option value=\"D\""+DSelected+">"+LC.L_Day_S/*Day(s)*****/+"</option>");
		sbMain.append("	<option value=\"W\""+WSelected+">"+LC.L_Week_S/*Week(s)*****/+"</option>");
		sbMain.append("<option value=\"M\""+MSelected+">"+LC.L_Month_S/*Month(s)*****/+"</option>");
		sbMain.append("<option value=\"Y\""+YSelected+">"+LC.L_Year_S/*Year(s)*****/+"</option>");
		sbMain.append("</Select></td>");
	}

	sbMain.append("</tr>");

	//JM:mR1
	sbMain.append("</table>");
	sbMain.append("<hr class= \"thinLine\" />");

	sbMain.append("<TABLE cellspacing = 5  width=\"100%\" style=\"border: solid black 1px;border-collapse: collapse;\">");

	sbMain.append("<tr><TH class=\"reportHeading\" WIDTH=\"40%\" ALIGN=\"CENTER\">"+LC.L_Milestone_Description/*Milestone Description*****/+"</TH>");
	sbMain.append("<TH class=\"reportHeading\" WIDTH=\"10%\" ALIGN=\"CENTER\">"+LC.L_Achieved/*Achieved*****/+"</TH>");
	sbMain.append("<TH class=\"reportHeading\" WIDTH=\"10%\" ALIGN=\"CENTER\">"+LC.L_Amount_Invoiced/*Amount Invoiced*****/+"</TH></tr>");

	sbMain.append("<TR ><td colspan=3> <p class=\"sectionHeadings\"> "+LC.L_Organization/*Organization*****/+": " + invSiteName + " </p></td></TR>");



	for (int t = 0; t <arMilestoneType.length;t++)
 		{

 		InvoiceDetailDao inv = new InvoiceDetailDao ();
 		StringBuffer sbPM = new StringBuffer();
 		//get patient milestones invoice details
 	    
 		// Added for FIN-20599 ::: Raviesh
 		if(arMilestoneType[t].equalsIgnoreCase("EM") || arMilestoneType[t].equalsIgnoreCase("AM")){
 			inv = InvDetB.getInvoiceDetailsForMilestoneTypeEMAM(invPk,arMilestoneType[t],htParam);
 		}else{	
 		    inv = InvDetB.getInvoiceDetailsForMilestoneType(invPk,arMilestoneType[t],htParam);
 		}

 		ArrayList arMilestoneIds = new ArrayList();
 		ArrayList arMilestoneRuleDescs = new ArrayList();

 		ArrayList arMilestoneAchievedCounts = new ArrayList();
 	//	ArrayList arMileAmounts = new ArrayList();
 		ArrayList arMileAmountsInvoiced = new ArrayList();

 		ArrayList arMilePatientCodes = new ArrayList();
 		ArrayList arMileAchievedDate = new ArrayList();
 		ArrayList arShowDetail = new ArrayList();
		ArrayList arDetailType = new ArrayList();
		ArrayList arInvDetailPKs = new ArrayList();

 		String mileStoneId = "";
 		arMilestoneIds = inv.getMilestone();
		arMilestoneRuleDescs = inv.getMilestonesDesc();
		arMilestoneAchievedCounts = inv.getMilestonesAchievedCount();
 		//arMileAmounts = inv.getAmountDue();
 		arMileAmountsInvoiced =  inv.getAmountInvoiced();

 		arMilePatientCodes = inv.getPatCode();
 		arMileAchievedDate = inv.getAchDate();
 		arShowDetail = inv.getDisplayDetail();
 		arDetailType = inv.getDetailType();
 		arInvDetailPKs = inv.getId();

 		String mileDesc = "";
 		int milecount = 0;
 		double amountInvoiced = 0.0;
 		double totalAmountInvoiced = 0.0;
 		String detailType = "";
 		String invDetailPk = "";
	 	String showDetail = "";




		if (arMilestoneIds != null)
 		{
 			milecount = arMilestoneIds.size();
 		}
 		else
 		{
	 		milecount  = 0;
 		}

 		// for each milestone, get the milestone achieved details

 		sbPM.append("<tr style=\"border: none;\" colspan=4><td><b>"+arMilestoneTypeHeader[t]+"</b></td></tr>");

 		for (int i =0; i < milecount ; i++)
 		{

 			mileStoneId = (String )arMilestoneIds.get(i);
 			System.out.println("(String )arMileAmountsInvoiced.get(i)" + (String )arMileAmountsInvoiced.get(i));
			amountInvoiced = Double.parseDouble((String )arMileAmountsInvoiced.get(i));

			detailType = (String) arDetailType.get(i);

			invDetailPk = (String) arInvDetailPKs.get(i);

			totalDetailCount = totalDetailCount + 1;

			showDetail = (String) arShowDetail.get(i);

			if (detailType.equals("H") )
			{
	 		   mileDesc = (String ) arMilestoneRuleDescs.get(i);
	 		}
	 		else
	 		{
	 			mileDesc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (String)(arMilePatientCodes.get(i)==null? "" : arMilePatientCodes.get(i));
	 		}

			if(mileDesc.length() > 500) { //KM
				mileDesc = mileDesc.substring(0,500)+"...";
			}

	 		if (( showDetail.equals("1") && detailType.equals("D") ) || ( showDetail.equals("0") && detailType.equals("H") ))
	 		{
	 			   totalAmountInvoiced = totalAmountInvoiced + amountInvoiced;
	 		}


			sbPM.append("<tr ");
				if(i%2==0){
				 sbPM.append(" class=\"browserEvenRow\"");
			}else{

				 sbPM.append(" class=\"browserOddRow\"");
			}

			sbPM.append(" style=\"border: none;\"><td>"+ mileDesc+"</td>");
			sbPM.append("<td>");

			if (detailType.equals("H"))
			{
				 if(!arMilestoneType[t].equals("AM")) {
					 Object[] arguments = {(String) arMilestoneAchievedCounts.get(i)};
					 	sbPM.append(VelosResourceBundle.getLabelString("L_MstoneAchv",arguments));/*sbPM.append((String) arMilestoneAchievedCounts.get(i) + "&nbsp; milestones achieved");*****/	 					
	 				}
	 				else
	 				{
	 					sbPM.append("N/A");
	 				}

	 		}
			else
			{
				sbPM.append((String) arMileAchievedDate.get(i));
	 		}
 			sbPM.append("</td>");

 			if(mode.equals("V"))
			{
				if ( ! (showDetail.equals("1") && detailType.equals("H") ) )
				{
 					sbPM.append("<td>" + decFormatter.format(amountInvoiced)  + "</td>");
 				}
 				else
 				{
 					sbPM.append("<td>&nbsp;</td>");
 				}
 			}
 			else
 			{
 				if ( ! (showDetail.equals("1") && detailType.equals("H") ) )
				{
 					sbPM.append("<td><input type='text' name='amountInvoiced' value="+decFormatter.format(amountInvoiced) +"><input type='hidden' name='invDetailPk' value="+invDetailPk +"></td>");
 				}
 				else
 				{
 					sbPM.append("<td>&nbsp;<input type='hidden' name='amountInvoiced' value="+amountInvoiced +"><input type='hidden' name='invDetailPk' value="+invDetailPk +"></td>");
 				}
 			}

 			sbPM.append("</tr>");

		} // for patient milestones
			sbPM.append("<tr><td colspan=4 align=\"right\">");
			Object[] arguments1 = {arMilestoneTypeHeader[t],decFormatter.format(totalAmountInvoiced)};
		 	sbPM.append(VelosResourceBundle.getMessageString("M_TotAmt_All",arguments1));/*sbPM.append("Total Amount for All "+arMilestoneTypeHeader[t]+": " + decFormatter.format(totalAmountInvoiced));*****/
			sbPM.append("</td></tr>");

			totalAmountInvoicedForAll =  totalAmountInvoicedForAll + totalAmountInvoiced;
 			totalAmountInvoiced = 0.0;

		 sbMain.append(sbPM);

		} // for milestone type loop
 	} // if pkInv > 0

	sbMain.append("</table><hr class= \"thinLine\" />");
	Object[] arguments1 = {decFormatter.format(totalAmountInvoicedForAll)};
	sbMain.append("<p class=\"sectionHeadings\" align=\"right\"> "+VelosResourceBundle.getMessageString("M_TotAmt_MstoneAchv",arguments1)/*Total Amount for All Milestones Achieved ": " + decFormatter.format(totalAmountInvoicedForAll)*****/ + "</p>");


	if(mode.equals("V"))
		{
			sbMain.append(invFooter);

		}
		else
		{
			sbMain.append("<table width=\"100%\" style=\"border: none;border-collapse: separate ;\"><tr><td style=\"border: solid black 1px;\" class=tdDefault><P class=\"sectionHeadings\">"+LC.L_Footer/*Footer*****/+"</P></td></tr><tr><td><Textarea name=invFooter rows=4 cols=100>"+invFooter+"</textarea> </td></tr></table>");
		}

	if(!mode.equals("V"))
		{

		sbMain.append("<table><tr>	<td  class=tdDefault>"+LC.L_Notes/*Notes*****/+" : <BR><Textarea name=invNotes rows=4 cols=100 >"+invNotes+"</textarea> </td></tr></table>");

	 }



	if(mode.equals("E"))
	{

		sbMain.append("<input type='hidden' name='invPk' value="+invPk+"> <input type='hidden' name='totalDetailCount' value="+totalDetailCount+">");
 	}

   	sbMain.append("</BODY>");

	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));


	StringBuffer sbMain1 = new StringBuffer();
	sbMain1.append("<BODY class=\"repBody\">");

	if(mode.equals("V"))
	{

			//Added by Manimaran to fix the Bug2796.
		String str =sbMain.toString();
		String printLink=reportIO.saveReportToDoc(str,"htm","reporthtml");
		String excelLink=reportIO.saveReportToDoc(str,"xls","reporthtml");
		String wordLink =reportIO.saveReportToDoc(str,"doc","reporthtml");

		if (pageRight < 6)
		{
			String strRightMsg = "javascript:alert(&quot;"+MC.M_NoRgtTo_DldRpt/*You do not have access rights to download the report*****/+"&quot;);";
			wordLink = strRightMsg;
			excelLink = strRightMsg;
			printLink = strRightMsg;
		}

		sbMain1.append("<table bgcolor=\"lightgrey\" width=\"100%\">");
		sbMain1.append("<tr><td class=\"reportPanel\">"+MC.M_Download_ReportIn/*Download the report in*****/+":");
		sbMain1.append("&nbsp;<A href=\" " + wordLink + "\"><img border=\"0\" title='"+LC.L_Word_Format+"' alt='"+LC.L_Word_Format+"' src=\"./images/word.GIF\" >"/*Word Format*****/+"</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		sbMain1.append("<A href=\" " + excelLink + "\"><img border=\"0\" title=\'"+LC.L_Excel_Format+"' alt='"+LC.L_Excel_Format+"' src=\"./images/excel.GIF\" >"/*Excel Format*****/+"</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		sbMain1.append("<A href=\" " + printLink + "\"><img border=\"0\" title=\'"+LC.L_Printer_FriendlyFormat+"' alt='"+LC.L_Excel_Format+"' src=\"./images/printer.gif\" >"/*Printer Friendly Format*****/+"</A><br><br>");
		sbMain1.append("</td></tr></table>");
	}

	sbMain1.append(sbMain);
	out.println(sbMain1.toString());


	if(mode.equals("E"))
	{ %>
	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="viewInvFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	</Form>
	<% }
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</DIV>

<script>

</script>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</HTML>





