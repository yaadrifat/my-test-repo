<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title>Bulk Upload Preview</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
<script type="text/javascript">
var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 650 : 500;
jQuery.noConflict();
function openSequenceDialog() //YK: Added for PCAL-20461 - Opens the Dialog Window
{
		jQuery("#embedDataHere").dialog({ 
		height: defaultHeight,width: 700,position: 'right' ,modal: true,
		closeText:'',title:'Uploading Data',
		buttons: [
		          {
		              text: L_Close,
		              click: function() { 
		        	  jQuery(this).dialog("close");
		              }
		          }
		      ],
		close: function() { $j("#embedDataHere").dialog("destroy"); },
		beforeClose: function(event, ui) { if(!checkBeforeClose()){ return false;} }
        });
  
}
var loadModifySequenceDialog = function(mapName, autospecid,savemapfld) {
			openSequenceDialog(); 
		    jQuery('#embedDataHere').load("insertBulkUpload.jsp?mapName="+mapName+"&autospecid="+autospecid+"&savemapfld="+savemapfld);	  
	}
function checkBeforeClose()
{
//alert("");
//var i=0;
//for(i=0;i<10;i++)
//document.getElementById('embedDataHere').innerHTML = i ;
	return true;
}
</script>
</head>

<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page  language = "java" import="com.velos.esch.business.common.*,com.velos.eres.bulkupload.web.FileUploadAction,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<DIV class="tabDefTopN" id="div1">
		<jsp:include page="inventorytabs.jsp" flush="true">
		<jsp:param name="selectedTab" value="1"/>
		</jsp:include>
</DIV>
  <%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
	TreeMap<String,String> saveMapping=null;
	String autospecid="";
	String savemapfld="";
	savemapfld=request.getParameter("savemapfld");
	savemapfld=savemapfld.replace(" ","*****").trim();
	String mapName="";
	ArrayList<Integer> insertFlag=new ArrayList();
	ArrayList<Integer> rowBulk=(ArrayList)tSession.getAttribute("holefilerows");
	String accId = (String) tSession.getValue("accountId");
	HashMap<Integer,String> bulkPkName=null;
	ArrayList <FileUploadAction> holeFileData=new ArrayList(); 
	holeFileData=(ArrayList<FileUploadAction>)tSession.getAttribute("holefiledata");
	saveMapping=(TreeMap)request.getAttribute("saveMapping");
	bulkPkName=(HashMap)request.getAttribute("pkbulk");
	autospecid=request.getParameter("autospecid");
	if (StringUtil.isEmpty(autospecid))
		{
			autospecid = "";
		}
	mapName=request.getParameter("savemapchk");
		if (StringUtil.isEmpty(mapName))
		{
			mapName = "";
		}
		FileUploadAction istData=new FileUploadAction();
   	%> 
	<DIV class="BrowserBotN BrowserBotN_MI_4" id="div3">
	  <table class="basetbl outline" width="100%" cellspacing="4" cellpadding="4" border="0">
	  	<tr>
	 <td>
	<button id="openNewWindow" name="openNewWindow" onclick="loadModifySequenceDialog('<%=mapName%>','<%=autospecid%>','<%=savemapfld%>')">Upload</button>	
    </td>
        </tr>
	 <%for (Map.Entry<String, String> entry : saveMapping.entrySet()) {%>
	  <tr class="browserOddRow">
	  <th width="14%" align ="center"><%=bulkPkName.get(Integer.parseInt(entry.getKey()))%></th>
	<%for(FileUploadAction t1:holeFileData) {%>
	 <%if(t1.colnum==Integer.parseInt(entry.getValue())){%>
	  <td>
	 <%=t1.fileData%>
	  </td>	
	 <%}%>
	  <%}%>
     </tr>
	 <%}%>
	<tr>
	 <td><button id="openNewWindow" name="openNewWindow" onclick="loadModifySequenceDialog('<%=mapName%>','<%=autospecid%>','<%=savemapfld%>')">Upload</button>		 
	 <br/>
<br/></td>
        </tr>

    </table>	
	</div>
	<div id="editVisitDiv" title="Upload Data" style="display:none">
<div id='embedDataHere'>

</div>
</div>


<%
tSession.setAttribute("insertFlag",insertFlag);
%> 

  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <% 
}
%>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>
</html>