<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@page import="com.velos.esch.web.eventdef.EventdefJB,com.velos.esch.web.eventassoc.EventAssocJB"%>
<%@page import="com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil"%>
<%@page import="java.util.ArrayList,java.util.Enumeration"%>
<%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("resultMsg", "User is not logged in.");*****/
		out.println(jsObj.toString());
		return;
	}
	
	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -4);
		jsObj.put("resultMsg", MC.M_EtrWrongEsign_PlsTryAgain);/*jsObj.put("resultMsg", "You entered a wrong e-signature. Please correct and try again.");*****/
		out.println(jsObj.toString());
		return;
	}
	
	Enumeration params = request.getParameterNames();
	ArrayList visitList = new ArrayList();
	while (params.hasMoreElements()) {
	    String param = (String)params.nextElement();
	    if (param.startsWith("ctype_")) {
	        visitList.add(param.substring(param.indexOf("v")+1));
	    }
	}
	
	String ipAdd = (String) tSession.getAttribute("ipAdd");
    String userId = (String) tSession.getAttribute("userId");
	String accountId = (String) tSession.getAttribute("accountId");

	String mode=request.getParameter("mode");
	String calassoc=request.getParameter("calassoc");
 	String calledFrom = request.getParameter("calledfrom");
	String protId = request.getParameter("calProtocolId");
	String protocolId = request.getParameter("protocolId");
	String tableName = request.getParameter("tableName");
	String eventId= StringUtil.trueValue(request.getParameter("eventId"));

	if (visitList.size() == 0) {
		jsObj.put("result", 0);
		jsObj.put("resultMsg", MC.M_No_OperPerformed);/*jsObj.put("resultMsg", "No operations were performed");*****/
		out.println(jsObj.toString());
		return;
	}
	
    try {
        int maxLen = 4000;
		for (int iX=0; iX<visitList.size(); iX++) {
			if (visitList.get(iX) == null) { continue; }
			String vId = (String)visitList.get(iX);
			if ("S".equals(calledFrom)) {
				int realEventId;
				if (!"0".equals(vId)){
			    	realEventId = eventdefB.fetchEventIdByVisit(eventId, "event_assoc", vId);
				}else{
					realEventId = StringUtil.stringToNum(eventId);
				}
				if (realEventId > 0){
					EventAssocJB evtassB = new EventAssocJB();
					evtassB.setEvent_id(realEventId);
					evtassB.getEventAssocDetails();
					if (!"0".equals(vId)){
						evtassB.setEventVisit(vId);
					}
					evtassB.setCreator(userId);
					evtassB.setIpAdd(ipAdd);
					int cType = EJBUtil.stringToNum(StringUtil.trueValue(request.getParameter("ctype_e"+eventId+"v"+vId)).trim());
					evtassB.setEventCoverageType(String.valueOf(cType));
					String newNotes = StringUtil.trueValue(request.getParameter("notes_e"+eventId+"v"+vId));
					if (newNotes.length() > maxLen) { newNotes = newNotes.substring(0, maxLen); }
					evtassB.setCoverageNotes(StringUtil.stripScript(newNotes));
					int setId = evtassB.updateEventAssoc();
					if (setId < 0) { throw new Exception(MC.M_CntSave_EvtDets/*"Could not save event details"*****/); }
				}
			} else if ("P".equals(calledFrom) || "L".equals(calledFrom)) {
				int realEventId;
				if (!"0".equals(vId)){
			    	realEventId = eventdefB.fetchEventIdByVisit(eventId, "event_def", vId);
				}else{
					realEventId = StringUtil.stringToNum(eventId);
				}
				if (realEventId > 0){
			    	EventdefJB evtdefB = new EventdefJB();
				    evtdefB.setEvent_id(realEventId);
					evtdefB.getEventdefDetails();
					if (!"0".equals(vId)){
						evtdefB.setEventVisit(vId);
					}
					evtdefB.setCreator(userId);
					evtdefB.setIpAdd(ipAdd);
					int cType = EJBUtil.stringToNum(StringUtil.trueValue(request.getParameter("ctype_e"+eventId+"v"+vId)).trim());
					evtdefB.setEventCoverageType(String.valueOf(cType));
					String newNotes = StringUtil.trueValue(request.getParameter("notes_e"+eventId+"v"+vId));
					if (newNotes.length() > maxLen) { newNotes = newNotes.substring(0, maxLen); }
					evtdefB.setCoverageNotes(StringUtil.stripScript(newNotes));
					int setId = evtdefB.updateEventdef();
					if (setId < 0) { throw new Exception(MC.M_CntSave_EvtDets/*"Could not save event details"*****/); }
				}
			}
		}
	} catch(Exception e) {
		jsObj.put("result", -3);
		jsObj.put("resultMsg", e);
		out.println(jsObj.toString());
		return;
	}
	
    jsObj.put("result", 0);
    jsObj.put("resultMsg", MC.M_Changes_SavedSucc);/*jsObj.put("resultMsg", "Changes saved successfully");*****/
	out.println(jsObj.toString());
%>
