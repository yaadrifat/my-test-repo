<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.*,java.io.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
	MultiSearchResult src=null;
	String sUri = request.getRequestURI();
	if(sUri== null) sUri = "getresults.jsp";	

	//which browser has been clicked
	
	int resultSet= EJBUtil.stringToNum(request.getParameter("resultSet"));
	int xmlid=1;
	if(resultSet==2) xmlid=resultSet+2;
	//the names of xmls and xsls of both the browsers should be different so start the 
	//names of second browser from 4(2+2).
%>
<Body>
	<xml id="W<%=xmlid%>">
<%

	try
	{
		HttpSession tSession = request.getSession(true);
		Object Obj = null;
		Obj = tSession.getAttribute("src"+resultSet);
	
			src = (Obj==null? null: (MultiSearchResult)Obj);
			String sScroll = null;
			sScroll = (String)request.getAttribute("scroll"+resultSet);

			if(sScroll == null)
			{
			System.out.println("in scroll" +sScroll);
			String sType = request.getParameter("sortType");
			if(sType==null||sType.equals(""))
			{
				String sFilter = request.getParameter("filter");
				System.out.println("filter "+sFilter);
				
				if(sFilter==null||sFilter.equals(""))
				{
					Object obj = request.getAttribute("vec");
					if(obj != null)
					{
						System.out.println("in new");
						Vector vColumn = (Vector)obj;
						String srchType=null;
						String sSQL = (String)request.getAttribute("sSQL");
						String sTtl = (String)request.getAttribute("sTitle");
						System.out.println(sSQL);
						String srcType[][]=null;
						if(srchType == null)
						{
							src = new MultiSearchResult(sSQL, vColumn, sTtl );
							}
						else
						{
						System.out.println("hi 12323..... "+srchType);
						src = new MultiSearchResult(sSQL, vColumn, sTtl, srcType);
						System.out.println("hi 12323==== "+srchType);							
							}
							
				//store the MultiSearchResult object in separate variables depending on the result set							
						tSession.setAttribute("src"+resultSet, src);

					}
				}
				else
				{
					src.filter(sFilter);
				}
			}
			else
			{
				System.out.println("in sort");
				String sOrder = request.getParameter("sortOrder");
				int iCol = Integer.parseInt(request.getParameter("sortCol"));
				src.sort(sType.trim().charAt(0),sOrder.equals("1"), iCol);
			}
%>
				<%= src.showXML(1)%>
				
<%		
		}
		else
		{ //called when Next or back is pressed
%>		
			<%= src.showXML(Integer.parseInt(sScroll))%>
<%				
		}
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
	}
%>
	</xml>

<% xmlid++;%> 

<!--src="../xsl/studyResultSet.xsl">-->
	<xml id="W<%=xmlid%>"> 
	<%
	if(src!=null)
	{
	%>
	<%=src.throwXSL("xsl",null)%>
	<%
	}
	else
	{
	%>

	<%
	}
	%>
	</xml>

	
	
<% xmlid++; %>
<!--src="../xml/searchconfig.xml">-->	
	<xml id="W<%=xmlid%>">
	<%
	if(src!=null)
	{
	%>
	<%=src.throwXSL("xml",null)%>
	<%
	}
	else
	{
	%>

	<%
	}
	%>
	</xml>

	<DIV id ="SResult<%=resultSet%>">
	</DIV>
	

<script>
var opnrhref = "<%=sUri%>";
</script>
</Body>
