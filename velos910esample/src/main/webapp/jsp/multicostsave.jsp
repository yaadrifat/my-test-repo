<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventcostB" scope="request" class="com.velos.esch.web.eventcost.EventcostJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.MC"%>

<%

String src = request.getParameter("srcmenu");
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String costmode = request.getParameter("costmode");
String mode = request.getParameter("mode");
String fromPage = request.getParameter("fromPage");
String calStatus = request.getParameter("calStatus");
String eventmode = request.getParameter("eventmode");
String displayDur=request.getParameter("displayDur");
String displayType=request.getParameter("displayType");
String eSign = request.getParameter("eSign");	
String costId = "";
String totRows = request.getParameter("totrows");

String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); //SV, 10/19/04, cal-enh-05
String propagateInEventFlag = request.getParameter("propagateInEventFlag");

String eventName = request.getParameter("eventName");

 String calAssoc = request.getParameter("calassoc");
    calAssoc = (calAssoc == null) ? "" : calAssoc;
   

String costs[] = null;
String descs[] = null;
String curs[] = null;
int cnt=0;

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
String ipAdd = (String) tSession.getValue("ipAdd");
String usr = (String) tSession.getValue("userId");

String cost="";
String costDesc ="";
String cur = "";

if (totRows.equals("1")){
   cost = request.getParameter("cost");
   costDesc = request.getParameter("costDesc");
   cur = request.getParameter("cur");
   
   if ((cost != null) && (cost.trim()!= ""))
		{
	     eventcostB.setEventcostDescId(costDesc); 
         eventcostB.setEventcostValue(cost);
	     eventcostB.setEventId(eventId);
	     eventcostB.setCurrencyId(cur);
	     eventcostB.setCreator(usr); 
	     eventcostB.setIpAdd(ipAdd);
	     eventcostB.setEventcostDetails();
	     
	// Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";
		
		if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
			//"N" - New/Add, "P" - protocol/library calendar.
	   		eventcostB.propagateEventcost(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "N", calledFrom);  
		}
	     
	   }
}else {
	costs = request.getParameterValues("cost");
	descs = request.getParameterValues("costDesc");
	curs = 	request.getParameterValues("cur");
	
	for (cnt =0;cnt< EJBUtil.stringToNum(totRows); cnt++ ){
		cost = null;
		cost = costs[cnt];
		costDesc = descs[cnt];
		cur = curs[cnt];
		
		if (".".equals(cost)) { 
		    continue;
		}
		System.out.println("**" + cost + "**");
		if ((cost != null) && (cost.trim()!= ""))
		{
			eventcostB.setEventcostValue(cost);
			eventcostB.setEventcostDescId(costDesc); 
    			eventcostB.setEventId(eventId);
			eventcostB.setCurrencyId(cur);
	     		eventcostB.setCreator(usr); 
		     	eventcostB.setIpAdd(ipAdd);		
  		    	eventcostB.setEventcostDetails();
		}
		

	// Cal-enh - 05, now check if the user wishes to propagate these changes to other events.
		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";
		
		if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
			//"N" - New, "P" - protocol/library calendar.			
	   		eventcostB.propagateEventcost(EJBUtil.stringToNum(protocolId), propagateInVisitFlag, propagateInEventFlag, "N", "P");  
		}


	}
} 
%>
<br><br><br><br><br>

<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%-- Data saved successfully.*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventcost.jsp?selectedTab=3&srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&eventName=<%=eventName%>&calassoc=<%=calAssoc%>">
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


