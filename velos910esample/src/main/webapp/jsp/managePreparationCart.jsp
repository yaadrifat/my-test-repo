<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="localization.jsp" flush="true"/><%@page import="com.velos.eres.service.util.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Manage_PreparationCart%><%--Manage Preparation Cart*****--%></title>
<script type="text/javascript">
function setVals(formobj)
{		
	var deleteindexes = "[INDEXSEP]";
	var totalchecked = 0;	
	if(formobj.checkRow != undefined){
		var checks= formobj.checkRow;
	if(formobj.checkRow.length == undefined && formobj.checkRow.value=="0" && formobj.checkRow.checked){
		//alert("hi");
	deleteindexes = "[INDEXSEP]0[INDEXSEP]";
	totalchecked++;
 	}
	else{
	for(i=0;i<checks.length;i++){	
		if(checks[i].checked){	
		deleteindexes = deleteindexes + i + "[INDEXSEP]";
		totalchecked++;
		}
  	}
	}
	if(totalchecked > 0){
	formobj.action = formobj.action +"?rem=" + deleteindexes;	
	}	
	else{		
		alert("<%=MC.M_SelItemsToRem_FromCart%>");/*alert("Please select item(s) to remove from the cart.");*****/
		return false;
	 }
	}
	else{
		alert("<%=MC.M_No_DataInCart%>");/*alert("No data in cart!");*****/
		return false;
	}	
   //Ashu: Added for BUG#5826(10thMar11)
	if (!(validate_col('e-Signature',formobj.eSign))) return false
	
    if(isNaN(formobj.eSign.value) == true) {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   }
	return true;
}

//function to select all rows
function checkAll()
{
var formobj=document.manageCartFrm;
var checks= formobj.checkRow;
if(formobj.checkallrow.checked){
	if(formobj.checkRow.length == undefined && formobj.checkRow.value=="0")
		{
			formobj.checkRow.checked = true;
		}
	else{
		for(i=0;i<formobj.checkRow.length;i++){			
			formobj.checkRow[i].checked=true;
			}			
	  	}
	}
else{
	if(formobj.checkRow.length == undefined && formobj.checkRow.value=="0")
	{
		formobj.checkRow.checked = false;
	}
else{
	for(i=0;i<formobj.checkRow.length;i++){	
		formobj.checkRow[i].checked=false;
		 }			
  	}
	}
}

</script>
</head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"></jsp:include>

<body>
<%HttpSession tSession = request.getSession(true); 
tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
	{	
 %> 
<form name="manageCartFrm" id="manageCartFrm" action = "updateManageCart.jsp" method="post" onSubmit="if (setVals(manageCartFrm)== false) {setValidateFlag('false'); return false;} else {setValidateFlag('true'); return true;}">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="30%">&nbsp;</td><td>&nbsp;</td></tr>
<tr>
<td  align="left"  bgcolor = "#cccccc" style="vertical-align: bottom"  colspan = "2"><b><%=LC.L_Manage_PreparationCart%><%--Manage Preparation Cart*****--%></b></td>
<td align="right" bgcolor = "#cccccc" >
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>		
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="formID" value="manageCartFrm"/>		
</jsp:include></td>
</tr>
<!-- Fixed Bug#7908 : Raviesh -->
<tr><td  colspan = "3"><font class = "comments"><%=MC.M_FlwgPatPrepCart_RemAutoVelos%><!-- The following patients have been selected for sample preparation. You may view these patients in the 'Preparation Cart' until their specimen(s) are prepared or the patients are removed from the Cart. Any patients remaining in the
'Preparation Cart' will be removed automatically from the Cart when you log out of Velos eSample.-->
</font></td></tr>
<tr><td colspan = "3">&nbsp;</td></tr>
</table>
<jsp:include page="preparationCart.jsp" flush="true"> 
		<jsp:param name="checkboxdisplay" value="Y"/>
		<jsp:param name="checkName" value="<%=LC.L_Rem_All%>"/>	
</jsp:include> 
</form>
<%}
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body>
</html>