<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>

<title><%=MC.M_MngPat_PatSch%><%--Manage Patients >> Patient Schedules*****--%> </title>
<jsp:include page="panel.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/>
<SCRIPT Language="javascript">
window.name="allSchedules";
var orgObject;

var statusLink=function(elCell, oRecord, oColumn, oData)
{
	var reason=oRecord.getData("PATSTUDYSTAT_REASON");
	if (!(reason)) reason="";
	var note=oRecord.getData("PATSTUDYSTAT_NOTE");
	if (!(note)) note="";
	note=htmlEncode(note);
	var per=oRecord.getData("FK_PER");
	if (!per) per="";
	var htmlStr=oData;
	
	if (per.length>0) {
		var pcode=escape(encodeString(oRecord.getData("PER_CODE")));
		htmlStr=htmlStr+"  <A Title=\"<%=LC.L_Change_Status%><%--Change Status*****--%>\" href =\"enrollpatient.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=2&pkey="+oRecord.getData("FK_PER")+"&patProtId="+oRecord.getData("PK_PATPROT")+"&patientCode="+pcode+"&page=patientEnroll&studyId="+oRecord.getData("FK_STUDY")+"\"";
		htmlStr=htmlStr+" onmouseover=\"return overlib('<tr><td><font size=2><b><%=LC.L_Reason%><%--Reason*****--%>: </b></font><font size=1>"+reason+"</font></td></tr><tr><td><font size=2><b><%=LC.L_Status_Date%><%--Status Date*****--%>: </b></font><font size=1>"+oRecord.getData("PATSTUDYSTAT_DATE_DATESORT")+
			"</font></td></tr><tr><td><font size=2><b><%=LC.L_Notes%><%--Notes*****--%>: </b></font><font size=1>"+note+"</font></td></tr>',CAPTION,'<%=LC.L_Patient_Status%><%--Patient Status*****--%>',RIGHT,ABOVE);\"  onmouseout=\"return nd();\"><img border=\"0\" src=\"./images/edit.gif\" /></A>"
	}
	elCell.innerHTML=htmlStr;
}

var editEvent=function(elCell, oRecord, oColumn, oData)
{
	 var subType=oRecord.getData("PATSTUDYSTAT_SUBTYPE");
	 var htmlStr="";
	 if (subType)
	 {
	 	if ((subType.toLowerCase())!="lockdown")
		 {
		 	var htmlStr="<A href=\"javascript:void(0)\" onClick=\"openEditDetails('" +oData+"','"+oRecord.getData("FK_PER")+"','"+ oRecord.getData("FK_STUDY")+"')\"><img border=\"0\" src=\"./images/edit.gif\" title=\"<%=LC.L_Edit%>\"/></A>"
		 }
	 }
	 elCell.innerHTML=htmlStr;
}

pStdLink=function(elCell, oRecord, oColumn, oData){
	var htmlStr="";
	//var pcode=htmlEncode(oRecord.getData("PER_CODE"));
	var pcode=escape(encodeString(oRecord.getData("PER_CODE")));
	htmlStr="<A href =\"patientschedule.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=3&mode=M&pkey="+oRecord.getData("FK_PER")+"&patProtId="+oRecord.getData("PK_PATPROT")+"&patientCode="+pcode+"&page=patientEnroll&studyId="+oRecord.getData("FK_STUDY")
	          +"&generate=N&statid="+oRecord.getData("PK_PATSTUDYSTAT")+"\">" + oData +"</A>";
	elCell.innerHTML=htmlStr;
}

perIdLink=function(elCell, oRecord, oColumn, oData)
{
	var htmlStr="";
	
	var right =parseInt(oRecord.getData("RIGHT_MASK"));
	if (right>=4)
	{
		var patHPhone="",patBPhone="";
		var pcode=escape(encodeString(oData));
		var url="patientdetails.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=1&mode=M&pkey="+oRecord.getData("FK_PER")+"&patProtId="+oRecord.getData("PK_PATPROT")+"&patientCode="+pcode+"&page=patientEnroll&studyId="+oRecord.getData("FK_STUDY")+"&generate=N&statid="+oRecord.getData("PK_PATSTUDYSTAT")+"&studyVer=null";
		//Parse the phone numbers
		var phoneStr=oRecord.getData("MASK_PERSON_PHONE");

		if (phoneStr!= null){
			var phoneArray=phoneStr.split("[VELSEP]");
			if (phoneArray)
			{
				if (phoneArray[1]) patHPhone=phoneArray[1];
				if (phoneArray[2]) patBPhone=phoneArray[2];
			}
		 }
		//Parse the Address
		var patAddress1="",patAddress2="",patCity="",patState="",patZip="",patCountry="";
		var personNotes=oRecord.getData("PERSONNOTES");
		if (!personNotes) personNotes="";
		
		personNotes=htmlEncode(personNotes);

		var addStr=oRecord.getData("MASK_PATADDRESS");
		if (addStr != null){
			var addArray=addStr.split("[VELSEP]");
			if (addArray)
			{
				if (addArray[1]) patAddress1=addArray[1]||"";
				if (addArray[2]) patAddress2=addArray[2]||"";
				if (addArray[3]) patCity=addArray[3]||"";
				if (addArray[4]) patState=addArray[4]||"";
				if (addArray[5]) patZip=addArray[5]||"";
				if (addArray[6]) patCountry=addArray[6]||"";
			
			  }
		}
		var plname=oRecord.getData("MASK_PERSON_LNAME");
		if (!plname) plname="";

		var pfname=oRecord.getData("MASK_PERSON_FNAME");
		if (!pfname) pfname="";

		var patientName=plname+","+pfname;
		var paramArray = [patientName];
		 htmlStr="<table style=\"border-style:none\"><tr style=\"border-style:none\"><td style=\"border-style:none\"><A href="+url+" onmouseover=\"return overlib('<tr><td><font size=2><b><%=LC.L_Home_Phone%><%--Home Phone*****--%> : </b></font><font size=1>"+patHPhone+"</font></td></tr><tr><td><font size=2><b><%=LC.L_Work_Phone%><%--Work Phone*****--%>: </b></font><font size=1>"+patBPhone+
		"</font></td></tr><tr><td><font size=2><b><%=LC.L_Address%><%--Address*****--%>: </b></font></td></tr><tr><td><font size=1>"+patAddress1+"</font></td></tr><tr><td><font size=1>"+patAddress2+"</font></td></tr><tr><td><font size=1>"+patCity+"&nbsp;<font size=1>"+patState+
		"</font>&nbsp;<font size=1>"+patZip+"</font></td></tr><tr><td><font size=1>"+patCountry+
		"</font></td></tr><tr><td><font size=2><b><%=LC.L_Notes%><%--Notes*****--%>: </b></font><font size=1>"+personNotes+"</font></td></tr>',CAPTION,'"+getLocalizedMessageString('L_Pat_Hyp',paramArray)<%--Patient -" +patientName+ "*****--%>+"',RIGHT,ABOVE);\" onmouseout=\"return nd();\" ><img src=\"./images/View.gif\" border=\"0\" align=\"left\"/></A>";
		htmlStr = htmlStr + "</td> <td style=\"border-style:none\">"+ oData + "</td></tr></table>";
	} else
		{
	 	htmlStr=oData;
	}
	elCell.innerHTML=htmlStr;
}

function checkForAll()
{
	return true;
}


function back(pk,patId) {

	window.opener.document.reports.id.value = pk;
	window.opener.document.reports.selPatient.value = patId;
	window.opener.document.reports.val.value = patId;
	self.close();

}

	function  validate(formobj){

	 /*if (!(isInteger(formobj.filterLastVisit.value))){
	 		alert("Please enter a valid number");
			formobj.filterLastVisit.focus();
            return false;
		 }*/
		
		}
function openEditDetails(patientProtId,patientId,studId){

	windowName=window.open("editMulEventDetails.jsp?patProtId="+patientProtId+"&pkey="+patientId+"&studyId="+studId+"&calledFrom=E","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=500,left=150");
	windowName.focus();
}

function openExcelWin(formobj)
{
	param1=formobj.excelLink.value ;
    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,width=675,height=600,top=60,left=200");
	windowName.focus();

}
function openWinStatus(patId,patStatPk,changeStatusMode)
{
	windowName= window.open("patstudystatus.jsp?changeStatusMode="+changeStatusMode+"&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=500");
	windowName.focus();

}

function setOrder(formObj,orderBy) //orderBy column name code
{
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
	}
	formObj.orderByColumnCode.value= orderBy;

	formObj.submitMode.value="P";//pagination
	formObj.submit();
}

function jumpToPage(formObj,pagenum)
{

	formObj.currentpage.value= pagenum;
	formObj.submitMode.value="P";//pagination

	formObj.submit();
}



function setfocus(){
   //km-to fix the Bug 2321
   if(document.forms[0].flag.value==1)
      document.forms[0].patientid.focus();
}

//Added by Gopu for May-June 2006 Enhancement (#P1)

function openOrgpopupView(patId,viewCompRight) {

	var viewCRight ;
	var rtStr;

	viewCRight = parseInt(viewCompRight);

	if (viewCRight <=0)
	{
		rtStr = "nx";
	}
	else
	{

		rtStr = "dx";
	}

    windowName = window.open("patOrgview.jsp?patId="+patId+"&c="+rtStr,"patient","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=300,height=150,left=400,top=400");
	windowName.focus();
}

var paginate_study;
$E.addListener(window, "load", function() {
  	paginate_study=new VELOS.Paginator('allSchedules',{
		sortDir:"asc",
		sortKey:"NEXT_VISIT_DATESORT",
		defaultParam:"userId,accountId,grpId",
		filterParam:"filterNextVisit",
		dataTable:'serverpagination',
		refreshMethod:'Refresh()',
		searchEnable:false,
		rowSelection:[5,10,25,50]

	});
  	paginate_study.runFilter();
 }
) ;


</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />
<jsp:useBean id="CommonB" scope="page" class="com.velos.eres.web.common.CommonJB" />


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="reportIO" scope="session" class="com.velos.eres.service.util.ReportIO"/>


<% String src;



src= request.getParameter("srcmenu");

String openMode=request.getParameter("openMode"); //open this window as popup or full screen

%>






<body onload="setfocus()" class="yui-skin-sam">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>



<%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{

  String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	int pageRight = 0;
	String superUserRights = "";

	//Added by Manimaran to give access right for default admin group to the delete link
	String userId = (String) tSession.getValue("userId");
    int usrId = EJBUtil.stringToNum(userId);

    userB = (UserJB) tSession.getValue("currentUser");
	String accId = (String) tSession.getValue("accountId");


	String defGroup = userB.getUserGrpDefault();

	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();

	superUserRights = groupB.getDefaultStudySuperUserRights(userId);

	if (StringUtil.isEmpty(superUserRights))
	{
		superUserRights = "";
	}



	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

	if ( pageRight >= 4)

	{

%>
<br>

<%
	CodeDao cdDisplay = new CodeDao();
 	String filDoneOn = "";
	String dStatus = "";
	
	// selFirst="";
	String filNext = "";

	//Added by Manimaran on Apr21,2005 for Enrolled PatientStudyId search
	int p;

	Integer iStat;
	String anySiteSelected = "0";

    String selectedTab="" ;

    selectedTab=request.getParameter("selectedTab") ;

	boolean showStudyCentric = false;
	SettingsDao settingsdao = new SettingsDao();
	ArrayList arSettingsValue = new ArrayList();
	String studyCentricStr = "";

	filNext = request.getParameter("filterNextVisit") ;
	if ("".equals(filNext) || filNext == null){
		filNext = "T";
	}

	String orderType = "";
	orderType = request.getParameter("orderType");



	String pagenum = "";
	int curPage = 0;
	long startPage = 1;
	String stPage;
	long cntr = 0;
	pagenum = request.getParameter("currentpage");

	String submitMode = request.getParameter("submitMode");

	if (StringUtil.isEmpty(submitMode))
	{
		submitMode = "S"; //not submitted via pagination
	}

	if (submitMode.equals("S"))
	{
			pagenum = "1";
	}

		if (pagenum == null)
		{
			pagenum = "1";
		}


		curPage = EJBUtil.stringToNum(pagenum);


	if (StringUtil.isEmpty(orderType) )
	{
		orderType = "asc";
	}

	int tableSize = 0;
	int colWidth = 90;

	//here 12/11 is the number of columns in the browser
	if (groupName.equalsIgnoreCase("admin"))
	{
		tableSize = 13 * colWidth;
	}
	else
	{
		tableSize = 12 * colWidth;
	}

	String dNextVisit = EJBUtil.getRelativeTimeDDSchedule("filterNextVisit\' id=\'filterNextVisit",filNext);

	String uName = (String) tSession.getValue("userName");
	int defCnt = 0;

	int patDataDetail = 0;
	int personPK = 0;

	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));


		StringBuffer strb = new StringBuffer();
		strb.append("<table width='100%' cellspacing='1' cellpadding='0'>");
		strb.append("<tr><th width='15%'>"+LC.L_Study_Number/*Study Number*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Patient_Id/*Patient ID*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Patient_StudyId/*Patient Study ID*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_F_Name/*F Name*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_L_Name/*L Name*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Pt_Status/*Pt. Status*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Next_Due/*Next Due*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Visit/*Visit*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Visit_Status/*Visit Status*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Assigned_To/*Assigned To*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_PI/*PI*****/+"</th>");
		strb.append("<th width='8%'></th></tr>");

	%>

<Form  name="search" METHOD="POST" onsubmit="return false;">
 <input type=hidden name="orderType" value="<%=orderType%>">
 <input type=hidden name="openMode" value="<%=openMode%>">
<input type=hidden name="submitMode" value="">
<input type=hidden name="flag" value="1">



<%
	StringBuffer sbStd = new StringBuffer();

%>

<DIV class="tabDefTopN" id="div1">
  <jsp:include page="mgpatienttabs.jsp" flush="true"/>
</div>

<DIV class="tabFormTopN tabFormTopN_PS" id="div2">


<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top">
	<tr height="10">
		<td class="labelFont" width="10%"><b><%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<input type="hidden" id="srcmenu" Value="<%=src%>">
	<input type="hidden" id="selectedTab" value="<%=selectedTab%>">
	<tr>
		<td width="18%" align="left">
		<P><%=LC.L_Upcoming_Visits%><%--Upcoming Visits*****--%>:&nbsp; <%=dNextVisit%>&nbsp;&nbsp;
		<button type="submit" onclick="paginate_study.rowsPerPage=0;  (paginate_study.runFilter());"><%=LC.L_Search%></button>
		</P>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

</div>


<DIV class="tabFormBotN tabFormBotN_P_3" id="browser">

<DIV>
	<table style="display:block;">
		<tr height="1"></tr>
  		<tr>
			<td colspan = "7"><B><%=LC.L_MatchPat_Sch%><%--Matching Patient Schedules*****--%> </B>&nbsp;</td>
		</tr>
		<tr></tr>
	</table>
		
  <%
        defCnt = 0;
        Calendar now = Calendar.getInstance();
		String startDt = "";
		String endDt =  "";

		//startDt =  ""+ (now.get(now.MONTH) + 1) + "/" + "01" + "/" + now.get(now.YEAR) ;

		startDt = DateUtil.getFormattedDateString(String.valueOf(now.get(now.YEAR)),"01",String.valueOf( now.get(now.MONTH) + 1) ) ;

	    now.add(now.MONTH,1);

	    //endDt = "" + (now.get(now.MONTH) + 1) + "/" + now.getActualMaximum(now.DAY_OF_MONTH) + "/" + now.get(now.YEAR)  ;

	    endDt  = DateUtil.getFormattedDateString(String.valueOf(now.get(now.YEAR)),String.valueOf(now.getActualMaximum(now.DAY_OF_MONTH)),String.valueOf( now.get(now.MONTH) + 1) ) ;
%>


			<Input type=hidden name="repName" value=""/>
			<Input type=hidden name="repId" value=""/>
			<Input type=hidden name="studyPk" value=""/>
			<Input type=hidden name="filterType" value = ""/>
			<input type=hidden name="id" value=""/>
			<input type=hidden name="protId" value=""/>
			<input type=hidden name="dateFrom" value="<%=startDt%>"/>
			<input type=hidden  name="dateTo" value="<%=endDt%>"/>
			<Input type="hidden" name="currentpage" value="<%=curPage%>"/>
			<input type="hidden" id="grpId" value="<%=defGroup%>"/>
			<input type="hidden" id="userId" value="<%=usrId%>"/>
			<input type="hidden" id="accountId" value="<%=accId%>"/>
			<input type="hidden" id="groupName" value="<%=groupName%>"/>


<div id="serverpagination"></div>







  <%} //end of if body for page right

else



{



%>



<jsp:include page="accessdenied.jsp" flush="true"/>

<%



} //end of else body for page right





  ///////////////

}//end of if body for session

else{

%>



  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>

</div>

</Form>

<div class ="mainMenu" id = "emenu">

 <jsp:include page="getmenu.jsp" flush="true"/>

</div>


</body>
</html>
