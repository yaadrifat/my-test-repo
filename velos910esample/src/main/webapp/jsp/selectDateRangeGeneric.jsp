<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=LC.L_Date_Filter%><%--Date Filter*****--%></title>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT Language="javascript">

function back(parm,sec,formobj) {
	var from = formobj.from.value;

	if(from == "patientreports"){
		if (document.layers) {
			window.opener.document.div1.document.reports.range.value ="";
			window.opener.document.div1.document.reports.year.value = "";
			window.opener.document.div1.document.reports.year1.value = "";
			window.opener.document.div1.document.reports.month.value = "";
			window.opener.document.div1.document.reports.dateFrom.value = "";
			window.opener.document.div1.document.reports.dateTo.value = "";
			if (parm == "Y") {
				window.opener.document.div1.document.reports.range.value ="Year: "  +formobj.year[formobj.year.selectedIndex].value;
				window.opener.document.div1.document.reports.year.value = formobj.year[formobj.year.selectedIndex].value;
			}
			if (parm == "M") {
				window.opener.document.div1.document.reports.year1.value =formobj.year1[formobj.year1.selectedIndex].value;
				window.opener.document.div1.document.reports.month.value = formobj.month[formobj.month.selectedIndex].value;
				window.opener.document.div1.document.reports.range.value ="Month: "  +formobj.month[formobj.month.selectedIndex].value +" Year: " +formobj.year1[formobj.year1.selectedIndex].value;
			}
			if (parm == "D"){
		 		window.opener.document.div1.document.reports.dateFrom.value = formobj.dateFrom.value;
				window.opener.document.div1.document.reports.dateTo.value = formobj.dateTo.value;
				window.opener.document.div1.document.reports.range.value ="From: "  +formobj.dateFrom.value +" To: " +formobj.dateTo.value; 
			}
		}
		else
		{
			window.opener.document.reports.range.value ="";
			window.opener.document.reports.year.value = "";
			window.opener.document.reports.year1.value = "";
			window.opener.document.reports.month.value = "";
			window.opener.document.reports.dateFrom.value = "";
			window.opener.document.reports.dateTo.value = "";
			if (parm == "Y") {
				window.opener.document.reports.range.value ="Year: "  +formobj.year[formobj.year.selectedIndex].value;
				window.opener.document.reports.year.value = formobj.year[formobj.year.selectedIndex].value;
			}
			if (parm == "M") {
				window.opener.document.reports.year1.value =formobj.year1.value;
				window.opener.document.reports.month.value = formobj.month.value;
				window.opener.document.reports.range.value ="Month: "  +formobj.month.value +" Year: " +formobj.year1.value;
			}
			if (parm == "D"){
				if (CompareDates(formobj.dateFrom.value,formobj.dateTo.value,'>')==true) {
					alert("<%=MC.M_ToDtGtrThan_FromDt%>");/*alert("To Date should be greater than From Date");*****/
					return false;
				}
		 		window.opener.document.reports.dateFrom.value = formobj.dateFrom.value;
				window.opener.document.reports.dateTo.value = formobj.dateTo.value;
				window.opener.document.reports.range.value ="From: "  +formobj.dateFrom.value +" To: " +formobj.dateTo.value; 
			}
		}
	} else {
		if (document.layers) {
			window.opener.document.div1.document.reports.range.value ="";
			window.opener.document.div1.document.reports.year.value = "";
			window.opener.document.div1.document.reports.year1.value = "";
			window.opener.document.div1.document.reports.month.value = "";
			window.opener.document.div1.document.reports.dateFrom.value = "";
			window.opener.document.div1.document.reports.dateTo.value = "";
			if (parm == "Y") {
				window.opener.document.div1.document.reports.range.value ="Year: "  +formobj.year[formobj.year.selectedIndex].value;
				window.opener.document.div1.document.reports.year.value = formobj.year[formobj.year.selectedIndex].value;
			}
			if (parm == "M") {
				window.opener.document.div1.document.reports.year1.value =formobj.year1[formobj.year1.selectedIndex].value;
				window.opener.document.div1.document.reports.month.value = formobj.month[formobj.month.selectedIndex].value;
				window.opener.document.div1.document.reports.range.value ="Month: "  +formobj.month[formobj.month.selectedIndex].value +" Year: " +formobj.year1[formobj.year1.selectedIndex].value;
			}
			if (parm == "D"){
		 		window.opener.document.div1.document.reports.dateFrom.value = formobj.dateFrom.value;
				window.opener.document.div1.document.reports.dateTo.value = formobj.dateTo.value;
				window.opener.document.div1.document.reports.range.value ="From: "  +formobj.dateFrom.value +" To: " +formobj.dateTo.value; 
			}
		}
		else
		{
			window.opener.document.reports.range.value ="";
			window.opener.document.reports.year.value = "";
			window.opener.document.reports.year1.value = "";
			window.opener.document.reports.month.value = "";
			window.opener.document.reports.dateFrom.value = "";
			window.opener.document.reports.dateTo.value = "";
			if (parm == "Y") {
				window.opener.document.reports.range.value ="Year: "  +formobj.year[formobj.year.selectedIndex].value;
				window.opener.document.reports.year.value = formobj.year[formobj.year.selectedIndex].value;
			}
			if (parm == "M") {
				window.opener.document.reports.year1.value =formobj.year1.value;
				window.opener.document.reports.month.value = formobj.month.value;
				window.opener.document.reports.range.value ="Month: "  +formobj.month.value +" Year: " +formobj.year1.value;
			}
			if (parm == "D"){
				if (CompareDates(formobj.dateFrom.value,formobj.dateTo.value,'>')==true) {
					alert("<%=MC.M_ToDtGtrThan_FromDt%>");/*alert("To Date should be greater than From Date");*****/
					return false;
				}
		 		window.opener.document.reports.dateFrom.value = formobj.dateFrom.value;
				window.opener.document.reports.dateTo.value = formobj.dateTo.value;
				window.opener.document.reports.range.value ="From: "  +formobj.dateFrom.value +" To: " +formobj.dateTo.value; 
			}
		}
	}
	self.close();
}

</SCRIPT>

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>


<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

</head>



<body>
<%-- INF-20084 Datepicker-- AGodara --%>
<jsp:include page="jqueryUtils.jsp" flush="true"></jsp:include>
<Form  name="date" method="post">
<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String parm=request.getParameter("parm") ;
	String section=request.getParameter("section");
	String from=request.getParameter("from");	
	Calendar cal = Calendar.getInstance();
	int currYear = cal.get(cal.YEAR);
	
	from = (from==null)?"-":from;
%>
<input type='hidden' name='from' value='<%=from%>'>
<P class=defComments> <%=MC.M_Specify_DtFilter%><%--Please specify the Date Filter*****--%></P>

  <% if (parm.equals("Y")) {%> 
  <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td class=tdDefault width=80%> 
	  <P class="defComments"><%=LC.L_Select_Year%><%--Select Year*****--%>: <select name="year"> 
    	<% int i = currYear - 50;
	    int count = 0;
    	for (count= i; count<= currYear + 10 ;count ++){
    		if (count == currYear) {%>
    			<option value=<%=count%> SELECTED><%=count%></option>
	    	<%}else{ %>
    			<option value=<%=count%>><%=count%></option>
    		<%}%>
	    <%}%>
		</select>
   	</P>
   </td>
   <%}%>
   <% if (parm.equals("M")) {%> 
  <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td class=tdDefault width=80%> 
	    <P class="defComments"><%=LC.L_Sel_MthOrYear%><%--Select Month/Year*****--%>: 
		<select name="month"> 
        <% int cnt = 0;
        for (cnt= 1; cnt<= 12;cnt ++){%>
        	<option value=<%=cnt%>><%=cnt%></option>
        <%}%>
        </select>
        
        <select name="year1"> 
        <% int j = currYear - 50;
        for (cnt= j; cnt<= currYear + 10;cnt ++){
        	if (cnt == currYear) {%>
        		<option value=<%=cnt%> SELECTED><%=cnt%></option>
        	<%}else{ %>
        		<option value=<%=cnt%>><%=cnt%></option>
        
        	<%}%>
        <%}%>
        </select>
     </P>
	</select>
   </P>
   </td>
   <%}%>
   <% if (parm.equals("D")) {%>
 <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td class=tdDefault width=20%> 
	   <P class="defComments">
       	   <%=LC.L_Date_From%><%--Date From*****--%>: 
       </P>
       	</td>
<%-- INF-20084 Datepicker-- AGodara --%>         	
      	<td>
       	   <P class="defComments">
       	   <Input type=text name="dateFrom" class="datefield" READONLY> 
       	   </P>
	       <td><P class="defComments">        	   <%=LC.L_Date_To%><%--Date To*****--%>: 
       	   </P></td>
       	   <td><P class="defComments">
      	   <Input type=text name="dateTo" class="datefield" READONLY> 
        	   </P>
        	</td>
          </tr>
          <tr>
           	<td><P class="defComments"></P></td>
        	<td><P class="defComments"></P></td>
		  </tr>
   <tr>
   <%}%>
 
     <td class=tdDefault width=20%> 
     <!--   <Input type="submit" name="submit" value="Go"> 
	 <input type="image" src="../images/jpg/go_white.gif" align="bottom" border="0" onClick="back('<%//=parm%>','<%//=section%>')">-->
	
		<button type="submit" onclick="return back('<%=parm%>','<%=section%>',document.date)"><%=LC.L_Search%></button> 


      </td>
    </tr>
  </table>
</Form>
<%}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
</div>
<div class ="mainMenu" id = "emenu"> </div>
</body>
</html>
