var subjGridChangeList = null;
var scItemChangeList = null;
var scItemPurgeList = [];
var purgedItemLabelArray = null;
var visitLabelArray = null;
var visitDisplacementArray = null;
var itemLabelArray = null;
var incomingUrlParams = null;
var calStatus = null;
var lastTimeSubmitted = 0;
var horizontalPage = 0;
var subjectCostItemCount = 0;
var formatterFail = false;

var myDataTable;
var mycostCatIds = [];
var mycostCats = [];
var pgRight=0;
var myDialog = null;
includeJS('js/velos/velosUtil.js');
var Total_Chg=L_Total_Chg;
var ErrGetResp_CnctVelos=M_ErrGetResp_CnctVelos;
var ValEtrFwg_12DgtValid=M_ValEtrFwg_12DgtValid;
var ValFwg_PipeQuoteValid=M_ValFwg_PipeQuoteValid;
var ValEtrCrit_250CharPlsEtr=M_ValEtrCrit_250CharPlsEtr;
var Preview_AndSave=L_Preview_AndSave;
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var PlsEnterEsign=M_PlsEnterEsign;
var CldNtCont_InvldIndx=M_CldNtCont_InvldIndx;
var CnfrmVst_ItemChg=M_CnfrmVst_ItemChg;
var ThereNoChg_Save=M_ThereNoChg_Save;
var Esignature=L_Esignature;
var Valid_Esign=L_Valid_Esign;
var Invalid_Esign=L_Invalid_Esign;
var Delete_Row=L_Delete_Row;
var varSelc=L_Selc;
var Unselc=L_Unselc;
var LoadingPlsWait=L_LoadingPlsWait;
var PleaseWait_Dots=L_PleaseWait_Dots;
var ValEtrFwg_UnitCst11DgtMax=M_ValEtrFwg_UnitCst11DgtMax;
var Item_Name=L_Item_Name;
var saveDropDown=false;
// Define VELOS.subjectGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.subjectGrid = function (url, args) {
	var isIe = jQuery.browser.msie;
	pgRight = parseInt(YAHOO.util.Dom.get("pageRight").value);
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updateEvtVisits.jsp
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}

	showPanel(); // Show the wait panel; this will be hidden later

	// Define handleSuccess() of VELOS.subjectGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'serverpagination'; // name of the div to hold subjectGrid
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			alert(ErrGetResp_CnctVelos);/*alert('Error getting response from server. Please contact Velos Support.');*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
			alert(getLocalizedMessageString("L_Error_Msg",paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}

		mycostCatIds = respJ.costCategoryId;
		mycostCats = respJ.costCategory;

		var itemCount = respJ.itemCount;
		subjectCostItemCount = itemCount;

		var myFieldArray = [];
		var myColumnDefs = [];
		if (respJ.colArray) {
			myColumnDefs = respJ.colArray;
			for (var iX=0; iX<myColumnDefs.length; iX++) {

				myColumnDefs[iX].width = (myColumnDefs[iX].key == 'item') ?
					200:(myColumnDefs[iX].key == 'costCat') ?
					150:(myColumnDefs[iX].key == 'checkItem') ? 30:100;

				myColumnDefs[iX].resizeable = (myColumnDefs[iX].key == 'costCat') ? false : true;
				myColumnDefs[iX].sortable = false;

				if (myColumnDefs[iX].key == 'delete') {
					myColumnDefs[iX].madeUp = true;
					myColumnDefs[iX].width = 50;
					myColumnDefs[iX].editor = 'image';
				}

				if (myColumnDefs[iX].key == 'costCat') {
					myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.costCategory,disableBtns:true});
				}

				if (myColumnDefs[iX].type!=undefined){
					var type= myColumnDefs[iX].type;
				 	var typeInfo = VELOS.subjectGrid.types[type];
				 	if (typeInfo.parser!=undefined)
				 		myColumnDefs[iX].parser=typeInfo.parser;
				 	if (typeInfo.formatter!=undefined)
				 		myColumnDefs[iX].formatter=typeInfo.formatter;
				 	if (typeInfo.stringer!=undefined)
				 		myColumnDefs[iX].stringer=typeInfo.stringer;
					if (typeInfo.editorOptions!=undefined)
						myColumnDefs[iX].editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);

					if (type == 'number'){ 
						myColumnDefs[iX].formatter= function(el, oRecord, oColumn, oData) {
							formatterFail = false;
							if (oData != '0'){
								oData = oData.replace(/^[0]+/g,""); //remove leading zeros
							}
							if (/^\.\d{0,2}?$/.test(oData)){ // If no zero before decimal add one
								oData = "0" + oData;
							}
							if(oData == '') oData ="0"; //If empty set 0
							if (oData == '0'){
								el.innerHTML = oData;
								oRecord.setData.oColumn= el.innerHTML;
								if (myDataTable && myDataTable._oCellEditor){
									myDataTable._oCellEditor.value = oData;
								}
							} else {
								if (/^\d{0,11}([.]\d{0,2})?$/.test(oData)){
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
								}else{
									formatterFail = true;
									if (myDialog)
										myDialog.hide();
									if (myDataTable && myDataTable._oCellEditor){
										myDataTable._oCellEditor.value = null;
									}
									alert(ValEtrFwg_UnitCst11DgtMax);/*alert("The value entered does not pass the following criteria:\n \n -'Unit Cost' should be a number.\n -'Unit Cost' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
								}
							}
						};
					}
					if (type == 'integer'){ 
						myColumnDefs[iX].formatter= function(el, oRecord, oColumn, oData) {
							formatterFail = false;
							if (oData != '0'){
								oData = oData.replace(/^[0]+/g,""); //remove leading zeros
							}
							if(oData == '') oData ="0"; //If empty set 0
							if (oData == '0'){
								el.innerHTML = oData;
								oRecord.setData.oColumn= el.innerHTML;
								if (myDataTable && myDataTable._oCellEditor){
									myDataTable._oCellEditor.value = oData;
								}
							} else {
								if (/^\d{0,11}$/.test(oData)){
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myDataTable && myDataTable._oCellEditor){
										myDataTable._oCellEditor.value = oData;
									}
								}else{
									formatterFail = true;
									if (myDialog)
										myDialog.hide();
									if (myDataTable && myDataTable._oCellEditor){
										myDataTable._oCellEditor.value = null;
									}
									alert(ValEtrFwg_12DgtValid);/*alert("The value entered does not pass the following criteria:\n \n -'Number of Units' should be an integer.\n -'Number of Units' should be less than 12 digits.\n \nPlease enter a valid value.");*****/
								}
							}
						};
					}
					if (type == 'varchar'){ 
						myColumnDefs[iX].formatter= function(el, oRecord, oColumn, oData) {
							formatterFail = false;
							if (!oData)
								oData = '';

							if (/['"|]+/g.test(oData)){
								formatterFail = true;
								if (myDialog)
									myDialog.hide();
								if (myDataTable && myDataTable._oCellEditor){
									myDataTable._oCellEditor.value = null;
								}
								itemLabelArray['e'+oRecord.getData('itemSeq')] = ''; // clear this elem in itemLabelArray[] because corresponding data will be cleared by cell editor
								alert(ValFwg_PipeQuoteValid);/*alert("The value entered does not pass the following criterion:\n \n - 'Item Name' should not contain following special characters: pipe(|), single quote('), double quote(\").\n \nPlease enter a valid value.");*****/
								return;
							}
							
							if (oData.length < 251){
								el.innerHTML = oData;
								oRecord.setData(oColumn.key, VELOS.subjectGrid.decodeData(oData));
							}else{
								formatterFail = true;
								if (myDialog)
									myDialog.hide();
								if (myDataTable && myDataTable._oCellEditor){
										myDataTable._oCellEditor.value = null;
								}
								alert(ValEtrCrit_250CharPlsEtr);/*alert("The value entered does not pass the following criterion:\n \n -'Item Name' length should not be greater than 250 characters.\n \nPlease enter a valid value.");*****/
							}
						};
					 
					}
				}

				var fieldElem = [];
				if (myColumnDefs[iX].key && myColumnDefs[iX].key.match(/^v[0-9]+$/)) {
					myColumnDefs[iX].formatter = 'checkbox';
					myColumnDefs[iX].editor = 'checkbox';
					fieldElem['parser'] = 'checkbox';
				} else if (myColumnDefs[iX].key == 'costCatId'
					|| myColumnDefs[iX].key == 'itemId' || myColumnDefs[iX].key == 'itemSeq'){
					myColumnDefs[iX].hidden = true;
				} else if (myColumnDefs[iX].key == 'checkItem') {
					myColumnDefs[iX].editor = 'checkbox';
				}
				fieldElem['key'] = myColumnDefs[iX].key;
				myFieldArray.push(fieldElem);
			}
		}
		visitLabelArray = [];
		for (var iX=0; iX<myColumnDefs.length; iX++) {
			if (!myColumnDefs[iX].label) { continue; }
			if (myColumnDefs[iX].key == 'item' || myColumnDefs[iX].key == 'itemId') { continue; }
			var inputPos = myColumnDefs[iX].label.indexOf(" <input type");
			visitLabelArray[myColumnDefs[iX].key] = myColumnDefs[iX].label.slice(0, inputPos);
		}
		visitDisplacementArray = [];
		if (respJ.displacements) {
			var myDisplacements = [];
			myDisplacements = respJ.displacements;
			for (var iX=0; iX<myDisplacements.length; iX++) {
				visitDisplacementArray[myDisplacements[iX].key] = myDisplacements[iX].value;
			}
		}

		var myDataSource = new YAHOO.util.DataSource(respJ.dataArray);
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
			fields: myFieldArray
		};

		var maxWidth = 800;
		var maxHeight = 800;

		if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 800; }
		else if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = 1000; }

		if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
		else if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = 480; }

		var calcHeight = respJ.dataArray.length*40 + 40;
		if (calcHeight > maxHeight) { calcHeight = maxHeight; }

		calcHeight = 320;
		myDataTable = new YAHOO.widget.DataTable(
			this.dataTable,
			myColumnDefs, myDataSource,
			{
				width:"98%",
					//maxWidth+"px",
				height:calcHeight+"px"
				//caption:"DataTable Caption",
				//scrollable:true
			}
		);

		//FIX #6285, #6319
		if (YAHOO.util.Dom.get("rowCount")){
			YAHOO.util.Dom.get("rowCount").value="0";
		}
		
		itemIdArray = []; 	itemSeqArray = []; 		checkItemArray = [];
		itemLabelArray = [];		purgedItemLabelArray = [];
		delButtonArray = [];
		var itemIdCells = $D.getElementsByClassName('yui-dt-col-itemId', 'td', myDataTable.getTableEl());
		for (var iX=0; iX<itemIdCells.length; iX++) {
			var el = new YAHOO.util.Element(itemIdCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			
			var itemSeqTd = parent.getElementsByClassName('yui-dt-col-itemSeq', 'td')[0];
			var itemSeqEl = new YAHOO.util.Element(itemSeqTd);
			var itemSeq = itemSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			itemSeqArray.push(itemSeq);
			
			var itemIdTd = parent.getElementsByClassName('yui-dt-col-itemId', 'td')[0];
			var itemIdEl = new YAHOO.util.Element(itemIdTd);
			var itemId = itemIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			itemIdArray.push(itemId);
			
			var itemTd = parent.getElementsByClassName('yui-dt-col-item', 'td')[0];
			var itemEl = new YAHOO.util.Element(itemTd);
			var itemName = itemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			itemLabelArray['e'+itemSeq] = itemName;
			
			var checkItemTd = parent.getElementsByClassName('yui-dt-col-checkItem', 'td')[0];
			var checkItemEl = new YAHOO.util.Element(checkItemTd);
			var checkItem = checkItemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			checkItemArray.push(checkItem);
			
			var parentId = parent.get('id');
			var iHTML = '<input type="checkbox"'
			+ ' name="all_e'+itemSeq+'" id="all_e'+itemSeq+'"'
			+ ' onclick="VELOS.subjectGrid.itemAll(\''+parentId+'\','+itemSeq+','+itemId+');" /> ';
			
			checkItemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=iHTML;
			
			var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
			var delButtonEl = new YAHOO.util.Element(delButtonTd);
			var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			delButtonArray.push(delButton);
			
			delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
			= '<div align="center"><input type="image" src="../images/delete.png" title="'+Delete_Row+'" alt="'+Delete_Row/*Delete Row*****/+'" border="0"'
			+ ' name="delete_e'+itemId+'" id="delete_e'+itemId+'"/></div> ';
		}
		
		subjGridChangeList = [];
		myDataTable.subscribe("checkboxClickEvent", function(oArgs){
			var elCheckbox = oArgs.target;
			var oRecord = this.getRecord(elCheckbox);
			var column = this.getColumn(elCheckbox);
			var itemId = oRecord.getData().itemId;
			
			if (itemId == 0){ 
				if(!f_check_perm_noAlert(pgRight,'N')){
					elCheckbox.checked = (!elCheckbox.checked);
					return;
				}
			}else{
				if(!f_check_perm_noAlert(pgRight,'E')){
					elCheckbox.checked = (!elCheckbox.checked);
					return;
				}
			}
			
			var oKey = 'e'+oRecord.getData().itemSeq+column.key
			oRecord.setData(oKey, elCheckbox.checked);
			if (subjGridChangeList[oKey] == undefined) {
				subjGridChangeList[oKey] = elCheckbox.checked;
			} else {
				subjGridChangeList[oKey] = undefined;
			}
		});

        // Subscribe to items for row selection
		myDataTable.subscribe("rowMouseoverEvent", function(oArgs) {
			//myDataTable.onEventHighlightRow(oArgs);
			var oRecord = this.getRecord(oArgs.target);
			return overlib(itemLabelArray['e'+oRecord.getData().item],CAPTION,Item_Name);
		});
		myDataTable.subscribe("rowMouseoutEvent", function(oArgs) {
			myDataTable.onEventUnhighlightRow(oArgs);
			return nd();
		});
		myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);
		myDataTable.focus();

		// I subscribe to the cellClickEvent to respond to the action icons and, if none, to pop up the cell editor
		myDataTable.subscribe('cellClickEvent',function(oArgs) {
			var target = oArgs.target;
			var column = this.getColumn(target);
			var key = column.key;
			var oRecord = this.getRecord(target);
			var itemSeq=oRecord.getData('itemSeq');
			var itemId = parseInt(oRecord.getData('itemId'));
			if (formatterFail) return false; //YK:Changes done for Bug#5838
			// columns with action icons should have an action property
			// The action thus independent of the visual image, if you click on that cell, whatever is displayed in it, the action will happen
			switch (column.action) {
				// for the delete action I request user confirmation and call the ask method of the DataSource
				case 'delete':
					if (f_check_perm(pgRight,'E')){
						var paramArray = [oRecord.getData('item')];
						if (confirm(getLocalizedMessageString("M_WantDel_PermSv",paramArray)))/*(confirm("Are you sure you want to delete Item '"+oRecord.getData('item')+"'? \n \nTo delete this Item permanently, please click on 'Preview and Save'."))*****/ {
							VELOS.subjectGrid.auditAction(itemSeq, 'Deleted', oRecord.getData('item'));
							//delete (itemLabelArray['e'+itemSeq]);
							//Pushing it in the Purged array
							purgedItemLabelArray['e'+itemSeq]=oRecord.getData('item');
	
							if (oRecord.getData('itemId')!="0"){
								var data = '|'+itemSeq+'|'+oRecord.getData('itemId');
	
								if (oRecord.getData('item') != null)
									data +='|'+oRecord.getData('item');
								else data +='|';
	
								data+='|'+oRecord.getData('costCatId')
								+'|'+oRecord.getData('costCat');
	
								if (oRecord.getData('cost') != null)
									data +='|'+oRecord.getData('cost');
								else data +='|';
	
								if (oRecord.getData('units') != null)
									data +='|'+oRecord.getData('units');
								else data +='|';
	
								data +='|';
								scItemPurgeList['itemSeq'+itemSeq] = data;
							}
							myDataTable.deleteRow(target);
						}
					}
					break;
				default:
					if (column.key != 'checkItem'){
						//for checkItem access rights are checked in itemAll()
						if (itemId == 0){ //New Record
							if (f_check_perm(pgRight,'N')){
								// If no action is given, I try to edit it
								this.onEventShowCellEditor(oArgs);
							}
						}else{//Existing Record 
							if (f_check_perm(pgRight,'E')){
								// If no action is given, I try to edit it
								this.onEventShowCellEditor(oArgs);
							}
						}
					}
					break;
			}
		});
	var checked=false;	
		var onCellSave = function(oArgs) {
			var elCell = oArgs.editor.getTdEl();
			var oOldData = oArgs.oldData;
			var oNewData = oArgs.newData;

			var oRecord = this.getRecord(elCell);
			var column = this.getColumn(elCell);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
			var itemSeqTd = parent.getElementsByClassName('yui-dt-col-itemSeq', 'td')[0];
			var itemSeqEl = new YAHOO.util.Element(itemSeqTd);
			var itemSeq = itemSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;

			var itemTd = parent.getElementsByClassName('yui-dt-col-item', 'td')[0];
			var itemEl = new YAHOO.util.Element(itemTd);
			var itemName = itemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			
			//Bug #10465 : AGodara
			//Bug #10445 : Tarun Kumar 
			if(saveDropDown==true){
			var elCell = oArgs.editor.getTdEl();
			var oOldData = oArgs.oldData;
			var oNewData = oArgs.newData;
			
			//var el = new YAHOO.util.Element(elCell);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
			var itemSeqTd = parent.getElementsByClassName('yui-dt-col-itemSeq', 'td')[0];
			var itemSeqEl = new YAHOO.util.Element(itemSeqTd);
			var itemSeq = itemSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;

			var itemTd = parent.getElementsByClassName('yui-dt-col-item', 'td')[0];
			var itemEl = new YAHOO.util.Element(itemTd);
			var itemName = itemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			
			if (oOldData != oNewData){
				VELOS.subjectGrid.auditAction(itemSeq, 'Updated', itemName);
			}
				this.onEventShowCellEditor({target:elCell});
				saveDropDown=false;
				return false;
			}
			
			if (column.key == 'costCat'){
				for (var j=0; j<mycostCatIds.length; j++){
					if (mycostCats[j] == oRecord.getData('costCat')) break;
				}
				oRecord.setData('costCatId', mycostCatIds[j]);
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
				var costCatIdTd = parent.getElementsByClassName('yui-dt-col-costCatId', 'td')[0];
				var costCatIdEl = new YAHOO.util.Element(costCatIdTd);
				var costCatId = costCatIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				costCatIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = mycostCatIds[j];
			}else {//YK:Changes done for Bug#5838
				var cellValue = (column.key != 'item')?'0':'';
				if(checked){
					checked=false;
					oRecord.setData(column.key, cellValue);
					this.onEventShowCellEditor({target:elCell}); 
					return;
				}
				if(formatterFail){
					checked=true;
					oRecord.setData(column.key, cellValue);
					this.onEventShowCellEditor({target:elCell}); 
					return;
				}
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
				var Td = parent.getElementsByClassName('yui-dt-col-'+column.key, 'td')[0];
				var El = new YAHOO.util.Element(Td);
				var oData = oNewData;
				//oData is null if formatter encounters error
				if (oData != null){
					oData = VELOS.subjectGrid.encodeData(oData);
					El.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = oData;
					oRecord.setData(column.key, oNewData);
				}else{
					oData = oOldData;
					oRecord.setData(column.key, oData);
					this.onEventShowCellEditor({target:elCell});
				}
			}
			if (oNewData != null && oOldData != oNewData){
				auditEditedRow(oArgs);
			}
		};
		myDataTable.subscribe("editorSaveEvent", onCellSave);

		// Set up editing flow
		var highlightEditableCell = function(oArgs) {
			var elCell = oArgs.target;
			if(YAHOO.util.Dom.hasClass(elCell, "yui-dt-editable")) {
				this.highlightCell(elCell);
			}
		};
		myDataTable.subscribe("cellMouseoverEvent", highlightEditableCell);
		myDataTable.subscribe("cellMouseoutEvent", myDataTable.onEventUnhighlightCell);
		
		var handleFocus = function (target){
			var oColumn = myDataTable.getColumn(target);
			var key = oColumn.key;
			var oRecord = myDataTable.getRecord(target);
			var itemSeq=oRecord.getData('itemSeq');
			var focusMe;
			if(!target.disabled){
				if (key.match(/^v[0-9]+$/)){
					//focus on the checkbox
					var targetEl = new YAHOO.util.Element(target);
					var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
					var parentDivEl = new YAHOO.util.Element(parentDiv);
					var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
					focusMe = chkBox;
					
				}
				if (key == 'checkItem'){
					//focus on the checkbox
					var chkBox = document.getElementsByName('all_e'+itemSeq)[0];
					focusMe = chkBox;
				}
				if (key == 'delete'){
					//focus on the image
					var targetEl = new YAHOO.util.Element(target);
					var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
					var parentDivEl = new YAHOO.util.Element(parentDiv);
					var childDiv = parentDivEl.getElementsByTagName('div')[0];
					var childDivEl = new YAHOO.util.Element(childDiv);
					var image = childDivEl.getElementsByTagName('input')[0];
					focusMe = image;					
				}
			}
			if (focusMe && !focusMe.disabled){
				myDataTable.saveCellEditor();
				target.onkeydown = function(e) {
					e = e || window.event;
					var charCode = e.keyCode || e.which;
					switch (charCode) {
					case 9:
						YAHOO.util.Event.stopEvent(e);
						if (e.shiftKey) {
							editPrevious(target);
						} else {
							editNext(target);
						}
						break;
					}
				}
				focusMe.tabindex = -1;
				focusMe.keydown = target.keydown;
				focusMe.focus();
			}
			return;
		}
		
		var editNext = function(cell) {
			cell = myDataTable.getNextTdEl(cell);
			while (cell && !myDataTable.getColumn(cell).editor) {
				cell = myDataTable.getNextTdEl(cell);
			}
			if (cell) {
				var oRecord = myDataTable.getRecord(cell);
				var returnVal = VELOS.subjectGrid.enableDisableChildColumns(myDataTable.getColumn(cell));
				if (returnVal < 1) {
					editNext(cell);
				}else{
					if(formatterFail) return;//SM:fix #5835
					var oColumn = myDataTable.getColumn(cell);
					var justFocus = (oColumn.madeUp==true
							|| oColumn.editor._sType=='checkbox')?true:false;
					if (justFocus){
						handleFocus(cell);
					}else {
						YAHOO.util.UserAction.click(cell);
					}
				}
			}
		};
		var editPrevious = function(cell) {
			cell = myDataTable.getPreviousTdEl(cell);
			while (cell && !myDataTable.getColumn(cell).editor) {
				cell = myDataTable.getPreviousTdEl(cell);
			}
			if (cell) {
				var oRecord = myDataTable.getRecord(cell);
				var returnVal = VELOS.subjectGrid.enableDisableChildColumns(myDataTable.getColumn(cell));
				if (returnVal < 1) {
					editPrevious(cell);
				}else{
					if(formatterFail) return;//SM:fix #5835
					var oColumn = myDataTable.getColumn(cell);
					var justFocus = (oColumn.madeUp==true
							|| oColumn.editor._sType=='checkbox')?true:false;
					if (justFocus){
						handleFocus(cell);
					}else {
						YAHOO.util.UserAction.click(cell);
					}
				}
			}
		};

		// Start of TAB key implementation Bug#5798 Date:05-June-2012 Ankit
		myDataTable.subscribe("editorKeydownEvent",function(oArgs) {
			var ed = this._oCellEditor;
			var ev = oArgs.event;
			var KEY = YAHOO.util.KeyListener.KEY;
			var Textbox = YAHOO.widget.TextboxCellEditor;
			var Textarea = YAHOO.widget.TextareaCellEditor;
			var cell = ed.getTdEl();
			var oColumn = ed.getColumn();
			var row,rec;
			
			//Bug #10465 : AGodara
			if (ed){
				if(ed instanceof YAHOO.widget.DropdownCellEditor && isIe == true){
					switch (ev.keyCode) {
					case KEY.TAB:
						YAHOO.util.Event.stopEvent(ev);
						ed.save();
						if (ev.shiftKey) {
							editPrevious(cell);
						} else {
							editNext(cell);
						}
						break;
					case KEY.ENTER:
						YAHOO.util.Event.stopEvent(ev);
						ed.save();
						break;
					default :
						saveDropDown=true;
						break;
					}						
				}else{
					switch (ev.keyCode) {
					case KEY.TAB:
						YAHOO.util.Event.stopEvent(ev);
						ed.save();
						if (ev.shiftKey) {
							editPrevious(cell);
						} else {
							editNext(cell);
						}
						break;
					}
					
				}
			}
		});// Close of TAB key implementation
			
			
		VELOS.subjectGrid.enableDisableChildColumns = function (oColumn){
			var key = oColumn.key;
			if (key == 'itemId'){
				return 0;
			}
			return 1;
		};	
			
		// Audit Edited Row
		var auditEditedRow = function(oArgs) {
			var elCell = oArgs.editor.getTdEl();
			var oOldData = oArgs.oldData;
			var oNewData = oArgs.newData;
			
			//var el = new YAHOO.util.Element(elCell);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
			var itemSeqTd = parent.getElementsByClassName('yui-dt-col-itemSeq', 'td')[0];
			var itemSeqEl = new YAHOO.util.Element(itemSeqTd);
			var itemSeq = itemSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;

			var itemTd = parent.getElementsByClassName('yui-dt-col-item', 'td')[0];
			var itemEl = new YAHOO.util.Element(itemTd);
			var itemName = itemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			
			if (oOldData != oNewData){
				VELOS.subjectGrid.auditAction(itemSeq, 'Updated', itemName);
			}
			//set global array

			if(!formatterFail){
				if (itemLabelArray['e'+itemSeq] == undefined){
					if (itemName!= undefined && itemName!=null && itemName!=''){
						itemLabelArray['e'+itemSeq] = itemName;
					}
				}else{
					itemLabelArray['e'+itemSeq] = itemName;
				}
			}
		};

		myDataTable.subscribe("editorBlurEvent", myDataTable.onEventSaveCellEditor); 

		//addRowsClick was defined here
		//var btn = new YAHOO.widget.Button("addRows");
		//btn.on("click", addRowsClick,this,true);

		if (!$('save_changes')) {
			var dt = $(this.dataTable);
			var saveDiv = document.createElement('div');
			saveDiv.innerHTML = "<br/><table border=0 width='100%'><tbody><tr align='left'>" +
				"<td width='350'></td>"+
				"<td align='left'><input onclick='VELOS.subjectGrid.saveDialog();' type='button' id='save_changes' name='save_changes' value='"+Preview_AndSave/*Preview and Save*****/+"'/></td>"+
				"</td></tr></tbody></table>";
			$D.insertAfter(saveDiv, dt);

		}

		calStatus = respJ.calStatus;
		var cells = null;
		if (calStatus == 'O') {
			var offlineArray = [];
			var offlineKeys = respJ.offlineKeys;
			var offlineValues = respJ.offlineValues;
			if (offlineKeys && offlineValues) {
				for (var iX = 0; iX < offlineKeys.length; iX++) {
					offlineArray[offlineKeys[iX].key] = offlineValues[iX].value
				}
			}
			cells = $D.getElementsByClassName('yui-dt-checkbox', 'input', myDataTable.getTableEl());
			for (var iX = 0; iX < cells.length; iX++) {
				if (!cells[iX].checked) { continue; }
				var el = new YAHOO.util.Element(cells[iX]);
				var myTd = new YAHOO.util.Element($D.getAncestorByTagName(el, 'td'));
				var vId = myTd.get('headers').slice(myTd.get('headers').indexOf('v'));
				vId = YAHOO.lang.trim(vId);
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
				var itemSeqTd = parent.getElementsByClassName('yui-dt-col-itemSeq', 'td')[0];
				var itemSeqEl = new YAHOO.util.Element(itemSeqTd);
				var itemSeq = itemSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				var myKey = 'e'+itemSeq+vId;
				if (!offlineArray[myKey]) { continue; }
				if (offlineArray[myKey]=='1'||offlineArray[myKey]=='2') { cells[iX].disabled = true; }
			}
		}
		//JM: 23MAR2011: 5938: As 'R' is a common status like 'W', we needed to free the 'R'
		//if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F' || calStatus == 'R') {
		if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F') {
			cells = $D.getElementsByClassName('yui-dt-checkbox', 'input', myDataTable.getTableEl());
			for (var iX = 0; iX < cells.length; iX++) {
				cells[iX].disabled = true;
			}
			if ($('save_changes')) { $('save_changes').disabled = true; }
		}

		hidePanel();
	};
	this.handleFailure = function(o) {
		alert(CommErr_CnctVelos);/*alert('Communication Error. Please contact Velos Support');******/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}

VELOS.subjectGrid.updateSCIChangeList = function(myDataTable){
	var arrRecords = myDataTable.getRecordSet();
	scItemChangeList = [];

	for(rec=0; rec < arrRecords.getLength(); rec++){
		var oRecord = arrRecords.getRecord(rec);
		var seq = oRecord.getData('itemSeq');

		if (purgedItemLabelArray['e'+seq] == undefined){
			//Item is not purged
			//alert(YAHOO.lang.dump(oRecord.getData()));
			/*var dataJSON = {
				"itemSeq":seq,
				"itemId":oRecord.getData('itemId'),
				"itemName":oRecord.getData('item'),
				"costCatId":oRecord.getData('costCatId'),
				"costCat":oRecord.getData('costCat'),
				"cost":oRecord.getData('cost'),
				"units":oRecord.getData('units')
			};
			scItemChangeList['itemSeq'+seq] = YAHOO.lang.dump(dataJSON);
			*/
			var data = '|'+seq+'|'+oRecord.getData('itemId')
			+'|'+oRecord.getData('item')
			+'|'+oRecord.getData('costCatId')
			+'|'+oRecord.getData('costCat');
			if (oRecord.getData('cost') != null)
				data +='|'+oRecord.getData('cost');
			else data +='|';
			if (oRecord.getData('units') != null)
				data +='|'+oRecord.getData('units');
			else data +='|';
			data +='|';
			scItemChangeList['itemSeq'+seq] = data;
		}
	}
}

VELOS.subjectGrid.auditAction = function(itemSeq, action, itemName){
	var change=subjGridChangeList['itemSeq'+itemSeq];
	var prevAct ='';

	if(change == undefined){
		if(action=='Updated'){
			var paramArray = [itemName];
			subjGridChangeList['itemSeq'+itemSeq] = getLocalizedMessageString("L_Updt",paramArray)/*'Updated ['+itemName+']'*****/;
		}else{
			var paramArray = [itemName];
			subjGridChangeList['itemSeq'+itemSeq] = getLocalizedMessageString("L_Deleted_Bk",paramArray)/*'Deleted ['+itemName+']'*****/;
		}
	}else{
		var pos =change.indexOf(' ');
		if (pos>0){
			prevAct = change.slice(0,pos);
		}else{
			prevAct = change;
		}

		if(prevAct=='Added'){
			if(action!='Updated'){
				var paramArray = [action,itemName];
				subjGridChangeList['itemSeq'+itemSeq] = getLocalizedMessageString("L_Aded",paramArray)/*'Added/'+ action + ' ['+itemName+']'*****/;
			}
		}else if(prevAct=='Added/Updated'){
			if(action=='Updated'){
				var paramArray = [itemName];
				subjGridChangeList['itemSeq'+itemSeq] = getLocalizedMessageString("L_AddUpdt",paramArray)/*'Added/Updated ['+itemName+']'*****/;
			}else{
				var paramArray = [itemName];
				subjGridChangeList['itemSeq'+itemSeq] = getLocalizedMessageString("L_AddUpdtDel",paramArray)/*'Added/Updated/Deleted ['+itemName+']'*****/;
			}
		}else if(prevAct=='Deleted'){
			//This case is not valid
		}else if(prevAct=='Updated'){
			var paramArray = [itemName];
			subjGridChangeList['itemSeq'+itemSeq] = getLocalizedMessageString("L_Updt",paramArray)/*'Updated ['+itemName+']'*****/;
		}
	}
}

//Apply attributes to the newly created rows
//Includes setting innetHTML
VELOS.subjectGrid.applyRowAttribs = function(myDataTable, rCount) {
	var tabLength= myDataTable.getRecordSet().getLength();

	for (var iX=(tabLength-rCount); iX<tabLength; iX++) {
		var delButtonCells= $D.getElementsByClassName('yui-dt-col-delete', 'td');
		var el = new YAHOO.util.Element(delButtonCells[iX]);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var parentId = parent.get('id');

		var itemSeqTd = parent.getElementsByClassName('yui-dt-col-itemSeq', 'td')[0];
		var itemSeqEl = new YAHOO.util.Element(itemSeqTd);
		var itemSeq = itemSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		itemSeqArray.push(itemSeq);
		itemIdArray.push(0);
		itemLabelArray.push('');
		
		var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
		var delButtonEl = new YAHOO.util.Element(delButtonTd);
		var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		//delButtonArray.push(delButton);
		delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
		= '<div align="center"><input type="image" src="../images/delete.png" title="'+Delete_Row+'" alt="'+Delete_Row/*Delete Row*****/+'" border="0"'
		+ ' name="delete_e'+itemSeq+'" id="delete_e'+itemSeq+'" /></div> ';

		var checkItemTd = parent.getElementsByClassName('yui-dt-col-checkItem', 'td')[0];
		var checkItemEl = new YAHOO.util.Element(checkItemTd);
		var checkItem = checkItemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		checkItemArray.push(checkItem);
		
		var parentId = parent.get('id');
		var itemId =0;
		var iHTML = '<input type="checkbox"'
		+ ' name="all_e'+itemSeq+'" id="all_e'+itemSeq+'"'
		+ ' onclick="VELOS.subjectGrid.itemAll(\''+parentId+'\','+itemSeq+','+itemId+');" /> ';
		checkItemEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=iHTML;
	}
}


VELOS.subjectGrid.types = {
	'varchar':{
		editor:'textbox',
		editorOptions:{disableBtns:true}
	},
	'date': {
		parser:'date',
		formatter:'date',
		editor:'date',
		// I don't want the calendar to have the Ok-Cancel buttons below
		editorOptions:{
			disableBtns:true,
			validator: YAHOO.widget.DataTable.validateDate
		},
		// I want to send the dates to my server in the same format that it sends it to me
		// so I use this stringer to convert data on the way from the client to the server
		stringer: function (date) {
			return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() +
				' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
		}
	},
	integer: {
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}$'
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	number: {
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}([.,]\\d{0,2})?$'
			//regExp:'^\\d{11}$',
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	currency: {
		parser:'number',
		formatter:'currency',
		editor:'regexp',
		// for currency I accept numbers with up to two decimals, with dot or comma as a separator
		// It won't accept currency signs and thousands separators really messes things up
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d*([.,]\\d{0,2})?$',
			validator: function (value) {
				return parseFloat(value.replace(',','.'));
			}
		},
		// When I pop up the cell editor, if I don't change the dots into commas, the whole thing will look inconsistent.
		cellEditorFormatter: function(value) {
			return ('' + value).replace('.',',');
		},
		disableBtns:true
	},
	// This is the default, do-nothing data type.
	// There is no need to parse, format or stringify anything and if an editor is needed, the textbox editor will have to do.
	string: {
		editor:'textbox'
	}
};

VELOS.subjectGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
				alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {/*if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {*****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}
VELOS.subjectGrid.visitAll = function(vId) {
	var NewRight = f_check_perm_noAlert(pgRight,'N');
	var EditRight = f_check_perm_noAlert(pgRight,'E');
	
	var selColEls = $D.getElementsByClassName('yui-dt-col-'+vId, 'td');
	for (var iX=0; iX<selColEls.length; iX++) {
		var el = new YAHOO.util.Element(selColEls[iX]);		
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var itemIdTd = parent.getElementsByClassName('yui-dt-col-itemId', 'td')[0];
		var itemIdEl = new YAHOO.util.Element(itemIdTd);
		var itemId = itemIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		
		if (itemId == 0){ 
			if(!NewRight)	continue;
		}else{
			if(!EditRight)	continue;
		}
		var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (input.disabled) { continue; }
		
		var itemSeqTd = parent.getElementsByClassName('yui-dt-col-itemSeq', 'td')[0];
		var itemSeqEl = new YAHOO.util.Element(itemSeqTd);
		var itemSeq = itemSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		var oKey = 'e'+itemSeq+vId;
		if (input.checked != document.getElementById('all_'+vId).checked) {
			// itemLabelArray['e'+itemId] = itemName;
			if (subjGridChangeList[oKey] == undefined) {
				subjGridChangeList[oKey] = document.getElementById('all_'+vId).checked;
			} else {
				subjGridChangeList[oKey] = undefined;
			}
			input.checked = document.getElementById('all_'+vId).checked;
		}
	}
}

VELOS.subjectGrid.itemAll = function(rId, eSeq, itemId) {
	if (itemId == 0){ 
		if(!f_check_perm(pgRight,'N')){
			return;
		}
	}else{
		if(!f_check_perm(pgRight,'E')){
			return;
		}
	}
	var cells = $D.getChildren($(rId));
	for (var iX=0; iX<cells.length; iX++) {
		if (!cells[iX].className) { continue; }
		var pattern = /\byui-dt-col-v[0-9]+\b/;
		var cName = cells[iX].className.match(pattern);
		if (!cName) { continue; }
		var vId = (''+cName).slice(11); // To skip 'yui-dt-col-'
		var el = new YAHOO.util.Element(cells[iX]);
		var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (input.disabled) { continue; }
		if (input.checked != $('all_e'+eSeq).checked) {
			var oKey = 'e'+eSeq+vId;
			if (subjGridChangeList[oKey] == undefined) {
				subjGridChangeList[oKey] = $('all_e'+eSeq).checked;
			} else {
				subjGridChangeList[oKey] = undefined;
			}
			input.checked = $('all_e'+eSeq).checked;
		}
	}
}
VELOS.subjectGrid.validate = function() {
	var flag_invalid=0;
	var itemLabel='';

	for(var i=1; i<=subjectCostItemCount; i++){
		itemLabel=itemLabelArray['e'+i];
		//alert(i+' Item: '+itemLabel + ' Am I purged? ' + purgedItemLabelArray['e'+i]);

		if (purgedItemLabelArray['e'+i] == undefined){
			//Item is not purged
			if (itemLabel == undefined || itemLabel.length == 0){
				flag_invalid=i;
				break;
			}
		}/*else{
		purgedItemLabelArray['e'+i] != undefined
		means the item is purged in this session. Hence no need to process it.
		It won't be saved to the database even though item name is null
		}*/
	}
	return flag_invalid;
}
VELOS.subjectGrid.addRows = function() {
	var firstCostCatId= mycostCatIds[0];
	var firstCostCat= mycostCats[0];
	var count = eval(YAHOO.util.Dom.get("rowCount").value);

	if(YAHOO.lang.isNumber(count)) {
		var myArray = [];
		for(var i=0;i<count;i++) {
			subjectCostItemCount++;
			var dataJSON = {"itemSeq":subjectCostItemCount,"delete":"","item":"","costCatId":firstCostCatId,"costCat":firstCostCat,
					"itemId":0,"cost":0,"units":0};

			var record = YAHOO.widget.DataTable._cloneObject(dataJSON);
			record.row = i;
			myArray.push(record);
			subjGridChangeList['itemSeq'+subjectCostItemCount] = 'Added';
		}
		myDataTable.addRows(myArray);
		if (count>0) {
			VELOS.subjectGrid.applyRowAttribs(myDataTable,count);
		}
		YAHOO.util.Dom.get("rowCount").value = "0";
		return;
	}
	YAHOO.log(CldNtCont_InvldIndx);/*YAHOO.log("Could not continue due to invalid index.");*****/
	YAHOO.util.Dom.get("rowCount").value = "0";
}

VELOS.subjectGrid.saveDialog = function(e) {
	myDataTable.saveCellEditor();

	if (formatterFail) return;

	var row =VELOS.subjectGrid.validate();
	if (row > 0){
		if(e) YAHOO.util.Event.stopEvent(e);
		var paramArray = [row];
		alert(getLocalizedMessageString("M_EtrCstName_Row",paramArray));/*alert("Enter 'Cost Item Name' for Row #"+row+".");*****/
		var columnCells = $D.getElementsByClassName('yui-dt-col-item', 'td', myDataTable.getTableEl());
		for (var iX=0; iX<columnCells.length; iX++) {
			if(row == iX+1){
				var el = new YAHOO.util.Element(columnCells[iX]);
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
				var elCell = parent.getElementsByClassName('yui-dt-col-item', 'td')[0];
				myDataTable.showCellEditor(elCell);				
				return false;
			}
		}
		return;
	}

	VELOS.subjectGrid.updateSCIChangeList(myDataTable);

	var saveDialog = document.createElement('div');
	if (!$('saveDialog')) {
		saveDialog.setAttribute('id', 'saveDialog');
		saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CnfrmVst_ItemChg/*Confirm Visit/Item Changes*****/+'</div>';
		saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
		$D.insertAfter(saveDialog, $('save_changes'));
	}

	var noChange = true;
	var numChanges = 0;
	var maxChangesDisplayedBeforeScroll = 25;
	$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="updateSCIVisits.jsp?'+incomingUrlParams+'"><div id="changeRowsContent"></div><br/><div id="insertFormContent"></div></form>';
	for (oKey in subjGridChangeList) {
		if (oKey.match(/^e[0-9]+v[0-9]+$/) && subjGridChangeList[oKey] != undefined) {
			noChange = false;
			var visitId = oKey.slice(oKey.indexOf('v'));
			var itemSeq = oKey.slice(0, oKey.indexOf('v'));

			if (purgedItemLabelArray[itemSeq] == undefined){
				//Item is not purged so create input
				var visitName = visitLabelArray[visitId];
				var itemName = itemLabelArray[itemSeq];

				$('insertFormContent').innerHTML += visitName+' / '+itemName+' => '
					+(subjGridChangeList[oKey]?varSelc/*"<b>S</b>elected"*****/:Unselc)/*"<b>U</b>nselected")*****/+'<br/>';
				numChanges++;

				var opVal = (subjGridChangeList[oKey]?'A':'D')+'|'+oKey+'|'+
				visitDisplacementArray[oKey.slice(oKey.indexOf('v'))]+'|'+itemName;
				$('insertFormContent').innerHTML +=
					'<input type="hidden" name="ops" value="'+opVal+'" />';
			}
		}else{
			if (oKey.match(/^itemSeq[0-9]+$/) && subjGridChangeList[oKey] != undefined) {
				noChange = false;
				var itemSeq = oKey.slice(oKey.indexOf('itemSeq')+7);
				var paramArray = [itemSeq,subjGridChangeList[oKey]];
				$('changeRowsContent').innerHTML += getLocalizedMessageString("L_Row_EqGt",paramArray)/*'Row #'+itemSeq+' => '+ subjGridChangeList[oKey]****/+'<br/>';
				numChanges++;
			}
		}
	}
	//$('insertFormContent').innerHTML += '<BR/>';
	for (oItem in scItemChangeList) {
		if (oItem.match(/^itemSeq[0-9]+$/) && scItemChangeList[oItem] != undefined) {
			var itemSeq = oItem.slice(oItem.indexOf('itemSeq')+7);
			var itemVal = scItemChangeList[oItem];
			itemVal = VELOS.subjectGrid.encodeData(itemVal);
			$('insertFormContent').innerHTML += '<input type="hidden" name="items" value="'+itemVal+'" />';
		}
	}
	//$('insertFormContent').innerHTML += '<BR/>';
	for (oItem in scItemPurgeList) {
		if (oItem.match(/^itemSeq[0-9]+$/) && scItemPurgeList[oItem] != undefined) {
			var itemSeq = oItem.slice(oItem.indexOf('itemSeq')+7);
			var itemVal = scItemPurgeList[oItem];
			itemVal = VELOS.subjectGrid.encodeData(itemVal);
			$('insertFormContent').innerHTML += '<input type="hidden" name="purgedItems" value="'+itemVal+'" />';
		}
	}

	if (numChanges < maxChangesDisplayedBeforeScroll) {
		$('insertForm').style.width = null;
		$('insertForm').style.height = null;
		$('insertForm').style.overflow = 'visible';
	} else {
		$('insertForm').style.width = '51em';
		$('insertForm').style.height = '35em';
		$('insertForm').style.overflow = 'scroll';
	}
	if (noChange) {
		$('insertFormContent').innerHTML += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
		noChange = true;
	} else {
		$('insertFormContent').innerHTML += Total_Chg/*Total changes*****/+': '+numChanges+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
			'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
			'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
			' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
			' onkeypress="return VELOS.subjectGrid.fnOnceEnterKeyPress(event)" '+
			' />&nbsp;</td></tr></table>';
		$('insertFormContent').innerHTML += '<input id="changeCount" name="changeCount" type="hidden" value="'+numChanges+'"/>';
	}

	myDialog = new YAHOO.widget.Dialog('saveDialog',
			{
				visible:false, fixedcenter:true, modal:true, resizeable:true,
				draggable:"true", autofillheight:"body", constraintoviewport:false
			});
	var handleCancel = function(e) {
		myDialog.cancel();
	};
	var handleSubmit = function(e) {
		try {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
				alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
			//Changes By Sudhir On 3/30/2012 for Bug # 5796
			if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {/*if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {*****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
			//Changes By Sudhir On 3/30/2012 for Bug # 5796
	        var thisTimeSubmitted = new Date();
	        if (lastTimeSubmitted && thisTimeSubmitted) {
	        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
		        lastTimeSubmitted = thisTimeSubmitted;
	        }
		} catch(e) {}
		myDialog.submit();
	};
	var onButtonsReady = function() {
	    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
	    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
	}
	YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
	var myButtons = noChange ?
		[   { text: getLocalizedMessageString("L_Close"), handler: handleCancel } ] :
		[
			{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
			{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
		];
	var onSuccess = function(o) {
		hideTransitPanel();
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			// Log error here
			return;
		}
		if (respJ.result == 0) {
			// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
			showFlashPanel(respJ.resultMsg);
			if (window.parent.reloadSubjectGrid) { setTimeout('window.parent.reloadSubjectGrid();', 1500); }
		} else {
			var paramArray = [respJ.resultMsg];
			alert(getLocalizedMessageString("L_Error_Cl",paramArray));/*alert("Error: " + respJ.resultMsg);*****/
		}
	};
	var onFailure = function(o) {
		hideTransitPanel();
		var paramArray = [o.status];
		alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
	};
	var onStart = function(o) {
		showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
	}
	myDialog.callback.customevents = { onStart:onStart };
	myDialog.callback.success = onSuccess;
	myDialog.callback.failure = onFailure;
	myDialog.cfg.queueProperty("buttons", myButtons);
	myDialog.render(document.body);
	if (!formatterFail){
		myDialog.show();
		if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
	}

	var showFlashPanel = function(msg) {
		// Initialize the temporary Panel to display while waiting for external content to load
		if (!(VELOS.flashPanel)) {
			VELOS.flashPanel =
				new YAHOO.widget.Panel("flashPanel",
					{ width:"240px",
					  fixedcenter:true,
					  close:false,
					  draggable:false,
					  zindex:4,
					  modal:true,
					  visible:false
					}
				);
		}
		VELOS.flashPanel.setHeader("");
		VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.flashPanel.render(document.body);
		VELOS.flashPanel.show();
		setTimeout('VELOS.flashPanel.hide();', 1500);
	}

	var showTransitPanel = function(msg) {
		if (!VELOS.transitPanel) {
			VELOS.transitPanel =
				new YAHOO.widget.Panel("transitPanel",
					{ width:"240px",
					  fixedcenter:true,
					  close:false,
					  draggable:false,
					  zindex:4,
					  modal:true,
					  visible:false
					}
				);
		}
		VELOS.transitPanel.setHeader("");
		VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.transitPanel.render(document.body);
		VELOS.transitPanel.show();
	}

	var hideTransitPanel = function() {
		if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
	}
	
	$j(function() {
		if ($j("input:submit")){
			$j("input:submit").button();
		}
		if ($j("a:submit")){
			$j("a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});
}

VELOS.subjectGrid.decodeData = function(oData) {
	oData = oData.replace(/[&]lt;/g,"<"); //replace less than
	oData = oData.replace(/[&]gt;/g,">"); //replace greater than
	oData = oData.replace(/[&]amp;/g,"&"); //replace &
	return oData;
}

VELOS.subjectGrid.encodeData = function(oData) {
	oData = oData.replace(/[&]/g,"&amp;"); //replace &
	oData = oData.replace(/[<]/g,"&lt;"); //replace less than
	oData = oData.replace(/[>]/g,"&gt;"); //replace greater than
	return oData;
}

VELOS.subjectGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'serverpagination'; }
	if ((args.success) && (typeof args.success == 'function')) {
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.subjectGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.subjectGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.subjectGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait =
			new YAHOO.widget.Panel("wait",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.subjectGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.subjectGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}

