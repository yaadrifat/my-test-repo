var milestoneRowChangeList = [];
var milestoneRowChangeListRemainder = null;
var milestoneDataChangeList = null;
var milePurgeList = [];
var purgedMileArray = null;
var purgedMileArrayRemainder = null;
var milestoneArray = null;
var mileDescArray = null; //milestone description
var incomingUrlParams = null;
var calStatus = null;
var lastTimeSubmitted = 0;
var horizontalPage = 0;
var mileCount = 0;
var firstAMSeq = 0;
var lastAMSeq = 0;

var myPMGrid, myVMGrid, myEMGrid, mySMGrid, myAMGrid;
var myMileTypeJSON;

var myMiletypeIds = [];
var myMiletypes = [];

var myPaytypeIds = [];
var myPaytypes = [];
var myPayforIds = [];
var myPayfors = [];
var myMileStatusIds = [];
var myMileStatuses = [];

var myMileDifStatusIds = [];
var myMileDifStatuses = [];
var mileStatusSubTypes = [];

var depIds = []; 
var depNames =[];

var VMvisitIds = []; 
var VMvisitNames =[];

var myEMruleIds = [];
var myEMrules = [];
var myVMruleIds = [];
var myVMrules = [];
var patStatus = [];
var patStatusIds = [];
var studyStatus = [];
var studyStatusIds = [];
var eventStatus = [];
var eventStatusIds = []; 

var myCalendarIds = [];
var myCalendars = [];
var myCalendarTypes = [];
var myVisitIds = [];
var myVisits = [];
var myEventIds = [];
var myEvents = [];

var myFieldArray = [];
var myColumnDefs = [];

var updateRows = [];
var updateRowsRemainder = null;

var pgRight=0;

var respJSON;
var defaultPayTypeId, defaultPayType;
//APR-13-11,BK ,FIXED #6016
var PMrecordNum,VMrecordNum,EMrecordNum,SMrecordNum,AMrecordNum;
function includeJS(file)
{
  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;
  document.getElementsByTagName('head').item(0).appendChild(script);
}
includeJS('js/velos/velosUtil.js');
var Total_Chg=L_Total_Chg;
var ErrGetResp_CnctVelos=M_ErrGetResp_CnctVelos;
var ValEtrFwg_11DgtPatCnt=M_ValEtrFwg_11DgtPatCnt;
var ValEtrInt_11DgtLessValid=M_ValEtrInt_11DgtLessValid;
var ValEtrFwg_AmtFmt11Dgt=M_ValEtrFwg_AmtFmt11Dgt;
var ValEtrCrit_MstoneDescValid=	M_ValEtrCrit_MstoneDescValid;
var ValEtrCrit_4kCharPlsEtr=M_ValEtrCrit_4kCharPlsEtr;
var Preview_AndSave=L_Preview_AndSave;
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var PlsEnterEsign=M_PlsEnterEsign;
var CldNtCont_InvldIndx=M_CldNtCont_InvldIndx;
var ThereNoChg_Save=M_ThereNoChg_Save;
var Esignature=L_Esignature;
var Valid_Esign=L_Valid_Esign;
var Invalid_Esign=L_Invalid_Esign;
var CnfrmStdLvl_Change=M_CnfrmStdLvl_Change;
var ValEtrFwg_11DgtLessValid=M_ValEtrFwg_11DgtLessValid;
var Pat_StatusMstones=L_Pat_StatusMstones;
var PleaseWait_Dots=L_PleaseWait_Dots;
var LoadingPlsWait=L_LoadingPlsWait;
var Visit_Mstones=L_Visit_Mstones;
var Evt_Mstones=L_Evt_Mstones;
var StdPat_Mstone=L_StdPat_Mstone;
var Addl_Mstones=L_Addl_Mstones;
var CnfrmMstone_Change=M_CnfrmMstone_Change;
var Pat_Count=L_Pat_Count;
var Patient_Status=L_Patient_Status;
var Mstone_Status=L_Mstone_Status;
var Calendar=L_Calendar;
var Visit=L_Visit;
var Event=L_Event;
var Milestone_Rule=L_Milestone_Rule;
var Event_Status=L_Event_Status;
var Study_Status=L_Study_Status;
var Milestone_Description=L_Milestone_Description;
var Additional=L_Additional;
var saveDropDown=true;
// Define VELOS.milestoneGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.milestoneGrid = function (url, args) {	
	pgRight = parseInt(YAHOO.util.Dom.get("pageRight").value);
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updateEvtVisits.jsp
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}

	showPanel(); // Show the wait panel; this will be hidden later

	// Define handleSuccess() of VELOS.milestoneGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {		
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'serverpagination'; // name of the div to hold milestoneGrid
        var pMileTypeId;
        pMileTypeId = args.pMileTypeId;
		var respJ = null; // response in JSON format
		try {	
			respJ = $J.parse(o.responseText);
			respJSON = respJ;
		} catch(e) {
			alert(ErrGetResp_CnctVelos);/*alert('Error getting response from server. Please contact Velos Support.');*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
			alert(getLocalizedMessageString("L_Error_Msg",paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}

		myMileTypeJSON = respJ.mileTypeJSON;
		myMileTypeIds = respJ.milestoneTypeId;
		myMileTypes = respJ.milestoneType;
		
		defaultPayTypeId = respJ.defaultPayTypeId;
		defaultPayType = respJ.defaultPayType;	
		
		myCalendarIds = respJ.studyProtId;
		myCalendars = respJ.studyProtocols;
		myCalendarTypes = respJ.studyProtTypes;
		
		myVisitIds = respJ.studyVisitId;
		myVisits = respJ.studyVisits;
		myEventIds = respJ.studyEventId;
		myEvents = respJ.studyEvents;
		
		myPaytypesIds = respJ.milePayTypeId;
		myPaytypes = respJ.milePayType;
		myPayforIds = respJ.milePayForId;
		myPayfors = respJ.milePayFor;
		myMileStatusIds = respJ.mileStatusId;
		myMileStatuses = respJ.mileStatus;
		
		myMileDifStatusIds = respJ.mileDifStatusId;
		myMileDifStatuses = respJ.mileDifStatus;
		mileStatusSubTypes = respJ.mileStatusSubType;
		
		myEMruleIds = respJ.EM_mileRuleId;
		myEMrules =  respJ.EM_mileRule;
		myVMruleIds = respJ.VM_mileRuleId;
		myVMrules = respJ.VM_mileRule;
		patStatusIds = respJ.patStatusId;
		patStatus = respJ.patStatus;
		studyStatusIds = respJ.studyStatusId;
		studyStatus = respJ.studyStatus;
		eventStatusIds = respJ.eventStatusId;
		eventStatus = respJ.eventStatus;
		
	    mileCount = respJ.mileCount;		

		var maxWidth = 800;
		var maxHeight = 800;
		/*YK(05Aug11): Fix for Bug #6760 to resolve width issue*/
		if (screen.availWidth >= 800 && screen.availWidth < 900) { maxWidth = 800; }
		else if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 900; }
		else if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = 1180; }
		else if (screen.availWidth >= 1290 ) { maxWidth = 1280; }

		if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
		else if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = 480; }

		
		var calcHeight =0; 
		var pmMileIdCells;var amMileIdCells;var vmMileIdCells;var emMileIdCells;var smMileIdCells;
		/*YK 03Aug11:Code removed (reloadMilestoneGrid('ALL')) for  Enhancement FIN-20073, Now not in use*/
		mileTypeId = pMileTypeId;
		if (mileTypeId=='PM'){			
			colArray = respJ.PM_colArray;
			dataArray = respJ.PM_dataArray;
			PMrecordNum = respJ.PMmileCount;
			//alert("PM case:"+recordNum);
		} else if (mileTypeId == 'VM'){
			colArray = respJ.VM_colArray;
			dataArray = respJ.VM_dataArray;
			VMrecordNum = respJ.VMmileCount;
		} else if (mileTypeId == 'EM'){
			colArray = respJ.EM_colArray;
			dataArray = respJ.EM_dataArray;
			EMrecordNum = respJ.EMmileCount;
		} else if (mileTypeId == 'SM'){
			colArray = respJ.SM_colArray;
			dataArray = respJ.SM_dataArray;
			SMrecordNum = respJ.SMmileCount;
		} else if (mileTypeId == 'AM'){
			colArray = respJ.AM_colArray;
			dataArray = respJ.AM_dataArray;
			AMrecordNum = respJ.AMmileCount;
		}
		
		//Flush out all
		// Modified by Parminder Singh 10672#
		//milestoneRowChangeList = [];
		//purgedMileArray = [];
		//updateRows =[];
		
		var otherRegularExp =  new RegExp('mileSeq[0-9]+$');
		for (oKey in purgedMileArrayRemainder){
			if (oKey.match(otherRegularExp) && purgedMileArrayRemainder[oKey] != undefined) {
				purgedMileArray[oKey] = purgedMileArrayRemainder[oKey];
			}
		}
		for (oKey in milestoneRowChangeListRemainder){
			if (oKey.match(otherRegularExp) && milestoneRowChangeListRemainder[oKey] != undefined) {
				milestoneRowChangeList[oKey] = milestoneRowChangeListRemainder[oKey];
			}
		}
		for (oKey in updateRowsRemainder){
			if (oKey.match(otherRegularExp) && updateRowsRemainder[oKey] != undefined) {
				updateRows[oKey] = updateRowsRemainder[oKey];
			}
		}
		
		if (colArray){
		
			calcHeight = dataArray.length*40 + 40;
			if (calcHeight > maxHeight) { calcHeight = maxHeight; }	
			
			myColumnDefs = VELOS.milestoneGrid.processColumns(mileTypeId, respJ, colArray);
			
			var myDataSource = new YAHOO.util.DataSource(dataArray);
			myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			myDataSource.responseSchema = {
				fields: myFieldArray
			};
			var myGrid = new YAHOO.widget.DataTable(
				mileTypeId +'_'+ this.dataTable,
				myColumnDefs, myDataSource,
				{
					width:maxWidth+"px"//, 
					//height:calcHeight+"px",
					//caption:'my'+mileTypeId+'Grid'
					//caption:"DataTable Caption",
					//scrollable:true
				}
			);			
			if (mileTypeId=='PM'){			
				myPMGrid = myGrid;
				pmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());				
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(pmMileIdCells);
				myPMGrid.focus();
			} else if (mileTypeId == 'VM'){
				myVMGrid = myGrid;
				vmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(vmMileIdCells);
				myVMGrid.focus();
			} else if (mileTypeId == 'EM'){
				myEMGrid = myGrid;
				emMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(emMileIdCells);
				myEMGrid.focus();
			} else if (mileTypeId == 'SM'){
				mySMGrid = myGrid;
				smMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(smMileIdCells);
				mySMGrid.focus();
			} else if (mileTypeId == 'AM'){
				myAMGrid = myGrid;
				amMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(amMileIdCells);
				myAMGrid.focus();
			}
	  }

		
	//FIX #6283
	if (YAHOO.util.Dom.get("PM_rowCount")){
		YAHOO.util.Dom.get("PM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("VM_rowCount")){
		YAHOO.util.Dom.get("VM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("EM_rowCount")){
		YAHOO.util.Dom.get("EM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("SM_rowCount")){
		YAHOO.util.Dom.get("SM_rowCount").value="0";
	}
	if (YAHOO.util.Dom.get("AM_rowCount")){
		YAHOO.util.Dom.get("AM_rowCount").value="0";
	}
	
		
		mileIdArray = []; 	mileSeqArray = []; 		checkItemArray = [];
		milestoneArray = [];		
		delButtonArray = []; mileDescArray = [];
		
		if (!purgedMileArray) purgedMileArray = [];
		if (!milestoneRowChangeList) milestoneRowChangeList = [];
		if (!updateRows) updateRows = [];
		
		milestoneRowChangeListRemainder =[]; purgedMileArrayRemainder = [];
		updateRowsRemainder =[];

		var onMouseOver = function(oArgs) {
			this.onEventHighlightRow(oArgs);
		}
		
		var onMouseOut = function(oArgs) {
			this.onEventUnhighlightRow(oArgs);
			return nd();
		}

		// I subscribe to the cellClickEvent to respond to the action icons and, if none, to pop up the cell editor
		var onCellClick = function(oArgs) {
			var target = oArgs.target;
			var column = this.getColumn(target);
			var oRecord = this.getRecord(target);
			var mileSeq = oRecord.getData('mileSeq');
			var mileId = parseInt(oRecord.getData('mileId'));
			var mileTypeId = oRecord.getData('mileTypeId');
			var mileStatusId = oRecord.getData('mileStaticStatusId');
			var mileStatusSubType = mileStatusSubTypes[myMileStatusIds.indexOf(mileStatusId)];

			if (column.key == 'recNum' || column.key == 'mileType' || column.key == 'delete')
				return;
			if (mileTypeId == 'VM' || mileTypeId == 'EM'){
			/*Ankit 16Aug2011 Bug#6788: 'Patient Count' and 'Patient Status' columns are not allowed to edit*/
				if (column.key == 'calendar' || column.key == 'visit' || column.key == 'event' ){
					if (mileId != 0){
						return;
					}
				}
				if (column.key == 'patCount' || column.key == 'patStatus'){
					if (oRecord.getData('calendarType')=='A')
						return;
					if (mileId != 0)
						return;
				}
			}
			switch(column.formatter){
			case 'customInteger':
				column.formatter = function(el, oRecord, oColumn, oData) {
					var mileTypeId = oRecord.getData('mileTypeId');
					var myGrid = (mileTypeId=='PM')? myPMGrid :
						(mileTypeId=='VM')? myVMGrid :
						(mileTypeId=='EM')? myEMGrid :
						(mileTypeId=='SM')? mySMGrid : myAMGrid;
					//precautionary change
					if (oData != '0'){
						if (oData != null && oData != ''){
							if (/^[0]+/g.test(oData)){//FIX #6272
								oData = oData.replace(/^[0]+/g,""); //remove leading zeros
							}
						}
					}
					//FIX #6048
					if ((oColumn.key == 'patCount' || oColumn.key == 'limit')
						&& (oData == null || oData == '')){
						if (oColumn.key == 'patCount')
							oData = -1;
						if (oColumn.key == 'limit')
							oData = '';
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = oData;
						}
						el.innerHTML = oData;
						oRecord.setData(oColumn.key, oData);
						return;
					}
					if(oData == ''){
						if(oColumn.key != 'limit')
								oData ='0'; //If empty set 0
					}
					if (oData == '0'){							
						el.innerHTML = oData;
						oRecord.setData.oColumn= el.innerHTML;
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = oData;
						}
					} else {
						switch(oColumn.key){
							case 'patCount':
								if (/^-{0,1}\d{0,10}$/.test(oData) && oData >= -1){
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									alert(ValEtrFwg_11DgtPatCnt);/*alert("The value entered does not pass the following criteria:\n \n -'Patient Count' should be an integer.\n -'Patient Count' should be less than 11 digits.\n -'Patient Count' should not be less than -1.\n \nPlease enter a valid value.");*****/
								}
								break;
							case 'limit':
								if (/^\d{0,10}$/.test(oData)){
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									alert(ValEtrFwg_11DgtLessValid);/*alert("The value entered does not pass the following criteria:\n \n -'Limit' should be an integer.\n -'Limit' should be less than 11 digits.\n -'Limit' should not be less than 0.\n \nPlease enter a valid value.");*****/
								}
								break;
							default:
								if (/^\d{0,11}$/.test(oData)){
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, oData);
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = oData;
									}
								}else{
									if (myGrid && myGrid._oCellEditor){
										myGrid._oCellEditor.value = null;
									}
									alert(ValEtrInt_11DgtLessValid);/*alert("The value entered does not pass the following criteria:\n \n -'' should be an integer.\n -'' should be less than 11 digits.\n -'' should not be less than -1.\n \nPlease enter a valid value.");*****/
								}
								break;
						}
					}
				};
				if (mileId == 0){
					if (f_check_perm(pgRight,'N')){
						this.onEventShowCellEditor(oArgs);
					}
				}else{
					//FIX #6329
					if (f_check_perm(pgRight,'E') && (mileStatusSubType != 'A' && mileStatusSubType != 'IA')){
						this.onEventShowCellEditor(oArgs);
					}
				}
				return;
				break;
			case 'customNumber':
				column.formatter = function(el, oRecord, oColumn, oData) {									
					var mileTypeId = oRecord.getData('mileTypeId');
					var myGrid = (mileTypeId=='PM')? myPMGrid :
						(mileTypeId=='VM')? myVMGrid :
						(mileTypeId=='EM')? myEMGrid :
						(mileTypeId=='SM')? mySMGrid : myAMGrid;
					
					if (oData != '0'){
						if (oData != '' || oData != null){
							if (/^[0]+/g.test(oData)){//FIX #6272
								oData = oData.replace(/^[0]+/g,""); //remove leading zeros
							}
						}
					}
					if (/^\.\d{0,2}?$/.test(oData)){ // If no zero before decimal add one
						oData = "0" + oData;
					}

					if(oData == '') {
						oData ='0'; //If empty set 0
						//alert("oData empty");
					}
					if (oData == '0'){					
						el.innerHTML = oData;
						oRecord.setData(oColumn.key, oData);
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = oData;
						}
					} else {
						if (/^\d{0,11}([.]\d{0,2})?$/.test(oData)){
							el.innerHTML = oData;
							oRecord.setData(oColumn.key, oData);
							if (myGrid && myGrid._oCellEditor){
								myGrid._oCellEditor.value = oData;
							}
						}else{
							if (myGrid && myGrid._oCellEditor){
								myGrid._oCellEditor.value = null;
							}
							alert(ValEtrFwg_AmtFmt11Dgt);/*alert("The value entered does not pass the following criteria:\n \n -'Amount' should be a number.\n -'Amount' should conform to format - ###########.## \n    i.e. max 11 digits followed by max 2 decimal places.\n \nPlease enter a valid value.");*****/
						}
					}
				};
				if (mileId == 0){
					if (f_check_perm(pgRight,'N')){
						this.onEventShowCellEditor(oArgs);
					}
				}else{
					//FIX #6329
					if (f_check_perm(pgRight,'E') && (mileStatusSubType != 'A' && mileStatusSubType != 'IA')){
						this.onEventShowCellEditor(oArgs);
					}
				}
				return;
				break;
			case 'customVarchar':
				column.formatter = function(el, oRecord, oColumn, oData) {
					var mileTypeId = oRecord.getData('mileTypeId');
					var myGrid = (mileTypeId=='PM')? myPMGrid :
						(mileTypeId=='VM')? myVMGrid :
						(mileTypeId=='EM')? myEMGrid :
						(mileTypeId=='SM')? mySMGrid : myAMGrid;
					if (/['"|]+/g.test(oData)){							
						//this._oCellEditor.value = null;
						alert(ValEtrCrit_MstoneDescValid);/*alert("The value entered does not pass the following criterion:\n \n - 'Milestone Description' should not contain following special characters: pipe(|), single quote('), double quote(\").\n \nPlease enter a valid value.");*****/
						return;
					}
					
					if (oData.length < 4001){							
						el.innerHTML = oData;
						oRecord.setData(oColumn.key, VELOS.milestoneGrid.decodeData(oData));							
						
						return;
					}else{	
						if (myGrid && myGrid._oCellEditor){
							myGrid._oCellEditor.value = null;
						}
						alert(ValEtrCrit_4kCharPlsEtr);/*alert("The value entered does not pass the following criterion:\n \n -'Milestone Description' length should not be greater than 4000 characters.\n \nPlease enter a valid value.");*****/
						return;
					}
					
				};
				//FIX #6329
				if (mileId == 0 || (f_check_perm(pgRight,'E') && (mileStatusSubType != 'A' && mileStatusSubType != 'IA'))){
					this.onEventShowCellEditor(oArgs);
				}
				return;
				break;
			default:
				//for checkItem access rights are checked in itemAll()
				if (mileId == 0){ //New Record
					if (f_check_perm(pgRight,'N')){
						if (column.key != 'mileType'){
							if (column.key == 'mileStatus'){
								column.editor.dropdownOptions = myMileStatuses;
							}
							if (column.key == 'visit'){	
								VELOS.milestoneGrid.updateDependent(column,oRecord,'calendarId','c',respJ);	
							}
							else if (column.key == 'event'){									
								VELOS.milestoneGrid.updateDependent(column,oRecord,'visitId','v',respJ);
							}
							else if (column.key == 'evtStatus'){	
								 var temp = oRecord.getData('mileRule');
								  if(temp == 'On Scheduled Date' ){									 								
									  //APR-13-11,BK ,FIXED #6017
									  return;
								  }
								  else{									  
									  column.editor.dropdownOptions =  respJ.eventStatus;
								  }								 
							}
							// If no action is given, I try to edit it
							//FIX #6033, #6275
							if (column.editor instanceof YAHOO.widget.DropdownCellEditor){
								if (column.editor.dropdownOptions != null){
									column.editor.render();
									this.onEventShowCellEditor(oArgs);
								}
							}else{
								if (column.editor instanceof YAHOO.widget.TextboxCellEditor){
									this.onEventShowCellEditor(oArgs);
								}
							}
							return;
						}						
					}
				}else{//Existing Record
					if (f_check_perm(pgRight,'E')){
						if (column.key == 'mileStatus'){
							var status = oRecord.getData('mileStaticStatusId');
							var statusIndex = myMileStatusIds.indexOf(status);
							var mileAchievedCount = oRecord.getData('mileAchievedCount');
							///write milestatus logic
							if( (statusIndex == mileStatusSubTypes.indexOf('A') && mileAchievedCount != '0' )||statusIndex == mileStatusSubTypes.indexOf('IA')){
								column.editor.dropdownOptions = myMileDifStatuses;
							}
							else{	
								column.editor.dropdownOptions = myMileStatuses;
							}
							column.editor.render();
							this.onEventShowCellEditor(oArgs);	
							return;
						}
						if (column.key != 'mileType'){
							//FIX #6329
							if (mileStatusSubType != 'A' && mileStatusSubType != 'IA'){								
								if (column.key == 'visit'){						
									VELOS.milestoneGrid.updateDependent(column,oRecord,'calendarId','c',respJ);	
								}
								if (column.key == 'event'){								
									VELOS.milestoneGrid.updateDependent(column,oRecord,'visitId','v',respJ);						
								}
								if (column.key == 'evtStatus'){	
									 var temp = oRecord.getData('mileRule');
									  if(temp == 'On Scheduled Date' ){									 								
										  column.editor.dropdownOptions =  [''];
										  column.editor.render(); 
										  return;
									  }
									  else{									  
										  column.editor.dropdownOptions =  respJ.eventStatus;
									  }
								}
								//FIX #6033, #6275
								if (column.editor instanceof YAHOO.widget.DropdownCellEditor){
									if (column.editor.dropdownOptions != null){
										column.editor.render();
										this.onEventShowCellEditor(oArgs);
									}
								}else{
									if (column.editor instanceof YAHOO.widget.TextboxCellEditor){
										this.onEventShowCellEditor(oArgs);
									}
								}
								return;
							} else {
								if (column.key == 'payFor'){
									if (column.editor.dropdownOptions != null){
										column.editor.render();
										this.onEventShowCellEditor(oArgs);
									}
									return;	
								}
							}
						}
					}
				}
			}
		};
		
		
		var onCellSave = function(oArgs) {			
			var elCell = oArgs.editor.getTdEl();
			var oOldData = oArgs.oldData;
			var oNewData = oArgs.newData;			
			var oRecord = this.getRecord(elCell);			
			var column =  this.getColumn(elCell);
			var mileSeq = oRecord.getData('mileSeq');
			var recNum = oRecord.getData('recNum');
			
			//Bug #10465 : AGodara
			if(saveDropDown==false){
				oRecord.setData(column.key, oArgs.oNewData);
				this.onEventShowCellEditor({target:elCell}); 
				saveDropDown=true;
				return false;
			}
			
			mileTypeId = oRecord.getData('mileTypeId');
			if (column.key == 'payType'){					
				VELOS.milestoneGrid.setDropDownId(this,elCell,myPaytypesIds,myPaytypes);
			}
			else if(column.key == 'payFor'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,myPayforIds,myPayfors);
			}
			else if(column.key == 'mileStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,myMileStatusIds,myMileStatuses);
			}
			else if(column.key == 'patStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,patStatusIds,patStatus);
			}
			else if(column.key == 'stdStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,studyStatusIds,studyStatus);
			}
			else if(column.key == 'evtStatus'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,eventStatusIds,eventStatus);
			}	
			else if (column.key  == 'mileRule'){
				if (mileTypeId == 'EM')
					VELOS.milestoneGrid.setDropDownId(this,elCell,myEMruleIds,myEMrules);
				
				if (mileTypeId == 'VM')
					VELOS.milestoneGrid.setDropDownId(this,elCell,myVMruleIds,myVMrules);
				
				//FIX #6050
				if(oNewData == "On Scheduled Date"){
					VELOS.milestoneGrid.eraseDependent(this,elCell,'evtStatus','');
					VELOS.milestoneGrid.eraseDependent(this,elCell,'evtStatusId',0);
				}
			}
			else if (column.key == 'calendar'){				
				VELOS.milestoneGrid.setDropDownId(this,elCell,myCalendarIds,myCalendars);
				if(oNewData == '***Patient Calendars***' || oNewData == '***Admin Calendars***' ){	
					VELOS.milestoneGrid.eraseDependent(this,elCell,column.key,'');
				}
				if (oOldData != oNewData ){				
					VELOS.milestoneGrid.eraseDependent(this,elCell,'visit','');					
					VELOS.milestoneGrid.eraseDependent(this,elCell,'visitId',0);					
					if(mileTypeId == 'EM'){
						VELOS.milestoneGrid.eraseDependent(this,elCell,'event','');					
						VELOS.milestoneGrid.eraseDependent(this,elCell,'eventId',0);
					}
				}
				if (oRecord.getData('calendarType')=='A'){
					VELOS.milestoneGrid.eraseDependent(this,elCell,'patCount',-1);
					VELOS.milestoneGrid.eraseDependent(this,elCell,'patStatus','');
					VELOS.milestoneGrid.eraseDependent(this,elCell,'patStatusId','0');
				}
			}
			else if(column.key == 'visit'){				 
			 if(mileTypeId == 'VM'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,VMvisitIds,VMvisitNames);
				}
				else{
				VELOS.milestoneGrid.setDropDownId(this,elCell,depIds,depNames);
				}	
				if (oOldData != oNewData){				
					VELOS.milestoneGrid.eraseDependent(this,elCell,'event','');
					VELOS.milestoneGrid.eraseDependent(this,elCell,'eventId',0);
				}		
			}
			else if(column.key == 'event'){
				VELOS.milestoneGrid.setDropDownId(this,elCell,depIds,depNames);
			}
			else{				
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
				var Td = parent.getElementsByClassName('yui-dt-col-'+column.key, 'td')[0];
				var El = new YAHOO.util.Element(Td);
				var oData = oNewData;
				//oData is null if formatter encounters error
				if (oData != null){					
					oData = VELOS.milestoneGrid.encodeData(oData);
					El.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = oData;
					oRecord.setData(column.key, oNewData);
				}
				else if(oData == null){					
					oData = oOldData;
					oRecord.setData(column.key, oData);
					this.onEventShowCellEditor({target:elCell});
				}
				/*if (itemLabelArray['e'+itemSeq] == undefined){
					if (itemName!= undefined && itemName!=null && itemName!=''){
						itemLabelArray['e'+itemSeq] = itemName;
					}
				}else{
					itemLabelArray['e'+itemSeq] = itemName;
				}*/
			}			
			if(oNewData == L_Select_AnOption){				
				VELOS.milestoneGrid.eraseDependent(this,elCell,column.key,'');
			}
			//FIX #6249
			if (oOldData != oNewData){
				if(!((oOldData == null || oOldData == '') && (oNewData == L_Select_AnOption
						|| oNewData == '' || oNewData==null ))){
					VELOS.milestoneGrid.auditAction(mileTypeId, mileSeq, 'Updated', recNum);
				}
			}
		};	
		
		onCheckClick = function(oArgs){
			var elCheckbox = oArgs.target;
			var oRecord = this.getRecord(elCheckbox);
			var column = this.getColumn(elCheckbox);			
			var mileSeq = oRecord.getData('mileSeq');
			var mileId = parseInt(oRecord.getData('mileId'));
			var mileTypeId = oRecord.getData('mileTypeId');
			var rowNum = oRecord.getData('recNum');
			
			if (purgedMileArray==null) purgedMileArray =[];
			if (mileId == 0) {
				if (!f_check_perm_noAlert(pgRight, 'N')) {
					elCheckbox.checked = (!elCheckbox.checked);
					return;
				}
			} else {
				if (!f_check_perm_noAlert(pgRight, 'E')) {
					elCheckbox.checked = (!elCheckbox.checked);
					return;
				}
			}			

			oRecord.setData('delete', ""+elCheckbox.checked);
			if(elCheckbox.checked){
			var paramArray = [rowNum];
					purgedMileArray[mileTypeId+'mileSeq'+mileSeq]=getLocalizedMessageString("L_Del_Row",paramArray)/*'Deleted Row #'+rowNum+''*****/;
					if (oRecord.getData('mileId')!="0"){
						var data = oRecord.getData('mileId');				
						milePurgeList[mileTypeId+'mileSeq'+mileSeq] = data;						
					}
					//this.deleteRow(target);		
		    }
			else{
				purgedMileArray[mileTypeId+'mileSeq'+mileSeq]= undefined;				
				milePurgeList[mileTypeId+'mileSeq'+mileSeq] = undefined;
			}		
		}

		var onHeaderClick = function(oArgs) {
			var myGrid;
			if (this == myPMGrid){
				myGrid = this;
				pmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(pmMileIdCells);
				myPMGrid.focus();
			}
			if (this == myVMGrid){
				myGrid = this;
				vmMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(vmMileIdCells);
				myVMGrid.focus();
			}
			if (this == myEMGrid){
				myGrid = this;
				emMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(emMileIdCells);
				myEMGrid.focus();
			}
			if (this == mySMGrid){
				myGrid = this;
				smMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(smMileIdCells);
				mySMGrid.focus();
			}
			if (this == myAMGrid){
				myGrid = this;
				amMileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
				VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(amMileIdCells);
				myAMGrid.focus();
			}
			
		};
		var handleFocus = function (myGrid, target){
			var oColumn = myGrid.getColumn(target);
			var key = oColumn.key;
			var oRecord = myGrid.getRecord(target);
			var focusMe;
			if(!target.disabled){
				if (key.match(/^v[0-9]+$/)){
					//focus on the checkbox
					var targetEl = new YAHOO.util.Element(target);
					var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
					var parentDivEl = new YAHOO.util.Element(parentDiv);
					var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
					focusMe = chkBox;
					
				}
				if (key == 'delete'){
					//focus on the checkbox
					var targetEl = new YAHOO.util.Element(target);
					var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
					var parentDivEl = new YAHOO.util.Element(parentDiv);
					var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
					focusMe = chkBox;					
				}
			}
			if (focusMe && !focusMe.disabled){
				myGrid.saveCellEditor();
				target.onkeydown = function(e) {
					e = e || window.event;
					var charCode = e.keyCode || e.which;
					switch (charCode) {
					case 9:
						YAHOO.util.Event.stopEvent(e);
						if (e.shiftKey) {
							editPrevious(myGrid, target);
						} else {
							editNext(myGrid, target);
						}
						break;
					}
				}
				focusMe.tabindex = -1;
				focusMe.keydown = target.keydown;
				focusMe.focus();
			}
			return;
		}
		var editNext = function(myGrid, cell) {
			cell = myGrid.getNextTdEl(cell);
			while (cell && !myGrid.getColumn(cell).editor) {
				cell = myGrid.getNextTdEl(cell);
			}
			if (cell) {
				//Enabling/disabling time-point child columns
				var oRecord = myGrid.getRecord(cell);
					var tMileId = oRecord.getData('mileId');
				var tMileTypeId = oRecord.getData('mileTypeId');
				var tMileStatusId = oRecord.getData('mileStaticStatusId');
				var tMileStatusSubType = mileStatusSubTypes[myMileStatusIds.indexOf(tMileStatusId)];
				var returnVal = VELOS.milestoneGrid.enableDisableChildColumns(myGrid.getColumn(cell),tMileStatusSubType,oRecord,respJ);
				if (returnVal < 1) {
					editNext(myGrid, cell);
				}else{
					var oColumn = myGrid.getColumn(cell);
					var justFocus = (oColumn.madeUp==true
					|| oColumn.editor._sType=='checkbox')?true:false;
					if (justFocus){
						handleFocus(myGrid, cell);
					}else {
						YAHOO.util.UserAction.click(cell);
					}
				}
			}
		};
		var editPrevious = function(myGrid, cell) {
			cell = myGrid.getPreviousTdEl(cell);
			while (cell && !myGrid.getColumn(cell).editor) {
				cell = myGrid.getPreviousTdEl(cell);
			}
			if (cell) {
				//Enabling/disabling time-point child columns
				var oRecord = myGrid.getRecord(cell);
				var tMileId = oRecord.getData('mileId');
				var tMileTypeId = oRecord.getData('mileTypeId');
				var tMileStatusId = oRecord.getData('mileStaticStatusId');
				var tMileStatusSubType = mileStatusSubTypes[myMileStatusIds.indexOf(tMileStatusId)];
				var returnVal = VELOS.milestoneGrid.enableDisableChildColumns(myGrid.getColumn(cell),tMileStatusSubType,oRecord,respJ);
				if (returnVal < 1) {
					editPrevious(myGrid, cell);
				}else{
					var oColumn = myGrid.getColumn(cell);
					var justFocus = (oColumn.madeUp==true
					|| oColumn.editor._sType=='checkbox')?true:false;
					if (justFocus){
						handleFocus(myGrid, cell);
					}else {
						YAHOO.util.UserAction.click(cell);
					}
				}
			}
		};
		
		//SM:Introduced a common function to avoid repeated Tabbing code
		var onEditorKeyDown = function(oArgs) {
			var isIe = jQuery.browser.msie;
			var myGrid = this;
			if (myGrid){
				var ed = myGrid._oCellEditor;  // Should be: oArgs.editor, see: http://yuilibrary.com/projects/yui2/ticket/2513909
				var ev = oArgs.event;
				var KEY = YAHOO.util.KeyListener.KEY;
				var Textbox = YAHOO.widget.TextboxCellEditor;
				var Textarea = YAHOO.widget.TextareaCellEditor;
				if (ed){
					var cell = ed.getTdEl();
					var oColumn = ed.getColumn();
					var row,rec;
					//Bug #10465 : AGodara
					if(ed instanceof YAHOO.widget.DropdownCellEditor && isIe == true){
						switch (ev.keyCode) {
						case KEY.TAB:
							YAHOO.util.Event.stopEvent(ev);
							ed.save();
							if (ev.shiftKey) {
								editPrevious(myGrid, cell);
							} else {
								editNext(myGrid, cell);
							}
							break;
						case KEY.ENTER:
							YAHOO.util.Event.stopEvent(ev);
							ed.save();
							break;
						default :
							saveDropDown=false;
							break;
						}						
					}else{
						switch (ev.keyCode) {
						case KEY.TAB:
							YAHOO.util.Event.stopEvent(ev);
							ed.save();
							if (ev.shiftKey) {
								editPrevious(myGrid, cell);
							} else {
								editNext(myGrid, cell);
							}
							break;
						}
					}
				}
			}
		};
		   
		if (pMileTypeId == 'ALL' || pMileTypeId == 'PM'){
			if(myPMGrid != null){
				myPMGrid.subscribe('cellClickEvent', onCellClick);
				myPMGrid.subscribe("editorSaveEvent", onCellSave);
				myPMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myPMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myPMGrid.subscribe("rowMouseoutEvent", onMouseOut);
				myPMGrid.subscribe("rowClickEvent", myPMGrid.onEventSelectRow);
				myPMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myPMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'VM'){
			if(myVMGrid != null){
				myVMGrid.subscribe('cellClickEvent', onCellClick);
				myVMGrid.subscribe("editorSaveEvent", onCellSave);
				myVMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myVMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myVMGrid.subscribe("rowMouseoutEvent", onMouseOut);
				myVMGrid.subscribe("rowClickEvent", myVMGrid.onEventSelectRow);
				myVMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myVMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'EM'){
			if(myEMGrid != null){
				myEMGrid.subscribe('cellClickEvent', onCellClick);
				myEMGrid.subscribe("editorSaveEvent", onCellSave);
				myEMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myEMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myEMGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myEMGrid.subscribe("rowClickEvent", myEMGrid.onEventSelectRow);
				myEMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myEMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'SM'){
			if(mySMGrid != null){
				mySMGrid.subscribe('cellClickEvent', onCellClick);
				mySMGrid.subscribe("editorSaveEvent", onCellSave);
				mySMGrid.subscribe("checkboxClickEvent", onCheckClick);
				mySMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				mySMGrid.subscribe("rowMouseoutEvent", onMouseOut)
				mySMGrid.subscribe("rowClickEvent", mySMGrid.onEventSelectRow);
				mySMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				mySMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		if (pMileTypeId == 'ALL' || pMileTypeId == 'AM'){
			if(myAMGrid != null){
				myAMGrid.subscribe('cellClickEvent', onCellClick);
				myAMGrid.subscribe("editorSaveEvent", onCellSave);
				myAMGrid.subscribe("checkboxClickEvent", onCheckClick);
				myAMGrid.subscribe("rowMouseoverEvent", onMouseOver);
				myAMGrid.subscribe("rowMouseoutEvent", onMouseOut)
				myAMGrid.subscribe("rowClickEvent", myAMGrid.onEventSelectRow);
				myAMGrid.subscribe("headerCellClickEvent", onHeaderClick);
				//TAB key implementation Bug#6182 Date:30-05-2012 Ankit
				myAMGrid.subscribe("editorKeydownEvent",onEditorKeyDown);
			}
		}
		
	/*	//addRowsClick was defined here
		if ($('PM_addRows')){
			var btn = new YAHOO.widget.Button("PM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myPMGrid, 'PM',respJ);},this,true);
		}
		if ($('VM_addRows')){
			var btn = new YAHOO.widget.Button("VM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myVMGrid, 'VM',respJ);},this,true);
		}
		if ($('EM_addRows')){		
			var btn = new YAHOO.widget.Button("EM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myEMGrid, 'EM',respJ);},this,true);
		}
		if ($('SM_addRows')){
			var btn = new YAHOO.widget.Button("SM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(mySMGrid, 'SM',respJ);},this,true);
		}
		if ($('AM_addRows')){
			var btn = new YAHOO.widget.Button("AM_addRows");
			btn.on("click", function () {VELOS.milestoneGrid.addRows(myAMGrid, 'AM',respJ);},this,true);
		}*/
		
		if (!$('save_changes')) {
			var dt = $(this.dataTable);
			var saveDiv = document.createElement('div');
			saveDiv.innerHTML = "<br/><table border=0 width='100%'><tbody><tr align='left'>" +
				"<td width='350'></td>"+
				"<td align='left'><input onclick='VELOS.milestoneGrid.saveDialog();' type='button' id='save_changes' name='save_changes' value='"+Preview_AndSave/*Preview and Save*****/+"'/></td>"+
				"</td></tr></tbody></table>";
			$D.insertAfter(saveDiv, dt);

		}

		calStatus = respJ.calStatus;
		var cells = null;
		if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F' || calStatus == 'R') {
			cells = $D.getElementsByClassName('yui-dt-checkbox', 'input', myPMGrid.getTableEl());
			for (var iX = 0; iX < cells.length; iX++) {
				cells[iX].disabled = true;
			}
			if ($('save_changes')) { $('save_changes').disabled = true; }
		}

		hidePanel();
	};
	this.handleFailure = function(o) {
		alert(CommErr_CnctVelos);/*alert('Communication Error. Please contact Velos Support');*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
	
}

// TAB key implementation Bug#6182,#10074,#10077 Date:30-05-2012 Ankit 
VELOS.milestoneGrid.enableDisableChildColumns = function (oColumn,tMileStatusSubType,oRecord,respJ){
	var key = oColumn.key;
	var tMileId = oRecord.getData('mileId');
	var tMileTypeId = oRecord.getData('mileTypeId');
	var calTyp = oRecord.getData('calendarType');
	var cal = oRecord.getData('calendar');
	if (key == 'delete') { 
		//alert(YAHOO.lang.dump(oRecord));
		var parent = new YAHOO.util.Element(oRecord._sId);
		var target = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
		var targetEl = new YAHOO.util.Element(target);
		var parentDiv = targetEl.getElementsByClassName('yui-dt-liner', 'div')[0];
		var parentDivEl = new YAHOO.util.Element(parentDiv);
		var chkBox = parentDivEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (chkBox && !chkBox.disabled){
			return 1;
		} else return 0; 
	}

	if ((tMileId != 0) && (tMileTypeId == 'PM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'mileType') ||
			(key == 'patCount') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType'))
			return 0;
	}
	if (tMileTypeId == 'VM' || tMileTypeId == 'EM')
	{
		if (key == 'patCount' || key == 'patStatus'){
			if (calTyp == 'A')
				return 0;
			if (tMileId != 0)
				return 0;
		}
	}
	
	if ((tMileId != 0) && (tMileTypeId == 'VM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'calendar') ||
			(key == 'visit') ||
			(key == 'mileRule') ||
			(key == 'evtStatus') ||
			(key == 'patCount') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType'))
			return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'VM') && (tMileStatusSubType == 'WIP' ||(tMileStatusSubType!='IA' || tMileStatusSubType != 'A'))){
		if ((key == 'calendar') ||
			(key == 'visit'))
			return 0;
	}
	if((tMileId == 0) && (tMileTypeId == 'VM') && (key == 'visit')){
			var tCalendar = oRecord.getData('calendar');
			if(tCalendar == '')
				return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'EM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'calendar') ||
			(key == 'visit') ||
			(key == 'event') ||
			(key == 'mileRule') ||
			(key == 'evtStatus') ||
			(key == 'patCount') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType'))
			return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'EM') && (tMileStatusSubType == 'WIP' ||(tMileStatusSubType!='IA' || tMileStatusSubType != 'A'))){
		if ((key == 'calendar') ||
			(key == 'visit') ||
			(key == 'event'))
			return 0;
	}
	if((tMileId == 0) && (tMileTypeId == 'EM') && (key == 'visit' || key == 'event')){
			var tCalendar = oRecord.getData('calendar');
			var depCount = 0;
			if(tCalendar == '')
				return 0;
			if (key == 'visit'){
				depCount = VELOS.milestoneGrid.checkDependent(oColumn,oRecord,'calendarId','c',respJ);
			}
			if (key == 'event'){
				var tVisit = oRecord.getData('visit');
				if(tVisit != ''){
					depCount = VELOS.milestoneGrid.checkDependent(oColumn,oRecord,'visitId','v',respJ);
				}
			}
			if(depCount == 0)
				return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'SM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'mileType') ||
			(key == 'patCount') ||
			(key == 'stdStatus') ||
			(key == 'patStatus') ||
			(key == 'amount') ||
			(key == 'limit') ||
			(key == 'payType'))
			return 0;
	}
	if ((tMileId != 0) && (tMileTypeId == 'AM') && (tMileStatusSubType == 'A' || tMileStatusSubType == 'IA')){
		if ((key == 'mileDesc') ||
			(key == 'amount') ||
			(key == 'payType'))
			return 0;
	}
	if ((key == 'recNum')||(key == 'mileType')){
		return 0;
	}
	if (key == 'evtStatus' && (oRecord.getData('mileRule') == 'On Scheduled Date'))	{
		return 0;
	}
	return 1;
};
VELOS.milestoneGrid.checkDependent = function (columnObject,recordObject,dependent,depValue,respJ){
    var dependentId = recordObject.getData(dependent);
    var depArray = respJ[depValue+dependentId];
    depIds = [];
    VMvisitIds = [];
    depNames = [];
    VMvisitNames = [];   
    for (var iL=0; iL<depArray.length; iL++){
        var depJSON = depArray[iL];
        for(key in depJSON){
            depIds[iL] = key;
            VMvisitIds[iL] = key;
            depNames[iL] = depJSON[key];
            VMvisitNames[iL] = depJSON[key];
        }
    }
    return (depNames.length);
}
VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones = function (mileIdCells){
	for (var iX=0; iX<mileIdCells.length; iX++) {
		var el = new YAHOO.util.Element(mileIdCells[iX]);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		
		var mileStatusIdTd = parent.getElementsByClassName('yui-dt-col-mileStatusId', 'td')[0];
		var mileStatusIdEl = new YAHOO.util.Element(mileStatusIdTd);
		var mileStatusId = mileStatusIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		var mileStatusSubType = mileStatusSubTypes[myMileStatusIds.indexOf(mileStatusId)];
		
		var mileAchievedCountTd = parent.getElementsByClassName('yui-dt-col-mileAchievedCount', 'td')[0];
		var mileAchievedCountEl = new YAHOO.util.Element(mileAchievedCountTd);
		var mileAchievedCount = mileAchievedCountEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	
		//alert('Row #'+iX+' mileStatusSubType:'+mileStatusSubType+' mileAchievedCount:'+mileAchievedCount);
		if ((mileStatusSubType == 'A' && mileAchievedCount != '0') || mileStatusSubType == 'IA'){
			var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
			var delButtonEl = new YAHOO.util.Element(delButtonTd);
			var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			
			delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML ="";
		}
	}
}

VELOS.milestoneGrid.updateDependent = function (columnObject,recordObject,dependent,depValue,respJ){
	var dependentId = recordObject.getData(dependent);		
	var depArray = respJ[depValue+dependentId];
	depIds = [];
	VMvisitIds = [];
	depNames = [];
	VMvisitNames = [];	
	
	for (var iL=0; iL<depArray.length; iL++){
		var depJSON = depArray[iL];
		for(key in depJSON){
			depIds[iL] = key;
			VMvisitIds[iL] = key;
			depNames[iL] = depJSON[key];
			VMvisitNames[iL] = depJSON[key];
		}
	}
	VMvisitIds.push("-1");
	VMvisitNames.push("All");
	var mileTypeId = recordObject.getData('mileTypeId');
	 if(mileTypeId == 'VM' && columnObject.key == 'visit'){
	 	//FIX #6033, #6275
		VMvisitNames = (VMvisitNames.length > 0)? VMvisitNames:null;
		columnObject.editor.dropdownOptions = VMvisitNames;
	 }
	 else{ 
		//FIX #6033, #6275
		depNames = (depNames.length > 0)? depNames:null;
		columnObject.editor.dropdownOptions = depNames;
	 }
	//columnObject.editor.render();
	//alert('rendered');
}

VELOS.milestoneGrid.eraseDependent = function (myGrid,elCell,dependent,depValue){
	var oRecord = myGrid.getRecord(elCell);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var depTd = parent.getElementsByClassName('yui-dt-col-'+dependent, 'td')[0];	
	var depEl = new YAHOO.util.Element(depTd);	
	var dep = depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;	
	oRecord.setData(dependent, depValue);
	depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = depValue;
}

VELOS.milestoneGrid.setDropDownId = function (myGrid,elCell,myIds,myTypes){		
	
	var oRecord = myGrid.getRecord(elCell);			
	var oColumn =  myGrid.getColumn(elCell);	

	for (var j=0; j<myIds.length; j++){
		if (myTypes[j] == oRecord.getData(oColumn.key)) break;
	}

	oRecord.setData(oColumn.key +'Id', myIds[j]);
	
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var idTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key+'Id', 'td')[0];

	var idEl = new YAHOO.util.Element(idTd);

	var Id = idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = myIds[j];
	
	if(oColumn.key == 'calendar'){
		oRecord.setData(oColumn.key +'Type', myCalendarTypes[j]);
		var idTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key+'Type', 'td')[0];

		var idEl = new YAHOO.util.Element(idTd);

		var Id = idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		idEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = myCalendarTypes[j];
	}
};

VELOS.milestoneGrid.sortNumber = function (a, b, desc, field) {
	a = a.getData(field);
	if (/<[^>]+>/.test(a)){		
		a = a.replace(/<[^>]+>/, '');
	}
	b = b.getData(field);
	if (/<[^>]+>/.test(b)){
		b = b.replace(/<[^>]+>/, '');
	}
    
	// Deal with empty values   
	if(!YAHOO.lang.isValue(a)) {   
		return (!YAHOO.lang.isValue(b)) ? 0 : 1;   
	}   
	else if(!YAHOO.lang.isValue(b)) {   
		return -1;   
	}   
	
	a = parseFloat(a);
	b = parseFloat(b);
	    
	return YAHOO.util.Sort.compare(a, b, desc);   
}; 

VELOS.milestoneGrid.processColumns = function (mileTypeId,respJ,colArray){
	myFieldArray = [];
	myColumnDefs = [];
	var isIe = jQuery.browser.msie;	
	if (colArray) {
		var tempColumnDefs =[];
		myColumnDefs = colArray;
		tempColumnDefs = colArray;
		for (var iX=0; iX<myColumnDefs.length; iX++) {

			var indx = myColumnDefs[iX].newIndex; 
			if(mileTypeId == 'VM'){
			myColumnDefs[iX].width = (myColumnDefs[iX].key == 'mileRule') ?
					(isIe==true?80:100):(myColumnDefs[iX].key == 'mileDesc')? (isIe==true?80:150):
				(myColumnDefs[iX].key == 'delete')? 56:(myColumnDefs[iX].key == 'recNum')? 60:
				(myColumnDefs[iX].key == 'patStatus')? (isIe==true?80:50):(myColumnDefs[iX].key == 'patCount') ? (isIe==true?80:50):(isIe==true?80:60);
			}else if(mileTypeId == 'EM'){
				myColumnDefs[iX].width = (myColumnDefs[iX].key == 'mileRule') ?
						(isIe==true?80:100):(myColumnDefs[iX].key == 'mileDesc')? (isIe==true?80:150):
					(myColumnDefs[iX].key == 'delete')? 56:(myColumnDefs[iX].key == 'recNum')? 60:
					(myColumnDefs[iX].key == 'patStatus')? (isIe==true?80:40) :(myColumnDefs[iX].key == 'event') ? (isIe==true?80:40): (myColumnDefs[iX].key == 'patCount') ? (isIe==true?80:40):(isIe==true?80:60);
				}else{
				myColumnDefs[iX].width = (myColumnDefs[iX].key == 'mileRule') ?
						150:(myColumnDefs[iX].key == 'mileDesc')? 200:
						(myColumnDefs[iX].key == 'delete')? 56:(myColumnDefs[iX].key == 'recNum')? 60:
						(myColumnDefs[iX].key == 'patStatus' || myColumnDefs[iX].key == 'patCount') ? 110:100;
			}
			myColumnDefs[iX].resizeable = true;
			//Fix #5968: making columns sortable
			myColumnDefs[iX].sortable = true;
			if (myColumnDefs[iX].type=="number" || myColumnDefs[iX].type=="integer"){
				myColumnDefs[iX].sortOptions = {sortFunction:VELOS.milestoneGrid.sortNumber};
			}

			if (myColumnDefs[iX].key == 'delete') {
				myColumnDefs[iX].madeUp = true;
			}

			var ddArray = null;
			if (myColumnDefs[iX].key == 'mileType'){
				ddArray = respJ.milestoneType;
				myColumnDefs[iX].sortable = false;
			}
			if (myColumnDefs[iX].key == 'calendar')
				ddArray = respJ.studyProtocols;
			if (myColumnDefs[iX].key == 'visit')			
				ddArray = respJ.studyVisits;				
			if (myColumnDefs[iX].key == 'event')
				ddArray = respJ.studyEvents;
			if (myColumnDefs[iX].key == 'mileStatus'){
				ddArray = respJ.mileStatus;				
			}
			if (myColumnDefs[iX].key == 'payType')
				ddArray = respJ.milePayType;
			if (myColumnDefs[iX].key == 'payFor')
				ddArray = respJ.milePayFor;
			if (myColumnDefs[iX].key == 'patStatus')
				ddArray = respJ.patStatus;
			if (myColumnDefs[iX].key == 'evtStatus')
				ddArray = respJ.eventStatus;			
			if (myColumnDefs[iX].key == 'stdStatus')
					ddArray = respJ.studyStatus;
			if (myColumnDefs[iX].key == 'mileRule' && mileTypeId == 'EM')
				ddArray = respJ.EM_mileRule;	
			if (myColumnDefs[iX].key == 'mileRule' && mileTypeId == 'VM')
				ddArray = respJ.VM_mileRule;			
			
			if (ddArray != null){
				myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:ddArray,disableBtns:true});	
			}

			if (myColumnDefs[iX].type!=undefined){
				var type= myColumnDefs[iX].type;
			 	var typeInfo = VELOS.milestoneGrid.types[type];
			 	if (typeInfo.parser!=undefined)
			 		myColumnDefs[iX].parser=typeInfo.parser;
			 	if (myColumnDefs[iX].formatter == undefined){
			 		if (typeInfo.formatter!=undefined)
			 			myColumnDefs[iX].formatter=typeInfo.formatter;
			 	}
			 	if (typeInfo.stringer!=undefined)
			 		myColumnDefs[iX].stringer=typeInfo.stringer;
				if (typeInfo.editorOptions!=undefined)
					myColumnDefs[iX].editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);
			}

			var fieldElem = [];
			if (myColumnDefs[iX].key && myColumnDefs[iX].key == 'delete') {
				myColumnDefs[iX].formatter = 'checkbox';
				myColumnDefs[iX].editor = 'checkbox';
				fieldElem['parser'] = 'checkbox';
			} 
			fieldElem['key'] = myColumnDefs[iX].key;
			myFieldArray.push(fieldElem);						
		}
	}
	return myColumnDefs;
	
}

VELOS.milestoneGrid.updateMilestoneChangeList = function(pMileType,myGrid){
	//alert("Inside:1");
	var arrRecords = myGrid.getRecordSet();
	milestoneDataChangeList = [];

	var data = yuiDataTabletoJSON(myGrid);			
	milestoneDataChangeList[pMileType+'mileSeq'+mileSeq] = data;
}

VELOS.milestoneGrid.auditAction = function(mileTypeId, mileSeq, action, rowNum){
	if (milestoneRowChangeList == null)milestoneRowChangeList = [];
	
	var change=milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq];
	var prevAct ='';

	if(change == undefined){
		if(action=='Updated'){
			var paramArray = [rowNum];
			milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] =getLocalizedMessageString("L_Update_Row",paramArray) /*'Updated Row #'+rowNum+''*****/;
			if (updateRows == null) updateRows = [];
			updateRows[mileTypeId+'mileSeq'+mileSeq] = rowNum;
		}else{
			var paramArray = [rowNum];
			milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] = getLocalizedMessageString("L_Del_Row",paramArray)/*'Deleted Row #'+rowNum+''*****/;
		}
	}else{
		var pos =change.indexOf(' ');
		if (pos>0){
			prevAct = change.slice(0,pos);
		}else{
			prevAct = change;
		}

		if(prevAct=='Added'){
			if(action!='Updated'){
				var paramArray = [action,rowNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] = getLocalizedMessageString("L_Added_Row",paramArray)/*'Added/'+ action + ' Row #'+rowNum+''*****/;
			}
		}else if(prevAct=='Added/Updated'){
			if(action=='Updated'){
				var paramArray = [rowNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] =getLocalizedMessageString("L_AddUpdt_Row",paramArray) /*'Added/Updated Row #'+rowNum+''*****/;
			}else{
				var paramArray = [rowNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] = getLocalizedMessageString("M_AddUpdtDel_Row",paramArray)/*'Added/Updated/Deleted Row #'+rowNum+''*****/;
			}
		}else if(prevAct=='Deleted'){
			//This case is not valid
		}else if(prevAct=='Updated'){
			var paramArray = [rowNum];
			milestoneRowChangeList[mileTypeId+'mileSeq'+mileSeq] =getLocalizedMessageString("L_Update_Row",paramArray)/* 'Updated Row #'+rowNum+''*****/;
		}
	}
}

//Apply attributes to the newly created rows

VELOS.milestoneGrid.types = {
	'varchar':{
		editor:'textbox',
		editorOptions:{disableBtns:true},
		formatter:'varchar'
	},
	'date': {
		parser:'date',
		formatter:'date',
		editor:'date',
		// I don't want the calendar to have the Ok-Cancel buttons below
		editorOptions:{
			disableBtns:true,
			validator: YAHOO.widget.DataTable.validateDate
		},
		// I want to send the dates to my server in the same format that it sends it to me
		// so I use this stringer to convert data on the way from the client to the server
		stringer: function (date) {
			return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() +
				' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
		}
	},
	integer: {
		formatter:'integer',
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}$'
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	number: {
		formatter:'number',
		parser:'number',
		// I am using my own RegularExpressionCellEditor, defined below
		editor:'regexp',
		// I'm only accepting digits (probably this data type should be called integer)
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d{0,11}([.,]\\d{0,2})?$'
			//regExp:'^\\d{11}$',
			//validator: YAHOO.widget.DataTable.validateNumber
		}
	},
	currency: {
		parser:'number',
		formatter:'currency',
		editor:'regexp',
		// for currency I accept numbers with up to two decimals, with dot or comma as a separator
		// It won't accept currency signs and thousands separators really messes things up
		editorOptions: {
			disableBtns:true,
			regExp:'^\\d*([.,]\\d{0,2})?$',
			validator: function (value) {
				return parseFloat(value.replace(',','.'));
			}
		},
		// When I pop up the cell editor, if I don't change the dots into commas, the whole thing will look inconsistent.
		cellEditorFormatter: function(value) {
			return ('' + value).replace('.',',');
		},
		disableBtns:true
	},
	// This is the default, do-nothing data type.
	// There is no need to parse, format or stringify anything and if an editor is needed, the textbox editor will have to do.
	string: {
		editor:'textbox'
	}
};

VELOS.milestoneGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (L_Valid_Esign != document.getElementById('eSignMessage').innerHTML) {
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}


VELOS.milestoneGrid.validate = function(pMileType,myGrid) {	
	var arrRecords = myGrid.getRecordSet();
	var mileSeq;
	var oRecord;	
	var deleteInfo
	var flag_invalid=0;	
	
	if(pMileType == 'PM'){
		var patStatus;
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			var patStatus = oRecord.getData('patStatus');	
			var mileStatus = oRecord.getData('mileStatus');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];			
			//alert("desc  "+patStatus+"  seq "+mileSeq+" deleteinfo:" +deleteInfo);  
			
			if(deleteInfo == undefined){
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patCount', Pat_Count))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patCount', 'Patient Count'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patStatus', Patient_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'patStatus', 'Patient Status'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		return true;
	}
	else if(pMileType == 'VM' || pMileType == 'EM'){
		var patStatus;
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];			
			//alert("1  "+evtStatus+"  2 "+event+" 3:" +deleteInfo + "4: " +calendar+ "5:" +visit +"6:" +mileRule +"7:" +mileStatus ); 
			
			if(deleteInfo == undefined){
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'calendar', Calendar))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'calendar', 'Calendar'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'visit', Visit))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'visit', 'Visit'))*****/
					return false;
				if(pMileType == 'EM'){
					if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'event', Event))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'event', 'Event'))*****/
						return false;
				}
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileRule', Milestone_Rule))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileRule', 'Milestone Rule'))*****/
					return false;
				
				 if(oRecord.getData('mileRule') != 'On Scheduled Date' ){		
					if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'evtStatus', Event_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'evtStatus', 'Event Status'))*****/
						return false;
				 }
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		return true;
		
	}
	else if(pMileType == 'SM'){
		var patStatus;
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];			
			//alert("desc  "+stdStatus+"  seq "+mileSeq+" deleteinfo:" +deleteInfo+" mileStatus:" +mileStatus);  
			if(deleteInfo == undefined){
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'stdStatus', Study_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'stdStatus', 'Study Status'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		return true;
	}
	else if(pMileType == 'AM'){
		var desc='';	
		for(rec=0; rec < arrRecords.getLength(); rec++){
			oRecord = arrRecords.getRecord(rec);
			mileSeq = oRecord.getData('mileSeq');
			deleteInfo = purgedMileArray[pMileType+'mileSeq'+mileSeq];
			if(deleteInfo == undefined){
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileDesc', Milestone_Description))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileDesc', 'Milestone Description'))*****/
					return false;
				if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', Mstone_Status))/*if (!VELOS.milestoneGrid.validateColumnContent(rec, oRecord, 'mileStatus', 'Milestone Status'))*****/
					return false;
			}
		}
		return true;
	}
}

VELOS.milestoneGrid.validateColumnContent = function(recCount, oRecord, columnKey, columnLabel) {
	var columnContent = oRecord.getData(columnKey);
	
	if(columnContent == undefined || columnContent.length == 0){
		var paramArray = [columnLabel,(recCount+1)];
		alert(getLocalizedMessageString("M_EtrFor_Row",paramArray));/*alert("Enter "+columnLabel+" for row #" + (recCount+1));*****/
		return false;
 	}
	return true;
}
//APR-13-11,BK ,FIXED #6016
VELOS.milestoneGrid.addRows = function(mileTypeId) {
	var recordNum;
	var mileType;
	if(f_check_perm(pgRight,'N')){	
	if (mileTypeId == "PM"){
		mileType = Patient_Status;/*mileType = "Patient Status";*****/
		recordNum = PMrecordNum;
	} else if (mileTypeId == "VM"){				
		mileType = Visit;/*mileType = "Visit";*****/
		recordNum = VMrecordNum;
	} else if (mileTypeId == "EM"){
		mileType = Event;/*mileType = "Event";*****/
		recordNum = EMrecordNum;
	} else if (mileTypeId == "SM"){
		mileType = Study_Status;/*mileType = "Study Status";*****/
		recordNum = SMrecordNum;
	} else if (mileTypeId == "AM"){
		mileType = Additional;/*mileType = "Additional";*****/
		recordNum = AMrecordNum;
	}
	//alert(recordNum);	
	var cntFld = ""+mileTypeId+"_rowCount";	
	var count = eval(YAHOO.util.Dom.get(cntFld).value);	
	if(YAHOO.lang.isNumber(count)) {
		if (count > 0){//FIX #6281
			var myArray = [];
			for(var i=0;i<count;i++) {
				mileCount++;recordNum++;
				
				// Modified By Parminder Singh Bug 10672#
				while(milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount]!=undefined)
				{
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount]=milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount];
				mileCount++;
				}
				
				//FIX #6048
				/*var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"limit":'',"mileId":0,"patCount":'-1',
						"evtStatus":'',"evtStatusId":0,
						"patStatus":'',"patStatusId":0,
						"mileAchievedCount":0,"mileDesc":'',"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};*/
				if (mileTypeId == "PM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"mileId":0,"patCount":'-1',"patStatus":'',"patStatusId":0,
						"mileAchievedCount":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "VM"){				
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"mileId":0,"patCount":'-1',
						"evtStatus":'',"evtStatusId":0,
						"patStatus":'',"patStatusId":0,
						"mileAchievedCount":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "EM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"mileId":0,"patCount":'-1',
						"evtStatus":'',"evtStatusId":0,
						"patStatus":'',"patStatusId":0,
						"mileAchievedCount":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "SM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"mileId":0,
						"mileAchievedCount":0,"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				} else if (mileTypeId == "AM"){
					var dataJSON = {"mileSeq":mileCount,"mileTypeId":mileTypeId,"mileType":mileType,
						"amount":0,"mileId":0,
						"mileAchievedCount":0,"mileDesc":'',"mileStaticStatusId":0,
						"payTypeId":defaultPayTypeId, "payType":defaultPayType,"payForId":0, "payFor":'',
						"recNum":recordNum};
				}
				var record = YAHOO.widget.DataTable._cloneObject(dataJSON);
				record.row = i;			
				myArray.push(record);
				if(milestoneRowChangeList==null) milestoneRowChangeList =[];
				var paramArray = [recordNum];
				milestoneRowChangeList[mileTypeId+'mileSeq'+mileCount] = getLocalizedMessageString("L_Add_Row",paramArray)/*'Added Row #'+recordNum;*****/;		
			}
			if (mileTypeId == "PM"){
				PMrecordNum = recordNum;
				myPMGrid.addRows(myArray);
			}
			else if(mileTypeId == "VM"){
				VMrecordNum = recordNum;
				myVMGrid.addRows(myArray);
			}
			else if(mileTypeId == "EM"){
				EMrecordNum = recordNum;
				myEMGrid.addRows(myArray);
			}
			else if(mileTypeId == "SM"){
				SMrecordNum = recordNum;
				mySMGrid.addRows(myArray);
			}
			else{
				AMrecordNum = recordNum;
				myAMGrid.addRows(myArray);
			}
			//myGrid.addRows(myArray);
			//if (count>0){
				//VELOS.milestoneGrid.applyRowAttribs(myGrid,count);
			//}
			YAHOO.util.Dom.get(cntFld).value = "0";		
			return;
		}
	 }
	YAHOO.log(CldNtCont_InvldIndx);/*YAHOO.log("Could not continue due to invalid index.");*****/
	YAHOO.util.Dom.get(mileTypeId+"_rowCount").value = "0";
 }	
}

VELOS.milestoneGrid.saveDialog = function(pMileTypeId) {	
	var myGrid = (pMileTypeId=='PM')? myPMGrid :
		(pMileTypeId=='VM')? myVMGrid :
		(pMileTypeId=='EM')? myEMGrid :
		(pMileTypeId=='SM')? mySMGrid : myAMGrid;
	var myGridName = (pMileTypeId=='PM')? Pat_StatusMstones/*'Patient Status Milestones'*****/ :
		(pMileTypeId=='VM')?  Visit_Mstones/*'Visit Milestones'*****/ :
		(pMileTypeId=='EM')?  Evt_Mstones/*'Event Milestones' *****/:
		(pMileTypeId=='SM')?  StdPat_Mstone/*'Study Status Milestones'******/ : Addl_Mstones/*'Additional Milestones'*****/;
	var regularExp =  new RegExp('^'+pMileTypeId+'mileSeq[0-9]+$'); 
	var otherRegularExp =  new RegExp('mileSeq[0-9]+$');
	
	myGrid.saveCellEditor();
	
	//reloadMilestoneGrid(pMileTypeId)
	if( VELOS.milestoneGrid.validate(pMileTypeId,myGrid)){	
		var saveDialog = document.createElement('div');
		if (!$('saveDialog')) {
			saveDialog.setAttribute('id', 'saveDialog');
			saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CnfrmMstone_Change/*Confirm Milestone Changes*****/+'</div>';
			saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
			$D.insertAfter(saveDialog, $('save_changes'));
		}
	   
		var noChange = true;
		var numChanges = 0;
		var updateJSON = "[" ; 
		var mileVal ="";
		var rows = 0;
		var maxChangesDisplayedBeforeScroll = 25;
		$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="updateMilestone.jsp?'+incomingUrlParams+'"><div id="changeRowsContent"></div><br/><div id="insertFormContent"></div></form>';
		//alert("milerowchange"+milestoneRowChangeList);
		for (oKey in milestoneRowChangeList) {
			var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));

			if (mileTypeId == pMileTypeId){
				if (oKey.match(regularExp) && milestoneRowChangeList[oKey] != undefined) {
					noChange = false;
					if(purgedMileArray[oKey]==undefined){
						var mileSeq = oKey.slice(oKey.indexOf('mileSeq')+7);
						$('changeRowsContent').innerHTML += milestoneRowChangeList[oKey]+'<br/>';
						numChanges++;
						if(updateRows[oKey] != undefined){
							if(rows != 0){
								updateJSON += ",";
							}						
							updateJSON += "{\""+rows+"\":\""+updateRows[oKey]+"\"}";
							rows ++;
						}
					}
				}
			}
		}
		updateJSON += "]";
		//alert("updateJSON: "+updateJSON)
		for (oKey in purgedMileArray) {
			var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
			if (mileTypeId == pMileTypeId){
				if (oKey.match(regularExp) && purgedMileArray[oKey] != undefined) {
					noChange = false;
					var mileSeq = oKey.slice(oKey.indexOf('mileSeq')+7);
					$('changeRowsContent').innerHTML += purgedMileArray[oKey]+'<br/>';
					numChanges++;
				}
			}
		}
		if (numChanges > 0){
			var paramArray = [myGridName];
			$('changeRowsContent').innerHTML = '<b>'+getLocalizedMessageString("L_For_Cl",paramArray)/*'For '+ myGridName + ''*****/+':</b><br/>' + $('changeRowsContent').innerHTML;
			
			myGrid.sortColumn(myGrid.getColumn('recNum'),"yui-dt-asc");
			var mileIdCells = $D.getElementsByClassName('yui-dt-col-mileId', 'td', myGrid.getTableEl());
			VELOS.milestoneGrid.disableDeleteChecksForActiveMilestones(mileIdCells);		
			
			mileVal = yuiDataTabletoJSON(myGrid);			
			mileVal = htmlEncode(mileVal);
			updateJSON = htmlEncode(updateJSON);			
			$('insertFormContent').innerHTML += '<input type="hidden" name="myMilestoneGrid" value="'+mileVal+'" />';
			$('insertFormContent').innerHTML += '<input type="hidden" name="updateInfo" value="'+updateJSON+'" />';			
			
		}
		if (numChanges < maxChangesDisplayedBeforeScroll) {
			$('insertForm').style.width = null;
			$('insertForm').style.height = null;
			$('insertForm').style.overflow = 'visible';
		} else {
			$('insertForm').style.width = '51em';
			$('insertForm').style.height = '35em';
			$('insertForm').style.overflow = 'scroll';
		}
		if (noChange) {
			$('insertFormContent').innerHTML += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
			noChange = true;
		} else {
			$('insertFormContent').innerHTML += Total_Chg/*Total changes*****/+': '+numChanges+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
				'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+'<FONT class="Mandatory">*</FONT>&nbsp</td>'+
				'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
				' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
				' onkeypress="return VELOS.milestoneGrid.fnOnceEnterKeyPress(event)" '+
				' />&nbsp;</td></tr></table>';
			$('insertFormContent').innerHTML += '<input id="changeCount" name="changeCount" type="hidden" value="'+numChanges+'"/>';
		}
		if($('insertFormContentOne')){
			$('insertFormContentOne').innerHTML =""; //YK: 26APR2011 Bug#6008 
		}
		
		var myDialog = new YAHOO.widget.Dialog('saveDialog',
				{
					visible:false, fixedcenter:true, modal:true, resizeable:true,
					draggable:"true", autofillheight:"body", constraintoviewport:false
				});
		var handleCancel = function(e) {
			myDialog.cancel();
		};
		var handleSubmit = function(e) {
			try {
				if ('' == document.getElementById('eSignMessage').innerHTML) {
					alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
	        		return false;
				}
		        var thisTimeSubmitted = new Date();
		        if (lastTimeSubmitted && thisTimeSubmitted) {
		        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
			        lastTimeSubmitted = thisTimeSubmitted;
		        }
			} catch(e) {}
			myDialog.submit();
		};
		var onButtonsReady = function() {
		    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
		    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
		}
		YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
		var myButtons = noChange ?
			[   { text: getLocalizedMessageString("L_Close"), handler: handleCancel } ] :
			[
				{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
				{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
			];
		var onSuccess = function(o) {
			hideTransitPanel();
			var respJ = null; // response in JSON format
			try {
				respJ = $J.parse(o.responseText);
			} catch(e) {
				// Log error here
				return;
			}
			if (respJ.result == 0) {
				// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
				showFlashPanel(respJ.resultMsg);
				//Very Important Fix #6184, 6273, 6274
				VELOS.milestoneGrid.flushListAndResetRemainder(pMileTypeId);
				if (window.parent.reloadMilestoneGrid(pMileTypeId)) { setTimeout('window.parent.reloadMilestoneGrid('+pMileTypeId.value+');', 1500); }
			} else {
				var paramArray = [respJ.resultMsg];
				alert(getLocalizedMessageString("L_Error_Cl",paramArray));/*alert("Error: " + respJ.resultMsg);*****/
			}
		};
		var onFailure = function(o) {
			hideTransitPanel();
			
			var paramArray = [o.status];
			alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
		};
		var onStart = function(o) {
			showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
		}
		myDialog.callback.customevents = { onStart:onStart };
		myDialog.callback.success = onSuccess;
		myDialog.callback.failure = onFailure;
		myDialog.cfg.queueProperty("buttons", myButtons);
		myDialog.render(document.body);
		myDialog.show();
		if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
	
		var showFlashPanel = function(msg) {
			// Initialize the temporary Panel to display while waiting for external content to load
			if (!(VELOS.flashPanel)) {
				VELOS.flashPanel =
					new YAHOO.widget.Panel("flashPanel",
						{ width:"240px",
						  fixedcenter:true,
						  close:false,
						  draggable:false,
						  zindex:4,
						  modal:true,
						  visible:false
						}
					);
			}
			VELOS.flashPanel.setHeader("");
			VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
			VELOS.flashPanel.render(document.body);
			VELOS.flashPanel.show();
			setTimeout('VELOS.flashPanel.hide();', 1500);
		}
	
		var showTransitPanel = function(msg) {
			if (!VELOS.transitPanel) {
				VELOS.transitPanel =
					new YAHOO.widget.Panel("transitPanel",
						{ width:"240px",
						  fixedcenter:true,
						  close:false,
						  draggable:false,
						  zindex:4,
						  modal:true,
						  visible:false
						}
					);
			}
			VELOS.transitPanel.setHeader("");
			VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
			VELOS.transitPanel.render(document.body);
			VELOS.transitPanel.show();
		}
	
		var hideTransitPanel = function() {
			if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
		}
	}
	
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
}


VELOS.milestoneGrid.decodeData = function(oData) {
	oData = oData.replace(/&lt;+/g,"<"); //replace less than
	oData = oData.replace(/&gt;+/g,">"); //replace greater than
	oData = oData.replace(/&amp;+/g,"&"); //replace &
	return oData;
}

VELOS.milestoneGrid.encodeData = function(oData) {	
	oData = oData.replace(/[&]+/g,"&amp;"); //replace &
	oData = oData.replace(/[<]+/g,"&lt;"); //replace less than
	oData = oData.replace(/[>]+/g,"&gt;"); //replace greater than	
	return oData;
}

VELOS.milestoneGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'serverpagination'; }
	if ((args.success) && (typeof args.success == 'function')) {
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.milestoneGrid.flushListAndResetRemainder = function (pMileTypeId){	
	var regularExp =  new RegExp('^'+pMileTypeId+'mileSeq[0-9]+$'); 
	var otherRegularExp =  new RegExp('mileSeq[0-9]+$');
	
	for (oKey in milestoneRowChangeList) {
		var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
		if (mileTypeId == pMileTypeId){			
			milestoneRowChangeList[oKey] = undefined;
			milestoneRowChangeListRemainder[oKey] = undefined;
		}else{
			if (oKey.match(otherRegularExp) && milestoneRowChangeList[oKey] != undefined) {
				milestoneRowChangeListRemainder[oKey] = milestoneRowChangeList[oKey];
			}
		}
	}
				
	for (oKey in purgedMileArray) {
		var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
		if (mileTypeId == pMileTypeId){
			purgedMileArray[oKey] = undefined;
			purgedMileArrayRemainder[oKey] = undefined;
		}else{
			if (oKey.match(otherRegularExp) && purgedMileArray[oKey] != undefined) {
				purgedMileArrayRemainder[oKey] = purgedMileArray[oKey];
			}
		}
	}
	
	//FIX #6286
	for (oKey in updateRows) {
		var mileTypeId = oKey.slice(0,oKey.indexOf('mileSeq'));
		if (mileTypeId == pMileTypeId){
			updateRows[oKey] = undefined;
			updateRowsRemainder[oKey] = undefined;
		}else{
			if (oKey.match(otherRegularExp) && updateRows[oKey] != undefined) {
				updateRowsRemainder[oKey] = updateRows[oKey];
			}
		}
	}		
}

/*YK 29Mar2011 -DFIN20 */
VELOS.milestoneGrid.setStudyMileStat = function(studyID,studyName,mileStatus,statusDesc ) {
	var noChange = true;
		
			if(mileStatus.length>0 && mileStatus !="")
			{
				noChange=false;
			}
			
			 
			setStudyMileStatus="keyword=setStudyMileStatus&studyId="+studyID+"&setMileStat="+mileStatus;
			     var setStatus = document.createElement('div');
					if (!$('setStatus')) {
						setStatus.setAttribute('id', 'setStatus');
						setStatus.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CnfrmStdLvl_Change/*Confirm Study Level Milestone Status Changes*****/+'</div>';
						setStatus.innerHTML += '<div class="bd" id="insertFormOne" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
						$D.insertAfter(setStatus, $('save_changes'));
					}
					$('insertFormOne').innerHTML = '<form id="dialogForm" method="POST" action="setStudyMileStatus.jsp?'+setStudyMileStatus+'"><div id="insertFormContentOne"></div></form>';
					if (noChange) {
						$('insertFormOne').style.width = '30em';
						$('insertFormOne').style.height = null;
						$('insertFormOne').style.overflow = 'visible';
					} else {
						$('insertFormOne').style.width = '40em';
						$('insertFormOne').style.height = null;
						$('insertFormOne').style.overflow = 'visible';
					}
				
					if (noChange) {
						$('insertFormContentOne').innerHTML += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
						noChange = true;
					} else {
						var paramArray = [statusDesc,studyName];
						$('insertFormContentOne').innerHTML =getLocalizedMessageString("M_SetStdMstone_ForStd",paramArray)/*'Set Study Milestone Status to:"<b>'+statusDesc+'</b>" for Study :"<b>'+studyName+'</b>"'*****/; 
						$('insertFormContentOne').innerHTML += '<br/><table align=left><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
							'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
							'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
							' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
							' onkeypress="return VELOS.milestoneGrid.fnOnceEnterKeyPress(event)" '+
							' />&nbsp;</td></tr></table>';
						
					}
					if($('insertFormContent')){
						$('insertFormContent').innerHTML =""; //YK: 26APR2011 Bug#6008 
						}
					
					var myDialog = new YAHOO.widget.Dialog('setStatus',
							{
								visible:false, fixedcenter:true, modal:true, resizeable:true,
								draggable:"true", autofillheight:"body", constraintoviewport:false
							});
					var handleCancel = function(e) {
						myDialog.cancel();
					};
					var handleSubmit = function(e) {
						try {
							if ('' == document.getElementById('eSignMessage').innerHTML) {
								alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
				        		return false;
							}
					        var thisTimeSubmitted = new Date();
					        if (lastTimeSubmitted && thisTimeSubmitted) {
					        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
						        lastTimeSubmitted = thisTimeSubmitted;
					        }
						} catch(e) {}
						myDialog.submit();
					};
					var onButtonsReady = function() {
					    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
					    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
					}
					YAHOO.util.Event.onContentReady("setStatus", onButtonsReady);
					var myButtons = noChange ?
						[   { text: "Close", handler: handleCancel } ] :
						[
							{ text: "Save",   handler: handleSubmit },
							{ text: "Cancel", handler: handleCancel }
						];
					var onSuccess = function(o) {
						hideTransitPanel();
						var respJ = null; // response in JSON format
						try {
							respJ = $J.parse(o.responseText);
						} catch(e) {
							// Log error here
							return;
						}
						if (respJ.result == 0) {
							// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
							showFlashPanel(respJ.resultMsg);
							setTimeout('window.location.reload();', 1500); 
						} else {
							alert("Error: " + respJ.resultMsg);
						}
					};
					var onFailure = function(o) {
						hideTransitPanel();
						var paramArray = [o.status];
						alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
					};
					var onStart = function(o) {
						showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
					}
					myDialog.callback.customevents = { onStart:onStart };
					myDialog.callback.success = onSuccess;
					myDialog.callback.failure = onFailure;
					myDialog.cfg.queueProperty("buttons", myButtons);
					myDialog.render(document.body);
					myDialog.show();
					if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
				
					var showFlashPanel = function(msg) {
						// Initialize the temporary Panel to display while waiting for external content to load
						if (!(VELOS.flashPanel)) {
							VELOS.flashPanel =
								new YAHOO.widget.Panel("flashPanel",
									{ width:"240px",
									  fixedcenter:true,
									  close:false,
									  draggable:false,
									  zindex:4,
									  modal:true,
									  visible:false
									}
								);
						}
						VELOS.flashPanel.setHeader("");
						VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
						VELOS.flashPanel.render(document.body);
						VELOS.flashPanel.show();
						setTimeout('VELOS.flashPanel.hide();', 1500);
					}
				
					var showTransitPanel = function(msg) {
						if (!VELOS.transitPanel) {
							VELOS.transitPanel =
								new YAHOO.widget.Panel("transitPanel",
									{ width:"240px",
									  fixedcenter:true,
									  close:false,
									  draggable:false,
									  zindex:4,
									  modal:true,
									  visible:false
									}
								);
						}
						VELOS.transitPanel.setHeader("");
						VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
						VELOS.transitPanel.render(document.body);
						VELOS.transitPanel.show();
					}
				
					var hideTransitPanel = function() {
						if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
					}
			
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
}
/*YK 29Mar2011 -DFIN20 */

VELOS.milestoneGrid.prototype.startRequest = function() {
	//alert("bk alert 15 start of function startrequest");
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
	//alert("bk alert 15 end of function startrequest");
}

VELOS.milestoneGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.milestoneGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait =
			new YAHOO.widget.Panel("wait",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.milestoneGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.milestoneGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}

