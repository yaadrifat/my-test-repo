$j(document).ready(function(){
		
	// first example
	$j("#navigation").treeview({
		collapsed: true,
		unique: true,
		persist: "location"
	});

	
	// second example
	$j("#browser").treeview({
		animated:"normal",
		persist: "cookie"
	});

	$j("#samplebutton").click(function(){
		var branches = $j("<li><span class='folder'>New Sublist</span><ul>" + 
			"<li><span class='file'>Item1</span></li>" + 
			"<li><span class='file'>Item2</span></li></ul></li>").appendTo("#browser");
		$j("#browser").treeview({
			add: branches
		});
	});


	// third example
	$j("#red").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol"
	});


});