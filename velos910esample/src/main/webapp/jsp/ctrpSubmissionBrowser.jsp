<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
// The only scriptlet in this JSP
try {
	if (!sessionmaint.isValidSession(request.getSession(false))) { return; }
	GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
	if (grpRights == null) { return; }
	if (Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP")) < 4) {
		out.println("<div><p>&nbsp;</p><p class='sectionHeadings' align='center'>"+LC.L_Access_Forbidden+"</p></div>");
		return;
	}
} catch(Exception e) { return; }
%>

<p>Add CTRP Submission Browser here</p>
