<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%@ page import="java.util.ArrayList" %>
<%@ page import="com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil" %>
<%@ page import="com.velos.eres.service.util.LC"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%!
private String getHrefBySubtype(String subtype) {
    if (subtype == null) { return "#"; }
    if ("ctrp_draft_tab".equals(subtype)) {
        return "ctrpDraftBrowser";
    }
    if ("ctrp_subm_tab".equals(subtype)) {
        return "ctrpSubmissionBrowser";
    }
    return "#";
}
%>
<%
String selclass;
String acc = null;
String tab = null;
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    int pageRightReporting = Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP"));
    acc = (String) tSession.getValue("accountId");
    if (!StringUtil.isEmpty(request.getParameter("selectedTab"))) { tab = request.getParameter("selectedTab"); }
%>
<div>
<table cellspacing="2" cellpadding="0" border="0">
    <tr>
<%
    ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
    ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "ctrp_tab");
    
    for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings)tabList.get(iX);
        if ("0".equals(settings.getObjVisible())) {
            continue;
        }

        boolean showThisTab = false;
        if ("ctrp_draft_tab".equals(settings.getObjSubType())) {
            if (pageRightReporting >= 4 ) { showThisTab = true; }
        }
        else if ("ctrp_subm_tab".equals(settings.getObjSubType())) {
            if (pageRightReporting >= 4 ) { showThisTab = true; }
        }
        if (!showThisTab) { continue; }

        if (tab == null) {
            selclass = "unselectedTab";
        } else if (tab.equals(settings.getObjSubType())) {
            selclass = "selectedTab";
        } else {
            selclass = "unselectedTab";
        }
%>
        <td  valign="TOP">
        <table class="<%=selclass%>" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td class="<%=selclass%>">
                    <a href="<%=getHrefBySubtype(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a>
                </td>
            </tr>
        </table>
        </td>
<%
    } // End of tabList loop
%>
    </tr>
</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>
</div>
<%
 } // End of session
%>
