<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*, com.velos.eres.business.common.MilestoneDao"%>

<%

String protocolId = request.getParameter("protocolId");
String studyId  = request.getParameter("studyId");

String totRows = request.getParameter("visitCount");
String fk_visit = request.getParameter("dVisit");

String eSign = request.getParameter("eSign");	

String rules[] = null;
String amounts[] = null;


String fk_visits[] = null;

String msType = "VM";
String delFlag = "N";

int cnt=0;
int ret = 0;

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
	MilestoneDao milestoneDao = new MilestoneDao();
//	String oldESign = (String) tSession.getValue("eSign");
//	if(!oldESign.equals(eSign)) {
%>
 
<%
//	} else {
	
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

	String amount="";

	String rule = "";
	String count="";
	String limit = "";
	String patStatus = "";
	String paymentType = "";
	String paymentFor = "";
	String milestoneStat = "";//KM
	String eventStatus = "";

	String limits[] = null;
	String patStatuses[] = null;
	String paymentTypes[] = null;
	String paymentFors[] = null;
	String milestoneStats[] = null;
    String counts[] = null;      
   	String eventStatuses[] = null;      

	System.out.println("totRows" + totRows);
	
if (totRows.equals("1")) {


	
	fk_visit = request.getParameter("dVisit");

  	if(EJBUtil.stringToNum(fk_visit) != 0) {	
		
		amount = request.getParameter("amount");
		if(amount.equals(".")){
			amount="0";
		}
		rule = request.getParameter("rule");
		count = request.getParameter("count");
		limit = request.getParameter("mLimit");

	     if (StringUtil.isEmpty(count))    
	     {
	     	count = "-1"; 
	     }

	
	    patStatus = request.getParameter("dStatus");

  		
	    paymentType = request.getParameter("dpayType");
	    paymentFor = request.getParameter("dpayFor");
		milestoneStat = request.getParameter("milestoneStat");
	    
   		
	    eventStatus = request.getParameter("dEventStatus");
         
	   	milestoneB.setMilestoneType(msType);
		milestoneB.setMilestoneStudyId(studyId);				
		milestoneB.setMilestoneCalId(protocolId);			

   
		milestoneB.setMilestoneRuleId(rule);	
		milestoneB.setMilestoneAmount(amount);
		milestoneB.setMilestoneDelFlag("N");

		milestoneB.setCreator(usr);
	    milestoneB.setIpAdd(ipAdd);
	    milestoneB.setMilestoneVisitFK(fk_visit);
	    
		milestoneB.setMilestoneStatFK(milestoneStat);
	    milestoneB.setMilestoneLimit( limit) ;
	    milestoneB.setMilestonePayFor( paymentFor) ;
	    milestoneB.setMilestonePayType( paymentType) ;
	    milestoneB.setMilestoneStatus( patStatus );
	    milestoneB.setMilestoneCount(count);
	    
	    milestoneB.setMilestoneEventStatus(eventStatus);
	    milestoneB.setMilestoneVisit("0");
	    
 	    
		ret = milestoneB.setMilestoneDetails();
	}
} else {
	amounts = request.getParameterValues("amount");
    rules = request.getParameterValues("rule");

    fk_visits = request.getParameterValues("dVisit");
    
    counts = request.getParameterValues("count");
    limits = request.getParameterValues("mLimit");
	patStatuses = request.getParameterValues("dStatus");
    paymentTypes = request.getParameterValues("dpayType");
    paymentFors = request.getParameterValues("dpayFor");
	milestoneStats = request.getParameterValues("milestoneStat");
    eventStatuses = request.getParameterValues("dEventStatus");
	
		for (cnt =0;cnt< EJBUtil.stringToNum(totRows) ; cnt++ ) {
			fk_visit = fk_visits[cnt];
			
			if (EJBUtil.stringToNum(fk_visit) != 0) //save only if a visit is selected
			{
				amount = null;

				amount = amounts[cnt];
				
				if(amount.equals(".")){
					amount="0";
				}
				rule = rules[cnt];
				
				count = counts[cnt];
				limit = limits[cnt];
			    patStatus = patStatuses[cnt];
			    paymentType = paymentTypes[cnt];
			    paymentFor = paymentFors[cnt];
				milestoneStat = milestoneStats[cnt];
				eventStatus = eventStatuses[cnt] ;
				
			     if (StringUtil.isEmpty(count))    
				     {
				     	count = "-1";
				     }
				
				

				milestoneB.setMilestoneStudyId(studyId); 
				milestoneB.setMilestoneCalId(protocolId);			
				milestoneB.setMilestoneAmount(amount);
				milestoneB.setMilestoneCount(count);
				milestoneB.setMilestoneDelFlag("N");
				milestoneB.setMilestoneRuleId(rule);
				milestoneB.setMilestoneType(msType);	
				milestoneB.setCreator(usr); 
				milestoneB.setIpAdd(ipAdd);
				milestoneB.setMilestoneVisitFK(fk_visit);
						
				milestoneB.setMilestoneStatFK(milestoneStat);
		        milestoneB.setMilestoneLimit( limit) ;
			    milestoneB.setMilestonePayFor( paymentFor) ;
			    milestoneB.setMilestonePayType( paymentType) ;
			    milestoneB.setMilestoneStatus( patStatus);
	    	    milestoneB.setMilestoneEventStatus(eventStatus);
	    	    milestoneB.setMilestoneVisit("0");
			    
		   		ret = milestoneB.setMilestoneDetails();
   		
			
			}
		}

	
}
   if (ret >= 0) {
%>

<SCRIPT>
   	window.opener.location.reload();
	self.close();
</SCRIPT>

<%	
   } else {
%>   
	<br><br><br><br><br>
	<p class = "sectionHeadings" align = center> <%=MC.M_DataCntSvd_ContacAdmin%><%--Data could not be saved. Please contact the administrator.*****--%> </p>
	<br>
	<button onClick = "self.close()"><%=LC.L_Close%></button>
<%	
   }
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


