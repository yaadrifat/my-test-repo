<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<head>

<title><%=LC.L_Pat_Logout%><%--<%=LC.Pat_Patient%> Logout*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<%@ page language = "java" import="com.velos.eres.service.util.*"%>


<%


HttpSession tSession = request.getSession(true);

if (tSession != null){

//unbind the session for session tracking

tSession.removeAttribute("pp_currentLogin");
tSession.removeAttribute("pp_loginname");
tSession.removeAttribute("pp_patientName");
tSession.removeAttribute("pp_ipAdd");
tSession.removeAttribute("pp_accountId");
tSession.removeAttribute("pp_selfLogout");
tSession.removeAttribute("pp_session");
tSession.removeAttribute("sessionName");
tSession.removeAttribute("appScWidth");
tSession.removeAttribute("appScHeight");
tSession.removeAttribute("pp_consenting");
}

%>

<body>
  <%=LC.L_Logging_Out%><%--Logging out*****--%>......
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=patientlogin.jsp">


</body>

</html>


