<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
  <jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
  <%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.business.common.*,com.velos.eres.service.util.MC"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");
	String moduleTable = request.getParameter("moduleTable");
	String modulePk = request.getParameter("modulePk");
	String status = request.getParameter("status");



	if(mode.equals("M"))
	{
		status = request.getParameter("statCode");
	}

	ArrayList subTypStats =null;
	CodeDao cdao = new CodeDao();
	cdao.getCodeValuesById(EJBUtil.stringToNum(status));
	subTypStats = (cdao.getCSubType());
	String subTypStat = subTypStats.get(0).toString();
	String statusDate = request.getParameter("statusDate");
	String notes = request.getParameter("notes");


	if (EJBUtil.isEmpty(mode))
		mode = "N";

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

%>

<%
	if(mode.equals("M"))
	{
	        String statusId = request.getParameter("statusId");


	 		statusB.setStatusId(EJBUtil.stringToNum(statusId));
			statusB.getStatusHistoryDetails();
	}

			statusB.setStatusModuleId(modulePk);
    		statusB.setStatusModuleTable(moduleTable);
    		statusB.setStatusCodelstId(status);

    		if (moduleTable.equals("er_invoice")){
    		InvB.setId(EJBUtil.stringToNum(modulePk));
    		InvB.getInvoiceDetails();
    		InvB.setInvStat(subTypStat);
    		InvB.updateInvoice();
    		}

    		statusB.setStatusStartDate(statusDate);
    		statusB.setStatusNotes(notes);
    		statusB.setIpAdd(ipAdd);

	if(mode.equals("N"))
	{
    		statusB.setCreator(usr);
    		statusB.setStatusHistoryDetails();

	}	//mode = N
	else if (mode.equals("M"))
	{
    		statusB.setModifiedBy(usr);
       		statusB.updateStatusHistory();

	}
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>

<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





