<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Manage_Org%><%--Manage Organizations*****--%></title>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">	
 function  validate(formobj){
    if (!(validate_col('e-Sign',formobj.eSign))) return false
   
   	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}
}


//Added by Manimaran for June Enhancement #GL2
function checkAll(formobj){
        primOrgTotCnt = formobj.primOrgTotCnt.value;
        normalTotCnt = formobj.normalTotCnt.value;
		if(formobj.All.checked) {
			if (normalTotCnt==0) {
			   formobj.right.checked = true;
  	           row = formobj.rowCnt.value;
	           if (row > 0) {
		          formobj.rights[row].value = 1;
	           } else {
		          formobj.rights.value = 1;
	           }
            } else {
  		
	        	for (i=0;i<=normalTotCnt;i++) {
		          formobj.right[i].checked = true;
		          row = formobj.rowCnt[i].value;
		          formobj.rights[row].value = 1;
		        }	  	  
            }
			
			
			if (primOrgTotCnt==0) {
			   formobj.right_P.checked = true;
  	           row = formobj.rowCnt_P.value;
	           if (row > 0) {
		          formobj.rights[row].value = 1;
	           } else {
		          formobj.rights.value = 1;
	           }
            } else {
  		
	        	for (i=0;i<=primOrgTotCnt;i++) {
		          formobj.right_P[i].checked = true;
		          row = formobj.rowCnt_P[i].value;
		          formobj.rights[row].value = 1;
		        }	  	  
            }
		
		}
        else {  
		   if (normalTotCnt==0) {
			   formobj.right.checked = false;
  	           row = formobj.rowCnt.value;
	           if (row > 0) {
		          formobj.rights[row].value = 0;
	           } else {
		          formobj.rights.value = 0;
	           }
            } else {
  		
	        	for (i=0;i<=normalTotCnt;i++) {
		          formobj.right[i].checked = false;
		          row = formobj.rowCnt[i].value;
		          formobj.rights[row].value = 0;
		        }	  	  
            }

		   if (primOrgTotCnt==0) {
			   formobj.right_P.checked = false;
  	           row = formobj.rowCnt_P.value;
	           if (row > 0) {
		          formobj.rights[row].value = 0;
	           } else {
		          formobj.rights.value = 0;
	           }
            } else {
  		
	        	for (i=0;i<=primOrgTotCnt;i++) {
		          formobj.right_P[i].checked = false;
		          row = formobj.rowCnt_P[i].value;
		          formobj.rights[row].value = 0;
		        }	  	  
            }
       }
  }

function changeRights(obj,row,formobj,rightsRow)	{
	
	  selrow = row ;	  
	  totrows = formobj.totalrows.value;
	  
	  primOrgCnt = formobj.primOrgTotCnt.value;
	  normalCnt =  formobj.normalTotCnt.value;

	 if (totrows > 1) 	rights =formobj.rights[rightsRow].value;
	  else 	rights =formobj.rights.value;

	  objName = obj.name;
	
	  
   if (totrows>1){
	if (obj.checked) {         
       	if (objName == "right"){
		
			if (normalCnt==0) {
				if (!formobj.right.checked){ 
					formobj.right.checked = true;
					rights = 1;		
				}
			} else {
				if (!formobj.right[selrow].checked){ 
					formobj.right[selrow].checked = true;
					rights = 1;					
				}
			}
		}

       	if (objName == "right_P"){		 
		
			if (primOrgCnt==0) {
				if (!formobj.right_P.checked){
     				formobj.right_P.checked = true;
     				rights = 1;
     			}
			} else {
    			if (!formobj.right_P[selrow].checked){ 
     				formobj.right_P[selrow].checked = true;
     				rights = 1;
     			} 
			}
		}		

     	if (objName == "right") rights = 1;
     	if (objName == "right_P") rights = 1;		

		if (totrows > 1 )
			formobj.rights[rightsRow].value = rights;
		else
			formobj.rights.value = rights;
  }else	{
       	if (objName == "right") rights = 0;
       	if (objName == "right_P") rights = 0;		
          
	if (totrows > 1 ) {
			formobj.rights[rightsRow].value = rights;
	 } 
	else {
		formobj.rights.value = rights; 
  	 }
  }
  } else if (totrows==1) {  
  
  	if (obj.checked) {	         
       	if (objName == "right"){			
			if (normalCnt==0) {
				if (!formobj.right.checked){ 
					formobj.right.checked = true;
					rights = 1;		
				}
			} else {
				if (!formobj.right.checked){ 
					formobj.right.checked = true;
					rights = 1;					
				}
			}
		}

       	if (objName == "right_P"){		 
			if (primOrgCnt==0) {
				if (formobj.right_P.checked){
     				formobj.right_P.checked = true;
     				rights = 1;
     			}
			} else {
    			if (formobj.right_P.checked){ 
     				formobj.right_P.checked = true;
     				rights = 1;
     			} 
			}
		}		

     	
    	formobj.rights.value = rights;
		
  }
  else	{
  
       	if (objName == "right") rights = 0;
       	if (objName == "right_P") rights = 0;
		formobj.rights.value = rights;	
		}
  }
}

function chkAllChild(formobj) {
//if user clicks on 'User has access to all child organizations' radio button then
//check new,edit & view rights for all the children of Primary Organization
  primOrgCnt = formobj.primOrgTotCnt.value;
  
  if (primOrgCnt==0) {
	  formobj.right_P.checked = true;
  	  row = formobj.rowCnt_P.value;
	  if (row > 0) {
		  formobj.rights[row].value = 1;
	  } else {
		  formobj.rights.value = 1;
	  }
  } else {
  		
		for (i=0;i<=primOrgCnt;i++) {
		  formobj.right_P[i].checked = true;
		  row = formobj.rowCnt_P[i].value;
		  formobj.rights[row].value = 1;
		}	  	  
  }
}

</SCRIPT>

<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>
<jsp:useBean id ="stdSiteB" scope="request" class="com.velos.eres.web.stdSiteRights.StdSiteRightsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="teamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
	

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<%@ page language = "java" import="java.util.*,java.io.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.userSite.*,com.velos.eres.web.user.UserJB"%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
  	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<div class="popDefault">
<%		
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/> 
	<jsp:include page="include.jsp" flush="true"/>

<%
     int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
 	 int userId = EJBUtil.stringToNum(request.getParameter("userId"));

	 
	 String sessUserId =	(String) tSession.getValue("userId");

	 int teamId = EJBUtil.stringToNum(request.getParameter("teamId"));
	 	 

	 int curUser = EJBUtil.stringToNum(sessUserId);
	 teamB.setId(teamId);
	 teamB.getTeamDetails();
	 String strSiteFlag = teamB.getSiteFlag();
	 if(strSiteFlag == null)
	 	 strSiteFlag = "S";
	 
	 userB.setUserId(userId);
	 userB.getUserDetails(); 
	 
 	 String  userLastName = userB.getUserLastName();
	 String  userFirstName = userB.getUserFirstName();
	 
	 /////////////////////////////
	  int pageRightMngOrg = 0;
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	 
	 if (curUser == userId )
		 pageRightMngOrg	 = 6;
	 else	
	 	 pageRightMngOrg = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
	
%>
	<%Object[] arguments1 = {userFirstName,userLastName}; %>
	<P class="sectionHeadings"> <%=VelosResourceBundle.getMessageString("M_Usr_MngOrgFor",arguments1)%><%--Users >> Manage Organizations for '<%=userFirstName%> <%=userLastName%>'*****--%></P>
<%
	 int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
	 String userSiteFlag = userB.getUserSiteFlag();
	 if (userSiteFlag==null) userSiteFlag="S"; //set this as default
%>   
   <Form name="siteRights" id="siteRightsFrm" method="post" action="updateStdSiteRights.jsp" onSubmit="return validate(document.siteRights)">
   <input type="hidden" name="userPrimOrg" value=<%=userPrimOrg%>>
   <input type="hidden" name="userId" value=<%=userId%>> 
    <input type="hidden" name="teamId" value=<%=teamId%>>   
   <table>
   <tr>
   <td>
   <%if (strSiteFlag.equals("A")) {%>
	   <input type="radio" name="siteFlag" value="A" CHECKED onclick="chkAllChild(document.siteRights)"><%=MC.M_UsrAces_AllChildOrgs%><%--User has access to all child organizations*****--%>
   <%}else{%> 
	   <input type="radio" name="siteFlag" value="A" onclick="chkAllChild(document.siteRights)" ><%=MC.M_UsrAces_AllChildOrgs%><%--User has access to all child organizations*****--%>
	<%}%>
   </td>
   </tr>
   <tr>
   <td> 
   <%if (strSiteFlag.equals("S")) {%>
   <input type="radio" name="siteFlag"  value="S" CHECKED><%=MC.M_UsrAces_ToOrg%><%--User has access to only specified organizations*****--%>
    <%}else{%>
   <input type="radio" name="siteFlag" value="S"><%=MC.M_UsrAces_ToOrg%><%--User has access to only specified organizations*****--%>	
	<%}%> 
   </td>
   </tr>
   </table>
   <br>
<%   
     int stdSiteId = 0;
     int stdSiteSiteId = 0;
     int stdSiteRight = 0;
     String stdSiteName = "";
     int stdSiteIndent = 0;
	 int stdSiteParent = 0;
  	 String spaces = "";
  	  String rChkName = "";
	 String newChkName = "";
 	 String editChkName = "";
 	 String viewChkName = "";
	 String rowCntName = "";
	 int selRow = 0;
	 boolean primFlag;
	 
	 int primOrgCounter = -1;
	 int normalCounter = -1;
     
     ArrayList stdSiteIds = new ArrayList();		
     ArrayList stdSiteSiteIds = new ArrayList();	
     ArrayList stdSiteRights = new ArrayList();					
     ArrayList stdSiteNames = new ArrayList();
     ArrayList stdSiteIndents = new ArrayList();
	 ArrayList stdSiteParents = new ArrayList();
       
     int studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
     StdSiteRightsDao stdSiteDao = stdSiteB.getStudySiteTree(accountId,userId,studyId);
     stdSiteIds = stdSiteDao.getStdSiteRightsIds();   
     stdSiteSiteIds = stdSiteDao.getSiteIds();
     stdSiteNames = stdSiteDao.getStudySiteNames();
     stdSiteRights = stdSiteDao.getStdSiteRights();
     stdSiteIndents = stdSiteDao.getStudySiteIndents();
     stdSiteParents = stdSiteDao.getStudySiteParents();
     
     
     int len = stdSiteIds.size();
     %>
     <Input type="hidden" name="totalrows" value= <%=len%> >
 	  <table width ="50%" border=0>
	  <tr>
	  <TH colspan=2 align=center><%=LC.L_Organization%><%--Organization*****--%></TH>
	  <!-- Modified by Amar for Bugzilla issue #3023 -->
	  <tr><td align="left"><input type="checkbox" name="All"  onClick="checkAll(document.siteRights)">  <%=LC.L_SelOrDeSel_All%><%--Select / Deselect All*****--%></td> </tr>
	   
      
      </tr>
      	  
     <%
     for (int counter=0;counter<len;counter++) {
	 //pkUserSiteId
     	stdSiteId=EJBUtil.stringToNum(((stdSiteIds.get(counter)) == null)?"-":(stdSiteIds.get(counter)).toString());
	//fk_site
     	stdSiteSiteId = EJBUtil.stringToNum(((stdSiteSiteIds.get(counter)) == null)?"-":(stdSiteSiteIds.get(counter)).toString());
     	stdSiteRight = EJBUtil.stringToNum(((stdSiteRights.get(counter)) == null)?"-":(stdSiteRights.get(counter)).toString());
   		stdSiteIndent = EJBUtil.stringToNum(((stdSiteIndents.get(counter)) == null)?"-":(stdSiteIndents.get(counter)).toString());
   		stdSiteName = ((stdSiteNames.get(counter)) == null)?"-":(stdSiteNames.get(counter)).toString();
		stdSiteParent = EJBUtil.stringToNum(((stdSiteParents.get(counter)) == null)?"-":(stdSiteParents.get(counter)).toString());		


		spaces = "";
   		for (int i=1;i<=stdSiteIndent;i++) {
   			spaces = spaces + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
   		}   			
   		stdSiteName = spaces + stdSiteName; //for indentation
		
		//create checkbox names dynamically, 
		//add _P if the site is child of User Primary Organization
		
		if (userPrimOrg == stdSiteParent) {
			primFlag = true;
			primOrgCounter ++;
			rChkName = "right" + "_P";
			rowCntName = "rowCnt" + "_P";
		} else {
			primFlag = false;
			normalCounter ++;
			rChkName = "right";
			rowCntName = "rowCnt";		
		}
		
		if (primFlag) selRow = primOrgCounter;
		else selRow = normalCounter;
     %>
	 
     	<tr>
	        <Input type="hidden" name="rights" value=<%=stdSiteRight%> >
			<Input type="hidden" name="siteIds" value=<%=stdSiteSiteId%> >
	        <Input type="hidden" name="pkStdSite" value=<%=stdSiteId%> >								
	        <Input type="hidden" name="<%=rowCntName%>" value=<%=counter%> >
			
     		<td><%=stdSiteName%></td>	
     		<td align="center">		
			<%if (stdSiteRight==1){%>		
			   <Input type="checkbox" name="<%=rChkName%>" value="1" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
		   <%}else {%>			
			   <Input type="checkbox" name="<%=rChkName%>" value="0" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>">			
		   <%}%>								
		   	  </td>
     	</tr>
     <%
     } //end of for
%>	 
<input type="hidden" name="primOrgTotCnt" value=<%=primOrgCounter%>>
<input type="hidden" name="normalTotCnt" value=<%=normalCounter%>>
<input type="hidden" name="studyId" value=<%=studyId%>>




	
<%if  ( pageRightMngOrg >= 6 )
{ %>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="siteRightsFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<%}%>

<%
} else {  //else of if body for session

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>

</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
