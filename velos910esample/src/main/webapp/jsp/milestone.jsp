<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Milestone_Open%><%--Milestone >> Open*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT>
	//Added for Bug#9388 09-May-2012, Ankit
	 $j(document).ready(function() {
			$j("[name=Del]").attr('checked', false);
			$j("[name=chkAll]").attr('checked', false);
	 });
//JM: 20MAR2008: clickable sorting added for milestone browser headers...

	function setOrder(formObj,orderBy) {

		if (formObj.orderType.value=="asc") {
			formObj.orderType.value= "desc";
			
		} else 	if (formObj.orderType.value=="desc") {
			formObj.orderType.value= "asc";
		}
		var includeMenus = formObj.includeMenus.value;

			orderType=formObj.orderType.value;
			formObj.orderBy.value = orderBy;
			lsrc = formObj.srcmenu.value;
			studyId=formObj.studyId.value;
		if (includeMenus == 'Y') {
			formObj.action="milestone.jsp?mode=M&srcmenu="+lsrc+"&orderBy="+orderBy+"&orderType="+orderType+"&selectedTab=1&studyId="+studyId+"&includeMenus="+includeMenus;
		} else {
			formObj.action="protLinkedMilestones.jsp?"+formObj.protLinkedParams.value+
				"&milestoneParams=\"mode=M&srcmenu="+lsrc+"&orderBy="+orderBy+"&orderType="+orderType+"&selectedTab=1&studyId="+studyId+"&includeMenus="+includeMenus+"\"";
		}
		formObj.submit();

	}


	function chkExcludeMilestone(formObj){

		if (formObj.inactiveMiles.checked){
			formObj.inactiveMiles.value="1";
			formObj.chkflg.value="1";
			}
		else{

			formObj.chkflg.value="0";
			}

	}



function checkAll(formobj){



 	totcount=formobj.totcount.value;


    if (formobj.chkAll.checked){


    if (totcount==1){
          formobj.Del.checked =true ;
	}
    else {
         for (i=0;i<totcount;i++){


			formobj.Del[i].checked=true;


         }
    }
    }else{
    	if (totcount==1){
			formobj.Del.checked =false ;
		}
    	else {
        for (i=0;i<totcount;i++){
			formobj.Del[i].checked=false;
        }
    }

    }

}


 function deleteMilestones(formobj,rt){

   if(!f_check_perm(rt,'E')) {
	          return false;
	  }

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu  = formobj.srcmenu.value;
	 studyId  = formobj.studyId.value;


	 selectedTab = formobj.selectedTab.value;


	 submit="no";
	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelMstone_ToDel%>");/*alert("Please select the Milestones to be deleted");*****/
	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			msg="<%=MC.M_WantToDel_SelMstone%>";/*msg="Do you want to delete the selected Milestone?";*****/
			if (confirm(msg))
			{

				cnt++;

				windowName=  window.open("milestonedelete.jsp?searchFrom=search&srcmenu="+srcmenu+"&studyId="+studyId+"&milestoneId="+formobj.Del.value+"&selectedTab="+selectedTab,"Delete","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950, height=330,left=125,top=200")
         		windowName.focus();


		 	}else{
				return false;
			}
		 }

	 }else{

	 	for(i=0;i<totcount;i++){

	  		if (formobj.Del[i].checked){

		  		selStrs[j] = formobj.Del[i].value;
		  		j++;
		  		cnt++;
			}
	  	}


		if(cnt>0){
			  msg="<%=MC.M_WantToDel_SelMstone%>";/*msg="Do you want to delete the selected Milestone?";*****/
			  if(confirm(msg)) {
					windowName=  window.open("milestonedelete.jsp?searchFrom=search&srcmenu="+srcmenu+"&studyId="+studyId+"&selectedTab="+selectedTab+"&&milestoneId="+selStrs,"Delete","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950, height=330,left=125,top=200")
	         		windowName.focus();

  	    	  }else{
				  	return false;
			  }
		}

	 }
	 if(cnt==0){
		 alert("<%=MC.M_PlsSelMstone_Del%>");/*alert("Please select Milestone to be deleted");*****/
		 return false;
	 }


 }

//JM: added: 25Mar2008:
 function editStatusWindow(formobj, rt,changeStatusTo){


    if(!f_check_perm(rt,'E')) {
	          return false;
	  }

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu  = formobj.srcmenu.value;
	 studyId  = formobj.studyId.value;


	 submit="no";
	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelMstone_ToUpdt%>");/*alert("Please select the Milestones to be updated");*****/
	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			cnt++;

			windowName=  window.open("editmilestonestatus.jsp?searchFrom=search&srcmenu="+srcmenu+"&milestoneId="+formobj.Del.value+"&studyId="+studyId+"&changeStatusTo="+changeStatusTo,"Update","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950, height=330,left=125,top=200")
         	windowName.focus();

	  	 }
	  	 else{
	  	  alert("<%=MC.M_SelMstone_ToUpdt%>");/*alert("Please select the Milestones to be updated");*****/
	   	  return false;
	  	 }

	 }else{

	  for(i=0;i<totcount;i++){

	 	if (formobj.Del[i].checked){

			selStrs[j] = formobj.Del[i].value;
		  	j++;
		  	cnt++;
		 }
		}
		if(cnt>0){

			windowName=  window.open("editmilestonestatus.jsp?searchFrom=search&srcmenu="+srcmenu+"&milestoneId="+selStrs+"&studyId="+studyId+"&changeStatusTo="+changeStatusTo,"Update","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950, height=330,left=125,top=200")
         	windowName.focus();
	  }
	  if(cnt==0)
		 {
		  alert("<%=MC.M_SelMstone_ToUpdt%>");/*alert("Please select the Milestones to be updated");*****/
		   return false;
		 }

	 }

 }
	function AddMilestone(formObj,windowName,pgRight)
	{
	 if (f_check_perm(pgRight,'N') == true) {
         windowName=  window.open(windowName,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950, height=330,left=125,top=200")
         windowName.focus();
	}else {
		return false;
	}

	}
	function confirmBox(name,pageRight,value){

          name = decodeString(name);

         var paramArray = [name];     
         if (confirm(getLocalizedMessageString("M_Want_DelRule",paramArray))) {/*if (confirm("Do you want to Delete Rule "+ name+"?" )) {*****/
           if(f_check_perm(pageRight,value)) {
	       return true;
	   }
       }
      else
	  return false;

}

	function openwin1(milestoneId, pageRight, visitName) {
		if (f_check_perm(pageRight, 'E') == true) {
			//SV, 11/01, show visit name in edit window.
			var windowToOpen = "editmilestonerule.jsp?milestoneId=" + milestoneId+"&visitName="+visitName;
			window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=500,top=200,left=200");
		}else {
			return false;
		}
	}

  function viewAch(formobj, rt){

	 studyId  = formobj.studyId.value;

    if(!f_check_perm(rt,'E')) {
	          return false;
	  }


	  windowName =window.open("deleteAchMS.jsp?studyId="+studyId, "Del","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=850,height=400,top=100,left=100");
	  windowName.focus();


 }

</SCRIPT>



<%
String milestoneParams = StringUtil.htmlDecode(request.getParameter("milestoneParams"));
if (!StringUtil.isEmpty(milestoneParams) && !"null".equals(milestoneParams)) {
    milestoneParams = milestoneParams.replaceAll("&quot;","");
%>
    <script>
    if (document.milestone) {
        document.milestone.action = "milestone.jsp?"+"<%=milestoneParams%>";
        document.milestone.submit();
    }
    </script>
<%
}

String src;
src= request.getParameter("srcmenu");

String includeMenus="";

 includeMenus= request.getParameter("includeMenus");

 if (StringUtil.isEmpty(includeMenus))
 {
 	includeMenus="Y";
 }

 String bottomdivClass = "BrowserBotN BrowserBotN_M_2";


%>

<% if (includeMenus.equals("Y")) { %>
	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<!-- Bug NO: 3976 fixed by PK -->
 <div id="overDiv" style="position:absolute; visibility:collapse; z-index:1000;"> </div>
 <script language="JavaScript" src="overlib.js"> <!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

	<% } else
		{

			bottomdivClass="popDefault";
			%>
				<body>
				<!-- Bug NO: 3976 fixed by PK -->
 <div id="overDiv" style="position:absolute; visibility:collapse; z-index:1000;"> </div>
 <script language="JavaScript" src="overlib.js"> <!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
				<%
		}
%>



<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.MilestoneDao,com.velos.eres.business.common.TeamDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*, com.velos.eres.business.common.*"%>

<%
 String selectedTab = request.getParameter("selectedTab");
 String study = request.getParameter("studyId");


%>
<jsp:include page="include.jsp" flush="true"/>
	<DIV class="BrowserTopn" id="div1">
<% if (includeMenus.equals("Y")) { %>

	  <jsp:include page="milestonetabs.jsp" flush="true">
		<jsp:param name="studyId" value="<%=study%>"/>
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	  </jsp:include>

	<% } %>

		</div>
<DIV class="<%=bottomdivClass%>" id="div2">

  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{

	GrpRightsJB grprightsJB = (GrpRightsJB) tSession.getValue("GRights");
	int mileGrpRight =0;

	mileGrpRight = Integer.parseInt(grprightsJB.getFtrRightsByValue("MILEST"));


    int pageRight= 0;
    int mileRight= 0;
	String userIdFromSession = (String) tSession.getValue("userId");
    int studyId = EJBUtil.stringToNum(study);

	ArrayList teamId ;
	teamId = new ArrayList();
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();

	if (teamId.size() <=0)
	{
		mileRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	ArrayList teamRights =null;
	if (teamId.size() == 0) {
		mileRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));

					teamRights  = new ArrayList();
					teamRights = teamDao.getTeamRights();

					stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
					stdRightsB.loadStudyRights();


		if ((stdRightsB.getFtrRights().size()) == 0){
			mileRight = 0;
		}else{
			mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
		}
	}
	tSession.setAttribute("mileRight",(new Integer(mileRight)).toString());

	pageRight = mileRight;



 	if (pageRight > 0 && mileGrpRight >= 4)
	{
		//JM:20Mar2008: Search condition for the milestone is applied...


		//Milestone Type
		boolean withSelect = true;
		//KM-#D-FIN7
		CodeDao cdStat = new CodeDao();
		int wipPk = cdStat.getCodeId("milestone_stat","WIP");
		String wipPkStr = wipPk +"";
		//KM-08Jun10-#4966
		String wipDesc = cdStat.getCodeDescription(wipPk);

		int actPk = cdStat.getCodeId("milestone_stat","A");
		String actPkStr = actPk + "";

		int inactPk = cdStat.getCodeId("milestone_stat","IA");
		String inactPkStr = inactPk + "";
		String inactDesc = cdStat.getCodeDescription(inactPk);

		CodeDao cdMsTypDao = new CodeDao();
		cdMsTypDao.getCodeValues("milestone_type");


		String msType ="";
		String selMileStone = "";
		selMileStone = request.getParameter("milestoneType");
		if (selMileStone==null) selMileStone="";

		if (EJBUtil.isEmpty(selMileStone))

			msType	= cdMsTypDao.toPullDown("milestoneType");
		else
			msType = cdMsTypDao.toPullDown("milestoneType",EJBUtil.stringToNum(selMileStone),withSelect);


		String selMilestoneType  = "";

		selMilestoneType = cdMsTypDao.getCodeSubtype(EJBUtil.stringToNum(selMileStone));
		if (selMilestoneType ==null) selMilestoneType="";


		//Status
		String msStat = "";
		CodeDao cdMsStatDao = new CodeDao();
		cdMsStatDao.getCodeValues("milestone_stat");

		String selStatus = "";
		selStatus = request.getParameter("milestoneStat");
		if (selStatus==null) selStatus="";



		if (EJBUtil.isEmpty(selStatus))
			msStat = cdMsStatDao.toPullDown("milestoneStat");
		else
			msStat = cdMsStatDao.toPullDown("milestoneStat",EJBUtil.stringToNum(selStatus),withSelect);


		String selMilestoneStat="";

		selMilestoneStat  = cdMsStatDao.getCodeSubtype(EJBUtil.stringToNum(selStatus));
		if (selMilestoneStat ==null) selMilestoneStat="";


		//Payment type
		CodeDao cdMsPayDao = new CodeDao();
		cdMsPayDao.getCodeValues("milepaytype");
		String msPayType = "";

		String selMilePayType = "";
		selMilePayType = request.getParameter("milestPayType");
		if (selMilePayType==null) selMilePayType="";



		if (EJBUtil.isEmpty(selMilePayType))
			msPayType = cdMsPayDao.toPullDown("milestPayType");
		else
			msPayType = cdMsPayDao.toPullDown("milestPayType",EJBUtil.stringToNum(selMilePayType),withSelect);


		String srchStrCalName = StringUtil.decodeString(request.getParameter("calName"));
		if (srchStrCalName ==null) srchStrCalName="";

		String srchStrVisitName = request.getParameter("visitName");
		if (srchStrVisitName ==null) srchStrVisitName="";

		String srchStrEvtName = request.getParameter("evtName");
		if (srchStrEvtName ==null) srchStrEvtName="";




		String showFlag = "false";

		if ( (!srchStrCalName.equals("")) || (!srchStrVisitName.equals("")) || (!srchStrEvtName.equals(""))){
			showFlag = "true";
		}

		String isChecked =  request.getParameter("inactiveMiles");


		String inActiveFlag = request.getParameter("chkflg");
		if (inActiveFlag==null) inActiveFlag="1";



		ArrayList milestoneIds = null;
    	ArrayList milestoneTypes = null;
    	ArrayList milestoneRuleDescs = null;
    	ArrayList milestoneAmounts = null;
    	ArrayList milestoneCounts = null;
    	ArrayList milestoneUsers =null;

		ArrayList protocolVisitNames = null;
    	ArrayList milestoneEventDescs = null;
    	ArrayList milestoneEventStats = null;
    	ArrayList milestoneCalDescs = null;
    	ArrayList milestonePaymentTypes = null;
    	ArrayList milestoneAchievedCounts = null;
		ArrayList milestoneStatFKs = null;
		ArrayList milestoneSubTypes = null; /* YK 14DEC2010 :FIXED Bug #3379 */

    	MilestoneDao milestoneDao = new MilestoneDao();

    	int len = 0;
    	int milestoneId = 0;

    	String milestoneType= "";
    	String milestoneRuleDesc= "";
    	String milestoneAmount="";
    	String milestoneCount= "";
    	String milestoneUser= "";
    	String milestoneAchievedCount = "";
    	int intMilestoneAchievedCount = 0;
    	int intMilestoneCount = 0;
    	
    	String protocolVisitName = "";
    	String milestoneEventDesc = "";
    	String milestoneEventStat = "";
    	String milestoneCalDesc = "";
    	String milestonePaymentType = "";
		String milestoneStatFK = "";//KM
		
		String milestoneSubType= ""; /* YK 14DEC2010 :FIXED Bug #3379 */

	    //JM: 120706
	    String mileStoneStat = "";

		String milestoneHead="";
		String milestoneDesc=""	;
		String hrefVal="";


		boolean pmExist = false;
		boolean vmExist = false;
		boolean emExist = false;



    	String orderBy="", orderType = "";

		orderBy=request.getParameter("orderBy");
		if (orderBy==null) orderBy="patientStatus";

		orderType = request.getParameter("orderType");
		if (orderType == null) orderType = "asc";

		ArrayList curr = new ArrayList();
		String studyCurrency = "";

		studyB.setId(EJBUtil.stringToNum(study));
		studyB.getStudyDetails();
		studyCurrency = studyB.getStudyCurrency();
		String currency = "";

		if(studyCurrency != null) {
			SchCodeDao cdDesc = new SchCodeDao();
			cdDesc.getCodeValuesById(EJBUtil.stringToNum(studyCurrency));
			curr =  cdDesc.getCSubType();
			currency=(((curr.get(0)) ==null)?"-":(curr.get(0)).toString());
		}


		long rowsReturned = 0;


//JM: 16Aug2010: #4971

		String roleCodePk="";

			ArrayList tId = new ArrayList();

			TeamDao teamRoleDao = new TeamDao();
			teamRoleDao.getTeamRights(EJBUtil.stringToNum(study),EJBUtil.stringToNum(userIdFromSession));
			tId = teamRoleDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamRoleDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			}
			else
			{
				roleCodePk ="";
			}



		CodeDao cdMs = new CodeDao();

		cdMs.getCodeValuesForStudyRole("milestone_stat",roleCodePk);

		ArrayList statSubTypes = cdMs.getCSubType();
		// 16Mar2011 @Ankit #5924
		MileAchievedDao achDao = new MileAchievedDao();

%>


	<Form name="milestone" method="post" action="" onsubmit="">

	<input type="hidden" name="orderType" value="<%=orderType%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="orderBy" value="<%=orderBy%>">
	<input type="hidden" name="studyId" value="<%=studyId%>">
	<input type="hidden" name="checked" value="<%=isChecked%>">
	<input type="hidden" name="chkflg" value="<%=inActiveFlag%>">
    <Input type="hidden" name="selectedTab" value="<%=selectedTab%>">


     <table width="99%" border=0 cellspacing="0" cellpadding="0" class="basetbl">
	<tr height="20">
	<td colspan="6" > <b> <%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr>
	<td width="15%" > <%=LC.L_Milestone_Type%><%--Milestone Type*****--%>:</td>
		 <td width="18%" ><%=msType%></td>
    <td width="15%"  align="right"> <%=LC.L_Status%><%--Status*****--%>:&nbsp;</td>
    	 <td width="18%" ><%=msStat%></td>
    <td width="15%" align="right"> <%=LC.L_Payment_Type%><%--Payment Type*****--%>:&nbsp;</td>
    	 <td width="18%" ><%=msPayType%></td>
    </tr>
    <tr>
    <td > <%=LC.L_Calendar%><%--Calendar*****--%>:</td>
    	 <td ><input type="text" name="calName" value="<%=srchStrCalName%>" size="25" ></td>
    <td align="right"> <%=LC.L_Visit%><%--Visit*****--%>:&nbsp;</td>
    	 <td ><input type="text" name="visitName" value="<%=srchStrVisitName%>" size="18" ></td>
    <td align="right"> <%=LC.L_Event%><%--Event*****--%>:&nbsp;</td>
    	 <td ><input type="text" name="evtName" value="<%=srchStrEvtName%>" size="17" ></td>
	</tr>
	<tr >
	<% if (isChecked!=null){ %>
	<td colspan="5" align="right"><input type="checkbox" name="inactiveMiles" onclick=" return chkExcludeMilestone(document.milestone)" checked>
	<%=LC.L_Exclude_InactiveItems%><%--Exclude Inactive Items*****--%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<%}else if(isChecked==null && inActiveFlag.equals("1")){%>
	<td colspan="5" align="right"><input type="checkbox" name="inactiveMiles" onclick="return chkExcludeMilestone(document.milestone)" checked>
	<%=LC.L_Exclude_InactiveItems%><%--Exclude Inactive Items*****--%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<%}else if(isChecked==null && inActiveFlag.equals("0")){%>
	<td colspan="5" align="right"><input type="checkbox" name="inactiveMiles" onclick="return chkExcludeMilestone(document.milestone)" >
	<%=LC.L_Exclude_InactiveItems%><%--Exclude Inactive Items*****--%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<%}%>
	<td align="center"><button type="submit"><%=LC.L_Search%></button></td>

	</tr>
	</table>
	<table  width="99%" border="0" cellspacing="0" cellpadding="0" class="midalign">

	  <tr height="20">
	  <!-- Modified By parminder singh Bug#10127-->
        <td width = "30%"> <b class="sectionHeadingsFrm" style="padding-bottom: 1px;"> <%=LC.L_Saved_Mstones%><%--Saved Milestones*****--%>:   </b> </td>
      <% if (statSubTypes.contains("A")){%>

      	<td width = "15%" align="center"> <A href="javascript:void(0)" title="<%=LC.L_ActivSel_Mstone%><%--Activate selected Milestones*****--%>" onClick="editStatusWindow(document.milestone,'<%=pageRight%>',<%=actPk%>);" ><%=LC.L_Activate%><%--Activate*****--%></A></td>
      <%}else{%>
      	<td width = "15%" align="center"> </td>
	  <%}%>
        <td width = "15%" align="center"><A href="javascript:void(0)" title="<%=LC.L_DelSel_Mstone%><%--Delete selected Milestones*****--%>" onClick ="deleteMilestones(document.milestone,'<%=pageRight%>');" ><%=LC.L_Delete%><%--Delete*****--%></A></td>

      <% if (statSubTypes.contains("WIP")){Object[] arguments = {wipDesc};%>
       <!--KM-08Jun10-#4966-->
       
	   <td width = "15%" align="center"><A href="javascript:void(0)" title="<%=VelosResourceBundle.getMessageString("M_RevertSelMstone_BackTo",arguments)%>" onClick ="editStatusWindow(document.milestone,'<%=pageRight%>',<%=wipPk%>);" ><%=VelosResourceBundle.getLabelString("L_Rvrt_To",arguments)%><%--Revert to <%=wipDesc%>*****--%></A></td>
	  <%}else{%>
       <td width = "15%" align="center"></td>
      <%}%>
        <td width = "15%" align="center"><A href="javascript:void(0)" title="<%=LC.L_ViewOrDel_Achv%><%--View/Delete Achievements*****--%>" onClick ="viewAch(document.milestone,'<%=pageRight%>');" ><%=LC.L_ViewOrDel_Achv%><%--View/Delete Achievements*****--%></A></td>
      </tr>

    </table>

    <table width="99%" border="0" cellspacing="0" cellpadding="0" class="midalign" >
      <tr>
        <th width="25%" onClick="setOrder(document.milestone,'patientStatus')"> <%=LC.L_Rule%><%--Rule*****--%> &loz;</th>
        <th width="10%" onClick="setOrder(document.milestone,'cal_desc')"> <%=LC.L_Calendar%><%--Calendar*****--%> &loz;</th>
        <th width="10%" onClick="setOrder(document.milestone,'visit_name')")> <%=LC.L_Visit%><%--Visit*****--%> &loz;</th>
        <th width="10%" onClick="setOrder(document.milestone,'event_desc')"> <%=LC.L_Event%><%--Event*****--%> &loz;</th>
        <th width="10%" onClick="setOrder(document.milestone,'fk_codelst_milestone_stat')" align = left> <%=LC.L_Status%><%--Status*****--%> &loz;</th>
        <th width="15%" onClick="setOrder(document.milestone,'milestone_amount')" align = left> <%Object[] arguments1 = {currency}; %><%=VelosResourceBundle.getLabelString("L_Amt_In",arguments1)%><%--Amount <i>(in <%=currency%>)</i>*****--%>&loz;</th>
<!--<th width="10%">&nbsp; </th>-->
		<th width="10%" onClick="setOrder(document.milestone,'payment_type')"> <%=LC.L_Type%><%--Type*****--%> &loz;</th>

        <th width=18  align =center ><%=LC.L_Select%><%--Select*****--%><input type="checkbox" name="chkAll" value="" onClick="checkAll(document.milestone)"></th>
      </tr>
	<tr>
		<td>
		<!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm">
				<%=LC.L_Pat_StatusMstones%><%--<%=LC.Pat_Patient%> Status Milestones*****--%>
			</B>
			</td>
			<td colspan=7 align=right>
			<A onClick="return AddMilestone(document.milestone,'patstatmilestone.jsp?studyId=<%=studyId%>','<%=pageRight%>')" HREF="javascript:void(0)"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
<%





  if ((selMilestoneType.equals("PM") || selMilestoneType.equals("")) && showFlag.equals("false")){

		milestoneDao = milestoneB.getMilestoneRows(studyId, "PM", "","", selStatus, selMilePayType, srchStrCalName.trim(), srchStrVisitName.trim(), srchStrEvtName.trim(), inActiveFlag, orderBy, orderType);
		 milestoneIds = milestoneDao.getMilestoneIds();
		 milestoneTypes= milestoneDao.getMilestoneTypes();
    	 milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();
    	 milestoneAmounts=milestoneDao.getMilestoneAmounts();
    	 milestoneCounts= milestoneDao.getMilestoneCounts();
    	 milestoneUsers= milestoneDao.getMilestoneUsers();
		 milestoneAchievedCounts= milestoneDao.getMilestoneAchievedCounts();
    	//JM:
    	milestonePaymentTypes = milestoneDao.getMilestonePaymentTypes();
		milestoneStatFKs = milestoneDao.getMilestoneStatFK();//KM




	    len = milestoneIds.size();

    for(int counter = 0;counter<len;counter++)
	{


		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());

    	//JM:
    	milestonePaymentType=(((milestonePaymentTypes.get(counter)) == null)?"-":(milestonePaymentTypes.get(counter)).toString());


		String milestoneRuleDescSubStr = "";

		if(milestoneRuleDesc.length() > 20) {
		   milestoneRuleDescSubStr = milestoneRuleDesc.substring(0,20);

		}
		else {
		   milestoneRuleDescSubStr = milestoneRuleDesc;
		}


		milestoneAmount=(((milestoneAmounts.get(counter)) == null)?MC.M_NoAssocAmount/*"No Associated Amount"*****/:(milestoneAmounts.get(counter)).toString());
		if(milestoneAmount.trim().equals("0.00")){
			milestoneAmount = MC.M_NoAssocAmount/*"No Associated Amount"*****/;
		}
		milestoneCount=(((milestoneCounts.get(counter)) == null)?"-":(milestoneCounts.get(counter)).toString());
		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());

		milestoneAchievedCount = (String)milestoneAchievedCounts.get(counter);
    	if (StringUtil.isEmpty(milestoneAchievedCount))
    	{
    		milestoneAchievedCount="0";
    	}
    	intMilestoneCount = EJBUtil.stringToNum(milestoneCount);
    	intMilestoneAchievedCount = EJBUtil.stringToNum(milestoneAchievedCount);
    	
		milestoneStatFK = (((milestoneStatFKs.get(counter)) == null)?(wipPkStr):(milestoneStatFKs.get(counter)).toString());
		// 16Mar2011 @Ankit #5924
		boolean flage = achDao.checkAchievedMilestones(milestoneId, studyId);
		//KM-02Jun10-#4965
		if ( (!milestoneStatFK.equals(actPkStr) && !milestoneStatFK.equals(inactPkStr) && !flage ) || (milestoneAchievedCount.equals("0") &&				milestoneStatFK.equals(actPkStr) && !flage)  ){
			rowsReturned=rowsReturned +1;
		}

		mileStoneStat = cdStat.getCodeDescription(EJBUtil.stringToNum(milestoneStatFK));

			milestoneDesc = milestoneRuleDesc;
			if ((counter%2)==0) {
%>
		      <tr class="browserEvenRow">
<%
			} else{
%>
		      <tr class="browserOddRow">
<%
			}
%>
		  		 	<%--SV, 11/1, no visit name for patient milestone.--%>
				<td><A onClick="openwin1(<%=milestoneId%>,'<%=pageRight%>','')" href="javascript:void(0)"><%=milestoneDesc%></A></td>
				 <td>&nbsp;</td>
				  <td>&nbsp;</td>
				   <td>&nbsp;</td>

				 <!--JM: added, 120706-->
				 <td><%=mileStoneStat%></A></td>
				 <td><%=milestoneAmount%></A></td>
		        <td><%=milestonePaymentType%> </td>

		        <td width =11% align="center">
		        <% //KM-02Jun10-#4965 ,16Mar2011 @Ankit #5924
		        int deleteOK =0;
		        if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
		        	deleteOK =1;
				}
		        if (milestoneStatFK.equals(inactPkStr)){
		        	deleteOK =1;
				}
		        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
		        	deleteOK =1;
		        }
				if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
				%>
		        <input type="checkbox" name="Del" value="<%=milestoneId%>">

		        <% }else{%>
		        <input type="hidden" name="missDel" value="<%=milestoneId%>">
		        <%} %>
		        </td>

		      </tr>

<%

	} //for loop

  }//PM ends here
%>


	<tr>
		<td>
		  <!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm">
				<%=LC.L_Visit_Mstones%><%--Visit Milestones*****--%>
			</B>
			</td>
			<td colspan=7 align=right>
			<A onClick="return AddMilestone(document.milestone,'visitmilestone.jsp?studyId=<%=studyId%>','<%=pageRight%>')" HREF="javascript:void(0)"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
<%

  if ((selMilestoneType.equals("VM") || selMilestoneType.equals("")) && srchStrEvtName.equals("")){
	milestoneDao = milestoneB.getMilestoneRows(studyId, "VM", "","", selStatus, selMilePayType, srchStrCalName.trim(), srchStrVisitName.trim(), srchStrEvtName.trim(), inActiveFlag, orderBy, orderType);

	milestoneIds = milestoneDao.getMilestoneIds();
    milestoneTypes= milestoneDao.getMilestoneTypes();
    
    milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();
    milestoneAmounts=milestoneDao.getMilestoneAmounts();
    milestoneCounts= milestoneDao.getMilestoneCounts();
    milestoneUsers= milestoneDao.getMilestoneUsers();
	milestoneStatFKs = milestoneDao.getMilestoneStatFK();//KM


	protocolVisitNames = milestoneDao.getProtocolVisitNames();
	milestoneEventDescs = milestoneDao.getMilestoneEventDescs();
	milestoneEventStats = milestoneDao.getMilestoneEventStatuses();
	milestoneCalDescs = milestoneDao.getMilestoneCalDescs();
	milestonePaymentTypes = milestoneDao.getMilestonePaymentTypes();
	milestoneAchievedCounts= milestoneDao.getMilestoneAchievedCounts();
	
	milestoneSubTypes = milestoneDao.getMileStoneSubType(); /* YK 14DEC2010 :FIXED Bug #3379 */

	len = milestoneIds.size();
	  /* YK 14DEC2010 :FIXED Bug #3379 */
	StringBuffer noOnSchedule= new StringBuffer();
	StringBuffer onSchedule= new StringBuffer();
	  /* YK 14DEC2010 :FIXED Bug #3379 */

    for(int counter = 0;counter<len;counter++)
	{

		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());
		milestoneAmount=(((milestoneAmounts.get(counter)) == null)?MC.M_NoAssocAmount/*"No Associated Amount"*****/:(milestoneAmounts.get(counter)).toString());


		//JM:
		protocolVisitName=(((protocolVisitNames.get(counter)) == null)?"-":(protocolVisitNames.get(counter)).toString());
		milestoneCalDesc=(((milestoneCalDescs.get(counter)) == null)?"-":(milestoneCalDescs.get(counter)).toString());
		milestonePaymentType =(((milestonePaymentTypes.get(counter)) == null)?"-":(milestonePaymentTypes.get(counter)).toString());

		/* YK 14DEC2010 :FIXED Bug #3379 */
		milestoneSubType =(((milestoneSubTypes.get(counter)) == null)?"-":(milestoneSubTypes.get(counter)).toString());



		if(milestoneAmount.trim().equals("0.00")){
			milestoneAmount = MC.M_NoAssocAmount/*"No Associated Amount"*****/;
		}
		// KN
		String milestoneRuleDescSubStr = "";
		if(milestoneRuleDesc.length() > 20) {
		   milestoneRuleDescSubStr = milestoneRuleDesc.substring(0,20);

		}
		else {
		   milestoneRuleDescSubStr = milestoneRuleDesc;
		}


		milestoneCount=(((milestoneCounts.get(counter)) == null)?"-":(milestoneCounts.get(counter)).toString());
		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneStatFK = (((milestoneStatFKs.get(counter)) == null)?(wipPkStr):(milestoneStatFKs.get(counter)).toString());


        	milestoneAchievedCount = (String)milestoneAchievedCounts.get(counter);
    	if (StringUtil.isEmpty(milestoneAchievedCount))
    	{
    		milestoneAchievedCount="0";
    	}
    	//FIX #6231, #5924
    	intMilestoneCount = EJBUtil.stringToNum(milestoneCount);
    	intMilestoneAchievedCount = EJBUtil.stringToNum(milestoneAchievedCount);
    	
		// 16Mar2011 @Ankit #5924
    	boolean flage = achDao.checkAchievedMilestones(milestoneId, studyId);
		//KM-02Jun10-#4965
		if ( (!milestoneStatFK.equals(actPkStr) && !milestoneStatFK.equals(inactPkStr) && !flage ) || (milestoneAchievedCount.equals("0") && milestoneStatFK.equals(actPkStr) && !flage )  ){
			rowsReturned=rowsReturned +1;
		}

		mileStoneStat = cdStat.getCodeDescription(EJBUtil.stringToNum(milestoneStatFK));

		milestoneDesc=milestoneRuleDesc;
		
		// YK 12MAY2011 Bug#5689
		if (orderBy.equalsIgnoreCase("patientStatus")|| orderBy.equalsIgnoreCase("event_desc"))
		  {
			   /* YK 14DEC2010 :FIXED Bug #3379 */
			   
				if(milestoneSubType.equals("vm_4")) // Checks for the Milestone SubType for vm_4 
				{
					if ((counter%2)==0) {
						
								    onSchedule.append("<tr class=\"browserEvenRow\">");
						
									} else{
						
								   onSchedule.append("<tr class=\"browserOddRow\">");
						
									}
					onSchedule.append("<td><A onClick=\"openwin1('"+milestoneId+"','"+pageRight+"', ' ')\" href=\"javascript:void(0)\">"+milestoneDesc+"</A></td>");
					onSchedule.append("<td>"+milestoneCalDesc+" </td>");
					onSchedule.append("<td>"+protocolVisitName+" </td>");
					onSchedule.append("<td>&nbsp;</td>");
					onSchedule.append("<td> "+mileStoneStat+"</A></td>");
					onSchedule.append("<td>"+milestoneAmount+" </td>");
					onSchedule.append("<td>"+milestonePaymentType+" </td>");
					onSchedule.append("<td width =11% align='center'>");
	    	    	//FIX #6231, #5924
					int deleteOK =0;
			        if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
			        	deleteOK =1;
					}
			        if (milestoneStatFK.equals(inactPkStr)){
			        	deleteOK =1;
					}
			        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
			        	deleteOK =1;
			        }
			        
					if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
						onSchedule.append("<input type=\"checkbox\" name=\"Del\" value=\""+milestoneId+"\">"); //Ashu modified for BUG#5688 (9Feb11).
					}
					else{
						onSchedule.append("<input type=\"hidden\" name=\"missDel\" value=\""+milestoneId+"\">");} //Ashu modified for BUG#5688 (9Feb11).
				
					onSchedule.append("</td></tr>");
				}else //if Milestone SubType is not equal to vm_4
				{
					if ((counter%2)==0) {
						
								     noOnSchedule.append("<tr class=\"browserEvenRow\">");
						
									} else{
						
								     noOnSchedule.append("<tr class=\"browserOddRow\">");
						
									}
					
					noOnSchedule.append("<td><A onClick=\"openwin1('"+milestoneId+"','"+pageRight+"', ' ')\" href=\"javascript:void(0)\">"+milestoneDesc+"</A></td>");
					noOnSchedule.append("<td>"+milestoneCalDesc+" </td>");
					noOnSchedule.append("<td>"+protocolVisitName+" </td>");
					noOnSchedule.append("<td>&nbsp;</td>");
					noOnSchedule.append("<td> "+mileStoneStat+"</A></td>");
					noOnSchedule.append("<td>"+milestoneAmount+" </td>");
					noOnSchedule.append("<td>"+milestonePaymentType+" </td>");
					noOnSchedule.append("<td width =11% align='center'>");
					//FIX #6231, #5924
					int deleteOK =0;
			        if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
			        	deleteOK =1;
					}
			        if (milestoneStatFK.equals(inactPkStr)){
			        	deleteOK =1;
					}
			        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
			        	deleteOK =1;
			        }
			        
					if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
					noOnSchedule.append("<input type=\"checkbox\" name=\"Del\" value=\""+milestoneId+"\">");
					}
					else{
					noOnSchedule.append("<input type=\"hidden\" name=\"missDel\" value=\""+milestoneId+"\">");}
				
					noOnSchedule.append("</td></tr>");
				}
			}else{ // YK 12MAY2011 Bug#5689 
				if ((counter%2)==0) {
					
				    onSchedule.append("<tr class=\"browserEvenRow\">");
		
					} else{
		
				   onSchedule.append("<tr class=\"browserOddRow\">");
		
					}
				onSchedule.append("<td><A onClick=\"openwin1('"+milestoneId+"','"+pageRight+"', ' ')\" href=\"javascript:void(0)\">"+milestoneDesc+"</A></td>");
				onSchedule.append("<td>"+milestoneCalDesc+" </td>");
				onSchedule.append("<td>"+protocolVisitName+" </td>");
				onSchedule.append("<td>&nbsp;</td>");
				onSchedule.append("<td> "+mileStoneStat+"</A></td>");
				onSchedule.append("<td>"+milestoneAmount+" </td>");
				onSchedule.append("<td>"+milestonePaymentType+" </td>");
				onSchedule.append("<td width =11% align='center'>");
				//FIX #6231, #5924
				int deleteOK =0;
		        if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
		        	deleteOK =1;
				}
		        if (milestoneStatFK.equals(inactPkStr)){
		        	deleteOK =1;
				}
		        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
		        	deleteOK =1;
		        }
		        
				if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
					onSchedule.append("<input type=\"checkbox\" name=\"Del\" value=\""+milestoneId+"\">"); //Ashu modified for BUG#5688 (9Feb11).
				}
				else{
					onSchedule.append("<input type=\"hidden\" name=\"missDel\" value=\""+milestoneId+"\">");} //Ashu modified for BUG#5688 (9Feb11).
			
				onSchedule.append("</td></tr>");
			}
	} //for loop
	 if(orderType.equals("desc"))// Based on orderBy sorting for On Schedule Date is done
		{	
	%>
	
		<%=""+noOnSchedule.append(onSchedule) %>	
	  	<%}else { %>
	   	<%=""+onSchedule.append(noOnSchedule) %>	
	  
	  	<%} %>
<%
/* YK 14DEC2010 :FIXED Bug #3379 */
  }//VM end here
%>
	<tr>
		<td>
		 <!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm">
				<%=LC.L_Evt_Mstones%><%--Event Milestones*****--%>
			</B>
			</td>
			<td colspan=7 align=right>
			<A onClick="return AddMilestone(document.milestone,'eventmilestone.jsp?studyId=<%=studyId%>','<%=pageRight%>')" HREF="javascript:void(0)"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
<%

  if (selMilestoneType.equals("EM") || selMilestoneType.equals("")){
	milestoneDao = milestoneB.getMilestoneRows(studyId, "EM", "","", selStatus, selMilePayType, srchStrCalName.trim(), srchStrVisitName.trim(), srchStrEvtName.trim(), inActiveFlag, orderBy, orderType);
	milestoneIds = milestoneDao.getMilestoneIds();
    milestoneTypes= milestoneDao.getMilestoneTypes();
    milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();
    milestoneAmounts=milestoneDao.getMilestoneAmounts();
    milestoneCounts= milestoneDao.getMilestoneCounts();
    milestoneUsers= milestoneDao.getMilestoneUsers();

	protocolVisitNames = milestoneDao.getProtocolVisitNames();
	milestoneEventDescs = milestoneDao.getMilestoneEventDescs();
	milestoneCalDescs = milestoneDao.getMilestoneCalDescs();
	milestonePaymentTypes = milestoneDao.getMilestonePaymentTypes();
	milestoneAchievedCounts= milestoneDao.getMilestoneAchievedCounts();

	milestoneStatFKs = milestoneDao.getMilestoneStatFK();

	milestoneSubTypes = milestoneDao.getMileStoneSubType(); /* YK 14DEC2010 :FIXED Bug #3379 */
	
	len = milestoneIds.size();
	  /* YK 14DEC2010 :FIXED Bug #3379 */
	StringBuffer noOnSchedule= new StringBuffer();
	StringBuffer onSchedule= new StringBuffer();
	  /* YK 14DEC2010 :FIXED Bug #3379 */
    for(int counter = 0;counter<len;counter++)
	{

		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());

		//JM:
		protocolVisitName=(((protocolVisitNames.get(counter)) == null)?"-":(protocolVisitNames.get(counter)).toString());
		milestoneEventDesc=(((milestoneEventDescs.get(counter)) == null)?"-":(milestoneEventDescs.get(counter)).toString());
		milestoneCalDesc=(((milestoneCalDescs.get(counter)) == null)?"-":(milestoneCalDescs.get(counter)).toString());
		milestonePaymentType =(((milestonePaymentTypes.get(counter)) == null)?"-":(milestonePaymentTypes.get(counter)).toString());

		 /* YK 14DEC2010 :FIXED Bug #3379 */
		milestoneSubType =(((milestoneSubTypes.get(counter)) == null)?"-":(milestoneSubTypes.get(counter)).toString());

		milestoneAmount=(((milestoneAmounts.get(counter)) == null)?MC.M_NoAssocAmount/*"No Associated Amount"*****/:(milestoneAmounts.get(counter)).toString());
		if(milestoneAmount.trim().equals("0.00")){
			milestoneAmount = MC.M_NoAssocAmount/*"No Associated Amount"*****/;
		}

		String milestoneRuleDescSubStr = "";
		if(milestoneRuleDesc.length() > 20) {
		   milestoneRuleDescSubStr = milestoneRuleDesc.substring(0,20);

		}
		else {
		   milestoneRuleDescSubStr = milestoneRuleDesc;
		}

		milestoneCount=(((milestoneCounts.get(counter)) == null)?"-":(milestoneCounts.get(counter)).toString());
		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneStatFK = (((milestoneStatFKs.get(counter)) == null)?(wipPkStr):(milestoneStatFKs.get(counter)).toString());

		milestoneAchievedCount = (String)milestoneAchievedCounts.get(counter);
    	if (StringUtil.isEmpty(milestoneAchievedCount))
    	{
    		milestoneAchievedCount="0";
    	}
    	//FIX #6231, #5924
    	intMilestoneCount = EJBUtil.stringToNum(milestoneCount);
    	intMilestoneAchievedCount = EJBUtil.stringToNum(milestoneAchievedCount);
    	
    	// 16Mar2011 @Ankit #5924
    	boolean flage = achDao.checkAchievedMilestones(milestoneId, studyId);
		//KM-02Jun10-#4965
		if ( (!milestoneStatFK.equals(actPkStr) && !milestoneStatFK.equals(inactPkStr) ) || (milestoneAchievedCount.equals("0") &&				milestoneStatFK.equals(actPkStr) )  ){
			rowsReturned=rowsReturned +1;
		}

		mileStoneStat = cdStat.getCodeDescription(EJBUtil.stringToNum(milestoneStatFK));

		milestoneDesc = milestoneRuleDesc;
		 
    if (orderBy.equalsIgnoreCase("patientStatus")) // YK 12MAY2011 Bug#5689 
	 {
		 /* YK 14DEC2010 :FIXED Bug #3379 */
		 
		if(milestoneSubType.equals("em_4")) // Checks for the Milestone SubType for em_4
		{
			if ((counter%2)==0) {
				
						    onSchedule.append("<tr class=\"browserEvenRow\">");
				
							} else{
				
						   onSchedule.append("<tr class=\"browserOddRow\">");
				
							}
			onSchedule.append("<td><A onClick=\"openwin1('"+milestoneId+"','"+pageRight+"', ' ')\" href=\"javascript:void(0)\">"+milestoneDesc+"</A></td>");
			onSchedule.append("<td>"+milestoneCalDesc+" </td>");
			onSchedule.append("<td>"+protocolVisitName+" </td>");
			onSchedule.append("<td>"+milestoneEventDesc+"</td>");
			onSchedule.append("<td> "+mileStoneStat+"</A></td>");
			onSchedule.append("<td>"+milestoneAmount+" </td>");
			onSchedule.append("<td>"+milestonePaymentType+" </td>");
			onSchedule.append("<td width =11% align='center'>");
			//FIX #6231, #5924
			int deleteOK =0;
	        if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
	        	deleteOK =1;
			}
	        if (milestoneStatFK.equals(inactPkStr)){
	        	deleteOK =1;
			}
	        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
	        	deleteOK =1;
	        }
	        
			if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
				onSchedule.append("<input type=\"checkbox\" name=\"Del\" value=\""+milestoneId+"\">"); //Ashu modified for BUG#5688 (9Feb11).
			}
			else{
				onSchedule.append("<input type=\"hidden\" name=\"missDel\" value=\""+milestoneId+"\">");} //Ashu modified for BUG#5688 (9Feb11).
		
			onSchedule.append("</td></tr>");
		}else //if Milestone SubType is not equal to em_4
		{
			if ((counter%2)==0) {
				
						     noOnSchedule.append("<tr class=\"browserEvenRow\">");
				
							} else{
				
						     noOnSchedule.append("<tr class=\"browserOddRow\">");
				
							}
			
			noOnSchedule.append("<td><A onClick=\"openwin1('"+milestoneId+"','"+pageRight+"', ' ')\" href=\"javascript:void(0)\">"+milestoneDesc+"</A></td>");
			noOnSchedule.append("<td>"+milestoneCalDesc+" </td>");
			noOnSchedule.append("<td>"+protocolVisitName+" </td>");
			noOnSchedule.append("<td>"+milestoneEventDesc+"</td>");
			noOnSchedule.append("<td> "+mileStoneStat+"</A></td>");
			noOnSchedule.append("<td>"+milestoneAmount+" </td>");
			noOnSchedule.append("<td>"+milestonePaymentType+" </td>");
			noOnSchedule.append("<td width =11% align='center'>");
			//FIX #6231, #5924
			int deleteOK =0;
			if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
	        	deleteOK =1;
			}
	        if (milestoneStatFK.equals(inactPkStr)){
	        	deleteOK =1;
			}
	        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
	        	deleteOK =1;
	        }
	        
			if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
			noOnSchedule.append("<input type=\"checkbox\" name=\"Del\" value=\""+milestoneId+"\">");
			}
			else{
			noOnSchedule.append("<input type=\"hidden\" name=\"missDel\" value=\""+milestoneId+"\">");}
		
			noOnSchedule.append("</td></tr>");
		}
	}else{ /* YK 14DEC2010 :FIXED Bug #3379 */
		if ((counter%2)==0) {
			
		    onSchedule.append("<tr class=\"browserEvenRow\">");

			} else{

		   onSchedule.append("<tr class=\"browserOddRow\">");

			}
		onSchedule.append("<td><A onClick=\"openwin1('"+milestoneId+"','"+pageRight+"', ' ')\" href=\"javascript:void(0)\">"+milestoneDesc+"</A></td>");
		onSchedule.append("<td>"+milestoneCalDesc+" </td>");
		onSchedule.append("<td>"+protocolVisitName+" </td>");
		onSchedule.append("<td>"+milestoneEventDesc+"</td>");
		onSchedule.append("<td> "+mileStoneStat+"</A></td>");
		onSchedule.append("<td>"+milestoneAmount+" </td>");
		onSchedule.append("<td>"+milestonePaymentType+" </td>");
		onSchedule.append("<td width =11% align='center'>");
		//FIX #6231, #5924
		int deleteOK =0;
        if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
        	deleteOK =1;
		}
        if (milestoneStatFK.equals(inactPkStr)){
        	deleteOK =1;
		}
        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
        	deleteOK =1;
        }
        
		if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
		onSchedule.append("<input type=\"checkbox\" name=\"Del\" value=\""+milestoneId+"\">"); //Ashu modified for BUG#5688 (9Feb11).
		}
		else{
		onSchedule.append("<input type=\"hidden\" name=\"missDel\" value=\""+milestoneId+"\">");} //Ashu modified for BUG#5688 (9Feb11).
		
		onSchedule.append("</td></tr>");
	}

	} //for loop
    if(orderType.equals("desc")) // Based on orderBy sorting for On Schedule Date is done
	{	
%>
	<%=""+noOnSchedule.append(onSchedule) %>	
  	<%}else { %>
   	<%=""+onSchedule.append(noOnSchedule) %>	
  
  	<%} %>

  <%
  /* YK 14DEC2010 :FIXED Bug #3379 */
  }//EM ends here
%>

	<tr>
		<td>
		 <!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm">
				<%=LC.L_StdPat_Mstone%><%--<%=LC.Std_Study%> Status Milestones*****--%>
			</B>
			</td>
			<td colspan=7 align=right>
			<A onClick="return AddMilestone(document.milestone,'patstatmilestone.jsp?studyId=<%=studyId%>&milestoneType=SM','<%=pageRight%>')" HREF="javascript:void(0)"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>

<%

  if ((selMilestoneType.equals("SM") || selMilestoneType.equals("")) && showFlag.equals("false")){

	 milestoneDao = milestoneB.getMilestoneRows(studyId, "SM", "","", selStatus, selMilePayType, srchStrCalName.trim(), srchStrVisitName.trim(), srchStrEvtName.trim(), inActiveFlag, orderBy, orderType);
	 milestoneIds = milestoneDao.getMilestoneIds();
     milestoneTypes= milestoneDao.getMilestoneTypes();
   	 milestoneRuleDescs= milestoneDao.getMilestoneRuleDescs();
   	 milestoneAmounts=milestoneDao.getMilestoneAmounts();
   	 milestoneCounts= milestoneDao.getMilestoneCounts();
   	 milestoneUsers= milestoneDao.getMilestoneUsers();

   	 milestonePaymentTypes = milestoneDao.getMilestonePaymentTypes();
	 milestoneAchievedCounts= milestoneDao.getMilestoneAchievedCounts();

	 milestoneStatFKs = milestoneDao.getMilestoneStatFK();
	 len = milestoneIds.size();


    for(int counter = 0;counter<len;counter++)
	{


		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneAmount=(((milestoneAmounts.get(counter)) == null)?MC.M_NoAssocAmount/*"No Associated Amount"*****/:(milestoneAmounts.get(counter)).toString());
		milestoneRuleDesc=(((milestoneRuleDescs.get(counter)) == null)?"-":(milestoneRuleDescs.get(counter)).toString());


		//JM:
		milestonePaymentType =(((milestonePaymentTypes.get(counter)) == null)?"-":(milestonePaymentTypes.get(counter)).toString());



		String milestoneRuleDescSubStr = "";

		if(milestoneRuleDesc.length() > 20) {
		   milestoneRuleDescSubStr = milestoneRuleDesc.substring(0,20);

		}
		else {
		   milestoneRuleDescSubStr = milestoneRuleDesc;
		}
		if(milestoneAmount.trim().equals("0.00")){
			milestoneAmount = MC.M_NoAssocAmount/*"No Associated Amount"*****/;
		}

		milestoneAchievedCount = (String)milestoneAchievedCounts.get(counter);
    	if (StringUtil.isEmpty(milestoneAchievedCount))
    	{
    		milestoneAchievedCount="0";
    	}
		//FIX #6231, #5924, #6338
    	intMilestoneCount = EJBUtil.stringToNum(milestoneCount);
    	intMilestoneAchievedCount = EJBUtil.stringToNum(milestoneAchievedCount);

		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneStatFK = (((milestoneStatFKs.get(counter)) == null)?(wipPkStr):(milestoneStatFKs.get(counter)).toString());

		mileStoneStat = cdStat.getCodeDescription(EJBUtil.stringToNum(milestoneStatFK));
		// 16Mar2011 @Ankit #5924
		boolean flage = achDao.checkAchievedMilestones(milestoneId, studyId);
		//KM-02Jun10-#4965
		if ( (!milestoneStatFK.equals(actPkStr) && !milestoneStatFK.equals(inactPkStr) && !flage ) || (milestoneAchievedCount.equals("0") && milestoneStatFK.equals(actPkStr) && !flage )  ){
			rowsReturned=rowsReturned +1;
		}
		milestoneDesc =milestoneRuleDesc;
			if ((counter%2)==0) {
%>
		      <tr class="browserEvenRow">
<%
			} else{
%>
		      <tr class="browserOddRow">
<%
			}
%>
		  		 	<%--SV, 11/1, no visit name for patient milestone.--%>
				<td><A onClick="openwin1(<%=milestoneId%>,'<%=pageRight%>','')" href="javascript:void(0)"><%=milestoneDesc%></A></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<!-- JM: added-->
				<td><%=mileStoneStat%></A></td>
        		<td><%=milestoneAmount%> </td>
        		<td><%=milestonePaymentType%> </td>
		          <td width =11% align="center">
    	        <%
    	        //FIX #6231, #5924
    	        int deleteOK =0;
		        if (milestoneStatFK.equals(actPkStr) && intMilestoneAchievedCount <= 0){
		        	deleteOK =1;
				}
		        if (milestoneStatFK.equals(inactPkStr)){
		        	deleteOK =1;
				}
		        if (deleteOK == 0 && (!milestoneStatFK.equals(inactPkStr) && !milestoneStatFK.equals(actPkStr))){
		        	deleteOK =1;
		        }
		        
				if (deleteOK == 1 && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) {
    	       	%>
		        <input type="checkbox" name="Del" value="<%=milestoneId%>">
		        <% }else{%>
		        <input type="hidden" name="missDel" value="<%=milestoneId%>">
		        <%} %>
		        </td>
		      </tr>
		      </tr>
<%

	} //for loop

  }//SM ends here
%>
<!--Added by Manimaran for the enhancement #FIN10 -->
<tr>
		<td>
		 <!-- Modified By parminder singh Bug#10127-->
			<B class="sectionHeadingsFrm">
				<%=LC.L_Addl_Mstones%><%--Additional Milestones*****--%>
			</B>
			</td>
			<td colspan=7 align=right>
			<A onClick="return AddMilestone(document.milestone,'patstatmilestone.jsp?studyId=<%=studyId%>&milestoneType=AM','<%=pageRight%>')" HREF="javascript:void(0)"><img title="<%=LC.L_Add_New%>" src="./images/Create.gif" border ="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>

<%

  if ((selMilestoneType.equals("AM") || selMilestoneType.equals("")) && showFlag.equals("false")){

	 milestoneDao = milestoneB.getMilestoneRows(studyId, "AM", "","", selStatus, selMilePayType, srchStrCalName.trim(), srchStrVisitName.trim(), srchStrEvtName.trim(), inActiveFlag, orderBy, orderType);
	 milestoneIds = milestoneDao.getMilestoneIds();
	 milestoneAmounts=milestoneDao.getMilestoneAmounts();
   	 milestoneUsers= milestoneDao.getMilestoneUsers();


   	 milestonePaymentTypes = milestoneDao.getMilestonePaymentTypes();

	 milestoneStatFKs = milestoneDao.getMilestoneStatFK();
	 ArrayList milestoneDescriptions = milestoneDao.getMilestoneDescriptions();
	 String milestoneDescription ="";

	len = milestoneIds.size();

	for(int counter = 0;counter<len;counter++)
	{


		milestoneId=EJBUtil.stringToNum(((milestoneIds.get(counter)) == null)?"-":(milestoneIds.get(counter)).toString());
		milestoneAmount=(((milestoneAmounts.get(counter)) == null)?MC.M_NoAssocAmount/*"No Associated Amount"*****/:(milestoneAmounts.get(counter)).toString());
		milestoneDescription=(((milestoneDescriptions.get(counter)) == null)?"-":(milestoneDescriptions.get(counter)).toString());

		milestonePaymentType =(((milestonePaymentTypes.get(counter)) == null)?"-":(milestonePaymentTypes.get(counter)).toString());//JM

		String milestoneDescSubStr ="";
		if(milestoneDescription.length() > 20) {
		   milestoneDescSubStr = milestoneDescription.substring(0,20);
		}
		else
			milestoneDescSubStr = milestoneDescription; //KM-3326

		if(milestoneDescription.length() > 100) { //KM
		   milestoneDescription = milestoneDescription.substring(0,100)+"...";
		}

		if(milestoneAmount.trim().equals("0.00")){
			milestoneAmount = MC.M_NoAssocAmount/*"No Associated Amount"*****/;
		}
		milestoneUser=(((milestoneUsers.get(counter)) == null)?"-":(milestoneUsers.get(counter)).toString());
		milestoneStatFK = (((milestoneStatFKs.get(counter)) == null)?(wipPkStr):(milestoneStatFKs.get(counter)).toString());

			if ( ! milestoneStatFK.equals(inactPkStr) ){
				rowsReturned=rowsReturned +1;
			}

		mileStoneStat = cdStat.getCodeDescription(EJBUtil.stringToNum(milestoneStatFK));

			if ((counter%2)==0) {
%>
		      <tr class="browserEvenRow">
<%
			} else{
%>
		      <tr class="browserOddRow">
<%
			}
%>

				<td><A onClick="openwin1(<%=milestoneId%>,'<%=pageRight%>','')" href="javascript:void(0)"><%=milestoneDescription%></A></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><%=mileStoneStat%></A></td>
        		<td><%=milestoneAmount%> </td>
        		<td><%=milestonePaymentType%> </td>
		          <td width =11% align="center">
		        <%if (! milestoneStatFK.equals(inactPkStr) && mileStoneStat!=null && !(mileStoneStat.equalsIgnoreCase(inactDesc))) { %>
		        <input type="checkbox" name="Del" value="<%=milestoneId%>">
		        <% }else{%>
		        <input type="hidden" name="missDel" value="<%=milestoneId%>">
		        <%} %>
		        </td>
		      </tr>
		      </tr>
<%

	} //for loop

  }//AM ends here

%>



	</table>
	<input type="hidden" name="totcount" Value="<%=rowsReturned%>"> <!-- total need to be counted... -->
	<input type="hidden" name="includeMenus" Value="<%=includeMenus%>">
    <%
        String protLinkedParams = StringUtil.htmlDecode(request.getParameter("protLinkedParams"));
    %>
	<input type="hidden" name="protLinkedParams"
		Value="<%=StringUtil.htmlEncodeXss(protLinkedParams)%>">

  </Form>


<%


	} //end of if body for page right

else

{
//KM-#4099 - Though the tab is hidden, we should not show blank page
%>

   <jsp:include page="accessdenied.jsp" flush="true"/>

  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<% if (includeMenus.equals("Y")) { %>
	<div class ="mainMenu" id="emenu">
	  	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
<%}%>
</body>

</html>

