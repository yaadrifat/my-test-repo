<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Patient Budget</title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.budgetcal.BudgetcalStateKeeper" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="budgetB" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budgetcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id="lineitemB" scope="request" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT>


function openLibprot(formobj) {
	if(formobj.mode.value=='M')
	{
	budgetId=formobj.budgetId.value;	
	
	
    windowName=window.open("bgtprotocollist.jsp?" + "budgetId=" + budgetId + "&protType=L" ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	windowName.focus();
	}	
	else
	{
	alert("Please save the budget first before attaching the calender");
	return false;
	} 
	
}


function openStudyprot(formobj) {
if(formobj.mode.value=='M')
	{
	budgetId=formobj.budgetId.value;
	studyId=formobj.budgetStudyId.value;
	if(studyId=="null"){
	
	alert("Please attach a study before selecting protocol");
	return;
	}
	
    windowName=window.open("bgtprotocollist.jsp?" + "budgetId=" + budgetId + "&protType=S&studyId=" +studyId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	windowName.focus(); 
	}else
	{
	alert("Please save the budget first before attaching the calender");
	return false;
	}		

}

function openBgtProt(formobj) {
	budgetId=formobj.budgetId.value;
	budgetStatus = formobj.budgetStatus.value;
    windowName=window.open("editbgtprotocols.jsp?" + "budgetId=" + budgetId + "&budgetStatus=" + budgetStatus ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	windowName.focus();
}

function confirmBox(lineitemName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		if (confirm("Delete " + lineitemName + " from Budget?")) {
		    return true;
		} else {
			return false;
		}
	} else { 
		return false;
	}			
}

function  validate(formobj){

	if(calculate(formobj) == false) {
		return false;
	}
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;

	if(isNaN(formobj.eSign.value) == true) {
	alert("Incorrect e-Signature. Please enter again");
	formobj.eSign.focus();
	return false;
	}
}

function openPrint(formobj){
	budgetId=formobj.budgetId.value;
	bgtcalId=formobj.bgtcalId.value;
  windowName=window.open("patientbudgetprint.jsp?&budgetId="+budgetId+"&bgtcalId="+bgtcalId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,width=600,height=460");
	windowName.focus();
}


function  calculate(formobj) {
	var totalSponsorCost = 0;
	var totalClinicCost = 0;
	var totalOtherCost = 0;
	var grandTotat = 0;
	var i = 0;
	if(formobj.rows.value == 1) {
		if(isNaN(formobj.sponsorCost.value) == true) {
			alert("Please enter a valid Sponsor Cost.");
			formobj.sponsorCost.focus();
			return false;
		}
		if(isNaN(formobj.clinicCost.value) == true) {
			alert("Please enter a valid Clinic Cost.");
			formobj.clinicCost.focus();
			return false;
		}
		if(isNaN(formobj.otherCost.value) == true) {
			alert("Please enter a valid Other Cost.");
			formobj.otherCost.focus();
			return false;
		}
		formobj.sponsorCost.value = Math.round(formobj.sponsorCost.value *100)/100;
		formobj.clinicCost.value = Math.round(formobj.clinicCost.value *100)/100;
		formobj.otherCost.value = Math.round(formobj.otherCost.value *100)/100;		

		totalSponsorCost = formobj.sponsorCost.value;
		totalClinicCost = formobj.clinicCost.value;
		totalOtherCost = formobj.otherCost.value;
	} else {
		for(i=0;i<formobj.rows.value;i++) {
			if(isNaN(formobj.sponsorCost[i].value) == true) {
				alert("Please enter a valid Sponsor Cost.");
				formobj.sponsorCost[i].focus();
				return false;
			}
			if(isNaN(formobj.clinicCost[i].value) == true) {
				alert("Please enter a valid Clinic Cost.");
				formobj.clinicCost[i].focus();
				return false;
			}
			if(isNaN(formobj.otherCost[i].value) == true) {
				alert("Please enter a valid Other Cost.");
				formobj.otherCost[i].focus();
				return false;
			}
			formobj.sponsorCost[i].value = Math.round(formobj.sponsorCost[i].value *100)/100;
			formobj.clinicCost[i].value = Math.round(formobj.clinicCost[i].value *100)/100;			
			formobj.otherCost[i].value = Math.round(formobj.otherCost[i].value *100)/100;
						
			totalSponsorCost = totalSponsorCost + parseFloat(formobj.sponsorCost[i].value);
			totalClinicCost = totalClinicCost + parseFloat(formobj.clinicCost[i].value);
			totalOtherCost = totalOtherCost + parseFloat(formobj.otherCost[i].value);
		}
	}
	if(isNaN(formobj.sponsorOHead.value) == true) {
		alert("Please enter a valid Sponsor Overhead.");
		formobj.sponsorOHead.focus();
		return false;
	}

	if(isNaN(formobj.clinicOHead.value) == true) {
		alert("Please enter a valid linic Overhead.");
		formobj.clinicOHead.focus();
		return false;
	}
		
	formobj.sponsorOHead.value = Math.round(formobj.sponsorOHead.value *100)/100;
	formobj.clinicOHead.value = Math.round(formobj.clinicOHead.value *100)/100;
					
	if(formobj.sponsorOHeadApply.checked == true){
		totalSponsorCost = parseFloat(totalSponsorCost) + ((parseFloat(totalSponsorCost) * parseFloat(formobj.sponsorOHead.value)) / 100.0);	
	}
	
	if(formobj.clinicOHeadApply.checked == true){
		totalClinicCost = parseFloat(totalClinicCost) + ((parseFloat(totalClinicCost) * parseFloat(formobj.clinicOHead.value)) / 100.0);	
	}

	formobj.totalClinicCost.value = Math.round(totalClinicCost*100)/100;
	formobj.totalSponsorCost.value = Math.round(totalSponsorCost*100)/100;
	formobj.totalOtherCost.value = Math.round(totalOtherCost*100)/100;

	grandTotal = Math.round((parseFloat(totalSponsorCost) + parseFloat(totalClinicCost) + parseFloat(totalOtherCost))*100)/100;

	formobj.grandTotal.value = grandTotal;
}
</SCRIPT>

</head>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String selectedBgtcalId = "";
String mode = request.getParameter("mode");

//variable pageMode can have initial and final as values. initial in case the page is opened from a page other than this one and final in case it is being opened from a link on this page itself
String pageMode = request.getParameter("pageMode");
if(pageMode == null) pageMode = "initial";

if(pageMode.equals("final")) {
	selectedBgtcalId = request.getParameter("bgtcalId");
	
}
int studyId = 0;
%>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include> 
<BR>
<DIV class="browserDefault" id="div1">  
<%
	HttpSession tSession = request.getSession(true); 
	String uName = (String) tSession.getValue("userName");
	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	
	if (sessionmaint.isValidSession(tSession))
	{
	
		int accId = EJBUtil.stringToNum(acc);
		int userId = EJBUtil.stringToNum(userIdFromSession);
		int budgetId = EJBUtil.stringToNum(request.getParameter("budgetId"));
		String budgetStudyId="";

		budgetB.setBudgetId(budgetId);
		budgetB.getBudgetDetails();
		budgetStudyId=budgetB.getBudgetStudyId();
		
		String budgetType = "P";
		int pageRight = 0;
		int stdRight =0;
	%>
	
	<%
		//*get user rights for the budget and put it in the session for use in other budget tabs	
	
		String rightStr = budgetUsersB.getBudgetUserRight(budgetId,userId);
		int rightLen = rightStr.length();
		
		ctrl.getControlValues("bgt_rights");

		ArrayList feature =  ctrl.getCValue();
		ArrayList ftrDesc = ctrl.getCDesc();
		ArrayList ftrSeq = ctrl.getCSeq();
		ArrayList ftrRight = new ArrayList();
		String strR;		

		for (int counter = 0; counter <= (rightLen - 1);counter ++)
		{
			strR = String.valueOf(rightStr.charAt(counter));
			ftrRight.add(strR);
		}
	
		//grpRights bean has the methods for parding the rights so use its set methods 
		grpRights.setGrSeq(ftrSeq);
    	grpRights.setFtrRights(ftrRight);		
		grpRights.setGrValue(feature);
		grpRights.setGrDesc(ftrDesc);
		
		//set the grpRights Bean in session so that it can be used in other   
		//budget pages directly to get the pageRight.
		
		tSession.putValue("BRights",grpRights);
		
		//*end of setting pageRight
			
		pageRight  = Integer.parseInt(grpRights.getFtrRightsByValue("BGTDET"));
		
		if ((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )
		{
			int i = 0;
			BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);

			ArrayList bgtNames = budgetDao.getBgtNames(); 
			ArrayList bgtStats = budgetDao.getBgtStats();
			ArrayList bgtStudyTitles = budgetDao.getBgtStudyTitles();
			ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
			ArrayList bgtSites = budgetDao.getBgtSites();
			String bgtName = (String) bgtNames.get(0);
			String bgtStat = (String) bgtStats.get(0);
			String bgtStudyTitle = (String) bgtStudyTitles.get(0);
			String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
			String bgtSite = (String) bgtSites.get(0);
			bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
			bgtStat = (bgtStat == null || bgtStat.equals(""))?"-":bgtStat;
			if(bgtStat.equals("F")) {
				bgtStat = "Freeze";
			} else if(bgtStat.equals("W")) {
				bgtStat = "Work in Progress";
			}
			bgtStudyTitle = (bgtStudyTitle == null || bgtStudyTitle.equals(""))?"-":bgtStudyTitle;
			bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
			bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;
			
			BudgetcalDao budgetcalDao = budgetcalB.getAllBgtCals(budgetId);									
			ArrayList bgtcalIds = budgetcalDao.getBgtcalIds();
			ArrayList bgtcalProtNames = budgetcalDao.getBgtcalProtNames();
			
			String currentBgtcalId = "";
			int bgtcalId = 0; 
			String protDD = "No Protocol Calendar Selected";
			if(bgtcalIds.size() > 1) {
				protDD = "<select name=bgtcalId>";
				for(i=0;i<bgtcalIds.size();i++) {
					if(pageMode.equals("initial")) {
 
						if(i == 0) {
							protDD = protDD + "<option value=" + bgtcalIds.get(i) + " selected>" + bgtcalProtNames.get(i) + "</option>";
							bgtcalId = ((Integer) bgtcalIds.get(i)).intValue();
						} else {
							protDD = protDD + "<option value=" + bgtcalIds.get(i) + ">" + bgtcalProtNames.get(i) + "</option>";
						}
					} else {
			currentBgtcalId = ((Integer) bgtcalIds.get(i)).toString();
			if(selectedBgtcalId.equals(currentBgtcalId)){
				protDD = protDD + "<option value=" + bgtcalIds.get(i) + " selected>" + bgtcalProtNames.get(i) + "</option>";
				bgtcalId = ((Integer) bgtcalIds.get(i)).intValue();
			} else {
				protDD = protDD + "<option value=" + bgtcalIds.get(i) + ">" + bgtcalProtNames.get(i) + "</option>";		
			}
		}
	}
		protDD = protDD + "</select>";
	} else {
		currentBgtcalId = ((Integer) bgtcalIds.get(0)).toString();
		bgtcalId = ((Integer) bgtcalIds.get(0)).intValue();
	}
			LineitemDao lineitemDao = lineitemB.getLineitemsOfBgtcal(bgtcalId);
			ArrayList lineitemIds = lineitemDao.getLineitemIds(); 
            ArrayList lineitemNames = lineitemDao.getLineitemNames();
            ArrayList lineitemSponsorUnits = lineitemDao.getLineitemSponsorUnits();
            ArrayList lineitemClinicNOfUnits = lineitemDao.getLineitemClinicNOfUnits();
            ArrayList lineitemOtherCosts = lineitemDao.getLineitemOtherCosts();
            ArrayList lineitemDelFlags = lineitemDao.getLineitemDelFlags();
            ArrayList bgtSectionIds = lineitemDao.getBgtSectionIds();
            ArrayList bgtSectionIntervals = lineitemDao.getBgtSectionIntervals();
            ArrayList bgtSectionVisits = lineitemDao.getBgtSectionVisits();
            ArrayList bgtSectionDelFlags = lineitemDao.getBgtSectionDelFlags();
            ArrayList bgtSectionSequences = lineitemDao.getBgtSectionSequences();

			int rows = 0;
			for(i=0;i<lineitemDao.getBRows();i++) {
				if(lineitemNames.get(i) != null && !lineitemNames.get(i).equals("")){
					rows++;
				}
			}
			
			int totalrows = lineitemDao.getBRows();

			int lineitemId = 0;
			String lineitemName = "";
			float lineitemSponsorUnit = 0;
			float lineitemClinicNOfUnit = 0;
			float lineitemOtherCost = 0;
			String lineitemDelFlag = "";
			int bgtSectionId = 0;
			String bgtSectionInterval = "";
			String bgtSectionVisit = "";
			String bgtSectionDelFlag = "";
			String bgtSectionSequence = "";
			
			int bgtSectionIdOld = 0;
			
			budgetcalB.setBudgetcalId(bgtcalId);
			BudgetcalStateKeeper bcsk = budgetcalB.getBudgetcalDetails();
			String sponsorOHead = budgetcalB.getSpOverHead();
			String sponsorOHeadApply = budgetcalB.getSpFlag();
			String clinicOHead = budgetcalB.getClOverHead();			
			String clinicOHeadApply = budgetcalB.getClFlag();
			
			sponsorOHead = (sponsorOHead == null)?"0.0":sponsorOHead;
			clinicOHead = (clinicOHead == null)?"0.0":clinicOHead;
			sponsorOHeadApply = (sponsorOHeadApply == null)?"N":sponsorOHeadApply;
			clinicOHeadApply = (clinicOHeadApply == null)?"N":clinicOHeadApply;			
%>



<P class="sectionHeadings"> Budget >> Open >> Patient </P> 

	<jsp:include page="budgettabs.jsp" flush="true">
	<jsp:param name="budgetType" value="P"/>
	<jsp:param name="mode" value="<%=mode%>"/>	  
	</jsp:include>
			
<Form name="budget" method="post" action="patientbudget.jsp?mode=<%=mode%>&budgetId=<%=budgetId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">
<%
if (pageRight == 4) {
%>
   <P class = "defComments"><FONT class="Mandatory">You have only View permission</Font></P>
<%}%>
    <input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
	
    <input type="hidden" name="selectedTab" size = 20  value = <%=selectedTab%> >
    <input type="hidden" name="mode" size = 20  value = <%=mode%> >
	<input type="hidden" name="pageMode" size = 20  value="final">
	<input type="hidden" name="budgetId" size = 20  value=<%=budgetId%>>
	<input type="hidden" name="budgetStatus" size = 20  value=<%=bgtStat%>>

	
	
	<input type="hidden" name="budgetStudyId" size = 20  value=<%=budgetStudyId%>>
	
	
	
    <table width="100%" cellspacing="0" cellpadding="0">
	    <tr> 
			<td class=tdDefault width="50%"><B> Budget Name:</B>&nbsp;&nbsp;<%=bgtName%></td>
	        <td class=tdDefault width="50%"><B> Organization:</B>&nbsp;&nbsp;<%=bgtSite%></td>
		</tr>
	    <tr> 
			<td class=tdDefault width="50%"><B> Study Number:</B>&nbsp;&nbsp;<%=bgtStudyNumber%></td>
	        <td class=tdDefault width="50%"><B> Budget Status:</B>&nbsp;&nbsp;<%=bgtStat%></td>
		</tr>			
	    <tr> 
			<td class=tdDefault width="50%"><B> Study Title:</B>&nbsp;&nbsp;<%=bgtStudyTitle%></td>
	        <td class=tdDefault width="50%" align=right><A onClick="return f_check_perm(<%=pageRight%>,'E')" HREF="budget.jsp?budgetId=<%=budgetId%>&mode=M&srcmenu=<%=src%>&selectedTab=1"><img src="../images/jpg/editdetails.gif" align="absmiddle" border="0"> </img></A></td>
		</tr>										
	</table>
	
<table>
	<tr>
		<td class=tdDefault width="50%">
			<B>
			
				Select a Protocol Calendar </B> <%=protDD%>				
<%				
				if(!protDD.equals("No Protocol Calendar Selected")){
%>				
				<button type="submit"><%=LC.L_Search%></button>
<%
				}
%>				
		</td>
        <td class=tdDefault width="25%">
			<A HREF="#" onClick="return calculate(document.bgtprot);">Refresh Calculations</A>
		</td>
 	<td class=tdDefault width="25%">
			<A HREF="#" onclick="openPrint(document.bgtprot)">Export to Excel/Print</A>
		</td>
	<tr>
</table>
		

<table width="600" cellspacing="2" cellpadding="2">
	<tr><td>
	<%if ((!(bgtStat ==null)) && (bgtStat.equals("Freeze"))&&(mode.equals("M"))) {%>
    	<P class = "defComments"><FONT class="Mandatory">Budget Status is Freeze. You cannot make any changes to the budget.</Font></P>
	<%} else {%>	
	<table>
		<tr>
		  <td>&nbsp</td>
		<td>	
		  <A href="#" onclick="openStudyprot(document.budget)" >Add Protocol Calender from study</A>  
		  </td>
		<td>&nbsp;&nbsp;&nbsp</td>
		<td>
    	<A href="#" onclick="openLibprot(document.budget)">Add Protocol Calender from Library</A>
		</td>
		<td>&nbsp;&nbsp;&nbsp</td>
		<td>
    	<A href="#" onclick="openBgtProt(document.budget)">Delete Selected Protocol Calendars</A>
		</td>
		</tr>
		</table>
	<%}%>
	
	</td></tr>
</table>


</FORM>

<Form name="bgtprot" method=post action="savepatientbudget.jsp"  onSubmit="return validate(document.bgtprot);">
<input type=hidden name=rows value=<%=rows%>>
<input type=hidden name=mode value=<%=mode%>>
<input type=hidden name=srcmenu value=<%=src%>>
<input type=hidden name=selectedTab value=1>
<input type=hidden name=budgetId value=<%=budgetId%>>
<input type=hidden name=bgtcalId value=<%=bgtcalId%>>
<input type=hidden name=budgetType value=<%=budgetType%>>


<table class = tableDefault width="100%" cellspacing="3" cellpadding="0" border=0>
<tr> 
      <td class=tdDefault colspan=4> </td>
	  <td class=tdDefault align=right>
			<A onClick="return f_check_perm(<%=pageRight%>,'E')" HREF="budgetsections.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&budgetType=<%=budgetType%>&selectedTab=<%=selectedTab%>&calId=<%=bgtcalId%>&mode=<%=mode%>&bgtStat=<%=bgtStat%>">Edit Sections</A>
		</td>						
	</tr>
	<tr>
		<th width=35% align =center>Event</th>
	    <th width=20% align =center>Sponsor Cost</th>
  	    <th width=15% align =center>Clinic Cost</th>
  	    <th width=15% align =center>Other Cost</th>
  	    <th width=15% align =center>Line item</th>				
	</tr>

<%
	bgtSectionIdOld = ((Integer) lineitemIds.get(0)).intValue();

	for(i=0;i<totalrows;i++) {
	
		lineitemId = ((Integer) lineitemIds.get(i)).intValue();
		lineitemName = (String) lineitemNames.get(i);
		lineitemSponsorUnit = EJBUtil.stringToFloat((String) lineitemSponsorUnits.get(i));
		lineitemClinicNOfUnit = EJBUtil.stringToFloat((String) lineitemClinicNOfUnits.get(i));
		lineitemOtherCost = EJBUtil.stringToFloat((String) lineitemOtherCosts.get(i));
		lineitemDelFlag = (String) lineitemDelFlags.get(i);
		bgtSectionId = ((Integer) bgtSectionIds.get(i)).intValue();
		bgtSectionInterval = (String) bgtSectionIntervals.get(i);
		bgtSectionVisit = (String) bgtSectionVisits.get(i);
		bgtSectionDelFlag = (String) bgtSectionDelFlags.get(i);
		bgtSectionSequence = (String) bgtSectionSequences.get(i);
		String itemId = EJBUtil.integerToString(new Integer(lineitemId));
		if(bgtSectionIdOld != bgtSectionId) {
%>
		<tr>
			<td><B><%=bgtSectionInterval%>,&nbsp;<%=bgtSectionVisit%></B>
			</td>
			<td colspan=3>&nbsp;
			</td>
			<td align=right>
			<%if (!(bgtStat.equals("Freeze"))) {%>

				<A onClick="return f_check_perm(<%=pageRight%>,'N')" HREF="additempatient.jsp?srcmenu=<%=src%>&bgtcalId=<%=bgtcalId%>&budgetType=<%=budgetType%>&mode=N&budgetId=<%=budgetId%>&bgtSectionId=<%=bgtSectionId%>&bgtSectionInterval=<%=bgtSectionInterval%>&bgtSectionVisit=<%=bgtSectionVisit%>&selectedTab=<%=selectedTab%>&bgtStat=<%=bgtStat%>&lineitemMode=N">Add New</A>
			<%}%>
			</td>
		</tr>
<%		
		}
	if(lineitemName != null && !lineitemName.equals("")) {		

	 	   if ((i%2)==0) {
%>
      <tr class="browserEvenRow"> 
<%
	 		   }else{
%>
      <tr class="browserOddRow"> 
<% 
	 		   } 
%>
			<td>
			<A HREF="additempatient.jsp?srcmenu=<%=src%>&bgtcalId=<%=bgtcalId%>&budgetType=<%=budgetType%>&mode=M&budgetId=<%=budgetId%>&bgtSectionId=<%=bgtSectionId%>&bgtSectionInterval=<%=bgtSectionInterval%>&bgtSectionVisit=<%=bgtSectionVisit%>&selectedTab=<%=selectedTab%>&bgtStat=<%=bgtStat%>&lineitemId=<%=itemId%>&lineitemName=<%=lineitemName%>&lineitemMode=N"><%=lineitemName%></A>
			</td>
			<td align=center>
				<input type=text name=sponsorCost size=10 maxlength=10 class="rightAlign" value=<%=lineitemSponsorUnit%>>
			</td>
			<td align=center>
				<input type=text name=clinicCost size=10 maxlength=10 class="rightAlign" value=<%=lineitemClinicNOfUnit%>>
			</td>
			<td align=center>
				<input type=text name=otherCost size=10 maxlength=10 class="rightAlign" value=<%=lineitemOtherCost%>>
			</td>
			<td>
			<%if (!(bgtStat.equals("Freeze"))) {%>
				<A onclick="return confirmBox('<%=lineitemName%>',<%=pageRight%>)" HREF="lineitemdelete.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetId=<%=budgetId%>&lineitemId=<%=lineitemId%>&bgtcalId=<%=bgtcalId%>&budgetType=<%=budgetType%>">Delete</A>
			<%}%>
			</td>
		</tr>
		<input type=hidden name=lineitemId value=<%=lineitemId%>>											
<%		
	}
		bgtSectionIdOld = bgtSectionId;
		
	}
	
	
	if (((totalrows)%2)==0) {
%>
      <tr class="browserEvenRow"> 
<%
	 		   }else{
%>
      <tr class="browserOddRow"> 
<% 
	 		   } 
%>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>Total/Patient</B>				
			</td>
			<td align=center>
				<input type=text name=totalSponsorCost size=10 class="rightAlign" value=0.0 readonly>
			</td>
			<td align=center>
				<input type=text name=totalClinicCost size=10 class="rightAlign" value=0.0 readonly>
			</td>
			<td align=center>
				<input type=text name=totalOtherCost size=10 class="rightAlign" value=0.0 readonly>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>		
<%		
	if (((totalrows+1)%2)==0) {
%>
      <tr class="browserEvenRow"> 
<%
	 		   }else{
%>
      <tr class="browserOddRow"> 
<% 
	 		   } 
%>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>Grand Total</B>				
			</td>
			<td>
				&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
			<td align=center>
				<input type=text name=grandTotal size=10 class="rightAlign" value=0.0 readonly>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>				
</table>
<table>
	<tr>
		<td class=tdDefault width=20%>
			<B>
				Sponsor Overhead
			</B> 
		</td>
		<td class=tdDefault width=80%>
			<B>
				<input type=text name=sponsorOHead class="rightAlign" value=<%=sponsorOHead%> maxlength=5 size=3>&nbsp;%&nbsp;
<%
	if(sponsorOHeadApply.equals("N")) {
%>				
				<input type=checkbox name=sponsorOHeadApply>&nbsp;&nbsp;
<%
	} else {
%>		
				<input type=checkbox name=sponsorOHeadApply checked>&nbsp;&nbsp;		
<%
	}
%>				
				Apply to calculation
			</B>
		</td>
	</tr>
	<tr>
		<td class=tdDefault width=20%>
			<B>
				Clinic Overhead
			</B> 
		</td>
		<td class=tdDefault width=80%>
			<B>				 
				<input type=text name=clinicOHead class="rightAlign" value=<%=clinicOHead%> maxlength=5 size=3>&nbsp;%&nbsp;
<%
	if(clinicOHeadApply.equals("N")) {
%>				
				<input type=checkbox name=clinicOHeadApply>&nbsp;&nbsp;
<%
	} else {
%>		
				<input type=checkbox name=clinicOHeadApply checked>&nbsp;&nbsp;		
<%
	}
%>
				Apply to calculation
			</B>
		</td>
	</tr>
</table>


<%
if ((!((!(bgtStat ==null)) && (bgtStat.equals("Freeze"))&&(mode.equals("M")))) && (pageRight>=6)){%>


<TABLE width=100% cellspacing="0" cellpadding="0" >
	<tr>
		<td width=50%></td>
		<td width=35%>
			e-Signature <FONT class="Mandatory">* </FONT> 
			<input type="password" name="eSign" maxlength="8" SIZE=15 >
	 	 </td>
		 <td width=15%>
		 	<input type=image src="../images/jpg/Submit.gif" onClick=submit border="0">
		 </td>
	</tr>	
</TABLE>

<%}%>	

</Form>

<script>
	calculate(document.bgtprot);
</script>
			
<%	}	
else {
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
 } //end of else body for page right
    
	}//end of if body for session
   	else
	{
	%>
<BR>
<DIV class="formDefault" id="div1"> 
	<jsp:include page="budgettabs.jsp" flush="true"> 
	<jsp:param name="budgetType" value="P"/>
	<jsp:param name="mode" value="<%=mode%>"/>	  
	</jsp:include>	

	<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div>
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</DIV>
	
<div class ="mainMenu" id="emenu"> 
  	<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</html>

