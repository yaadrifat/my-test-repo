<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title><%=ES.ES_ViewBlk_FldMapp %><%--View Bulk Field Mapping *****--%></title>
<script>
</script>
</head>
<%@ page autoFlush="true" buffer="1094kb"%>   
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<%@page import="com.velos.eres.bulkupload.business.*,java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page  language = "java" import="com.velos.eres.bulkupload.business.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<body onload="clearcon();">
  <%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
String logPk="";
String createdOn="";
String userName="";
String toRecords="";
String succRecords="";
String unSuccRecords="";
logPk=request.getParameter("logPk");
createdOn=request.getParameter("createdOn");
userName=request.getParameter("userNm");
toRecords=request.getParameter("totReds");
succRecords=request.getParameter("succRec");
unSuccRecords=request.getParameter("unSuccRec");
ArrayList errorMessage=null;
ArrayList fileRowNumber=null;
String userId="";
userId = (String) tSession.getValue("userId");
%> 
	 <table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0">
	 <%if(!StringUtil.isEmpty(logPk)){
	 bulkUploadDao bulkUplDao=new bulkUploadDao();
	bulkUplDao.getUploadLogs(userId,StringUtil.stringToNum(logPk));
	errorMessage=bulkUplDao.getErrorMsg();
	fileRowNumber=bulkUplDao.getRowNum();
	 %>
	 <tr class="tmpHeight"><td><font size="3"><b><%=ES.ES_Upld_Num %><%--Upload Number *****--%> :<%=logPk%></b></font></td> </tr>
	 <tr class="tmpHeight"><td><font size="3"><b><%=ES.ES_TotFile_Rec %><%--Total File records *****--%>:<%=toRecords%></b></font></td> </tr>
	 <tr class="tmpHeight"><td><font size="3"><b><%=ES.ES_RecUpld_Succ %><%--Number of records uploaded successfully *****--%>:<%=succRecords%></b></font></td> </tr>
	 <tr class="tmpHeight"><td><font size="3"><b><%=ES.ES_RecNot_UpldSucc %><%--Number of records not uploaded *****--%>:<%=unSuccRecords%></b></font><br/></td> </tr>
	 <%if(StringUtil.stringToNum(unSuccRecords)>0){%>
	 <%for (int errSize=0;errSize<errorMessage.size();errSize++){%>
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<tr class="tmpHeight"><td><font size="3"><%=ES.ES_Row_Num %><%--Row Number *****--%>:<%=fileRowNumber.get(errSize)%></font></td> </tr>
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<tr class="tmpHeight"><td><font size="3"><%=ES.ES_Faiure_Reason %><%--Reason for failure *****--%>:<p><%=errorMessage.get(errSize)%></p></font></td> </tr>
	 <%}%>
	 <%}%>
	  </table>
	  <%}%>
  <%

}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <% 
}
%>

</body>
</html>