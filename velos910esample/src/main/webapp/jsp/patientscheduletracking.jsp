<%-- Used by patientschedule.jsp to track a session variable --%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil"%>
<%!
private final static String LastSelVisit = "lastSelVisit";
%>
<%
	HttpSession tSession = request.getSession(false);
	if (sessionmaint.isValidSession(tSession)) {
	    int fkVisit = EJBUtil.stringToNum(request.getParameter("visitId"));
	    if (fkVisit == 0) { fkVisit = -1; }
	    if (fkVisit > 0) {
	        tSession.setAttribute(LastSelVisit, new Integer(fkVisit));
	    } else {
	        tSession.removeAttribute(LastSelVisit);
	    }
	}
%>
