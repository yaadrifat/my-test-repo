<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_PatOrComp_Bgt%><%--<%=LC.Pat_Patient%>/ Comparative Budget*****--%></title>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.*,com.velos.eres.business.common.ReportDaoNew,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.budgetcal.impl.BudgetcalBean"%>

<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="budgetcalB" scope="request"	class="com.velos.esch.web.budgetcal.BudgetcalJB" />
<jsp:useBean id="lineitemB" scope="request"	class="com.velos.esch.web.lineitem.LineitemJB" />
<jsp:useBean id="ctrl" scope="request"	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="grpRights" scope="request"	class="com.velos.eres.web.grpRights.GrpRightsJB" />
<jsp:useBean id="codeLst" scope="request"	class="com.velos.esch.business.common.SchCodeDao" />
<jsp:useBean id="budgetUsersB" scope="request"	class="com.velos.esch.web.budgetUsers.BudgetUsersJB" />
<jsp:useBean id="userB" scope="request"	class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="grpB" scope="request"	class="com.velos.eres.web.group.GroupJB" />
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>



function openBudReport(formobj,budgetId,bgtcalId)
{

	reppk = formobj.dBudReport.value;

	var w = formobj.dBudReport.selectedIndex;
	var repName = formobj.dBudReport.options[w].text;

	win = window.open("budrepretrieve.jsp?repId="+reppk +"&budgetPk="+budgetId+"&repName="+repName+"&protId="+bgtcalId,'BudgetReport','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');


}

</script>
</head>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String selectedBgtcalId = "";
String mode = request.getParameter("mode");

//variable pageMode can have initial and final as values. initial in case the page is opened from a page other than this one and final in case it is being opened from a link on this page itself
String pageMode = request.getParameter("pageMode");
if(pageMode == null) pageMode = "initial";

// includedIn will be passed as P when this page is included in Protocol Calendar tabs to show the default budget

String includedIn = request.getParameter("includedIn");
if(StringUtil.isEmpty(includedIn))
{
 includedIn = "";
}

//*** Added specifically for combined budget
String from = request.getParameter("from");
if(StringUtil.isEmpty(from))
{
	from = "";
}
//***

if(pageMode.equals("final")) {
	selectedBgtcalId = request.getParameter("bgtcalId");

}
int studyId = 0;
String topdivClass="";
String bottomdivClass="";
String tabFormTopNClass="";
%>

<% if ( includedIn.equals("P")) {

	topdivClass="";
	bottomdivClass="popDefault";
	tabFormTopNClass="";
	%>
<jsp:include page="include.jsp" flush="true"/>
<body>
<% } else
	{

	topdivClass="tabDefTopN";
	tabFormTopNClass="tabFormTopN";
	bottomdivClass="tabFormBotN";

	%>

	<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
    <% } %>
	<div id="overDiv"
	style="position:absolute; visibility:hidden; z-index:1000;"></div>

<% if (! includedIn.equals("P")) { %>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>" />
</jsp:include>
<% } %>

<script language="JavaScript" src="budget.js"></script>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"> <!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"> <!-- overLIB (c) Erik Bosrup --></script>
<%
	HttpSession tSession = request.getSession(true);

	String budgetStudyId="";
	String bgtStat="", budgetTotalTotalCost="",budgetTotalCost="";
	SchCodeDao schD = new SchCodeDao();
	int perCodeId = schD.getCodeId("category","ctgry_per");
	//String budgetTemplate;
	
	
	//JM: 10FEB2011: #D-FIN9
	SchCodeDao schStatDao = new SchCodeDao();
	
	
	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
		String userIdFromSession = (String) tSession.getValue("userId");
		String acc = (String) tSession.getValue("accountId");


		int accId = EJBUtil.stringToNum(acc);
		int userId = EJBUtil.stringToNum(userIdFromSession);
		int budgetId = EJBUtil.stringToNum(request.getParameter("budgetId"));
		String budgetTemplate = request.getParameter("budgetTemplate");
		budgetTemplate=(budgetTemplate==null)?"":budgetTemplate;
	%>
<DIV class="<%=topdivClass%>" id="div1">
		<%
  	    if(budgetId>0)
  	    {
	  	    budgetB.setBudgetId(budgetId);
			budgetB.getBudgetDetails();
			budgetStudyId=budgetB.getBudgetStudyId();
			budgetTemplate = budgetB.getBudgetType();
		}

  	     %>
  	     <% if (! includedIn.equals("P")) { %>

			<jsp:include page="budgettabs.jsp" flush="true">
				<jsp:param name="mode" value="<%=mode%>" />
				<jsp:param name="studyId" value="<%=budgetStudyId%>" />
			</jsp:include></DIV>
		<% } %>

<DIV class="<%=tabFormTopNClass%>" id="div1" style="oveflow:none;"><%
	if(budgetId==0) {
	%> <jsp:include page="budgetDoesNotExist.jsp" flush="true" /> <%

	   } else {


		//copy budget is controlled by budget new group right
		GrpRightsJB budgetGrpRights = (GrpRightsJB) tSession.getValue("GRights");
		int budgetGrpRight = Integer.parseInt(budgetGrpRights.getFtrRightsByValue("BUDGT"));


		int pageRight = 0;
		int stdRight =0;

		userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
		String defaultGrp=userB.getUserGrpDefault();
		grpB.setGroupId(EJBUtil.stringToNum(defaultGrp));
		grpB.getGroupDetails();

		//*get user rights for the budget and put it in the session for use in other budget tabs
		String rightStr = budgetUsersB.getBudgetUserRight(budgetId,userId);
		int rightLen = rightStr.length();

		if (rightLen==0)
		{
		rightStr=grpB.getGroupSupBudRights();
		rightLen=rightStr.length();
		}

		ctrl.getControlValues("bgt_rights");

		ArrayList feature =  ctrl.getCValue();
		ArrayList ftrDesc = ctrl.getCDesc();
		ArrayList ftrSeq = ctrl.getCSeq();
		ArrayList ftrRight = new ArrayList();
		String strR;

		for (int counter = 0; counter <= (rightLen - 1);counter ++)
		{
			strR = String.valueOf(rightStr.charAt(counter));
			ftrRight.add(strR);
		}

		//grpRights bean has the methods for parding the rights so use its set methods
		grpRights.setGrSeq(ftrSeq);
    	grpRights.setFtrRights(ftrRight);
		grpRights.setGrValue(feature);
		grpRights.setGrDesc(ftrDesc);

		//set the grpRights Bean in session so that it can be used in other
		//budget pages directly to get the pageRight.

		tSession.putValue("BRights",grpRights);

		//*end of setting pageRight

		pageRight  = Integer.parseInt(grpRights.getFtrRightsByValue("BGTDET"));


		if ((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )
		{
			int i = 0;
			int j = 0;
			int sectionLineItemCount = 0;
			int prevSecLICount = 0;//SV, 8/30/04, added as part of fix for #1706.
				String budgetStatusDesc = "";

			BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);

			ArrayList bgtNames = budgetDao.getBgtNames();
			ArrayList bgtStats = budgetDao.getBgtStats();
			ArrayList bgtStudyTitles = budgetDao.getBgtStudyTitles();
			ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
			ArrayList bgtSites = budgetDao.getBgtSites();
			ArrayList bgtCurrs = budgetDao.getBgtCurrs();

			ArrayList bgtStatDescs= budgetDao.getBgtCodeListStatusesDescs();

			String bgtName =(String) bgtNames.get(0);
			bgtStat = (String) bgtStats.get(0);

			budgetStatusDesc = (String) bgtStatDescs.get(0);

			String bgtStatCode = (String) bgtStats.get(0);
			String bgtStudyTitle = (String) bgtStudyTitles.get(0);
			String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
			String bgtSite = (String) bgtSites.get(0);


			String currency = "";

			String bgtName_Disp = "";
			String bgtStudyNumber_Disp = "";
			String bgtSite_Disp = "";
			//String bgtStudyTitle_Disp = "";

			if (bgtCurrs.size() >0){

				String bgtCurr = (String) bgtCurrs.get(0);


				if(bgtCurr==null)
					bgtCurr ="0";
				int codeId = Integer.parseInt(bgtCurr);

				currency = codeLst.getCodeDescription(codeId);


			}


			boolean sectionFlag = true;

			bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
			bgtStat = (bgtStat == null || bgtStat.equals(""))?"-":bgtStat;
			if(bgtStat.equals("F")) {
				bgtStat = LC.L_Freeze/*"Freeze"*****/;
			} else if(bgtStat.equals("W")) {
				bgtStat = LC.L_Work_InProgress/*"Work in Progress"*****/;
			}else if(bgtStat.equals("T")) {
				bgtStat = LC.L_Template/*"Template"*****/;
				}

			bgtStudyTitle = (bgtStudyTitle == null || bgtStudyTitle.equals(""))?"-":bgtStudyTitle;
			bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
			bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;
			BudgetcalDao budgetcalDao = budgetcalB.getAllBgtCals(budgetId);
			ArrayList bgtcalIds = budgetcalDao.getBgtcalIds();
			ArrayList bgtcalProtNames = budgetcalDao.getBgtcalProtNames();
			ArrayList bgtcalProtIds = budgetcalDao.getBgtcalProtIds();
			String currentBgtcalId = "";
			int bgtcalId = 0;
			String selectedBudgetProtocolID = "";
			String studyWithSelectedBudgetProtocolID  = "";
			String selectedCalendarStatus = "";

			if(bgtcalIds.size() > 1){
				currentBgtcalId = ((Integer) bgtcalIds.get(1)).toString();
				bgtcalId = ((Integer) bgtcalIds.get(1)).intValue();
				selectedBudgetProtocolID = (String) bgtcalProtIds.get(1);
			}

			String protDD = MC.M_NoCalSelected/*"No Calendar Selected"*****/;
			if(bgtcalIds.size() > 1) {


				protDD = "<select name=bgtcalId style='width:177px;'>";
				for(i=0;i<bgtcalIds.size();i++) {
					if (!bgtcalProtIds.get(i).equals("0")) {
						if(pageMode.equals("initial")) {
							if(i == 0) {
								protDD = protDD + "<option value=" + bgtcalIds.get(i) + " selected>" + bgtcalProtNames.get(i) + "</option>";
								bgtcalId = ((Integer) bgtcalIds.get(i)).intValue();
								selectedBudgetProtocolID = (String) bgtcalProtIds.get(i);

							} else {
								protDD = protDD + "<option value=" + bgtcalIds.get(i) + ">" + bgtcalProtNames.get(i) + "</option>";
							}
						} else {
							currentBgtcalId = ((Integer) bgtcalIds.get(i)).toString();
							if(selectedBgtcalId.equals(currentBgtcalId)){
								protDD = protDD + "<option value=" + bgtcalIds.get(i) + " selected>" + bgtcalProtNames.get(i) + "</option>";
								bgtcalId = ((Integer) bgtcalIds.get(i)).intValue();

								selectedBudgetProtocolID = (String) bgtcalProtIds.get(i);


							} else {
								protDD = protDD + "<option value=" + bgtcalIds.get(i) + ">" + bgtcalProtNames.get(i) + "</option>";
							}
						}
					}
				} //for loop
				protDD = protDD + "</select>";
			 } else {
				currentBgtcalId = ((Integer) bgtcalIds.get(0)).toString();
				bgtcalId = ((Integer) bgtcalIds.get(0)).intValue();
				selectedBudgetProtocolID = (String) bgtcalProtIds.get(0);
			 }

			 	eventassocB.setEvent_id(EJBUtil.stringToNum(selectedBudgetProtocolID));
			 	eventassocB.getEventAssocDetails();
			 	studyWithSelectedBudgetProtocolID = eventassocB.getChain_id();
			 	
			 	//JM: 10FEB2011: #D-FIN9
			 	//selectedCalendarStatus = eventassocB.getStatus();			 	 
			 	selectedCalendarStatus = schStatDao.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));

			 	if (StringUtil.isEmpty(studyWithSelectedBudgetProtocolID))
			 	{
			 		studyWithSelectedBudgetProtocolID = "0";
			 		selectedCalendarStatus = "";
			 	}



%>

<Form name="budget" method="post"	action="patientbudget.jsp?mode=<%=mode%>&budgetId=<%=budgetId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetTemplate=<%=budgetTemplate%>">
<%
if (pageRight == 4) {
%>
<P class="defComments"><FONT class="Mandatory"><%=MC.M_YouOnly_ViewPerm%><%--You have only View
permission*****--%></FONT></P>
<%}%> <input type="hidden" name="srcmenu" size=20 value=<%=src%>> <input
	type="hidden" name="selectedTab" size=20 value=<%=selectedTab%>> <input
	type="hidden" name="mode" size=20 value=<%=mode%>> <input
	type="hidden" name="pageMode" size=20 value="final"> <input
	type="hidden" name="budgetId" size=20 value=<%=budgetId%>> <input
	type="hidden" name="budgetStatus" size=20 value=<%=bgtStat%>> <input
	type="hidden" name="budgetStudyId" size=20 value=<%=budgetStudyId%>>
	<input type="hidden" name="currentBgtcalId" size=20 value=<%=currentBgtcalId%>>
	<input type="hidden" name="selectedBudgetProtocolID" size=20 value=<%=selectedBudgetProtocolID%>>
	<input type="hidden" name="studyWithSelectedBudgetProtocolID" size=20 value=<%=studyWithSelectedBudgetProtocolID%>>
	<input type="hidden" name="selectedCalendarStatus" size=20 value=<%=selectedCalendarStatus%>>


<% if (! includedIn.equals("P")) { %>
<table width="102%" cellspacing="0" cellpadding="0" border="0">

	<tr>
		<td width="15%"><font size="1"><B> <%=LC.L_Budget_Name%><%--Budget Name*****--%>:</B>&nbsp;&nbsp;
		<%
		 //JM: 14Aug2008: #3727
			if(bgtName.length() > 27){
		   		bgtName_Disp = bgtName.substring(0,27);
		   		bgtName_Disp = bgtName_Disp + "...";
		   	%>
			<a href="#" onmouseover="return overlib('<%=bgtName%>',CAPTION,'<%=LC.L_Budget_Name%><%--Budget Name*****--%>:' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtName_Disp%></a>
			<%}else{%>
			<%=bgtName%>
		<%}%>

		</font></td>

		<td width="15%"><font size="1"><B> <%=LC.L_Bgt_Status%><%--Budget Status*****--%>:</B>&nbsp;&nbsp;<%=budgetStatusDesc%></font></td>
		<td width="15%"><font size="1"><B> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:</B>&nbsp;&nbsp;
		<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=budgetStudyId%>">

		<%
	        //JM: 14Aug2008: #3727
			if(bgtStudyNumber.length() > 27){
		   		bgtStudyNumber_Disp = bgtStudyNumber.substring(0,27);
		   		bgtStudyNumber_Disp = bgtStudyNumber_Disp + "...";
		   		%>
		   	<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=budgetStudyId%>" onmouseover="return overlib('<%=bgtStudyNumber%>',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtStudyNumber_Disp%>
			<%}else{%>
			<%=bgtStudyNumber%>
			<%}%>
			</a></font>
		<img src="../images/jpg/help.jpg" border="0"
		onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(bgtStudyTitle)%>',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:' ,LEFT , ABOVE );"
		onmouseout="return nd();" alt=""></img>

		</td>

		<td width="15%"><font size="1"><B> <%=LC.L_Organizations%><%--Organization*****--%>:</B>&nbsp;&nbsp;</font>

		<%
	        //JM: 14Aug2008: #3727
			if(bgtSite.length() > 27){
		   		bgtSite_Disp = bgtSite.substring(0,27);
		   		bgtSite_Disp = bgtSite_Disp + "...";
		   		%>
		   	<a href="#" onmouseover="return overlib('<%=bgtSite%>',CAPTION,'<%=LC.L_Organizations%><%--Organization*****--%>:' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtSite_Disp%></a>
			<%}else{%>
	        	<%=bgtSite%>
			<%}%>
			</td>
				<% if (! includedIn.equals("P")) { %>
		<td align="center" valign="top" width="15%">
	<font size = -2 color="#696969"><b><%=LC.L_Copy%><%--Copy*****--%></b></font><BR>
			<A onClick="return f_check_perm(<%=pageRight%>,'N')"
			href="copybudget.jsp?from=initial&srcmenu=<%=src%>&budgetId=<%=budgetId%>"><img src="./images/format.gif" alt="<%=LC.L_Copy_Bgt%><%--Copy Budget*****--%>" width="16" height="18" border = 1 align=absbotton></A></td>

		<td align="center" valign="top" width="20%">

		<%
			//for reports dropdown

			ReportDaoNew repDao = new ReportDaoNew();
			repDao.getReports("pat_bud",accId);
			String strBudRep = "";

			strBudRep = EJBUtil.createPullDownWithStrNoSelect("dBudReport","",repDao.getPkReport(),repDao.getRepName());


		%>
			<font size = -2 color="#696969"><b><%=LC.L_Reports%><%--Reports*****--%></b></font><BR>
			<%=strBudRep%>
			<A href="#"	onClick="return openBudReport(document.budget,<%=budgetId%>,<%=bgtcalId%>);">
			<img src="../images/jpg/displayreport.jpg" align="absmiddle" border="0"	alt=""></A>



		</td>
		<% } %>

	</tr>
	<!-- <tr>
		<td colspan="4"><font size="1"><B> <%=LC.Std_Study%> Title:</B>&nbsp;&nbsp;<%if (bgtStudyTitle.length() > 125){%><%=bgtStudyTitle.substring(1,125)%>...<%}else {%><%=bgtStudyTitle%><%}%></font></td>

	</tr> -->

</table>
<% } //if includedin=P
	%>


<table width="100%" border="0">
	<tr bgcolor="#dcdcdc" >
<% if (includedIn.equals("P")) { %>
	<td class=tdDefault width="25%"><b><%=LC.L_Budget_Name%><%--Budget Name*****--%>:</b> &nbsp; <%=bgtName%></td>
	<td class=tdDefault  width="20%"><B> <%=LC.L_Bgt_Status%><%--Budget Status*****--%>:</B>&nbsp;&nbsp;<%=budgetStatusDesc%></td>
	<td class=tdDefault width="25%"><A href="#" onclick="openBudget(<%=budgetId%>,'P','<%=from%>')"><%=LC.L_Edit_Dets%><%--Edit Details*****--%></A></td>
<% } %>
<% if (! includedIn.equals("P")) { %>

		<td class=tdDefault width="25%"><B> <%if(bgtcalIds.size() > 1) {%>
		<%=LC.L_Select_Cal%><%--Select Calendar*****--%> </B> <BR><%}%> <%=protDD%><%
				if(bgtcalIds.size() > 1) {
%> <button type="submit"><%=LC.L_Search%></button> <%
				}
%> <%if (!((!(bgtStat ==null)) && ( bgtStat.equals("Freeze") || bgtStat.equals("Template")   )&&(mode.equals("M")))) {%>

		<td align="center" valign="top" width="10%">
	<font size = -2 color="#696969"><b><%=LC.L_Sel_StdCal%><%--Select <%=LC.Std_Study%> Cal*****--%></b></font><BR>
			<A href="#" onclick="openStudyprot(document.budget, <%=pageRight%>)"><img src="./images/studysearch.jpg" alt="<%=MC.M_SelCal_Std%><%--Select Calendar from <%=LC.Std_Study%>*****--%>" width="16" height="18" border = 1 align=absbotton></A>
		</td>

			<td align="center" valign="top" width="10%">
	<font size = -2 color="#696969"><b><%=LC.L_Select_LibCal%><%--Select Lib. Cal*****--%></b></font><BR>
	<A href="#" onclick="openLibprot(document.budget, <%=pageRight%>,'','','','','','')"><img src="./images/library.gif" alt="<%=MC.M_SelCal_Lib%><%--Select Calendar from Library*****--%>" width="16" height="18" border = 1 align=absbotton></A>
			</td>

		<td align="center" valign="top" width="10%">
	<font size = -2 color="#696969"><b><%=LC.L_Del_Selected%><%--Delete Selected*****--%></b></font><BR>
	<A href="#" onclick="openBgtProt(document.budget, <%=pageRight%>)"><img src="./images/disableformat.gif" alt="<%=LC.L_Del_Selected%><%--Delete Selected*****--%>" width="16" height="18" border = 1 align=absbotton></A>


		</td>
		<%}%>
<% } //include in P %>

	<td align="center" valign="top" width="7%">
	<font size = -2 color="#696969"><b><%=LC.L_Sections%><%--Sections*****--%></b></font><BR>
		<A HREF="budgetsections.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&selectedTab=<%=selectedTab%>&calId=<%=bgtcalId%>&mode=<%=mode%>&bgtStat=<%=bgtStat%>">
		<img src="./images/section.gif" alt="<%=MC.M_AddEdt_Sec%><%--Add and Edit Sections*****--%>" width="16" height="18" border = 1 align=absbotton></A></td>

  <% if (! includedIn.equals("P")) { %>
	<td align="center" valign="top" width="7%">
	<font size = -2 color="#696969"><b><%=LC.L_Repeating%><%--Repeating*****--%></b></font><BR>
		<A href="#" onClick="openRepeatLineItems(document.budget,'<%=mode%>',<%=bgtcalId%>,'<%=bgtStatCode%>','<%=budgetTemplate%>')">
		<img src="./images/note.gif" alt="<%=MC.M_AddEdt_RepLine%><%--Add and Edit Repeating Line Items*****--%>" width="16" height="18" border = 1 align=absbotton></A></td>

	<td align="center" valign="top" width="10%">
	<font size = -2 color="#696969"><b><%=LC.L_Personnel_Costs%><%--Personnel Costs*****--%></b></font><BR>
		<A href="#" onClick="openEditPersonnelCostWin(document.budget,'<%=mode%>',<%=bgtcalId%>,'<%=bgtStatCode%>','<%=budgetTemplate%>')">
		<img src="./images/patientsearch.jpg" alt="<%=MC.M_AddEdt_Personnel%><%--Add and Edit Personnel Costs*****--%>" width="16" height="18" border = 1 align=absbotton></A></td>
<% } //for includeIN=P%>

		<!-- Added by gopu to fix the bugzilla Issue #2391 on 21-04-2006 -->
		<%if (!(bgtStat.equals("Freeze") || bgtStat.equals("Template"))) {%>
	<td align="center" valign="top" width="10%">
	<font size = -2 color="#696969"><b><%=LC.L_Exclude_Multiple%><%--Exclude Multiple*****--%></b></font><BR>
			<A href="#" onClick="openDelete(<%=bgtcalId%>,<%=pageRight%>)"><img src="./images/delete.gif" alt="<%=LC.L_Exclude_Multiple%><%--Exclude Multiple*****--%>" width="16" height="18" border = 1 align=absbotton></A>
		<%} else{%>
		<td align="center" valign="top" width="10%">
	<font size = -2 color="#696969"><b><%=LC.L_Hide_Multi%><%--Hide Multiple*****--%></b></font><BR>
			<A href="#" onClick="return fnMessage(); "><img src="./images/delete.gif" alt="<%=LC.L_Del_Multi%><%--Delete Multiple*****--%>" width="16" height="18" border = 1 align=absbotton></A> <%	}%>


		<td align="center" valign="top" width="10%">
	<font size = -2 color="#696969"><b><%=LC.L_Create_Mstones%><%--Create Milestones*****--%></b></font><BR>
			<A href="#" onclick="openMile(document.budget, <%=pageRight%>)"><img src="./images/velosbudgeting.gif" alt="<%=LC.L_Create_Mstones%><%--Create Milestones*****--%>" width="16" height="18" border = 1 align=absbotton></A>
			</td>


		<%// sv, 08/10, fix for bug #1456, f_check_parm on pageRight %>
		<!--	<td class=tdDefault width="15%"> <A onClick="return f_check_perm(<%=pageRight%>,'N')" href="copybudget.jsp?from=initial&srcmenu=<%=src%>&budgetId=<%=budgetId%>">Copy this Budget</A></td>	-->
		<!--	 <A HREF="#" onclick="openPrint(document.bgtprot, 'P')">Print Budget</A> -->

		<%if(protDD.equals("No Calendar Selected")){%>
		<!-- <td align=left><B>
		<P class="defComments">Baseline Budget</P>
		</B></td> -->
		<%}%>
<!--	</tr>
</table> -->



<%if ((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template") )&&(mode.equals("M"))) { Object[] arguments = {budgetStatusDesc}; %>
<!-- <table width="98%" cellspacing="0" cellpadding="0">
	<tr> -->
		<td>
		<P class="defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=budgetStatusDesc%>'.
		You cannot make any changes to the budget.*****--%></FONT></P>
		</td>
	</tr>
</table>


<%}%>

<table width="100%">
		<TR>
		<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER"><%=LC.L_Event%><%--Event*****--%></TH>
		<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER"><%=LC.L_Category%><%--Category*****--%></TH>
		<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER"><%=LC.L_Cost_Type%><%--Cost Type*****--%></TH>
		<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER"><%=LC.L_Unit_Cost%><%--Unit Cost*****--%></TH>
		<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER" title="<%=LC.L_Number_OfUnits%><%--Number of Units*****--%>"><%=LC.L_Units%><%--Units*****--%></TH>
		<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER" title="<%=MC.M_Apply_DcntOrMarkup%><%--Apply Discount OR Markup*****--%>"><%=LC.L_DOrM%><%--D/M*****--%></TH>
		<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER"><%=LC.L_Direct_CostOrPat%><%--Direct Cost/<%=LC.Pat_Patient%>*****--%></TH>
		<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER"><%=LC.L_IOrD_Applied%><%--I/D Applied*****--%></TH>
		<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER"><%=LC.L_IOrD_Amount%><%--I/D Amount*****--%></TH>
		<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER"><%=LC.L_Total_CostOrPat%><%--Total Cost/<%=LC.Pat_Patient%>*****--%></TH>
		<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER"><%=LC.L_CostOrAll_Pat%><%--Cost/ All <%=LC.Pat_Patients%>*****--%></TH>


		<%
  	      if ( budgetTemplate.equals("C") ){
 	    %>

			<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER"><%=LC.L_Sponsor_Amount%><%--Sponsor Amount*****--%></TH>
			<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER"><%=LC.L_Variance%><%--Variance*****--%></TH>

		<% } %>

	</tr>
</table>
</DIV>

</FORM>

<DIV class="<%=bottomdivClass%>" id="div1">


<Form name="bgtprot" id="budgetForm" method=post action="savepatientbudget.jsp">


<input type=hidden
	name=mode value=<%=mode%>>
	<input type=hidden name=srcmenu
	value=<%=src%>>
	<input type=hidden name=selectedTab
	value=<%=selectedTab%>>
	<input type=hidden name=budgetId
	value=<%=budgetId%>>
	<input type=hidden name=bgtcalId
	value=<%=bgtcalId%>>
	<input type=hidden name=budgetTemplate
	value=<%=budgetTemplate%>>

<input type="hidden" name="includedIn" value="<%=includedIn%>">

	<%


String ctgryPullDn = "";
SchCodeDao schDao = new SchCodeDao();
schDao.getCodeValues("category");
%>



<BR>
	<BR>



<jsp:include page="budrepretrieve.jsp" flush="true">
	<jsp:param name="repId" value="110" />
	<jsp:param name="budgetPk" value="<%=budgetId%>" />
	<jsp:param name="repName" value="" />
	<jsp:param name="protId" value="<%=bgtcalId%>" />
	<jsp:param name="mode" value="M" />
	<jsp:param name="budgetTemplate" value="<%=budgetTemplate%>" />
	<jsp:param name="pageRight" value="<%=pageRight%>" />
	<jsp:param name="bgtStat" value="<%=bgtStat%>" />
	<jsp:param name="includedIn" value="<%=includedIn%>" />

</jsp:include>


<input type="hidden" name="perCodeId" value="<%=perCodeId%>">

</Form>



<%	}
else {
%> <jsp:include page="accessdenied.jsp" flush="true" /> <%
 } //end of else body for page right
 }//budgetexisits
 }//end of if body for session
   	else
	{
	%> <jsp:include page="timeout.html" flush="true" /> <%
	}
	%>

<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</DIV>

<% if (! includedIn.equals("P")) { %>


<div class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp"
	flush="true" /></div>

<% } %>
</body>


</html>

