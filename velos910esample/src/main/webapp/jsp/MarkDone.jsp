<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_Event_Status%><%--Event Status*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:include page="include.jsp" flush="true"/>
</HEAD>

<script>
function test(formobj) {

//-------------JM: 18Feb2008------------

	var total_count_rows ;

 	total_count_rows = formobj.countRows.value;


 	if (total_count_rows>1){
 	cnt = formobj.selRole.length;

	for(i = 0; i<cnt ; i++)
	{
		id = formobj.selRole[i].value;

		if ( id > 0)
		{

				if(isNaN(formobj.durationDD[i].value) == true)
				{
				alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationDD[i].focus();
				return false;
				}

				if(isNaN(formobj.durationHH[i].value) == true)
				{
					alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationHH[i].focus();
				return false;
				}

				if(isNaN(formobj.durationMM[i].value) == true)
				{
					alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationMM[i].focus();
				return false;
				}

				if(isNaN(formobj.durationSS[i].value) == true)
				{
					alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationSS[i].focus();
				return false;
				}

		}
		else
		{

				durationDD = parseInt(  (formobj.durationDD[i].value)  ) ;
				durationHH = parseInt(  (formobj.durationHH[i].value ) ) ;
				durationMM =parseInt( ( formobj.durationMM[i].value ) ) ;
				durationSS	= parseInt( (  formobj.durationSS[i].value) ) ;

				dd = false ; hh = false ; mm=false ; ss = false ;
				ddO = false ; hhO = false ; mmO =false ; ssO = false ;

				if (    isNaN(durationDD)   )
				{

					if (  !formobj.durationDD[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationDD[i].focus();
						 return false ;
					 }
					dd = true;


				}
				if (   isNaN(durationHH)  )
				{
					if (  !formobj.durationHH[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationHH[i].focus();
						return false ;
					}
					hh = true ;

				}

				if ( isNaN( durationMM  ) )
				{

					if (  !formobj.durationMM[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationMM[i].focus();
						return false ;
					}

					mm = true ;

				}
				if ( isNaN(  durationSS  ) )
				{

					if (  !formobj.durationSS[i].value=="" )
					{
						alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
						formobj.durationSS[i].focus();
						return false ;
					}

					ss = true ;

				}


				if ( durationDD > 0)
				{
					ddO = true;

				}
				if ( durationHH > 00)
				{
					hhO = true ;
				}
				if ( durationMM > 00  )
				{
					mmO = true ;
				}
				if ( durationSS > 00)
				{
					ssO= true ;
				}





				if  (  ddO || hhO || mmO ||   ssO )
				{

				alert("<%=MC.M_Check_TheRole%>");/*alert("Please check the role");*****/
				formobj.roleId[i].focus( );
				return false;

				}

				if  (     (dd || !ddO)  && (hh || !hhO) && ( mm || !mmO )  && ( ss || !ssO )  )
				{


				}


			} //else of id ==0


	}


	}
	else if (total_count_rows == 1)
	{
	   if(isNaN(formobj.durationDD.value) == true)
				{
		   alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationDD.focus();
				return false;
				}

				if(isNaN(formobj.durationHH.value) == true)
				{
			alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationHH.focus();
				return false;
				}

				if(isNaN(formobj.durationMM.value) == true)
				{
				alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationMM.focus();
				return false;
				}

				if(isNaN(formobj.durationSS.value) == true)
				{
				alert("<%=MC.M_InvalidNum_EtrAgain%>");/*alert("Invalid Number.Please enter again");*****/
				formobj.durationSS.focus();
				return false;
				}

	}



//---------JM: 18Feb2008 ---------------------------


	//z = window.document.MarkDone.mode.value;
    mode = formobj.mode.value;

	if (!(validate_col('Actual Date',formobj.actualDt))) return false
	if (!(validate_col('eventstatus',formobj.eventstatus))) return false
 	if (!(validate_col('e-Signature',formobj.eSign))) return false



 	//JM: 11Jun2010: Enh-#D-FIN23
 	if (formobj.ddCoverage!= undefined){
 		if (formobj.eventCoverageType.value != formobj.ddCoverage.value){

	 		if (fnTrimSpaces(formobj.reasonForCoverageChange.value)=="" && formobj.ddCoverage.value !=''){

	 			alert("<%=MC.M_EtrReason_ForChg%>");/*alert("Please enter the Reason for Change in Coverage Type");*****/
	 			return false;
	 		}
 		}
 	}

	var fdaVal = document.getElementById("fdaStudy").value;
	if (fdaVal == "1"){
		if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
	}

     if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }

		userId=formobj.userId.value;

		notes=formobj.notes.value;
		actualDt=formobj.actualDt.value;


		protocolId=formobj.protocolId.value;
		eventId=formobj.eventId.value;

		pkey=formobj.pkey.value;
		eSign=formobj.eSign.value;
	//	window.opener.document.location = "MarkEventDone.jsp?actualDt=" + actualDt+ //"&userId="  + userId  +"&notes="  + notes +"&protocolId="  + protocolId  +"&eventId="  + //eventId +"&pkey="  + pkey + "&eSign=" + eSign;

	//window.close();
return true;
}

//JM: 25Jun2010, #5058, function to selectively enable/disable the reason for change in coverage type ..
function callChangeInCovergeType(formobj){

	//KM-#5859
	if(formobj.eventCoverageType.value =='0' && formobj.ddCoverage.value == '') {
		formobj.reasonForCoverageChange.value = '';
		formobj.reasonForCoverageChange.disabled = true;
	}
	else if ((formobj.eventCoverageType.value != formobj.ddCoverage.value) && formobj.ddCoverage.value !=''){

	 	document.getElementById('reasonForCoverageChange').disabled=false;
	 	formobj.attrib_text.value="enabled";

	}else if ((formobj.eventCoverageType.value != formobj.ddCoverage.value) && formobj.eventCoverageType.value =='0'){

		document.getElementById('reasonForCoverageChange').disabled=true;
		formobj.attrib_text.value="disabled";

	}else if ((formobj.eventCoverageType.value != formobj.ddCoverage.value) && formobj.ddCoverage.value ==''){

		document.getElementById('reasonForCoverageChange').disabled=false;
		formobj.attrib_text.value="enabled";

	}else{

		document.getElementById('reasonForCoverageChange').disabled=true;
		formobj.attrib_text.value="disabled";
	}

}
</script>


<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="EventuserB" scope="request" class="com.velos.esch.web.eventuser.EventuserJB"/>
<jsp:useBean id="eventResB" scope="request" class="com.velos.esch.web.eventresource.EventResourceJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />

<DIV class="popDefault" id="div1">

<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
		 <jsp:include page="include.jsp" flush="true"/>
<%
  String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	int accountId=0;
	accountId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));

	String studyId = (String) tSession.getValue("studyId");
	studyB.setId(EJBUtil.stringToNum(studyId));
    studyB.getStudyDetails();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

	//JM: fkEventId is used to get sch_eventusr.fk_Event
	String fkEventId = "";
	fkEventId = request.getParameter("fk_eventId");

	//JM: eventId refers to the SCH_EVENTS1.event_id
	String eventId = "";
	eventId = request.getParameter("event_id");
	int iEventId = EJBUtil.stringToNum(eventId);

	CodeDao codeDao = new CodeDao();
	codeDao.getCodeValues("site_type","service");

	ArrayList siteType = new ArrayList();
	siteType = codeDao.getCId();

	String strSiteType = EJBUtil.ArrayListToString(siteType);
	SiteDao siteDao = siteB.getBySiteTypeCodeId(accountId,strSiteType);

	String ddSOS ="";
	String ddCoverage ="";

	SchCodeDao schDao = new SchCodeDao();
	schDao = new SchCodeDao();
	schDao.getCodeValues("coverage_type");

	int eventSOSId = 0;
	int eventCoverageType =0;

	String mode = request.getParameter("mode");

	if (StringUtil.isEmpty(mode))
	{
		mode="M";
	}



	String schDate = request.getParameter("schDate");
	schDate = (schDate==null)?"":schDate;


	//JM: #3823: 28Oct2008
	String stDate = request.getParameter("startdt");
	stDate = (stDate==null)?"":stDate;
		/*if (stDate!="")
		{
			stDate= stDate.toString().substring(5,7) + "/" + stDate.toString().substring(8,10) + "/" + stDate.toString().substring(0,4);
		}*/

	//String eventName = request.getParameter("eventName");

	//KM-15Sep09
	String org_id = request.getParameter("fk_eventId");
	String calledFrom = request.getParameter("calledFrom");



	//JM: 28Jun2010, #5061
	String calassoc=request.getParameter("calAssoc");
	calassoc=(calassoc==null)?"":calassoc;


	String eventName="";

	if (calledFrom.equals("P")){

  		  	eventdefB.setEvent_id(EJBUtil.stringToNum(org_id));
			eventdefB.getEventdefDetails();
		    eventName = eventdefB.getName();
	 }else{

			eventassocB.setEvent_id(EJBUtil.stringToNum(org_id));
			eventassocB.getEventAssocDetails();
			eventName = eventassocB.getName();
	 }

	EventAssocDao assocDao = new EventAssocDao();
	assocDao.getPatSchEvent_SOS_CoverType(iEventId);
	
	ArrayList tpArray = new ArrayList();
	tpArray = assocDao.getEventSOSIds();
    eventSOSId = (tpArray == null || tpArray.size()==0)?0:EJBUtil.stringToNum(""+tpArray.get(0));
    tpArray = new ArrayList(); 
    tpArray = assocDao.getEventCoverageTypes();
	eventCoverageType = (tpArray == null || tpArray.size()==0)?0:EJBUtil.stringToNum(""+tpArray.get(0));

	ddSOS=siteDao.toPullDown("ddSos",eventSOSId, " ");
	//ddCoverage = schDao.toPullDown("ddCoverage",eventCoverageType," ");




	if (eventName ==null){
		eventName="";
	}

	String notes="";
	String ex_on="";
	String reasonForCoverageCng = "";

	java.util.ArrayList notes_list = new ArrayList();
	java.util.ArrayList exeons = new ArrayList();
	java.util.ArrayList reasonForCoverageChanges =new ArrayList();

	EventdefDao eventdefDao = eventdefB.FetchSchedule(EJBUtil.stringToNum(eventId));

	if (mode.equals("M")){
		//EventdefDao eventdefDao = eventdefB.FetchSchedule(EJBUtil.stringToNum(eventId));
		notes_list = eventdefDao.getNotes();
		exeons=eventdefDao.getEvent_exeons();

	}

	if (mode.equals("M")){
		notes=((notes_list.get(0)) == null)?"":(notes_list.get(0)).toString();
			java.sql.Date exedate = (java.sql.Date)exeons.get(0);


		if(exedate !=null){

			ex_on = DateUtil.dateToString(exedate);
		}

	}

	reasonForCoverageChanges = eventdefDao.getReasonForCoverageChange();
	reasonForCoverageCng = 	((reasonForCoverageChanges.get(0)) == null)?"":(reasonForCoverageChanges.get(0)).toString();


%>
<Form class=formDefault name="MarkDone" id="markDoneFrm" action="MarkEventDone.jsp" method="post" onSubmit="if (test(document.MarkDone)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<% String statDesc = request.getParameter("statDesc");%>
<% String statid = request.getParameter("statid");
	SchCodeDao cd = new SchCodeDao();
	String dCur = "";
	cd.getCodeValues("eventstatus",0);
	cd.setCType("eventstatus");
	cd.setForGroup(defUserGroup);


//JM: 15Jul2008: #3586
 boolean found_val = false;
 String codeSubType ="";
 SchCodeDao schcdDao = new SchCodeDao();

 if (!statid.equals("") && mode.equals("M")){
	found_val = schcdDao.getCodeValuesById(EJBUtil.stringToNum(statid));
	if (found_val){

		ArrayList codeSubTypes = null;
		codeSubTypes = (schcdDao.getCSubType());
		codeSubType = codeSubTypes.get(0).toString();
		if (codeSubType.equals("null")){
			codeSubType ="";
		}
	}
 }



	//JM: 02Jul2008, #3586
	//dCur =  cd.toPullDown("eventstatus",EJBUtil.stringToNum(statid));
 	if(mode.equals("M")){

	 	dCur =  cd.toPullDown("eventstatus",EJBUtil.stringToNum(statid), "disabled");
	}
 	else{
		dCur =  cd.toPullDown("eventstatus");

 	}





//	ex_on = request.getParameter("exe_on");
	if (ex_on==null || ex_on.toUpperCase().equals("NULL"))
	{
		ex_on = "";
	}




%>
<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>>
<INPUT NAME="statDesc" TYPE=hidden   value="<%=statDesc%>">
<INPUT NAME="statid" TYPE=hidden  value=<%=statid%>>
<INPUT NAME="eventId" TYPE=hidden  value=<%=eventId%>>
<INPUT NAME="protocolId" TYPE=hidden  value=<%=request.getParameter("protocol_id")%>>
<INPUT NAME="pkey" TYPE=hidden  value=<%=request.getParameter("pkey")%>>
<INPUT NAME="eventCoverageType" TYPE="hidden"  value=<%=eventCoverageType%>>
<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">





<%
	String userIdFromSession = (String) tSession.getValue("userId");
%>
<table >
	<tr>
		<td colspan = 2>
			<P class="sectionHeadings"> <%=LC.L_Event_Status%><%--Event Status*****--%> </P>
		<td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center"><%=LC.L_Event_Name_Upper%><%--EVENT NAME*****--%>: <font class=mandatory></font></td>
		<td ><%=eventName%> <font class=mandatory></font></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td class=tdDefault align="right"><%=LC.L_Event_Status%><%--Event Status*****--%>:<font class=mandatory>*</font> &nbsp;&nbsp;&nbsp;</td>
		<td>	<%=dCur%> </td>
	</tr>
	<tr>
		<td></td>
		<td colspan=2 id="tdComments"></td>
	</tr>
	<tr>
		<td class=tdDefault align="right" width=30%><%=LC.L_Status_ValidFrom%><%--Status Valid From*****--%>:<font class=mandatory>*</font>&nbsp;&nbsp;&nbsp;</td>
		<td class=tdDefault>
	<% if (mode.equals("M") && (!(codeSubType.trim().equalsIgnoreCase("ev_notdone")) )
		&& (!codeSubType.equals("")) ){%>
<%-- INF-20084 Datepicker-- AGodara --%>
		<INPUT type=text name=actualDt class="datefield" READONLY size=10 value=<%=ex_on%>>
	<% } else if (mode.equals("M") && (codeSubType.trim().equalsIgnoreCase("ev_notdone")) && (ex_on.equals(""))){
			%>
		<INPUT type=text name=actualDt class="datefield" READONLY size=10 value=<%=stDate%>>
	<%
	} else if (mode.equals("M") && (codeSubType.trim().equalsIgnoreCase("ev_notdone")) && (!ex_on.equals(""))){%>
		<INPUT type=text name=actualDt class="datefield" READONLY size=10 value=<%=ex_on%>>
	<% } else if (mode.equals("N")){%>
		<INPUT type=text name=actualDt class="datefield" READONLY size=10 value="" >
	<% }%>
		</td>
	</tr>

	<% if (!calassoc.equals("S")){%>
	<tr>
		<td class=tdDefault align="right" width=30%><%=LC.L_Site_OfService%><%--Site of Service*****--%>: &nbsp;&nbsp;&nbsp;</td>
		<td class=tdDefault>
			 <%=ddSOS%>
		</td>
	</tr>
	<tr>
		<td class=tdDefault align="right" width=30%><%=LC.L_Coverage_Type%><%--VelosResourceBundle.getLabelString("Evt_CoverageType")*****--%>: &nbsp;&nbsp;&nbsp;</td>
		<td class=tdDefault>
<%

	StringBuffer ddCoverageBuffer = new StringBuffer();

	ddCoverage = schDao.toPullDown("ddCoverage",eventCoverageType," ");

	int len = ddCoverage.length();

	int strIndex = ddCoverage.indexOf(">");

	String strSel =  ddCoverage.substring(0, strIndex);

	//strSel = strSel + " "+ "onchange=document.getElementById('reasonForCoverageChange').disabled=false >";
	strSel = strSel + " "+ "onchange=callChangeInCovergeType(document.MarkDone); >";
	ddCoverageBuffer.append(strSel);
	String strApp = ddCoverage.substring(strIndex+1, len);
	ddCoverageBuffer.append(strApp);
	ddCoverage = ddCoverageBuffer.toString();


%>

			 <%=ddCoverage%>
		</td>
	</tr>

	<!--JM: 10June2020: D-FIN23 -->

	<tr>
		<td class=tdDefault align="right" width=30%><%=MC.M_ReasonChg_CvgType %><%-- <%=VelosResourceBundle.getLabelString("Reason_For_Change_CT")%>*****--%>: &nbsp;&nbsp;&nbsp;</td>
		<td class=tdDefault>
			 <textarea id="reasonForCoverageChange" name="reasonForCoverageChange" rows=2 cols=30 disabled><%=reasonForCoverageCng%></textarea>
			 <!--JM: 25Jun2010: #5060-->
			 <input type="hidden" name="reasonForCoverageChangeTxt" value="<%=reasonForCoverageCng%>">
			 <input type = "hidden" name="attrib_text" value="disabled">
		</td>
	</tr>
	<%}%>
	<tr>
		<td> <INPUT type=hidden name=userId value=<%=userIdFromSession%>> </td>
	</tr>
	<%--tr>
		<td class=tdDefault align="right">
		<td class=tdDefault>
		<!-- <p class = "defComments"><font class=mandatory>
		<i>When Event Status is Changed to 'Done', this date is the 'Execution Date' of the Event.</i></font></p> -->
		&nbsp;
		</td>
	</tr> --%>
	<tr>
		<td class=tdDefault valign=top align="right"><%=LC.L_Notes%><%--Notes*****--%>:&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 	<td class=tdDefault>
			<TEXTAREA id=notes name=notes rows=3 cols=30><%=notes%></TEXTAREA>
		</td>
	</tr>
	<tr>
      <td class=tdDefault valign=top align="right"><%=LC.L_ReasonForChangeFDA%>
      	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
      </td>
	  <td class=tdDefault><textarea id="remarks" name="remarks" rows="3" cols="30" MAXLENGTH="4000"></textarea></td>
  	</tr>


<%


	ArrayList evtResTrackIds = null;
	ArrayList evtStats = null;

    ArrayList cDescs = null;
    ArrayList cIds = null;
    ArrayList cDuration = null;

    String evtResTrackId = "";
    String evtStat = "";
    String cDesc = "";
	int cId = 0;
	String cRoleDur = null;

	//JM: 14Feb2008: added
	EventResourceDao evResDao = eventResB.getAllRolesForEvent(EJBUtil.stringToNum(eventId));

	   evtResTrackIds = evResDao.getEvtResTrackIds();
	   evtStats = evResDao.getEvtStats();
	   cIds = evResDao.getEvtRoles();
	   cDuration = evResDao.getEvtTrackDurations();
	   cDescs = evResDao.getResourceNames();

	   int cRows = cIds.size();


if (cRows > 0){
%>
<!--JM: 13Feb2008: enh. #FIN12-->

	<tr></tr><tr></tr>
	<tr>
	<td colspan = 2>
	<P class="sectionHeadings"> <%=MC.M_AssignResrc_TimeSpent%><%--Assigned Resources and Time Spent*****--%> </P>

	<td>
	</tr>

	<tr><td>&nbsp;</td></tr>


	<TR>

	 <td class=tdDefault width=30% align =center><b><%=LC.L_Role_Type%><%--Role Type*****--%></b></td>
     	<td class=tdDefault width=30% align =center><b><%=MC.M_ActualSpt_DdHhMmSs%><%--Actual Time Spent (DD:HH:MM:SS)*****--%></b></td>

	</TR>


<% }
		int fidx = 0;
		int midx = 0;
		int lidx = 0;


	for (int j=0; j<cRows; j++){


		String ddval = "00";
		String hhval = "00";
   		String mmval = "00";
   		String ssval = "00";

		cDesc = (String) cDescs.get(j);
    	cId =  EJBUtil.stringToNum(((String) cIds.get(j)));
		cRoleDur = (String) cDuration.get(j);
		evtResTrackId = (String) evtResTrackIds.get(j);
	   if (!(cRoleDur == null) && ! (cRoleDur.equals("null")) && cRoleDur.trim().length() > 0)
	{


		// get the dd, hh, mm, ss values

		fidx = 	cRoleDur.indexOf(":");
		midx = 	cRoleDur.indexOf(":",fidx+1);
		lidx = 	cRoleDur.lastIndexOf(":");

		ddval = cRoleDur.substring(0,fidx);
		hhval = cRoleDur.substring(fidx+1,midx);
		mmval = cRoleDur.substring(midx + 1,lidx);
		ssval = cRoleDur.substring(lidx+1);



	}


%>
<input type="hidden" name="selRole" value=<%=cId%>>
<input type="hidden" name="trackingIds" value=<%=evtResTrackId%>>

<TR>
	 <td class=tdDefault>
	 <%=cDesc%></td>


	 	<td align =center>
		<input type="text" name="durationDD" maxlength = 2 SIZE = 2 value="<%=ddval%>">:
		<input type="text" name="durationHH" maxlength = 2 SIZE = 2 value="<%=hhval%>">:
		<input type="text" name="durationMM" maxlength = 2 SIZE = 2 value="<%=mmval%>">:
		<input type="text" name="durationSS" maxlength = 2 SIZE =2 value="<%=ssval%>">
		</td>
	 </TR>



	<%

	}//end of for loop
	%>

	     </table>


<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="markDoneFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>



<input type="hidden" name="countRows" value="<%=cRows%>">

	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
</div>
<div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

  <br>

