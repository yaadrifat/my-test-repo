<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>

<%@ page language="java" import="com.velos.eres.business.common.SettingsDao,com.velos.eres.business.common.GroupDao,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.velos.eres.service.util.StringUtil"%>


<body>
<DIV>
<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	
%>	
	<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
 	if (sessionmaint.isValidSession(tSession))

   {
	   String eSign= request.getParameter("eSign");
	   String oldESign = (String) tSession.getValue("eSign");
	   String accId = (String) tSession.getValue("accountId");
	   accId=(accId==null)?"":accId;
	   String ipAdd = (String) tSession.getValue("ipAdd");
	   String usr = (String) tSession.getValue("userId");
	   if(oldESign.equals(eSign)) 
	   {
	   
	   	
	   	ArrayList purgeList=new ArrayList();	
	   	
		int ret=0,grpId=0;
		String groupSupRights="";
	   	String tarea=request.getParameter("tareapk");
		tarea=(tarea==null)?"":tarea;
		String tareaPkStr=(request.getParameter("tareasettingpk"));
		tareaPkStr=(tareaPkStr==null)?"0":tareaPkStr;
		
		int tareaPk=EJBUtil.stringToNum(tareaPkStr);
		
		
		String studyDiv=request.getParameter("study_divisionpk");
		studyDiv=(studyDiv==null)?"":studyDiv;
		String divPkStr=request.getParameter("divsettingpk");
		divPkStr=(divPkStr==null)?"0":divPkStr;
		int divPk=EJBUtil.stringToNum(divPkStr);
		
		String diseaseSite=request.getParameter("disease_sitepk");
		diseaseSite=(diseaseSite==null)?"":diseaseSite;
		String diseasePkStr=request.getParameter("diseasesettingpk");
		diseasePkStr=(diseasePkStr==null)?"0":diseasePkStr;
		
		int diseasePk=EJBUtil.stringToNum(diseasePkStr);
		
		String groupId=request.getParameter("groupid");
		groupId=(groupId==null)?"":groupId;
		
		String type=request.getParameter("type");
		type=(type==null)?"":type;
		
		if (type.equals("STUDY"))
		{
		grpId = Integer.parseInt(groupId);
		groupB.setGroupId(grpId);
		groupB.getGroupDetails();
		groupSupRights=groupB.getGroupSuperUserRights();
		}
		
		//prepare a list to remove previous values
		if ((tareaPk>0) && (tarea.length()==0))
		 purgeList.add(tareaPkStr);
		 
		 if ((divPk>0) && (studyDiv.length()==0))
		 purgeList.add(divPkStr);
		 
		 if ((diseasePk>0) && (diseaseSite.length()==0))
		  purgeList.add(diseasePkStr);
		
		SettingsDao settingsDao=commonB.getSettingsInstance();
		
		//set object for therapeutic area
		if (tarea.length()>0)
		{
			settingsDao.setSettingPK(tareaPkStr);
			settingsDao.setSettingsModName("4");
			settingsDao.setSettingsModNum(groupId);
			settingsDao.setSettingValue(StringUtil.replace(tarea,";",","));
			settingsDao.setSettingKeyword(type+"_TAREA");
			settingsDao.setSettingCreator(usr);
			settingsDao.setSettingIpadd(ipAdd);
			settingsDao.setSettingModifiedBy(usr);
			}
		
		//set object for Study Division area
		if (studyDiv.length()>0)
		{
			settingsDao.setSettingPK(divPkStr);
			settingsDao.setSettingsModName("4");
			settingsDao.setSettingsModNum(groupId);
			settingsDao.setSettingValue(StringUtil.replace(studyDiv,";",","));
			settingsDao.setSettingKeyword(type+"_DIVISION");
			settingsDao.setSettingCreator(usr);
			settingsDao.setSettingIpadd(ipAdd);
			settingsDao.setSettingModifiedBy(usr);
		}
		
		//set object for Study Division area
		if (diseaseSite.length()>0)
		{
			settingsDao.setSettingPK(diseasePkStr);
			settingsDao.setSettingsModName("4");
			settingsDao.setSettingsModNum(groupId);
			settingsDao.setSettingValue(StringUtil.replace(diseaseSite,";",","));
			settingsDao.setSettingKeyword(type+"_DISSITE");
			settingsDao.setSettingCreator(usr);
			settingsDao.setSettingIpadd(ipAdd);
			settingsDao.setSettingModifiedBy(usr);
			}
		
		if (purgeList.size()>0)
		ret=settingsDao.purgeSettings(purgeList);
		
		ret=settingsDao.saveSettings();
		
		//Settings are saved 
		
		/* commented by sonia 10.22.07, changed super user implementation
		if (ret>=0)
		{
		 GroupDao gDao=groupB.getGroupDaoInstance();
		 ret=gDao.revokeSupUser(grpId,accId);
		 if (ret>=0) ret=gDao.grantSupUser(grpId,accId,usr, ipAdd, groupSupRights);
		} 
		*/
		
		if (ret>=0)
		{%>
		<script>
		this.window.close();
		</script>
		<%}else {%>
		
		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_DataCnt_SvdSucc%><%-- Data could not be saved successfully.*****--%></p>
		<button onClick = "self.close()"><%=LC.L_Close%></button>
		<%}%>
		
		
		
		
	   
	 
  
 	
<%	  
	}else{//end of else of incorrect of esign 
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%}
  }//end for sessionmaint

}else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</body>
</html>
