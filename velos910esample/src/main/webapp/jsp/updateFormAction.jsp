<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Update_FormAction%><%--Update Form Action*****--%></title> 

<%@ page import="com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.common.*, com.velos.eres.business.formAction.*,com.velos.eres.web.formAction.* " %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body>
<br>
<DIV id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{

%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");

		String param = "";
		String paramVal = "";
		String paramName = "";
		String mode= "";
		String actionMsg="";
		int ret = 0;
		String targetFormId ="";
		String targetField = "";
		
		FormActionJB fajb = new FormActionJB();  
		System.out.println("In updateformAction.jsp");
		
	   if(oldESign.equals(eSign)) 
	   {
		    String formId = "";
		    String actionType = "";
			String actionCondition  = "";
			String sourceField = "";
			String fieldActionId = "";
			String formActionId = "";
			String ipAdd = (String) tSession.getValue("ipAdd");
	        String usr = (String) tSession.getValue("userId");
		    String accountId = (String) tSession.getValue("accountId");
    		   
			formId = request.getParameter("formId");
			actionType = request.getParameter("actionType");
			actionCondition = request.getParameter("actionCondition");
			
			fieldActionId =   request.getParameter("fieldActionId");
			mode = request.getParameter("mode");
//
			formActionId = request.getParameter("formActionId");
//	
			targetFormId = request.getParameter("targetFormId");
			 //actionType = request.getParameter("actionType");
			 actionCondition = request.getParameter("actionCondition");
			 sourceField =	   request.getParameter("sourceFldDD");
			 targetField =   request.getParameter("veltargetid");
			 actionMsg=request.getParameter("actMessage");
			 actionMsg=(actionMsg==null)?"":actionMsg;
			 //fieldActionId =   request.getParameter("fieldActionId");
			 
			 //mode = request.getParameter("mode");
			 fajb.setFormActionOperator(actionCondition);
			 fajb.setFormActionSourceFormId(Integer.parseInt(formId));
			 fajb.setFormActionTargetFormId(Integer.parseInt(targetFormId));
			 fajb.setFormActionSourceFld(sourceField);
			 fajb.setFormActionTargetFld(targetField);
			 fajb.setFormActionMsg(actionMsg);
			 fajb.setFormActionName("");//060408ML this field is not used, set to empty string 060408ML
			 fajb.setFormActionFilledFormId(0);
			 
			 
			 if(mode.equals("N")){
				 fajb.setFormActionId(Integer.parseInt("0"));//new form action
				 fajb.setFormActionDetails();
	   			}
			 else if (mode.equals("M")){
				fajb.setFormActionId(Integer.parseInt(formActionId ));
	   			ret = fajb.updateFormAction();
			 }
		  	//end of 0604ML
			 /*
				if (paramVal.indexOf("[VELSEP]") < 0)
				  {		
					 fsk.setFldId(sourceField );
				  } 
				 
				 fsk.setFldActionInfo(htFldActionInfo);
				 
				 if (mode.equals("N"))
				 {
					 fsk.setCreator(usr);
				 }
				 else
				 {
				
				 	 fsk.setFldActionId(EJBUtil.stringToNum(fieldActionId ));
				 	 fsk.setModifiedBy(usr);
				 	
				 }
				 
				 
				*/
			// save the field Action
				
				//ret = fldActionDao.setFieldActionData(fsk,mode);
				
				if (ret>=0) {
					
					//generate the action script
					//fldActionDao.generateFieldActionsScript(formId);		
				%>
					
				<BR><BR><BR><BR><BR>
				<P class="successfulmsg" align="center"><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%></P>
				<script>
					window.opener.location.reload();
					setTimeout("self.close()",1000);
				</script>

				<% } else {%>		
				<BR><BR><BR><BR><BR>
				<P class="successfulmsg" align="center"><%=MC.M_Data_NotSvdSucc%><%--Data not Saved Successfully*****--%></P>
				
				<%
					}
			
	   }//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
		}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>
</div>
</body>
</html>
