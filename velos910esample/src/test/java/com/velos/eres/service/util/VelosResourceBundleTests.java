package com.velos.eres.service.util;


import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class VelosResourceBundleTests {

    @Before
    public void setUp() throws Exception {
        if (System.getProperty("ERES_HOME") == null) {
            System.setProperty("ERES_HOME", "D:\\work\\eResearch_v62\\conf\\eDevN\\");
        }
    }
    
    @Test
    public void getString() {
        assertTrue("Coverage Type".equals(
                VelosResourceBundle.getString(VelosResourceBundle.LABEL_BUNDLE, LabelConst.Evt_CoverageType)));
    }

    @Test
    public void getLabelString() {
        System.out.println(LabelConst.Evt_CoverageType);
        System.out.println(VelosResourceBundle.getLabelString(LabelConst.Evt_CoverageType, "foo"));
        assertTrue("Coverage Type".equals(
                VelosResourceBundle.getLabelString(LabelConst.Evt_CoverageType)));
    }
}
