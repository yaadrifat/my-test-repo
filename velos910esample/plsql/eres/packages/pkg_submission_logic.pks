CREATE OR REPLACE PACKAGE      PKG_SUBMISSION_LOGIC AS
/******************************************************************************
   NAME:       PKG_SUBMISSION_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.
******************************************************************************/


  Function f_check_irb(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_cro(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_rsc(p_study IN NUMBER) return t_nested_results_table;


  Function f_check_cic(p_study IN NUMBER) return t_nested_results_table;

  Function f_check_src(p_study IN NUMBER) return t_nested_results_table;

END PKG_SUBMISSION_LOGIC;
/


