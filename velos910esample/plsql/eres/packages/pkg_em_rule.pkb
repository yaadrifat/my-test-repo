CREATE OR REPLACE PACKAGE BODY        "PKG_EM_RULE"
AS
   PROCEDURE SP_EVENT_DONE_PATIENT (
      P_STUDY                    NUMBER,
      P_CAL                      NUMBER,
      P_PROTOCOL                 NUMBER,
      P_EVENT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      P_PATIENT                  NUMBER,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Arvind Kumar
  Date: 18th June 2002
  Purpose - Rule: Event Status marked 'Done', em_2
  Returns the number of patients whose event status is marked done
  Input - study id, calendar id, event id
  Returns - count: # of times this Event Milestone - em_2 is achieved.
*/
      V_COUNT              NUMBER;
      V_MILESTONEFLAG      NUMBER                   := 0;
      V_ACHIEVEMENT_DATE   DATE                     := PKG_DATEUTIL.f_get_null_date_str;
      V_DATEARRAY          TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
   BEGIN
      O_COUNT := 0;
      FOR I IN( SELECT DISTINCT PK_PATPROT, FK_PER
                  FROM ERV_PATSTUDY_EVEALL
                 WHERE FK_STUDY = P_STUDY
                   AND EVENT_PROTOCOL_ID = P_CAL
                   AND FK_PER = P_PATIENT)
      LOOP
         SP_EVENT_DONE (
            P_EVENT,
            I.PK_PATPROT,
            P_STDATE,
            P_ENDDATE,
            V_MILESTONEFLAG,
            V_ACHIEVEMENT_DATE
         );
         O_COUNT := O_COUNT + V_MILESTONEFLAG;
         IF (V_MILESTONEFLAG = 1) THEN
            V_DATEARRAY.EXTEND;
            V_DATEARRAY (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_EVENT_DONE_PATIENT;

   PROCEDURE SP_EVENT_DONE (
      P_EVENT_ID         NUMBER,
      P_PATPROT          NUMBER,
      P_STDATE           DATE,
      P_ENDDATE          DATE,
      O_RESULT     OUT   NUMBER,
      O_ACH_DATE   OUT   DATE
   )
   AS
/* Author: Arvind Kumar
   Date: 17th June, 2002
   Purpose - Checks whether the rule 'Event is marked done' for a patprot, em_2
   Returns 0 if Event Type Milestone is not met
   Returns 1 if Event Type Milestone is met
*/
      V_ACH_DATE   DATE;
   BEGIN
      O_RESULT := 0;
      BEGIN
   --the count of patients in a study for which event is marked 'Done'.
         SELECT TO_DATE(TO_CHAR(EVENT_EXEON,pkg_dateUTIL.f_get_dateformat ()),pkg_dateUTIL.f_get_dateformat ())
         INTO V_ACH_DATE
           FROM ERV_PATSTUDY_EVEALL
          WHERE EVENT_ID_ASSOC = P_EVENT_ID
            AND PK_PATPROT = P_PATPROT
            AND EVENT_STATUS_CODELST_SUBTYP = 'ev_done'
            AND PK_EVENTSTAT = (SELECT  MAX(PK_EVENTSTAT)
                                 FROM ERV_PATSTUDY_EVEALL
                                 WHERE EVENT_ID_ASSOC = P_EVENT_ID
                                 AND PK_PATPROT = P_PATPROT
                                 AND TO_DATE(TO_CHAR(EVENT_EXEON,pkg_dateUTIL.f_get_dateformat ()),pkg_dateUTIL.f_get_dateformat ()) >= P_STDATE
                                 AND TO_DATE(TO_CHAR(EVENT_EXEON,pkg_dateUTIL.f_get_dateformat ()),pkg_dateUTIL.f_get_dateformat ()) <= P_ENDDATE);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            O_RESULT := 0;
      END;
      IF V_ACH_DATE IS NULL THEN
         O_RESULT := 0;
      ELSE
         O_RESULT := 1;
      END IF;
      O_ACH_DATE := V_ACH_DATE;
   END SP_EVENT_DONE;

---------------------------------------------------------------------------------------------------------
   PROCEDURE SP_EVENT_DONE_COUNT (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_EVENT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Author: Arvind Kumar
  Date: 18th June 2002
  Purpose - Rule: Event Status marked 'Done', em_2
  Returns the number of patients whose event status is marked done
  Input - study id, calendar id, event id
  Returns - count: # of times this Event Milestone - em_2 is achieved.
*/
      V_COUNT              NUMBER;
      V_MILESTONEFLAG      NUMBER := 0;
      V_ACHIEVEMENT_DATE   DATE   := PKG_DATEUTIL.f_get_null_date_str;
	 V_PATPROT           NUMBER;
	 V_PER               NUMBER;
	 V_CUR   TYPES.cursorType;
   BEGIN
      O_COUNT := 0;
      O_ACHIEVEMENTDATES :=  TYPES.SMALL_STRING_ARRAY();

      IF P_ORG_ID > 0 THEN
        OPEN V_CUR FOR SELECT DISTINCT PK_PATPROT, FK_PER
                  FROM ERV_PATSTUDY_EVEALL
                 WHERE FK_STUDY = P_STUDY
                   AND EVENT_PROTOCOL_ID = P_CAL
			    AND PER_SITE = P_ORG_ID ;
	 ELSE
       OPEN V_CUR FOR SELECT DISTINCT PK_PATPROT, FK_PER
                  FROM ERV_PATSTUDY_EVEALL
                 WHERE FK_STUDY = P_STUDY
                   AND EVENT_PROTOCOL_ID = P_CAL ;
	 END IF;
      LOOP
        FETCH V_CUR INTO V_PATPROT, V_PER;
        EXIT WHEN V_CUR%NOTFOUND;
         SP_EVENT_DONE (
            P_EVENT,
            V_PATPROT,
            P_STDATE,
            P_ENDDATE,
            V_MILESTONEFLAG,
            V_ACHIEVEMENT_DATE
         );
         O_COUNT := O_COUNT + V_MILESTONEFLAG;
         IF (V_MILESTONEFLAG = 1) THEN
            O_ACHIEVEMENTDATES.EXTEND;
            O_ACHIEVEMENTDATES (O_COUNT) :=
               TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
         END IF;

     END LOOP;
    CLOSE V_CUR;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_EVENT_DONE_COUNT;

------------------------------------------------------------------------------------
   PROCEDURE SP_EVE_RULE_CRF (
      P_EVENTID           NUMBER,
      P_CRFSTATUS         VARCHAR2,
      P_PATPROTID         NUMBER,
      P_BEGINDATE         DATE,
      P_ENDDATE           DATE,
      O_ACHDATE     OUT   DATE,
      O_OUTPUT      OUT   NUMBER
   )
/*
Author  : Dinesh Khurana
Date    : June 19,2002
Purpose : to check the status of an event if all the crf associated to an
          event are Completed/Submitted(related to a particular schedule and pat
ient)
      (EM_3 )
Input   : event id(related to event_assoc),crf status subtype(completed/sumbitte
d),patprot_id
Output  : Returns 0 if all the crf status of an event in a schedule are submitte
d/completed(for a patient)
*/
   AS
      V_CFTSTATUS            VARCHAR2 (200);
      V_CRFSTATID            NUMBER;
      V_CRFSTATCREATEDON     DATE;
      V_CRFACHIEVEMENTDATE   DATE      := PKG_DATEUTIL.f_get_null_date_str;
      V_CRFSUBTYPE           VARCHAR2 (200);
      V_GLOBALFLAG           NUMBER;
   BEGIN
      V_GLOBALFLAG := 0;
      O_OUTPUT := 0;
	 --   V_CRFACHIEVEMENTDATE := to_date ('01/01/1900', 'mm/dd/yyyy');
      FOR I IN( SELECT PK_CRF
                  FROM SCH_CRF A, SCH_EVENTS1 B
                 WHERE A.FK_PATPROT = P_PATPROTID
                   AND LPAD (A.FK_EVENTS1, 10, '0') = B.EVENT_ID
                   AND B.FK_ASSOC = P_EVENTID)
      LOOP
         V_CRFSTATID := 0;
         BEGIN
            SELECT TO_DATE(TO_CHAR(A.CREATED_ON,pkg_dateUTIL.f_get_dateformat ()),pkg_dateUTIL.f_get_dateformat ()) CREATED_ON, B.CODELST_SUBTYP, A.PK_CRFSTAT
              INTO V_CRFSTATCREATEDON, V_CRFSUBTYPE, V_CRFSTATID
              FROM SCH_CRFSTAT A, SCH_CODELST B
             WHERE A.FK_CODELST_CRFSTAT = B.PK_CODELST
               AND PK_CRFSTAT =
                      (SELECT MAX (PK_CRFSTAT)
                         FROM SCH_CRFSTAT
                        WHERE FK_CRF = I.PK_CRF
                          AND TO_DATE (TO_CHAR (A.CREATED_ON, pkg_dateUTIL.f_get_dateformat ()), pkg_dateUTIL.f_get_dateformat () ) <=    P_ENDDATE
                          AND A.CREATED_ON IS NOT NULL);
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               V_CRFSTATID := 0;
         END;
--dbms_output.put_line('Ret Code 3/5 - return value ' || --v_crfstatCreatedOn);
         IF (V_CRFSTATID != 0) THEN
            IF (trim (V_CRFSUBTYPE) != trim (P_CRFSTATUS)) THEN
-- dbms_output.put_line('Ret Code 3/5 - return value ' || p_crfStatus) ;
               O_OUTPUT := 0;
               O_ACHDATE := NULL;
               RETURN;
            END IF;
            IF  (P_BEGINDATE <=  V_CRFSTATCREATEDON )AND (V_CRFSTATCREATEDON  <= P_ENDDATE  ) THEN
               V_GLOBALFLAG := 1;
               IF (V_CRFACHIEVEMENTDATE < V_CRFSTATCREATEDON) THEN
                  V_CRFACHIEVEMENTDATE := V_CRFSTATCREATEDON;
-- dbms_output.put_line('Ret Code 3/5 - return value DKDK ' || --v_crf AchievementDate) ;
               END IF;
            END IF;
         END IF;
      END LOOP;
      IF (V_GLOBALFLAG = 1) THEN
         O_ACHDATE := V_CRFACHIEVEMENTDATE;
         O_OUTPUT := 1;
      END IF;
/**********************************
TEST
set serveroutput on
declare
output number ;
achDate date;
begin
SP_EVE_RULE_CRF(10273,'completed',959,to_date('06/24/2002','mm/dd/yyyy'),
to_date('06/26/2002','mm/dd/yyyy'),achDate,output);
dbms_output.put_line(to_char(output)) ;
dbms_output.put_line(achDate) ;
end ;
**************************************/
   END SP_EVE_RULE_CRF;

---------------------------------------------------------------------------------------------------
   PROCEDURE SP_EVE_RULE_CRF_COUNT (
      P_EVENTID                  NUMBER,
      P_CRFSTATUS                VARCHAR2,
      P_STUDYID                  NUMBER,
      P_ORG_ID                   NUMBER,
      P_BEGINDATE                DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*
Author  : Dinesh Khurana
Date    : June 21,2002
Purpose : to check the count of an event if all the crf associated to an
          event are Completed/Submitted in a study (EM_3 )
Input   : event id(related to event_assoc),crf status subtype(completed/sumbitte
d),studyId,beginDate,endDate
Output  : Returns count,achievement date array for which all the crf status of a
n event in a study are submitted/completed
*/
      V_COUNTTOTAL        NUMBER;
      V_CRFSTATUS         NUMBER;
      V_ACHIEVEMENTDATE   DATE                     := PKG_DATEUTIL.f_get_null_date_str;
      V_DATE_ARRAY        TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_CUR   TYPES.cursorType;
      V_PATPROT NUMBER;
   BEGIN
    O_COUNT := 0;
    V_COUNTTOTAL := 0;
    IF P_ORG_ID > 0 THEN
       OPEN V_CUR FOR SELECT PK_PATPROT
                  FROM ER_PATPROT, ER_PER
                 WHERE FK_STUDY = P_STUDYID
			  AND ER_PATPROT.FK_PER = ER_PER.PK_PER
			  AND ER_PER.FK_SITE = P_ORG_ID ;
   ELSE
      OPEN V_CUR FOR SELECT PK_PATPROT
                  FROM ER_PATPROT
                 WHERE FK_STUDY = P_STUDYID;
   END IF;
     LOOP
     FETCH V_CUR INTO V_PATPROT;
      EXIT WHEN V_CUR%NOTFOUND;
         V_CRFSTATUS := 0;
         SP_EVE_RULE_CRF (
            P_EVENTID,
            P_CRFSTATUS,
            V_PATPROT,
            P_BEGINDATE,
            P_ENDDATE,
            V_ACHIEVEMENTDATE,
            V_CRFSTATUS
         );
         V_COUNTTOTAL := V_COUNTTOTAL + V_CRFSTATUS;
         IF (V_CRFSTATUS = 1) THEN
            V_DATE_ARRAY.EXTEND;
            V_DATE_ARRAY (V_COUNTTOTAL) :=
               TO_CHAR (V_ACHIEVEMENTDATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
            DBMS_OUTPUT.PUT_LINE (
               'Ret Code 3/5 - return value DKDK ' || V_ACHIEVEMENTDATE
            );
         END IF;

    END LOOP;
     CLOSE V_CUR;
	 O_ACHIEVEMENTDATES := V_DATE_ARRAY;
      O_COUNT := V_COUNTTOTAL;
/**********************************
set serveroutput on
declare
output number ;
actDates types.SMALL_STRING_ARRAY ;
begin
PKG_EM_RULE.SP_EVE_RULE_CRF_COUNT(17733,'completed',4045,to_date('01/01/1900','mm/dd/yyyy
'),to_date('06/24/2005','mm/dd/yyyy'),output,actDates);
dbms_output.put_line(to_char(output)) ;
end ;
**************************************/
   END SP_EVE_RULE_CRF_COUNT;


---------------------------------------------------------------------------------------------------------
   PROCEDURE SP_FORE_EVENT_DONE_COUNT (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_EVENT                    NUMBER,
      P_STDATE                   DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*Forecast
  Purpose - Rule: Event Status marked 'Done', em_2
*/
      V_COUNT              NUMBER;
      V_ACHIEVEMENT_DATE   DATE   := PKG_DATEUTIL.f_get_null_date_str;
	 V_PATPROT           NUMBER;
	 V_CUR   TYPES.cursorType;
   BEGIN
      O_COUNT := 0;
      O_ACHIEVEMENTDATES :=  TYPES.SMALL_STRING_ARRAY();


       OPEN V_CUR FOR SELECT PK_PATPROT, MAX (ACTUAL_SCHDATE) SCH_ON
                  FROM SCH_EVENTS1 s, SCH_CODELST c ,  ERV_ALLPATSTUDYSTAT v
                  WHERE v.FK_STUDY = P_STUDY
                   AND FK_PROTOCOL = P_CAL
			    AND FK_ASSOC = P_EVENT
                   AND TO_DATE(TO_CHAR(ACTUAL_SCHDATE,pkg_dateUTIL.f_get_dateformat ()),pkg_dateUTIL.f_get_dateformat ())  >= P_STDATE
                   AND TO_DATE(TO_CHAR(ACTUAL_SCHDATE,pkg_dateUTIL.f_get_dateformat ()),pkg_dateUTIL.f_get_dateformat ())  <= P_ENDDATE
                   AND c.codelst_subtyp  <> 'ev_done'
			    AND v.PK_PATPROT = s.fk_patprot AND c.pk_codelst = s.isconfirmed
			    AND c.codelst_type = 'eventstatus'
                 GROUP BY PK_PATPROT ORDER BY 2 DESC ;

      LOOP
        FETCH V_CUR INTO V_PATPROT, V_ACHIEVEMENT_DATE;
        EXIT WHEN V_CUR%NOTFOUND;
         O_COUNT := O_COUNT + 1;
         O_ACHIEVEMENTDATES.EXTEND;
         O_ACHIEVEMENTDATES (O_COUNT) := TO_CHAR (V_ACHIEVEMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
     END LOOP;
    CLOSE V_CUR;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         O_COUNT := 0;
   END SP_FORE_EVENT_DONE_COUNT;

------------------------------------------------------------------------------------

   PROCEDURE SP_FORE_EVE_RULE_CRF_COUNT (
      P_EVENTID                  NUMBER,
      P_CRFSTATUS                VARCHAR2,
      P_STUDYID                  NUMBER,
      P_ORG_ID                   NUMBER,
      P_BEGINDATE                DATE,
      P_ENDDATE                  DATE,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   )
   AS
/*
 foreacst em3, em4
*/
      V_COUNT        NUMBER;
      V_ACHIEVEMENTDATE   DATE                     := pkg_dateUTIL.f_get_null_date_str;
      V_DATE_ARRAY        TYPES.SMALL_STRING_ARRAY
            := TYPES.SMALL_STRING_ARRAY ();
      V_CUR   TYPES.cursorType;
      V_PATPROT NUMBER;
   BEGIN
    V_COUNT := 0;
     O_COUNT := 0;
      OPEN V_CUR FOR SELECT PK_PATPROT, MAX (EVENT_ACTUAL_SCHDATE) SCH_ON
                FROM SCH_CRF crf, SCH_CODELST c ,  ERV_PATSTUDY_EVE v
                  WHERE FK_STUDY = P_STUDYID
			       AND EVENT_ID_ASSOC = P_EVENTID
                   AND c.codelst_subtyp  <> P_CRFSTATUS
			    AND v.SCH_EVENTS1_PK = crf.fk_events1 AND c.pk_codelst = crf.crf_curstat
			    AND c.codelst_type = 'crfstatus'
                   AND crf.crf_delflag <> 'Y'
                 GROUP BY PK_PATPROT ORDER BY 2 DESC;
     LOOP
     FETCH V_CUR INTO V_PATPROT,V_ACHIEVEMENTDATE  ;
      EXIT WHEN V_CUR%NOTFOUND;
       V_COUNT := V_COUNT + 1;
       V_DATE_ARRAY.EXTEND;
       V_DATE_ARRAY (V_COUNT) := TO_CHAR (V_ACHIEVEMENTDATE, PKG_DATEUTIL.F_GET_DATEFORMAT);
    END LOOP;
     CLOSE V_CUR;
	 O_ACHIEVEMENTDATES := V_DATE_ARRAY;
      O_COUNT := V_COUNT;

   END SP_FORE_EVE_RULE_CRF_COUNT;


---------------------------------------------------------------------------------------------------------

END Pkg_Em_Rule;
/


CREATE SYNONYM ESCH.PKG_EM_RULE FOR PKG_EM_RULE;


CREATE SYNONYM EPAT.PKG_EM_RULE FOR PKG_EM_RULE;


GRANT EXECUTE, DEBUG ON PKG_EM_RULE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_EM_RULE TO ESCH;

