CREATE OR REPLACE PACKAGE        "PKG_LOOKUP" IS

   PROCEDURE SP_REPEAT_LOOKUP ( p_lkp_field Number, p_lkp_form_field Number, p_section Number, o_ret OUT Number);

   FUNCTION SP_GET_LKP_SYSIDS ( p_lkpstr Varchar2) RETURN TYPES.SMALL_STRING_ARRAY ;

/******************************************************************************
   NAME:       PKG_LOOKUP
   PURPOSE:    Implement form lookup

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10/20/2003   Sonia Sahni      1. Created this package.

******************************************************************************/
END PKG_LOOKUP;
/


CREATE SYNONYM ESCH.PKG_LOOKUP FOR PKG_LOOKUP;


CREATE SYNONYM EPAT.PKG_LOOKUP FOR PKG_LOOKUP;


GRANT EXECUTE, DEBUG ON PKG_LOOKUP TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_LOOKUP TO ESCH;

