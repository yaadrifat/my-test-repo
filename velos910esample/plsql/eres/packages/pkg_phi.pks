CREATE OR REPLACE PACKAGE        "PKG_PHI" 
AS
 FUNCTION f_get_patientright(p_user NUMBER, p_group NUMBER, p_patient NUMBER ) RETURN NUMBER;
 FUNCTION F_Is_Superuser(p_user  NUMBER, p_study NUMBER) RETURN NUMBER;
 FUNCTION F_Gen_PatDump_phi(p_study VARCHAR2,p_site VARCHAR2,p_patient VARCHAR2,p_txarm VARCHAR2,p_startdate VARCHAR2, p_enddate VARCHAR2, p_user NUMBER,p_pi in varchar2) RETURN CLOB;

 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_STUDYSTAT', pLEVEL  => Plog.LFATAL);
END Pkg_Phi ;
/


