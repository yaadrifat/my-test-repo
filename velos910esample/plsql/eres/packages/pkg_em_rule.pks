CREATE OR REPLACE PACKAGE        "PKG_EM_RULE" 
AS
   PROCEDURE SP_EVENT_DONE_PATIENT (
      P_STUDY                    NUMBER,
      P_CAL                      NUMBER,
      P_PROTOCOL                 NUMBER,
      P_EVENT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      P_PATIENT                  NUMBER,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );PROCEDURE SP_EVENT_DONE (
      P_EVENT_ID         NUMBER,
      P_PATPROT          Number,
      P_STDATE           Date,
      P_ENDDATE          Date,
      O_RESULT     OUT   Number,
      O_ACH_DATE   OUT   Date
   );PROCEDURE SP_EVENT_DONE_COUNT (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_EVENT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );PROCEDURE SP_EVE_RULE_CRF (
      P_EVENTID           NUMBER,
      P_CRFSTATUS         VARCHAR2,
      P_PATPROTID         NUMBER,
      P_BEGINDATE         date,
      P_ENDDATE           date,
      O_ACHDATE     OUT   date,
      O_OUTPUT      OUT   NUMBER
   );
   PROCEDURE SP_EVE_RULE_CRF_COUNT (
      P_EVENTID                  NUMBER,
      P_CRFSTATUS                VARCHAR2,
      P_STUDYID                  NUMBER,
      P_ORG_ID                   NUMBER,
      P_BEGINDATE                date,
      P_ENDDATE                  date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );

   PROCEDURE SP_FORE_EVENT_DONE_COUNT (
      P_STUDY                    NUMBER,
      P_ORG_ID                   NUMBER,
      P_CAL                      NUMBER,
      P_EVENT                    NUMBER,
      P_STDATE                   Date,
      P_ENDDATE                  Date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );

   PROCEDURE SP_FORE_EVE_RULE_CRF_COUNT (
      P_EVENTID                  NUMBER,
      P_CRFSTATUS                VARCHAR2,
      P_STUDYID                  NUMBER,
      P_ORG_ID                   NUMBER,
      P_BEGINDATE                date,
      P_ENDDATE                  date,
      O_COUNT              OUT   NUMBER,
      O_ACHIEVEMENTDATES   OUT   TYPES.SMALL_STRING_ARRAY
   );
END PKG_EM_RULE;
/


CREATE SYNONYM ESCH.PKG_EM_RULE FOR PKG_EM_RULE;


CREATE SYNONYM EPAT.PKG_EM_RULE FOR PKG_EM_RULE;


GRANT EXECUTE, DEBUG ON PKG_EM_RULE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_EM_RULE TO ESCH;

