CREATE OR REPLACE FUNCTION        "F_GET_SUM4_DATA" (p_study NUMBER, p_subtyp VARCHAR2)
RETURN  VARCHAR2
AS
  v_retval VARCHAR2(200);
BEGIN
SELECT DECODE(
	   (SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_type),'corr',
	   		   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study =
			   (SELECT pk_study FROM ER_STUDY i WHERE i.pk_study = o.study_assoc) AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = p_subtyp)),
			   (SELECT studyid_id FROM ER_STUDYID WHERE fk_study = pk_study AND fk_codelst_idtype =
			   (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = p_subtyp)))
	INTO v_retval
	FROM ER_STUDY o
	WHERE pk_study = p_study;
RETURN v_retval;
END;
/


CREATE SYNONYM ESCH.F_GET_SUM4_DATA FOR F_GET_SUM4_DATA;


CREATE SYNONYM EPAT.F_GET_SUM4_DATA FOR F_GET_SUM4_DATA;


GRANT EXECUTE, DEBUG ON F_GET_SUM4_DATA TO EPAT;

GRANT EXECUTE, DEBUG ON F_GET_SUM4_DATA TO ESCH;

