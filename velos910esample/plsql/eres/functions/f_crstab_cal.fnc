CREATE OR REPLACE FUNCTION        "F_CRSTAB_CAL" (p_study NUMBER,p_protocol NUMBER, p_site VARCHAR2)
RETURN  CLOB
AS
  v_xml CLOB;
  v_visit_number NUMBER;
  v_counter NUMBER;
  v_studynum VARCHAR2(100);
  v_protocol VARCHAR2(100);
  v_maxvisit NUMBER;
  v_find NUMBER;
  v_site VARCHAR2(100);
  v_sites VARCHAR2(1000) := p_site;
BEGIN

	 SELECT study_number INTO v_studynum FROM er_study WHERE pk_study = p_study;
	 SELECT NAME INTO v_protocol FROM event_assoc WHERE event_id = p_protocol;
	 v_xml := v_xml || '<ROWSET>' || '<STUDY_NUM>' || v_studynum ||  '</STUDY_NUM>' || '<PROTOCOL>' ||v_protocol || '</PROTOCOL>' || '<CAL_VISITS>';

--	 select max(visit) into v_maxvisit from erv_patsch where fk_study = p_study and to_number(protocolid) = p_protocol;

--	 select count(*) into v_maxvisit from sch_protocol_visit where fk_protocol = p_protocol;

--	 for i in 1.. v_maxvisit
	 FOR i IN (SELECT visit_name  FROM sch_protocol_visit WHERE fk_protocol = p_protocol ORDER BY visit_no)
	 LOOP
	 	 v_xml := v_xml || '<CVISIT>' || i.visit_name || '</CVISIT>';
		 v_maxvisit := v_maxvisit + 1;
	 END LOOP;
	  v_xml := v_xml || '</CAL_VISITS><PATIENTS>';


	  LOOP

	  v_find := INSTR(p_site,',');
	  IF v_find > 0 THEN
	  	 v_site := SUBSTR(v_sites,1,v_find-1);
	  	 v_sites := SUBSTR(v_sites,v_find+1);
	 ELSE
	 	 v_site := p_site;
	 END IF;

	  FOR j IN (SELECT pk_patprot, patprot_patstdid FROM er_patprot, er_per WHERE fk_study = p_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL AND fk_site = TO_NUMBER(v_site) AND fk_per = pk_per AND fk_protocol = p_protocol)
	  LOOP
	  	  v_xml := v_xml || '<PATIENT>';

	 	  v_xml := v_xml || '<ID>' || j.patprot_patstdid || '</ID>';
	  	  v_xml := v_xml || '<VISITS>';

		  v_counter := 0;
		  FOR k IN (SELECT fk_patprot, visit,event_status, COUNT(*) AS stat_count FROM erv_patsch WHERE fk_study = p_study AND TO_NUMBER(protocolid) = p_protocol AND fk_patprot = j.pk_patprot GROUP BY fk_patprot, visit,event_status )
		  LOOP
		  	  v_counter := v_counter + 1;
			  IF v_counter = 1 THEN
	  	  	  	 v_xml := v_xml || '<PATVISIT><EVENTSTATUS>';
			  	 v_visit_number := k.visit;
			  END IF;

			  IF k.visit <> v_visit_number THEN
	  	 	  	  v_xml := v_xml || '</EVENTSTATUS>';

				  FOR l IN (SELECT CRFSTAT_SUBTYPE,crfstat_desc_current, COUNT(*) crfcount FROM (
				  	  	SELECT DISTINCT fk_patprot,visit,CRFSTAT_SUBTYPE, crf_name,crfstat_desc_current  FROM erv_crf, erv_patsch
							   WHERE
							   		  fk_events1 (+) = erv_patsch.event_id AND
									   crfstat_desc_current IS NOT NULL AND
									   	crfstat_desc_current <> 'NO Associated Forms' AND
										fk_patprot = k.fk_patprot AND
										visit = v_visit_number
										) GROUP BY CRFSTAT_SUBTYPE,crfstat_desc_current)
				  LOOP
				  	  v_xml := v_xml || '<CRF statsubtype="'|| trim(l.crfstat_subtype) || '">[ ' ||l.crfstat_desc_current || ':' || l.crfcount || '] </CRF>';
				  END LOOP;
				   v_xml := v_xml || '</PATVISIT><PATVISIT><EVENTSTATUS>';
			  END IF;
		  	  v_xml := v_xml || '[' || k.event_status || ':' || k.stat_count || '] ';
			  v_visit_number := k.visit;
		  END LOOP;
		  IF v_counter > 0 THEN
	 	  	  v_xml := v_xml || '</EVENTSTATUS></PATVISIT>';
		   END IF;
	  	  v_xml := v_xml || '</VISITS>';

	  	  v_xml := v_xml || '</PATIENT>';
	  END LOOP;

	  IF v_find = 0 THEN
	  	 EXIT;
	  END IF;

	  END LOOP;

	  v_xml := v_xml || '</PATIENTS>';



	  v_xml := v_xml || '</ROWSET>';
		RETURN v_xml;
END;
/


CREATE SYNONYM ESCH.F_CRSTAB_CAL FOR F_CRSTAB_CAL;


CREATE SYNONYM EPAT.F_CRSTAB_CAL FOR F_CRSTAB_CAL;


GRANT EXECUTE, DEBUG ON F_CRSTAB_CAL TO EPAT;

GRANT EXECUTE, DEBUG ON F_CRSTAB_CAL TO ESCH;

