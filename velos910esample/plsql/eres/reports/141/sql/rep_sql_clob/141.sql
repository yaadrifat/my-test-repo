select case when lower(':storageId') like 'select%' then
	to_clob('<?xml version="1.0" encoding="UTF-8"?><ROWSET><ROW1><STORE_NAME>Storage filter [ALL] does not apply to this report. Please select a storage in filter.</STORE_NAME></ROW1></ROWSET>')
else pkg_inventory.f_get_storage_trail(:sessAccId, ':storageId')
end from dual
