select * from
(
select
ceil(dts.days/7) weeks ,
decode(mod(dts.days,7),0,7,mod(dts.days,7)) days ,
a.event_id ,
a.chain_id ,
a.displacement ,
a.name  ,
a.duration ,
a.FUZZY_PERIOD ,
prot.pname ,
study.study_number ,
to_char(study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
ESCH.lst_cost(a.event_id) lstcost,
ESCH.lst_usr(a.event_id) lstusr,
ESCH.lst_doc(a.event_id)  lstdoc,
ESCH.lst_forms(a.event_id)  lstforms
from
(select a.name pname from erv_eveassoc a where a.event_id = ~1 ) prot ,
er_tmprep a,
( select rownum days
from all_objects
where rownum <
( select max(displacement)+1 days
from er_tmprep
)
) dts ,
(select study_number study_number , study_actualdt from er_study where pk_study = ~2 ) study
where  a.displacement(+)=dts.days)
where name is not null order  by 1,2