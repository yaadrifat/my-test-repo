SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
CBBID, STORAGE_LOC, CBU_COLLECTION_SITE,f_cbu_notes(~1,F_CODELST_ID('note_cat','cbu_info'),'1')cbu_info_notes, ELIGIBLE_STATUS,f_cbu_notes(~1,F_CODELST_ID('note_cat','eligiblity'),'1') eligibility_notes,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status,f_cbu_notes(~1,F_CODELST_ID('note_cat','lab_sum'),'1') labsummary_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
HEPARIN_THOU_PER,HEPARIN_THOU_ML,HEPARIN_FIVE_PER,HEPARIN_FIVE_ML,HEPARIN_TEN_PER,HEPARIN_TEN_ML,HEPARIN_SIX_PER,HEPARIN_SIX_ML,CPDA_PER,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,f_cbu_notes(~1,F_CODELST_ID('note_cat','id'),'1')ids_notes,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,f_cbu_notes(~1,F_CODELST_ID('note_cat','hla'),'1') hla_notes,(select f_lab_summary(~1) from dual) lab_summary,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID('doc_categ','cbu_info_cat'))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID('doc_categ','hla'))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID('doc_categ','eligibile_cat'))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID('doc_categ','lab_sum_cat'))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID('doc_categ','process_info'))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID('doc_categ','infect_mark'))) IDM_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID('doc_categ','hlth_scren'))) HHS_ATTACHMENTS,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS,F_GETIDMDETAILS(~1) IDMDETAILS,f_cbu_notes(~1,F_CODELST_ID('note_cat','infct_dis_mark'),'1') IDM_NOTES,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),
        NULL,'0',
        (SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),'1')ordertypedata
FROM
 REP_CBU_DETAILS
WHERE
 PK_CORD = ~1