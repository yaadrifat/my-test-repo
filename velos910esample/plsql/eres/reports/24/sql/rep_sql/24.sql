
select codelst_desc study_stat,
    to_char(STUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) stat_date ,
    er_study.study_number,
    to_char(er_study.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,studystat_note
      from er_codelst,
           er_studystat,
           er_study
      where er_studystat.FK_STUDY = '~1'
        and er_study.pk_study = er_studystat.fk_study
        and pk_codelst = FK_CODELST_STUDYSTAT
        and CODELST_TYPE= 'studystat'
      order by STUDYSTAT_DATE desc 
