
SELECT
  PERSON_CODE                    , PAT_STUDYID,
  PERSON_NAME                    ,
STUDY_NUMBER                   ,
SITE_NAME,
STUDY_TITLE                    ,
  TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
PROTOCOLID   ,
PROTOCOL_NAME                  ,
TO_CHAR(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,
 EVENT_NAME                     ,
 MONTH                          ,
  TO_CHAR(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) st ,
  fk_patprot ,
 EVENT_STATUS,
 cr.CRF_NUMBER,
cr.CRF_NAME,
 TO_CHAR(cr.CRFSTAT_REVIEWON,PKG_DATEUTIL.F_GET_DATEFORMAT) CRFSTAT_REVIEWON,
  NVL(cr.CRFSTAT_DESC_CURRENT,'-') CRFSTAT_DESC,
visit_name as  visit
 FROM   erv_patsch, erv_crf cr
   WHERE  fk_per = ~1  AND
       fk_study = ~2  AND
    START_DATE_TIME BETWEEN '~3'  AND '~4' AND
    cr.fk_events1 (+) = erv_patsch.event_id AND
scheve_status = 0
UNION
SELECT
  PERSON_CODE                    , PAT_STUDYID ,
  PERSON_NAME                    ,
STUDY_NUMBER                   ,
SITE_NAME,
STUDY_TITLE                    ,
  TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_ENROLDT,
PROTOCOLID   ,
PROTOCOL_NAME                  ,
TO_CHAR(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,
 EVENT_NAME                     ,
 MONTH                          ,
  TO_CHAR(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) st ,
  fk_patprot ,
 EVENT_STATUS,
 cr.CRF_NUMBER,
cr.CRF_NAME,
 TO_CHAR(cr.CRFSTAT_REVIEWON,PKG_DATEUTIL.F_GET_DATEFORMAT) CRFSTAT_REVIEWON,
  NVL(cr.CRFSTAT_DESC_CURRENT,'-') CRFSTAT_DESC,
visit_name as  visit
 FROM   erv_patsch, erv_crf cr
   WHERE  fk_per = ~1  AND
       fk_study = ~2  AND
    START_DATE_TIME BETWEEN '~3'  AND '~4' AND
    cr.fk_events1 (+) = erv_patsch.event_id AND
scheve_status = 5 AND
EVENT_STATUS = 'Done' AND
cr.crfstat_desc_current IS NOT NULL AND
cr.crfstat_desc_current <> 'NO Associated Forms' ORDER BY visit  
