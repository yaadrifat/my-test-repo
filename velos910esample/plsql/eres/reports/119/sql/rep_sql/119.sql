SELECT * FROM (
SELECT DECODE(trial_type_agent,'Y',1,DECODE(trial_type_nonagent,'Y',2,DECODE(trial_type_epi,'Y',3,DECODE(trial_type_comp,'Y',4)))) AS s1,
DECODE(trial_type_agent,'Y','Agent OR Device',DECODE(trial_type_nonagent,'Y','Trials Involving other Interventions',DECODE(trial_type_epi,'Y','Epidemiologic OR other Observational Studies',DECODE(trial_type_comp,'Y','Companion, ANCILLARY OR Correlative Studies','NA')))) AS trial_type,
RESEARCH_TYPE,STUDY_TYPE,STUDY_SCOPE,tharea,STUDY_SPONSOR,DISEASE_SITE,STUDY_NUMBER,PI,STUDY_MAJ_AUTH,TAREA,DATE_OPEN,DATE_CLOSED,STUDY_PHASE,STUDY_TITLE,TARGET,CTR_TO_DATE,CTR_12_MOS, CLOSED_DATE,
F_extsum4(ps,'G', 'male', '~2', '~3',NULL) AS gm,
F_extsum4(ps,'G', 'female', '~2', '~3',NULL) AS gf,
F_extsum4(ps,'G', NULL, '~2', '~3',NULL) AS g0,
F_extsum4(ps,'R', 'race_asian', '~2', '~3',NULL) AS ra,
F_extsum4(ps,'R', 'race_white', '~2', '~3',NULL) AS rw ,
F_extsum4(ps,'R', 'race_notrep', '~2', '~3',NULL) AS rn ,
F_extsum4(ps,'R', 'race_indala', '~2', '~3',NULL) AS ri ,
F_extsum4(ps,'R', 'race_blkafr', '~2', '~3',NULL) AS rb ,
F_extsum4(ps,'R', 'race_hwnisl', '~2', '~3',NULL) AS rh ,
F_extsum4(ps,'R', 'race_unknown', '~2', '~3',NULL) AS ru ,
F_extsum4(ps,'R', NULL, '~2', '~3',NULL) AS r0 ,
F_extsum4(ps,'E', 'hispanic', '~2', '~3',NULL) AS eh ,
F_extsum4(ps,'E', 'nonhispanic', '~2', '~3',NULL) AS en ,
F_extsum4(ps,'E', 'notreported', '~2', '~3',NULL) AS er ,
F_extsum4(ps,'E', 'unknown', '~2', '~3',NULL) AS eu ,
F_extsum4(ps,'E', NULL, '~2', '~3',NULL) AS e0,
F_extsum4(ps,'R', 'race_asian', '~2', '~3','hispanic') AS ha,
F_extsum4(ps,'R', 'race_white', '~2', '~3','hispanic') AS hw ,
F_extsum4(ps,'R', 'race_notrep', '~2', '~3','hispanic') AS hn ,
F_extsum4(ps,'R', 'race_indala', '~2', '~3','hispanic') AS hi ,
F_extsum4(ps,'R', 'race_blkafr', '~2', '~3','hispanic') AS hb ,
F_extsum4(ps,'R', 'race_hwnisl', '~2', '~3','hispanic') AS hh ,
F_extsum4(ps,'R', 'race_unknown', '~2', '~3','hispanic') AS hu ,
F_extsum4(ps,'R', NULL, '~2', '~3','hispanic') AS h0
FROM (
SELECT
a.pk_study AS ps,
F_Get_Sum4_Data(a.pk_study,'sum4_agent') AS trial_type_agent,
F_Get_Sum4_Data(a.pk_study,'sum4_beh') AS trial_type_nonagent,
F_Get_Sum4_Data(a.pk_study,'sum4_na') AS trial_type_epi,
F_Get_Sum4_Data(a.pk_study,'sum4_comp') AS trial_type_comp,
F_Codelst_Desc(a.fk_codelst_restype) AS research_type,
F_Codelst_Desc(a.fk_codelst_type) AS study_type,
F_Codelst_Desc(a.fk_codelst_scope) AS study_scope,
F_Codelst_Desc(a.fk_codelst_tarea) AS tharea,
study_sponsor,F_Getdis_Site(a.study_disease_site) AS disease_site,
study_number,
(SELECT usr_lastname || ', ' || usr_firstname FROM ER_USER WHERE pk_user = a.study_prinv) || ' ' || study_otherprinv AS PI,
study_maj_auth,
(SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = F_Codelst_Id('studyidtype','sum4_prg')) AS tarea,
TO_CHAR(study_actualdt,'mm/dd/yyyy') AS date_open,
(CASE WHEN study_end_date >= '~2' AND study_end_date <= '~3'
THEN TO_CHAR(study_end_date,'mm/dd/yyyy') ELSE NULL
END) AS date_closed,
F_Codelst_Desc(a.fk_codelst_phase) AS study_phase,
SUBSTR(study_title,1,125) AS study_title,
study_samplsize AS target,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL) AS ctr_to_date,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND patprot_enroldt BETWEEN '~2' AND '~3' ) AS ctr_12_mos,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = F_Codelst_Id('studystat','active_cls')),'mm/dd/yyyy') AS closed_date
FROM ER_STUDY a,ER_STUDYSTAT b WHERE a.fk_account = ~1 AND b.FK_STUDY = pk_study AND
fk_codelst_studystat = F_Codelst_Id('studystat','active') AND
(study_end_date IS NULL OR study_end_date >= '~2') AND study_actualdt <= '~3'
)
)
WHERE trial_type <> 'NA'
ORDER BY s1,research_type,tarea,pi
