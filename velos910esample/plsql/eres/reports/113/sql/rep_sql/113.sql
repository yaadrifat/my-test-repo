SELECT * FROM (
SELECT DECODE(trial_type_agent,'Y',1,DECODE(trial_type_nonagent,'Y',2,DECODE(trial_type_epi,'Y',3,DECODE(trial_type_comp,'Y',4)))) AS trial_type_sort,
DECODE(trial_type_agent,'Y','Agent OR Device',DECODE(trial_type_nonagent,'Y','Trials Involving other Interventions',DECODE(trial_type_epi,'Y','Epidemiologic OR other Observational Studies',DECODE(trial_type_comp,'Y','Companion, ANCILLARY OR Correlative Studies','NA')))) AS trial_type,
RESEARCH_TYPE,STUDY_TYPE,STUDY_SCOPE,STUDY_SPONSOR,DISEASE_SITE,STUDY_NUMBER,PRIMARY_INVESTIGATOR,STUDY_MAJ_AUTH,TAREA,DATE_OPEN,DATE_CLOSED,STUDY_PHASE,STUDY_TITLE,TARGET,CTR_TO_DATE,CTR_TO_DATE_OTR,CTR_12_MOS, CTR_12_MOS_OTR, CLOSED_DATE
 FROM (
SELECT
f_get_sum4_data(a.pk_study,'sum4_agent') AS trial_type_agent,
f_get_sum4_data(a.pk_study,'sum4_beh') AS trial_type_nonagent,
f_get_sum4_data(a.pk_study,'sum4_na') AS trial_type_epi,
f_get_sum4_data(a.pk_study,'sum4_comp') AS trial_type_comp,
F_Get_Codelstdesc(a.fk_codelst_restype) AS research_type,
F_Get_Codelstdesc(a.fk_codelst_type) AS study_type,
F_Get_Codelstdesc(a.fk_codelst_scope) AS study_scope,
study_sponsor,F_Getdis_Site(a.study_disease_site) AS disease_site,
study_number,
(SELECT usr_lastname || ', ' || usr_firstname FROM ER_USER WHERE pk_user = a.study_prinv) || ' ' || study_otherprinv AS PRIMARY_INVESTIGATOR,
study_maj_auth,
(SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_prg')) AS tarea,
TO_CHAR(study_actualdt,'mm/dd/yyyy') AS date_open,
(CASE WHEN study_end_date >='~2' AND study_end_date <= '~3'
THEN TO_CHAR(study_end_date,'mm/dd/yyyy') ELSE NULL
END) AS date_closed,
F_Get_Codelstdesc(a.fk_codelst_phase) AS study_phase,
(SELECT studyid_id FROM ER_STUDYID WHERE fk_study = a.pk_study AND fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studyidtype' AND codelst_subtyp = 'std_title')) AS study_title,
DECODE((SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope),
'std_cen_multi',
				DECODE((SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = (SELECT fk_codelst_studysitetype FROM ER_STUDYSITES WHERE fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = ~1 AND site_stat = 'Y'))),'PRIMARY',
				'(' || study_nsamplsize || ') ' || (SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = ~1 AND site_stat = 'Y') ),
				(SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = ~1 AND site_stat = 'Y') )),
'std_cen_single',study_nsamplsize,(SELECT studysite_lsamplesize FROM ER_STUDYSITES r WHERE r.fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = ~1 AND site_stat = 'Y') )) AS target,
F_Nci_Sum4_count(a.pk_study,NULL,NULL,'Y') AS ctr_to_date,
F_Nci_Sum4_count(a.pk_study,NULL,NULL,'N') AS ctr_to_date_otr,
F_Nci_Sum4_count(a.pk_study,'~2','~3','Y') AS ctr_12_mos,
F_Nci_Sum4_count(a.pk_study,'~2','~3','N') AS ctr_12_mos_otr,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_site = (SELECT pk_site FROM ER_SITE WHERE fk_account = ~1 AND site_stat = 'Y') AND fk_codelst_studystat IN (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'active_cls' OR codelst_subtyp = 'prmnt_cls')),'mm/dd/yyyy') AS closed_date
FROM ER_STUDY a,ER_STUDYSTAT b WHERE a.fk_account = ~1 AND b.FK_STUDY = pk_study AND
fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'studystat' AND codelst_subtyp = 'active') AND
(study_end_date IS NULL OR study_end_date >='~2') AND study_actualdt <= '~3'
)
)
WHERE trial_type <> 'NA'
ORDER BY trial_type_sort,research_type,tarea,primary_investigator
