select PERSON_CODE , PAT_STUDYID, FK_PER ,  STUDY_NUMBER,  STUDY_TITLE ,
to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
PROTOCOL_NAME,  PATPROT_ENROLDT,
SITE_NAME,
to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT) PATPROT_START,
EVENT_NAME, MONTH, to_char(EVENT_SCHDATE,'YYYY') Year ,
to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) START_DATE_TIME, EVENT_STATUS ,
to_char(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON ,
( select max(codelst_desc)
from ESCH.SCH_EVENTCOST A, ESCH.SCH_CODELST B
where  A.FK_CURRENCY = B.PK_CODELST
and A.fk_event = fk_assoc
) cost_currency,
to_char(ESCH.tot_cost(fk_assoc),'9999999999.99') evecost,
ESCH.lst_cost(fk_assoc) event_costlst,
ESCH.lst_costdesc(fk_assoc) event_costdesc ,
FK_PATPROT, visit_name as VISIT
from erv_patsch
where protocolid = ~1
and fk_site in (~4)
and EVENT_SCHDATE between '~2' and '~3'   and event_status = 'Done'
Order by person_code, EVENT_SCHDATE