
SELECT  (SELECT PK_ACCOUNT FROM ER_ACCOUNT WHERE pk_account = fk_account) AS  accountid,
  (SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_form) AS form_name,
  TO_CHAR(acctforms_filldate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS forms_filldate,
  fa_fldname,
  fa_systemid,
  decode(fa_datetime,null,'Not Applicable',TO_CHAR(fa_datetime,pkg_dateutil.f_get_datetimeformat))  AS fa_datetime,
  DECODE(instr(fa_oldvalue,'[VELSEP1]'),0,fa_oldvalue,substr(fa_oldvalue,0,instr(fa_oldvalue,'[VELSEP1]') - 1)) as fa_oldvalue,
  DECODE(instr(fa_newvalue,'[VELSEP1]'),0,fa_newvalue,substr(fa_newvalue,0,instr(fa_newvalue,'[VELSEP1]') - 1)) as fa_newvalue,
  SUBSTR(fa_modifiedby_name,INSTR(fa_modifiedby_name,',')+1) AS  fa_modifiedby_name,
'A' form_type,pk_formauditcol,FA_REASON,PK_FORMAUDITCOL
FROM ER_FORMAUDITCOL,  ER_ACCTFORMS
WHERE SUBSTR(fa_modifiedby_name,1,4) <> 'ERES' AND
fk_filledform = ~1 AND
fk_formlib = fk_form AND
fk_filledform = pk_acctforms
ORDER BY fa_systemid, fa_datetime
