
select
       STUDY_NUMBER,
       STUDY_TITLE,
       to_char(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
       TA,
       AUTHOR_SITE,
decode(PRI_INV_NAME,null,STUDY_OTHERPRINV,PRI_INV_NAME || decode(STUDY_OTHERPRINV,null,'','; ' || STUDY_OTHERPRINV)) AS PRI_INV_NAME
  from erv_study
 where fk_account = ~1
   and FK_CODELST_TAREA = '~2'
   and CREATED_ON between '~3' and '~4'
 order by STUDY_TITLE
