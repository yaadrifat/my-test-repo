Select
DISTINCT
STUDY_NUMBER,
TO_CHAR(STUDYACTUAL_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE, STUDY_TITLE, TA, PHASE, RESEARCH_TYPE, STUDY_TYPE, DECODE(PI,NULL,STUDY_OTHERPRINV,PI || DECODE(STUDY_OTHERPRINV,NULL,'','; ' || STUDY_OTHERPRINV)) AS PI,
(SELECT Pkg_Util.f_join(CURSOR( SELECT  (SELECT site_name FROM ER_SITE WHERE pk_site=a.fk_site) site_name  FROM ER_STUDYSITES a WHERE fk_study=pk_study),',') FROM dual) AS ORGANIZATION,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division
  FROM erv_studyactive_protocols
  WHERE fk_account = :sessAccId
    AND STUDYACTUAL_DATE <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND (studyend_date IS NULL or studyend_date >=  TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) )
    AND fk_codelst_tarea IN (:tAreaId)
    AND pk_study IN (:studyId)
    AND (study_division IN (:studyDivId) OR study_division IS NULL)
    AND (study_prinv IS NULL OR study_prinv IN (:userId))
    AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )
    ORDER BY TA 
