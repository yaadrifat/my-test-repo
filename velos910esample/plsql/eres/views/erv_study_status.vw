/* Formatted on 2/9/2010 1:39:38 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STUDY_STATUS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   PRIMARY_INVESTIGATOR,
   OTHER_PI,
   STUDY_CONTACT,
   ORGANIZATION,
   STUDY_STATUS,
   STUDY_VALID_FROM,
   STUDY_VALID_UNTIL,
   DOCUMENTED_BY,
   NOTES,
   FK_STUDY,
   CREATED_ON,
   FK_ACCOUNT,
   SSTAT_STUD_STRT_DT,
   SSTAT_STUD_END_DT,
   SSTAT_END_DATE,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   SSTAT_RESPONSE_ID,
   CURRENT_STAT,
   STUDY_TAREA,
   STUDYSTAT_MEETDT,
   OUTCOME,
   STATUS_TYPE,
   REVIEW_BOARD,
   STUDYSTAT_ASSIGNEDTO
)
AS
   SELECT   study_number,
            study_title,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = study_prinv)
               primary_investigator,
            study_otherprinv other_pi,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = study_coordinator)
               study_contact,
            (SELECT   site_name
               FROM   ER_SITE
              WHERE   pk_site = fk_site)
               ORGANIZATION,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_studystat)
               study_status,
            TRUNC (studystat_date) study_valid_from,
            TRUNC (studystat_validt) study_valid_until,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = fk_user_docby)
               documented_by,
            studystat_note notes,
            fk_study,
            ER_STUDYSTAT.CREATED_ON,
            fk_account,
            STUDY_ACTUALDT SSTAT_STUD_STRT_DT,
            STUDY_END_DATE SSTAT_STUD_END_DT,
            STUDYSTAT_ENDT SSTAT_END_DATE,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = ER_STUDYSTAT.CREATOR)
               CREATOR,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = ER_STUDYSTAT.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDYSTAT.LAST_MODIFIED_DATE,
            ER_STUDYSTAT.RID,
            PK_STUDYSTAT SSTAT_RESPONSE_ID,
            f_get_yesno (current_stat) current_stat,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_tarea)
               study_tarea,
            STUDYSTAT_MEETDT,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = OUTCOME)
               OUTCOME,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = STATUS_TYPE)
               STATUS_TYPE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_REVBOARD
                      AND codelst_type = 'rev_board')
               review_board,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = STUDYSTAT_ASSIGNEDTO)
               STUDYSTAT_ASSIGNEDTO
     FROM   ER_STUDYSTAT, ER_STUDY
    WHERE   pk_study = fk_study;


CREATE SYNONYM ESCH.ERV_STUDY_STATUS FOR ERV_STUDY_STATUS;


CREATE SYNONYM EPAT.ERV_STUDY_STATUS FOR ERV_STUDY_STATUS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDY_STATUS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDY_STATUS TO ESCH;

