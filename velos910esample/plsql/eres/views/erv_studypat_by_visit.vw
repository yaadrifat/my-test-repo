/* Formatted on 2/9/2010 1:39:42 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STUDYPAT_BY_VISIT
(
   FK_PER,
   PER_SITE,
   STUDY_NUMBER,
   STUDY_TITLE,
   PK_PATPROT,
   FK_STUDY,
   PER_CODE,
   PATSTUDYSTAT_DESC,
   PATSTUDYSTAT_SUBTYPE,
   PATSTUDYSTAT_ID,
   PK_PATSTUDYSTAT,
   PATSTUDYSTAT_DATE,
   PATSTUDYSTAT_NOTE,
   PATSTUDYSTAT_REASON,
   PATSTUDYSTAT_REASON_DESC,
   PATPROT_ENROLDT,
   CUR_VISIT,
   CUR_VISIT_DATE,
   NEXT_VISIT,
   NEXT_VISIT_NAME,
   NEXT_VISIT_NO,
   PATPROT_PATSTDID,
   LAST_VISIT_NAME,
   PI,
   ASSIGNEDTO_NAME,
   PHYSICIAN_NAME,
   ENROLLEDBY_NAME,
   TREATINGORG_NAME,
   STATUS_BROWSER_FLAG
)
AS
   SELECT   fk_per,
            per_site,
            study_number,
            study_title,
            pk_patprot,
            fk_study,
            per_code,
            patstudystat_desc,
            patstudystat_subtype,
            patstudystat_id,
            pk_patstudystat,
            patstudystat_date,
            patstudystat_note,
            patstudystat_reason,
			patstudystat_reason_desc,
            patprot_enroldt,
            cur_visit,
            (SELECT   MIN (event_exeon)
               FROM   sch_events1
              WHERE       fk_patprot = m.pk_patprot
                      AND visit = cur_visit
                      AND status = 0
                      AND isconfirmed !=
                            (SELECT   pk_codelst
                               FROM   sch_codelst
                              WHERE   TRIM (codelst_type) = 'eventstatus'
                                      AND TRIM (codelst_subtyp) =
                                            'ev_notdone'))
               cur_visit_date,
            (SELECT   MIN (actual_schdate)
               FROM   sch_events1
              WHERE   fk_patprot = m.pk_patprot
                      --KM--AND visit >= cur_visit + 1
                      AND (actual_schdate >
                              (CASE
                                  WHEN (SELECT   MAX (actual_schdate)
                                          FROM   sch_events1
                                         WHERE   fk_patprot = m.pk_patprot
                                                 AND isconfirmed !=
                                                       (SELECT   pk_codelst
                                                          FROM   sch_codelst
                                                         WHERE   TRIM(codelst_type) =
                                                                    'eventstatus'
                                                                 AND TRIM(codelst_subtyp) =
                                                                       'ev_notdone')) IS NULL
                                  THEN
                                     (SELECT   MIN (actual_schdate) - 1
                                        FROM   sch_events1
                                       WHERE   fk_patprot = m.pk_patprot)
                                  ELSE
                                     (SELECT   MAX (actual_schdate)
                                        FROM   sch_events1
                                       WHERE   fk_patprot = m.pk_patprot
                                               AND isconfirmed !=
                                                     (SELECT   pk_codelst
                                                        FROM   sch_codelst
                                                       WHERE   TRIM(codelst_type) =
                                                                  'eventstatus'
                                                               AND TRIM(codelst_subtyp) =
                                                                     'ev_notdone'))
                               END))
                      AND status = 0
                      AND isconfirmed =
                            (SELECT   pk_codelst
                               FROM   sch_codelst
                              WHERE   TRIM (codelst_type) = 'eventstatus'
                                      AND TRIM (codelst_subtyp) =
                                            'ev_notdone'))
               next_visit,
               (SELECT   visit_name
               FROM   sch_protocol_visit
              WHERE   pk_protocol_visit =
                         (SELECT   MAX (fk_visit)
                            FROM   sch_events1 i
                           WHERE       i.fk_patprot = m.pk_patprot
                                   AND i.visit = next_visit_no
                                   AND status = 0))
               next_visit_name,
               next_visit_no,
            patprot_patstdid,
            (SELECT   visit_name
               FROM   sch_protocol_visit
              WHERE   pk_protocol_visit =
                         (SELECT   MAX (fk_visit)
                            FROM   sch_events1 i
                           WHERE       i.fk_patprot = m.pk_patprot
                                   AND i.visit = cur_visit
                                   AND status = 0))
               last_visit_name,
            PI,
            assignedto_name,
            physician_name,
            enrolledby_name,
            treatingorg_name,
			STATUS_BROWSER_FLAG
     FROM   (SELECT   DISTINCT
                      fk_per,
                      per_site,
                      study_number,
                      study_title,
                      pk_patprot,
                      fk_study,
                      per_code,
                      patstudystat_desc,
                      patstudystat_subtype,
                      patstudystat_id,
                      pk_patstudystat,
                      patstudystat_date,
                      patstudystat_note,
                      patstudystat_reason,
					  patstudystat_reason_desc,
                      patprot_enroldt,
                      patprot_patstdid,
                      NVL (
                         (SELECT   MAX (visit)
                            FROM   sch_events1
                           WHERE   fk_patprot = o.pk_patprot
                                   AND isconfirmed !=
                                         (SELECT   pk_codelst
                                            FROM   sch_codelst
                                           WHERE   TRIM (codelst_type) =
                                                      'eventstatus'
                                                   AND TRIM (codelst_subtyp) =
                                                         'ev_notdone')
                                   AND actual_schdate =
                                         (SELECT   MAX (actual_schdate)
                                            FROM   sch_events1
                                           WHERE   fk_patprot = o.pk_patprot
                                                   AND isconfirmed !=
                                                         (SELECT   pk_codelst
                                                            FROM   sch_codelst
                                                           WHERE   TRIM(codelst_type) =
                                                                      'eventstatus'
                                                                   AND TRIM(codelst_subtyp) =
                                                                         'ev_notdone'))),
                         0
                      )
                         cur_visit,
                      NVL (
                         (SELECT   MIN (visit)
		               FROM   sch_events1
		              WHERE   fk_patprot = o.pk_patprot
                      AND (actual_schdate >
                      (CASE
                          WHEN (SELECT   MAX (actual_schdate)
                                  FROM   sch_events1
                                 WHERE   fk_patprot = o.pk_patprot
                                         AND isconfirmed !=
                                               (SELECT   pk_codelst
                                                  FROM   sch_codelst
                                                 WHERE   TRIM(codelst_type) =
                                                            'eventstatus'
                                                         AND TRIM(codelst_subtyp) =
                                                               'ev_notdone')) IS NULL
                          THEN
                             (SELECT   MIN (actual_schdate) - 1
                                FROM   sch_events1
                               WHERE   fk_patprot = o.pk_patprot)
                          ELSE
                             (SELECT   MAX (actual_schdate)
                                FROM   sch_events1
                               WHERE   fk_patprot = o.pk_patprot
                                       AND isconfirmed !=
                                             (SELECT   pk_codelst
                                                FROM   sch_codelst
                                               WHERE   TRIM(codelst_type) =
                                                          'eventstatus'
                                                       AND TRIM(codelst_subtyp) =
                                                             'ev_notdone'))
                               END))
		                      AND status = 0
		                      AND isconfirmed =
		                            (SELECT   pk_codelst
		                               FROM   sch_codelst
		                              WHERE   TRIM (codelst_type) = 'eventstatus'
		                                      AND TRIM (codelst_subtyp) =
		                                            'ev_notdone')),
                         0) next_visit_no,
                      PI,
                      assignedto_name,
                      physician_name,
                      enrolledby_name,
                      treatingorg_name,
					  STATUS_BROWSER_FLAG
               FROM   erv_patstudy_latest_eve o) m;


CREATE SYNONYM ESCH.ERV_STUDYPAT_BY_VISIT FOR ERV_STUDYPAT_BY_VISIT;


CREATE SYNONYM EPAT.ERV_STUDYPAT_BY_VISIT FOR ERV_STUDYPAT_BY_VISIT;


