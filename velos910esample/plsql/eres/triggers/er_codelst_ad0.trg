create or replace TRIGGER "ERES"."ER_CODELST_AD0"
AFTER DELETE
ON ER_CODELST
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_CODELST', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_codelst) || '|' ||
  to_char(:old.fk_account) || '|' ||
  :old.codelst_type || '|' ||
  :old.codelst_subtyp || '|' ||
  :old.codelst_desc || '|' ||
  :old.codelst_hide || '|' ||
  to_char(:old.codelst_seq) || '|' ||
  :old.codelst_maint || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  :old.CODELST_CUSTOM_COL|| '|' ||
  :old.CODELST_CUSTOM_COL1|| '|' ||
  :old.CODELST_STUDY_ROLE;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/
