CREATE OR REPLACE TRIGGER "ER_FLDLIB_AD0" 
 AFTER UPDATE--DELETE
 ON ER_FLDLIB
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);


rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FLDLIB', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_field) || '|' ||
  TO_CHAR(:OLD.fk_catlib) || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  :OLD.fld_libflag || '|' ||
  :OLD.fld_name || '|' ||
  :OLD.fld_desc || '|' ||
  :OLD.fld_uniqueid || '|' ||
  :OLD.fld_systemid || '|' ||
  :OLD.fld_keyword || '|' ||
  :OLD.fld_type || '|' ||
  :OLD.fld_datatype || '|' ||
  :OLD.fld_instructions || '|' ||
  TO_CHAR(:OLD.fld_length) || '|' ||
  TO_CHAR(:OLD.fld_decimal) || '|' ||
  TO_CHAR(:OLD.fld_linesno) || '|' ||
  TO_CHAR(:OLD.fld_charsno) || '|' ||
  :OLD.fld_defresp || '|' ||
  TO_CHAR(:OLD.fk_lookup) || '|' ||
  TO_CHAR(:OLD.fld_isunique) || '|' ||
  TO_CHAR(:OLD.fld_isreadonly) || '|' ||
  TO_CHAR(:OLD.fld_isvisible) || '|' ||
  TO_CHAR(:OLD.fld_colcount) || '|' ||
  :OLD.fld_format || '|' ||
  TO_CHAR(:OLD.fld_repeatflag) || '|' ||
  TO_CHAR(:OLD.fld_bold) || '|' ||
  TO_CHAR(:OLD.fld_italics) || '|' ||
  TO_CHAR(:OLD.fld_sameline) || '|' ||
  :OLD.fld_align || '|' ||
  TO_CHAR(:OLD.fld_underline) || '|' ||
  :OLD.fld_color || '|' ||
  :OLD.fld_font || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.fld_fontsize) || '|' ||
  :OLD.fld_lkpdataval || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  :OLD.fld_lkpdispval || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' || :OLD.fld_lkptype;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


