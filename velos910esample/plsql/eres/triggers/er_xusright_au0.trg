CREATE OR REPLACE TRIGGER ER_XUSRIGHT_AU0
AFTER UPDATE
ON ER_XUSRIGHT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_study varchar2(1000) ;
  new_study varchar2(1000) ;
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_XUSRIGHT', :old.rid, 'U', usr);
  if nvl(:old.pk_xusright,0) !=
     NVL(:new.pk_xusright,0) then
     audit_trail.column_update
       (raid, 'PK_XUSRIGHT',
       :old.pk_xusright, :new.pk_xusright);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     select study_title into old_study
     from er_study where pk_study = :old.fk_study ;
     select study_title into new_study
     from er_study where pk_study = :new.fk_study ;
     audit_trail.column_update
       (raid, 'FK_STUDY',
       old_study, new_study);
  end if;
  if nvl(:old.fk_userx,0) !=
     NVL(:new.fk_userx,0) then
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.fk_userx ;
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.fk_userx ;
     audit_trail.column_update
       (raid, 'FK_USERX',
       old_modby, new_modby);
  end if;
  if nvl(:old.fk_usergrantor,0) !=
     NVL(:new.fk_usergrantor,0) then
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.fk_usergrantor ;
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.fk_usergrantor ;
     audit_trail.column_update
       (raid, 'FK_USERGRANTOR',
       old_modby, new_modby);
  end if;
  if nvl(:old.xusright_stat,' ') !=
     NVL(:new.xusright_stat,' ') then
     audit_trail.column_update
       (raid, 'XUSRIGHT_STAT',
       :old.xusright_stat, :new.xusright_stat);
  end if;
  if nvl(:old.xusright,0) !=
     NVL(:new.xusright,0) then
     audit_trail.column_update
       (raid, 'XUSRIGHT',
       :old.xusright, :new.xusright);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
end;
/


