create or replace TRIGGER "ERES".ER_MILESTONE_BI0 BEFORE INSERT ON ER_MILESTONE
FOR EACH ROW
WHEN (NEW.rid IS NULL OR NEW.rid = 0)
DECLARE
 erid NUMBER(10);
 raid NUMBER(10);
 insert_data CLOB;
 usr varchar2(2000);
     BEGIN

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:new.creator);
  Audit_Trail.record_transaction    (raid, 'ER_MILESTONE', erid, 'I', usr );
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.FK_VISIT||'|'|| :NEW.PK_MILESTONE||'|'||
    :NEW.FK_STUDY||'|'||:NEW.MILESTONE_TYPE||'|'|| :NEW.FK_CAL||'|'||
    :NEW.FK_CODELST_RULE||'|'|| :NEW.MILESTONE_AMOUNT||'|'|| :NEW.MILESTONE_VISIT||'|'||
    :NEW.FK_EVENTASSOC||'|'|| :NEW.MILESTONE_COUNT||'|'||:NEW.MILESTONE_DELFLAG||'|'||
    :NEW.MILESTONE_USERSTO||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD   ||'|'||:NEW.milestone_limit ||'|'||:NEW.milestone_status
  ||'|'||:NEW.milestone_paytype ||'|'||:NEW.milestone_paydueby ||'|'||:NEW.milestone_paybyunit   ||'|'||:NEW.milestone_payfor ||'|'||:NEW.milestone_eventstatus
  ||'|'||:NEW.milestone_achievedcount
  ||'|'||TO_CHAR(:NEW.last_checked_on ,PKG_DATEUTIL.f_get_dateformat) ||'|'||:NEW.milestone_description
  ||'|'|| :NEW.fk_codelst_milestone_stat; --KM


 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);

 END ;
/