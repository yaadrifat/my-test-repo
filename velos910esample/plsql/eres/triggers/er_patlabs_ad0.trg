CREATE OR REPLACE TRIGGER "ER_PATLABS_AD0" AFTER DELETE ON ER_PATLABS
FOR EACH ROW
DECLARE
   raid number(10);
   deleted_data varchar2(2000);
BEGIN
   select seq_audit.nextval into raid from dual;
   audit_trail.record_transaction (raid, 'ER_PATLABS', :old.rid, 'D');
   deleted_data :=
      to_char(:old.PK_PATLABS) || '|' ||
      to_char(:old.FK_PER) || '|' ||
      to_char(:old.FK_TESTID) || '|' ||
      to_char(:old.TEST_DATE) || '|' ||
      to_char(:old.TEST_RESULT) || '|' ||
      :old.TEST_TYPE || '|' ||
      :old.TEST_UNIT || '|' ||
      :old.TEST_RANGE || '|' ||
      :old.TEST_LLN || '|' ||
      :old.TEST_ULN || '|' ||
      to_char(:old.FK_SITE) || '|' ||
      to_char(:old.FK_CODELST_TSTSTAT) || '|' ||
      :old.ACCESSION_NUM || '|' ||
      to_char(:old.FK_TESTGROUP) || '|' ||
      to_char(:old.FK_CODELST_ABFLAG) || '|' ||
      to_char(:old.FK_PATPROT) || '|' ||
      to_char(:old.FK_EVENT_ID) || '|' ||
      :old.CUSTOM001 || '|' ||
      :old.CUSTOM002 || '|' ||
      :old.CUSTOM003 || '|' ||
      :old.CUSTOM004 || '|' ||
      :old.CUSTOM005 || '|' ||
      :old.CUSTOM006 || '|' ||
      :old.CUSTOM007 || '|' ||
      :old.CUSTOM008 || '|' ||
      :old.CUSTOM009 || '|' ||
      :old.CUSTOM010 || '|' ||
      :old.ER_NAME || '|' ||
      to_char(:old.ER_GRADE) || '|' ||
      to_char(:old.FK_CODELST_STDPHASE) || '|' ||
      to_char(:old.FK_STUDY) || '|' ||
      to_char(:old.rid) || '|' ||
      to_char(:old.creator) || '|' ||
      to_char(:old.last_modified_by) || '|' ||
      to_char(:old.last_modified_date) || '|' ||
      to_char(:old.created_on) || '|' ||
      :old.ip_add||'|'||:old.FK_SPECIMEN;
    insert into audit_delete (raid, row_data) values (raid, deleted_data);
END;
/


