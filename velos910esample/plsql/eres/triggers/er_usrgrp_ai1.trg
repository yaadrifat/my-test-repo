CREATE OR REPLACE TRIGGER "ER_USRGRP_AI1" 
AFTER INSERT
ON ER_USRGRP
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_formid NUMBER;
v_fk_object NUMBER;
v_obj_number NUMBER;
v_parent NUMBER;
BEGIN


/*Author: Sonika, Nov 25, 03
* to insert the user in the Form share with so that the newly added user to the Group
* is able to access the Forms available to this Group
*/

/*FOR i IN (SELECT FK_FORMLIB
          FROM ER_FORMSHAREWITH
          WHERE FSW_ID = :NEW.fk_grp
          AND FSW_TYPE = 'G')
LOOP
    v_formid := i.fk_formlib;
    INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
		 VALUES (SEQ_ER_FORMSHAREWITH.NEXTVAL,v_formid,:NEW.fk_user,'U',:NEW.creator ,'N',SYSDATE,:NEW.ip_add);
END LOOP;*/

/* May 03, 04
* to insert the user in the Object share table so that the newly added user to the Group
* is able to access the module record available to this Group
*/
FOR i IN (SELECT pk_objectshare,fk_object, object_number
          FROM ER_OBJECTSHARE
          WHERE fk_objectshare_id = :NEW.fk_grp
          AND objectshare_type = 'G')
LOOP
    v_fk_object := i.fk_object ;
    v_obj_number := i.object_number;
	v_parent  := i.pk_objectshare;
    INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT ,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE, CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,fk_objectshare)
		 VALUES (SEQ_ER_OBJECTSHARE.NEXTVAL,v_obj_number, v_fk_object,:NEW.fk_user,'U',:NEW.creator ,'N',SYSDATE,:NEW.ip_add,v_parent);


END LOOP;


END;
/


