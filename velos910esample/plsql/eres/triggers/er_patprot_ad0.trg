CREATE OR REPLACE TRIGGER "ER_PATPROT_AD0" 
  AFTER DELETE
  ON ER_PATPROT
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  usr VARCHAR(2000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  	usr := Getuser(:OLD.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'ER_PATPROT', :OLD.rid, 'D',usr);

  deleted_data :=
  TO_CHAR(:OLD.pk_patprot) || '|' ||
  TO_CHAR(:OLD.fk_protocol) || '|' ||
  TO_CHAR(:OLD.fk_study) || '|' ||
  TO_CHAR(:OLD.fk_user) || '|' ||
  TO_CHAR(:OLD.patprot_enroldt) || '|' ||
  TO_CHAR(:OLD.fk_per) || '|' ||
  TO_CHAR(:OLD.patprot_start) || '|' ||
  :OLD.patprot_notes || '|' ||
  TO_CHAR(:OLD.patprot_stat) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  TO_CHAR(:OLD.patprot_discdt) || '|' ||
  :OLD.patprot_reason || '|' ||
  TO_CHAR(:OLD.patprot_schday) || '|' ||
  TO_CHAR(:OLD.FK_TIMEZONE) || '|' ||
  :OLD.PATPROT_PATSTDID || '|' ||
  TO_CHAR(:OLD.FK_USERASSTO) || '|' ||
  TO_CHAR(:OLD.FK_CODELSTLOC) || '|' ||
  :OLD.PATPROT_RANDOM || '|' ||
  TO_CHAR(:OLD.PATPROT_PHYSICIAN) || '|' ||
  TO_CHAR(:OLD.FK_CODELST_PTST_EVAL_FLAG) || '|' ||
  TO_CHAR(:OLD.FK_CODELST_PTST_EVAL) || '|' ||
  TO_CHAR(:OLD.FK_CODELST_PTST_INEVAL) || '|' ||
  TO_CHAR(:OLD.FK_CODELST_PTST_SURVIVAL) || '|' ||
  TO_CHAR(:OLD.FK_CODELST_PTST_DTH_STDREL) || '|' ||
  :OLD.DEATH_STD_RLTD_OTHER || '|' ||
  :OLD.INFORM_CONSENT_VER || '|' ||
  TO_CHAR(:OLD.NEXT_FOLLOWUP_ON) || '|' ||
  TO_CHAR(:OLD.DATE_OF_DEATH)|| '|' ||
--JM: 11Jul2008: #3534----
  TO_CHAR(:OLD.PATPROT_CONSIGN) || '|' ||
  TO_CHAR(:OLD.PATPROT_CONSIGNDT) || '|' ||
  TO_CHAR(:OLD.PATPROT_RESIGNDT1) || '|' ||
  TO_CHAR(:OLD.PATPROT_RESIGNDT2) || '|' ||
  TO_CHAR(:OLD.FK_SITE_ENROLLING) || '|' ||
  :OLD.PATPROT_TREATINGORG;
--JM: 11Jul2008: #3534----

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


