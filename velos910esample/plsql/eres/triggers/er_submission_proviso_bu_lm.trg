CREATE OR REPLACE TRIGGER "ER_SUBMISSION_PROVISO_BU_LM" BEFORE UPDATE ON ER_SUBMISSION_PROVISO
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
BEGIN
:NEW.last_modified_date := SYSDATE ;
END;
/


