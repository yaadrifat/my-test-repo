CREATE OR REPLACE TRIGGER "ER_PATSTUDYSTAT_AI0" 
AFTER INSERT
ON ER_PATSTUDYSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  v_codedesc VARCHAR2(255);
  v_codesubtyp  VARCHAR2(255);
BEGIN
SELECT trim(codelst_Desc), trim(codelst_subtyp)
   INTO v_codedesc, v_codesubtyp
   FROM er_codelst
  WHERE pk_codelst = :NEW.fk_codelst_stat ;

  -- by sonia abrol, for enrollment status notifications

/*  if (lower(v_codesubtyp) = 'enrolled'  or lower(v_codesubtyp)  = 'enroll_appr' or lower(v_codesubtyp) = 'enroll_denied') then
         pkg_patient.SP_PATENRMAIL(:new.fk_per, v_codesubtyp,:new.fk_study, :new.PATSTUDYSTAT_DATE);
 end if; */


  IF UPPER(v_codesubtyp) = 'REACTIVATE' OR UPPER(v_codesubtyp) = 'ACTIVE'  THEN
  /*
  FIRE AN UPDATE TO SCH_DISPATCHMSG TO UPDATE ALL -1 STATUS TO 0
  */

-- If p_stat = 'active' then

    --changed for clubbed mail

--  Else
--  End if ;

--   sp_stopmail(:new.fk_study, :new.fk_per, :new.creator, :new.ip_add,'active') ;
  UPDATE sch_msgtran
     SET msg_status = 0
   WHERE fk_user = :NEW.fk_per
     AND fk_study = :NEW.fk_study
     AND msg_status = -1
 AND msg_sendon >= SYSDATE  ;
  ELSE
--   sp_stopmail(:new.fk_study, :new.fk_per, :new.creator, :new.ip_add) ;
  UPDATE sch_msgtran
     SET msg_status = -1
   WHERE fk_user = :NEW.fk_per
     AND fk_study = :NEW.fk_study
     AND msg_status = 0 ;
  END IF ;

  /* commented for issue 625*/


  --FIRE AN UPDATE TO SCH_DISPATCHMSG TO UPDATE ALL -1 STATUS TO 0

 /*
  If upper(v_codesubtyp) = 'SUSPEND' or upper(v_codesubtyp) = 'DEACTIVATE'  then
     PKG_STUDYSTAT.change_stat(:new.fk_study, :new.fk_per, :new.creator, :new.ip_add,:new.PATSTUDYSTAT_DATE) ;
  End if ; */


END;
/


