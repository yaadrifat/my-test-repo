CREATE OR REPLACE TRIGGER "ER_FORMACTION_AI" 
AFTER INSERT
ON ER_FORMACTION REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN
update er_formlib set form_xslrefresh=1 where pk_formlib=:NEW.formaction_sourceform;
END;
/


