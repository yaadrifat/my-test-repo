CREATE OR REPLACE TRIGGER "ER_OBJECTSHARE_AD0"
  AFTER DELETE --delete
  ON er_objectshare
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete



  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_OBJECTSHARE', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_objectshare) || '|' ||
  TO_CHAR(:OLD.object_number) || '|' ||
  TO_CHAR(:OLD.fk_object) || '|' ||
  TO_CHAR(:OLD.fk_objectshare_id) || '|' ||
  :OLD.objectshare_type || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);

END;
/


