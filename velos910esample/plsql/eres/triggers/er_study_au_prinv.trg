CREATE OR REPLACE TRIGGER "ER_STUDY_AU_PRINV" 
AFTER UPDATE OF STUDY_PRINV,FK_AUTHOR
ON ER_STUDY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_prinv_role NUMBER;
v_teamid NUMBER;
v_dm_role NUMBER;
v_existing_role NUMBER;
v_seq_teamid NUMBER;
BEGIN
/*
Trigger is activated when Principal Investigator or Author of Study is changed.
Creates record in er_studyteam
Modified on 09/10/04 by sonia:
         1. In study team a new user is added if:
             a) old author is changed
            b) old PI is changed
        2. Existing users from the team is not deleted
        3. If a user is already a PI and is not added as an author, its role of PI remains same

*/
SELECT pk_codelst
INTO v_prinv_role
FROM ER_CODELST
WHERE codelst_type = 'role' AND
codelst_subtyp = 'role_prin';

-- get author/data manager's role
SELECT pk_codelst
INTO v_dm_role
FROM ER_CODELST
WHERE codelst_type = 'role' AND
codelst_subtyp = 'role_dm';


IF (NVL(:OLD.FK_AUTHOR,0) <> NVL(:NEW.FK_AUTHOR,0)) THEN
    --check if the new author is  already existing in the Study Team then
     --replace its role by the author Role type
       BEGIN
        SELECT pk_studyteam,FK_CODELST_TMROLE
        INTO v_teamid,v_existing_role
        FROM ER_STUDYTEAM
        WHERE fk_study = :NEW.pk_study
        AND fk_user = :NEW.FK_AUTHOR
        AND (study_team_usr_type = 'D' OR study_team_usr_type = 'X');--Modified by Manimaran to fix the Bug2815
       EXCEPTION WHEN NO_DATA_FOUND THEN
          v_teamid := 0;
       END;
     IF NVL(v_teamid,0) > 0 THEN
         IF (NVL(v_existing_role,0) <> v_prinv_role AND (:NEW.FK_AUTHOR <> :NEW.STUDY_PRINV)) THEN
             UPDATE ER_STUDYTEAM
             SET fk_codelst_tmrole = v_dm_role
             WHERE pk_studyteam = v_teamid ;
        END IF;
      ELSE
        --insert new PI
        SELECT ERES.SEQ_ER_STUDYTEAM.NEXTVAL INTO v_seq_teamid FROM dual;
        INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM , FK_CODELST_TMROLE ,  FK_USER , FK_STUDY ,
             CREATOR ,  CREATED_ON , IP_ADD  , STUDY_TEAM_USR_TYPE , STUDYTEAM_STATUS   )
        VALUES(v_seq_teamid,v_dm_role,:NEW.FK_AUTHOR ,:NEW.pk_study,:NEW.last_modified_by,SYSDATE,
        :NEW.ip_add,'D','Active');
        --Added by Manimaran to fix the Bug 2704
        INSERT INTO ER_STATUS_HISTORY(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,IP_ADD,LAST_MODIFIED_BY,RECORD_TYPE,STATUS_ISCURRENT)
        VALUES(SEQ_ER_STATUS_HISTORY.NEXTVAL,v_seq_teamid,'er_studyteam',(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type='teamstatus' AND codelst_subtyp='Active'),SYSDATE,:NEW.last_modified_by,:NEW.ip_add,:NEW.last_modified_by,'N','1');
     END IF;
 END IF;

 v_teamid := 0;
 v_existing_role := 0;

    IF (NVL(:OLD.STUDY_PRINV,0) <> NVL(:NEW.STUDY_PRINV,0) and  :NEW.STUDY_PRINV is not null) THEN
    --check if the new principal investigator is  already existing in the Study Team then already
     --existing Study Team record is replaced by the Pricipal Investigator Role type study team record.
       BEGIN
        SELECT pk_studyteam
        INTO v_teamid
        FROM ER_STUDYTEAM
        WHERE fk_study = :NEW.pk_study
        AND fk_user = :NEW.STUDY_PRINV
        AND (study_team_usr_type = 'D' OR study_team_usr_type = 'X');--Modified by Manimaran to fix the Bug2815
       EXCEPTION WHEN NO_DATA_FOUND THEN
          v_teamid := 0;
       END;
     IF NVL(v_teamid,0) > 0 THEN
        UPDATE ER_STUDYTEAM
        SET fk_codelst_tmrole = v_prinv_role
        WHERE pk_studyteam = v_teamid ;
      ELSE
        --insert new PI
        SELECT ERES.SEQ_ER_STUDYTEAM.NEXTVAL INTO v_seq_teamid FROM dual;
        INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM , FK_CODELST_TMROLE ,  FK_USER , FK_STUDY ,
             CREATOR ,  CREATED_ON , IP_ADD  , STUDY_TEAM_USR_TYPE , STUDYTEAM_STATUS   )
        VALUES(v_seq_teamid,v_prinv_role,:NEW.STUDY_PRINV ,:NEW.pk_study,:NEW.last_modified_by,SYSDATE,
        :NEW.ip_add,'D','Active');
        --Added by Manimaran to fix the Bug 2704
        INSERT INTO ER_STATUS_HISTORY(PK_STATUS,STATUS_MODPK,STATUS_MODTABLE,FK_CODELST_STAT,STATUS_DATE,CREATOR,IP_ADD,LAST_MODIFIED_BY,RECORD_TYPE,STATUS_ISCURRENT)
        VALUES(SEQ_ER_STATUS_HISTORY.NEXTVAL,v_seq_teamid,'er_studyteam',(SELECT pk_codelst FROM ER_CODELST WHERE codelst_type='teamstatus' AND codelst_subtyp='Active'),SYSDATE,:NEW.last_modified_by,:NEW.ip_add,:NEW.last_modified_by,'N','1');
      END IF;

    -- see if the old PI is = new author

    IF (:OLD.STUDY_PRINV = :NEW.FK_AUTHOR AND :NEW.STUDY_PRINV <> :NEW.FK_AUTHOR and :NEW.STUDY_PRINV is not null) THEN
       --update the role of author to data manager
       BEGIN
           UPDATE ER_STUDYTEAM
           SET fk_codelst_tmrole = v_dm_role
           WHERE fk_study = :NEW.pk_study
           AND fk_user = :NEW.FK_AUTHOR
           AND study_team_usr_type = 'D';
       EXCEPTION WHEN NO_DATA_FOUND THEN
          v_teamid := 0; -- dummy
       END;
    END IF;


 END IF;

END;
/


