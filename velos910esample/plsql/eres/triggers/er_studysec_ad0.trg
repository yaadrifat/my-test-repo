CREATE OR REPLACE TRIGGER "ER_STUDYSEC_AD0" 
AFTER DELETE
ON ER_STUDYSEC
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
--  deleted_data varchar2(2000);
  deleted_data clob;
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYSEC', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_studysec) || '|' ||
  to_char(:old.fk_study) || '|' ||
  :old.studysec_name || '|' ||
  :old.studysec_pubflag || '|' ||
  to_char(:old.studysec_seq) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add||'|'||
  dbms_lob.substr(:old.STUDYSEC_TEXT,4000,1);
insert into audit_delete
(raid, row_data) values (raid, dbms_lob.substr(deleted_data,4000,1));
end;
/


