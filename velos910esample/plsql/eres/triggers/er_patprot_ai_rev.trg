CREATE OR REPLACE TRIGGER "ER_PATPROT_AI_REV" 
AFTER INSERT
ON ER_PATPROT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
/* **********************************
   **
   ** Author: Sonia Sahni 04/15/2004
   ** for reverse A2A
   ** Insert a record in er_rev_pendingdata
     ** Modified by Sonia abrol 03/07/06, for reverse A2A CCDID enhancements
	 ** Modified by Sonia abrol 03/07/06, for reverse A2A CCDID enhancements, send reg date
   *************************************
*/
v_key NUMBER;
v_pk NUMBER;
v_tabname VARCHAR2(50);
v_per NUMBER;
v_account NUMBER;
v_site_code VARCHAR2(255);
v_sponsor_study NUMBER;
v_err VARCHAR2(4000);

v_module_pat VARCHAR2(20);
v_module_commmon VARCHAR2(20);
v_sponsor_account NUMBER;


v_ccd_Site NUMBER;
v_patfacility_pk NUMBER;


BEGIN

v_module_pat := 'pat_enr';
v_module_commmon := 'pat_common';

-- get account information and site code
v_per := :NEW.FK_PER;

  -- get account site code and sponsor study for :new.fk_study

PKG_IMPEX_REVERSE.SP_GET_STUDYSPONSORINFO(:NEW.FK_STUDY, v_sponsor_study , v_site_code, v_sponsor_account, v_err);

IF LENGTH(trim(v_site_code)) >  0 THEN
   IF v_sponsor_study > 0 THEN

     	 -- get CCD site for the study

	 SELECT  NVL(network_site,0) INTO v_ccd_Site  FROM ER_REV_STUDYSPONSOR
	   		 	WHERE FK_SITESTUDY = :NEW.FK_STUDY;

			IF (v_ccd_Site > 0) THEN -- insert patient facility id for the patient and ccd site

							 -- check if the patient already has CCDID for the CCD Site, if he does, then update

							 BEGIN
							 				 SELECT NVL(pk_patfacility,0)
											 INTO v_patfacility_pk
							 				 FROM ER_PATFACILITY WHERE fk_per = :NEW.FK_PER AND fk_site = v_ccd_Site;

								EXCEPTION WHEN NO_DATA_FOUND THEN
										  v_patfacility_pk := 0;
								END ;

						IF v_patfacility_pk = 0 THEN
							 SELECT seq_er_patfacility.NEXTVAL INTO  v_patfacility_pk FROM dual;

							   INSERT INTO ER_PATFACILITY ( PK_PATFACILITY, FK_PER, FK_SITE, PAT_FACILITYID,
							 		PATFACILITY_ACCESSRIGHT, PATFACILITY_DEFAULT, CREATED_ON, IS_READONLY ,PATFACILITY_REGDATE) VALUES (
									v_patfacility_pk,  :NEW.FK_PER , v_ccd_Site, pkg_patient.f_generate_CCID( :NEW.FK_PER) , 7
									, 0,  SYSDATE, 1,SYSDATE);
						/* else
									update er_patfacility set PAT_FACILITYID = pkg_patient.f_generate_CCID( :NEW.FK_PER),PATFACILITY_ACCESSRIGHT = 7,
									 PATFACILITY_DEFAULT = 0, IS_READONLY = 1 ,  PATFACILITY_REGDATE = sysdate where pk_patfacility = v_patfacility_pk ;*/

						END IF; -- for v_patfacility_pk = 0

					 -- insert data in er_rev_pendingdata
					 	v_tabname := 'er_patfacility';
						v_pk := v_patfacility_pk;

						SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL	INTO v_key FROM dual;

						  INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
					    VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_pat);


			END IF;	 -- for v_ccd_site > 0


--insert data for enrollment
	v_tabname := 'er_patprot';
	v_pk := :NEW.PK_PATPROT ;
	SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	INTO v_key FROM dual;

  INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
  VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_pat);


--insert data for patient
	SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	INTO v_key FROM dual;
	v_tabname := 'er_per';
	v_pk := :NEW.FK_PER;

	INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE,FK_STUDY ,RP_MODULE)
	VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY , v_module_pat);

--insert data for users
	SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	INTO v_key FROM dual;
	v_tabname := 'er_user';
	v_pk := :NEW.FK_USER;

	INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
	VALUES (v_key ,v_tabname,v_pk,v_site_code , :NEW.FK_STUDY, v_module_commmon);

	SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	INTO v_key FROM dual;
	v_tabname := 'er_user';
	v_pk := :NEW.FK_USERASSTO;

	INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
	VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_commmon );

	SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
	INTO v_key FROM dual;
	v_tabname := 'er_user';
	v_pk := :NEW.PATPROT_PHYSICIAN;

	INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk , RP_SITECODE, FK_STUDY,RP_MODULE)
	VALUES (v_key ,v_tabname,v_pk,v_site_code,:NEW.FK_STUDY,v_module_commmon );



  END IF; -- for if v_sponsor_study > 0
 END IF; -- for v_site_code
END;
/


