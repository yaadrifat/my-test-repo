CREATE OR REPLACE TRIGGER "ER_USER_AD0" AFTER DELETE ON ER_USER
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_USER', :old.rid, 'D');

  deleted_data :=
  to_char(:old.pk_user) || '|' ||
  to_char(:old.fk_account) || '|' ||
  to_char(:old.fk_siteid) || '|' ||
  :old.usr_lastname || '|' ||
  :old.usr_firstname || '|' ||
  :old.usr_midname || '|' ||
  :old.usr_wrkexp || '|' ||
  :old.usr_pahseinv || '|' ||
  to_char(:old.usr_sesstime) || '|' ||
  :old.usr_logname || '|' ||
  :old.usr_pwd || '|' ||
  :old.usr_secques || '|' ||
  :old.usr_ans || '|' ||
  :old.usr_stat || '|' ||
  to_char(:old.fk_codelst_spl) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.fk_grp_default) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  to_char(:old.fk_peradd) || '|' ||
  to_char(:old.fk_codelst_jobtype) || '|' ||
  :old.usr_code || '|' ||
  to_char(:old.usr_pwdxpiry) || '|' ||
  to_char(:old.usr_pwddays) || '|' ||
  to_char(:old.usr_pwdremind) || '|' ||
  :old.usr_es || '|' ||
  to_char(:old.usr_esxpiry) || '|' ||
  to_char(:old.usr_esdays) || '|' ||
  to_char(:old.usr_esremind) || '|' ||
  :old.ip_add || '|' ||
  to_char(:old.fk_timezone) || '|' ||
  to_char(:old.usr_attemptcnt) || '|' ||
  to_char(:old.usr_loggedinflag) || '|' ||
  :old.usr_siteflag || '|' ||
  :old.user_hidden; --KM

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


