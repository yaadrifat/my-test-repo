CREATE OR REPLACE TRIGGER "ER_XUSRIGHT_BI0" BEFORE INSERT ON ER_XUSRIGHT
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
    usr := getuser(:NEW.creator);

SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
:NEW.rid := erid ;
SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_XUSRIGHT',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_XUSRIGHT||'|'|| :NEW.FK_STUDY||'|'||
     :NEW.FK_USERX||'|'|| :NEW.FK_USERGRANTOR||'|'||:NEW.XUSRIGHT_STAT||'|'||
  :NEW.XUSRIGHT||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
  :NEW.IP_ADD;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END tr_ER_XUSRIGHT_bichn_01;
/


