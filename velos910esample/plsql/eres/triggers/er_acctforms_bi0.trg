CREATE OR REPLACE TRIGGER ER_ACCTFORMS_BI0 BEFORE INSERT ON ER_ACCTFORMS
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
      raid NUMBER(10);
         erid NUMBER(10);
        usr VARCHAR(2000);
         insert_data CLOB;
     BEGIN
    BEGIN
        SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
 EXCEPTION
WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

             SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_ACCTFORMS',erid, 'I',usr);
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_ACCTFORMS||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.FK_ACCOUNT||'|'|| TO_CHAR(:NEW.ACCTFORMS_FILLDATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
     :NEW.RID||'|'||:NEW.RECORD_TYPE||'|'|| :NEW.FORM_COMPLETED||'|'||
  :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| :NEW.ISVALID||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'|| :NEW.NOTIFICATION_SENT||'|'||
   :NEW.PROCESS_DATA||'|'|| :NEW.FK_FORMLIBVER || '|' || :NEW.FK_SPECIMEN;

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/


