CREATE OR REPLACE TRIGGER "ER_STUDY_SITE_RIGHTS_AD0" AFTER DELETE ON ER_STUDY_SITE_RIGHTS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Gopu for Audit Report After Delete
DECLARE
   raid number(10);
   deleted_data varchar2(2000);
    usr VARCHAR(2000);

   usr := Getuser(:OLD.last_modified_by);

BEGIN
   select seq_audit.nextval into raid from dual;
   audit_trail.record_transaction (raid, 'ER_STUDY_SITE_RIGHTS', :old.rid, 'D', usr);
   deleted_data :=
      to_char(:old.PK_STUDY_SITE_RIGHTS) || '|' ||
      to_char(:old.FK_SITE) || '|' ||
      to_char(:old.FK_STUDY) || '|' ||
      to_char(:old.FK_USER) || '|' ||
      to_char(:old.USER_STUDY_SITE_RIGHTS) || '|' ||
      to_char(:old.rid) || '|' ||
      to_char(:old.creator) || '|' ||
      to_char(:old.last_modified_by) || '|' ||
      to_char(:old.last_modified_date) || '|' ||
      to_char(:old.created_on) || '|' ||
      :old.ip_add;
    insert into audit_delete (raid, row_data) values (raid, deleted_data);
END;
/


