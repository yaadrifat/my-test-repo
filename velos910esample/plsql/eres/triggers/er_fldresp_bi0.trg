create or replace TRIGGER "ER_FLDRESP_BI0" 
  before insert
  on er_fldresp
  for each row
   WHEN (new.rid is null or new.rid = 0) declare
  raid number(10);
  erid number(10);
  USR varchar2(100);

 begin
 begin

 Select to_char(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 into usr from er_user
 where pk_user = :new.creator ;
 Exception When NO_DATA_FOUND then
 USR := 'New User' ;
 End ;
  select trunc(seq_rid.nextval)
  into erid
  from dual;
  :new.rid := erid ;
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction

    (raid, 'ER_FLDRESP', erid, 'I', USR );
end;
/


