CREATE OR REPLACE TRIGGER "ER_SETTINGS_BU_LM" 
BEFORE UPDATE
ON ER_SETTINGS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Before update
begin :new.last_modified_date := sysdate;
end;
/


