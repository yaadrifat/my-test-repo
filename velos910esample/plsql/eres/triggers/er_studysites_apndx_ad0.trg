CREATE OR REPLACE TRIGGER "ER_STUDYSITES_APNDX_AD0" 
AFTER DELETE
ON ER_STUDYSITES_APNDX
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(2000);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  Audit_Trail.record_transaction
    (raid, 'ER_STUDYSITES_APNDX', :OLD.rid, 'D');
  deleted_data :=
  TO_CHAR(:OLD.pk_studysites_apndx) || '|' ||
  TO_CHAR(:OLD.fk_studysites) || '|' ||
  :OLD.apndx_type || '|' ||
  :OLD.apndx_name || '|' ||
  :OLD.apndx_description || '|' ||
TO_CHAR(:OLD.apndx_filesize) || '|' ||
TO_CHAR(:OLD.rid) || '|' ||
 TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_on) || '|' ||
  :OLD.ip_add;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


