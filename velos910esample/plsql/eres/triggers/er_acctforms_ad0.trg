CREATE OR REPLACE TRIGGER "ER_ACCTFORMS_AD0" 
AFTER
-- Added by Ganapathy on 28/06/05 for Audit Delete
 UPDATE--DELETE
ON ER_ACCTFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  rtype CHAR;
  usr VARCHAR(2000);

BEGIN
 rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:NEW.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'ER_ACCTFORMS', :OLD.rid, 'D',usr);

  deleted_data :=
  TO_CHAR(:OLD.pk_acctforms) || '|' ||
  TO_CHAR(:OLD.fk_formlib) || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  TO_CHAR(:OLD.acctforms_filldate) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.form_completed) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  TO_CHAR(:OLD.notification_sent) || '|' ||
  TO_CHAR(:OLD.process_data) || '|' ||
  TO_CHAR(:OLD.fk_formlibver) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.isvalid) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  :OLD.ip_add;
INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


