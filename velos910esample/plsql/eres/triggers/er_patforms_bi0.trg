CREATE OR REPLACE TRIGGER "ER_PATFORMS_BI0" BEFORE INSERT ON ER_PATFORMS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
 insert_data CLOB;
     BEGIN
  BEGIN

 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
 INTO usr FROM ER_USER
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

     SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
   INTO usr FROM ER_USER  WHERE pk_user = :NEW.creator ;
  audit_trail.record_transaction(raid, 'ER_PATFORMS',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_PATFORMS||'|'|| :NEW.FK_FORMLIB||'|'||
     :NEW.FK_PER||'|'|| :NEW.FK_PATPROT||'|'||
      TO_CHAR(:NEW.PATFORMS_FILLDATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
     :NEW.RID||'|'|| :NEW.FORM_COMPLETED||'|'||:NEW.RECORD_TYPE||'|'|| :NEW.CREATOR||'|'||
   :NEW.LAST_MODIFIED_BY||'|'|| :NEW.ISVALID||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||
    :NEW.NOTIFICATION_SENT||'|'|| :NEW.PROCESS_DATA||'|'|| :NEW.FK_FORMLIBVER || '|' || :NEW.FK_SPECIMEN;

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/


