create or replace TRIGGER "ER_STORAGE_BI0" BEFORE INSERT ON ER_STORAGE
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
--Created by Manimaran for Audit Insert
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_STORAGE',erid, 'I',usr);
   --KM-Template field added.
   insert_data:= :NEW.PK_STORAGE||'|'||:NEW.STORAGE_ID||'|'||:NEW.STORAGE_NAME||'|'||
   :NEW.FK_CODELST_STORAGE_TYPE||'|'||:NEW.FK_STORAGE||'|'||:NEW.STORAGE_CAP_NUMBER||'|'||
   :NEW.FK_CODELST_CAPACITY_UNITS||'|'||--:NEW.STORAGE_STATUS||'|'||--KM
   :NEW.STORAGE_DIM1_CELLNUM||'|'||:NEW.STORAGE_DIM1_NAMING||'|'||:NEW.STORAGE_DIM1_ORDER||'|'||
   :NEW.STORAGE_DIM2_CELLNUM||'|'||:NEW.STORAGE_DIM2_NAMING||'|'||:NEW.STORAGE_DIM1_ORDER||'|'||
   :NEW.STORAGE_AVAIL_UNIT_COUNT||'|'||:NEW.STORAGE_COORDINATE_X||'|'||
   :NEW.STORAGE_COORDINATE_Y||'|'||:NEW.STORAGE_LABEL||'|'||
   :NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
   ||'|'||:NEW.IP_ADD||'|'||:NEW.FK_ACCOUNT||'|'||:NEW.STORAGE_ISTEMPLATE|| '|' ||
   :NEW.STORAGE_TEMPLATE_TYPE|| '|' ||:NEW.STORAGE_ALTERNALEID|| '|' ||:NEW.STORAGE_UNIT_CLASS|| '|' ||:NEW.STORAGE_KIT_CATEGORY|| '|' ||:NEW.FK_STORAGE_KIT;--YK Bug#5827

   INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END ;
/