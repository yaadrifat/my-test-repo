CREATE OR REPLACE TRIGGER "ERES"."ER_STUDY_INDIDE_BI0" BEFORE INSERT ON ER_STUDY_INDIDE 
FOR EACH ROW 
DECLARE
	raid NUMBER(10);
	erid NUMBER(10);
	usr VARCHAR(2000);
	insert_data CLOB;
BEGIN
	BEGIN
		SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
		WHERE pk_user = :NEW.creator ;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		USR := 'New User' ;
	END ;

	SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
	:NEW.rid := erid ;
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;

	audit_trail.record_transaction(raid, 'ER_STUDY_INDIDE',erid, 'I',usr);
	insert_data:= :NEW.PK_STUDY_INDIIDE||'|'||
	:NEW.FK_STUDY||'|'||
	:NEW.INDIDE_TYPE||'|'||
	:NEW.INDIDE_NUMBER||'|'||
	:NEW.FK_CODELST_INDIDE_GRANTOR||'|'||
	:NEW.FK_CODELST_INDIDE_HOLDER||'|'||
	:NEW.FK_CODELST_PROGRAM_CODE||'|'||
	:NEW.INDIDE_EXPAND_ACCESS||'|'||
	:NEW.FK_CODELST_ACCESS_TYPE||'|'||
	:NEW.INDIDE_EXEMPT||'|'||
	:NEW.RID||'|'||
	:NEW.IP_ADD||'|'||
	:NEW.CREATOR||'|'||
	TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||
	:NEW.LAST_MODIFIED_BY||'|'||
	TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);
	
	INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END; 
/
