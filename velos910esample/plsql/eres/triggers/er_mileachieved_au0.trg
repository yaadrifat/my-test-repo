CREATE OR REPLACE TRIGGER ER_MILEACHIEVED_AU0
AFTER UPDATE
ON ER_MILEACHIEVED REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:NEW.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'ER_MILEACHIEVED', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_mileachieved ,0) !=
     NVL(:NEW.pk_mileachieved ,0) THEN
     Audit_Trail.column_update
       (raid, 'pk_mileachieved ',
       :OLD.pk_mileachieved , :NEW.pk_mileachieved );
  END IF;

  IF NVL(:OLD.fk_milestone,0) !=
     NVL(:NEW.fk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'fk_milestone',
       :OLD.fk_milestone, :NEW.fk_milestone);
  END IF;

  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     Audit_Trail.column_update
       (raid, 'fk_per',
       :OLD.fk_per, :NEW.fk_per);
  END IF;

  IF NVL(:OLD.table_name,' ') !=
     NVL(:NEW.table_name,' ') THEN
     Audit_Trail.column_update
       (raid, 'table_name',
       :OLD.table_name, :NEW.table_name);
  END IF;

IF NVL(:OLD.ach_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.ach_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'ach_date',
       to_char(:OLD.ach_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.ach_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'fk_study',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
---------------------------------
  IF NVL(:OLD.fk_patprot ,0) !=     NVL(:NEW.fk_patprot ,0) THEN
     Audit_Trail.column_update   (raid, 'fk_patprot ',    :OLD.fk_patprot , :NEW.fk_patprot );
  END IF;

  IF NVL(:OLD.table_pk,0) !=     NVL(:NEW.table_pk,0) THEN
     Audit_Trail.column_update   (raid, 'table_pk',    :OLD.table_pk, :NEW.table_pk);
  END IF;

old_modby := '';
new_modby := '';

  IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
      BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',    to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update (raid, 'CREATED_ON',       to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
  END IF;

END;
/


