CREATE OR REPLACE TRIGGER "ER_DYNREPVIEW_AD0" AFTER DELETE ON ER_DYNREPVIEW
FOR EACH ROW
--Created by Gopu for After Delete
DECLARE
   raid number(10);
   deleted_data varchar2(2000);
BEGIN
   select seq_audit.nextval into raid from dual;
   audit_trail.record_transaction (raid, 'ER_DYNREPVIEW', :old.rid, 'D');
   deleted_data :=
      to_char(:old.PK_DYNREPVIEW) || '|' ||
      to_char(:old.FK_DYNREP) || '|' ||
      :old.DYNREPVIEW_COL || '|' ||
      to_char(:old.DYNREPVIEW_SEQ) || '|' ||
      :old.DYNREPVIEW_WIDTH || '|' ||
      :old.DYNREPVIEW_FORMAT || '|' ||
      :old.DYNREPVIEW_DISPNAME || '|' ||
      to_char(:old.rid) || '|' ||
      to_char(:old.creator) || '|' ||
      to_char(:old.last_modified_by) || '|' ||
      to_char(:old.last_modified_date) || '|' ||
      to_char(:old.created_on) || '|' ||
      :old.ip_add || '|' ||
      :old.DYNREPVIEW_COLNAME || '|' ||
      :old.DYNREPVIEW_COLDATATYPE || '|' ||
      to_char(:old.FK_FORM) || '|' ||
      to_char(:old.FORM_TYPE) || '|' ||
      to_char(:old.USEUNIQUEID) || '|' ||
      to_char(:old.USEDATAVAL) || '|' ||
      :old.CALCPER || '|' ||
      :old.CALCSUM || '|' ||
      :old.REPEAT_NUMBER ;
    insert into audit_delete (raid, row_data) values (raid, deleted_data);
END;
/


