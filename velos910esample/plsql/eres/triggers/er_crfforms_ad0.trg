CREATE OR REPLACE TRIGGER "ER_CRFFORMS_AD0" 
AFTER  UPDATE-- DELETE
ON ER_CRFFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'ER_CRFFORMS', :OLD.rid, 'D');
  deleted_data :=
  TO_CHAR(:OLD.pk_crfforms) || '|' ||
  TO_CHAR(:OLD.fk_formlib) || '|' ||
  TO_CHAR(:OLD.fk_schevent1) || '|' ||
  TO_CHAR(:OLD.fk_crf) || '|' ||
  TO_CHAR(:OLD.crfforms_filldate) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.form_completed) || '|' ||
  TO_CHAR(:OLD.isvalid) || '|' ||
  TO_CHAR(:OLD.notification_sent) || '|' ||
  TO_CHAR(:OLD.process_data) || '|' ||
  TO_CHAR(:OLD.fk_formlibver) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;
INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


