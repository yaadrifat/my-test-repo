create or replace TRIGGER ERES.ER_CODELST_AU0
AFTER UPDATE
ON ERES.ER_CODELST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  old_account varchar2(30) ;
  new_account varchar2(30) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_CODELST', :old.rid, 'U', usr);
  if nvl(:old.pk_codelst,0) !=
     NVL(:new.pk_codelst,0) then
     audit_trail.column_update
       (raid, 'PK_CODELST',
       :old.pk_codelst, :new.pk_codelst);
  end if;
  if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
    Begin
     select ac_name into old_account
     from er_account where pk_account = :old.fk_account ;
    Exception When NO_DATA_FOUND then
     old_account := null ;
    End ;
    Begin
     select ac_name into new_account
     from er_account where pk_account = :new.fk_account ;
    Exception When NO_DATA_FOUND then
     new_account := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       old_account , new_account );
  end if;
  if nvl(:old.codelst_type,' ') !=
     NVL(:new.codelst_type,' ') then
     audit_trail.column_update
       (raid, 'CODELST_TYPE',
       :old.codelst_type, :new.codelst_type);
  end if;
  if nvl(:old.codelst_subtyp,' ') !=
     NVL(:new.codelst_subtyp,' ') then
     audit_trail.column_update
       (raid, 'CODELST_SUBTYP',
       :old.codelst_subtyp, :new.codelst_subtyp);
  end if;
  if nvl(:old.codelst_desc,' ') !=
     NVL(:new.codelst_desc,' ') then
     audit_trail.column_update
       (raid, 'CODELST_DESC',
       :old.codelst_desc, :new.codelst_desc);
  end if;
  if nvl(:old.codelst_hide,' ') !=
     NVL(:new.codelst_hide,' ') then
     audit_trail.column_update
       (raid, 'CODELST_HIDE',
       :old.codelst_hide, :new.codelst_hide);
  end if;
  if nvl(:old.codelst_seq,0) !=
     NVL(:new.codelst_seq,0) then
     audit_trail.column_update
       (raid, 'CODELST_SEQ',
       :old.codelst_seq, :new.codelst_seq);
  end if;
  if nvl(:old.codelst_maint,' ') !=
     NVL(:new.codelst_maint,' ') then
     audit_trail.column_update
       (raid, 'CODELST_MAINT',
       :old.codelst_maint, :new.codelst_maint);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
    Exception When NO_DATA_FOUND then
     old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
    Exception When NO_DATA_FOUND then
     new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;

  if nvl(:old.CODELST_CUSTOM_COL,' ') !=
     NVL(:new.CODELST_CUSTOM_COL,' ') then
     audit_trail.column_update
       (raid, 'CODELST_CUSTOM_COL',
       :old.CODELST_CUSTOM_COL, :new.CODELST_CUSTOM_COL);
  end if;


  if nvl(:old.CODELST_CUSTOM_COL1,' ') !=
     NVL(:new.CODELST_CUSTOM_COL1,' ') then
     audit_trail.column_update
       (raid, 'CODELST_CUSTOM_COL1',
       :old.CODELST_CUSTOM_COL1, :new.CODELST_CUSTOM_COL1);
  end if;


  if nvl(:old.CODELST_STUDY_ROLE,' ') !=
     NVL(:new.CODELST_STUDY_ROLE,' ') then
     audit_trail.column_update
       (raid, 'CODELST_STUDY_ROLE',
       :old.CODELST_STUDY_ROLE, :new.CODELST_STUDY_ROLE);
  end if;


end;
/