CREATE OR REPLACE TRIGGER "CB_MAIL_DOCUMENTS_AU0"
AFTER UPDATE OF
PK_MAIL_DOCUMENTS,
DOCUMENT_FILE,
DOCUMENT_CONTENTTYPE,
DOCUMENT_FILENAME,
ENTITY_ID,
ENTITY_TYPE,
ATTACHMENT_TYPE,
RID,
LAST_MODIFIED_BY,
CREATOR ON CB_MAIL_DOCUMENTS REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
    RAID NUMBER(10);
    USR VARCHAR(200); 
    OLD_MODIFIED_BY VARCHAR2(100);
    NEW_MODIFIED_BY VARCHAR2(100);
BEGIN

 SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;
 
     USR := GETUSER(:NEW.LAST_MODIFIED_BY);
 
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CB_MAIL_DOCUMENTS', :OLD.RID, 'U', USR);    
  
  IF NVL(:OLD.PK_MAIL_DOCUMENTS,0) != NVL(:NEW.PK_MAIL_DOCUMENTS,0) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'PK_MAIL_DOCUMENTS', :OLD.PK_MAIL_DOCUMENTS, :NEW.PK_MAIL_DOCUMENTS);
    END IF;        
    IF NVL(:OLD.DOCUMENT_FILE,0) != NVL(:NEW.DOCUMENT_FILE,0) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'DOCUMENT_FILE',:OLD.DOCUMENT_FILE, :NEW.DOCUMENT_FILE);
    END IF;    
    IF NVL(:OLD.DOCUMENT_CONTENTTYPE,' ') != NVL(:NEW.DOCUMENT_CONTENTTYPE,' ') THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'DOCUMENT_CONTENTTYPE',:OLD.DOCUMENT_CONTENTTYPE, :NEW.DOCUMENT_CONTENTTYPE);
    END IF;
    IF NVL(:OLD.DOCUMENT_FILENAME,' ') != NVL(:NEW.DOCUMENT_FILENAME,' ') THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'DOCUMENT_FILENAME',:OLD.DOCUMENT_FILENAME, :NEW.DOCUMENT_FILENAME);
    END IF;    
    IF NVL(:OLD.ENTITY_ID,0) != NVL(:NEW.ENTITY_ID,0) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'ENTITY_ID',:OLD.ENTITY_ID, :NEW.ENTITY_ID);
    END IF;     
    IF NVL(:OLD.ENTITY_TYPE,' ') != NVL(:NEW.ENTITY_TYPE,' ') THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'ENTITY_TYPE',:OLD.ENTITY_TYPE, :NEW.ENTITY_TYPE);
    END IF;        
    IF NVL(:OLD.ATTACHMENT_TYPE,' ') != NVL(:NEW.ATTACHMENT_TYPE,' ') THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'ATTACHMENT_TYPE',:OLD.ATTACHMENT_TYPE, :NEW.ATTACHMENT_TYPE);
    END IF;    
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'CREATOR',:OLD.CREATOR, :NEW.CREATOR);
    END IF;     
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'RID', :OLD.RID, :NEW.RID);     
    END IF;
    
    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
       Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
            old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
            new_modified_by := null;
    End ;
      AUDIT_TRAIL.COLUMN_UPDATE (RAID,'LAST_MODIFIED_BY' , OLD_MODIFIED_BY, NEW_MODIFIED_BY);    
    END IF; 
END;
/

