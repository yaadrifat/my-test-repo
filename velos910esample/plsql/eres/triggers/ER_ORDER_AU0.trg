create or replace TRIGGER "ERES".ER_ORDER_AU0 after update of
PK_ORDER,ORDER_TYPE,FK_ORDER_HEADER,ORDER_STATUS,RECENT_HLA_ENTERED_FLAG,ADDITI_TYPING_FLAG,CBU_AVAIL_CONFIRM_FLAG,IS_CORD_AVAIL_FOR_NMDP,CORD_AVAIL_CONFIRM_DATE,SHIPMENT_SCH_FLAG,CORD_SHIPPED_FLAG,PACKAGE_SLIP_FLAG,NMDP_SAMPLE_SHIPPED_FLAG,COMPLETE_REQ_INFO_TASK_FLAG,
FINAL_REVIEW_TASK_FLAG,ORDER_LAST_VIEWED_BY,ORDER_LAST_VIEWED_DATE,ORDER_VIEW_CONFIRM_FLAG,ORDER_VIEW_CONFIRM_DATE,ORDER_PRIORITY,
FK_CURRENT_HLA,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID,TASK_ID,TASK_NAME,ASSIGNED_TO,ORDER_STATUS_DATE,
ORDER_REVIEWED_BY,ORDER_REVIEWED_DATE,ORDER_ASSIGNED_DATE,FK_ORDER_RESOL_BY_CBB,FK_ORDER_RESOL_BY_TC,ORDER_RESOL_DATE,ORDER_ACK_FLAG,
ORDER_ACK_DATE,ORDER_ACK_BY,RESULT_REC_DATE,ACCPT_TO_CANCEL_REQ,CANCEL_CONFORM_DATE,CANCELED_BY,CLINIC_INFO_CHECKLIST_STAT,RECENT_HLA_TYPING_AVAIL,
ADDI_TEST_RESULT_AVAIL,ORDER_SAMPLE_AT_LAB,FK_CASE_MANAGER,CASE_MANAGER,TRANS_CENTER_ID,TRANS_CENTER_NAME,SEC_TRANS_CENTER_NAME,ORDER_PHYSICIAN,
SEC_TRANS_CENTER_ID,CM_MAIL_ID,CM_CONTACT_NO,REQ_CLIN_INFO_FLAG,RESOL_ACK_FLAG,FK_SAMPLE_TYPE_AVAIL,
FK_ALIQUOTS_TYPE  ON ER_ORDER   for each row
declare
  raid number(10);

   usr varchar2(100);

   old_modby varchar2(100);

   new_modby varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_ORDER', :old.RID, 'U', usr);

  if nvl(:old.PK_ORDER,0) != NVL(:new.PK_ORDER,0) then
     audit_trail.column_update(raid, 'PK_ORDER',:old.PK_ORDER, :new.PK_ORDER);
  end if;
  if nvl(:old.ORDER_TYPE,0) != NVL(:new.ORDER_TYPE,0) then
     audit_trail.column_update(raid, 'ORDER_TYPE',:old.ORDER_TYPE, :new.ORDER_TYPE);
  end if;
 if nvl(:old.FK_ORDER_HEADER,0) != NVL(:new.FK_ORDER_HEADER,0) then
     audit_trail.column_update(raid, 'FK_ORDER_HEADER',:old.FK_ORDER_HEADER, :new.FK_ORDER_HEADER);
  end if;
  if nvl(:old.ORDER_STATUS,0) != NVL(:new.ORDER_STATUS,0) then
     audit_trail.column_update(raid, 'ORDER_STATUS',:old.ORDER_STATUS, :new.ORDER_STATUS);
  end if;
  if nvl(:old.RECENT_HLA_ENTERED_FLAG,' ') !=  NVL(:new.RECENT_HLA_ENTERED_FLAG,' ') then
     audit_trail.column_update(raid, 'RECENT_HLA_ENTERED_FLAG',:old.RECENT_HLA_ENTERED_FLAG, :new.RECENT_HLA_ENTERED_FLAG);
  end if;
  if nvl(:old.ADDITI_TYPING_FLAG,' ') !=  NVL(:new.ADDITI_TYPING_FLAG,' ') then
     audit_trail.column_update(raid, 'ADDITI_TYPING_FLAG',:old.ADDITI_TYPING_FLAG, :new.ADDITI_TYPING_FLAG);
  end if;
  if nvl(:old.CBU_AVAIL_CONFIRM_FLAG,' ') !=  NVL(:new.CBU_AVAIL_CONFIRM_FLAG,' ') then
     audit_trail.column_update(raid, 'CBU_AVAIL_CONFIRM_FLAG',:old.CBU_AVAIL_CONFIRM_FLAG, :new.CBU_AVAIL_CONFIRM_FLAG);
  end if;
  if nvl(:old.IS_CORD_AVAIL_FOR_NMDP,' ') !=  NVL(:new.IS_CORD_AVAIL_FOR_NMDP,' ') then
     audit_trail.column_update(raid, 'IS_CORD_AVAIL_FOR_NMDP',:old.IS_CORD_AVAIL_FOR_NMDP, :new.IS_CORD_AVAIL_FOR_NMDP);
  end if;
  if nvl(:old.CORD_AVAIL_CONFIRM_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=  NVL(:new.CORD_AVAIL_CONFIRM_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'CORD_AVAIL_CONFIRM_DATE',
       to_char(:old.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.SHIPMENT_SCH_FLAG,' ') !=  NVL(:new.SHIPMENT_SCH_FLAG,' ') then
     audit_trail.column_update(raid, 'SHIPMENT_SCH_FLAG',:old.SHIPMENT_SCH_FLAG, :new.SHIPMENT_SCH_FLAG);
  end if;
  if nvl(:old.CORD_SHIPPED_FLAG,' ') !=  NVL(:new.CORD_SHIPPED_FLAG,' ') then
     audit_trail.column_update(raid, 'CORD_SHIPPED_FLAG',:old.CORD_SHIPPED_FLAG, :new.CORD_SHIPPED_FLAG);
  end if;
  if nvl(:old.PACKAGE_SLIP_FLAG,' ') !=  NVL(:new.PACKAGE_SLIP_FLAG,' ') then
     audit_trail.column_update(raid, 'PACKAGE_SLIP_FLAG',:old.PACKAGE_SLIP_FLAG, :new.PACKAGE_SLIP_FLAG);
  end if;
  if nvl(:old.NMDP_SAMPLE_SHIPPED_FLAG,' ') !=  NVL(:new.NMDP_SAMPLE_SHIPPED_FLAG,' ') then
     audit_trail.column_update(raid, 'NMDP_SAMPLE_SHIPPED_FLAG',:old.NMDP_SAMPLE_SHIPPED_FLAG, :new.NMDP_SAMPLE_SHIPPED_FLAG);
  end if;
  if nvl(:old.COMPLETE_REQ_INFO_TASK_FLAG,' ') !=  NVL(:new.COMPLETE_REQ_INFO_TASK_FLAG,' ') then
     audit_trail.column_update(raid, 'COMPLETE_REQ_INFO_TASK_FLAG',:old.COMPLETE_REQ_INFO_TASK_FLAG, :new.COMPLETE_REQ_INFO_TASK_FLAG);
  end if;
  if nvl(:old.FINAL_REVIEW_TASK_FLAG,' ') !=  NVL(:new.FINAL_REVIEW_TASK_FLAG,' ') then
     audit_trail.column_update(raid, 'FINAL_REVIEW_TASK_FLAG',:old.FINAL_REVIEW_TASK_FLAG, :new.FINAL_REVIEW_TASK_FLAG);
  end if;
  if nvl(:old.ORDER_LAST_VIEWED_BY,0) !=  NVL(:new.ORDER_LAST_VIEWED_BY,0) then
     audit_trail.column_update(raid, 'ORDER_LAST_VIEWED_BY',:old.ORDER_LAST_VIEWED_BY, :new.ORDER_LAST_VIEWED_BY);
  end if;
  if nvl(:old.ORDER_LAST_VIEWED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ORDER_LAST_VIEWED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'ORDER_LAST_VIEWED_DATE',
       to_char(:old.ORDER_LAST_VIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_LAST_VIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ORDER_VIEW_CONFIRM_FLAG,' ') !=  NVL(:new.ORDER_VIEW_CONFIRM_FLAG,' ') then
     audit_trail.column_update(raid, 'ORDER_VIEW_CONFIRM_FLAG',:old.ORDER_VIEW_CONFIRM_FLAG, :new.ORDER_VIEW_CONFIRM_FLAG);
  end if;
  if nvl(:old.ORDER_VIEW_CONFIRM_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ORDER_VIEW_CONFIRM_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'ORDER_VIEW_CONFIRM_DATE',
       to_char(:old.ORDER_VIEW_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_VIEW_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ORDER_PRIORITY,0) != NVL(:new.ORDER_PRIORITY,0) then
     audit_trail.column_update(raid, 'ORDER_PRIORITY',:old.ORDER_PRIORITY, :new.ORDER_PRIORITY);
  end if;
  if nvl(:old.FK_CURRENT_HLA,0) != NVL(:new.FK_CURRENT_HLA,0) then
     audit_trail.column_update(raid, 'FK_CURRENT_HLA',:old.FK_CURRENT_HLA, :new.FK_CURRENT_HLA);
  end if;
   if nvl(:old.CREATOR,0) !=     NVL(:new.CREATOR,0) then
     audit_trail.column_update (raid, 'CREATOR', :old.CREATOR, :new.CREATOR);
  end if;
   if nvl(:old.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from er_user  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;
   if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
   if nvl(:old.ip_add,' ') !=  NVL(:new.ip_add,' ') then
     audit_trail.column_update(raid, 'IP_ADD',:old.ip_add, :new.ip_add);
  end if;  
  if nvl(:old.DELETEDFLAG,0) !=   NVL(:new.DELETEDFLAG,0) then
     audit_trail.column_update (raid, 'DELETEDFLAG',:old.DELETEDFLAG, :new.DELETEDFLAG);
  end if;
  if nvl(:old.rid,0) !=    NVL(:new.rid,0) then
     audit_trail.column_update (raid, 'RID',:old.rid, :new.rid);
  end if;  
  if nvl(:old.TASK_ID,0) !=   NVL(:new.TASK_ID,0) then
     audit_trail.column_update  (raid, 'TASK_ID', :old.TASK_ID, :new.TASK_ID);
  end if;
    if nvl(:old.TASK_NAME,' ') !=  NVL(:new.TASK_NAME,' ') then
     audit_trail.column_update  (raid, 'TASK_NAME',       :old.TASK_NAME, :new.TASK_NAME);
  end if;
  if nvl(:old.ASSIGNED_TO,0) !=  NVL(:new.ASSIGNED_TO,0) then
     audit_trail.column_update (raid, 'ASSIGNED_TO', :old.ASSIGNED_TO, :new.ASSIGNED_TO);
  end if;
  if nvl(:old.ORDER_STATUS_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ORDER_STATUS_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'ORDER_STATUS_DATE',
       to_char(:old.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ORDER_REVIEWED_BY,0) !=   NVL(:new.ORDER_REVIEWED_BY,0) then
     audit_trail.column_update  (raid, 'ORDER_REVIEWED_BY',:old.ORDER_REVIEWED_BY, :new.ORDER_REVIEWED_BY);
  end if;
  if nvl(:old.ORDER_REVIEWED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ORDER_REVIEWED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'ORDER_REVIEWED_DATE', to_char(:old.ORDER_REVIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_REVIEWED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
   if nvl(:old.ORDER_ASSIGNED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ORDER_ASSIGNED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update (raid, 'ORDER_ASSIGNED_DATE',
       to_char(:old.ORDER_ASSIGNED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_ASSIGNED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;  
   if nvl(:old.FK_ORDER_RESOL_BY_CBB,0) !=   NVL(:new.FK_ORDER_RESOL_BY_CBB,0) then
     audit_trail.column_update   (raid, 'FK_ORDER_RESOL_BY_CBB',   :old.FK_ORDER_RESOL_BY_CBB, :new.FK_ORDER_RESOL_BY_CBB);
  end if;  
   if nvl(:old.FK_ORDER_RESOL_BY_TC,0) !=    NVL(:new.FK_ORDER_RESOL_BY_TC,0) then
     audit_trail.column_update  (raid, 'FK_ORDER_RESOL_BY_TC',  :old.FK_ORDER_RESOL_BY_TC, :new.FK_ORDER_RESOL_BY_TC);
  end if;  
    if nvl(:old.ORDER_RESOL_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ORDER_RESOL_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'ORDER_RESOL_DATE',
       to_char(:old.ORDER_RESOL_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_RESOL_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if; 
  if nvl(:old.ORDER_ACK_FLAG,' ') !=   NVL(:new.ORDER_ACK_FLAG,' ') then
     audit_trail.column_update   (raid, 'ORDER_ACK_FLAG',
       :old.ORDER_ACK_FLAG, :new.ORDER_ACK_FLAG);
  end if;
   if nvl(:old.ORDER_ACK_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ORDER_ACK_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update     (raid, 'ORDER_ACK_DATE',
       to_char(:old.ORDER_ACK_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ORDER_ACK_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if; 
   if nvl(:old.ORDER_ACK_BY,0) !=     NVL(:new.ORDER_ACK_BY,0) then
     audit_trail.column_update  (raid, 'ORDER_ACK_BY',       :old.ORDER_ACK_BY, :new.ORDER_ACK_BY);
  end if; 
   if nvl(:old.RESULT_REC_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.RESULT_REC_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'RESULT_REC_DATE',
       to_char(:old.RESULT_REC_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.RESULT_REC_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if; 
  if nvl(:old.ACCPT_TO_CANCEL_REQ,' ') !=  NVL(:new.ACCPT_TO_CANCEL_REQ,' ') then
     audit_trail.column_update(raid, 'ACCPT_TO_CANCEL_REQ', :old.ACCPT_TO_CANCEL_REQ, :new.ACCPT_TO_CANCEL_REQ);
  end if;
   if nvl(:old.CANCEL_CONFORM_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.CANCEL_CONFORM_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CANCEL_CONFORM_DATE',to_char(:old.CANCEL_CONFORM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.CANCEL_CONFORM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;   
   if nvl(:old.CANCELED_BY,0) !=     NVL(:new.CANCELED_BY,0) then
     audit_trail.column_update (raid, 'CANCELED_BY', :old.CANCELED_BY, :new.CANCELED_BY);
  end if;   
   if nvl(:old.CLINIC_INFO_CHECKLIST_STAT,' ') !=   NVL(:new.CLINIC_INFO_CHECKLIST_STAT,' ') then
     audit_trail.column_update   (raid, 'CLINIC_INFO_CHECKLIST_STAT',:old.CLINIC_INFO_CHECKLIST_STAT, :new.CLINIC_INFO_CHECKLIST_STAT);
  end if;
  if nvl(:old.RECENT_HLA_TYPING_AVAIL,' ') !=    NVL(:new.RECENT_HLA_TYPING_AVAIL,' ') then
     audit_trail.column_update (raid, 'RECENT_HLA_TYPING_AVAIL',   :old.RECENT_HLA_TYPING_AVAIL, :new.RECENT_HLA_TYPING_AVAIL);
  end if;
  if nvl(:old.ADDI_TEST_RESULT_AVAIL,' ') !=  NVL(:new.ADDI_TEST_RESULT_AVAIL,' ') then
     audit_trail.column_update (raid, 'ADDI_TEST_RESULT_AVAIL', :old.ADDI_TEST_RESULT_AVAIL, :new.ADDI_TEST_RESULT_AVAIL);
  end if;
  if nvl(:old.ORDER_SAMPLE_AT_LAB,' ') != NVL(:new.ORDER_SAMPLE_AT_LAB,' ') then
     audit_trail.column_update(raid, 'ORDER_SAMPLE_AT_LAB', :old.ORDER_SAMPLE_AT_LAB, :new.ORDER_SAMPLE_AT_LAB);
  end if;
   if nvl(:old.FK_CASE_MANAGER,0) !=     NVL(:new.FK_CASE_MANAGER,0) then
     audit_trail.column_update(raid, 'FK_CASE_MANAGER',  :old.FK_CASE_MANAGER, :new.FK_CASE_MANAGER);
  end if; 
   if nvl(:old.CASE_MANAGER,' ') !=     NVL(:new.CASE_MANAGER,' ') then
     audit_trail.column_update(raid, 'CASE_MANAGER', :old.CASE_MANAGER, :new.CASE_MANAGER);
  end if;
   if nvl(:old.TRANS_CENTER_ID,0) != NVL(:new.TRANS_CENTER_ID,0) then
     audit_trail.column_update (raid, 'TRANS_CENTER_ID',  :old.TRANS_CENTER_ID, :new.TRANS_CENTER_ID);
  end if; 
    if nvl(:old.TRANS_CENTER_NAME,' ') !=   NVL(:new.TRANS_CENTER_NAME,' ') then
     audit_trail.column_update (raid, 'TRANS_CENTER_NAME', :old.TRANS_CENTER_NAME, :new.TRANS_CENTER_NAME);
  end if;
    if nvl(:old.SEC_TRANS_CENTER_NAME,' ') != NVL(:new.SEC_TRANS_CENTER_NAME,' ') then
     audit_trail.column_update (raid, 'SEC_TRANS_CENTER_NAME', :old.SEC_TRANS_CENTER_NAME, :new.SEC_TRANS_CENTER_NAME);
  end if;
 if nvl(:old.ORDER_PHYSICIAN,0) !=    NVL(:new.ORDER_PHYSICIAN,0) then
     audit_trail.column_update      (raid, 'ORDER_PHYSICIAN',       :old.ORDER_PHYSICIAN, :new.ORDER_PHYSICIAN);
  end if; 
   if nvl(:old.SEC_TRANS_CENTER_ID,' ') !=  NVL(:new.SEC_TRANS_CENTER_ID,' ') then
     audit_trail.column_update       (raid, 'SEC_TRANS_CENTER_ID',    :old.SEC_TRANS_CENTER_ID, :new.SEC_TRANS_CENTER_ID);
  end if;
if nvl(:old.CM_MAIL_ID,' ') !=  NVL(:new.CM_MAIL_ID,' ') then
     audit_trail.column_update(raid, 'CM_MAIL_ID',:old.CM_MAIL_ID, :new.CM_MAIL_ID);
  end if;
if nvl(:old.CM_CONTACT_NO,' ') != NVL(:new.CM_CONTACT_NO,' ') then
     audit_trail.column_update(raid, 'CM_CONTACT_NO',:old.CM_CONTACT_NO, :new.CM_CONTACT_NO);
  end if;
if nvl(:old.REQ_CLIN_INFO_FLAG,' ') !=NVL(:new.REQ_CLIN_INFO_FLAG,' ') then
     audit_trail.column_update(raid, 'REQ_CLIN_INFO_FLAG',:old.REQ_CLIN_INFO_FLAG, :new.REQ_CLIN_INFO_FLAG);
  end if;
if nvl(:old.RESOL_ACK_FLAG,' ') !=NVL(:new.RESOL_ACK_FLAG,' ') then
     audit_trail.column_update (raid, 'RESOL_ACK_FLAG',:old.RESOL_ACK_FLAG, :new.RESOL_ACK_FLAG);
  end if; 
   if nvl(:old.FK_SAMPLE_TYPE_AVAIL,0) !=NVL(:new.FK_SAMPLE_TYPE_AVAIL,0) then
     audit_trail.column_update (raid, 'FK_SAMPLE_TYPE_AVAIL',:old.FK_SAMPLE_TYPE_AVAIL, :new.FK_SAMPLE_TYPE_AVAIL);
  end if;
   if nvl(:old.FK_ALIQUOTS_TYPE,0) !=NVL(:new.FK_ALIQUOTS_TYPE,0) then
     audit_trail.column_update(raid, 'FK_ALIQUOTS_TYPE',:old.FK_ALIQUOTS_TYPE, :new.FK_ALIQUOTS_TYPE);
  END IF;
END;
/