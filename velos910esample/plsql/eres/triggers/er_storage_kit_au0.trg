CREATE OR REPLACE TRIGGER "ER_STORAGE_KIT_AU0" AFTER UPDATE ON ER_STORAGE_KIT FOR EACH ROW
DECLARE
  raid NUMBER(10);
  new_usr VARCHAR2(100);
  old_usr VARCHAR2(100);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_STORAGE_KIT', :OLD.rid, 'U', usr);

  IF NVL(:OLD.PK_STORAGE_KIT,0) !=
     NVL(:NEW.PK_STORAGE_KIT,0) THEN
     audit_trail.column_update
       (raid, 'PK_STORAGE_KIT',
       :OLD.PK_STORAGE_KIT, :NEW.PK_STORAGE_KIT);
  END IF;

  IF NVL(:OLD.FK_STORAGE,0) !=
     NVL(:NEW.FK_STORAGE,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE',
       :OLD.FK_STORAGE, :NEW.FK_STORAGE);
  END IF;

  IF NVL(:OLD.DEF_SPECIMEN_TYPE,0) !=
     NVL(:NEW.DEF_SPECIMEN_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'DEF_SPECIMEN_TYPE',
       :OLD.DEF_SPECIMEN_TYPE, :NEW.DEF_SPECIMEN_TYPE);
  END IF;

  IF NVL(:OLD.DEF_PROCESSING_TYPE,' ') !=
     NVL(:NEW.DEF_PROCESSING_TYPE, ' ' ) THEN
     audit_trail.column_update
       (raid, 'DEF_PROCESSING_TYPE',
       :OLD.DEF_PROCESSING_TYPE, :NEW.DEF_PROCESSING_TYPE);
  END IF;

  IF NVL(:OLD.DEF_SPEC_PROCESSING_SEQ,' ' ) !=
     NVL(:NEW.DEF_SPEC_PROCESSING_SEQ, ' ' ) THEN
     audit_trail.column_update
       (raid, 'DEF_SPEC_PROCESSING_SEQ',
       :OLD.DEF_SPEC_PROCESSING_SEQ, :NEW.DEF_SPEC_PROCESSING_SEQ);
  END IF;

IF NVL(:OLD.DEF_SPEC_QUANTITY,0) !=
     NVL(:NEW.DEF_SPEC_QUANTITY,0) THEN
     audit_trail.column_update
       (raid, 'DEF_SPEC_QUANTITY',
       :OLD.DEF_SPEC_QUANTITY, :NEW.DEF_SPEC_QUANTITY);
  END IF;

  IF NVL(:OLD.DEF_SPEC_QUANTITY_UNIT,0) !=
     NVL(:NEW.DEF_SPEC_QUANTITY_UNIT,0) THEN
     audit_trail.column_update
       (raid, 'DEF_SPEC_QUANTITY_UNIT',
       :OLD.DEF_SPEC_QUANTITY_UNIT, :NEW.DEF_SPEC_QUANTITY_UNIT);
  END IF;

  IF NVL(:OLD.KIT_SPEC_DISPOSITION,0) !=
     NVL(:NEW.KIT_SPEC_DISPOSITION,0) THEN
     audit_trail.column_update
       (raid, 'KIT_SPEC_DISPOSITION',
       :OLD.KIT_SPEC_DISPOSITION, :NEW.KIT_SPEC_DISPOSITION);
  END IF;

  IF NVL(:OLD.KIT_SPEC_ENVT_CONS, ' ') !=
     NVL(:NEW.KIT_SPEC_ENVT_CONS, ' ') THEN
     audit_trail.column_update
       (raid, 'KIT_SPEC_ENVT_CONS',
       :OLD.KIT_SPEC_ENVT_CONS, :NEW.KIT_SPEC_ENVT_CONS);
  END IF;

IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;



END;
/


