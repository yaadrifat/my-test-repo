CREATE OR REPLACE TRIGGER "ER_STORAGE_STATUS_AD0" AFTER DELETE ON ER_STORAGE_STATUS
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
  usr VARCHAR(2000);

begin

  usr := Getuser(:old.last_modified_by);
  select seq_audit.nextval into raid from dual;

audit_trail.record_transaction
    (raid, 'ER_STORAGE_STATUS', :old.rid, 'D', usr);

deleted_data :=
to_char(:old.PK_STORAGE_STATUS)	|| '|' ||
to_char(:old.FK_STORAGE)	|| '|' ||
to_char(:old.SS_START_DATE) || '|' ||
to_char(:old.FK_CODELST_STORAGE_STATUS)	|| '|' ||
--:old.SS_NOTES || '|' ||--KM--to fix the issue3171
to_char(:old.FK_USER) || '|' ||
:old.SS_TRACKING_NUMBER	|| '|' ||
to_char(:old.FK_STUDY) || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
:old.IP_ADD;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


