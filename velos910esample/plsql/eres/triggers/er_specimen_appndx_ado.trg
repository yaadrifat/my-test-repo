CREATE OR REPLACE TRIGGER "ER_SPECIMEN_APPNDX_ADO" AFTER DELETE ON ER_SPECIMEN_APPNDX
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_SPECIMEN_APPNDX', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_specimen_appndx) || '|' ||
  to_char(:old.fk_specimen) || '|' ||
  :old.sa_desc || '|' ||
  :old.sa_uri || '|' ||
  :old.sa_type || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


