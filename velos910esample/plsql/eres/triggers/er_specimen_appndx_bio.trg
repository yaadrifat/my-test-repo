create or replace TRIGGER "ER_SPECIMEN_APPNDX_BIO" BEFORE INSERT ON ER_SPECIMEN_APPNDX
FOR EACH ROW
 WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  insert_data CLOB;
  usr varchar2(2000);
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  --KM-#3634
  usr := getuser(:new.creator);
  Audit_Trail.record_transaction
    (raid, 'ER_SPECIMEN_APPNDX', erid, 'I', usr );

insert_data:= :NEW.PK_SPECIMEN_APPNDX||'|'||
	      :NEW.FK_SPECIMEN||'|'|| :NEW.SA_DESC||'|'||
              :NEW.SA_URI||'|'||:NEW.SA_TYPE||'|'||
	      :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
	      TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
              TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;
              INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/


