CREATE OR REPLACE TRIGGER ERES.ER_CTRP_DOCUMENTS_AU0
AFTER UPDATE OF PK_CTRP_DOC,FK_CTRP_DRAFT,FK_STUDYAPNDX,FK_CODELST_CTRP_DOCTYPE,
IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE
ON ERES.ER_CTRP_DOCUMENTS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);
	old_modby VARCHAR2(100);
	new_modby VARCHAR2(100);
BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);
	audit_trail.record_transaction
	(raid, 'ER_CTRP_DOCUMENTS', :OLD.rid, 'U', usr);

	IF NVL(:OLD.PK_CTRP_DOC,0) != NVL(:NEW.PK_CTRP_DOC,0) then
		audit_trail.column_update (raid, 'PK_CTRP_DOC',:OLD.PK_CTRP_DOC,:NEW.PK_CTRP_DOC);
	end if;
	IF NVL(:OLD.FK_CTRP_DRAFT,0) != NVL(:NEW.FK_CTRP_DRAFT,0) then
		audit_trail.column_update (raid, 'FK_CTRP_DRAFT',:OLD.FK_CTRP_DRAFT,:NEW.FK_CTRP_DRAFT);
	end if;
	IF NVL(:OLD.FK_STUDYAPNDX,0) != NVL(:NEW.FK_STUDYAPNDX,0) then
		audit_trail.column_update (raid, 'FK_STUDYAPNDX',:OLD.FK_STUDYAPNDX,:NEW.FK_STUDYAPNDX);
	end if;
	IF NVL(:OLD.FK_CODELST_CTRP_DOCTYPE,0) != NVL(:NEW.FK_CODELST_CTRP_DOCTYPE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_CTRP_DOCTYPE',:OLD.FK_CODELST_CTRP_DOCTYPE,:NEW.FK_CODELST_CTRP_DOCTYPE);
	end if;
	IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') then
		audit_trail.column_update (raid, 'IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD);
	end if;

	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) then
		BEGIN
			SELECT TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
			INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				old_modby := NULL;
		END ;
		BEGIN
			SELECT TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
			INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				new_modby := NULL;
		 END ;
		audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
	end if;
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'LAST_MODIFIED_DATE',to_char(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
END;
/