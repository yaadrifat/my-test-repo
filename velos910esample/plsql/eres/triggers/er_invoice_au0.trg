CREATE OR REPLACE TRIGGER ER_INVOICE_AU0 AFTER UPDATE ON ER_INVOICE FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN


SELECT seq_audit.NEXTVAL INTO raid FROM dual;

usr := Getuser(:NEW.last_modified_by);

Audit_Trail.record_transaction   (raid, 'er_invoice', :OLD.rid, 'U', usr);



IF NVL(:OLD.pk_invoice,0) !=
     NVL(:NEW.pk_invoice,0) THEN
     Audit_Trail.column_update
       (raid, 'pk_invoice',
       :OLD.pk_invoice, :NEW.pk_invoice);
END IF;

IF NVL(:OLD.inv_number,' ') !=
     NVL(:NEW.inv_number,' ') THEN
     Audit_Trail.column_update
       (raid, 'inv_number',
       :OLD.inv_number, :NEW.inv_number);
END IF;

IF NVL(:OLD.inv_header,' ') !=
     NVL(:NEW.inv_header,' ') THEN
     Audit_Trail.column_update
       (raid, 'inv_header',
       :OLD.inv_header, :NEW.inv_header);
END IF;

IF NVL(:OLD.inv_footer,' ') !=
     NVL(:NEW.inv_footer,' ') THEN
     Audit_Trail.column_update
       (raid, 'inv_footer',
       :OLD.inv_footer, :NEW.inv_footer);
END IF;

IF NVL(:OLD.inv_milestatustype,' ') !=
     NVL(:NEW.inv_milestatustype,' ') THEN
     Audit_Trail.column_update
       (raid, 'inv_milestatustype',
       :OLD.inv_milestatustype, :NEW.inv_milestatustype);
END IF;

IF NVL(:OLD.inv_intervaltype,' ') !=
     NVL(:NEW.inv_intervaltype,' ') THEN
     Audit_Trail.column_update
       (raid, 'inv_intervaltype',
       :OLD.inv_intervaltype, :NEW.inv_intervaltype);
END IF;

IF NVL(:OLD.inv_notes,' ') !=
     NVL(:NEW.inv_notes,' ') THEN
     Audit_Trail.column_update
       (raid, 'inv_notes',
       :OLD.inv_notes, :NEW.inv_notes);
END IF;

IF NVL(:OLD.inv_other_user,' ') !=
     NVL(:NEW.inv_other_user,' ') THEN
     Audit_Trail.column_update
       (raid, 'inv_other_user',
       :OLD.inv_other_user, :NEW.inv_other_user);
END IF;


IF NVL(:OLD.inv_addressedto,0) !=
     NVL(:NEW.inv_addressedto,0) THEN
     Audit_Trail.column_update
       (raid, 'inv_addressedto',
       :OLD.inv_addressedto, :NEW.inv_addressedto);
END IF;

IF NVL(:OLD.inv_sentFrom,0) !=
     NVL(:NEW.inv_sentFrom,0) THEN
     Audit_Trail.column_update
       (raid, 'inv_sentFrom',
       :OLD.inv_sentFrom, :NEW.inv_sentFrom);
END IF;


IF NVL(:OLD.inv_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.inv_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'inv_date',
       to_char(:OLD.inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.inv_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
END IF;

IF NVL(:OLD.inv_intervalfrom,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.inv_intervalfrom,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'inv_intervalfrom',
       to_char(:OLD.inv_intervalfrom, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.inv_intervalfrom, PKG_DATEUTIL.F_GET_DATEFORMAT));
END IF;

IF NVL(:OLD.inv_intervalto,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.inv_intervalto,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'inv_intervalto',
       to_char(:OLD.inv_intervalto, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.inv_intervalto, PKG_DATEUTIL.F_GET_DATEFORMAT));
END IF;

  --JM: 111406

IF NVL(:OLD.int_acc_num,' ') !=
     NVL(:NEW.int_acc_num,' ') THEN
     Audit_Trail.column_update
       (raid, 'int_acc_num',
       :OLD.int_acc_num, :NEW.int_acc_num);
END IF;


old_modby := '';
new_modby := '';

IF NVL(:OLD.last_modified_by,0) !=    NVL(:NEW.last_modified_by,0) THEN
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL;
  END;

 BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL;
 END;
	     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_BY', old_modby, new_modby);
END IF;

IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update     (raid, 'LAST_MODIFIED_DATE',    to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
END IF;

IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN Audit_Trail.column_update    (raid, 'RID',  :OLD.rid, :NEW.rid);
END IF;



--JM: 21Feb2008: added auditing for the five columns below..

IF NVL(:OLD.inv_payunit,' ') !=
     NVL(:NEW.inv_payunit,' ') THEN
     Audit_Trail.column_update
       (raid, 'INV_PAYUNIT',
       :OLD.inv_payunit, :NEW.inv_payunit);
END IF;


IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
END IF;

IF NVL(:OLD.fk_site,0) !=
     NVL(:NEW.fk_site,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_SITE',
       :OLD.fk_site, :NEW.fk_site);
END IF;

IF NVL(:OLD.inv_payment_dueby,' ') !=
     NVL(:NEW.inv_payment_dueby,' ') THEN
     Audit_Trail.column_update
       (raid, 'INV_PAYMENT_DUEBY',
       :OLD.inv_payment_dueby, :NEW.inv_payment_dueby);
END IF;

IF NVL(:OLD.inv_status,' ') !=
     NVL(:NEW.inv_status,' ') THEN
     Audit_Trail.column_update
       (raid, 'INV_STATUS',
       :OLD.inv_status, :NEW.inv_status);

END IF;

  --KM-Two columns removed for Enh.#FIN12

END;
/


