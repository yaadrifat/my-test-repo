CREATE OR REPLACE TRIGGER "ER_STUDYTEAM_AU_TYPE" 
AFTER UPDATE OF study_team_usr_type
ON ER_STUDYTEAM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.study_team_usr_type <> OLD.study_team_usr_type
      )
DECLARE

  v_new_fk_study NUMBER;
  v_new_fk_user NUMBER;
  v_new_creator NUMBER;
  v_new_ip_add VARCHAR2(15);

BEGIN

 v_new_fk_study := :NEW.fk_study ;
 v_new_fk_user := :NEW.fk_user ;
 v_new_creator := :NEW.creator;
 v_new_ip_add := :NEW.ip_add;

IF :NEW.study_team_usr_type = 'X' THEN
	Pkg_Studystat.sp_revoke_studyteam_access(v_new_fk_study , v_new_fk_user );
END IF;

IF :NEW.study_team_usr_type <> 'X' AND :OLD.study_team_usr_type = 'X'  THEN
	Pkg_Studystat.sp_grant_studyteam_access( v_new_fk_study, v_new_fk_user,v_new_creator,v_new_ip_add);
END IF;

END;
/


