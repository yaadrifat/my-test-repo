CREATE OR REPLACE TRIGGER "EVENT_DEF_AU1" AFTER UPDATE ON EVENT_DEF  REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
(old.event_type='P' or old.event_type='A') and nvl(old.displacement,-1) <> 0
      )
DECLARE
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_DEF_AU1', pLEVEL  => Plog.LFATAL);
v_pk_lineitem number;
v_return number;

BEGIN
     plog.debug(pctx,':old.fk_visit'||:old.fk_visit || ':new.fk_visit' || :new.fk_visit);

    if (:old.event_type='A' and nvl(:old.fk_visit,0) <> nvl(:new.fk_visit,0) ) then

         plog.debug(pctx,':old.fk_visit'||:old.fk_visit || ':new.fk_visit-----add');

        PKG_BGT.sp_addToProtocolBudget( :new.event_id,'L',:new.chain_id, :new.creator , :new.ip_Add , v_return , 0 ,:new.fk_visit,
        :new.name,:new.event_cptcode,:new.event_line_category,:new.description,:new.event_sequence);
     end if;

    begin
        -- get default budget lineitem info for this event

        if (
                :old.event_type='A' and ( (:new.name <> :old.name) or  ( nvl(:new.description,' ') <> nvl(:old.description,' ') ) or
                   ( nvl(:new.event_cptcode,' ') <> nvl(:old.event_cptcode,' ') )  or
                   ( nvl(:new.event_line_category,0) <> nvl(:old.event_line_category,0) )
            or    ( nvl(:new.event_sequence,0) <> nvl(:old.event_sequence,0) )
            )
            )then -- only for events


                --update the lineitem
                update sch_lineitem
                set last_modified_by = :new.last_modified_by ,last_modified_date = sysdate, IP_ADD = :new.ip_add,
                LINEITEM_NAME =  :new.name,LINEITEM_DESC= :new.description, LINEITEM_CPTCODE = :new.event_cptcode,
                --FK_CODELST_CATEGORY = :new.event_line_category, --KM-#4381
                lineitem_seq = :new.event_sequence
                where pk_lineitem in (select pk_lineitem
                                 from sch_lineitem l
                 where l.fk_event = :old.event_id and l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_bgtcal =
                 (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.chain_id and fk_budget = (
                 select  pk_budget from sch_budget where budget_calendar = :old.chain_id)
                 )
             ) ) ;

        end if;
    exception when no_data_found then
        v_pk_lineitem := 0;
    end;


v_return := 1;
END;
/


