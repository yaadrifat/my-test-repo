/********************************************************************/
* Folder: triggers
* Parent Folder: purged
* Schema: esch
*
* Created on: 08/31/2010 
* Purpose: This folder will hold all the purged PL-SQL trigger code for esch schema.
*
* Instructions:
* 1. Only purged trigger code needs to be stored in this folder.
* 2. Purged trigger code should be in original format. File Name should not be changed. (e.g trigger- *.trg)
* 3. The PL-SQL files added to this folder need to be removed from the original PL-SQL folder (i.e. plsql\triggers).
* 5. Do not add any PL-SQL code to this folder that you don't wish to purge.
* 6. Purged code can be used in future for reference.
/********************************************************************/