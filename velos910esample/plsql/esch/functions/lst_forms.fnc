CREATE OR REPLACE FUNCTION        "LST_FORMS" (event NUMBER ) RETURN VARCHAR IS
CURSOR cur_form
IS
/*Function to get a list of associated crfs with an event
*Author: Sonika Talwar, on March 18, 04
* Modified by Sonia Abrol, 08/22/06
*/
 SELECT CRFLIB_NAME
   FROM sch_crflib
  WHERE fk_events = event;
form_lst VARCHAR2(4000) ;
BEGIN
 FOR l_rec IN cur_form LOOP
  form_lst :=   l_rec.crflib_name || ', '|| form_lst ;
 END LOOP ;

 -- get CRF forms from sch_event_crf

 FOR k IN (SELECT DECODE(fk_form,0,other_links,(SELECT form_name FROM er_formlib WHERE pk_formlib = fk_form)) AS formname
 	   	  FROM sch_event_crf WHERE fk_event = event ORDER BY fk_form
 )
 LOOP
 	    form_lst :=  k.formname || ', '|| form_lst ;
 END LOOP;


 form_lst := SUBSTR(trim(form_lst),1,LENGTH(trim(form_lst))-1) ;
 DBMS_OUTPUT.PUT_LINE (SUBSTR(form_lst,1,LENGTH(form_lst)-1) ) ;
RETURN form_lst ;
END   ;
/


CREATE SYNONYM ERES.LST_FORMS FOR LST_FORMS;


CREATE SYNONYM EPAT.LST_FORMS FOR LST_FORMS;


GRANT EXECUTE, DEBUG ON LST_FORMS TO EPAT;

GRANT EXECUTE, DEBUG ON LST_FORMS TO ERES;

