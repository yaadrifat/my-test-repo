CREATE OR REPLACE FUNCTION  "LST_ADDITIONALCODE" (event number ,p_additional_code_type varchar2 ) return varchar is
cursor addl_code
is
select trim('[ ' || (select codelst_desc from er_codelst  where pk_codelst = md_modelementpk) || ' - ' || md_modelementdata || ' ]') event_addlcode
from  er_moredetails
where md_modname = p_additional_code_type  and fk_modpk = event;

addl_lst varchar2(4000);
begin
 for l_rec in addl_code loop
	addl_lst := l_rec.event_addlcode || ', '|| addl_lst ;
 end loop;
  addl_lst := substr(trim(addl_lst),1,length(trim(addl_lst))-1) ;
 
 --dbms_output.put_line (substr(addl_lst,1,length(addl_lst)-1) ) ;
 
return addl_lst ;
end ;
/


CREATE or replace SYNONYM ERES.LST_ADDITIONALCODE FOR LST_ADDITIONALCODE;


GRANT EXECUTE, DEBUG ON LST_ADDITIONALCODE TO ERES;

