CREATE OR REPLACE FUNCTION        "LST_COST" (event number ) return varchar is
cursor cur_cost
is
select trim('[ ' || b.codelst_desc || ' - ' || trim(to_char(EVENTCOST_VALUE,9999999999.99)) || ' ' || a.codelst_desc || ' ]') event_cost,
    b.codelst_desc dddd, a.codelst_desc cccc, EVENTCOST_VALUE aaaa
  from sch_eventcost , sch_codelst a, sch_codelst b
 where sch_eventcost.fk_event = event
   and FK_CURRENCY = a.pk_codelst
   and FK_COST_DESC = b.pk_codelst
   order by dddd desc, cccc desc, aaaa desc;

cost_lst varchar2(4000) ;
begin
 for l_rec in cur_cost loop
  cost_lst :=   l_rec.event_cost || ', '|| cost_lst ;
 end loop ;
 cost_lst := substr(trim(cost_lst),1,length(trim(cost_lst))-1) ;
 dbms_output.put_line (substr(cost_lst,1,length(cost_lst)-1) ) ;
return cost_lst ;
end ;
/


CREATE SYNONYM ERES.LST_COST FOR LST_COST;


CREATE SYNONYM EPAT.LST_COST FOR LST_COST;


GRANT EXECUTE, DEBUG ON LST_COST TO EPAT;

GRANT EXECUTE, DEBUG ON LST_COST TO ERES;

