CREATE OR REPLACE TRIGGER "SCH_BGTUSERS_BI0"
BEFORE INSERT
ON SCH_BGTUSERS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  insert_data CLOB;
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_BGTUSERS', erid, 'I', :NEW.LAST_MODIFIED_BY );

   --   Added by Ganapathy on 06/23/05 for Audit insert
   insert_data:= :NEW.PK_BGTUSERS||'|'|| :NEW.FK_BUDGET||'|'|| :NEW.FK_USER||'|'||
         :NEW.BGTUSERS_RIGHTS||'|'||:NEW.BGTUSERS_TYPE||'|'|| :NEW.RID||'|'||
         :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
      TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
      TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;

     INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


