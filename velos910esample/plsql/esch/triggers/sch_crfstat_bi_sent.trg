CREATE OR REPLACE TRIGGER "SCH_CRFSTAT_BI_SENT" 
BEFORE INSERT
ON SCH_CRFSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.CRFSTAT_SENTFLAG = 1
      )
begin

--if :new.CRFSTAT_SENTFLAG = 1 and trunc(:new.CRFSTAT_SENTON) = trunc(sysdate) then
-- if the max date from the sent to users is eq or less than the senton date send it immediately.

if   :new.CRFSTAT_SENTFLAG = 1
 and trunc(:new.CRFSTAT_SENTON) <= trunc(pkg_tz.f_getmaxtz(:new.CRFSTAT_SENTTO)) then
PKG_DAILY.SP_CRFTRACKING (:new.PK_CRFSTAT ,
                          :new.FK_CRF   ,
                          :new.FK_CODELST_CRFSTAT  ,
                          :new.CRFSTAT_SENTTO ,
                          :new.CRFSTAT_SENTBY ,
                          :new.CRFSTAT_SENTON) ;

  :new.CRFSTAT_SENTFLAG := 2 ;

end if;

end;
/


