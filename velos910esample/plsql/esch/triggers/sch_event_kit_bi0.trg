CREATE OR REPLACE TRIGGER SCH_EVENT_KIT_BI0 BEFORE INSERT ON SCH_EVENT_KIT
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
erid NUMBER(10);
usr VARCHAR(2000);
raid NUMBER(10);
insert_data CLOB;

BEGIN

 BEGIN
  usr := getuser(:NEW.creator);
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;

SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'SCH_EVENT_KIT',erid, 'I',usr);

insert_data:=:NEW.PK_EVENTKIT ||'|'|| :NEW.FK_EVENT ||'|'|| :NEW.FK_storage
||'|'|| :NEW.PROPAGATE_FROM
||'|'|| :NEW.RID ||'|'|| :NEW.CREATOR
||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
||'|'|| :NEW.LAST_MODIFIED_BY
||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)
||'|'|| :NEW.IP_ADD;

INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);

END;
/


