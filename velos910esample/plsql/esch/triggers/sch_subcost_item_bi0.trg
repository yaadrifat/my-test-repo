  create or replace TRIGGER "ESCH"."SCH_SUBCOST_ITEM_BI0"
  BEFORE INSERT
  ON SCH_SUBCOST_ITEM
  REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
  DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(2000);
  insert_data CLOB;
--Created by Manimaran for Audit Insert
BEGIN

 BEGIN
   usr := getuser(:NEW.creator);
   EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction  (raid, 'SCH_SUBCOST_ITEM', erid, 'I', usr );
  insert_data:= :NEW.PK_SUBCOST_ITEM||'|'|| :NEW.FK_CALENDAR||'|'|| :NEW.SUBCOST_ITEM_NAME||'|'||
      :NEW.SUBCOST_ITEM_COST||'|'||:NEW.SUBCOST_ITEM_UNIT||'|'||:NEW.SUBCOST_ITEM_SEQ||'|'||:NEW.FK_CODELST_CATEGORY||'|'||
	  :NEW.FK_CODELST_COST_TYPE||'|'||:NEW.RID||'|'|| :NEW.CREATOR||'|'||
    :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;
   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/