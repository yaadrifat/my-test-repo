CREATE OR REPLACE TRIGGER "SCH_CRFSTAT_AD0" 
AFTER DELETE
ON SCH_CRFSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_CRFSTAT', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_crfstat) || '|' ||
   to_char(:old.fk_crf) || '|' ||
   to_char(:old.fk_codelst_crfstat) || '|' ||
   to_char(:old.crfstat_enterby) || '|' ||
   to_char(:old.crfstat_reviewby) || '|' ||
   to_char(:old.crfstat_reviewon) || '|' ||
   :old.crfstat_sentto || '|' ||
   to_char(:old.crfstat_sentflag) || '|' ||
   to_char(:old.crfstat_sentby) || '|' ||
   to_char(:old.crfstat_senton) || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


