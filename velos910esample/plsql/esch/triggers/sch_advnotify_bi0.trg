CREATE OR REPLACE TRIGGER "SCH_ADVNOTIFY_BI0"
  BEFORE INSERT
  ON SCH_ADVNOTIFY   FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;

BEGIN
 BEGIN
   v_new_creator := :NEW.creator;
   SELECT TO_CHAR(pk_user) ||', ' || trim(usr_lastname) ||', ' ||trim(usr_firstname)
   INTO usr FROM er_user WHERE pk_user = v_new_creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;


  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid

  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction  (raid, 'SCH_ADVNOTIFY', erid, 'I', usr );
  --   Added by Ganapathy on 06/23/05 for Audit insert
  insert_data:= :NEW.PK_ADVNOTIFY||'|'|| :NEW.FK_ADVERSEVE||'|'|| :NEW.FK_CODELST_NOT_TYPE||'|'||
   TO_CHAR(:NEW.ADVNOTIFY_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.ADVNOTIFY_NOTES||'|'|| :NEW.RID||'|'||
    :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
 TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'|| :NEW.ADVNOTIFY_VALUE;
  INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


