CREATE OR REPLACE TRIGGER "ESCH"."SCH_PORTAL_FORMS_AD0" 
AFTER DELETE
ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_PORTAL_FORMS', :old.rid, 'D');

  deleted_data :=
   :old.PK_PF || '|' ||
   :old.FK_PORTAL || '|' ||
   :old.FK_CALENDAR || '|' ||
   :old.FK_EVENT || '|' ||
   :old.FK_FORM || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.created_on) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


 