CREATE OR REPLACE TRIGGER "SCH_ADVERSEVE_AD0" AFTER DELETE ON SCH_ADVERSEVE
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVERSEVE', :old.rid, 'D');

  deleted_data :=
  to_char(:old.pk_adveve) || '|' ||
  :old.fk_events1 || '|' ||
  to_char(:old.fk_codlst_aetype) || '|' ||
  :old.ae_desc || '|' ||
  to_char(:old.ae_stdate) || '|' ||
  to_char(:old.ae_enddate) || '|' ||
  to_char(:old.ae_enterby) || '|' ||
  to_char(:old.ae_reportedby) || '|' ||
  :old.ae_outtype || '|' ||
  to_char(:old.ae_outdate) || '|' ||
  :old.ae_outnotes || '|' ||
  :old.ae_addinfo || '|' ||
  :old.ae_notes || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.ae_discvrydate) || '|' ||
  to_char(:old.ae_loggeddate) || '|' ||
  to_char(:old.fk_link_adverseve) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  to_char(:old.fk_study) || '|' ||
  to_char(:old.fk_per) || '|' ||
  to_char(:old.ae_severity) || '|' ||
  to_char(:old.ae_bdsystem_aff) || '|' ||
  to_char(:old.ae_relationship) || '|' ||
  to_char(:old.ae_recovery_desc) || '|' ||
  to_char(:old.AE_GRADE) || '|' ||
  to_char(:old.AE_NAME) || '|' ||
  to_char(:old.FORM_STATUS) || '|' ||
  to_char(:old.AE_TREATMENT_COURSE) || '|' ||
  to_char(:old.FK_CODELST_OUTACTION) || '|' ||
  to_char(:old.MEDDRA) || '|' ||
  to_char(:old.DICTIONARY);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/