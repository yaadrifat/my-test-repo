CREATE OR REPLACE TRIGGER PAT_PERAPNDX_AU0
AFTER UPDATE OF RID,FK_PER,IP_ADD,CREATOR,CREATED_ON,PK_PERAPNDX,PERAPNDX_URI,PERAPNDX_DATE,PERAPNDX_DESC,PERAPNDX_TYPE
ON PAT_PERAPNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   usr := getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'PAT_PERAPNDX', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_perapndx,0) !=
     NVL(:NEW.pk_perapndx,0) THEN
     audit_trail.column_update
       (raid, 'PK_PERAPNDX',
       :OLD.pk_perapndx, :NEW.pk_perapndx);
  END IF;
  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;
  IF NVL(:OLD.perapndx_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.perapndx_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_DATE',
       to_char(:OLD.perapndx_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.perapndx_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.perapndx_desc,' ') !=
     NVL(:NEW.perapndx_desc,' ') THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_DESC',
       :OLD.perapndx_desc, :NEW.perapndx_desc);
  END IF;
  IF NVL(:OLD.perapndx_uri,' ') !=
     NVL(:NEW.perapndx_uri,' ') THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_URI',
       :OLD.perapndx_uri, :NEW.perapndx_uri);
  END IF;
  IF NVL(:OLD.perapndx_type,' ') !=
     NVL(:NEW.perapndx_type,' ') THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_TYPE',
       :OLD.perapndx_type, :NEW.perapndx_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
      BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
     END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
     END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
     to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;
/


