CREATE OR REPLACE TRIGGER "PERSON_BU_LM" BEFORE UPDATE ON PERSON
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
begin
:new.last_modified_date := sysdate ;
 end;
/


