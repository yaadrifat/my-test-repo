For Mega Menu Panel please follow the below steps

1. Fire the SQL file first on DB.
2. Take update from CVS
3. Build project.
4. Start your server.
5. Change the theme to default.
  * Accounts --> Personalise account ---> Settings --> Choose Color scheme to "Velos Default"

  
  Datepicker Implementation (INF-20084)


We have started implementation of datepicker across the application. In the initial phase we have implemented this feature  in following Modules:

1. Manage Inventory
2. MileStones

Here is the List of JSP's on which  this feature is implemented.


1.invoice_step1.jsp
2.editstatus.jsp
3.viewInvoice.jsp
4.milepayments.jsp


5.specimenbrowser.jsp
6.editmultiplespecimenstatus.jsp
7.specimenstatus.jsp
8.specimendetails.jsp
9.preparationAreaBrowser.jsp
10.storagestatus.jsp
11.storageunitdetails.jsp
12.editmultiplestoragestatus.jsp
13.labmodify.jsp
14.getFormSpecimenData.jsp
15.labdata.jsp
  