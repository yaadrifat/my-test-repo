--When records are deleted from SCH_BGTUSERS table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_BGTUSERS_AD1" 
AFTER DELETE
ON ESCH.SCH_BGTUSERS 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
  v_rowid number(10);/*variable used to fetch value of sequence */
BEGIN
  SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.sp_row_insert (v_rowid, 'SCH_BGTUSERS', :OLD.RID,:OLD.FK_BUDGET,'D',:NEW.Creator);
	
	--Inserting rows in AUDIT_COLUMN_MODULE table.
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'PK_BGTUSERS',:OLD.PK_BGTUSERS,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'FK_BUDGET',:OLD.FK_BUDGET,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'FK_USER',:OLD.FK_USER,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'BGTUSERS_RIGHTS',:OLD.BGTUSERS_RIGHTS,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'BGTUSERS_TYPE',:OLD.BGTUSERS_TYPE,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'RID',:OLD.RID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'CREATOR',:OLD.CREATOR,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'LAST_MODIFIED_BY',TO_CHAR(:OLD.LAST_MODIFIED_BY),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'CREATED_ON',:OLD.CREATED_ON,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.sp_column_insert (v_rowid,'IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,7,'07_SCH_BGTUSERS_AD1.sql',sysdate,'9.0.0 Build#606');

commit;