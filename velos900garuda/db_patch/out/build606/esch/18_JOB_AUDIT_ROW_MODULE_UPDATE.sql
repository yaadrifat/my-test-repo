--Scheduled Job to execute Procedure SP_AUDIT_ROW_MODULE_UPDATE for updating null Module_IDs / User IDs in audit_row_module table
BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'AUDIT_ROW_MODULE_UPDATE',
   start_date         =>  SYSTIMESTAMP,
   repeat_interval    =>  'FREQ=MINUTELY; INTERVAL=1;',
   job_type           =>  'STORED_PROCEDURE',
   job_action         =>  'ESCH.SP_AUDIT_ROW_MODULE_UPDATE',
   enabled            =>  TRUE,
   comments           =>  'To Update Blank Module IDs in Budget Audit row table');
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,18,'18_JOB_AUDIT_ROW_MODULE_UPDATE.sql',sysdate,'9.0.0 Build#606');

commit;