set define off;

Declare
	lkpID INTEGER DEFAULT 0;
	lkpvwID INTEGER DEFAULT 0;
	lkpColID INTEGER DEFAULT 0;
	lkpvwColID INTEGER DEFAULT 0;
BEGIN

SELECT PK_LKPLIB INTO lkpID
FROM ER_LKPLIB 
WHERE LKPTYPE_NAME = 'dynReports' AND LKPTYPE_DESC = 'Patient Study Status';

SELECT PK_LKPVIEW INTO lkpvwID
FROM ER_LKPVIEW 
WHERE LKPVIEW_NAME = 'Patient Study ID' AND FK_LKPLIB = lkpID;

Update ER_REPFILTERMAP 
SET REPFILTERMAP_COLUMN = '<td><DIV id="patientDIV"><FONT class = "comments"><A href="javascript:void(0);" onClick="return openLookup(document.dashboardpg,''viewId='|| lkpvwID ||'&form=dashboardpg&seperator=,&defaultvalue=&keyword=selpatientId|PATIENT_STUDY_ID~parampatientId|LKP_PK|[VELHIDE]'','''')">Select Patient</A> </FONT> </DIV></td>
	       	<td><DIV id="patientdataDIV"><Input TYPE="text" NAME="selpatientId" SIZE="50" READONLY value="">
	       	<Input TYPE="hidden" NAME="parampatientId" value=""></DIV>
	       	</td>'
WHERE FK_REPFILTER = 10 AND REPFILTERMAP_REPCAT ='form query management'
AND REPFILTERMAP_SEQ = 6;

commit;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,103,2,'02_EDC_AT9.sql',sysdate,'8.10.0 Build#560');

commit;
