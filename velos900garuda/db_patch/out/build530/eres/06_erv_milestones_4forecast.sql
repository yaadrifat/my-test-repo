 CREATE OR REPLACE FORCE VIEW "ERES"."ERV_MILESTONES_4FORECAST" ("PK_MILESTONE", "FK_STUDY", "MILESTONE_TYPE", "FK_CAL", "MILESTONE_AMOUNT", "FK_EVENTASSOC", "MILESTONE_COUNT", "FK_VISIT", "MILESTONE_EVENTSTATUS", "CODELST_SUBTYP") AS 
  SELECT pk_milestone, fk_study, milestone_type, fk_cal,milestone_amount, fk_eventassoc,
milestone_count, fk_visit, milestone_eventstatus,codelst_subtyp
FROM ER_MILESTONE, ER_CODELST
WHERE (milestone_type = 'VM' OR milestone_type = 'EM') AND
fk_codelst_rule = pk_codelst AND fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='A') AND
(milestone_eventstatus IS NULL OR
milestone_eventstatus = (SELECT pk_codelst FROM sch_codelst WHERE
trim(codelst_type) = 'eventstatus' AND trim(codelst_subtyp) = 'ev_done')
) ;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,73,6,'06_erv_milestones_4forecast.sql',sysdate,'8.9.0 Build#530');

commit;