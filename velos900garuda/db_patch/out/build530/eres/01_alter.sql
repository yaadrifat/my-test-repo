alter table er_milestone add fk_codelst_milestone_stat number;
comment on column er_milestone.fk_codelst_milestone_stat is 'FK to er_codelst. Stores the code for Milestone status.';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,73,1,'01_alter.sql',sysdate,'8.9.0 Build#530');

commit;
