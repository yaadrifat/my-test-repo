#Build 609

===========================================================================================================
Garuda :
	We have released two NMDP Specific Large data scripts with this build, and 
they are kept on Velos FTP server(66.237.42.81) under following path://Garuda/12282011/NMDPPatch/..... 
There is another ReadMe given along with these files with instructions to execute. 
These files will be required by BT-ESB Team, QA and Build going to NMDP. 

There are two files ("Additional Configurations for CDR.doc" and "Garuda_Checklist_And_Maintenance.xls")
for cumulative Garuda configurations. The new item is #4 "eResearch Forms Integration" in the Word document.


===========================================================================================================
eResearch Localization:

Copy these properties files (messageBundle.properties and labelBundle.properties) 
in ereshome\ and paste them to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

Following Files have been Modified:

1.	labelBundle.properties
2.	LC.java
3.	LC.jsp
4.	MC.java
5.	MC.jsp
6.	messageBundle.properties

Following JSP Files have been Modified for String Concatenation for localization.

1.  accountbrowser.jsp
2.  accountcreation.jsp
3.  accountdelete.jsp
4.  accountforms.jsp
5.  budgetrights.jsp
6.  budgetbrowserpg.jsp
7.  budget.jsp
8.  aboutus.jsp
===========================================================================================================
