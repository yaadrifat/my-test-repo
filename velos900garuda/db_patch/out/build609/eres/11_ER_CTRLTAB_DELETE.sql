set define off;

delete from ER_CTRLTAB where CTRL_VALUE='CB_NOTES' and CTRL_KEY='app_rights';
delete from ER_CTRLTAB where CTRL_VALUE='G' and CTRL_KEY='acc_type_rights' and CTRL_DESC='CB_NOTES';

delete from ER_CTRLTAB where CTRL_VALUE='CB_MANGCBB' and CTRL_KEY='app_rights';
delete from ER_CTRLTAB where CTRL_VALUE='G' and CTRL_KEY='acc_type_rights' and CTRL_DESC='CB_MANGCBB';

delete from ER_CTRLTAB where CTRL_VALUE='CB_CONFELIGSTAT' and CTRL_KEY='app_rights';
delete from ER_CTRLTAB where CTRL_VALUE='G' and CTRL_KEY='acc_type_rights' and CTRL_DESC='CB_CONFELIGSTAT';


delete from ER_CTRLTAB where CTRL_VALUE='CB_CONFLICSTAT' and CTRL_KEY='app_rights';
delete from ER_CTRLTAB where CTRL_VALUE='G' and CTRL_KEY='acc_type_rights' and CTRL_DESC='CB_CONFLICSTAT';


delete from ER_CTRLTAB where CTRL_VALUE='CB_FCBUREV' and CTRL_KEY='app_rights';
delete from ER_CTRLTAB where CTRL_VALUE='G' and CTRL_KEY='acc_type_rights' and CTRL_DESC='CB_FCBUREV';


delete from ER_CTRLTAB where CTRL_VALUE='CB_CORDPP' and CTRL_KEY='app_rights';
delete from ER_CTRLTAB where CTRL_VALUE='G' and CTRL_KEY='acc_type_rights' and CTRL_DESC='CB_CORDPP';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,152,11,'11_ER_CTRLTAB_DELETE.sql',sysdate,'9.0.0 Build#609');

commit;
