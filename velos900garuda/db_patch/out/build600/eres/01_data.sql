set define off;

delete from eres.er_repxsl where fk_report = 99;

commit;

--Update skin name.
Update er_codelst set CODELST_DESC = '510K Compliant' where CODELST_TYPE='skin' 
and CODELST_SUBTYP = 'vel_bethematch';

COMMIT;

--Hide existing skins.
Update er_codelst set CODELST_HIDE = 'Y' where CODELST_TYPE='skin' and CODELST_SUBTYP IN
('vel_salmon','vel_green','vel_purple','vel_violet','vel_skyblue','vel_teal', 'vel_violet','vel_defaultCSS3');

COMMIT;

DECLARE
skinId NUMBER; 
BEGIN
	SELECT PK_CODELST INTO skinId
	FROM ER_CODELST 
	WHERE CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_default';
	
	UPDATE ER_USER SET FK_CODELST_SKIN = skinId
	WHERE FK_CODELST_SKIN IN (SELECT PK_CODELST
	FROM ER_CODELST 
	WHERE CODELST_TYPE='skin' and CODELST_SUBTYP IN
	('vel_salmon','vel_green','vel_purple','vel_violet','vel_skyblue','vel_teal', 'vel_violet','vel_defaultCSS3')
	);
	
	COMMIT;
END; 
/
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,143,1,'01_data.sql',sysdate,'9.0.0 Build#600');

commit;