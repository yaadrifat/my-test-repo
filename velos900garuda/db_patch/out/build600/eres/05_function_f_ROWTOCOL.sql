Set Define Off;
create or replace
FUNCTION "f_ROWTOCOL" ( p_slct IN VARCHAR2,

     p_dlmtr IN VARCHAR2 DEFAULT ',' ) RETURN VARCHAR2
     AUTHID CURRENT_USER AS
     TYPE c_refcur IS REF CURSOR;
     lc_str VARCHAR2(4000);
     lc_colval VARCHAR2(4000);
     c_dummy c_refcur;
     l number;
     BEGIN
     OPEN c_dummy FOR p_slct;
     LOOP
     FETCH c_dummy INTO lc_colval;
     EXIT WHEN c_dummy%NOTFOUND;
     lc_str := lc_str || p_dlmtr || lc_colval;
     END LOOP;
     CLOSE c_dummy;
     RETURN SUBSTR(lc_str,2);
     EXCEPTION
     WHEN OTHERS THEN
     lc_str := SQLERRM;
     IF c_dummy%ISOPEN THEN
     CLOSE c_dummy;
     END IF;
     RETURN lc_str;
     END;
 
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,143,5,'05_function_f_ROWTOCOL.sql',sysdate,'9.0.0 Build#600');

commit;