Set Define Off;

UPDATE ERES.CB_SHIPMENT SET FK_SHIP_COMPANY_ID=(SELECT PK_CODELST FROM ERES.ER_CODELST WHERE CODELST_TYPE='shipper' AND CODELST_SUBTYP='fedex');

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,143,7,'07_CB_SHIPMENT_UPDATE.sql',sysdate,'9.0.0 Build#600');

commit;