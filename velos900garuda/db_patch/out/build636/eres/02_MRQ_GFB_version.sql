--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE --

--MRQ GFB FORM--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_cb_prev_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_cb_prev_ind'),null,null,null,'1',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind'),null,null,null,'2',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'2.a',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),null,null,null,'3',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='propecia_last_month_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='propecia_last_month_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'3.a',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='accutane_last_month_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='accutane_last_month_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'3.b',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='soriatane_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='soriatane_12m_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'3.c',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.d--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='Hep_B_imm_glob_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='Hep_B_imm_glob_12m_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'3.d',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.e--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bovine_insulin_since_1980_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bovine_insulin_since_1980_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'3.e',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.f--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ever_tkn_tegison_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ever_tkn_tegison_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'3.f',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.g--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_hmn_pit_gh_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_hmn_pit_gh_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'3.g',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_ind'),null,null,null,'4',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'4.a',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_smallpox_vaccine_8wk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_smallpox_vaccine_8wk_ind'),null,null,null,'5',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4wk_illness_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4wk_illness_symptom_ind'),null,null,null,'6',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4wk_illness_symptom_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4wk_illness_symptom_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'6.a',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind'),null,null,null,'7',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'7.a',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cncr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cncr_ind'),null,null,null,'8',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_disease_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_disease_ind'),null,null,null,'9',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_prblm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_prblm_ind'),null,null,null,'10',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='west_nile_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='west_nile_preg_ind'),null,null,null,'11',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hep_pos_tst_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hep_pos_tst_ind'),null,null,null,'12',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind'),null,null,null,'13',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind'),null,null,null,'14',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dura_mater_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dura_mater_ind'),null,null,null,'15',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_xeno_tx_med_procedure_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_xeno_tx_med_procedure_ind'),null,null,null,'16',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='liv_cntc_xeno_tx_med_proc_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='liv_cntc_xeno_tx_med_proc_ind'),null,null,null,'17',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='malaria_antimalr_drug_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='malaria_antimalr_drug_3yr_ind'),null,null,null,'18',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind'),null,null,null,'19',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_canada_us_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_canada_us_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''mrq_res1'' and CODELST_SUBTYP in(''yes'',''momdntans'', ''bankdntask'')',','),'19.a',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='blood_transf_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='blood_transf_12m_ind'),null,null,null,'20',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_12m_ind'),null,null,null,'21',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tissue_graft_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tissue_graft_12m_ind'),null,null,null,'22',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_12m_ind'),null,null,null,'23',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_shared_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_shared_12m_ind'),null,null,null,'24',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='piercing_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='piercing_12m_ind'),null,null,null,'25',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 26 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS 
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acc_ns_stk_cntc_bld_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acc_ns_stk_cntc_bld_12m_ind'),null,null,null,'26',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 27 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_gono_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_gono_12m_ind'),null,null,null,'27',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 28 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_mn_dr_pmt_for_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_mn_dr_pmt_for_sex_12m_ind'),null,null,null,'28',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 29 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_tk_mn_dr_pmt_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_tk_mn_dr_pmt_sex_12m_ind'),null,null,null,'29',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 30 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_cntc_liv_jaund_hep_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_cntc_liv_jaund_hep_12m_ind'),null,null,null,'30',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 31 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_dr_in_pst_5yr_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_dr_in_pst_5yr_12m_ind'),null,null,null,'31',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 32 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_man_oth_pst_5yr_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_man_oth_pst_5yr_12m_ind'),null,null,null,'32',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 33 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind'),null,null,null,'33',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 34 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_hiv_aids_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_hiv_aids_12m_ind'),null,null,null,'34',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 35 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='prison_jail_contin_72hrs_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='prison_jail_contin_72hrs_ind'),null,null,null,'35',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_tk_mn_dr_pmt_sex_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_tk_mn_dr_pmt_sex_ind'),null,null,null,'36',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 37 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_iv_drug_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_iv_drug_ind'),null,null,null,'37',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 38 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aids_hiv_screen_test_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aids_hiv_screen_test_ind'),null,null,null,'38',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 39 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='htlv_incl_screen_test_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='htlv_incl_screen_test_ind'),null,null,null,'39',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 40 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind'),null,null,null,'40',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 41 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind'),null,null,null,'41',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 42 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind'),null,null,null,'42',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 43 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind'),null,null,null,'43',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 44 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind'),null,null,null,'44',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 45 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind'),null,null,null,'45',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 46 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind'),null,null,null,'46',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 47 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_2_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_2_ge_6m_ind'),null,null,null,'47',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 48 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind'),null,null,null,'48',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 49 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='trav_ctry_rec_bld_or_med_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='trav_ctry_rec_bld_or_med_ind'),null,null,null,'49',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 50 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_born_live_in_ctry_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_born_live_in_ctry_ind'),null,null,null,'50',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 51 --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='addendum_questions_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='addendum_questions_ind'),null,null,null,'51',null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--END--

--INSERTING DATA INTO CB_QUESTION_GROUP--

--MRQ GFB FORM--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_cb_prev_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='donate_cb_prev_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='refused_bld_dnr_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='refused_bld_dnr_ind_desc'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='tkn_medications_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='propecia_last_month_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='propecia_last_month_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='accutane_last_month_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='accutane_last_month_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='soriatane_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='soriatane_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.d--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='Hep_B_imm_glob_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='Hep_B_imm_glob_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.e--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bovine_insulin_since_1980_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='bovine_insulin_since_1980_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.f--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ever_tkn_tegison_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='ever_tkn_tegison_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.g--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_hmn_pit_gh_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='evr_tkn_hmn_pit_gh_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='shots_vacc_12wk_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_12wk_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='shots_vacc_12wk_desc'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_smallpox_vaccine_8wk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cntc_smallpox_vaccine_8wk_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4wk_illness_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='pst_4wk_illness_symptom_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='symptom_desc'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='major_ill_srgry_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='major_ill_desc'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cncr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cncr_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_disease_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='bld_disease_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_prblm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='bld_prblm_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='west_nile_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='west_nile_preg_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hep_pos_tst_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='jaund_liver_hep_pos_tst_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_dises_babesiosis_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cjd_diagnosis_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dura_mater_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='dura_mater_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_xeno_tx_med_procedure_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='recv_xeno_tx_med_procedure_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='liv_cntc_xeno_tx_med_proc_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='liv_cntc_xeno_tx_med_proc_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='malaria_antimalr_drug_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='malaria_antimalr_drug_3yr_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='outside_us_or_canada_3yr_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_canada_us_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='A. Illness and Medications'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='outside_canada_us_desc'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='blood_transf_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='blood_transf_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='tx_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tissue_graft_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='tissue_graft_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='tattoo_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_shared_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='tattoo_shared_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='piercing_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='piercing_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acc_ns_stk_cntc_bld_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='acc_ns_stk_cntc_bld_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_gono_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='had_treat_syph_gono_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 28--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_mn_dr_pmt_for_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='give_mn_dr_pmt_for_sex_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 29--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_tk_mn_dr_pmt_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_tk_mn_dr_pmt_sex_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 30--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_cntc_liv_jaund_hep_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_cntc_liv_jaund_hep_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 31--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_dr_in_pst_5yr_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_iv_dr_in_pst_5yr_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 32--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_man_oth_pst_5yr_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_man_oth_pst_5yr_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 33--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_clotting_factor_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 34--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_hiv_aids_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_hiv_aids_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 35--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='prison_jail_contin_72hrs_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='prison_jail_contin_72hrs_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_tk_mn_dr_pmt_sex_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='pst_5yr_tk_mn_dr_pmt_sex_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 37--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_iv_drug_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='pst_5yr_iv_drug_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 38--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aids_hiv_screen_test_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='aids_hiv_screen_test_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='htlv_incl_screen_test_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='htlv_incl_screen_test_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 40--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='B. Behavior and Exposure Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_contag_understand_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 41--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='C. CJD Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='live_travel_europe_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='C. CJD Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spent_uk_ge_3m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='C. CJD Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='recv_transfusion_uk_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 44--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='C. CJD Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spent_europe_ge_5y_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 45--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='C. CJD Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='us_military_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 46--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='C. CJD Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='mil_base_europe_1_ge_6m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 47--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_2_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='C. CJD Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='mil_base_europe_2_ge_6m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 48--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='D. HIV-1 Group O Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='born_live_cam_afr_nig_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 49--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='trav_ctry_rec_bld_or_med_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='D. HIV-1 Group O Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='trav_ctry_rec_bld_or_med_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 50--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_born_live_in_ctry_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='D. HIV-1 Group O Risk'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_born_live_in_ctry_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 51--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='addendum_questions_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFB');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='E. Addendum'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='addendum_questions_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFB'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--END--



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,2,'02_MRQ_GFB_version.sql',sysdate,'9.0.0 Build#636');

commit;