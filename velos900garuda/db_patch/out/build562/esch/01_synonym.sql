create or REPLACE synonym eres.SCH_PORTAL_FORMS for esch.SCH_PORTAL_FORMS;


grant all on SCH_PORTAL_FORMS to eres;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,105,1,'01_synonym.sql',sysdate,'8.10.0 Build#562');

commit;