DROP INDEX ERES.IDX_ER_STORAGE_ACC_TYPE;

CREATE INDEX ERES.IDX_ER_STORAGE_ACC_TYPE ON ERES.ER_STORAGE
(FK_ACCOUNT, FK_STORAGE)
LOGGING
NOPARALLEL;


DROP INDEX ERES.IDX_ER_STORAGE_FKACC;

CREATE INDEX ERES.IDX_ER_STORAGE_FKACC ON ERES.ER_STORAGE
(FK_ACCOUNT)
LOGGING
NOPARALLEL;


DROP INDEX ERES.IDX_ER_STORAGE_FKSTOR;

CREATE INDEX ERES.IDX_ER_STORAGE_FKSTOR ON ERES.ER_STORAGE
(FK_STORAGE)
LOGGING
NOPARALLEL;


DROP INDEX ERES.IDX_ER_STORAGE_FK_TYPE;

CREATE INDEX ERES.IDX_ER_STORAGE_FK_TYPE ON ERES.ER_STORAGE
(FK_CODELST_STORAGE_TYPE)
LOGGING
NOPARALLEL;



DROP INDEX ERES.IDX_ER_STORAGESTAT_FKSTOR;

CREATE INDEX ERES.IDX_ER_STORAGESTAT_FKSTOR ON ERES.ER_STORAGE_STATUS
(FK_STORAGE)
LOGGING
NOPARALLEL;


drop INDEX ERES.IDX_SPEC_FK_STORAGE ;

CREATE INDEX ERES.IDX_SPEC_FK_STORAGE ON ERES.ER_SPECIMEN
(FK_STORAGE)
LOGGING
NOPARALLEL;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,77,8,'08_indices.sql',sysdate,'8.9.0 Build#534');

commit;