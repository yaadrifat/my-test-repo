declare 
	v_code_count number;
begin

	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'subm_status' and codelst_subtyp = 'pi_respond';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ)
			 Values
			   (seq_er_codelst.nextval, NULL, 'subm_status', 'pi_respond', 'PI Responded', 
			    'N', 9);
		
		COMMIT;

	end if;
	
	--------- Withdrawn
	
	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'subm_status' and codelst_subtyp = 'withdrawn';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ,codelst_custom_col1)
			 Values
			   (seq_er_codelst.nextval, NULL, 'subm_status', 'withdrawn', 'Withdrawn', 
			    'N', 11,'overall,final');
		
		COMMIT;

	end if;
	
	
	--------- Pending
	
	/* select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'subm_status' and codelst_subtyp = 'pending';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ,codelst_custom_col1)
			 Values
			   (seq_er_codelst.nextval, NULL, 'subm_status', 'pending', 'Pending', 
			    'N', 12,'overall');
		
		COMMIT;

	end if;
	
	*/
	
	--------- Rejected
	
	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'subm_status' and codelst_subtyp = 'rejected';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ,codelst_custom_col1)
			 Values
			   (seq_er_codelst.nextval, NULL, 'subm_status', 'rejected', 'Rejected', 
			    'N', 13,'overall,final');
		
		COMMIT;

	end if;
	
	-- for existing records
	update er_codelst
	set codelst_custom_col1 = 'overall,final'
	where codelst_type = 'subm_status' and codelst_subtyp = 'rejected';
	
	update er_codelst
	set codelst_custom_col1 = ''
	where codelst_type = 'subm_status' and codelst_subtyp = 'submitted';
	
	
	update er_codelst
	set codelst_custom_col = 'app_CHR'
	where codelst_type = 'subm_status' and codelst_subtyp = 'approved';
	
	update er_codelst
	set codelst_custom_col = 'rej_CHR'
	where codelst_type = 'subm_status' and codelst_subtyp = 'rejected';
	
	
	update er_codelst
	set codelst_custom_col = 'not_active_with'
	where codelst_type = 'subm_status' and codelst_subtyp = 'withdrawn';
	

	update er_browserconf
	set browserconf_settings = '{"key":"LETTER_LINK","label":"Outcome Letter","format":"letterLink", "sortable":true, "resizeable":true,"hideable":true}'
	where browserconf_colname = 'LETTER_LINK';
	
	commit;


  		select count(*) into v_code_count 
  		from ER_OBJECT_SETTINGS where OBJECT_NAME = 'irb_subm_tab' and OBJECT_SUBTYPE = 'irb_appr_tab';
   
   if (v_code_count = 0) then
			update er_object_settings 
			set OBJECT_SEQUENCE = 8 where  OBJECT_SEQUENCE = 7 and OBJECT_NAME = 'irb_subm_tab';


			Insert into ER_OBJECT_SETTINGS
			   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
			    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
			 Values
			   (seq_ER_OBJECT_SETTINGS.nextval, 'T', 'irb_appr_tab', 'irb_subm_tab', 7, 
			    1, 'Final Outcome', 0);

			COMMIT;

   end if;		
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,77,2,'02_data.sql',sysdate,'8.9.0 Build#534');

commit;