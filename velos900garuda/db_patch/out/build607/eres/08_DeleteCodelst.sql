set define off;

Delete from er_codelst where codelst_type='all_ids';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,150,8,'08_DeleteCodelst.sql',sysdate,'9.0.0 Build#607');

commit;
