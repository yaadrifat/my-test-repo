==================================================================================
Garuda :

The file "dcmsAttachment.xml" in
[eresearch_dir]\server\eresearch\deploy\velos.ear\velos.war\jsp\xml
is a configuration file and will not be included in any build.

==================================================================================
eResearch:

For CTRP, four addition sections have been added to the Complete Data Capture screen
for non-industrial CTRP drafts. The mandatory field validation behavior has not been
completed and will change in a later build.

==================================================================================
eResearch Localization:

Copy these files (messageBundle.properties and labelBundle.properties) 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

All the reports XSLs have been localized.

==================================================================================
