<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <fo:layout-master-set>
    <fo:simple-page-master master-name="eSample" page-width="3in" page-height="2in"
        margin-top="0.1in" margin-bottom="0.05in" margin-left="0.1in" margin-right="0.1in">
      <fo:region-body/>
    </fo:simple-page-master>
  </fo:layout-master-set>
  <fo:page-sequence master-reference="eSample" initial-page-number="1">
    <fo:flow flow-name="xsl-region-body">
      <fo:block text-align="center">Storage ID: {VEL_STORAGE_ID}</fo:block>
      <fo:block font-size="12pt"
                font-family="sans-serif"
                line-height="15pt"
                space-after.optimum="3pt"
                text-align="center">
              {VEL_STORAGE_TYPE}
      </fo:block>
      <fo:block font-size="12pt"
                font-family="sans-serif"
                line-height="15pt"
                space-after.optimum="0pt"
                text-align="center">
        <fo:external-graphic src="file:{VEL_PDF417_BARCODE}"
            width="116px" height="66px"/>
      </fo:block>
    </fo:flow>
  </fo:page-sequence>
</fo:root>