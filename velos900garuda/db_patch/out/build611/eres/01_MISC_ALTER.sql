set define off;

--STARTS ADDING COLUMN ORDER_STATUS TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'ORDER_STATUS';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER ADD(ORDER_STATUS NUMBER(10,0))';
  end if;
end;
/
--END--


COMMENT ON COLUMN ER_ORDER.ORDER_STATUS IS 'Stores service status';

set define off;
--STARTS ADDING COLUMN FK_SITE_ID TO ER_ORDER_HEADER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER_HEADER'
    AND column_name = 'FK_SITE_ID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER_HEADER ADD(FK_SITE_ID NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ER_ORDER_HEADER.FK_SITE_ID IS 'Stores foreign key reference to ER_SITE table';

--STARTS MODIFYING COLUMN EA_KEYCONDITION FROM WFACTIVITY_CONDITION TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'WFACTIVITY_CONDITION'
    AND column_name = 'EA_KEYCONDITION';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE ERES.WFACTIVITY_CONDITION MODIFY(EA_KEYCONDITION VARCHAR2(200 BYTE))';
  end if;
end;
/
--END--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'NOTE_VISIBLE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_NOTES add NOTE_VISIBLE VARCHAR2(25)');
  end if;
end;
/
COMMENT ON COLUMN CB_NOTES.NOTE_VISIBLE IS 'Refernce from ER_CODELST to store visibility of note'; 
commit;

--STARTS ADDING COLUMN ORDER_TYPE to ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'ORDER_TYPE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER ADD(ORDER_TYPE NUMBER(10,0))';
  end if;
end;
/
--END--

--Sql for Table CB_ENTITY_SAMPLES to add column No. of Segments Available
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_REP_ALT_CON'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_REP_ALT_CON number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_REP_ALT_CON IS 'This Column will store the CBU Number of other representative aliquots stored under alternate conditions.'; 
commit;

--Sql for Table CB_ENTITY_SAMPLES to add column No. of Segments Available
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'SI_NO_MISC_MAT'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add SI_NO_MISC_MAT number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.SI_NO_MISC_MAT IS 'This Column will store the Sample Inventory Number of Miscellaneous Maternal Samples.'; 
commit;

 --Sql for Table CB_ENTITY_SAMPLES to add column CBU SAMPLES FILTER PAPER AVAILABLE ; 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_FILT_PAP'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_FILT_PAP number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_FILT_PAP IS 'This Column will store the Number of Filter Paper Samples.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of RBC Pellets  ; 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_RBC_PEL'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_RBC_PEL number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_RBC_PEL IS 'This Column will store the Number of RBC Pellets .'; 
commit;

 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of Extracted DNA Samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_EXT_DNA'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_EXT_DNA number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_EXT_DNA IS 'This Column will store the Number of Extracted DNA Samples .'; 
commit;

 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of Serum Samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_SERUM'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_SERUM number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_SERUM IS 'This Column will store the Number of Serum Samples.'; 
commit;



 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of Plasma Samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_PLASMA'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_PLASMA number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_PLASMA IS 'This Column will store the CBU Number of Plasma Samples.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of Non-Viable Cell samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_NON_VIA'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_NON_VIA number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_NON_VIA IS 'This Column will store the CBU Number of Non-Viable Cell samples.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of viable samples not representative of the final product
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_NT_REP_FIN'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_NT_REP_FIN number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_NT_REP_FIN IS 'This Column will store the CBU Number of viable samples not representative of the final product.'; 
commit;



 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of Segments 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_OF_SEG'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_OF_SEG number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_OF_SEG IS 'This Column will store the CBU Number of Segments .'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of other representative aliquots stored under conditions consisten with those of the final product
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_OT_REP_CON_FIN'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_OT_REP_CON_FIN number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_OT_REP_CON_FIN IS 'This Column will store the CBU Number of other representative aliquots stored under conditions consisten with those of the final product.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column CBU Number of other representative aliquots stored under alternate conditions
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'CBU_NO_REP_ALT_CON'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add CBU_NO_REP_ALT_CON number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.CBU_NO_REP_ALT_CON IS 'This Column will store the CBU Number of other representative aliquots stored under alternate conditions.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column Maternal Samples Number of Maternal Serum Samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'MT_NO_MAT_SER'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add MT_NO_MAT_SER number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.MT_NO_MAT_SER IS 'This Column will store the MT Number of Maternal Serum Samples.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column Maternal Samples Number of Maternal Plasma Samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'MT_NO_MAT_PAL'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add MT_NO_MAT_PAL number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.MT_NO_MAT_PAL IS 'This Column will store the MT Number of Maternal Plasma Sample.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column Maternal Samples Number of Maternal Extracted DNA Samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'MT_NO_MAT_EXT_DNA'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add MT_NO_MAT_EXT_DNA number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.MT_NO_MAT_EXT_DNA IS 'This Column will store the MT Number of Maternal Extracted DNA Samples.'; 
commit;


 --Sql for Table CB_ENTITY_SAMPLES to add column Maternal Samples Number of Miscellaneous Maternal Samples
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_ENTITY_SAMPLES' AND column_name = 'MT_NO_MIS_MAT'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_ENTITY_SAMPLES add MT_NO_MIS_MAT number(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_ENTITY_SAMPLES.MT_NO_MIS_MAT IS 'This Column will store the MT Number of Maternal Extracted DNA Samples.'; 
commit;


--STARTS ADDING COLUMN ORDER_STATUS TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_HLA_TEMP'
    AND column_name = 'GUID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_HLA_TEMP ADD(GUID varchar2(25))';
  end if;
end;
/
--END--

COMMENT ON COLUMN CB_HLA_TEMP.GUID IS 'Stores guid';

commit;

DECLARE
  v_column_exists INTEGER;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME ='CB_CORD' AND column_name = 'FK_RESOLUTION_CODE';
   if (v_column_exists = 0) then
       execute immediate ('alter table CB_CORD add FK_RESOLUTION_CODE NUMBER(10)');
   end if;
end;
/
COMMENT ON COLUMN cb_cord.FK_RESOLUTION_CODE IS 'Resolution code value for cbu cord stores in this column';
commit; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'REPLY'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_NOTES add REPLY VARCHAR2(1)');
  end if;
end;
/
COMMENT ON COLUMN CB_NOTES.REPLY IS 'Flag to store reply of note'; 
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,1,'01_MISC_ALTER.sql',sysdate,'9.0.0 Build#611');

commit;
