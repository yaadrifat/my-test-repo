set define off;

Alter trigger ER_CB_CORD_BI0 disable;
Alter Trigger  ER_CB_CORD_AU0 disable;

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,5,'05_TRIGGER_DISABLE.sql',sysdate,'9.0.0 Build#611');

commit;