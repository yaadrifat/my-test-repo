-- YK 09May2011 Bug# 5821 $ #5818
drop trigger SCH_SUBCOST_ITEM_AU1;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,122,1,'01_Drop_SCH_SUBCOST_ITEM_AU1_trg.sql',sysdate,'8.10.0 Build#579');

commit;
