set define off;
  CREATE OR REPLACE FORCE VIEW "ERES"."ERV_FORMQUERY" ("PATIENT_ID", "PATIENT_STUDY_ID", "FK_STUDY", "STUDY_NUMBER", "STUDY_TITLE", "FK_PER", "FK_ACCOUNT", "PAT_NAME", "AGE", "GENDER", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "PK_FORMLIB", "FORM_NAME", "RESPONSE_ID", "FIELD_NAME", "FIELD_KEYWORD", "FORMQUERY_TYPE", "QUERY_NOTES", "QUERY_TYPE", "QUERY_STATUS") AS 
  SELECT
(SELECT per_code FROM ER_PER WHERE pk_per = f.fk_per) AS patient_id,
g.patprot_patstdid AS patient_study_id, fk_study,
study_number,STUDY_TITLE, g.fk_per,z.fk_account,
(SELECT PERSON_FNAME || ' ' || PERSON_LNAME FROM PERSON WHERE PK_PERSON = f.fK_PER) PAT_NAME,
(SELECT ROUND((ROUND(SYSDATE - PERSON_DOB))/365) FROM epat.person WHERE pk_person = f.fk_per) AS AGE,
(SELECT F_GET_CODELSTDESC(FK_CODELST_GENDER) FROM epat.person WHERE pk_person = f.fk_per) AS GENDER,
a.created_on CREATED_ON,
(SELECT USR_FIRSTNAME || ' ' || USR_LASTNAME FROM ER_USER WHERE PK_USER = a.CREATOR) creator,
(SELECT USR_FIRSTNAME || ' ' || USR_LASTNAME FROM ER_USER WHERE PK_USER = b.LAST_MODIFIED_BY) LAST_MODIFIED_BY,
b.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
e.pk_formlib, e.form_name, PK_patforms response_id,
(SELECT fld_name FROM ER_FLDLIB WHERE pk_field  = c.fk_field) field_name,
(SELECT fld_uniqueid FROM ER_FLDLIB WHERE pk_field  = c.fk_field) field_keyword,
(CASE WHEN formquery_type = 1 THEN 'System query' WHEN formquery_type = 2 THEN 'Manual query' WHEN formquery_type = 3 THEN 'Response' END) AS formquery_type,
query_notes,
(CASE
	 WHEN formquery_type = 1 THEN (SELECT codelst_desc FROM ER_CODELST WHERE codelst_type = 'form_query' AND pk_codelst = fk_codelst_querytype)
	 WHEN formquery_type = 2 THEN (SELECT codelst_desc FROM ER_CODELST WHERE codelst_type = 'form_query' AND pk_codelst = fk_codelst_querytype)
	 WHEN formquery_type = 3 THEN (SELECT codelst_desc FROM ER_CODELST WHERE codelst_type IN ('form_resp','form_query') AND pk_codelst = fk_codelst_querytype) --10-May-2011@Ankit #5727
END) query_type,
(SELECT codelst_desc FROM ER_CODELST WHERE codelst_type = 'query_status' AND pk_codelst = fk_codelst_querystatus) query_status
FROM ER_FORMQUERY a, ER_FORMQUERYSTATUS b, ER_FORMFLD c, ER_FORMSEC d,
ER_FORMLIB e, ER_PATFORMS f, ER_PATPROT g,ER_STUDY z
WHERE b.fk_formquery = a.pk_formquery
AND a.fk_field = c.fk_field
AND d.pk_formsec = c.fk_formsec
AND e.pk_formlib = d.fk_formlib
AND pk_formlib = f.fk_formlib
AND pk_patforms = fk_querymodule
AND pk_patprot = fk_patprot
AND pk_study = fk_study(+)
AND b.pk_formquerystatus = (SELECT MAX(x.pk_formquerystatus) FROM ER_FORMQUERYSTATUS x WHERE x.fk_formquery = a.pk_formquery
AND TRUNC(x.entered_on) = (SELECT MAX(TRUNC(y.entered_on)) FROM ER_FORMQUERYSTATUS y WHERE y.fk_formquery = a.pk_formquery))
AND f.RECORD_TYPE <> 'D';
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,122,9,'09_ERV_FORMQUERY.sql',sysdate,'8.10.0 Build#579');

commit;