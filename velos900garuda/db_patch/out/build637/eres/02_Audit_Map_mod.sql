set define off;
DECLARE
  COUNTFLAG NUMBER(5);
  COUNTFLAG1 number(5);
  COUNTFLAG2 number(5);
  COUNTFLAG3 NUMBER(5);
  
begin
    select COUNT(*) INTO COUNTFLAG from AUDIT_MAPPEDVALUES where FROM_VALUE='CB_CORD.maternal_local_id';
     if (COUNTFLAG = 1) then
     UPDATE AUDIT_MAPPEDVALUES SET FROM_VALUE='CB_CORD.MATERNAL_LOCAL_ID' where FROM_VALUE='CB_CORD.maternal_local_id';
commit;
END IF;
 SELECT COUNT(*) INTO COUNTFLAG1 from AUDIT_MAPPEDVALUES where FROM_VALUE='CB_CORD.registry_maternal_id';
    IF (COUNTFLAG1 = 1) THEN
    UPDATE AUDIT_MAPPEDVALUES SET FROM_VALUE='CB_CORD.REGISTRY_MATERNAL_ID' where FROM_VALUE='CB_CORD.registry_maternal_id';
commit;
END IF; 

SELECT COUNT(*) INTO COUNTFLAG2 from AUDIT_MAPPEDVALUES where FROM_VALUE='CB_CORD.CORD_NMDP_STATUS' AND TO_VALUE='Stores reference of cbu status';
    IF (COUNTFLAG2 = 1) THEN
    UPDATE AUDIT_MAPPEDVALUES SET TO_VALUE='Cord Status' where FROM_VALUE='CB_CORD.CORD_NMDP_STATUS' AND TO_VALUE='Stores reference of cbu status';
commit;
END IF;


SELECT COUNT(*) INTO COUNTFLAG3 from AUDIT_MAPPEDVALUES where FROM_VALUE='CB_CORD.FK_CORD_CBU_STATUS' AND TO_VALUE='Stores reference of cord';
    if (COUNTFLAG3 = 1) then
    UPDATE AUDIT_MAPPEDVALUES SET TO_VALUE='CBU Status' where FROM_VALUE='CB_CORD.FK_CORD_CBU_STATUS' AND TO_VALUE='Stores reference of cord';
commit;
END IF;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,180,2,'02_Audit_Map_mod.sql',sysdate,'9.0.0 Build#637');

commit;