set define off;

DELETE FROM "ERES"."ER_REPXSL" WHERE PK_REPXSL = 89;

Commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,180,19,'19_hotfix3_delete_RepXsl.sql',sysdate,'9.0.0 Build#637');

commit;