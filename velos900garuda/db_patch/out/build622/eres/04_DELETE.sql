delete from ER_LKPCOL where FK_LKPLIB in (select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE' and LKPTYPE_NAME= 'dynReports');


delete from ER_LKPVIEWCOL where FK_LKPVIEW in (select PK_LKPVIEW from ER_LKPVIEW where LKPVIEW_NAME='CBU CBB PROCESSING PROCEDURE');

delete from ER_LKPVIEW where FK_LKPLIB in (select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE' and LKPTYPE_NAME= 'dynReports');

delete from ER_LKPLIB where  LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE' and LKPTYPE_NAME= 'dynReports';

--STARTS DELETING THE OLD IDM RECORD FROM ER_PATLABS--

delete from er_patlabs where FK_TESTGROUP =(select pk_labgroup from er_labgroup where group_type='I');
delete from er_patlabs where FK_TESTGROUP =(select pk_labgroup from er_labgroup where group_type='IOG');

--END--

--STARTS DELETING THE OLD IDM RECORD FROM CB_ASSESSMENT--

delete from cb_assessment where SUB_ENTITY_TYPE =(select pk_codelst from er_codelst where codelst_subtyp='IDM');

--END--


commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,4,'04_DELETE.sql',sysdate,'9.0.0 Build#622');

commit;