CREATE OR REPLACE FORCE VIEW "ERES"."REP_CBU_LAB_SUMMARY" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "PROCESS_DATE", "BACT_CULT_DATE", "FUNG_CULT_DATE", "BACTERIAL_CULTURE", "FUNGAL_CULTURE", "ABO_TYPE", "RH_TYPE", "HMGLBN_TSTNG", "FREEZE_DATE", "FUNG_COMMENT", "BACT_COMMENT", "FK_ACCOUNT", "CREATOR", "CREATED_ON", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE")
AS
  SELECT cord.CORD_REGISTRY_ID,
    cord.CORD_LOCAL_CBU_ID,
    cord.REGISTRY_MATERNAL_ID ,
    cord.MATERNAL_LOCAL_ID,
    cord.PRCSNG_START_DATE,
    CORD.BACT_CULT_DATE,
    CORD.FUNG_CULT_DATE,
    f_codelst_desc(CORD.FK_CORD_BACT_CUL_RESULT),
    f_codelst_desc(CORD.FK_CORD_FUNGAL_CUL_RESULT),
    f_codelst_desc(CORD.FK_CORD_ABO_BLOOD_TYPE),
    f_codelst_desc(CORD.FK_CORD_RH_TYPE),
    f_codelst_desc(CORD.HEMOGLOBIN_SCRN),
    CORD.FRZ_DATE,
    CORD.FUNG_COMMENT,
    CORD.BACT_COMMENT,
    er.fk_account AS FK_ACCOUNT,
    u.usr_firstname
    ||' '
    || u.usr_lastname,
    e.CREATED_ON,
    u.usr_firstname
    ||' '
    ||u.usr_lastname,
    e.LAST_MODIFIED_DATE
  FROM CB_CORD CORD,
    ER_PATLABS e,
    ER_SITE er,
    ER_USER u
  WHERE cord.fk_cbb_id   = er.pk_site
  AND e.creator          = u.PK_USER
  AND e.LAST_MODIFIED_BY = u.PK_USER;

  

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,35,'35_REP_CBU_LAB_SUMMARY.sql',sysdate,'9.0.0 Build#622');

commit;