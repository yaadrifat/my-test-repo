create or replace TRIGGER "ERES".ER_ATTACHMENTS_AU0  AFTER UPDATE OF PK_ATTACHMENT,
  ENTITY_ID,
  ENTITY_TYPE,
  FK_ATTACHMENT_TYPE,
  DCMS_FILE_ATTACHMENT_ID,
  CREATOR,
  CREATED_ON,
  IP_ADD,
  LAST_MODIFIED_BY,
  LAST_MODIFIED_DATE,
  RID,
  ATTACHMENTS_TYPE_REM,
  DOCUMENT_TYPE,
  DELETEDFLAG,
  ATTACHMENT_FILE_NAME ON ER_ATTACHMENTS REFERENCING OLD AS OLD NEW AS NEW

FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR(200); 
  old_modified_by VARCHAR2(100);
  new_modified_by VARCHAR2(100);
  NEW_FK_ATTACHMENT_TYPE  VARCHAR2(200);
  OLD_FK_ATTACHMENT_TYPE  VARCHAR2(200);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
   usr := getuser(:NEW.LAST_MODIFIED_BY);
  IF NVL(:OLD.FK_ATTACHMENT_TYPE,0) !=  NVL(:NEW.FK_ATTACHMENT_TYPE,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ATTACHMENT_TYPE)		INTO NEW_FK_ATTACHMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ATTACHMENT_TYPE := '';
   END; 
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_ATTACHMENT_TYPE)		INTO OLD_FK_ATTACHMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_ATTACHMENT_TYPE := '';
   END;
   END IF;
  audit_trail.record_transaction
    (raid, 'ER_ATTACHMENTS', :OLD.RID, 'U', usr);
    
  IF NVL(:OLD.PK_ATTACHMENT,0) != NVL(:NEW.PK_ATTACHMENT,0) THEN
     audit_trail.column_update
       (raid, 'PK_ATTACHMENT',
       :OLD.PK_ATTACHMENT, :NEW.PK_ATTACHMENT);
  END IF;
  IF NVL(:OLD.ENTITY_ID,0) != NVL(:NEW.ENTITY_ID,0) THEN
     audit_trail.column_update
       (raid, 'ENTITY_ID',
       :OLD.ENTITY_ID, :NEW.ENTITY_ID);
  END IF;
  IF NVL(:OLD.ENTITY_TYPE,0) != NVL(:NEW.ENTITY_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'ENTITY_TYPE',
       :OLD.ENTITY_TYPE, :NEW.ENTITY_TYPE);
  END IF;  
  IF NVL(:OLD.FK_ATTACHMENT_TYPE,0) !=  NVL(:NEW.FK_ATTACHMENT_TYPE,0) THEN
     audit_trail.column_update(raid, 'FK_ATTACHMENT_TYPE',OLD_FK_ATTACHMENT_TYPE, NEW_FK_ATTACHMENT_TYPE);
  END IF;
  IF NVL(:OLD.DCMS_FILE_ATTACHMENT_ID,0) != NVL(:NEW.DCMS_FILE_ATTACHMENT_ID,0) THEN
     audit_trail.column_update
       (raid, 'DCMS_FILE_ATTACHMENT_ID',
       :OLD.DCMS_FILE_ATTACHMENT_ID, :NEW.DCMS_FILE_ATTACHMENT_ID);
  END IF;
   IF NVL(:OLD.CREATOR,0) !=
     NVL(:NEW.CREATOR,0) THEN
     audit_trail.column_update
       (raid, 'CREATOR',
       :OLD.CREATOR, :NEW.CREATOR);
  END IF;  
  IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);     
    END IF;    
  if nvl(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) then
    Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
			old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
			new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);
    end if;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.RID, :NEW.RID);     
    END IF;
	IF NVL(:OLD.DOCUMENT_TYPE,' ') != NVL(:NEW.DOCUMENT_TYPE,' ') THEN
     audit_trail.column_update(raid, 'DOCUMENT_TYPE',:OLD.DOCUMENT_TYPE, :NEW.DOCUMENT_TYPE);
  END IF; 
  IF NVL(:OLD.ATTACHMENTS_TYPE_REM,' ') != NVL(:NEW.ATTACHMENTS_TYPE_REM,' ') THEN
     audit_trail.column_update
       (raid, 'ATTACHMENTS_TYPE_REM',
       :OLD.ATTACHMENTS_TYPE_REM, :NEW.ATTACHMENTS_TYPE_REM);
  END IF;  
  IF NVL(:OLD.ATTACHMENT_FILE_NAME,' ') != NVL(:NEW.ATTACHMENT_FILE_NAME,' ') THEN
     audit_trail.column_update
       (raid, 'ATTACHMENT_FILE_NAME',
       :OLD.ATTACHMENT_FILE_NAME, :NEW.ATTACHMENT_FILE_NAME);
  END IF;	
END ;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,24,'24_ER_ATTACHMENTS_AU0.sql',sysdate,'9.0.0 Build#622');

commit;