set define off;

-------------------------ER ATTACHMENTS Table------------------------------


  CREATE TABLE "ER_ATTACHMENTS" 
  (	"PK_ATTACHMENT" NUMBER(10,0) PRIMARY KEY, 
	"ENTITY_ID" NUMBER(10,0), 
	"ENTITY_TYPE" NUMBER(10,0), 
	"FK_ATTACHMENT_TYPE" NUMBER(10,0), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" VARCHAR2(1 BYTE), 
	"RID" NUMBER(10,0), 
	"DOCUMENT_TYPE" VARCHAR2(25 BYTE), 
	"DOCUMENT_FILE" BLOB, 
	"ATTACHMENTS_TYPE_REM" VARCHAR2(255 CHAR), 
	"ATTACHMENT_FILE_NAME" VARCHAR2(100 BYTE), 
	"DCMS_FILE_ATTACHMENT_ID" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 262144 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "ERES_USER" 
 LOB ("DOCUMENT_FILE") STORE AS "SYS_LOB0000261733C00012$$"(
  TABLESPACE "ERES_USER" ENABLE STORAGE IN ROW CHUNK 8192 PCTVERSION 10
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) ;
 

   COMMENT ON TABLE "ER_ATTACHMENTS" IS 'Table to store user-uploaded attachments. This works similarly as ER_STUDYAPNDX, etc., but this can be used for general purpose.';

   COMMENT ON COLUMN "ER_ATTACHMENTS"."PK_ATTACHMENT" IS 'Primery Key';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."ENTITY_ID" IS 'Id of the entity like ERU, Doner etc';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."ENTITY_TYPE" IS 'Type of entity';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."FK_ATTACHMENT_TYPE" IS 'Code for Attachment type reference from codelist(ER_CODELST)';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."DOCUMENT_TYPE" IS 'Type of the document';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."ATTACHMENTS_TYPE_REM" IS 'Remarks for the attachment';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."ATTACHMENT_FILE_NAME" IS 'Name of the file attached';
 
   COMMENT ON COLUMN "ER_ATTACHMENTS"."DCMS_FILE_ATTACHMENT_ID" IS 'Random Id genrated for DCMS Connection.';
 

-------------------------ER ATTACHMENTS Table Ends Here------------------------------

-------------------------ER Attachment Table Sequence-----------------------------------

   
   CREATE SEQUENCE  "SEQ_ER_ATTACHMENTS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;

-------------------------ER Attachment Table Sequence Ends Here------------------------


---------------------------ER MAILCONFIG Table ---------------------------------------

CREATE TABLE "ER_MAILCONFIG" 
   (	"PK_MAILCONFIG" NUMBER PRIMARY KEY, 
	"MAILCONFIG_MAILCODE" VARCHAR2(10 BYTE), 
	"MAILCONFIG_SUBJECT" VARCHAR2(500 BYTE), 
	"MAILCONFIG_CONTENT" VARCHAR2(4000 BYTE), 
	"MAILCONFIG_CONTENTTYPE" VARCHAR2(100 BYTE), 
	"MAILCONFIG_FROM" VARCHAR2(100 BYTE), 
	"MAILCONFIG_BCC" VARCHAR2(500 BYTE), 
	"MAILCONFIG_CC" VARCHAR2(500 BYTE), 
	"DELETEDFLAG" NUMBER DEFAULT 0, 
	"FK_CODELSTORGID" NUMBER DEFAULT 0, 
	"RID" NUMBER, 
	"MAILCONFIG_HASATTACHEMENT" NUMBER(1,0), 
	"CREATED_ON" DATE, 
	"CREATOR" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"IP_ADD" VARCHAR2(15 BYTE)
    )
    TABLESPACE "ERES_USER" PCTFREE 10 INITRANS 2 MAXTRANS 255
    STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 

   COMMENT ON TABLE "ER_MAILCONFIG" IS 'Table to store mail configuration settings.';

   COMMENT ON COLUMN "ER_MAILCONFIG"."PK_MAILCONFIG" IS 'Primary Key';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."MAILCONFIG_SUBJECT" IS 'Subject of the mail';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."MAILCONFIG_CONTENT" IS 'Content of the mail';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."MAILCONFIG_CONTENTTYPE" IS 'Type of the content of the mail';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."MAILCONFIG_FROM" IS 'Sender of the mail';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."MAILCONFIG_BCC" IS 'Default BCC in the email';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."MAILCONFIG_CC" IS 'Default CC in the email';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."MAILCONFIG_HASATTACHEMENT" IS 'Whether mail Attached or not';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
 
   COMMENT ON COLUMN "ER_MAILCONFIG"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
---------------------------ER MAILCONFIG Table Ends Here ----------------------------------------------------

----------------------------ER MAILCONFIG Table Sequence----------------------------------------------------


   CREATE SEQUENCE  "SEQ_ER_MAILCONFIG"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
 
---------------------------ER MAILCONFIG Table Sequence Ends Here-------------------------------------------

---------------------------ER NOTIFICATION Table-------------------------------------------------------------

  CREATE TABLE "ER_NOTIFICATION" 
   (	"PK_NOTIFICATION" NUMBER PRIMARY KEY, 
	"NOTIFY_FROM" VARCHAR2(500 BYTE), 
	"NOTIFY_TO" VARCHAR2(500 BYTE), 
	"NOTIFY_CC" VARCHAR2(500 BYTE), 
	"NOTIFY_BCC" VARCHAR2(500 BYTE), 
	"NOTIFY_SUBJECT" VARCHAR2(500 BYTE), 
	"NOTIFY_CONTENT" VARCHAR2(4000 BYTE), 
	"NOTIFY_ISSENT" NUMBER DEFAULT 0, 
	"DELETEDFLAG" NUMBER DEFAULT 0, 
	"ATTACHMENT_QUERY" VARCHAR2(2000 BYTE), 
	"CREATED_ON" DATE, 
	"CREATOR" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"RID" NUMBER(10,0)
    )
    TABLESPACE "ERES_USER" PCTFREE 10 INITRANS 2 MAXTRANS 255
    STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 

   COMMENT ON TABLE "ER_NOTIFICATION" IS 'Table to store notifications to the users.';

   COMMENT ON COLUMN "ER_NOTIFICATION"."PK_NOTIFICATION" IS 'Primary Key';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."NOTIFY_FROM" IS 'Notification sent from';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."NOTIFY_TO" IS 'Notification sent to';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."NOTIFY_CC" IS 'Notification copied  to';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."NOTIFY_BCC" IS 'Notification BCC to';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."NOTIFY_SUBJECT" IS 'Subject of the notification';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."NOTIFY_CONTENT" IS 'Content of the notification';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."NOTIFY_ISSENT" IS 'Whether the notification has sent successfully';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "ER_NOTIFICATION"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. ';
 
---------------------------ER NOTIFICATION Table Ends Here---------------------------------------------------------------------------------------

----------------------------ER NOTIFICATION Table Sequence-----------------------------------------------------------------


   CREATE SEQUENCE  "SEQ_ER_NOTIFICATION"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
 
----------------------------ER NOTIFICATION Table Sequence Ends Here-----------------------------------------------------------------

---------------------------ER TASK LIST Table-------------------------------------------------------

 CREATE TABLE "ER_TASK_LIST" 
   (	"PK_TASK" NUMBER(10,0) PRIMARY KEY, 
	"TASK_TYPE" NUMBER(10,0), 
	"TASK_START_DATE" DATE, 
	"TASK_END_DATE" DATE, 
	"TASK_ASSIGNED_TO" NUMBER(10,0), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" VARCHAR2(1 BYTE), 
	"RID" NUMBER(10,0), 
	"DOMAIN_ID" NUMBER(10,0)
    )
    TABLESPACE "ERES_USER" PCTFREE 10 INITRANS 2 MAXTRANS 255
    STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 

   COMMENT ON TABLE "ER_TASK_LIST" IS 'Table for Task Management.';

   COMMENT ON COLUMN "ER_TASK_LIST"."PK_TASK" IS 'Primary Key';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."TASK_TYPE" IS 'This column stores the type of task.';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."TASK_START_DATE" IS 'This column stores the start date of the task.';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."TASK_END_DATE" IS 'This column stores the end date of the task.';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.  ';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "ER_TASK_LIST"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. ';
 
---------------------------ER TASK LIST Table Ends Here-------------------------------------------------------


---------------------------ER TASK LIST Table Sequence-------------------------------------------------------

   CREATE SEQUENCE  "SEQ_ER_TASK_LIST"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
 
----------------------------ER TASK LIST Table Sequence Ends Here---------------------------------------------


---------------------------ER ORDER Table------------------------------------------

  CREATE TABLE "ER_ORDER" 
   (	"PK_ORDER" NUMBER(10,0) PRIMARY KEY, 
	"ORDER_NUMBER" VARCHAR2(20 BYTE), 
	"ORDER_ENTITYID" NUMBER(10,0), 
	"ORDER_ENTITYTYPE" NUMBER(10,0), 
	"ORDER_DATE" DATE, 
	"FK_ORDER_STATUS" NUMBER, 
	"ORDER_REMARK" VARCHAR2(200 BYTE), 
	"ORDER_BY" NUMBER(10,0), 
	"FK_ORDER_ITEM_ID" NUMBER, 
	"FK_ORDER_TYPE" NUMBER(10,0), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER, 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" NUMBER(10,0), 
	"RID" NUMBER(10,0), 
	"FK_ATTACHMENT" NUMBER(10,0)
    )
    TABLESPACE "ERES_USER" PCTFREE 10 INITRANS 2 MAXTRANS 255
    STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 

   COMMENT ON TABLE "ER_ORDER" IS 'Table for Order Management.';

   COMMENT ON COLUMN "ER_ORDER"."PK_ORDER" IS 'Primary Key';
 
   COMMENT ON COLUMN "ER_ORDER"."ORDER_NUMBER" IS 'Number given to Order';
 
   COMMENT ON COLUMN "ER_ORDER"."ORDER_ENTITYID" IS 'Entity ID to which the order is linked with';
 
   COMMENT ON COLUMN "ER_ORDER"."ORDER_ENTITYTYPE" IS 'Type of the Entity ID ( For example ERU. DONOR)';
 
   COMMENT ON COLUMN "ER_ORDER"."ORDER_DATE" IS 'Order Date';
 
   COMMENT ON COLUMN "ER_ORDER"."FK_ORDER_STATUS" IS 'Status of the Order';
 
   COMMENT ON COLUMN "ER_ORDER"."ORDER_REMARK" IS 'Order comments';
 
   COMMENT ON COLUMN "ER_ORDER"."ORDER_BY" IS 'Order given by';
 
   COMMENT ON COLUMN "ER_ORDER"."FK_ORDER_ITEM_ID" IS 'Order Item reference';
 
   COMMENT ON COLUMN "ER_ORDER"."FK_ORDER_TYPE" IS 'Order type referenced from Code List';
 
   COMMENT ON COLUMN "ER_ORDER"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.  ';
 
   COMMENT ON COLUMN "ER_ORDER"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "ER_ORDER"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "ER_ORDER"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "ER_ORDER"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "ER_ORDER"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "ER_ORDER"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
 
   COMMENT ON COLUMN "ER_ORDER"."FK_ATTACHMENT" IS 'Reference to Attachment';
 

---------------------------ER ORDER Table Ends Here------------------------------------------

---------------------------ER ORDER Table Sequence-------------------------------------------------------------

   CREATE SEQUENCE  "SEQ_ER_ORDER"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 18 NOCACHE  NOORDER  NOCYCLE ;
 
---------------------------ER ORDER Table Sequence Ends Here-------------------------------------------------------------

---------------------------ER ORDER ATTACHMENTS Table--------------------------------------------


 CREATE TABLE "ER_ORDER_ATTACHMENTS" 
   (	"PK_ORDER_ATTACHMENT" NUMBER(10,0) PRIMARY KEY, 
	"FK_ORDER" NUMBER(10,0), 
	"FK_ATTACHMENT" NUMBER(19,0),
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER, 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"DELETEDFLAG" NUMBER(10,0), 
	"RID" NUMBER(10,0) 
    )
    TABLESPACE "ERES_USER" PCTFREE 10 INITRANS 2 MAXTRANS 255
    STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
    PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 

   COMMENT ON TABLE "ER_ORDER_ATTACHMENTS" IS 'Table for managing attachments to orders.';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."PK_ORDER_ATTACHMENT" IS 'Primary Key';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."FK_ORDER" IS 'FK TO ER_ORDER';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."FK_ATTACHMENT" IS 'Attachment reference (ER_ATTACHMENTS)';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. ';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row. ';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
   COMMENT ON COLUMN "ER_ORDER_ATTACHMENTS"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

---------------------------ER ORDER ATTACHMENTS Table Ends Here------------------------------------------


   CREATE SEQUENCE  "SEQ_ER_ORDER_ATTACHMENTS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
 


---------------------------ER ORDER ATTACHMENTS Table Sequence-------------------------------------


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,133,1,'01_Er_tables.sql',sysdate,'9.0.0 Build#590');

commit;