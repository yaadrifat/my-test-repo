set define off;
set serveroutput on;

DECLARE
  start_value NUMBER(10);
  seq_count NUMBER(10);
BEGIN
  select count(1) into seq_count from user_sequences where 
  lower(sequence_name) = 'seq_sch_codelst';
  if (seq_count > 0) then
    execute immediate 'DROP SEQUENCE seq_sch_codelst';
  end if;
  select count(1) into seq_count from user_sequences where 
  lower(sequence_name) = 'sch_codelst_seq1';
  if (seq_count > 0) then
    execute immediate 'DROP SEQUENCE sch_codelst_seq1';
  end if;
  select max(pk_codelst)+1 into start_value from sch_codelst;
  execute immediate 'CREATE SEQUENCE "ESCH"."SCH_CODELST_SEQ1" '||
  ' MINVALUE 250 MAXVALUE 999999999999999999999999999 '||
  ' INCREMENT BY 1 START WITH '||start_value||' NOCACHE ORDER NOCYCLE ';
  dbms_output.put_line('Successfully dropped seq_sch_codelst and recreated sch_codelst_seq1');
EXCEPTION when others then
  dbms_output.put_line('Error while dropping seq_sch_codelst and recreating sch_codelst_seq1');
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,1,'01_fix_sch_codelst_seq1.sql',sysdate,'9.0.0 Build#627');

commit;