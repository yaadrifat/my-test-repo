CREATE OR REPLACE FORCE VIEW "ERES"."REP_CB_CORD_AUDT_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "COLUMN_NAME", "COLUMN_DISPLAY_NAME", "OLD_VALUE", "NEW_VALUE", "REASON", "REMARKS", "FK_ROW_ID")
AS
  SELECT cord.CORD_REGISTRY_ID,
    cord.Cord_Local_Cbu_Id,
    cord.Registry_Maternal_Id,
    cord.Registry_Maternal_Id,
    cord.Created_On ,
    cord.Creator,
    cord.Last_Modified_By,
    cord.Last_Modified_Date,
    acm.column_name,
    ACM.COLUMN_DISPLAY_NAME,
    acm.old_value,
    acm.new_value,
    f_codelst_desc(acm.FK_CODELST_REASON) AS REASON,
    acm.REMARKS,
    acm.fk_row_id
  FROM REP_CB_CORD_VIEW cord,
    AUDIT_COLUMN_MODULE acm
  WHERE cord.pk_row_id = acm.fk_row_id;
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,2,'02_REP_CB_CORD_AUDT_VIEW.sql',sysdate,'9.0.0 Build#627');

commit;