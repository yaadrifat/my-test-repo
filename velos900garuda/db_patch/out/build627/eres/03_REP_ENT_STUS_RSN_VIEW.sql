CREATE OR REPLACE FORCE VIEW "ERES"."REP_ENT_STUS_RSN_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATOR", "CREATED_ON", "IP_ADDRESS", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "STATUS_TYPE", "STATUS_VALUE", "REASON", "PK_ROW_ID", "MODULE_ID")
AS
  SELECT Cord.Cord_Registry_Id,
    Cord.Cord_Local_Cbu_Id,
    Cord.Registry_Maternal_Id,
    Cord.Maternal_Local_Id,
    (SELECT Er_User.Usr_Firstname
      || ' '
      || Er_User.Usr_Lastname
    FROM Er_User
    WHERE Pk_User = Rsn.Creator
    )Creator,
    Rsn.Created_On,
    Rsn.Ip_Add,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || Er_User.Usr_Lastname
    FROM Er_User
    WHERE Pk_User = Rsn.Last_Modified_By
    ) Last_Modified_By,
    Rsn.Last_Modified_Date,
    status.status_type_code,
    F_Codelst_Desc(status.fk_status_value),
    F_Codelst_Desc(Rsn.Fk_Reason_Id) AS REASON,
    arm.PK_ROW_ID,
    arm.module_id
  FROM Cb_Entity_Status_Reason Rsn,
    AUDIT_ROW_MODULE arm,
    Cb_Cord Cord,
    Cb_Entity_Status Status
  WHERE status.entity_id          = cord.pk_cord
  AND Rsn.PK_ENTITY_STATUS_REASON = arm.module_id
  AND Status.Pk_Entity_Status     = Rsn.Fk_Entity_Status;
  
  

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,3,'03_REP_ENT_STUS_RSN_VIEW.sql',sysdate,'9.0.0 Build#627');

commit;