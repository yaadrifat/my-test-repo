--getAlertCondition functin

create or replace
function getAlertCondition(alertId number,alerttype varchar2) return varchar2
is
var_alertCondition varchar2(4000);
var_keycond varchar2(4000);
var_temp varchar2(255 byte) := '';
begin
for alerts in (select 
  START_BRACE,
  TABLE_NAME,
  COLUMN_NAME,
  CONDITION_VALUE,
  ARITHMETIC_OPERATOR,
  LOGICAL_OPERATOR,
  END_BRACE,
  KEY_CONDITION
from cb_alert_conditions where upper(condition_type)=alerttype and fk_codelst_alert=alertId order by PK_ALERT_CONDITIONS)
loop
--dbms_output.put_line('var_temp:::::::::'||var_temp||'...............');
--dbms_output.put_line('alerts.key_condition:::::::::'||alerts.key_condition);
if var_temp = alerts.key_condition then
  dbms_output.put('');
else
  var_keycond := var_keycond||'AND '||alerts.key_condition;
  --dbms_output.put_line('???????????????????????????????'||var_keycond);
  var_temp:=alerts.key_condition;
  end if;
  var_alertcondition := var_alertcondition||alerts.START_BRACE||alerts.TABLE_NAME||'.'||alerts.COLUMN_NAME||alerts.ARITHMETIC_OPERATOR||ALERTS.CONDITION_VALUE||alerts.END_BRACE||' '||alerts.LOGICAL_OPERATOR||' ';
end loop;
--dbms_output.put_line('Alert condition is===========>'||var_alertcondition);
return var_alertcondition||var_keycond;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,11,'11_getAlertCondition.sql',sysdate,'9.0.0 Build#627');

commit;
