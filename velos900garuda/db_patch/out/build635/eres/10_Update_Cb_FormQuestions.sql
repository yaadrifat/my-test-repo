--STARTS UPDATING RECORD FROM CB_FORMS TABLE--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'V1';
  if (v_record_exists = 1) then
	UPDATE CB_FORMS SET VERSION='N2G' WHERE FORMS_DESC = 'MRQ' AND VERSION = 'V1';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'FMHQ'
    AND VERSION = 'V1';
  if (v_record_exists = 1) then
	UPDATE CB_FORMS SET VERSION='N2D' WHERE FORMS_DESC = 'FMHQ' AND VERSION = 'V1';
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,178,10,'10_Update_Cb_FormQuestions.sql',sysdate,'9.0.0 Build#635');

commit;