delete from cb_question_group where pk_question_group not in(select distinct fk_question_group from cb_question_grp ) and QUESTION_GRP_DESC in ('A. Illness and Medications','B. Behavior and Exposure Risk','C. CJD Risk','D. HIV-1 Group O Risk');
commit;

--STARTS ADDING COLUMN TO CB_FORM_QUESTIONS--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_QUESTIONS'
    AND column_name = 'QUES_SEQ';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_QUESTIONS ADD(QUES_SEQ VARCHAR2(15 BYTE))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_QUESTIONS'
    AND column_name = 'FK_MASTER_QUES';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_QUESTIONS ADD(FK_MASTER_QUES NUMBER(10,0))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_QUESTIONS'
    AND column_name = 'FK_DEPENDENT_QUES';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_QUESTIONS ADD(FK_DEPENDENT_QUES NUMBER(10,0))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_QUESTIONS'
    AND column_name = 'FK_DEPENDENT_QUES_VALUE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_QUESTIONS ADD(FK_DEPENDENT_QUES_VALUE VARCHAR2(400 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN CB_FORM_QUESTIONS.FK_DEPENDENT_QUES IS 'Stores reference to the CB_QUESTION table which determines the dependent question';

COMMENT ON COLUMN CB_FORM_QUESTIONS.FK_MASTER_QUES IS 'Stores reference to the CB_QUESTION table which determines the master question under which this question will display as sub question';

COMMENT ON COLUMN CB_FORM_QUESTIONS.QUES_SEQ IS 'Stores the sequence order of the question';

COMMENT ON COLUMN CB_FORM_QUESTIONS.FK_DEPENDENT_QUES_VALUE IS 'Stores dependent questions response value';

--START CPOYING DATA FROM CB_QUESTIONS TO CB_FORM_QUESTIONS --

update cb_form_questions frmques set frmques.ques_seq=(select ques.ques_seq from cb_questions ques where frmques.fk_question=ques.pk_questions) ;

update cb_form_questions frmques set frmques.FK_MASTER_QUES=(select ques.FK_MASTER_QUES from cb_questions ques where frmques.fk_question=ques.pk_questions) ;

update cb_form_questions frmques set frmques.FK_DEPENDENT_QUES=(select FK_DEPENDENT_QUES from cb_questions ques where frmques.fk_question=ques.pk_questions) ;

update cb_form_questions frmques set frmques.FK_DEPENDENT_QUES_VALUE=(select ques.FK_DEPENDENT_QUES_VALUE from cb_questions ques where frmques.fk_question=ques.pk_questions) ;

--END--

--STARTS DROPING THE COLUMN FROM CB_QUESTIONS--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_QUESTIONS'
    AND column_name = 'QUES_SEQ';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE ERES.CB_QUESTIONS DROP COLUMN QUES_SEQ';
      commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_QUESTIONS'
    AND column_name = 'FK_MASTER_QUES';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE ERES.CB_QUESTIONS DROP COLUMN FK_MASTER_QUES';
      commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_QUESTIONS'
    AND column_name = 'FK_DEPENDENT_QUES';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE ERES.CB_QUESTIONS DROP COLUMN FK_DEPENDENT_QUES';
      commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_QUESTIONS'
    AND column_name = 'FK_DEPENDENT_QUES_VALUE';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE ERES.CB_QUESTIONS DROP COLUMN FK_DEPENDENT_QUES_VALUE';
      commit;
  end if;
end;
/
--End--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,178,9,'09_alter_Cb_Question.sql',sysdate,'9.0.0 Build#635');

commit;