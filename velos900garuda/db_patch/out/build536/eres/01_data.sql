SET DEFINE OFF;

Insert into ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE, 
    OBJECT_DISPLAYTEXT, FK_ACCOUNT)
 Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'T', '6', 'protocol_tab', 6, 1, 
    'Event-Visit Grid', 0);

COMMIT;


DELETE FROM ER_REPXSL WHERE FK_REPORT = 110; 
DELETE FROM ER_REPXSL WHERE FK_REPORT = 159; 

commit;


set define off

update er_browserconf
set browserconf_settings = '{"key":"LETTER_LINK","label":"Outcome Letter","format":"letterLink", "sortable":false, "resizeable":true,"hideable":true}' where browserconf_colname = 'LETTER_LINK';

update er_browserconf
set browserconf_settings = '{"key":"SEND_LINK","label":"Send Back to Review Board","format":"sendLink", "sortable":false, "resizeable":true,"hideable":true}'
where browserconf_colname = 'SEND_LINK';

update er_ctrltab
set ctrl_desc = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pre-review'
where ctrl_key = 'app_rights' and ctrl_value = 'MIRB_SAS';


update er_ctrltab
set ctrl_desc = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Review Assignment'
where ctrl_key = 'app_rights' and ctrl_value = 'MIRB_SCS';

update er_ctrltab
set ctrl_desc = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Post Review Processing'
where ctrl_key = 'app_rights' and ctrl_value = 'MIRB_SPRS';


commit;



declare
v_ct number;
begin

	select count(*) into v_ct from ER_IRBFORMS_SETUP where IRB_TAB_SUBTYPE = 'irb_view_all';
	
	if (v_ct = 0 ) then

		Insert into ER_IRBFORMS_SETUP
		   (PK_IRBFORMS_SETUP, IRB_TAB_SUBTYPE, CODELST_SUBMISSION_SUBTYPE, FORM_CATLIB_SUBTYPE, FORM_SUBMISSION_SUBTYPE)
		 Values
		   (seq_ER_IRBFORMS_SETUP.nextval, 'irb_view_all', '', 'irb_check1', '');

		Insert into ER_IRBFORMS_SETUP
		   (PK_IRBFORMS_SETUP, IRB_TAB_SUBTYPE, CODELST_SUBMISSION_SUBTYPE, FORM_CATLIB_SUBTYPE, FORM_SUBMISSION_SUBTYPE)
		 Values
		   (seq_ER_IRBFORMS_SETUP.nextval, 'irb_view_all', '', 'irb_check2', '');

		Insert into ER_IRBFORMS_SETUP
		   (PK_IRBFORMS_SETUP, IRB_TAB_SUBTYPE, CODELST_SUBMISSION_SUBTYPE, FORM_CATLIB_SUBTYPE, FORM_SUBMISSION_SUBTYPE)
		 Values
		   (seq_ER_IRBFORMS_SETUP.nextval, 'irb_view_all', '', 'irb_check3', '');

		   
		COMMIT;
	
	end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,79,1,'01_data.sql',sysdate,'8.9.0 Build#536');

commit;