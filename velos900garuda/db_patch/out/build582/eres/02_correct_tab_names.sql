set define off;

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Patient Cost Items' WHERE OBJECT_DISPLAYTEXT = 'Subject Cost Items' and OBJECT_NAME = 'protocol_tab';

commit;

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Milestones Setup' WHERE OBJECT_DISPLAYTEXT = 'Calendar Milestone Setup' and OBJECT_NAME = 'protocol_tab';

commit;

DECLARE 
  lkpId NUMBER default 0; 
BEGIN 
  select PK_LKPLIB into lkpId from ER_LKPLIB
  where LKPTYPE_NAME = 'dynReports' AND LKPTYPE_TYPE = 'dyn_s'
  and LKPTYPE_DESC = 'Study SCI';
  
  if (lkpId > 0) then 
	Update ER_LKPLIB Set LKPTYPE_DESC = 'Study Calendar Patient Cost Items',
	LKPTYPE_VERSION ='1.1'
	where PK_LKPLIB = lkpId;
	
	Update ER_LKPVIEW Set LKPVIEW_NAME = 'Study Pat Cost Items',
	LKPVIEW_DESC = 'Study Calendar Patient Cost Items' 
	where FK_LKPLIB = lkpId;
	
	commit;
  end if;
  
  lkpId := 0;
  select PK_LKPLIB into lkpId from ER_LKPLIB
  where LKPTYPE_NAME = 'dynReports' AND LKPTYPE_TYPE = 'dyn_a'
  and LKPTYPE_DESC = 'Library SCI';
  
  if (lkpId > 0) then 
	Update ER_LKPLIB Set LKPTYPE_DESC = 'Library Calendar Patient Cost Items',
	LKPTYPE_VERSION ='1.1'
	where PK_LKPLIB = lkpId;
	
	Update ER_LKPVIEW Set LKPVIEW_NAME = 'Lib Pat Cost Items',
	LKPVIEW_DESC = 'Library Calendar Patient Cost Items' 
	where FK_LKPLIB = lkpId;
	
	commit;
  end if;	
  
   
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,125,2,'02_correct_tab_names.sql',sysdate,'8.10.0 Build#582');

commit;