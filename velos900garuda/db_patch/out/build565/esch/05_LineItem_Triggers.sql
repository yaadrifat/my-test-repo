CREATE OR REPLACE TRIGGER "SCH_LINEITEM_AD0"
AFTER DELETE
ON SCH_LINEITEM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_LINEITEM', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_lineitem) || '|' ||
  TO_CHAR(:OLD.fk_bgtsection) || '|' ||
  :OLD.lineitem_desc || '|' ||
  :OLD.lineitem_sponsorunit || '|' ||
  :OLD.lineitem_clinicnofunit || '|' ||
  :OLD.lineitem_othercost || '|' ||
  :OLD.lineitem_delflag || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  :OLD.lineitem_name || '|' ||
  :OLD.lineitem_notes || '|' ||
  TO_CHAR(:OLD.lineitem_inpersec) || '|' ||
  TO_CHAR(:OLD.lineitem_stdcarecost) || '|' ||
  TO_CHAR(:OLD.lineitem_rescost) || '|' ||
  TO_CHAR(:OLD.lineitem_invcost) || '|' ||
  TO_CHAR(:OLD.lineitem_incostdisc) || '|' ||
  :OLD.lineitem_cptcode || '|' ||
  TO_CHAR(:OLD.lineitem_repeat) || '|' ||
  TO_CHAR(:OLD.lineitem_parentid) || '|' ||
  TO_CHAR(:OLD.lineitem_applyinfuture) || '|' ||
  TO_CHAR(:OLD.fk_codelst_category) || '|' ||
  :OLD.lineitem_tmid || '|' ||
  :OLD.lineitem_cdm || '|' ||
  :OLD.lineitem_applyindirects || '|' ||
  :OLD.lineitem_totalcost || '|' ||
  :OLD.lineitem_sponsoramount || '|' ||
  :OLD.lineitem_variance || '|' ||
  TO_CHAR(:OLD.FK_EVENT) ||'|'|| TO_CHAR(:OLD.lineitem_seq)
    ||'|'|| TO_CHAR(:OLD.fk_codelst_cost_type)
	||'|'|| TO_CHAR(:OLD.SUBCOST_ITEM_FLAG);

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid,dbms_lob.substr(deleted_data,4000,1));
END;
/


CREATE OR REPLACE TRIGGER SCH_LINEITEM_AU0
AFTER UPDATE
OF PK_LINEITEM,
FK_BGTSECTION,
LINEITEM_DESC,
LINEITEM_SPONSORUNIT,
LINEITEM_CLINICNOFUNIT,
LINEITEM_OTHERCOST,
LINEITEM_DELFLAG,
RID,
CREATOR,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
CREATED_ON,
IP_ADD,
LINEITEM_NAME,
LINEITEM_NOTES,
LINEITEM_INPERSEC,
LINEITEM_STDCARECOST,
LINEITEM_RESCOST,
LINEITEM_INVCOST,
LINEITEM_INCOSTDISC,
LINEITEM_CPTCODE,
LINEITEM_REPEAT,
LINEITEM_PARENTID,
LINEITEM_APPLYINFUTURE,
FK_CODELST_CATEGORY,
LINEITEM_TMID,
LINEITEM_CDM,
LINEITEM_APPLYINDIRECTS,
LINEITEM_TOTALCOST,
LINEITEM_SPONSORAMOUNT,
LINEITEM_VARIANCE,FK_EVENT,
lineitem_seq,
fk_codelst_cost_type,
SUBCOST_ITEM_FLAG
ON SCH_LINEITEM REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_LINEITEM', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_lineitem,0) !=
     NVL(:NEW.pk_lineitem,0) THEN
     audit_trail.column_update
       (raid, 'PK_LINEITEM',
       :OLD.pk_lineitem, :NEW.pk_lineitem);
  END IF;
  IF NVL(:OLD.fk_bgtsection,0) !=
     NVL(:NEW.fk_bgtsection,0) THEN
     audit_trail.column_update
       (raid, 'FK_BGTSECTION',
       :OLD.fk_bgtsection, :NEW.fk_bgtsection);
  END IF;
  IF NVL(:OLD.lineitem_desc,' ') !=
     NVL(:NEW.lineitem_desc,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_DESC',
       :OLD.lineitem_desc, :NEW.lineitem_desc);
  END IF;

  IF NVL(:OLD.lineitem_sponsorunit,' ') !=
     NVL(:NEW.lineitem_sponsorunit,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SPONSORUNIT',
       :OLD.lineitem_sponsorunit, :NEW.lineitem_sponsorunit);
  END IF;

  IF NVL(:OLD.lineitem_clinicnofunit,' ') !=
     NVL(:NEW.lineitem_clinicnofunit,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CLINICNOFUNIT',
       :OLD.lineitem_clinicnofunit, :NEW.lineitem_clinicnofunit);
  END IF;

  IF NVL(:OLD.lineitem_othercost,' ') !=
     NVL(:NEW.lineitem_othercost,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_OTHERCOST',
       :OLD.lineitem_othercost, :NEW.lineitem_othercost);
  END IF;


  IF NVL(:OLD.lineitem_delflag,' ') !=
     NVL(:NEW.lineitem_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_DELFLAG',
       :OLD.lineitem_delflag, :NEW.lineitem_delflag);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
                   to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  IF NVL(:OLD.lineitem_name,' ') !=
     NVL(:NEW.lineitem_name,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_NAME',
       :OLD.lineitem_name, :NEW.lineitem_name);
  END IF;

  IF NVL(:OLD.lineitem_notes,' ') !=
     NVL(:NEW.lineitem_notes,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_NOTES',
       :OLD.lineitem_notes, :NEW.lineitem_notes);
  END IF;
  IF NVL(:OLD.lineitem_inpersec,0) !=
     NVL(:NEW.lineitem_inpersec,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_inpersec',
       :OLD.lineitem_inpersec, :NEW.lineitem_inpersec);
  END IF;

  IF NVL(:OLD.lineitem_stdcarecost,0) !=
     NVL(:NEW.lineitem_stdcarecost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_STDCARECOST',
       :OLD.lineitem_stdcarecost, :NEW.lineitem_stdcarecost);
  END IF;
  IF NVL(:OLD.lineitem_rescost,0) !=
     NVL(:NEW.lineitem_rescost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_RESCOST',
       :OLD.lineitem_rescost, :NEW.lineitem_rescost);
  END IF;
  IF NVL(:OLD.lineitem_invcost,0) !=
     NVL(:NEW.lineitem_invcost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_INVCOST',
       :OLD.lineitem_invcost, :NEW.lineitem_invcost);
  END IF;
  IF NVL(:OLD.lineitem_incostdisc,0) !=
     NVL(:NEW.lineitem_incostdisc,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_INCOSTDISC',
       :OLD.lineitem_incostdisc, :NEW.lineitem_incostdisc);
  END IF;
  IF NVL(:OLD.lineitem_cptcode,' ') !=
     NVL(:NEW.lineitem_cptcode,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CPTCODE',
       :OLD.lineitem_cptcode, :NEW.lineitem_cptcode);
  END IF;
  IF NVL(:OLD.lineitem_repeat,0) !=
     NVL(:NEW.lineitem_repeat,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_REPEAT',
       :OLD.lineitem_repeat, :NEW.lineitem_repeat);
  END IF;
  IF NVL(:OLD.lineitem_parentid,0) !=
     NVL(:NEW.lineitem_parentid,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_PARENTID',
       :OLD.lineitem_parentid, :NEW.lineitem_parentid);
  END IF;
  IF NVL(:OLD.lineitem_applyinfuture,0) !=
     NVL(:NEW.lineitem_applyinfuture,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_APPLYINFUTURE',
       :OLD.lineitem_applyinfuture, :NEW.lineitem_applyinfuture);
  END IF;

  ----

  IF NVL(:OLD.fk_codelst_category,0) !=
     NVL(:NEW.fk_codelst_category,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_CATEGORY',
       :OLD.fk_codelst_category, :NEW.fk_codelst_category);
  END IF;

  IF NVL(:OLD.lineitem_tmid,' ') !=
     NVL(:NEW.lineitem_tmid,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_TMID',
       :OLD.lineitem_tmid, :NEW.lineitem_tmid);
  END IF;

  IF NVL(:OLD.lineitem_cdm,' ') !=
     NVL(:NEW.lineitem_cdm,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CDM',
       :OLD.lineitem_cdm, :NEW.lineitem_cdm);
  END IF;

  IF NVL(:OLD.lineitem_applyindirects,' ') !=
     NVL(:NEW.lineitem_applyindirects,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_APPLYINDIRECTS',
       :OLD.lineitem_applyindirects, :NEW.lineitem_applyindirects);
  END IF;

--JM: 02/16/2007
  IF NVL(:OLD.lineitem_totalcost,0) !=
     NVL(:NEW.lineitem_totalcost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_TOTALCOST',
       :OLD.lineitem_totalcost, :NEW.lineitem_totalcost);
  END IF;
  IF NVL(:OLD.lineitem_sponsoramount,0) !=
     NVL(:NEW.lineitem_sponsoramount,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SPONSORAMOUNT',
       :OLD.lineitem_sponsoramount, :NEW.lineitem_sponsoramount);
  END IF;
  IF NVL(:OLD.lineitem_variance,0) !=
     NVL(:NEW.lineitem_variance,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_VARIANCE',
       :OLD.lineitem_variance, :NEW.lineitem_variance);
  END IF;

  IF NVL(:OLD.fk_event,0) !=
     NVL(:NEW.fk_event,0) THEN
     audit_trail.column_update
       (raid, 'FK_EVENT',
       :OLD.fk_event, :NEW.fk_event);
  END IF;

  IF NVL(:OLD.lineitem_seq,0) !=
     NVL(:NEW.lineitem_seq,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SEQ',
       :OLD.lineitem_seq, :NEW.lineitem_seq);
  END IF;

IF NVL(:OLD.fk_codelst_cost_type,0) !=
     NVL(:NEW.fk_codelst_cost_type,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_COST_TYPE',
       :OLD.fk_codelst_cost_type, :NEW.fk_codelst_cost_type);
  END IF;

IF NVL(:OLD.SUBCOST_ITEM_FLAG,0) !=
     NVL(:NEW.SUBCOST_ITEM_FLAG,0) THEN
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_FLAG',
       :OLD.SUBCOST_ITEM_FLAG, :NEW.SUBCOST_ITEM_FLAG);
END IF;

END;
/


CREATE OR REPLACE TRIGGER "SCH_LINEITEM_BI0"
BEFORE INSERT
ON SCH_LINEITEM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  insert_data CLOB;
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_LINEITEM', erid, 'I', :NEW.CREATOR );


      --   Added by Ganapathy on 06/23/05 for Audit insert
      -- JM: Modified, 02/16/2007

   insert_data:= :NEW.FK_CODELST_CATEGORY||'|'||:NEW.LINEITEM_TMID||'|'||:NEW.LINEITEM_CDM||'|'||
         :NEW.LINEITEM_APPLYINDIRECTS||'|'|| :NEW.PK_LINEITEM||'|'|| :NEW.FK_BGTSECTION||'|'||
        :NEW.LINEITEM_DESC||'|'||:NEW.LINEITEM_SPONSORUNIT||'|'||:NEW.LINEITEM_CLINICNOFUNIT||'|'||
        :NEW.LINEITEM_OTHERCOST||'|'||:NEW.LINEITEM_DELFLAG||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||
        :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
        TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||:NEW.LINEITEM_NAME||'|'||
        :NEW.LINEITEM_NOTES||'|'|| :NEW.LINEITEM_INPERSEC||'|'||:NEW.LINEITEM_STDCARECOST||'|'||
        :NEW.LINEITEM_RESCOST||'|'||:NEW.LINEITEM_INVCOST||'|'|| :NEW.LINEITEM_INCOSTDISC||'|'||
        :NEW.LINEITEM_CPTCODE||'|'|| :NEW.LINEITEM_REPEAT||'|'|| :NEW.LINEITEM_PARENTID||'|'||
        :NEW.LINEITEM_APPLYINFUTURE ||'|'|| :NEW.LINEITEM_TOTALCOST||'|'|| :NEW.LINEITEM_SPONSORAMOUNT ||'|'|| :NEW.LINEITEM_VARIANCE
        ||'|'|| TO_CHAR(:NEW.FK_EVENT) ||'|'|| TO_CHAR(:NEW.lineitem_seq) ||'|'|| TO_CHAR(:NEW.fk_codelst_cost_type) ||'|'|| 
        TO_CHAR(:NEW.SUBCOST_ITEM_FLAG);

     INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, dbms_lob.substr(insert_data,4000,1));
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,108,5,'05_LineItem_Triggers.sql',sysdate,'8.10.0 Build#565');

commit;
