create or replace
TRIGGER "ERES"."ER_STORAGE_AU0" AFTER UPDATE ON ERES.ER_STORAGE FOR EACH ROW
DECLARE
  raid NUMBER(10);
  new_usr VARCHAR2(100);
  old_usr VARCHAR2(100);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
BEGIN
--Created by Manimaran for Audit Update
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);
  
 --KM-#3897 - To avoid insertion of Update records in audit_row table while handling clob column in new and edit mode 
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN   
    audit_trail.record_transaction
    (raid, 'ER_STORAGE', :OLD.rid, 'U', usr);
 end if;
  
  IF NVL(:OLD.PK_STORAGE,0) !=
     NVL(:NEW.PK_STORAGE,0) THEN
     audit_trail.column_update
       (raid, 'PK_STORAGE',
       :OLD.PK_STORAGE, :NEW.PK_STORAGE);
  END IF;

  IF NVL(:OLD.STORAGE_ID,' ') !=
     NVL(:NEW.STORAGE_ID,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_ID',
       :OLD.STORAGE_ID, :NEW.STORAGE_ID);
  END IF;

  IF NVL(:OLD.STORAGE_NAME,' ') !=
     NVL(:NEW.STORAGE_NAME,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_NAME',
       :OLD.STORAGE_NAME, :NEW.STORAGE_NAME);
  END IF;

  IF NVL(:OLD.FK_CODELST_STORAGE_TYPE,0) !=
     NVL(:NEW.FK_CODELST_STORAGE_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STORAGE_TYPE',
       :OLD.FK_CODELST_STORAGE_TYPE, :NEW.FK_CODELST_STORAGE_TYPE);
  END IF;

  IF NVL(:OLD.FK_STORAGE,0) !=
     NVL(:NEW.FK_STORAGE,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE',
       :OLD.FK_STORAGE, :NEW.FK_STORAGE);
  END IF;

  IF NVL(:OLD.STORAGE_CAP_NUMBER,0) !=
     NVL(:NEW.STORAGE_CAP_NUMBER,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_CAP_NUMBER',
       :OLD.STORAGE_CAP_NUMBER, :NEW.STORAGE_CAP_NUMBER);
  END IF;

  IF NVL(:OLD.FK_CODELST_CAPACITY_UNITS,0) !=
     NVL(:NEW.FK_CODELST_CAPACITY_UNITS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_CAPACITY_UNITS',
       :OLD.FK_CODELST_CAPACITY_UNITS, :NEW.FK_CODELST_CAPACITY_UNITS);
  END IF;

 --KM--Removal of storage_status column in the table.

 IF NVL(:OLD.STORAGE_DIM1_CELLNUM,0) !=
     NVL(:NEW.STORAGE_DIM1_CELLNUM,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM1_CELLNUM',
       :OLD.STORAGE_DIM1_CELLNUM, :NEW.STORAGE_DIM1_CELLNUM);
  END IF;

  IF NVL(:OLD.STORAGE_DIM1_NAMING,' ') !=
     NVL(:NEW.STORAGE_DIM1_NAMING,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM1_NAMING',
       :OLD.STORAGE_DIM1_NAMING, :NEW.STORAGE_DIM1_NAMING);
  END IF;

  IF NVL(:OLD.STORAGE_DIM1_ORDER,' ') !=
     NVL(:NEW.STORAGE_DIM1_ORDER,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM1_ORDER',
       :OLD.STORAGE_DIM1_ORDER, :NEW.STORAGE_DIM1_ORDER);
  END IF;

  IF NVL(:OLD.STORAGE_DIM2_CELLNUM,0) !=
     NVL(:NEW.STORAGE_DIM2_CELLNUM,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM2_CELLNUM',
       :OLD.STORAGE_DIM2_CELLNUM, :NEW.STORAGE_DIM2_CELLNUM);
  END IF;

  IF NVL(:OLD.STORAGE_DIM2_NAMING,' ') !=
     NVL(:NEW.STORAGE_DIM2_NAMING,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM2_NAMING',
       :OLD.STORAGE_DIM2_NAMING, :NEW.STORAGE_DIM2_NAMING);
  END IF;

  IF NVL(:OLD.STORAGE_DIM2_ORDER,' ') !=
     NVL(:NEW.STORAGE_DIM2_ORDER,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_DIM2_ORDER',
       :OLD.STORAGE_DIM2_ORDER, :NEW.STORAGE_DIM2_ORDER);
  END IF;

  IF NVL(:OLD.STORAGE_AVAIL_UNIT_COUNT,0) !=
     NVL(:NEW.STORAGE_AVAIL_UNIT_COUNT,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_AVAIL_UNIT_COUNT',
       :OLD.STORAGE_AVAIL_UNIT_COUNT, :NEW.STORAGE_AVAIL_UNIT_COUNT);
  END IF;

  IF NVL(:OLD.STORAGE_COORDINATE_X,0) !=
     NVL(:NEW.STORAGE_COORDINATE_X,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_COORDINATE_X',
       :OLD.STORAGE_COORDINATE_X, :NEW.STORAGE_COORDINATE_X);
  END IF;

  IF NVL(:OLD.STORAGE_COORDINATE_Y,0) !=
     NVL(:NEW.STORAGE_COORDINATE_Y,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_COORDINATE_Y',
       :OLD.STORAGE_COORDINATE_Y, :NEW.STORAGE_COORDINATE_Y);
  END IF;

  IF NVL(:OLD.STORAGE_LABEL,' ') !=
     NVL(:NEW.STORAGE_LABEL,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_LABEL',
       :OLD.STORAGE_LABEL, :NEW.STORAGE_LABEL);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       :OLD.last_modified_date, :NEW.last_modified_date);
  END IF;
  IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       :OLD.created_on, :NEW.created_on);
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  --Added by Manimaran on 092007
  if nvl(:old.FK_ACCOUNT,0) !=
     NVL(:new.FK_ACCOUNT,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.FK_ACCOUNT, :new.FK_ACCOUNT);
  end if;
  --Added by Manimaran on 040108
  IF NVL(:OLD.STORAGE_ISTEMPLATE,0) !=
     NVL(:NEW.STORAGE_ISTEMPLATE,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_ISTEMPLATE',
       :OLD.STORAGE_ISTEMPLATE, :NEW.STORAGE_ISTEMPLATE);
  END IF;

 IF NVL(:OLD.STORAGE_TEMPLATE_TYPE,0) !=
     NVL(:NEW.STORAGE_TEMPLATE_TYPE,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_TEMPLATE_TYPE',
       :OLD.STORAGE_TEMPLATE_TYPE, :NEW.STORAGE_TEMPLATE_TYPE);
  END IF;

  IF NVL(:OLD.STORAGE_ALTERNALEID,' ') !=
     NVL(:NEW.STORAGE_ALTERNALEID,' ') THEN
     audit_trail.column_update
       (raid, 'STORAGE_ALTERNALEID',
       :OLD.STORAGE_ALTERNALEID, :NEW.STORAGE_ALTERNALEID);
  END IF;


 IF NVL(:OLD.STORAGE_UNIT_CLASS,0) !=
     NVL(:NEW.STORAGE_UNIT_CLASS,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_UNIT_CLASS',
       :OLD.STORAGE_UNIT_CLASS, :NEW.STORAGE_UNIT_CLASS);
  END IF;

  IF NVL(:OLD.STORAGE_KIT_CATEGORY,0) !=
     NVL(:NEW.STORAGE_KIT_CATEGORY,0) THEN
     audit_trail.column_update
       (raid, 'STORAGE_KIT_CATEGORY',
       :OLD.STORAGE_KIT_CATEGORY, :NEW.STORAGE_KIT_CATEGORY);
  END IF;


END;
/

create or replace TRIGGER ERES.ER_STORAGE_STATUS_AU0 AFTER UPDATE ON ERES.ER_STORAGE_STATUS FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);
  --KM-08Jun10-#3897
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.record_transaction
      (raid, 'ER_STORAGE_STATUS', :OLD.rid, 'U', usr);
  end if;

  IF NVL(:OLD.PK_STORAGE_STATUS,0) !=
     NVL(:NEW.PK_STORAGE_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'PK_STORAGE_STATUS',
       :OLD.PK_STORAGE_STATUS, :NEW.PK_STORAGE_STATUS);
  END IF;

  IF NVL(:OLD.FK_STORAGE,0) !=
     NVL(:NEW.FK_STORAGE,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE',
       :OLD.FK_STORAGE, :NEW.FK_STORAGE);
  END IF;

  IF NVL(:OLD.SS_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.SS_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'SS_START_DATE',
       to_char(:OLD.SS_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.SS_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.FK_CODELST_STORAGE_STATUS,0) !=
     NVL(:NEW.FK_CODELST_STORAGE_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STORAGE_STATUS',
       :OLD.FK_CODELST_STORAGE_STATUS, :NEW.FK_CODELST_STORAGE_STATUS);
  END IF;

  --KM-3171--Removed Storage status notes

  IF NVL(:OLD.FK_USER,0) !=
     NVL(:NEW.FK_USER,0) THEN
     audit_trail.column_update
       (raid, 'FK_USER',
       :OLD.FK_USER, :NEW.FK_USER);
  END IF;

  IF NVL(:OLD.SS_TRACKING_NUMBER,' ') !=
     NVL(:NEW.SS_TRACKING_NUMBER,' ') THEN
     audit_trail.column_update
       (raid, 'SS_TRACKING_NUMBER',
       :OLD.SS_TRACKING_NUMBER, :NEW.SS_TRACKING_NUMBER);
  END IF;

  IF NVL(:OLD.FK_STUDY,0) !=
     NVL(:NEW.FK_STUDY,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.FK_STUDY, :NEW.FK_STUDY);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;
/

create or replace
TRIGGER ERES.ER_STUDY_AU0
AFTER UPDATE
ON ERES.ER_STUDY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

  raid NUMBER(10);

  old_codetype VARCHAR2(200) ;

  new_codetype VARCHAR2(200) ;

  old_modby VARCHAR2(100) ;

  new_modby VARCHAR2(100) ;

  old_site VARCHAR2(50) ;

  new_site VARCHAR2(50) ;

  old_account VARCHAR2(30) ;

  new_account VARCHAR2(30) ;

  usr VARCHAR2(100);

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;



  usr := Getuser(:NEW.last_modified_by);

  --KM-10Jun10-#4835
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.record_transaction (raid, 'ER_STUDY', :OLD.rid, 'U', usr);
  END IF;
 
 
  IF NVL(:OLD.pk_study,0) !=

     NVL(:NEW.pk_study,0) THEN

     Audit_Trail.column_update

       (raid, 'PK_STUDY',

       :OLD.pk_study, :NEW.pk_study);

  END IF;

  IF NVL(:OLD.study_sponsor,' ') !=

     NVL(:NEW.study_sponsor,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_SPONSOR',

       :OLD.study_sponsor, :NEW.study_sponsor);

  END IF;
-- Added By Rohit to Fix Bug No 4900
  IF NVL(:OLD.STUDY_SPONSORID,' ') !=

     NVL(:NEW.STUDY_SPONSORID,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_SPONSORID',

       :OLD.STUDY_SPONSORID, :NEW.STUDY_SPONSORID);

  END IF;

  IF NVL(:OLD.FK_CODELST_SPONSOR,0) !=

     NVL(:NEW.FK_CODELST_SPONSOR,0) THEN

     Audit_Trail.column_update

       (raid, 'FK_CODELST_SPONSOR',

       :OLD.FK_CODELST_SPONSOR, :NEW.FK_CODELST_SPONSOR);

  END IF;

  IF NVL(:OLD.fk_account,0) !=

     NVL(:NEW.fk_account,0) THEN

    BEGIN

     SELECT ac_name INTO old_account

     FROM ER_ACCOUNT WHERE pk_account = :OLD.fk_account ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_account := NULL ;

    END ;

    BEGIN

     SELECT ac_name INTO new_account

     FROM ER_ACCOUNT WHERE pk_account = :NEW.fk_account ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_account := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_ACCOUNT',

	old_account, new_account);

  END IF;

  IF NVL(:OLD.study_contact,' ') !=

     NVL(:NEW.study_contact,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_CONTACT',

       :OLD.study_contact, :NEW.study_contact);

  END IF;

  IF NVL(:OLD.study_pubflag,' ') !=

     NVL(:NEW.study_pubflag,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PUBFLAG',

       :OLD.study_pubflag, :NEW.study_pubflag);

  END IF;

  IF NVL(:OLD.study_title,' ') !=

     NVL(:NEW.study_title,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_TITLE',

       :OLD.study_title, :NEW.study_title);

  END IF;

  /*
  IF NVL(:OLD.study_obj,' ') !=

     NVL(:NEW.study_obj,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_OBJ',

       :OLD.study_obj, :NEW.study_obj);

  END IF;

  IF NVL(:OLD.study_sum,' ') !=

     NVL(:NEW.study_sum,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_SUM',

       :OLD.study_sum, :NEW.study_sum);

  END IF;

  */
  IF NVL(:OLD.study_prodname,' ') !=

     NVL(:NEW.study_prodname,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PRODNAME',

       :OLD.study_prodname, :NEW.study_prodname);

  END IF;

  IF NVL(:OLD.study_samplsize,0) !=

     NVL(:NEW.study_samplsize,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_SAMPLSIZE',

       :OLD.study_samplsize, :NEW.study_samplsize);

  END IF;

  IF NVL(:OLD.study_dur,0) !=

     NVL(:NEW.study_dur,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_DUR',

       :OLD.study_dur, :NEW.study_dur);

  END IF;

  IF NVL(:OLD.study_durunit,' ') !=

     NVL(:NEW.study_durunit,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_DURUNIT',

       :OLD.study_durunit, :NEW.study_durunit);

  END IF;

  IF NVL(:OLD.study_estbegindt,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=

     NVL(:NEW.study_estbegindt,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_ESTBEGINDT',

       to_char(:OLD.study_estbegindt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.study_estbegindt,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.study_actualdt,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=

     NVL(:NEW.study_actualdt,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_ACTUALDT',

       to_char(:OLD.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT),to_char(:NEW.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.study_prinv,' ') !=

     NVL(:NEW.study_prinv,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PRINV',

       :OLD.study_prinv, :NEW.study_prinv);

  END IF;

  IF NVL(:OLD.study_partcntr,' ') !=

     NVL(:NEW.study_partcntr,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PARTCNTR',

       :OLD.study_partcntr, :NEW.study_partcntr);

  END IF;

  IF NVL(:OLD.study_keywrds,' ') !=

     NVL(:NEW.study_keywrds,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_KEYWRDS',

       :OLD.study_keywrds, :NEW.study_keywrds);

  END IF;

  IF NVL(:OLD.study_pubcollst,' ') !=

     NVL(:NEW.study_pubcollst,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PUBCOLLST',

       :OLD.study_pubcollst, :NEW.study_pubcollst);

  END IF;

  IF NVL(:OLD.study_parentid,0) !=

     NVL(:NEW.study_parentid,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_PARENTID',

       :OLD.study_parentid, :NEW.study_parentid);

  END IF;

  IF NVL(:OLD.fk_author,0) !=

     NVL(:NEW.fk_author,0) THEN

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO old_modby

     FROM ER_USER

     WHERE pk_user = :OLD.fk_author ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_modby := NULL ;

    END ;

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO new_modby

     FROM ER_USER

     WHERE pk_user = :NEW.fk_author ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_modby := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_AUTHOR',

       old_modby, new_modby);

  END IF;

  IF NVL(:OLD.fk_codelst_tarea,0) !=

     NVL(:NEW.fk_codelst_tarea,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_tarea ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_tarea ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_TAREA',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_phase,0) !=

     NVL(:NEW.fk_codelst_phase,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_phase ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_phase ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_PHASE',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_restype,0) !=

     NVL(:NEW.fk_codelst_restype,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_restype ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_restype ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_RESTYPE',

       old_codetype, new_codetype);

  END IF;
  ------Added By Amarnadh for issue #3208 28th Nov '07
  IF NVL(:OLD.fk_codelst_scope,0) !=

     NVL(:NEW.fk_codelst_scope,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_scope ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_scope ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_SCOPE',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_type,0) !=

     NVL(:NEW.fk_codelst_type,0) THEN

   BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_type ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_type  ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_TYPE',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_blind,0) !=

     NVL(:NEW.fk_codelst_blind,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_blind ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_blind ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_BLIND',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.fk_codelst_random,0) !=

     NVL(:NEW.fk_codelst_random,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_random ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_random ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_RANDOM',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.rid,0) !=

     NVL(:NEW.rid,0) THEN

     Audit_Trail.column_update

       (raid, 'RID',

       :OLD.rid, :NEW.rid);

  END IF;

  IF NVL(:OLD.study_ver_no,' ') !=

     NVL(:NEW.study_ver_no,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_VER_NO',

       :OLD.study_ver_no, :NEW.study_ver_no);

  END IF;

  IF NVL(:OLD.study_current,' ') !=

     NVL(:NEW.study_current,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_CURRENT',

       :OLD.study_current, :NEW.study_current);

  END IF;

  IF NVL(:OLD.last_modified_by,0) !=

     NVL(:NEW.last_modified_by,0) THEN

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO old_modby

     FROM ER_USER

     WHERE pk_user = :OLD.last_modified_by ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_modby := NULL ;

    END ;

    BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname

     INTO new_modby

     FROM ER_USER

     WHERE pk_user = :NEW.last_modified_by ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_modby := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'LAST_MODIFIED_BY',

       old_modby, new_modby);

  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=

     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN

     Audit_Trail.column_update

       (raid, 'LAST_MODIFIED_DATE',

       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=

     NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN

     Audit_Trail.column_update

       (raid, 'CREATED_ON',

       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.study_number,' ') !=

     NVL(:NEW.study_number,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_NUMBER',

       :OLD.study_number, :NEW.study_number);

  END IF;

    IF NVL(:OLD.study_info,' ') !=

     NVL(:NEW.study_info,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_INFO',

       :OLD.study_info, :NEW.study_info);

  END IF;

  IF NVL(:OLD.IP_ADD,' ') !=

     NVL(:NEW.IP_ADD,' ') THEN

     Audit_Trail.column_update

       (raid, 'IP_ADD',

       :OLD.IP_ADD, :NEW.IP_ADD);

  END IF;

  IF NVL(:OLD.STUDY_VERPARENT,0) !=

     NVL(:NEW.STUDY_VERPARENT,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_VERPARENT',

       :OLD.STUDY_VERPARENT, :NEW.STUDY_VERPARENT);

  END IF;



  IF NVL(:OLD.study_end_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=

     NVL(:NEW.study_end_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_END_DATE',

       to_char(:OLD.study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.study_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

    IF NVL(:OLD.STUDY_DIVISION,0) !=

     NVL(:NEW.STUDY_DIVISION,0) THEN

    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype

	FROM ER_CODELST WHERE pk_codelst =  :OLD.STUDY_DIVISION ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL ;

    END ;

    BEGIN

	SELECT trim(codelst_desc) INTO new_codetype

	FROM ER_CODELST WHERE pk_codelst =  :NEW.STUDY_DIVISION ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL ;

    END ;

     Audit_Trail.column_update

       (raid, 'STUDY_DIVISION',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.STUDY_INVIND_FLAG,0) !=

     NVL(:NEW.STUDY_INVIND_FLAG,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_INVIND_FLAG',

       :OLD.STUDY_INVIND_FLAG, :NEW.STUDY_INVIND_FLAG);

  END IF;

 IF NVL(:OLD.STUDY_INVIND_NUMBER,' ') !=

     NVL(:NEW.STUDY_INVIND_NUMBER,' ') THEN

     Audit_Trail.column_update

       (raid, 'STUDY_INVIND_NUMBER',

       :OLD.STUDY_INVIND_NUMBER, :NEW.STUDY_INVIND_NUMBER);

  END IF;

  --JM:added 080808 for issue #3782

  IF NVL(:OLD.STUDY_ADVLKP_VER,0) !=

     NVL(:NEW.STUDY_ADVLKP_VER,0) THEN

     Audit_Trail.column_update

       (raid, 'STUDY_ADVLKP_VER',

       :OLD.STUDY_ADVLKP_VER, :NEW.STUDY_ADVLKP_VER);

  END IF;

  IF NVL(:OLD.STUDY_CREATION_TYPE,' ') !=
     NVL(:NEW.STUDY_CREATION_TYPE,' ') THEN
     Audit_Trail.column_update
       (raid, 'STUDY_CREATION_TYPE',
       :OLD.STUDY_CREATION_TYPE, :NEW.STUDY_CREATION_TYPE);
  END IF;

--Commented by Manimaran to fix the Bug 2743.
/* IF NVL(:OLD.study_obj_clob,' ') !=

     NVL(:NEW.study_obj_clob,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_OBJ_CLOB',

       dbms_lob.substr(:OLD.study_obj_clob,4000), dbms_lob.substr(:NEW.study_obj_clob,4000));

  END IF;

  IF NVL(:OLD.study_sum_clob,' ') !=

     NVL(:NEW.study_sum_clob,' ') THEN

     audit_trail.column_update

       (raid, 'STUDY_SUM_CLOB',

       dbms_lob.substr(:OLD.study_sum_clob,4000), dbms_lob.substr(:NEW.study_sum_clob,4000));

  END IF;
*/
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,76,7,'07_triggers.sql',sysdate,'8.9.0 Build#533');

commit;
