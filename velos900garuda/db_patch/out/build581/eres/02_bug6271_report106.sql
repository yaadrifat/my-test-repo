set define off;

Update er_report set rep_sql = 'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(trim(VISIT_INTERVAL),''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE,
EVENT_CPTCODE
,visit_win_before, visit_win_after
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,PK_PROTOCOL_VISIT,event_sequence' where pk_report = 106;

commit;

Update er_report set rep_sql_clob = 'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(trim(VISIT_INTERVAL),''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE,
EVENT_CPTCODE
,visit_win_before, visit_win_after
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,PK_PROTOCOL_VISIT,event_sequence' where pk_report = 106;

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,124,2,'02_bug6271_report106.sql',sysdate,'8.10.0 Build#581');

commit;