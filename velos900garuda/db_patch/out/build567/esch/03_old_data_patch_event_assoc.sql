Declare 


id_A_assoc number;
id_D_assoc number;
id_O_assoc number;
id_W_assoc number;
id_R_assoc number;

gen_id number;

Begin 
 


select pk_codelst into id_A_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='A';
select pk_codelst into id_D_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='D';
select pk_codelst into id_O_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='O';
select pk_codelst into id_W_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='W';
select pk_codelst into id_R_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='R';


For i in (select EVENT_ID, status from event_Assoc where UPPER(EVENT_TYPE)='P' and status is not null)
loop

	if    i.status = 'A' then gen_id:=id_A_assoc; 
	elsif i.status = 'D' then gen_id:=id_D_assoc; 
	elsif i.status = 'O' then gen_id:=id_O_assoc; 
	elsif i.status = 'W' then gen_id:=id_W_assoc;
	elsif i.status = 'R' then gen_id:=id_R_assoc;
	
	end if;

	update event_Assoc set fk_codelst_calstat = gen_id where EVENT_ID = i.EVENT_ID;
	

End loop;

commit;
End;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,3,'03_old_data_patch_event_assoc.sql',sysdate,'8.10.0 Build#567');

commit;