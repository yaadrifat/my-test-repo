alter table event_def add FK_CODELST_CALSTAT number;

Comment on column event_def.FK_CODELST_CALSTAT is 'this column stores the calendar status, pk_codelst of the sch_codelst table';


alter table event_assoc add FK_CODELST_CALSTAT number;

Comment on column event_assoc.FK_CODELST_CALSTAT is 'this column stores the calendar status, pk_codelst of the sch_codelst table';



alter table SCH_PROTSTAT add FK_CODELST_CALSTAT number;

Comment on column SCH_PROTSTAT.FK_CODELST_CALSTAT is 'this column stores the calendar status, pk_codelst of the sch_codelst table';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,1,'01_alter_tables.sql',sysdate,'8.10.0 Build#567');

commit;