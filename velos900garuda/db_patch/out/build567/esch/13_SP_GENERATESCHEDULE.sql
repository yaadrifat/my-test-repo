set define off;
create or replace
PROCEDURE        "SP_GENERATESCHEDULE" (
   P_PROTOCOL    IN   NUMBER,
   P_PATIENTID   IN   NUMBER,
   P_DATE        IN   DATE,
   P_PATPROTID   IN   NUMBER ,
   P_DAYS        IN   NUMBER ,
   P_DMLBY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2
)
AS
/*
This procedure will generate the schedule of a patient for a particular protocol.
The start date of the schedule is passed and the duration of the events is used to derive
the actual dates of the schedule

** Modification History
**
** Modified By         Date     	Remarks
** Charanjiv           29/Sept 	   Changed the call to Sp_addmessage to include the study id
** Charanjiv           06/Oct 	   now getting the SCH_EVENTS1.ISCONFIRMED values from sch_codelst
** Sonia               08/Nov      to save patprotid for a set of schedule
** JM:				   09FEB2011   #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS for EVENT_ASSOC table 
*/
   EVENT_ID           NUMBER;
   CHAIN_ID           NUMBER;
   EVENT_TYPE         CHAR (1);
   NAME               VARCHAR2 (50);
   NOTES              VARCHAR2 (1000);
   COST               NUMBER;
   COST_DESCRIPTION   VARCHAR2 (200);
   DURATION           NUMBER;
   USER_ID            NUMBER;
   LINKED_URI         VARCHAR2 (200);
   FUZZY_PERIOD       VARCHAR2 (10);
   MSG_TO             CHAR (1);   
   codeStatus         number; --JM://04FEB11
   DESCRIPTION        VARCHAR2 (200);
   DISPLACEMENT       NUMBER;
   ORG_ID             NUMBER;
   P_PRO_ID           CHAR (10);
   P_CURRVAL          CHAR (10);
   MY_DATE            DATE;
   v_study            number ;
   v_notdone number ;
   CURSOR C1
   is
      select EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
             COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
             FUZZY_PERIOD, MSG_TO, fk_codelst_calstat, DESCRIPTION, DISPLACEMENT,
             ORG_ID
        FROM EVENT_ASSOC
       WHERE CHAIN_ID = P_PROTOCOL
         and EVENT_TYPE = 'A'
         and DISPLACEMENT >= P_DAYS ;
BEGIN
 SELECT chain_id
   into v_study
   from event_assoc
  where event_id = p_protocol
    and event_type = 'P' ;

 select pk_codelst
   into v_notdone
   from sch_codelst
  where codelst_subtyp = 'ev_notdone'  ;

   OPEN C1;
   LOOP
      FETCH C1 INTO EVENT_ID,
                    CHAIN_ID,
                    EVENT_TYPE,
                    NAME,
                    NOTES,
                    COST,
                    COST_DESCRIPTION,
                    DURATION,
                    USER_ID,
                    LINKED_URI,
                    FUZZY_PERIOD,
                    MSG_TO,
                    codeStatus,
                    DESCRIPTION,
                    DISPLACEMENT,
                    ORG_ID;

      select lpad (to_char (SCH_EVENTS1_SEQ1.NEXTVAL), 10, '0')
        into P_CURRVAL
        from DUAL;

      P_PRO_ID := lpad (to_char (P_PROTOCOL), 10, '0');
      MY_DATE := P_DATE + DISPLACEMENT - 1;
      EXIT WHEN C1%NOTFOUND;

      INSERT INTO SCH_EVENTS1
                  (
                                 EVENT_ID,
                                 LOCATION_ID,
                                 NOTES,
                                 DESCRIPTION,
                                 BOOKED_BY,
                                 START_DATE_TIME,
                                 END_DATE_TIME,
                                 PATIENT_ID,
                                 OBJECT_ID,
                                 STATUS,
                                 TYPE_ID,
                                 SESSION_ID,
                                 VISIT_TYPE_ID,
                                 FK_ASSOC,
                                 ISCONFIRMED,
                                 BOOKEDON,
						   CREATOR ,
                                 LAST_MODIFIED_BY       ,
                                 LAST_MODIFIED_DATE     ,
                                 CREATED_ON             ,
                                 IP_ADD           ,
      					   FK_PATPROT
                  )
           VALUES(
              P_CURRVAL,
              '173',
              DESCRIPTION,
              NAME,
              '0000000525',
              MY_DATE,
              MY_DATE + DURATION,
              lpad (to_char (P_PATIENTID), 10, '0'),
              '0000001476',
              0,
              '165',
              lpad (to_char (P_PROTOCOL), 10, '0'),
              FUZZY_PERIOD,
              EVENT_ID,
              v_notdone,
              P_DATE,
		    P_DMLBY ,
		    P_DMLBY,
		    SYSDATE ,
		    SYSDATE ,
		    P_IPADD,
		    P_PATPROTID
           );

    SP_ADDMESSAGE (EVENT_ID, P_PATIENTID, MY_DATE, MSG_TO, P_CURRVAL,v_study,P_DMLBY, P_IPADD);
   END LOOP;

   CLOSE C1;
/**********************************
TEST
set serveroutput on
declare
newProtId number ;
begin
sp_addprottostudy(657,123,newProtId) ;
dbms_output.put_line(to_char(newProtId) ) ;
end ;
**************************************/

   COMMIT;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,13,'13_SP_GENERATESCHEDULE.sql',sysdate,'8.10.0 Build#567');

commit;

