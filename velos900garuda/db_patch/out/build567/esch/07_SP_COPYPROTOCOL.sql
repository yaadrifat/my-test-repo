set define off;
create or replace
PROCEDURE             "SP_COPYPROTOCOL" (
   P_PROTOCOL      IN       NUMBER,
   P_NAME          IN       VARCHAR,
   p_cal_type	   IN	    NUMBER,--JM:
   P_NEWPROTOCOL   OUT      NUMBER,
   P_USER          IN       VARCHAR,
   P_IPADD         IN       VARCHAR
)
AS
/****************************************************************************************************
** Procedure to copy a protocol with a different name

** user can copy an existing protocol with a different name

** Parameter Description

** p_protocol     event id of the protocol that is to be copied
** P_name         new name of the protocol
** P_newProtocol  protocol id of the new copied protocol

**
** Author: Dinesh Khurana 15th June 2001
**
** Modification History
**
** Modified By		Date		Remarks
** Dinesh			26June		(1)To add events attributes along with event details
								(2) To return status of new protocol as 'Work in Progress'
** SamV				Sep092004	added code to handle sharedwith option for Calendar.
** Sam V			Sep292004	Added code to copy Duration Unit, Calendar SharedWith
** Sonia Abrol					copy new fields in event details
** JM				21May2007	p_cal_type calendar type, the one user has selected from the GUI, it may be same or different than the existing one
** Manimaran 					new field in copy event details
** Sammie			30Apr2010	new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
** Bikash			28jul2010	new columns COVERAGE_NOTES
****************************************************************************************************************************************************
*/
EVENT_ID           NUMBER;
CHAIN_ID           NUMBER;
EVENT_TYPE         CHAR (1);
NAME               VARCHAR2 (4000); --KM
NOTES              VARCHAR2 (1000);
COST               NUMBER;
COST_DESCRIPTION   VARCHAR2 (200);
DURATION           NUMBER;
USER_ID            NUMBER;
LINKED_URI         VARCHAR2 (200);
FUZZY_PERIOD       VARCHAR2 (10);
MSG_TO             CHAR (1);
FK_CODELST_CALSTAT NUMBER; --KM, 02/03/11

DESCRIPTION        VARCHAR2 (1000);
DISPLACEMENT       NUMBER;
EVENTMSG           VARCHAR2 (2000);
EVENTRES           CHAR (1);
EVENTFLAG          NUMBER;

PAT_DAYSBEFORE     NUMBER (3);
PAT_DAYSAFTER      NUMBER (3);
USR_DAYSBEFORE     NUMBER (3);
USR_DAYSAFTER      NUMBER (3);
PAT_MSGBEFORE      VARCHAR2 (4000);
PAT_MSGAFTER       VARCHAR2 (4000);
USR_MSGBEFORE      VARCHAR2 (4000);
USR_MSGAFTER       VARCHAR2 (4000);

ORG_ID             NUMBER;
P_CHAIN_ID         NUMBER;
P_CURRVAL          NUMBER;
P_COUNT            NUMBER;
P_USER_ID          NUMBER;
P_STATUS           NUMBER;
V_DURATION_UNIT	  CHAR(1); --SV, 9/29
V_CALENDAR_SHAREDWITH  CHAR(1); --SV, 9/29

V_CAL_SHAREWITH CHAR(1); -- RK, 01/28/05
v_objectids VARCHAR2(1000);

V_EVENT_VISIT_ID	  NUMBER; --SV, 9/30
V_RET			  NUMBER;

V_CATLIB NUMBER;

v_visit_num NUMBER;

v_event_fuzzyafter VARCHAR2(10);
v_event_durationafter CHAR(1);
v_event_cptcode  VARCHAR2(255);
v_event_durationbefore CHAR(1);
event_category varchar2(100); --KM,4/18
event_sequence number(22);--KM

v_event_library_type Number;
v_event_line_category Number;
v_SERVICE_SITE_ID Number;
v_FACILITY_ID Number;
v_FK_CODELST_COVERTYPE Number;

v_new_protocolPK Number;
V_COVERAGE_NOTES VARCHAR2(4000); --BK 28JUL2010 SWFIN5b

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_copyprotocol', pLEVEL  => Plog.LDEBUG);

-- The new protocol id generated from the EVENT_DEFINIITON_SEQ sequence. This is stored in the
-- p_newProtocol
-- This variable is an OUT parameter
--KM-041808
  /*KM - DFIN#9 */
	CURSOR C1
	IS
		SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES, COST,
		COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,
		FUZZY_PERIOD, MSG_TO, FK_CODELST_CALSTAT, DESCRIPTION, DISPLACEMENT,
		ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
		PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
		PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, CALENDAR_SHAREDWITH, DURATION_UNIT, FK_VISIT,FK_CATLIB,
		event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore,event_category,event_sequence,
		event_library_type,event_line_category,
		SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
		FROM EVENT_DEF
		WHERE CHAIN_ID = P_PROTOCOL
		ORDER BY EVENT_TYPE DESC, COST, DISPLACEMENT;
	BEGIN

	-- to check if the protocol name already exists in the user Protocol library
	SELECT USER_ID
	INTO P_USER_ID
	FROM EVENT_DEF
	WHERE EVENT_ID = P_PROTOCOL;
	SELECT COUNT (EVENT_ID)
	INTO P_COUNT
	FROM EVENT_DEF
	WHERE LOWER(trim(NAME)) = LOWER(trim(P_NAME)) --KM--to fix the Bug1756
	AND USER_ID = P_USER_ID
	AND EVENT_TYPE = 'P';

	IF P_COUNT > 0 THEN
		P_NEWPROTOCOL := -1;
		RETURN;
	END IF;

	OPEN C1;

	LOOP
		FETCH C1 INTO EVENT_ID,
		CHAIN_ID,
		EVENT_TYPE,
		NAME,
		NOTES,
		COST,
		COST_DESCRIPTION,
		DURATION,
		USER_ID,
		LINKED_URI,
		FUZZY_PERIOD,
		MSG_TO,
		FK_CODELST_CALSTAT,
		DESCRIPTION,
		DISPLACEMENT,
		ORG_ID,
		EVENTMSG,
		EVENTRES,
		EVENTFLAG,
		PAT_DAYSBEFORE,
		PAT_DAYSAFTER,
		USR_DAYSBEFORE,
		USR_DAYSAFTER,
		PAT_MSGBEFORE,
		PAT_MSGAFTER,
		USR_MSGBEFORE,
		USR_MSGAFTER,
		V_CALENDAR_SHAREDWITH,
		V_DURATION_UNIT, V_EVENT_VISIT_ID,V_CATLIB,
		v_event_fuzzyafter, v_event_durationafter, v_event_cptcode,v_event_durationbefore,event_category,event_sequence,
		v_event_library_type,v_event_line_category,
		v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,V_COVERAGE_NOTES;

		EXIT WHEN C1%NOTFOUND;

		SELECT EVENT_DEFINITION_SEQ.NEXTVAL
		INTO P_CURRVAL
		FROM DUAL;

		IF C1%ROWCOUNT = 1 THEN
			/*
			Name for the Protocol as passed in the procedure, the name we get from the cursor will be the old
			name. Status 'W' for the Protocol and from 2nd row onwards events will have their own status
			P_Chain_Id will remain the same for all the records inserted ie the id of the first protocol
			The new protocol is is returned from the procedure.
			*/
			NAME := P_NAME;
			V_CATLIB := p_cal_type;--JM:
      /* KM - DFIN#9 */
      select pk_codelst into fk_codelst_calstat from sch_codelst where codelst_type = 'calStatLib' and trim(upper(codelst_subtyp)) = 'W';
			P_CHAIN_ID := P_CURRVAL;
			P_NEWPROTOCOL := P_CHAIN_ID;

			v_new_protocolPK := P_CURRVAL;

			V_CAL_SHAREWITH := V_CALENDAR_SHAREDWITH;

			FOR i IN (SELECT fk_objectshare_id
				FROM er_objectshare
				WHERE fk_object = p_protocol AND
				objectshare_type = V_CAL_SHAREWITH
				)
			LOOP
				v_objectids := v_objectids || i.fk_objectshare_id || ',';
			END LOOP;
			v_objectids := SUBSTR(v_objectids,1,LENGTH(v_objectids)-1);
			Sp_Objectsharewith( P_NEWPROTOCOL,'3', TO_CHAR(v_objectids)  , V_CAL_SHAREWITH, TO_CHAR(p_user) , P_IPADD , v_ret, 'N' ); --SV, 9/29/04, cal-enh-01. 'N' at the end is to avoid commits inside proc.

		END IF;
		--event id of the the protocol generated is the chain id of all the child events
plog.DEBUG(pCTX,'EVENTID IS'||EVENT_ID||V_COVERAGE_NOTES);
		INSERT INTO EVENT_DEF(
			EVENT_ID,
			CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,
			FK_CODELST_CALSTAT,
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID,
			EVENT_MSG,
			EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER,
			USR_DAYSBEFORE,
			USR_DAYSAFTER,
			PAT_MSGBEFORE,
			PAT_MSGAFTER,
			USR_MSGBEFORE,
			USR_MSGAFTER, CREATOR,IP_ADD,
			CALENDAR_SHAREDWITH,
			DURATION_UNIT,FK_CATLIB,
			event_fuzzyafter, event_durationafter, event_cptcode,event_durationbefore,event_category,event_sequence,
			event_library_type,event_line_category,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES
		) VALUES(
			P_CURRVAL,
			P_CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,
			FK_CODELST_CALSTAT, --KM 02/03/11
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID,
			EVENTMSG,
			EVENTRES,
			EVENTFLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER,
			USR_DAYSBEFORE,
			USR_DAYSAFTER,
			PAT_MSGBEFORE,
			PAT_MSGAFTER,
			USR_MSGBEFORE,
			USR_MSGAFTER,TO_CHAR(p_user),P_IPADD,
			V_CALENDAR_SHAREDWITH,
			V_DURATION_UNIT,V_CATLIB,
			v_event_fuzzyafter, v_event_durationafter, v_event_cptcode,v_event_durationbefore,event_category,event_sequence,
			v_event_library_type,v_event_line_category,
			v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,V_COVERAGE_NOTES
		);

		IF C1%ROWCOUNT = 1 THEN
			--copy all visits
			Sp_Copy_Protocol_Visits(P_PROTOCOL, P_NEWPROTOCOL, P_USER, P_IPADD, V_RET); --SV, 09/30, copy "all" the visits from old to new calendar.
		end if;

		IF C1%ROWCOUNT != 1 THEN

			-- SV, 9/30, copy the visit for the event to the new protocol calendar and update the event record with the visit id. Then update the new event record with new visit id.
			IF (v_event_visit_id > 0) THEN

				--Get the visit no to differentiate between 2 events with same displacement
				SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT  WHERE pk_protocol_visit=v_event_visit_id;

				--plog.DEBUG(pCTX,'visit_no'||v_visit_num||'Visit'||v_event_visit_id);

				Sp_Get_Prot_Visit(P_NEWPROTOCOL,DISPLACEMENT, P_USER, P_IPADD, v_ret,v_visit_num);
				IF (v_ret > 0) THEN
					UPDATE EVENT_DEF
					SET FK_VISIT = v_ret
					WHERE EVENT_ID = P_CURRVAL;

					-- plog.DEBUG(pCTX,'v_ret-new fk_visit'||v_ret);
				END IF;
			END IF;
			/*
			call the cpevedtls procedure to copy the details of the events associated with the protocol.
			*/
			Sp_Cpevedtls (EVENT_ID, P_CURRVAL,'C', P_STATUS, P_USER, P_IPADD,0);

		END IF;
	END LOOP;
	CLOSE C1;
	/**********************************
	TEST
	set serveroutput on
	declare
	myevent_id number ;

	begin
	sp_copyprotocol(5128,'checkup1',myevent_id) ;
	dbms_output.put_line(to_char(myevent_id) ) ;
	end ;
	**************************************/

	--SONIA , apparently P_NEWPROTOCOL was getting reset somewhere,set it again

	P_NEWPROTOCOL := v_new_protocolPK ;
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,7,'07_SP_COPYPROTOCOL.sql',sysdate,'8.10.0 Build#567');

commit;
