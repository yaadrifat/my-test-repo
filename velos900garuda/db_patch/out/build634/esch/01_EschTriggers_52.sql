create or replace
TRIGGER "ESCH"."EVENT_ASSOC_AU0" AFTER UPDATE ON ESCH.EVENT_ASSOC FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);

BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);

	audit_trail.record_transaction
		(raid, 'EVENT_ASSOC', :OLD.rid, 'U', usr);

	IF NVL(:OLD.event_id,0) !=
		NVL(:NEW.event_id,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_ID', :OLD.event_id, :NEW.event_id);
	END IF;
	IF NVL(:OLD.chain_id,0) !=
		NVL(:NEW.chain_id,0) THEN
		audit_trail.column_update
		(raid, 'CHAIN_ID', :OLD.chain_id, :NEW.chain_id);
	END IF;
	IF NVL(:OLD.event_type,' ') !=
		NVL(:NEW.event_type,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_TYPE', :OLD.event_type, :NEW.event_type);
	END IF;
	IF NVL(:OLD.NAME,' ') !=
		NVL(:NEW.NAME,' ') THEN
		audit_trail.column_update
		(raid, 'NAME', :OLD.NAME, :NEW.NAME);
	END IF;
	IF NVL(:OLD.notes,' ') !=
		NVL(:NEW.notes,' ') THEN
		audit_trail.column_update
		(raid, 'NOTES', :OLD.notes, :NEW.notes);
	END IF;
	IF NVL(:OLD.COST,0) !=
		NVL(:NEW.COST,0) THEN
		audit_trail.column_update
		(raid, 'COST', :OLD.COST, :NEW.COST);
	END IF;
	IF NVL(:OLD.cost_description,' ') !=
		NVL(:NEW.cost_description,' ') THEN
		audit_trail.column_update
		(raid, 'COST_DESCRIPTION', :OLD.cost_description, :NEW.cost_description);
	END IF;
	IF NVL(:OLD.duration,0) !=
		NVL(:NEW.duration,0) THEN
		audit_trail.column_update
		(raid, 'DURATION', :OLD.duration, :NEW.duration);
	END IF;
	IF NVL(:OLD.user_id,0) !=
		NVL(:NEW.user_id,0) THEN
		audit_trail.column_update
		(raid, 'USER_ID', :OLD.user_id, :NEW.user_id);
	END IF;
	IF NVL(:OLD.linked_uri,' ') !=
		NVL(:NEW.linked_uri,' ') THEN
		audit_trail.column_update
		(raid, 'LINKED_URI', :OLD.linked_uri, :NEW.linked_uri);
	END IF;
	IF NVL(:OLD.fuzzy_period,' ') !=
		NVL(:NEW.fuzzy_period,' ') THEN
		audit_trail.column_update
		(raid, 'FUZZY_PERIOD', :OLD.fuzzy_period, :NEW.fuzzy_period);
	END IF;
	IF NVL(:OLD.msg_to,' ') !=
		NVL(:NEW.msg_to,' ') THEN
		audit_trail.column_update
		(raid, 'MSG_TO', :OLD.msg_to, :NEW.msg_to);
	END IF;
	/*IF NVL(:OLD.status,' ') !=
		NVL(:NEW.status,' ') THEN
		audit_trail.column_update
		(raid, 'STATUS', :OLD.status, :NEW.status);
	END IF;*/
	IF NVL(:OLD.description,' ') !=
		NVL(:NEW.description,' ') THEN
		audit_trail.column_update
		(raid, 'DESCRIPTION', :OLD.description, :NEW.description);
	END IF;
	IF NVL(:OLD.displacement,0) !=
		NVL(:NEW.displacement,0) THEN
		audit_trail.column_update
		(raid, 'DISPLACEMENT', :OLD.displacement, :NEW.displacement);
	END IF;
	IF NVL(:OLD.org_id,0) !=
		NVL(:NEW.org_id,0) THEN
		audit_trail.column_update
		(raid, 'ORG_ID', :OLD.org_id, :NEW.org_id);
	END IF;

	--JM: added

	IF NVL(:OLD.orig_cal,0) !=
		NVL(:NEW.orig_cal,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_CAL', :OLD.orig_cal, :NEW.orig_cal);
	END IF;

	IF NVL(:OLD.orig_event,0) !=
		NVL(:NEW.orig_event,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_EVENT', :OLD.orig_event, :NEW.orig_event);
	END IF;

	IF NVL(:OLD.orig_study,0) !=
		NVL(:NEW.orig_study,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_STUDY', :OLD.orig_study, :NEW.orig_study);
	END IF;
	--

	IF NVL(:OLD.event_msg,' ') !=
		NVL(:NEW.event_msg,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_MSG', :OLD.event_msg, :NEW.event_msg);
	END IF;
	IF NVL(:OLD.event_res,' ') !=
		NVL(:NEW.event_res,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_RES', :OLD.event_res, :NEW.event_res);
	END IF;

	IF NVL(:OLD.event_flag,0) !=
		NVL(:NEW.event_flag,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_FLAG', :OLD.event_flag, :NEW.event_flag);
	END IF;
	IF NVL(:OLD.pat_daysbefore,0) !=
		NVL(:NEW.pat_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSBEFORE', :OLD.pat_daysbefore, :NEW.pat_daysbefore);
	END IF;
	IF NVL(:OLD.pat_daysafter,0) !=
		NVL(:NEW.pat_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSAFTER', :OLD.pat_daysafter, :NEW.pat_daysafter);
	END IF;
	IF NVL(:OLD.usr_daysbefore,0) !=
		NVL(:NEW.usr_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSBEFORE', :OLD.usr_daysbefore, :NEW.usr_daysbefore);
	END IF;
	IF NVL(:OLD.usr_daysafter,0) !=
		NVL(:NEW.usr_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSAFTER', :OLD.usr_daysafter, :NEW.usr_daysafter);
	END IF;
	IF NVL(:OLD.pat_msgbefore,' ') !=
		NVL(:NEW.pat_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGBEFORE', :OLD.pat_msgbefore, :NEW.pat_msgbefore);
	END IF;
	IF NVL(:OLD.pat_msgafter,' ') !=
		NVL(:NEW.pat_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGAFTER', :OLD.pat_msgafter, :NEW.pat_msgafter);
	END IF;
	IF NVL(:OLD.usr_msgbefore,' ') !=
		NVL(:NEW.usr_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGBEFORE', :OLD.usr_msgbefore, :NEW.usr_msgbefore);
	END IF;
	IF NVL(:OLD.usr_msgafter,' ') !=
		NVL(:NEW.usr_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGAFTER', :OLD.usr_msgafter, :NEW.usr_msgafter);
	END IF;
	IF NVL(:OLD.status_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
		NVL(:NEW.status_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
		audit_trail.column_update
		(raid, 'STATUS_DT', to_char(:old.status_dt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.status_dt, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.status_changeby,0) !=
		NVL(:NEW.status_changeby,0) THEN
		audit_trail.column_update
		(raid, 'STATUS_CHANGEBY', :OLD.status_changeby, :NEW.status_changeby);
	END IF;
	IF NVL(:OLD.rid,0) !=
		NVL(:NEW.rid,0) THEN
		audit_trail.column_update
		(raid, 'RID',:OLD.rid, :NEW.rid);
	END IF;
	IF NVL(:OLD.last_modified_by,0) !=
		NVL(:NEW.last_modified_by,0) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_BY', :OLD.last_modified_by, :NEW.last_modified_by);
	END IF;
	IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
		NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_DATE', to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.ip_add,' ') !=
		NVL(:NEW.ip_add,' ') THEN
		audit_trail.column_update
		(raid, 'IP_ADD', :OLD.ip_add, :NEW.ip_add);
	END IF;
	IF NVL(:OLD.duration_unit,' ') !=
		NVL(:NEW.duration_unit,' ') THEN
		audit_trail.column_update
		(raid, 'DURATION_UNIT', :OLD.duration_unit, :NEW.duration_unit);
	END IF;
	IF NVL(:OLD.fk_visit,0) !=
		NVL(:NEW.fk_visit,0) THEN
		audit_trail.column_update
		(raid, 'FK_VISIT', :OLD.fk_visit, :NEW.fk_visit);
	END IF;
	IF NVL(:OLD.event_cptcode,' ') !=
		NVL(:NEW.event_cptcode,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CPTCODE', :OLD.event_cptcode, :NEW.event_cptcode);
	END IF;
	IF NVL(:OLD.event_fuzzyafter,' ') !=
		NVL(:NEW.event_fuzzyafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_FUZZYAFTER', :OLD.event_fuzzyafter, :NEW.event_fuzzyafter);
	END IF;
	IF NVL(:OLD.event_durationafter,' ') !=
		NVL(:NEW.event_durationafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONAFTER', :OLD.event_durationafter, :NEW.event_durationafter);
	END IF;
	IF NVL(:OLD.event_durationbefore,' ') !=
		NVL(:NEW.event_durationbefore,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONBEFORE', :OLD.event_durationbefore, :NEW.event_durationbefore);
	END IF;
	--JM : changed    IF NVL(:OLD.fk_catlib,' ') !=
	IF NVL(:OLD.fk_catlib,0) !=
		NVL(:NEW.fk_catlib,0) THEN
		audit_trail.column_update
		(raid, 'FK_CATLIB', :OLD.fk_catlib, :NEW.fk_catlib);
	END IF;
	IF NVL(:OLD.event_calassocto,' ') !=
		NVL(:NEW.event_calassocto,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CALASSOCTO', :OLD.event_calassocto, :NEW.event_calassocto);
	END IF;
	--JM: IF NVL(:OLD.event_calschdate,' ') !=
	IF NVL(:OLD.event_calschdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
		NVL(:NEW.event_calschdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
		audit_trail.column_update
		(raid, 'EVENT_CALSCHDATE', to_char(:old.event_calschdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.event_calschdate, PKG_DATEUTIL.F_GET_DATEFORMAT) );
	END IF;

	--KM-28Mar08
	IF NVL(:OLD.EVENT_CATEGORY,' ') !=
		NVL(:NEW.EVENT_CATEGORY,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CATEGORY', :OLD.EVENT_CATEGORY, :NEW.EVENT_CATEGORY);
	END IF;

	-- KM - 21Aug09
	IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) !=
		NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LIBRARY_TYPE',:OLD.EVENT_LIBRARY_TYPE, :NEW.EVENT_LIBRARY_TYPE);
	END IF;

	IF NVL(:OLD.EVENT_LINE_CATEGORY,0) !=
		NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LINE_CATEGORY',:OLD.EVENT_LINE_CATEGORY, :NEW.EVENT_LINE_CATEGORY);
	END IF;
---------------------------------------
	IF NVL(:OLD.SERVICE_SITE_ID,0) !=
		NVL(:NEW.SERVICE_SITE_ID,0) THEN
		audit_trail.column_update
		(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
	END IF;

	IF NVL(:OLD.FACILITY_ID,0) !=
		NVL(:NEW.FACILITY_ID,0) THEN
		audit_trail.column_update
		(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
	END IF;

	IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
		NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
	END IF;  
  IF NVL(:OLD.COVERAGE_NOTES,0) !=                      --BK 23rd july 2010
		NVL(:NEW.COVERAGE_NOTES,0) THEN
		audit_trail.column_update
		(raid, 'COVERAGE_NOTES',:OLD.COVERAGE_NOTES, :NEW.COVERAGE_NOTES);
	END IF;
	--JM: 07FEB2011, #D-FIN9
	IF NVL(:OLD.FK_CODELST_CALSTAT,0) !=
		NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_CALSTAT', :OLD.FK_CODELST_CALSTAT, :NEW.FK_CODELST_CALSTAT);
	END IF;
END;
/
--When EVENT_ASSOC table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AU2"
AFTER UPDATE ON ESCH.EVENT_ASSOC 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);   /*variable used to fetch value of sequence */
	v_mouduleId number(10);
	V_EVENT_LIBRARY_TYPE_OLD VARCHAR2(200) :=NULL;
	V_EVENT_LINE_CATEGORY_OLD VARCHAR2(200) :=NULL;
	V_FK_CODELST_COVERTYPE_OLD VARCHAR2(200) :=NULL;
	V_FK_CODELST_CALSTAT_OLD VARCHAR2(200) :=NULL;
	V_EVENT_LIBRARY_TYPE_NEW VARCHAR2(200) :=NULL;
	V_EVENT_LINE_CATEGORY_NEW VARCHAR2(200) :=NULL;
	V_FK_CODELST_COVERTYPE_NEW VARCHAR2(200) :=NULL;
	V_FK_CODELST_CALSTAT_NEW VARCHAR2(200) :=NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    BEGIN
    IF (:OLD.EVENT_TYPE= 'P') THEN
    v_mouduleId := :OLD.EVENT_ID;
    ELSIF (:OLD.EVENT_TYPE= 'A') THEN
    v_mouduleId := :OLD.CHAIN_ID;
    END IF;
    END;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on EVENT_ASSOC
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'EVENT_ASSOC',:OLD.RID,v_mouduleId,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.EVENT_ID,0) != NVL(:NEW.EVENT_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_ID',:OLD.EVENT_ID,:NEW.EVENT_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CHAIN_ID,0) != NVL(:NEW.CHAIN_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','CHAIN_ID',:OLD.CHAIN_ID,:NEW.CHAIN_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_TYPE,' ') != NVL(:NEW.EVENT_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_TYPE',:OLD.EVENT_TYPE,:NEW.EVENT_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.NAME,' ') != NVL(:NEW.NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','NAME',:OLD.NAME,:NEW.NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.NOTES,' ') != NVL(:NEW.NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','NOTES',:OLD.NOTES,:NEW.NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.COST,0) != NVL(:NEW.COST,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','COST',:OLD.COST,:NEW.COST,NULL,NULL);
    END IF;
    IF NVL(:OLD.COST_DESCRIPTION,' ') != NVL(:NEW.COST_DESCRIPTION,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','COST_DESCRIPTION',:OLD.COST_DESCRIPTION,:NEW.COST_DESCRIPTION,NULL,NULL);
    END IF;
    IF NVL(:OLD.DURATION,0) != NVL(:NEW.DURATION,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','DURATION',:OLD.DURATION,:NEW.DURATION,NULL,NULL);
    END IF;
    IF NVL(:OLD.USER_ID,0) != NVL(:NEW.USER_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','USER_ID',:OLD.USER_ID,:NEW.USER_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.LINKED_URI,' ') != NVL(:NEW.LINKED_URI,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','LINKED_URI',:OLD.LINKED_URI,:NEW.LINKED_URI,NULL,NULL);
    END IF;
    IF NVL(:OLD.FUZZY_PERIOD,' ') != NVL(:NEW.FUZZY_PERIOD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','FUZZY_PERIOD',:OLD.FUZZY_PERIOD,:NEW.FUZZY_PERIOD,NULL,NULL);
    END IF;
    IF NVL(:OLD.MSG_TO,' ') != NVL(:NEW.MSG_TO,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','MSG_TO',:OLD.MSG_TO,:NEW.MSG_TO,NULL,NULL);
    END IF;
    IF NVL(:OLD.STATUS,' ') != NVL(:NEW.STATUS,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','STATUS',:OLD.STATUS,:NEW.STATUS,NULL,NULL);
    END IF;
    IF NVL(:OLD.DESCRIPTION,' ') != NVL(:NEW.DESCRIPTION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','DESCRIPTION',:OLD.DESCRIPTION,:NEW.DESCRIPTION,NULL,NULL);
    END IF;
    IF NVL(:OLD.DISPLACEMENT,0) != NVL(:NEW.DISPLACEMENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','DISPLACEMENT',:OLD.DISPLACEMENT,:NEW.DISPLACEMENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.ORG_ID,0) != NVL(:NEW.ORG_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','ORG_ID',:OLD.ORG_ID,:NEW.ORG_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_MSG,' ') != NVL(:NEW.EVENT_MSG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_MSG',:OLD.EVENT_MSG,:NEW.EVENT_MSG,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_RES,' ') != NVL(:NEW.EVENT_RES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_RES',:OLD.EVENT_RES,:NEW.EVENT_RES,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_FLAG,0) != NVL(:NEW.EVENT_FLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_FLAG',:OLD.EVENT_FLAG,:NEW.EVENT_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL. F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_DAYSBEFORE,0) != NVL(:NEW.PAT_DAYSBEFORE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','PAT_DAYSBEFORE',:OLD.PAT_DAYSBEFORE,:NEW.PAT_DAYSBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_DAYSAFTER,0) != NVL(:NEW.PAT_DAYSAFTER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','PAT_DAYSAFTER',:OLD.PAT_DAYSAFTER,:NEW.PAT_DAYSAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_DAYSBEFORE,0) != NVL(:NEW.USR_DAYSBEFORE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','USR_DAYSBEFORE',:OLD.USR_DAYSBEFORE,:NEW.USR_DAYSBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_DAYSAFTER,0) != NVL(:NEW.USR_DAYSAFTER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','USR_DAYSAFTER',:OLD.USR_DAYSAFTER,:NEW.USR_DAYSAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_MSGBEFORE,' ') != NVL(:NEW.PAT_MSGBEFORE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','PAT_MSGBEFORE',:OLD.PAT_MSGBEFORE,:NEW.PAT_MSGBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_MSGAFTER,' ') != NVL(:NEW.PAT_MSGAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','PAT_MSGAFTER',:OLD.PAT_MSGAFTER,:NEW.PAT_MSGAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_MSGBEFORE,' ') != NVL(:NEW.USR_MSGBEFORE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','USR_MSGBEFORE',:OLD.USR_MSGBEFORE,:NEW.USR_MSGBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_MSGAFTER,' ') != NVL(:NEW.USR_MSGAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','USR_MSGAFTER',:OLD.USR_MSGAFTER,:NEW.USR_MSGAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.STATUS_DT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.STATUS_DT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','STATUS_DT',TO_CHAR(:OLD.STATUS_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.STATUS_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.STATUS_CHANGEBY,0) != NVL(:NEW.STATUS_CHANGEBY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','STATUS_CHANGEBY',:OLD.STATUS_CHANGEBY,:NEW.STATUS_CHANGEBY,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.ORIG_STUDY,0) != NVL(:NEW.ORIG_STUDY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','ORIG_STUDY',:OLD.ORIG_STUDY,:NEW.ORIG_STUDY,NULL,NULL);
    END IF;
    IF NVL(:OLD.ORIG_CAL,0) != NVL(:NEW.ORIG_CAL,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','ORIG_CAL',:OLD.ORIG_CAL,:NEW.ORIG_CAL,NULL,NULL);
    END IF;
    IF NVL(:OLD.ORIG_EVENT,0) != NVL(:NEW.ORIG_EVENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','ORIG_EVENT',:OLD.ORIG_EVENT,:NEW.ORIG_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.DURATION_UNIT,' ') != NVL(:NEW.DURATION_UNIT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','DURATION_UNIT',:OLD.DURATION_UNIT,:NEW.DURATION_UNIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_VISIT,0) != NVL(:NEW.FK_VISIT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','FK_VISIT',:OLD.FK_VISIT,:NEW.FK_VISIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_CPTCODE,' ') != NVL(:NEW.EVENT_CPTCODE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_CPTCODE',:OLD.EVENT_CPTCODE,:NEW.EVENT_CPTCODE,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_FUZZYAFTER,' ') != NVL(:NEW.EVENT_FUZZYAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_FUZZYAFTER',:OLD.EVENT_FUZZYAFTER,:NEW.EVENT_FUZZYAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_DURATIONAFTER,' ') != NVL(:NEW.EVENT_DURATIONAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_DURATIONAFTER',:OLD.EVENT_DURATIONAFTER,:NEW.EVENT_DURATIONAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_DURATIONBEFORE,' ') != NVL(:NEW.EVENT_DURATIONBEFORE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','EVENT_DURATIONBEFORE',:OLD.EVENT_DURATIONBEFORE,:NEW.EVENT_DURATIONBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_CATLIB,0) !=NVL(:NEW.FK_CATLIB,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','FK_CATLIB',:OLD.FK_CATLIB,:NEW.FK_CATLIB,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_CALASSOCTO,' ') !=NVL(:NEW.EVENT_CALASSOCTO,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','EVENT_CALASSOCTO',:OLD.EVENT_CALASSOCTO,:NEW.EVENT_CALASSOCTO,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_CALSCHDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.EVENT_CALSCHDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_ASSOC','LAST_MODIFIED_DATE',TO_CHAR(:OLD.EVENT_CALSCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.EVENT_CALSCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_CATEGORY,' ') !=NVL(:NEW.EVENT_CATEGORY,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','EVENT_CATEGORY',:OLD.EVENT_CATEGORY,:NEW.EVENT_CATEGORY,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_SEQUENCE,0) !=NVL(:NEW.EVENT_SEQUENCE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','EVENT_SEQUENCE',:OLD.EVENT_SEQUENCE,:NEW.EVENT_SEQUENCE,NULL,NULL);
    END IF;
    IF NVL(:OLD.OFFLINE_FLAG,0) !=NVL(:NEW.OFFLINE_FLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','OFFLINE_FLAG',:OLD.OFFLINE_FLAG,:NEW.OFFLINE_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.HIDE_FLAG,0) !=NVL(:NEW.HIDE_FLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','HIDE_FLAG',:OLD.HIDE_FLAG,:NEW.HIDE_FLAG,NULL,NULL);
    END IF;
	
		IF (NVL(:OLD.EVENT_LIBRARY_TYPE,0) > 0) THEN
          V_EVENT_LIBRARY_TYPE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.EVENT_LIBRARY_TYPE);  
        END IF;
        IF (NVL(:NEW.EVENT_LIBRARY_TYPE,0)>0) THEN
            V_EVENT_LIBRARY_TYPE_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.EVENT_LIBRARY_TYPE);  
        END IF;
    IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) !=NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','EVENT_LIBRARY_TYPE',V_EVENT_LIBRARY_TYPE_OLD,V_EVENT_LIBRARY_TYPE_NEW,NULL,NULL);
    END IF;
	
		IF (NVL(:OLD.EVENT_LINE_CATEGORY,0) > 0) THEN
          V_EVENT_LINE_CATEGORY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.EVENT_LINE_CATEGORY);  
        END IF;
        IF (NVL(:NEW.EVENT_LINE_CATEGORY,0)>0) THEN
            V_EVENT_LINE_CATEGORY_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.EVENT_LINE_CATEGORY);  
        END IF;
    IF NVL(:OLD.EVENT_LINE_CATEGORY,0) !=NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','EVENT_LINE_CATEGORY',V_EVENT_LINE_CATEGORY_OLD,V_EVENT_LINE_CATEGORY_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.SERVICE_SITE_ID,0) !=NVL(:NEW.SERVICE_SITE_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID,:NEW.SERVICE_SITE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.FACILITY_ID,0) !=NVL(:NEW.FACILITY_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','FACILITY_ID',:OLD.FACILITY_ID,:NEW.FACILITY_ID,NULL,NULL);
    END IF;
		IF (NVL(:OLD.FK_CODELST_COVERTYPE,0) > 0) THEN
          V_FK_CODELST_COVERTYPE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_COVERTYPE);  
        END IF;
        IF (NVL(:NEW.FK_CODELST_COVERTYPE,0)>0) THEN
            V_FK_CODELST_COVERTYPE_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_COVERTYPE);  
        END IF;
	
    IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','FK_CODELST_COVERTYPE',V_FK_CODELST_COVERTYPE_OLD,V_FK_CODELST_COVERTYPE_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.BUDGET_TEMPLATE,0) !=NVL(:NEW.BUDGET_TEMPLATE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','BUDGET_TEMPLATE',:OLD.BUDGET_TEMPLATE,:NEW.BUDGET_TEMPLATE,NULL,NULL);
    END IF;
    IF NVL(:OLD.COVERAGE_NOTES,' ') !=NVL(:NEW.COVERAGE_NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','COVERAGE_NOTES',:OLD.COVERAGE_NOTES,:NEW.COVERAGE_NOTES,NULL,NULL);
    END IF;
	IF (NVL(:OLD.FK_CODELST_CALSTAT,0) > 0) THEN
	  V_FK_CODELST_CALSTAT_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_CALSTAT);  
	END IF;
	IF (NVL(:NEW.FK_CODELST_CALSTAT,0)>0) THEN
		V_FK_CODELST_CALSTAT_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_CALSTAT);  
	END IF;
    IF NVL(:OLD.FK_CODELST_CALSTAT,0) !=NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_ASSOC','FK_CODELST_CALSTAT',V_FK_CODELST_CALSTAT_OLD,V_FK_CODELST_CALSTAT_NEW,NULL,NULL);
    END IF;
	
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger EVENT_ASSOC_AU2 ');
END;
/
CREATE OR REPLACE TRIGGER EVENT_DEF_AU0 AFTER UPDATE ON EVENT_DEF FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);

	audit_trail.record_transaction
	(raid, 'EVENT_DEF', :OLD.rid, 'U', usr );

	IF NVL(:OLD.event_id,0) !=
		NVL(:NEW.event_id,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_ID',:OLD.event_id, :NEW.event_id);
	END IF;
	IF NVL(:OLD.chain_id,0) !=
		NVL(:NEW.chain_id,0) THEN
		audit_trail.column_update
		(raid, 'CHAIN_ID',:OLD.chain_id, :NEW.chain_id);
	END IF;
	IF NVL(:OLD.event_type,' ') !=
		NVL(:NEW.event_type,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_TYPE',:OLD.event_type, :NEW.event_type);
	END IF;
	IF NVL(:OLD.NAME,' ') !=
		NVL(:NEW.NAME,' ') THEN
		audit_trail.column_update
		(raid, 'NAME',:OLD.NAME, :NEW.NAME);
	END IF;
	IF NVL(:OLD.notes,' ') !=
		NVL(:NEW.notes,' ') THEN
		audit_trail.column_update
		(raid, 'NOTES',:OLD.notes, :NEW.notes);
	END IF;
	IF NVL(:OLD.COST,0) !=
		NVL(:NEW.COST,0) THEN
		audit_trail.column_update
		(raid, 'COST',:OLD.COST, :NEW.COST);
	END IF;
	IF NVL(:OLD.cost_description,' ') !=
		NVL(:NEW.cost_description,' ') THEN
		audit_trail.column_update
		(raid, 'COST_DESCRIPTION',:OLD.cost_description, :NEW.cost_description);
	END IF;
	IF NVL(:OLD.duration,0) !=
		NVL(:NEW.duration,0) THEN
		audit_trail.column_update
		(raid, 'DURATION',:OLD.duration, :NEW.duration);
	END IF;
	IF NVL(:OLD.user_id,0) !=
		NVL(:NEW.user_id,0) THEN
		audit_trail.column_update
		(raid, 'USER_ID',:OLD.user_id, :NEW.user_id);
	END IF;
	IF NVL(:OLD.linked_uri,' ') !=
		NVL(:NEW.linked_uri,' ') THEN
		audit_trail.column_update
		(raid, 'LINKED_URI',:OLD.linked_uri, :NEW.linked_uri);
	END IF;
	IF NVL(:OLD.fuzzy_period,' ') !=
		NVL(:NEW.fuzzy_period,' ') THEN
		audit_trail.column_update
		(raid, 'FUZZY_PERIOD',:OLD.fuzzy_period, :NEW.fuzzy_period);
	END IF;
	IF NVL(:OLD.msg_to,' ') !=
		NVL(:NEW.msg_to,' ') THEN
		audit_trail.column_update
		(raid, 'MSG_TO',:OLD.msg_to, :NEW.msg_to);
	END IF;
	/*
	--JM: 07Feb2011: #D-FIN9
	IF NVL(:OLD.status,' ') !=
		NVL(:NEW.status,' ') THEN
		audit_trail.column_update
		(raid, 'STATUS',:OLD.status, :NEW.status);
	END IF;*/
	IF NVL(:OLD.description,' ') !=
		NVL(:NEW.description,' ') THEN
		audit_trail.column_update
		(raid, 'DESCRIPTION',:OLD.description, :NEW.description);
	END IF;
	IF NVL(:OLD.displacement,0) !=
		NVL(:NEW.displacement,0) THEN
		audit_trail.column_update
		(raid, 'DISPLACEMENT',:OLD.displacement, :NEW.displacement);
	END IF;
	IF NVL(:OLD.org_id,0) !=
		NVL(:NEW.org_id,0) THEN
		audit_trail.column_update
		(raid, 'ORG_ID',:OLD.org_id, :NEW.org_id);
	END IF;
	IF NVL(:OLD.event_msg,' ') !=
		NVL(:NEW.event_msg,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_MSG',:OLD.event_msg, :NEW.event_msg);
	END IF;
	IF NVL(:OLD.event_res,' ') !=
		NVL(:NEW.event_res,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_RES',:OLD.event_res, :NEW.event_res);
	END IF;
	IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
		NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
		audit_trail.column_update
		(raid, 'CREATED_ON',to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.event_flag,0) !=
		NVL(:NEW.event_flag,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_FLAG',:OLD.event_flag, :NEW.event_flag);
	END IF;
	IF NVL(:OLD.pat_daysbefore,0) !=
		NVL(:NEW.pat_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSBEFORE',:OLD.pat_daysbefore, :NEW.pat_daysbefore);
	END IF;
	IF NVL(:OLD.pat_daysafter,0) !=
		NVL(:NEW.pat_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSAFTER',:OLD.pat_daysafter, :NEW.pat_daysafter);
	END IF;
	IF NVL(:OLD.usr_daysbefore,0) !=
		NVL(:NEW.usr_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSBEFORE',:OLD.usr_daysbefore, :NEW.usr_daysbefore);
	END IF;
	IF NVL(:OLD.usr_daysafter,0) !=
		NVL(:NEW.usr_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSAFTER',:OLD.usr_daysafter, :NEW.usr_daysafter);
	END IF;
	IF NVL(:OLD.pat_msgbefore,' ') !=
		NVL(:NEW.pat_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGBEFORE',:OLD.pat_msgbefore, :NEW.pat_msgbefore);
	END IF;
	IF NVL(:OLD.pat_msgafter,' ') !=
		NVL(:NEW.pat_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGAFTER',:OLD.pat_msgafter, :NEW.pat_msgafter);
	END IF;
	IF NVL(:OLD.usr_msgbefore,' ') !=
		NVL(:NEW.usr_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGBEFORE',:OLD.usr_msgbefore, :NEW.usr_msgbefore);
	END IF;
	IF NVL(:OLD.usr_msgafter,' ') !=
		NVL(:NEW.usr_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGAFTER',:OLD.usr_msgafter, :NEW.usr_msgafter);
	END IF;
	IF NVL(:OLD.rid,0) !=
		NVL(:NEW.rid,0) THEN
		audit_trail.column_update
		(raid, 'RID',:OLD.rid, :NEW.rid);
	END IF;
	IF NVL(:OLD.last_modified_by,0) !=
		NVL(:NEW.last_modified_by,0) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_BY',:OLD.last_modified_by, :NEW.last_modified_by);
	END IF;
	IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
		NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_DATE',to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.ip_add,' ') !=
		NVL(:NEW.ip_add,' ') THEN
		audit_trail.column_update
		(raid, 'IP_ADD',:OLD.ip_add, :NEW.ip_add);
	END IF;

--JM: newly added
	IF NVL(:OLD.calendar_sharedwith,' ') !=
		NVL(:NEW.calendar_sharedwith,' ') THEN
		audit_trail.column_update
		(raid, 'CALENDAR_SHAREDWITH',:OLD.calendar_sharedwith, :NEW.calendar_sharedwith);
	END IF;

	IF NVL(:OLD.duration_unit,' ') !=
		NVL(:NEW.duration_unit,' ') THEN
		audit_trail.column_update
		(raid, 'DURATION_UNIT',:OLD.duration_unit, :NEW.duration_unit);
	END IF;

	IF NVL(:OLD.fk_visit,0) !=
		NVL(:NEW.fk_visit,0) THEN
		audit_trail.column_update
		(raid, 'FK_VISIT',:OLD.fk_visit, :NEW.fk_visit);
	END IF;
------------

	IF NVL(:OLD.event_fuzzyafter,' ') !=
		NVL(:NEW.event_fuzzyafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_FUZZYAFTER',:OLD.event_fuzzyafter, :NEW.event_fuzzyafter);
	END IF;

	IF NVL(:OLD.event_durationafter,' ') !=
		NVL(:NEW.event_durationafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONAFTER',:OLD.event_durationafter, :NEW.event_durationafter);
	END IF;

	IF NVL(:OLD.event_cptcode,' ') !=
		NVL(:NEW.event_cptcode,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CPTCODE',:OLD.event_cptcode, :NEW.event_cptcode);
	END IF;

	IF NVL(:OLD.event_durationbefore,' ') !=
		NVL(:NEW.event_durationbefore,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONBEFORE',:OLD.event_durationbefore, :NEW.event_durationbefore);
	END IF;

	IF NVL(:OLD.fk_catlib,0) !=
		NVL(:NEW.fk_catlib,0) THEN
		audit_trail.column_update
		(raid, 'FK_CATLIB',:OLD.fk_catlib, :NEW.fk_catlib);
	END IF;
-- Added by Manimaran for Enh#C7
	IF NVL(:OLD.EVENT_CATEGORY,' ') !=
		NVL(:NEW.EVENT_CATEGORY,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CATEGORY',:OLD.EVENT_CATEGORY, :NEW.EVENT_CATEGORY);
	END IF;
-- KM - 07Aug09
	IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) !=
		NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LIBRARY_TYPE',:OLD.EVENT_LIBRARY_TYPE, :NEW.EVENT_LIBRARY_TYPE);
	END IF;

	IF NVL(:OLD.EVENT_LINE_CATEGORY,0) !=
		NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LINE_CATEGORY',:OLD.EVENT_LINE_CATEGORY, :NEW.EVENT_LINE_CATEGORY);
	END IF;
---------------------------------------
	IF NVL(:OLD.SERVICE_SITE_ID,0) !=
		NVL(:NEW.SERVICE_SITE_ID,0) THEN
		audit_trail.column_update
		(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
	END IF;

	IF NVL(:OLD.FACILITY_ID,0) !=
		NVL(:NEW.FACILITY_ID,0) THEN
		audit_trail.column_update
		(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
	END IF;

	IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
		NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
	END IF;
  
  --BK 23rd july 2010
    IF NVL(:OLD.COVERAGE_NOTES,0) !=
		NVL(:NEW.COVERAGE_NOTES,0) THEN
		audit_trail.column_update
		(raid, 'COVERAGE_NOTES',:OLD.COVERAGE_NOTES, :NEW.COVERAGE_NOTES);
	END IF;
--JM: 07Feb: #D-FIN9	
	IF NVL(:OLD.FK_CODELST_CALSTAT,0) !=
		NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_CALSTAT',:OLD.FK_CODELST_CALSTAT, :NEW.FK_CODELST_CALSTAT);
	END IF;
	
END;
/
--When EVENT_ASSOC table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."EVENT_DEF_AU1" 
AFTER UPDATE ON ESCH.EVENT_DEF 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);   /*variable used to fetch value of sequence */
	v_mouduleId number(10);
    V_EVENT_LIBRARY_TYPE_OLD VARCHAR2(200) :=NULL;
    V_EVENT_LINE_CATEGORY_OLD VARCHAR2(200) :=NULL;
    V_FK_CODELST_COVERTYPE_OLD VARCHAR2(200) :=NULL;
    V_FK_CODELST_CALSTAT_OLD VARCHAR2(200) :=NULL;
    V_EVENT_LIBRARY_TYPE_NEW VARCHAR2(200) :=NULL;
    V_EVENT_LINE_CATEGORY_NEW VARCHAR2(200) :=NULL;
    V_FK_CODELST_COVERTYPE_NEW VARCHAR2(200) :=NULL;
    V_FK_CODELST_CALSTAT_NEW VARCHAR2(200) :=NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
	BEGIN
    IF (:OLD.EVENT_TYPE= 'P') THEN
    v_mouduleId := :OLD.EVENT_ID;
    ELSIF (:OLD.EVENT_TYPE= 'A') THEN
    v_mouduleId := :OLD.CHAIN_ID;
    END IF;
    END;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on EVENT_DEF
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'EVENT_DEF',:OLD.RID,v_mouduleId,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.EVENT_ID,0) != NVL(:NEW.EVENT_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_ID',:OLD.EVENT_ID,:NEW.EVENT_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CHAIN_ID,0) != NVL(:NEW.CHAIN_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','CHAIN_ID',:OLD.CHAIN_ID,:NEW.CHAIN_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_TYPE,' ') != NVL(:NEW.EVENT_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_TYPE',:OLD.EVENT_TYPE,:NEW.EVENT_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.NAME,' ') != NVL(:NEW.NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','NAME',:OLD.NAME,:NEW.NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.NOTES,' ') != NVL(:NEW.NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','NOTES',:OLD.NOTES,:NEW.NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.COST,0) != NVL(:NEW.COST,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','COST',:OLD.COST,:NEW.COST,NULL,NULL);
    END IF;
    IF NVL(:OLD.COST_DESCRIPTION,' ') != NVL(:NEW.COST_DESCRIPTION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','COST_DESCRIPTION',:OLD.COST_DESCRIPTION,:NEW.COST_DESCRIPTION,NULL,NULL);
    END IF;
    IF NVL(:OLD.DURATION,0) != NVL(:NEW.DURATION,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','DURATION',:OLD.DURATION,:NEW.DURATION,NULL,NULL);
    END IF;
    IF NVL(:OLD.USER_ID,0) != NVL(:NEW.USER_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','USER_ID',:OLD.USER_ID,:NEW.USER_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.LINKED_URI,' ') != NVL(:NEW.LINKED_URI,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','LINKED_URI',:OLD.LINKED_URI,:NEW.LINKED_URI,NULL,NULL);
    END IF;
    IF NVL(:OLD.FUZZY_PERIOD,' ') != NVL(:NEW.FUZZY_PERIOD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','FUZZY_PERIOD',:OLD.FUZZY_PERIOD,:NEW.FUZZY_PERIOD,NULL,NULL);
    END IF;
    IF NVL(:OLD.MSG_TO,' ') != NVL(:NEW.MSG_TO,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','MSG_TO',:OLD.MSG_TO,:NEW.MSG_TO,NULL,NULL);
    END IF;
    IF NVL(:OLD.STATUS,' ') != NVL(:NEW.STATUS,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','STATUS',:OLD.STATUS,:NEW.STATUS,NULL,NULL);
    END IF;
    IF NVL(:OLD.DESCRIPTION,' ') != NVL(:NEW.DESCRIPTION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','DESCRIPTION',:OLD.DESCRIPTION,:NEW.DESCRIPTION,NULL,NULL);
    END IF;
    IF NVL(:OLD.DISPLACEMENT,0) != NVL(:NEW.DISPLACEMENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','DISPLACEMENT',:OLD.DISPLACEMENT,:NEW.DISPLACEMENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.ORG_ID,0) != NVL(:NEW.ORG_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','ORG_ID',:OLD.ORG_ID,:NEW.ORG_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_MSG,' ') != NVL(:NEW.EVENT_MSG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_MSG',:OLD.EVENT_MSG,:NEW.EVENT_MSG,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_RES,' ') != NVL(:NEW.EVENT_RES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_RES',:OLD.EVENT_RES,:NEW.EVENT_RES,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_FLAG,0) != NVL(:NEW.EVENT_FLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_FLAG',:OLD.EVENT_FLAG,:NEW.EVENT_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_DAYSBEFORE,0) != NVL(:NEW.PAT_DAYSBEFORE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','PAT_DAYSBEFORE',:OLD.PAT_DAYSBEFORE,:NEW.PAT_DAYSBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_DAYSAFTER,0) != NVL(:NEW.PAT_DAYSAFTER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','PAT_DAYSAFTER',:OLD.PAT_DAYSAFTER,:NEW.PAT_DAYSAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_DAYSBEFORE,0) != NVL(:NEW.USR_DAYSBEFORE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','USR_DAYSBEFORE',:OLD.USR_DAYSBEFORE,:NEW.USR_DAYSBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_DAYSAFTER,0) != NVL(:NEW.USR_DAYSAFTER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','USR_DAYSAFTER',:OLD.USR_DAYSAFTER,:NEW.USR_DAYSAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_MSGBEFORE,' ') != NVL(:NEW.PAT_MSGBEFORE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','PAT_MSGBEFORE',:OLD.PAT_MSGBEFORE,:NEW.PAT_MSGBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.PAT_MSGAFTER,' ') != NVL(:NEW.PAT_MSGAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','PAT_MSGAFTER',:OLD.PAT_MSGAFTER,:NEW.PAT_MSGAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_MSGBEFORE,' ') != NVL(:NEW.USR_MSGBEFORE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','USR_MSGBEFORE',:OLD.USR_MSGBEFORE,:NEW.USR_MSGBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.USR_MSGAFTER,' ') != NVL(:NEW.USR_MSGAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','USR_MSGAFTER',:OLD.USR_MSGAFTER,:NEW.USR_MSGAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.CALENDAR_SHAREDWITH,' ') != NVL(:NEW.CALENDAR_SHAREDWITH,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_DEF','CALENDAR_SHAREDWITH',:OLD.CALENDAR_SHAREDWITH,:NEW.CALENDAR_SHAREDWITH,NULL,NULL);
    END IF;
    IF NVL(:OLD.DURATION_UNIT,' ') != NVL(:NEW.DURATION_UNIT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'EVENT_DEF','DURATION_UNIT',:OLD.DURATION_UNIT,:NEW.DURATION_UNIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_VISIT,0) != NVL(:NEW.FK_VISIT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','FK_VISIT',:OLD.FK_VISIT,:NEW.FK_VISIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_FUZZYAFTER,' ') != NVL(:NEW.EVENT_FUZZYAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_FUZZYAFTER',:OLD.EVENT_FUZZYAFTER,:NEW.EVENT_FUZZYAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_DURATIONAFTER,' ') != NVL(:NEW.EVENT_DURATIONAFTER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_DURATIONAFTER',:OLD.EVENT_DURATIONAFTER,:NEW.EVENT_DURATIONAFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_CPTCODE,' ') != NVL(:NEW.EVENT_CPTCODE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_CPTCODE',:OLD.EVENT_CPTCODE,:NEW.EVENT_CPTCODE,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_DURATIONBEFORE,' ') != NVL(:NEW.EVENT_DURATIONBEFORE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_DURATIONBEFORE',:OLD.EVENT_DURATIONBEFORE,:NEW.EVENT_DURATIONBEFORE,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_CATLIB,0) != NVL(:NEW.FK_CATLIB,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','FK_CATLIB',:OLD.FK_CATLIB,:NEW.FK_CATLIB,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_CATEGORY,' ') != NVL(:NEW.EVENT_CATEGORY,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_CATEGORY',:OLD.EVENT_CATEGORY,:NEW.EVENT_CATEGORY,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_SEQUENCE,0) != NVL(:NEW.EVENT_SEQUENCE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_SEQUENCE',:OLD.EVENT_SEQUENCE,:NEW.EVENT_SEQUENCE,NULL,NULL);
    END IF;
         IF (NVL(:OLD.EVENT_LIBRARY_TYPE,0) > 0) THEN
          V_EVENT_LIBRARY_TYPE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.EVENT_LIBRARY_TYPE);  
        END IF;
        IF (NVL(:NEW.EVENT_LIBRARY_TYPE,0)>0) THEN
            V_EVENT_LIBRARY_TYPE_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.EVENT_LIBRARY_TYPE);  
        END IF;
    IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) != NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_LIBRARY_TYPE',V_EVENT_LIBRARY_TYPE_OLD, V_EVENT_LIBRARY_TYPE_NEW,NULL,NULL);
    END IF;
        IF (NVL(:OLD.EVENT_LINE_CATEGORY,0) > 0) THEN
          V_EVENT_LINE_CATEGORY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.EVENT_LINE_CATEGORY);  
        END IF;
        IF (NVL(:NEW.EVENT_LINE_CATEGORY,0)>0) THEN
            V_EVENT_LINE_CATEGORY_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.EVENT_LINE_CATEGORY);  
        END IF;
    IF NVL(:OLD.EVENT_LINE_CATEGORY,0) != NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','EVENT_LINE_CATEGORY',V_EVENT_LINE_CATEGORY_OLD,V_EVENT_LINE_CATEGORY_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.SERVICE_SITE_ID,0) != NVL(:NEW.SERVICE_SITE_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID,:NEW.SERVICE_SITE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.FACILITY_ID,0) != NVL(:NEW.FACILITY_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','FACILITY_ID',:OLD.FACILITY_ID,:NEW.FACILITY_ID,NULL,NULL);
    END IF;
           IF (NVL(:OLD.FK_CODELST_COVERTYPE,0) > 0) THEN
          V_FK_CODELST_COVERTYPE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_COVERTYPE);  
        END IF;
        IF (NVL(:NEW.FK_CODELST_COVERTYPE,0)>0) THEN
            V_FK_CODELST_COVERTYPE_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_COVERTYPE);  
        END IF;
    
    IF NVL(:OLD.FK_CODELST_COVERTYPE,0) != NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','FK_CODELST_COVERTYPE',V_FK_CODELST_COVERTYPE_OLD,V_FK_CODELST_COVERTYPE_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.COVERAGE_NOTES,' ') != NVL(:NEW.COVERAGE_NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','COVERAGE_NOTES',:OLD.COVERAGE_NOTES,:NEW.COVERAGE_NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.REASON_FOR_COVERAGECHANGE,' ') != NVL(:NEW.REASON_FOR_COVERAGECHANGE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','REASON_FOR_COVERAGECHANGE',:OLD.REASON_FOR_COVERAGECHANGE,:NEW.REASON_FOR_COVERAGECHANGE,NULL,NULL);
    END IF;
        IF (NVL(:OLD.FK_CODELST_CALSTAT,0) > 0) THEN
      V_FK_CODELST_CALSTAT_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_CALSTAT);  
      END IF;
       IF (NVL(:NEW.FK_CODELST_CALSTAT,0)>0) THEN
        V_FK_CODELST_CALSTAT_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_CALSTAT);  
      END IF;
    IF NVL(:OLD.FK_CODELST_CALSTAT,0) != NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'EVENT_DEF','FK_CODELST_CALSTAT',V_FK_CODELST_CALSTAT_OLD,V_FK_CODELST_CALSTAT_NEW,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger EVENT_DEF_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_ADVERSEVE_AU0 AFTER UPDATE OF RID,FK_PER,IP_ADD,AE_DESC,CREATOR,AE_NOTES,FK_STUDY,AE_STDATE,PK_ADVEVE,AE_ADDINFO,AE_ENDDATE,AE_ENTERBY,AE_OUTDATE,AE_OUTTYPE,CREATED_ON,FK_EVENTS1,AE_OUTNOTES,AE_SEVERITY,AE_LOGGEDDATE,AE_REPORTEDBY,AE_DISCVRYDATE,AE_BDSYSTEM_AFF,AE_RELATIONSHIP,AE_RECOVERY_DESC,FK_CODLST_AETYPE,FK_LINK_ADVERSEVE ON SCH_ADVERSEVE FOR EACH ROW
declare
  raid number(10);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVERSEVE', :old.rid, 'U', :new.LAST_MODIFIED_BY);

  if nvl(:old.pk_adveve,0) !=
     NVL(:new.pk_adveve,0) then
     audit_trail.column_update
       (raid, 'PK_ADVEVE',
       :old.pk_adveve, :new.pk_adveve);
  end if;
  if nvl(:old.fk_codlst_aetype,0) !=
     NVL(:new.fk_codlst_aetype,0) then
     audit_trail.column_update
       (raid, 'FK_CODLST_AETYPE',
       :old.fk_codlst_aetype, :new.fk_codlst_aetype);
  end if;
  if nvl(:old.ae_desc,' ') !=
     NVL(:new.ae_desc,' ') then
     audit_trail.column_update
       (raid, 'AE_DESC',
       :old.ae_desc, :new.ae_desc);
  end if;
  if nvl(:old.ae_stdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ae_stdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'AE_STDATE',
       to_char(:old.ae_stdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_stdate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_enddate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ae_enddate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'AE_ENDDATE',
       to_char(:old.ae_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_enterby,0) !=
     NVL(:new.ae_enterby,0) then
     audit_trail.column_update
       (raid, 'AE_ENTERBY',
       :old.ae_enterby, :new.ae_enterby);
  end if;
   if nvl(:old.ae_reportedby,0) !=
     NVL(:new.ae_reportedby,0) then
     audit_trail.column_update
       (raid, 'AE_REPORTEDBY',
       :old.ae_reportedby, :new.ae_reportedby);
  end if;
  if nvl(:old.ae_outtype,' ') !=
     NVL(:new.ae_outtype,' ') then
     audit_trail.column_update
       (raid, 'AE_OUTTYPE',
       :old.ae_outtype, :new.ae_outtype);
  end if;

  if nvl(:old.fk_link_adverseve,0) !=
     NVL(:new.fk_link_adverseve,0) then
     audit_trail.column_update
       (raid,'FK_LINK_ADVERSEVE',
       :old.fk_link_adverseve, :new.fk_link_adverseve);
  end if;

  if nvl(:old.ae_outdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ae_outdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'AE_OUTDATE',
       to_char(:old.ae_outdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_outdate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_discvrydate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ae_discvrydate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'AE_DISCVRY',
       to_char(:old.ae_discvrydate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_discvrydate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ae_loggeddate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.ae_loggeddate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'AE_LOGGEDDATE',
       to_char(:old.ae_loggeddate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ae_loggeddate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ae_outnotes,' ') !=
     NVL(:new.ae_outnotes,' ') then
     audit_trail.column_update
       (raid, 'AE_OUTNOTES',
       :old.ae_outnotes, :new.ae_outnotes);
  end if;
  if nvl(:old.ae_addinfo,' ') !=
     NVL(:new.ae_addinfo,' ') then
     audit_trail.column_update
       (raid, 'AE_ADDINFO',
       :old.ae_addinfo, :new.ae_addinfo);
  end if;
  if nvl(:old.ae_notes,' ') !=
     NVL(:new.ae_notes,' ') then
     audit_trail.column_update
       (raid, 'AE_NOTES',
       :old.ae_notes, :new.ae_notes);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
  end if;
  if nvl(:old.fk_per,0) !=
     NVL(:new.fk_per,0) then
     audit_trail.column_update
       (raid, 'FK_PER',
       :old.fk_per, :new.fk_per);
  end if;
  if nvl(:old.ae_severity,0) !=
     NVL(:new.ae_severity,0) then
     audit_trail.column_update
       (raid, 'AE_SEVERITY',
       :old.ae_severity, :new.ae_severity);
  end if;
  if nvl(:old.ae_bdsystem_aff,0) !=
     NVL(:new.ae_bdsystem_aff,0) then
     audit_trail.column_update
       (raid, 'AE_BDSYSTEM_AFF',
       :old.ae_bdsystem_aff, :new.ae_bdsystem_aff);
  end if;
  if nvl(:old.ae_relationship,0) !=
     NVL(:new.ae_relationship,0) then
     audit_trail.column_update
       (raid, 'AE_RELATIONSHIP',
       :old.ae_relationship, :new.ae_relationship);
  end if;
  if nvl(:old.ae_recovery_desc,0) !=
     NVL(:new.ae_recovery_desc,0) then
     audit_trail.column_update
       (raid, 'AE_RECOVERY_DESC',
       :old.ae_recovery_desc, :new.ae_recovery_desc);
  end if;

END;
/
CREATE OR REPLACE TRIGGER SCH_ADVINFO_AU0
  AFTER UPDATE OF
  pk_advinfo,
  fk_adverse,
  fk_codelst_info,
  advinfo_value,
  advinfo_type,
  creator,
  created_on,
  ip_add,
  rid
  ON sch_advinfo
  FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVINFO', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_advinfo,0) !=
     NVL(:NEW.pk_advinfo,0) THEN
     audit_trail.column_update
       (raid, 'PK_ADVINFO',
       :OLD.pk_advinfo, :NEW.pk_advinfo);
  END IF;
  IF NVL(:OLD.fk_adverse,0) !=
     NVL(:NEW.fk_adverse,0) THEN
     audit_trail.column_update
       (raid, 'FK_ADVERSE',
       :OLD.fk_adverse, :NEW.fk_adverse);
  END IF;
  IF NVL(:OLD.fk_codelst_info,0) !=
     NVL(:NEW.fk_codelst_info,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_INFO',
       :OLD.fk_codelst_info, :NEW.fk_codelst_info);
  END IF;
  IF NVL(:OLD.advinfo_value,0) !=
     NVL(:NEW.advinfo_value,0) THEN
     audit_trail.column_update
       (raid, 'ADVINFO_VALUE',
       :OLD.advinfo_value, :NEW.advinfo_value);
  END IF;
  IF NVL(:OLD.advinfo_type,' ') !=
     NVL(:NEW.advinfo_type,' ') THEN
     audit_trail.column_update
       (raid, 'ADVINFO_TYPE',
       :OLD.advinfo_type, :NEW.advinfo_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
      to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

END;
/
CREATE OR REPLACE TRIGGER SCH_ADVNOTIFY_AU0
  AFTER UPDATE OF
  pk_advnotify,
  fk_adverseve,
  fk_codelst_not_type,
  advnotify_date,
  advnotify_notes,
  creator,
  created_on,
  ip_add,
  advnotify_value,
  rid
  ON sch_advnotify
  FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_ADVNOTIFY', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_advnotify,0) !=
     NVL(:NEW.pk_advnotify,0) THEN
     audit_trail.column_update
       (raid, 'PK_ADVNOTIFY',
       :OLD.pk_advnotify, :NEW.pk_advnotify);
  END IF;
  IF NVL(:OLD.fk_adverseve,0) !=
     NVL(:NEW.fk_adverseve,0) THEN
     audit_trail.column_update
       (raid, 'FK_ADVERSEVE',
       :OLD.fk_adverseve, :NEW.fk_adverseve);
  END IF;
  IF NVL(:OLD.fk_codelst_not_type,0) !=
     NVL(:NEW.fk_codelst_not_type,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_NOT_TYPE',
       :OLD.fk_codelst_not_type, :NEW.fk_codelst_not_type);
  END IF;
  IF NVL(:OLD.advnotify_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.advnotify_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'ADVNOTIFY_DATE',
       to_char(:old.advnotify_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.advnotify_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.advnotify_notes,' ') !=
     NVL(:NEW.advnotify_notes,' ') THEN
     audit_trail.column_update
       (raid, 'ADVNOTIFY_NOTES',
       :OLD.advnotify_notes, :NEW.advnotify_notes);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  IF NVL(:OLD.advnotify_value,0) !=
     NVL(:NEW.advnotify_value,0) THEN
     audit_trail.column_update
       (raid, 'ADVNOTIFY_VALUE',
       :OLD.advnotify_value, :NEW.advnotify_value);
  END IF;

END;
/
CREATE OR REPLACE TRIGGER SCH_ALERTNOTIFY_AU0
AFTER UPDATE
ON SCH_ALERTNOTIFY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_ALERTNOTIFY', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_alnot,0) !=
      NVL(:NEW.pk_alnot,0) THEN
      audit_trail.column_update
        (raid, 'PK_ALNOT',
        :OLD.pk_alnot, :NEW.pk_alnot);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.fk_codelst_an,0) !=
      NVL(:NEW.fk_codelst_an,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_AN',
        :OLD.fk_codelst_an, :NEW.fk_codelst_an);
   END IF;
   IF NVL(:OLD.alnot_type,' ') !=
      NVL(:NEW.alnot_type,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_TYPE',
        :OLD.alnot_type, :NEW.alnot_type);
   END IF;
   IF NVL(:OLD.alnot_users,' ') !=
      NVL(:NEW.alnot_users,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_USERS',
        :OLD.alnot_users, :NEW.alnot_users);
   END IF;
   IF NVL(:OLD.alnot_mobeve,' ') !=
      NVL(:NEW.alnot_mobeve,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_MOBEVE',
        :OLD.alnot_mobeve, :NEW.alnot_mobeve);
   END IF;
   IF NVL(:OLD.alnot_flag,0) !=
      NVL(:NEW.alnot_flag,0) THEN
      audit_trail.column_update
        (raid, 'ALNOT_FLAG',
        :OLD.alnot_flag, :NEW.alnot_flag);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_study,0) !=
      NVL(:NEW.fk_study,0) THEN
      audit_trail.column_update
        (raid, 'FK_STUDY',
        :OLD.fk_study, :NEW.fk_study);
   END IF;
   IF NVL(:OLD.fk_protocol,0) !=
      NVL(:NEW.fk_protocol,0) THEN
      audit_trail.column_update
        (raid, 'FK_PROTOCOL',
        :OLD.fk_protocol, :NEW.fk_protocol);
   END IF;
   IF NVL(:OLD.alnot_globalflag,' ') !=
      NVL(:NEW.alnot_globalflag,' ') THEN
      audit_trail.column_update
        (raid, 'ALNOT_GLOBALFLAG',
        :OLD.alnot_globalflag, :NEW.alnot_globalflag);
   END IF;

END;
/
CREATE OR REPLACE TRIGGER SCH_BGTAPNDX_AU0
AFTER UPDATE OF BGTAPNDX_DESC,BGTAPNDX_TYPE,BGTAPNDX_URI,CREATED_ON,CREATOR,FK_BUDGET,IP_ADD,PK_BGTAPNDX,RID
ON SCH_BGTAPNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_BGTAPNDX', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);
  IF NVL(:OLD.pk_bgtapndx,0) !=
     NVL(:NEW.pk_bgtapndx,0) THEN
     audit_trail.column_update
       (raid, 'PK_BGTAPNDX',
       :OLD.pk_bgtapndx, :NEW.pk_bgtapndx);
  END IF;
  IF NVL(:OLD.fk_budget,0) !=
     NVL(:NEW.fk_budget,0) THEN
     audit_trail.column_update
       (raid, 'FK_BUDGET',
       :OLD.fk_budget, :NEW.fk_budget);
  END IF;
  IF NVL(:OLD.bgtapndx_desc,' ') !=
     NVL(:NEW.bgtapndx_desc,' ') THEN
     audit_trail.column_update
       (raid, 'BGTAPNDX_DESC',
       :OLD.bgtapndx_desc, :NEW.bgtapndx_desc);
  END IF;
  IF NVL(:OLD.bgtapndx_uri,' ') !=
     NVL(:NEW.bgtapndx_uri,' ') THEN
     audit_trail.column_update
       (raid, 'BGTAPNDX_URI',
       :OLD.bgtapndx_uri, :NEW.bgtapndx_uri);
  END IF;
  IF NVL(:OLD.bgtapndx_type,' ') !=
     NVL(:NEW.bgtapndx_type,' ') THEN
     audit_trail.column_update
       (raid, 'BGTAPNDX_TYPE',
       :OLD.bgtapndx_type, :NEW.bgtapndx_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;
/
--When SCH_BGTAPNDX table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE
TRIGGER "ESCH"."SCH_BGTAPNDX_AU1" 
AFTER UPDATE ON ESCH.SCH_BGTAPNDX 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */   
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;    
BEGIN
  SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;  
	--Inserting row in AUDIT_ROW_MODULE table.
	PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT(v_rowid, 'SCH_BGTAPNDX', :OLD.RID,:NEW.FK_BUDGET,'U',:NEW.LAST_MODIFIED_BY);
  
	--Inserting rows in AUDIT_ROW_MODULE table.
    IF NVL(:OLD.PK_BGTAPNDX,0) != NVL(:NEW.PK_BGTAPNDX,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'PK_BGTAPNDX',:OLD.PK_BGTAPNDX, :NEW.PK_BGTAPNDX,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_BUDGET,0) != NVL(:NEW.FK_BUDGET,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'FK_BUDGET',:OLD.FK_BUDGET, :NEW.FK_BUDGET,NULL,NULL);
    END IF;
    IF NVL(:OLD.BGTAPNDX_DESC,' ') != NVL(:NEW.BGTAPNDX_DESC,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'BGTAPNDX_DESC',:OLD.BGTAPNDX_DESC, :NEW.BGTAPNDX_DESC,NULL,NULL);
    END IF;
    IF NVL(:OLD.BGTAPNDX_URI,' ') != NVL(:NEW.BGTAPNDX_URI,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'BGTAPNDX_URI',:OLD.BGTAPNDX_URI, :NEW.BGTAPNDX_URI,NULL,NULL);
    END IF;    
	IF NVL(:OLD.BGTAPNDX_TYPE,' ') != NVL(:NEW.BGTAPNDX_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'IP_ADD',:OLD.BGTAPNDX_TYPE, :NEW.BGTAPNDX_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'RID',:OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
    END IF;
   -- IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
   -- END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTAPNDX', 'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
    END IF;
        EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_BGTAPNDX_AU1 ');

END;
/
CREATE OR REPLACE TRIGGER SCH_BGTCAL_AU0
AFTER UPDATE
OF PK_BGTCAL,
FK_BUDGET,
BGTCAL_PROTID,
BGTCAL_PROTTYPE,
BGTCAL_DELFLAG,
BGTCAL_SPONSOROHEAD,
BGTCAL_CLINICOHEAD,
BGTCAL_SPONSORFLAG,
BGTCAL_CLINICFLAG,
BGTCAL_INDIRECTCOST,
RID,
CREATOR,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
CREATED_ON, IP_ADD,
BGTCAL_FRGBENEFIT,
BGTCAL_FRGFLAG,
BGTCAL_DISCOUNT,
BGTCAL_DISCOUNTFLAG,
BGTCAL_EXCLDSOCFLAG,
GRAND_RES_COST,
GRAND_RES_TOTALCOST,
GRAND_SOC_COST,
GRAND_SOC_TOTALCOST,
GRAND_TOTAL_COST,
GRAND_TOTAL_TOTALCOST,
GRAND_SALARY_COST,
GRAND_SALARY_TOTALCOST,
GRAND_FRINGE_COST,
GRAND_FRINGE_TOTALCOST,
GRAND_DISC_COST,
GRAND_DISC_TOTALCOST,
GRAND_IND_COST,
GRAND_IND_TOTALCOST,
GRAND_RES_SPONSOR,
GRAND_SOC_SPONSOR,
GRAND_SOC_VARIANCE,
GRAND_TOTAL_SPONSOR,
GRAND_TOTAL_VARIANCE,
GRAND_RES_VARIANCE,bgtcal_sp_overhead,
bgtcal_sp_overhead_flag
ON SCH_BGTCAL REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_BGTCAL', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_bgtcal,0) !=
     NVL(:NEW.pk_bgtcal,0) THEN
     audit_trail.column_update
       (raid, 'PK_BGTCAL',
       :OLD.pk_bgtcal, :NEW.pk_bgtcal);
  END IF;
  IF NVL(:OLD.fk_budget,0) !=
     NVL(:NEW.fk_budget,0) THEN
     audit_trail.column_update
       (raid, 'FK_BUDGET',
       :OLD.fk_budget, :NEW.fk_budget);
  END IF;
  IF NVL(:OLD.bgtcal_protid,0) !=
     NVL(:NEW.bgtcal_protid,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_PROTID',
       :OLD.bgtcal_protid, :NEW.bgtcal_protid);
  END IF;
  IF NVL(:OLD.bgtcal_prottype,' ') !=
     NVL(:NEW.bgtcal_prottype,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_PROTTYPE',
       :OLD.bgtcal_prottype, :NEW.bgtcal_prottype);
  END IF;
  IF NVL(:OLD.bgtcal_delflag,' ') !=
     NVL(:NEW.bgtcal_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_DELFLAG',
       :OLD.bgtcal_delflag, :NEW.bgtcal_delflag);
  END IF;
  IF NVL(:OLD.bgtcal_sponsorflag,' ') !=
     NVL(:NEW.bgtcal_sponsorflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SPONSORFLAG',
       :OLD.bgtcal_sponsorflag, :NEW.bgtcal_sponsorflag);
  END IF;
  IF NVL(:OLD.bgtcal_clinicflag,' ') !=
     NVL(:NEW.bgtcal_clinicflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTCAL_CLINICFLAG',
       :OLD.bgtcal_clinicflag, :NEW.bgtcal_clinicflag);
  END IF;
-----

  IF NVL(:OLD.bgtcal_sponsorohead,0) !=
     NVL(:NEW.bgtcal_sponsorohead,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SPONSOROHEAD',
       :OLD.bgtcal_sponsorohead, :NEW.bgtcal_sponsorohead);
  END IF;
  IF NVL(:OLD.bgtcal_clinicohead,0) !=
     NVL(:NEW.bgtcal_clinicohead,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_CLINICOHEAD',
       :OLD.bgtcal_clinicohead, :NEW.bgtcal_clinicohead);
  END IF;
  IF NVL(:OLD.bgtcal_indirectcost,0) !=
     NVL(:NEW.bgtcal_indirectcost,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_INDIRECTCOST',
       :OLD.bgtcal_indirectcost, :NEW.bgtcal_indirectcost);
  END IF;
  IF NVL(:OLD.bgtcal_frgbenefit,0) !=
     NVL(:NEW.bgtcal_frgbenefit,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_FRGBENEFIT',
       :OLD.bgtcal_frgbenefit, :NEW.bgtcal_frgbenefit);
  END IF;
  IF NVL(:OLD.bgtcal_frgflag,0) !=
     NVL(:NEW.bgtcal_frgflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_FRGFLAG',
       :OLD.bgtcal_frgflag, :NEW.bgtcal_frgflag);
  END IF;
  IF NVL(:OLD.bgtcal_discount,0) !=
     NVL(:NEW.bgtcal_discount,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_DISCOUNT',
       :OLD.bgtcal_discount, :NEW.bgtcal_discount);
  END IF;
  IF NVL(:OLD.bgtcal_discountflag,0) !=
     NVL(:NEW.bgtcal_discountflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_DISCOUNTFLAG',
       :OLD.bgtcal_discountflag, :NEW.bgtcal_discountflag);
  END IF;

-------

IF NVL(:OLD.bgtcal_excldsocflag,0) !=
     NVL(:NEW.bgtcal_excldsocflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_EXCLDSOCFLAG',
       :OLD.bgtcal_excldsocflag, :NEW.bgtcal_excldsocflag);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
--JM: added 02/15/07
  IF NVL(:OLD.grand_res_cost,' ') !=
     NVL(:NEW.grand_res_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_COST',
       :OLD.grand_res_cost, :NEW.grand_res_cost);
  END IF;
  IF NVL(:OLD.grand_res_totalcost,' ') !=
     NVL(:NEW.grand_res_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_TOTALCOST',
       :OLD.grand_res_totalcost, :NEW.grand_res_totalcost);
  END IF;
  IF NVL(:OLD.grand_soc_cost,' ') !=
     NVL(:NEW.grand_soc_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_COST',
       :OLD.grand_soc_cost, :NEW.grand_soc_cost);
  END IF;
  IF NVL(:OLD.grand_soc_totalcost,' ') !=
     NVL(:NEW.grand_soc_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_TOTALCOST',
       :OLD.grand_soc_totalcost, :NEW.grand_soc_totalcost);
  END IF;
  IF NVL(:OLD.grand_total_cost,' ') !=
     NVL(:NEW.grand_total_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_COST',
       :OLD.grand_total_cost, :NEW.grand_total_cost);
  END IF;
  IF NVL(:OLD.grand_total_totalcost,' ') !=
     NVL(:NEW.grand_total_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_TOTALCOST',
       :OLD.grand_total_totalcost, :NEW.grand_total_totalcost);
  END IF;
  IF NVL(:OLD.grand_salary_cost,' ') !=
     NVL(:NEW.grand_salary_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SALARY_COST',
       :OLD.grand_salary_cost, :NEW.grand_salary_cost);
  END IF;
  IF NVL(:OLD.grand_salary_totalcost,' ') !=
     NVL(:NEW.grand_salary_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SALARY_TOTALCOST',
       :OLD.grand_salary_totalcost, :NEW.grand_salary_totalcost);
  END IF;
  IF NVL(:OLD.grand_fringe_cost,' ') !=
     NVL(:NEW.grand_fringe_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_FRINGE_COST',
       :OLD.grand_fringe_cost, :NEW.grand_fringe_cost);
  END IF;
  IF NVL(:OLD.grand_fringe_totalcost,' ') !=
     NVL(:NEW.grand_fringe_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_FRINGE_TOTALCOST',
       :OLD.grand_fringe_totalcost, :NEW.grand_fringe_totalcost);
  END IF;
  IF NVL(:OLD.grand_disc_cost,' ') !=
     NVL(:NEW.grand_disc_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_DISC_COST',
       :OLD.grand_disc_cost, :NEW.grand_disc_cost);
  END IF;
  IF NVL(:OLD.grand_disc_totalcost,' ') !=
     NVL(:NEW.grand_disc_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_DISC_TOTALCOST',
       :OLD.grand_disc_totalcost, :NEW.grand_disc_totalcost);
  END IF;
  IF NVL(:OLD.grand_ind_cost,' ') !=
     NVL(:NEW.grand_ind_cost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_IND_COST',
       :OLD.grand_ind_cost, :NEW.grand_ind_cost);
  END IF;
  IF NVL(:OLD.grand_ind_totalcost,' ') !=
     NVL(:NEW.grand_ind_totalcost,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_IND_TOTALCOST',
       :OLD.grand_ind_totalcost, :NEW.grand_ind_totalcost);
  END IF;
  IF NVL(:OLD.grand_res_sponsor,' ') !=
     NVL(:NEW.grand_res_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_SPONSOR',
       :OLD.grand_res_sponsor, :NEW.grand_res_sponsor);
  END IF;
  IF NVL(:OLD.grand_res_variance,' ') !=
     NVL(:NEW.grand_res_variance,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_RES_VARIANCE',
       :OLD.grand_res_variance, :NEW.grand_res_variance);
  END IF;
  IF NVL(:OLD.grand_soc_sponsor,' ') !=
     NVL(:NEW.grand_soc_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_SPONSOR',
       :OLD.grand_soc_sponsor, :NEW.grand_soc_sponsor);
  END IF;
  IF NVL(:OLD.grand_soc_variance,' ') !=
     NVL(:NEW.grand_soc_variance,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_SOC_VARIANCE',
       :OLD.grand_soc_variance, :NEW.grand_soc_variance);
  END IF;
  IF NVL(:OLD.grand_total_sponsor,' ') !=
     NVL(:NEW.grand_total_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_SPONSOR',
       :OLD.grand_total_sponsor, :NEW.grand_total_sponsor);
  END IF;
  IF NVL(:OLD.grand_total_variance,' ') !=
     NVL(:NEW.grand_total_variance,' ') THEN
     audit_trail.column_update
       (raid, 'GRAND_TOTAL_VARIANCE',
       :OLD.grand_total_variance, :NEW.grand_total_variance);
  END IF;

  IF NVL(:OLD.bgtcal_sp_overhead,0) !=
     NVL(:NEW.bgtcal_sp_overhead,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SP_OVERHEAD',
       :OLD.bgtcal_sp_overhead, :NEW.bgtcal_sp_overhead);
  END IF;

  IF NVL(:OLD.bgtcal_sp_overhead_flag,0) !=
     NVL(:NEW.bgtcal_sp_overhead_flag,0) THEN
     audit_trail.column_update
       (raid, 'BGTCAL_SP_OVERHEAD_FLAG',
       :OLD.bgtcal_sp_overhead_flag, :NEW.bgtcal_sp_overhead_flag);
  END IF;


END;
/
--When SCH_BGTCAL table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_BGTCAL_AU2" 
AFTER UPDATE ON ESCH.SCH_BGTCAL 
REFERENCING NEW AS NEW OLD AS OLD FOR EACH ROW 
DECLARE
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;    
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
	--This Block run when there is Delete operation perform on SCH_BGTCAL
	  IF :NEW.BGTCAL_DELFLAG = 'Y' THEN
	  --Inserting row in AUDIT_ROW_MODULE table.Action - 'D' is used for Logical Deletion of the record.
      PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid, 'SCH_BGTCAL', :OLD.RID,:OLD.FK_BUDGET,'D',:NEW.LAST_MODIFIED_BY);
      
	  --Inserting rows in AUDIT_COLUMN_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'PK_BGTCAL',:OLD.PK_BGTCAL,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'FK_BUDGET',:OLD.FK_BUDGET,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTID', :OLD.BGTCAL_PROTID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTTYPE', :OLD.BGTCAL_PROTTYPE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DELFLAG', :OLD.BGTCAL_DELFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSOROHEAD', :OLD.BGTCAL_SPONSOROHEAD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICOHEAD', :OLD.BGTCAL_CLINICOHEAD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSORFLAG', :OLD.BGTCAL_SPONSORFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICFLAG', :OLD.BGTCAL_CLINICFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_INDIRECTCOST', :OLD.BGTCAL_INDIRECTCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'RID', :OLD.RID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATOR', :OLD.CREATOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'IP_ADD', :OLD.IP_ADD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGBENEFIT', :OLD.BGTCAL_FRGBENEFIT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGFLAG', :OLD.BGTCAL_FRGFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNT', :OLD.BGTCAL_DISCOUNT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNTFLAG', :OLD.BGTCAL_DISCOUNTFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_EXCLDSOCFLAG', :OLD.BGTCAL_EXCLDSOCFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_COST', :OLD.GRAND_RES_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_TOTALCOST', :OLD.GRAND_RES_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_COST', :OLD.GRAND_SOC_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_TOTALCOST', :OLD.GRAND_SOC_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_COST', :OLD.GRAND_TOTAL_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_TOTALCOST', :OLD.GRAND_TOTAL_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_COST', :OLD.GRAND_SALARY_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_TOTALCOST', :OLD.GRAND_SALARY_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_COST', :OLD.GRAND_FRINGE_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_TOTALCOST', :OLD.GRAND_FRINGE_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_COST', :OLD.GRAND_DISC_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_TOTALCOST', :OLD.GRAND_DISC_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_COST', :OLD.GRAND_IND_COST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_TOTALCOST', :OLD.GRAND_IND_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_SPONSOR', :OLD.GRAND_RES_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_VARIANCE', :OLD.GRAND_RES_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_SPONSOR', :OLD.GRAND_SOC_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_VARIANCE', :OLD.GRAND_SOC_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_SPONSOR', :OLD.GRAND_TOTAL_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_VARIANCE', :OLD.GRAND_TOTAL_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD', :OLD.BGTCAL_SP_OVERHEAD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD_FLAG', :OLD.BGTCAL_SP_OVERHEAD_FLAG,NULL,NULL,NULL);
    ELSE
	--This Block execute when there is Update operation perform on SCH_BGTCAL
		--Inserting row in AUDIT_ROW_MODULE table.
		PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid, 'SCH_BGTCAL', :OLD.RID,:OLD.FK_BUDGET,'U',:NEW.LAST_MODIFIED_BY);
	  
		--Inserting rows in AUDIT_COLUMN_MODULE table.
	  IF NVL(:OLD.PK_BGTCAL,0) != NVL(:NEW.PK_BGTCAL,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'PK_BGTCAL',:OLD.PK_BGTCAL, :NEW.PK_BGTCAL,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_BUDGET,0) != NVL(:NEW.FK_BUDGET,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'FK_BUDGET',:OLD.FK_BUDGET, :NEW.FK_BUDGET,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_PROTID,0) != NVL(:NEW.BGTCAL_PROTID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTID', :OLD.BGTCAL_PROTID, :NEW.BGTCAL_PROTID,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_PROTTYPE,' ') != NVL(:NEW.BGTCAL_PROTTYPE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_PROTTYPE', :OLD.BGTCAL_PROTTYPE, :NEW.BGTCAL_PROTTYPE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_DELFLAG,' ') != NVL(:NEW.BGTCAL_DELFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DELFLAG', :OLD.BGTCAL_DELFLAG, :NEW.BGTCAL_DELFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SPONSOROHEAD,0) != NVL(:NEW.BGTCAL_SPONSOROHEAD,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSOROHEAD', :OLD.BGTCAL_SPONSOROHEAD, :NEW.BGTCAL_SPONSOROHEAD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_CLINICOHEAD,0) != NVL(:NEW.BGTCAL_CLINICOHEAD,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICOHEAD', :OLD.BGTCAL_CLINICOHEAD, :NEW.BGTCAL_CLINICOHEAD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SPONSORFLAG,' ') != NVL(:NEW.BGTCAL_SPONSORFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SPONSORFLAG', :OLD.BGTCAL_SPONSORFLAG, :NEW.BGTCAL_SPONSORFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_CLINICFLAG,' ') != NVL(:NEW.BGTCAL_CLINICFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_CLINICFLAG', :OLD.BGTCAL_CLINICFLAG, :NEW.BGTCAL_CLINICFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_INDIRECTCOST,0) != NVL(:NEW.BGTCAL_INDIRECTCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_INDIRECTCOST', :OLD.BGTCAL_INDIRECTCOST, :NEW.BGTCAL_INDIRECTCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATOR', :OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'IP_ADD', :OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_FRGBENEFIT,0) != NVL(:NEW.BGTCAL_FRGBENEFIT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGBENEFIT', :OLD.BGTCAL_FRGBENEFIT, :NEW.BGTCAL_FRGBENEFIT,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_FRGFLAG,0) != NVL(:NEW.BGTCAL_FRGFLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_FRGFLAG', :OLD.BGTCAL_FRGFLAG, :NEW.BGTCAL_FRGFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_DISCOUNT,0) != NVL(:NEW.BGTCAL_DISCOUNT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNT', :OLD.BGTCAL_DISCOUNT, :NEW.BGTCAL_DISCOUNT,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_DISCOUNTFLAG,0) != NVL(:NEW.BGTCAL_DISCOUNTFLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_DISCOUNTFLAG', :OLD.BGTCAL_DISCOUNTFLAG, :NEW.BGTCAL_DISCOUNTFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_EXCLDSOCFLAG,0) != NVL(:NEW.BGTCAL_EXCLDSOCFLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_EXCLDSOCFLAG', :OLD.BGTCAL_EXCLDSOCFLAG, :NEW.BGTCAL_EXCLDSOCFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_COST,' ') != NVL(:NEW.GRAND_RES_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_COST', :OLD.GRAND_RES_COST, :NEW.GRAND_RES_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_TOTALCOST,' ') != NVL(:NEW.GRAND_RES_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_TOTALCOST', :OLD.GRAND_RES_TOTALCOST, :NEW.GRAND_RES_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_COST,' ') != NVL(:NEW.GRAND_SOC_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_COST', :OLD.GRAND_SOC_COST, :NEW.GRAND_SOC_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_TOTALCOST,' ') != NVL(:NEW.GRAND_SOC_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_TOTALCOST', :OLD.GRAND_SOC_TOTALCOST, :NEW.GRAND_SOC_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_COST,' ') != NVL(:NEW.GRAND_TOTAL_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_COST', :OLD.GRAND_TOTAL_COST, :NEW.GRAND_TOTAL_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_TOTALCOST,' ') != NVL(:NEW.GRAND_TOTAL_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_TOTALCOST', :OLD.GRAND_TOTAL_TOTALCOST, :NEW.GRAND_TOTAL_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SALARY_COST,' ') != NVL(:NEW.GRAND_SALARY_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_COST', :OLD.GRAND_SALARY_COST, :NEW.GRAND_SALARY_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SALARY_TOTALCOST,' ') != NVL(:NEW.GRAND_SALARY_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SALARY_TOTALCOST', :OLD.GRAND_SALARY_TOTALCOST, :NEW.GRAND_SALARY_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_FRINGE_COST,' ') != NVL(:NEW.GRAND_FRINGE_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_COST', :OLD.GRAND_FRINGE_COST, :NEW.GRAND_FRINGE_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_FRINGE_TOTALCOST,' ') != NVL(:NEW.GRAND_FRINGE_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_FRINGE_TOTALCOST', :OLD.GRAND_FRINGE_TOTALCOST, :NEW.GRAND_FRINGE_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_DISC_COST,' ') != NVL(:NEW.GRAND_DISC_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_COST', :OLD.GRAND_DISC_COST, :NEW.GRAND_DISC_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_DISC_TOTALCOST,' ') != NVL(:NEW.GRAND_DISC_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_DISC_TOTALCOST', :OLD.GRAND_DISC_TOTALCOST, :NEW.GRAND_DISC_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_IND_COST,' ') != NVL(:NEW.GRAND_IND_COST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_COST', :OLD.GRAND_IND_COST, :NEW.GRAND_IND_COST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_IND_TOTALCOST,' ') != NVL(:NEW.GRAND_IND_TOTALCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_IND_TOTALCOST', :OLD.GRAND_IND_TOTALCOST, :NEW.GRAND_IND_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_SPONSOR,' ') != NVL(:NEW.GRAND_RES_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_SPONSOR', :OLD.GRAND_RES_SPONSOR, :NEW.GRAND_RES_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_RES_VARIANCE,' ') != NVL(:NEW.GRAND_RES_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_RES_VARIANCE', :OLD.GRAND_RES_VARIANCE, :NEW.GRAND_RES_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_SPONSOR,' ') != NVL(:NEW.GRAND_SOC_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_SPONSOR', :OLD.GRAND_SOC_SPONSOR, :NEW.GRAND_SOC_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_SOC_VARIANCE,' ') != NVL(:NEW.GRAND_SOC_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_SOC_VARIANCE', :OLD.GRAND_SOC_VARIANCE, :NEW.GRAND_SOC_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_SPONSOR,' ') != NVL(:NEW.GRAND_TOTAL_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_SPONSOR', :OLD.GRAND_TOTAL_SPONSOR, :NEW.GRAND_TOTAL_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.GRAND_TOTAL_VARIANCE,' ') != NVL(:NEW.GRAND_TOTAL_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'GRAND_TOTAL_VARIANCE', :OLD.GRAND_TOTAL_VARIANCE, :NEW.GRAND_TOTAL_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SP_OVERHEAD,0) != NVL(:NEW.BGTCAL_SP_OVERHEAD,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD', :OLD.BGTCAL_SP_OVERHEAD, :NEW.BGTCAL_SP_OVERHEAD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTCAL_SP_OVERHEAD_FLAG,0) != NVL(:NEW.BGTCAL_SP_OVERHEAD_FLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTCAL', 'BGTCAL_SP_OVERHEAD_FLAG', :OLD.BGTCAL_SP_OVERHEAD_FLAG, :NEW.BGTCAL_SP_OVERHEAD_FLAG,NULL,NULL);
      END IF;
    END IF;
	
    EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_BGTCAL_AU2 ');

END;
/
CREATE OR REPLACE TRIGGER SCH_BGTSECTION_AU0
AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_BGTCAL,CREATED_ON,PK_BUDGETSEC,BGTSECTION_NAME,BGTSECTION_TYPE,BGTSECTION_PATNO,BGTSECTION_VISIT,BGTSECTION_DELFLAG,BGTSECTION_SEQUENCE,BGTSECTION_PERSONLFLAG,FK_VISIT
ON SCH_BGTSECTION REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_BGTSECTION', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_budgetsec,0) !=
     NVL(:NEW.pk_budgetsec,0) THEN
     audit_trail.column_update
       (raid, 'PK_BUDGETSEC',
       :OLD.pk_budgetsec, :NEW.pk_budgetsec);
  END IF;
  IF NVL(:OLD.fk_bgtcal,0) !=
     NVL(:NEW.fk_bgtcal,0) THEN
     audit_trail.column_update
       (raid, 'FK_BGTCAL',
       :OLD.fk_bgtcal, :NEW.fk_bgtcal);
  END IF;
  IF NVL(:OLD.bgtsection_name,' ') !=
     NVL(:NEW.bgtsection_name,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_NAME',
       :OLD.bgtsection_name, :NEW.bgtsection_name);
  END IF;
  IF NVL(:OLD.bgtsection_visit,' ') !=
     NVL(:NEW.bgtsection_visit,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_VISIT',
       :OLD.bgtsection_visit, :NEW.bgtsection_visit);
  END IF;
  IF NVL(:OLD.bgtsection_delflag,' ') !=
     NVL(:NEW.bgtsection_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_DELFLAG',
       :OLD.bgtsection_delflag, :NEW.bgtsection_delflag);
  END IF;
  IF NVL(:OLD.bgtsection_sequence,0) !=
     NVL(:NEW.bgtsection_sequence,0) THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_SEQUENCE',
       :OLD.bgtsection_sequence, :NEW.bgtsection_sequence);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.bgtsection_type,' ') !=
     NVL(:NEW.bgtsection_type,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_TYPE',
       :OLD.bgtsection_type, :NEW.bgtsection_type);
  END IF;
  IF NVL(:OLD.bgtsection_patno, ' ') !=
     NVL(:NEW.bgtsection_patno,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_PATNO',
       :OLD.bgtsection_patno, :NEW.bgtsection_patno);
  END IF;
  IF NVL(:OLD.bgtsection_notes, ' ') !=
     NVL(:NEW.bgtsection_notes,' ') THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_NOTES',
       :OLD.bgtsection_notes, :NEW.bgtsection_notes);
  END IF;

  IF NVL(:OLD.bgtsection_personlflag,0) !=
     NVL(:NEW.bgtsection_personlflag,0) THEN
     audit_trail.column_update
       (raid, 'BGTSECTION_PERSONLFLAG',
       :OLD.bgtsection_personlflag, :NEW.bgtsection_personlflag);
  END IF;
  IF NVL(:OLD.srt_cost_total,' ') !=
     NVL(:NEW.srt_cost_total,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_TOTAL',
       :OLD.srt_cost_total, :NEW.srt_cost_total);
  END IF;
  IF NVL(:OLD.srt_cost_grandtotal,' ') !=
     NVL(:NEW.srt_cost_grandtotal,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_GRANDTOTAL',
       :OLD.srt_cost_grandtotal, :NEW.srt_cost_grandtotal);
  END IF;
  IF NVL(:OLD.soc_cost_total,' ') !=
     NVL(:NEW.soc_cost_total,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_TOTAL',
       :OLD.soc_cost_total, :NEW.soc_cost_total);
  END IF;
  IF NVL(:OLD.soc_cost_grandtotal,' ') !=
     NVL(:NEW.soc_cost_grandtotal,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_GRANDTOTAL',
       :OLD.soc_cost_grandtotal, :NEW.soc_cost_grandtotal);
  END IF;
  IF NVL(:OLD.srt_cost_sponsor,' ') !=
     NVL(:NEW.srt_cost_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_SPONSOR',
       :OLD.srt_cost_sponsor, :NEW.srt_cost_sponsor);
  END IF;
  IF NVL(:OLD.srt_cost_variance,' ') !=
     NVL(:NEW.srt_cost_variance,' ') THEN
     audit_trail.column_update
       (raid, 'SRT_COST_VARIANCE',
       :OLD.srt_cost_variance, :NEW.srt_cost_variance);
  END IF;
  IF NVL(:OLD.soc_cost_sponsor,' ') !=
     NVL(:NEW.soc_cost_sponsor,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_SPONSOR',
       :OLD.soc_cost_sponsor, :NEW.soc_cost_sponsor);
  END IF;
  IF NVL(:OLD.soc_cost_variance,' ') !=
     NVL(:NEW.soc_cost_variance,' ') THEN
     audit_trail.column_update
       (raid, 'SOC_COST_VARIANCE',
       :OLD.soc_cost_variance, :NEW.soc_cost_variance);
  END IF;

    IF NVL(:OLD.FK_VISIT,0) !=
     NVL(:NEW.FK_VISIT,0) THEN
     audit_trail.column_update
       (raid, 'FK_VISIT',
       :OLD.FK_VISIT, :NEW.FK_VISIT);
  END IF;

END;
/
--When SCH_BGTSECTION table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER ESCH."SCH_BGTSECTION_AU1" 
AFTER UPDATE ON ESCH.SCH_BGTSECTION 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;    
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
	--This Block run when there is Logical Delete operation perform on SCH_BGTSECTION
    IF :NEW.BGTSECTION_DELFLAG='Y' THEN
	  --Inserting row in AUDIT_ROW_MODULE table. 'D' is used for Logical Deletion of the record
      PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_BGTSECTION',:OLD.RID,NULL,'D',:NEW.LAST_MODIFIED_BY);
	  
	  --Inserting rows in AUDIT_COLUMN_MODULE table.
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','PK_BUDGETSEC',:OLD.PK_BUDGETSEC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','FK_BGTCAL',:OLD.FK_BGTCAL,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_NAME',:OLD.BGTSECTION_NAME,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_VISIT',:OLD.BGTSECTION_VISIT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_DELFLAG',:OLD.BGTSECTION_DELFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_SEQUENCE',:OLD.BGTSECTION_SEQUENCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','RID',:OLD.RID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','CREATOR',:OLD.CREATOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','LAST_MODIFIED_DATE',to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','CREATED_ON',To_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_TYPE',:OLD.BGTSECTION_TYPE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_PATNO',:OLD.BGTSECTION_PATNO,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_NOTES',:OLD.BGTSECTION_NOTES,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_PERSONLFLAG',:OLD.BGTSECTION_PERSONLFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_TOTAL',:OLD.SRT_COST_TOTAL,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_GRANDTOTAL',:OLD.SRT_COST_GRANDTOTAL,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_TOTAL',:OLD.SOC_COST_TOTAL,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_GRANDTOTAL',:OLD.SOC_COST_GRANDTOTAL,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_SPONSOR',:OLD.SRT_COST_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_VARIANCE',:OLD.SRT_COST_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_SPONSOR',:OLD.SOC_COST_SPONSOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_VARIANCE',:OLD.SOC_COST_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','FK_VISIT',:OLD.FK_VISIT,NULL,NULL,NULL);
    ELSE
	--This Block run when there is Update operation perform on SCH_BGTSECTION
		--Inserting row in the table AUDIT_ROW_MODULE.
		PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'SCH_BGTSECTION',:OLD.RID,NULL,'U',:NEW.LAST_MODIFIED_BY);

		--Inserting row in the table AUDIT_Column_MODULE	
      IF NVL(:OLD.PK_BUDGETSEC,0) != NVL(:NEW.PK_BUDGETSEC,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','PK_BUDGETSEC',:OLD.PK_BUDGETSEC,:NEW.PK_BUDGETSEC,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_BGTCAL,0) != NVL(:NEW.FK_BGTCAL,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','FK_BGTCAL',:OLD.FK_BGTCAL,:NEW.FK_BGTCAL,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_NAME,' ') != NVL(:NEW.BGTSECTION_NAME,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_NAME',:OLD.BGTSECTION_NAME,:NEW.BGTSECTION_NAME,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_VISIT,'') != NVL(:NEW.BGTSECTION_VISIT,'') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_VISIT',:OLD.BGTSECTION_VISIT,:NEW.BGTSECTION_VISIT,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_DELFLAG,' ') != NVL(:NEW.BGTSECTION_DELFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_DELFLAG',:OLD.BGTSECTION_DELFLAG,:NEW.BGTSECTION_DELFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_SEQUENCE,0) != NVL(:NEW.BGTSECTION_SEQUENCE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_SEQUENCE',:OLD.BGTSECTION_SEQUENCE,:NEW.BGTSECTION_SEQUENCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','RID',:OLD.RID,:NEW.RID,NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
      END IF;
      --IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
      --END IF;
      IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','LAST_MODIFIED_DATE',to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),to_char(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','CREATED_ON',to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),to_char(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_TYPE,' ') != NVL(:NEW.BGTSECTION_TYPE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_TYPE',:OLD.BGTSECTION_TYPE,:NEW.BGTSECTION_TYPE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_PATNO,' ') != NVL(:NEW.BGTSECTION_PATNO,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_PATNO',:OLD.BGTSECTION_PATNO,:NEW.BGTSECTION_PATNO,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_NOTES,' ') != NVL(:NEW.BGTSECTION_NOTES,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_NOTES',:OLD.BGTSECTION_NOTES,:NEW.BGTSECTION_NOTES,NULL,NULL);
      END IF;
      IF NVL(:OLD.BGTSECTION_PERSONLFLAG,0) != NVL(:NEW.BGTSECTION_PERSONLFLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','BGTSECTION_PERSONLFLAG',:OLD.BGTSECTION_PERSONLFLAG,:NEW.BGTSECTION_PERSONLFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.SRT_COST_TOTAL,' ') != NVL(:NEW.SRT_COST_TOTAL,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_TOTAL',:OLD.SRT_COST_TOTAL,:NEW.SRT_COST_TOTAL,NULL,NULL);
      END IF;
      IF NVL(:OLD.SRT_COST_GRANDTOTAL,' ') != NVL(:NEW.SRT_COST_GRANDTOTAL,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_GRANDTOTAL',:OLD.SRT_COST_GRANDTOTAL,:NEW.SRT_COST_GRANDTOTAL,NULL,NULL);
      END IF;
      IF NVL(:OLD.SOC_COST_TOTAL,' ') != NVL(:NEW.SOC_COST_TOTAL,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_TOTAL',:OLD.SOC_COST_TOTAL,:NEW.SOC_COST_TOTAL,NULL,NULL);
      END IF;
      IF NVL(:OLD.SOC_COST_GRANDTOTAL,' ') != NVL(:NEW.SOC_COST_GRANDTOTAL,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_GRANDTOTAL',:OLD.SOC_COST_GRANDTOTAL,:NEW.SOC_COST_GRANDTOTAL,NULL,NULL);
      END IF;
      IF NVL(:OLD.SRT_COST_SPONSOR,' ') != NVL(:NEW.SRT_COST_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_SPONSOR',:OLD.SRT_COST_SPONSOR,:NEW.SRT_COST_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.SRT_COST_VARIANCE,' ') != NVL(:NEW.SRT_COST_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SRT_COST_VARIANCE',:OLD.SRT_COST_VARIANCE,:NEW.SRT_COST_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.SOC_COST_SPONSOR,' ') != NVL(:NEW.SOC_COST_SPONSOR,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_SPONSOR',:OLD.SOC_COST_SPONSOR,:NEW.SOC_COST_SPONSOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.SOC_COST_VARIANCE,' ') != NVL(:NEW.SOC_COST_VARIANCE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','SOC_COST_VARIANCE',:OLD.SOC_COST_VARIANCE,:NEW.SOC_COST_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_VISIT,0) != NVL(:NEW.soc_cost_variance,0) THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTSECTION','FK_VISIT',:OLD.FK_VISIT,:NEW.FK_VISIT,NULL,NULL);
      END IF;
    END IF;
	
EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_BGTSECTION_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_BGTUSERS_AU0
AFTER UPDATE OF BGTUSERS_RIGHTS,BGTUSERS_TYPE,CREATED_ON,CREATOR,FK_BUDGET,FK_USER,IP_ADD,PK_BGTUSERS,RID
ON SCH_BGTUSERS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_BGTUSERS', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);
  IF NVL(:OLD.pk_bgtusers,0) !=
     NVL(:NEW.pk_bgtusers,0) THEN
     audit_trail.column_update
       (raid, 'PK_BGTUSERS',
       :OLD.pk_bgtusers, :NEW.pk_bgtusers);
  END IF;
  IF NVL(:OLD.fk_budget,0) !=
     NVL(:NEW.fk_budget,0) THEN
     audit_trail.column_update
       (raid, 'FK_BUDGET',
       :OLD.fk_budget, :NEW.fk_budget);
  END IF;
  IF NVL(:OLD.fk_user,0) !=
     NVL(:NEW.fk_user,0) THEN
     audit_trail.column_update
       (raid, 'FK_USER',
       :OLD.fk_user, :NEW.fk_user);
  END IF;
  IF NVL(:OLD.bgtusers_rights,' ') !=
     NVL(:NEW.bgtusers_rights,' ') THEN
     audit_trail.column_update
       (raid, 'BGTUSERS_RIGHTS',
       :OLD.bgtusers_rights, :NEW.bgtusers_rights);
  END IF;
  IF NVL(:OLD.bgtusers_type,' ') !=
     NVL(:NEW.bgtusers_type,' ') THEN
     audit_trail.column_update
       (raid, 'BGTUSERS_TYPE',
       :OLD.bgtusers_type, :NEW.bgtusers_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;
/
--When records are updated in SCH_BGTUSERS table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE
TRIGGER ESCH."SCH_BGTUSERS_AU1" 
AFTER UPDATE ON ESCH.SCH_BGTUSERS 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION; 
BEGIN	
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
	 --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid, 'SCH_BGTUSERS', :OLD.RID,:OLD.FK_BUDGET,'U',:NEW.LAST_MODIFIED_BY);
	
	 --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_BGTUSERS,0) != NVL(:NEW.PK_BGTUSERS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'PK_BGTUSERS',:OLD.PK_BGTUSERS, :NEW.PK_BGTUSERS,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_BUDGET,0) != NVL(:NEW.FK_BUDGET,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'FK_BUDGET',:OLD.FK_BUDGET, :NEW.FK_BUDGET,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_USER,0) != NVL(:NEW.FK_USER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'FK_USER',:OLD.FK_USER, :NEW.FK_USER,NULL,NULL);
    END IF;
    IF NVL(:OLD.BGTUSERS_RIGHTS,' ') != NVL(:NEW.BGTUSERS_RIGHTS,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'BGTUSERS_RIGHTS',:OLD.BGTUSERS_RIGHTS, :NEW.BGTUSERS_RIGHTS,NULL,NULL);
    END IF;
    IF NVL(:OLD.BGTUSERS_TYPE,' ') != NVL(:NEW.BGTUSERS_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'BGTUSERS_TYPE',:OLD.BGTUSERS_TYPE, :NEW.BGTUSERS_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'RID',:OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'LAST_MODIFIED_DATE',TO_CHAR(:old.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'CREATED_ON',TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BGTUSERS', 'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
    END IF;
	
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_BGTUSERS_AU1 ');
END;
/
create or replace TRIGGER SCH_BUDGET_AU0
AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_SITE,FK_STUDY,PK_BUDGET,FK_ACCOUNT,BUDGET_DESC,BUDGET_NAME,BUDGET_RIGHTS,BUDGET_STATUS,BUDGET_CALFLAG,BUDGET_CREATOR,BUDGET_DELFLAG,BUDGET_VERSION,BUDGET_CURRENCY,BUDGET_SITEFLAG,BUDGET_TEMPLATE,LAST_MODIFIED_BY,BUDGET_RIGHTSCOPE
ON ESCH.SCH_BUDGET REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_BUDGET', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_budget,0) !=
     NVL(:NEW.pk_budget,0) THEN
     audit_trail.column_update
       (raid, 'PK_BUDGET',
       :OLD.pk_budget, :NEW.pk_budget);
  END IF;
  IF NVL(:OLD.budget_name,' ') !=
     NVL(:NEW.budget_name,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_NAME',
       :OLD.budget_name, :NEW.budget_name);
  END IF;
  IF NVL(:OLD.budget_version,' ') !=
     NVL(:NEW.budget_version,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_VERSION',
       :OLD.budget_version, :NEW.budget_version);
  END IF;
  IF NVL(:OLD.budget_desc,' ') !=
     NVL(:NEW.budget_desc,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_DESC',
       :OLD.budget_desc, :NEW.budget_desc);
  END IF;
  IF NVL(:OLD.budget_creator,0) !=
     NVL(:NEW.budget_creator,0) THEN
     audit_trail.column_update
       (raid, 'BUDGET_CREATOR',
       :OLD.budget_creator, :NEW.budget_creator);
  END IF;
  IF NVL(:OLD.budget_template,0) !=
     NVL(:NEW.budget_template,0) THEN
     audit_trail.column_update
       (raid, 'BUDGET_TEMPLATE',
       :OLD.budget_template, :NEW.budget_template);
  END IF;
  IF NVL(:OLD.budget_status,' ') !=
     NVL(:NEW.budget_status,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_STATUS',
       :OLD.budget_status, :NEW.budget_status);
  END IF;
  IF NVL(:OLD.budget_currency,0) !=
     NVL(:NEW.budget_currency,0) THEN
     audit_trail.column_update
       (raid, 'BUDGET_CURRENCY',
       :OLD.budget_currency, :NEW.budget_currency);
  END IF;
  IF NVL(:OLD.budget_siteflag,' ') !=
     NVL(:NEW.budget_siteflag,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_SITEFLAG',
       :OLD.budget_siteflag, :NEW.budget_siteflag);
  END IF;
  IF NVL(:OLD.budget_calflag,' ') !=
     NVL(:NEW.budget_calflag,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_CALFLAG',
       :OLD.budget_calflag, :NEW.budget_calflag);
  END IF;
   IF NVL(:OLD.budget_delflag,' ') !=
     NVL(:NEW.budget_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_DELFLAG',
       :OLD.budget_delflag, :NEW.budget_delflag);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.fk_account,0) !=
     NVL(:NEW.fk_account,0) THEN
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :OLD.fk_account, :NEW.fk_account);
  END IF;
  IF NVL(:OLD.fk_site,0) !=
     NVL(:NEW.fk_site,0) THEN
     audit_trail.column_update
       (raid, 'FK_SITE',
       :OLD.fk_site, :NEW.fk_site);
  END IF;
  IF NVL(:OLD.budget_rightscope,' ') !=
     NVL(:NEW.budget_rightscope,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_RIGHTSCOPE',
       :OLD.budget_rightscope, :NEW.budget_rightscope);
  END IF;
  IF NVL(:OLD.budget_rights,' ') !=
     NVL(:NEW.budget_rights,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_RIGHTS',
       :OLD.budget_rights, :NEW.budget_rights);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.FK_CODELST_STATUS,0) !=
     NVL(:NEW.FK_CODELST_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STATUS',
       :OLD.FK_CODELST_STATUS, :NEW.FK_CODELST_STATUS);
  END IF;

  IF NVL(:OLD.BUDGET_COMBFLAG,' ') !=
     NVL(:NEW.BUDGET_COMBFLAG,' ') THEN
     audit_trail.column_update
       (raid, 'BUDGET_COMBFLAG',
       :OLD.BUDGET_COMBFLAG, :NEW.BUDGET_COMBFLAG);
  END IF;
END;
/
--When records are updated on SCH_BUDGET table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER ESCH.SCH_BUDGET_AU3
AFTER UPDATE ON ESCH.SCH_BUDGET 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE 
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */
    V_BUDGET_CURRENCY_OLD VARCHAR2(200) :=NULL;
    V_BUDGET_TEMPLATE_OLD VARCHAR2(200) :=NULL;
    V_FK_CODELST_STATUS_OLD VARCHAR2(200):=NULL;
    V_BUDGET_CURRENCY_NEW VARCHAR2(200):=NULL;
    V_BUDGET_TEMPLATE_NEW VARCHAR2(200) :=NULL;
    V_FK_CODELST_STATUS_NEW VARCHAR2(200):=NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;   
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
  
    --This Block run when there is Logical Delete operation perform on SCH_BUDGET
    IF :NEW.budget_delflag = 'Y' THEN
      --Inserting row in AUDIT_ROW_MODULE table. 'D' is used for Logical Deletion of the record
      PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_BUDGET',:OLD.RID,:OLD.PK_BUDGET,'D',:NEW.LAST_MODIFIED_BY);      
      --Inserting row in AUDIT_COLUMN_MODULE table.
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','PK_BUDGET',:OLD.PK_BUDGET,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_NAME',:OLD.BUDGET_NAME,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_VERSION',:OLD.BUDGET_VERSION,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_DESC',:OLD.BUDGET_DESC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CREATOR',:OLD.BUDGET_CREATOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_TYPE',:OLD.BUDGET_TYPE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_STATUS',:OLD.BUDGET_STATUS,NULL,NULL,NULL);
      
      IF (NVL(:OLD.BUDGET_CURRENCY,0) > 0) THEN
          V_BUDGET_CURRENCY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.BUDGET_CURRENCY);  
      END IF;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CURRENCY',V_BUDGET_CURRENCY_OLD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_SITEFLAG',:OLD.BUDGET_SITEFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CALFLAG',:OLD.BUDGET_CALFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_STUDY',:OLD.FK_STUDY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_ACCOUNT',:OLD.FK_ACCOUNT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_SITE',:OLD.FK_SITE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_RIGHTSCOPE',:OLD.BUDGET_RIGHTSCOPE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_RIGHTS',:OLD.BUDGET_RIGHTS,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','RID',:OLD.RID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','CREATOR',:OLD.CREATOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','LAST_MODIFIED_DATE', to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
      
      IF (NVL(:OLD.BUDGET_TEMPLATE,0) > 0) THEN
          V_BUDGET_TEMPLATE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.BUDGET_TEMPLATE);  
      END IF;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_TEMPLATE',V_BUDGET_TEMPLATE_OLD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_DELFLAG',:OLD.BUDGET_DELFLAG,NULL,NULL,NULL);
      
      IF (NVL(:OLD.FK_CODELST_STATUS,0) > 0) THEN
          V_FK_CODELST_STATUS_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_STATUS);  
      END IF;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_CODELST_STATUS',V_FK_CODELST_STATUS_OLD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CALENDAR',:OLD.BUDGET_CALENDAR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_COMBFLAG',:OLD.BUDGET_COMBFLAG,NULL,NULL,NULL);
    ELSE
        --Inserting row in AUDIT_ROW_MODULE table.
        PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_BUDGET',:OLD.RID,:OLD.PK_BUDGET,'U',:NEW.LAST_MODIFIED_BY);    
      
        --Inserting rows in AUDIT_COLUMN_MODULE table.
      IF NVL(:OLD.PK_BUDGET,0) != NVL(:NEW.PK_BUDGET,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','PK_BUDGET',:OLD.PK_BUDGET,:NEW.PK_BUDGET,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_NAME,' ') != NVL(:NEW.BUDGET_NAME,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_NAME',:OLD.BUDGET_NAME,:NEW.BUDGET_NAME,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_VERSION,' ') != NVL(:NEW.BUDGET_VERSION,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_VERSION',:OLD.BUDGET_VERSION,:NEW.BUDGET_VERSION,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_DESC,' ') != NVL(:NEW.BUDGET_DESC,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_DESC',:OLD.BUDGET_DESC,:NEW.BUDGET_DESC,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_CREATOR,0) != NVL(:NEW.BUDGET_CREATOR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CREATOR',:OLD.BUDGET_CREATOR,:NEW.BUDGET_CREATOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_TYPE,' ') != NVL(:NEW.BUDGET_TYPE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_TYPE',:OLD.BUDGET_TYPE,:NEW.BUDGET_TYPE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_STATUS,' ') != NVL(:NEW.BUDGET_STATUS,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_STATUS',:OLD.BUDGET_STATUS,:NEW.BUDGET_STATUS,NULL,NULL);
      END IF;
      
        IF (NVL(:OLD.BUDGET_CURRENCY,0) > 0) THEN
          V_BUDGET_CURRENCY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.BUDGET_CURRENCY);  
        END IF;
        IF (NVL(:NEW.BUDGET_CURRENCY,0)>0) THEN
            V_BUDGET_CURRENCY_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.BUDGET_CURRENCY);  
        END IF;
      
      IF NVL(:OLD.BUDGET_CURRENCY,0) != NVL(:NEW.BUDGET_CURRENCY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CURRENCY',V_BUDGET_CURRENCY_OLD,V_BUDGET_CURRENCY_NEW,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_SITEFLAG,' ') != NVL(:NEW.BUDGET_SITEFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_SITEFLAG',:OLD.BUDGET_SITEFLAG,:NEW.BUDGET_SITEFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_CALFLAG,' ') != NVL(:NEW.BUDGET_CALFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CALFLAG',:OLD.BUDGET_CALFLAG,:NEW.BUDGET_CALFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_STUDY,0) != NVL(:NEW.FK_STUDY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_STUDY',:OLD.FK_STUDY,:NEW.FK_STUDY,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_ACCOUNT,0) != NVL(:NEW.FK_ACCOUNT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_ACCOUNT',:OLD.FK_ACCOUNT,:NEW.FK_ACCOUNT,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_SITE,0) != NVL(:NEW.FK_SITE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_SITE',:OLD.FK_SITE,:NEW.FK_SITE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_RIGHTSCOPE,'') != NVL(:NEW.BUDGET_RIGHTSCOPE,'') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_RIGHTSCOPE',:OLD.BUDGET_RIGHTSCOPE,:NEW.BUDGET_RIGHTSCOPE,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_RIGHTS,' ') != NVL(:NEW.BUDGET_RIGHTS,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_RIGHTS',:OLD.BUDGET_RIGHTS,:NEW.BUDGET_RIGHTS,NULL,NULL);
      END IF;
      IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','RID',:OLD.RID,:NEW.RID,NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
      END IF;
      --IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
      --END IF;
      IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
      END IF;
      
       IF (NVL(:OLD.BUDGET_TEMPLATE,0) > 0) THEN
          V_BUDGET_TEMPLATE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.BUDGET_TEMPLATE);  
        END IF;
        IF (NVL(:NEW.BUDGET_TEMPLATE,0)>0) THEN
            V_BUDGET_TEMPLATE_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.BUDGET_TEMPLATE);  
        END IF;
        
      IF NVL(:OLD.BUDGET_TEMPLATE,0) != NVL(:NEW.BUDGET_TEMPLATE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_TEMPLATE',V_BUDGET_TEMPLATE_OLD,V_BUDGET_TEMPLATE_NEW,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_DELFLAG,' ') != NVL(:NEW.BUDGET_DELFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'SCH_BUDGET','BUDGET_DELFLAG',:OLD.BUDGET_DELFLAG,:NEW.BUDGET_DELFLAG,NULL,NULL);
      END IF;
      
        IF (NVL(:OLD.FK_CODELST_STATUS,0) > 0) THEN
          V_FK_CODELST_STATUS_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_STATUS);  
        END IF;
        IF (NVL(:NEW.FK_CODELST_STATUS,0)>0) THEN
            V_FK_CODELST_STATUS_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_STATUS);  
        END IF;
      IF NVL(:OLD.FK_CODELST_STATUS,0) != NVL(:NEW.FK_CODELST_STATUS,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','FK_CODELST_STATUS',V_FK_CODELST_STATUS_OLD,V_FK_CODELST_STATUS_NEW,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_CALENDAR,0) != NVL(:NEW.BUDGET_CALENDAR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_CALENDAR',:OLD.BUDGET_CALENDAR,:NEW.BUDGET_CALENDAR,NULL,NULL);
      END IF;
      IF NVL(:OLD.BUDGET_COMBFLAG,' ') != NVL(:NEW.BUDGET_COMBFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_BUDGET','BUDGET_COMBFLAG',:OLD.BUDGET_COMBFLAG,:NEW.BUDGET_COMBFLAG,NULL,NULL);
      END IF;
    END IF;
	
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_BUDGET_AU3 ');
	
END;
/
CREATE OR REPLACE TRIGGER SCH_CODELST_AU0
AFTER UPDATE
ON SCH_CODELST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CODELST', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_codelst,0) !=
      NVL(:NEW.pk_codelst,0) THEN
      audit_trail.column_update
        (raid, 'PK_CODELST',
        :OLD.pk_codelst, :NEW.pk_codelst);
   END IF;
   IF NVL(:OLD.codelst_type,' ') !=
      NVL(:NEW.codelst_type,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_TYPE',
        :OLD.codelst_type, :NEW.codelst_type);
   END IF;
   IF NVL(:OLD.codelst_subtyp,' ') !=
      NVL(:NEW.codelst_subtyp,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_SUBTYP',
        :OLD.codelst_subtyp, :NEW.codelst_subtyp);
   END IF;
   IF NVL(:OLD.codelst_desc,' ') !=
      NVL(:NEW.codelst_desc,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_DESC',
        :OLD.codelst_desc, :NEW.codelst_desc);
   END IF;
   IF NVL(:OLD.codelst_hide,' ') !=
      NVL(:NEW.codelst_hide,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_HIDE',
        :OLD.codelst_hide, :NEW.codelst_hide);
   END IF;
   IF NVL(:OLD.codelst_seq,0) !=
      NVL(:NEW.codelst_seq,0) THEN
      audit_trail.column_update
        (raid, 'CODELST_SEQ',
        :OLD.codelst_seq, :NEW.codelst_seq);
   END IF;
   IF NVL(:OLD.codelst_maint,' ') !=
      NVL(:NEW.codelst_maint,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_MAINT',
        :OLD.codelst_maint, :NEW.codelst_maint);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.fk_account,0) !=
      NVL(:NEW.fk_account,0) THEN
      audit_trail.column_update
        (raid, 'FK_ACCOUNT',
        :OLD.fk_account, :NEW.fk_account);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

   IF NVL(:OLD.CODELST_CUSTOM_COL1,' ') !=
      NVL(:NEW.CODELST_CUSTOM_COL1,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_CUSTOM_COL1',
        :OLD.CODELST_CUSTOM_COL1, :NEW.CODELST_CUSTOM_COL1);
   END IF;

   IF NVL(:OLD.CODELST_STUDY_ROLE,' ') !=
      NVL(:NEW.CODELST_STUDY_ROLE,' ') THEN
      audit_trail.column_update
        (raid, 'CODELST_STUDY_ROLE',
        :OLD.CODELST_STUDY_ROLE, :NEW.CODELST_STUDY_ROLE);
   END IF;


END;
/
CREATE OR REPLACE TRIGGER SCH_CRFLIB_AU0
AFTER UPDATE
ON SCH_CRFLIB REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CRFLIB', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_crflib,0) !=
      NVL(:NEW.pk_crflib,0) THEN
      audit_trail.column_update
        (raid, 'PK_CRFLIB',
        :OLD.pk_crflib, :NEW.pk_crflib);
   END IF;
   IF NVL(:OLD.fk_events,0) !=
      NVL(:NEW.fk_events,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENTS',
        :OLD.fk_events, :NEW.fk_events);
   END IF;
   IF NVL(:OLD.crflib_number,' ') !=
      NVL(:NEW.crflib_number,' ') THEN
      audit_trail.column_update
        (raid, 'CRFLIB_NUMBER',
        :OLD.crflib_number, :NEW.crflib_number);
   END IF;
   IF NVL(:OLD.crflib_name,' ') !=
      NVL(:NEW.crflib_name,' ') THEN
      audit_trail.column_update
        (raid, 'CRFLIB_NAME',
        :OLD.crflib_name, :NEW.crflib_name);
   END IF;
   IF NVL(:OLD.crflib_flag,' ') !=
      NVL(:NEW.crflib_flag,' ') THEN
      audit_trail.column_update
        (raid, 'CRFLIB_FLAG',
        :OLD.crflib_flag, :NEW.crflib_flag);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
--When SCH_CRFLIB table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_CRFLIB_AU2" AFTER
UPDATE ON ESCH.SCH_CRFLIB 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
  v_rowid NUMBER(10);   /*variable used to fetch value of sequence */
  v_chainid number(10);
  v_ErrorType VARCHAR2(20):='EXCEPTION';
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
	v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:NEW.FK_EVENTS);    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
	
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_CRFLIB
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_CRFLIB',:OLD.RID,v_chainid,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_CRFLIB,0) != NVL(:NEW.PK_CRFLIB,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','PK_CRFLIB',:OLD.PK_CRFLIB,:NEW.PK_CRFLIB,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENTS,0) != NVL(:NEW.FK_EVENTS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','FK_EVENTS',:OLD.FK_EVENTS,:NEW.FK_EVENTS,NULL,NULL);
    END IF;
    IF NVL(:OLD.CRFLIB_NUMBER,' ') != NVL(:NEW.CRFLIB_NUMBER,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','CRFLIB_NUMBER',:OLD.CRFLIB_NUMBER,:NEW.CRFLIB_NUMBER,NULL,NULL);
    END IF;
    IF NVL(:OLD.CRFLIB_NAME,' ') != NVL(:NEW.CRFLIB_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','CRFLIB_NAME',:OLD.CRFLIB_NAME,:NEW.CRFLIB_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.CRFLIB_FLAG,' ') != NVL(:NEW.CRFLIB_FLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','CRFLIB_FLAG',:OLD.CRFLIB_FLAG,:NEW.CRFLIB_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,0) != NVL(:NEW.IP_ADD,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.CRFLIB_FORMFLAG,0) != NVL(:NEW.CRFLIB_FORMFLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','CRFLIB_FORMFLAG',:OLD.CRFLIB_FORMFLAG,:NEW.CRFLIB_FORMFLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPAGATE_FROM,0) != NVL(:NEW.PROPAGATE_FROM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_CRFLIB','PROPAGATE_FROM',:OLD.PROPAGATE_FROM,:NEW.PROPAGATE_FROM,NULL,NULL);
    END IF;
	
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_CRFLIB_AU2 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_CRFNOTIFY_AU0
AFTER UPDATE
ON SCH_CRFNOTIFY REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CRFNOTIFY', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_crfnot,0) !=
      NVL(:NEW.pk_crfnot,0) THEN
      audit_trail.column_update
        (raid, 'PK_CRFNOT',
        :OLD.pk_crfnot, :NEW.pk_crfnot);
   END IF;
   IF NVL(:OLD.fk_crf,0) !=
      NVL(:NEW.fk_crf,0) THEN
      audit_trail.column_update
        (raid, 'FK_CRF',
        :OLD.fk_crf, :NEW.fk_crf);
   END IF;
   IF NVL(:OLD.fk_codelst_notstat,0) !=
      NVL(:NEW.fk_codelst_notstat,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_NOTSTAT',
        :OLD.fk_codelst_notstat, :NEW.fk_codelst_notstat);
   END IF;
   IF NVL(:OLD.crfnot_usersto,' ') !=
      NVL(:NEW.crfnot_usersto,' ') THEN
      audit_trail.column_update
        (raid, 'CRFNOT_USERSTO',
        :OLD.crfnot_usersto, :NEW.crfnot_usersto);
   END IF;
   IF NVL(:OLD.crfnot_notes,' ') !=
      NVL(:NEW.crfnot_notes,' ') THEN
      audit_trail.column_update
        (raid, 'CRFNOT_NOTES',
        :OLD.crfnot_notes, :NEW.crfnot_notes);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
                    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_study,0) !=
      NVL(:NEW.fk_study,0) THEN
      audit_trail.column_update
        (raid, 'FK_STUDY',
        :OLD.fk_study, :NEW.fk_study);
   END IF;
   IF NVL(:OLD.fk_protocol,0) !=
      NVL(:NEW.fk_protocol,0) THEN
      audit_trail.column_update
        (raid, 'FK_PROTOCOL',
        :OLD.fk_protocol, :NEW.fk_protocol);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.crfnot_globalflag,' ') !=
      NVL(:NEW.crfnot_globalflag,' ') THEN
      audit_trail.column_update
        (raid, 'CRFNOT_GLOBALFLAG',
        :OLD.crfnot_globalflag, :NEW.crfnot_globalflag);
   END IF;

END;
/
CREATE OR REPLACE TRIGGER SCH_CRFSTAT_AU0
AFTER UPDATE
ON SCH_CRFSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CRFSTAT', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_crfstat,0) !=
      NVL(:NEW.pk_crfstat,0) THEN
      audit_trail.column_update
        (raid, 'PK_CRFSTAT',
        :OLD.pk_crfstat, :NEW.pk_crfstat);
   END IF;
   IF NVL(:OLD.fk_crf,0) !=
      NVL(:NEW.fk_crf,0) THEN
      audit_trail.column_update
        (raid, 'FK_CRF',
        :OLD.fk_crf, :NEW.fk_crf);
   END IF;
   IF NVL(:OLD.fk_codelst_crfstat,0) !=
      NVL(:NEW.fk_codelst_crfstat,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_CRFSTAT',
        :OLD.fk_codelst_crfstat, :NEW.fk_codelst_crfstat);
   END IF;
   IF NVL(:OLD.crfstat_enterby,0) !=
      NVL(:NEW.crfstat_enterby,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_ENTERBY',
        :OLD.crfstat_enterby, :NEW.crfstat_enterby);
   END IF;
   IF NVL(:OLD.crfstat_reviewby,0) !=
      NVL(:NEW.crfstat_reviewby,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_REVIEWBY',
        :OLD.crfstat_reviewby, :NEW.crfstat_reviewby);
   END IF;
   IF NVL(:OLD.crfstat_reviewon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.crfstat_reviewon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_REVIEWON',
        to_char(:OLD.crfstat_reviewon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.crfstat_reviewon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.crfstat_sentto,' ') !=
      NVL(:NEW.crfstat_sentto,' ') THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTTO',
        :OLD.crfstat_sentto, :NEW.crfstat_sentto);
   END IF;
   IF NVL(:OLD.crfstat_sentflag,0) !=
      NVL(:NEW.crfstat_sentflag,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTFLAG',
        :OLD.crfstat_sentflag, :NEW.crfstat_sentflag);
   END IF;
   IF NVL(:OLD.crfstat_sentby,0) !=
      NVL(:NEW.crfstat_sentby,0) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTBY',
        :OLD.crfstat_sentby, :NEW.crfstat_sentby);
   END IF;
   IF NVL(:OLD.crfstat_senton,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.crfstat_senton,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CRFSTAT_SENTON',
        to_char(:OLD.crfstat_senton,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.crfstat_senton,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
                    to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
CREATE OR REPLACE TRIGGER SCH_CRF_AU0
AFTER UPDATE
ON SCH_CRF REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_CRF', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_crf,0) !=
      NVL(:NEW.pk_crf,0) THEN
      audit_trail.column_update
        (raid, 'PK_CRF',
        :OLD.pk_crf, :NEW.pk_crf);
   END IF;
   IF NVL(:OLD.fk_events1,' ') !=
      NVL(:NEW.fk_events1,' ') THEN
      audit_trail.column_update
        (raid, 'FK_EVENTS1',
        :OLD.fk_events1, :NEW.fk_events1);
   END IF;
   IF NVL(:OLD.crf_number,' ') !=
      NVL(:NEW.crf_number,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_NUMBER',
        :OLD.crf_number, :NEW.crf_number);
   END IF;
   IF NVL(:OLD.crf_name,' ') !=
      NVL(:NEW.crf_name,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_NAME',
        :OLD.crf_name, :NEW.crf_name);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.crf_flag,' ') !=
      NVL(:NEW.crf_flag,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_FLAG',
        :OLD.crf_flag, :NEW.crf_flag);
   END IF;
   IF NVL(:OLD.crf_delflag,' ') !=
      NVL(:NEW.crf_delflag,' ') THEN
      audit_trail.column_update
        (raid, 'CRF_DELFLAG',
        :OLD.crf_delflag, :NEW.crf_delflag);
   END IF;

END;
/
CREATE OR REPLACE TRIGGER "SCH_DISPATCHMSG_AU0" 
AFTER UPDATE
ON SCH_DISPATCHMSG REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

 audit_trail.record_transaction
    (raid, 'SCH_DISPATCHMSG', :OLD.rid, 'U');
/*
  if nvl(:old.pk_msg,0) !=
     NVL(:new.pk_msg,0) then
     audit_trail.column_update
       (raid, 'PK_MSG',
       :old.pk_msg, :new.pk_msg);
  end if;

  if nvl(trim(:old.msg_sendon),TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(trim(:new.msg_sendon),TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'MSG_SENDON',
       :old.msg_sendon, :new.msg_sendon);
  end if;
*/

  IF NVL(:OLD.msg_status,0) !=
     NVL(:NEW.msg_status,0) THEN
     audit_trail.column_update
       (raid, 'MSG_STATUS',
       :OLD.msg_status, :NEW.msg_status);
  END IF;

/*
  if nvl(:old.msg_type,' ') !=
     NVL(:new.msg_type,' ') then
     audit_trail.column_update
       (raid, 'MSG_TYPE',
       :old.msg_type, :new.msg_type);
  end if;
  if nvl(:old.fk_event,0) !=
     NVL(:new.fk_event,0) then
     audit_trail.column_update
       (raid, 'FK_EVENT',
       :old.fk_event, :new.fk_event);
  end if;
  if nvl(:old.msg_text,' ') !=
     NVL(:new.msg_text,' ') then
     audit_trail.column_update
       (raid, 'MSG_TEXT',
       :old.msg_text, :new.msg_text);
  end if;
  if nvl(:old.fk_pat,0) !=
     NVL(:new.fk_pat,0) then
     audit_trail.column_update
       (raid, 'FK_PAT',
       :old.fk_pat, :new.fk_pat);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       :old.last_modified_date, :new.last_modified_date);
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       :old.created_on, :new.created_on);

  end if;
  */
END;
/
CREATE OR REPLACE TRIGGER SCH_DOCS_AU0
AFTER UPDATE
ON SCH_DOCS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_DOCS', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_docs,0) !=
      NVL(:NEW.pk_docs,0) THEN
      audit_trail.column_update
        (raid, 'PK_DOCS',
        :OLD.pk_docs, :NEW.pk_docs);
   END IF;
   IF NVL(:OLD.doc_name,' ') !=
      NVL(:NEW.doc_name,' ') THEN
      audit_trail.column_update
        (raid, 'DOC_NAME',
        :OLD.doc_name, :NEW.doc_name);
   END IF;
   IF NVL(:OLD.doc_desc,' ') !=
      NVL(:NEW.doc_desc,' ') THEN
      audit_trail.column_update
        (raid, 'DOC_DESC',
        :OLD.doc_desc, :NEW.doc_desc);
   END IF;
   IF NVL(:OLD.doc_type,' ') !=
      NVL(:NEW.doc_type,' ') THEN
      audit_trail.column_update
        (raid, 'DOC_TYPE',
        :OLD.doc_type, :NEW.doc_type);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
        to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.doc_size,0) !=
      NVL(:NEW.doc_size,0) THEN
      audit_trail.column_update
        (raid, 'DOC_SIZE',
        :OLD.doc_size, :NEW.doc_size);
   END IF;

END;
/
--When SCH_DOCS table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_DOCS_AU1" 
AFTER UPDATE ON ESCH.SCH_DOCS 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10); 	/*variable used to fetch value of sequence */
  	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_DOCS
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_DOCS',:OLD.RID,NULL,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_DOCS,0) != NVL(:NEW.PK_DOCS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','PK_DOCS',:OLD.PK_DOCS,:NEW.PK_DOCS,NULL,NULL);
    END IF;
    IF NVL(:OLD.DOC_NAME,' ') != NVL(:NEW.DOC_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','DOC_NAME',:OLD.DOC_NAME,:NEW.DOC_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.DOC_DESC,' ') != NVL(:NEW.DOC_DESC,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','DOC_DESC',:OLD.DOC_DESC,:NEW.DOC_DESC,NULL,NULL);
    END IF;
    IF NVL(:OLD.DOC_TYPE,' ') != NVL(:NEW.DOC_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','DOC_TYPE',:OLD.DOC_TYPE,:NEW.DOC_TYPE,NULL,NULL);
    END IF;
	--IF NVL(:OLD.DOC,' ') != NVL(:NEW.DOC,' ') THEN
	-- BLOB FIELD BELOW      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','DOC',NULL,:NEW.PK_EVENTKIT,NULL,NULL);
	--END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.DOC_SIZE,0) != NVL(:NEW.DOC_SIZE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_DOCS','DOC_SIZE',:OLD.DOC_SIZE,:NEW.DOC_SIZE,NULL,NULL);
    END IF;
	
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_DOCS_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_EVENTCOST_AU0
AFTER UPDATE
ON SCH_EVENTCOST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTCOST', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventcost,0) !=
      NVL(:NEW.pk_eventcost,0) THEN
      audit_trail.column_update
        (raid, 'PK_EVENTCOST',
        :OLD.pk_eventcost, :NEW.pk_eventcost);
   END IF;
   IF NVL(:OLD.eventcost_value,0) !=
      NVL(:NEW.eventcost_value,0) THEN
      audit_trail.column_update
        (raid, 'EVENTCOST_VALUE',
        :OLD.eventcost_value, :NEW.eventcost_value);
   END IF;
   IF NVL(:OLD.fk_event,0) !=
      NVL(:NEW.fk_event,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   IF NVL(:OLD.fk_cost_desc,0) !=
      NVL(:NEW.fk_cost_desc,0) THEN
      audit_trail.column_update
        (raid, 'FK_COST_DESC',
        :OLD.fk_cost_desc, :NEW.fk_cost_desc);
   END IF;
   IF NVL(:OLD.fk_currency,0) !=
      NVL(:NEW.fk_currency,0) THEN
      audit_trail.column_update
        (raid, 'FK_CURRENCY',
        :OLD.fk_currency, :NEW.fk_currency);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
     to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
    to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
--When SCH_EVENTCOST table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTCOST_AU2" 
AFTER UPDATE ON ESCH.SCH_EVENTCOST 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	v_chainid number(10);
	V_FK_COST_DESC_OLD VARCHAR2(200) :=NULL;
	V_FK_CURRENCY_OLD VARCHAR2(200) :=NULL;
	V_FK_COST_DESC_NEW VARCHAR2(200) :=NULL;
	V_FK_CURRENCY_NEW VARCHAR2(200) :=NULL;
 	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    --Chain ID from event_assoc will be used as module id
    v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:NEW.FK_EVENT);
    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENTCOST
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENTCOST',:OLD.RID,v_chainid,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTCOST,0) != NVL(:NEW.PK_EVENTCOST,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','PK_EVENTCOST',:OLD.PK_EVENTCOST,:NEW.PK_EVENTCOST,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTCOST_VALUE,0) != NVL(:NEW.EVENTCOST_VALUE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','EVENTCOST_VALUE',:OLD.EVENTCOST_VALUE,:NEW.EVENTCOST_VALUE,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,0) != NVL(:NEW.FK_EVENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
		IF (NVL(:OLD.FK_COST_DESC,0) > 0) THEN
          V_FK_COST_DESC_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_COST_DESC);  
        END IF;
        IF (NVL(:NEW.FK_COST_DESC,0)>0) THEN
            V_FK_COST_DESC_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_COST_DESC);  
        END IF;	
    IF NVL(:OLD.FK_COST_DESC,0) != NVL(:NEW.FK_COST_DESC,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','FK_COST_DESC',V_FK_COST_DESC_OLD,V_FK_COST_DESC_NEW,NULL,NULL);
    END IF;
		IF (NVL(:OLD.FK_CURRENCY,0) > 0) THEN
          V_FK_CURRENCY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CURRENCY);  
        END IF;
        IF (NVL(:NEW.FK_CURRENCY,0)>0) THEN
            V_FK_CURRENCY_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CURRENCY);  
        END IF;	
    IF NVL(:OLD.FK_CURRENCY,0) != NVL(:NEW.FK_CURRENCY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','FK_CURRENCY',V_FK_CURRENCY_OLD,V_FK_CURRENCY_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPAGATE_FROM,0) != NVL(:NEW.PROPAGATE_FROM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTCOST','PROPAGATE_FROM',:OLD.PROPAGATE_FROM,:NEW.PROPAGATE_FROM,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENTCOST_AU2 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_EVENTDOC_AU0
AFTER UPDATE
ON SCH_EVENTDOC REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTDOC', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventdoc,0) !=
      NVL(:NEW.pk_eventdoc,0) THEN
      audit_trail.column_update
        (raid, 'PK_EVENTDOC',
        :OLD.pk_eventdoc, :NEW.pk_eventdoc);
   END IF;
   IF NVL(:OLD.pk_docs,0) !=
      NVL(:NEW.pk_docs,0) THEN
      audit_trail.column_update
        (raid, 'PK_DOCS',
        :OLD.pk_docs, :NEW.pk_docs);
   END IF;
   IF NVL(:OLD.fk_event,0) !=
      NVL(:NEW.fk_event,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
   to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
   to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
--When SCH_EVENTDOC table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTDOC_AU1" 
AFTER UPDATE ON ESCH.SCH_EVENTDOC 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	v_chainid number(10);
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    --Chain ID from event_assoc will be used as module id
    v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:OLD.FK_EVENT);    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENTDOC
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENTDOC',:OLD.RID,v_chainid,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTDOC,0) != NVL(:NEW.PK_EVENTDOC,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','PK_EVENTDOC',:OLD.PK_EVENTDOC,:NEW.PK_EVENTDOC,NULL,NULL);
    END IF;
    IF NVL(:OLD.PK_DOCS,0) != NVL(:NEW.PK_DOCS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','PK_DOCS',:OLD.PK_DOCS,:NEW.PK_DOCS,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,0) != NVL(:NEW.FK_EVENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPAGATE_FROM,0) != NVL(:NEW.PROPAGATE_FROM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTDOC','PROPAGATE_FROM',:OLD.PROPAGATE_FROM,:NEW.PROPAGATE_FROM,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENTDOC_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_AU0" AFTER UPDATE ON ESCH.SCH_EVENTS1 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTS1', :OLD.rid, 'U', usr );

   IF NVL(:OLD.event_id,' ') !=
      NVL(:NEW.event_id,' ') THEN
      audit_trail.column_update
        (raid, 'EVENT_ID',
        :OLD.event_id, :NEW.event_id);
   END IF;
   IF NVL(:OLD.object_id,' ') !=
      NVL(:NEW.object_id,' ') THEN
      audit_trail.column_update
        (raid, 'OBJECT_ID',
        :OLD.object_id, :NEW.object_id);
   END IF;
   IF NVL(:OLD.visit_type_id,' ') !=
      NVL(:NEW.visit_type_id,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE_ID',
        :OLD.visit_type_id, :NEW.visit_type_id);
   END IF;
   IF NVL(:OLD.child_service_id,' ') !=
      NVL(:NEW.child_service_id,' ') THEN
      audit_trail.column_update
        (raid, 'CHILD_SERVICE_ID',
        :OLD.child_service_id, :NEW.child_service_id);
   END IF;
   IF NVL(:OLD.session_id,' ') !=
      NVL(:NEW.session_id,' ') THEN
      audit_trail.column_update
        (raid, 'SESSION_ID',
        :OLD.session_id, :NEW.session_id);
   END IF;
   IF NVL(:OLD.occurence_id,' ') !=
      NVL(:NEW.occurence_id,' ') THEN
      audit_trail.column_update
        (raid, 'OCCURENCE_ID',
        :OLD.occurence_id, :NEW.occurence_id);
   END IF;
   IF NVL(:OLD.location_id,' ') !=
      NVL(:NEW.location_id,' ') THEN
      audit_trail.column_update
        (raid, 'LOCATION_ID',
        :OLD.location_id, :NEW.location_id);
   END IF;
   IF NVL(:OLD.notes,' ') !=
      NVL(:NEW.notes,' ') THEN
      audit_trail.column_update
        (raid, 'NOTES',
        :OLD.notes, :NEW.notes);
   END IF;
   IF NVL(:OLD.type_id,' ') !=
      NVL(:NEW.type_id,' ') THEN
      audit_trail.column_update
        (raid, 'TYPE_ID',
        :OLD.type_id, :NEW.type_id);
   END IF;
   IF NVL(:OLD.ROLES,0) !=
      NVL(:NEW.ROLES,0) THEN
      audit_trail.column_update
        (raid, 'ROLES',
        :OLD.ROLES, :NEW.ROLES);
   END IF;
   IF NVL(:OLD.svc_type_id,' ') !=
      NVL(:NEW.svc_type_id,' ') THEN
      audit_trail.column_update
        (raid, 'SVC_TYPE_ID',
        :OLD.svc_type_id, :NEW.svc_type_id);
   END IF;
   IF NVL(:OLD.status,0) !=
      NVL(:NEW.status,0) THEN
      audit_trail.column_update
        (raid, 'STATUS',
        :OLD.status, :NEW.status);
   END IF;
   IF NVL(:OLD.service_id,' ') !=
      NVL(:NEW.service_id,' ') THEN
      audit_trail.column_update
        (raid, 'SERVICE_ID',
        :OLD.service_id, :NEW.service_id);
   END IF;
   IF NVL(:OLD.booked_by,' ') !=
      NVL(:NEW.booked_by,' ') THEN
      audit_trail.column_update
        (raid, 'BOOKED_BY',
        :OLD.booked_by, :NEW.booked_by);
   END IF;
   IF NVL(:OLD.isconfirmed,0) !=
      NVL(:NEW.isconfirmed,0) THEN
      audit_trail.column_update
        (raid, 'ISCONFIRMED',
        :OLD.isconfirmed, :NEW.isconfirmed);
   END IF;
   IF NVL(:OLD.description,' ') !=
      NVL(:NEW.description,' ') THEN
      audit_trail.column_update
        (raid, 'DESCRIPTION',
        :OLD.description, :NEW.description);
   END IF;
   IF NVL(:OLD.attended,0) !=
      NVL(:NEW.attended,0) THEN
      audit_trail.column_update
        (raid, 'ATTENDED',
        :OLD.attended, :NEW.attended);
   END IF;
   IF NVL(:OLD.start_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.start_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'START_DATE_TIME',
        to_char(:OLD.start_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.start_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.end_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.end_date_time,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'END_DATE_TIME',
        to_char(:OLD.end_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.end_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.bookedon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.bookedon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'BOOKEDON',
        to_char(:OLD.bookedon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.bookedon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.iswaitlisted,0) !=
      NVL(:NEW.iswaitlisted,0) THEN
      audit_trail.column_update
        (raid, 'ISWAITLISTED',
        :OLD.iswaitlisted, :NEW.iswaitlisted);
   END IF;
   IF NVL(:OLD.object_name,' ') !=
      NVL(:NEW.object_name,' ') THEN
      audit_trail.column_update
        (raid, 'OBJECT_NAME',
        :OLD.object_name, :NEW.object_name);
   END IF;
   IF NVL(:OLD.location_name,' ') !=
      NVL(:NEW.location_name,' ') THEN
      audit_trail.column_update
        (raid, 'LOCATION_NAME',
        :OLD.location_name, :NEW.location_name);
   END IF;
   IF NVL(:OLD.session_name,' ') !=
      NVL(:NEW.session_name,' ') THEN
      audit_trail.column_update
        (raid, 'SESSION_NAME',
        :OLD.session_name, :NEW.session_name);
   END IF;
   IF NVL(:OLD.user_name,' ') !=
      NVL(:NEW.user_name,' ') THEN
      audit_trail.column_update
        (raid, 'USER_NAME',
        :OLD.user_name, :NEW.user_name);
   END IF;
   IF NVL(:OLD.patient_id,' ') !=
      NVL(:NEW.patient_id,' ') THEN
      audit_trail.column_update
        (raid, 'PATIENT_ID',
        :OLD.patient_id, :NEW.patient_id);
   END IF;
   IF NVL(:OLD.svc_type_name,' ') !=
      NVL(:NEW.svc_type_name,' ') THEN
      audit_trail.column_update
        (raid, 'SVC_TYPE_NAME',
        :OLD.svc_type_name, :NEW.svc_type_name);
   END IF;
   IF NVL(:OLD.event_num,0) !=
      NVL(:NEW.event_num,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_NUM',
        :OLD.event_num, :NEW.event_num);
   END IF;
   IF NVL(:OLD.visit_type_name,' ') !=
      NVL(:NEW.visit_type_name,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE_NAME',
        :OLD.visit_type_name, :NEW.visit_type_name);
   END IF;
   IF NVL(:OLD.obj_location_id,' ') !=
      NVL(:NEW.obj_location_id,' ') THEN
      audit_trail.column_update
        (raid, 'OBJ_LOCATION_ID',
        :OLD.obj_location_id, :NEW.obj_location_id);
   END IF;
   IF NVL(:OLD.fk_assoc,0) !=
      NVL(:NEW.fk_assoc,0) THEN
      audit_trail.column_update
        (raid, 'FK_ASSOC',
        :OLD.fk_assoc, :NEW.fk_assoc);
   END IF;
   IF NVL(:OLD.event_exeon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.event_exeon,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'EVENT_EXEON',
        to_char(:OLD.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.event_exeby,0) !=
      NVL(:NEW.event_exeby,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_EXEBY',
        :OLD.event_exeby, :NEW.event_exeby);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.actual_schdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.actual_schdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'ACTUAL_SCHDATE',
        to_char(:OLD.actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.adverse_count,0) !=
      NVL(:NEW.adverse_count,0) THEN
      audit_trail.column_update
        (raid, 'ADVERSE_COUNT',
        :OLD.adverse_count, :NEW.adverse_count);
   END IF;
   IF NVL(:OLD.visit,0) !=
      NVL(:NEW.visit,0) THEN
      audit_trail.column_update
        (raid, 'VISIT',
        :OLD.visit, :NEW.visit);
   END IF;

   IF NVL(:OLD.alnot_sent,0) !=
      NVL(:NEW.alnot_sent,0) THEN
      audit_trail.column_update
        (raid, 'ALNOT_SENT',
        :OLD.alnot_sent, :NEW.alnot_sent);
   END IF;

   IF NVL(:OLD.fk_visit,0) !=
      NVL(:NEW.fk_visit,0) THEN
      audit_trail.column_update
        (raid, 'FK_VISIT',
        :OLD.fk_visit, :NEW.fk_visit);
   END IF;

  --JM: 02/16/2007
  IF NVL(:OLD.fk_study,0) !=
      NVL(:NEW.fk_study,0) THEN
      audit_trail.column_update
        (raid, 'FK_STUDY',
        :OLD.fk_study, :NEW.fk_study);
   END IF;

   IF NVL(:OLD.event_notes,0) !=
      NVL(:NEW.event_notes,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_NOTES',
        :OLD.event_notes, :NEW.event_notes);
   END IF;

   IF NVL(:OLD.SERVICE_SITE_ID,0) !=
      NVL(:NEW.SERVICE_SITE_ID,0) THEN
      audit_trail.column_update
	(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
   END IF;

   IF NVL(:OLD.FACILITY_ID,0) !=
      NVL(:NEW.FACILITY_ID,0) THEN
      audit_trail.column_update
	(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
   END IF;

   IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
      NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
      audit_trail.column_update
	(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
   END IF;


   IF NVL(:OLD.REASON_FOR_COVERAGECHANGE,0) !=
      NVL(:NEW.REASON_FOR_COVERAGECHANGE,0) THEN
      audit_trail.column_update
	(raid, 'REASON_FOR_COVERAGECHANGE',:OLD.REASON_FOR_COVERAGECHANGE, :NEW.REASON_FOR_COVERAGECHANGE);
   END IF;

END;
/
--When SCH_EVENTS1 table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_AU2" 
AFTER UPDATE ON ESCH.SCH_EVENTS1 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
	v_rowid NUMBER(10); /*variable used to fetch value of sequence */
	V_ISCONFIRMED_OLD VARCHAR2(200) :=NULL;
	V_FK_CODELST_COVERTYPE_OLD VARCHAR2(200) := NULL;
	V_ISCONFIRMED_NEW VARCHAR2(200) :=NULL;
	V_FK_CODELST_COVERTYPE_NEW VARCHAR2(200) := NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;    
    --This Block run when there is Update operation perform on SCH_EVENTS1
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENTS1',:OLD.RID,:OLD.SESSION_ID,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.EVENT_ID,' ') != NVL(:NEW.EVENT_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','EVENT_ID',:OLD.EVENT_ID,:NEW.EVENT_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.OBJECT_ID,' ') != NVL(:NEW.OBJECT_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','OBJECT_ID',:OLD.OBJECT_ID,:NEW.OBJECT_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.VISIT_TYPE_ID,' ') != NVL(:NEW.VISIT_TYPE_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','VISIT_TYPE_ID',:OLD.VISIT_TYPE_ID,:NEW.VISIT_TYPE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CHILD_SERVICE_ID,' ') != NVL(:NEW.CHILD_SERVICE_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','CHILD_SERVICE_ID',:OLD.CHILD_SERVICE_ID,:NEW.CHILD_SERVICE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.SESSION_ID,' ') != NVL(:NEW.SESSION_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','SESSION_ID',:OLD.SESSION_ID,:NEW.SESSION_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.OCCURENCE_ID,' ') != NVL(:NEW.OCCURENCE_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','OCCURENCE_ID',:OLD.OCCURENCE_ID,:NEW.OCCURENCE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.LOCATION_ID,' ') != NVL(:NEW.LOCATION_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','LOCATION_ID',:OLD.LOCATION_ID,:NEW.LOCATION_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.NOTES,' ') != NVL(:NEW.NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','NOTES',:OLD.NOTES,:NEW.NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.TYPE_ID,' ') != NVL(:NEW.TYPE_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','TYPE_ID',:OLD.TYPE_ID,:NEW.TYPE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.ROLES,0) != NVL(:NEW.ROLES,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','ROLES',:OLD.ROLES,:NEW.ROLES,NULL,NULL);
    END IF;
    IF NVL(:OLD.SVC_TYPE_ID,' ') != NVL(:NEW.SVC_TYPE_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','SVC_TYPE_ID',:OLD.SVC_TYPE_ID,:NEW.SVC_TYPE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.STATUS,0) != NVL(:NEW.STATUS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','STATUS',:OLD.STATUS,:NEW.STATUS,NULL,NULL);
    END IF;
    IF NVL(:OLD.SERVICE_ID,' ') != NVL(:NEW.SERVICE_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','SERVICE_ID',:OLD.SERVICE_ID,:NEW.SERVICE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.BOOKED_BY,' ') != NVL(:NEW.BOOKED_BY,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','BOOKED_BY',:OLD.BOOKED_BY,:NEW.BOOKED_BY,NULL,NULL);
    END IF;
		IF (NVL(:OLD.ISCONFIRMED,0) > 0) THEN
          V_ISCONFIRMED_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.ISCONFIRMED);  
        END IF;
        IF (NVL(:NEW.ISCONFIRMED,0)>0) THEN
            V_ISCONFIRMED_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.ISCONFIRMED);  
        END IF;		
    IF NVL(:OLD.ISCONFIRMED,0) != NVL(:NEW.ISCONFIRMED,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','ISCONFIRMED',V_ISCONFIRMED_OLD,V_ISCONFIRMED_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.DESCRIPTION,' ') != NVL(:NEW.DESCRIPTION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','DESCRIPTION',:OLD.DESCRIPTION,:NEW.DESCRIPTION,NULL,NULL);
    END IF;
    IF NVL(:OLD.ATTENDED,0) != NVL(:NEW.ATTENDED,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','ATTENDED',:OLD.ATTENDED,:NEW.ATTENDED,NULL,NULL);
    END IF;
    IF NVL(:OLD.START_DATE_TIME,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.START_DATE_TIME,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','START_DATE_TIME',TO_CHAR(:OLD.START_DATE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.START_DATE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.END_DATE_TIME,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.END_DATE_TIME,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','END_DATE_TIME',TO_CHAR(:OLD.END_DATE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.END_DATE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.BOOKEDON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.BOOKEDON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','BOOKEDON',TO_CHAR(:OLD.BOOKEDON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.BOOKEDON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.ISWAITLISTED,0) != NVL(:NEW.ISWAITLISTED,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','ISWAITLISTED',:OLD.ISWAITLISTED,:NEW.ISWAITLISTED,NULL,NULL);
    END IF;
    IF NVL(:OLD.OBJECT_NAME,' ') != NVL(:NEW.OBJECT_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','OBJECT_NAME',:OLD.OBJECT_NAME,:NEW.OBJECT_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.LOCATION_NAME,' ') != NVL(:NEW.LOCATION_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','LOCATION_NAME',:OLD.LOCATION_NAME,:NEW.LOCATION_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.SESSION_NAME,' ') != NVL(:NEW.SESSION_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','SESSION_NAME',:OLD.SESSION_NAME,:NEW.SESSION_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.USER_NAME,' ') != NVL(:NEW.USER_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','USER_NAME',:OLD.USER_NAME,:NEW.USER_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.PATIENT_ID,' ') != NVL(:NEW.PATIENT_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','PATIENT_ID',:OLD.PATIENT_ID,:NEW.PATIENT_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.SVC_TYPE_NAME,' ') != NVL(:NEW.SVC_TYPE_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','SVC_TYPE_NAME',:OLD.SVC_TYPE_NAME,:NEW.SVC_TYPE_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_NUM,0) != NVL(:NEW.EVENT_NUM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','EVENT_NUM',:OLD.EVENT_NUM,:NEW.EVENT_NUM,NULL,NULL);
    END IF;
    IF NVL(:OLD.VISIT_TYPE_NAME,' ') != NVL(:NEW.VISIT_TYPE_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','VISIT_TYPE_NAME',:OLD.VISIT_TYPE_NAME,:NEW.VISIT_TYPE_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.OBJ_LOCATION_ID,' ') != NVL(:NEW.OBJ_LOCATION_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','OBJ_LOCATION_ID',:OLD.OBJ_LOCATION_ID,:NEW.OBJ_LOCATION_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_ASSOC,0) != NVL(:NEW.FK_ASSOC,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','FK_ASSOC',:OLD.FK_ASSOC,:NEW.FK_ASSOC,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_EXEON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.EVENT_EXEON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','EVENT_EXEON',TO_CHAR(:OLD.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_EXEBY,0) != NVL(:NEW.EVENT_EXEBY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','EVENT_EXEBY',:OLD.EVENT_EXEBY,:NEW.EVENT_EXEBY,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_PATPROT,0) != NVL(:NEW.FK_PATPROT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','FK_PATPROT',:OLD.FK_PATPROT,:NEW.FK_PATPROT,NULL,NULL);
    END IF;
    IF NVL(:OLD.ACTUAL_SCHDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.ACTUAL_SCHDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','ACTUAL_SCHDATE',TO_CHAR(:OLD.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.ADVERSE_COUNT,0) != NVL(:NEW.ADVERSE_COUNT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','ADVERSE_COUNT',:OLD.ADVERSE_COUNT,:NEW.ADVERSE_COUNT,NULL,NULL);
    END IF;
    IF NVL(:OLD.VISIT,0) != NVL(:NEW.VISIT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','VISIT',:OLD.VISIT,:NEW.VISIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.ALNOT_SENT,0) != NVL(:NEW.ALNOT_SENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','ALNOT_SENT',:OLD.ALNOT_SENT,:NEW.ALNOT_SENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_VISIT,0) != NVL(:NEW.FK_VISIT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','FK_VISIT',:OLD.FK_VISIT,:NEW.FK_VISIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_STUDY,0) != NVL(:NEW.FK_STUDY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','FK_STUDY',:OLD.FK_STUDY,:NEW.FK_STUDY,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_NOTES,' ') != NVL(:NEW.EVENT_NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT(v_rowid,'SCH_EVENTS1','EVENT_NOTES',:OLD.EVENT_NOTES,:NEW.EVENT_NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENT_SEQUENCE,0) != NVL(:NEW.EVENT_SEQUENCE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','EVENT_SEQUENCE',:OLD.EVENT_SEQUENCE,:NEW.EVENT_SEQUENCE,NULL,NULL);
    END IF;
    IF NVL(:OLD.SERVICE_SITE_ID,0) != NVL(:NEW.SERVICE_SITE_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID,:NEW.SERVICE_SITE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.FACILITY_ID,0) != NVL(:NEW.FACILITY_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','FACILITY_ID',:OLD.FACILITY_ID,:NEW.FACILITY_ID,NULL,NULL);
    END IF;
		IF (NVL(:OLD.FK_CODELST_COVERTYPE,0) > 0) THEN
          V_FK_CODELST_COVERTYPE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_COVERTYPE);  
        END IF;
        IF (NVL(:NEW.FK_CODELST_COVERTYPE,0)>0) THEN
            V_FK_CODELST_COVERTYPE_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_COVERTYPE);  
        END IF;
    IF NVL(:OLD.FK_CODELST_COVERTYPE,0) != NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','FK_CODELST_COVERTYPE',V_FK_CODELST_COVERTYPE_OLD,V_FK_CODELST_COVERTYPE_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.REASON_FOR_COVERAGECHANGE,' ') != NVL(:NEW.REASON_FOR_COVERAGECHANGE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTS1','REASON_FOR_COVERAGECHANGE',:OLD.REASON_FOR_COVERAGECHANGE,:NEW.REASON_FOR_COVERAGECHANGE,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENTS1_AU2 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_EVENTSTAT_AU0
AFTER UPDATE
ON SCH_EVENTSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTSTAT', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventstat,0) !=
      NVL(:NEW.pk_eventstat,0) THEN
      audit_trail.column_update
        (raid, 'PK_EVENTSTAT',
        :OLD.pk_eventstat, :NEW.pk_eventstat);
   END IF;
   IF NVL(:OLD.eventstat_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.eventstat_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT_DT',
        to_char(:OLD.eventstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.eventstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.eventstat_notes,' ') !=
      NVL(:NEW.eventstat_notes,' ') THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT_NOTES',
        :OLD.eventstat_notes, :NEW.eventstat_notes);
   END IF;
   IF NVL(:OLD.fk_event,' ') !=
      NVL(:NEW.fk_event,' ') THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
   to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
   to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.eventstat,0) !=
      NVL(:NEW.eventstat,0) THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT',
        :OLD.eventstat, :NEW.eventstat);
   END IF;
   IF NVL(:OLD.eventstat_enddt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.eventstat_enddt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT_ENDDT',
        to_char(:OLD.eventstat_enddt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.eventstat_enddt,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;

END;
/
--When SCH_EVENTSTAT table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTSTAT_AU1" 
AFTER UPDATE ON ESCH.SCH_EVENTSTAT 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	V_EVENTSTAT_OLD VARCHAR2(200) :=NULL;
	V_EVENTSTAT_NEW VARCHAR2(200) :=NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENTSTAT
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENTSTAT',:OLD.RID,NULL,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTSTAT,0) != NVL(:NEW.PK_EVENTSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','PK_EVENTSTAT',:OLD.PK_EVENTSTAT,:NEW.PK_EVENTSTAT,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTSTAT_DT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.EVENTSTAT_DT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT_DT',TO_CHAR(:OLD.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTSTAT_NOTES,' ') != NVL(:NEW.EVENTSTAT_NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT_NOTES',:OLD.EVENTSTAT_NOTES,:NEW.EVENTSTAT_NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,' ') != NVL(:NEW.FK_EVENT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
		IF (NVL(:OLD.EVENTSTAT,0) > 0) THEN
	      V_EVENTSTAT_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.EVENTSTAT);  
		END IF;
		IF (NVL(:NEW.EVENTSTAT,0) > 0) THEN
	      V_EVENTSTAT_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.EVENTSTAT);  
		END IF;
	
    IF NVL(:OLD.EVENTSTAT,0) != NVL(:NEW.EVENTSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT',V_EVENTSTAT_OLD,V_EVENTSTAT_NEW,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTSTAT_ENDDT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.EVENTSTAT_ENDDT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTSTAT','EVENTSTAT_ENDDT',TO_CHAR(:OLD.EVENTSTAT_ENDDT,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.EVENTSTAT_ENDDT,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENTSTAT_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_EVENTUSR_AU0
AFTER UPDATE
ON SCH_EVENTUSR REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

  raid NUMBER(10);

  usr VARCHAR2(100);



BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);



  audit_trail.record_transaction

    (raid, 'SCH_EVENTUSR', :OLD.rid, 'U', usr );



   IF NVL(:OLD.pk_eventusr,0) !=

      NVL(:NEW.pk_eventusr,0) THEN

      audit_trail.column_update

        (raid, 'PK_EVENTUSR',

        :OLD.pk_eventusr, :NEW.pk_eventusr);

   END IF;

   IF NVL(:OLD.eventusr,0) !=

      NVL(:NEW.eventusr,0) THEN

      audit_trail.column_update

        (raid, 'EVENTUSR',

        :OLD.eventusr, :NEW.eventusr);

   END IF;

   IF NVL(:OLD.eventusr_type,' ') !=

      NVL(:NEW.eventusr_type,' ') THEN

      audit_trail.column_update

        (raid, 'EVENTUSR_TYPE',

        :OLD.eventusr_type, :NEW.eventusr_type);

   END IF;

   IF NVL(:OLD.fk_event,0) !=

      NVL(:NEW.fk_event,0) THEN

      audit_trail.column_update

        (raid, 'FK_EVENT',

        :OLD.fk_event, :NEW.fk_event);

   END IF;

   IF NVL(:OLD.rid,0) !=

      NVL(:NEW.rid,0) THEN

      audit_trail.column_update

        (raid, 'RID',

        :OLD.rid, :NEW.rid);

   END IF;

   IF NVL(:OLD.last_modified_by,0) !=

      NVL(:NEW.last_modified_by,0) THEN

      audit_trail.column_update

        (raid, 'LAST_MODIFIED_BY',

        :OLD.last_modified_by, :NEW.last_modified_by);

   END IF;

   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

      audit_trail.column_update

        (raid, 'LAST_MODIFIED_DATE',

                    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));


   END IF;

   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

      audit_trail.column_update

        (raid, 'CREATED_ON',

                    to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));


   END IF;



   IF NVL(:OLD.ip_add,' ') !=  NVL(:NEW.ip_add,' ') THEN

      audit_trail.column_update

        (raid, 'IP_ADD',

        :OLD.ip_add, :NEW.ip_add);

   END IF;



    IF NVL(:OLD.eventusr_duration,' ') !=  NVL(:NEW.eventusr_duration,' ') THEN

      audit_trail.column_update

        (raid, 'eventusr_duration',

        :OLD.eventusr_duration, :NEW.eventusr_duration);

   END IF;



    IF NVL(:OLD.eventusr_notes,' ') !=  NVL(:NEW.eventusr_notes,' ') THEN

      audit_trail.column_update

        (raid, 'eventusr_notes',

        :OLD.eventusr_notes, :NEW.eventusr_notes);

   END IF;


END;
/
--When SCH_EVENTUSR table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTUSR_AU1" 
AFTER UPDATE ON ESCH.SCH_EVENTUSR 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	v_chainid number(10);
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    --Chain ID from event_assoc will be used as module id
    v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:NEW.FK_EVENT);
    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENTUSR
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENTUSR',:OLD.RID,v_chainid,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTUSR,0) != NVL(:NEW.PK_EVENTUSR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','PK_EVENTUSR',:OLD.PK_EVENTUSR,:NEW.PK_EVENTUSR,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTUSR,0) != NVL(:NEW.EVENTUSR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','EVENTUSR',:OLD.EVENTUSR,:NEW.EVENTUSR,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTUSR_TYPE,' ') != NVL(:NEW.EVENTUSR_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','EVENTUSR_TYPE',:OLD.EVENTUSR_TYPE,:NEW.EVENTUSR_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,0) != NVL(:NEW.FK_EVENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTUSR_DURATION,' ') != NVL(:NEW.EVENTUSR_DURATION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','EVENTUSR_DURATION',:OLD.EVENTUSR_DURATION,:NEW.EVENTUSR_DURATION,NULL,NULL);
    END IF;
    IF NVL(:OLD.EVENTUSR_NOTES,' ') != NVL(:NEW.EVENTUSR_NOTES,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','EVENTUSR_NOTES',:OLD.EVENTUSR_NOTES,:NEW.EVENTUSR_NOTES,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPAGATE_FROM,0) != NVL(:NEW.PROPAGATE_FROM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENTUSR','PROPAGATE_FROM',:OLD.PROPAGATE_FROM,:NEW.PROPAGATE_FROM,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENTUSR_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENT_CRF_AU0"
AFTER UPDATE
ON ESCH.SCH_EVENT_CRF REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENT_CRF', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventcrf,0) !=
      NVL(:NEW.pk_eventcrf,0) THEN
      audit_trail.column_update
        (raid, 'PK_eventCRF',
        :OLD.pk_eventcrf, :NEW.pk_eventcrf);
   END IF;
   IF NVL(:OLD.fk_event,' ') !=
      NVL(:NEW.fk_event,' ') THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   IF NVL(:OLD.fk_form,0) !=
      NVL(:NEW.fk_form,0) THEN
      audit_trail.column_update
        (raid, 'FK_FORM',
        :OLD.fk_form, :NEW.fk_form);
   END IF;
   IF NVL(:OLD.form_type,' ') !=
      NVL(:NEW.form_type,' ') THEN
      audit_trail.column_update
        (raid, 'form_type',
        :OLD.form_type, :NEW.form_type);
   END IF;
   IF NVL(:OLD.other_links,' ') !=
      NVL(:NEW.other_links,' ') THEN
      audit_trail.column_update
        (raid, 'other_links',
        :OLD.other_links, :NEW.other_links);
   END IF;
   IF NVL(:OLD.propagate_from,' ') !=
      NVL(:NEW.propagate_from,' ') THEN
      audit_trail.column_update
        (raid, 'propagate_from',
        :OLD.propagate_from, :NEW.propagate_from);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;

    IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;


   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
--When SCH_EVENT_CRF table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE
TRIGGER "ESCH"."SCH_EVENT_CRF_AU1" 
AFTER UPDATE ON ESCH.SCH_EVENT_CRF 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	v_chainid number(10);
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    --Chain ID from event_assoc will be used as module id
    v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:OLD.FK_EVENT);    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENT_CRF
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENT_CRF',:OLD.RID,v_chainid,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTCRF,0) != NVL(:NEW.PK_EVENTCRF,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','PK_EVENTCRF',:OLD.PK_EVENTCRF,:NEW.PK_EVENTCRF,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,' ') != NVL(:NEW.FK_EVENT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_FORM,0) != NVL(:NEW.FK_FORM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','FK_FORM',:OLD.FK_FORM,:NEW.FK_FORM,NULL,NULL);
    END IF;
    IF NVL(:OLD.FORM_TYPE,' ') != NVL(:NEW.FORM_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','FORM_TYPE',:OLD.FORM_TYPE,:NEW.FORM_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.OTHER_LINKS,' ') != NVL(:NEW.OTHER_LINKS,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','OTHER_LINKS',:OLD.OTHER_LINKS,:NEW.OTHER_LINKS,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPAGATE_FROM,' ') != NVL(:NEW.PROPAGATE_FROM,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','PROPAGATE_FROM',:OLD.PROPAGATE_FROM,:NEW.PROPAGATE_FROM,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_CRF','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
	
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENT_CRF_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER SCH_EVENT_KIT_AU0
AFTER UPDATE
ON SCH_EVENT_KIT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENT_KIT', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventkit,0) !=
      NVL(:NEW.pk_eventkit,0) THEN
      audit_trail.column_update
        (raid, 'pk_eventkit',
        :OLD.pk_eventkit, :NEW.pk_eventkit);
   END IF;

   IF NVL(:OLD.fk_event,' ') !=
      NVL(:NEW.fk_event,' ') THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;

   IF NVL(:OLD.fk_storage,0) !=
      NVL(:NEW.fk_storage,0) THEN
      audit_trail.column_update
        (raid, 'FK_STORAGE',
        :OLD.fk_storage, :NEW.fk_storage);
   END IF;


   IF NVL(:OLD.propagate_from,' ') !=
      NVL(:NEW.propagate_from,' ') THEN
      audit_trail.column_update
        (raid, 'propagate_from',
        :OLD.propagate_from, :NEW.propagate_from);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;

    IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;


   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
      to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
--When SCH_EVENT_KIT table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENT_KIT_AU1" 
AFTER UPDATE ON ESCH.SCH_EVENT_KIT 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	v_chainid number(10);
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
 BEGIN
    --Chain ID from event_assoc will be used as module id
    v_chainid := PKG_AUDIT_TRAIL_MODULE.F_GETCHAINID(:OLD.FK_EVENT);    
    IF (v_chainid =0) THEN
        RETURN;
    END IF;
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_EVENT_KIT
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_EVENT_KIT',:OLD.RID,v_chainid,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_EVENTKIT,0) != NVL(:NEW.PK_EVENTKIT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','PK_EVENTKIT',:OLD.PK_EVENTKIT,:NEW.PK_EVENTKIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,' ') != NVL(:NEW.FK_EVENT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_STORAGE,0) != NVL(:NEW.FK_STORAGE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','FK_STORAGE',:OLD.FK_STORAGE,:NEW.FK_STORAGE,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPAGATE_FROM,' ') != NVL(:NEW.PROPAGATE_FROM,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','PROPAGATE_FROM',:OLD.PROPAGATE_FROM,:NEW.PROPAGATE_FROM,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_EVENT_KIT','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
	
	EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_EVENT_KIT_AU1 ');
END;
/
CREATE OR REPLACE TRIGGER "SCH_EVRES_TRACK_AU0"
AFTER UPDATE
ON SCH_EVRES_TRACK REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

  raid NUMBER(10);

  usr VARCHAR2(100);

-- JM: on 02/14/08 for Audit Update

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);



  audit_trail.record_transaction

    (raid, 'SCH_EVRES_TRACK', :OLD.rid, 'U', usr );



   IF NVL(:OLD.pk_evres_track,0) !=

      NVL(:NEW.pk_evres_track,0) THEN

      audit_trail.column_update

        (raid, 'PK_EVRES_TRACK',

        :OLD.pk_evres_track, :NEW.pk_evres_track);

   END IF;

   IF NVL(:OLD.fk_eventstat,0) !=

      NVL(:NEW.fk_eventstat,0) THEN

      audit_trail.column_update

        (raid, 'FK_EVENTSTAT',

        :OLD.fk_eventstat, :NEW.fk_eventstat);

   END IF;


   IF NVL(:OLD.fk_codelst_role,0) !=

      NVL(:NEW.fk_codelst_role,0) THEN

      audit_trail.column_update

        (raid, 'fk_codelst_role',

        :OLD.fk_codelst_role, :NEW.fk_codelst_role);

   END IF;

   IF NVL(:OLD.evres_track_duration,' ') !=

      NVL(:NEW.evres_track_duration,' ') THEN

      audit_trail.column_update

        (raid, 'EVRES_TRACK_DURATION',

        :OLD.evres_track_duration, :NEW.evres_track_duration);

   END IF;

   IF NVL(:OLD.rid,0) !=

      NVL(:NEW.rid,0) THEN

      audit_trail.column_update

        (raid, 'RID',

        :OLD.rid, :NEW.rid);

   END IF;

   IF NVL(:OLD.last_modified_by,0) !=

      NVL(:NEW.last_modified_by,0) THEN

      audit_trail.column_update

        (raid, 'LAST_MODIFIED_BY',

        :OLD.last_modified_by, :NEW.last_modified_by);

   END IF;

   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

      audit_trail.column_update

        (raid, 'LAST_MODIFIED_DATE',

                    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));


   END IF;

   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

      audit_trail.column_update

        (raid, 'CREATED_ON',

                    to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));


   END IF;



   IF NVL(:OLD.ip_add,' ') !=  NVL(:NEW.ip_add,' ') THEN

      audit_trail.column_update

        (raid, 'IP_ADD',

        :OLD.ip_add, :NEW.ip_add);

   END IF;

END;
/
CREATE OR REPLACE TRIGGER SCH_LINEITEM_AU0
AFTER UPDATE
OF PK_LINEITEM,
FK_BGTSECTION,
LINEITEM_DESC,
LINEITEM_SPONSORUNIT,
LINEITEM_CLINICNOFUNIT,
LINEITEM_OTHERCOST,
LINEITEM_DELFLAG,
RID,
CREATOR,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
CREATED_ON,
IP_ADD,
LINEITEM_NAME,
LINEITEM_NOTES,
LINEITEM_INPERSEC,
LINEITEM_STDCARECOST,
LINEITEM_RESCOST,
LINEITEM_INVCOST,
LINEITEM_INCOSTDISC,
LINEITEM_CPTCODE,
LINEITEM_REPEAT,
LINEITEM_PARENTID,
LINEITEM_APPLYINFUTURE,
FK_CODELST_CATEGORY,
LINEITEM_TMID,
LINEITEM_CDM,
LINEITEM_APPLYINDIRECTS,
LINEITEM_TOTALCOST,
LINEITEM_SPONSORAMOUNT,
LINEITEM_VARIANCE,FK_EVENT,
lineitem_seq,
fk_codelst_cost_type,
SUBCOST_ITEM_FLAG
ON SCH_LINEITEM REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_LINEITEM', :OLD.rid, 'U', :NEW.LAST_MODIFIED_BY);

  IF NVL(:OLD.pk_lineitem,0) !=
     NVL(:NEW.pk_lineitem,0) THEN
     audit_trail.column_update
       (raid, 'PK_LINEITEM',
       :OLD.pk_lineitem, :NEW.pk_lineitem);
  END IF;
  IF NVL(:OLD.fk_bgtsection,0) !=
     NVL(:NEW.fk_bgtsection,0) THEN
     audit_trail.column_update
       (raid, 'FK_BGTSECTION',
       :OLD.fk_bgtsection, :NEW.fk_bgtsection);
  END IF;
  IF NVL(:OLD.lineitem_desc,' ') !=
     NVL(:NEW.lineitem_desc,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_DESC',
       :OLD.lineitem_desc, :NEW.lineitem_desc);
  END IF;

  IF NVL(:OLD.lineitem_sponsorunit,' ') !=
     NVL(:NEW.lineitem_sponsorunit,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SPONSORUNIT',
       :OLD.lineitem_sponsorunit, :NEW.lineitem_sponsorunit);
  END IF;

  IF NVL(:OLD.lineitem_clinicnofunit,' ') !=
     NVL(:NEW.lineitem_clinicnofunit,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CLINICNOFUNIT',
       :OLD.lineitem_clinicnofunit, :NEW.lineitem_clinicnofunit);
  END IF;

  IF NVL(:OLD.lineitem_othercost,' ') !=
     NVL(:NEW.lineitem_othercost,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_OTHERCOST',
       :OLD.lineitem_othercost, :NEW.lineitem_othercost);
  END IF;


  IF NVL(:OLD.lineitem_delflag,' ') !=
     NVL(:NEW.lineitem_delflag,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_DELFLAG',
       :OLD.lineitem_delflag, :NEW.lineitem_delflag);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
                   to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
  IF NVL(:OLD.lineitem_name,' ') !=
     NVL(:NEW.lineitem_name,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_NAME',
       :OLD.lineitem_name, :NEW.lineitem_name);
  END IF;

  IF NVL(:OLD.lineitem_notes,' ') !=
     NVL(:NEW.lineitem_notes,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_NOTES',
       :OLD.lineitem_notes, :NEW.lineitem_notes);
  END IF;
  IF NVL(:OLD.lineitem_inpersec,0) !=
     NVL(:NEW.lineitem_inpersec,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_inpersec',
       :OLD.lineitem_inpersec, :NEW.lineitem_inpersec);
  END IF;

  IF NVL(:OLD.lineitem_stdcarecost,0) !=
     NVL(:NEW.lineitem_stdcarecost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_STDCARECOST',
       :OLD.lineitem_stdcarecost, :NEW.lineitem_stdcarecost);
  END IF;
  IF NVL(:OLD.lineitem_rescost,0) !=
     NVL(:NEW.lineitem_rescost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_RESCOST',
       :OLD.lineitem_rescost, :NEW.lineitem_rescost);
  END IF;
  IF NVL(:OLD.lineitem_invcost,0) !=
     NVL(:NEW.lineitem_invcost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_INVCOST',
       :OLD.lineitem_invcost, :NEW.lineitem_invcost);
  END IF;
  IF NVL(:OLD.lineitem_incostdisc,0) !=
     NVL(:NEW.lineitem_incostdisc,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_INCOSTDISC',
       :OLD.lineitem_incostdisc, :NEW.lineitem_incostdisc);
  END IF;
  IF NVL(:OLD.lineitem_cptcode,' ') !=
     NVL(:NEW.lineitem_cptcode,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CPTCODE',
       :OLD.lineitem_cptcode, :NEW.lineitem_cptcode);
  END IF;
  IF NVL(:OLD.lineitem_repeat,0) !=
     NVL(:NEW.lineitem_repeat,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_REPEAT',
       :OLD.lineitem_repeat, :NEW.lineitem_repeat);
  END IF;
  IF NVL(:OLD.lineitem_parentid,0) !=
     NVL(:NEW.lineitem_parentid,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_PARENTID',
       :OLD.lineitem_parentid, :NEW.lineitem_parentid);
  END IF;
  IF NVL(:OLD.lineitem_applyinfuture,0) !=
     NVL(:NEW.lineitem_applyinfuture,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_APPLYINFUTURE',
       :OLD.lineitem_applyinfuture, :NEW.lineitem_applyinfuture);
  END IF;

  ----

  IF NVL(:OLD.fk_codelst_category,0) !=
     NVL(:NEW.fk_codelst_category,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_CATEGORY',
       :OLD.fk_codelst_category, :NEW.fk_codelst_category);
  END IF;

  IF NVL(:OLD.lineitem_tmid,' ') !=
     NVL(:NEW.lineitem_tmid,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_TMID',
       :OLD.lineitem_tmid, :NEW.lineitem_tmid);
  END IF;

  IF NVL(:OLD.lineitem_cdm,' ') !=
     NVL(:NEW.lineitem_cdm,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_CDM',
       :OLD.lineitem_cdm, :NEW.lineitem_cdm);
  END IF;

  IF NVL(:OLD.lineitem_applyindirects,' ') !=
     NVL(:NEW.lineitem_applyindirects,' ') THEN
     audit_trail.column_update
       (raid, 'LINEITEM_APPLYINDIRECTS',
       :OLD.lineitem_applyindirects, :NEW.lineitem_applyindirects);
  END IF;

--JM: 02/16/2007
  IF NVL(:OLD.lineitem_totalcost,0) !=
     NVL(:NEW.lineitem_totalcost,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_TOTALCOST',
       :OLD.lineitem_totalcost, :NEW.lineitem_totalcost);
  END IF;
  IF NVL(:OLD.lineitem_sponsoramount,0) !=
     NVL(:NEW.lineitem_sponsoramount,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SPONSORAMOUNT',
       :OLD.lineitem_sponsoramount, :NEW.lineitem_sponsoramount);
  END IF;
  IF NVL(:OLD.lineitem_variance,0) !=
     NVL(:NEW.lineitem_variance,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_VARIANCE',
       :OLD.lineitem_variance, :NEW.lineitem_variance);
  END IF;

  IF NVL(:OLD.fk_event,0) !=
     NVL(:NEW.fk_event,0) THEN
     audit_trail.column_update
       (raid, 'FK_EVENT',
       :OLD.fk_event, :NEW.fk_event);
  END IF;

  IF NVL(:OLD.lineitem_seq,0) !=
     NVL(:NEW.lineitem_seq,0) THEN
     audit_trail.column_update
       (raid, 'LINEITEM_SEQ',
       :OLD.lineitem_seq, :NEW.lineitem_seq);
  END IF;

IF NVL(:OLD.fk_codelst_cost_type,0) !=
     NVL(:NEW.fk_codelst_cost_type,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_COST_TYPE',
       :OLD.fk_codelst_cost_type, :NEW.fk_codelst_cost_type);
  END IF;

IF NVL(:OLD.SUBCOST_ITEM_FLAG,0) !=
     NVL(:NEW.SUBCOST_ITEM_FLAG,0) THEN
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_FLAG',
       :OLD.SUBCOST_ITEM_FLAG, :NEW.SUBCOST_ITEM_FLAG);
END IF;

END;
/
--When SCH_LINEITEM table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_LINEITEM_AU1" 
AFTER UPDATE ON ESCH.SCH_LINEITEM 
REFERENCING NEW AS NEW OLD AS OLD FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	V_LINEITEM_REPEAT_OLD VARCHAR2(200):= NULL;
	V_FK_CODELST_CATEGORY_OLD VARCHAR2(200) :=NULL;
	V_FK_CODELST_COST_TYPE_OLD VARCHAR2(200) :=NULL;
	V_LINEITEM_REPEAT_NEW VARCHAR2(200):= NULL;
	V_FK_CODELST_CATEGORY_NEW VARCHAR2(200) :=NULL;
	V_FK_CODELST_COST_TYPE_NEW VARCHAR2(200) :=NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION; 

BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
	--This Block run when there is Delete operation perform on SCH_LINEITEM
    IF :NEW.LINEITEM_DELFLAG='Y' THEN
	  --Inserting row in AUDIT_ROW_MODULE table.Action - 'D' is used for Logical Deletion of the record.
      PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_LINEITEM',:OLD.RID,NULL,'D',:NEW.LAST_MODIFIED_BY);
	  
      --Inserting rows in AUDIT_Column_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','PK_LINEITEM',:OLD.PK_LINEITEM,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','FK_BGTSECTION',:OLD.FK_BGTSECTION,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_DESC',:OLD.LINEITEM_DESC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_SPONSORUNIT',:OLD.LINEITEM_SPONSORUNIT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_CLINICNOFUNIT',:OLD.LINEITEM_CLINICNOFUNIT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_OTHERCOST',:OLD.LINEITEM_OTHERCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_DELFLAG',:OLD.LINEITEM_DELFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','RID',:OLD.RID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','CREATOR',:OLD.CREATOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_NAME',:OLD.LINEITEM_NAME,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_NOTES',:OLD.LINEITEM_NOTES,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_INPERSEC',:OLD.LINEITEM_INPERSEC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_STDCARECOST',:OLD.LINEITEM_STDCARECOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_RESCOST',:OLD.LINEITEM_RESCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_INVCOST',:OLD.LINEITEM_INVCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_INCOSTDISC',:OLD.LINEITEM_INCOSTDISC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_CPTCODE',:OLD.LINEITEM_CPTCODE,NULL,NULL,NULL);
	 
      IF (NVL(:OLD.LINEITEM_REPEAT,0) > 0) THEN
          V_LINEITEM_REPEAT_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.LINEITEM_REPEAT);  
      END IF;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_REPEAT',V_LINEITEM_REPEAT_OLD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_PARENTID',:OLD.LINEITEM_PARENTID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_APPLYINFUTURE',:OLD.LINEITEM_APPLYINFUTURE,NULL,NULL,NULL);
	  
      IF (NVL(:OLD.FK_CODELST_CATEGORY,0) > 0) THEN
          V_FK_CODELST_CATEGORY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_CATEGORY);  
      END IF;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','FK_CODELST_CATEGORY',V_FK_CODELST_CATEGORY_OLD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_TMID',:OLD.LINEITEM_TMID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_CDM',:OLD.LINEITEM_CDM,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_APPLYINDIRECTS',:OLD.LINEITEM_APPLYINDIRECTS,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_TOTALCOST',:OLD.LINEITEM_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_SPONSORAMOUNT',:OLD.LINEITEM_SPONSORAMOUNT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_VARIANCE',:OLD.LINEITEM_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','FK_EVENT',:OLD.FK_EVENT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','LINEITEM_SEQ',:OLD.LINEITEM_SEQ,NULL,NULL,NULL);
      
      IF (NVL(:OLD.FK_CODELST_COST_TYPE,0) > 0) THEN
          V_FK_CODELST_COST_TYPE_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_COST_TYPE);  
      END IF;
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','FK_CODELST_COST_TYPE',V_FK_CODELST_COST_TYPE_OLD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','SUBCOST_ITEM_FLAG',:OLD.SUBCOST_ITEM_FLAG,NULL,NULL,NULL);
   ELSE
    --This Block run when there is Update operation perform on SCH_LINEITEM 
	  --Inserting row in the table AUDIT_ROW_MODULE.
      PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_LINEITEM',:OLD.RID,NULL,'U',:NEW.LAST_MODIFIED_BY);
      
	--Inserting row in the table AUDIT_COLUMN_MODULE.
	  IF NVL(:OLD.PK_LINEITEM,0) != NVL(:NEW.PK_LINEITEM,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'PK_LINEITEM',:OLD.PK_LINEITEM, :NEW.PK_LINEITEM,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_BGTSECTION,0) != NVL(:NEW.FK_BGTSECTION,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'FK_BGTSECTION',:OLD.FK_BGTSECTION, :NEW.FK_BGTSECTION,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_DESC,' ') != NVL(:NEW.LINEITEM_DESC,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_DESC',:OLD.LINEITEM_DESC, :NEW.LINEITEM_DESC,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_SPONSORUNIT,' ') != NVL(:NEW.LINEITEM_SPONSORUNIT,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_SPONSORUNIT',:OLD.LINEITEM_SPONSORUNIT,:NEW.LINEITEM_SPONSORUNIT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_CLINICNOFUNIT,' ') != NVL(:NEW.LINEITEM_CLINICNOFUNIT,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_CLINICNOFUNIT',:OLD.LINEITEM_CLINICNOFUNIT, :NEW.LINEITEM_CLINICNOFUNIT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_OTHERCOST,' ') != NVL(:NEW.LINEITEM_OTHERCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_OTHERCOST',:OLD.LINEITEM_OTHERCOST, :NEW.LINEITEM_OTHERCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_DELFLAG,' ') != NVL(:NEW.LINEITEM_DELFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_DELFLAG',:OLD.LINEITEM_DELFLAG, :NEW.LINEITEM_DELFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'RID',:OLD.RID, :NEW.RID,NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_NAME,' ') != NVL(:NEW.LINEITEM_NAME,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_NAME',:OLD.LINEITEM_NAME, :NEW.LINEITEM_NAME,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_NOTES,' ') != NVL(:NEW.LINEITEM_NOTES,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_NOTES',:OLD.LINEITEM_NOTES, :NEW.LINEITEM_NOTES,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_INPERSEC,0) != NVL(:NEW.LINEITEM_INPERSEC,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_INPERSEC',:OLD.LINEITEM_INPERSEC, :NEW.LINEITEM_INPERSEC,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_STDCARECOST,0) != NVL(:NEW.LINEITEM_STDCARECOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_STDCARECOST',:OLD.LINEITEM_STDCARECOST, :NEW.LINEITEM_STDCARECOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_RESCOST,0) != NVL(:NEW.LINEITEM_RESCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_RESCOST',:OLD.LINEITEM_RESCOST, :NEW.LINEITEM_RESCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_INVCOST,0) != NVL(:NEW.LINEITEM_INVCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_INVCOST',:OLD.LINEITEM_INVCOST, :NEW.LINEITEM_INVCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_INCOSTDISC,0) != NVL(:NEW.LINEITEM_INCOSTDISC,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_INCOSTDISC',:OLD.LINEITEM_INCOSTDISC, :NEW.LINEITEM_INCOSTDISC,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_CPTCODE,' ') != NVL(:NEW.LINEITEM_CPTCODE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_CPTCODE',:OLD.LINEITEM_CPTCODE, :NEW.LINEITEM_CPTCODE,NULL,NULL);
      END IF;
	  
        IF (NVL(:OLD.LINEITEM_REPEAT,0) > 0) THEN
          V_LINEITEM_REPEAT_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.LINEITEM_REPEAT);  
        END IF;
        IF (NVL(:NEW.LINEITEM_REPEAT,0)>0) THEN
            V_LINEITEM_REPEAT_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.LINEITEM_REPEAT);  
        END IF;
      IF NVL(:OLD.LINEITEM_REPEAT,0) != NVL(:NEW.LINEITEM_REPEAT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_REPEAT',V_LINEITEM_REPEAT_OLD,V_LINEITEM_REPEAT_NEW,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_PARENTID,0) != NVL(:NEW.LINEITEM_PARENTID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_PARENTID',:OLD.LINEITEM_PARENTID, :NEW.LINEITEM_PARENTID,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_APPLYINFUTURE,0) != NVL(:NEW.LINEITEM_APPLYINFUTURE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_APPLYINFUTURE',:OLD.LINEITEM_APPLYINFUTURE, :NEW.LINEITEM_APPLYINFUTURE,NULL,NULL);
      END IF;
	  
        IF (NVL(:OLD.FK_CODELST_CATEGORY,0) > 0) THEN
          V_FK_CODELST_CATEGORY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_CATEGORY);  
        END IF;
        IF (NVL(:NEW.FK_CODELST_CATEGORY,0)>0) THEN
            V_FK_CODELST_CATEGORY_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_CATEGORY);  
        END IF;
      IF NVL(:OLD.FK_CODELST_CATEGORY,0) != NVL(:NEW.FK_CODELST_CATEGORY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'FK_CODELST_CATEGORY',V_FK_CODELST_CATEGORY_OLD,V_FK_CODELST_CATEGORY_NEW,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_TMID,' ') != NVL(:NEW.LINEITEM_TMID,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_TMID',:OLD.LINEITEM_TMID, :NEW.LINEITEM_TMID,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_CDM,' ') != NVL(:NEW.LINEITEM_CDM,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_CDM',:OLD.LINEITEM_CDM, :NEW.LINEITEM_CDM,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_APPLYINDIRECTS,' ') != NVL(:NEW.LINEITEM_APPLYINDIRECTS,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_APPLYINDIRECTS',:OLD.LINEITEM_APPLYINDIRECTS, :NEW.LINEITEM_APPLYINDIRECTS,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_TOTALCOST,0) != NVL(:NEW.LINEITEM_TOTALCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_TOTALCOST',:OLD.LINEITEM_TOTALCOST, :NEW.LINEITEM_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_SPONSORAMOUNT,0) != NVL(:NEW.LINEITEM_SPONSORAMOUNT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_SPONSORAMOUNT',:OLD.LINEITEM_SPONSORAMOUNT, :NEW.LINEITEM_SPONSORAMOUNT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_VARIANCE,0) != NVL(:NEW.LINEITEM_VARIANCE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_VARIANCE',:OLD.LINEITEM_VARIANCE, :NEW.LINEITEM_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_EVENT,0) != NVL(:NEW.FK_EVENT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'FK_EVENT',:OLD.FK_EVENT, :NEW.FK_EVENT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_SEQ,0) != NVL(:NEW.LINEITEM_SEQ,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'LINEITEM_SEQ',:OLD.LINEITEM_SEQ, :NEW.LINEITEM_SEQ,NULL,NULL);
      END IF;
	  
        IF (NVL(:OLD.FK_CODELST_COST_TYPE,0) > 0) THEN
          V_FK_CODELST_CATEGORY_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_COST_TYPE);  
        END IF;
        IF (NVL(:NEW.FK_CODELST_COST_TYPE,0)>0) THEN
            V_FK_CODELST_CATEGORY_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_COST_TYPE);  
        END IF;
      IF NVL(:OLD.FK_CODELST_COST_TYPE,0) != NVL(:NEW.FK_CODELST_COST_TYPE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM', 'FK_CODELST_COST_TYPE',V_FK_CODELST_CATEGORY_OLD,V_FK_CODELST_CATEGORY_NEW,NULL,NULL);
      END IF;
      IF NVL(:OLD.SUBCOST_ITEM_FLAG,0) != NVL(:NEW.SUBCOST_ITEM_FLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_LINEITEM','SUBCOST_ITEM_FLAG',:OLD.SUBCOST_ITEM_FLAG, :NEW.SUBCOST_ITEM_FLAG,NULL,NULL);
      END IF;
    END IF;
	
    EXCEPTION    
    WHEN OTHERS THEN    
    PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('BGT',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_LINEITEM_AU1 ');
END;
/
create or replace
TRIGGER "ESCH"."SCH_PORTAL_FORMS_AU0"
AFTER UPDATE
ON ESCH.SCH_PORTAL_FORMS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_PORTAL_FORMS', :OLD.rid, 'U', usr );


   IF NVL(:OLD.PK_PF,0) !=
      NVL(:NEW.PK_PF,0) THEN
      audit_trail.column_update
        (raid, 'PK_PF',
        :OLD.PK_PF, :NEW.PK_PF);
   END IF;

   IF NVL(:OLD.FK_PORTAL,0) !=
      NVL(:NEW.FK_PORTAL,0) THEN
      audit_trail.column_update
        (raid, 'FK_PORTAL',
        :OLD.FK_PORTAL, :NEW.FK_PORTAL);
   END IF;

   IF NVL(:OLD.FK_CALENDAR,0) !=
      NVL(:NEW.FK_CALENDAR,0) THEN
      audit_trail.column_update
        (raid, 'FK_CALENDAR',
        :OLD.FK_CALENDAR, :NEW.FK_CALENDAR);
   END IF;

   IF NVL(:OLD.FK_EVENT,0) !=
      NVL(:NEW.FK_EVENT,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.FK_EVENT, :NEW.FK_EVENT);
   END IF;
   IF NVL(:OLD.FK_FORM,0) !=
      NVL(:NEW.FK_FORM,0) THEN
      audit_trail.column_update
        (raid, 'FK_FORM',
        :OLD.FK_FORM, :NEW.FK_FORM);
   END IF;

   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;

   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;


   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
create or replace
TRIGGER "ESCH"."SCH_PORTAL_FORMS_AU0"
AFTER UPDATE
ON ESCH.SCH_PORTAL_FORMS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_PORTAL_FORMS', :OLD.rid, 'U', usr );


   IF NVL(:OLD.PK_PF,0) !=
      NVL(:NEW.PK_PF,0) THEN
      audit_trail.column_update
        (raid, 'PK_PF',
        :OLD.PK_PF, :NEW.PK_PF);
   END IF;

   IF NVL(:OLD.FK_PORTAL,0) !=
      NVL(:NEW.FK_PORTAL,0) THEN
      audit_trail.column_update
        (raid, 'FK_PORTAL',
        :OLD.FK_PORTAL, :NEW.FK_PORTAL);
   END IF;

   IF NVL(:OLD.FK_CALENDAR,0) !=
      NVL(:NEW.FK_CALENDAR,0) THEN
      audit_trail.column_update
        (raid, 'FK_CALENDAR',
        :OLD.FK_CALENDAR, :NEW.FK_CALENDAR);
   END IF;

   IF NVL(:OLD.FK_EVENT,0) !=
      NVL(:NEW.FK_EVENT,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.FK_EVENT, :NEW.FK_EVENT);
   END IF;
   IF NVL(:OLD.FK_FORM,0) !=
      NVL(:NEW.FK_FORM,0) THEN
      audit_trail.column_update
        (raid, 'FK_FORM',
        :OLD.FK_FORM, :NEW.FK_FORM);
   END IF;

   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;

   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;


   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;

END;
/
--When SCH_PROTOCOL_VISIT table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_PROTOCOL_VISIT_AU2" 
AFTER UPDATE ON ESCH.SCH_PROTOCOL_VISIT 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);  /*variable used to fetch value of sequence */
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_PROTOCOL_VISIT
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_PROTOCOL_VISIT',:OLD.RID,:OLD.FK_PROTOCOL,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_PROTOCOL_VISIT,0) != NVL(:NEW.PK_PROTOCOL_VISIT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','PK_PROTOCOL_VISIT',:OLD.PK_PROTOCOL_VISIT,:NEW.PK_PROTOCOL_VISIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_PROTOCOL,0) != NVL(:NEW.FK_PROTOCOL,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','FK_PROTOCOL',:OLD.FK_PROTOCOL,:NEW.FK_PROTOCOL,NULL,NULL);
    END IF;
    IF NVL(:OLD.VISIT_NO,0) != NVL(:NEW.VISIT_NO,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','VISIT_NO',:OLD.VISIT_NO,:NEW.VISIT_NO,NULL,NULL);
    END IF;
    IF NVL(:OLD.VISIT_NAME,' ') != NVL(:NEW.VISIT_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','VISIT_NAME',:OLD.VISIT_NAME,:NEW.VISIT_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.DESCRIPTION,' ') != NVL(:NEW.DESCRIPTION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','DESCRIPTION',:OLD.DESCRIPTION,:NEW.DESCRIPTION,NULL,NULL);
    END IF;
    IF NVL(:OLD.DISPLACEMENT,0) != NVL(:NEW.DISPLACEMENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','DISPLACEMENT',:OLD.DISPLACEMENT,:NEW.DISPLACEMENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.NUM_MONTHS,0) != NVL(:NEW.NUM_MONTHS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','NUM_MONTHS',:OLD.NUM_MONTHS,:NEW.NUM_MONTHS,NULL,NULL);
    END IF;
    IF NVL(:OLD.NUM_WEEKS,0) != NVL(:NEW.NUM_WEEKS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','NUM_WEEKS',:OLD.NUM_WEEKS,:NEW.NUM_WEEKS,NULL,NULL);
    END IF;
    IF NVL(:OLD.NUM_DAYS,0) != NVL(:NEW.NUM_DAYS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','NUM_DAYS',:OLD.NUM_DAYS,:NEW.NUM_DAYS,NULL,NULL);
    END IF;
    IF NVL(:OLD.INSERT_AFTER,0) != NVL(:NEW.INSERT_AFTER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','INSERT_AFTER',:OLD.INSERT_AFTER,:NEW.INSERT_AFTER,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.INSERT_AFTER_INTERVAL,0) != NVL(:NEW.INSERT_AFTER_INTERVAL,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','INSERT_AFTER_INTERVAL',:OLD.INSERT_AFTER_INTERVAL,:NEW.INSERT_AFTER_INTERVAL,NULL,NULL);
    END IF;
    IF NVL(:OLD.INSERT_AFTER_INTERVAL_UNIT,' ') != NVL(:NEW.INSERT_AFTER_INTERVAL_UNIT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','INSERT_AFTER_INTERVAL_UNIT',:OLD.INSERT_AFTER_INTERVAL_UNIT,:NEW.INSERT_AFTER_INTERVAL_UNIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROTOCOL_TYPE,' ') != NVL(:NEW.PROTOCOL_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','PROTOCOL_TYPE',:OLD.PROTOCOL_TYPE,:NEW.PROTOCOL_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.VISIT_TYPE,' ') != NVL(:NEW.VISIT_TYPE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','VISIT_TYPE',:OLD.VISIT_TYPE,:NEW.VISIT_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.OFFLINE_FLAG,0) != NVL(:NEW.OFFLINE_FLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','OFFLINE_FLAG',:OLD.OFFLINE_FLAG,:NEW.OFFLINE_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.HIDE_FLAG,0) != NVL(:NEW.HIDE_FLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','HIDE_FLAG',:OLD.HIDE_FLAG,:NEW.HIDE_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.NO_INTERVAL_FLAG,0) != NVL(:NEW.NO_INTERVAL_FLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','NO_INTERVAL_FLAG',:OLD.NO_INTERVAL_FLAG,:NEW.NO_INTERVAL_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.WIN_BEFORE_NUMBER,0) != NVL(:NEW.WIN_BEFORE_NUMBER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','WIN_BEFORE_NUMBER',:OLD.WIN_BEFORE_NUMBER,:NEW.WIN_BEFORE_NUMBER,NULL,NULL);
    END IF;
    IF NVL(:OLD.WIN_BEFORE_UNIT,' ') != NVL(:NEW.WIN_BEFORE_UNIT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','WIN_BEFORE_UNIT',:OLD.WIN_BEFORE_UNIT,:NEW.WIN_BEFORE_UNIT,NULL,NULL);
    END IF;
    IF NVL(:OLD.WIN_AFTER_NUMBER,0) != NVL(:NEW.WIN_AFTER_NUMBER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','WIN_AFTER_NUMBER',:OLD.WIN_AFTER_NUMBER,:NEW.WIN_AFTER_NUMBER,NULL,NULL);
    END IF;
    IF NVL(:OLD.WIN_AFTER_UNIT,' ') != NVL(:NEW.WIN_AFTER_UNIT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTOCOL_VISIT','WIN_AFTER_UNIT',:OLD.WIN_AFTER_UNIT,:NEW.WIN_AFTER_UNIT,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_PROTOCOL_VISIT_AU2 ');
END;
/
create or replace
TRIGGER SCH_PROTOCOL_VISIT_AUO
AFTER UPDATE
ON SCH_PROTOCOL_VISIT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_PROTOCOL_VISIT', :OLD.rid, 'U', usr );



   IF NVL(:OLD.pk_protocol_visit,0) !=
      NVL(:NEW.pk_protocol_visit,0) THEN
      audit_trail.column_update
        (raid, 'pk_protocol_visit', :OLD.pk_protocol_visit, :NEW.pk_protocol_visit);
   END IF;
   IF NVL(:OLD.fk_protocol,0) !=
      NVL(:NEW.fk_protocol,0) THEN
      audit_trail.column_update
        (raid, 'FK_PROTOCOL', :OLD.fk_protocol, :NEW.fk_protocol);
   END IF;
   IF NVL(:OLD.visit_no,0) !=
      NVL(:NEW.visit_no,0) THEN
      audit_trail.column_update
        (raid, 'VISIT_NO', :OLD.visit_no, :NEW.visit_no);
   END IF;

   IF NVL(:OLD.visit_name,' ') !=
      NVL(:NEW.visit_name,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_NAME', :OLD.visit_name, :NEW.visit_name);
   END IF;

   IF NVL(:OLD.description,' ') !=
      NVL(:NEW.description,' ') THEN
      audit_trail.column_update
        (raid, 'DESCRIPTION', :OLD.description, :NEW.description);
   END IF;

   IF NVL(:OLD.displacement,0) !=
      NVL(:NEW.displacement,0) THEN
      audit_trail.column_update
        (raid, 'DISPLACEMENT', :OLD.displacement, :NEW.displacement);
   END IF;

   IF NVL(:OLD.num_months,0) !=
      NVL(:NEW.NUM_MONTHS,0) THEN
      audit_trail.column_update
        (raid, 'NUM_MONTHS', :OLD.num_months, :NEW.num_months);
   END IF;

   IF NVL(:OLD.num_days,0) !=
      NVL(:NEW.NUM_MONTHS,0) THEN
      audit_trail.column_update
        (raid, 'NUM_DAYS', :OLD.num_days, :NEW.num_days);
   END IF;

   IF NVL(:OLD.insert_after,0) !=
      NVL(:NEW.insert_after,0) THEN
      audit_trail.column_update
        (raid, 'INSERT_AFTER', :OLD.insert_after, :NEW.insert_after);
   END IF;

   IF NVL(:OLD.num_weeks,0) !=
      NVL(:NEW.num_weeks,0) THEN
      audit_trail.column_update
        (raid, 'NUM_WEEKS', :OLD.num_weeks, :NEW.num_weeks);
   END IF;

   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID', :OLD.rid, :NEW.rid);
   END IF;

   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY', :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	     to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD', :OLD.ip_add, :NEW.ip_add);
   END IF;

   IF NVL(:OLD.insert_after_interval,0) !=
      NVL(:NEW.insert_after_interval,0) THEN
      audit_trail.column_update
        (raid, 'INSERT_AFTER_INTERVAL', :OLD.insert_after_interval, :NEW.insert_after_interval);
   END IF;
   IF NVL(:OLD.insert_after_interval_unit,' ') !=
      NVL(:NEW.insert_after_interval_unit,' ') THEN
      audit_trail.column_update
        (raid, 'INSERT_AFTER_INTERVAL_UNIT', :OLD.insert_after_interval_unit, :NEW.insert_after_interval_unit);
   END IF;
   IF NVL(:OLD.protocol_type,' ') !=
      NVL(:NEW.protocol_type,' ') THEN
      audit_trail.column_update
        (raid, 'PROTOCOL_TYPE', :OLD.protocol_type, :NEW.protocol_type);
   END IF;

   IF NVL(:OLD.visit_type,' ') !=
      NVL(:NEW.visit_type,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE', :OLD.visit_type, :NEW.visit_type);
   END IF;

   IF NVL(:OLD.offline_flag,0) !=
      NVL(:NEW.offline_flag,0) THEN
      audit_trail.column_update
        (raid, 'offline_flag', :OLD.offline_flag, :NEW.offline_flag);
   END IF;
   
   IF NVL(:OLD.hide_flag,0) !=
      NVL(:NEW.hide_flag,0) THEN
      audit_trail.column_update
        (raid, 'hide_flag', :OLD.hide_flag, :NEW.hide_flag);
   END IF;
   
   IF NVL(:OLD.no_interval_flag,0) !=
      NVL(:NEW.no_interval_flag,0) THEN
      audit_trail.column_update
        (raid, 'no_interval_flag', :OLD.no_interval_flag, :NEW.no_interval_flag);
   END IF;
   
   IF NVL(:OLD.win_before_number,0) !=
      NVL(:NEW.win_before_number,0) THEN
      audit_trail.column_update
        (raid, 'win_before_number', :OLD.win_before_number, :NEW.win_before_number);
   END IF;
    
   IF NVL(:OLD.win_before_unit,' ') !=
      NVL(:NEW.win_before_unit,' ') THEN
      audit_trail.column_update
        (raid, 'win_before_unit', :OLD.win_before_unit, :NEW.win_before_unit);
   END IF; 
   
   IF NVL(:OLD.win_after_number,0) !=
      NVL(:NEW.win_after_number,0) THEN
      audit_trail.column_update
        (raid, 'win_after_number', :OLD.win_after_number, :NEW.win_after_number);
   END IF;
   
   IF NVL(:OLD.win_after_unit,' ') !=
      NVL(:NEW.win_after_unit,' ') THEN
      audit_trail.column_update
        (raid, 'win_after_unit', :OLD.win_after_unit, :NEW.win_after_unit);
   END IF;

END;
/
CREATE OR REPLACE TRIGGER SCH_PROTSTAT_AU0
AFTER UPDATE
ON SCH_PROTSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_PROTSTAT', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_protstat,0) !=
      NVL(:NEW.pk_protstat,0) THEN
      audit_trail.column_update
        (raid, 'PK_PROTSTAT',
        :OLD.pk_protstat, :NEW.pk_protstat);
   END IF;
   IF NVL(:OLD.fk_event,0) !=
      NVL(:NEW.fk_event,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   /*IF NVL(:OLD.protstat,' ') !=
      NVL(:NEW.protstat,' ') THEN
      audit_trail.column_update
        (raid, 'PROTSTAT',
        :OLD.protstat, :NEW.protstat);
   END IF;*/
   IF NVL(:OLD.protstat_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.protstat_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'PROTSTAT_DT',
        to_char(:OLD.protstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.protstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.protstat_by,0) !=
      NVL(:NEW.protstat_by,0) THEN
      audit_trail.column_update
        (raid, 'PROTSTAT_BY',
        :OLD.protstat_by, :NEW.protstat_by);
   END IF;
   IF NVL(:OLD.protstat_note,' ') !=
      NVL(:NEW.protstat_note,' ') THEN
      audit_trail.column_update
        (raid, 'PROTSTAT_NOTE',
        :OLD.protstat_note, :NEW.protstat_note);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
             to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
             to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   --JM: 07FEB2011, #D-FIN9
   IF NVL(:OLD.FK_CODELST_CALSTAT,0) !=
      NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_CALSTAT',
        :OLD.FK_CODELST_CALSTAT, :NEW.FK_CODELST_CALSTAT);
   END IF;

END;
/
--When SCH_PROTSTAT table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_PROTSTAT_AU1" 
AFTER UPDATE ON ESCH.SCH_PROTSTAT
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10); /*variable used to fetch value of sequence */
	V_FK_CODELST_CALSTAT_OLD VARCHAR2(200) :=NULL;
	V_FK_CODELST_CALSTAT_NEW VARCHAR2(200) :=NULL;
	v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    --This Block run when there is Update operation perform on SCH_PROTSTAT
    --Inserting row in AUDIT_ROW_MODULE table.
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (v_rowid,'SCH_PROTSTAT',:OLD.RID,:OLD.FK_EVENT,'U',:NEW.LAST_MODIFIED_BY);
    --Inserting rows in AUDIT_COLUMN_MODULE table.
    IF NVL(:OLD.PK_PROTSTAT,0) != NVL(:NEW.PK_PROTSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','PK_PROTSTAT',:OLD.PK_PROTSTAT,:NEW.PK_PROTSTAT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_EVENT,0) != NVL(:NEW.FK_EVENT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','FK_EVENT',:OLD.FK_EVENT,:NEW.FK_EVENT,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROTSTAT,' ') != NVL(:NEW.PROTSTAT,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','PROTSTAT',:OLD.PROTSTAT,:NEW.PROTSTAT,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROTSTAT_DT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.PROTSTAT_DT,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','PROTSTAT_DT',TO_CHAR(:OLD.PROTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.PROTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.PROTSTAT_BY,0) != NVL(:NEW.PROTSTAT_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','PROTSTAT_BY',:OLD.PROTSTAT_BY,:NEW.PROTSTAT_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROTSTAT_NOTE,' ') != NVL(:NEW.PROTSTAT_NOTE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','PROTSTAT_NOTE',:OLD.PROTSTAT_NOTE,:NEW.PROTSTAT_NOTE,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','CREATOR',:OLD.CREATOR,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) !=NVL(:NEW.LAST_MODIFIED_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
		IF (NVL(:OLD.FK_CODELST_CALSTAT,0) > 0) THEN
          V_FK_CODELST_CALSTAT_OLD := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:OLD.FK_CODELST_CALSTAT);  
        END IF;
        IF (NVL(:NEW.FK_CODELST_CALSTAT,0)>0) THEN
            V_FK_CODELST_CALSTAT_NEW := ESCH.PKG_AUDIT_TRAIL_MODULE.F_GETCODELSTVALUE(:NEW.FK_CODELST_CALSTAT);  
        END IF;	
    IF NVL(:OLD.FK_CODELST_CALSTAT,0) != NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'SCH_PROTSTAT','FK_CODELST_CALSTAT',V_FK_CODELST_CALSTAT_OLD,V_FK_CODELST_CALSTAT_NEW,NULL,NULL);
    END IF;
	
	EXCEPTION
	WHEN OTHERS THEN
	PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Trigger', 'Exception Raised:'||sqlerrm||'','','Error In Trigger SCH_PROTSTAT_AU1 ');
END;
/
create or replace TRIGGER ESCH."SCH_SUBCOST_ITEM_AU0"
AFTER UPDATE
ON ESCH.SCH_SUBCOST_ITEM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
   raid number(10);
   USR VARCHAR2(2000);
   old_modby varchar2(100);
   new_modby varchar2(100);
--Created by Manimaran for Audit Update   
begin

	BEGIN
		USR := getuser(NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR));
		EXCEPTION WHEN NO_DATA_FOUND THEN
		USR := 'New User' ;
	END;
	
   select seq_audit.nextval into raid from dual;
   audit_trail.record_transaction(raid, 'SCH_SUBCOST_ITEM', :old.rid, 'U', USR);
   
   if nvl(:old.PK_SUBCOST_ITEM,0) !=
     nvl(:new.PK_SUBCOST_ITEM,0) then
     audit_trail.column_update
       (raid, 'PK_SUBCOST_ITEM',
       :old.PK_SUBCOST_ITEM, :new.PK_SUBCOST_ITEM);
   end if;
   
   if nvl(:old.FK_CALENDAR,0) !=
     nvl(:new.FK_CALENDAR,0) then
     audit_trail.column_update
       (raid, 'FK_CALENDAR',
       :old.FK_CALENDAR, :new.FK_CALENDAR);
   end if;
   
   if nvl(:old.SUBCOST_ITEM_NAME,' ') !=
     nvl(:new.SUBCOST_ITEM_NAME,' ') then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_NAME',
       :old.SUBCOST_ITEM_NAME, :new.SUBCOST_ITEM_NAME);
   end if;
   
   if nvl(:old.SUBCOST_ITEM_COST,0) !=
     nvl(:new.SUBCOST_ITEM_COST,0) then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_COST',
       :old.SUBCOST_ITEM_COST, :new.SUBCOST_ITEM_COST);
  end if;
  
  if nvl(:old.SUBCOST_ITEM_UNIT,0) !=
     nvl(:new.SUBCOST_ITEM_UNIT,0) then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_UNIT',
       :old.SUBCOST_ITEM_UNIT, :new.SUBCOST_ITEM_UNIT);
  end if;
  
  if nvl(:old.SUBCOST_ITEM_SEQ,0) !=
     nvl(:new.SUBCOST_ITEM_SEQ,0) then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_SEQ',
       :old.SUBCOST_ITEM_SEQ, :new.SUBCOST_ITEM_SEQ);
  end if;
  
  if nvl(:old.FK_CODELST_CATEGORY,0) !=
     nvl(:new.FK_CODELST_CATEGORY,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_CATEGORY',
       :old.FK_CODELST_CATEGORY, :new.FK_CODELST_CATEGORY);
  end if;
  
  if nvl(:old.FK_CODELST_COST_TYPE,0) !=
     nvl(:new.FK_CODELST_COST_TYPE,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_COST_TYPE',
       :old.FK_CODELST_COST_TYPE, :new.FK_CODELST_COST_TYPE);
  end if;
  
  IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   
   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;
   
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
END;
/
create or replace TRIGGER ESCH."SCH_SUBCOST_ITEM_VISIT_AU0"
AFTER UPDATE
ON ESCH.SCH_SUBCOST_ITEM_VISIT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
   raid number(10);
   USR VARCHAR2(2000);
   old_modby varchar2(100);
   new_modby varchar2(100);
--Created by Manimaran for Audit Update   
begin

	BEGIN
		USR := getuser(NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR));
		EXCEPTION WHEN NO_DATA_FOUND THEN
		USR := 'New User' ;
	END ;
   select seq_audit.nextval into raid from dual;
   audit_trail.record_transaction(raid, 'SCH_SUBCOST_ITEM_VISIT', :old.rid, 'U', USR);
   
   if nvl(:old.PK_SUBCOST_ITEM_VISIT,0) !=
     nvl(:new.PK_SUBCOST_ITEM_VISIT,0) then
     audit_trail.column_update
       (raid, 'PK_SUBCOST_ITEM_VISIT',
       :old.PK_SUBCOST_ITEM_VISIT, :new.PK_SUBCOST_ITEM_VISIT);
   end if;
   
   if nvl(:old.FK_SUBCOST_ITEM,0) !=
     nvl(:new.FK_SUBCOST_ITEM,0) then
     audit_trail.column_update
       (raid, 'FK_SUBCOST_ITEM',
       :old.FK_SUBCOST_ITEM, :new.FK_SUBCOST_ITEM);
   end if;
   
   if nvl(:old.FK_PROTOCOL_VISIT,0) !=
     nvl(:new.FK_PROTOCOL_VISIT,0) then
     audit_trail.column_update
       (raid, 'FK_PROTOCOL_VISIT',
       :old.FK_PROTOCOL_VISIT, :new.FK_PROTOCOL_VISIT);
   end if;
   
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   
   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;
   
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,177,1,'01_EschTriggers_52.sql',sysdate,'9.0.0 Build#634');

commit;