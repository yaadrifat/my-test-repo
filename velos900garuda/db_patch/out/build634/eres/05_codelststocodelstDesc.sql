create or replace function codelststocodelstDesc(v_str  varchar2,v_delimiter char) return varchar2 is
v_temp varchar2(4000):='';
v_temp1 varchar2(4000):='';
v_str1 varchar2(4000):=v_str||',';
v_cstr varchar2(4000):=v_str1;
v_count number:=1;
v_tot_commas number:=to_number((LENGTH(v_cstr) - LENGTH(REPLACE(v_cstr, ',', ''))) ,999999999999);
v_temp_no number:=0;
begin

WHILE v_count <= v_tot_commas
LOOP
    dbms_output.put_line('count:::::'||v_count||'input str'||v_str1);
    v_str1:=trim(v_str1);
    v_temp:=to_number(substr(v_str1, 0, instr(v_str1, v_delimiter)),9999999999);
    dbms_output.put_line('seperated str:::::'||v_temp);
    if v_count != v_tot_commas then
      v_temp1:=v_temp1||f_codelst_desc(v_temp)||',';
    else
     v_temp1:=v_temp1||f_codelst_desc(v_temp);
    end if;
    dbms_output.put_line('Append comma:::::'||v_temp1);
    v_temp_no:=to_number(LENGTH(v_temp)+1,'9999999999');
    --dbms_output.put_line('after comma replace:::::'||v_temp_no);
    v_temp:=substr(v_str1, 0, v_temp_no);
    --dbms_output.put_line('after comma replace:::::'||v_temp);
    v_str1:=replace(v_str1,v_temp,'');
    dbms_output.put_line('after str replace:::::'||v_str1);
    v_count:=v_count+1;
END LOOP;


return v_temp1;
end;
/

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,177,5,'05_codelststocodelstDesc.sql',sysdate,'9.0.0 Build#634');

commit;
