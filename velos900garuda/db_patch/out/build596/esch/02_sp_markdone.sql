create or replace
PROCEDURE  esch."SP_MARKDONE" (
p_event_id IN NUMBER,
p_notes IN VARCHAR,
p_date IN DATE,
p_user IN NUMBER,
p_status IN NUMBER,
p_ipadd VARCHAR,
p_mode in char,
p_sosId in NUMBER,
p_coverageTypeId in NUMBER,
p_reason_for_change_CT in varchar
)
AS
v_date date;
v_status number;
v_notes varchar2(4000);

BEGIN

--update sch_events1 with newly inserted status

update sch_events1
   set NOTES = p_notes,
       EVENT_EXEON = p_date,
   EVENT_EXEBY = p_user,
   ISCONFIRMED = p_status, LAST_MODIFIED_BY = p_user, LAST_MODIFIED_DATE = sysdate,
   IP_ADD = p_ipadd,
   SERVICE_SITE_ID= p_sosId,
   FK_CODELST_COVERTYPE=p_coverageTypeId,
   REASON_FOR_COVERAGECHANGE = p_reason_for_change_CT
 where EVENT_ID = lpad(to_char(p_event_id),10,'0');


if (p_mode = 'M' ) then

    --JM: 29Aug2008 #3586 SM #4005

     update SCH_EVENTSTAT
     set  EVENTSTAT_DT = p_date,
          EVENTSTAT_NOTES =  p_notes , LAST_MODIFIED_BY = p_user, LAST_MODIFIED_DATE = sysdate, IP_ADD = p_ipadd
     where fk_event = lpad(to_char(p_event_id),10,'0')
     and PK_EVENTSTAT = (select max(PK_EVENTSTAT) from SCH_EVENTSTAT where fk_event=lpad(to_char(p_event_id),10,'0'));


end if;


if (p_mode = 'N' ) then
      UPDATE sch_eventstat
    SET eventstat_enddt = SYSDATE, LAST_MODIFIED_BY = p_user, LAST_MODIFIED_DATE = sysdate, IP_ADD = p_ipadd
  WHERE fk_event = lpad(to_char(p_event_id),10,'0')
    AND eventstat_enddt IS NULL ;
end if;

COMMIT;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,139,2,'02_sp_markdone.sql',sysdate,'9.0.0 Build#596');

commit;