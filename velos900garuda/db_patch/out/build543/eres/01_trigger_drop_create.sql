drop trigger ER_ER_PATSTUDYSTAT_BU_LM;


CREATE OR REPLACE TRIGGER "ERES"."ER_PATSTUDYSTAT_BU0" 
BEFORE UPDATE ON ER_PATSTUDYSTAT REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW


when (new.last_modified_by is not null) begin
:new.last_modified_date := sysdate ; 

end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,86,1,'01_trigger_drop_create.sql',sysdate,'8.9.0 Build#543');

commit;