update er_report set rep_sql='select
case when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient''
when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NULL) THEN ''Study''
when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NULL) THEN ''Account''
when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient Study''
end as spec_Kind,
spec.pk_specimen, SPEC_ID,spec_alternate_id,
(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen) SpecChildCnt,
(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen) PSPEC_ID,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) SPEC_TYPE,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = (SELECT s.FK_CODELST_STATUS FROM er_specimen_status s
WHERE spec.pk_specimen = s.fk_specimen AND s.PK_SPECIMEN_STATUS = (SELECT MAX(ss.PK_SPECIMEN_STATUS) FROM er_specimen_status ss
WHERE ss.fk_specimen = spec.pk_specimen AND ss.SS_DATE = (SELECT MAX(ss1.SS_DATE) FROM er_specimen_status ss1
WHERE ss1.fk_specimen = spec.pk_specimen )))) SPEC_STATUS,
SPEC_ORIGINAL_QUANTITY || '' ''||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) SPEC_ORIGINAL_QUANTITY,
TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
(SELECT site_name FROM ER_SITE WHERE pk_site =  FK_SITE) AS spec_orgn,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_ANATOMIC_SITE) SPEC_ANATOMIC_SITE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_TISSUE_SIDE) SPEC_TISSUE_SIDE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_PATHOLOGY_STAT) SPEC_PATHOLOGY_STAT,
(SELECT study_number FROM ER_STUDY WHERE pk_study = spec.FK_STUDY) STUDY_NUMBER,
(SELECT per_code FROM ER_PER WHERE pk_per = SPEC.fk_per) AS patid,
DECODE(spec.fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = spec.fk_specimen) AS spec_parent,
stor.pk_storage,STORAGE_ID,STORAGE_NAME,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE) STORE_TYPE,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME  FROM ER_USER where pk_user=spec.FK_USER_PATH ) AS SPEC_PATHOLOGY_NAME,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_SURG ) AS SPEC_SURGEON_NAME,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_COLLTECH ) AS SPEC_COLLTECH_NAME,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_PROCTECH ) AS SPEC_PROCTECH_NAME
from er_specimen spec, er_storage stor
where stor.pk_storage(+) = spec.fk_storage
AND (('':specTyp''=''NonStudy'' AND spec.fk_study is null AND spec.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND spec.fk_study in (:studyId)))
AND (spec.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = spec.fk_per and per.fk_account =:sessAccId
and usr.fk_user=:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
order by storage_id' where pk_report=145;

commit;

update er_report set rep_sql_clob='select
case when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient''
when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NULL) THEN ''Study''
when (spec.FK_STUDY IS NULL AND SPEC.fk_per IS NULL) THEN ''Account''
when (spec.FK_STUDY IS NOT NULL AND SPEC.fk_per IS NOT NULL) THEN ''Patient Study''
end as spec_Kind,
spec.pk_specimen, SPEC_ID,spec_alternate_id,
(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = spec.pk_specimen) SpecChildCnt,
(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = spec.fk_specimen) PSPEC_ID,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) SPEC_TYPE,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = (SELECT s.FK_CODELST_STATUS FROM er_specimen_status s
WHERE spec.pk_specimen = s.fk_specimen AND s.PK_SPECIMEN_STATUS = (SELECT MAX(ss.PK_SPECIMEN_STATUS) FROM er_specimen_status ss
WHERE ss.fk_specimen = spec.pk_specimen AND ss.SS_DATE = (SELECT MAX(ss1.SS_DATE) FROM er_specimen_status ss1
WHERE ss1.fk_specimen = spec.pk_specimen )))) SPEC_STATUS,
SPEC_ORIGINAL_QUANTITY || '' ''||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) SPEC_ORIGINAL_QUANTITY,
TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT)AS spec_collection_date,
(SELECT site_name FROM ER_SITE WHERE pk_site =  FK_SITE) AS spec_orgn,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_ANATOMIC_SITE) SPEC_ANATOMIC_SITE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_TISSUE_SIDE) SPEC_TISSUE_SIDE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = SPEC_PATHOLOGY_STAT) SPEC_PATHOLOGY_STAT,
(SELECT study_number FROM ER_STUDY WHERE pk_study = spec.FK_STUDY) STUDY_NUMBER,
(SELECT per_code FROM ER_PER WHERE pk_per = SPEC.fk_per) AS patid,
DECODE(spec.fk_specimen,NULL,'''' ,''Yes'') AS child_yes,
(SELECT spec_id FROM ER_SPECIMEN WHERE pk_specimen = spec.fk_specimen) AS spec_parent,
stor.pk_storage,STORAGE_ID,STORAGE_NAME,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE) STORE_TYPE,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME  FROM ER_USER where pk_user=spec.FK_USER_PATH ) AS SPEC_PATHOLOGY_NAME,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_SURG ) AS SPEC_SURGEON_NAME,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_COLLTECH ) AS SPEC_COLLTECH_NAME,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=spec.FK_USER_PROCTECH ) AS SPEC_PROCTECH_NAME
from er_specimen spec, er_storage stor
where stor.pk_storage(+) = spec.fk_storage
AND (('':specTyp''=''NonStudy'' AND spec.fk_study is null AND spec.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND spec.fk_study in (:studyId)))
AND (spec.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = spec.fk_per and per.fk_account =:sessAccId
and usr.fk_user=:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
order by storage_id' where pk_report=145;

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,4,'04_ER_REPORT_Rep#145.sql',sysdate,'8.9.0 Build#528');

commit;