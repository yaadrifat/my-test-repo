CREATE OR REPLACE PROCEDURE  "ESCH"."SP_UPDATE_EVENT_COVERTYPE" (
   p_eventId NUMBER ,
   eventCoverType NUMBER
)
AS

/** Author: Sammie Mhalagi 05/03/2010
**  This procedure updates the events Site of Service.
** the input parameters are Event Id and Site of Service id 
**
** Modification History
**
*/

BEGIN
	IF (eventCoverType = 0) THEN
		Update sch_events1 set FK_CODELST_COVERTYPE = NULL WHERE F_TO_NUMBER(EVENT_ID) = p_eventId;
	ELSE
		Update sch_events1 set FK_CODELST_COVERTYPE = eventCoverType WHERE F_TO_NUMBER(EVENT_ID) = p_eventId;
	END IF;
	COMMIT;
END ;
/


CREATE SYNONYM ERES.SP_UPDATE_EVENT_COVERTYPE FOR SP_UPDATE_EVENT_COVERTYPE;

CREATE SYNONYM EPAT.SP_UPDATE_EVENT_COVERTYPE FOR SP_UPDATE_EVENT_COVERTYPE;

GRANT EXECUTE, DEBUG ON SP_UPDATE_EVENT_COVERTYPE TO EPAT;

GRANT EXECUTE, DEBUG ON SP_UPDATE_EVENT_COVERTYPE TO ERES;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,11,'11_SP_UPDATE_EVENT_COVERTYPE.SQL',sysdate,'8.9.0 Build#528');

commit;
