update er_lkpcol set lkpcol_dispval = 'Age (in Years)' where fk_lkplib = 6001 and lkpcol_name = 'age';
commit;


set define off;

update er_browserconf set browserconf_seq = 3 where fk_browser = 2 and browserconf_colname = 'STUDY_NUMBER' and browserconf_seq = 2;
commit;

update er_browserconf set browserconf_seq = 33 where fk_browser = 3 and browserconf_colname = 'RIGHT_MASK' and browserconf_seq = 32;
commit;

update er_browserconf set browserconf_seq = 34 where fk_browser = 3 and browserconf_colname = 'MASK_PATADDRESS' and browserconf_seq = 32;
commit;


begin

for i in (select browserconf_colname,browserconf_seq from er_browserconf where browserconf_seq between 12 and 31 and fk_browser = 3 order by browserconf_seq ) loop
update er_browserconf set browserconf_seq = (i.browserconf_seq +1)  where browserconf_seq = i.browserconf_seq and browserconf_colname = i.browserconf_colname and fk_browser = 3;
commit;
end loop;

end;
/

update er_browserconf set browserconf_seq = 12 where fk_browser = 3 and browserconf_colname = 'PATSTUDYCUR_STAT' and browserconf_seq = 11;
commit;



UPDATE ER_BrowserConf
SET browserconf_settings ='{"key":"EVENT_SCHDATE_DATESORT", "label":"Scheduled Date", 
"resizeable":true,"sortable":true}',browserconf_colname='EVENT_SCHDATE_DATESORT'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='EVENT_SCHDATE'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;

update er_browserconf 
set BROWSERCONF_SETTINGS = '{"key":"NEXT_VISIT_DATESORT", "label":"Visit Date", "sortable":true, "resizeable":true,"hideable":true}',
BROWSERCONF_EXPLABEL = 'Visit Date'
where fk_browser in 
(select pk_browser from ER_BROWSER where BROWSER_MODULE ='allSchedules' and BROWSER_NAME='Patient Schedule Browser')
and BROWSERCONF_EXPLABEL = 'Next due'
and BROWSERCONF_COLNAME = 'NEXT_VISIT_DATESORT';

commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,116,1,'01_data.sql',sysdate,'8.10.0 Build#573');

commit;