set define off;
declare 
	v_code_count number;
begin

	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'mile_setstat' and codelst_subtyp = 'mile_setstat_1';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ, CREATED_ON,CODELST_STUDY_ROLE)
			 Values
			   (seq_er_codelst.nextval, NULL, 'mile_setstat', 'mile_setstat_1', 'Draft', 'N', 1, sysdate,'default_data');
		
		COMMIT;

	 end if;
   	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'mile_setstat' and codelst_subtyp = 'mile_setstat_2';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ, CREATED_ON,CODELST_STUDY_ROLE)
			 Values
			   (seq_er_codelst.nextval, NULL, 'mile_setstat', 'mile_setstat_2', 'Ready for review', 'N', 2, sysdate,'default_data');
		
		COMMIT;

	 end if;
   	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'mile_setstat' and codelst_subtyp = 'mile_setstat_3';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ, CREATED_ON,CODELST_STUDY_ROLE)
			 Values
			   (seq_er_codelst.nextval, NULL, 'mile_setstat', 'mile_setstat_3', 'Approved', 'N', 3, sysdate,'default_data');
		
		COMMIT;

	 end if;
   	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'mile_setstat' and codelst_subtyp = 'mile_setstat_4';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ, CREATED_ON,CODELST_STUDY_ROLE)
			 Values
			   (seq_er_codelst.nextval, NULL, 'mile_setstat', 'mile_setstat_4', 'Offline', 'N', 4, sysdate,'default_data');
		
		COMMIT;

	 end if;
  end;
/
  
  

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,116,3,'03_Insert_New_CODETYPE_ER_CODELST.sql',sysdate,'8.10.0 Build#573');

commit;