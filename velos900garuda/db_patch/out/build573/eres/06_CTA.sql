/* Formatted on 2/9/2010 1:39:29 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_PATSTUDY_LATEST_EVE
(
   PATSTUDYSTAT_ID,
   PATSTUDYSTAT_DESC,
   FK_PER,
   PER_CODE,
   FK_STUDY,
   STUDY_NUMBER,
   FK_ACCOUNT,
   PATSTUDYSTAT_DATE,
   PATSTUDYSTAT_NOTE,
   PATSTUDYSTAT_REASON,
   PATPROT_ENROLDT,
   PK_PATPROT,
   PK_PATSTUDYSTAT,
   PER_SITE,
   PATSTUDYSTAT_SUBTYPE,
   EVENT_PROTOCOL_ID,
   SCH_EVENTS1_PK,
   EVENT_STATUS_ID,
   EVENT_START_DATE,
   EVENT_ID_ASSOC,
   EVENT_EXEON,
   EVENT_EXEBY,
   EVENT_ACTUAL_SCHDATE,
   VISIT,
   EVENT_STATUS_DESC,
   EVENT_STATUS_CODELST_SUBTYP,
   STUDY_TITLE,
   PATPROT_PATSTDID,
   FK_USERASSTO,
   PI,
   ASSIGNEDTO_NAME,
   PATPROT_PHYSICIAN,
   PHYSICIAN_NAME,
   FK_USER,
   ENROLLEDBY_NAME,
   PATPROT_TREATINGORG,
   TREATINGORG_NAME,
   FK_VISIT
)
AS
     SELECT   b.fk_codelst_stat status_id,                                 --1
              f.codelst_desc status_desc,                                  --2
              b.fk_per,
              d.per_code,                                                  --4
              b.fk_study,                                                  --5
              e.study_number,                                              --6
              e.fk_account,                                                --7
              b.patstudystat_date,                                         --8
              b.patstudystat_note,
              b.patstudystat_reason,
              c.patprot_enroldt,                                           --9
              c.pk_patprot,                                               --10
              b.pk_patstudystat,                                          --11
              c.fk_site_enrolling,                                        --12
              f.codelst_subtyp,                                           --13
              c.fk_protocol,                                              --14
              s.event_id,
              s.isconfirmed,                                              --16
              s.start_date_time,                                          --17
              s.fk_assoc,                                                 --18
              s.event_exeon,                                              --19
              s.event_exeby,                                              --20
              s.actual_schdate,                                           --21
              s.visit,                                                    --22
              (SELECT   c.codelst_desc
                 FROM   sch_codelst c
                WHERE   pk_codelst = s.isconfirmed),                      --23
              (SELECT   c.codelst_subtyp
                 FROM   sch_codelst c
                WHERE   pk_codelst = s.isconfirmed),                      --24
              e.study_title,                                              --25
              c.patprot_patstdid,
              c.fk_userassto,
              (SELECT   usr_firstname || ' ' || usr_lastname 
              	 FROM er_user 
              	WHERE pk_user = e.STUDY_PRINV) 
              	 AS PI, 
              (SELECT   usr_firstname || ' ' || usr_lastname
                 FROM   er_user
                WHERE   pk_user = c.fk_userassto)
                 AS assignedto_name,
              c.patprot_physician,
              (SELECT   usr_firstname || ' ' || usr_lastname
                 FROM   er_user
                WHERE   pk_user = c.patprot_physician)
                 AS physician,
              c.fk_user,
              (SELECT   usr_firstname || ' ' || usr_lastname
                 FROM   er_user
                WHERE   pk_user = c.fk_user)
                 AS enrolledby,
              c.patprot_treatingorg,
              (SELECT   site_name
                 FROM   er_site
                WHERE   pk_site = c.patprot_treatingorg)
                 AS treatingorg_name,
   			FK_VISIT
       FROM   er_patstudystat b,
              er_patprot c,
              er_per d,
              er_study e,
              er_codelst f,
              sch_events1 s
      WHERE       c.patprot_stat = 1
              AND c.pk_patprot = s.fk_patprot(+)
              AND b.fk_per = c.fk_per
              AND b.fk_study = c.fk_study
              AND b.pk_patstudystat =
                    NVL (
                       (SELECT   MAX (pk_patstudystat)
                          FROM   er_patstudystat
                         WHERE   patstudystat_date =
                                    NVL (
                                       (SELECT   MAX (patstudystat_date)
                                          FROM   er_patstudystat
                                         WHERE   fk_per = c.fk_per
                                                 AND fk_study = c.fk_study
                                                 AND fk_codelst_stat IN
                                                          (SELECT   pk_codelst
                                                             FROM   er_codelst
                                                            WHERE   codelst_type =
                                                                       'patStatus'
                                                                    AND codelst_custom_col =
                                                                          'browser')),
                                       (SELECT   MAX (patstudystat_date)
                                          FROM   er_patstudystat
                                         WHERE   fk_per = c.fk_per
                                                 AND fk_study = c.fk_study)
                                    )
                                 AND fk_per = c.fk_per
                                 AND fk_study = c.fk_study
                                 AND fk_codelst_stat IN
                                          (SELECT   pk_codelst
                                             FROM   er_codelst
                                            WHERE   codelst_type = 'patStatus'
                                                    AND codelst_custom_col =
                                                          'browser')),
                       -- JM: 03/09/2006: Modified for bugzilla issue #2516
                       -- (select max(pk_patstudystat) from er_patstudystat where fk_per=c.fk_per and FK_STUDY = c.FK_STUDY))  and
                       (SELECT   MAX (pk_patstudystat)
                          FROM   er_patstudystat
                         WHERE   fk_per = c.fk_per AND fk_study = c.fk_study
                                 AND patstudystat_date =
                                       (SELECT   MAX (patstudystat_date)
                                          FROM   er_patstudystat
                                         WHERE   fk_per = c.fk_per
                                                 AND fk_study = c.fk_study))
                    )
              AND b.fk_per = d.pk_per
              AND b.fk_study = e.pk_study
              AND e.study_verparent IS NULL
              AND e.study_actualdt IS NOT NULL
              AND b.fk_codelst_stat = f.pk_codelst
   ORDER BY   fk_study, fk_per;


CREATE OR Replace SYNONYM ESCH.ERV_PATSTUDY_LATEST_EVE FOR ERV_PATSTUDY_LATEST_EVE;


CREATE OR Replace SYNONYM EPAT.ERV_PATSTUDY_LATEST_EVE FOR ERV_PATSTUDY_LATEST_EVE;


CREATE OR REPLACE FORCE VIEW "ERES"."ERV_STUDYPAT_UPCOMING_VISIT" ("FK_PER", "PER_SITE", "STUDY_NUMBER", "STUDY_TITLE", "PK_PATPROT", "FK_STUDY", "PER_CODE", "PATSTUDYSTAT_DESC", "PATSTUDYSTAT_SUBTYPE", "PATSTUDYSTAT_ID", "PK_PATSTUDYSTAT", "PATSTUDYSTAT_DATE", "PATSTUDYSTAT_NOTE", "PATSTUDYSTAT_REASON", "PATPROT_ENROLDT", "PATPROT_PATSTDID", "FK_VISIT", "EVENT_ACTUAL_SCHDATE", "NEXT_VISIT", "NEXT_VISIT_NAME", "NEXT_VISIT_NO", "PI", "ASSIGNEDTO_NAME", "PHYSICIAN_NAME", "ENROLLEDBY_NAME", "TREATINGORG_NAME")
AS
  SELECT DISTINCT fk_per,
    per_site,
    study_number,
    study_title,
    pk_patprot,
    fk_study,
    per_code,
    patstudystat_desc,
    patstudystat_subtype,
    patstudystat_id,
    pk_patstudystat,
    patstudystat_date,
    patstudystat_note,
    patstudystat_reason,
    patprot_enroldt,
    patprot_patstdid,
    fk_visit,
    EVENT_ACTUAL_SCHDATE,
    (EVENT_ACTUAL_SCHDATE) next_visit,
    (SELECT visit_name FROM sch_protocol_visit WHERE pk_protocol_visit = fk_visit
    ) next_visit_name,
    fk_visit next_visit_no,
    PI,
    assignedto_name,
    physician_name,
    enrolledby_name,
    treatingorg_name
  FROM erv_patstudy_latest_eve o;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,116,6,'06_CTA.sql',sysdate,'8.10.0 Build#573');

commit;