set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '9.0.0 Build#588' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,131,0,'00_er_version.sql',sysdate,'9.0.0 Build#588');

commit;

