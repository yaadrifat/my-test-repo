set define off;

update er_milestone set FK_VISIT = -1, MILESTONE_VISIT = -1 where MILESTONE_TYPE = 'VM' and FK_VISIT =0;

commit;
 

DELETE FROM ER_REPXSL WHERE FK_REPORT in (110,159);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,120,2,'02_data.sql',sysdate,'8.10.0 Build#577');

commit;