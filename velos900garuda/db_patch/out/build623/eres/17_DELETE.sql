set define off;


--STARTS DELETING THE OLD IDM RECORD FROM ER_PATLABS--

delete from er_patlabs where FK_TESTGROUP =(select pk_labgroup from er_labgroup where group_type='I');
delete from er_patlabs where FK_TESTGROUP =(select pk_labgroup from er_labgroup where group_type='IOG');

--END--

--STARTS DELETING THE OLD IDM RECORD FROM CB_ASSESSMENT--

delete from cb_assessment where SUB_ENTITY_TYPE =(select pk_codelst from er_codelst where codelst_subtyp='IDM');

--END--
--STARTS DELETING THE OLD IDM RECORD FROM CB_IDM--
delete from cb_idm;
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,166,17,'17_DELETE.sql',sysdate,'9.0.0 Build#623');

commit;
