--When ER_ORDER_HEADER table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ERES"."ER_ORDER_HEADER_AU1" AFTER UPDATE ON eres.ER_ORDER_HEADER 
referencing OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;
	
	pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER_HEADER',:OLD.rid,:OLD.PK_ORDER_HEADER,'U',:NEW.creator);
	
IF NVL(:OLD.PK_ORDER_HEADER,0) != NVL(:NEW.PK_ORDER_HEADER,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'PK_ORDER_HEADER',:OLD.PK_ORDER_HEADER, :NEW.PK_ORDER_HEADER,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_GUID,' ') !=  NVL(:NEW.ORDER_GUID,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_GUID',:OLD.ORDER_GUID, :NEW.ORDER_GUID,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_NUM,' ') !=  NVL(:NEW.ORDER_NUM,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_NUM',:OLD.ORDER_NUM, :NEW.ORDER_NUM,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_ENTITYID,0) !=  NVL(:NEW.ORDER_ENTITYID,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_ENTITYID',:OLD.ORDER_ENTITYID, :NEW.ORDER_ENTITYID,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_ENTITYTYPE,0) != NVL(:NEW.ORDER_ENTITYTYPE,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_ENTITYTYPE',:OLD.ORDER_ENTITYTYPE, :NEW.ORDER_ENTITYTYPE,NULL,NULL);
  END IF;
 IF NVL(:OLD.ORDER_OPEN_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=
     NVL(:NEW.ORDER_OPEN_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_OPEN_DATE',
       TO_CHAR(:OLD.ORDER_OPEN_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ORDER_OPEN_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_CLOSE_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=
     NVL(:NEW.ORDER_CLOSE_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_CLOSE_DATE',TO_CHAR(:OLD.ORDER_CLOSE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ORDER_CLOSE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
 IF NVL(:OLD.FK_ORDER_TYPE,0) != NVL(:NEW.FK_ORDER_TYPE,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_ORDER_TYPE',:OLD.FK_ORDER_TYPE, :NEW.FK_ORDER_TYPE,NULL,NULL);
  END IF;
 IF NVL(:OLD.FK_ORDER_STATUS,0) !=NVL(:NEW.FK_ORDER_STATUS,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_ORDER_STATUS',:OLD.FK_ORDER_STATUS, :NEW.FK_ORDER_STATUS,NULL,NULL);
  END IF;
IF NVL(:OLD.ORDER_STATUS_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) != NVL(:NEW.ORDER_STATUS_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_STATUS_DATE',
       TO_CHAR(:OLD.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_REMARKS,' ') !=  NVL(:NEW.ORDER_REMARKS,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_REMARKS',:OLD.ORDER_REMARKS, :NEW.ORDER_REMARKS,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_REQUESTED_BY,0) !=  NVL(:NEW.ORDER_REQUESTED_BY,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_REQUESTED_BY',:OLD.ORDER_REQUESTED_BY, :NEW.ORDER_REQUESTED_BY,NULL,NULL);
  END IF;
IF NVL(:OLD.ORDER_APPROVED_BY,0) !=   NVL(:NEW.ORDER_APPROVED_BY,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_APPROVED_BY',:OLD.ORDER_APPROVED_BY, :NEW.ORDER_APPROVED_BY,NULL,NULL);
  END IF;  
 IF NVL(:OLD.CREATOR,0) !=   NVL(:NEW.CREATOR,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
  END IF;
   IF NVL(:OLD.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=  NVL(:NEW.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
 IF nvl(:OLD.last_modified_by,0) != nvl(:NEW.last_modified_by,0) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'LAST_MODIFIED_BY',:OLD.last_modified_by,:NEW.last_modified_by,NULL,NULL);
      END IF;   
   IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=
     NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
   IF NVL(:OLD.IP_ADD,' ') !=  NVL(:NEW.IP_ADD,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF;  
  IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
  END IF;
  IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'RID',:OLD.RID, :NEW.RID,NULL,NULL);
  END IF;  
 IF NVL(:OLD.FK_SITE_ID,0) !=NVL(:NEW.FK_SITE_ID,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_SITE_ID',:OLD.FK_SITE_ID, :NEW.FK_SITE_ID,NULL,NULL);
  END IF;  
  END;
  /
  
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,161,9,'09_ER_ORDER_HEADER_AU1.sql',sysdate,'9.0.0 Build#618');

commit;


