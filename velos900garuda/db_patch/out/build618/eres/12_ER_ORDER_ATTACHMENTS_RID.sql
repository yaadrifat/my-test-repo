set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  ER_ORDER_ATTACHMENTS WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update ER_ORDER_ATTACHMENTS  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,161,12,'12_ER_ORDER_ATTACHMENTS_RID.sql',sysdate,'9.0.0 Build#618');

commit;
