Set Define Off;

Create table CB_ENTITY_SAMPLES
(
PK_ENTITY_SAMPLES NUMBER(10) Primary Key,
ENTITY_ID NUMBER(10),
ENTITY_TYPE NUMBER(10),
FK_SAMPLE_TYPE NUMBER(10),
FK_SAMPLE_STATUS NUMBER(10),
SAMPLE_QUANTITY NUMBER(10,2),
SAMPLE_VOLUME NUMBER(10,2),
QC_INDICATOR VARCHAR2(1),
RECEIVE_DATE DATE,
STORAGE_DATE DATE,
SEQUENCE NUMBER(10),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_ON DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_ENTITY_SAMPLES" IS 'Table to store cord samples';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."PK_ENTITY_SAMPLES" IS 'Primary key of the table';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."ENTITY_ID" IS 'This column will store the entity id.';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."ENTITY_TYPE" IS 'This column will store type of entity.';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."FK_SAMPLE_TYPE" IS 'Cord Sample Type.';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."FK_SAMPLE_STATUS" IS 'Cord sample status.';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."SAMPLE_QUANTITY" IS 'This column wil store the sample quantity.';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."SAMPLE_VOLUME" IS 'This column will store volume of the sample';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."QC_INDICATOR" IS 'Quality control indicator for sample.';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."RECEIVE_DATE" IS 'This column will store receive date of sample';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."STORAGE_DATE" IS 'This column will store storage date of sample';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."SEQUENCE" IS 'Sequence of sample - ESB mapping.';
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."LAST_MODIFIED_ON" IS 'This column stores the date on which column last modified.'; 
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.'; 
COMMENT ON COLUMN "CB_ENTITY_SAMPLES"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE SEQ_CB_ENTITY_SAMPLES MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,140,1,'01_CB_CREATE_TABLE.sql',sysdate,'9.0.0 Build#597');

commit;
