CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTCOST_BD0"
BEFORE DELETE
ON SCH_EVENTCOST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  v_name Varchar2(4000);
  v_protocol Number;
  v_creator Number;
  v_ipadd Varchar2(20);
  v_visit Number;
  v_cpt varchar2(50);
  v_desc  varchar2(4000);
  v_line_category number;
  v_add_line boolean := false;
  v_pk_lineitem Number;
  v_calledfrom char(1);
  v_event_seq number;
  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_EVENTCOST_BD0', pLEVEL  => Plog.LFATAL);

 BEGIN
    -- check in event_assoc

    begin

    select name, chain_id, creator , ip_Add , fk_visit,
    event_cptcode,event_line_category,description ,event_sequence
    into v_name,v_protocol,v_creator,v_ipadd,v_visit,v_cpt,v_line_category,v_desc,v_event_seq
    from event_assoc
    where event_id = :old.fk_event and event_type = 'A' and nvl(displacement,-99999999) <> 0;

  -- nvl for displacement because events added to 'no interval visit' has displacement as null

        v_add_line := true;
        v_calledfrom := 'S';

    exception when others then
        v_add_line := false;
    end;


 if v_add_line = true then -- check if there is a lineitem added for this event cost:

     begin
         select pk_lineitem
         into v_pk_lineitem
         from sch_lineitem l,sch_budget,sch_bgtcal,sch_bgtsection
         where l.fk_event = :old.fk_event 
         and l.fk_bgtsection = pk_budgetsec and sch_bgtsection.fk_visit = v_visit
         and PK_BGTCAL = fk_bgtcal  and bgtcal_protid = v_protocol  and pk_budget = fk_budget
         and budget_calendar = v_protocol
         and l.FK_CODELST_COST_TYPE = :old.FK_COST_DESC
         and rownum < 2;

     exception when no_data_found then
         v_pk_lineitem := 0;
     end ;


     --delete lineitem
     if v_pk_lineitem > 0 then
         begin
             delete from sch_lineitem
             where pk_lineitem = v_pk_lineitem;
         exception when no_data_found then
             plog.fatal(pCTX,'Exception:'||sqlerrm);
         end ;
     end if;

 end if;

END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,84,3,'03_trigger.sql',sysdate,'8.9.0 Build#541');

commit;