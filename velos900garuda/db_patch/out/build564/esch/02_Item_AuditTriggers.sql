set define off;

create or replace TRIGGER ESCH.SCH_SUBCOST_ITEM_AD0 AFTER DELETE ON SCH_SUBCOST_ITEM
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;

begin
select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ESCH_SUBCOST_ITEM', :old.rid, 'D');
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_SUBCOST_ITEM) || '|' ||
to_char(:old.FK_CALENDAR) || '|' ||
:old.SUBCOST_ITEM_NAME || '|' ||
:old.SUBCOST_ITEM_COST || '|' ||
:old.SUBCOST_ITEM_UNIT || '|' ||
to_char(:old.SUBCOST_ITEM_SEQ) || '|' ||
to_char(:old.FK_CODELST_CATEGORY) || '|' ||
to_char(:old.FK_CODELST_COST_TYPE) || '|' ||
:old.SUBCOST_ITEM_DELFLAG || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
:old.IP_ADD;

insert into AUDIT_DELETE
(raid, row_data) values (raid, deleted_data);
end;
/


create or replace TRIGGER ESCH."SCH_SUBCOST_ITEM_AU0"
AFTER UPDATE
ON ESCH.SCH_SUBCOST_ITEM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
--Created by Manimaran for Audit Update   
begin
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   audit_trail.record_transaction(raid, 'ER_SUBCOST_ITEM', :old.rid, 'U', usr);
   
   if nvl(:old.PK_SUBCOST_ITEM,0) !=
     nvl(:new.PK_SUBCOST_ITEM,0) then
     audit_trail.column_update
       (raid, 'PK_SUBCOST_ITEM',
       :old.PK_SUBCOST_ITEM, :new.PK_SUBCOST_ITEM);
   end if;
   
   if nvl(:old.FK_CALENDAR,0) !=
     nvl(:new.FK_CALENDAR,0) then
     audit_trail.column_update
       (raid, 'FK_CALENDAR',
       :old.FK_CALENDAR, :new.FK_CALENDAR);
   end if;
   
   if nvl(:old.SUBCOST_ITEM_NAME,' ') !=
     nvl(:new.SUBCOST_ITEM_NAME,' ') then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_NAME',
       :old.SUBCOST_ITEM_NAME, :new.SUBCOST_ITEM_NAME);
   end if;
   
   if nvl(:old.SUBCOST_ITEM_COST,' ') !=
     nvl(:new.SUBCOST_ITEM_COST,' ') then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_COST',
       :old.SUBCOST_ITEM_COST, :new.SUBCOST_ITEM_COST);
  end if;
  
  if nvl(:old.SUBCOST_ITEM_UNIT,' ') !=
     nvl(:new.SUBCOST_ITEM_UNIT,' ') then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_UNIT',
       :old.SUBCOST_ITEM_UNIT, :new.SUBCOST_ITEM_UNIT);
  end if;
  
  if nvl(:old.SUBCOST_ITEM_SEQ,0) !=
     nvl(:new.SUBCOST_ITEM_SEQ,0) then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_SEQ',
       :old.SUBCOST_ITEM_SEQ, :new.SUBCOST_ITEM_SEQ);
  end if;
  
  if nvl(:old.FK_CODELST_CATEGORY,0) !=
     nvl(:new.FK_CODELST_CATEGORY,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_CATEGORY',
       :old.FK_CODELST_CATEGORY, :new.FK_CODELST_CATEGORY);
  end if;
  
  if nvl(:old.FK_CODELST_COST_TYPE,0) !=
     nvl(:new.FK_CODELST_COST_TYPE,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_COST_TYPE',
       :old.FK_CODELST_COST_TYPE, :new.FK_CODELST_COST_TYPE);
  end if;
  
  if nvl(:old.SUBCOST_ITEM_DELFLAG,' ') !=
     nvl(:new.SUBCOST_ITEM_DELFLAG,' ') then
     audit_trail.column_update
       (raid, 'SUBCOST_ITEM_DELFLAG',
       :old.SUBCOST_ITEM_DELFLAG, :new.SUBCOST_ITEM_DELFLAG);
  end if;
  
  IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   
   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;
   
   IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
 END;
/ 


  create or replace TRIGGER "ESCH"."SCH_SUBCOST_ITEM_BI0"
  BEFORE INSERT
  ON SCH_SUBCOST_ITEM
  REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
  DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(2000);
  insert_data CLOB;
--Created by Manimaran for Audit Insert
BEGIN

 BEGIN
   usr := getuser(:NEW.creator);
   EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction  (raid, 'SCH_SUBCOST_ITEM', erid, 'I', usr );
  insert_data:= :NEW.PK_SUBCOST_ITEM||'|'|| :NEW.FK_CALENDAR||'|'|| :NEW.SUBCOST_ITEM_NAME||'|'||
      :NEW.SUBCOST_ITEM_COST||'|'||:NEW.SUBCOST_ITEM_UNIT||'|'||:NEW.SUBCOST_ITEM_SEQ||'|'||:NEW.FK_CODELST_CATEGORY||'|'||
	  :NEW.FK_CODELST_COST_TYPE||'|'||:NEW.SUBCOST_ITEM_DELFLAG||'|'||:NEW.RID||'|'|| :NEW.CREATOR||'|'||
    :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;
   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


CREATE OR REPLACE TRIGGER "ESCH"."SCH_SUBCOST_ITEM_BU0" 
BEFORE UPDATE
ON SCH_SUBCOST_ITEM
FOR EACH ROW
when (NEW.last_modified_by IS NOT NULL) 
--Created by Manimaran for Audit Update
begin 
	:new.last_modified_date := sysdate;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,107,2,'02_Item_AuditTriggers.sql',sysdate,'8.10.0 Build#564');

commit;


