CREATE OR REPLACE TRIGGER CB_MAIL_DOCUMENTS_BI0 BEFORE INSERT ON CB_MAIL_DOCUMENTS   
REFERENCING OLD AS OLD NEW AS NEW 
	FOR EACH ROW 
	DECLARE
	raid NUMBER(10);
	erid NUMBER(10);
	usr VARCHAR(2000);
	insert_data VARCHAR2(4000);
	 BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_MAIL_DOCUMENTS',erid, 'I',usr);
       insert_data:=:NEW.PK_MAIL_DOCUMENTS || '|' ||
TO_CHAR(:NEW.DOCUMENT_FILE) || '|' ||
TO_CHAR(:NEW.DOCUMENT_CONTENTTYPE) || '|' ||
TO_CHAR(:NEW.DOCUMENT_FILENAME) || '|' ||
TO_CHAR(:NEW.ENTITY_ID) || '|' ||
TO_CHAR(:NEW.ENTITY_TYPE) || '|' ||
TO_CHAR(:NEW.ATTACHMENT_TYPE) || '|' ||
TO_CHAR(:NEW.CREATOR) || '|' ||
TO_CHAR(:NEW.RID);
    INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,172,25,'25_CB_MAIL_DOCUMENTS_BI0.sql',sysdate,'9.0.0 Build#629');

commit;