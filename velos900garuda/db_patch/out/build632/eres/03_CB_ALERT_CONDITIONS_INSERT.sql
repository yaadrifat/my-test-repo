create or replace
PROCEDURE SP_ALERTS(entity_id number,entity_type number) AS
TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
cur_cv   CurTyp;  -- declare cursor variable
v_alertId number;
v_alert_title varchar2(4000);
v_alert_desc varchar2(4000);
v_alert_wording varchar2(4000);
v_alert_precondition varchar2(4000) :='';
v_col_name varchar2(4000);
v_alert_precond_cnt number :=0;
v_query varchar2(4000);
sql_fieldval varchar2(4000);
var_instance_cnt number;
v_userId number;
v_cordregid varchar2(255);
V_ORDERTYPE VARCHAR2(255);
v_sampleatlab varchar2(1);
V_ALERTNAME VARCHAR2(255);
v_resol number:=0;
v_resoldesc VARCHAR2(50 CHAR):='N/A';
v_orderId number;
v_schshipdate varchar2(50);
v_temp number;
V_ALERT_ID_TEMP NUMBER;
v_entity_order number;
v_entity_cord number;
BEGIN
FOR CUR_ALERTS IN(select pk_codelst,CODELST_SUBTYP,codelst_desc,codelst_custom_col1,codelst_custom_col from er_codelst where codelst_type in('alert','alert_resol') AND CODELST_HIDE='N' order by pk_codelst)
LOOP
  v_alertId := cur_alerts.pk_codelst;
  V_ALERTNAME:=cur_alerts.codelst_subtyp;
	v_alert_title := cur_alerts.codelst_desc;
	v_alert_desc := cur_alerts.codelst_custom_col1;
	v_alert_wording :=cur_alerts.codelst_custom_col;
  v_alert_precondition := getalertcondition(v_alertid,'PRE');
if v_alert_precondition<>' ' then
    dbms_output.put_line('alert Id :::'||v_alertid);
    dbms_output.put_line('alert pre condition is::'||v_alert_precondition);

    if v_alertname in('alert_01','alert_02','alert_03','alert_04','alert_15','alert_27','alert_20','alert_16','alert_29','alert_30','alert_31','alert_32','alert_33','alert_34','alert_35','alert_36','alert_37','alert_38','alert_39') then
      v_query := 'select count(*) cnt from er_order where '||v_alert_precondition;
    elsif v_alertname in('alert_05','alert_06') then
      v_query := 'select count(*) cnt from er_order,er_order_header where er_order.fk_order_header=er_order_header.pk_order_header and '||v_alert_precondition;
    elsif v_alertname in('alert_07','alert_08','alert_09','alert_10','alert_11') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_shipment where er_order.fk_order_header=er_order_header.pk_order_header and cb_shipment.fk_order_id=er_order.pk_order and '||v_alert_precondition;
    elsif v_alertname='alert_19' then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and '||v_alert_precondition;
    elsif v_alertname IN('alert_23','alert_24','alert_25','alert_26') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord,er_attachments,cb_upload_info where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and er_attachments.entity_id=cb_cord.pk_cord and er_attachments.entity_type=(select pk_codelst from er_codelst where codelst_type=''entity_type'' and codelst_subtyp=''CBU'') and er_attachments.pk_attachment=cb_upload_info.fk_attachmentid and '||v_alert_precondition;
    end if;
    v_query:=replace(v_query,'#keycolumn',entity_id);
     dbms_output.put_line('^^^^^^^^^^^'||v_query);
    open cur_cv for v_query;
     LOOP
          FETCH cur_cv INTO sql_fieldval ;
          EXIT WHEN cur_cv%NOTFOUND;
           dbms_output.put_line('display result  ' || sql_fieldval );
           if sql_fieldval>0 then
           select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
           select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
           if entity_type=v_entity_order then
              select nvl(assigned_to,0),nvl(crd.cord_registry_id,'N/A'),nvl(ord.order_sample_at_lab,'-'),case when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is null then 0 when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is not null then ord.fk_order_resol_by_cbb when ord.fk_order_resol_by_tc is not null and ord.fk_order_resol_by_cbb is null then ord.fk_order_resol_by_tc else 0 end into v_userid,v_cordregid,v_sampleatlab,v_resol from er_order ord left outer join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) where pk_order=entity_id;
              dbms_output.put_line('v_resol............'||v_resol);
              select ord.pk_order,nvl(to_char(ship.sch_shipment_date,'Mon DD, YYYY'),'N/A') into v_temp,v_schshipdate from er_order ord left outer join (select fk_order_id,sch_shipment_date from cb_shipment where fk_order_id=entity_id and fk_shipment_type=(select pk_codelst from er_codelst where codelst_type='shipment_type' and codelst_subtyp='CBU Shipment')) ship on(ship.fk_order_id=ord.pk_order) where ord.pk_order=entity_id;
            end if;
            if entity_type=v_entity_cord then
              select nvl(cord_registry_id,'N/A') into v_cordregid from cb_cord where pk_cord=entity_id;
            end if;
           if v_resol<>0 then
           dbms_output.put_line('before getting resolution desc...............');
            select cbu_status_desc into v_resoldesc from cb_cbu_status where pk_cbu_status=v_resol;
            dbms_output.put_line('after getting resolution desc...............'||v_resoldesc);
           end if;

           v_alert_wording:=replace(v_alert_wording,'<CBU Registry ID>',v_cordregid);
           v_alert_wording:=replace(v_alert_wording,'<Request Type>',v_ordertype);
           v_alert_wording:=replace(v_alert_wording,'<Resolution Code>',v_resoldesc);
           v_alert_wording:=replace(v_alert_wording,'<Shipment Scheduled Date>',v_schshipdate);
           if v_sampleatlab='Y' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Lab');
           elsif v_sampleatlab='N' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Ship');
           end if;
           select pk_codelst into v_alert_id_temp from er_codelst where codelst_type='alert' and codelst_subtyp='alert_20';
           dbms_output.put_line('v_alert_id_temp::::::::'||v_alert_id_temp);
           select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=entity_id and alrt_inst.entity_type=entity_type and alrt_inst.fk_userid=v_userid and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify');
                dbms_output.put_line('var_instance_cnt:::::'||var_instance_cnt);
            if var_instance_cnt=0 or v_alert_id_temp=v_alertid then

               insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,entity_id,entity_type,v_userid,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify'),v_alert_wording);

            end if;
            for usrs in(select altr.FK_USER,altr.EMAIL_FLAG email_flag,altr.NOTIFY_FLAG notify_flag,altr.PROMPT_FLAG prompt_flag,addr.add_email mail from cb_user_alerts altr,er_user usr,er_add addr where altr.fk_user=usr.pk_user and usr.fk_peradd=addr.pk_add and fk_codelst_alert=v_alertid)
            loop
              dbms_output.put_line('users that has assigned alert is:::'||usrs.fk_user);
              if usrs.email_flag=1 then
              select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=entity_id and alrt_inst.entity_type=entity_type and alrt_inst.entity_type=(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER') and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail');
              if var_instance_cnt=0 or v_alert_id_temp=v_alertid then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,entity_id,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER'),usrs.fk_user,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail'),usrs.mail,v_alert_wording);
              dbms_output.put_line('Email alerts implemented');
                insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
              end if;
              end if;
              end loop;
              commit;
           end if;
      END LOOP;
    dbms_output.put_line('alert wording is::'||v_alert_wording);
end if;
END LOOP;
if entity_type=v_entity_order then
  update er_order set data_modified_flag='' where pk_order=entity_id;
end if;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
  dbms_output.put_line ('A SELECT...INTO did not return any row.');
end;
/


create or replace
function getAlertCondition(alertId number,alerttype varchar2) return varchar2 is
var_alertCondition varchar2(32767):='';
var_keycond varchar2(32767):='';
var_temp varchar2(32767) := '';
var_start_brace varchar2(32767):='';
var_table_name varchar2(32767):='';
var_column_name varchar2(32767):='';
var_condition_value varchar2(32767):='';
var_arithematic_operator varchar2(32767):='';
var_logical_operator varchar2(32767):='';
var_end_brace varchar2(32767):='';
var_key_condition varchar2(32767):='';

begin
for alerts in (select START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION from cb_alert_conditions where upper(condition_type)=alerttype and fk_codelst_alert=alertId order by PK_ALERT_CONDITIONS)
loop
var_start_brace :=alerts.START_BRACE;
var_table_name:=alerts.TABLE_NAME;
var_column_name :=alerts.COLUMN_NAME;
var_condition_value :=alerts.CONDITION_VALUE;
var_arithematic_operator :=alerts.ARITHMETIC_OPERATOR;
var_logical_operator :=alerts.LOGICAL_OPERATOR;
var_end_brace :=alerts.END_BRACE;
var_key_condition :=alerts.KEY_CONDITION;

--dbms_output.put_line('var_temp:::::::::'||var_temp||'...............');
--dbms_output.put_line('alerts.key_condition:::::::::'||var_key_condition);
if var_temp = var_key_condition then
  dbms_output.put_line('testttttttttttt');
else
  var_keycond := var_keycond||'AND '||var_key_condition;
  --dbms_output.put_line('???????????????????????????????'||var_keycond);
  var_temp:=alerts.key_condition;
  end if;
  var_alertcondition := var_alertcondition||var_start_brace||var_table_name||'.'||var_column_name||var_arithematic_operator||var_condition_value||var_end_brace||' '||var_logical_operator||' ';
end loop;
--dbms_output.put_line('Alert condition is===========>'||var_alertcondition);
if var_alertcondition='' then
var_alertcondition:='select count(*) from er_order where pk_order=0';
end if;
var_alertcondition:=var_alertcondition||var_keycond;
return var_alertcondition;
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('ERROR IS:::');
var_alertcondition:=var_alertcondition||var_keycond;
return var_alertcondition;
end;
/



CREATE OR REPLACE TRIGGER ER_ORDER_ALERTS
AFTER INSERT OR UPDATE ON ERES.ER_ORDER
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
INSERT INTO CB_ALERTS_MAINTENANCE(ENTITY_ID,ENTITY_TYPE) VALUES(:NEW.PK_ORDER,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='ORDER'));
END;
/


create or replace
procedure SP_RESOLVE_ORDERS(V_ORDERID NUMBER,V_ORDERTYPE VARCHAR2)
AS
V_CORDID NUMBER;
V_PATIENTID NUMBER;
V_ORDER_ID NUMBER;
V_ORDER_TYPE VARCHAR2(250 CHAR);
V_PK_ORDER_TYE NUMBER;
BEGIN
SELECT ORDHDR.ORDER_ENTITYID,PAT.FK_PERSON_ID INTO V_CORDID,V_PATIENTID FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR,CB_RECEIPANT_INFO PAT WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND PAT.FK_ORDER_ID=ORD.PK_ORDER AND ORD.PK_ORDER=V_ORDERID AND ORDHDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU');
dbms_output.put_line('CORD ID :::'||V_CORDID);
dbms_output.put_line('PATIENT ID :::'||V_PATIENTID);
FOR ORDERS IN(SELECT ORD.PK_ORDER ORDER_ID,ORD.ORDER_TYPE ORDER_TYPE FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR,CB_RECEIPANT_INFO PAT WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND PAT.FK_ORDER_ID=ORD.PK_ORDER AND ORDHDR.ORDER_ENTITYID=V_CORDID AND ORDHDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU') AND PAT.FK_PERSON_ID=V_PATIENTID AND ORD.PK_ORDER<>V_ORDERID AND (ORD.FK_ORDER_RESOL_BY_TC IS NULL AND ORD.FK_ORDER_RESOL_BY_CBB IS NULL))
LOOP
dbms_output.put_line('ORDER ID :::'||ORDERS.ORDER_ID);
V_ORDER_ID:=ORDERS.ORDER_ID;
dbms_output.put_line('ORDER TYPE :::'||ORDERS.ORDER_TYPE);
V_ORDER_TYPE:=ORDERS.ORDER_TYPE;
IF V_ORDERTYPE='CTORDER' THEN
SELECT PK_CODELST INTO V_PK_ORDER_TYE FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='CT';
  IF V_ORDER_TYPE=V_PK_ORDER_TYE THEN
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RCT'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  ELSE
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CT'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  END IF;
END IF;
IF V_ORDERTYPE='HEORDER' THEN
SELECT PK_CODELST INTO V_PK_ORDER_TYE FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='HE';
  IF V_ORDER_TYPE=V_PK_ORDER_TYE THEN
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RHE'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  ELSE
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='HE'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  END IF;
END IF;
IF V_ORDERTYPE='ORORDER' THEN
  UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='OR'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
END IF;
END LOOP;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Data Not Found in sp_resolve_order');
END;
/






--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_01')
    AND COLUMN_NAME = 'ORDER_SAMPLE_AT_LAB';
  if (v_record_exists = 1) then
      UPDATE CB_ALERT_CONDITIONS SET CONDITION_VALUE='''N''' where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_01')
    AND COLUMN_NAME = 'ORDER_SAMPLE_AT_LAB';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_02')
    AND COLUMN_NAME = 'ORDER_SAMPLE_AT_LAB';
  if (v_record_exists = 1) then
      UPDATE CB_ALERT_CONDITIONS SET CONDITION_VALUE='''Y''' where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_02')
    AND COLUMN_NAME = 'ORDER_SAMPLE_AT_LAB';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_05')
    AND COLUMN_NAME = 'FINAL_REVIEW_TASK_FLAG' AND END_BRACE=')';
  if (v_record_exists = 1) then
      UPDATE CB_ALERT_CONDITIONS SET LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_05')
    AND COLUMN_NAME = 'FINAL_REVIEW_TASK_FLAG' AND END_BRACE=')';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_20')
    AND COLUMN_NAME = 'FK_SUBCATEGORY';
  if (v_record_exists = 1) then
      UPDATE CB_ALERT_CONDITIONS SET LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_20')
    AND COLUMN_NAME = 'FK_SUBCATEGORY';
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_05')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='ORDER_TYPE';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_05'),'PRE','ER_ORDER','ORDER_TYPE','(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_type'' and CODELST_SUBTYP=''OR'')','=','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--



--STARTS DELETING RECORDS FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists from CB_ALERT_CONDITIONS where FK_CODELST_ALERT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_09','alert_10','alert_12','alert_13','alert_14','alert_21','alert_22'));
  if (v_record_exists > 1) then
    DELETE FROM CB_ALERT_CONDITIONS where FK_CODELST_ALERT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_09','alert_10','alert_12','alert_13','alert_14','alert_21','alert_22'));
    commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORDS FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists from ER_CODELST where CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_10','alert_12','alert_13','alert_14','alert_21','alert_22');
  if (v_record_exists > 1) then
    UPDATE ER_CODELST SET CODELST_HIDE='Y' where CODELST_TYPE='alert' AND CODELST_SUBTYP IN('alert_10','alert_12','alert_13','alert_14','alert_21','alert_22');
    commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME ='FK_ORDER_RESOL_BY_TC' AND CONDITION_VALUE NOT IN('(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS)');
  if (v_record_exists > 1) then
      UPDATE CB_ALERT_CONDITIONS SET END_BRACE=')' where TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME ='FK_ORDER_RESOL_BY_TC' AND CONDITION_VALUE NOT IN('(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS)');
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME ='FK_ORDER_RESOL_BY_CBB' AND CONDITION_VALUE NOT IN('(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS)');
  if (v_record_exists > 1) then
      UPDATE CB_ALERT_CONDITIONS SET START_BRACE='(' where TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME ='FK_ORDER_RESOL_BY_CBB' AND CONDITION_VALUE NOT IN('(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS)');
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25')
	AND TABLE_NAME = 'CB_UPLOAD_INFO'
    AND COLUMN_NAME ='FK_SUBCATEGORY';
  if (v_record_exists = 1) then
      UPDATE CB_ALERT_CONDITIONS SET LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25')
	AND TABLE_NAME = 'CB_UPLOAD_INFO'
    AND COLUMN_NAME ='FK_SUBCATEGORY';
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='ORDER_TYPE';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25'),'PRE','ER_ORDER','ORDER_TYPE','(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_type'' and CODELST_SUBTYP=''OR'')','=','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS DELETING RECORDS FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists from CB_ALERT_CONDITIONS where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP ='alert_19');
  if (v_record_exists > 1) then
    DELETE FROM CB_ALERT_CONDITIONS where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP ='alert_19');
    commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='CB_CORD'
    AND COLUMN_NAME='FK_CORD_CBU_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION,LOGICAL_OPERATOR) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19'),'PRE','CB_CORD','FK_CORD_CBU_STATUS','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''IN'')','=','PK_ORDER = #keycolumn','AND');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='ORDER_TYPE';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19'),'PRE','ER_ORDER','ORDER_TYPE','(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_type'' AND CODELST_SUBTYP=''OR'')','=','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='RECENT_HLA_TYPING_AVAIL' AND CONDITION_VALUE='''Y''';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION,LOGICAL_OPERATOR) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09'),'PRE','ER_ORDER','RECENT_HLA_TYPING_AVAIL','''Y''','=','PK_ORDER = #keycolumn','AND');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='ADDITI_TYPING_FLAG' AND CONDITION_VALUE='''Y''';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION,LOGICAL_OPERATOR) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09'),'PRE','ER_ORDER','ADDITI_TYPING_FLAG','''Y''','=','PK_ORDER = #keycolumn','AND');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='RECENT_HLA_ENTERED_FLAG' AND CONDITION_VALUE=' IS NULL';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION,LOGICAL_OPERATOR,START_BRACE) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09'),'PRE','ER_ORDER','RECENT_HLA_ENTERED_FLAG',' IS NULL','','PK_ORDER = #keycolumn','OR','(');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='RECENT_HLA_ENTERED_FLAG' AND CONDITION_VALUE='''N''';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION,LOGICAL_OPERATOR,END_BRACE) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09'),'PRE','ER_ORDER','RECENT_HLA_ENTERED_FLAG','''N''','=','PK_ORDER = #keycolumn','AND',')');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='CB_SHIPMENT'
    AND COLUMN_NAME='SCH_SHIPMENT_DATE-SYSDATE' AND CONDITION_VALUE='-1';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09'),'PRE','CB_SHIPMENT','SCH_SHIPMENT_DATE-SYSDATE','1','>','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_11')
	AND TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME ='ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE CB_ALERT_CONDITIONS SET CONDITION_VALUE='(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_type'' AND CODELST_SUBTYP=''CT'')' where FK_CODELST_ALERT=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_11')
	AND TABLE_NAME = 'ER_ORDER'
    AND COLUMN_NAME ='ORDER_TYPE';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_11')
	AND TABLE_NAME = 'CB_SHIPMENT'
    AND COLUMN_NAME ='FK_SHIPMENT_TYPE';
  if (v_record_exists = 1) then
      UPDATE CB_ALERT_CONDITIONS SET CONDITION_VALUE='(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''shipment_type'' AND CODELST_SUBTYP=''CBU'')' where FK_CODELST_ALERT=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_11')
	AND TABLE_NAME = 'CB_SHIPMENT'
    AND COLUMN_NAME ='FK_SHIPMENT_TYPE';
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,175,3,'03_CB_ALERT_CONDITIONS_INSERT.sql',sysdate,'9.0.0 Build#632');

commit;


