SET DEFINE OFF;

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = '12' and 
  OBJECT_NAME = 'study_tab';
  if (row_count < 1) then 
  UPDATE ER_OBJECT_SETTINGS SET OBJECT_SEQUENCE = OBJECT_SEQUENCE + 1
  WHERE OBJECT_SEQUENCE >=6 and OBJECT_NAME = 'study_tab';
  
   Insert into ER_OBJECT_SETTINGS
    (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
     OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
   Values
    (SEQ_ER_OBJECT_SETTINGS.nextval, 'T', '12', 'study_tab', 6, 0, 'Milestones', 0);
  end if;
END;
/

COMMIT;


SET DEFINE OFF;

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_CODELST where codelst_type = 'skin' and 
  codelst_subtyp = 'vel_defaultCSS3';
  if (row_count < 1) then 
  Insert into ER_CODELST
   (pk_codelst, fk_account, codelst_type, codelst_subtyp, codelst_desc, 
    codelst_hide, codelst_custom_col)
 Values
   (seq_ER_CODELST.nextval, NULL, 'skin', 'vel_defaultCSS3', 'Round Edges', 
    'N' , 'defaultCSS3');
commit;
  end if;
END;
/

COMMIT;



set define off;

DECLARE
 col_count NUMBER;
BEGIN
 select count(*) into col_count from ER_OBJECT_SETTINGS where object_name='protocol_tab' and OBJECT_DISPLAYTEXT='Calendar Milestone Setup';
 if col_count = 0 then
   insert into ER_OBJECT_SETTINGS values(SEQ_ER_OBJECT_SETTINGS.nextval, 'T','9','protocol_tab',9,1,'Calendar Milestone Setup',0);
    commit;
 end if;
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,112,1,'01_data.sql',sysdate,'8.10.0 Build#569');

commit;
