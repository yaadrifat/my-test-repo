
DELETE FROM CBB_PROCESSING_PROCEDURES_INFO;
DELETE FROM CBB_PROCESSING_PROCEDURES;
delete from ER_CODELST where CODELST_TYPE='ineligible';

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,145,4,'04_DELETE_SCRIPT.sql',sysdate,'9.0.0 Build#602');

commit;