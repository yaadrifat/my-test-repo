update ER_CODELST set CODELST_DESC='Not Applicable Prior To 05/25/2005' where CODELST_TYPE='eligibility' and CODELST_SUBTYP='not_appli_prior';

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,145,7,'07_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#602');

commit;