--STARTS DROP THE COLUMN REVIEW_CONFIRM_FLAG FROM CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD'
    AND COLUMN_NAME = 'REVIEW_CONFIRM_FLAG'; 

  if (v_column_exists != 0) then
      execute immediate 'alter table CB_CORD DROP COLUMN REVIEW_CONFIRM_FLAG';
  end if;
end;
/
--END--



--STARTS DROP THE COLUMN REVIEW_ACCEPTABLE_FLAG FROM CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD'
    AND COLUMN_NAME = 'REVIEW_ACCEPTABLE_FLAG'; 

  if (v_column_exists != 0) then
      execute immediate 'alter table CB_CORD DROP COLUMN REVIEW_ACCEPTABLE_FLAG';
  end if;
end;
/
--END--



--STARTS ADD THE COLUMN FINAL_REVIEW_CONFIRM_FLAG FROM CB_CORD_FINAL_REVIEW TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'FINAL_REVIEW_CONFIRM_FLAG'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (FINAL_REVIEW_CONFIRM_FLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--



--STARTS ADD THE COLUMN REVIEW_CORD_ACCEPTABLE_FLAG FROM CB_CORD_FINAL_REVIEW TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'REVIEW_CORD_ACCEPTABLE_FLAG'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (REVIEW_CORD_ACCEPTABLE_FLAG NUMBER(10,2))';
  end if;
end;
/
--END--



--STARTS ADD THE COLUMN FK_ORDER_ID FROM CB_CORD_FINAL_REVIEW TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'FK_ORDER_ID'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (FK_ORDER_ID NUMBER(10,0))';
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,145,10,'10_CB_CORDFINALREVIEW.sql',sysdate,'9.0.0 Build#602');

commit;
