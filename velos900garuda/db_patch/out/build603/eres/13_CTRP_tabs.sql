set define off;

DECLARE
 col_count NUMBER;
BEGIN
 select count(*) into col_count from ER_OBJECT_SETTINGS where object_name='ctrp_tab' and OBJECT_DISPLAYTEXT='Drafts';
 if col_count = 0 then
   insert into ER_OBJECT_SETTINGS values(SEQ_ER_OBJECT_SETTINGS.nextval, 'T','ctrp_draft_tab','ctrp_tab',1,1,'Drafts',0);
    commit;
	dbms_output.put_line('Drafts tab created');
 else
    dbms_output.put_line('Object setting Drafts tab already exists');
 end if;
 
 select count(*) into col_count from ER_OBJECT_SETTINGS where object_name='ctrp_tab' and OBJECT_DISPLAYTEXT='Submissions';
 if col_count = 0 then
   insert into ER_OBJECT_SETTINGS values(SEQ_ER_OBJECT_SETTINGS.nextval, 'T','ctrp_subm_tab','ctrp_tab',2,1,'Submissions',0);
    commit;
	dbms_output.put_line('Submissions tab created');
 else
    dbms_output.put_line('Object setting Submissions tab already exists');
 end if;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,146,13,'13_CTRP_tabs.sql',sysdate,'9.0.0 Build#603');

commit;