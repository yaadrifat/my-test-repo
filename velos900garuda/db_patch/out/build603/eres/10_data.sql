set define off;

delete from er_repxsl where pk_repxsl in (1,7,82,86,88,121,128,130,131,132);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,146,10,'10_data.sql',sysdate,'9.0.0 Build#603');

commit;