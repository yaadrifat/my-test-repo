SET DEFINE OFF;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
  OBJECT_SUBTYPE = 'regreporting_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'regreporting_menu', 'top_menu', 4, 
      0, 'Regulatory Reporting', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cbu_menu for top_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'ctrp_reporting' and OBJECT_NAME = 'regreporting_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'ctrp_reporting', 'regreporting_menu', 1, 
     0, 'CTRP', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting ctrp_reporting for regreporting_menu already exists');
  end if;
END;
/

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,146,12,'12_CTRP_menu.sql',sysdate,'9.0.0 Build#603');

commit;