SET DEFINE OFF;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CBB WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CBB  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,13,'13_CBB_RID.sql',sysdate,'9.0.0 Build#617');

commit;