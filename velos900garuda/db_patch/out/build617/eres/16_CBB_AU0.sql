create or replace TRIGGER "ERES".CBB_AU0 AFTER  UPDATE OF PK_CBB,
    CBB_ID,
    CREATOR,
    CREATED_ON,
    LAST_MODIFIED_BY,
    LAST_MODIFIED_DATE,
    IP_ADD,
    DELETEDFLAG,
    RID,
    FK_PERSON,
    FK_ADD,
    FK_SITE,
    ENABLE_EDIT_ANTIGENS,
    DISPLAY_MATERNAL_ANTIGENS,
    ENABLE_CBU_INVENT_FEATURE,
    DEFAULT_CT_SHIPPER,
    DEFAULT_OR_SHIPPER,
    MICRO_CONTAM_DATE,
    CFU_TEST_DATE,
    VIAB_TEST_DATE,
    ENABLE_CORD_INFO_LOCK,
    FK_DATE_FORMAT,
    MEMBER,
    USING_CDR,
    USE_OWN_DUMN,
    DOMESTIC_NMDP,
    USE_FDOE,
	LOC_CBU_ID_PRE,
	LOC_MAT_ID_PRE,
	LOC_CBU_ID_STR_NUM,
	FK_LOC_CBU_ASSGN_ID,
	FK_LOC_MAT_ASSGN_ID,
	LOC_MAT_ID_STR_NUM,
	COMP_CORD_ENTRY,
	COMP_ACK_RES_TASK,
	FK_SHP_PWRK_LOC,
	FK_CBU_PCK_ADD,
	ATTENTION,
	FK_TIME_FORMAT,
	FK_ACCREDITATION	ON  CBB FOR EACH ROW DECLARE raid NUMBER(10);
  usr                                         VARCHAR2(100);
  old_modby                                   VARCHAR2(100);
  new_modby                                   VARCHAR2(100);
  BEGIN
    SELECT seq_audit.nextval INTO raid FROM dual;
    usr := getuser(:new.last_modified_by);
    audit_trail.record_transaction (raid, 'CBB', :old.rid, 'U', usr);
    IF NVL(:old.PK_CBB,0) != NVL(:new.PK_CBB,0) THEN
      audit_trail.column_update (raid, 'PK_CBB', :old.PK_CBB, :new.PK_CBB);
    END IF;
    IF NVL(:old.CBB_ID,' ') != NVL(:new.CBB_ID,' ') THEN
      audit_trail.column_update (raid, 'CBB_ID', :OLD.CBB_ID, :NEW.CBB_ID);
    END IF;
    IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      audit_trail.column_update (raid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR);
    END IF;
    IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO old_modby
        FROM er_user
        WHERE pk_user = :old.last_modified_by ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        old_modby := NULL;
      END ;
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO new_modby
        FROM er_user
        WHERE pk_user = :new.LAST_MODIFIED_BY ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        new_modby := NULL;
      END ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    END IF;
    IF NVL(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
    IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      audit_trail.column_update (raid, 'IP_ADD', :old.ip_add, :new.ip_add);
    END IF;
    IF NVL(:old.DELETEDFLAG,' ') != NVL(:new.DELETEDFLAG,' ') THEN
      audit_trail.column_update (raid, 'DELETEDFLAG', :old.DELETEDFLAG, :new.DELETEDFLAG);
    END IF;
    IF NVL(:old.rid,0) != NVL(:new.rid,0) THEN
      audit_trail.column_update (raid, 'RID', :OLD.rid, :NEW.rid);
    END IF;
    IF NVL(:old.FK_PERSON,0) != NVL(:new.FK_PERSON,0) THEN
      audit_trail.column_update (raid, 'FK_PERSON', :OLD.FK_PERSON, :NEW.FK_PERSON);
    END IF;
    IF NVL(:old.FK_ADD,0) != NVL(:new.FK_ADD,0) THEN
      audit_trail.column_update (raid, 'FK_ADD', :OLD.FK_ADD, :NEW.FK_ADD);
    END IF;
    IF NVL(:old.FK_SITE,0) != NVL(:new.FK_SITE,0) THEN
     audit_trail.column_update (raid, 'FK_SITE', :OLD.FK_SITE, :NEW.FK_SITE);
    END IF;
    IF NVL(:old.ENABLE_EDIT_ANTIGENS,' ') != NVL(:new.ENABLE_EDIT_ANTIGENS,' ') THEN
      audit_trail.column_update (raid, 'ENABLE_EDIT_ANTIGENS', :OLD.ENABLE_EDIT_ANTIGENS, :NEW.ENABLE_EDIT_ANTIGENS);
    END IF;
    IF NVL(:old.DISPLAY_MATERNAL_ANTIGENS,' ') != NVL(:new.DISPLAY_MATERNAL_ANTIGENS,' ') THEN
      audit_trail.column_update (raid, 'DISPLAY_MATERNAL_ANTIGENS', :OLD.DISPLAY_MATERNAL_ANTIGENS, :NEW.DISPLAY_MATERNAL_ANTIGENS);
    END IF;
    IF NVL(:old.ENABLE_CBU_INVENT_FEATURE,' ') != NVL(:new.ENABLE_CBU_INVENT_FEATURE,' ') THEN
      audit_trail.column_update (raid, 'ENABLE_CBU_INVENT_FEATURE', :OLD.ENABLE_CBU_INVENT_FEATURE, :NEW.ENABLE_CBU_INVENT_FEATURE);
    END IF;
    IF NVL(:old.DEFAULT_CT_SHIPPER,' ') != NVL(:new.DEFAULT_CT_SHIPPER,' ') THEN
      audit_trail.column_update (raid, 'DEFAULT_CT_SHIPPER', :OLD.DEFAULT_CT_SHIPPER, :NEW.DEFAULT_CT_SHIPPER);
    END IF;
    IF NVL(:old.DEFAULT_OR_SHIPPER,' ') != NVL(:new.DEFAULT_OR_SHIPPER,' ') THEN
      audit_trail.column_update (raid, 'DEFAULT_OR_SHIPPER', :OLD.DEFAULT_OR_SHIPPER, :NEW.DEFAULT_OR_SHIPPER);
    END IF;
    IF NVL(:old.MICRO_CONTAM_DATE,' ') != NVL(:new.MICRO_CONTAM_DATE,' ') THEN
      audit_trail.column_update (raid, 'MICRO_CONTAM_DATE', :OLD.MICRO_CONTAM_DATE, :NEW.MICRO_CONTAM_DATE);
    END IF;
    IF NVL(:old.CFU_TEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CFU_TEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'CFU_TEST_DATE', TO_CHAR(:OLD.CFU_TEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CFU_TEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;   
    IF NVL(:old.VIAB_TEST_DATE,' ') != NVL(:new.VIAB_TEST_DATE,' ') THEN
      audit_trail.column_update (raid, 'VIAB_TEST_DATE', :OLD.VIAB_TEST_DATE, :NEW.VIAB_TEST_DATE);
    END IF;
    IF NVL(:old.ENABLE_CORD_INFO_LOCK,' ') != NVL(:new.ENABLE_CORD_INFO_LOCK,' ') THEN
      audit_trail.column_update (raid, 'ENABLE_CORD_INFO_LOCK', :OLD.ENABLE_CORD_INFO_LOCK, :NEW.ENABLE_CORD_INFO_LOCK);
    END IF;
    IF NVL(:old.FK_DATE_FORMAT,0) != NVL(:new.FK_DATE_FORMAT,0) THEN
      audit_trail.column_update (raid, 'FK_DATE_FORMAT', :OLD.FK_DATE_FORMAT, :NEW.FK_DATE_FORMAT);
    END IF;
    IF NVL(:old.MEMBER,0) != NVL(:new.MEMBER,0) THEN
      audit_trail.column_update (raid, 'MEMBER', :OLD.MEMBER, :NEW.MEMBER);
    END IF;
    IF NVL(:old.USING_CDR,' ') != NVL(:new.USING_CDR,' ') THEN
      audit_trail.column_update (raid, 'USING_CDR', :OLD.USING_CDR, :NEW.USING_CDR);
    END IF;
    IF NVL(:old.USE_OWN_DUMN,' ') != NVL(:new.USE_OWN_DUMN,' ') THEN
      audit_trail.column_update (raid, 'USE_OWN_DUMN', :OLD.USE_OWN_DUMN, :NEW.USE_OWN_DUMN);
    END IF;
    IF NVL(:old.DOMESTIC_NMDP,' ') != NVL(:new.DOMESTIC_NMDP,' ') THEN
      audit_trail.column_update (raid, 'DOMESTIC_NMDP', :OLD.DOMESTIC_NMDP, :NEW.DOMESTIC_NMDP);
    END IF;
    IF NVL(:old.USE_FDOE,' ') != NVL(:new.USE_FDOE,' ') THEN
      audit_trail.column_update (raid, 'USE_FDOE', :OLD.USE_FDOE, :NEW.USE_FDOE);
    END IF;
	IF NVL(:old.LOC_CBU_ID_PRE,' ') != NVL(:new.LOC_CBU_ID_PRE,' ') THEN
      audit_trail.column_update (raid, 'LOC_CBU_ID_PRE', :OLD.LOC_CBU_ID_PRE, :NEW.LOC_CBU_ID_PRE);
    END IF;
	IF NVL(:old.LOC_MAT_ID_PRE,' ') != NVL(:new.LOC_MAT_ID_PRE,' ') THEN
      audit_trail.column_update (raid, 'LOC_MAT_ID_PRE', :OLD.LOC_MAT_ID_PRE, :NEW.LOC_MAT_ID_PRE);
    END IF;
	IF NVL(:old.LOC_CBU_ID_STR_NUM,0) != NVL(:new.LOC_CBU_ID_STR_NUM,0) THEN
      audit_trail.column_update (raid, 'LOC_CBU_ID_STR_NUM', :OLD.LOC_CBU_ID_STR_NUM, :NEW.LOC_CBU_ID_STR_NUM);
    END IF;
	IF NVL(:old.FK_LOC_CBU_ASSGN_ID,0) != NVL(:new.FK_LOC_CBU_ASSGN_ID,0) THEN
      audit_trail.column_update (raid, 'FK_LOC_CBU_ASSGN_ID', :OLD.FK_LOC_CBU_ASSGN_ID, :NEW.FK_LOC_CBU_ASSGN_ID);
    END IF;
	IF NVL(:old.FK_LOC_MAT_ASSGN_ID,0) != NVL(:new.FK_LOC_MAT_ASSGN_ID,0) THEN
      audit_trail.column_update (raid, 'FK_LOC_MAT_ASSGN_ID', :OLD.FK_LOC_MAT_ASSGN_ID, :NEW.FK_LOC_MAT_ASSGN_ID);
    END IF;
	IF NVL(:old.LOC_MAT_ID_STR_NUM,0) != NVL(:new.LOC_MAT_ID_STR_NUM,0) THEN
      audit_trail.column_update (raid, 'LOC_MAT_ID_STR_NUM', :OLD.LOC_MAT_ID_STR_NUM, :NEW.LOC_MAT_ID_STR_NUM);
    END IF;
	IF NVL(:old.COMP_CORD_ENTRY,' ') != NVL(:new.COMP_CORD_ENTRY,' ') THEN
      audit_trail.column_update (raid, 'COMP_CORD_ENTRY', :OLD.COMP_CORD_ENTRY, :NEW.COMP_CORD_ENTRY);
    END IF;
	IF NVL(:old.COMP_ACK_RES_TASK,' ') != NVL(:new.COMP_ACK_RES_TASK,' ') THEN
      audit_trail.column_update (raid, 'COMP_ACK_RES_TASK', :OLD.COMP_ACK_RES_TASK, :NEW.COMP_ACK_RES_TASK);
    END IF;
	IF NVL(:old.FK_SHP_PWRK_LOC,0) != NVL(:new.FK_SHP_PWRK_LOC,0) THEN
      audit_trail.column_update (raid, 'FK_SHP_PWRK_LOC', :OLD.FK_SHP_PWRK_LOC, :NEW.FK_SHP_PWRK_LOC);
    END IF;
	IF NVL(:old.FK_CBU_PCK_ADD,0) != NVL(:new.FK_CBU_PCK_ADD,0) THEN
      audit_trail.column_update (raid, 'FK_CBU_PCK_ADD', :OLD.FK_CBU_PCK_ADD, :NEW.FK_CBU_PCK_ADD);
    END IF;
	IF NVL(:old.ATTENTION,' ') != NVL(:new.ATTENTION,' ') THEN
      audit_trail.column_update (raid, 'ATTENTION', :OLD.ATTENTION, :NEW.ATTENTION);
    END IF;
	IF NVL(:old.FK_TIME_FORMAT,0) != NVL(:new.FK_TIME_FORMAT,0) THEN
      audit_trail.column_update (raid, 'FK_TIME_FORMAT', :OLD.FK_TIME_FORMAT, :NEW.FK_TIME_FORMAT);
    END IF;
	IF NVL(:old.FK_ACCREDITATION,0) != NVL(:new.FK_ACCREDITATION,0) THEN
      audit_trail.column_update (raid, 'FK_ACCREDITATION', :OLD.FK_ACCREDITATION, :NEW.FK_ACCREDITATION);
    END IF;
  END;
  /
  INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,16,'16_CBB_AU0.sql',sysdate,'9.0.0 Build#617');

commit;