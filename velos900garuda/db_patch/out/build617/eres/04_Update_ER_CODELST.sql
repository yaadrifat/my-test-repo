set define off;
begin
  update ER_CODELST set CODELST_CUSTOM_COL1='../images/jpg/room.jpg' where CODELST_TYPE='store_type' and CODELST_SUBTYP='Room';
  update ER_CODELST set CODELST_CUSTOM_COL1='../images/jpg/building.jpg' where CODELST_TYPE='store_type' and CODELST_SUBTYP='Building';
  commit;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,4,'04_Update_ER_CODELST.sql',sysdate,'9.0.0 Build#617');

commit;