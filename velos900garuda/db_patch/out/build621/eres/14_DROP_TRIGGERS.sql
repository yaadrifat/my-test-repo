SET define OFF;
DECLARE 
valueCount NUMBER(5);
valueCount_au0 NUMBER(5);
valueCount_bi0 NUMBER(5);
valueCount_ai1 NUMBER(5);
BEGIN
    SELECT COUNT(*) into VALUECOUNT FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'CB_CORD_AU3';
    IF (VALUECOUNT = 1) THEN
        EXECUTE IMMEDIATE 'drop TRIGGER CB_CORD_AU3';
        COMMIT;
    END IF;    
    SELECT COUNT(*) into valueCount_au0 FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'ER_CB_CORD_AU0';
    IF (valueCount_au0 = 1) THEN
        EXECUTE IMMEDIATE 'drop TRIGGER ER_CB_CORD_AU0';
        COMMIT;
    END IF;    
    SELECT COUNT(*) into valueCount_bi0 FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'ER_CB_CORD_BI0';
    IF (valueCount_bi0 = 1) THEN
        EXECUTE IMMEDIATE 'drop TRIGGER ER_CB_CORD_BI0';
        COMMIT;
    END IF;    
    SELECT COUNT(*) INTO valueCount_ai1 FROM DBA_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME = 'CB_CORD_AI1';
    IF (valueCount_ai1 = 1) THEN
        EXECUTE IMMEDIATE 'drop TRIGGER CB_CORD_AI1';
        COMMIT;
    END IF;    
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,14,'14_DROP_TRIGGERS.sql',sysdate,'9.0.0 Build#621');

commit;
