set define off;

update ER_BROWSER 
set browser_module = 'ctrpDraftBrowser'
where browser_module = 'studydraftBrowser';
  
commit;

DECLARE
  v_item_exists number := 0;  
  browser_Pk number := 0;  
BEGIN
  select pk_browser into browser_Pk from ER_BROWSER where browser_module = 'ctrpDraftBrowser'; 
  select count(*) into v_item_exists from ER_BROWSERCONF where fk_browser = browser_Pk and BROWSERCONF_COLNAME ='PK_CTRP_DRAFT';
  if (v_item_exists = 0) then
     INSERT INTO ER_BROWSERCONF 
     VALUES (SEQ_ER_BROWSERCONF.nextval, browser_Pk,'PK_CTRP_DRAFT',11,null,null);
  end if;
COMMIT;

end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,148,27,'27_CTRP_BugFix#7883.sql',sysdate,'9.0.0 Build#605');

commit;