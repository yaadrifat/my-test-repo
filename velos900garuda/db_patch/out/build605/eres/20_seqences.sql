set define off;

DECLARE
  v_seq_exists number := 0;  
BEGIN
	select count(*) into v_seq_exists from ALL_SEQUENCES where SEQUENCE_NAME = 'SEQ_ER_CTRP_DRAFT';
	if (v_seq_exists = 0) then
		execute immediate 'CREATE SEQUENCE ERES.SEQ_ER_CTRP_DRAFT
		  START WITH 1
		  MAXVALUE 999999999999999999999999999
		  MINVALUE 1
		  NOCYCLE
		  CACHE 20
		  ORDER';
	else
		dbms_output.put_line('Sequence SEQ_ER_CTRP_DRAFT already exists');
	end if;
	
	select count(*) into v_seq_exists from ALL_SEQUENCES where SEQUENCE_NAME = 'SEQ_ER_STUDY_INDIDE';
	if (v_seq_exists = 0) then
		execute immediate 'CREATE SEQUENCE ERES.SEQ_ER_STUDY_INDIDE
		  START WITH 1
		  MAXVALUE 999999999999999999999999999
		  MINVALUE 1
		  NOCYCLE
		  CACHE 20
		  ORDER';
	else
		dbms_output.put_line('Sequence SEQ_ER_STUDY_INDIDE already exists');
	end if;
	
	select count(*) into v_seq_exists from ALL_SEQUENCES where SEQUENCE_NAME = 'SEQ_ER_STUDY_NIH_GRANT';
	if (v_seq_exists = 0) then
		execute immediate 'CREATE SEQUENCE ERES.SEQ_ER_STUDY_NIH_GRANT
		  START WITH 1
		  MAXVALUE 999999999999999999999999999
		  MINVALUE 1
		  NOCYCLE
		  CACHE 20
		  ORDER';
	else
		dbms_output.put_line('Sequence SEQ_ER_STUDY_NIH_GRANT already exists');
	end if;
	
	select count(*) into v_seq_exists from ALL_SEQUENCES where SEQUENCE_NAME = 'SEQ_ER_CTRP_DOCUMENTS';
	if (v_seq_exists = 0) then
		execute immediate 'CREATE SEQUENCE ERES.SEQ_ER_CTRP_DOCUMENTS
		  START WITH 1
		  MAXVALUE 999999999999999999999999999
		  MINVALUE 1
		  NOCYCLE
		  CACHE 20
		  ORDER';
	else
		dbms_output.put_line('Sequence SEQ_ER_CTRP_DOCUMENTS already exists');
	end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,148,20,'20_seqences.sql',sysdate,'9.0.0 Build#605');

commit;