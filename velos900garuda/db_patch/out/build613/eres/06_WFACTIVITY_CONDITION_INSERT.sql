set define off;

--STARTS UPDATING RECORD FROM WFACTIVITY_CONDITION TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from WFACTIVITY_CONDITION
    where EA_TABLENAME = 'CB_CORD' AND EA_COLUMNNAME='CBU_AVAIL_CONFIRM_FLAG';
  if (v_record_exists > 1) then
      UPDATE WFACTIVITY_CONDITION SET EA_TABLENAME = 'ER_ORDER',EA_KEYCONDITION='PK_ORDER = #keycolumn' WHERE EA_TABLENAME = 'CB_CORD' AND EA_COLUMNNAME='CBU_AVAIL_CONFIRM_FLAG';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM WFACTIVITY_CONDITION TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from WFACTIVITY_CONDITION
    where EA_KEYCONDITION='FK_ORDER_ID = #keycolumn';
  if (v_record_exists >= 1) then
      UPDATE WFACTIVITY_CONDITION SET EA_KEYCONDITION='PK_ORDER = #keycolumn' WHERE EA_KEYCONDITION='FK_ORDER_ID = #keycolumn';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM WFACTIVITY_CONDITION TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from WFACTIVITY_CONDITION
    where EA_TABLENAME = 'CB_SHIPMENT' AND EA_COLUMNNAME='SHIPMENT_SCH_FLAG';
  if (v_record_exists > 1) then
      UPDATE WFACTIVITY_CONDITION SET EA_TABLENAME = 'ER_ORDER' WHERE EA_TABLENAME = 'CB_SHIPMENT' AND EA_COLUMNNAME='SHIPMENT_SCH_FLAG';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM WFACTIVITY_CONDITION TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from WFACTIVITY_CONDITION
    where EA_TABLENAME = 'CB_SHIPMENT' AND EA_COLUMNNAME='PACKAGE_SLIP_FLAG';
  if (v_record_exists > 1) then
      UPDATE WFACTIVITY_CONDITION SET EA_TABLENAME = 'ER_ORDER' WHERE EA_TABLENAME = 'CB_SHIPMENT' AND EA_COLUMNNAME='PACKAGE_SLIP_FLAG';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM WFACTIVITY_CONDITION TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from WFACTIVITY_CONDITION
    where EA_TABLENAME = 'ER_ORDER' AND EA_COLUMNNAME='NMDP_SAMPLE_SHIPPED_FLAG';
  if (v_record_exists = 1) then
      UPDATE WFACTIVITY_CONDITION SET EA_VALUE = 'select ''''''Y'''''' from dual' WHERE EA_TABLENAME = 'ER_ORDER' AND EA_COLUMNNAME='NMDP_SAMPLE_SHIPPED_FLAG';
	commit;
  end if;
end;
/
--END--
commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,156,6,'06_WFACTIVITY_CONDITION_INSERT.sql',sysdate,'9.0.0 Build#613');

commit;