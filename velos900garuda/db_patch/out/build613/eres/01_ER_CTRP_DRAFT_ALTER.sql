set define off;
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_CTRP_DRAFT' AND column_name = 'LEAD_ORG_NAME'; 
  if (v_column_exists > 0) then
      execute immediate ('ALTER TABLE ER_CTRP_DRAFT DROP COLUMN LEAD_ORG_NAME');
  end if;
end;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,156,1,'01_ER_CTRP_DRAFT_ALTER.sql',sysdate,'9.0.0 Build#613');

commit;