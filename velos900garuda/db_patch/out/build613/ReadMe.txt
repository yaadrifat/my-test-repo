/* This readMe is specific to Velos eResearch version 9.0 build #613 */
=====================================================================================================================================
Garuda :
1. The "08_ER_OBJECT_SETTING_INSERT.sql"  patch released with this build is specific to NMDP.
This patch consist one new menu items:-
a) FAQ

2. HLA Antigen data has been released in this build. This data is specific to NMDP deployment.
The data and readme file is uploaded over FTP server 66.237.42.81 in the directory -  /Garuda/HLA-Data/Jan-20 

3.We have released two reports (xsl fo Based)under folder xsl-fo with this build. This is NMDP Specific Report.
   161.xsl and 162.xsl and its related script (02_ER_REPORT.sql) are Specific to NMDP.

=====================================================================================================================================
eResearch:

1.In this build we have released CTRP-20576-8 & CTRP-21401-10.These two enhancements are not for QA release, 
It is for internal review purpose only.

2.Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1   ereslogin.jsp
2   login.css
3	MC.java
4	MC.jsp
5	LC.java
6	LC.jsp
7	messageBundle.properties
8	labelBundle.properties

Login screens (ereslogin.jsp and patientlogin.jsp) have been modified
=====================================================================================================================================
