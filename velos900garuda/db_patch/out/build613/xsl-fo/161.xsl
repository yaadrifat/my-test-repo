<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
            xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="regId" match="ROW" use="concat(CORD_REGISTRY_ID,' ')"/>
  <xsl:key name="isbiDinCode" match="ROW" use="concat(CORD_ISBI_DIN_CODE,' ')"/>
  <xsl:key name="isitProdCode" match="ROW" use="concat(CORD_ISIT_PRODUCT_CODE,' ')"/>
  <xsl:key name="tncFrozen" match="ROW" use="concat(CORD_TNC_FROZEN,' ')"/>
  <xsl:key name="licenceStatus" match="ROW" use="concat(LICENCE_STATUS,' ')"/>
  <xsl:key name="eligibleStatus" match="ROW" use="concat(ELIGIBLE_STATUS,' ')"/>
  <xsl:key name="externalCbuId" match="ROW" use="concat(CORD_EXTERNAL_CBU_ID,' ')"/>
  <xsl:key name="localCbuId" match="ROW" use="concat(CORD_LOCAL_CBU_ID,' ')"/>

	  
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>

   <xsl:template match="/">
  <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
      <fo:simple-page-master master-name="pdf" page-height="11in" page-width="10in" 
          margin-top="0.5in" margin-bottom="0.5in" margin-left="0.5in" margin-right="0.5in">
	<fo:region-body margin-top="1.0in" margin-bottom="1.0in"/>
	<fo:region-before extent="1.0in"/>
        <fo:region-after extent="0.5in"/>
      </fo:simple-page-master>
    </fo:layout-master-set>
      <xsl:apply-templates select="ROWSET"/>
  </fo:root>
  </xsl:template>
  
  
  <xsl:template match="ROWSET">

  <fo:page-sequence master-reference="pdf" initial-page-number="1" font-family="serif">
  <fo:static-content flow-name="xsl-region-before">
  <fo:block font-size="10pt" font-weight="bold"> NMDP Proprietory and Confidential</fo:block>
  </fo:static-content>
   <fo:static-content flow-name="xsl-region-after">
   <fo:table>
   <fo:table-body>
   <fo:table-row>
	<fo:table-cell>  <fo:block text-align="left" font-size="10pt" font-weight="bold"> NMDP Proprietory and Confidential</fo:block>	</fo:table-cell>
	<fo:table-cell> <fo:block  text-align="left" font-size="10pt" font-weight="bold">Page <fo:page-number/> of
   <fo:page-number-citation ref-id="theEnd"/></fo:block>	</fo:table-cell>
   </fo:table-row>
   </fo:table-body>
   </fo:table>
   </fo:static-content>


   <fo:flow flow-name="xsl-region-body">
	<fo:block font-family="Helvetica" text-align="center" font-size="11pt" font-weight="bold"  space-after.optimum="12pt">
	CBU Summary Report
	</fo:block>

	<fo:table width="650pt" table-layout="fixed"  border-width="0.4mm" border-style="solid">
		<fo:table-column column-width="80pt" column-number="1"/>
		<fo:table-column column-width="100pt" column-number="2"/>
		<fo:table-column column-width="80pt" column-number="3"/>
		<fo:table-column column-width="50pt" column-number="4"/>
		<fo:table-column column-width="100pt" column-number="5"/>
		<fo:table-column column-width="80pt" column-number="6"/>
		<fo:table-column column-width="80pt" column-number="7"/>
		<fo:table-column column-width="80pt" column-number="8"/>

	 <fo:table-header>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">Registry Id</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">ISBT DIN</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">ISBT Product Code</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">TNC Frozen</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">Licensure Status</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">Eligible Status</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">External CBU ID</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">CBU Local ID</fo:block>
	  </fo:table-cell>
	</fo:table-header> 
	 
	 
	 <fo:table-body start-indent="0pt" text-align="start"> 
	
	
	<xsl:for-each select="ROW[count(. | key('regId', concat(CORD_REGISTRY_ID,' '))[1])=1]">
	<fo:table-row>
	
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	<fo:block font-size="10pt" text-align="center">
	 <xsl:variable name="reg_id" select="CORD_REGISTRY_ID" />
         <xsl:value-of select="$reg_id"/>
        </fo:block>
        </fo:table-cell>

	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	      <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="isbi_din_code" select="CORD_ISBI_DIN_CODE" />
                   <xsl:value-of select="$isbi_din_code"/>
		 </fo:block>
        </fo:table-cell> 	

	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="isit_prod_code" select="CORD_ISIT_PRODUCT_CODE" />
                   <xsl:value-of select="$isit_prod_code"/>
	       </fo:block>
         </fo:table-cell>
	
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="tnc_frozen" select="CORD_TNC_FROZEN" />
                   <xsl:value-of select="$tnc_frozen"/>
               </fo:block>
         </fo:table-cell>	
	
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="licence_status" select="LICENCE_STATUS" />
                   <xsl:value-of select="$licence_status"/>
              </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="eligible_status" select="ELIGIBLE_STATUS" />
                   <xsl:value-of select="$eligible_status"/>
               </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="external_cbu_id" select="CORD_EXTERNAL_CBU_ID" />
                   <xsl:value-of select="$external_cbu_id"/>
              </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="local_cbu_id" select="CORD_LOCAL_CBU_ID" />
                   <xsl:value-of select="$local_cbu_id"/>
               </fo:block>
            </fo:table-cell> 

        </fo:table-row>
	</xsl:for-each>

	 </fo:table-body>
        </fo:table>

	<fo:block id="theEnd"/>	

   </fo:flow>
 </fo:page-sequence>  
</xsl:template>
</xsl:stylesheet>
	  
	 