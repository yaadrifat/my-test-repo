set define off;

CREATE OR REPLACE TRIGGER ER_SITE_BI0 BEFORE INSERT ON ER_SITE
FOR EACH ROW
WHEN (NEW.rid IS NULL OR NEW.rid = 0)
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
   BEGIN
  usr := getuser(:NEW.creator);
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_SITE',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:=:NEW.SITE_ID||'|'|| :NEW.SITE_SEQ||'|'||
     :NEW.PK_SITE||'|'||:NEW.FK_CODELST_TYPE||'|'|| :NEW.FK_ACCOUNT||'|'|| :NEW.FK_PERADD||'|'||
  :NEW.SITE_NAME||'|'||:NEW.SITE_INFO||'|'||:NEW.SITE_PARENT||'|'||:NEW.SITE_STAT||'|'||
 :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
 TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||:NEW.SITE_NOTES||'|'||:NEW.SITE_HIDDEN||'|'|| --KM
  :NEW.GUID; -- 22-Dec-2011, New column GUID added, Ankit
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
 END ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,151,4,'04_ER_SITE_BI0.sql',sysdate,'9.0.0 Build#608');

commit;
