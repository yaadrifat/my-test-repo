CREATE OR REPLACE TRIGGER ERES.CBB_PROC_PROC_INFO_BI0 BEFORE INSERT ON CBB_PROCESSING_PROCEDURES_INFO

REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW

DECLARE
  ERID NUMBER(10);
  USR VARCHAR(200); 
  RAID NUMBER(10);
  INSERT_DATA CLOB;
  
BEGIN
 SELECT TRUNC(SEQ_RID.NEXTVAL)  INTO ERID FROM DUAL;
  :NEW.RID :=ERID;
  USR := getuser(:NEW.LAST_MODIFIED_BY);
  
  
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;
  
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CBB_PROCESSING_PROCEDURES_INFO', ERID, 'I',USR);
  
  
  INSERT_DATA := :NEW.PK_PROC_INFO || '|' ||  
     TO_CHAR(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.ACD) || '|' ||
to_char(:NEW.CPD) || '|' ||
to_char(:NEW.CPDA) || '|' ||
to_char(:NEW.CRYOBAG_MANUFAC) || '|' ||
to_char(:NEW.FILTER_PAPER) || '|' ||
to_char(:NEW.FIVE_PER_DEXTROSE) || '|' ||
to_char(:NEW.FIVE_PER_HUMAN_ALBU) || '|' ||
to_char(:NEW.FIVE_UNIT_PER_ML_HEPARIN) || '|' ||
to_char(:NEW.FK_BAG_TYPE) || '|' ||
to_char(:NEW.FK_CONTRL_RATE_FREEZING) || '|' ||
to_char(:NEW.FK_FREEZ_MANUFAC) || '|' ||
to_char(:NEW.FK_FROZEN_IN) || '|' ||
to_char(:NEW.FK_IF_AUTOMATED) || '|' ||
to_char(:NEW.FK_PROC_METH_ID) || '|' ||
to_char(:NEW.FK_PROCESSING_ID) || '|' ||
to_char(:NEW.FK_PRODUCT_MODIFICATION) || '|' ||
to_char(:NEW.FK_STOR_METHOD) || '|' ||
to_char(:NEW.FK_STOR_TEMP) || '|' ||
to_char(:NEW.HUN_PER_DMSO) || '|' ||
to_char(:NEW.HUN_PER_GLYCEROL) || '|' ||
to_char(:NEW.MAX_VALUE) || '|' ||
to_char(:NEW.NO_OF_CELL_MATER_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_EXTR_DNA_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_EXTR_DNA_MATER_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_INDI_FRAC) || '|' ||
to_char(:NEW.NO_OF_NONVIABLE_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_PLASMA_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_PLASMA_MATER_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_SEGMENTS) || '|' ||
to_char(:NEW.NO_OF_SERUM_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_SERUM_MATER_ALIQUOTS) || '|' ||
to_char(:NEW.NO_OF_VIABLE_CELL_ALIQUOTS) || '|' ||
to_char(:NEW.OTH_CRYOPROTECTANT) || '|' ||
to_char(:NEW.OTH_DILUENTS) || '|' ||
to_char(:NEW.OTHER_ANTICOAGULANT) || '|' ||
to_char(:NEW.OTHER_FREEZ_MANUFAC) || '|' ||
to_char(:NEW.OTHER_FROZEN_CONT) || '|' ||
to_char(:NEW.OTHER_PROCESSING) || '|' ||
to_char(:NEW.OTHER_PROD_MODI) || '|' ||
to_char(:NEW.PLASMALYTE) || '|' ||
to_char(:NEW.POINNT_NINE_PER_NACL) || '|' ||
to_char(:NEW.RBC_PALLETS) || '|' ||
to_char(:NEW.SIX_PER_HYDROXYETHYL_STARCH) || '|' ||
to_char(:NEW.SOP_REF_NO) || '|' ||
to_char(:NEW.SOP_STRT_DATE) || '|' ||
to_char(:NEW.SOP_TERMI_DATE) || '|' ||
to_char(:NEW.SOP_TITLE) || '|' ||
to_char(:NEW.SPEC_OTH_CRYOPRO) || '|' ||
to_char(:NEW.SPEC_OTH_DILUENTS) || '|' ||
to_char(:NEW.SPECIFY_OTH_ANTI) || '|' ||
to_char(:NEW.TEN_PER_DEXTRAN_40) || '|' ||
to_char(:NEW.TEN_UNIT_PER_ML_HEPARIN) || '|' ||
to_char(:NEW.THOU_UNIT_PER_ML_HEPARIN) || '|' ||
to_char(:NEW.TOT_CBU_ALIQUOTS) || '|' ||
to_char(:NEW.TOT_MATER_ALIQUOTS) || '|' ||
to_char(:NEW.TWEN_FIVE_HUM_ALBU) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.CPDA_PER) || '|' ||
to_char(:NEW.CPD_PER) || '|' ||
to_char(:NEW.ACD_PER) || '|' ||
to_char(:NEW.PLASMALYTE_PER) || '|' ||
to_char(:NEW.HUN_PER_DMSO_PER) || '|' ||
to_char(:NEW.HUN_PER_GLYCEROL_PER) || '|' ||
to_char(:NEW.TEN_PER_DEXTRAN_40_PER) || '|' ||
to_char(:NEW.TWEN_FIVE_HUM_ALBU_PER) || '|' ||
to_char(:NEW.OTH_CRYOPROTECTANT_PER) || '|' ||
to_char(:NEW.FIVE_PER_DEXTROSE_PER) || '|' ||
to_char(:NEW.OTH_DILUENTS_PER) || '|' ||
to_char(:NEW.FK_NUM_OF_BAGS) || '|' ||
to_char(:NEW.NO_OF_VIABLE_SMPL_FINAL_PROD) || '|' ||
to_char(:NEW.NO_OF_OTH_REP_ALLIQUOTS_F_PROD) || '|' ||
to_char(:NEW.NO_OF_OTH_REP_ALLIQ_ALTER_COND) || '|' ||
to_char(:NEW.FK_BAG_1_TYPE) || '|' ||
to_char(:NEW.FK_BAG_2_TYPE) || '|' ||
to_char(:NEW.THOU_UNIT_PER_ML_HEPARIN_PER) || '|' ||
to_char(:NEW.FIVE_UNIT_PER_ML_HEPARIN_PER) || '|' ||
to_char(:NEW.TEN_UNIT_PER_ML_HEPARIN_PER) || '|' ||
to_char(:NEW.SIX_PER_HYDRO_STARCH_PER) || '|' ||
to_char(:NEW.OTHER_ANTICOAGULANT_PER) || '|' ||
to_char(:NEW.FIVE_PER_HUMAN_ALBU_PER) || '|' ||
to_char(:NEW.POINNT_NINE_PER_NACL_PER) || '|' ||
to_char(:NEW.OTHER_BAG_TYPE);   
   INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);                 
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,159,18,'18_CBB_PROC_PROC_INFO_BI0.sql',sysdate,'9.0.0 Build#616');

commit;