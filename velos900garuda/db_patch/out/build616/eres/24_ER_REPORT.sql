set define off;

-- INSERTING into ER_REPORT


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 164;
  if (v_record_exists = 0) then
	Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
	values (164,'TNC Count of NMDP Cords Shipped','Analysis of the TNC Count of NMDP Cords Shipped (NCBI and Non-NCBI)','',0,null,null,'N',null,null,1,'rep_cbu','',null,null);
	COMMIT;	
	
	Update ER_REPORT set REP_SQL = 'select f_tnc_count('':selYear'') tnc from dual' where pk_report = 164;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'select f_tnc_count('':selYear'') tnc from dual' where pk_report = 164;
	COMMIT;

  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 166;
  if (v_record_exists = 0) then
	Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
	values (166,'TNC Count of NCBI Funded Cords Shipped','Analysis of the TNC Count of NCBI Funded Cords Shipped','',0,null,null,'N',null,null,1,'rep_cbu','',null,null);
	COMMIT;	
	
	Update ER_REPORT set REP_SQL = 'select f_tnc_count_ncbi('':selYear'') tnc from dual' where pk_report = 166;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'select f_tnc_count_ncbi('':selYear'') tnc from dual' where pk_report = 166;
	COMMIT;

  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,159,24,'24_ER_REPORT.sql',sysdate,'9.0.0 Build#616');

commit;