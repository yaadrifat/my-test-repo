set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_HLA WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_HLA  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,159,12,'12_CB_HLA_RID.sql',sysdate,'9.0.0 Build#616');

commit;
