CREATE OR REPLACE FORCE VIEW "ERES"."REP_CBU_DETAILS" ("PK_CORD", "CORD_REGISTRY_ID", "REGISTRY_MATERNAL_ID", "CORD_LOCAL_CBU_ID", "MATERNAL_LOCAL_ID", "CORD_ISBI_DIN_CODE", "IDNOTES", "IDNOTES_VISIBLE_TC", "HLANOTES", "HLANOTES_VISIBLE_TC", "CBBID", "CBB_ID", "STORAGE_LOC", "CBU_COLLECTION_SITE", "CBU_INFO_NOTES", "CBU_INFO_NOTES_VISIBLE_TC", "ELIGIBLE_STATUS", "LIC_STATUS", "ELIGIBILITY_NOTES", "ELIGIBILITY_NOTES_VISIBLE_TC", "IDM_NOTES_VISIBLE_TC", "PRCSNG_START_DATE", "BACT_CULTURE", "BACT_COMMENT", "FRZ_DATE", "FUNGAL_CULTURE", "FUNG_COMMENT", "ABO_BLOOD_TYPE", "HEMOGLOBIN_SCRN", "RH_TYPE", "LAB_SUM_NOTES", "LAB_SUM_NOTES_VISIBLE_TC", "PROC_NAME", "PROC_START_DATE", "PROC_TERMI_DATE", "PROCESSING", "AUTOMATED_TYPE", "OTHER_PROCESSING", "PRODUCT_MODIFICATION", "OTHER_PRODUCT_MODI", "STORAGE_METHOD", "FREEZER_MANUFACT", "OTHER_FREEZER_MANUFACT", "FROZEN_IN", "OTHER_FROZEN_CONT", "NO_OF_BAGS", "CRYOBAG_MANUFAC", "BAGTYPE", "BAG1TYPE", "BAG2TYPE", "OTHER_BAG", "HEPARIN_THOU_PER",
  "HEPARIN_THOU_ML", "HEPARIN_FIVE_PER", "HEPARIN_FIVE_ML", "HEPARIN_TEN_PER", "HEPARIN_TEN_ML", "HEPARIN_SIX_PER", "HEPARIN_SIX_ML", "CPDA_PER", "CPDA_ML", "CPD_PER", "CPD_ML", "ACD_PER", "ACD_ML", "OTHR_ANTI_PER", "OTHR_ANTI_ML", "SPECI_OTHR_ANTI", "HUN_DMSO_PER", "HUN_DMSO_ML", "HUN_GLYCEROL_PER", "HUN_GLYCEROL_ML", "TEN_DEXTRAN_40_PER", "TEN_DEXTRAN_40_ML", "FIVE_HUMAN_ALBU_PER", "FIVE_HUMAN_ALBU_ML", "TWENTYFIVE_HUMAN_ALBU_PER", "TWENTYFIVE_HUMAN_ALBU_ML", "PLASMALYTE_PER", "PLASMALYTE_ML", "OTHR_CRYOPROTECTANT_PER", "OTHR_CRYOPROTECTANT_ML", "SPEC_OTHR_CRYOPROTECTANT", "FIVE_DEXTROSE_PER", "FIVE_DEXTROSE_ML", "POINT_NINE_NACL_PER", "POINT_NINE_NACL_ML", "OTHR_DILUENTS_PER", "OTHR_DILUENTS_ML", "SPEC_OTHR_DILUENTS", "PRODUCTCODE", "STORAGE_TEMPERATURE", "MAX_VOL", "CTRL_RATE_FREEZING", "INDIVIDUAL_FRAC", "FILTER_PAPER", "RPC_PELLETS", "EXTR_DNA", "SERUM_ALIQUOTES", "PLASMA_ALIQUOTES", "NONVIABLE_ALIQUOTES", "VIABLE_SAMP_FINAL_PROD", "NO_OF_SEGMENTS",
  "NO_OF_OTH_REP_ALLIQUOTS_F_PROD", "NO_OF_OTH_REP_ALLIQ_ALTER_COND", "NO_OF_SERUM_MATER_ALIQUOTS", "NO_OF_PLASMA_MATER_ALIQUOTS", "NO_OF_EXTR_DNA_MATER_ALIQUOTS", "NO_OF_CELL_MATER_ALIQUOTS", "BACT_CUL_STRT_DT", "FUNG_CUL_STRT_DT", "INELIGIBLEREASON", "UNLICENSEREASON", "ELIGCOMMENTS", "ELGIBLE_MODI_DT", "LIC_MODI_DT", "ELI_FLAG", "LIC_FLAG")
AS
  SELECT pk_cord,
    cord_registry_id,
    registry_maternal_id,
    cord_local_cbu_id,
    maternal_local_id,
    CORD_ISBI_DIN_CODE,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE entity_id   in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''id''      
)',' ,' ) idnotes,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE visibility      = ''1''    
AND entity_id  in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''id''      
)',' ,') idnotes_visible_tc,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE entity_id in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''hla''      
)',', ' ) hlanotes,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE visibility      = ''1''    
AND entity_id  in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''hla''      
)',', ' ) hlanotes_visible_tc,
    (SELECT site_name FROM er_site WHERE pk_site = fk_cbb_id
    ) cbbid,
    (SELECT site_id FROM er_site WHERE pk_site = fk_cbb_id
    ) cbb_id,
    (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = fk_cbu_stor_loc
    ) storage_loc,
    (SELECT site_name FROM er_site WHERE pk_site = fk_cbu_coll_site
    ) cbu_collection_site,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE entity_id in('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''cbu_info''      
)',', ') CBU_INFO_NOTES,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE visibility      = ''1''    
AND entity_id in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''cbu_info''      
)',' ,') cbu_info_notes_visible_tc,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst = fk_cord_cbu_eligible_status
    ) eligible_status,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst = fk_cord_cbu_lic_status
    ) lic_status,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE entity_id  in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''eligiblity''      
)',', ') eligibility_notes,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE visibility      = ''1''    
AND entity_id  in ('
    ||pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''eligiblity''      
)',', ')ELIGIBILITY_NOTES_VISIBLE_TC,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE visibility      = ''1''    
AND entity_id  in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''infct_dis_mark''      
)',', ') idm_notes_visible_tc,
    TO_CHAR(prcsng_start_date,'Mon DD,YYYY') prcsng_start_date,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst = fk_cord_bact_cul_result
    ) bact_culture ,
    bact_comment,
    TO_CHAR(frz_date,'Mon DD,YYYY')frz_date,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst = fk_cord_fungal_cul_result
    ) fungal_culture,
    fung_comment,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst = fk_cord_abo_blood_type
    ) abo_blood_type,
    (SELECT codelst_desc FROM er_codelst WHERE pk_codelst = hemoglobin_scrn
    ) hemoglobin_scrn,
    (SELECT codelst_desc FROM er_codelst WHERE pk_codelst =fk_cord_rh_type
    ) rh_type,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE entity_id  in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''lab_sum''      
)',', ') lab_sum_notes,
    rowtocol('SELECT notes    
FROM cb_notes    
WHERE visibility      = ''1''    
AND entity_id in ('
    ||a.pk_cord
    ||')    
AND fk_notes_category =      
(SELECT pk_codelst      
FROM er_codelst      
WHERE codelst_type = ''note_cat''      
AND codelst_subtyp = ''lab_sum''      
)',', ') lab_sum_notes_visible_tc,
    (SELECT proc_name
    FROM cbb_processing_procedures
    WHERE pk_proc =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) proc_name,
    (SELECT TO_CHAR(proc_start_date,'Mon DD, YYYY')
    FROM cbb_processing_procedures
    WHERE pk_proc =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) proc_start_date,
    (SELECT TO_CHAR(proc_termi_date,'Mon DD, YYYY')
    FROM cbb_processing_procedures
    WHERE pk_proc =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) proc_termi_date,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_proc_meth_id
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) processing,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_if_automated
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) automated_type,
    (SELECT other_processing
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) other_processing,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_product_modification
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) product_modification,
    (SELECT other_prod_modi
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) other_product_modi,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_stor_method
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) storage_method,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_freez_manufac
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) freezer_manufact,
    (SELECT other_freez_manufac
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) other_freezer_manufact,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_frozen_in
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) frozen_in,
    (SELECT other_frozen_cont
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) other_frozen_cont,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_num_of_bags
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) no_of_bags,
    (SELECT cryobag_manufac
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) cryobag_manufac,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_bagtype
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) bagtype,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_bag_1_type
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) bag1type,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_bag_2_type
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) bag2type,
    (SELECT other_bag_type
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) other_bag,
    (SELECT THOU_UNIT_PER_ML_HEPARIN_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_thou_per,
    (SELECT THOU_UNIT_PER_ML_HEPARIN
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_thou_ml,
    (SELECT FIVE_UNIT_PER_ML_HEPARIN_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_five_per,
    (SELECT FIVE_UNIT_PER_ML_HEPARIN
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_five_ml,
    (SELECT TEN_UNIT_PER_ML_HEPARIN_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_ten_per,
    (SELECT TEN_UNIT_PER_ML_HEPARIN
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_ten_ml,
    (SELECT SIX_PER_HYDRO_STARCH_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_six_per,
    (SELECT SIX_PER_HYDROXYETHYL_STARCH
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) heparin_six_ml,
    (SELECT CPDA_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) cpda_per,
    (SELECT CPDA
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) cpda_ml,
    (SELECT CPD_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) cpd_per,
    (SELECT CPD
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) cpd_ml,
    (SELECT ACD_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) acd_per,
    (SELECT ACD
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) acd_ml,
    (SELECT OTHER_ANTICOAGULANT_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) othr_anti_per,
    (SELECT OTHER_ANTICOAGULANT
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) othr_anti_ml,
    (SELECT SPECIFY_OTH_ANTI
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) speci_othr_anti,
    (SELECT HUN_PER_DMSO_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) hun_dmso_per,
    (SELECT HUN_PER_DMSO
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) hun_dmso_ml,
    (SELECT HUN_PER_GLYCEROL_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) hun_glycerol_per,
    (SELECT HUN_PER_GLYCEROL
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) hun_glycerol_ml,
    (SELECT TEN_PER_DEXTRAN_40_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) ten_dextran_40_per,
    (SELECT TEN_PER_DEXTRAN_40
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) ten_dextran_40_ml,
    (SELECT FIVE_PER_HUMAN_ALBU_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) five_human_albu_per,
    (SELECT FIVE_PER_HUMAN_ALBU
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) five_human_albu_ml,
    (SELECT TWEN_FIVE_HUM_ALBU_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) twentyfive_human_albu_per,
    (SELECT TWEN_FIVE_HUM_ALBU
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) twentyfive_human_albu_ml,
    (SELECT PLASMALYTE_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) plasmalyte_per,
    (SELECT PLASMALYTE
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) plasmalyte_ml,
    (SELECT OTH_CRYOPROTECTANT_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) othr_cryoprotectant_per,
    (SELECT OTH_CRYOPROTECTANT
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) othr_cryoprotectant_ml,
    (SELECT SPEC_OTH_CRYOPRO
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) spec_othr_cryoprotectant,
    (SELECT FIVE_PER_DEXTROSE_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) five_dextrose_per,
    (SELECT FIVE_PER_DEXTROSE
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) five_dextrose_ml,
    (SELECT POINNT_NINE_PER_NACL_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) point_nine_nacl_per,
    (SELECT POINNT_NINE_PER_NACL
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) point_nine_nacl_ml,
    (SELECT OTH_DILUENTS_PER
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) othr_diluents_per,
    (SELECT OTH_DILUENTS
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) othr_diluents_ml,
    (SELECT SPEC_OTH_DILUENTS
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) spec_othr_diluents,
    PRODUCT_CODE productcode,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_stor_temp
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) storage_temperature,
    (SELECT max_value
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) max_vol,
    (SELECT codelst_desc
    FROM er_codelst
    WHERE pk_codelst=
      (SELECT fk_contrl_rate_freezing
      FROM cbb_processing_procedures_info
      WHERE fk_processing_id =
        (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
        )
      )
    ) ctrl_rate_freezing,
    (SELECT no_of_indi_frac
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) individual_frac,
    (SELECT filter_paper
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) filter_paper,
    (SELECT rbc_pallets
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) rpc_pellets,
    (SELECT no_of_extr_dna_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) extr_dna,
    (SELECT no_of_serum_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) serum_aliquotes,
    (SELECT no_of_plasma_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) plasma_aliquotes,
    (SELECT no_of_nonviable_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) nonviable_aliquotes,
    (SELECT no_of_viable_smpl_final_prod
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) viable_samp_final_prod,
    (SELECT no_of_segments
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) no_of_segments,
    (SELECT no_of_oth_rep_alliquots_f_prod
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) no_of_oth_rep_alliquots_f_prod,
    (SELECT no_of_oth_rep_alliq_alter_cond
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) no_of_oth_rep_alliq_alter_cond,
    (SELECT no_of_serum_mater_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) no_of_serum_mater_aliquots,
    (SELECT no_of_plasma_mater_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) no_of_plasma_mater_aliquots,
    (SELECT no_of_extr_dna_mater_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) no_of_extr_dna_mater_aliquots,
    (SELECT no_of_cell_mater_aliquots
    FROM cbb_processing_procedures_info
    WHERE fk_processing_id =
      (SELECT fk_cbb_procedure FROM cb_cord WHERE pk_cord = a.pk_cord
      )
    ) NO_OF_CELL_MATER_ALIQUOTS,
    TO_CHAR(a.bact_cult_date,'Mon DD,YYYY') bact_cul_strt_dt,
    TO_CHAR(a.fung_cult_date,'Mon DD,YYYY') fung_cul_strt_dt,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') INELIGIBLEREASON,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') UNLICENSEREASON,
    a.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
    (SELECT TO_CHAR(MAX(CREATED_ON),'Mon DD, YYYY')
    FROM CB_ENTITY_STATUS
    WHERE ENTITY_ID     =a.pk_cord
    AND STATUS_TYPE_CODE='eligibility'
    ) ELGIBLE_MODI_DT,
    (SELECT TO_CHAR(MAX(CREATED_ON),'Mon DD, YYYY')
    FROM CB_ENTITY_STATUS
    WHERE ENTITY_ID     =a.pk_cord
    AND STATUS_TYPE_CODE='licence'
    ) LIC_MODI_DT,
    DECODE(a.FK_CORD_CBU_ELIGIBLE_STATUS,
    (SELECT PK_CODELST
    FROM ER_CODELST
    WHERE CODELST_TYPE='eligibility'
    AND CODELST_SUBTYP='eligible'
    ),'0',
    (SELECT PK_CODELST
    FROM ER_CODELST
    WHERE CODELST_TYPE='eligibility'
    AND CODELST_SUBTYP='ineligible'
    ),'1',
    (SELECT PK_CODELST
    FROM ER_CODELST
    WHERE CODELST_TYPE='eligibility'
    AND CODELST_SUBTYP='incomplete'
    ),'2',
    (SELECT PK_CODELST
    FROM ER_CODELST
    WHERE CODELST_TYPE='eligibility'
    AND CODELST_SUBTYP='not_appli_prior'
    ),'3') ELI_FLAG,
    DECODE(a.FK_CORD_CBU_LIC_STATUS,
    (SELECT PK_CODELST
    FROM ER_CODELST
    WHERE CODELST_TYPE='licence'
    AND CODELST_SUBTYP='licensed'
    ),'0',
    (SELECT PK_CODELST
    FROM ER_CODELST
    WHERE CODELST_TYPE='licence'
    AND CODELST_SUBTYP='unlicensed'
    ),'1') LIC_FLAG
  FROM cb_cord a;
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,167,6,'06_VIEW_REP_CBU_DETAILS.sql',sysdate,'9.0.0 Build#624');

commit;