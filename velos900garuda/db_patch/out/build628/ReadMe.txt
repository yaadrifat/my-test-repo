/* This readMe is specific to Velos eResearch version 9.0 build #628 */

=====================================================================================================================================
Garuda :

1. Patch 04_ER_CODELST_DELETE.sql & 05_ER_CODELST_INSERT.SQL are Specific to NMDP.

	
2. For E-MAIL Configuration 
A. we have included two jar files i.e. activation.jar and mail.jar under the following directory:
		 	JBOSS_HOME/server/eresearch/lib/
B. We have Placed the aithentmail_garuda.properties file under the following directory:
		 	JBOSS_HOME/server/eresearch/conf/	
C. To Configure the aithentmail_garuda.properties file:
-------------------------------------------------------		
			INITIAL_CONTEXT_FACTORY=org.jnp.interfaces.NamingContextFactory
			PROVIDER_URL=jnp://<system_ip_address_of_jboss_server>:1099
			URL_PKG_PREFIXES =
			
			#mail will be sent if the value is true
			mailenable = true
			
			#Data Base connection for mail
			
			ORACLE_CONNECTION =jdbc:oracle:thin:@<current_DB_ip_address>:<corresponding_DB_port_number>:<SID_name>
			
			ORACLE_USER_NAME =<user_name>
			
			ORACLE_PASSWORD=<password>
						
	
	     
=====================================================================================================================================
eResearch:



=====================================================================================================================================
eResearch Localization:


=====================================================================================================================================
