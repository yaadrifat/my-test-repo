--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_spec_type'
    AND codelst_subtyp = 'cbu_seg';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'cbu_spec_type','cbu_seg','CBU Segment','N',1,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_spec_type'
    AND codelst_subtyp = 'fltr_ppr_seg';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'cbu_spec_type','fltr_ppr_seg','Filter paper from segment','N',2,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_spec_type'
    AND codelst_subtyp = 'fltr_ppr_pro';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'cbu_spec_type','fltr_ppr_pro','Filter paper from processing','N',3,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_spec_type'
    AND codelst_subtyp = 'red_cel_plt';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'cbu_spec_type','red_cel_plt','Red cell pellet','N',4,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_spec_type'
    AND codelst_subtyp = 'ext_dna';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'cbu_spec_type','ext_dna','Extracted DNA','N',5,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_spec_type'
    AND codelst_subtyp = 'cord_blood';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'cbu_spec_type','cord_blood','Whole cord blood','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'mat_spec_type'
    AND codelst_subtyp = 'mat_serum';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'mat_spec_type','mat_serum','Maternal Serum','N',1,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'mat_spec_type'
    AND codelst_subtyp = 'mat_plasma';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'mat_spec_type','mat_plasma','Maternal Plasma','N',2,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'mat_spec_type'
    AND codelst_subtyp = 'ext_dna';
  if (v_record_exists = 0) then
      Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
	values(SEQ_ER_CODELST.nextval,null,'mat_spec_type','ext_dna','Extracted DNA','N',3,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,171,5,'05_ER_CODELST_INSERT.sql',sysdate,'9.0.0 Build#628');

commit;