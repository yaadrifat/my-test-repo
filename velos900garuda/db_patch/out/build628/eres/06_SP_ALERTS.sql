create or replace
PROCEDURE SP_ALERTS(order_id number) AS
TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
cur_cv   CurTyp;  -- declare cursor variable
v_alertId number;
v_alert_title varchar2(4000);
v_alert_desc varchar2(4000);
v_alert_wording varchar2(4000);
v_alert_precondition varchar2(4000) :='';
v_col_name varchar2(4000);
v_alert_precond_cnt number :=0;
v_query varchar2(4000);
sql_fieldval varchar2(4000);
var_instance_cnt number;
v_userId number;
v_cordregid varchar2(255);
V_ORDERTYPE VARCHAR2(255);
v_sampleatlab varchar2(1);
V_ALERTNAME VARCHAR2(255);
v_resol number:=0;
v_resoldesc VARCHAR2(50 CHAR):='N/A';
v_orderId number;
v_schshipdate varchar2(50);
v_temp number;
BEGIN
FOR CUR_ALERTS IN(select pk_codelst,CODELST_SUBTYP,codelst_desc,codelst_custom_col1,codelst_custom_col from er_codelst where codelst_type in('alert','alert_resol') order by pk_codelst)
LOOP
  v_alertId := cur_alerts.pk_codelst;
  V_ALERTNAME:=cur_alerts.codelst_subtyp;
	v_alert_title := cur_alerts.codelst_desc;
	v_alert_desc := cur_alerts.codelst_custom_col1;
	v_alert_wording :=cur_alerts.codelst_custom_col;
  v_alert_precondition := getalertcondition(v_alertid,'PRE');
if v_alert_precondition<>' ' then
    dbms_output.put_line('alert Id :::'||v_alertid);
    dbms_output.put_line('alert pre condition is::'||v_alert_precondition);

    if v_alertname in('alert_01','alert_02','alert_03','alert_04','alert_15','alert_27','alert_20') then
      v_query := 'select count(*) cnt from er_order where '||v_alert_precondition;
    elsif v_alertname in('alert_05','alert_06') then
      v_query := 'select count(*) cnt from er_order,er_order_header where er_order.fk_order_header=er_order_header.pk_order_header and '||v_alert_precondition;
    elsif v_alertname in('alert_07','alert_08','alert_09','alert_10','alert_11') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_shipment where er_order.fk_order_header=er_order_header.pk_order_header and cb_shipment.fk_order_id=er_order.pk_order and '||v_alert_precondition;
    elsif v_alertname='alert_19' then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and '||v_alert_precondition;
    elsif v_alertname IN('alert_23','alert_24','alert_25') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord,er_attachments,cb_upload_info where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and er_attachments.entity_id=cb_cord.pk_cord and er_attachments.entity_type=(select pk_codelst from er_codelst where codelst_type=''entity_type'' and codelst_subtyp=''CBU'') and er_attachments.pk_attachment=cb_upload_info.fk_attachmentid and '||v_alert_precondition;
    end if;
    v_query:=replace(v_query,'#keycolumn',order_id);
     dbms_output.put_line('^^^^^^^^^^^'||v_query);
    open cur_cv for v_query;
     LOOP
          FETCH cur_cv INTO sql_fieldval ;
          EXIT WHEN cur_cv%NOTFOUND;
           dbms_output.put_line('display result  ' || sql_fieldval );
           if sql_fieldval>0 then
            select nvl(assigned_to,0),nvl(crd.cord_registry_id,'N/A'),nvl(ord.order_sample_at_lab,'-'),nvl(ord.fk_order_resol_by_tc,0) into v_userid,v_cordregid,v_sampleatlab,v_resol from er_order ord left outer join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) where pk_order=order_id;
            dbms_output.put_line('v_resol............'||v_resol);
           if v_resol<>0 then
           dbms_output.put_line('before getting resolution desc...............');
            select cbu_status_desc into v_resoldesc from cb_cbu_status where pk_cbu_status=v_resol;
            dbms_output.put_line('after getting resolution desc...............'||v_resoldesc);
           end if;
           select ord.pk_order,nvl(to_char(ship.sch_shipment_date,'Mon DD, YYYY'),'N/A') into v_temp,v_schshipdate from er_order ord left outer join (select fk_order_id,sch_shipment_date from cb_shipment where fk_order_id=order_id and fk_shipment_type=(select pk_codelst from er_codelst where codelst_type='shipment_type' and codelst_subtyp='CBU Shipment')) ship on(ship.fk_order_id=ord.pk_order) where ord.pk_order=order_id;
           v_alert_wording:=replace(v_alert_wording,'<CBU Registry ID>',v_cordregid);
           v_alert_wording:=replace(v_alert_wording,'<Request Type>',v_ordertype);
           v_alert_wording:=replace(v_alert_wording,'<Resolution Code>',v_resoldesc);
           v_alert_wording:=replace(v_alert_wording,'<Shipment Scheduled Date>',v_schshipdate);
           if v_sampleatlab='Y' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Lab');
           elsif v_sampleatlab='N' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Ship');
           end if;
           select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=order_id and alrt_inst.fk_userid=v_userid and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify');
                dbms_output.put_line('var_instance_cnt:::::'||var_instance_cnt);
                if var_instance_cnt=0 then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,order_id,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER'),v_userid,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify'),v_alert_wording);
                end if;
            for usrs in(select altr.FK_USER,altr.EMAIL_FLAG email_flag,altr.NOTIFY_FLAG notify_flag,altr.PROMPT_FLAG prompt_flag,addr.add_email mail from cb_user_alerts altr,er_user usr,er_add addr where altr.fk_user=usr.pk_user and usr.fk_peradd=addr.pk_add and fk_codelst_alert=v_alertid)
            loop
              dbms_output.put_line('users that has assigned alert is:::'||usrs.fk_user);
              if usrs.email_flag=1 then
              select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=order_id and alrt_inst.entity_type=(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER') and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail');
              if var_instance_cnt=0 then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,order_id,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER'),usrs.fk_user,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail'),usrs.mail,v_alert_wording);
              dbms_output.put_line('Email alerts implemented');
                insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
              end if;
              end if;
              end loop;
              commit;
           end if;
      END LOOP;
    dbms_output.put_line('alert wording is::'||v_alert_wording);
end if;
END LOOP;
update er_order set data_modified_flag='' where pk_order=order_id;
EXCEPTION
  WHEN NO_DATA_FOUND THEN 
  dbms_output.put_line ('A SELECT...INTO did not return any row.'); 
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,171,6,'06_SP_ALERTS.sql',sysdate,'9.0.0 Build#628');

commit;



