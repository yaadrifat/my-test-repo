set define off;
delete from er_repxsl where pk_repxsl in (110,159);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,171,1,'01_ER_REPXSL.sql',sysdate,'9.0.0 Build#628');

commit;