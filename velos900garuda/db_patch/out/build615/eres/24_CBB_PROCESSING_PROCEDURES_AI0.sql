CREATE OR REPLACE TRIGGER "ERES"."CBB_PROCESSING_PROCEDURES_AI0"  AFTER INSERT ON ERES.CBB_PROCESSING_PROCEDURES 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
V_ROWID NUMBER(10);

BEGIN
		SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO V_ROWID FROM DUAL;

		PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (V_ROWID, 'CBB_PROCESSING_PROCEDURES', :NEW.RID,:NEW.PK_PROC,'I',:NEW.CREATOR);        
		
	IF :NEW.PK_PROC IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'PK_PROC',NULL,:NEW.PK_PROC,NULL,NULL);
	END IF;
	IF :NEW.CREATOR IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
	END IF;
	
	IF :NEW.CREATED_ON IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	END IF;
	
	IF :NEW.IP_ADD IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
	END IF;
	IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
	END IF;
	IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	END IF;
	IF :NEW.PROC_NO IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'PROC_NO',NULL,:NEW.PROC_NO,NULL,NULL);
	END IF;
	IF :NEW.PROC_NAME IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'PROC_NAME',NULL,:NEW.PROC_NAME,NULL,NULL);
	END IF;
	IF :NEW.PROC_START_DATE IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'PROC_START_DATE',NULL,TO_CHAR(:NEW.PROC_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	END IF;
	IF :NEW.PROC_TERMI_DATE IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'PROC_TERMI_DATE',NULL,TO_CHAR(:NEW.PROC_TERMI_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	END IF;
	IF :NEW.PROC_VERSION IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'PROC_VERSION',NULL,:NEW.PROC_VERSION,NULL,NULL);
	END IF;
	IF :NEW.RID IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'RID',NULL,:NEW.RID,NULL,NULL);
	END IF;
	IF :NEW.FK_PROCESSING_ID IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'FK_PROCESSING_ID',NULL,:NEW.FK_PROCESSING_ID,NULL,NULL);
	END IF;
	IF :NEW.FK_SITE IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'FK_SITE',NULL,:NEW.FK_SITE,NULL,NULL);
	END IF;
	IF :NEW.PRODUCT_CODE IS NOT NULL THEN
		PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'PRODUCT_CODE',NULL,:NEW.PRODUCT_CODE,NULL,NULL);
	END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,24,'24_CBB_PROCESSING_PROCEDURES_AI0.sql',sysdate,'9.0.0 Build#615');

commit;