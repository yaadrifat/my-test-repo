alter table sch_protocol_visit add WIN_BEFORE_NUMBER number;
comment on column sch_protocol_visit.WIN_BEFORE_NUMBER is 'This column stores number of days before detail for Visit Window';

alter table sch_protocol_visit add WIN_BEFORE_UNIT char(1);
comment on column sch_protocol_visit.WIN_BEFORE_UNIT is 'This column stores before unit detail for Visit Window. Possible values:D,Y,M,W';

alter table sch_protocol_visit add WIN_AFTER_NUMBER number;
comment on column sch_protocol_visit.WIN_AFTER_NUMBER is 'This column stores number of days after detail for Visit Window';

alter table sch_protocol_visit add WIN_AFTER_UNIT char(1);
comment on column sch_protocol_visit.WIN_AFTER_UNIT is 'This column stores after unit detail for Visit Window. Possible values:D,Y,M,W';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,69,1,'01_sch_protocol_visit_addcolumn.sql',sysdate,'8.9.0 Build#526');

commit;