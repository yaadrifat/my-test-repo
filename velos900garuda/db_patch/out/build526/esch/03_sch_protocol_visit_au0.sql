create or replace
TRIGGER SCH_PROTOCOL_VISIT_AUO
AFTER UPDATE
ON SCH_PROTOCOL_VISIT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_PROTOCOL_VISIT', :OLD.rid, 'U', usr );



   IF NVL(:OLD.pk_protocol_visit,0) !=
      NVL(:NEW.pk_protocol_visit,0) THEN
      audit_trail.column_update
        (raid, 'pk_protocol_visit', :OLD.pk_protocol_visit, :NEW.pk_protocol_visit);
   END IF;
   IF NVL(:OLD.fk_protocol,0) !=
      NVL(:NEW.fk_protocol,0) THEN
      audit_trail.column_update
        (raid, 'FK_PROTOCOL', :OLD.fk_protocol, :NEW.fk_protocol);
   END IF;
   IF NVL(:OLD.visit_no,0) !=
      NVL(:NEW.visit_no,0) THEN
      audit_trail.column_update
        (raid, 'VISIT_NO', :OLD.visit_no, :NEW.visit_no);
   END IF;

   IF NVL(:OLD.visit_name,' ') !=
      NVL(:NEW.visit_name,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_NAME', :OLD.visit_name, :NEW.visit_name);
   END IF;

   IF NVL(:OLD.description,' ') !=
      NVL(:NEW.description,' ') THEN
      audit_trail.column_update
        (raid, 'DESCRIPTION', :OLD.description, :NEW.description);
   END IF;

   IF NVL(:OLD.displacement,0) !=
      NVL(:NEW.displacement,0) THEN
      audit_trail.column_update
        (raid, 'DISPLACEMENT', :OLD.displacement, :NEW.displacement);
   END IF;

   IF NVL(:OLD.num_months,0) !=
      NVL(:NEW.NUM_MONTHS,0) THEN
      audit_trail.column_update
        (raid, 'NUM_MONTHS', :OLD.num_months, :NEW.num_months);
   END IF;

   IF NVL(:OLD.num_days,0) !=
      NVL(:NEW.NUM_MONTHS,0) THEN
      audit_trail.column_update
        (raid, 'NUM_DAYS', :OLD.num_days, :NEW.num_days);
   END IF;

   IF NVL(:OLD.insert_after,0) !=
      NVL(:NEW.insert_after,0) THEN
      audit_trail.column_update
        (raid, 'INSERT_AFTER', :OLD.insert_after, :NEW.insert_after);
   END IF;

   IF NVL(:OLD.num_weeks,0) !=
      NVL(:NEW.num_weeks,0) THEN
      audit_trail.column_update
        (raid, 'NUM_WEEKS', :OLD.num_weeks, :NEW.num_weeks);
   END IF;

   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID', :OLD.rid, :NEW.rid);
   END IF;

   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY', :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	     to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD', :OLD.ip_add, :NEW.ip_add);
   END IF;

   IF NVL(:OLD.insert_after_interval,0) !=
      NVL(:NEW.insert_after_interval,0) THEN
      audit_trail.column_update
        (raid, 'INSERT_AFTER_INTERVAL', :OLD.insert_after_interval, :NEW.insert_after_interval);
   END IF;
   IF NVL(:OLD.insert_after_interval_unit,' ') !=
      NVL(:NEW.insert_after_interval_unit,' ') THEN
      audit_trail.column_update
        (raid, 'INSERT_AFTER_INTERVAL_UNIT', :OLD.insert_after_interval_unit, :NEW.insert_after_interval_unit);
   END IF;
   IF NVL(:OLD.protocol_type,' ') !=
      NVL(:NEW.protocol_type,' ') THEN
      audit_trail.column_update
        (raid, 'PROTOCOL_TYPE', :OLD.protocol_type, :NEW.protocol_type);
   END IF;

   IF NVL(:OLD.visit_type,' ') !=
      NVL(:NEW.visit_type,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE', :OLD.visit_type, :NEW.visit_type);
   END IF;

   IF NVL(:OLD.offline_flag,0) !=
      NVL(:NEW.offline_flag,0) THEN
      audit_trail.column_update
        (raid, 'offline_flag', :OLD.offline_flag, :NEW.offline_flag);
   END IF;
   
   IF NVL(:OLD.hide_flag,0) !=
      NVL(:NEW.hide_flag,0) THEN
      audit_trail.column_update
        (raid, 'hide_flag', :OLD.hide_flag, :NEW.hide_flag);
   END IF;
   
   IF NVL(:OLD.no_interval_flag,0) !=
      NVL(:NEW.no_interval_flag,0) THEN
      audit_trail.column_update
        (raid, 'no_interval_flag', :OLD.no_interval_flag, :NEW.no_interval_flag);
   END IF;
   
   IF NVL(:OLD.win_before_number,0) !=
      NVL(:NEW.win_before_number,0) THEN
      audit_trail.column_update
        (raid, 'win_before_number', :OLD.win_before_number, :NEW.win_before_number);
   END IF;
    
   IF NVL(:OLD.win_before_unit,' ') !=
      NVL(:NEW.win_before_unit,' ') THEN
      audit_trail.column_update
        (raid, 'win_before_unit', :OLD.win_before_unit, :NEW.win_before_unit);
   END IF; 
   
   IF NVL(:OLD.win_after_number,0) !=
      NVL(:NEW.win_after_number,0) THEN
      audit_trail.column_update
        (raid, 'win_after_number', :OLD.win_after_number, :NEW.win_after_number);
   END IF;
   
   IF NVL(:OLD.win_after_unit,' ') !=
      NVL(:NEW.win_after_unit,' ') THEN
      audit_trail.column_update
        (raid, 'win_after_unit', :OLD.win_after_unit, :NEW.win_after_unit);
   END IF;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,69,3,'03_sch_protocol_visit_au0.sql',sysdate,'8.9.0 Build#526');

commit;