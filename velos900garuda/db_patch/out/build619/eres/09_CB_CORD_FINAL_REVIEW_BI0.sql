create or replace TRIGGER "ERES".CB_CORD_FINAL_REVIEW_BI0 BEFORE INSERT ON CB_CORD_FINAL_REVIEW
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_CORD_FINAL_REVIEW',erid, 'I',usr);

  INSERT_DATA:=:NEW.PK_CORD_FINAL_REVIEW || '|' ||
to_char(:NEW.FK_CORD_ID) || '|' ||
to_char(:NEW.LICENSURE_CONFIRM_FLAG) || '|' ||
to_char(:NEW.ELIGIBLE_CONFIRM_FLAG) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.FINAL_REVIEW_CONFIRM_FLAG) || '|' ||
to_char(:NEW.REVIEW_CORD_ACCEPTABLE_FLAG) || '|' ||
to_char(:NEW.FK_ORDER_ID) || '|' ||
to_char(:NEW.LIC_CREATOR) || '|' ||
to_char(:NEW.LIC_CREATED_ON) || '|' ||
to_char(:NEW.ELIG_CREATOR) || '|' ||
to_char(:NEW.ELIG_CREATED_ON) || '|' ||
to_char(:NEW.FINAL_REVIEW_CREATOR) || '|' ||
to_char(:NEW.FINAL_REVIEW_CREATED_ON) || '|' ||
to_char(:NEW.FK_FINAL_ELIG_QUES);
INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,162,9,'09_CB_CORD_FINAL_REVIEW_BI0.sql',sysdate,'9.0.0 Build#619');

commit;