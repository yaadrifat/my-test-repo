1. Replace the existing labelBundle.properties file which is in eres_home path by the latest file released in this build.

Note
----
Please refer 'Label handling in DB' document posted in the Reference Document section.
1. In Report Central page, Report names, Report names heading, Tool tip content and Filter links will not be affected by Study and Patient Label changes as per the requirement.
2. In reports, the report name, filters will not be affected by Study and Patient Label changes as per the requirement.
3. Ad-Hoc Queries are not covered. Other reports will be covered probably in the next build.

Reports Modified for Study and Patient Label Change in Report Central Module for this Release
---------------------------------------------------------------------------------------------
1	Active Protocols
2	Protocols By User
6	Public Protocols
7	All Protocols
15	Summary for Public Broadcast
23	Study Team
24	Protocol Tracking
43	User Profile and Access
48	Milestone Forecast by Interval
66	Patient Form Tracking
67	Milestones Summary (AR/ AP)
68	Milestones Details Report
70	Milestones by Interval
76	Milestones/Payment Discrepancy by Interval
79	Study Enrollment
82	Active/Enrolling Protocols
84	Protocol Compliance
88	Adverse Events by Patient
89	Cost Estimate by Type
91	Summary 3 - Accrual to Therapeutic Protocols
93	Patient Timeline
97	Study Dump
98	Study Dump
108	By Category
109	By SOC
110	By Visit
111	Study Budget by Type
112	Study Budget by Category
114	NCI Summary 3 - Accrual to Therapeutic Protocols
115	Adverse Event Summary 
116	Study Enrollment by Race/Gender
117	Enrollment by Race / Ethnicity /Gender
121	Accrual Trend Summary
122	Patient Status Grid
123	Milestone Forecast Details
124	Overview
126	Form Queries Overview
127	Form Query Details
128	Specimens by Patient
129	Storage Summary
130	Specimens by Study
132	Invoice by Milestone Type
136	Depleted Specimens
137	Specimen Trail (Study Specimens)
138	Specimen Processing History
141	Storage Trail & Occupancy
142	Specimen Storage Association
143	Specimen Provider Association
145	Specimen Status History
153	IRB Protocol History
154	Specimen Trail (Non Study Specimens)
155	Specimen Listing
159	Combined Patient Budget
160	By Coverage Type

