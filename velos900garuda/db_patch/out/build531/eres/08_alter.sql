ALTER TABLE ERES.ER_SUBMISSION_PROVISO
ADD (fk_codelst_provisotype NUMBER);

COMMENT ON COLUMN 
ERES.ER_SUBMISSION_PROVISO.fk_codelst_provisotype IS 
'Stores FK to er_codelst, code_type=prov_type';

alter trigger ER_SUBMISSION_PROVISO_AU0 disable;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,74,8,'08_alter.sql',sysdate,'8.9.0 Build#531');

commit;