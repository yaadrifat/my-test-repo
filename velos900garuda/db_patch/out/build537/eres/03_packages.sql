set define off;
create or replace
PACKAGE BODY      "PKG_REPORT"
AS
 PROCEDURE Sp_GenReport (
   P_REPID          NUMBER,
   P_ACCID          NUMBER,
   P_PARAM         ARRAY_STRING,
   P_PARAMVAL ARRAY_STRING,
   P_SXML     OUT   CLOB
   )
IS

--   V_STR      VARCHAR2 (2000)DEFAULT P_PARAMS || ':';
   V_SQLSTR   LONG;
  -- V_PARAMS   VARCHAR2 (2000)DEFAULT P_PARAMS || ':';
   V_POS      NUMBER         := 0;
   V_CNT      NUMBER         := 0;
   V_SQLXML   CLOB;
   V_INS      VARCHAR2 (4000);
   V_CID      NUMBER;
   V_CID2      NUMBER;
   v_param VARCHAR2 (255);
   v_param_val CLOB;
   V_GENXML NUMBER;
   v_clobtemp CLOB;

BEGIN
/*
Resets any XML options set before
*/
   Xmlgen.RESETOPTIONS;
  -- V_CID := SUBSTR (V_STR, 1, INSTR (V_STR, ':') - 1);
  -- v_cid2 := v_cid ;
   -- For RepId 21va global temp table is populated and then the SQL executed on the temp table.
   IF P_REPID = 21 THEN
   DELETE FROM ER_TMPREP;
   V_INS :=
'insert into er_tmprep (
      EVENT_ID               , --1
 CHAIN_ID               , --2
 EVENT_TYPE             , -- 3
 NAME                   , --4
 NOTES                  , --5
 COST                   , --6
 COST_DESCRIPTION       , --7
 DURATION               , --8
 USER_ID                , --9
 LINKED_URI             , --10
 FUZZY_PERIOD           , --11
 MSG_TO                 , --12
 STATUS                 , -- 13
 DESCRIPTION            , --14
 DISPLACEMENT           , --15
 ORG_ID                 , --16
 EVENT_MSG              , --17
 EVENT_RES              , --18
 CREATED_ON             , --19
 EVENT_FLAG             , --20
 EVENTCOST_DESC         , --21
 EVENTCOST_CURR         , --22
 EVE_VALUE          , --23
 EVENTCOST_VALUE        , --24
 FK_EVENT               , --25
 SUM_EVENT              )   --26
      (
       SELECT
 a.EVENT_ID               ,
        a.CHAIN_ID               ,
        a.EVENT_TYPE             ,
        a.NAME                   ,
        a.NOTES                  , --5
        a.COST                   ,
        a.COST_DESCRIPTION       ,
        a.DURATION               ,
        a.USER_ID                ,
        a.LINKED_URI             , --10
        a.FUZZY_PERIOD           ,
        a.MSG_TO                 ,
        a.STATUS                 ,
        a.DESCRIPTION            ,
        a.DISPLACEMENT           , --15
        a.ORG_ID                 ,
        a.EVENT_MSG              ,
        a.EVENT_RES              ,
        a.CREATED_ON             ,
        a.EVENT_FLAG             , --20
        b.EVENTCOST_DESC ,
 b.EVENTCOST_CURR ,
 b.EVENTCOST_VALUE  EVE_VALUE         , --23
        trim(b.EVENTCOST_CURR ||'' '' ||  b.EVENTCOST_VALUE) EVENTCOST_VALUE , --24
        b.FK_EVENT , --25
        (SELECT      MIN(c.eventcost_curr) || '' '' || SUM(c.EVENTCOST_VALUE)
           FROM erv_evecost c
          WHERE c.fk_event = a.event_id   ) sum_event --25
  FROM erv_eveassoc a , erv_evecost b
 WHERE a.event_id = b.fk_event
   AND a.chain_id =  :1
   AND a.DISPLACEMENT > 0  )';
 EXECUTE IMMEDIATE V_INS
   USING V_CID;
 -- If the report is the protocol calender template
 ELSIF p_repid = 44 THEN
  v_ins := 'insert into er_tmprep
 ( event_id , chain_id , displacement , NAME , duration , fuzzy_period  )
   SELECT a.event_id ,
   a.chain_id ,
   a.displacement ,
   a.NAME ,
   a.duration ,
   a.FUZZY_PERIOD
     FROM erv_eveassoc a
    WHERE a.chain_id = :1 '  ;
-- DBMS_OUTPUT.PUT_LINE (substr(v_ins,-200)) ;
 EXECUTE IMMEDIATE V_INS
  USING v_cid ;
  END IF;
   -- Get the Report SQL from the ER_REPORT Table for the preport id p_repid
   SELECT REP_SQL_CLOB,NVL(GENERATE_XML,1)
     INTO V_SQLSTR,V_GENXML
     FROM ER_REPORT
    WHERE PK_REPORT = P_REPID;
   -- Parse the parameter string passed and separate the comma delimited paramters.
   -- Replace the variables in the SQL with the parameters
/*   LOOP
      EXIT WHEN V_PARAMS IS NULL;
      V_CNT := V_CNT + 1;
      V_POS := INSTR (V_STR, ':');
      V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
      V_SQLSTR := REPLACE (V_SQLSTR, '~' || V_CNT, V_PARAMS);
      V_STR := SUBSTR (V_STR, V_POS + 1);
    DBMS_OUTPUT.PUT_LINE (SUBSTR(v_sqlstr,1,200)) ;
      DBMS_OUTPUT.PUT_LINE (SUBSTR(v_sqlstr,-200)) ;
   END LOOP;*/
  /*  ******************************************************************************  /
    Go through the report_SQL ,find the keyword prefixed with ':' and replace them
 with values
  /*  ******************************************************************************  */



     FOR i IN 1 .. p_param.COUNT()
              LOOP
     v_param:=p_param(i);
      v_param_val:=p_paramval(i);

--  INSERT INTO T (c,created_on) VALUES(v_param || ' *** ' || v_param_val,SYSDATE);
--  COMMIT;

     IF (v_param_val='[ALL]') THEN
     -- check if any SQL is defined within the mapping,If not,use the generic one
      SELECT repfiltermap_valuesql INTO v_param_val FROM ER_REPFILTERMAP WHERE repfiltermap_repcat=
      (SELECT (SELECT codelst_custom_col FROM ER_CODELST WHERE codelst_subtyp=rep_type AND codelst_type='report') AS repcat  FROM ER_REPORT WHERE pk_report=p_repid)
      AND fk_repfilter=(SELECT pk_repfilter FROM ER_REPFILTER WHERE repfilter_keyword=v_param);
      --if not defined for mapping
      IF (NVL(dbms_lob.getLength(v_param_val),0)=0)  THEN

       SELECT repfilter_valuesql INTO v_param_val FROM ER_REPFILTER WHERE
       repfilter_keyword=v_param;


      plog.DEBUG(pCTX,'v_param_val2'||v_param_val);
      END IF;
      END IF;



     IF ( INSTR(v_sqlstr,':'||v_param) >0) THEN
     v_sqlstr:=REPLACE(v_sqlstr,':'||v_param,v_param_val);
     END IF;
  END LOOP;

 --Run the Loop again so that value inserted through repfilter_valuesql should be replaced
 -- with value. It's better to run the loop here than to keep running this loop withtin previous loop
 --to replace the values in valueSQL.Since oracle does not support searching a value in a collection
 --we will have to run loop for each value where '[ALL]' was selected
      FOR i IN 1 .. p_param.COUNT()
              LOOP
     v_param:=p_param(i);
      v_param_val:=p_paramval(i);

      --Replace any [ALL] values
       IF (v_param_val='[ALL]') THEN
     -- check if any SQL is defined within the mapping,If not,use the generic one
      SELECT repfiltermap_valuesql INTO v_param_val FROM ER_REPFILTERMAP WHERE repfiltermap_repcat=
      (SELECT (SELECT codelst_custom_col FROM ER_CODELST WHERE codelst_subtyp=rep_type AND codelst_type='report') AS repcat  FROM ER_REPORT WHERE pk_report=p_repid)
      AND fk_repfilter=(SELECT pk_repfilter FROM ER_REPFILTER WHERE repfilter_keyword=v_param);
      --if not defined for mapping
      IF (NVL(dbms_lob.getLength(v_param_val),0)=0)  THEN

       SELECT repfilter_valuesql INTO v_param_val FROM ER_REPFILTER WHERE
       repfilter_keyword=v_param;


      plog.DEBUG(pCTX,'v_param_val2'||v_param_val);
      END IF;
      END IF;

      --end Replacing

     IF ( INSTR(v_sqlstr,':'||v_param) >0) THEN
     v_sqlstr:=REPLACE(v_sqlstr,':'||v_param,v_param_val);
     END IF;
  END LOOP;

   --DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,1,255)) ;
   -- DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,245,255)) ;
   -- Execute the SQL created, pass it as a variable in the XMLGEN.getXML procedure
   -- This will return the XML for the SQL. Even if the SQL returns no rows the XML will be created


     IF V_GENXML = 1 THEN

  --This need to be fixed and f_doced should be enabled. Rajeev commented this line temporarily to run
  -- a client report.
--   v_sqlstr:=Pkg_Util.f_decode(v_sqlstr);
  v_sqlstr := REPLACE(v_sqlstr, '[VELSQUOTE]','''');



     P_SXML := dbms_Xmlgen.GETXML (V_SQLSTR);

--      P_SXML := REPLACE (P_SXML, '&amp;#', '&#');
--    P_SXML := REPLACE (P_SXML, '<?xml version = ''1.0''?>', '<?xml version = ''1.0''  encoding=''windows-1253''?>');


--     plog.fatal(pctx,'P_SXML' || P_SXML);
 ELSE

--  INSERT INTO T(c) VALUES(v_sqlstr);
 -- COMMIT;

--  plog.DEBUG(pctx,'V_SQLSTR' || V_SQLSTR);

EXECUTE IMMEDIATE V_SQLSTR INTO P_SXML;
 END IF;
     DBMS_OUTPUT.PUT_LINE ('XML ' || DBMS_LOB.SUBSTR (P_SXML, 200, 200));

END;

FUNCTION F_Gen_PatDump(p_study VARCHAR2,p_site VARCHAR2,p_patient VARCHAR2,p_txarm VARCHAR2,p_startdate VARCHAR2, p_enddate VARCHAR2)
RETURN  CLOB
AS
v_xml CLOB := EMPTY_CLOB();
v_studynum VARCHAR2(100);
v_studytitle VARCHAR2(4000);
v_site VARCHAR2(100);
v_count NUMBER;
v_temp VARCHAR2(30);
tab_study split_tbl;
tab_org split_tbl;
v_study VARCHAR2(2000);
v_org VARCHAR2(20);
v_counter NUMBER :=1;
v_perCode VARCHAR2(20);
v_perLName VARCHAR2(200);
v_perFName VARCHAR2(200);
v_initials VARCHAR2(10);
v_gender VARCHAR2(50);
v_enrollDt VARCHAR2(20);
v_patStudyId VARCHAR2(20);
v_race VARCHAR2(50);
v_ethnicity VARCHAR2(50);
v_deathDate VARCHAR2(20);
v_enrollPhysn VARCHAR2(200);
v_assndTo VARCHAR2(200);
v_sql VARCHAR2(4000);
v_cur Gk_Cv_Types.GenericCursorType;
v_statcur Gk_Cv_Types.GenericCursorType;
v_statSQL VARCHAR2(4000);
v_perPk NUMBER;
v_statDate VARCHAR2(20);
v_statReason VARCHAR2(4000);
v_statStr CLOB;
v_headerStr VARCHAR2(4000);
v_txarm VARCHAR2(100);
v_where VARCHAR2(4000);
v_add CHAR(1);
v_infVer VARCHAR2(250);
v_txPK NUMBER;
v_off_study_stat NUMBER;
v_off_treat_stat NUMBER;
v_off_study_count NUMBER;
v_off_treat_count NUMBER;
v_tx_desc 		  VARCHAR2(4000);
v_tempStr VARCHAR2(32000);
v_finalclob CLOB := EMPTY_CLOB();

 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'f_gen_patdump', pLEVEL  => Plog.LDEBUG);
BEGIN

	DBMS_LOB.CREATETEMPORARY(v_xml,TRUE);
	DBMS_LOB.CREATETEMPORARY(v_finalclob,TRUE);


--v_xml := v_xml || '<ROWSET>';
--Create header String
v_add:='Y';
   FOR k IN (SELECT pk_codelst,codelst_subtyp,codelst_desc FROM ER_CODELST WHERE codelst_type='patStatus' ORDER BY codelst_seq )
   LOOP
   IF (LENGTH(v_headerStr)>0)  THEN
   v_headerStr:=v_headerStr||'<name>'||k.codelst_desc||'</name>';
   ELSE
--   v_headerStr:='<'||k.codelst_subtyp||'>'||k.codelst_desc||'</'||k.codelst_subtyp||'>';
   v_headerStr:='<name>'||k.codelst_desc||'</name>';
   END IF;
   END LOOP;
 v_headerStr:='<HEADER>'||v_headerStr||'</HEADER>';
--End header

SELECT Pkg_Util.f_split(p_study) INTO tab_study FROM dual;
plog.DEBUG(pCTX,'v_xml2-'||v_xml);
SELECT Pkg_Util.f_split(p_site) INTO tab_org FROM dual;

 -- get off treatment code
 SELECT Pkg_Util.f_getcodepk ('offtreat', 'patStatus')
 INTO v_off_treat_stat FROM dual;

 -- get off study code
 SELECT Pkg_Util.f_getcodepk ('offstudy', 'patStatus')
 INTO v_off_study_stat FROM dual;

 ----------


 FOR i IN (SELECT COLUMN_VALUE AS study FROM TABLE(tab_study))
 LOOP
 v_study:=i.study;
 plog.DEBUG(pCTX,'study-'||v_study);

  -- get offtreatment count
 SELECT COUNT(DISTINCT fk_per)
 INTO v_off_treat_count
 FROM ER_PATPROT pp, ER_PER WHERE fk_study = v_study AND
   pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
 AND  patprot_stat=1
 AND  ( (patprot_enroldt BETWEEN  TO_DATE(p_startdate,PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE(p_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT)) OR patprot_enroldt IS NULL) AND
 EXISTS (SELECT * FROM ER_PATSTUDYSTAT P WHERE P.fk_per = pp.fk_per AND P.fk_study = pp.fk_study
 AND fk_codelst_stat = v_off_treat_stat);

  -- get off study count  Select count(distinct fk_per)
 SELECT COUNT(DISTINCT fk_per)
 INTO v_off_study_count
 FROM ER_PATPROT pp, ER_PER WHERE fk_study = v_study
 AND  pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
 AND  patprot_stat=1
 AND  ( (patprot_enroldt BETWEEN  TO_DATE(p_startdate,PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE(p_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT)) OR patprot_enroldt IS NULL) AND
 EXISTS (SELECT * FROM ER_PATSTUDYSTAT P WHERE P.fk_per = pp.fk_per AND P.fk_study = pp.fk_study
 AND fk_codelst_stat = v_off_study_stat);



 -----------------------
 FOR j IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
 LOOP
 v_org:=j.org;
 plog.DEBUG(pCTX,'site-'||v_org);
SELECT study_number, study_title INTO v_studynum, v_studytitle FROM ER_STUDY WHERE pk_study  = v_study;

-- Bug fix for #3526 SM 06/11/08
SELECT replace(site_name, '&', '&amp;') INTO v_site FROM ER_SITE WHERE pk_site =  v_org;

plog.DEBUG(pCTX,'TXARM'||p_txarm||'date'||p_startdate||'date1'||p_enddate);

v_sql:= ' select * from (SELECT  fk_per,person_code AS patient_id,patprot_patstdid AS patient_study_id,person_lname as patient_lname , person_fname AS patient_fname,SUBSTR(Person_fname,1,1) || SUBSTR(person_mname,1,1) || SUBSTR(person_lname,1,1) as initials,f_get_codelstdesc(fk_codelst_gender) as gender,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity,
TO_CHAR(person_deathdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS death_date,(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = patprot_physician) AS enroll_physn,(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = fk_userassto) AS usr_assdto,TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS enrolled_on ,'
|| '(SELECT tx_name FROM ER_STUDYTXARM, ER_PATTXARM WHERE pk_studytxarm = fk_studytxarm AND  fk_patprot in (select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') AND tx_start_date = (SELECT MIN(tx_start_date) FROM ER_PATTXARM WHERE fk_patprot  in  ( select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') ) AND ROWNUM = 1) AS txarm,'
|| '(SELECT fk_studytxarm FROM ER_STUDYTXARM, ER_PATTXARM WHERE pk_studytxarm = fk_studytxarm AND fk_patprot in (select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') AND tx_start_date = (SELECT MIN(tx_start_date) FROM ER_PATTXARM WHERE fk_patprot in (select pk_patprot from er_patprot where fk_per = pk_person and fk_study='||v_study||') ) AND ROWNUM = 1) AS fk_studytxarm'
||'  FROM ER_PATPROT ,epat.PERSON  WHERE fk_study='||v_study
|| '  AND pk_person=fk_per AND  fk_site='||v_org || '  AND  patprot_stat=1 and  (patprot_enroldt BETWEEN  to_date('''||p_startdate||''',PKG_DATEUTIL.F_GET_DATEFORMAT)  AND to_date(''' ||p_enddate||''',PKG_DATEUTIL.F_GET_DATEFORMAT) or patprot_enroldt is null))';

--Put txarm filter
v_where:='';

/*
IF (LENGTH(p_txarm) >0) THEN
   IF (LENGTH(v_where)>0) THEN
   v_where:=v_where|| '  and fk_studytxarm  in ('||p_txarm||')';
--      v_where:=v_where|| '  and (fk_studytxarm is null or fk_studytxarm  in ('||p_txarm||'))';
 ELSE
 v_where:='fk_studytxarm  in ('||p_txarm||')';
--   v_where:='fk_studytxarm is null or fk_studytxarm  in ('||p_txarm||')';
 END IF;
END IF;
*/

plog.DEBUG(pCTX,'v_where'||v_where||'v_Add'||v_Add);
--IF ((LENGTH(v_where)>0) AND (v_add='Y') ) THEN
IF ((LENGTH(v_where)>0) ) THEN
v_sql:=v_sql||' where  '||v_where;
--v_add:='N';
END IF;
v_sql:=v_sql|| '  order by enrolled_on desc';

plog.DEBUG(pCTX,'SQL'||v_sql);
OPEN v_cur FOR v_sql;
 LOOP
 FETCH v_cur INTO v_perPk,v_perCode,v_patStudyId,v_perLName,v_perFName,v_initials,v_gender,v_race,v_ethnicity,v_deathDate,v_enrollPhysn,v_assndTo,v_enrollDt,v_txarm,v_txPk;
 EXIT WHEN v_cur %NOTFOUND;
--   v_xml := v_xml || '<ROW num="'||v_counter||'">';

 -- get TX description

 --	v_tempclob :=  empty_clob();
v_tempStr := '';

 	IF (NVL(v_txPk,0) > 0) THEN
	 	SELECT  NVL(tx_desc,'&#xa0;' ) INTO v_tx_desc FROM ER_STUDYTXARM WHERE pk_studytxarm = v_txPk;
	ELSE
		v_tx_desc  := '&#xa0;';
	END IF;

v_tempStr:= v_tempStr || '<ROW num="'||TO_CHAR(v_counter)||'">';
   v_tempStr := v_tempStr  ||'<STUDY_NUM>' || v_studynum ||  '</STUDY_NUM><OFFTREAT_CT>'||v_off_treat_count ||'</OFFTREAT_CT><OFFSTUDY_CT>'||v_off_study_count ||'</OFFSTUDY_CT><SITE>' ||v_site || '</SITE><PATIENT_ID>'||v_perCode||'</PATIENT_ID><PATIENT_STUDY_ID>'||v_patStudyId||'</PATIENT_STUDY_ID>';
  v_tempStr :=v_tempStr ||'<PATIENT_LNAME>'|| NVL(v_perLName, '&#xa0;') ||'</PATIENT_LNAME><PATIENT_FNAME>'|| NVL(v_perFName,'&#xa0;') ||'</PATIENT_FNAME><INITIALS>'|| NVL(v_initials, '&#xa0;') ||'</INITIALS><GENDER>'|| NVL(v_gender, '&#xa0;') ||'</GENDER><TXARM>'||NVL(v_txarm,'&#xa0;')||'</TXARM><TXDESC>'||  v_tx_desc  || '</TXDESC><RACE>'||NVL(v_race,'&#xa0;') ||'</RACE><ETHNICITY>'|| NVL(v_ethnicity,'&#xa0;')||'</ETHNICITY><DEATH_DATE>'|| NVL(v_deathDate, '&#xa0;') ||'</DEATH_DATE><ENROLL_PHYSN>'|| NVL(v_enrollPhysn, '&#xa0;') ||'</ENROLL_PHYSN><USR_ASSDTO>'|| NVL(v_assndTo, '&#xa0;') ||'</USR_ASSDTO>';
   v_counter:=v_counter+1;

   FOR i IN (SELECT pk_codelst,codelst_subtyp,codelst_desc FROM ER_CODELST WHERE codelst_type='patStatus'  ORDER BY codelst_seq)
   LOOP
      v_statStr:='';
   v_statSQL:= 'SELECT  to_char(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) as statDate , (select codelst_desc from er_codelst where pk_codelst=patstudystat_reason) as reason, inform_consent_ver  FROM ER_PATSTUDYSTAT WHERE fk_per='||v_perPk||' and fk_study='||v_study|| ' and fk_codelst_stat='||i.pk_codelst||'  order by patstudystat_date desc'   ;
--   OPEN v_statcur FOR v_statSQL;
   FOR x IN (SELECT  TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS statDate , (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst=patstudystat_reason) AS reason, inform_consent_ver  FROM ER_PATSTUDYSTAT WHERE fk_per=v_perPk AND fk_study=v_study AND fk_codelst_stat= i.pk_codelst  ORDER BY patstudystat_date DESC)
   LOOP
--   FETCH v_statcur INTO  v_statDate,v_statReason, v_infVer;
--    EXIT WHEN v_statcur %NOTFOUND;
v_statDate:=x.statDate;
 IF (i.codelst_subtyp='infConsent') THEN
 IF (LENGTH(v_statStr)>0) THEN
-- v_statStr:=v_statStr||' , '||CHR(13)||v_statDate;
 v_statStr:=v_statStr||' , '||CHR(13)|| x.statDate;
 ELSE
-- v_statStr:=v_statDate||' Ver. '||v_infVer||'';
 v_statStr:=v_statDate||' Ver. '|| x.inform_consent_ver||'';
 END IF;
 ELSE
 IF (LENGTH(v_statStr)>0) THEN
-- v_statStr:=v_statStr||' , '||CHR(13)||v_statDate||' '||v_statReason;
 v_statStr:=v_statStr||' , '||CHR(13)||v_statDate||' '||x.reason;
 ELSE
-- v_statStr:=v_statDate||' '||v_statReason;
 v_statStr:=v_statDate||' '||x.reason;
 END IF;
END IF;
   END LOOP; --end loop for status SQL

 v_tempStr  := v_tempStr  ||'<data>'|| NVL(v_statStr,'&#xa0;') ||'</data>';

  END LOOP; -- loop for codelst types

  v_tempStr  :=  v_tempStr || '</ROW>';

   dbms_lob.writeappend( v_xml, LENGTH(v_tempStr), v_tempStr );

 --  plog.DEBUG(pCTX,'v_xml-final'||v_xml);


 END LOOP;

END LOOP;

END LOOP;



   --v_xml :='<ROWSET>' ||v_headerStr|| v_xml || '</ROWSET>';

   dbms_lob.WRITEAPPEND (  v_finalclob, 8, '<ROWSET>' );
   dbms_lob.WRITEAPPEND (  v_finalclob, LENGTH(v_headerStr), v_headerStr );
   dbms_lob.APPEND (  v_finalclob, v_xml);
    dbms_lob.WRITEAPPEND (  v_finalclob, 9, '</ROWSET>' );


  -- plog.DEBUG(pCTX,'v_xml-final2'||v_finalclob);
--    plog.DEBUG(pCTX,'final-'||v_xml);



  RETURN v_finalclob;
END;

FUNCTION F_Gen_UsrProfile(p_user VARCHAR2)
RETURN  CLOB
AS
v_userclob CLOB ;
v_group CLOB ;
v_site CLOB ;
v_username VARCHAR2(200);
v_experience VARCHAR2(4000);
v_phases VARCHAR2(200);
v_jobtype VARCHAR2(4000);
v_special VARCHAR2(4000);
v_address VARCHAR2(4000);
v_city VARCHAR2(200);
v_state VARCHAR2(200);
v_zip VARCHAR2(50);
v_country VARCHAR2(200);
v_phone VARCHAR2(200);
v_email VARCHAR2(4000);
v_groupname VARCHAR2(200);
v_groupdft VARCHAR2(50);
v_sitename VARCHAR2(200);
v_primsite VARCHAR2(50);
v_study CLOB;
v_studypk NUMBER;
v_studynum VARCHAR2(200);
v_studytitle VARCHAR2(4000);
v_teamrole VARCHAR2(200);
v_orgname CLOB;
v_org CLOB;
v_notify CLOB;
v_ntype CLOB;
v_nstudy CLOB;
v_npatient CLOB;
v_nform CLOB;
v_ncalendar CLOB;
v_nvisit CLOB;
v_ndesc CLOB;

v_primary_site NUMBER;
v_user VARCHAR2(200);
--v_finalclob CLOB := EMPTY_CLOB();
v_index_comma NUMBER;
v_account Number;
BEGIN
--temporary fix so that if multiple users are passed, the report does not crash
v_user  := p_user;
v_index_comma  := INSTR(v_user,',');

IF  v_index_comma  > 0 THEN
	v_user  := SUBSTR(v_user,1, (v_index_comma -1)  );
END IF  ;


--collect user data
SELECT usr_firstname || ' ' || usr_lastname,usr_wrkexp,usr_pahseinv,F_Get_Codelstdesc(fk_codelst_jobtype),F_Get_Codelstdesc(fk_codelst_spl),address,add_city,
       add_state,add_zipcode,add_country,add_phone,add_email,fk_siteid,ER_USER.fk_account
INTO v_username,v_experience,v_phases,v_jobtype,v_special,v_address,v_city,v_state,v_zip,v_country,v_phone,v_email,v_primary_site,v_account
FROM ER_USER,ER_ADD
WHERE pk_user = v_user AND pk_add = fk_peradd ;

	  --escape spl chars
	    v_username := Pkg_Util.f_escapespecialcharsforxml (v_username);
	    v_experience := Pkg_Util.f_escapespecialcharsforxml (v_experience);
	    v_address := Pkg_Util.f_escapespecialcharsforxml (v_address);
	    v_email := Pkg_Util.f_escapespecialcharsforxml (v_email);

--open <user> and add user data as attributes
v_userclob := v_userclob || '<user name="' || v_username || '" experience="' || v_experience || '" phases="' || v_phases || '" jobtype="' || v_jobtype || '" special="' || v_special || '" Address="' || v_address || '" city="' || v_city || '" state="' || v_state || '" zip="' || v_zip || '" country="' || v_country || '" phone="' || v_phone || '" email="' || v_email || '"> ' ;

--open <groups>
v_group := v_group || '<groups>' ;

--collect group data
FOR i IN (SELECT grp_name,CASE WHEN (SELECT fk_grp_default FROM ER_USER WHERE fk_user = pk_user)= fk_grp THEN 'Yes' ELSE 'No' END AS grpdef
          FROM ER_USRGRP,ER_GRPS WHERE fk_user = v_user AND fk_grp = pk_grp ORDER BY  1 )
   LOOP
    v_groupname := i.grp_name;

    v_groupdft := i.grpdef;

   v_groupname := Pkg_Util.f_escapespecialcharsforxml (v_groupname);

      --open <group> and add group data as attributes
      v_group := v_group || '<group name="' || v_groupname || '" default="' || v_groupdft || '"></group>'; --close <group>
   END LOOP;

--close <groups>
v_group := v_group || '</groups>';

--open <sites>
v_site := v_site || '<sites>' ;

--collect site data
-- Bug fix for #3015-1 By SM 06/18/08
FOR i IN (SELECT distinct site_name,CASE WHEN (v_primary_site = pk_site) THEN 'Yes' ELSE 'No' END AS sitedef
      FROM ER_STUDY_SITE_RIGHTS,ER_SITE WHERE fk_user = v_user AND fk_site = pk_site AND user_study_site_rights > 0)
    LOOP

   v_sitename := i.site_name;

    v_sitename := Pkg_Util.f_escapespecialcharsforxml (v_sitename);

   v_primsite := i.sitedef;

      --open <site> and add site data as attributes
      v_site:= v_site || '<site name="' || v_sitename || '" primary="' ||  v_primsite || '"/>'; --close <site>
   END LOOP;

--close <sites>
v_site := v_site || '</sites>';

--open <studies>
v_study := v_study || '<studies>';

--collect study data
FOR i IN (SELECT pk_study,study_number,study_title,F_Get_Codelstdesc(fk_codelst_tmrole) strole
          FROM ER_STUDYTEAM,ER_STUDY WHERE fk_account = v_account and pk_study = fk_study AND fk_user = v_user and study_team_usr_type <> 'X'
          union
          SELECT pk_study,study_number,study_title,'Super User'
          from ER_STUDY WHERE fk_account = v_account  and  Pkg_Superuser.F_Is_Superuser(v_user, pk_study) = 1
          ORDER BY  2)
   LOOP

   v_studypk := i.pk_study;
   v_studynum := i.study_number;
   v_studytitle := i.study_title;
   v_teamrole := i.strole;

   v_studytitle := Pkg_Util.f_escapespecialcharsforxml (v_studytitle);
   v_studynum := Pkg_Util.f_escapespecialcharsforxml ( v_studynum);
   v_teamrole := Pkg_Util.f_escapespecialcharsforxml ( v_teamrole);

      --open <study> and add study data as attributes
      v_study:= v_study || '<study number="' || v_studynum || '" title="' || v_studytitle || '" role="' || v_teamrole || '" org="';

      v_orgname := '';

      -- Bug fix for #3015-2 By SM 05/29/08
      if v_teamrole = 'Super User' then
        v_orgname := 'All the Organizations user has access as per User Details page';
      else
        -- Bug fix for #3015-4 By SM 06/18/08
        --collect Site Org Name
        FOR j IN (SELECT distinct site_name,fk_study FROM ER_STUDY_SITE_RIGHTS,ER_SITE WHERE fk_user = v_user AND fk_study = v_studypk AND pk_site = fk_site AND user_study_site_rights > 0 ORDER BY  1)
          LOOP

          v_orgname := v_orgname ||  Pkg_Util.f_escapespecialcharsforxml (j.site_name) || ', ';

          END LOOP;
      end if;

      v_org := trim(TRAILING ',' FROM (trim(TRAILING ' ' FROM v_orgname)));

      --open <org> and add org names
       v_study:= v_study || v_org || '"></study>'; --close <org> --close <study>

      END LOOP;

--close <studies>
v_study:= v_study || '</studies>';

--open <notifications>
v_notify := v_notify || '<notifications>' ;

--collect notification information
FOR i IN (
				SELECT TYPE,study_number   study,
	       NULL AS  patient,(SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_crf) FORM,
	       (SELECT NAME FROM event_assoc WHERE event_id = fk_cal) calendar,(SELECT visit_name FROM sch_protocol_visit WHERE pk_protocol_visit = fk_visit) visit,description
	FROM (
	  SELECT alnot_users users,'Alert' TYPE,fk_study,fk_patprot,fk_protocol fk_cal,NULL fk_crf,NULL fk_visit,
	         (SELECT codelst_desc FROM sch_codelst WHERE pk_codelst = fk_codelst_an) description FROM sch_alertnotify WHERE fk_patprot IS NULL AND alnot_users IS NOT NULL AND   ',' || alnot_users || ',' LIKE '%,' || v_user || ',%'
	  UNION
		    SELECT fn_users,'Form',fk_study,NULL,NULL,fn.fk_formlib,NULL,
	         DECODE(fn_sendtype,'F' ,'First time form is filled' , 'Every time form is filled')
	  FROM ER_FORMNOTIFY fn,ER_LINKEDFORMS sf WHERE fn.fk_formlib = sf.fk_formlib AND  fn_msgtype = 'N'
	  AND fn_users IS NOT NULL AND   ',' || fn_users || ',' LIKE '%,' || v_user || ',%'
	  UNION
	  SELECT milestone_usersto,'Milesone',fk_study,NULL,fk_cal,NULL,NULL,Pkg_Milestone_New.f_getmilestonedesc(pk_milestone) FROM ER_MILESTONE
	  WHERE milestone_usersto IS NOT NULL AND ',' || milestone_usersto || ',' LIKE '%,' || v_user || ',%'
	  UNION
	  SELECT TO_CHAR(eventusr),'Calendar',CASE WHEN event_type = 'P' THEN chain_id ELSE (SELECT ea.chain_id FROM event_assoc ea WHERE e.chain_id = ea.event_id) END,NULL,
	         CASE WHEN event_type = 'P' THEN event_id ELSE (SELECT ea.event_id FROM event_assoc ea WHERE e.chain_id = ea.event_id) END,NULL,fk_visit,
	         CASE WHEN usr_daysbefore IS NULL THEN '' WHEN usr_daysbefore = 0 THEN '' ELSE 'Message sent to user ' || usr_daysbefore || ' days before event: ' || NAME END ||
	         '   ' || CASE WHEN usr_daysafter IS NULL THEN '' WHEN usr_daysafter = 0 THEN '' ELSE 'Message sent to user ' || usr_daysafter || ' days after event: ' || NAME END descript
	  FROM event_assoc e RIGHT JOIN sch_eventusr ON fk_event = event_id WHERE eventusr_type = 'S'
	  AND ',' || eventusr || ',' LIKE '%,' || v_user || ',%'
	  AND (usr_daysbefore IS NOT NULL OR usr_daysafter IS NOT NULL)) a
	LEFT JOIN ER_STUDY ON pk_study = fk_study
ORDER BY TYPE
)
  LOOP
    v_ntype := i.TYPE;
    v_nstudy := CASE WHEN i.study IS NULL THEN NULL ELSE 'Study=' || i.study END;
    v_npatient := CASE WHEN i.patient IS NULL THEN NULL ELSE 'Patient=' || i.patient END;
    v_nform := CASE WHEN i.FORM IS NULL THEN NULL ELSE 'Form=' || i.FORM END;
    v_ncalendar := CASE WHEN i.calendar IS NULL THEN NULL ELSE 'Calendar=' || i.calendar END;
    v_nvisit := CASE WHEN i.visit IS NULL THEN NULL ELSE 'Visit=' || i.visit END;
    v_ndesc := i.description;

	    v_nstudy  := Pkg_Util.f_escapespecialcharsforxml (v_nstudy ) ;
	    v_npatient   := Pkg_Util.f_escapespecialcharsforxml (v_npatient  ) ;
       v_nform    := Pkg_Util.f_escapespecialcharsforxml (   v_nform  ) ;
       v_ncalendar    := Pkg_Util.f_escapespecialcharsforxml (   v_ncalendar ) ;
	    v_nvisit    := Pkg_Util.f_escapespecialcharsforxml (    v_nvisit  ) ;
		 v_ndesc    := Pkg_Util.f_escapespecialcharsforxml (   v_ndesc ) ;

    --open <notify> and add attributes
    v_notify := v_notify || '<notify type="' || v_ntype || '" link="' || v_nstudy || ' ' || v_npatient || ' ' || v_nform || ' ' || v_ncalendar || ' ' || v_nvisit;
    v_notify := v_notify || '" Description="' || v_ndesc || '"/>';

  END LOOP;

  --close <notifications>
  v_notify := v_notify || '</notifications>';

--add <groups>, <sites> and <studies> and close <user>

--v_user := v_user ||     '</user>';

v_userclob := v_userclob || v_group || v_site || v_study || v_notify || '</user>';



  RETURN v_userclob;

END;

END Pkg_Report;
/



set define off;
create or replace
PACKAGE BODY      "PKG_MILESTONE_NEW" AS
   /*
Date : 10/26/05
Author: Sonia Abrol
Purpose: new milestone procedures for December 05 enhancements
*/ PROCEDURE sp_transfer_ach_pm AS
   /* Date : 10/26/05
    Author: Sonia Abrol
    Purpose - will run on scheduled intervals to calculate achieved milestones and transfer records to er_mileachieved. Will also transfer milestone notifications
 */ v_stdate DATE;
  v_milestones_already_achieved NUMBER := 0;
  v_milestones_count NUMBER := 0;
  v_milestone_remaining NUMBER := 0;
  v_limit NUMBER := 0;
  v_counter NUMBER := 0;
  v_milestone_percount NUMBER := 0;
  v_milestones_achieved_rows NUMBER := 0;
  v_msg VARCHAR2(4000);
  v_fparam VARCHAR2(32000);
  v_studynumber VARCHAR2(1000);
  v_pm_msgtemp VARCHAR2(4000);
  v_pm_users VARCHAR2(500);
  v_exp_rows NUMBER;
  v_row_count NUMBER;
  v_patcode_string VARCHAR2(32000);
  BEGIN
    v_pm_msgtemp := pkg_common.sch_getmailmsg('c_pm');
    -- get all thepatient milestones defined
    -- KM-10May2010-#D-FIN7
    FOR i IN
      (SELECT pk_milestone,
         ER_MILESTONE.fk_study,
         milestone_count,
         milestone_usersto,
         study_number,
         study_actualdt,
         NVL(milestone_limit,    0) milestone_limit,
         milestone_status,
         NVL(last_checked_on,    study_actualdt) last_checked_on,
         NVL(milestone_achievedcount,    0) milestone_achievedcount,
         f_getmilestonedesc(pk_milestone) miledesc
       FROM ER_MILESTONE,
         ER_STUDY
       WHERE milestone_delflag <> 'Y'
       AND fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='A')
       AND(last_checked_on < SYSDATE OR last_checked_on IS NULL)
       AND TRIM(milestone_type) = 'PM'
       AND ER_MILESTONE.fk_study = pk_study
       AND(NVL(milestone_achievedcount,    0) < milestone_limit OR milestone_limit IS NULL))
    LOOP
      -- loop 1
      SELECT COUNT(*)
      INTO v_milestones_achieved_rows
      FROM ER_MILEACHIEVED
      WHERE fk_milestone = i.pk_milestone;
      v_milestones_already_achieved := i.milestone_achievedcount;
      v_milestone_percount := i.milestone_count;
      v_limit := i.milestone_limit;
      v_pm_users := i.milestone_usersto;
      v_studynumber := i.study_number;
      IF v_limit > 0 THEN
        v_milestone_remaining := v_limit -v_milestones_already_achieved;
      END IF;
      v_stdate := i.last_checked_on;
      v_counter := 0;
/*AND NOT EXISTS
          (SELECT *
           FROM ER_MILEACHIEVED m
           WHERE m.fk_milestone = i.pk_milestone
           AND m.fk_per = ev.fk_per
           AND m.fk_study = ev.fk_study)*/
      FOR P IN
        (SELECT patstudystat_date ,
           fk_per,
           Pkg_Patient.f_get_patcodes(fk_per) patcodes
         FROM ER_PATSTUDYSTAT ev
         WHERE ev.fk_study = i.fk_study
         AND fk_codelst_stat = i.milestone_status
         AND(ev.created_on >= v_stdate OR v_stdate IS NULL)
          ORDER BY patstudystat_date)
      LOOP
        IF(v_limit > 0
         AND v_milestone_percount <= 0) THEN
          IF v_counter = v_milestone_remaining THEN
            EXIT;
          END IF;
        END IF;
        INSERT
        INTO ER_MILEACHIEVED(pk_mileachieved,   fk_milestone,   fk_per,   fk_study,   ach_date,   TABLE_NAME,   created_on,   is_complete)
        VALUES(seq_er_mileachieved.NEXTVAL,   i.pk_milestone,   P.fk_per,   i.fk_study,   P.patstudystat_date,   'er_patstudystat',   SYSDATE,   0);
        /* IF ( LENGTH(trim(NVL(v_pm_users,'')) ) > 0 )  THEN
                                             v_fparam :=  v_studynumber || '~Patient Status~'|| i.miledesc ||'~'|| TO_CHAR(P.PATSTUDYSTAT_DATE) ||'~'|| P.patcodes ;
                                              v_msg :=    PKG_COMMON.SCH_GETMAIL(V_PM_MSGTEMP ,v_fparam);
                                             -- call SP_TRANMAIL to Insert a record in SCH_MSGTRAN for notification for every user
                                              Pkg_Milnot.SP_TRANMAIL(v_msg,v_pm_users,NULL);
                                    END IF; */
        IF(v_milestone_percount > 0
         AND v_limit > 0) THEN
          v_milestones_achieved_rows := v_milestones_achieved_rows + 1;
          IF FLOOR(v_milestones_achieved_rows / v_milestone_percount) = v_limit THEN
            EXIT;
          END IF;
        END IF;
        v_counter := v_counter + 1;
      END LOOP;
      -- for milestone patients/details
      IF(v_milestone_percount = 0 OR v_milestone_percount = -1) THEN
        v_milestones_count := v_milestones_already_achieved + v_counter;
        -- expected milestone rows:
        v_exp_rows := v_counter;
      ELSE
        SELECT COUNT(*)
        INTO v_milestones_already_achieved
        FROM ER_MILEACHIEVED
        WHERE fk_milestone = i.pk_milestone;
        SELECT FLOOR(v_milestones_already_achieved / v_milestone_percount)
        INTO v_milestones_count
        FROM dual;
        v_exp_rows :=(v_milestones_count -i.milestone_achievedcount) * v_milestone_percount;
      END IF;
      UPDATE ER_MILESTONE
      SET milestone_achievedcount = v_milestones_count,
        last_checked_on = SYSDATE
      WHERE pk_milestone = i.pk_milestone;
      -- for updating the complete flag of completed milestones
      v_exp_rows := v_exp_rows + 1;
      -- create notifications for each achieved milestone
      IF v_milestone_percount <= 0 THEN
        v_milestone_percount := 1;
      END IF;
      --plog.DEBUG(pctx, 'v_pm_users' || v_pm_users);
      IF(LENGTH(TRIM(NVL(v_pm_users,   ''))) > 0) THEN
        v_row_count := 1;
        -- start with first row
        --                         plog.DEBUG(pctx, 'i have users');
        FOR ct IN
          (SELECT pk_mileachieved,
             ach_date,
             fk_per
           FROM ER_MILEACHIEVED
           WHERE fk_milestone = i.pk_milestone
           AND ROWNUM < v_exp_rows
           AND NVL(is_complete,    0) = 0
           ORDER BY pk_mileachieved ASC)
        LOOP
          v_patcode_string := v_patcode_string || CHR(13) || ' Patient ID: ' || Pkg_Patient.f_get_patcodes(ct.fk_per);
          plog.DEBUG(pctx,   'v_row_count' || v_row_count || 'v_milestone_percount ' || v_milestone_percount);
          IF v_row_count = v_milestone_percount THEN
            --send notification
            IF LENGTH(v_patcode_string) > 3800 THEN
              v_patcode_string := SUBSTR(v_patcode_string,   1,   3800);
            END IF;
            v_fparam := v_studynumber || '~Patient Status~' || i.miledesc || '~' || TO_CHAR(ct.ach_date) || '~' || v_patcode_string;
            v_msg := pkg_common.sch_getmail(v_pm_msgtemp,   v_fparam);
            -- call SP_TRANMAIL to Insert a record in SCH_MSGTRAN for notification for every user
            Pkg_Milnot.sp_tranmail(v_msg,   v_pm_users,   NULL);
            plog.DEBUG(pctx,   'got a milestone');
            --reset flag
            v_row_count := 1;
            v_patcode_string := '';
            ELSIF v_row_count < v_milestone_percount THEN
              --send notification
              plog.DEBUG(pctx,   'not yet');
              v_row_count := v_row_count + 1;
            END IF;
          END LOOP;
        END IF;
        -- for v_pm_users
        UPDATE ER_MILEACHIEVED
        SET is_complete = 1
        WHERE NVL(is_complete,   0) = 0
         AND fk_milestone = i.pk_milestone
         AND pk_mileachieved IN
          (SELECT pk_mileachieved
           FROM ER_MILEACHIEVED
           WHERE fk_milestone = i.pk_milestone
           AND ROWNUM < v_exp_rows
           AND NVL(is_complete,    0) = 0)
        ;
        COMMIT;
      END LOOP;
    END sp_transfer_ach_pm;
    PROCEDURE sp_transfer_ach_vmem AS
     /* Date : 10/27/05
    Author: Sonia Abrol
    Purpose - will run on scheduled intervals to calculate achieved Visit milestones and transfer records to er_mileachieved
 */
     v_stdate DATE;
    v_milestones_already_achieved NUMBER := 0;
    v_milestones_count NUMBER := 0;
    v_milestone_remaining NUMBER := 0;
    v_limit NUMBER := 0;
    v_counter NUMBER := 0;
    v_milestone_percount NUMBER := 0;
    v_milestones_achieved_rows NUMBER := 0;
    v_sql LONG;
    v_cur Types.cursortype;
    v_date DATE;
    v_patprot NUMBER;
    v_per NUMBER;
    v_all_ev_count NUMBER := 0;
    v_msg VARCHAR2(4000);
    v_fparam VARCHAR2(32000); --KM-17May10
    v_studynumber VARCHAR2(1000);
    v_m_msgtemp VARCHAR2(4000);
    v_m_users VARCHAR2(500);
    v_miletype VARCHAR2(10);
    v_exp_rows NUMBER;
    v_row_count NUMBER;
    v_patcode_string VARCHAR2(32000);
    v_cal_type VARCHAR2(2);
    v_study NUMBER;
    v_stdate_formatted VARCHAR(100);
    BEGIN
      v_m_msgtemp := pkg_common.sch_getmailmsg('c_pm');
      -- get all thepatient milestones defined
      FOR i IN
        (SELECT pk_milestone,
           ER_MILESTONE.fk_study,
           milestone_count,
           milestone_usersto,
           study_number,
           study_actualdt,
           NVL(milestone_limit,    0) milestone_limit,
           NVL(milestone_status,    0) milestone_status,
           NVL(last_checked_on,    study_actualdt) last_checked_on,
           NVL(milestone_achievedcount,    0) milestone_achievedcount,
           codelst_subtyp,
           NVL(milestone_eventstatus,    0) milestone_eventstatus,
           fk_cal,
           fk_visit,
           fk_eventassoc,
           f_getmilestonedesc(pk_milestone) miledesc,
           milestone_type
         FROM ER_MILESTONE,
           ER_STUDY,
           ER_CODELST
         WHERE milestone_delflag <> 'Y'
         AND fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='A')
         AND(last_checked_on < SYSDATE OR last_checked_on IS NULL)
         AND TRIM(milestone_type) IN('VM',    'EM')
         AND ER_MILESTONE.fk_study = pk_study
         AND(NVL(milestone_achievedcount,    0) < milestone_limit OR milestone_limit IS NULL)
         AND pk_codelst = fk_codelst_rule
         AND TRIM(codelst_type) IN('VM',    'EM'))
      LOOP
        -- loop 1
        SELECT COUNT(*)
        INTO v_milestones_achieved_rows
        FROM ER_MILEACHIEVED
        WHERE fk_milestone = i.pk_milestone;
        --get cal linking type
        SELECT NVL(event_calassocto,'P')
        INTO v_cal_type
        FROM event_assoc
        WHERE event_id = i.fk_cal AND event_type = 'P';
        v_milestones_already_achieved := i.milestone_achievedcount;
        v_milestone_percount := i.milestone_count;
        v_limit := i.milestone_limit;
        IF(i.milestone_type = 'VM') THEN
          v_miletype := 'Visit';
        ELSE
          v_miletype := 'Event';
        END IF;
        IF v_limit > 0 THEN
          v_milestone_remaining := v_limit -v_milestones_already_achieved;
        END IF;
        v_m_users := i.milestone_usersto;
        v_studynumber := i.study_number;
        v_stdate := i.last_checked_on;
        v_stdate_formatted  := TO_CHAR(i.last_checked_on,PKG_DATEUTIL.F_GET_DATETimeFORMAT);
        v_counter := 0;
        -- define sql for each rule type:
        IF(i.codelst_subtyp = 'vm_3') THEN
          -- atleast one event in the visit are marked 'with the given status', get the minimum date
         IF (v_cal_type = 'P')     THEN
              v_sql := ' SELECT MIN (EVENT_EXEON) EVENT_EXEON, PK_PATPROT, FK_PER   FROM ERV_PATSTUDY_EVEALL A
                                    WHERE FK_STUDY =' || i.fk_study || ' AND      FK_PROTOCOL = ' || i.fk_cal || ' AND event_status_id = ' || i.milestone_eventstatus;
         ELSE
             ------------here status is checked bcoz in admin schedule there is no way to separate old and new schedules besided status
              v_sql := ' SELECT  min(EVENT_EXEON),  FK_STUDY  FROM sch_events1 ,sch_eventstat a
                                    WHERE FK_STUDY =' || i.fk_study || ' AND      session_id = ' || i.fk_cal ||
                                    ' and status = 0 and EVENT_ID = a.FK_EVENT AND a.EVENTSTAT = ' || i.milestone_eventstatus;
         END IF;
          IF(i.fk_visit > 0) THEN
            -- A visit was selected
            v_sql := v_sql || ' and fk_visit = ' || i.fk_visit;
          END IF;
          v_sql := v_sql || ' AND  ( a.created_on  >= to_date(''' || v_stdate || ''') OR  a.last_modified_date >= to_date(''' || v_stdate || ''') ) ';
          IF (v_cal_type = 'P')     THEN
             v_sql := v_sql || ' AND NOT EXISTS
            ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_per = a.fk_per AND m.fk_study = a.fk_study
                       AND  m.fk_patprot = a.pk_patprot)  ';
            IF(i.milestone_status > 0) THEN
                v_sql := v_sql || ' and exists ( select * from er_patstudystat s where s.fk_per = a.fk_per and s.fk_study = a.fk_study and s.fk_codelst_stat = ' || i.milestone_status || ')';
              END IF;
              v_sql := v_sql || ' group by pk_patprot,fk_per';
          ELSE
                v_sql := v_sql || ' AND NOT EXISTS
            ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_study = fk_study )  ';
            v_sql := v_sql || ' group by fk_study';
          END IF;
          ELSIF i.codelst_subtyp = 'vm_2' THEN
               -- all events in the visit are marked 'with the given status', get the maximum date
              IF (v_cal_type = 'P')     THEN
                v_sql := ' SELECT MAX (EVENT_EXEON) EVENT_EXEON, PK_PATPROT, FK_PER , SCH_EVENTS1_PK  FROM ERV_PATSTUDY_EVEALL A
                                    WHERE FK_STUDY =' || i.fk_study || 'AND      FK_PROTOCOL = ' || i.fk_cal || '  AND event_status_id = ' || i.milestone_eventstatus;
               ELSE
                       ------------here status is checked bcoz in admin schedule there is no way to separate old and new schedules besided status
                  v_sql := ' SELECT  max(EVENT_EXEON) EVENT_EXEON,  FK_STUDY  , event_id FROM sch_events1 ,sch_eventstat a
                                    WHERE FK_STUDY =' || i.fk_study || ' AND      session_id = ' || i.fk_cal ||
                                    ' and status = 0 and EVENT_ID = a.FK_EVENT AND a.EVENTSTAT = ' || i.milestone_eventstatus;
               END IF;
            IF(i.fk_visit > 0) THEN
              -- A visit was selected
              v_sql := v_sql || ' and fk_visit = ' || i.fk_visit;
            END IF;
            v_sql := v_sql || ' AND  ( a.created_on  >= to_date(''' || v_stdate || ''') OR  a.last_modified_date >= to_date(''' || v_stdate || ''') )';
            IF (v_cal_type = 'P')     THEN
                     v_sql := v_sql || ' AND NOT EXISTS
                ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_per = a.fk_per AND m.fk_study = a.fk_study
                           AND  m.fk_patprot = a.pk_patprot)  ';
                 IF(i.milestone_status > 0) THEN
                  v_sql := v_sql || ' and exists ( select * from er_patstudystat s where s.fk_per = a.fk_per and s.fk_study = a.fk_study and s.fk_codelst_stat = ' || i.milestone_status || ')';
                END IF;
            v_sql := v_sql || ' group by pk_patprot,fk_per,  SCH_EVENTS1_PK';
             ELSE
                         v_sql := v_sql || ' AND NOT EXISTS
                ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_study = fk_study )  ';
                    v_sql := v_sql || ' group by fk_study, event_id ';
             END IF;
            IF (v_cal_type = 'P')     THEN
                v_sql := 'SELECT  MAX(EVENT_EXEON), PK_PATPROT, FK_PER  , COUNT(*) FROM( ' || v_sql || ') A   GROUP BY PK_PATPROT, FK_PER ';
                v_sql := v_sql || '  HAVING COUNT(*) = (SELECT COUNT(*) FROM SCH_EVENTS1 i   WHERE I.FK_PATPROT = A.PK_PATPROT ';
            ELSE
                v_sql := 'SELECT  MAX(EVENT_EXEON), fk_study, COUNT(*) FROM( ' || v_sql || ') A   GROUP BY fk_study ';
                v_sql := v_sql || '  HAVING COUNT(*) = (SELECT COUNT(*) FROM SCH_EVENTS1 i   WHERE I.FK_study = A.fk_study and i.session_id = ' ||
                i.fk_cal
                || ' and status = 0';
            END IF;
            IF(i.fk_visit > 0) THEN
              -- A visit was selected
              v_sql := v_sql || '  and I.fk_visit = ' || i.fk_visit;
            END IF;
            v_sql := v_sql || ')';
            ELSIF i.codelst_subtyp = 'vm_4' THEN
              -- on scheduled date
                IF (v_cal_type = 'P')     THEN
                      v_sql := ' SELECT MAX(trunc(event_actual_schdate))  schdate , PK_PATPROT, FK_PER   FROM ERV_PATSTUDY_EVEALL A
                                    WHERE FK_STUDY =' || i.fk_study || '  AND      FK_PROTOCOL = ' || i.fk_cal;
                 ELSE
                     v_sql := ' SELECT  max(trunc(actual_schdate)) schdate,  FK_STUDY   FROM sch_events1  a
                                    WHERE FK_STUDY =' || i.fk_study || ' AND      session_id = ' || i.fk_cal ||
                                    ' and status = 0  ';
                 END IF;
              IF(i.fk_visit > 0) THEN
                -- A visit was selected
                v_sql := v_sql || ' and fk_visit = ' || i.fk_visit;
              END IF;
              IF (v_cal_type = 'P')     THEN
                      v_sql := v_sql || '  AND NOT EXISTS
                ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_per = a.fk_per AND m.fk_study = a.fk_study
                           AND  m.fk_patprot = a.pk_patprot)  ';
                  IF(i.milestone_status > 0) THEN
                    v_sql := v_sql || ' and exists ( select * from er_patstudystat s where s.fk_per = a.fk_per and s.fk_study = a.fk_study and s.fk_codelst_stat = ' || i.milestone_status || ')';
                  END IF;
                  v_sql := v_sql || ' group by pk_patprot,fk_per  having MAX(event_actual_schdate)  <=  (sysdate)';
              ELSE
                  v_sql := v_sql || '  AND NOT EXISTS
                ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_study = a.fk_study)  ';
                v_sql := v_sql || ' group by fk_study having MAX(actual_schdate)  <=  (sysdate)';
              END IF;
              ------- event milestone
              ELSIF(i.codelst_subtyp = 'em_2') THEN
                -- event status selected as
               IF (v_cal_type = 'P')     THEN
                        v_sql := ' SELECT EVENT_EXEON, PK_PATPROT, FK_PER   FROM ERV_PATSTUDY_EVEALL A
                                    WHERE FK_STUDY =' || i.fk_study || ' AND      FK_PROTOCOL = ' || i.fk_cal || ' AND event_status_id = ' || i.milestone_eventstatus;
                    IF(i.fk_eventassoc > 0) THEN
                      -- A event  was selected
                      v_sql := v_sql || ' and event_id_assoc = ' || i.fk_eventassoc;
                    END IF;
                    --AND NOT EXISTS( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_per = a.fk_per AND m.fk_study = a.fk_study
                   --AND  m.fk_patprot = a.pk_patprot)  ';
                 ELSE
                         ------------here status is checked bcoz in admin schedule there is no way to separate old and new schedules besided status
                          v_sql := ' SELECT  EVENT_EXEON,  FK_STUDY  FROM sch_events1 ,sch_eventstat a
                                                WHERE FK_STUDY =' || i.fk_study || ' AND     session_id = ' || i.fk_cal ||
                                                ' and status = 0 and EVENT_ID = a.FK_EVENT AND a.EVENTSTAT = ' || i.milestone_eventstatus;
                          IF(i.fk_eventassoc > 0) THEN
                              -- A event  was selected
                              v_sql := v_sql || ' and fk_assoc = ' || i.fk_eventassoc;
                            END IF;
                 END IF;
                v_sql := v_sql || ' AND  ( to_date(to_char(a.created_on,PKG_DATEUTIL.F_GET_DATETimeFORMAT),PKG_DATEUTIL.F_GET_DATETimeFORMAT ) >= to_date(''' || v_stdate_formatted  || ''',PKG_DATEUTIL.F_GET_DATETimeFORMAT)  ) ';
                IF (v_cal_type = 'P')     THEN
                    IF(i.milestone_status > 0) THEN
                          v_sql := v_sql || ' and exists ( select * from er_patstudystat s where s.fk_per = a.fk_per and s.fk_study = a.fk_study and s.fk_codelst_stat = ' || i.milestone_status || ')';
                    END IF;
                       --v_sql := v_sql || ' group by pk_patprot,fk_per';
               -- else
                 --    v_sql := v_sql || ' group by fk_study';
                END IF;
                -- plog.fatal(pctx,   ' v_sql1' || v_sql);
                ELSIF i.codelst_subtyp = 'em_4' THEN
                  -- event milestone on scheduled date
                   IF (v_cal_type = 'P')     THEN
                          v_sql := ' SELECT MAX(trunc(event_actual_schdate))  schdate , PK_PATPROT, FK_PER   FROM ERV_PATSTUDY_EVEALL A
                                        WHERE FK_STUDY =' || i.fk_study || '  AND      FK_PROTOCOL = ' || i.fk_cal;
                      IF(i.fk_eventassoc > 0) THEN
                        -- A visit was selected
                        v_sql := v_sql || ' and event_id_assoc  = ' || i.fk_eventassoc;
                      END IF;
                      v_sql := v_sql || '  AND NOT EXISTS
                    ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone ||
                    '  AND m.fk_per = a.fk_per AND m.fk_study = a.fk_study AND  m.fk_patprot = a.pk_patprot)  ';
                      IF(i.milestone_status > 0) THEN
                        v_sql := v_sql || ' and exists ( select * from er_patstudystat s where s.fk_per = a.fk_per and s.fk_study = a.fk_study and s.fk_codelst_stat = ' || i.milestone_status || ')';
                      END IF;
                       v_sql := v_sql || ' group by pk_patprot,fk_per  having MAX(event_actual_schdate)  <=  (sysdate)';
                    ELSE
                    v_sql := ' SELECT  max(trunc(actual_schdate)) schdate,  FK_STUDY   FROM sch_events1  a
                                    WHERE FK_STUDY =' || i.fk_study || ' AND      session_id = ' || i.fk_cal ||
                                    ' and status = 0  ';
                      IF(i.fk_eventassoc > 0) THEN
                        -- A visit was selected
                        v_sql := v_sql || ' and fk_assoc = ' || i.fk_eventassoc;
                      END IF;
                       v_sql := v_sql || '  AND NOT EXISTS
                ( SELECT * FROM ER_MILEACHIEVED m WHERE m.fk_milestone = ' || i.pk_milestone || '  AND m.fk_study = a.fk_study)  ';
                        v_sql := v_sql || ' group by fk_study having MAX(actual_schdate)  <=  (sysdate)';
                    END IF;
                --    plog.fatal(pctx,   ' v_sql2' || v_sql);
                END IF;
                OPEN v_cur FOR v_sql;
                LOOP
            IF (v_cal_type = 'P')     THEN
                  IF(i.codelst_subtyp = 'vm_3' OR i.codelst_subtyp = 'vm_4' OR i.codelst_subtyp = 'em_2' OR i.codelst_subtyp = 'em_4') THEN
                    FETCH v_cur
                    INTO v_date,
                      v_patprot,
                      v_per;
                    ELSIF i.codelst_subtyp = 'vm_2' THEN
                      FETCH v_cur
                      INTO v_date,
                        v_patprot,
                        v_per,
                        v_all_ev_count;
                    END IF;
               ELSE
                       IF(i.codelst_subtyp = 'vm_3' OR i.codelst_subtyp = 'vm_4' OR i.codelst_subtyp = 'em_2' OR i.codelst_subtyp = 'em_4') THEN
                    FETCH v_cur
                    INTO v_date,
                      v_study;
                    ELSIF i.codelst_subtyp = 'vm_2' THEN
                      FETCH v_cur
                      INTO v_date,
                        v_study,
                        v_all_ev_count;
                    END IF;
               END IF;
                    EXIT
                  WHEN v_cur % NOTFOUND;
                  IF (v_cal_type = 'S')     THEN
                          v_per:= NULL;
                          v_patprot := NULL;
                          v_milestone_percount := 0;
                  END IF;
                  IF(v_limit > 0
                   AND v_milestone_percount <= 0) THEN
                    IF v_counter = v_milestone_remaining THEN
                      EXIT;
                    END IF;
                  END IF;
                  INSERT
                  INTO ER_MILEACHIEVED(pk_mileachieved,   fk_milestone,   fk_per,   fk_study,   ach_date,   TABLE_NAME,   created_on,   fk_patprot,   is_complete)
                  VALUES(seq_er_mileachieved.NEXTVAL,   i.pk_milestone,   v_per,   i.fk_study,   v_date,   'sch_events1',   SYSDATE,   v_patprot,   0);
                  /* IF ( LENGTH(trim(NVL(v_m_users,'')) ) > 0 )  THEN
                                             v_fparam :=  v_studynumber || '~'|| v_miletype ||'~'|| i.miledesc ||'~'|| TO_CHAR(v_DATE) ||'~'|| Pkg_Patient.f_get_patcodes(v_per)  ;
                                              v_msg :=    PKG_COMMON.SCH_GETMAIL(V_M_MSGTEMP ,v_fparam);
                                             -- call SP_TRANMAIL to Insert a record in SCH_MSGTRAN for notification for every user
                                              Pkg_Milnot.SP_TRANMAIL(v_msg,v_m_users,NULL);
                END IF;*/
                  IF(v_milestone_percount > 0
                   AND v_limit > 0) THEN
                    v_milestones_achieved_rows := v_milestones_achieved_rows + 1;
                    IF FLOOR(v_milestones_achieved_rows / v_milestone_percount) = v_limit THEN
                      EXIT;
                    END IF;
                  END IF;
                  v_counter := v_counter + 1;
                END LOOP;
                -- for milestone
                IF(v_milestone_percount = 0 OR v_milestone_percount = -1) THEN
                  v_milestones_count := v_milestones_already_achieved + v_counter;
                  v_exp_rows := v_counter;
                ELSE
                  SELECT COUNT(*)
                  INTO v_milestones_already_achieved
                  FROM ER_MILEACHIEVED
                  WHERE fk_milestone = i.pk_milestone;
                  SELECT FLOOR(v_milestones_already_achieved / v_milestone_percount)
                  INTO v_milestones_count
                  FROM dual;
                  v_exp_rows :=(v_milestones_count -i.milestone_achievedcount) * v_milestone_percount;
                END IF;
                UPDATE ER_MILESTONE
                SET milestone_achievedcount = v_milestones_count,
                  last_checked_on = SYSDATE
                WHERE pk_milestone = i.pk_milestone;
                -- for updating the complete flag of completed milestones
                v_exp_rows := v_exp_rows + 1;
                ------------------
                -- create notifications for each achieved milestone
                IF v_milestone_percount <= 0 THEN
                  v_milestone_percount := 1;
                END IF;
                --plog.DEBUG(pctx, 'v_pm_users' || v_pm_users);
                IF(LENGTH(TRIM(NVL(v_m_users,   ''))) > 0) THEN
                  v_row_count := 1;
                  -- start with first row
                  --                         plog.DEBUG(pctx, 'i have users');
                  FOR ct IN
                    (SELECT pk_mileachieved,
                       ach_date,
                       fk_per
                     FROM ER_MILEACHIEVED
                     WHERE fk_milestone = i.pk_milestone
                     AND ROWNUM < v_exp_rows
                     AND NVL(is_complete,    0) = 0
                     ORDER BY pk_mileachieved ASC)
                  LOOP
                IF (v_cal_type = 'P')     THEN
                    v_patcode_string := v_patcode_string || CHR(13) || ' Patient ID: ' || Pkg_Patient.f_get_patcodes(ct.fk_per);
                 END IF;
                    plog.DEBUG(pctx,   'v_row_count' || v_row_count || 'v_milestone_percount ' || v_milestone_percount);
                    IF v_row_count = v_milestone_percount THEN
                      --send notification
                        IF (v_cal_type = 'P')     THEN
                              IF LENGTH(v_patcode_string) > 3800 THEN
                                v_patcode_string := SUBSTR(v_patcode_string,   1,   3800);
                              END IF;
                        ELSE
                            v_patcode_string := 'N/A';
                        END IF;
                      v_fparam := v_studynumber || '~' || v_miletype
                      || '~' || i.miledesc
                      || '~' || TO_CHAR(ct.ach_date) || '~' || v_patcode_string;
                      v_msg := pkg_common.sch_getmail(v_m_msgtemp,   v_fparam);
                      -- call SP_TRANMAIL to Insert a record in SCH_MSGTRAN for notification for every user
                      Pkg_Milnot.sp_tranmail(v_msg,   v_m_users,   NULL);
                      plog.DEBUG(pctx,   'got a milestone');
                      --reset flag
                      v_row_count := 1;
                      v_patcode_string := '';
                      ELSIF v_row_count < v_milestone_percount THEN
                        --send notification
                        plog.DEBUG(pctx,   'not yet');
                        v_row_count := v_row_count + 1;
                      END IF;
                    END LOOP;
                  END IF;
                  -- for v_m_users
                  ------------------
                  UPDATE ER_MILEACHIEVED
                  SET is_complete = 1
                  WHERE NVL(is_complete,   0) = 0
                   AND fk_milestone = i.pk_milestone
                   AND pk_mileachieved IN
                    (SELECT pk_mileachieved
                     FROM ER_MILEACHIEVED
                     WHERE fk_milestone = i.pk_milestone
                     AND ROWNUM < v_exp_rows
                     AND NVL(is_complete,    0) = 0)
                  ;
                  COMMIT;
                END LOOP;
              END sp_transfer_ach_vmem;
              PROCEDURE sp_transfer_ach_sm AS
               /* Date : 11/03/05
    Author: Sonia Abrol
    Purpose - will run on scheduled intervals to calculate achieved milestones and transfer records to er_mileachieved, for study milestones
 */ v_stdate DATE;
              v_milestones_already_achieved NUMBER := 0;
              v_milestones_count NUMBER := 0;
              v_milestone_remaining NUMBER := 0;
              v_limit NUMBER := 0;
              v_counter NUMBER := 0;
              v_milestone_percount NUMBER := 0;
              v_milestones_achieved_rows NUMBER := 0;
              v_msg VARCHAR2(4000);
              v_fparam VARCHAR2(500);
              v_studynumber VARCHAR2(1000);
              v_sm_msgtemp VARCHAR2(4000);
              v_sm_users VARCHAR2(500);
              v_exp_rows NUMBER;
              BEGIN
                v_sm_msgtemp := pkg_common.sch_getmailmsg('c_pm');
                -- get all thepatient milestones defined
                FOR i IN
                  (SELECT pk_milestone,
                     ER_MILESTONE.fk_study,
                     NVL(milestone_count,    0) milestone_count,
                     milestone_usersto,
                     study_number,
                     study_actualdt,
                     NVL(milestone_limit,    0) milestone_limit,
                     milestone_status,
                     last_checked_on,
                     NVL(milestone_achievedcount,    0) milestone_achievedcount,
                     f_getmilestonedesc(pk_milestone) miledesc
                   FROM ER_MILESTONE,
                     ER_STUDY
                   WHERE milestone_delflag <> 'Y'
                   AND fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='A')
                   AND(last_checked_on < SYSDATE OR last_checked_on IS NULL)
                   AND TRIM(milestone_type) = 'SM'
                   AND ER_MILESTONE.fk_study = pk_study
                   AND(NVL(milestone_achievedcount,    0) < milestone_limit OR milestone_limit IS NULL))
                LOOP
                  -- loop 1
                  SELECT COUNT(*)
                  INTO v_milestones_achieved_rows
                  FROM ER_MILEACHIEVED
                  WHERE fk_milestone = i.pk_milestone;
                  v_milestones_already_achieved := i.milestone_achievedcount;
                  v_milestone_percount := i.milestone_count;
                  v_limit := i.milestone_limit;
                  v_sm_users := i.milestone_usersto;
                  v_studynumber := i.study_number;
                  IF v_limit > 0 THEN
                    v_milestone_remaining := v_limit -v_milestones_already_achieved;
                  END IF;
                  v_stdate := i.last_checked_on;
                  v_counter := 0;
                  FOR P IN
                    (SELECT studystat_date,
                       pk_studystat
                     FROM ER_STUDYSTAT
                     WHERE fk_study = i.fk_study
                     AND fk_codelst_studystat = i.milestone_status
                     AND(created_on >= v_stdate OR last_modified_date >= v_stdate OR v_stdate IS NULL)
                     AND NOT EXISTS
                      (SELECT *
                       FROM ER_MILEACHIEVED m
                       WHERE m.fk_milestone = i.pk_milestone
                       AND m.fk_per = pk_studystat
                       AND m.fk_study = i.fk_study)
                    ORDER BY studystat_date)
                  LOOP
                    IF(v_limit > 0
                     AND v_milestone_percount <= 0) THEN
                      IF v_counter = v_milestone_remaining THEN
                        EXIT;
                      END IF;
                    END IF;
                    --insret pk_studystat for study status record in fk_per
                    INSERT
                    INTO ER_MILEACHIEVED(pk_mileachieved,   fk_milestone,   fk_study,   ach_date,   TABLE_NAME,   created_on,   fk_per,   is_complete)
                    VALUES(seq_er_mileachieved.NEXTVAL,   i.pk_milestone,   i.fk_study,   P.studystat_date,   'er_studystat',   SYSDATE,   P.pk_studystat,   0);
                    IF(LENGTH(TRIM(NVL(v_sm_users,   ''))) > 0) THEN
                      v_fparam := v_studynumber || '~Study Status~' || i.miledesc || '~' || TO_CHAR(P.studystat_date) || '~Not Applicable';
                      v_msg := pkg_common.sch_getmail(v_sm_msgtemp,   v_fparam);
                      -- call SP_TRANMAIL to Insert a record in SCH_MSGTRAN for notification for every user
                      Pkg_Milnot.sp_tranmail(v_msg,   v_sm_users,   NULL);
                    END IF;
                    IF(v_milestone_percount > 0
                     AND v_limit > 0) THEN
                      v_milestones_achieved_rows := v_milestones_achieved_rows + 1;
                      IF FLOOR(v_milestones_achieved_rows / v_milestone_percount) = v_limit THEN
                        EXIT;
                      END IF;
                    END IF;
                    v_counter := v_counter + 1;
                  END LOOP;
                  -- for milestone
                  IF(v_milestone_percount = 0 OR v_milestone_percount = -1) THEN
                    v_milestones_count := v_milestones_already_achieved + v_counter;
                    v_exp_rows := v_counter;
                  ELSE
                    SELECT COUNT(*)
                    INTO v_milestones_already_achieved
                    FROM ER_MILEACHIEVED
                    WHERE fk_milestone = i.pk_milestone;
                    SELECT FLOOR(v_milestones_already_achieved / v_milestone_percount)
                    INTO v_milestones_count
                    FROM dual;
                    v_exp_rows :=(v_milestones_count -i.milestone_achievedcount) * v_milestone_percount;
                  END IF;
                  --    plog.debug(pCTX,' v_exp_rows' ||  v_exp_rows);
                  UPDATE ER_MILESTONE
                  SET milestone_achievedcount = v_milestones_count,
                    last_checked_on = SYSDATE
                  WHERE pk_milestone = i.pk_milestone;
                  -- for updating the complete flag of completed milestones
                  v_exp_rows := v_exp_rows + 1;
                  BEGIN
                    UPDATE ER_MILEACHIEVED
                    SET is_complete = 1
                    WHERE NVL(is_complete,   0) = 0
                     AND fk_milestone = i.pk_milestone
                     AND pk_mileachieved IN
                      (SELECT pk_mileachieved
                       FROM ER_MILEACHIEVED
                       WHERE fk_milestone = i.pk_milestone
                       AND ROWNUM < v_exp_rows
                       AND NVL(is_complete,    0) = 0)
                    ;
                  EXCEPTION
                  WHEN OTHERS THEN
                    plog.fatal(pctx,   ' SM:' || SQLERRM);
                  END;
                  COMMIT;
                END LOOP;
              END sp_transfer_ach_sm;
              FUNCTION f_getmilestonedesc(p_milestoneid IN NUMBER) RETURN VARCHAR2 IS v_desc VARCHAR2(4000);
              v_type VARCHAR2(3);
              v_patientstatus VARCHAR2(300);
              v_count NUMBER;
              v_milestone_limit NUMBER;
              v_cal_name VARCHAR2(150);
              v_rule_desc VARCHAR2(300);
              v_visit_name VARCHAR2(150);
              v_eventstatus VARCHAR2(300);
              v_event_desc long; --KM-17May10
              v_patientcountstring VARCHAR2(300);
              v_statusdesc VARCHAR(20);
              v_statusdescplural VARCHAR(20);
              v_milestonedesc VARCHAR(4000);
              BEGIN
                BEGIN
                  SELECT NVL(a.milestone_count,   0),
                    milestone_type,
                      (SELECT b.codelst_desc
                     FROM ER_CODELST b
                     WHERE pk_codelst = a.milestone_status)
                  patientstatus,
                    milestone_limit,
                    (DECODE(NVL(fk_cal,   0),   0,   ' ',
                      (SELECT c.name
                     FROM event_assoc c
                     WHERE a.fk_cal = c.event_id)))
                  cal_name,
                    (DECODE(NVL(fk_codelst_rule,   0),   0,   ' ',
                      (SELECT b.codelst_desc
                     FROM ER_CODELST b
                     WHERE pk_codelst = a.fk_codelst_rule)))
                  rule_desc,
                    (DECODE(NVL(fk_visit,   0),   0,   'All ',   NVL(
                    (SELECT v.visit_name
                     FROM sch_protocol_visit @LNK2SCH v
                     WHERE a.fk_visit = v.pk_protocol_visit),    'All')))
                  visit_name,
                      (SELECT codelst_desc
                     FROM sch_codelst
                     WHERE pk_codelst = milestone_eventstatus)
                  eventstatus,
                    (DECODE(NVL(a.fk_eventassoc,   0),   0,   ' ',
                      (SELECT c.name
                     FROM event_assoc c
                     WHERE a.fk_eventassoc = c.event_id)))
                  event_desc,
                    milestone_description
                  INTO v_count,
                    v_type,
                    v_patientstatus,
                    v_milestone_limit,
                    v_cal_name,
                    v_rule_desc,
                    v_visit_name,
                    v_eventstatus,
                    v_event_desc,
                    v_milestonedesc --KM
                  FROM ER_MILESTONE a
                  WHERE pk_milestone = p_milestoneid;
                  IF(v_type = 'EM' OR v_type = 'VM') THEN
                    v_rule_desc := ', Rule: ' || v_rule_desc;
                  END IF;
                  IF(LENGTH(NVL(v_patientstatus,   '')) > 0) THEN
                    v_patientstatus := '''' || v_patientstatus || '''';
                  END IF;
                  IF(v_type = 'SM') THEN
                    v_statusdesc := 'status';
                    v_statusdescplural := 'statuses';
                   ELSE
                      v_statusdesc := 'patient';
                      v_statusdescplural := 'patients';
                    END IF;
                    IF(v_count <= 0) THEN
                      v_patientcountstring := 'Every ' || v_patientstatus || '  ' || v_statusdesc;
                    ELSE
                      v_patientcountstring := v_count || ' ' || v_patientstatus || ' ' || v_statusdescplural;
                    END IF;
                    IF(LENGTH(v_event_desc) > 0) THEN
                       v_event_desc := ', Event : ' || v_event_desc;
                    END IF;
                    IF(v_type = 'EM' OR v_type = 'VM') THEN
                      v_desc := 'Visit: ' || v_visit_name || v_event_desc || v_rule_desc || v_eventstatus || ' [ ' || v_patientcountstring || '], Calendar:' || v_cal_name;
                    ELSIF (v_type = 'AM') THEN   -- Manimaran-032008
                       v_desc := v_milestonedesc;
                    ELSE
                      v_desc := v_patientcountstring;
                    END IF;
                    RETURN v_desc;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    RETURN v_desc;
                  END;
                END;
                /*executes all milestone calculation procedures */ PROCEDURE sp_process_milestones AS
                BEGIN
                  sp_transfer_ach_pm;
                  sp_transfer_ach_vmem;
                  sp_transfer_ach_sm;
                END;
                FUNCTION f_get_mile_desc_sql(p_from_date IN VARCHAR2,   p_to_date IN VARCHAR2,   p_site IN VARCHAR2,   p_study IN VARCHAR2,   p_division IN VARCHAR2,   p_tarea IN VARCHAR2,   p_restype IN VARCHAR2,   p_sponsor IN VARCHAR2,   p_studystatus IN VARCHAR2,   p_user IN VARCHAR2,   p_patient IN VARCHAR2,   p_budget IN VARCHAR2,   p_account IN NUMBER,   p_loggedinuser IN NUMBER) RETURN CLOB IS v_sql CLOB;
                v_main_sql CLOB;
                v_sub_sql CLOB;
                p_sxml CLOB;
                v_recid NUMBER;
                v_payid NUMBER;
                BEGIN
                  SELECT pk_codelst
                  INTO v_recid
                  FROM ER_CODELST
                  WHERE TRIM(codelst_type) = 'milepaytype'
                   AND codelst_subtyp = 'rec';
                  SELECT pk_codelst
                  INTO v_payid
                  FROM ER_CODELST
                  WHERE TRIM(codelst_type) = 'milepaytype'
                   AND codelst_subtyp = 'pay';
                  IF LENGTH(p_study) > 0 THEN
                    v_sql := v_sql || '  and fk_study in ( ' || p_study || ')';
                  END IF;
                  IF LENGTH(p_patient) > 0 THEN
                    v_sql := v_sql || '  and fk_per in ( ' || p_patient || ')';
                  END IF;
                  IF LENGTH(p_site) > 0 THEN
                    v_sql := v_sql || '  and ( fk_site in   ( ' || p_site || ')  or nvl(fk_site,0) = 0)';
                  END IF;
                  IF LENGTH(p_from_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE >=  to_date ('' ' || p_from_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_to_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE <=  to_date ('' ' || p_to_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_division) > 0 THEN
                    v_sql := v_sql || '  and a.study_division in   ( ' || p_division || ')';
                  END IF;
                  IF LENGTH(p_tarea) > 0 THEN
                    v_sql := v_sql || '  and a.fk_codelst_tarea  in   ( ' || p_tarea || ')';
                  END IF;
                  IF LENGTH(p_restype) > 0 THEN
                    v_sql := v_sql || '  and a.fk_codelst_restype  in   ( ' || p_restype || ')';
                  END IF;
                  IF LENGTH(p_sponsor) > 0 THEN
                    v_sql := v_sql || '  and lower(a.study_sponsor) like  ''%' || LOWER(p_sponsor) || '%''';
                  END IF;
                  IF LENGTH(p_studystatus) > 0 THEN
                    v_sql := v_sql || '  and pk_studystat in   ( ' || p_studystatus || ')';
                  END IF;
                  v_sub_sql := v_sql;
                  IF LENGTH(p_user) > 0 THEN
                    v_sql := v_sql || '  and ( study_prinv in   ( ' || p_user || ')  or  study_prinv is null ) ';
                    v_sub_sql := v_sub_sql || '  and ( s.study_prinv in   ( ' || p_user || ')  or s.study_prinv is null )';
                  END IF;
                  v_sql := '  SELECT FK_MILESTONE, count(*) achcount  FROM ERV_MILEACHIEVED a where a.fk_account = ' || p_account || v_sql;
                  -- append access rights for the logged in user
                  v_sql := v_sql || ' and     decode(    MILESTONE_TYPE,''SM'',1, (pkg_user.f_chk_right_for_studysite(fk_study,' || p_loggedinuser || ',fk_site)) )  > 0 ';
                  v_sql := v_sql || ' group by fk_milestone ORDER By   fk_milestone';
                  v_main_sql := ' SELECT PK_STUDY, Pkg_Milestone_New.f_getMilestoneDesc( a.fK_MILESTONE) mileDesc , (select codelst_desc from er_codelst where pk_codelst = a.milestone_paytype) milestone_paytype_desc ,
                                                   a.study_division,a.study_division_desc,a.fk_codelst_tarea,a.fk_codelst_tarea_desc,
                           a.fk_codelst_restype,a.fk_codelst_restype_desc,a.study_sponsor,a.study_prinv,a.study_prinv_name,
                           a.pk_studystat, ( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = (Pkg_Studystat.f_getLatestStudyStatus(a.fk_study))) studystat_desc,
                           a.study_number,   DECODE(milestone_count,0,T.achcount ,1,T.achcount ,FLOOR( ABS(T.achcount /milestone_count ))) achcount,
                                                   TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2) achamount,
                                                   DECODE(milestone_paytype ,' || v_recid || ', TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2),0) recMileAmountCalc,
                                                   DECODE(milestone_paytype ,' || v_recid || ',0, TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2)) payMileAmountCalc,
                                                   (SELECT site_name FROM ER_SITE WHERE a.fk_site = pk_site) site_name,
                                                   patfacilityids,(SELECT patprot_patstdid FROM ER_PATPROT pp WHERE a.fk_per = pp.fk_per AND a.fk_study = pp.fk_study AND pp.patprot_stat = 1 AND ROWNUM < 2) pat_study_id,TO_CHAR(ACH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACH_DATE,milestone_amount
                           FROM  ER_STUDY s, (' || v_sql || ' )  T , erv_mileachieved a WHERE T.fk_milestone = a.fk_milestone AND a.fk_study = s.pk_study AND s.fk_account = ' || p_account || v_sub_sql || ' ORDER BY  a.study_division,  a.fk_codelst_tarea, a.study_number,a.fk_milestone';
                  plog.DEBUG(pctx,   'V_SQL_main 1' || v_main_sql);
                  plog.DEBUG(pctx,   'V_SQL_main 2' || v_sql || ' )  T , erv_mileachieved a WHERE T.fk_milestone = a.fk_milestone AND a.fk_study = s.pk_study AND s.fk_account = ' || p_account || v_sub_sql || ' ORDER BY  a.study_division,  a.fk_codelst_tarea, a.study_number,a.fk_milestone');
                  p_sxml := dbms_Xmlgen.getxml(v_main_sql);
                  RETURN p_sxml;
                END;
                FUNCTION f_get_mile_interval_xml(p_from_date IN VARCHAR2,   p_to_date IN VARCHAR2,   p_study IN VARCHAR2,   p_division IN VARCHAR2,   p_tarea IN VARCHAR2,   p_restype IN VARCHAR2,   p_sponsor IN VARCHAR2,   p_studystatus IN VARCHAR2,   p_user IN VARCHAR2,   p_account IN NUMBER,   p_loggedinuser IN NUMBER) RETURN CLOB IS v_sql CLOB;
                v_main_sql CLOB;
                v_sql_mile CLOB;
                p_sxml CLOB;
                BEGIN
                  v_sql := '  SELECT FK_MILESTONE, count(*) achcount  FROM ERV_MILEACHIEVED a where a.fk_account = ' || p_account;
                  IF LENGTH(p_study) > 0 THEN
                    v_sql := v_sql || '  and fk_study in ( ' || p_study || ')';
                  END IF;
                  IF LENGTH(p_from_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE >=  to_date ('' ' || p_from_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_to_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE <=  to_date ('' ' || p_to_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_division) > 0 THEN
                    v_sql := v_sql || '  and study_division in   ( ' || p_division || ')';
                  END IF;
                  IF LENGTH(p_tarea) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_tarea  in   ( ' || p_tarea || ')';
                  END IF;
                  IF LENGTH(p_restype) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_restype  in   ( ' || p_restype || ')';
                  END IF;
                  IF LENGTH(p_sponsor) > 0 THEN
                    v_sql := v_sql || '  and lower(study_sponsor) like  ''%' || LOWER(p_sponsor) || '%''';
                  END IF;
                  IF LENGTH(p_studystatus) > 0 THEN
                    v_sql := v_sql || '  and pk_studystat in   ( ' || p_studystatus || ')';
                  END IF;
                  IF LENGTH(p_user) > 0 THEN
                    v_sql := v_sql || '   and (study_prinv in   ( ' || p_user || ') or study_prinv is null) ';
                  END IF;
                  -- append access rights for the logged in user
                  v_sql := v_sql || ' and     decode(    MILESTONE_TYPE,''SM'',1, (pkg_user.f_chk_right_for_studysite(fk_study,' || p_loggedinuser || ',fk_site)) )  > 0 ';
                  v_sql := v_sql || ' group by fk_milestone ORDER By   fk_milestone';
                  v_sql_mile := '  select fk_study, sum(decode(milestone_count,0,t.achcount ,1,t.achcount ,FLOOR( abs(t.achcount /milestone_count )))) achcount,
                               SUM(TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2)) achamount
                               FROM   ER_MILESTONE a, (' || v_sql || ')  T  WHERE pk_milestone = fk_milestone  GROUP BY fk_study';
                  v_main_sql := ' SELECT PK_STUDY,
                                             study_division,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) study_division_desc,
                           fk_codelst_tarea, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) fk_codelst_tarea_desc,
                           fk_codelst_restype, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) fk_codelst_restype_desc,
                           DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor))  study_sponsor,
                           study_prinv,Usr_Lst(study_prinv)  study_prinv_name,
                           Pkg_Studystat.f_getLatestStudyStatus(pk_study) pk_studystat, ( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = (Pkg_Studystat.f_getLatestStudyStatus(pk_study))) studystat_desc,
                           study_number,   achcount,  achamount
                           FROM  ER_STUDY s, (' || v_sql_mile || ' )  m  WHERE m.fk_study = pk_study AND s.fk_account = ' || p_account || ' ORDER BY  study_division,  fk_codelst_tarea, study_number';
                  plog.DEBUG(pctx,   'V_SQL_main' || v_main_sql);
                  p_sxml := dbms_Xmlgen.getxml(v_main_sql);
                  RETURN p_sxml;
                END;
                FUNCTION f_get_mile_arap_xml(p_from_date IN VARCHAR2,   p_to_date IN VARCHAR2,   p_study IN VARCHAR2,   p_division IN VARCHAR2,   p_tarea IN VARCHAR2,   p_restype IN VARCHAR2,   p_sponsor IN VARCHAR2,   p_studystatus IN VARCHAR2,   p_user IN VARCHAR2,   p_account IN NUMBER,   p_loggedinuser IN NUMBER) RETURN CLOB IS v_sql CLOB;
                v_main_sql CLOB;
                p_sxml CLOB;
                v_recid NUMBER;
                v_payid NUMBER;
                BEGIN
                  SELECT pk_codelst
                  INTO v_recid
                  FROM ER_CODELST
                  WHERE TRIM(codelst_type) = 'milepaytype'
                   AND codelst_subtyp = 'rec';
                  SELECT pk_codelst
                  INTO v_payid
                  FROM ER_CODELST
                  WHERE TRIM(codelst_type) = 'milepaytype'
                   AND codelst_subtyp = 'pay';
                  v_sql := '  SELECT FK_MILESTONE, count(*) achcount  FROM ERV_MILEACHIEVED a where a.fk_account = ' || p_account;
                  IF LENGTH(p_study) > 0 THEN
                    v_sql := v_sql || '  and fk_study in ( ' || p_study || ')';
                  END IF;
                  IF LENGTH(p_from_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE >=  to_date ('' ' || p_from_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_to_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE <=  to_date ('' ' || p_to_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_division) > 0 THEN
                    v_sql := v_sql || '  and study_division in   ( ' || p_division || ')';
                  END IF;
                  IF LENGTH(p_tarea) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_tarea  in   ( ' || p_tarea || ')';
                  END IF;
                  IF LENGTH(p_restype) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_restype  in   ( ' || p_restype || ')';
                  END IF;
                  IF LENGTH(p_sponsor) > 0 THEN
                    v_sql := v_sql || '  and lower(study_sponsor) like  ''%' || LOWER(p_sponsor) || '%''';
                  END IF;
                  IF LENGTH(p_studystatus) > 0 THEN
                    v_sql := v_sql || '  and pk_studystat in   ( ' || p_studystatus || ')';
                  END IF;
                  IF LENGTH(p_user) > 0 THEN
                    v_sql := v_sql || '  and (study_prinv in   ( ' || p_user || ') or study_prinv is null) ';
                  END IF;
                  -- append access rights for the logged in user
                  v_sql := v_sql || ' and     decode(    MILESTONE_TYPE,''SM'',1, (pkg_user.f_chk_right_for_studysite(fk_study,' || p_loggedinuser || ',fk_site)) )  > 0 ';
                  v_sql := v_sql || ' group by fk_milestone ORDER By   fk_milestone';
                  v_main_sql := ' SELECT PK_STUDY, Pkg_Milestone_New.f_getMilestoneDesc( PK_MILESTONE) mileDesc , (select codelst_desc from er_codelst where pk_codelst = milestone_paytype) milestone_paytype_desc ,
                        study_division,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) study_division_desc,
                        fk_codelst_tarea, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) fk_codelst_tarea_desc,
                        fk_codelst_restype, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) fk_codelst_restype_desc,
                        DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor))  study_sponsor,
                        study_prinv,Usr_Lst(study_prinv)  study_prinv_name,
                        Pkg_Studystat.f_getLatestStudyStatus(pk_study) pk_studystat, ( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = (Pkg_Studystat.f_getLatestStudyStatus(pk_study))) studystat_desc,
                        study_number,   DECODE(milestone_count,0,T.achcount ,1,T.achcount ,FLOOR( ABS(T.achcount /milestone_count ))) achcount,
                        TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2) achamount,
                        DECODE(milestone_paytype ,' || v_recid || ',  Pkg_Milestone_New.getMileInvoiceAmount(m.pk_milestone,''' || p_from_date || ''',''' || p_to_date || ''') ,0 ) amount_invoiced,
                        NVL(Pkg_Milestone_New. getMilePaymentAmount(m.pk_milestone,''' || p_from_date || ''',''' || p_to_date || ''',''payrec'') ,0) amount_received,
                        NVL(Pkg_Milestone_New.getMilePaymentAmount(m.pk_milestone, ''' || p_from_date || ''',''' || p_to_date || ''', ''paymade'') ,0)  amount_paid,
                        DECODE(milestone_paytype ,' || v_recid || ', TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2),0) recMileAmountCalc,
                        DECODE(milestone_paytype ,' || v_recid || ',0, TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2)) payMileAmountCalc
                        FROM  ER_STUDY s, (' || v_sql || ' )  T ,  ER_MILESTONE m WHERE T.fk_milestone = pk_milestone AND m.fk_study = pk_study AND s.fk_account = ' || p_account || ' ORDER BY  study_division,  fk_codelst_tarea, study_number,pk_milestone';
                  plog.DEBUG(pctx,   'V_SQL_main' || v_main_sql);
                  p_sxml := dbms_Xmlgen.getxml(v_main_sql);
                  RETURN p_sxml;
                END;
                FUNCTION getmileinvoiceamount(p_pkmilestone NUMBER,   p_fromdate VARCHAR2,   p_to_date VARCHAR2) RETURN NUMBER IS v_amount NUMBER;
                BEGIN
                  BEGIN
                      --Bug Fix for #3579 SM 06/27/2008 Date format
                    SELECT NVL(SUM(NVL(amount_invoiced,   0)),   0)
                    INTO v_amount
                    FROM ER_INVOICE_DETAIL det,
                      ER_INVOICE
                    WHERE det.fk_milestone = p_pkmilestone
                     AND det.fk_per = 0
                     AND pk_invoice = fk_inv
                     AND inv_date BETWEEN TO_DATE(p_fromdate, PKG_DATEUTIL.F_GET_DATEFORMAT)
                     AND TO_DATE(p_to_date, PKG_DATEUTIL.F_GET_DATEFORMAT);
                  EXCEPTION
                  WHEN OTHERS THEN
                    plog.fatal(pctx,   'getMileInvoiceAmount' || SQLERRM);
                    RETURN 0;
                  END;
                  RETURN v_amount;
                END;
                FUNCTION getmilepaymentamount(p_pkmilestone NUMBER,   p_fromdate VARCHAR2,   p_to_date VARCHAR2,   p_payment_type VARCHAR2) RETURN NUMBER IS v_amount NUMBER;
                BEGIN
                  BEGIN
                      --Bug Fix for #3579 SM 06/27/2008 Date format
                    SELECT SUM(NVL(mp_amount,   0))
                    INTO v_amount
                    FROM ER_MILEPAYMENT_DETAILS det,
                      ER_MILEPAYMENT
                    WHERE mp_linkto_type = 'M'
                     AND det.mp_linkto_id = p_pkmilestone
                     AND det.mp_level1_id = 0
                     AND mp_level2_id = 0
                     AND pk_milepayment = fk_milepayment
                     AND milepayment_date BETWEEN TO_DATE(p_fromdate,PKG_DATEUTIL.F_GET_DATEFORMAT)
                     AND TO_DATE(p_to_date, PKG_DATEUTIL.F_GET_DATEFORMAT)
                     AND milepayment_type =
                      (SELECT pk_codelst
                       FROM ER_CODELST
                       WHERE codelst_type = 'paymentCat'
                       AND TRIM(codelst_subtyp) = p_payment_type)
                    ;
                  EXCEPTION
                  WHEN OTHERS THEN
                    plog.fatal(pctx,   'getMilePaymentAmount' || SQLERRM);
                    RETURN 0;
                  END;
                  RETURN v_amount;
                END;
                --------------------------
                FUNCTION f_get_mile_pay_discrepancy_xml(p_from_date IN VARCHAR2,   p_to_date IN VARCHAR2,   p_study IN VARCHAR2,   p_division IN VARCHAR2,   p_tarea IN VARCHAR2,   p_restype IN VARCHAR2,   p_sponsor IN VARCHAR2,   p_studystatus IN VARCHAR2,   p_user IN VARCHAR2,   p_account IN NUMBER,   p_loggedinuser IN NUMBER) RETURN CLOB IS v_sql CLOB;
                v_main_sql CLOB;
                v_sql_mile CLOB;
                p_sxml CLOB;
                v_recid NUMBER;
                BEGIN
                  SELECT pk_codelst
                  INTO v_recid
                  FROM ER_CODELST
                  WHERE TRIM(codelst_type) = 'milepaytype'
                   AND codelst_subtyp = 'rec';
                  v_sql := '  SELECT FK_MILESTONE, count(*) achcount  FROM ERV_MILEACHIEVED a where a.fk_account = ' || p_account || ' and milestone_paytype = ' || v_recid;
                  IF LENGTH(p_study) > 0 THEN
                    v_sql := v_sql || '  and fk_study in ( ' || p_study || ')';
                  END IF;
                  IF LENGTH(p_from_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE >=  to_date ('' ' || p_from_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_to_date) > 0 THEN
                    v_sql := v_sql || '  and  ACH_DATE <=  to_date ('' ' || p_to_date || ''',PKG_DATEUTIL.F_GET_DATEFORMAT)';
                  END IF;
                  IF LENGTH(p_division) > 0 THEN
                    v_sql := v_sql || '  and study_division in   ( ' || p_division || ')';
                  END IF;
                  IF LENGTH(p_tarea) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_tarea  in   ( ' || p_tarea || ')';
                  END IF;
                  IF LENGTH(p_restype) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_restype  in   ( ' || p_restype || ')';
                  END IF;
                  IF LENGTH(p_sponsor) > 0 THEN
                    v_sql := v_sql || '  and lower(study_sponsor) like  ''%' || LOWER(p_sponsor) || '%''';
                  END IF;
                  IF LENGTH(p_studystatus) > 0 THEN
                    v_sql := v_sql || '  and pk_studystat in   ( ' || p_studystatus || ')';
                  END IF;
                  IF LENGTH(p_user) > 0 THEN
                    v_sql := v_sql || '  and ( study_prinv in   ( ' || p_user || ')  or study_prinv is null) ';
                  END IF;
                  -- append access rights for the logged in user
                  v_sql := v_sql || ' and     decode(    MILESTONE_TYPE,''SM'',1, (pkg_user.f_chk_right_for_studysite(fk_study,' || p_loggedinuser || ',fk_site)) )  > 0 ';
                  v_sql := v_sql || ' group by fk_milestone ORDER By   fk_milestone';
                  v_sql_mile := '  select fk_study, sum(decode(milestone_count,0,t.achcount ,1,t.achcount ,FLOOR( abs(t.achcount /milestone_count )))) achcount,
                               SUM(TRUNC(DECODE(milestone_count,0,(T.achcount * milestone_amount ) ,1,(T.achcount * milestone_amount ) ,(FLOOR( ABS(T.achcount /milestone_count )) *  milestone_amount )),2)) achamount,
                               SUM(TRUNC(NVL(Pkg_Milestone_New. getMilePaymentAmount(a.pk_milestone,''' || p_from_date || ''',''' || p_to_date || ''',''payrec'') ,0) ,2)) payment_rec
                               FROM   ER_MILESTONE a, (' || v_sql || ')  T  WHERE pk_milestone = fk_milestone  GROUP BY fk_study';
                  v_main_sql := ' SELECT PK_STUDY,
                                             study_division,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) study_division_desc,
                           fk_codelst_tarea, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) fk_codelst_tarea_desc,
                           fk_codelst_restype, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) fk_codelst_restype_desc,
                           DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor)) study_sponsor,
                           study_prinv,Usr_Lst(study_prinv)  study_prinv_name,
                           Pkg_Studystat.f_getLatestStudyStatus(pk_study) pk_studystat, ( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = (Pkg_Studystat.f_getLatestStudyStatus(pk_study))) studystat_desc,
                           study_number,   achcount,  achamount, payment_rec
                           FROM  ER_STUDY s, (' || v_sql_mile || ' )  m  WHERE m.fk_study = pk_study AND s.fk_account = ' || p_account || ' ORDER BY  study_division,  fk_codelst_tarea, study_number';
                  plog.DEBUG(pctx,   'V_SQL_main' || v_main_sql);
                  p_sxml := dbms_Xmlgen.getxml(v_main_sql);
                  RETURN p_sxml;
                END;
                FUNCTION f_get_mile_forecast_xml(p_from_date IN VARCHAR2,   p_to_date IN VARCHAR2,   p_study IN VARCHAR2,   p_division IN VARCHAR2,   p_tarea IN VARCHAR2,   p_restype IN VARCHAR2,   p_sponsor IN VARCHAR2,   p_studystatus IN VARCHAR2,   p_user IN VARCHAR2,   p_account IN NUMBER,   p_loggedinuser IN NUMBER) RETURN CLOB IS v_milesql CLOB;
                v_sql CLOB;
                v_main_sql CLOB;
                v_sql_mile CLOB;
                p_sxml CLOB;
                BEGIN
                  -- loop through all milestones for forecast
                  v_sql := '  select pkg_milestone_new.F_GET_MILE_FORECAST_COUNT(fk_study , codelst_subtyp, milestone_count,' || p_loggedinuser || ' ,''' || p_from_date || ''',''' || p_to_date || ''', fk_eventassoc,fk_visit, fk_cal) ach_count, milestone_amount, fk_study
               FROM ERV_MILESTONES_4FORECAST';
                  IF LENGTH(p_study) > 0 THEN
                    v_sql := v_sql || '  Where  fk_study in ( ' || p_study || ')';
                  ELSE
                    v_sql := v_sql || '  , er_study where fk_study  = pk_study and fk_account = ' || p_account;
                  END IF;
                  v_milesql := ' select sum(ach_count) achcount, sum(ach_count * milestone_amount) achamount , fk_study   from (' || v_sql || ') group by fk_study';
                  v_main_sql := ' SELECT PK_STUDY,study_division,(select codelst_desc from er_codelst where pk_codelst = study_division) study_division_desc,
                           fk_codelst_tarea, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) fk_codelst_tarea_desc,
                           fk_codelst_restype, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) fk_codelst_restype_desc,
                           DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor)) study_sponsor
                           ,study_prinv,Usr_Lst(study_prinv)  study_prinv_name,
                            ( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = (Pkg_Studystat.f_getLatestStudyStatus(pk_study))) studystat_desc,
                           study_number,   NVL(achcount,0) achcount,  NVL(achamount,0) achamount
                           FROM  ER_STUDY s, (' || v_milesql || ' )  m  WHERE m.fk_study = pk_study AND s.fk_account = ' || p_account;
                  IF LENGTH(p_study) > 0 THEN
                    v_main_sql := v_main_sql || '  and fk_study in ( ' || p_study || ')';
                  END IF;
                  IF LENGTH(p_division) > 0 THEN
                    v_main_sql := v_main_sql || '  and study_division in   ( ' || p_division || ')';
                  END IF;
                  IF LENGTH(p_tarea) > 0 THEN
                    v_main_sql := v_main_sql || '  and fk_codelst_tarea  in   ( ' || p_tarea || ')';
                  END IF;
                  IF LENGTH(p_restype) > 0 THEN
                    v_main_sql := v_main_sql || '  and fk_codelst_restype  in   ( ' || p_restype || ')';
                  END IF;
                  IF LENGTH(p_sponsor) > 0 THEN
                    v_main_sql := v_main_sql || '  and lower(decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) ) like  ''%' || LOWER(p_sponsor) || '%''';
                  END IF;
                  IF LENGTH(p_studystatus) > 0 THEN
                    v_main_sql := v_main_sql || '    and pkg_studystat.f_getLatestStudyStatus(pk_study)  in   ( ' || p_studystatus || ')';
                  END IF;
                  IF LENGTH(p_user) > 0 THEN
                    v_main_sql := v_main_sql || '  and (study_prinv in   ( ' || p_user || ') or study_prinv is null) ';
                  END IF;
                  v_main_sql := v_main_sql || ' ORDER By  study_division,  fk_codelst_tarea, study_number';
                  plog.DEBUG(pctx,   'v_main_sql forecast' || v_main_sql);
                  p_sxml := dbms_Xmlgen.getxml(v_main_sql);
                  RETURN p_sxml;
                END;
                FUNCTION f_get_mile_foredtl_xml(p_from_date IN VARCHAR2,   p_to_date IN VARCHAR2,   p_site IN VARCHAR2,   p_study IN VARCHAR2,   p_division IN VARCHAR2,   p_tarea IN VARCHAR2,   p_restype IN VARCHAR2,   p_sponsor IN VARCHAR2,   p_studystatus IN VARCHAR2,   p_user IN VARCHAR2,   p_patient IN VARCHAR2,   p_account IN NUMBER,   p_loggedinuser IN NUMBER) RETURN CLOB IS v_milesql CLOB;
                v_sql CLOB;
                v_main_sql CLOB;
                v_sql_mile CLOB;
                p_sxml CLOB;
                v_done number;
                v_from_date date;
                v_to_date date;
                BEGIN
                  -- loop through all milestones for forecast
                  v_from_date := to_date(p_from_date, PKG_DATEUTIL.F_GET_DATEFORMAT);
                  v_to_date := to_date(p_to_date, PKG_DATEUTIL.F_GET_DATEFORMAT);
                SELECT pk_codelst
                into v_done
                FROM sch_codelst
               WHERE TRIM (codelst_type) = 'eventstatus'
                 AND TRIM (codelst_subtyp) = 'ev_done';
                  v_sql := 'SELECT
            study_number,
            Pkg_Milestone_New.f_getMilestoneDesc( pK_MILESTONE) mile_desc ,
            (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = x.milestone_paytype) milestone_paytype_desc,
            per_code,(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
            (SELECT patprot_patstdid FROM ER_PATPROT WHERE fk_per = pk_per AND fk_study = pk_study AND patprot_stat = 1) AS pat_studyid,
            milestone_amount,TO_CHAR(actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT) AS actual_schdate,
            (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) study_division,
             (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) tarea,
             (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) restype,
            DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor))  study_sponsor,
            Usr_Lst(study_prinv)  study_prinv,
            (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = (Pkg_Studystat.f_getLatestStudyStatus(pk_study))) study_status
            FROM ER_MILESTONE x,sch_events1 y, ER_STUDY z, ER_PER P
            WHERE pk_study = x.fk_study AND pk_study IN (' || p_study || ') AND fk_cal = TO_NUMBER(session_id) and status = 0 AND
            actual_schdate BETWEEN  to_date(''' ||p_from_date|| ''', PKG_DATEUTIL.F_GET_DATEFORMAT) AND  to_date(''' ||p_to_date|| ''', PKG_DATEUTIL.F_GET_DATEFORMAT)  AND
            (y.fk_visit = x.fk_visit  OR fk_eventassoc = fk_assoc) AND
            ((milestone_type = ''VM'' OR milestone_type = ''EM'' ) and (milestone_eventstatus IS NULL
           OR milestone_eventstatus = '|| v_done ||' )) AND
            (fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type =''milestone_stat'' and codelst_subtyp=''A'')) AND pk_per = TO_NUMBER(patient_id) AND
            fk_site IN (' || p_site || ')  ';
                  IF LENGTH(p_division) > 0 THEN
                    v_sql := v_sql || '  and study_division in   ( ' || p_division || ')';
                  END IF;
                  IF LENGTH(p_tarea) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_tarea  in   ( ' || p_tarea || ')';
                  END IF;
                  IF LENGTH(p_restype) > 0 THEN
                    v_sql := v_sql || '  and fk_codelst_restype  in   ( ' || p_restype || ')';
                  END IF;
                  IF LENGTH(p_sponsor) > 0 THEN
                    v_sql := v_sql || '  and lower(decode(fk_codelst_sponsor,null,study_sponsor,( select codelst_desc from er_codelst where pk_codelst = fk_codelst_sponsor)) ) like  ''%' || LOWER(p_sponsor) || '%''';
                  END IF;
                  IF LENGTH(p_studystatus) > 0 THEN
                    v_sql := v_sql || '    and pkg_studystat.f_getLatestStudyStatus(pk_study)  in   ( ' || p_studystatus || ')';
                  END IF;
                  IF LENGTH(p_user) > 0 THEN
                    v_sql := v_sql || '  and (study_prinv in   ( ' || p_user || ') or study_prinv is null)  ';
                  END IF;
                  IF LENGTH(p_user) > 0 THEN
                    v_sql := v_sql || '  and pk_per in ( ' || p_patient || ')';
                  END IF;
                  v_sql := v_sql || ' ORDER By  study_division,  fk_codelst_tarea, study_number';
                  /*
            study_division IN (:studyDivId) AND
            fk_codelst_tarea IN (:tAreaId) AND
            fk_codelst_restype IN (:studyResType) AND
            study_sponsor LIKE ('%:studySponsor%') AND
            Pkg_Studystat.f_getLatestStudyStatus(pk_study) IN (:studyStatusId) AND
            study_prinv IN (:userId) AND
            pk_per IN (:patientId);
      v_sql:= '  select pkg_milestone_new.F_GET_MILE_FORECAST_COUNT(fk_study , codelst_subtyp, milestone_count,' || p_loggedinuser || ' ,''' || p_from_date || ''','''||
             p_to_date  || ''', fk_eventassoc,fk_visit, fk_cal) ach_count, milestone_amount, fk_study
               FROM ERV_MILEPAT_4CAST';
       IF    LENGTH(p_study) > 0 THEN
                        v_sql:= v_sql  || '  Where  fk_study in ( '  || p_study || ')';
    ELSE
                        v_sql:= v_sql  || '  , er_study where fk_study  = pk_study and fk_account = ' || p_account;
    END IF;
    v_milesql :=     ' select sum(ach_count) achcount, sum(ach_count * milestone_amount) achamount , fk_study   from (' || v_sql || ') group by fk_study';
    v_main_sql   := ' SELECT PK_STUDY,study_division,(select codelst_desc from er_codelst where pk_codelst = study_division) study_division_desc,
                           fk_codelst_tarea, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) fk_codelst_tarea_desc,
                           fk_codelst_restype, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) fk_codelst_restype_desc,
                           study_sponsor,study_prinv,Usr_Lst(study_prinv)  study_prinv_name,
                            ( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = (Pkg_Studystat.f_getLatestStudyStatus(pk_study))) studystat_desc,
                           study_number,   NVL(achcount,0) achcount,  NVL(achamount,0) achamount
                           FROM  ER_STUDY s, (' ||v_milesql || ' )  m  WHERE m.fk_study = pk_study AND s.fk_account = ' ||  p_account   ;
                            IF    LENGTH(p_study) > 0 THEN
                                        v_main_sql := v_main_sql  || '  and fk_study in ( '  || p_study || ')';
                            END IF;
                            IF    LENGTH( p_Division) > 0 THEN
                                        v_main_sql := v_main_sql || '  and study_division in   ( '  ||  p_Division || ')';
                            END IF;
                            IF    LENGTH( p_tarea) > 0 THEN
                                        v_main_sql := v_main_sql || '  and fk_codelst_tarea  in   ( '  ||  p_tarea || ')';
                            END IF;
                            IF    LENGTH( p_restype) > 0 THEN
                                        v_main_sql := v_main_sql || '  and fk_codelst_restype  in   ( '  ||  p_restype || ')';
                            END IF;
                          IF    LENGTH( p_Sponsor) > 0 THEN
                                    v_main_sql := v_main_sql || '  and lower(study_sponsor) like  ''%'  || LOWER( p_Sponsor) || '%''';
                        END IF;
                        IF    LENGTH(p_studystatus) > 0 THEN
                                    v_main_sql := v_main_sql  || '    and pkg_studystat.f_getLatestStudyStatus(pk_study)  in   ( '  ||     p_studystatus || ')';
                        END IF;
                        IF    LENGTH(p_user) > 0 THEN
                                    v_main_sql := v_main_sql || '  and study_prinv in   ( '  ||  p_user || ')';
                        END IF;
                        v_main_sql := v_main_sql ||  ' ORDER By  study_division,  fk_codelst_tarea, study_number' ;
*/ plog.DEBUG(pctx,   'v_main_sql:forecast details ' || v_sql);
                  p_sxml := dbms_Xmlgen.getxml(v_sql);
                  RETURN p_sxml;
                END;
                FUNCTION f_get_mile_forecast_count(p_study IN NUMBER,   p_rule IN VARCHAR2,   p_count IN NUMBER,   p_user IN NUMBER,   p_fromdate IN VARCHAR2,   p_todate IN VARCHAR2,   p_event IN VARCHAR2,   p_visit IN NUMBER,   p_cal IN NUMBER) RETURN NUMBER IS v_sql CLOB;
                v_todate DATE;
                v_fromdate DATE;
                v_patcount NUMBER;
                v_mileachieved NUMBER;
                BEGIN
                  v_todate := TO_DATE(p_todate,PKG_DATEUTIL.F_GET_DATEFORMAT);
                  v_fromdate := TO_DATE(p_fromdate,PKG_DATEUTIL.F_GET_DATEFORMAT);
                  IF(p_rule = 'em_4' OR p_rule = 'em_2') THEN
                    --even on scheduled date/ event status marked as-- ()
                    plog.DEBUG(pctx,   'sessionid = ' || LPAD(p_event,   10,   '0'));
                    SELECT COUNT(DISTINCT(ev.fk_patprot))
                    INTO v_patcount
                    FROM sch_events1 ev,
                      ER_PATPROT P
                    WHERE P.fk_study = p_study
                     AND pk_patprot = fk_patprot
                     AND patprot_stat = 1
                     AND fk_assoc = LPAD(p_event,   10,   '0')
                     AND status = 0
                     AND actual_schdate BETWEEN v_fromdate
                     AND v_todate
                     AND Pkg_User.f_chk_right_for_patprotsite(pk_patprot,   p_user) > 0;
                  END IF;
                  IF(p_rule = 'vm_4' OR p_rule = 'vm_2') THEN
                    --event on scheduled date/ event status marked as-- () for the visit
                    IF(p_visit > 0) THEN
                      SELECT COUNT(fk_patprot)
                      INTO v_patcount
                      FROM
                        (SELECT MAX(actual_schdate),
                           fk_patprot
                         FROM sch_events1 ev,
                           ER_PATPROT P
                         WHERE P.fk_study = p_study
                         AND pk_patprot = fk_patprot
                         AND patprot_stat = 1
                         AND fk_visit = p_visit
                         AND P.fk_protocol = p_cal
                         AND status = 0
                         AND Pkg_User.f_chk_right_for_patprotsite(pk_patprot,    p_user) > 0
                         GROUP BY fk_patprot HAVING MAX(actual_schdate) BETWEEN v_fromdate
                         AND v_todate)
                      ;
                    ELSE
                      SELECT COUNT(fk_patprot)
                      INTO v_patcount
                      FROM
                        (SELECT MAX(actual_schdate),
                           fk_patprot
                         FROM sch_events1 ev,
                           ER_PATPROT P
                         WHERE P.fk_study = p_study
                         AND pk_patprot = fk_patprot
                         AND patprot_stat = 1
                         AND P.fk_protocol = p_cal
                         AND status = 0
                         AND Pkg_User.f_chk_right_for_patprotsite(pk_patprot,    p_user) > 0
                         GROUP BY fk_patprot HAVING MAX(actual_schdate) BETWEEN v_fromdate
                         AND v_todate)
                      ;
                    END IF;
                  END IF;
                  IF(p_rule = 'vm_3') THEN
                    --atleast one event marked ()
                    IF(p_visit > 0) THEN
                      SELECT COUNT(fk_patprot)
                      INTO v_patcount
                      FROM
                        (SELECT MIN(actual_schdate),
                           fk_patprot
                         FROM sch_events1 ev,
                           ER_PATPROT P
                         WHERE P.fk_study = p_study
                         AND pk_patprot = fk_patprot
                         AND patprot_stat = 1
                         AND fk_visit = p_visit
                         AND P.fk_protocol = p_cal
                         AND status = 0
                         AND Pkg_User.f_chk_right_for_patprotsite(pk_patprot,    p_user) > 0
                         GROUP BY fk_patprot HAVING MAX(actual_schdate) BETWEEN v_fromdate
                         AND v_todate)
                      ;
                    ELSE
                      SELECT COUNT(fk_patprot)
                      INTO v_patcount
                      FROM
                        (SELECT MIN(actual_schdate),
                           fk_patprot
                         FROM sch_events1 ev,
                           ER_PATPROT P
                         WHERE P.fk_study = p_study
                         AND pk_patprot = fk_patprot
                         AND patprot_stat = 1
                         AND P.fk_protocol = p_cal
                         AND status = 0
                         AND Pkg_User.f_chk_right_for_patprotsite(pk_patprot,    p_user) > 0
                         GROUP BY fk_patprot HAVING MAX(actual_schdate) BETWEEN v_fromdate
                         AND v_todate)
                      ;
                    END IF;
                  END IF;
                  IF(p_count > 1) THEN
                    v_mileachieved := FLOOR(v_patcount / p_count);
                  ELSE
                    v_mileachieved := v_patcount;
                  END IF;
                  RETURN v_mileachieved;
                END;
                PROCEDURE sp_synch_inv_details(p_inv IN NUMBER,   p_user IN NUMBER,   p_ipadd IN VARCHAR2,   o_ret OUT NUMBER) AS
                v_milestone NUMBER;
                v_detail_count NUMBER;
                BEGIN
                  BEGIN
                    FOR i IN
                      (SELECT pk_invdetail,
                         fk_milestone,
                         amount_invoiced,
                         display_detail
                       FROM ER_INVOICE_DETAIL
                       WHERE fk_inv = p_inv
                       AND detail_type = 'H')
                    LOOP
                      IF(i.display_detail = 1) THEN
                        -- if display detail = 1, then change the header total with the sum of detail total
                        UPDATE ER_INVOICE_DETAIL
                        SET amount_invoiced =
                          (SELECT SUM(amount_invoiced)
                           FROM ER_INVOICE_DETAIL a
                           WHERE a.fk_milestone = i.fk_milestone
                           AND a.detail_type = 'D'
                           AND a.fk_inv = p_inv),
                          last_modified_by = p_user,
                          last_modified_date = SYSDATE,
                          ip_add = p_ipadd
                        WHERE pk_invdetail = i.pk_invdetail;
                      ELSE
                        -- display detail is off, so calculate the 'D' record's amount invoiced using the value of 'H' record
                        SELECT COUNT(pk_invdetail)
                        INTO v_detail_count
                        FROM ER_INVOICE_DETAIL a
                        WHERE a.fk_milestone = i.fk_milestone
                         AND a.detail_type = 'D'
                         AND a.fk_inv = p_inv;
                        IF(v_detail_count > 0) THEN
                          -- if display detail = 1, then change the header total with the sum of detail total
                          UPDATE ER_INVOICE_DETAIL c
                          SET c.amount_invoiced = ROUND((i.amount_invoiced / v_detail_count),   2),
                            last_modified_by = p_user,
                            last_modified_date = SYSDATE,
                            ip_add = p_ipadd
                          WHERE c.fk_milestone = i.fk_milestone
                           AND c.detail_type = 'D'
                           AND c.fk_inv = p_inv;
                        END IF;
                      END IF;
                      -- if display_detail =1
                    END LOOP;
                    COMMIT;
                    o_ret := 0;
                  EXCEPTION
                  WHEN OTHERS THEN
                    o_ret := -1;
                    plog.fatal(pctx,   'SP_SYNCH_INV_DETAILS exception' || SQLERRM);
                  END;
                END sp_synch_inv_details;


 PROCEDURE sp_create_vm_milestones(p_budget IN NUMBER,   p_study IN NUMBER,   p_user IN NUMBER,   p_ipadd IN VARCHAR2,   p_bgtcalid IN NUMBER,   p_rule IN NUMBER,   p_eventstatus IN NUMBER,   p_count IN NUMBER,   p_patstatus IN NUMBER,   p_limit IN NUMBER,   p_payment_type IN NUMBER,   p_paymentfor IN NUMBER,   o_ret OUT NUMBER) AS
                v_bgtcal NUMBER;
                v_protocol NUMBER;
                BEGIN
                  -- create milestones for all the visits in the budget's protocol calendar
                  v_bgtcal := p_bgtcalid;
                  SELECT bgtcal_protid
                  INTO v_protocol
                  FROM sch_bgtcal
                  WHERE pk_bgtcal = v_bgtcal;
                  --create milestones
  for k in (select decode(BGTSECTION_TYPE,'P',sum(nvl(TOTAL_COST_PER_PAT,0)),'O',sum(nvl(TOTAL_COST_ALL_PAT,0)),0) as amount,
            BUDGETSEC_FKVISIT,pk_budgetsec
            from erv_budget where pk_bgtcal = v_bgtcal and BUDGETSEC_FKVISIT is not null
        group by BUDGETSEC_FKVISIT,BGTSECTION_TYPE,pk_budgetsec)
  loop
                  INSERT
                  INTO ER_MILESTONE(pk_milestone,
                  fk_study,
                  milestone_type,
                  fk_cal,
                  fk_codelst_rule,
                  milestone_amount,
                  fk_eventassoc,
                  milestone_count,
                  milestone_delflag,
                  creator,   created_on,   ip_add,   fk_visit,   milestone_limit,   milestone_status,   milestone_paytype,   milestone_payfor,
                     milestone_eventstatus,   fk_codelst_milestone_stat,   fk_budget,   fk_bgtcal,   fk_bgtsection,   fk_lineitem)
                  SELECT seq_er_milestone.NEXTVAL,
                    p_study,
                    'VM',
                    v_protocol,
                    p_rule,
                    k.amount,
                    NULL,
                    p_count,
                    'N',
                    p_user,
                    SYSDATE,
                    p_ipadd,
                    k.BUDGETSEC_FKVISIT,
                    p_limit,
                    p_patstatus,
                    p_payment_type,
                    p_paymentfor,
                    p_eventstatus,
                    (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='WIP'), --KM--#D-FIN7
                    p_budget,
                    v_bgtcal,
                    k.pk_budgetsec,
                    NULL
                    from dual;
    end loop;
                  COMMIT;
                END sp_create_vm_milestones;

             PROCEDURE sp_create_em_milestones(p_budget IN NUMBER,   p_study IN NUMBER,   p_user IN NUMBER,   p_ipadd IN VARCHAR2,   p_bgtcalid IN NUMBER,   p_rule IN NUMBER,   p_eventstatus IN NUMBER,   p_count IN NUMBER,   p_patstatus IN NUMBER,   p_limit IN NUMBER,   p_payment_type IN NUMBER,   p_paymentfor IN NUMBER,   o_ret OUT NUMBER) AS
                v_bgtcal NUMBER;
                v_protocol NUMBER;
                BEGIN
                  -- create milestones for all the visits in the budget's protocol calendar
                  v_bgtcal := p_bgtcalid;
                  SELECT bgtcal_protid
                  INTO v_protocol
                  FROM sch_bgtcal
                  WHERE pk_bgtcal = v_bgtcal;
                  --create milestones
                  INSERT
                  INTO ER_MILESTONE(pk_milestone,   fk_study,   milestone_type,   fk_cal,   fk_codelst_rule,   milestone_amount,   fk_eventassoc,   milestone_count,   milestone_delflag,   creator,   created_on,   ip_add,   fk_visit,   milestone_limit,   milestone_status,   milestone_paytype,   milestone_payfor,   milestone_eventstatus,   fk_codelst_milestone_stat,   fk_budget,   fk_bgtcal,   fk_bgtsection,   fk_lineitem)
                  SELECT seq_er_milestone.NEXTVAL,
                    p_study,
                    'EM',
                    v_protocol,
                    p_rule,
                    decode(BGTSECTION_TYPE,'P',nvl(TOTAL_COST_PER_PAT,0),'O',nvl(TOTAL_COST_ALL_PAT,0),0),
                    lineitem_fkevent,
                    p_count,
                    'N',
                    p_user,
                    SYSDATE,
                    p_ipadd,
                    BUDGETSEC_FKVISIT,
                    p_limit,
                    p_patstatus,
                    p_payment_type,
                    p_paymentfor,
                    p_eventstatus,
                    (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='WIP'),
                    p_budget,
                    v_bgtcal,
                    pk_budgetsec,
                    pk_lineitem
                 from erv_budget where pk_bgtcal = v_bgtcal and BUDGETSEC_FKVISIT is not null and lineitem_fkevent is not null  ;
                  COMMIT;
                END sp_create_em_milestones;


                PROCEDURE   SP_CREATE_AM_MILESTONES(p_budget IN NUMBER, p_study IN NUMBER, p_user IN NUMBER,
                p_ipAdd IN VARCHAR2, p_payment_type IN NUMBER,
                 p_paymentfor IN NUMBER,  o_ret OUT NUMBER,p_bgtcalid NUMBER)
                 AS
                  v_protcount NUMBER;
                  v_sql LONG;
                 BEGIN
                 v_sql := ' INSERT   INTO ER_MILESTONE(pk_milestone,   fk_study,   milestone_type, ' ||
                    'fk_cal,   fk_codelst_rule,   milestone_amount,   fk_eventassoc,   milestone_count,   milestone_delflag,   '||
                    ' creator,   created_on,   ip_add,   fk_visit,   milestone_limit,   milestone_status,  milestone_paytype,   milestone_payfor, '||
                    '  milestone_eventstatus,   fk_codelst_milestone_stat,   fk_budget,   fk_bgtcal,   fk_bgtsection,   fk_lineitem,milestone_description) '||
                    ' SELECT seq_er_milestone.nextval, :study,''AM'', NULL, NULL, decode(BGTSECTION_TYPE,''P'',nvl(TOTAL_COST_PER_PAT,0),''O'',nvl(TOTAL_COST_ALL_PAT,0),0) , NULL, NULL,''N'', :p_user, :createdon,'||
                    ':p_ipadd,NULL,NULL,NULL,:p_payment_type,:p_paymentfor,NULL,(select pk_codelst from er_codelst where codelst_type =''milestone_stat'' and codelst_subtyp=''WIP''),:p_budget,:fk_bgtcal,pk_budgetsec,pk_lineitem,lineitem_name' ||
                      ' from erv_budget where pk_budget = :p_budget  and lineitem_fkevent is null and lineitem_name is not null ';

                   IF  NVL(p_bgtcalid,0) > 0 THEN
                       v_sql := v_sql || ' and pk_bgtcal = :p_bgtcalid ';
                   END IF;

                   IF  NVL(p_bgtcalid,0) > 0 THEN
                         EXECUTE IMMEDIATE v_sql USING  p_study,p_user,SYSDATE,p_ipadd,p_payment_type,p_paymentfor,p_budget,p_bgtcalid,p_budget,p_bgtcalid;
                   ELSE
                         EXECUTE IMMEDIATE v_sql USING  p_study,p_user,SYSDATE,p_ipadd,p_payment_type,p_paymentfor,p_budget,p_bgtcalid,p_budget;
                   END IF;
                  COMMIT;
                 END;
END Pkg_Milestone_New;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,80,3,'03_packages.sql',sysdate,'8.9.0 Build#537');

commit;