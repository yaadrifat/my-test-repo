ALTER TABLE ESCH.EVENT_ASSOC ADD (budget_Template NUMBER);

COMMENT ON COLUMN ESCH.EVENT_ASSOC.budget_Template IS 'This column represents budget template id for default budget.';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,80,1,'01_Alter.sql',sysdate,'8.9.0 Build#537');

commit;