create or replace
TRIGGER "ERES"."ER_ORDER_ATTACHMENTS_AU1" AFTER UPDATE ON eres.ER_ORDER_ATTACHMENTS 
referencing OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;
	
	pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER_ATTACHMENTS',:OLD.rid,:OLD.PK_ORDER_ATTACHMENT,'U',:NEW.LAST_MODIFIED_BY);
	
	IF NVL(:OLD.PK_ORDER_ATTACHMENT,0) != NVL(:NEW.PK_ORDER_ATTACHMENT,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'PK_ORDER_ATTACHMENT',:OLD.PK_ORDER_ATTACHMENT, :NEW.PK_ORDER_ATTACHMENT,NULL,NULL);
  END IF;
	 IF NVL(:OLD.FK_ORDER,0) !=  NVL(:NEW.FK_ORDER,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'FK_ORDER',:OLD.FK_ORDER, :NEW.FK_ORDER,NULL,NULL);
  END IF;
   IF NVL(:OLD.FK_ATTACHMENT,0) != NVL(:NEW.FK_ATTACHMENT,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'FK_ATTACHMENT',:OLD.FK_ATTACHMENT, :NEW.FK_ATTACHMENT,NULL,NULL);
  END IF;
	IF NVL(:OLD.CREATOR,0) !=   NVL(:NEW.CREATOR,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'CREATOR', :OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
  END IF;
   IF NVL(:OLD.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=  NVL(:NEW.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
 IF nvl(:OLD.last_modified_by,0) != nvl(:NEW.last_modified_by,0) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS','LAST_MODIFIED_BY',:OLD.last_modified_by,:NEW.last_modified_by,NULL,NULL);
      END IF;   
   IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) !=
     NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
   IF NVL(:OLD.IP_ADD,' ') !=  NVL(:NEW.IP_ADD,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF;  
  IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
  END IF;
  IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'RID',:OLD.RID, :NEW.RID,NULL,NULL);
  END IF;  
  END;
  /
  
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,174,26,'26_ER_ORDER_ATTACHMENTS_AU1.sql',sysdate,'9.0.0 Build#631');

commit;