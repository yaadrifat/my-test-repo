--START UPDATING TABLE CB_QUESTIONS--
DECLARE
  v_column_exists number := 0;
BEGIN
 Select count(*) into v_column_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_live_travel_country_ind';
  if (v_column_exists = 1) then
	execute immediate 'UPDATE CB_QUESTIONS SET REF_CHART=''chart_vCJD'' where QUES_CODE=''cjd_live_travel_country_ind''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
 Select count(*) into v_column_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spent_europe_ge_5y_cjd_cnt_ind';
  if (v_column_exists = 1) then
	execute immediate 'UPDATE CB_QUESTIONS SET REF_CHART=''chart_vCJD'' where QUES_CODE=''spent_europe_ge_5y_cjd_cnt_ind''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
 Select count(*) into v_column_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_1_2_o_performed_ind';
  if (v_column_exists = 1) then
	execute immediate 'UPDATE CB_QUESTIONS SET REF_CHART=''risk_HIV1'' where QUES_CODE=''hiv_1_2_o_performed_ind''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
 Select count(*) into v_column_exists
    from CB_QUESTIONS
    where QUES_CODE = 'born_live_cam_afr_nig_ind';
  if (v_column_exists = 1) then
	execute immediate 'UPDATE CB_QUESTIONS SET REF_CHART=''risk_HIV1'' where QUES_CODE=''born_live_cam_afr_nig_ind''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
 Select count(*) into v_column_exists
    from CB_QUESTIONS
    where QUES_CODE = 'trav_ctry_rec_bld_or_med_ind';
  if (v_column_exists = 1) then
	execute immediate 'UPDATE CB_QUESTIONS SET REF_CHART=''risk_HIV1'' where QUES_CODE=''trav_ctry_rec_bld_or_med_ind''';
  end if;
end;
/
--END--
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,174,4,'04_CB_QUESTION_UPDATE.sql',sysdate,'9.0.0 Build#631');

commit;
