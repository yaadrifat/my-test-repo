CREATE OR REPLACE TRIGGER "SCH_PROTOCOL_VISIT_BI1"
BEFORE INSERT
ON SCH_PROTOCOL_VISIT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
ntpd Number;
BEGIN
ntpd :=0;
Select F_SCH_CODELST_ID('timepointtype','NTPD') into ntpd from dual;
IF (:NEW.NO_INTERVAL_FLAG=ntpd) THEN
  :NEW.NO_INTERVAL_FLAG :=1;
ELSE
  :NEW.NO_INTERVAL_FLAG :=0;
END IF;
 IF NVL(:NEW.NO_INTERVAL_FLAG,0) = 1  THEN
    :NEW.displacement := null;
ELSIF (:NEW.INSERT_AFTER <0) THEN
   :NEW.NO_INTERVAL_FLAG:=1;
   :NEW.displacement := null;
ELSE
    IF NVL(:NEW.displacement ,0) = 0 THEN
		:NEW.displacement := 1;
    END IF;
  END IF;
  
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,168,1,'01_SCH_PROTOCOL_VISIT_BI1.sql',sysdate,'9.0.0 Build#625');

commit;

