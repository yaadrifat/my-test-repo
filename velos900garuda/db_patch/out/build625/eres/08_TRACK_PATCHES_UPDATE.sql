set define off;
begin
	UPDATE TRACK_PATCHES SET DB_PATCH_NAME ='06_INSERT_SCH_CODELST.sql'
	WHERE DB_VER_MJR=167 AND DB_VER_MNR=6;
  commit;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,168,8,'08_TRACK_PATCHES_UPDATE.sql',sysdate,'9.0.0 Build#625');

commit;