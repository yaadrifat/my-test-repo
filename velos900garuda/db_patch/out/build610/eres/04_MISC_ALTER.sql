set define off;


ALTER TABLE ER_ORDER ADD(FK_ORDER_HEADER NUMBER(10,0));


--STARTS ADDING COLUMN FK_ORDER_HEADER TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'FK_ORDER_HEADER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER ADD(FK_ORDER_HEADER NUMBER(10,0))';
  end if;
end;
/
--END--

--STARTS ADDING COLUMN FK_ORDER_HEADER TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_ORDER'
    AND column_name = 'FK_ORDER_HEADER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_ORDER ADD(FK_ORDER_HEADER NUMBER(10,0))';
  end if;
end;
/
--END--


--STARTS ADDING COLUMN FK_SHIPMENT_TYPE TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_SHIPMENT'
    AND column_name = 'FK_SHIPMENT_TYPE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_SHIPMENT ADD(FK_SHIPMENT_TYPE NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN CB_SHIPMENT.FK_SHIPMENT_TYPE IS 'Stores pk_codelst value for ''shipment_type''';


--STARTS ADDING COLUMN FK_PATLABS to CB_NOTES--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_NOTES'
    AND column_name = 'FK_PATLABS';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_NOTES 
ADD(FK_PATLABS NUMBER(10,0))';
  end if;
end;
/
--END--


COMMENT ON COLUMN CB_NOTES.FK_PATLABS IS 'Reference to ER_PATLABS';


--STARTS ADDING COLUMN FK_SITE TO ER_GRPS TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_GRPS'
    AND column_name = 'FK_SITE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.ER_GRPS ADD(FK_SITE NUMBER(10,0))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.ER_GRPS.FK_SITE IS 'Reference to ER_SITE';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,153,4,'04_MISC_ALTER.sql',sysdate,'9.0.0 Build#610');

commit;

