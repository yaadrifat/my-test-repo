set define off;
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols
    where TABLE_NAME = 'ER_STUDY_INDIDE'
    AND column_name = 'INDIDE_NUMBER';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE ER_STUDY_INDIDE MODIFY(INDIDE_NUMBER VARCHAR2(25 BYTE))';
  end if;
end;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,153,1,'01_ER_STUDY_INDIDE_ALTER.sql',sysdate,'9.0.0 Build#610');

commit;
