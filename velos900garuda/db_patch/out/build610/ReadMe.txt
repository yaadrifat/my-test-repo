#Build 610

=====================================================================================================
Garuda:

1. This Build(#610) includes an alter script for ER_GRPS table that is specific to Garuda.
DL04_MISC_ALTER.sql
In this script we are adding FK_SITE column in ER_GRPS table that is used to implement 
Group rights for Garuda.

2. The hibernate configuration file "hibernate.cfg.xml" has been renamed to 
"hibernate.garuda.cfg.xml" in 
server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib\garuda1_business-1.0.jar\

On the server, at the above location,
- Remove the existing "hibernate.cfg.xml"
- Keep the new "hibernate.garuda.cfg.xml"


=====================================================================================================
eResearch:

In this Build We have made partial release of CTRP20576_8(Validation Report) enhancement for 
INTERNAL REVIEW only. THIS IS NOT FOR QA . 


=====================================================================================================
eResearch Localization:

Copy these properties files (messageBundle.properties and labelBundle.properties) 
in ereshome\ and paste them to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

Following Files have been Modified:

1	aboutus.jsp
2	aboutusbak.jsp
3	accessdenied.jsp
4	accountbrowser.jsp
5	accountcreation.jsp
6	accountdelete.jsp
7	accountFormBrowser.jsp
8	accountforms.jsp
9	accountfreeze.jsp
10	accountlinkbrowser.jsp
11	accountreports.jsp
12	accountsave.jsp
13	accounttabs.jsp
14	accountUsers.jsp
15	accrepfilter.jsp
16	acctformdetails.jsp
17	activateuser.jsp
18	activateuservel.jsp
19	addbudgeturl.jsp
20	AddCost.jsp
21	addcrflinkforms.jsp
22	addeditquery.jsp
23	addevent.jsp
24	addeventcost.jsp
25	addeventfile.jsp
26	addeventinfo.jsp
27	addeventkit.jsp
28	addeventrole.jsp
29	addeventurl.jsp
30	addeventuser.jsp
31	addevtvisits.jsp
32	addevtvisitsave.jsp
33	addFieldToForm.jsp
34	addFieldToFormSubmit.jsp
35	addformtostudyacc.jsp
36	additempatient.jsp
37	additempatientback.jsp
38	additemstudy.jsp
39	addmileurl.jsp
40	addmultipleadvevent.jsp
41	addmultiplespecimens.jsp
42	addmultiplestorage.jsp
43	addNewQuery.jsp
44	addperurl.jsp
45	address.jsp
46	addspecimenurl.jsp
47	addstoragekit.jsp
48	addToCart.jsp
49	addUsersToMultipleStudies.jsp
50	adminauditreports.jsp
51	adminreports.jsp
52	adminreporttabs.jsp
53	bgtcaldelete.jsp
54	bgtnewsection.jsp
55	bgtprotocollist.jsp
56	blockedaccusers.jsp
57	bottompanel.jsp
58	budget.jsp
59	budget_bak.jsp
60	budgetapndx.jsp
61	budgetapndxdelete.jsp
62	budgetbrowser.jsp
63	budgetbrowserns.jsp
64	budgetbrowserpg.jsp
65	budgetdelete.jsp
66	budgetDoesNotExist.jsp
67	budgetfile.jsp
68	budgetfileupdate.jsp
69	budgetglobalusers.jsp
70	budgetrights.jsp
71	budgetsectiondelete.jsp
72	budgetsections.jsp
73	budgettabs.jsp
74	budgeturlsave.jsp
75	budgetuserrights.jsp
76	budgetusersdelete.jsp
77	budPaymentDetails.jsp
78	budrepretrieve.jsp
79	calculateGrade.jsp
80	calDoesNotExist.jsp
81	calendarstatus.jsp
82	calLibraryDelete.jsp
83	calMileStoneJSON.jsp
84	calMilestoneSetup.jsp
85	calpreview.jsp
86	calrepretrieve.jsp
87	caltolibrary.jsp
88	caslogin.jsp
89	category.jsp
90	categoryselectus.jsp
91	catLibDelete.jsp
92	labelBundle.properties
93	LC.java
94	LC.jsp
95	MC.java
96	MC.jsp
97	messageBundle.properties
98	patientbudget.jsp
=====================================================================================================
