--bug #6543 OLD DATA PATCH

UPDATE ESCH.SCH_PROTOCOL_VISIT SET DISPLACEMENT = NULL
WHERE NVL(NO_INTERVAL_FLAG,0)=1 AND DISPLACEMENT IS NOT NULL;

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,4,'04_583b_Bug6543_old_data_patch.sql',sysdate,'9.0.0 Build#594');

commit;