SET DEFINE OFF;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'pf_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'pf_menu', 'top_menu', 9, 
      0, 'PRODUCT FULLFILLMENT', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting pf_menu for top_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'manage_org' and OBJECT_NAME = 'pf_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'manage_org', 'pf_menu', 1, 
        0, 'Manage Organizations', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting Manage Organization for pf_menu already exists');
  end if;
END;
/
------------------------------------------------------Other Link--------------------------------------------------
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'pf_menu_ext' and OBJECT_NAME = 'pf_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'pf_menu_ext', 'pf_menu', 2, 
        0, 'PF Other Links', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting PF Other Links for pf_menu_ext already exists');
  end if;
END;
/
------------------------------------------------------Other Link--------------------------------------------------
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'cbb' and OBJECT_NAME = 'manage_org';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'cbb', 'manage_org', 1, 
        0, 'CBB', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cbb for manage_org already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'cbb_add' and OBJECT_NAME = 'cbb';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'cbb_add', 'cbb', 1, 
        0, ' Manage CBB', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cbb_add for pf_menu already exists');
  end if;
END;
/



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'cbb_proc' and OBJECT_NAME = 'cbb';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'cbb_proc', 'cbb', 2, 
        0, 'CBB Procedures', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cbb_proc for pf_menu already exists');
  end if;
END;
/


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'user_prf' and OBJECT_NAME = 'pf_menu_ext';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'user_prf', 'pf_menu_ext', 3, 
        0, 'User Profile', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting user_prf for pf_menu already exists');
  end if;
END;
/


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'prdt_fullfill' and OBJECT_NAME = 'pf_menu_ext';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'prdt_fullfill', 'pf_menu_ext', 4, 
        0, 'Product Fullfillment', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting prdt_fullfill for pf_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'opn_task' and OBJECT_NAME = 'pf_menu_ext';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'opn_task', 'pf_menu_ext', 5, 
        0, 'Open Task', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting opn_task for pf_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'pending_ordr' and OBJECT_NAME = 'pf_menu_ext';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'pending_ordr', 'pf_menu_ext', 6, 
        0, 'Pending Orders', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting pending_ordr for pf_menu already exists');
  end if;
END;
/

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'review_unit_report' and OBJECT_NAME = 'cbm_menu';
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
       (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
        OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
       (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'review_unit_report', 'cbm_menu', 5, 
        0, 'Unit Report', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting review_unit_report for cbm_menu already exists');
  end if;
END;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,137,7,'07_ER_Object_Setting_insert.sql',sysdate,'9.0.0 Build#594');

commit;