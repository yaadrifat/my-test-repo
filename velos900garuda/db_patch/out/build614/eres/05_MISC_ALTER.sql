set define off;

--ALTER THE CB_CORD TABLE--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'IDM_CMS_APPROVED_LAB';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(IDM_CMS_APPROVED_LAB varchar2(20))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'IDM_FDA_LICENSED_LAB';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(IDM_FDA_LICENSED_LAB varchar2(20))';
  end if;
end;
/
--END--

--ALTER THE ER_LABTEST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_LABTEST
    where  LABTEST_NAME = 'HIV I/II' AND LABTEST_SHORTNAME = 'HIVINII';

  if (v_column_exists = 1) then
      execute immediate 'DELETE FROM ER_LABTEST where  LABTEST_NAME = ''HIV I/II'' AND LABTEST_SHORTNAME = ''HIVINII''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_LABTEST
    where  LABTEST_NAME = 'HBsAg' AND LABTEST_SHORTNAME = 'HB';

  if (v_column_exists = 1) then
      execute immediate 'DELETE FROM ER_LABTEST where  LABTEST_NAME = ''HBsAg'' AND LABTEST_SHORTNAME = ''HB''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_LABTEST
    where  LABTEST_NAME = 'HCV' AND LABTEST_SHORTNAME = 'HCV';

  if (v_column_exists = 1) then
      execute immediate 'DELETE FROM ER_LABTEST where  LABTEST_NAME = ''HCV'' AND LABTEST_SHORTNAME = ''HCV''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_LABTEST
    where  LABTEST_NAME = 'T cruzi / Chagas' AND LABTEST_SHORTNAME = 'TCRUZI';

  if (v_column_exists = 1) then
      execute immediate 'DELETE FROM ER_LABTEST where  LABTEST_NAME = ''T cruzi / Chagas'' AND LABTEST_SHORTNAME = ''TCRUZI''';
  end if;
end;
/

--STARTS TO ALTER THE CB_ASSESSMENT TABLE --
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_ASSESSMENT'
    AND column_name = 'ASSESSMENT_POSTEDBY';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_ASSESSMENT ADD(ASSESSMENT_POSTEDBY NUMBER(10))';
  end if;
end;
/

COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_POSTEDBY" IS 'Storing the ASSESSMENT creator SITEID.';

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_ASSESSMENT'
    AND column_name = 'ASSESSMENT_POSTEDON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_ASSESSMENT ADD(ASSESSMENT_POSTEDON DATE)';
  end if;
end;
/
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_POSTEDON" IS 'Storing the ASSESSMENT creation Date.';

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_ASSESSMENT'
    AND column_name = 'ASSESSMENT_CONSULTBY';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_ASSESSMENT ADD(ASSESSMENT_CONSULTBY NUMBER(10))';
  end if;
end;
/
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_CONSULTBY" IS 'Storing the ASSESSMENT consult SITEID.';
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_ASSESSMENT'
    AND column_name = 'ASSESSMENT_CONSULTON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_ASSESSMENT ADD(ASSESSMENT_CONSULTON DATE)';
  end if;
end;
/
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_CONSULTON" IS 'Storing the ASSESSMENT consult Date.';
--END--

alter table cb_funding_guidelines rename column last_modified_on to LAST_MODIFIED_DATE;

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,5,'05_MISC_ALTER.sql',sysdate,'9.0.0 Build#614');

commit;