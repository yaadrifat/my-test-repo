create or replace
TRIGGER "ERES".CB_CORD_BI0 BEFORE INSERT ON CB_CORD
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_CORD',erid, 'I',usr);
       insert_data:=:NEW.PK_CORD || '|' ||		
to_char(:NEW.CORD_BABY_BIRTH_DATE) || '|' ||	
to_char(:NEW.CORD_REGISTRY_ID) || '|' ||	
to_char(:NEW.CORD_LOCAL_CBU_ID) || '|' ||	
to_char(:NEW.FK_CORD_BACT_CUL_RESULT) || '|' ||	
to_char(:NEW.FK_CORD_FUNGAL_CUL_RESULT) || '|' ||	
to_char(:NEW.FK_CORD_ABO_BLOOD_TYPE) || '|' ||	
to_char(:NEW.FK_CORD_RH_TYPE) || '|' ||	
to_char(:NEW.FK_CORD_BABY_GENDER_ID) || '|' ||	
to_char(:NEW.FK_CORD_CBU_LIC_STATUS) || '|' ||
to_char(:NEW.CORD_ISBI_DIN_CODE) || '|' ||	
to_char(:NEW.FK_CBB_ID) || '|' ||	
to_char(:NEW.FK_CORD_CBU_ELIGIBLE_STATUS) || '|' ||	
to_char(:NEW.FK_CORD_CBU_UNLICENSED_REASON) || '|' ||	
to_char(:NEW.REGISTRY_MATERNAL_ID) || '|' ||		
to_char(:NEW.MATERNAL_LOCAL_ID) || '|' ||		
to_char(:NEW.FK_CODELST_ETHNICITY) || '|' ||
to_char(:NEW.CORD_CREATION_DATE) || '|' ||			
to_char(:NEW.MULTIPLE_BIRTH) || '|' ||		
to_char(:NEW.FRZ_DATE) || '|' ||		
to_char(:NEW.HEMOGLOBIN_SCRN) || '|' ||	
to_char(:NEW.CORD_NMDP_MATERNAL_ID) || '|' ||		
to_char(:NEW.GUID) || '|' ||		
to_char(:NEW.FK_CBB_PROCEDURE) || '|' ||		
to_char(:NEW.CBB_PROCEDURE_START_DATE) || '|' ||		
to_char(:NEW.CBB_PROCEDURE_END_DATE) || '|' ||		
to_char(:NEW.FK_CBU_STOR_LOC) || '|' ||		
to_char(:NEW.FK_CBU_COLL_SITE) || '|' ||		
to_char(:NEW.FK_CBU_COLL_TYPE) || '|' ||		
to_char(:NEW.FK_CBU_DEL_TYPE) || '|' ||		
to_char(:NEW.BACT_COMMENT) || '|' ||		
to_char(:NEW.FUNG_COMMENT);      
    INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,9,'09_CB_CORD_BI0.sql',sysdate,'9.0.0 Build#614');

commit;