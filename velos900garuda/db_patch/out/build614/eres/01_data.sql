set define off;

delete from er_repxsl where pk_repxsl = 43;

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,1,'01_data.sql',sysdate,'9.0.0 Build#614');

commit;