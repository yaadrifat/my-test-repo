CREATE TABLE CB_FUNDING_REPORT(
				PK_FUNDING_REPORT NUMBER(10) Primary Key,
				FK_CBB_ID NUMBER(10),
				FK_REPORT_STATUS number(10),
				SCHEDULE_ON DATE,
				CREATOR NUMBER(10),
				CREATED_ON DATE,
				IP_ADD VARCHAR2(15),
				LAST_MODIFIED_BY NUMBER(10),
				LAST_MODIFIED_DATE DATE,
				RID NUMBER(10),
				DELETEDFLAG VARCHAR2(1)
				);

COMMENT ON TABLE "CB_FUNDING_REPORT" IS 'This table will store the Funding Reports';
COMMENT ON COLUMN "CB_FUNDING_REPORT"."PK_FUNDING_REPORT" IS 'Primary Key of the table'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."FK_CBB_ID" IS 'Reference to CBB'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."FK_REPORT_STATUS" IS 'This column stores the report status possible values defined in code list'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."SCHEDULE_ON" IS 'Schedule date of funding'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."LAST_MODIFIED_DATE" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_FUNDING_REPORT"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE "SEQ_CB_FUNDING_REPORT" MINVALUE 1 MAXVALUE 999999999999999999999999999 
INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;


CREATE TABLE CB_GENRATED_REPORT_DETAILS(
				PK_GENRATED_REPORT_DETAILS NUMBER(10) Primary Key,
				FK_CORD_ID NUMBER(10),
				TNC NUMBER(10),
				FUNDED varchar2(1),
				REQUEST_FUNDED varchar2(1),
				FK_REPORT_ID NUMBER,
				CREATOR NUMBER(10),
				CREATED_ON DATE,
				IP_ADD VARCHAR2(15),
				LAST_MODIFIED_BY NUMBER(10),
				LAST_MODIFIED_DATE DATE,
				RID NUMBER(10),
				DELETEDFLAG VARCHAR2(1)
				);

COMMENT ON TABLE "CB_GENRATED_REPORT_DETAILS" IS 'This table will store the  Reports Details of CBU';
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."PK_GENRATED_REPORT_DETAILS" IS 'Primary Key of the table'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."FK_CORD_ID" IS 'Reference to CB_CORD'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."FUNDED" IS 'This column stores wheather cord is funded or not.'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."REQUEST_FUNDED" IS 'This column will store request for funding'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."LAST_MODIFIED_DATE" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_GENRATED_REPORT_DETAILS"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE "SEQ_CB_GENRATED_REPORT_DETAILS" MINVALUE 1 MAXVALUE 999999999999999999999999999 
INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;

CREATE TABLE CB_DATA_MODI_REQUEST(
	PK_DATA_MODI_REQUEST NUMBER(10,0),
	REQUEST_TYPE NUMBER(10,0),
	REQUEST_TITLE VARCHAR2(50 CHAR),
	REQUEST_DUE_DATE DATE,
	REQUEST_DESC VARCHAR2(200 CHAR),
	REQUEST_REASON VARCHAR2(200 CHAR),
	AFFECTED_FIELDS VARCHAR2(100 CHAR),
	CBB_PROCESS_CHANGE VARCHAR2(100 CHAR),
	FK_SITEID NUMBER(10,0),
	REQUEST_APPROVAL_STATUS NUMBER(10,0),
	RID NUMBER,
	CREATOR NUMBER,
	LAST_MODIFIED_BY NUMBER,
	LAST_MODIFIED_DATE DATE,
	CREATED_ON DATE,
	IP_ADD VARCHAR2(15 BYTE),
	DELETEDFLAG NUMBER
);

COMMENT ON TABLE CB_DATA_MODI_REQUEST IS 'Stores data modification requests information';
comment on column cb_data_modi_request.pk_data_modi_request is 'Primary Key of the table';
comment on column cb_data_modi_request.affected_fields is 'Specifying Affected Fields';
comment on column cb_data_modi_request.cbb_process_change is 'CBB Process Change';
comment on column cb_data_modi_request.created_on is 'Date of Creation';
comment on column cb_data_modi_request.creator is 'Pk value User who created';
comment on column cb_data_modi_request.deletedflag is 'Deleted Flag';
comment on column cb_data_modi_request.fk_siteid is 'Foreign key for specifiying siteId';
comment on column cb_data_modi_request.ip_add is 'IP address';
comment on column cb_data_modi_request.last_modified_by is 'User who last modified';
comment on column cb_data_modi_request.last_modified_date is 'Last Modified Date';
comment on column cb_data_modi_request.request_approval_status is 'State of Approval refers ER_Codelst table';
comment on column cb_data_modi_request.request_desc is 'Description of Request';
comment on column cb_data_modi_request.request_due_date is 'Due date for request';
comment on column cb_data_modi_request.request_reason is 'Reason for request';
comment on column cb_data_modi_request.request_title is 'Data Request Title';
comment on column cb_data_modi_request.request_type is 'Request Type refers ER_Codelst table';
comment on column cb_data_modi_request.rid is 'Record Id';





CREATE SEQUENCE "ERES"."SEQ_CB_DATA_MODI_REQUEST" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;
Commit;

-- Create User-Site-Group  Table
DECLARE
  v_table_exists number := 0;  
BEGIN
	Select count(*) into v_table_exists
    from USER_TABLES
    where TABLE_NAME = 'ER_USRSITE_GRP'; 

	if (v_table_exists = 0) then
		execute immediate 'CREATE TABLE ER_USRSITE_GRP (PK_USR_SITE_GRP NUMBER(10) Primary Key,
					FK_GRP_ID NUMBER(10),
					FK_USER_SITE NUMBER(10),
					FK_USER NUMBER(10),
					CREATOR NUMBER(10),
					CREATED_ON DATE,
					IP_ADD VARCHAR2(15),
					LAST_MODIFIED_BY NUMBER(10),
					LAST_MODIFIED_DATE DATE,
					RID NUMBER(10),
					DELETEDFLAG VARCHAR2(1))';
		dbms_output.put_line('Table ER_USRSITE_GRP is created in eres schema');
	else
		dbms_output.put_line('Table ER_USRSITE_GRP exists in eres schema');
	end if;
end;
/




COMMENT ON TABLE "ER_USRSITE_GRP" IS 'This table will stores information of User-Site-Group';
COMMENT ON COLUMN "ER_USRSITE_GRP"."PK_USR_SITE_GRP" IS 'Primary Key of the table'; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."FK_GRP_ID" IS 'Reference to ER_GRPS'; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."FK_USER_SITE" IS 'Reference to ER_USERSITE';  
COMMENT ON COLUMN "ER_USRSITE_GRP"."FK_USER" IS 'Reference to ER_USER'; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."LAST_MODIFIED_DATE" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "ER_USRSITE_GRP"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 

DECLARE
  v_table_exists number := 0;  
BEGIN
	SELECT COUNT(*) into v_table_exists
	FROM USER_SEQUENCES 
	WHERE SEQUENCE_NAME='SEQ_ER_USRSITE_GRP';

	if (v_table_exists = 0) then
		execute immediate 'CREATE  SEQUENCE SEQ_ER_USRSITE_GRP MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1';
		dbms_output.put_line('Sequence SEQ_ER_USRSITE_GRP is created in eres schema');
	else
		dbms_output.put_line('Sequence SEQ_ER_USRSITE_GRP exists in eres schema');
	end if;
end;
/
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,3,'03_CREATE_TABLE.sql',sysdate,'9.0.0 Build#614');

commit;


------------------End-----------------
													
			