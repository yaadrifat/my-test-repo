set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '8.8.0 Build#525' 
where CTRL_KEY = 'app_version' ; 

commit;
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,68,0,'00_er_version.sql',sysdate,'8.8.0 Build#525');

commit;
