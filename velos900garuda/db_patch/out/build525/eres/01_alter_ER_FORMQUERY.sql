alter table ER_FORMQUERY modify (
LAST_MODIFIED_DATE Date default (null)
);


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,68,1,'01_alter_ER_FORMQUERY.sql',sysdate,'8.8.0 Build#525');

commit;
