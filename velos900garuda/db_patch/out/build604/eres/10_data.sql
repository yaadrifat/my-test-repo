Set define off;

UPDATE ER_OBJECT_SETTINGS SET OBJECT_VISIBLE=1 WHERE OBJECT_SUBTYPE='regreporting_menu' AND OBJECT_NAME='top_menu';

UPDATE ER_OBJECT_SETTINGS SET OBJECT_VISIBLE=1 WHERE OBJECT_SUBTYPE='ctrp_reporting' AND OBJECT_NAME='regreporting_menu';

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,147,10,'10_data.sql',sysdate,'9.0.0 Build#604');

commit;