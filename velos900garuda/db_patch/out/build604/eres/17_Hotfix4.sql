--STARTS ADDING COLUMN OTHER_BAG_TYPE TO CBB_PROCESSING_PROCEDURES_INFO--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTHER_BAG_TYPE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(OTHER_BAG_TYPE VARCHAR2(30 CHAR))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.OTHER_BAG_TYPE IS 'Stores other bag type description';


--STARTS ADDING COLUMN DELETEDFLAG TO CB_UPLOAD_INFO--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_UPLOAD_INFO'
    AND column_name = 'DELETEDFLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_UPLOAD_INFO ADD(DELETEDFLAG VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CB_UPLOAD_INFO.DELETEDFLAG IS 'Stores whether column is deleted';


create or replace
FUNCTION ROWTOCOL ( p_slct IN VARCHAR2,p_dlmtr IN VARCHAR2 DEFAULT ',' ) RETURN VARCHAR2 AUTHID CURRENT_USER AS
     TYPE c_refcur IS REF CURSOR;
     lc_str VARCHAR2(4000);
     lc_colval VARCHAR2(4000);
     c_dummy c_refcur;
     l number;
BEGIN
     OPEN c_dummy FOR p_slct;
     LOOP
     FETCH c_dummy INTO lc_colval;
     EXIT WHEN c_dummy%NOTFOUND;
     lc_str := lc_str || p_dlmtr || lc_colval;
     END LOOP;
     CLOSE c_dummy;
     RETURN SUBSTR(lc_str,2);
     EXCEPTION
     WHEN OTHERS THEN
     lc_str := SQLERRM;
     IF c_dummy%ISOPEN THEN
     CLOSE c_dummy;
     END IF;
     RETURN lc_str;
END; 
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,147,17,'17_Hotfix4.sql',sysdate,'9.0.0 Build#604');

commit;