set define off;

update ER_OBJECT_SETTINGS set OBJECT_TYPE = 'M', OBJECT_SEQUENCE = 16, OBJECT_NAME = 'report_menu' where
OBJECT_SUBTYPE = 'dash_menu';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,147,12,'12_object_settings.sql',sysdate,'9.0.0 Build#604');

commit;