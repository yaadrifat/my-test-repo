/* This readMe is specific to Velos eResearch version 9.0 build #633 */

=====================================================================================================================================
Garuda :

Important Note:

	Please update the server/eresearch/deploy/velos.ear/velos.war/jsp/xml/dcmsAttachment.xml file according to the below conditions

	1) If DCMS server is running on http
		<dcmsIpAddress>
			http://<IP ADDRESS>:<PORT NUMBER>
		</dcmsIpAddress>
	2)  If DCMS server is running on https
		<dcmsIpAddress>
			https://<IP ADDRESS>:<PORT NUMBER>
		</dcmsIpAddress>

	     
=====================================================================================================================================
eResearch:


=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. patientdetails.jsp
2. formfilledpatbrowser.jsp
3. visitmilestone.jsp
4. eventmilestone.jsp
5. reportsinstudy.jsp
6. patientreports.jsp
=====================================================================================================================================
