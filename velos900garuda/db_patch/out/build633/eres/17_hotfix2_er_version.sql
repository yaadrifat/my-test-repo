set define off;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,176,17,'17_hotfix2_er_version.sql',sysdate,'9.0.0 Build#633');

commit;

 
UPDATE TRACK_PATCHES SET DB_VER_MJR = 176, DB_VER_MNR = 16 WHERE 
DB_PATCH_NAME = '16_hotfix1_er_version.sql' AND APP_VERSION = '9.0.0 Build#633';

commit;