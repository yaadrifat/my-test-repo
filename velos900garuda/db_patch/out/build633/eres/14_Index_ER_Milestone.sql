set define off;

DECLARE
  numCount NUMBER default 0; 
BEGIN 
	SELECT COUNT(*) INTO numCount 
	FROM ALL_INDEXES 
	WHERE TABLE_NAME ='ER_MILESTONE'
	AND INDEX_NAME = 'IDX_FKSTUDY_MILESTONE';
	
	IF (numCount = 0) THEN
		execute immediate 'CREATE INDEX "ERES"."IDX_FKSTUDY_MILESTONE" ON "ERES"."ER_MILESTONE" 
		("FK_STUDY") 
		LOGGING 
		NOPARALLEL';
		dbms_output.put_line ('Index created');
	ELSE
		dbms_output.put_line ('Index not created');
	END IF;
end;
/
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,176,14,'14_Index_ER_Milestone.sql',sysdate,'9.0.0 Build#633');

commit;
