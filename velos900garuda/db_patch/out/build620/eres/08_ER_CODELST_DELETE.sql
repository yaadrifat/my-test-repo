--STARTS DELETING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'negi';
  if (v_record_exists = 1) then
     delete from ER_CODELST where CODELST_TYPE='hem_path_scrn' and CODELST_SUBTYP='negi';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'homozygous';
  if (v_record_exists = 1) then
     delete from ER_CODELST where CODELST_TYPE='hem_path_scrn' and CODELST_SUBTYP='homozygous';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'hetrozygous';
  if (v_record_exists = 1) then
     delete from ER_CODELST where CODELST_TYPE='hem_path_scrn' and CODELST_SUBTYP='hetrozygous';
	commit;
  end if;
end;
/

--END--


--STARTS DELETING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert';
  if (v_record_exists > 1) then
	DELETE FROM ER_CODELST where codelst_type = 'alert';
	commit;
  end if;
end;
/
--END--

--COMMENT FOR ER_PATLABS TABLE --
COMMENT ON COLUMN "ER_PATLABS"."CUSTOM003" IS 'Custom field - Used to store the Test Comments';
COMMENT ON COLUMN "ER_PATLABS"."CUSTOM008" IS 'Custom field - Used to store the Test Count';
commit;
--END--

--COMMENT FOR ER_PATLABS TABLE --
COMMENT ON COLUMN "CB_CORD"."IDM_CMS_APPROVED_LAB" IS 'This Column will store the IDM Test Common Question1';
COMMENT ON COLUMN "CB_CORD"."IDM_FDA_LICENSED_LAB" IS 'This Column will store the IDM Test Common Question2';
commit;
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,163,8,'08_ER_CODELST_DELETE.sql',sysdate,'9.0.0 Build#620');

commit;
