--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch')
    AND BROWSERCONF_COLNAME='STUDY_NUMBER';
  if (v_record_exists = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS='{"key":"STUDY_NUMBER", "label":"Study Number","sortable":true, "resizeable":true,"hideable":true,"format":"studyNumLink"}' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='STUDY_NUMBER';
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,163,6,'06_UPDATE_ER_BROWSERCONF.sql',sysdate,'9.0.0 Build#620');

commit;