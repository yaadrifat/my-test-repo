/* This readMe is specific to Velos eResearch version 9.0 build #620 */

=====================================================================================================================================

eResearch Localization:

Following Files have been Modified:

1	labelBundle.properties
2	LC.java
3	LC.jsp
4	MC.java
5	MC.jsp
6	messageBundle.properties
7	myHome.jsp
=====================================================================================================================================

eResearch :

1. For enhancements INF-22270 & INF-22237 the following two documents are released for QA reference.

1.INF-22270Ref document.xls
2.INF-22237-Reason-for-change SQLs for testing.xls

=====================================================================================================================================
