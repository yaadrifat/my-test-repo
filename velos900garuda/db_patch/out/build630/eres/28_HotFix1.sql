DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'hetrozygous �';
  if (v_record_exists = 1) then
	delete from er_codelst where codelst_type='hem_path_scrn' and codelst_subtyp='hetrozygous �';
	commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,173,28,'28_HotFix1.sql',sysdate,'9.0.0 Build#630');

commit;