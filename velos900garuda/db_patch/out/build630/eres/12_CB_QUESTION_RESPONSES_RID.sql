set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_QUESTION_RESPONSES WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_QUESTION_RESPONSES  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,173,12,'12_CB_QUESTION_RESPONSES_RID.sql',sysdate,'9.0.0 Build#630');

commit;