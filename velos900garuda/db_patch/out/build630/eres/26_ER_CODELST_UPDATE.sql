update er_codelst set codelst_desc='Availability Confirmation' where codelst_type='note_cat_pf' and codelst_subtyp='avail_cnfrm';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,173,26,'26_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#630');

commit;