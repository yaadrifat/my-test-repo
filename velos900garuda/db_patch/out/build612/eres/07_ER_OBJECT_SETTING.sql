set define off;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'cbu_tools_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'cbu_tools_menu', 'top_menu', 12, 0, 'CBU Tools', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting cbu_tools_menu for top_menu already exists');
  end if;
END;
/ 

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'app_msg_menu' and OBJECT_NAME = 'cbu_tools_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'app_msg_menu', 'cbu_tools_menu', 2, 0, 'Application Messages', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting app_msg_menu for cbu_tools_menu already exists');
  end if;
END;
/ 
commit;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'funding_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'funding_menu', 'top_menu', 5, 0, 'Funding', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting funding_menu for top_menu already exists');
  end if;
END;
/ 


Set define off;

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_OBJECT_SETTINGS'
    AND column_name = 'OBJECT_SUBTYPE';
  if (v_column_value_update =1) then
   UPDATE ER_OBJECT_SETTINGS SET OBJECT_TYPE='TM', OBJECT_NAME='top_menu',OBJECT_SUBTYPE='pending_ordr', OBJECT_SEQUENCE='10'
   WHERE OBJECT_NAME='pf_menu' AND OBJECT_TYPE='M' AND OBJECT_SUBTYPE='pending_ordr';
  end if;
end;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,155,7,'07_ER_OBJECT_SETTING.sql',sysdate,'9.0.0 Build#612');

commit;