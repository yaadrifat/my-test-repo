/* This readMe is specific to Velos eResearch version 9.0 build #612 */
=====================================================================================================================================
eResearch:

CTRP-
In this build we have changed names and data-types of few database columns in table ER_CTRP_DRAFT.
This has led to erasing data contained in these columns. 
This is a note to QA that, they will lose test data in following sections/columns, for CTRP Industrial drafts:

1. Submitting Organization
2. Summary4 Information


=========================================================================================================================

For Localization 

1. Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

2. Application server restart is needed. The change is not immediate.

=========================================================================================================================

eResearch Localization:

Following Files have been Modified:

1	calMilestoneSetup.js
2	calMilestoneSetup.js
3	coverage.js
4	createMultiMilestones.jsp
5	datagrid.js
6	ereslogin.jsp
7	eventmessage.jsp
8	labelBundle.properties
9	LC.java
10	LC.jsp
11	login.css
12	manageVisitsGrid.js
13	MC.java
14	MC.jsp
15	messageBundle.properties
16	milestonegrid.js
17	multipleChoiceBox.jsp
18	multiSpecimens.js
19	patientlogin.jsp
20	studybudget.jsp
21	studyschedule.jsp
22	subjectgrid.js

ereslogin.jsp and patientlogin.jsp files are changed for some values coming from database and properties file.
New ereslogin screens are implemented
=====================================================================================================================================
