DROP VIEW REP_ADV_MORE_DETAILS;

/* Formatted on 4/6/2009 3:43:59 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_ADV_MORE_DETAILS
(
   PK_MOREDETAILS,
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PAT_ID,
   PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   RESPONSE_ID,
   ADVE_TYPE,
   ADVE_NAME,
   AE_STDATE,
   STUDY_NUMBER,
   FK_STUDY
)
AS
   SELECT   PK_MOREDETAILS,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            pk_person,
            a.CREATED_ON,
            fk_account,
            PERson_CODE PAT_ID,
            (PERSON_FNAME || ' ' || PERSON_LNAME) PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID,
            (SELECT   codelst_desc
               FROM   SCH_CODELST
              WHERE   pk_codelst = fk_codlst_aetype)
               adve_type,
            ae_name,
            ae_stdate,
            (SELECT   study_number
               FROM   er_study
              WHERE   pk_study = fk_study)
               study_number,
            fk_study
     FROM   er_moredetails a, person, sch_adverseve
    WHERE       a.MD_MODNAME = 'advtype'
            AND FK_MODPK = pk_adveve
            AND pk_person = fk_per;
COMMENT ON TABLE REP_ADV_MORE_DETAILS IS 'view for adverse events more details';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.PAT_ID IS 'Patient Code for a facility';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.PAT_NAME IS 'Patient Name';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.CREATOR IS 'The user who created the record';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.LAST_MODIFIED_DATE IS 'The date record was last modified on';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.RID IS 'Database internal RID (used for audit trail)';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.RESPONSE_ID IS 'Primary Key of er_moredetails';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.ADVE_TYPE IS 'Adverse Event type';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.ADVE_NAME IS 'Adverse Event name';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.AE_STDATE IS 'Adverse Event Start Date';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.STUDY_NUMBER IS 'Study number of the study linked with the lab test (if any)';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FK_STUDY IS 'Study PK of the study linked with the lab test (if any)';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.PK_MOREDETAILS IS 'Primary Key of er_moredetails';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FIELD_NAME IS 'More Details Field Name';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FIELD_VALUE IS 'More Details Field Value';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FK_PER IS 'FK to ER_PER. Identifies the patient';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.CREATED_ON IS 'The date record was created';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FK_ACCOUNT IS 'FK to er_account. Identifies the account the record is linked with';


