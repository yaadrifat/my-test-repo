set define off;

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'SGMT';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_OF_SEG_AVAIL' where codelst_type = 'cbu_sample_type'AND codelst_subtyp = 'SGMT';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'FP';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='FILT_PAP_AVAIL' where codelst_type = 'cbu_sample_type'AND codelst_subtyp = 'FP';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'FPFS';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='CBU_FILT_PAP' where codelst_type = 'cbu_sample_type'AND codelst_subtyp = 'FPFS';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'RBCP';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='RBC_PEL_AVAIL' where codelst_type = 'cbu_sample_type'AND codelst_subtyp = 'RBCP';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'CBU';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Whole Cord Blood' where codelst_type = 'cbu_sample_type'AND codelst_subtyp = 'CBU';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'EDNA';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_EXT_DNA_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'EDNA';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'NVC';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_NON_VIA_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'NVC';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'SER';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_SER_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'SER';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'PLA';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_PLAS_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'PLA';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'VCEL';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_VIA_CEL_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'VCEL';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'MDNA';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_EXT_DNA_MAT_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'MDNA';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'MCEL';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_CEL_MAT_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'MCEL';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'MPLA';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_PLAS_MAT_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'MPLA';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'aliquots_type'
    AND codelst_subtyp = 'MSER';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL='CB_ENTITY_SAMPLES',CODELST_CUSTOM_COL1='NO_SER_MAT_ALI' where codelst_type = 'aliquots_type'AND codelst_subtyp = 'MSER';
	commit;
  end if;
end;
/
--END--

set define off;

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_assess'
    AND codelst_subtyp = 'tu';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Temporarily Unavailable' where codelst_type = 'note_assess' AND codelst_subtyp = 'tu';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'u.s_prior';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='U.S. Prior to May 25, 2005' where codelst_type = 'unlicensed' AND codelst_subtyp = 'u.s_prior';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'u.s_inel_scr';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='U.S. Ineligible - Donor Screening' where codelst_type = 'unlicensed' AND codelst_subtyp = 'u.s_inel_scr';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'u.s_inel_test';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='U.S. Ineligible - Donor Testing' where codelst_type = 'unlicensed' AND codelst_subtyp = 'u.s_inel_test';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'pre_lice';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='U.S. Eligible-pre-CBB licensure' where codelst_type = 'unlicensed' AND codelst_subtyp = 'pre_lice';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'post_lice';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='U.S. Eligible-post-CBB licensure' where codelst_type = 'unlicensed' AND codelst_subtyp = 'post_lice';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'licence'
    AND codelst_subtyp = 'unlicensed';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Unlicensed' where codelst_type = 'licence' AND codelst_subtyp = 'unlicensed';
	commit;
  end if;
end;
/
--END--

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_assess'
    AND codelst_subtyp = 'tu';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Temporarily Unavailable' where codelst_type = 'note_assess' AND codelst_subtyp = 'tu';
	commit;
  end if;
end;
/
--END--


