set define off;   


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'not_applicable';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'unlicensed','not_applicable','Not Applicable:  non-NMDP IND','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--
--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'incomplete';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'unlicensed','incomplete','U.S. Incomplete','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--SQL FOR THE CFU COUNT IN LAB SUMMARY
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cfu_cnt','total','Total(BFUE+BM+DEMM)','N',1,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cfu_cnt','gm','GM','N',2,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cfu_cnt','other','Other','N',3,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
       
  --SQL FOR THE VIABILITY IN LAB SUMMARY
  
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'vaibility','7aadt','7AADt','N',1,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'vaibility','acri_prop','Acridine Orange/Propidium Iodide','N',2,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'vaibility','ehi_brom','Ehidium Bromide','N',3,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'vaibility','try_blue','Trypan Blue','N',4,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'vaibility','other','Other','N',5,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   --STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'cordshipped';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_status','cordshipped','Cord Shipped','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

   
   commit;