update er_lkpcol set lkpcol_datatype ='clob' where lower(lkpcol_name) like '%clob';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,70,6,'06_BUG#2989_Fix_clob_columns.sql',sysdate,'8.9.0 Build#527');

commit;