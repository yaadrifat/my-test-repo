
ALTER TABLE ESCH.SCH_BUDGET ADD (BUDGET_COMBFLAG CHAR(1) DEFAULT NULL);

COMMENT ON COLUMN 
ESCH.SCH_BUDGET.BUDGET_COMBFLAG IS 
'This column indicates flag for being a combined budget.';

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,70,2,'02_Add SCH_BUDGET row.sql',sysdate,'8.9.0 Build#527');

commit;