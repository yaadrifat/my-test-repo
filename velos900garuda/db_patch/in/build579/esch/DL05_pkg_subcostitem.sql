set define off;
create or replace PACKAGE BODY      pkg_subcostitem AS
PROCEDURE sp_delete_visit_subCostItems (p_visitIds IN ARRAY_STRING, p_itemIds IN ARRAY_STRING,  usr IN NUMBER) as
/*
   Date : 23Jan2011
   Author: Sampada
   Purpose: Delete Subject cost items associated to a calendar
*/

v_cnt NUMBER;
i NUMBER;
v_itemId NUMBER;
v_visitId NUMBER;
BEGIN
   v_cnt := p_itemIds.COUNT;
   i:=1;

   WHILE i <= v_cnt LOOP
   	  v_visitId := to_number(p_visitIds(i));
      v_itemId := TO_NUMBER(p_itemIds(i));

      --KM -#5949
      execute immediate 'update sch_subcost_item_visit set last_modified_by = '|| usr ||' where  fk_subcost_item  = '||v_itemId ||' and fk_protocol_visit = '||v_visitId ;

      Delete from SCH_SUBCOST_ITEM_VISIT where FK_SUBCOST_ITEM = v_itemId and FK_PROTOCOL_VISIT = v_visitId;
      i := i+1;
   END LOOP;
END sp_delete_visit_subCostItems;
----------------------------------------------------------------------------------------------------------

PROCEDURE Sp_Copy_Protocol_SCItems(P_OLD_PROTOCOL_ID IN NUMBER, P_NEW_PROTOCOL_ID IN NUMBER, p_user IN NUMBER,
p_ip IN VARCHAR2, p_ret OUT NUMBER, p_Item_keys OUT NOCOPY tab_mapping)
IS
/******************************************************************************
 NAME:       Sp_Copy_Protocol_SCItems
 PURPOSE:    To copy protocol subject cost items from one calendar to another

 Object Name:     Sp_Copy_Protocol_SCItems
 Date/Time:       1/23/2011
 Username:        Sampada
 REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/23/2011    Sampada M        1. Created this procedure.
******************************************************************************/

v_NEW_ITEM_PK NUMBER;
v_OLD_ITEM_PK NUMBER;
v_ITEM_NAME VARCHAR2(50);
v_ITEM_COST NUMBER;
v_ITEM_UNIT NUMBER;
v_ITEM_SEQ NUMBER;
v_ITEM_CATEGORY NUMBER;
v_ITEM_COST_TYPE NUMBER;
v_count NUMBER;

v_rec_count NUMBER := 0;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_COPYPROTOCOL', pLEVEL  => Plog.LFATAL);

v_Item_mapping rec_mapping := rec_mapping(NULL,NULL);
v_Item_keys tab_mapping := tab_mapping(); --index-by table typ

CURSOR C1 IS
SELECT PK_SUBCOST_ITEM,SUBCOST_ITEM_NAME,SUBCOST_ITEM_COST,SUBCOST_ITEM_UNIT,
SUBCOST_ITEM_SEQ,FK_CODELST_CATEGORY,FK_CODELST_COST_TYPE
FROM SCH_SUBCOST_ITEM
WHERE FK_CALENDAR = p_old_protocol_id -- YK 09May2011 Bug# 5821 $ #5818
ORDER BY PK_SUBCOST_ITEM;

BEGIN
   	v_count := 0;

	OPEN  c1;
	LOOP

		FETCH c1
		INTO v_OLD_ITEM_PK, v_ITEM_NAME, v_ITEM_COST, v_ITEM_UNIT, v_ITEM_SEQ, v_ITEM_CATEGORY, v_ITEM_COST_TYPE;
		IF (c1%NOTFOUND) THEN
		   EXIT;
		END IF;

		SELECT SEQ_SCH_SUBCOST_ITEM.NEXTVAL INTO v_NEW_ITEM_PK FROM dual;

		INSERT INTO SCH_SUBCOST_ITEM (
		   	PK_SUBCOST_ITEM,FK_CALENDAR,SUBCOST_ITEM_NAME,SUBCOST_ITEM_COST,SUBCOST_ITEM_UNIT,
			SUBCOST_ITEM_SEQ,FK_CODELST_CATEGORY,FK_CODELST_COST_TYPE, CREATOR, CREATED_ON,IP_ADD)
		VALUES ( v_NEW_ITEM_PK, P_NEW_PROTOCOL_ID, v_ITEM_NAME, v_ITEM_COST, v_ITEM_UNIT,
			v_ITEM_SEQ, v_ITEM_CATEGORY, v_ITEM_COST_TYPE, p_user, SYSDATE, p_IP);

       	v_count := v_count + 1;
		-- Plog.DEBUG(pCTX,'mapping one old:' || v_OLD_ITEM_PK || ': new ' || v_NEW_ITEM_PK);

		v_rec_count := v_rec_count + 1;
   		v_Item_mapping.old_id := v_OLD_ITEM_PK ;
		v_Item_mapping.new_id := v_NEW_ITEM_PK;
		v_Item_keys.EXTEND;
		v_Item_keys(v_rec_count) := v_Item_mapping;
		v_rec_count  := v_Item_keys.COUNT();

	END LOOP;

	CLOSE c1;

	-- return value
	p_ret := v_count;
	p_Item_keys := v_Item_keys;

/**********************************
--TEST
set serveroutput on
declare

begin
Sp_Copy_Protocol_SCItems(40027, 40028, 1523, '66.237.42.110', v_ret) ;
end ;
**************************************/

END Sp_Copy_Protocol_SCItems;

end pkg_subcostitem;
/

CREATE OR REPLACE SYNONYM ERES.PKG_SUBCOSTITEM FOR PKG_SUBCOSTITEM;

CREATE OR REPLACE SYNONYM EPAT.PKG_SUBCOSTITEM FOR PKG_SUBCOSTITEM;

GRANT EXECUTE, DEBUG ON PKG_SUBCOSTITEM TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_SUBCOSTITEM TO ERES;