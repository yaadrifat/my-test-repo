alter table SCH_EVENTS1 add reason_for_coveragechange varchar2(4000);

Comment on column SCH_EVENTS1.reason_for_coveragechange is 'This column stores the reason for changes in Coverage Type';
Comment on column SCH_EVENTS1.FK_CODELST_COVERTYPE is 'This column stores the codelst id for the Coverage Type, stores the pk of er_codelst table';  