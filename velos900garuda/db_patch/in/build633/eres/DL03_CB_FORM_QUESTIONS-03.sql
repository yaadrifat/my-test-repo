--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE FOR FMHQ FORM --

--FMHQ Question 1--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 1.1--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_medhist_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 2--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 3--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 3.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dnr_egg_sperm_fmhq_avail_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 4--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 4.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_testname'), null,sysdate,null,null,null,null,null);

--FMHQ Question 4.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc'), null,sysdate,null,null,null,null,null);

--FMHQ Question 4.c--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 4.c.1--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_desc'), null,sysdate,null,null,null,null,null);

--FMHQ Question 5--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 5a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc'), null,sysdate,null,null,null,null,null);

--FMHQ Question 6--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 6a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_cause'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7.b.1--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7.b.1--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 7.b.1--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 8.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 9.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 10.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 11.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 12.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 13--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 13.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 14--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 14.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.c--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.c--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.c--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.c--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.c--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 15.c--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 16--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 16.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 17--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 17.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 17.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 17.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 17.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 18.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 19--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 19.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 20--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 20.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 21--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 21.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 22--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 22.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 23--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 23.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24.a--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5'), null,sysdate,null,null,null,null,null);

--FMHQ Question 24.b--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6'), null,sysdate,null,null,null,null,null);

--FMHQ Question 25--

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind'), null,sysdate,null,null,null,null,null);

-- END --
commit;
