Set Define off;


-------------------HLAOverrideClass--------------------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hla_over_class','c','Standard','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hla_over_class','h','High Correct','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hla_over_class','l','Low Correct','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hla_over_class','p','Prior Err Correct','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


-----------------------------------ReportCardStatus---------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_crd_stat','dt','Defer This','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_crd_stat','da','Defer all CBUs with this Mother','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_crd_stat','excp','Exception','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_crd_stat','ok','Acceptable','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_crd_stat','inelig','Ineligible','N',5,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



----------------------------------------NMDPRegistryStatus--------------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','av','Available','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','ac','Active','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','na','Not available','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','ne','newly entered','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','av','Available','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','tu','Temporarily unavailable','N',5,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','de','Deleted','N',6,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','in','Incomplete','N',7,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'nmdp_reg_stat','st','Staged','N',8,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

--------------------------SampleStatus--------------------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_stat','av','Available','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_stat','ca','Cancel','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_stat','ac','Active','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_stat','na','Not Available','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_stat','ds','Destroy Sample','N',5,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_stat','dx','No definition given','N',6,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


-----------------------ConfirmatoryTyping-------------------------------


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cnfrm_typ_sam','as','CBU Contiguous Attached Segment','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cnfrm_typ_sam','wb','Whole Blood Sample','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cnfrm_typ_sam','rc','Red Cell Fraction (pellet)','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cnfrm_typ_sam','fp','Blood Spotted Filter Paper','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'cnfrm_typ_sam','ed','Extracted DNA','N',5,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


---------------------------ProcessingRules---------------------------------


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'prcss_rule','fill','Fulfillable (Cord Information has been transferred to NMDP to support the fulfillment process','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'prcss_rule','inv','Count as Inventory','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'prcss_rule','src','Searchable Source.','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



-----------------------------StoredSampleInventory-----------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','exdn','No. of Non-viable Cell Aliquots','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','filtp','Filter Paper Available','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','mexdn','Maternal DNA Aliquots','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','mnovia','Maternal Cell Aliquots','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','mplsma','Maternal Plasma Aliquots','N',5,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','mserum','Maternal Serum Aliquots','N',6,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','noviab','No. of Non-viable Cell Aliquots','N',7,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','plsma','No. of Plasma Aliquots','N',8,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','rbcp','RBC Pellets Available','N',9,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','segm','Segments','N',10,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','serum','No. of Serum Aliquots','N',11,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','viab','No. of Viable Cell Aliquots','N',12,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','mnovia','Maternal Cell Aliquots','N',13,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','A','Frozen Aliquots','N',14,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','F','Filter Paper ','N',15,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','P','Cord Pellet','N',16,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','S','Buccal Swabs','N',17,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'store_sam_invt','T','Cord Filter Paper','N',18,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

-------------------------------------SampleTyping-------------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','sample_type','Sample type description','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','A','frozen aliquot for donor - 3 aliquots','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','F','filter paper for donor - 5 spots','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','Q','aliquot qc sample - 1 aliquot','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','R','aliquot qc sample - 1 aliquot','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','P','filter qc - 1 spot','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','T','cord pellet (frozen cord aliquot) - 1 aliquot','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','M','cord filter - 1 spot','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','','family member frozen aliquot - 3 aliquots','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','Y','family member filter paper - 5 spots','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','S','buccal swab storage, added 1/11/06 - 4 swabs','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','W','buccal swab QC storage, added 1/11/06 - 1 swab','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','L','cord pellet QC (frozen cord aliquot) - 1 aliqout','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','I','cord filter QC - 1 spot','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'sample_type','D','DOD buccal swab storage, added 10/22/07 - 2 swabs','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

---------------------------------ReportingMethodCode-------------------------------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_method','10','Form 10','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_method','cbb','Cord Blood Bank','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_method','emd','EMDIS','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_method','hsc','History Correction','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'rprt_method','lab','Contract Lab','N',5,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


----------------------------HLATYPINGCOMPLETEMETHODCODE--------------------


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hla_typ_cmp_mth','110','Form 110','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hla_typ_cmp_mth','117','Form 117','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'hla_typ_cmp_mth','lab','Laboratory (this means a sample is at the lab.)','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);

---------------------------EligibilityCategory---------------------------------

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'eligibile_cat','ac','Ac','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'eligibile_cat','bc','Bc','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'eligibile_cat','cc','Cc','N',6,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

	
	
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'eligibile_cat','dc','Dc','N',7,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


--------------------------------------------------------------------------------------------------------------------------



Commit;