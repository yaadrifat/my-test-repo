set define off;

-- Update ER_REPFILTER
Update ER_REPFILTER set REPFILTER_COLUMN = '<td>
	<DIV id="fqCreatorDIV" name="fqCreatorDIV">
      <label id="fqCreatorLabel">Select Form Query Creator <FONT class="Mandatory">* </FONT></label>
    </DIV>
</td>
<td>
	<DIV id="fqCreatorDataDIV" name="fqCreatorDataDIV">[FQCREATORID]
		<input TYPE="hidden" id ="selfqCreatorId" NAME="selfqCreatorId" value="ALL"/>
		<input TYPE="hidden" id ="paramfqCreatorId" NAME="paramfqCreatorId" value="role_All"/>
	</DIV>
</td>', REPFILTER_DISPDIV ='fqCreatorDIV,fqCreatorDataDIV'
where PK_REPFILTER = 26;

Update ER_REPFILTERMAP set REPFILTERMAP_COLUMN = '<td>
	<DIV id="fqCreatorDIV" name="fqCreatorDIV">
      <label id="fqCreatorLabel">Select Form Query Creator <FONT class="Mandatory">* </FONT></label>
    </DIV>
</td>
<td>
	<DIV id="fqCreatorDataDIV" name="fqCreatorDataDIV">[FQCREATORID]
		<input TYPE="hidden" id ="selfqCreatorId" NAME="selfqCreatorId" value="ALL"/>
		<input TYPE="hidden" id ="paramfqCreatorId" NAME="paramfqCreatorId" value="role_All"/>
	</DIV>
</td>', REPFILTERMAP_DISPDIV='fqCreatorDIV,fqCreatorDataDIV'
where FK_REPFILTER = 26;

commit;




-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE ERES."PKG_FORMQUERY" IS

   /* Sammie Mhalagi  11/11/10*/
   function F_Is_FQueryCreator(P_FormQueryId Number, P_StudyId Number, P_fqCreatorId Number, P_fqCreatorType VARCHAR2) return number;

    pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_FormQuery', pLEVEL  => Plog.LFATAL);
END;
/


CREATE OR REPLACE PACKAGE BODY ERES."PKG_FORMQUERY"
IS
	function F_Is_FQueryCreator(P_FormQueryId Number, P_StudyId Number, P_fqCreatorId Number, P_fqCreatorType VARCHAR2) 
	return number
	IS
	  v_Return Number default 0;
	  Begin
	  
	  CASE (P_fqCreatorType)
	  	  WHEN 'role_All' THEN
			v_Return := 1;
	  	  WHEN 'role_system' THEN
	  	    SELECT COUNT(*) INTO v_Return FROM ER_FORMQUERY, ER_FORMQUERYSTATUS 
	  	    WHERE PK_FORMQUERY=FK_FORMQUERY AND FK_QUERYMODULE = P_FormQueryId 
	  	    AND FORMQUERY_TYPE =1;
	  	  ELSE
	  	  	SELECT COUNT(*) INTO v_Return FROM ER_STUDYTEAM WHERE FK_STUDY = P_StudyId AND FK_USER = P_fqCreatorId
		  	AND P_fqCreatorType = (SELECT CODELST_SUBTYP FROM ER_CODELST WHERE PK_CODELST = FK_CODELST_TMROLE);
	  END CASE;
	  
	  return v_Return;
	End; -- for function
END;
/

CREATE SYNONYM EPAT.PKG_FORMQUERY FOR PKG_FORMQUERY;

CREATE SYNONYM ESCH.PKG_FORMQUERY FOR PKG_FORMQUERY;


