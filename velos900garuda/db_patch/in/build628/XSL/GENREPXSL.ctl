LOAD DATA INFILE * INTO TABLE er_repxsl
APPEND
FIELDS TERMINATED BY ','
(PK_REPXSL,
FK_REPORT,
FK_ACCOUNT,
REPXSL_NAME,
xsl_file filler char,
"REPXSL" LOBFILE (xsl_file) TERMINATED BY EOF NULLIF XSL_FILE = 'NONE',
xsl_file2 filler char,
"REPXSL_XSL" LOBFILE (xsl_file2) TERMINATED BY EOF NULLIF XSL_FILE2 = 'NONE')
BEGINDATA
110,110,0,Patient Budget by Visit,110.xsl,110.xsl
159,159,0,Combined Patient Budget,159.xsl,159.xsl
161,161,0,CBU Summary Report,161.xsl,161.xsl
167,167,0,CBU Detail Report,167.xsl,167.xsl
168,168,0,CBU Comprehensive Report,168.xsl,168.xsl
170,170,0,Processing Information Report,170.xsl,170.xsl