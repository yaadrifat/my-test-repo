<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">

<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--> <font size ="4"> National Marrow Donor Program� </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--><font size ="4"> Processing Information Report for HPC, Cord Blood </font>
</b>
</td>
</tr>
</table>
<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
<xsl:value-of select="ROW/CBBID"/>
</td>
<td align="right"> Cord Blood Bank ID: 
<xsl:value-of select="ROW/CBB_ID"/>
</td>
</tr>

<tr>
<td align="left"> CBU Identification Number on Bag:   
<xsl:value-of select="ROW/CORD_REGISTRY_ID"/> / <xsl:value-of select="ROW/CORD_LOCAL_CBU_ID"/> / <xsl:value-of select="ROW/CORD_ISBI_DIN_CODE"/> 
</td>
<td align="right"> CBU Registry ID:    
<xsl:value-of select="ROW/CORD_REGISTRY_ID"/>
</td>
</tr>
</table>

<hr class="thickLine" width="100%"/>
<xsl:apply-templates select="ROW"/>

<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 


<xsl:template match="ROW">
<xsl:for-each select="//ROW">
<br></br>
 <b><u><font size="3">CBB Processing Procedure </font></u></b> 

<table>
<tr>
<TD> Procedure Name: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PROC_NAME" /> 
</TD>
<TD> Procedure Start Date: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="PROC_START_DATE" />
</TD>
</tr>

<tr>
<TD> </TD> 
<TD class="reportData" align="left" >
</TD>
<TD> Procedure Termination Date: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="PROC_TERMI_DATE" />
</TD>
</tr>

</table>

<hr class="thickLine" width="100%"/>

<br></br> 


<b><u><font size="3">Product Modification </font></u></b> 

<table>
<tr>
<TD> Processing: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PROCESSING" /> 
</TD>
</tr>
<tr>
<TD> Type of automated process used to modify the CBU: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="AUTOMATED_TYPE" />
</TD>
</tr>

<tr>
<TD> Please Specify: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHER_PROCESSING" />
</TD>
</tr>
<tr>
<TD> Product Modification:  </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="PRODUCT_MODIFICATION" />
</TD>
</tr>

<tr>
<TD> Please Specify: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="OTHER_PRODUCT_MODI" /> 
</TD>
</tr>
</table>

<hr class="thickLine" width="100%"/>

<br></br> 


<b><u><font size="3">Storage Conditions </font></u></b> 
<table>
<tr>
<TD> Storage Method: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="STORAGE_METHOD" /> 
</TD>
<TD> Number of Individual Fractions that can be Thawed:</TD> 
<TD class="reportData" align="left">
<xsl:value-of select="INDIVIDUAL_FRAC" />
</TD>
</tr>

<tr>
<TD> Freezer Manufacturer: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FREEZER_MANUFACT" />
</TD>
<TD> Type of Bag: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="BAGTYPE" />
</TD>
</tr>

<tr>
<TD> Please Specify: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHER_FREEZER_MANUFACT" />
</TD>
<TD> Please Specify: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHER_BAG" />
</TD>
</tr>

<tr>
<TD>Storage Temperature: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="STORAGE_TEMPERATURE" />
</TD>
<TD> Cryobag Manufacturer: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CRYOBAG_MANUFAC" />
</TD>
</tr>

<tr>
<TD> Controlled Rate Freezing: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CTRL_RATE_FREEZING" />
</TD>
<TD> Maximum Volume: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="MAX_VOL" />
</TD>
</tr>

<tr>
<TD> Frozen in vials, tubes, bags or other: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FROZEN_IN" />
</TD>
</tr>
<tr>
<TD> Please Specify: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHER_FROZEN_CONT" />
</TD>
</tr>
</table>


<hr class="thickLine" width="100%"/>

<br></br> 
<b><u><font size="3">Frozen Product Composition</font></u></b> 


<br></br> <br></br>
<b><font size="3">Additives Before Volume Reductions</font></b> 


<table>

<tr>
<TD></TD><TD>%</TD><TD>ml</TD><TD></TD><TD>%</TD><TD>ml</TD>
</tr>

<tr>
<TD> 1000 unit/ml Heparin: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_THOU_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_THOU_ML" /> 
</TD>
<TD>CPDA:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CPDA_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="CPDA_ML" /> 
</TD>
</tr>

<tr>

<TD> 5000 unit/ml Heparin: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_FIVE_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_FIVE_ML" /> 
</TD>

<TD>CPD:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CPD_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="CPD_ML" /> 
</TD>
</tr>

<tr>
<TD>10,000 unit/ml Heparin:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_TEN_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_TEN_ML" /> 
</TD>
<TD>ACD:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="ACD_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="ACD_ML" /> 
</TD>
</tr>

<tr>
<TD>6% Hydroxyethyl Starch:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_SIX_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="HEPARIN_SIX_ML" /> 
</TD>

<TD>Other Anticoagulant:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHR_ANTI_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="OTHR_ANTI_ML" /> 
</TD>
</tr>

<tr>
<TD></TD>
<TD></TD>
<TD></TD>
<TD>Specify Other Anticoagulant:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="SPECI_OTHR_ANTI" /> 
</TD>
<TD class="reportData" align="left" ></TD>
</tr>
</table>


<br></br>
<b><font size="3">Cryoprotectants and Other Additives</font></b> 
<br></br>

<table>

<tr>
<TD></TD><TD>%</TD><TD>ml</TD><TD></TD><TD>%</TD><TD>ml</TD>
</tr>


<tr>
<TD> 100% DMSO: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="HUN_DMSO_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="HUN_DMSO_ML" /> 
</TD>
<TD> 25% Human Albumin: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="TWENTYFIVE_HUMAN_ALBU_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="TWENTYFIVE_HUMAN_ALBU_ML" /> 
</TD>
</tr>

<tr>
<TD> 100% Glycerol: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="HUN_GLYCEROL_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="HUN_GLYCEROL_ML" /> 
</TD>

<TD> Plasmalyte: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="PLASMALYTE_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="PLASMALYTE_ML" /> 
</TD>

</tr>

<tr>
<TD> 10% Dextran-40: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="TEN_DEXTRAN_40_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="TEN_DEXTRAN_40_ML" /> 
</TD>
<TD> Other Cryoprotectant: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHR_CRYOPROTECTANT_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="OTHR_CRYOPROTECTANT_ML" /> 
</TD>


</tr>

<tr>

<TD> 5% Human Albumin: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FIVE_HUMAN_ALBU_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="FIVE_HUMAN_ALBU_ML" /> 
</TD>


<TD> Specify Other Cryoprotectant: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="SPEC_OTHR_CRYOPROTECTANT" /> 
</TD>
<TD class="reportData" align="left" >
</TD>
<TD class="reportData" align="left" >
</TD>
</tr>

</table>

<br></br> <br></br>
<b><font size="3">Diluents</font></b> 
<br></br> <br></br>

<table>

<tr><TD></TD><TD>%</TD><TD>ml</TD><TD></TD><TD>%</TD><TD>ml</TD></tr>


<tr>
<TD> 5% Dextrose:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FIVE_DEXTROSE_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="FIVE_DEXTROSE_ML" /> 
</TD>
<TD> 0.9% NaCl:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="POINT_NINE_NACL_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="POINT_NINE_NACL_ML" /> 
</TD>
</tr>

<tr>
<TD> Other Diluents:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHR_DILUENTS_PER" /> 
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="OTHR_DILUENTS_ML" /> 
</TD>
<TD> Specify Other Diluents:</TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="SPEC_OTHR_DILUENTS" /> 
</TD>
<TD class="reportData" align="left" >
</TD>
</tr>
</table>


<hr class="thickLine" width="100%"/>

<br></br> 

<b> <u><font size="3"> Sample Inventory and Aliquots
 </font></u></b> 
<table>

<tr>
<TD> Number of Segments : </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_SEGMENTS" /> 
</TD>

<TD> Number of Maternal Extracted DNA Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_EXTR_DNA_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD> Number of Filter Paper Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="FILTER_PAPER" /> 
</TD>

<TD> Number of Maternal Serum Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_SERUM_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD> Number of RBC Pellets : </TD> <TD class="reportData" align="left" >
<xsl:value-of select="RPC_PELLETS" /> 
</TD>

<TD> Number of Maternal Plasma Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_PLASMA_MATER_ALIQUOTS" /> 
</TD>
</tr>


<tr>
<TD> Number of Extracted DNA Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="EXTR_DNA" /> 
</TD>

<TD>No. of Miscellaneous Maternal Samples:</TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_CELL_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD> Number of Serum Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="SERUM_ALIQUOTES" /> 
</TD>
<TD></TD>
<TD></TD>
</tr>

<tr>
<TD>Number of Plasma Samples:</TD> <TD class="reportData" align="left" >
<xsl:value-of select="PLASMA_ALIQUOTES" /> 
</TD>
<td></td>
<TD></TD>
</tr>

<tr>
<TD>Number of Non-Viable Cell Samples:</TD> <TD class="reportData" align="left">
<xsl:value-of select="NONVIABLE_ALIQUOTES" /> 
</TD>
<td></td>
<TD></TD>
</tr>

</table>
<hr class="thickLine" width="100%"/>

<br></br>

<p><b><u><font size="3"> Documents </font></u></b></p>
<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD>  
<TD align="center">File name</TD>
<TD align="center">Created on</TD>
</tr>
<xsl:for-each select="//PROCESSINFO_ATTACHMENTS/PROCESSINFO_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>


</xsl:for-each>
</xsl:template> 

</xsl:stylesheet>