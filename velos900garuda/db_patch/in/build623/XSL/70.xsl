<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

	</HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" bgcolor="lightgrey" >
<tr>
<td class="reportPanel"> 
VELMESSGE[M_Download_ReportIn]:<!-- Download the report in --> 
<A href='{$wd}' >
VELLABEL[L_Word_Format]<!-- Word Format -->
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
VELLABEL[L_Excel_Format]<!-- Excel Format -->
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
</A> 
</td>

</tr>
</table>
</xsl:if>

<table class="reportborder" width="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
<TR>
<TD class="reportData" WIDTH="100%" ALIGN="CENTER">
<BR/>VELLABEL[L_Selected_Filter_S]<!-- Selected Filter(s) -->: <xsl:value-of select="$argsStr" />
</TD>
</TR>
</TABLE>
<hr class="thinLine" />

<TABLE WIDTH="100%" BORDER="0">
<TR><TD class="reportData" ALIGN="CENTER">
	<b>VELMESSGE[M_DtFltrApld_DtMstoneAch]<!-- Date Filter Applied To: &quot;Date Milestone Achieved &quot; --></b>
</TD></TR>
<TR><TD>&#xa0;</TD></TR>
<TR><TD class="reportName" align="left"><b>VELLABEL[L_Total_Milestones]<!-- Total Milestones -->:&#xa0;<xsl:value-of select="sum(//ACHCOUNT)"/></b></TD></TR>
<TR>
<TD class="reportName" align="left"><b>VELLABEL[L_TotalAmount]<!-- Total Amount -->:&#xa0;$<xsl:value-of select="format-number(sum(//ACHAMOUNT),'##,###,###,###,###,##0.00')"/></b></TD>
</TR>

</TABLE>

<TABLE WIDTH="100%" BORDER="1">

<TR>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_Division]<!-- Division --></TH>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_Therapeutic_Area]<!-- Therapeutic Area --></TH>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_Study]<!-- Study --></TH>
<TH class="reportHeading" WIDTH="15%" ># VELLABEL[L_Milestones_Achieved]<!-- Milestones Achieved --></TH>
<TH class="reportHeading" WIDTH="10%" >VELLABEL[L_Amount]<!-- Amount --></TH>
<TH class="reportHeading" WIDTH="10%" >VELLABEL[L_Study_Status]<!-- Study Status --></TH>
<TH class="reportHeading" WIDTH="20%" >&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;VELLABEL[L_PI]<!-- PI -->&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</TH>
<TH class="reportHeading" WIDTH="20%" >VELLABEL[L_Research_Type]<!-- Research Type --></TH>
<TH class="reportHeading" WIDTH="15%" >VELLABEL[L_Sponsor]<!-- Sponsor --></TH>
</TR>

<xsl:apply-templates select="ROW"/>
</TABLE>
<TABLE WIDTH="100%" BORDER="0">
<TR>
<TD class="reportName" align="right"><b>VELLABEL[L_Total_Milestones]<!-- Total Milestones -->:&#xa0;<xsl:value-of select="sum(//ACHCOUNT)"/><BR/>
VELLABEL[L_TotalAmount]<!-- Total Amount -->:&#xa0;$<xsl:value-of select="format-number(sum(//ACHAMOUNT),'##,###,###,###,###,##0.00')"/></b></TD>
</TR>
</TABLE>

<hr class="thickLine" />
<TABLE WIDTH="100%" >
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>	
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >
<TR class="reportEvenRow" VALIGN="TOP">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_DIVISION_DESC" />
<xsl:text>&#xa0;</xsl:text>
</TD>
<TD>
<xsl:value-of select="FK_CODELST_TAREA_DESC" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_NUMBER" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="ACHCOUNT" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="ACHAMOUNT" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="STUDYSTAT_DESC" />
</TD>

<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="STUDY_PRINV_NAME" />
</TD>

<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="FK_CODELST_RESTYPE_DESC" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="STUDY_SPONSOR" />
</TD>

</TR>
</xsl:when> 
<xsl:otherwise>
<TR class="reportOddRow" VALIGN="TOP">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_DIVISION_DESC" />
<xsl:text>&#xa0;</xsl:text>
</TD>
<TD>
<xsl:value-of select="FK_CODELST_TAREA_DESC" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_NUMBER" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="ACHCOUNT" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="ACHAMOUNT" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="STUDYSTAT_DESC" />
</TD>

<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="STUDY_PRINV_NAME" />
</TD>

<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="FK_CODELST_RESTYPE_DESC" />
</TD>
<TD class="reportData" WIDTH="10%" >
<xsl:value-of select="STUDY_SPONSOR" />
</TD>
</TR>
</xsl:otherwise>
</xsl:choose> 
</xsl:template>

</xsl:stylesheet>
