<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--> <font size ="4"> National Marrow Donor Program® </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--><font size ="4"> Licensure and Eligibility Report for HPC, Cord Blood </font>
</b>
</td>
</tr>
</table>
<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
<xsl:value-of select="ROW/SITENAME"/>
</td>
<td align="right"> Cord Blood Bank ID: 
<xsl:value-of select="ROW/SITEID"/>
</td>
</tr>
<tr>
<td align="left"> CBU Identification Number on Bag: 


<xsl:for-each select="//ROW/IDSONBAG/IDSONBAG_ROW">

<xsl:if test="IDONBAGVAL='CBU Registry ID'">
	<xsl:value-of select="../../REGID" />&#160; 
</xsl:if>
<xsl:if test="IDONBAGVAL='CBU Local ID'">
	<xsl:value-of select="../../LOCALCBUID" />&#160; 
</xsl:if>
<xsl:if test="IDONBAGVAL='ISBT DIN'">
	<xsl:value-of select="../../ISTBTDIN" />&#160;  
</xsl:if>
<xsl:if test="IDONBAGVAL='CBU Additional ID'">
	<xsl:for-each select="./../../ADDIDS/ADDIDS_ROW">
		<xsl:value-of select="ADDIDVAL" />&#160;
	</xsl:for-each>
</xsl:if>
</xsl:for-each>

</td>
<td></td>
</tr>
</table>
<hr class="thickLine" width="100%"/>
<xsl:apply-templates select="ROW"/>

<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 


<xsl:template match="ROW">
<xsl:for-each select="//ROW">

<p align="center">As of <xsl:value-of select="LIC_MODI_DT" />, the licensure status of this HPC(CB) was reported as:</p> 

<br></br> 

<table>

<tr>
<TD class="reportData" align="center" >
<xsl:value-of select="LICSTATUS" /> 
</TD>
</tr>

<xsl:if test="LIC_FLAG='1'">
<tr>
<TD class="reportData" align="center" >
<p>Unlicensed reason(s):</p>
<xsl:value-of select="UNLICENSEREASON" /> 
</TD>
</tr>
</xsl:if>

</table>


<br></br> 

<p align="center">As of <xsl:value-of select="ELGIBLE_MODI_DT" />, the Eligibility Determination of this HPC(CB) was reported as:</p> 

<br></br> 

<table>

<tr>
<TD class="reportData" align="center" >
<xsl:value-of select="ELIGSTATUS" /> 
</TD>
</tr>

<xsl:if test="ELI_FLAG='1'">
<tr>
<TD class="reportData" align="center" >
<p>Ineligible Reason(s):</p>
<xsl:value-of select="INELIGIBLEREASON" /> 
</TD>
</tr>
</xsl:if>
<xsl:if test="ELI_FLAG='2'">
<tr>
<TD class="reportData" align="center" >
<p>Incomplete Reason(s):</p>
<xsl:value-of select="INELIGIBLEREASON" /> 
</TD>
</tr>
</xsl:if>

<xsl:if test="ELI_FLAG!='0'">
<tr>
<TD class="reportData" align="center" >
<p>Comments:<xsl:value-of select="ELIGCOMMENTS" /></p>
</TD>
</tr>
</xsl:if>


</table>



</xsl:for-each>
</xsl:template> 


</xsl:stylesheet>
