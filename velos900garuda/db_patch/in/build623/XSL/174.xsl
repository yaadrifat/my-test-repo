<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table width="100%">
<xsl:if test="$hdrflag='1'">
<tr>
<td width="100%" align="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
</table>

<table>
<tr>
<td width="20%"></td>
<td width="60%"></td>
<td width="20%" align="right">Report:R005</td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" align="CENTER">
<b><font size ="4"> NMDP Cord Blood Unit </font>
</b>
</td>
<td width="20%"></td>
</tr>
<!--
<tr>
<td width="20%">
<p>Run Date:</p><xsl:value-of select="ROW/RUN_DATE"/>
<br></br>
<p>Run Time:</p><xsl:value-of select="ROW/RUN_TIME"/>
</td>
<td width="60%"></td>
<td width="20%">
<p>Report:R005</p>
</td>
</tr>
-->
<tr>
<td width="20%"></td>
<td class="reportName" width="70%" align="CENTER">
<b><font size ="3"> Confirmatory Typing Request Form </font></b>
</td>
<td width="20%"></td>
</tr>
</table>

<br></br>
<br></br>


<table>
<tr>
<td align="left" width="100%">
<b>Instructions: This Information must be provided by the Cord Blood bank to accompany a cord blood unit sample is being shipped to the NMDP laboratory for confirmatory typing.</b>
</td> 
</tr>
</table>

<br></br>
<br></br>


<xsl:apply-templates select="ROW"/>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" align="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 


<xsl:template match="ROW">
<xsl:for-each select="//ROW">

<table>
<tr>
<TD><b>Cord Blood Bank Name:</b></TD>
<TD class="reportData" align="left" >
<xsl:value-of select="SITENAME" /> 
</TD>
<TD><b>Cord Blood Bank Address:</b></TD>
<TD class="reportData" align="right">
<xsl:value-of select="ADDRESS" />&#160;<xsl:value-of select="CITY" />&#160;<xsl:value-of select="ZIPCODE" />&#160; <xsl:value-of select="STATE" />&#160;<xsl:value-of select="COUNTRY" /> 
</TD>
</tr>


<tr>
<TD><b>NMDP CBU ID:</b></TD>
<TD class="reportData" align="left">
<xsl:value-of select="REGID" /> 
</TD>
<TD><b>Local CBU ID:</b></TD>
<TD class="reportData" align="right" >
<xsl:value-of select="LOCALCBUID" /> 
</TD>
</tr>

<tr>
<TD><b>ISBT ID:</b></TD>
<TD class="reportData" align="left">
<xsl:value-of select="ISBTDIN" /> 
</TD>
<TD><b>Local CBU ID(Full):</b></TD>
<TD class="reportData" align="right">
<xsl:value-of select="FULL" /> 
</TD>
</tr>

<tr>
<TD><b>CT Ship Date:</b></TD>
<TD class="reportData" align="left" >
<xsl:value-of select="SHIPDATE" /> 
</TD>
<TD><b>Type of Sample Shipped:</b></TD>
<TD class="reportData" align="right" >
<xsl:value-of select="SAMPLE_TYPE" /> 
</TD>
</tr>

</table>


<br></br> 

<b>Please note:</b> 
<br></br> 
<br></br>
<ul>
&#160;&#160;&#160;&#160;<li><b>Non-viable cell aliquots may include whole cord blood without cryoprotectant or buffycoat,leukocyte enriched sample</b></li>
<br></br> 
&#160;&#160;&#160;&#160;
<li><b>Viable cell aliquots may include detached segments or samples that contain cryoprotectant</b>
</li>
</ul>

<br></br> 
<br></br> 


<b><u>Current Hla Typing</u></b>
<br></br> 
<br></br> 
<table>
<tr>
<TD class="reportData" align="center" ><b>Locus</b></TD>
<TD class="reportData" align="center" ><b>Method</b></TD>
<TD class="reportData" align="center" ><b>Type 1</b></TD>
<TD class="reportData" align="center" ><b>Type 2</b></TD>
<TD class="reportData" align="center" ><b>Entry Date</b></TD>
</tr>
<tr>
<TD class="reportData" align="center" >
<xsl:value-of select="LUCUS" /> 
</TD>
<TD class="reportData" align="center" >
<xsl:value-of select="METH" /> 
</TD>
<TD class="reportData" align="center" >
<xsl:value-of select="HLA1" /> 
</TD>
<TD class="reportData" align="center" >
<xsl:value-of select="HLA2" /> 
</TD>
<TD class="reportData" align="center" >
<xsl:value-of select="HLA_CREATED_DT" /> 
</TD>
</tr>
</table>

<br></br>
<br></br>
<p align="left"><b>NMDP Contact Information</b></p>
<br></br>
<p align="left">3001 Broadway St N.E., Suite 100, Minneapolis, MN 55413 1-800-526-7809</p>


<br></br>
<br></br>
<table>
<tr>
<td><b>NMDP Contact Information</b></td>
<td><b>(Form Completed By)</b></td>
<td><b>Date</b></td>
</tr>
</table>
<br></br>
<p>Phone Number for Question regarding this shipment</p>


</xsl:for-each>
</xsl:template> 


</xsl:stylesheet>
