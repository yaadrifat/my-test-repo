CREATE OR REPLACE TRIGGER "ERES".CB_UPLOAD_INFO_AU0 AFTER  UPDATE OF
PK_UPLOAD_INFO,
DESCRIPTION,
COMPLETION_DATE,
TEST_DATE,
PROCESS_DATE,
VERIFICATION_TYPING,
RECEIVED_DATE,
FK_CATEGORY,
FK_SUBCATEGORY,
FK_ATTACHMENTID,
CREATOR,
CREATED_ON,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
IP_ADD,
RID,
DELETEDFLAG,
REPORT_DATE  ON CB_UPLOAD_INFO   FOR EACH ROW 
DECLARE 
raid NUMBER(10);
  usr                                         VARCHAR2(100);
  old_modby                                   VARCHAR2(100);
  new_modby                                   VARCHAR2(100);
  NEW_FK_CATEGORY                             VARCHAR2(200);
  NEW_FK_SUBCATEGORY                          VARCHAR2(200);
  OLD_FK_CATEGORY                             VARCHAR2(200);
  OLD_FK_SUBCATEGORY                          VARCHAR2(200);
  BEGIN
    SELECT seq_audit.nextval INTO raid FROM dual;
    usr := getuser(:NEW.last_modified_by);
    IF NVL(:OLD.FK_CATEGORY,0) != NVL(:NEW.FK_CATEGORY,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_CATEGORY)		INTO NEW_FK_CATEGORY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_CATEGORY := '';
   END; 
   BEGIN
   SELECT F_Codelst_Desc(:OLD.FK_CATEGORY)		INTO OLD_FK_CATEGORY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_CATEGORY := '';
   END;
  END IF;
   IF NVL(:OLD.FK_SUBCATEGORY,0) != NVL(:NEW.FK_SUBCATEGORY,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SUBCATEGORY)		INTO NEW_FK_SUBCATEGORY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_SUBCATEGORY := '';
   END;  
    BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SUBCATEGORY)		INTO OLD_FK_SUBCATEGORY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_SUBCATEGORY := '';
   END;
  END IF; 
    audit_trail.record_transaction (raid, 'CB_UPLOAD_INFO', :old.rid, 'U', usr);
	IF NVL(:old.PK_UPLOAD_INFO,0) != NVL(:new.PK_UPLOAD_INFO,0) THEN
      audit_trail.column_update (raid, 'PK_UPLOAD_INFO', :OLD.PK_UPLOAD_INFO, :NEW.PK_UPLOAD_INFO);
    END IF;
	IF NVL(:old.DESCRIPTION,' ') != NVL(:new.DESCRIPTION,' ') THEN
      audit_trail.column_update (raid, 'DESCRIPTION', :OLD.DESCRIPTION, :NEW.DESCRIPTION);
    END IF;
	IF NVL(:old.COMPLETION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.COMPLETION_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'COMPLETION_DATE', TO_CHAR(:OLD.COMPLETION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.COMPLETION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;  
	IF NVL(:old.TEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.TEST_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'TEST_DATE', TO_CHAR(:OLD.TEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.TEST_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:old.PROCESS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.PROCESS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'PROCESS_DATE', TO_CHAR(:OLD.PROCESS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROCESS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:old.VERIFICATION_TYPING,' ') != NVL(:new.VERIFICATION_TYPING,' ') THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'VERIFICATION_TYPING', :OLD.VERIFICATION_TYPING, :NEW.VERIFICATION_TYPING);
    END IF;
	IF NVL(:old.RECEIVED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.RECEIVED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'RECEIVED_DATE', TO_CHAR(:OLD.RECEIVED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.RECEIVED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
  IF NVL(:OLD.FK_CATEGORY,0) != NVL(:NEW.FK_CATEGORY,0) THEN
      audit_trail.column_update (raid, 'FK_CATEGORY', OLD_FK_CATEGORY, NEW_FK_CATEGORY);
    END IF;  
	IF NVL(:OLD.FK_SUBCATEGORY,0) != NVL(:NEW.FK_SUBCATEGORY,0) THEN
      audit_trail.column_update (raid, 'FK_SUBCATEGORY', OLD_FK_SUBCATEGORY, NEW_FK_SUBCATEGORY);
    END IF;
	IF NVL(:old.FK_ATTACHMENTID,0) != NVL(:new.FK_ATTACHMENTID,0) THEN
      audit_trail.column_update (raid, 'FK_ATTACHMENTID', :OLD.FK_ATTACHMENTID, :NEW.FK_ATTACHMENTID);
    END IF;
	IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      audit_trail.column_update (raid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR);
    END IF;
	IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
 IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      audit_trail.column_update (raid, 'IP_ADD', :old.ip_add, :new.ip_add);
    END IF;
 IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO old_modby
        FROM er_user
        WHERE pk_user = :old.last_modified_by ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        old_modby := NULL;
      END ;
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO new_modby
        FROM er_user
        WHERE pk_user = :new.LAST_MODIFIED_BY ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        new_modby := NULL;
      END ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    END IF;
	IF NVL(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;	
	IF NVL(:old.rid,0) != NVL(:new.rid,0) THEN
      audit_trail.column_update (raid, 'RID', :OLD.rid, :NEW.rid);
    END IF;
	IF NVL(:old.DELETEDFLAG,' ') != NVL(:new.DELETEDFLAG,' ') THEN
      audit_trail.column_update (raid, 'DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
    END IF;
	IF NVL(:old.REPORT_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.REPORT_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'REPORT_DATE', TO_CHAR(:OLD.REPORT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.REPORT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
END;
/