set define off;
  CREATE OR REPLACE PACKAGE BODY "ERES"."PKG_STUDYSTAT" AS
PROCEDURE Change_stat    (
 p_study IN NUMBER ,
 p_per IN NUMBER,
 p_dmlby NUMBER ,
 p_ipadd VARCHAR2,
 p_event_exeon DATE )
AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 12/05/2001
   **
   ** This procedure will be called when patient's study status changes
   ** to 'Deactivated' or 'Suspended'.
   ** The procedure changes :
   **	1) the status of the eresdispatcher mails to be sent to a user as  '-1'
   **   2) the latest schedule's status is changed to 5
   **   3) Status of all schedule's events that are not 'Done' to 'Suspended'
   **
****************************************************************************************************/
v_patprot NUMBER;
v_eventstat NUMBER;
v_eventstatdone NUMBER;
BEGIN
SELECT MAX(pk_patprot)
   INTO v_patprot
   FROM ER_PATPROT
  WHERE fk_per = p_per
    AND fk_study = p_study  ;
SELECT pk_codelst
   INTO v_eventstat
   FROM SCH_CODELST
  WHERE RTRIM(codelst_subtyp) = 'ev_suspend'  ;
SELECT pk_codelst
   INTO v_eventstatdone
   FROM SCH_CODELST
  WHERE RTRIM(codelst_subtyp) = 'ev_done'  ;
  --changed for clubbed mails
  UPDATE SCH_MSGTRAN
     SET msg_status = -1
   WHERE fk_patprot = v_patprot
     AND msg_status = 0
    AND msg_sendon >= SYSDATE  ;
   UPDATE SCH_EVENTS1
      SET isconfirmed = v_eventstat,
	     event_exeon = p_event_exeon,
          last_modified_by = p_dmlby ,
          last_modified_date = SYSDATE,
          ip_add = p_ipadd
    WHERE fk_patprot = v_patprot
      AND isconfirmed <> v_eventstatdone AND status < 5;
   UPDATE SCH_EVENTS1
      SET status = 5,
          last_modified_by = p_dmlby ,
          last_modified_date = SYSDATE,
          ip_add = p_ipadd
   WHERE fk_patprot = v_patprot AND status < 5;
   UPDATE ER_PATPROT
   SET PATPROT_DISCDT = p_event_exeon,
	    PATPROT_REASON  = 'Study Status Changed'
   WHERE pk_patprot = v_patprot;
END Change_stat ;
---------------------------------------------------------
PROCEDURE SP_COPYSTUDYVERSION (
   p_studyver IN NUMBER,
   p_newversion_number IN VARCHAR,p_version_date IN VARCHAR,p_version_cat IN VARCHAR,p_version_type IN VARCHAR,
   p_user IN NUMBER,
   p_vercreated OUT NUMBER)
AS
   /****************************************************************************************************
   ** Procedure to copy a version of study. This will copy the version details, sections and appendix
   ** Parameter Description
   ** p_studyver the versionid which is being copied
   ** p_newversion_number the new version number given by the user
   ** p_user 	user to whom the copy is being given
   ** p_vercreated 1 indicates version created successfully, -1 is error, -3 duplicate version number
   **
   ** Author: Sonika Talwar 10th Dec 2002
   ** Modified by Sonika Talwar 29th Dec 2003 to check duplicate version number
   ** Modified by Sonia Sahni 9th Aug 04 to add a default version status history record
   ** Modified by Sonia Sahni 30th Aug 04 while adding default version status history record, the status date
   ** used to have a 'time' part. Howver from application we do not send any time part. So truncated the time
   ** part
   *****************************************************************************************************
   */
  v_newstudyver NUMBER;
  v_dt          CHAR(12);
  v_cnt         NUMBER;
  v_study       NUMBER;
BEGIN
    --check if the same version number already exists of this study
    SELECT fk_study
	INTO v_study
	FROM ER_STUDYVER
	WHERE pk_studyver=p_studyver;
	SELECT COUNT(*)
	INTO v_cnt
	FROM ER_STUDYVER
	WHERE fk_study=v_study
--JM: 23June2006, modified
--	AND trim(studyver_number)=trim(p_newversion_number);
		AND LOWER(trim(studyver_number))=LOWER(trim(p_newversion_number));

     IF v_cnt > 0 THEN
         p_vercreated := -3;
	    RETURN;
	END IF;



     p_vercreated:=1;
  -- the DateTime format that will be appended to the file names in the Study Appendix where
   -- the apndx type is 'file'.
   SELECT TO_CHAR (SYSDATE, 'DDMMYYHHMISS')
     INTO v_dt
     FROM dual;
   -- The new study version id generated from the seq_er_studyver sequence.
   -- This is stored in the v_newstudyver variable to be used for all the INSERT/SELECT.
    SELECT seq_er_studyver.NEXTVAL
     INTO v_newstudyver
     FROM dual;
     DBMS_OUTPUT.PUT_LINE ('new study  ver' || TO_CHAR(v_newstudyver) ) ;
    BEGIN
    INSERT INTO ER_STUDYVER
            (PK_STUDYVER,FK_STUDY,STUDYVER_NUMBER,
            STUDYVER_STATUS,STUDYVER_NOTES,STUDYVER_DATE,STUDYVER_CATEGORY,STUDYVER_TYPE,ORIG_STUDY,
            CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
              CREATED_ON)
        SELECT v_newstudyver,FK_STUDY,p_newversion_number,
	           'W',STUDYVER_NOTES,STUDYVER_DATE,STUDYVER_CATEGORY,STUDYVER_TYPE,ORIG_STUDY,
			   p_user,p_user,SYSDATE,
               SYSDATE
		FROM ER_STUDYVER
		WHERE pk_studyver=p_studyver;
	 -- insert record into er_status_history for version history
	    INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
					STATUS_MODPK ,	STATUS_MODTABLE,  FK_CODELST_STAT ,
					STATUS_DATE ,
					CREATOR ,
					RECORD_TYPE,
					CREATED_ON,STATUS_ENTERED_BY)
        VALUES ( seq_er_status_history.NEXTVAL,v_newstudyver,'er_studyver',Pkg_Util.f_getCodePk('W','versionStatus'),
		TRUNC(SYSDATE),p_user,'N',SYSDATE,p_user);
      IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into study version ' || TO_CHAR(1)) ;
      ELSE
          DBMS_OUTPUT.PUT_LINE ('Error in sharing study version. ' ) ;
          RAISE_APPLICATION_ERROR (
            -20000,
            'Error in sharing study version. If error persists Pl. Check with Sys Admin'
         );
    		p_vercreated :=-1;
     END IF;
	END;
       -- The second insert for all the study version sections
       --	PK_STUDYSEC            the Primary key is generated
       --	FK_STUDYVER            the versiob id is updated with the new version id
	BEGIN
         INSERT INTO ER_STUDYSEC (
                        pk_studysec,
                        fk_study,
                        studysec_name,
				    studysec_num,
                        studysec_text,
                        studysec_pubflag,
                        studysec_seq,
                        creator,
                        last_modified_by,
                        last_modified_date,
                        created_on,
				    fk_studyver
                     )
             SELECT seq_er_studysec.NEXTVAL,
                    NULL,
                    studysec_name,
				studysec_num,
                    studysec_text,
                    studysec_pubflag,--Ak:Modified for BUG#5315
                    studysec_seq,
                    p_user,
                    p_user,
                    SYSDATE,
                    SYSDATE,
				v_newstudyver
               FROM ER_STUDYSEC
              WHERE fk_studyver = p_studyver;
      IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into study section ' || TO_CHAR(1)) ;
    	   /*  ELSE
          dbms_output.put_line ('Error in sharing study section. ' ) ;
          raise_application_error (
            -20000,
            'Error in sharing study section. If error persists Pl. Check with Sys Admin'
         );
    		p_vercreated :=-1; */
     END IF;
    END;
         -- 	PK_STUDYAPNDX           the pk is generated from seq_er_studyapndx
         -- 	FK_STUDY                the new study id is updated here
         -- 	STUDYAPNDX_FILE         the FILE name is changed for all appendix where type is 'file'
         -- 				a 'ddmmyyhhmiss' format has been appended to the file name
    BEGIN
         INSERT INTO ER_STUDYAPNDX (
                        pk_studyapndx,
                        fk_study,
                        studyapndx_uri,
                        studyapndx_desc,
                        studyapndx_pubflag,
                        creator,
                        last_modified_by,
                        last_modified_date,
                        created_on,
                        studyapndx_file,
                        studyapndx_type,
				    studyapndx_fileobj,
                        studyapndx_filesize,
				    fk_studyver
                     )
             SELECT seq_er_studyapndx.NEXTVAL,
                    NULL,
                    studyapndx_uri,
                    studyapndx_desc,
                    studyapndx_pubflag,--Ak:Modified for BUG#5315
                    p_user,
                    p_user,
                    SYSDATE,
                    SYSDATE,
                    DECODE (
                       trim (studyapndx_type),
                       'file', SUBSTR (
			          studyapndx_uri,
                                  1,
                                   (INSTR (studyapndx_uri, '.', -1) - 1)
                               ) ||
                               v_dt ||
                               SUBSTR (
                                  studyapndx_uri,
                                  INSTR (studyapndx_uri, '.', -1)
                               ),
                       studyapndx_uri
                    ),
                    studyapndx_type,
                    studyapndx_fileobj,
                    studyapndx_filesize,
 			     v_newstudyver
               FROM ER_STUDYAPNDX
              WHERE fk_studyver = p_studyver ;
      IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into study appendix ' || TO_CHAR(1)) ;
      /*ELSE
          dbms_output.put_line ('Error in sharing study appendix. ' ) ;
          raise_application_error (
            -20000,
            'Error in sharing study appendix. If error persists Pl. Check with Sys Admin'
         );
     	p_vercreated :=-1; */
     END IF;
	END;
	COMMIT;
END;
---------------------------------------------------------------------------------------------
 PROCEDURE SP_CREATE_DEFAULT_VERSION (p_study IN NUMBER,p_user IN NUMBER,p_ip IN VARCHAR2)
AS
  v_verpk NUMBER;
    BEGIN
   /****************************************************************************************************
   ** Procedure to insert a default version for the new study
   ** Parameter Description
   ** p_study 	the Study whose version has to be created
   ** p_user 	user who created the record
   ** p_ip  ip address of the user
   **
   ** Author: Sonika Talwar 16th Jan 2003
   ** Modified by Sonia Sahni 9th Aug 04 to add a default version status history record
   ** Modified by Sonia Sahni 30th Aug 04 while adding default version status history record, the status date
   ** used to have a 'time' part. Howver from application we do not send any time part. So truncated the time
   ** part
   *****************************************************************************************************
   */
   --JM: 020306: added default STUDYVER_CATEGORY = 'Miscellanous'
   SELECT  seq_er_studyver.NEXTVAL
   INTO  v_verpk
   FROM dual;
 INSERT INTO ER_STUDYVER (PK_STUDYVER,
                          FK_STUDY,
                          STUDYVER_NUMBER,
						  STUDYVER_STATUS,
						  STUDYVER_CATEGORY,
						  CREATOR,
						  CREATED_ON,
                          IP_ADD )
                  VALUES (v_verpk,
			  p_study,
                          '1',
			  'W',(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='studyvercat'AND codelst_subtyp='misc'),
			  p_user,SYSDATE,
			  p_ip);
 -- insert record into er_status_history for version history
	    INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
					STATUS_MODPK ,	STATUS_MODTABLE,
					FK_CODELST_STAT ,
					STATUS_DATE ,
					CREATOR , RECORD_TYPE, CREATED_ON,IP_ADD,STATUS_ENTERED_BY)
        VALUES ( seq_er_status_history.NEXTVAL,
			   v_verpk,'er_studyver',
			   Pkg_Util.f_getCodePk('W','versionStatus'),
			   TRUNC(SYSDATE),
			   p_user,'N',SYSDATE,p_ip,p_user);
   COMMIT;
   END;
---------------------------------------------------------------------------------------------
PROCEDURE Sp_Copystudy_Selective(
  p_origstudy IN NUMBER,
  p_statflag IN NUMBER,
  p_teamflag IN NUMBER,
  p_dictflag IN NUMBER,
  p_verarr IN ARRAY_STRING,
  p_calarr IN ARRAY_STRING,
  p_txarr IN ARRAY_STRING,
  p_formsarr IN ARRAY_STRING,
  p_newstudynum IN VARCHAR2,
  p_newstudydm IN NUMBER,
  p_newstudytitle IN VARCHAR2,
  p_user IN NUMBER,
  p_ip IN VARCHAR2,
  o_success OUT NUMBER)
  AS
   v_newstudy NUMBER;
   v_rights                      VARCHAR2(25);
   --added by salil on 24 sep 2003
   v_oldrights                      VARCHAR2(25);

   v_oldrole 		   NUMBER;--JM
   v_role  NUMBER;
   v_oldpkstudyver NUMBER;
   v_newpkstudyver NUMBER;
   v_newstudysite NUMBER;
   v_orig_studysite NUMBER;
   v_dt  CHAR(12);
   v_oldcal NUMBER;
   v_newcal NUMBER;
   v_new_ver NUMBER;
   cnt_datamng NUMBER;

   cnt_datamnger NUMBER;
      cnt_otherrole NUMBER;--JM
	  cnt_dm NUMBER;
   v_creator_site_cnt NUMBER;
   v_creator_site NUMBER;
   v_newstudyid NUMBER;
   v_oldtxarm NUMBER;
   v_newtx NUMBER;
   v_oldform NUMBER;
   v_newfrm NUMBER;
   v_ret NUMBER;
   v_formdisptype VARCHAR2(2);
   v_entrychar CHAR(1);
   v_formname VARCHAR2(50);
   v_formdesc VARCHAR2(255);
   v_newform NUMBER;
   v_pk_linkedform NUMBER;
   v_fk_account NUMBER;
   v_formsarr ARRAY_STRING := ARRAY_STRING();


   v_siteflag CHAR(1);
   v_studyteam_status VARCHAR2(30);
      v_newpkstudyteam NUMBER;--JM
      v_codelst_pk number;

      v_study_advlkp_ver number;

   v_old_pk NUMBER;
   v_new_pk NUMBER;

    BEGIN
   /****************************************************************************************************
   ** Procedure to create a copy of the study. Will be used for enhancements required for April'03 : multi-org requirements
   ** Parameter Description
   ** p_origstudy : the Study that is tobe copied
   ** p_statflag : flag if the study status will be copied - 0 : No, 1 : Yes
   ** p_teamflag : flag if the study team will be copied - 0 : No, 1 : Yes
   ** p_verarr : Array of version ids that will be copied
   ** p_calarr : Array of calendar ids that will be copied
   ** p_newstudynum : study number for new study
   ** p_newstudydm :data manager for new study
   ** p_newstudytitle : title for new study
   ** p_user : user who created the record
   ** p_ip : ip address of the user
   ** o_success : Out parameter to indicate success, 0 : successful, 1 : unsuccessful
   **
   ** Author: Sonia Sahni , 5th May 2003
   ** Modified By: Salil, 1st Oct 2003 to copy only study team members and not super users
   ** Modified By: Sam, 8/5/04  fix for bug #1551, when a study is copied, the calendar status must be WIP nd creator must be set. Also added last_modified_by and ip_add
   ** Modified By: Sonia, 08/06/04  copy new study columns : STUDY_DIVISION , STUDY_INVIND_FLAG , STUDY_INVIND_NUMBER
   ** Modified by Sonia Sahni 9th Aug 04 to add a default version status history record
   ** Modified by Sonia Sahni 30th Aug 04 : while adding default version status history record, the status date
   ** used to have a 'time' part. Howver from application we do not send any time part. So truncated the time
   ** part
   ** JM: 04-10-06 modified the query to rectify copy study problem, columns added are STUDY_OTHERPRINV, STUDY_MAJ_AUTH,
   ** STUDY_DISEASE_SITE,STUDY_ICDCODE1, STUDY_ICDCODE2, STUDY_NSAMPLSIZE, STUDY_ASSOC
   ** JM: 05-04-06 study_pubcollst were copied as '0000', changed to variable study_pubcollst
   **
   ** JM: 31July2006: modified: New Parameters added
   ** p_dictflag : flag if the study dictionaries and settings will be copied to the new study
   ** p_txarr	 : Array of tx Arm ids thaose will be coopied
   ** p_formsarr : Array of from ids those will be coopied
   *****************************************************************************************************
   */
      --JM:		04FEB2011,  #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS from EVENT_ASSOC table
     select pk_codelst into v_codelst_pk from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='W';
   -- copy study summary
    SELECT seq_er_study.NEXTVAL INTO v_newstudy   FROM dual;
    SELECT TO_CHAR (SYSDATE, 'DDMMYYHHMISS') INTO v_dt FROM dual;
    --check if the new study number is unique for the account, return -2 if not unique
	-- modified by gopu for september enhancement #S7 added two column (STUDY_SPONSORID,FK_CODELST_SPONSOR)
        -- modified by Amarnadh for issue #3208 28th Nov '07

        -- JM: 01Sep2008, #3782
        IF 	p_dictflag = 1 THEN
         SELECT study_advlkp_ver into  v_study_advlkp_ver FROM ER_STUDY WHERE pk_study=p_origstudy;
        else
         v_study_advlkp_ver := 0;
        end if;

        BEGIN
           INSERT INTO ER_STUDY (pk_study, study_sponsor, fk_account, study_contact, study_pubflag, study_title,
                        study_obj_clob,study_sum_clob, study_prodname, study_samplsize, study_dur,
                        study_durunit, study_estbegindt, study_actualdt, study_prinv,
			STUDY_OTHERPRINV, STUDY_MAJ_AUTH, STUDY_DISEASE_SITE,STUDY_ICDCODE1, STUDY_ICDCODE2, STUDY_NSAMPLSIZE, STUDY_ASSOC,
                        study_partcntr, study_keywrds, study_pubcollst, study_parentid,
                        fk_author, fk_codelst_tarea, fk_codelst_phase, fk_codelst_blind,
                        fk_codelst_random, creator, study_current, last_modified_by, last_modified_date,
                        created_on,study_number,fk_codelst_restype,FK_CODELST_CURRENCY,study_end_date,ip_add,
        STUDY_INFO,FK_CODELST_SCOPE,FK_CODELST_TYPE,STUDY_ADVLKP_VER,STUDY_DIVISION ,
                      STUDY_INVIND_FLAG , STUDY_INVIND_NUMBER,STUDY_COORDINATOR,STUDY_SPONSORID,FK_CODELST_SPONSOR,STUDY_CTRP_REPORTABLE
                     )
           SELECT   v_newstudy, study_sponsor,fk_account,study_contact,'N',
                    study_title,study_obj_clob,study_sum_clob,study_prodname,study_samplsize,
                    study_dur,study_durunit,study_estbegindt,study_actualdt,study_prinv,
		    STUDY_OTHERPRINV, STUDY_MAJ_AUTH, STUDY_DISEASE_SITE,STUDY_ICDCODE1, STUDY_ICDCODE2, STUDY_NSAMPLSIZE, STUDY_ASSOC,
		    study_partcntr,study_keywrds,study_pubcollst,p_origstudy,
                    p_newstudydm,fk_codelst_tarea,fk_codelst_phase,fk_codelst_blind,
                    fk_codelst_random,p_user,study_current,NULL,NULL,SYSDATE,p_newstudynum ,
                    fk_codelst_restype, FK_CODELST_CURRENCY,study_end_date,p_ip,study_info,fk_codelst_scope,
    FK_CODELST_TYPE,V_STUDY_ADVLKP_VER,STUDY_DIVISION ,
                      STUDY_INVIND_FLAG , STUDY_INVIND_NUMBER,STUDY_COORDINATOR, STUDY_SPONSORID,FK_CODELST_SPONSOR,STUDY_CTRP_REPORTABLE
           FROM ER_STUDY
           WHERE pk_study = p_origstudy;
   IF LENGTH(trim(p_newstudytitle)) > 0 THEN
      UPDATE ER_STUDY
         SET study_title = p_newstudytitle
     WHERE pk_study = v_newstudy;
   END IF;
   EXCEPTION WHEN NO_DATA_FOUND THEN
         o_success := -1;
         END ;
-- Ankit CTRP-20527 24-Nov-2011 added new column STUDY_CTRP_REPORTABLE for copy study.
--JM: added 04-10-06 copy more study details
	FOR i IN(SELECT PK_STUDYID FROM ER_STUDYID WHERE fk_study = p_origstudy)
	LOOP

	SELECT seq_er_studyid.NEXTVAL INTO v_newstudyid FROM dual;
	  -- copy more study details

	INSERT INTO ER_STUDYID ( PK_STUDYID,FK_STUDY,FK_CODELST_IDTYPE,STUDYID_ID,CREATOR,CREATED_ON,IP_ADD )

	SELECT v_newstudyid, v_newstudy,FK_CODELST_IDTYPE,STUDYID_ID,p_user,SYSDATE,p_ip
	FROM  ER_STUDYID WHERE PK_STUDYID=i.PK_STUDYID;
	END LOOP;


  /*INSERT INTO ER_STUDYSITES(PK_STUDYSITES, FK_STUDY, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE, CREATOR, CREATED_ON, IP_ADD)
  SELECT seq_er_studysites.NEXTVAL, v_newstudy, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE,p_user, SYSDATE,p_ip
  FROM ER_STUDYSITES WHERE fk_study = p_origstudy*/
  INSERT INTO ER_STUDYSITES(PK_STUDYSITES, FK_STUDY, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE, CREATOR, CREATED_ON, IP_ADD)
  SELECT  seq_er_studysites.NEXTVAL, v_newstudy,
  a,b,c,d,e,
  p_user, SYSDATE,p_ip
  FROM (
  SELECT  FK_SITE a, FK_CODELST_STUDYSITETYPE b, STUDYSITE_LSAMPLESIZE c, STUDYSITE_PUBLIC d, STUDYSITE_REPINCLUDE e
  FROM ER_STUDYSITES WHERE fk_study = p_origstudy
  UNION
  SELECT  FK_SITEID, NULL, NULL, NULL, NULL
  FROM ER_USER WHERE pk_user  = p_newstudydm
  AND fk_siteid NOT IN (SELECT fk_site FROM ER_STUDYSITES WHERE fk_study = p_origstudy)
  );
  -- copy access rights for user
  /*INSERT INTO ER_STUDY_SITE_RIGHTS(PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,
  USER_STUDY_SITE_RIGHTS,CREATOR, CREATED_ON, IP_ADD)
  SELECT seq_er_studysite_rights.NEXTVAL,fk_site, v_newstudy, FK_USER,
   USER_STUDY_SITE_RIGHTS,p_user, SYSDATE,p_ip
   FROM ER_STUDY_SITE_RIGHTS
   WHERE fk_study = p_origstudy
   */
  INSERT INTO ER_STUDY_SITE_RIGHTS(PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,
  USER_STUDY_SITE_RIGHTS,CREATOR, CREATED_ON, IP_ADD)
  SELECT seq_er_studysite_rights.NEXTVAL,a,v_newstudy,b,c,p_user, SYSDATE,p_ip
  FROM(
  SELECT fk_site a, FK_USER b, USER_STUDY_SITE_RIGHTS c
   FROM ER_STUDY_SITE_RIGHTS
   WHERE fk_study = p_origstudy
   UNION
   SELECT fk_site, FK_USER,
   CASE WHEN USERSITE_RIGHT > 0 THEN 1 ELSE 0 END CASE
   FROM ER_USERSITE
   WHERE fk_user =  p_newstudydm
   AND fk_user NOT IN(SELECT FK_USER
   FROM ER_STUDY_SITE_RIGHTS
   WHERE fk_study = p_origstudy )
   );
   FOR i IN(SELECT pk_studysites FROM ER_STUDYSITES WHERE fk_study = p_origstudy)
  LOOP
  -- copy study organizations
  SELECT seq_er_studysites.NEXTVAL INTO v_newstudysite FROM dual;
  -- copy study info
  INSERT INTO ER_STUDY_SITE_ADD_INFO(PK_STUDYSITES_ADDINFO,FK_STUDYSITES,FK_CODELST_SITEADDINFO,
  STUDYSITE_DATA,CREATOR,CREATED_ON,IP_ADD)
  SELECT seq_er_studysites_addinfo.NEXTVAL,v_newstudysite, FK_CODELST_SITEADDINFO,
  STUDYSITE_DATA,p_user, SYSDATE,p_ip
  FROM ER_STUDY_SITE_ADD_INFO
  WHERE  FK_STUDYSITES = i.pk_studysites;
  -- copy appendix info
  INSERT INTO ER_STUDYSITES_APNDX(PK_STUDYSITES_APNDX,FK_STUDYSITES,APNDX_TYPE,APNDX_NAME,APNDX_DESCRIPTION,
  APNDX_FILE,APNDX_FILESIZE,CREATOR,CREATED_ON,IP_ADD)
  SELECT seq_er_studysites_apndx.NEXTVAL, v_newstudysite, APNDX_TYPE,APNDX_NAME,APNDX_DESCRIPTION,
  APNDX_FILE,APNDX_FILESIZE,p_user, SYSDATE,p_ip
  FROM ER_STUDYSITES_APNDX
  WHERE FK_STUDYSITES = i.pk_studysites;
 END LOOP;
    -- copy study status records if p_statflag = 1
     IF p_statflag = 1 THEN
         DELETE FROM ER_STUDYSTAT WHERE fk_study = v_newstudy;
--JM: 04-10-06 modified: STUDYSTAT_VALIDT column added in the ql
         --current_stat is added by Manimaran for the september enhancement s8.
		 --Mukul:14-04-2010 modified: status_type column added in query to fix BUG-4830
         INSERT INTO ER_STUDYSTAT(PK_STUDYSTAT ,FK_USER_DOCBY  , FK_CODELST_STUDYSTAT ,FK_STUDY,
         STUDYSTAT_DATE ,STUDYSTAT_HSPN  ,STUDYSTAT_NOTE  ,STUDYSTAT_ENDT, STUDYSTAT_VALIDT,
         FK_CODELST_APRSTAT  , CREATOR   ,CREATED_ON  ,FK_CODELST_APRNO ,IP_ADD , fk_site, current_stat,status_type )
     SELECT seq_er_studystat.NEXTVAL, FK_USER_DOCBY  , FK_CODELST_STUDYSTAT ,v_newstudy,
         STUDYSTAT_DATE ,STUDYSTAT_HSPN  ,STUDYSTAT_NOTE  ,STUDYSTAT_ENDT, STUDYSTAT_VALIDT,
         FK_CODELST_APRSTAT  , p_user, SYSDATE,FK_CODELST_APRNO ,p_ip, fk_site, current_stat,status_type
     FROM ER_STUDYSTAT
     WHERE fk_study = p_origstudy ;
  UPDATE ER_STUDY a
     SET a.study_actualdt = (SELECT b.study_actualdt FROM
                             ER_STUDY b WHERE b.pk_study = p_origstudy ) ,
         a.study_end_date = (SELECT b.study_end_date FROM
                             ER_STUDY b WHERE b.pk_study = p_origstudy )
         WHERE a.pk_study = v_newstudy;
    ELSE
         UPDATE ER_STUDY
     SET study_actualdt = NULL, study_end_date = NULL
     WHERE pk_study = v_newstudy;
    END IF;
/*  --JM: 24July2006 blocked

	--copy study team if  p_teamflag = 1
    -- create rights string - all rights are given to the study data manager
       FOR i IN ( SELECT CTRL_VALUE
        FROM ER_CTRLTAB  WHERE CTRL_KEY ='study_rights'
        ORDER BY CTRL_SEQ )
        LOOP
             v_rights := v_rights || '7';
    END LOOP;

*/  --JM: 24July2006 blocked
--JM: 24July2006
SELECT CTRL_VALUE INTO v_rights FROM ER_CTRLTAB WHERE CTRL_KEY = 'role_dm';

    --insert into t(c3,c4) values ('intial',v_rights);
    --get role for data manager
    SELECT ctrl_value INTO v_role  FROM ER_CTRLTAB
     WHERE trim(ctrl_key) = 'author';
 -- insert study team record for data manager
    --insert into t(c3,c4) values ('for dm',v_rights);



-----------------------------------------------------------------------
	IF  p_teamflag = 1 THEN -- copy the existing team
		INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
                                   FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD,
           study_team_usr_type ,STUDY_SITEFLAG, STUDYTEAM_STATUS )
SELECT seq_er_studyteam.NEXTVAL, FK_CODELST_TMROLE, FK_USER , v_newstudy, p_user,SYSDATE,STUDY_TEAM_RIGHTS , p_ip,
  study_team_usr_type, STUDY_SITEFLAG, STUDYTEAM_STATUS
    FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy--AND fk_user <> p_newstudydm
	--JM now unblocked june29: logged in user will not be copied to the study team
	AND fk_user <> p_user;
--AND fk_user <> p_newstudydm;

	----JM: 05-15-06 Blocked as logged in user were not copied to the study team.
	--AND fk_user <> p_newstudydm 	--AND fk_user <> p_user;

--JM: 04-10-06 Blocked
    --AND NVL(study_team_usr_type,'D') = 'D' ;--added by salil on 24 sep 2003
	--END IF ;

	ELSE

	--///
	SELECT COUNT(*) INTO cnt_datamng FROM ER_STUDYTEAM
		    WHERE fk_study = p_origstudy AND fk_user = p_user
		       AND NVL(study_team_usr_type,'D') = 'D' ;


			   --JM: 20Jul2006

	SELECT COUNT(*) INTO cnt_datamnger FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user
    AND NVL(study_team_usr_type,'D') = 'D' AND FK_CODELST_TMROLE <> v_role;

			   ---------------------------------------------

   IF(cnt_datamng > 0) THEN---apply the existing role

		       --JM: 20Jul2006
		IF	( cnt_datamnger > 0) THEN

					INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
			              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type )

			        VALUES (seq_er_studyteam.NEXTVAL, v_role,p_newstudydm,v_newstudy, p_user,SYSDATE,
			         v_rights, p_ip, 'D'); --added with all rights

		ELSE

					   INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
				              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type )

				     VALUES (seq_er_studyteam.NEXTVAL, v_role,p_newstudydm,v_newstudy, p_user,SYSDATE,
				         v_rights, p_ip, 'D');
		END IF;



	ELSE
	--///

	INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type )

     VALUES (seq_er_studyteam.NEXTVAL, v_role,p_newstudydm,v_newstudy, p_user,SYSDATE,
         v_rights, p_ip, 'D');
	END IF;

   END IF;

 --//JM: 30June2006: When Study Team is copied and if the logged in user exists in the source Study Team,
--should go to the copied study as DM when p_newstudydm = p_user with all rights.

--JM: 09Nov2006

BEGIN
SELECT STUDY_SITEFLAG, STUDYTEAM_STATUS INTO v_siteflag,  v_studyteam_status FROM ER_STUDYTEAM WHERE
FK_STUDY=p_origstudy AND STUDY_TEAM_USR_TYPE IN ('D','X') AND FK_USER = p_user;


EXCEPTION WHEN NO_DATA_FOUND THEN

  		v_siteflag := '';
		v_studyteam_status := '';





END;
--JM: 09Nov2006


IF  p_teamflag = 1  THEN
IF p_newstudydm = p_user THEN
				INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
			     FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type, STUDY_SITEFLAG, STUDYTEAM_STATUS )
			    VALUES (seq_er_studyteam.NEXTVAL, v_role, p_newstudydm,v_newstudy, p_user,SYSDATE,
			     v_rights, p_ip, 'D', v_siteflag, v_studyteam_status);


 		  ----////////////////
END IF;
END IF;
--///////////////////////////////////////////////////

  IF  p_teamflag = 1 THEN
 -- if data manager and creator are different, insert a record for the creator also
IF p_newstudydm <> p_user THEN
SELECT fk_siteid INTO v_creator_site FROM ER_USER WHERE pk_user = p_user;
     SELECT COUNT(*) INTO v_creator_site_cnt
  FROM ER_STUDYSITES WHERE fk_site = (SELECT fk_siteid FROM ER_USER WHERE pk_user = p_user)
  AND fk_study = v_newstudy;
      --- to get the rights of the old data manager done by salil 24 sep 2003
   IF(v_creator_site_cnt = 0) THEN
     INSERT INTO ER_STUDYSITES(PK_STUDYSITES, FK_STUDY, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE, CREATOR, CREATED_ON, IP_ADD)
  VALUES(seq_er_studysites.NEXTVAL, v_newstudy,v_creator_site,NULL,NULL,NULL,NULL,p_user, SYSDATE, p_ip);
  END IF;

    SELECT COUNT(*) INTO cnt_datamng FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user
       AND NVL(study_team_usr_type,'D') = 'D' ;


	--JM: 20Jul2006

	SELECT COUNT(*) INTO cnt_otherrole FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user -- or p_newstudydm
    AND NVL(study_team_usr_type,'D') = 'D' AND FK_CODELST_TMROLE <> v_role;


			   ---------------------------------------------
   IF(cnt_datamng >0) THEN

   IF (cnt_otherrole>0) THEN

   ---1---old rights
   SELECT STUDY_TEAM_RIGHTS INTO v_oldrights FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user
       AND NVL(study_team_usr_type,'D') = 'D' AND FK_CODELST_TMROLE <> v_role;

	   ----2----old role
	   SELECT FK_CODELST_TMROLE INTO v_oldrole FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user
       AND NVL(study_team_usr_type,'D') = 'D' AND FK_CODELST_TMROLE <> v_role;

   --JM: 20Jul2006 for role other than the DM
   INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type,STUDY_SITEFLAG, STUDYTEAM_STATUS )
        VALUES (seq_er_studyteam.NEXTVAL, v_oldrole,p_user,v_newstudy, p_user,SYSDATE,
          v_oldrights, p_ip, 'D',v_siteflag, v_studyteam_status);

   ELSE

    SELECT STUDY_TEAM_RIGHTS INTO v_oldrights FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user
       AND NVL(study_team_usr_type,'D') = 'D' ;

        INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type,STUDY_SITEFLAG, STUDYTEAM_STATUS  )
        VALUES (seq_er_studyteam.NEXTVAL, v_role,p_user,v_newstudy, p_user,SYSDATE,
          v_oldrights, p_ip, 'D',v_siteflag, v_studyteam_status);-- v_oldrights added by salil on 24 sep 2003

END IF;

     INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON)
      SELECT SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL,FK_SITE,v_newstudy,p_user,1,p_user,SYSDATE
      FROM ER_USERSITE s,ER_USER u
      WHERE s.fk_user = u.pk_user
      AND s.fk_user = p_user
      AND USERSITE_RIGHT >= 4
      AND fk_user NOT IN(SELECT FK_USER
      FROM ER_STUDY_SITE_RIGHTS
      WHERE fk_study = p_origstudy );
      INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON)
      SELECT SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL,FK_SITE,v_newstudy,p_user,0,p_user,SYSDATE
      FROM ER_USERSITE s,ER_USER u
      WHERE s.fk_user = u.pk_user
      AND s.fk_user = p_user
      AND USERSITE_RIGHT < 4
      AND fk_user NOT IN(SELECT FK_USER
       FROM ER_STUDY_SITE_RIGHTS
       WHERE fk_study = p_origstudy );
     END IF;

--//JM: 30June2006, when two users are different  p_newstudydm should go to the Study Team

----------------------------------------
	--JM: 19July06

SELECT COUNT(*) INTO cnt_datamnger FROM ER_STUDYTEAM
    WHERE fk_study = v_newstudy AND fk_user = p_newstudydm
       AND NVL(study_team_usr_type,'D') = 'D' AND FK_CODELST_TMROLE <> v_role;
    IF(cnt_datamnger >0) THEN


	UPDATE ER_STUDYTEAM SET FK_CODELST_TMROLE = v_role ,STUDY_TEAM_RIGHTS=v_rights WHERE fk_study = v_newstudy AND  fk_user = p_newstudydm
	AND NVL(study_team_usr_type,'D') = 'D' ;


	END IF ;

--Specified DM goes in the ST team from here

--JM: 19July06
SELECT COUNT(*) INTO cnt_dm FROM ER_STUDYTEAM
	WHERE fk_study = v_newstudy AND fk_user = p_newstudydm AND NVL(study_team_usr_type,'D') = 'D'
	AND FK_CODELST_TMROLE = v_role;

	IF ( ( cnt_dm =0  ) OR (cnt_dm = NULL) OR (cnt_dm < 0 )) THEN


	 		   INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
			     FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type,STUDY_SITEFLAG, STUDYTEAM_STATUS )
			    VALUES (seq_er_studyteam.NEXTVAL, v_role,p_newstudydm,v_newstudy, p_user,SYSDATE,
			    v_rights , p_ip, 'D',v_siteflag, v_studyteam_status );

				ELSE


	UPDATE ER_STUDYTEAM SET FK_CODELST_TMROLE = v_role  ,STUDY_TEAM_RIGHTS=v_rights WHERE fk_study =v_newstudy AND  fk_user = p_newstudydm
	AND NVL(study_team_usr_type,'D') = 'D' ;


	END IF;

--//JM: existing 'S' into the Study Team

--//--------------------------------------------------------JM---------------------------------------------------



     END IF;-- study team selected
 END IF;


--//////////////////////////////JM: 9Nov2006, #2709---
--//Modified by Gopu to fix the bugzilla Issue #2711

IF  p_teamflag = 1  THEN
FOR j IN (SELECT PK_STUDYTEAM FROM ER_STUDYTEAM WHERE fk_study = v_newstudy AND STUDY_TEAM_USR_TYPE IN ('D','X'))
LOOP

	INSERT INTO ER_STATUS_HISTORY( PK_STATUS , STATUS_MODPK , STATUS_MODTABLE, FK_CODELST_STAT ,STATUS_DATE ,
	   CREATOR , RECORD_TYPE, CREATED_ON,IP_ADD,STATUS_ENTERED_BY, STATUS_ISCURRENT)
        VALUES ( seq_er_status_history.NEXTVAL,
      j.PK_STUDYTEAM,'er_studyteam',(SELECT pk_codelst FROM  ER_CODELST WHERE codelst_type = 'teamstatus'
	  AND CODELST_SUBTYP=(SELECT STUDYTEAM_STATUS FROM ER_STUDYTEAM WHERE PK_STUDYTEAM =j.PK_STUDYTEAM)),
      TRUNC(SYSDATE),
      p_user,'N',SYSDATE,p_ip,p_user,1);

END LOOP ;

END IF;
--//////////////////////////////JM: 9Nov2006, #2709---



 -- get study versions from   p_verarr (TYPES.NUMBER_ARRAY)
 FOR i IN 1..p_verarr.COUNT
 LOOP
          v_oldpkstudyver := TO_NUMBER(p_verarr(i));
  --copy version details
  SELECT seq_er_studyver.NEXTVAL INTO v_newpkstudyver  FROM dual;
  INSERT INTO ER_STUDYVER  (PK_STUDYVER,FK_STUDY,STUDYVER_NUMBER,
            STUDYVER_STATUS,STUDYVER_NOTES,STUDYVER_DATE,STUDYVER_CATEGORY,STUDYVER_TYPE, CREATOR, CREATED_ON, IP_ADD)
          SELECT v_newpkstudyver ,v_newstudy,STUDYVER_NUMBER,
            STUDYVER_STATUS,STUDYVER_NOTES,STUDYVER_DATE,STUDYVER_CATEGORY,STUDYVER_TYPE, p_user, SYSDATE, p_ip
     FROM ER_STUDYVER
  WHERE pk_studyver= v_oldpkstudyver;
   -- insert record into er_status_history for version history
     INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
     STATUS_MODPK , STATUS_MODTABLE,
     FK_CODELST_STAT ,
     STATUS_DATE ,
     CREATOR , RECORD_TYPE, CREATED_ON,IP_ADD,STATUS_ENTERED_BY)
        VALUES ( seq_er_status_history.NEXTVAL,
      v_newpkstudyver,'er_studyver',
      Pkg_Util.f_getCodePk('W','versionStatus'),
      TRUNC(SYSDATE),
      p_user,'N',SYSDATE,p_ip,p_user);
  --copy sections
  INSERT INTO ER_STUDYSEC ( pk_studysec, fk_study,studysec_name, studysec_num,
                        studysec_text, studysec_pubflag,
                        studysec_seq,creator, created_on,fk_studyver,ip_add)
             SELECT seq_er_studysec.NEXTVAL,NULL, studysec_name,studysec_num,
                    studysec_text, studysec_pubflag, studysec_seq,
                    p_user, SYSDATE,v_newpkstudyver,p_ip
               FROM ER_STUDYSEC
              WHERE fk_studyver = v_oldpkstudyver;
      --insert appendix
        INSERT INTO ER_STUDYAPNDX (pk_studyapndx,fk_study,studyapndx_uri,studyapndx_desc,
                        studyapndx_pubflag,creator,created_on,studyapndx_file,studyapndx_type,
        studyapndx_fileobj,studyapndx_filesize,fk_studyver,ip_add )
            SELECT seq_er_studyapndx.NEXTVAL, NULL,studyapndx_uri,studyapndx_desc,
                    studyapndx_pubflag,p_user,SYSDATE,  DECODE (trim (studyapndx_type), 'file', SUBSTR ( studyapndx_uri,
                                  1, (INSTR (studyapndx_uri, '.', -1) - 1) ) || v_dt ||
                               SUBSTR (studyapndx_uri, INSTR (studyapndx_uri, '.', -1)), studyapndx_uri),
                    studyapndx_type,  studyapndx_fileobj,  studyapndx_filesize,
         v_newpkstudyver,p_ip
               FROM ER_STUDYAPNDX
              WHERE fk_studyver = v_oldpkstudyver ;
 END LOOP;
 IF p_verarr.COUNT < 1 THEN --if no version is selected, insert a default version
   SELECT seq_er_studyver.NEXTVAL
   INTO v_new_ver
   FROM dual;
      INSERT INTO ER_STUDYVER (PK_STUDYVER,FK_STUDY,STUDYVER_NUMBER,STUDYVER_STATUS,
          CREATOR,CREATED_ON,IP_ADD )
      VALUES (v_new_ver,v_newstudy, '1','W', p_user, SYSDATE,p_ip);
   -- insert record into er_status_history for version history
     INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
     STATUS_MODPK , STATUS_MODTABLE,
     FK_CODELST_STAT ,
     STATUS_DATE ,
     CREATOR , RECORD_TYPE, CREATED_ON,IP_ADD,STATUS_ENTERED_BY)
        VALUES ( seq_er_status_history.NEXTVAL,
      v_new_ver,'er_studyver',
      Pkg_Util.f_getCodePk('W','versionStatus'),
      SYSDATE,
      p_user,'N',SYSDATE,p_ip,p_user);
 END IF;
 -- get study versions from   p_calarr (TYPES.NUMBER_ARRAY)
 FOR i IN 1..p_calarr.COUNT
 LOOP
         v_oldcal := TO_NUMBER(p_calarr(i));
     -- insert calendar record
        SELECT EVENT_DEFINITION_SEQ.NEXTVAL
        INTO v_newcal FROM DUAL;
  --SV, 8/5/04, fix for bug #1551, when a study is copied, the calendar status must be WIP nd creator must be set. Also added last_modified_by and ip_add
  --JM: 04-10-06 added 'event_calassocto' column in the query
    INSERT INTO EVENT_ASSOC
         (
        EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES,
       COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,event_calassocto,
           FUZZY_PERIOD, MSG_TO, fk_codelst_calstat, DESCRIPTION, DISPLACEMENT,
           ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
           PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
           PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
           STATUS_CHANGEBY, CREATOR, LAST_MODIFIED_BY, IP_ADD, FK_CATLIB
         )
         SELECT v_newcal,v_newstudy,EVENT_TYPE, NAME, NOTES,
       COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,event_calassocto,
           FUZZY_PERIOD, MSG_TO, v_codelst_pk, DESCRIPTION, DISPLACEMENT, ORG_ID,
   EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
           PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
           PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, SYSDATE,
           p_user, p_user, p_user, p_ip, FK_CATLIB
     FROM EVENT_ASSOC
     WHERE event_id =  v_oldcal AND CHAIN_ID = p_origstudy
		 AND event_type = 'P';
         -- copy event details of the protocol
         --changed by Sonika on March 09,04, added ip
         sp_cpprotevents(v_oldcal , v_newcal, p_user,p_ip ) ;
     END LOOP;
     
    
    -- end of procedure

--JM: 01August2005--//////////////


IF 	p_dictflag = 1 THEN

--JM:101906
--UPDATE ER_STUDY SET study_advlkp_ver =(SELECT study_advlkp_ver FROM ER_STUDY WHERE pk_study=p_origstudy ) WHERE pk_study=v_newstudy;
--end if;

INSERT INTO ER_SETTINGS (SETTINGS_PK, SETTINGS_MODNUM, SETTINGS_MODNAME, SETTINGS_KEYWORD, SETTINGS_VALUE )
SELECT SEQ_ER_SETTINGS.NEXTVAL, v_newstudy, SETTINGS_MODNAME, SETTINGS_KEYWORD, SETTINGS_VALUE FROM
ER_SETTINGS WHERE SETTINGS_MODNUM = p_origstudy ;
END IF;


FOR j IN 1..p_txarr.COUNT
LOOP
v_oldtxarm := TO_NUMBER(p_txarr(j));

SELECT SEQ_ER_STUDY_TX_ARM.NEXTVAL INTO v_newtx FROM dual;

INSERT INTO ER_STUDYTXARM (PK_STUDYTXARM, FK_STUDY, TX_NAME, TX_DRUG_INFO, TX_DESC, CREATOR,
LAST_MODIFIED_BY, CREATED_ON, LAST_MODIFIED_ON, IP_ADD)

SELECT v_newtx, v_newstudy, TX_NAME, TX_DRUG_INFO, TX_DESC, p_user, NULL, SYSDATE, NULL, NULL FROM  ER_STUDYTXARM
WHERE PK_STUDYTXARM = v_oldtxarm AND FK_STUDY =  p_origstudy;


END LOOP;

--///

SELECT fk_account INTO v_fk_account FROM ER_USER WHERE pk_user = p_user;

	v_formsarr.EXTEND;--JM: 010407

FOR j IN 1..p_formsarr.COUNT
LOOP

	v_oldform := TO_NUMBER(p_formsarr(j));

	--v_formsarr.EXTEND;--JM: blocked and moved up
	v_formsarr(1) := v_oldform;


	SELECT FORM_DESC INTO v_formdesc FROM  ER_FORMLIB WHERE PK_FORMLIB = v_oldform ;
	SELECT FORM_NAME INTO v_formname FROM  ER_FORMLIB WHERE PK_FORMLIB = v_oldform ;
	SELECT LF_DISPLAYTYPE INTO v_formdisptype FROM ER_LINKEDFORMS WHERE FK_FORMLIB = v_oldform AND fk_study = p_origstudy;
	SELECT LF_ENTRYCHAR INTO v_entrychar FROM ER_LINKEDFORMS WHERE FK_FORMLIB = v_oldform AND fk_study = p_origstudy;


	Pkg_Form.SP_COPY_MULTIPLE_FORMS(v_formsarr,v_formdesc, '0', 'S', v_newstudy, p_user, v_formname, p_ip, v_newform);



--JM: 31Oct2006
v_formsarr(1) := 0;


--///////////////////////


IF (v_newform >= 0 ) THEN

	SELECT seq_er_linkedforms.NEXTVAL INTO v_pk_linkedform FROM dual ;


	INSERT INTO ER_LINKEDFORMS
	  ( PK_LF ,  FK_FORMLIB ,    LF_DISPLAYTYPE , LF_LNKFROM ,
	        LF_ENTRYCHAR , FK_ACCOUNT , FK_STUDY ,
	    RECORD_TYPE,  CREATOR    , CREATED_ON,  IP_ADD )
	   VALUES
	  ( v_pk_linkedform,v_newform , v_formdisptype , 'S' ,
	    v_entrychar , v_fk_account, v_newstudy,
	    'N' , p_user, SYSDATE ,p_ip ) ;
END IF;


END LOOP;

   --insert all NIH Grant IDS of study
  --AK:Added for enhancement CTRP-20527-1
   FOR i IN (SELECT PK_STUDY_NIH_GRANT
		       FROM ER_STUDY_NIH_GRANT
		       WHERE fk_study=p_origstudy) LOOP
          BEGIN
		  v_old_pk := i.pk_study_nih_grant;
            -- The new NIH Grant id generated from the SEQ_ER_STUDY_NIH_GRANT sequence.
            -- This is stored in the v_newpknihgrantId variable to be used for all the INSERT/SELECT.
            SELECT SEQ_ER_STUDY_NIH_GRANT.NEXTVAL
            INTO v_new_pk
            FROM dual;
            DBMS_OUTPUT.PUT_LINE ('new NIH GRant Id' || TO_CHAR(v_new_pk) ) ;


         BEGIN
           INSERT INTO  ER_STUDY_NIH_GRANT (PK_STUDY_NIH_GRANT,FK_STUDY,FK_CODELST_FUNDMECH,
            FK_CODELST_INSTCODE,FK_CODELST_PROGRAM_CODE,NIH_GRANT_SERIAL,CREATOR, CREATED_ON, IP_ADD)
          SELECT v_new_pk ,v_newstudy,FK_CODELST_FUNDMECH,
            FK_CODELST_INSTCODE,FK_CODELST_PROGRAM_CODE,NIH_GRANT_SERIAL, p_user, SYSDATE, p_ip
     FROM ER_STUDY_NIH_GRANT
  WHERE PK_STUDY_NIH_GRANT= v_old_pk;
  
  END;
  END;
	END LOOP;
	
    --Ankit:Added for enhancement CTRP-20527-1, copy IND/IDE.
   FOR i IN (SELECT PK_STUDY_INDIIDE
		       FROM ER_STUDY_INDIDE
		       WHERE fk_study=p_origstudy) LOOP
          BEGIN
		  v_old_pk := i.PK_STUDY_INDIIDE;
		  
            SELECT SEQ_ER_STUDY_INDIDE.NEXTVAL
            INTO v_new_pk
            FROM dual;

          BEGIN
           INSERT INTO  ER_STUDY_INDIDE (PK_STUDY_INDIIDE, FK_STUDY, INDIDE_TYPE, INDIDE_NUMBER, 
	   FK_CODELST_INDIDE_GRANTOR, FK_CODELST_INDIDE_HOLDER,FK_CODELST_PROGRAM_CODE, INDIDE_EXPAND_ACCESS, 
	   FK_CODELST_ACCESS_TYPE, INDIDE_EXEMPT,IP_ADD, CREATOR, CREATED_ON)
	SELECT v_new_pk ,v_newstudy,INDIDE_TYPE, INDIDE_NUMBER, FK_CODELST_INDIDE_GRANTOR, 
		FK_CODELST_INDIDE_HOLDER,FK_CODELST_PROGRAM_CODE, INDIDE_EXPAND_ACCESS, FK_CODELST_ACCESS_TYPE, 
		INDIDE_EXEMPT,P_IP, P_USER, SYSDATE FROM ER_STUDY_INDIDE
	WHERE PK_STUDY_INDIIDE= v_old_pk;
  
  END;
  END;
	END LOOP;
   
-- for event CRFs, if the linked forms are 'specifc study', then check if the formID belongs to the new study, otherwise delete the linking
DELETE FROM esch.sch_event_crf c
WHERE EXISTS (SELECT * FROM event_assoc e WHERE event_type = 'A' AND e.event_id = c.fk_event AND e.chain_id IN (SELECT p.event_id FROM event_Assoc p WHERE chain_id = v_newstudy  AND p.event_type = 'P' ))
AND form_type = 'SP' AND NOT EXISTS ( SELECT * FROM ER_LINKEDFORMS l WHERE l.fk_study = v_newstudy AND l.fk_formlib = c.fk_form);

--//////////////
   COMMIT;
   o_success := v_newstudy; --returning study id
   END;
--////////////////////


	PROCEDURE SP_GET_PATIENT_RIGHT ( p_user IN NUMBER, p_group IN NUMBER, p_patient IN NUMBER, o_right OUT NUMBER)
	AS
	/*Get Patient Data Right
	* Modified by Sonika on March 23,04 to get the study rights irrespective of the patient study status, issue#1367
	*/

	BEGIN

		SELECT f_get_patientright(p_user , p_group , p_patient )
		INTO o_right
		FROM dual;

 	END;


	PROCEDURE SP_GET_STUDY_RIGHT ( p_user IN NUMBER, p_group IN NUMBER, p_study IN NUMBER, o_right OUT NUMBER)
	AS
	/*Get Patient Data Right**/
	  v_grpright_seq NUMBER;
	  v_grpright NUMBER;
	  v_studyright_seq NUMBER;
	  v_studyright NUMBER;
	  v_teamcount NUMBER;
	  v_supusr_flag NUMBER;
	  v_superuser_studRights VARCHAR2(100);

	BEGIN
		 -- get group right sequence for  'View complete patient details' for the user
		 SELECT ctrl_seq
		 INTO v_grpright_seq
		 FROM ER_CTRLTAB
		 WHERE ctrl_key = 'app_rights' AND ctrl_value = 'VIEWPATCOM';
 	      -- get group right for  'View complete patient details' for the user
		 SELECT SUBSTR(grp_rights,v_grpright_seq,1) , grp_supusr_flag ,grp_supusr_rights
		 INTO v_grpright, v_supusr_flag,v_superuser_studRights
		 FROM ER_GRPS,ER_USER
		 WHERE pk_grp = p_group
		 AND pk_grp=fk_grp_default
		 AND ER_USER.pk_user=p_user;
    	      -- get study right sequence for  'View complete patient details'
		 SELECT ctrl_seq
		 INTO v_studyright_seq
		 FROM ER_CTRLTAB
		 WHERE ctrl_key = 'study_rights' AND  ctrl_value='STUDYVPDET' ;

		 -- find if patient is currently enrolled in a protocol
		 SELECT COUNT(*)
		 INTO v_teamcount
		 FROM dual
		 WHERE ( EXISTS (SELECT * FROM ER_STUDYTEAM b WHERE b.fk_study = p_study AND  fk_user = p_user AND study_team_usr_type = 'D'  )
                  OR pkg_superuser.F_Is_Superuser(p_user, p_study) = 1 ) ;


		  IF  v_teamcount > 0 THEN
			 -- get max. of 'View complete patient details'' for all studies in which patient is enrolled.

			 	 SELECT MIN (NVL(TO_NUMBER(SUBSTR(rights,v_studyright_seq,1 )),0 ))
			  	 INTO v_studyright
			  	 FROM (
			  	 SELECT (NVL((SELECT study_team_rights FROM ER_STUDYTEAM x WHERE x.fk_user = p_user AND  x.fk_study = p_study
			  	  AND study_team_usr_type = 'D'), v_superuser_studRights ) ) RIGHTS
				   FROM dual
				 WHERE
	             ( EXISTS (SELECT * FROM ER_STUDYTEAM a WHERE a.fk_study = p_study AND  fk_user = p_user AND study_team_usr_type = 'D'  )
                  OR pkg_superuser.F_Is_Superuser(p_user, p_study) = 1 ) )   ;

		 ELSE
		 	 v_studyright := 4;
		    END IF;
		 IF v_studyright >= 4 AND v_grpright >= 4 THEN
		 	o_right := 4;
		 ELSE
		 	o_right := 0;
		 END IF;
	END;
	----------------------------------------***************-------------

	FUNCTION F_GET_USERRIGHTS_FOR_STUDY ( p_user IN NUMBER, p_study IN NUMBER)
    return VARCHAR2
    AS
    /*Get Any user Rights for a study (Super User/Study Team)**/
      v_studRights VARCHAR2(100);

    BEGIN
        select (nvl(
        (select study_team_rights from er_studyteam where fk_user = p_user and fk_study = p_study  and study_team_usr_type = 'D'),
        (select grp_supusr_rights from er_grps,er_user where pk_user = p_user and fk_grp_default = pk_grp))) STUDY_TEAM_RIGHTS
        into v_studRights
        from er_study
        where ( exists (select * from er_studyteam where fk_user = p_user and study_team_usr_type <> 'X' and fk_study = p_study)
        or  pkg_superuser.F_Is_Superuser(p_user , p_study) = 1 )
        and pk_study = p_study
        and study_actualdt is not null
        and er_study.STUDY_VERPARENT is null ;

        return v_studRights;
    END F_GET_USERRIGHTS_FOR_STUDY;
    ----------------------------------------***************-------------
	PROCEDURE SP_SET_ACTUAL_DATE(p_study IN NUMBER,p_status IN VARCHAR2)
	AS
	 v_activecount NUMBER;
    BEGIN
   /****************************************************************************************************
   ** Procedure to set actual date value to null if the status to be deleted is the only active status in the study
   ** Parameter Description
   ** p_study : Study Id
   ** p_status : status Subtype
   ** Author: Anu Khanna 13/April/04----
   **
   *****************************************************************************************************/
   ----get count of active status in a study----
   SELECT COUNT(*) INTO v_activecount
   	 FROM ER_STUDYSTAT,ER_CODELST
	 WHERE fk_study=p_study
	 AND codelst_type='studystat'
     AND fk_codelst_studystat = pk_codelst
	 AND codelst_subtyp='active';
-- if there is only one active status and the status to be deletd is also active then set actual date to null
	IF(v_activecount=1 AND p_status='active') THEN
		UPDATE ER_STUDY
		SET study_actualdt = NULL
		WHERE pk_study = p_study;
	END IF;
   COMMIT;
   END;
  /****************************************************************************************************
   ** Procedure to insert new verision in study versions
   ** Parameter Description
   ** p_study : Study Id
   ** p_version_number : version number
   ** p_version_notes : version notes
   ** p_user : user id
   ** p_vercreated 1 indicates version created successfully, -1 is error, -3 duplicate version number
   ** Author: Anu Khanna 26/Oct/04----
   **
   *****************************************************************************************************/
   PROCEDURE SP_NEWSTUDYVERSION (
   p_study IN NUMBER,
   p_version_number IN VARCHAR,
   p_version_notes IN VARCHAR2,p_version_date IN VARCHAR,
   p_version_cat IN VARCHAR,p_version_type IN VARCHAR,
   p_user IN NUMBER,
   p_vercreated OUT NUMBER)
   AS
   v_newstudyver NUMBER;
   v_cnt         NUMBER;
BEGIN
    --check if the same version number already exists of this study
	SELECT COUNT(*)
	INTO v_cnt
	FROM ER_STUDYVER
	WHERE fk_study=p_study
--JM: 051906 for blocking Case sensitive version number
--	AND trim(studyver_number)=trim(p_version_number);
	AND LOWER(trim(studyver_number))=LOWER(trim(p_version_number))
    AND LOWER(trim(studyver_category))=LOWER(trim(p_version_cat));
     IF v_cnt > 0 THEN
         p_vercreated := -3;
	    RETURN;
	END IF;
     p_vercreated:=1;
   -- The new study version id generated from the seq_er_studyver sequence.
    SELECT seq_er_studyver.NEXTVAL
     INTO v_newstudyver
     FROM dual;
     p_vercreated:=v_newstudyver;
     DBMS_OUTPUT.PUT_LINE ('new study  ver' || TO_CHAR(v_newstudyver) ) ;
    BEGIN
    --KM-13Mar09
    INSERT INTO ER_STUDYVER
            (PK_STUDYVER,FK_STUDY,STUDYVER_NUMBER,
            STUDYVER_STATUS,STUDYVER_NOTES,STUDYVER_DATE,STUDYVER_CATEGORY,STUDYVER_TYPE,
            CREATOR,
              CREATED_ON)
        VALUES(v_newstudyver,p_study,p_version_number,
	           'W',p_version_notes,TO_DATE(p_version_date,PKG_DATEUTIL.F_GET_DATEFORMAT),p_version_cat,p_version_type ,
			   p_user,
               SYSDATE);
	 -- insert record into er_status_history for version history
	    INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
					STATUS_MODPK ,	STATUS_MODTABLE,  FK_CODELST_STAT ,
					STATUS_DATE ,
					CREATOR ,
					RECORD_TYPE,
					CREATED_ON,STATUS_ENTERED_BY)
        VALUES ( seq_er_status_history.NEXTVAL,v_newstudyver,'er_studyver',Pkg_Util.f_getCodePk('W','versionStatus'),
		TRUNC(SYSDATE),p_user,'N',SYSDATE,p_user);
      IF SQL%FOUND THEN
          DBMS_OUTPUT.PUT_LINE ('Insert into study version ' || TO_CHAR(1)) ;
      ELSE
          DBMS_OUTPUT.PUT_LINE ('Error in sharing study version. ' ) ;
          RAISE_APPLICATION_ERROR (
            -20000,
            'Error in sharing study version. If error persists Pl. Check with Sys Admin'
         );
    		p_vercreated :=-1;
     END IF;
	END;
       	COMMIT;
END;
--------------------
  PROCEDURE SP_UPDATE_LOCAL_SAMPLE_SIZE ( p_studysites IN Array_String,  p_localsamples IN Array_String,  p_user IN NUMBER,
  										  			   	  				    p_ip_Add IN VARCHAR2,   p_ret OUT NUMBER)
 AS
 v_count NUMBER;
 i NUMBER;
 v_studysite_id NUMBER;
 v_local_sample NUMBER;
 BEGIN
   	  i := 1;
      v_count := p_studysites.COUNT();
	  WHILE( i <= v_count) LOOP
  v_studysite_id:= p_studysites(i);
  v_local_sample:= p_localsamples(i);
  BEGIN
  UPDATE ER_STUDYSITES
    SET
   STUDYSITE_LSAMPLESIZE = v_local_sample,
    LAST_MODIFIED_BY =p_user,
    IP_ADD = p_ip_Add
    WHERE
    PK_STUDYSITES =v_studysite_id;
  EXCEPTION  WHEN OTHERS THEN
         P('ERROR');
        p_ret:=-1;
     RETURN;
    END;
	i := i+1;
END LOOP;--end of while loop
COMMIT;
RETURN;
END SP_UPDATE_LOCAL_SAMPLE_SIZE;

	-----------------------------------------*********************----------
  FUNCTION f_getLatestStudyStatus(p_study NUMBER) RETURN NUMBER
  IS
  v_stat NUMBER;
  BEGIN
   BEGIN

	SELECT fk_codelst_studystat
	INTO 	v_stat
	FROM ER_STUDYSTAT WHERE pk_studystat = (
		SELECT MAX(pk_studystat)
		FROM ER_STUDYSTAT WHERE fk_study = p_study AND studystat_endt IS NULL);

	 	EXCEPTION WHEN NO_DATA_FOUND THEN

  		v_stat := 0;

     END;

	 RETURN v_stat ;
  END;

    FUNCTION f_getLatestStudyStatusPK(p_study NUMBER) RETURN NUMBER
  IS
  v_stat NUMBER;
  BEGIN
   BEGIN

		SELECT MAX(pk_studystat)
		INTO v_stat
		FROM ER_STUDYSTAT WHERE fk_study = p_study AND studystat_endt IS NULL;

	 	EXCEPTION WHEN NO_DATA_FOUND THEN

  		v_stat := 0;

     END;

	 RETURN v_stat ;
  END;


  	PROCEDURE sp_grant_studyteam_access(p_study IN NUMBER, p_user IN NUMBER,p_creator 	IN NUMBER,p_ipAdd IN VARCHAR2)
	AS
	v_pk_bud NUMBER(10);
  v_b_right VARCHAR2(10);

  v_new_fk_study NUMBER;
  v_new_fk_user NUMBER;
  v_new_creator NUMBER;
  v_new_ip_add VARCHAR2(15);
  v_formid NUMBER;
  v_object_number NUMBER;
  v_fk_object NUMBER;
 v_parent NUMBER;
	BEGIN
		 v_new_fk_study := p_study ;
		 v_new_fk_user := p_user ;
		 v_new_creator := p_creator;
		 v_new_ip_add := p_ipadd;


		FOR i IN (SELECT DISTINCT b.pk_budget,u.BGTUSERS_RIGHTS
		          FROM sch_budget b, sch_bgtusers u
		          WHERE b.fk_study IS NOT NULL AND
		          b.fk_study = v_new_fk_study AND
		          b.BUDGET_RIGHTSCOPE = 'S' AND
		          u.fk_budget = b.pk_budget AND u.BGTUSERS_TYPE = 'G' )
		LOOP
		    v_pk_bud :=   i.pk_budget;
		    v_b_right := i.BGTUSERS_RIGHTS;
		   INSERT INTO sch_bgtusers(PK_BGTUSERS,
		                                    FK_BUDGET ,
		                                    FK_USER  ,
		                                    BGTUSERS_RIGHTS ,
		                                    BGTUSERS_TYPE ,
		                                    CREATOR,
		                                    CREATED_ON ,
		                                    IP_ADD )
		   VALUES (SEQ_SCH_BGTUSERS.NEXTVAL,
		           v_pk_bud,
		           v_new_fk_user,
		           v_b_right,
		           'G',
		           v_new_creator,
		           SYSDATE,v_new_ip_add )  ;
		END LOOP;



			/*modified by Sonika on May 03, 04
			* to insert the user in Object Share table so that the newly added user to the Study Team
			* is able to access the module records available to this Study
			*/

			FOR i IN (SELECT pk_objectshare,fk_object, object_number
			          FROM ER_OBJECTSHARE
			          WHERE fk_objectshare_id = v_new_fk_study
			          AND objectshare_type = 'S')
			LOOP
			    v_fk_object := i.fk_object;
			    v_object_number := i.object_number ;
				v_parent := i.pk_objectshare;

			    INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT ,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE, CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,fk_objectshare)
					 VALUES (SEQ_ER_OBJECTSHARE.NEXTVAL,v_object_number, v_fk_object,v_new_fk_user,'U',v_new_creator ,'N',SYSDATE,v_new_ip_add,v_parent);
			END LOOP;



	END;


	   /* revokes the budget/object share access from the deleted/deactivated team user*/
	 PROCEDURE sp_revoke_studyteam_access(p_study IN NUMBER, p_user IN NUMBER)
	 AS
	 v_old_fk_study NUMBER;
 	 v_old_fk_user NUMBER;
	 BEGIN

		  v_old_fk_study :=  p_study ;
		   v_old_fk_user := p_user ;



						   DELETE
				FROM sch_bgtusers
				WHERE fk_budget IN
				( SELECT pk_budget
				  FROM sch_budget
				  WHERE fk_study IS NOT NULL AND
				  fk_study = v_old_fk_study AND
				  BUDGET_RIGHTSCOPE = 'S'
				)
				AND BGTUSERS_TYPE = 'G' AND
				fk_user = v_old_fk_user ;


				DELETE FROM ER_OBJECTSHARE
				  WHERE fk_object IN
				 (SELECT fk_object
				  FROM ER_OBJECTSHARE
				  WHERE objectshare_type = 'S' AND fk_objectshare_id = v_old_fk_study)
				  AND objectshare_type = 'U'
				  AND fk_objectshare_id = v_old_fk_user  AND
				fk_objectshare IN (SELECT pk_objectshare FROM ER_OBJECTSHARE i
								  WHERE i.objectshare_type = 'S' AND i.fk_objectshare_id = v_old_fk_study  AND i.fk_object = fk_object)  ;




	 END;

	 FUNCTION f_get_patientright(p_user NUMBER, p_group NUMBER, p_patient NUMBER ) RETURN NUMBER
	 AS
	  v_grpright_seq NUMBER;
	  v_grpright NUMBER;
	  v_studyright_seq NUMBER;
	  v_studyright NUMBER;
	  v_enrollcount NUMBER;
	  v_supusr_flag NUMBER;
	  v_studyright_str VARCHAR2(100);
	  v_superuser_studRights VARCHAR2(100);
	  o_right NUMBER;
	BEGIN
		 -- get group right sequence for  'View complete patient details' for the user
		 SELECT ctrl_seq
		 INTO v_grpright_seq
		 FROM ER_CTRLTAB
		 WHERE ctrl_key = 'app_rights' AND ctrl_value = 'VIEWPATCOM';
 	      -- get group right for  'View complete patient details' for the user
		 SELECT SUBSTR(grp_rights,v_grpright_seq,1) , grp_supusr_flag,grp_supusr_rights
		 INTO v_grpright, v_supusr_flag,v_superuser_studRights
		 FROM ER_GRPS,ER_USER
		 WHERE pk_grp = p_group
		 AND pk_grp=fk_grp_default
		 AND ER_USER.pk_user=p_user;
    	      -- get study right sequence for  'View complete patient details'
		 SELECT ctrl_seq
		 INTO v_studyright_seq
		 FROM ER_CTRLTAB
		 WHERE ctrl_key = 'study_rights' AND  ctrl_value='STUDYVPDET' ;
		 -- find if patient is currently enrolled in a protocol

		 SELECT COUNT(*)
		 INTO v_enrollcount
		 FROM erv_allpatstudystat a
		 WHERE fk_per = p_patient
		AND  ( EXISTS (SELECT * FROM ER_STUDYTEAM b WHERE b.fk_study = a.fk_study AND  fk_user = p_user AND study_team_usr_type = 'D'  )
                  OR pkg_superuser.F_Is_Superuser(p_user, a.fk_study) = 1 ) ;

		 IF  v_enrollcount > 0 THEN
			 -- get min. of 'View complete patient details'' for all studies in which patient is enrolled.

			  	 SELECT MIN (NVL(TO_NUMBER(SUBSTR(rights,v_studyright_seq,1 )),0 ))
			  	 INTO v_studyright
			  	 FROM (
			  	 SELECT (NVL((SELECT study_team_rights FROM ER_STUDYTEAM x WHERE x.fk_user = p_user AND  x.fk_study = c.fk_study
			  	  AND study_team_usr_type = 'D'), v_superuser_studRights ) ) RIGHTS
				   	 FROM ER_PATPROT C, ER_PATSTUDYSTAT B
				 WHERE C.fk_per = p_patient  AND
				  C.PATPROT_STAT = 1 AND
				  B.FK_PER = C.FK_PER AND
				  B.FK_STUDY = C.FK_STUDY  AND
	              B.PATSTUDYSTAT_ENDT IS NULL AND
	             ( EXISTS (SELECT * FROM ER_STUDYTEAM a WHERE a.fk_study = c.fk_study AND  fk_user = p_user AND study_team_usr_type = 'D'  )
                  OR pkg_superuser.F_Is_Superuser(p_user, c.fk_study) = 1 ) )   ;


		 ELSE
		 	 v_studyright := 4;
		    --Plog.DEBUG(pCTX,'hardcode');
		 END IF;
		 IF v_studyright >= 4 AND v_grpright >= 4 THEN
		 	o_right := 4;
		 ELSE
		 	o_right := 0;
		 END IF;
		    --Plog.DEBUG(pCTX,'hardcode');
		 RETURN o_right;

	END; -- end of function

    FUNCTION f_get_viewPHI(p_user NUMBER, p_group NUMBER) RETURN NUMBER
    AS
	  v_grpright_seq NUMBER;
	  v_grpright NUMBER;
	  v_studyright_seq NUMBER;
	  v_studyright NUMBER;
	  v_enrollcount NUMBER;
	  v_supusr_flag NUMBER;
	  v_studyright_str VARCHAR2(100);
	  v_superuser_studRights VARCHAR2(100);
      v_account number;
	  o_right NUMBER;
	BEGIN
		 -- get group right sequence for  'View complete patient details' for the user
		 SELECT ctrl_seq
		 INTO v_grpright_seq
		 FROM ER_CTRLTAB
		 WHERE ctrl_key = 'app_rights' AND ctrl_value = 'VIEWPATCOM';
 	      -- get group right for  'View complete patient details' for the user
		 SELECT SUBSTR(grp_rights,v_grpright_seq,1) , grp_supusr_flag,grp_supusr_rights,ER_USER.FK_ACCOUNT
		 INTO v_grpright, v_supusr_flag,v_superuser_studRights,v_account
		 FROM ER_GRPS,ER_USER
		 WHERE pk_grp = p_group
		 AND pk_grp=fk_grp_default
		 AND ER_USER.pk_user=p_user;
    	      -- get study right sequence for  'View complete patient details'
		 SELECT ctrl_seq
		 INTO v_studyright_seq
		 FROM ER_CTRLTAB
		 WHERE ctrl_key = 'study_rights' AND  ctrl_value='STUDYVPDET' ;
		 -- find if patient is currently enrolled in a protocol


			  	 SELECT MIN (NVL(TO_NUMBER(SUBSTR(rights,v_studyright_seq,1 )),0 ))
			  	 INTO v_studyright
			  	 FROM (
			  	 SELECT (NVL((SELECT study_team_rights FROM ER_STUDYTEAM x WHERE x.fk_user = p_user AND
                   x.fk_study = c.pk_study
			  	  AND study_team_usr_type = 'D'), v_superuser_studRights ) ) RIGHTS
				   	 FROM er_study c
				 WHERE  fk_account = v_account and
	             ( EXISTS (SELECT * FROM ER_STUDYTEAM a
                 WHERE a.fk_study = c.pk_study AND  fk_user = p_user AND study_team_usr_type = 'D'  )
                  OR pkg_superuser.F_Is_Superuser(p_user, c.pk_study) = 1 ) )   ;



		 IF v_studyright >= 4 AND v_grpright >= 4 THEN
		 	o_right := 4;
		 ELSE
		 	o_right := 0;
		 END IF;
		    --Plog.DEBUG(pCTX,'hardcode');
		 RETURN o_right;

	END; -- end of function


END Pkg_Studystat;
/
 
