-- Build 602
=========================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	ajaxengine.js
2	calMilestoneSetup.js
3	coverage.js
4	datagrid.js
5	dataview.js
6	date.js
7	dateformatSetting.js
8	manageVisitsGrid.js
9	milestonegrid.js
10	multiSpecimens.js
11	onload.js
12	onloadrefresh.js
13	patSearchUtil.js
14	prototype.js
15	specimen.js
16	subjectgrid.js
17	velosConfig.js
18	velosConfigCustom.js
19	velosUtil.js
20	yuiUtil.js
=========================================================================================================================
Garuda :
	After deploying the velos.ear file successfully.
	
	we can find two jar files ( inside the $JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib directory ) 
	named "hibernate-validator.jar" and "hibernate-validator-3.1.0.GA.jar" from which we need to remove one jar file based
	on the Operating System, where the application is deployed.
	
	1.In case of Windows we need to remove "hibernate-validator-3.1.0.GA.jar" and
	2.In case of Unix/Linux we need to remove "hibernate-validator.jar".
	 
DB Patch : DL02_ER_LAB_INSERT.sql - This SQL contains the CDR/PF specific ER_LABGROUPS entries. These are specific to NMDP.
=========================================================================================================================

=========================================================================================================================