Insert into ER_MAILCONFIG(
PK_MAILCONFIG,
MAILCONFIG_MAILCODE,
MAILCONFIG_SUBJECT,
MAILCONFIG_CONTENT,
MAILCONFIG_CONTENTTYPE,
MAILCONFIG_FROM,
MAILCONFIG_BCC,
MAILCONFIG_CC,
DELETEDFLAG,
FK_CODELSTORGID,
RID,
MAILCONFIG_HASATTACHEMENT,
CREATED_ON,
CREATOR,
LAST_MODIFIED_DATE,
LAST_MODIFIED_BY,
IP_ADD) 
values (SEQ_ER_MAILCONFIG.nextval,'TranCenter',
'Declaration Of Urgent medical Need',
'
To [USER]

From [CM]:

Dear Colleague,
Urgent Medical Need is required for CBU # [CBUID] for NMDP
Please Use This Link <a href="[URL]">CDR Clinical Record Link</a>  to sign off an Urgent Medical Need request.

Kind Regards,
[CM]
',
'text/html',
'velos_garuda@del.aithent.com',
'velos_garuda@del.aithent.com',
0,0,0,1,0,sysdate,null,sysdate,null,null
);

Commit;