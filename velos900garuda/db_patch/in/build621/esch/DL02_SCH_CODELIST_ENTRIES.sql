set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from SCH_CODELST where codelst_type = 'timepointtype' and codelst_subtyp = 'NTPD';
	if (v_item_exists = 0) then
		Insert into SCH_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ) 
		values (SEQ_SCH_CODELST.nextval,null,'timepointtype','NTPD','No Time Point Defined','N',1);
		dbms_output.put_line('Code-list item Type:timepointtype Subtype:NTPD inserted');
	else 
		dbms_output.put_line('Code-list item Type:timepointtype Subtype:NTPD already exists');
	end if;

	select count(*) into v_item_exists from SCH_CODELST where codelst_type = 'timepointtype' and codelst_subtyp = 'FTP';
	if (v_item_exists = 0) then
		Insert into SCH_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_SCH_CODELST.nextval,null,'timepointtype','FTP','Fixed Time Point','N',2);
		dbms_output.put_line('Code-list item Type:timepointtype Subtype:FTP inserted');
	else 
		dbms_output.put_line('Code-list item Type:timepointtype Subtype:FTP already exists');
	end if;
  
  select count(*) into v_item_exists from SCH_CODELST where codelst_type = 'timepointtype' and codelst_subtyp = 'DTP';
	if (v_item_exists = 0) then
		Insert into SCH_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_SCH_CODELST.nextval,null,'timepointtype','DTP','Dependent Time Point','N',3);
		dbms_output.put_line('Code-list item Type:timepointtype Subtype:DTP inserted');
	else 
		dbms_output.put_line('Code-list item Type:timepointtype Subtype:DTP already exists');
	end if;

COMMIT;

end;
/