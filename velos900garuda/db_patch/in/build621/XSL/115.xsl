<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output method="html" version="4.0" encoding="iso-8859-1"
		indent="yes" />
		<xsl:key name="RecordsByStudy" match="ROW" use="STUDY_NUM" />
		<xsl:key name="RecordsByStudyOrg" match="ROW" use="concat(STUDY_NUM,' ',SITE)" />
	<xsl:param name="hdrFileName" />
	<xsl:param name="ftrFileName" />
	<xsl:param name="repTitle" />
	<xsl:param name="repName" />
	<xsl:param name="repBy" />
	<xsl:param name="repDate" />
	<xsl:param name="argsStr" />
	<xsl:param name="wd" />
	<xsl:param name="xd" />
	<xsl:param name="hd" />
	<xsl:param name="cond" />
	<xsl:param name="hdrflag" />
	<xsl:param name="ftrflag" />
	<xsl:param name="tz" />
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$repName" />
				</TITLE>

				<link rel="stylesheet" href="./styles/common.css"
					type="text/css" />

			</HEAD>
			<BODY class="repBody">
				<xsl:if test="$cond='T'">
					<table width="100%">
						<tr class="reportGreyRow">
							<td class="reportPanel">
								VELMESSGE[M_Download_ReportIn]<!-- Download the report in -->:
								<A href='{$wd}'>VELLABEL[L_Word_Format]<!-- Word Format --></A>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<A href='{$xd}'>VELLABEL[L_Excel_Format]<!-- Excel Format --></A>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<xsl:text>&#xa0;</xsl:text>
								<A href='{$hd}'>
									VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
								</A>
							</td>
						</tr>
					</table>
					<TABLE WIDTH="100%">
						<xsl:if test="$hdrflag='1'">
							<TR>
								<TD WIDTH="100%" ALIGN="CENTER">
									<img src="{$hdrFileName}" />
								</TD>
							</TR>
						</xsl:if>
						<TR>
							<TD class="reportName" WIDTH="100%"
								ALIGN="CENTER">
								<xsl:value-of select="$repName" />
							</TD>
						</TR>
						</TABLE>
					
					<TABLE>
<TR>
<TD class="reportGrouping" WIDTH="100%" ALIGN="LEFT">
VELLABEL[L_Selected_Filter]<!-- Selected Filter -->: <xsl:value-of select="$argsStr" />
</TD>
</TR>
</TABLE>
				</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
				<xsl:for-each select="ROW[count(. | key('RecordsByStudyOrg', concat(STUDY_NUM,' ',SITE))[1])=1]">
				<xsl:variable name="str" select="key('RecordsByStudyOrg', concat(STUDY_NUM,' ',SITE))" />
				
					<br/><br/>		
				<TABLE WIDTH="80%">
					<TR>
						<TD class="reportGrouping" width="35%"
							ALIGN="left">
							VELLABEL[L_Study_Number]<!-- Study Number -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of select="STUDY_NUM" />
						</TD>
					</TR>
					<TR>
						<TD class="reportGrouping" ALIGN="left">
							VELLABEL[L_Study_Title]<!-- Study Title -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of select="STUDY_TITLE" />
						</TD>
					</TR>
										
					<TR>
						<TD class="reportGrouping" ALIGN="left">
							VELLABEL[L_Organization]<!-- Organization -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of select="SITE" />
						</TD>
					</TR>
					<TR>
						<TD class="reportGrouping" width="25%"	ALIGN="left">
							VELMESSGE[M_TotalPat_AllAes]<!-- Total Patients (All AEs) -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of select="ADVALL_PATCOUNT" />
						</TD>
					</TR>
					<TR>
						<TD class="reportGrouping" ALIGN="left">VELMESSGE[M_TotalPat_WithSaes]<!-- Total Patients with SAEs -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of	select="ADVS_PATCOUNT" />
						</TD>
					</TR>
					<TR>
						<TD class="reportGrouping" ALIGN="left">VELMESSGE[M_Tot_AdvEvt_AeSaes]<!-- Total Adverse Events (AEs + SAEs) -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of	select="ADVALL_COUNT" />
						</TD>
					</TR>
					<TR>
						<TD class="reportGrouping" ALIGN="left">VELLABEL[L_Toxicity_Scale]<!-- Toxicity Scale -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of	select="TOXICITY_SCALE" />
						</TD>
					</TR>
					<TR>
						<TD class="reportGrouping" ALIGN="left">VELMESSGE[M_TotNumbOf_PatEnrl]<!-- Total Number of Patients Enrolled -->:
						</TD>
						<TD class="reportGrouping">
							<xsl:value-of	select="TOTAL_ENR_PAT" />
						</TD>
					</TR>
					</TABLE>
					
					<TABLE>
					<TR><TD><B>VELLABEL[L_Treatment_Arms]<!-- Treatment Arms -->:</B></TD></TR>
					<xsl:for-each select="$str//TXS/STUDYTX">
					<TR>
					<TD class="reportGrouping">
						&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;<xsl:value-of select="."/>,&#xa0;VELLABEL[L_Description]<!-- Description -->: <xsl:value-of	select="./@txdesc" />
					</TD>
					</TR>
				</xsl:for-each>
					
				</TABLE>
				<hr class="thinLine" />
				<table width="100%">
					<tr>
						<td class="reportName" align="LEFT">
							<b>VELLABEL[L_Serious_AdverseEvents_Upper]<!-- SERIOUS ADVERSE EVENTS --></b>
						</td>
					</tr>
				</table>
				
				
				<table width="100%" border="1">
				<tr>
						<th class="reportHeading" width="20%">
							
						</th>
						<!--<th class="reportHeading" width="10%">
							Grade 0
						</th>-->
						<th class="reportHeading" width="10%" colspan="5">
							VELLABEL[L_AllArms_Grade]<!-- All Arms (Grade) -->
						</th>
					<xsl:for-each select="$str//TXS/STUDYTX">
					<TH class="reportHeading" WIDTH="10%" colspan="5">
						VELLABEL[L_Arm]<!-- Arm -->:&#xa0;<xsl:value-of select="."/>
						
					</TH>
				</xsl:for-each>
						
						</tr>
					<tr>
						<th class="reportHeading" width="20%">
							VELLABEL[L_Adverse_Event]<!-- Adverse Event -->
						</th>
						<!--<th class="reportHeading" width="10%">
							Grade 0
						</th>-->
						
						<th class="reportHeading" width="2%">
							1
						</th>
						<th class="reportHeading" width="2%">
							2
						</th>
						<th class="reportHeading" width="2%">
							3
						</th>
						<th class="reportHeading" width="2%">
							4
						</th>
						<th class="reportHeading" width="2%">
							5
						</th>
						<xsl:for-each select="$str//TXS/STUDYTX">
						<TH class="reportHeading" WIDTH="2%">
						1
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						2
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						3
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						4
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						5
						</TH>
				</xsl:for-each>
					</tr>
					<xsl:for-each select="$str//SAE">

						<xsl:variable name="class">
							<xsl:choose>
								<xsl:when
									test="number(position() mod 2)=0">
									reportEvenRow
								</xsl:when>
								<xsl:otherwise>
									reportOddRow
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<tr>
							<xsl:attribute name="class">
								<xsl:value-of select="$class" />
							</xsl:attribute>

							<td>
								<xsl:value-of select="./NAME/text()" />
							</td>
							<!--<td ALIGN="RIGHT">
								<xsl:if test="GRADE0!=0">
									<xsl:value-of
										select="./GRADE0/text()" />
								</xsl:if>
								&#xa0;
							</td>-->
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE1!=0">
									<xsl:value-of select="./GRADE1/text()" />
									<xsl:value-of select="./GRADE1_PER/text()" />
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE2!=0">
									<xsl:value-of
										select="./GRADE2/text()" />
										<xsl:value-of select="./GRADE2_PER/text()" />
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE3!=0">
									<xsl:value-of
										select="./GRADE3/text()" />
									<xsl:value-of select="./GRADE3_PER/text()" />
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE4!=0">
									<xsl:value-of
										select="./GRADE4/text()" />
										<xsl:value-of select="./GRADE4_PER/text()" />
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE5!=0">
									<xsl:value-of
										select="./GRADE5/text()" />
										<xsl:value-of select="./GRADE5_PER/text()" />
								</xsl:if>
								&#xa0;
							</td>
						<!-- Start here to print arm detail-->	
						<xsl:for-each select="./TXNAME">
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE1!=0">
									<xsl:value-of
										select="GRADE1/text()" />
									<xsl:value-of	select="GRADE1_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE2!=0">
									<xsl:value-of
										select="GRADE2/text()" />
								<xsl:value-of	select="GRADE2_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE3!=0">
									<xsl:value-of
										select="GRADE3/text()" />
								<xsl:value-of	select="GRADE3_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE4!=0">
									<xsl:value-of
										select="GRADE4/text()" />
										<xsl:value-of	select="GRADE4_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE5!=0">
									<xsl:value-of
										select="GRADE5/text()" />
										<xsl:value-of	select="GRADE5_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
									
						
					</xsl:for-each>
					<!--end here printing arm details-->
						</tr>
						<xsl:if test="position()=last()">
							<tr>
								<td class="reportGrouping">VELLABEL[L_Count_Upper]<!-- COUNT --></td>
								<!--<td class="reportGrouping"
									ALIGN="RIGHT">
									<xsl:value-of
										select="sum(//SAE/GRADE0[text()])" />
								</td>-->
								<td class="reportGrouping"
									ALIGN="RIGHT">
									<xsl:value-of
										select="sum($str//SAE/GRADE1[text()])" />
										
								</td>
								<td class="reportGrouping"
									ALIGN="RIGHT">
									<xsl:value-of
										select="sum($str//SAE/GRADE2[text()])" />
								</td>
								<td class="reportGrouping"
									ALIGN="RIGHT">
									<xsl:value-of
										select="sum($str//SAE/GRADE3[text()])" />
								</td>
								<td class="reportGrouping"
									ALIGN="RIGHT">
									<xsl:value-of
										select="sum($str//SAE/GRADE4[text()])" />
								</td>
								<td class="reportGrouping"
									ALIGN="RIGHT">
									<xsl:value-of
										select="sum($str//SAE/GRADE5[text()])" />
								</td>
								  <!--Do grouping for Arm grades-->
								 <xsl:for-each select="$str//TXS/STUDYTX">
								 <xsl:variable name="armName">  
								 <xsl:value-of select="."/>  
								 
								 </xsl:variable>
								 <td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//SAE/TXNAME[@name=$armName]/GRADE1[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//SAE/TXNAME[@name=$armName]/GRADE2[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//SAE/TXNAME[@name=$armName]/GRADE3[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//SAE/TXNAME[@name=$armName]/GRADE4[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//SAE/TXNAME[@name=$armName]/GRADE5[text()])" />
									</td>
									</xsl:for-each>
								 <!--End Ar Grouping-->
							</tr>
						</xsl:if>
					</xsl:for-each>
					
				</table>
				<table width="100%">
						<tr>
							<td class="reportName" align="LEFT">
								<BR />
								<b>VELLABEL[L_AllAdverse_Events_Upper]<!-- ALL ADVERSE EVENTS --></b>
							</td>
						</tr>
					</table>
					<table width="100%" border="1">
					<tr>
						<th class="reportHeading" width="20%">
							
						</th>
						<!--<th class="reportHeading" width="10%">
							Grade 0
						</th>-->
						<th class="reportHeading" width="10%" colspan="5">
							VELLABEL[L_AllArms_Grade]<!-- All Arms (Grade) -->
						</th>
					<xsl:for-each select="$str//TXS/STUDYTX">
					<TH class="reportHeading" WIDTH="10%" colspan="5">
						VELLABEL[L_Arm]<!-- Arm-->:&#xa0;<xsl:value-of select="."/>
						
						
					</TH>
				</xsl:for-each>
						
						</tr>
						<tr>
							<th class="reportHeading" width="20%">
								VELLABEL[L_Adverse_Event]<!-- Adverse Event-->
							</th>
							<!--<th class="reportHeading" width="10%">
								Grade 0
							</th>-->
							<th class="reportHeading" width="2%">
								1
							</th>
							<th class="reportHeading" width="2%">
								 2
							</th>
							<th class="reportHeading" width="2%">
								3
							</th>
							<th class="reportHeading" width="2%">
								 4
							</th>
							<th class="reportHeading" width="2%">
								5
							</th>
						<xsl:for-each select="$str//TXS/STUDYTX">
						<TH class="reportHeading" WIDTH="2%">
						1
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						2
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						3
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						4
						</TH>
						<TH class="reportHeading" WIDTH="2%">
						5
						</TH>
				</xsl:for-each>
						</tr>
						<xsl:for-each select="$str//AE">
						<xsl:variable name="aeName">
						<xsl:value-of select="@name"/>
						</xsl:variable>
						<xsl:variable name="class">
								<xsl:choose>
									<xsl:when
										test="number(position() mod 2)=0">
										reportEvenRow
									</xsl:when>
									<xsl:otherwise>
										reportOddRow
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>

							<tr>
								<xsl:attribute name="class">
									<xsl:value-of select="$class" />
								</xsl:attribute>
								<td>
									<xsl:value-of
										select="./NAME/text()" />
								</td>
								<!--<td ALIGN="RIGHT">
									<xsl:if test="GRADE0!=0">
										<xsl:value-of
											select="./GRADE0/text()" />
									</xsl:if>
									&#xa0;
								</td>-->								<td ALIGN="RIGHT">
									<xsl:if test="GRADE1!=0">
										<xsl:value-of
											select="./GRADE1/text()" />
										<xsl:value-of select="./GRADE1_PER/text()" />
									</xsl:if>
									&#xa0;
								</td>
								<td ALIGN="RIGHT">
									<xsl:if test="GRADE2!=0">
										<xsl:value-of
											select="./GRADE2/text()" />
											<xsl:value-of select="./GRADE2_PER/text()" />
									</xsl:if>
									&#xa0;
								</td>
								<td ALIGN="RIGHT">
									<xsl:if test="GRADE3!=0">
										<xsl:value-of
											select="./GRADE3/text()" />
										<xsl:value-of select="./GRADE3_PER/text()" />
									</xsl:if>
									&#xa0;
								</td>
								<td ALIGN="RIGHT">
									<xsl:if test="GRADE4!=0">
										<xsl:value-of
											select="./GRADE4/text()" />
											<xsl:value-of select="./GRADE4_PER/text()" />
									</xsl:if>
									&#xa0;
								</td>
								<td ALIGN="RIGHT">
									<xsl:if test="GRADE5!=0">
										<xsl:value-of
											select="./GRADE5/text()" />
											<xsl:value-of select="./GRADE5_PER/text()" />
									</xsl:if>
									&#xa0;
								</td>
								<!-- Start here to print arm detail-->	
						<xsl:for-each select="TXNAME">
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE1!=0">
									<xsl:value-of
										select="GRADE1/text()" />
										<xsl:value-of	select="GRADE1_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE2!=0">
									<xsl:value-of
										select="GRADE2/text()" />
										<xsl:value-of	select="GRADE2_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE3!=0">
									<xsl:value-of
										select="GRADE3/text()" />
										<xsl:value-of	select="GRADE3_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE4!=0">
									<xsl:value-of
										select="GRADE4/text()" />
										<xsl:value-of	select="GRADE4_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
							<td ALIGN="RIGHT">
								<xsl:if test="GRADE5!=0">
									<xsl:value-of
										select="GRADE5/text()" />
										<xsl:value-of	select="GRADE5_TPER/text()" />	
								</xsl:if>
								&#xa0;
							</td>
									
						
					</xsl:for-each>
					<!--end here printing arm details-->
							</tr>
							<xsl:if test="position()=last()">
								<tr>
									<td class="reportGrouping">
										VELLABEL[L_Count_Upper]<!-- COUNT -->
									</td>
									<!--<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum(//AE/GRADE0[text()])" />
									</td>-->
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/GRADE1[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/GRADE2[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/GRADE3[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/GRADE4[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/GRADE5[text()])" />
									</td>
								 <!--Do grouping for Arm grades-->
								 <xsl:for-each select="$str//TXS/STUDYTX">
								 <xsl:variable name="armName">  
								 <xsl:value-of select="."/>  
								 </xsl:variable>
								 <td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/TXNAME[@name=$armName]/GRADE1[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/TXNAME[@name=$armName]/GRADE2[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/TXNAME[@name=$armName]/GRADE3[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/TXNAME[@name=$armName]/GRADE4[text()])" />
									</td>
									<td class="reportGrouping"
										ALIGN="RIGHT">
										<xsl:value-of
											select="sum($str//AE/TXNAME[@name=$armName]/GRADE5[text()])" />
									</td>
									</xsl:for-each>
								 <!--End Ar Grouping-->
								</tr>
							</xsl:if>
						</xsl:for-each>
					</table>
				<table width="100%">
				<tr><td>&#xa0;</td></tr>
						<tr class="reportGreyRow">
						<td class="reportPanel" align = "center"> &#xa0; </td>
						</tr>
						</table>
				</xsl:for-each>
				<!--</xsl:for-each>-->
				
				
				<hr class="thickLine" />
			
				<TABLE WIDTH="100%">
					<TR>
					  <TD class="reportFooter" WIDTH="30%"	ALIGN="LEFT"> 
					     VELMESSGE[M_AdvEvtCnt_TreatDtStdDt]<!-- Adverse Events are counted from Patients&apos;&#xa0; &apos;Active/On Treatment&apos; date to 30 days after 'Off Study' date -->
					 </TD>
				</TR>
			</TABLE>
				<BR/>
				<TABLE WIDTH="100%">
					<TR>
						<TD class="reportFooter" WIDTH="30%"
							ALIGN="LEFT">
							VELLABEL[L_Report_By]<!-- Report By -->:
							<xsl:value-of select="$repBy" />
						</TD>
						<TD class="reportFooter" WIDTH="40%"
							ALIGN="CENTER">
							VELLABEL[L_Server_Timezone]<!-- Server Timezone -->:
							<xsl:value-of select="$tz" />
						</TD>
						<TD class="reportFooter" WIDTH="30%"
							ALIGN="RIGHT">
							VELLABEL[L_Date]<!-- Date -->:
							<xsl:value-of select="$repDate" />
						</TD>
					</TR>
				</TABLE>
				<xsl:if test="$ftrflag='1'">
					<TABLE>
						<TR>
							<TD WIDHT="100%" ALIGN="CENTER">
								<img src="{$ftrFileName}" />
							</TD>
						</TR>
					</TABLE>
				</xsl:if>
	</xsl:template>
</xsl:stylesheet>
