 ------------------------Alter er_add---------------------------------------------------------


alter table er_add add add_line_2 VARCHAR2(50 BYTE);

--------------------------------------------------------------------------------------------

------------------------Alter cbb-----------------------------------------------------

alter table CBB add ALLOW_MEMBER VARCHAR2(1 BYTE);

------------------------------------------------------------------------------------------

---------------------------------cb_cord-----------------------------------------------

alter table cb_cord modify CORD_REGISTRY_ID varchar2(50);
alter table cb_cord modify CORD_LOCAL_CBU_ID varchar2(50);
alter table cb_cord modify MATERNAL_LOCAL_ID varchar2(50);
alter table cb_cord modify REGISTRY_MATERNAL_ID varchar2(50);

----------------------------------------------------------------------------

----------------------------------CB_CORD------------------------------------------------

alter table CB_CORD add (registry_maternal_id varchar2(25), maternal_local_id varchar2(25),
fk_codelst_ethnicity varchar2(25), 
fk_codelst_race varchar2(25), funding_requested varchar2(25), funded_cbu varchar2(25)
,CORD_CDR_CBU_CREATED_BY varchar2(255),CORD_CDR_CBU_LAST_MOD_BY varchar2(255),
CORD_CDR_CBU_LAST_MOD_DT DATE,CORD_CREATION_DATE Date);


----------------------------------CB_CORD------------------------------------------------
COMMENT ON COLUMN "CB_CORD"."FK_CODELST_ETHNICITY" IS 'Code List reference for Ethnicity ( Alerts are stored in ER_CODELST)';
COMMENT ON COLUMN "CB_CORD"."FK_CODELST_RACE" IS 'Code List reference for Race (Values are stored in ER_CODELST)';
COMMENT ON COLUMN "CB_CORD"."CORD_CREATION_DATE" IS 'Cord creation date';
