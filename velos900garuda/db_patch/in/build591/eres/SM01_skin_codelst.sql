--Update custom cols of new skins.

Update er_codelst set CODELST_CUSTOM_COL = 'redmond' where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_redmond'
Update er_codelst set CODELST_CUSTOM_COL = 'bethematch' where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_bethematch';

COMMIT;

