set define off;

DECLARE

countFlage1 NUMBER(5);
BEGIN

SELECT COUNT(1) INTO countFlage1 FROM USER_TABLES WHERE TABLE_NAME='ER_OBJECT_SETTINGS';
if (countFlage1 > 0) then
   UPDATE ER_OBJECT_SETTINGS SET OBJECT_DISPLAYTEXT='Manage Visits'  where  object_name='protocol_tab' and OBJECT_DISPLAYTEXT = 'Schedule and Customize';
  commit;
end if;

END;
/