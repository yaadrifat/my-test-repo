--Build 596
======================================================================================================================
eResearch enhancement INF-18183:

We have renamed the �app_module� name as per the suggestions from Velos.This change is for all the files that are released in 596.
For already released files in Build 593 & 594 ,implementation will be done in future release.

Please refer to INF18183impactJsps.xls file for more details.


=======================================================================================================================
Garuda:


--Due to hibernate settings, constraints were created on deploy of application. These constraints are not required by the application. These constraints are created in following tables:
1. CB_CORD
Delete all constraints other then constraint type = 'Primary_Key'
2. CBB
Delete all constraints other then constraint type = 'Primary_Key'

=======================================================================================================================

Localization : (114 Files)

1	accessdenied.jsp
2	addbudgeturl.jsp
3	additemstudy.jsp
4	addmultiplespecimens.jsp
5	addmultiplestorage.jsp
6	addstoragekit.jsp
7	adminreporttabs.jsp
8	advStudysearchpg.jsp
9	ajaxFinancialDataFetch.jsp
10	ajaxStudyDataFetch.jsp
11	ajaxSubmit.jsp
12	budgetDoesNotExist.jsp
13	budgetfile.jsp
14	budgetusersdelete.jsp
15	changestudydates.jsp
16	crftabs.jsp
17	dashboardcentral.jsp
18	db_datamanager.jsp
19	db_financial.jsp
20	db_queryMgmt.jsp
21	deleteMultipleLineitems.jsp
22	duplicateformname.jsp
23	dynfilterbrowse.jsp
24	dynrepbrowse.jsp
25	dynreptabs.jsp
26	editPersonnelCost.jsp
27	editRepeatLineItems.jsp
28	editstoragekit.jsp
29	eventmilestone.jsp
30	eventtabs.jsp
31	exportData.jsp
32	formaccesserror.jsp
33	formdatarecords.jsp
34	formfilledaccountbrowser.jsp
35	getmultilookup.jsp
36	getsnapshot.jsp
37	homebottompanel.jsp
38	homepanel.jsp
39	invbrowsercommon.jsp
40	inventorytabs.jsp
41	irbForms.jsp
42	irbnewcheck.jsp
43	irbnewinit.jsp
44	irbnewtabs.jsp
45	irbreviewarea.jsp
46	irbsubmissions.jsp
47	jqueryUtils.jsp
48	labbrowser.jsp
49	managePreparationCart.jsp
50	mdynexport.jsp
51	mdynfilter.jsp
52	mdynorder.jsp
53	mdynpreview.jsp
54	mdynrep.jsp
55	mdynselectflds.jsp
56	meetingBrowser.jsp
57	meetingTopic.jsp
58	milestonehome.jsp
59	myhomepanel.jsp
60	newCombinedBudget.jsp
61	newOrganization.jsp
62	newStudyIds.jsp
63	notify.jsp
64	objectsharewith.jsp
65	ongoingBrowser.jsp
66	panel.jsp
67	patformdetails.jsp
68	patientbudget.jsp
69	patientdetails.jsp
70	patientdetailsquick.jsp
71	patientFacility.jsp
72	patientschedule.jsp
73	patientsearch.jsp
74	patstatmilestone.jsp
75	patstudystatus.jsp
76	perapndx.jsp
77	preparationAreaBrowser.jsp
78	prepareSpecimen.jsp
79	selectDateRange.jsp
80	specimenapndx.jsp
81	specimenForms.jsp
82	specimenlabbrowser.jsp
83	storageadminbrowser.jsp
84	storagekitbrowser.jsp
85	storageunitdetails.jsp
86	study.jsp
87	studyadmincal.jsp
88	studybrowserpg.jsp
89	studybudget.jsp
90	studycentricpatenroll.jsp
91	studyDictSettings.jsp
92	studyMilestones.jsp
93	studynotification.jsp
94	studypatients.jsp
95	studyprotocols.jsp
96	studystatusbrowser.jsp
97	studystatusdelete.jsp
98	studytrtarm.jsp
99	studyver.jsp
100	studyVerBrowser.jsp
101	teamBrowser.jsp
102	updateadditemstudy.jsp
103	updatebudget.jsp
104	updatemdynrep.jsp
105	updateMeetingTopic.jsp
106	updatemultschedules.jsp
107	updatestudyid.jsp
108	updatestudynotification.jsp
109	updatestudystatus.jsp
110	updatestudyver.jsp
111	updateTeamStatus.jsp
112	valueselect.jsp
113	viewInvoice.jsp
114	visitmilestone.jsp
=======================================================================================================================

