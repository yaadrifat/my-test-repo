-- Build 604
=========================================================================================================================
eResearch :

While working on UI Skin Requirement we restructured the CSS files in eresearch application. So from Build 604 whoever needs to work on CSS files has to keep these changes into consideration. 

1.ie_800.css & ns_800.css (../jsp/styles/..):We have removed these two css files from Styles root folder as well as from individual skin folders(bethematch, redmond and default), since we are no more supporting the 800 X 600. 

2.ns_1024.css, ns_gt1024.css: These two css earlier existed in styles root folder as well as on individual skin folders. But the one exist on style root folder is not updated for a longtime. What we did here is that we have again removed these two css files from the individual skin folders and made a generic css file for all these skins and replaced them with the one in the styleroot folder.So we will have only one reference of these files in the style root folder. 
Here please note that , ie_1024.css files in the three skins are used for IE but the common ns_(gt)1024.css is used for FF.There is a different approach for different browsers were followed. So At present , for IE we have to check the individual ie_1024.css in the three skins, but for FF we need to go to the common CSS at the root level. Very soon we are going to make this consistent for IE and FF.

 
3.velos_style.css, velos_style_old.css, velos_style-old.css: All these three css files were exist in styles root folder as well as on individual skin folders also. While working on the individual skins we found that these files were not in use. Also we have not used them in any of these three skins. So we have decided to delete them also. 

=========================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	accountforms.jsp
2	addeventuser.jsp
3	adveventbrowser.jsp
4	adveventlookup.jsp
5	ajaxFinancialDataFetch.jsp
6	ajaxStudyDataFetch.jsp
7	budgetbrowserpg.jsp
8	copyFromFieldLibrary.jsp
9	dataRecvd.jsp
10	dynfilterbrowse.jsp
11	dynrepbrowse.jsp
12	editbgtprotocols.jsp
13	editPersonnelCost.jsp
14	editRepeatLineItems.jsp
15	enrolledpatient.jsp
16	enrollpatientsearch.jsp
17	eventlibrary.jsp
18	eventlibrarycstest.jsp
19	fieldBrowser.jsp
20	fieldLibraryBrowser.jsp
21	formdatarecords.jsp
22	formfldbrowser.jsp
23	formLibraryBrowser.jsp
24	getlookup.jsp
25	getmultilookup.jsp
26	groupbrowserpg.jsp
27	labbrowser.jsp
28	labelBundle.properties
29	LC.java
30	LC.jsp
31	manageportallogins.jsp
32	MC.java
33	MC.jsp
34	messageBundle.properties
35	milestonehome.jsp
36	multipleChoiceBox.jsp
37	multipleChoiceSection.jsp
38	multipleusersearchdetails.jsp
39	multipleusersearchdetailswithsave.jsp
40	patientsearch.jsp
41	portal.jsp
42	protocollist.jsp
43	revSetup.jsp
44	savedrepbrowser.jsp
45	sectionBrowserNew.jsp
46	selecteventus.jsp
47	selectremoveusers.jsp
48	specimenbrowser.jsp
49	storageadminbrowser.jsp
50	storagekitbrowser.jsp
51	studyadmincal.jsp
52	studybrowserpg.jsp
53	studyprotocols.jsp
54	studyschedule.jsp
55	studyVerBrowser.jsp
56	updateaccountuser.jsp
57	usersearchdetails.jsp
58	viemsgbrowse.jsp
59	viemsglist.jsp
60	viewexpstatus.jsp
61	viewimpstatus.jsp
========================================================================================================================
Garuda :
	
	After deploying the velos.ear file successfully.
	
	we can find two jar files ( inside the $JBOSS_HOME/server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib directory ) 
	named "hibernate-validator.jar" and "hibernate-validator-3.1.0.GA.jar" from which we need to remove one jar file based
	on the Operating System, where the application is deployed.
	
	1.In case of Windows we need to remove "hibernate-validator-3.1.0.GA.jar" and
	2.In case of Unix/Linux we need to remove "hibernate-validator.jar".

=========================================================================================================================

