set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'B01';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','B01','B01','N',1);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:B01 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:B01 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'B08';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','B08','B08','N',2);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:B08 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:B08 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'B09';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','B09','B09','N',3);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:B09 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:B09 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'C07';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','C07','C06  ','N',4);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:C07 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:C07 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'DP1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','DP1 ','DP1 ','N',5);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:DP1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:DP1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'DP2 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','DP2 ','DP2 ','N',6);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:DP2  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:DP2  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'DP3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','DP3','DP3','N',7);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:DP3 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:DP3 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'D43 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','D43 ','D43 ','N',8);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:D43  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:D43  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'D71 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','D71 ','D71 ','N',9);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:D71  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:D71  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'E12';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','E12','E11 ','N',10);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:E12 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:E12 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F05 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F05 ','F05 ','N',11);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F05  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F05  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F30 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F30 ','F30 ','N',12);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F30  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F30  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F31 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F31 ','F31 ','N',13);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F31  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F31  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F32 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F32 ','F32 ','N',14);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F32  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F32  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F33 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F33 ','F33 ','N',15);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F33  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F33  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F34 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F34 ','F34 ','N',16);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F34  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F34  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F37 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F37 ','F37 ','N',17);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F37  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F37  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'F38 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','F38 ','F38 ','N',18);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F38  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:F38  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'G07 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','G07 ','G07 ','N',19);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G07  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G07  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'G08 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','G08 ','G08 ','N',20);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G08  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G08  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'G11 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','G11 ','G11 ','N',21);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G11  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G11  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'G12 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','G12 ','G12 ','N',22);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G12  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G12  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'G13 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','G13 ','G13 ','N',23);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G13  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G13  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'G20 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','G20 ','G20 ','N',24);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G20  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G20  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'G94 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','G94 ','G94 ','N',25);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G94  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:G94  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'HD5';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','HD5','HD4 ','N',26);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:HD5 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:HD5 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'HR!';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','HR!','HR!','N',27);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:HR! inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:HR! already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H13 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H13 ','H13 ','N',28);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H13  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H13  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H23';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H23','H23','N',29);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H23 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H23 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H25 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H25 ','H25 ','N',30);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H25  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H25  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H28';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H28','H28','N',31);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H28 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H28 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H50 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H50 ','H50 ','N',32);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H50  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H50  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H57 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H57 ','H57 ','N',33);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H57  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H57  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H62 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H62 ','H62 ','N',34);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H62  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H62  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H64 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H64 ','H64 ','N',35);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H64  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H64  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H75';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H75','H75','N',36);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H75 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H75 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'H79 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','H79 ','H79 ','N',37);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H79  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:H79  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'I02';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','I02','I01','N',38);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:I02 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:I02 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'KD2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','KD2','KD1 ','N',39);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:KD2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:KD2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'KL1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','KL1 ','KL1 ','N',40);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:KL1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:KL1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'KL2 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','KL2 ','KL2 ','N',41);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:KL2  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:KL2  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K01 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K01 ','K01 ','N',42);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K01  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K01  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K02 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K02 ','K02 ','N',43);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K02  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K02  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K05 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K05 ','K05 ','N',44);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K05  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K05  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K06 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K06 ','K06 ','N',45);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K06  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K06  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K07 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K07 ','K07 ','N',46);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K07  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K07  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K08 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K08 ','K08 ','N',47);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K08  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K08  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K12 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K12 ','K12 ','N',48);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K12  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K12  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K14 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K14 ','K14 ','N',49);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K14  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K14  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K18 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K18 ','K18 ','N',50);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K18  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K18  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K21';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K21','K21','N',51);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K21 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K21 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K22 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K22 ','K22 ','N',52);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K22  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K22  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K23 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K23 ','K23 ','N',53);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K23  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K23  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K24 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K24 ','K24 ','N',54);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K24  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K24  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K25 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K25 ','K25 ','N',55);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K25  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K25  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K26 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K26 ','K26 ','N',56);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K26  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K26  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K30 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K30 ','K30 ','N',57);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K30  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K30  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'K99 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','K99 ','K99 ','N',58);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K99  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:K99  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'L30 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','L30 ','L30 ','N',59);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L30  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L30  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'L32 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','L32 ','L32 ','N',60);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L32  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L32  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'L40 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','L40 ','L40 ','N',61);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L40  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L40  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'L50 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','L50 ','L50 ','N',62);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L50  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L50  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'L60 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','L60 ','L60 ','N',63);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L60  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:L60  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'M02';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','M02','M01 ','N',64);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:M02 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:M02 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'N01';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','N01','N01','N',65);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N01 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N01 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'N02';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','N02','N02','N',66);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N02 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N02 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'N03';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','N03','N03','N',67);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N03 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N03 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'N43';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','N43','N43','N',68);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N43 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N43 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'N44';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','N44','N44','N',69);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N44 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:N44 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'PL2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','PL2','PL1 ','N',70);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:PL2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:PL2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'PN1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','PN1 ','PN1 ','N',71);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:PN1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:PN1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'PN2 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','PN2 ','PN2 ','N',72);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:PN2  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:PN2  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P01 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P01 ','P01 ','N',73);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P01  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P01  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P20 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P20 ','P20 ','N',74);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P20  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P20  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P30 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P30 ','P30 ','N',75);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P30  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P30  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P40 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P40 ','P40 ','N',76);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P40  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P40  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P41 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P41 ','P41 ','N',77);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P41  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P41  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P42 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P42 ','P42 ','N',78);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P42  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P42  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P50 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P50 ','P50 ','N',79);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P50  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P50  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P51 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P51 ','P51 ','N',80);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P51  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P51  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P60 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P60 ','P60 ','N',81);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P60  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P60  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'P76';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','P76','P76','N',82);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P76 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:P76 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RC1';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RC1','RC1','N',83);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC1 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC1 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RC2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RC2','RC2','N',84);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RC3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RC3','RC3','N',85);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC3 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC3 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RC4';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RC4','RC4','N',86);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC4 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RC4 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RL1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RL1 ','RL1 ','N',87);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RL2 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RL2 ','RL2 ','N',88);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL2  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL2  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RL5 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RL5 ','RL5 ','N',89);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL5  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL5  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RL9 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RL9 ','RL9 ','N',90);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL9  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RL9  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'RS2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','RS2','RS1 ','N',91);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RS2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:RS2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R00 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R00 ','R00 ','N',92);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R00  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R00  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R01 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R01 ','R01 ','N',93);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R01  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R01  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R03 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R03 ','R03 ','N',94);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R03  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R03  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R04';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R04','R04','N',95);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R04 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R04 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R06';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R06','R06','N',96);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R06 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R06 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R08';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R08','R08','N',97);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R08 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R08 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R13 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R13 ','R13 ','N',98);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R13  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R13  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R15 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R15 ','R15 ','N',99);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R15  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R15  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R17 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R17 ','R17 ','N',100);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R17  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R17  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R18 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R18 ','R18 ','N',101);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R18  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R18  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R21 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R21 ','R21 ','N',102);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R21  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R21  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R24 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R24 ','R24 ','N',103);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R24  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R24  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R25 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R25 ','R25 ','N',104);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R25  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R25  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R30 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R30 ','R30 ','N',105);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R30  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R30  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R33 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R33 ','R33 ','N',106);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R33  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R33  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R34 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R34 ','R34 ','N',107);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R34  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R34  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R36 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R36 ','R36 ','N',108);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R36  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R36  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R37 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R37 ','R37 ','N',109);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R37  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R37  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R41 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R41 ','R41 ','N',110);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R41  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R41  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R42 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R42 ','R42 ','N',111);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R42  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R42  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R43 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R43 ','R43 ','N',112);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R43  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R43  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R44 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R44 ','R44 ','N',113);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R44  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R44  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R49 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R49 ','R49 ','N',114);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R49  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R49  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R55 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R55 ','R55 ','N',115);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R55  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R55  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R56 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R56 ','R56 ','N',116);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R56  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R56  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'R90 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','R90 ','R90 ','N',117);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R90  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:R90  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'SC1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','SC1 ','SC1 ','N',118);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:SC1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:SC1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'SC2 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','SC2 ','SC2 ','N',119);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:SC2  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:SC2  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'SC3 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','SC3 ','SC3 ','N',120);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:SC3  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:SC3  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'S06 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','S06 ','S06 ','N',121);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S06  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S06  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'S10 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','S10 ','S10 ','N',122);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S10  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S10  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'S11 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','S11 ','S11 ','N',123);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S11  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S11  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'S21 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','S21 ','S21 ','N',124);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S21  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S21  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'S22 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','S22 ','S22 ','N',125);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S22  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:S22  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'TL2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','TL2','TL1 ','N',126);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:TL2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:TL2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'TU3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','TU3','TU2 ','N',127);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:TU3 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:TU3 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T01 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T01 ','T01 ','N',128);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T01  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T01  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T02 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T02 ','T02 ','N',129);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T02  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T02  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T03';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T03','T03','N',130);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T03 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T03 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T06';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T06','T06','N',131);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T06 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T06 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T09 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T09 ','T09 ','N',132);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T09  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T09  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T14 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T14 ','T14 ','N',133);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T14  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T14  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T15 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T15 ','T15 ','N',134);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T15  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T15  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T32 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T32 ','T32 ','N',135);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T32  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T32  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T34 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T34 ','T34 ','N',136);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T34  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T34  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T35 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T35 ','T35 ','N',137);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T35  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T35  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T36 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T36 ','T36 ','N',138);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T36  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T36  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T37 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T37 ','T37 ','N',139);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T37  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T37  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T42 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T42 ','T42 ','N',140);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T42  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T42  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'T90 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','T90 ','T90 ','N',141);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T90  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:T90  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UA2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UA2','UA1 ','N',142);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UA2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UA2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UC1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UC1 ','UC1 ','N',143);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UC2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UC2','UC2','N',144);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UC3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UC3','UC3','N',145);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC3 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC3 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UC6 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UC6 ','UC6 ','N',146);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC6  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC6  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UC7 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UC7 ','UC7 ','N',147);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC7  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UC7  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UD2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UD2','UD1 ','N',148);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UD2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UD2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UE1';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UE1','UE1','N',149);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UE1 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UE1 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UE2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UE2','UE2','N',150);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UE2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UE2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UH1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UH1 ','UH1 ','N',151);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UH1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UH1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UH2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UH2','UH2','N',152);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UH2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UH2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UH3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UH3','UH3','N',153);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UH3 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UH3 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UL2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UL2','UL1 ','N',154);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UL2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UL2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UR1';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UR1','UR1','N',155);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR1 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR1 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UR3';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UR3','UR3','N',156);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR3 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR3 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UR6 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UR6 ','UR6 ','N',157);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR6  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR6  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UR8 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UR8 ','UR8 ','N',158);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR8  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UR8  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'US3 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','US3 ','US3 ','N',159);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:US3  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:US3  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'US4 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','US4 ','US4 ','N',160);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:US4  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:US4  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UT1 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UT1 ','UT1 ','N',161);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UT1  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UT1  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'UT2 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','UT2 ','UT2 ','N',162);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UT2  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:UT2  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U01 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U01 ','U01 ','N',163);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U01  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U01  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U09 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U09 ','U09 ','N',164);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U09  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U09  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U10 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U10 ','U10 ','N',165);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U10  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U10  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U11 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U11 ','U11 ','N',166);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U11  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U11  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U13 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U13 ','U13 ','N',167);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U13  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U13  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U14';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U14','U14','N',168);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U14 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U14 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U17 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U17 ','U17 ','N',169);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U17  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U17  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U18 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U18 ','U18 ','N',170);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U18  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U18  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U19 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U19 ','U19 ','N',171);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U19  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U19  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U1A ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U1A ','U1A ','N',172);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1A  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1A  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U1Q ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U1Q ','U1Q ','N',173);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1Q  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1Q  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U1S';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U1S','U1S','N',174);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1S inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1S already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U1T';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U1T','U1T','N',175);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1T inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1T already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U1V ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U1V ','U1V ','N',176);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1V  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U1V  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U21 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U21 ','U21 ','N',177);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U21  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U21  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U22 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U22 ','U22 ','N',178);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U22  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U22  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U23 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U23 ','U23 ','N',179);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U23  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U23  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U24';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U24','U24','N',180);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U24 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U24 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U27 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U27 ','U27 ','N',181);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U27  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U27  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U2G ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U2G ','U2G ','N',182);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U2G  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U2G  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U2R ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U2R ','U2R ','N',183);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U2R  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U2R  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U30 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U30 ','U30 ','N',184);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U30  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U30  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U32 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U32 ','U32 ','N',185);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U32  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U32  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U34';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U34','U34','N',186);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U34 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U34 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U36 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U36 ','U36 ','N',187);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U36  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U36  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U38 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U38 ','U38 ','N',188);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U38  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U38  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U41 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U41 ','U41 ','N',189);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U41  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U41  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U42 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U42 ','U42 ','N',190);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U42  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U42  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U43 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U43 ','U43 ','N',191);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U43  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U43  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U44 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U44 ','U44 ','N',192);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U44  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U44  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U45 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U45 ','U45 ','N',193);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U45  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U45  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U47 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U47 ','U47 ','N',194);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U47  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U47  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U48 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U48 ','U48 ','N',195);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U48  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U48  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U49 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U49 ','U49 ','N',196);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U49  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U49  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U50 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U50 ','U50 ','N',197);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U50  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U50  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U51 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U51 ','U51 ','N',198);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U51  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U51  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U52 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U52 ','U52 ','N',199);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U52  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U52  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U53 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U53 ','U53 ','N',200);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U53  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U53  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U54 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U54 ','U54 ','N',201);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U54  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U54  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U55 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U55 ','U55 ','N',202);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U55  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U55  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U56 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U56 ','U56 ','N',203);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U56  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U56  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U57 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U57 ','U57 ','N',204);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U57  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U57  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U58 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U58 ','U58 ','N',205);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U58  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U58  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U59 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U59 ','U59 ','N',206);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U59  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U59  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U60 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U60 ','U60 ','N',207);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U60  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U60  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U61 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U61 ','U61 ','N',208);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U61  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U61  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U62 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U62 ','U62 ','N',209);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U62  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U62  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U65 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U65 ','U65 ','N',210);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U65  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U65  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U66 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U66 ','U66 ','N',211);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U66  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U66  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U75 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U75 ','U75 ','N',212);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U75  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U75  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U79 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U79 ','U79 ','N',213);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U79  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U79  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U81 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U81 ','U81 ','N',214);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U81  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U81  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U82 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U82 ','U82 ','N',215);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U82  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U82  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U83 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U83 ','U83 ','N',216);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U83  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U83  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U84 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U84 ','U84 ','N',217);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U84  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U84  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U87';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U87','U87','N',218);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U87 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U87 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U88';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U88','U88','N',219);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U88 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U88 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'U90 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','U90 ','U90 ','N',220);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U90  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:U90  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'VF2';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','VF2','VF1 ','N',221);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:VF2 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:VF2 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'X01 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','X01 ','X01 ','N',222);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X01  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X01  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'X02 ';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','X02 ','X02 ','N',223);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X02  inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X02  already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'X06';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','X06','X06','N',224);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X06 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X06 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'X98';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','X98','X98','N',225);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X98 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:X98 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'Y01';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','Y01','Y01','N',226);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Y01 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Y01 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'Y02';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','Y02','Y02','N',227);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Y02 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Y02 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'Z01';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','Z01','Z01','N',228);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Z01 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Z01 already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'NIHFundMech' and codelst_subtyp = 'Z02';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ)
		values (SEQ_ER_CODELST.nextval,null,'NIHFundMech','Z02','Z02','N',229);
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Z02 inserted');
	else
		dbms_output.put_line('Code-list item Type:NIHFundMech Subtype:Z02 already exists');
	end if;

	COMMIT;

end;
/
