Set Define Off;

Declare

 V_Column_Exists Number := 0;
 
BEGIN
 SELECT COUNT(*) INTO V_COLUMN_EXISTS FROM USER_TAB_COLS WHERE TABLE_NAME ='CB_MAIL_DOCUMENTS' AND COLUMN_NAME = 'RID'; 
  IF (V_COLUMN_EXISTS = 0) THEN
      Execute Immediate ('ALTER TABLE CB_MAIL_DOCUMENTS ADD RID NUMBER(10,0)');
      COMMIT;
  END IF;
END;
/
COMMENT ON COLUMN CB_MAIL_DOCUMENTS.RID IS 'This column is used for the audit trail. Uniquely identifies the row in the database'; 
COMMIT;