create or replace
PROCEDURE SP_WORKFLOW_TEMP IS
OUTSTR VARCHAR2(100 BYTE);
var_order_id varchar2(10 CHAR);
var_order_type varchar2(10 CHAR);
BEGIN
for ORDERS in (SELECT ORDER_ID,ORDER_TYPE FROM ER_ORDER_TEMP)
loop
  var_order_id :=orders.order_id;
  var_order_type :=orders.order_type;
  sp_resolve_orders(var_order_id,var_order_type);
 sp_workflow(var_order_id,var_order_type,1,outstr);
 sp_alerts(var_order_id);
 END LOOP;
 delete from er_order_temp;
END SP_WORKFLOW_TEMP;
/