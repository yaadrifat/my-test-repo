

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='HE' AND 
	CODE_TYPE='Response';
  if (v_record_exists = 1) then
      UPDATE CB_CBU_STATUS SET CBU_STATUS_DESC='HE Requested' where CBU_STATUS_CODE='HE' AND CODE_TYPE='Response';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CT' AND 
	CODE_TYPE='Response';
  if (v_record_exists = 1) then
      UPDATE CB_CBU_STATUS SET CBU_STATUS_DESC='CT Requested' where CBU_STATUS_CODE='HE' AND CODE_TYPE='Response';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='OR' AND 
	CODE_TYPE='Response';
  if (v_record_exists = 1) then
      UPDATE CB_CBU_STATUS SET CBU_STATUS_DESC='OR Requested' where CBU_STATUS_CODE='OR' AND CODE_TYPE='Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'OT';

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET CBU_STATUS_DESC = 'Cord Unavailable - Other Reasons' WHERE CBU_STATUS_CODE = 'OT';
      commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='RCT' AND 
	CODE_TYPE='Response';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,IS_LOCAL_REG) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'RCT','Repeat CT','Response','1');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='RHE' AND 
	CODE_TYPE='Response';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,IS_LOCAL_REG) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'RHE','Repeat HE','Response','1');
	commit;
  end if;
end;
/
--END--

commit;