alter table cb_cord modify CORD_ELIGIBLE_ADDITIONAL_INFO varchar2(300);


--STARTS ADDING COLUMN PREV_CT_DONE_FLAG TO ER_ORDER TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'PREV_CT_DONE_FLAG';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ORDER ADD(PREV_CT_DONE_FLAG VARCHAR2(2 BYTE))';
END IF;
END;
/
--END
COMMENT ON COLUMN ER_ORDER.PREV_CT_DONE_FLAG IS 'Stores ''Y'' if CT has done previously else null or ''N''';


--STARTS ADDING COLUMN SWITCH_REASON TO ER_ORDER TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'SWITCH_REASON';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ORDER ADD(SWITCH_REASON NUMBER(10,0))';
END IF;
END;
/
--END

COMMENT ON COLUMN ER_ORDER.SWITCH_REASON IS 'Stores reference to er_codelst table where codelst_type=switchwf_rsn';


--STARTS ADDING COLUMN SWITCH_REASON_OTHER TO ER_ORDER TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'SWITCH_REASON_OTHER';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ORDER ADD(SWITCH_REASON_OTHER VARCHAR2(250 CHAR))';
END IF;
END;
/
--END


COMMENT ON COLUMN ER_ORDER.SWITCH_REASON_OTHER IS 'Stores description of reason if other';


--STARTS ADDING COLUMN SWITCH_DATE TO ER_ORDER TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'SWITCH_DATE';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ORDER ADD(SWITCH_DATE DATE)';
END IF;
END;
/
--END

COMMENT ON COLUMN ER_ORDER.SWITCH_DATE IS 'Stores when order is switched';


--STARTS ADDING COLUMN SWITCH_BY TO ER_ORDER TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'SWITCH_BY';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ORDER ADD(SWITCH_BY NUMBER(10,0))';
END IF;
END;
/
--END


COMMENT ON COLUMN ER_ORDER.SWITCH_BY IS 'Stores refernce of er_user table who logged in';




--STARTS MODIFYING COLUMN DOCUMENT_TYPE FROM ER_ATTACHMENTS TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ATTACHMENTS' AND COLUMN_NAME = 'DOCUMENT_TYPE';
IF (v_column_exists = 1) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ATTACHMENTS MODIFY(DOCUMENT_TYPE VARCHAR2(255 BYTE))';
END IF;
END;
/
--END



