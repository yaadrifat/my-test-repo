create or replace function f_get_createlastmod(pk_cord number,categry varchar2) return SYS_REFCURSOR IS
history_data SYS_REFCURSOR;
BEGIN
    OPEN history_data FOR    select 
                                  TO_CHAR(CREATED_ON,'Mon DD, YYYY') CREATED_ON,
                                  F_GETUSER(CREATOR) CREATOR,
                                  F_GETUSER(LAST_MODIFIED_BY) MODIFIED_BY,
                                  TO_CHAR(LAST_MODIFIED_DATE,'MON DD, YYYY') MODIFIED_ON,
                                  max(pk_form_responses) PK_MAX
                            from 
                                  cb_form_responses 
                            where 
                                  fk_form_version=(select max(pk_form_version) from cb_form_version where entity_id=pk_cord and fk_form=(select pk_form from cb_forms where forms_desc=categry)) group by TO_CHAR(CREATED_ON,'Mon DD, YYYY'), F_GETUSER(CREATOR), F_GETUSER(LAST_MODIFIED_BY), TO_CHAR(LAST_MODIFIED_DATE,'MON DD, YYYY');

RETURN HISTORY_DATA;
END;
/