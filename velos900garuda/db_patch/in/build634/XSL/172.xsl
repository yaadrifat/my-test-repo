<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--> <font size ="4"> National Marrow Donor Program® </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--><font size ="4"> Infectious Disease Markers Report for HPC, Cord Blood </font>
</b>
</td>
</tr>
</table>
<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
<xsl:value-of select="ROW/SITENAME"/>
</td>
<td align="right"> CBU Registry Id: 
	<xsl:value-of select="ROW/REGID" />
</td>
</tr>

<tr>
<td align="left"> Cord Blood Bank ID:
	<xsl:value-of select="ROW/SITEID" />
</td>
<td align="right"> CBU Identification Number on Bag: 


<xsl:for-each select="//ROW/IDSONBAG/IDSONBAG_ROW">
<xsl:if test="IDONBAGVAL='CBU Registry ID'">
	<xsl:value-of select="../../REGID" />
</xsl:if>
<xsl:if test="IDONBAGVAL='CBU Local ID'">
	<xsl:value-of select="../../LOCALCBUID" />
</xsl:if>
<xsl:if test="IDONBAGVAL='ISBT DIN'">
	<xsl:value-of select="../../ISTBTDIN" />
</xsl:if>
<xsl:if test="(position()=1)">

 <xsl:for-each select="../../ADDIDS/ADDIDS_ROW">
		<xsl:value-of select="ADDIDVAL" />&#160;
 </xsl:for-each>

</xsl:if>

</xsl:for-each>


</td>
</tr>
</table>



<hr class="thickLine" width="100%"/>

<p align="left"><xsl:value-of select="ROW/SOURCEVAL" /> IDM Results:</p> 
<br></br> 

<xsl:apply-templates select="ROW"/>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 


<xsl:template match="ROW">

<table border="1" bordercolor="black">
<xsl:if test="position()!=1">
<tr>
<TD align="left" colspan="2">
<xsl:value-of select="TESTDATEVAL" /> 
</TD>

<TD align="left" colspan="4">
<xsl:value-of select="LABTESTNAME" /> 
</TD>

<xsl:if test="position()!=18">
<TD align="left" colspan="2">
<xsl:value-of select="FK_TEST_OUTCOME" /> 
</TD>
</xsl:if>

<xsl:if test="position()=18">
<TD align="left" colspan="2">
<xsl:value-of select="LASTVAL" /> 
</TD>
</xsl:if>


<!--
<TD align="left" >
<xsl:value-of select="ASSESSMENT_REMARKS" /> 
</TD>

<TD align="left" >
<xsl:value-of select="ASSESSMENT_POSTEDON" /> 
</TD>


<TD align="left" >
<xsl:value-of select="ASSESSMENT_CONSULTON" /> 
</TD>

<TD align="left" >
<xsl:value-of select="ASSESSMENT_POSTEDBY" /> 
</TD>
-->
</tr>
</xsl:if>

</table>


</xsl:template> 


</xsl:stylesheet>