LOAD DATA INFILE * INTO TABLE er_repxsl
APPEND
FIELDS TERMINATED BY ','
(PK_REPXSL,
FK_REPORT,
FK_ACCOUNT,
REPXSL_NAME,
xsl_file filler char,
"REPXSL" LOBFILE (xsl_file) TERMINATED BY EOF NULLIF XSL_FILE = 'NONE',
xsl_file2 filler char,
"REPXSL_XSL" LOBFILE (xsl_file2) TERMINATED BY EOF NULLIF XSL_FILE2 = 'NONE')
BEGINDATA
1,1,0,Active Protocols,1.xsl,1.xsl
4,4,0,All Account Users,4.xsl,4.xsl
6,6,0,Public Protocols,6.xsl,6.xsl
7,7,0,All Protocols,7.xsl,7.xsl
43,43,0,User Profile and Access,43.xsl,43.xsl
106,106,0,Protocol Calendar Preview,106.xsl,106.xsl
159,159,0,Combined Patient Budget,159.xsl,159.xsl
167,167,0,CBU Detail Report,167.xsl,167.xsl
168,168,0,CBU Comprehensive Report,168.xsl,168.xsl
172,172,0,Infectious Disease Markers Report,172.xsl,172.xsl
178,178,0,MRQ Report,178.xsl,178.xsl
179,179,0,FMHQ Report,179.xsl,179.xsl