<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><font size ="4"> National Marrow Donor Program® </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><font size ="4"> Maternal Risk Questionnaire Report (<xsl:value-of select="ROW/LICSTATUS"/>) for HPC, Cord Blood </font>
</b>
</td>
</tr>
</table>

<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
	<xsl:value-of select="ROW/SITENAME"/>
</td>
<td align="right"> Cord Blood Bank ID:
	<xsl:value-of select="ROW/SITEID" />
</td>
</tr>

<tr>
<td align="left"> CBU Registry Id: 
	<xsl:value-of select="ROW/REGID" />
</td>
<td align="right"> CBU Identification Number on Bag: 

<xsl:for-each select="//ROW/IDSONBAG/IDSONBAG_ROW">
<xsl:if test="IDONBAGVAL='CBU Registry ID'">
	<xsl:value-of select="../../REGID" />
</xsl:if>
<xsl:if test="IDONBAGVAL='CBU Local ID'">
	<xsl:value-of select="../../LOCALCBUID" /> 
</xsl:if>
<xsl:if test="IDONBAGVAL='ISBT DIN'">
	<xsl:value-of select="../../ISTBTDIN" /> 
</xsl:if>
<xsl:if test="(position()=1)">

 <xsl:for-each select="../../ADDIDS/ADDIDS_ROW">
		<xsl:value-of select="ADDIDVAL" />&#160;
 </xsl:for-each>

</xsl:if>

</xsl:for-each>


</td>
</tr>
</table>


<hr class="thickLine" width="100%"/>

<br></br>
<br></br>
<br></br>
<xsl:variable name="VAR_DEPEN_VALUE" /> 
<xsl:variable name="VAR_DEPEN_QUES_PK" /> 
<xsl:variable name="VAR_PRE_RESPONSE" /> 
<xsl:variable name="FKDEPEN_VAL" /> 
<xsl:variable name="VAR_TEMP" />
<xsl:variable name="VAR_SPLITED" />
<xsl:variable name="VALUE1" />
<xsl:variable name="VALUE2" />
<xsl:variable name="VALUE3" />

<xsl:for-each select="//ROW/GRP/GRP_ROW">
	
	<table border="0" ><tr><td colspan="4"><xsl:value-of select="QUESTION_GRP_NAME" />&#160; </td></tr></table>


		<xsl:variable name="PKGRPVAL" select="PK_QUESTION_GRP" /> 
		<xsl:for-each select="//ROW/MRQ/MRQ_ROW">
			<xsl:if test="$PKGRPVAL=FK_QUES_GRP">

					<xsl:variable name="FKDEPEN_PKVAL" select="DEPENT_QUES_PK" /> 
					<xsl:variable name="VAR_DEPEN_QUES_PK" select="DEPENT_QUES_PK"/> 
					<xsl:variable name="VAR_DEPEN_VALUE" select="DEPENT_QUES_VALUE"/> 
					
						<xsl:variable name="VAR_TEMP_MULTI">
						  <xsl:choose>
							    <xsl:when test="DEPENT_QUES_PK!='0' and contains(DEPENT_QUES_VALUE, ',') and preceding-sibling::*[1]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[1]/RESPONSE_CODE" />
							    </xsl:when>
							    <xsl:when test="DEPENT_QUES_PK!='0' and contains(DEPENT_QUES_VALUE, ',') and preceding-sibling::*[2]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[2]/RESPONSE_CODE" />
							    </xsl:when>
							     <xsl:when test="DEPENT_QUES_PK!='0' and contains(DEPENT_QUES_VALUE, ',') and preceding-sibling::*[3]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[3]/RESPONSE_CODE" />
							    </xsl:when>
						 </xsl:choose>
						</xsl:variable>

						


					<xsl:if test="$FKDEPEN_PKVAL!='0' and contains($VAR_DEPEN_VALUE, ',')">
						 
						<xsl:variable name="VALUE1" select="substring-before($VAR_DEPEN_VALUE,',')"/>
						
						<xsl:if test="$VALUE1=$VAR_TEMP_MULTI">
							<table border="1" bordercolor="black">
							<tr>
							<td><xsl:value-of select="QUES_SEQ" /></td>
							<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
							<td>
							<xsl:if test="RESPONSE_TYPE='dropdown'">
								<xsl:value-of select="DROPDOWN_VALUE" />
							</xsl:if>
							<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
								<xsl:value-of select="RESPONSE_VAL" />
							</xsl:if>
							</td>
							<td><xsl:value-of select="ASSES_RESPONSE" /></td>
							<td><xsl:value-of select="ASSES_NOTES" /></td>
							</tr>
							</table>
						</xsl:if>

						<xsl:variable name="TEMPSTR2" select="substring-after($VAR_DEPEN_VALUE,',')"/>

						<xsl:if test="contains($TEMPSTR2, ',')">
							<xsl:variable name="VALUE2" select="substring-before($TEMPSTR2,',')"/>
							<xsl:if test="$VALUE2=$VAR_TEMP_MULTI">
								<table border="1" bordercolor="black">
								<tr>
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>
								</td>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
								</tr>
								</table>
							</xsl:if>
						</xsl:if>

						<xsl:variable name="TEMPSTR3" select="substring-after($TEMPSTR2,',')"/>
						
						<xsl:if test="not(contains($TEMPSTR3, ','))">
							<xsl:variable name="VALUE3" select="$TEMPSTR3"/>
							<xsl:if test="$VALUE3=$VAR_TEMP_MULTI">
								<table border="1" bordercolor="black">
								<tr>
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>
								</td>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
								</tr>
								</table>
							</xsl:if>
						</xsl:if>						
						
					</xsl:if>




					<xsl:variable name="VAR_TEMP">
						  <xsl:choose>
							    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[1]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[1]/RESPONSE_CODE" />
							    </xsl:when>
							    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[2]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[2]/RESPONSE_CODE" />
							    </xsl:when>
							     <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[3]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[3]/RESPONSE_CODE" />
							    </xsl:when>
						 </xsl:choose>
					</xsl:variable>




							
					<xsl:if test="$FKDEPEN_PKVAL!='0' and not(contains($VAR_DEPEN_VALUE, ','))">
						
						<xsl:if test="$VAR_DEPEN_VALUE=$VAR_TEMP">
							<table border="1" bordercolor="black">
							<tr>
							<td><xsl:value-of select="QUES_SEQ" /></td>
							<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
							<td>
							<xsl:if test="RESPONSE_TYPE='dropdown'">
								<xsl:value-of select="DROPDOWN_VALUE" />
							</xsl:if>
							<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
								<xsl:value-of select="RESPONSE_VAL" />
							</xsl:if>
							</td>
							<td><xsl:value-of select="ASSES_RESPONSE" /></td>
							<td><xsl:value-of select="ASSES_NOTES" /></td>
							</tr>
							</table>
						</xsl:if>
					</xsl:if>
					
					<xsl:if test="$FKDEPEN_PKVAL='0'">
					<table border="1" bordercolor="black">
					<tr>
					
			
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<td><xsl:value-of select="DROPDOWN_VALUE" /></td>
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<td><xsl:value-of select="RESPONSE_VAL" /></td>
								</xsl:if>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
				
		
					</tr>
					</table>
					</xsl:if>

					
					
					
					
					
					
				
			</xsl:if>
		</xsl:for-each>
		

	<br></br>
	<br></br>	
		
		
	

	
</xsl:for-each>

</xsl:template> 


</xsl:stylesheet>