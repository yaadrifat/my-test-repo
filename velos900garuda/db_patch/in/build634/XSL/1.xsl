<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByResType" match="ROW" use="RESEARCH_TYPE" />
<xsl:key name="RecordsByStudyType" match="ROW" use="STUDY_TYPE" />
<xsl:key name="RecordsByPhase" match="ROW" use="PHASE" />
<xsl:key name="RecordsByTA" match="ROW" use="TA" />


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>


<link rel="stylesheet" href="./styles/common.css" type="text/css"/> 


</HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">

<table width="100%" >
<tr class="reportGreyRow">
<td class="reportPanel"> 
VELMESSGE[M_Download_ReportIn]<!-- Download the report in -->: 
<A href='{$wd}' >
<img border="0" title="VELLABEL[L_Word_Format]" alt="VELLABEL[L_Word_Format]" src="./images/word.GIF" ></img><!-- Word Format -->
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
<img border="0" title="VELLABEL[L_Excel_Format]" alt="VELLABEL[L_Excel_Format]" src="./images/excel.GIF" ></img><!-- Excel Format -->
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
</A> 
</td>
</tr>
</table>
</xsl:if>
<table class="reportborder">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</td>
</tr>
<tr><td class="reportFooter" align="center">
VELMESSGE[M_RptStd_OpenedToAccrual]<!-- This report includes studies that have opened to accrual and are not yet permanently closed or retired -->
</td></tr>
<tr><td>&#xa0;</td></tr> 
<tr>
<td class="reportData" WIDTH="30%" ALIGN="CENTER">
VELLABEL[L_Selected_Filter_S]<!-- Selected Filter(s) -->: <xsl:value-of select="$argsStr" />
</td></tr>
</table>


<table border ="0" width ="100%">
	   <tr class="reportGraphRow">
	   	   
		   
				<td class="reportData" WIDTH="20%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>VELLABEL[L_ByStd_Type]<!-- By Study Type --> </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByStudyType', STUDY_TYPE)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByStudyType', STUDY_TYPE)" />	
			   		  <tr>
			   		  <xsl:variable name="v_studytype">
			   		  <xsl:value-of select="STUDY_TYPE" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_studytype!=''">
			   		  <td class="reportGraphRow" ALIGN="RIGHT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <xsl:variable name="studytype_count" select="format-number(((count($str[STUDY_TYPE=$v_studytype]) div count(//ROW))*100),'###.00')"/>
				   			  <td width = "70%" class="reportGraphRow" ALIGN="LEFT">
				   		  <xsl:value-of select="$v_studytype" />&#xa0;:
				   		  </td>
						  <td class="reportGraphRow" ALIGN="CENTER" width="20%">
				   		  <xsl:value-of select="count($str[STUDY_TYPE=$v_studytype])" />
				   		  </td>
						</tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
				
				
				<td class="reportData" WIDTH="20%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>VELLABEL[L_ByResearch_Type]<!-- By Research Type --> </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByResType', RESEARCH_TYPE)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByResType', RESEARCH_TYPE)" />	
			   		  <tr>
			   		  <xsl:variable name="v_restype">
			   		  <xsl:value-of select="RESEARCH_TYPE" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_restype != ''">
			   		  <td width = "60%" class="reportGraphRow" ALIGN="RIGHT">
				   		  <xsl:value-of select="$v_restype" />&#xa0; :
				   		  </td>
						  <td class="reportGraphRow" ALIGN="CENTER" width="5%">
				   		  <xsl:value-of select="count($str[RESEARCH_TYPE=$v_restype])" />
				   		  </td>
					  <td class="reportGraphRow" ALIGN="LEFT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <td class="reportGraphRow" ALIGN="LEFT" width="30%">
				   		  <xsl:variable name="restype_count" select="format-number(((count($str[RESEARCH_TYPE=$v_restype]) div count(//ROW))*100),'###.00')"/>
				   				 <table width="{$restype_count}" border="0">
								 <tr class="reportGraphBar">
								 <td >
								 <xsl:text>&#xa0; </xsl:text>
								 </td> 
								 </tr> 
								 </table> 
						</td>
									  
						</tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
				
				
			<td class="reportData" width="30%" valign="top">
	
				<table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
				<tr><td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>VELLABEL[L_ByThrp_Area]<!-- By Therapeutic Area --></b>
				</td></tr>
				<tr>
				<td>&#xa0;</td></tr>
				<xsl:for-each select="ROW[count(. | key('RecordsByTA', TA)[1])=1]">
				<xsl:variable name="str" select="key('RecordsByTA', TA)" />	
				<tr>
				<xsl:variable name="v_ta">
				<xsl:value-of select="TA" /> 
				</xsl:variable>
				<xsl:if test="$v_ta != ''">
				<td class="reportGraphRow" ALIGN="LEFT">
					<table width="100%" border="0">
					<tr>
					<td width = "60%" class="reportGraphRow" ALIGN="RIGHT">
					<xsl:value-of select="$v_ta" />&#xa0;: 
					</td>
					<td class="reportGraphRow" ALIGN="CENTER" width="5%">
					<xsl:value-of select="count($str[TA=$v_ta])" />
					</td>
					<td class="reportGraphRow" ALIGN="LEFT" width="30%">
					<xsl:variable name="ta_count" select="format-number(((count($str[TA=$v_ta]) div count(//ROW))*100),'###.00')"/>
								  <table width="{$ta_count}" border="0">
								  <tr class="reportGraphBar">
								  <td >
								  <xsl:text>&#xa0; </xsl:text>
								  </td> 
								  </tr> 
								  </table> 
					</td>
					</tr></table>
				</td>
			</xsl:if>
			</tr>
		</xsl:for-each>

		</table>

</td>
					
					<td class="reportData" WIDTH="15%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>VELLABEL[L_By_Phase]<!-- By Phase --> </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByPhase', PHASE)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByPhase', PHASE)" />	
			   		  <tr>
			   		  <xsl:variable name="v_phase">
			   		  <xsl:value-of select="PHASE" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_phase != ''">
			   		  <td class="reportGraphRow" ALIGN="RIGHT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <xsl:variable name="phase_count" select="format-number(((count($str[PHASE=$v_phase]) div count(//ROW))*100),'###.00')"/>
				   			  <td width = "70%" class="reportGraphRow" ALIGN="LEFT">
				   		  <xsl:value-of select="$v_phase" />&#xa0;:
				   		  </td>
						  <td class="reportGraphRow" ALIGN="LEFT" width="20%">
				   		  <xsl:value-of select="count($str[PHASE=$v_phase])" />
				   		  </td>
						</tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
				
					

</tr>
<tr><td>&#xa0;</td></tr> 
<tr>
	<td class="reportLabel" ALIGN="CENTER" colspan="4"><b>VELLABEL[L_Total_MatchingRows]<!-- Total Matching Rows -->: <xsl:value-of select="count(//ROW)" /></b></td>
</tr>

</table>
<!-- <hr class="thickLine"/> -->

<TABLE WIDTH="100%" BORDER="1">
<TR>
<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER">
VELLABEL[L_Study_Number]<!-- Study Number -->
</TH>

<TH class="reportHeading" WIDTH="7%" ALIGN="CENTER">
VELLABEL[L_Start_Date]<!-- Start Date -->
</TH>

<TH class="reportHeading" WIDTH="20%" ALIGN="CENTER">
VELLABEL[L_Title]<!-- Title -->
</TH>
<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER">
VELLABEL[L_Phase]<!-- Phase -->
</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Research_Type]<!-- Research Type -->
</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Study_Type]<!-- Study Type -->
</TH>
<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER">
VELLABEL[L_Division]<!-- Division -->
</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Therapeutic_Area]<!-- Therapeutic Area --> 
</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Organization_S]<!-- Organization(s) --> 
</TH>
<TH class="reportHeading" WIDTH="9%" ALIGN="CENTER">
VELLABEL[L_Principal_Investigator]<!-- Principal Investigator --> 
</TH>
</TR>
<xsl:apply-templates select="ROW"/>
</TABLE>

<!-- <hr class="thinLine" /> -->
<TABLE WIDTH="100%">
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">

VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />

</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">

VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />

</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >
<TR class="reportEvenRow" VALIGN="TOP">

<TD class="reportData">
<xsl:value-of select="STUDY_NUMBER" />

</TD>

<TD class="reportData">
<xsl:value-of select="STUDYACTUAL_DATE" />
</TD>

<TD class="reportData">
<xsl:value-of select="STUDY_TITLE" />
</TD>

<TD class="reportData">
<xsl:value-of select="PHASE" />
</TD>

<TD class="reportData">
<xsl:value-of select="RESEARCH_TYPE" />
</TD>

<TD class="reportData">
<xsl:value-of select="STUDY_TYPE" />
</TD>

<TD class="reportData">
<xsl:value-of select="DIVISION" />
</TD>

<TD class="reportData">
<xsl:value-of select="TA" />
</TD>

<TD class="reportData">
<xsl:value-of select="ORGANIZATION" />
</TD>

<TD class="reportData">
<xsl:value-of select="PI" />
</TD>

</TR>
</xsl:when> 
<xsl:otherwise>
<TR class="reportOddRow" VALIGN="TOP">

<TD class="reportData">
<xsl:value-of select="STUDY_NUMBER" />
</TD>

<TD class="reportData">
<xsl:value-of select="STUDYACTUAL_DATE" />
</TD>

<TD class="reportData">
<xsl:value-of select="STUDY_TITLE" />
</TD>
<TD class="reportData">
<xsl:value-of select="PHASE" />
</TD>

<TD class="reportData">
<xsl:value-of select="RESEARCH_TYPE" />
</TD>

<TD class="reportData">
<xsl:value-of select="STUDY_TYPE" />
</TD>
<TD class="reportData">
<xsl:value-of select="DIVISION" />
</TD>
<TD class="reportData">
<xsl:value-of select="TA" />
</TD>

<TD class="reportData">
<xsl:value-of select="ORGANIZATION" />
</TD>

<TD class="reportData">
<xsl:value-of select="PI" />
</TD>

</TR>



</xsl:otherwise>
</xsl:choose> 
</xsl:template> 
</xsl:stylesheet>
