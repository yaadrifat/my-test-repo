set define off;
--Sql for Table CB_CORD to add column Container.'; 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FK_CONTAINER'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FK_CONTAINER number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FK_CONTAINER IS 'Code Id for Container.'; 
commit;

--Sql for Table CB_CORD to add column ContainerMaxVolume
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FK_CONT_MAX_VOL'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FK_CONT_MAX_VOL number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FK_CONT_MAX_VOL IS 'Code Id for ContainerMaxVolume.'; 
commit;


--Sql for Table CB_CORD to add column fkOrderId
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FK_ORDER_ID'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FK_ORDER_ID number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FK_ORDER_ID IS 'Code Id for FK Order Id.'; 
commit;
