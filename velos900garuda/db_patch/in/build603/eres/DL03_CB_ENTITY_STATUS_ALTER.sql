set define off;
--STARTS ADD THE COLUMN LICN_ELIG_CHANGE_REASON FROM CB_ENTITY_STATUS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_STATUS'
    AND COLUMN_NAME = 'LICN_ELIG_CHANGE_REASON'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_ENTITY_STATUS add (LICN_ELIG_CHANGE_REASON  VARCHAR2(200 BYTE))';
  end if;
end;
/
--END--
--starts comment for the COLUMN LICN_ELIG_CHANGE_REASON FROM CB_ENTITY_STATUS TABLE--
comment on column CB_ENTITY_STATUS.LICN_ELIG_CHANGE_REASON  is 'Strong reason for changing the status of eligibility and licensure after review confirmation.';
--end--
commit;
