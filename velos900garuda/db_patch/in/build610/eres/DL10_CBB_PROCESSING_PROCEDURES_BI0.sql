create or replace
TRIGGER ERES.CBB_PROCESSING_PROCEDURES_BI0 BEFORE INSERT ON CBB_PROCESSING_PROCEDURES

REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW

DECLARE
  ERID NUMBER(10);
  USR VARCHAR(200); 
  RAID NUMBER(10);
  INSERT_DATA CLOB;
  
BEGIN
  SELECT TRUNC(SEQ_RID.NEXTVAL)  INTO ERID FROM DUAL;
  :NEW.RID :=ERID;
  USR := getuser(:NEW.LAST_MODIFIED_BY);
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;
  
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CBB_PROCESSING_PROCEDURES', ERID, 'I',USR);
  
  
  INSERT_DATA := :NEW.PK_PROC ||'|'|| to_char(:NEW.CREATOR) ||'|'|| to_char(:NEW.CREATED_ON) ||'|'|| to_char(:NEW.IP_ADD) ||'|'||
		to_char(:NEW.LAST_MODIFIED_BY) ||'|'|| to_char(:NEW.LAST_MODIFIED_DATE) ||'|'|| to_char(:NEW.PROC_NO) ||'|'|| to_char(:NEW.PROC_NAME) ||'|'|| 
                 to_char(:NEW.PROC_START_DATE) ||'|'|| to_char(:NEW.PROC_TERMI_DATE) ||'|'|| to_char(:NEW.PROC_VERSION) ||'|'||
                 to_char(:NEW.RID) ||'|'|| to_char(:NEW.FK_PROCESSING_ID) ||'|'|| to_char(:NEW.FK_SITE);
                 
                 
                 INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);                 
END;       
/                