set define off;
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols
    where TABLE_NAME = 'ER_LKPCOL'
    AND column_name = 'LKPCOL_DISPVAL';
  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_LKPCOL SET LKPCOL_DISPVAL = ''File Name/URL'' WHERE FK_LKPLIB=''6500'' and LKPCOL_NAME=''STUDYAPNDX_URI''';
  end if;
end;
/
commit;