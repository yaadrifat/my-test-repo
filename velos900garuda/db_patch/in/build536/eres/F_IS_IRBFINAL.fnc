set define off

CREATE OR REPLACE FUNCTION ERES."F_IS_IRBFINAL" (p_pkSubmission number)
/* returns 0 if submission is not final, greater than 0 if final*/
RETURN number
AS
v_count NUMBER;
BEGIN
 
	select count(1) into v_count
	from er_submission_status where 
	fk_submission = p_pkSubmission and fk_submission_board is null and 
	is_current = 1 and submission_status in (select pk_codelst from er_codelst where 
	 	   ','||codelst_custom_col1||',' like '%,final,%' ); 

	return v_count ;
		
 END;
/
