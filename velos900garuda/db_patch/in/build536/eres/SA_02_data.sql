set define off

update er_browserconf
set browserconf_settings = '{"key":"LETTER_LINK","label":"Outcome Letter","format":"letterLink", "sortable":false, "resizeable":true,"hideable":true}' where browserconf_colname = 'LETTER_LINK';

update er_browserconf
set browserconf_settings = '{"key":"SEND_LINK","label":"Send Back to Review Board","format":"sendLink", "sortable":false, "resizeable":true,"hideable":true}'
where browserconf_colname = 'SEND_LINK';

update er_ctrltab
set ctrl_desc = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pre-review'
where ctrl_key = 'app_rights' and ctrl_value = 'MIRB_SAS';


update er_ctrltab
set ctrl_desc = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Review Assignment'
where ctrl_key = 'app_rights' and ctrl_value = 'MIRB_SCS';

update er_ctrltab
set ctrl_desc = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Post Review Processing'
where ctrl_key = 'app_rights' and ctrl_value = 'MIRB_SPRS';


commit;