update er_report set rep_sql = 
'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(VISIT_INTERVAL,''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE,
EVENT_CPTCODE
,visit_win_before, visit_win_after
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,PK_PROTOCOL_VISIT' 
where pk_report = 106; 

update er_report set rep_sql_clob = 
'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(VISIT_INTERVAL,''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE,
EVENT_CPTCODE
,visit_win_before, visit_win_after
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,PK_PROTOCOL_VISIT' 
where pk_report = 106; 


commit;

update er_report set rep_sql =
'SELECT
PROT_ID,
PROT_NAME,
PROT_DESC,
PROT_DURATION,
nvl(sch_date_grp, ''No Interval'') as sch_date_grp,
nvl(sch_date_grp_disp, ''No Interval Defined'' ) as sch_date_grp_disp,
pkg_dateutil.f_get_first_dayofyear_str AS cal_start_date,
TO_CHAR(SCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS sch_date,
TO_CHAR((sch_date - FUZZY_DURATION_bef),PKG_DATEUTIL.F_GET_DATEFORMAT) || '' - '' || TO_CHAR((sch_date + FUZZY_DURATION_aft),PKG_DATEUTIL.F_GET_DATEFORMAT) AS fuzzy_dates,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
FORMS,
nvl(displacement,0) as displacement,
COVERAGE_TYPE, visit_win_before, visit_win_after
FROM erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''  and
nvl(HIDE_FLAG,0)=0
ORDER BY SCH_DATE,PK_PROTOCOL_VISIT'
where pk_report = 107;

update er_report set rep_sql_clob =
'SELECT
PROT_ID,
PROT_NAME,
PROT_DESC,
PROT_DURATION,
nvl(sch_date_grp, ''No Interval'') as sch_date_grp,
nvl(sch_date_grp_disp, ''No Interval Defined'' ) as sch_date_grp_disp,
pkg_dateutil.f_get_first_dayofyear_str AS cal_start_date,
TO_CHAR(SCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS sch_date,
TO_CHAR((sch_date - FUZZY_DURATION_bef),PKG_DATEUTIL.F_GET_DATEFORMAT) || '' - '' || TO_CHAR((sch_date + FUZZY_DURATION_aft),PKG_DATEUTIL.F_GET_DATEFORMAT) AS fuzzy_dates,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
FORMS,
nvl(displacement,0) as displacement,
COVERAGE_TYPE, visit_win_before, visit_win_after
FROM erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''  and
nvl(HIDE_FLAG,0)=0
ORDER BY SCH_DATE,PK_PROTOCOL_VISIT'
where pk_report = 107;
commit;