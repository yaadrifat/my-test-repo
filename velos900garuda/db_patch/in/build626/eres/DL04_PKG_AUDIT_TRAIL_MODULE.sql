CREATE OR REPLACE PACKAGE ERES."PKG_AUDIT_TRAIL_MODULE" AS 

PROCEDURE SP_AUDIT_ERROR_LOG (    
    p_ModuleName        IN  VARCHAR2,
    p_ErrorType         IN  VARCHAR2,
    p_ErrorSource       IN  VARCHAR2,
    p_ErrorMessage      IN  VARCHAR2,
    p_ErrorSql          IN  VARCHAR2,
    p_ErrorDescription  IN  VARCHAR2);

PROCEDURE SP_ROW_INSERT
    (p_rowid      IN NUMBER,
    p_tabname     IN VARCHAR2,
    p_rid         IN NUMBER,
    p_modulepk    IN NUMBER,
    p_act         IN VARCHAR2,
    p_userid      IN NUMBER);

PROCEDURE SP_COLUMN_INSERT
    (p_rowid      IN NUMBER,
    p_tablename   IN VARCHAR2,
    p_colname     IN VARCHAR2,
    p_oldval      IN VARCHAR2,
    p_newval      IN VARCHAR2,
    p_codelstid   IN NUMBER,
    p_remarks     IN VARCHAR2); 
    
FUNCTION F_GETMAPPEDVALUE(p_fromvalue IN VARCHAR2) RETURN VARCHAR2;

FUNCTION F_GETCHAINID(P_EVENTID NUMBER) RETURN NUMBER;

END PKG_AUDIT_TRAIL_MODULE;
/

CREATE OR REPLACE PACKAGE BODY ERES."PKG_AUDIT_TRAIL_MODULE" AS

---- procedure SP_AUDIT_ERROR_LOG
PROCEDURE SP_AUDIT_ERROR_LOG (    
    p_ModuleName        IN  VARCHAR2,
    p_ErrorType         IN  VARCHAR2,
    p_ErrorSource       IN  VARCHAR2,
    p_ErrorMessage      IN  VARCHAR2,
    p_ErrorSql          IN  VARCHAR2,
    p_ErrorDescription  IN  VARCHAR2
)
IS
BEGIN
INSERT INTO ERES.AUDIT_ERROR_LOG (PK_AUDIT_ERROR_LOG,MODULE_NAME,ERROR_TYPE, ERROR_SOURCE, ERROR_MESSAGE, ERROR_SQL,ERROR_DESCRIPTION) VALUES ( SEQ_AUDIT_ERROR_LOG.NEXTVAL, p_ModuleName,p_ErrorType,p_ErrorSource, p_ErrorMessage, p_ErrorSql, p_ErrorDescription);
COMMIT;
EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('Problem In SP_AUDIT_ERROR_LOG');
raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM); 
END SP_AUDIT_ERROR_LOG;

--To insert In AUDIT_ROW_MODULE 
PROCEDURE SP_ROW_INSERT
    (p_rowid     IN NUMBER,
    p_tabname   IN VARCHAR2,
    p_rid       IN NUMBER,
    p_modulepk  IN NUMBER,
    p_act       IN VARCHAR2,
    p_userid    IN NUMBER) AS
 
    v_table_display_value VARCHAR2(200);    
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    v_table_display_value := ERES.PKG_AUDIT_TRAIL_MODULE.F_GETMAPPEDVALUE(p_tabname); 
    INSERT INTO AUDIT_ROW_MODULE(PK_ROW_ID,TABLE_NAME,RID,MODULE_ID,ACTION,TIMESTAMP,USER_ID,TABLE_DISPLAY_NAME) VALUES (p_rowid, p_tabname, p_rid,p_modulepk,p_act,sysdate,p_userid,v_table_display_value);
    COMMIT;
    EXCEPTION 
    WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Procedure', 'Exception Raised:'||sqlerrm||'',NULL,'Error In Procedure SP_ROW_INSERT ');
END SP_ROW_INSERT;

--To insert In AUDIT_COLUMN_MODULE 
PROCEDURE SP_COLUMN_INSERT
    (p_rowid        IN NUMBER,
    p_tablename     IN VARCHAR2,
    p_colname       IN VARCHAR2,
    p_oldval        IN VARCHAR2,
    p_newval        IN VARCHAR2,
    p_codelstid     IN NUMBER,
    p_remarks       IN VARCHAR2) AS       
    v_column_display_value VARCHAR2(200);
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;    

BEGIN
    v_column_display_value :=ERES.PKG_AUDIT_TRAIL_MODULE.F_GETMAPPEDVALUE(p_tablename||'.'||p_colname); 
    INSERT INTO AUDIT_COLUMN_MODULE(PK_COLUMN_ID,FK_ROW_ID,COLUMN_NAME,OLD_VALUE,NEW_VALUE,FK_CODELST_REASON,REMARKS,COLUMN_DISPLAY_NAME) VALUES (SEQ_AUDIT_COLUMN_MODULE.nextval,p_rowid, p_colname, p_oldval, p_newval,p_codelstid,p_remarks,v_column_display_value);
    COMMIT;
    EXCEPTION 
    WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Procedure', 'Exception Raised:'||sqlerrm||'',NULL,'Error In Procedure SP_COLUMN_INSERT ');
END SP_COLUMN_INSERT;

FUNCTION F_GETCHAINID(P_EVENTID NUMBER)
    RETURN NUMBER
IS
   v_chainid NUMBER;
   v_ErrorType VARCHAR2(20):='EXCEPTION';
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
        SELECT chain_id INTO v_chainid FROM ERES.event_assoc WHERE event_id = p_eventid;
        RETURN v_chainid;
        EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN 0;        
        WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG('CAL',v_ErrorType, 'Function', 'Exception Raised:'||sqlerrm||'',NULL,'Error In FUNCTION F_GETCHAINID ');        
END;  
--To get Location Of Changes done in budget
FUNCTION F_GETMAPPEDVALUE( p_fromvalue IN varchar2 )
   RETURN varchar2 
IS
    v_tovalue  varchar2(200);
    v_ErrorType VARCHAR2(20):='EXCEPTION';
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT to_value INTO v_tovalue FROM ERES.AUDIT_MAPPEDVALUES WHERE from_value =''||p_fromvalue||'';
    RETURN v_tovalue;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN '';
WHEN OTHERS THEN
      PKG_AUDIT_TRAIL_MODULE.SP_AUDIT_ERROR_LOG(NULL,v_ErrorType, 'Function', 'Exception Raised:'||sqlerrm||'',NULL,'Error In FUNCTION F_GETCHAINID '); 
END F_GETMAPPEDVALUE;
END PKG_AUDIT_TRAIL_MODULE;
/