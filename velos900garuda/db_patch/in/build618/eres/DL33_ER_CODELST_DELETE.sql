--STARTS DELETING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp';
  if (v_record_exists > 1) then
	DELETE FROM ER_CODELST where codelst_type = 'storage_temp';
	commit;
  end if;
end;
/
--END--