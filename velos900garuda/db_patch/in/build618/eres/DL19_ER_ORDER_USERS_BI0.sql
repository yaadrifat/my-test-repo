create or replace TRIGGER "ERES".ER_ORDER_USERS_BI0 BEFORE INSERT ON ER_ORDER_USERS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_ORDER_USERS',erid, 'I',usr);

  INSERT_DATA:=:NEW.PK_ORDER_USER || '|' ||
to_char(:NEW.USER_LOGINID) || '|' ||
to_char(:NEW.USER_FNAME) || '|' ||
to_char(:NEW.USER_LNAME) || '|' ||
to_char(:NEW.USER_MAILID) || '|' ||
to_char(:NEW.USER_CONTACTNO) || '|' ||
to_char(:NEW.USER_COMMENTS) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.RID);
INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
END;
/