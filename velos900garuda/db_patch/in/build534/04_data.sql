declare 
	v_code_count number;
begin

	select count(*)
	into v_code_count 
	from er_codelst where codelst_type = 'subm_status' and codelst_subtyp = 'reviewer';

	if (v_code_count = 0) then

			Insert into ER_CODELST
			   (PK_CODELST, FK_ACCOUNT, CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC, 
			    CODELST_HIDE, CODELST_SEQ)
			 Values
			   (seq_er_codelst.nextval, NULL, 'subm_status', 'reviewer', 'Reviewer', 
			    'N', 10);
		
		COMMIT;

	end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,67,13,'04_data.sql',sysdate,'8.8.3 Build#524g');

commit;
