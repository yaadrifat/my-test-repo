CREATE OR REPLACE TRIGGER ER_SPECIMEN_AU0 AFTER UPDATE ON ER_SPECIMEN FOR EACH ROW
DECLARE
  raid NUMBER(10);
  new_usr VARCHAR2(100);
  old_usr VARCHAR2(100);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);


  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
    	audit_trail.record_transaction
    	(raid, 'ER_SPECIMEN', :OLD.rid, 'U', usr);
  END IF;


  IF NVL(:OLD.pk_specimen,0) !=
     NVL(:NEW.pk_specimen,0) THEN
     audit_trail.column_update
       (raid, 'PK_SPECIMEN',
       :OLD.pk_specimen, :NEW.pk_specimen);
  END IF;

  IF NVL(:OLD.spec_id,' ') !=
     NVL(:NEW.spec_id,' ') THEN
     audit_trail.column_update
       (raid, 'SPEC_ID',
       :OLD.spec_id, :NEW.spec_id);
  END IF;

  IF NVL(:OLD.spec_alternate_id,' ') !=
     NVL(:NEW.spec_alternate_id,' ') THEN
     audit_trail.column_update
       (raid, 'SPEC_ALTERNATE_ID',
       :OLD.spec_alternate_id, :NEW.spec_alternate_id);
  END IF;

  IF NVL(:OLD.spec_description,' ') !=
     NVL(:NEW.spec_description, ' ') THEN
     audit_trail.column_update
       (raid, 'SPEC_DESCRIPTION',
       :OLD.spec_description, :NEW.spec_description);
  END IF;

  IF NVL(:OLD.spec_original_quantity,0) !=
     NVL(:NEW.spec_original_quantity,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_ORIGINAL_QUANTITY',
       :OLD.spec_original_quantity, :NEW.spec_original_quantity);
  END IF;

  IF NVL(:OLD.spec_quantity_units,0) !=
     NVL(:NEW.spec_quantity_units,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_QUANTITY_UNITS',
       :OLD.spec_quantity_units, :NEW.spec_quantity_units);
  END IF;

  IF NVL(:OLD.spec_type,0) !=
     NVL(:NEW.spec_type,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_TYPE',
       :OLD.spec_type, :NEW.spec_type);
  END IF;

  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;

  IF NVL(:OLD.spec_collection_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.spec_collection_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'SPEC_COLLECTION_DATE',
       to_char(:OLD.spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;

  IF NVL(:OLD.spec_owner_fk_user,0) !=
     NVL(:NEW.spec_owner_fk_user,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_OWNER_FK_USER',
       :OLD.spec_owner_fk_user, :NEW.spec_owner_fk_user);
  END IF;


  IF NVL(:OLD.fk_storage,0) !=
     NVL(:NEW.fk_storage,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE',
       :OLD.fk_storage, :NEW.fk_storage);
  END IF;

  IF NVL(:OLD.fk_site,0) !=
     NVL(:NEW.fk_site,0) THEN
     audit_trail.column_update
       (raid, 'FK_SITE',
       :OLD.fk_site, :NEW.fk_site);
  END IF;


  IF NVL(:OLD.fk_visit,0) !=
     NVL(:NEW.fk_visit,0) THEN
     audit_trail.column_update
       (raid, 'FK_VISIT',
       :OLD.fk_visit, :NEW.fk_visit);
  END IF;


  IF NVL(:OLD.fk_sch_events1,0) !=
     NVL(:NEW.fk_sch_events1,0) THEN
     audit_trail.column_update
       (raid, 'FK_SCH_EVENTS1',
       :OLD.fk_sch_events1, :NEW.fk_sch_events1);
  END IF;


  IF NVL(:OLD.fk_specimen,0) !=
     NVL(:NEW.fk_specimen,0) THEN
     audit_trail.column_update
       (raid, 'FK_SPECIMEN',
       :OLD.fk_specimen, :NEW.fk_specimen);
  END IF;


  IF NVL(:OLD.spec_storage_temp,0) !=
     NVL(:NEW.spec_storage_temp,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_STORAGE_TEMP',
       :OLD.spec_storage_temp, :NEW.spec_storage_temp);
  END IF;

  IF NVL(:OLD.spec_storage_temp_unit,0) !=
     NVL(:NEW.spec_storage_temp_unit,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_STORAGE_TEMP_UNIT',
       :OLD.spec_storage_temp_unit, :NEW.spec_storage_temp_unit);
  END IF;


  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;


  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.fk_account,0) !=
     NVL(:NEW.fk_account,0) THEN
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :OLD.fk_account, :NEW.fk_account);
  END IF;
--KM-013008
IF NVL(:OLD.spec_processing_type,0) !=
     NVL(:NEW.spec_processing_type,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_PROCESSING_TYPE',
       :OLD.spec_processing_type, :NEW.spec_processing_type);
END IF;

IF NVL(:OLD.fk_storage_kit_component,0) !=
     NVL(:NEW.fk_storage_kit_component,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE_KIT_COMPONENT',
       :OLD.fk_storage_kit_component, :NEW.fk_storage_kit_component);
END IF;
--------------
  IF NVL(:OLD.FK_USER_PATH,0) !=
     NVL(:NEW.FK_USER_PATH,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.FK_USER_PATH ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.FK_USER_PATH ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_USER_PATH',
       old_modby, new_modby);
  END IF;

IF NVL(:OLD.FK_USER_SURG,0) !=
     NVL(:NEW.FK_USER_SURG,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.FK_USER_SURG ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.FK_USER_SURG ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_USER_SURG',
       old_modby, new_modby);
  END IF;

IF NVL(:OLD.FK_USER_COLLTECH,0) !=
     NVL(:NEW.FK_USER_COLLTECH,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.FK_USER_COLLTECH ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.FK_USER_COLLTECH ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_USER_COLLTECH',
       old_modby, new_modby);
  END IF;


IF NVL(:OLD.FK_USER_PROCTECH,0) !=
     NVL(:NEW.FK_USER_PROCTECH,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.FK_USER_PROCTECH ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.FK_USER_PROCTECH ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_USER_PROCTECH',
       old_modby, new_modby);
  END IF;


  IF NVL(:OLD.SPEC_REMOVAL_TIME,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.SPEC_REMOVAL_TIME,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'SPEC_REMOVAL_TIME',
       to_char(:OLD.SPEC_REMOVAL_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.SPEC_REMOVAL_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;


  IF NVL(:OLD.SPEC_FREEZE_TIME,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.SPEC_FREEZE_TIME,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'SPEC_FREEZE_TIME',
       to_char(:OLD.SPEC_FREEZE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.SPEC_FREEZE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.spec_anatomic_site ,0) !=
     NVL(:NEW.spec_anatomic_site ,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_ANATOMIC_SITE',
       :OLD.spec_anatomic_site , :NEW.spec_anatomic_site);
  END IF;

  IF NVL(:OLD.spec_tissue_side  ,0) !=
     NVL(:NEW.spec_tissue_side  ,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_TISSUE_SIDE',
       :OLD.spec_tissue_side  , :NEW.spec_tissue_side );
  END IF;

  IF NVL(:OLD.spec_pathology_stat  ,0) !=
     NVL(:NEW.spec_pathology_stat  ,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_PATHOLOGY_STAT',
       :OLD.spec_pathology_stat  , :NEW.spec_pathology_stat );
  END IF;

  IF NVL(:OLD.fk_storage_kit  ,0) !=
     NVL(:NEW.fk_storage_kit  ,0) THEN
     audit_trail.column_update
       (raid, 'FK_STORAGE_KIT',
       :OLD.fk_storage_kit  , :NEW.fk_storage_kit );
  END IF;

  IF NVL(:OLD.spec_expected_quantity  ,0) !=
     NVL(:NEW.spec_expected_quantity  ,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_EXPECTED_QUANTITY',
       :OLD.spec_expected_quantity  , :NEW.spec_expected_quantity );
  END IF;

  IF NVL(:OLD.spec_base_orig_quantity  ,0) !=
     NVL(:NEW.spec_base_orig_quantity  ,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_BASE_ORIG_QUANTITY',
       :OLD.spec_base_orig_quantity  , :NEW.spec_base_orig_quantity );
  END IF;


  IF NVL(:OLD.spec_expectedq_units  ,0) !=
     NVL(:NEW.spec_expectedq_units  ,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_EXPECTEDQ_UNITS',
       :OLD.spec_expectedq_units  , :NEW.spec_expectedq_units );
  END IF;


  IF NVL(:OLD.spec_base_origq_units  ,0) !=
     NVL(:NEW.spec_base_origq_units  ,0) THEN
     audit_trail.column_update
       (raid, 'SPEC_BASE_ORIGQ_UNITS',
       :OLD.spec_base_origq_units  , :NEW.spec_base_origq_units );
  END IF;


END;
/



CREATE OR REPLACE TRIGGER ER_SPECIMEN_STATUS_AU0 AFTER UPDATE ON ER_SPECIMEN_STATUS FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;

BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);


  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
  		audit_trail.record_transaction(raid, 'ER_SPECIMEN_STATUS', :OLD.rid, 'U', usr);
  END IF;

  IF NVL(:OLD.PK_SPECIMEN_STATUS,0) !=
     NVL(:NEW.PK_SPECIMEN_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'PK_SPECIMEN_STATUS',
       :OLD.PK_SPECIMEN_STATUS, :NEW.PK_SPECIMEN_STATUS);
  END IF;

  IF NVL(:OLD.FK_SPECIMEN,0) !=
     NVL(:NEW.FK_SPECIMEN,0) THEN
     audit_trail.column_update
       (raid, 'FK_SPECIMEN',
       :OLD.FK_SPECIMEN, :NEW.FK_SPECIMEN);
  END IF;

  IF NVL(:OLD.SS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.SS_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'SS_DATE',
       to_char(:OLD.SS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.SS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.FK_CODELST_STATUS,0) !=
     NVL(:NEW.FK_CODELST_STATUS,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_STATUS',
       :OLD.FK_CODELST_STATUS, :NEW.FK_CODELST_STATUS);
  END IF;


  IF NVL(:OLD.SS_QUANTITY,0) !=
     NVL(:NEW.SS_QUANTITY,0) THEN
     audit_trail.column_update
       (raid, 'SS_QUANTITY',
       :OLD.SS_QUANTITY, :NEW.SS_QUANTITY);
  END IF;

  IF NVL(:OLD.SS_QUANTITY_UNITS,0) !=
     NVL(:NEW.SS_QUANTITY_UNITS,0) THEN
     audit_trail.column_update
       (raid, 'SS_QUANTITY_UNITS',
       :OLD.SS_QUANTITY_UNITS, :NEW.SS_QUANTITY_UNITS);
  END IF;


  IF NVL(:OLD.SS_ACTION,0) !=
     NVL(:NEW.SS_ACTION,0) THEN
     audit_trail.column_update
       (raid, 'SS_ACTION',
       :OLD.SS_ACTION, :NEW.SS_ACTION);
  END IF;


  IF NVL(:OLD.FK_STUDY,0) !=
     NVL(:NEW.FK_STUDY,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.FK_STUDY, :NEW.FK_STUDY);
  END IF;


  IF NVL(:OLD.FK_USER_RECEPIENT,0) !=
     NVL(:NEW.FK_USER_RECEPIENT,0) THEN
     audit_trail.column_update
       (raid, 'FK_USER_RECEPIENT',
       :OLD.FK_USER_RECEPIENT, :NEW.FK_USER_RECEPIENT);
  END IF;

  IF NVL(:OLD.SS_TRACKING_NUMBER,' ') !=
     NVL(:NEW.SS_TRACKING_NUMBER,' ') THEN
     audit_trail.column_update
       (raid, 'SS_TRACKING_NUMBER',
       :OLD.SS_TRACKING_NUMBER, :NEW.SS_TRACKING_NUMBER);
  END IF;

   IF NVL(:OLD.SS_STATUS_BY,0) !=
     NVL(:NEW.SS_STATUS_BY,0) THEN
     audit_trail.column_update
       (raid, 'SS_STATUS_BY',
       :OLD.SS_STATUS_BY, :NEW.SS_STATUS_BY);
  END IF;

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;



  IF NVL(:OLD.ss_proc_type,0) !=
     NVL(:NEW.ss_proc_type,0) THEN
     audit_trail.column_update
       (raid, 'SS_PROC_TYPE',
       :OLD.ss_proc_type, :NEW.ss_proc_type);
  END IF;


  IF NVL(:OLD.ss_hand_off_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.ss_hand_off_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'SS_HAND_OFF_DATE',
       to_char(:OLD.ss_hand_off_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.ss_hand_off_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;


  IF NVL(:OLD.ss_proc_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.ss_proc_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.column_update
       (raid, 'SS_PROC_DATE',
       to_char(:OLD.ss_proc_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.ss_proc_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

END;
/


