Set Define off;

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'resolution_code','TU','Temporarily Unavailable','N',null,null,null,null,null,null,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'resolution_code','CA','Canceled','N',null,null,null,null,null,null,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
    (SEQ_ER_CODELST.nextval,null,'transplant_type','SU','Single Unit','N',null,'Y',null,
    null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
    (SEQ_ER_CODELST.nextval,null,'transplant_type','MU','Multiple Unit','N',null,'Y',null,
    null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
    (SEQ_ER_CODELST.nextval,null,'transplant_type','Ex-Vivo','Ex-Vivo Expansion Transplant','N',null,'Y',null,
    null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
    CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
    CREATED_ON,    IP_ADD,    CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
    (SEQ_ER_CODELST.nextval,null,'transplant_type','ONTT','Other Non-Traditional Transplant','N',null,'Y',null,
    null,null,sysdate,sysdate,null,null,null,null);

--Update the status of Unlicensure


update ER_CODELST set codelst_desc = 'U.S.-prior to May 25, 2005' where codelst_type='unlicensed' and codelst_subtyp='u.s_prior';



update ER_CODELST set codelst_desc = 'U.S. ineligible-donor screening' where codelst_type='unlicensed' and codelst_subtyp='u.s_inel';


update ER_CODELST set codelst_desc = 'U.S. ineligible-donor testing' where codelst_type='unlicensed' and codelst_subtyp='u.s_inel_test';


update ER_CODELST set codelst_subtyp='u.s_inel_scr' where codelst_type='unlicensed' and codelst_desc = 'U.S. ineligible-donor screening';



INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'unlicensed','u.s_inescrn','U.S. ineligible-donor testing and screening','N',NULL,SYSDATE,SYSDATE,NULL);
  
  
commit;


Commit;