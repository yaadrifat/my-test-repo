 

ALTER TABLE ER_ORDER ADD(REQ_CLIN_INFO_FLAG VARCHAR2(1 BYTE));
COMMENT ON COLUMN ER_ORDER.REQ_CLIN_INFO_FLAG IS 'To store the status whether required clinical information is entered completely';

ALTER TABLE CB_CORD ADD(CBU_AVAIL_CONFIRM_FLAG VARCHAR2(1 BYTE));
COMMENT ON COLUMN CB_CORD.CBU_AVAIL_CONFIRM_FLAG IS 'To store the status whether cbu availability confirmed or not';

ALTER TABLE CB_SHIPMENT ADD(SHIPMENT_SCH_FLAG VARCHAR2(1));
COMMENT ON COLUMN CB_SHIPMENT.SHIPMENT_SCH_FLAG IS 'To store the status whether shipment scheduled or not';

ALTER TABLE ER_ORDER ADD(RESOL_ACK_FLAG VARCHAR2(1));
COMMENT ON COLUMN ER_ORDER.RESOL_ACK_FLAG IS 'To store the status whether resolution acknowledged or not';

ALTER TABLE CB_SHIPMENT ADD(CORD_SHIPPED_FLAG VARCHAR2(1));
COMMENT ON COLUMN CB_SHIPMENT.CORD_SHIPPED_FLAG IS 'To store the status whether cord shipped or not';

alter table cb_cord modify CORD_ID_NUMBER_ON_CBU_BAG varchar2(50 Byte);


------------------Last Build Pending Scripts----------------------------------

ALTER TABLE CB_CORD ADD(CORD_NMDP_CBU_ID VARCHAR2(10 BYTE));
COMMENT ON COLUMN "CB_CORD"."CORD_NMDP_CBU_ID" IS 'To Store NMDP CBU ID';

ALTER TABLE CB_CORD ADD(CORD_NMDP_MATERNAL_ID VARCHAR2(50 BYTE));
COMMENT ON COLUMN "CB_CORD"."CORD_NMDP_MATERNAL_ID" IS 'To Store NMDP MATERNAL ID';

ALTER TABLE CB_NOTES ADD(NOTE_SEQ NUMBER(10,0));
COMMENT ON COLUMN "CB_NOTES"."NOTE_SEQ" IS 'This will store sequence number of the note entered for particular cord';

alter table CB_ENTITY_STATUS_REASON add FK_ATTACHMENTID NUMBER(10,0);
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."FK_ATTACHMENTID" IS 'Store the Primary Key for Attachment id';

alter table CB_ENTITY_STATUS_REASON add FK_FORMID NUMBER(10,0);
COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."FK_FORMID" IS 'Store the eresearch form id linked with any reason'; 
