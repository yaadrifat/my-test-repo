set define off;

CREATE OR REPLACE TRIGGER "ESCH"."SCH_SUBCOST_ITEM_VISIT_BU0" 
BEFORE UPDATE
ON SCH_SUBCOST_ITEM_VISIT
FOR EACH ROW
when (NEW.last_modified_by IS NOT NULL) 
--Created by Manimaran for Audit Update
begin 
	:new.last_modified_date := sysdate;
end;
/


create or replace TRIGGER ESCH.SCH_SUBCOST_ITEM_VISIT_AD0 AFTER DELETE ON SCH_SUBCOST_ITEM_VISIT
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;

begin
select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ESCH_SUBCOST_ITEM_VISIT', :old.rid, 'D');
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_SUBCOST_ITEM_VISIT) || '|' ||
to_char(:old.FK_SUBCOST_ITEM) || '|' ||
to_char(:old.FK_PROTOCOL_VISIT) || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
:old.IP_ADD;

insert into AUDIT_DELETE
(raid, row_data) values (raid, deleted_data);
end;
/


create or replace TRIGGER ESCH."SCH_SUBCOST_ITEM_VISIT_AU0"
AFTER UPDATE
ON ESCH.SCH_SUBCOST_ITEM_VISIT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
--Created by Manimaran for Audit Update   
begin
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   audit_trail.record_transaction(raid, 'ER_SUBCOST_ITEM_VISIT', :old.rid, 'U', usr);
   
   if nvl(:old.PK_SUBCOST_ITEM_VISIT,0) !=
     nvl(:new.PK_SUBCOST_ITEM_VISIT,0) then
     audit_trail.column_update
       (raid, 'PK_SUBCOST_ITEM_VISIT',
       :old.PK_SUBCOST_ITEM_VISIT, :new.PK_SUBCOST_ITEM_VISIT);
   end if;
   
   if nvl(:old.FK_SUBCOST_ITEM,0) !=
     nvl(:new.FK_SUBCOST_ITEM,0) then
     audit_trail.column_update
       (raid, 'FK_SUBCOST_ITEM',
       :old.FK_SUBCOST_ITEM, :new.FK_SUBCOST_ITEM);
   end if;
   
   if nvl(:old.FK_PROTOCOL_VISIT,0) !=
     nvl(:new.FK_PROTOCOL_VISIT,0) then
     audit_trail.column_update
       (raid, 'FK_PROTOCOL_VISIT',
       :old.FK_PROTOCOL_VISIT, :new.FK_PROTOCOL_VISIT);
   end if;
   
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   
   IF NVL(:OLD.creator,0) !=
      NVL(:NEW.creator,0) THEN
      audit_trail.column_update
        (raid, 'creator',
        :OLD.creator, :NEW.creator);
   END IF;
   
   IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
	to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
	to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;

   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
 END;
/ 


  create or replace TRIGGER "ESCH"."SCH_SUBCOST_ITEM_VISIT_BI0"
  BEFORE INSERT
  ON SCH_SUBCOST_ITEM_VISIT
  REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
  DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(2000);
  insert_data CLOB;
--Created by Manimaran for Audit Insert
BEGIN

 BEGIN
   usr := getuser(:NEW.creator);
   EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction  (raid, 'SCH_SUBCOST_ITEM_VISIT', erid, 'I', usr );
  insert_data:= :NEW.PK_SUBCOST_ITEM_VISIT||'|'|| :NEW.FK_SUBCOST_ITEM||'|'|| :NEW.FK_PROTOCOL_VISIT||'|'||
	:NEW.RID||'|'|| :NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;
   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/






