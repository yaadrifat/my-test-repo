Alter table ER_STORAGE Add( FK_STORAGE_KIT  NUMBER );
Alter table ER_SPECIMEN Add( SPEC_EXPETCTED_QUANTITY NUMBER );
Alter table ER_SPECIMEN Add( SPEC_DISPOSITION  NUMBER );
Alter table ER_SPECIMEN Add( SPEC_ENVT_CONS  Varchar2(255 CHAR) ); 

comment on column ER_STORAGE.FK_STORAGE_KIT  is     'For main kit copy only.Stores the pk of main kit.';
comment on column ER_SPECIMEN.SPEC_EXPETCTED_QUANTITY  is     'Stores the specimen expected quantity.';
comment on column ER_SPECIMEN.SPEC_DISPOSITION  is     'Stores the specimen disposition.';
comment on column ER_SPECIMEN.SPEC_ENVT_CONS  is     'Stores the specimen environmental constraints';