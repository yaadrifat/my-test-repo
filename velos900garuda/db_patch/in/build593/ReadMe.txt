eResearch v9.0 build #593

The library jar "commons-io-2.0.1.jar" is needed by FOP (for PDF generation) to be compatible with Struts 2.2.3 which will be used by the Garuda module. Place this jar in server\eresearch\lib. Then manually remove the older version "commons-io-1.3.1.jar" from the same folder on JBoss.


Given below are the list of JSP files and their respective modules where this feature is implemented so far.

	Front end JSP 		Modules 							Notes
	
1 	accountlinkbrowser.jsp 	Manage Account >> Account Links 	
2 	ulinkBrowser.jsp 	Personalize Account >> My Links 	
3 	accountbrowser.jsp 	Manage Account >> Active Account Users 	
4 	newOrganization.jsp 	Study >> Team 	
5 	formsettings.jsp 	Form >> Form Settings 						Only for deletion of Associated Messages
6 	mileapndx.jsp 		Milestone >> Appendix 	
7 	sectionBrowserNew.jsp 	Study >> Versions 	

8 	eventlibrary.jsp 	Event Library>> DeleteEvent 	
9 	eventbrowser.jsp 	Study>>StudySetup>>Calander>>Select Event 	
10 	studyprotocols.jsp 	Study>>StudySetup>>Calander>>Select Event 	
	
11 	specimenapndx,jsp 	Manage Inventory >> Specimen >> Appendix 	
12 	eventmessage.jsp 	Event Library >> Event Message 	
13 	fetchSubCostItems.jsp 	Calendar >> Patient Cost Items 

14	enrollpatient.jsp 	Patient Management
15	crflibbrowser.jsp 	Event Library
16	studyprotocols.jsp 	Protocol Management
17	linkforms.jsp 		Event Library
18	portal.jsp 			Portal Management


Following Files have been committed for EDC module by Localization Team

1	addcrflinkforms.jsp
2	addeventcost.jsp
3	addeventfile.jsp
4	addeventkit.jsp
5	addeventrole.jsp
6	addeventurl.jsp
7	addeventuser.jsp
8	addevtvisits.jsp
9	addFieldToForm.jsp
10	addFieldToFormSubmit.jsp
11	calDoesNotExist.jsp
12	calrepretrieve.jsp
13	category.jsp
14	commentType.jsp
15	commentTypeSubmit.jsp
16	copyevent.jsp
17	copyfieldtoform.jsp
18	copyFormFromLib.jsp
19	copyFormFromLibSubmit.jsp
20	copyFromFieldLibrary.jsp
21	copyprotocol.jsp
22	costsave.jsp
23	crflib.jsp
24	crflibbrowser.jsp
25	crflibsave.jsp
26	defineCalcFieldAction.jsp
27	defineFieldAction.jsp
28	deletecrf.jsp
29	deleteeventfromprot.jsp
30	deleteFieldAction.jsp
31	deletevisitseventsfromcal.jsp
32	editBox.jsp
33	editBoxSection.jsp
34	editBoxSectionSubmit.jsp
35	eventappendix.jsp
36	eventbrowser.jsp
37	eventcost.jsp
38	eventdetails.jsp
39	eventdetailsave.jsp
40	eventlibrary.jsp
41	eventmessage.jsp
42	eventmessageusers.jsp
43	eventresource.jsp
44	eventuserdelete.jsp
45	eventusersave.jsp
46	fetchCoverage.jsp
47	fetchProt.jsp
48	fetchProt2.jsp
49	fetchSubCostItems.jsp
50	fieldBrowser.jsp
51	fieldBrowserSubmit.jsp
52	fieldCategory.jsp
53	fielddelete.jsp
54	fieldLibraryBrowser.jsp
55	fldrespdelete.jsp
56	formfldbrowser.jsp
57	formflddelmultiple.jsp
58	formLibraryBrowser.jsp
59	formNotify.jsp
60	formNotifySubmit.jsp
61	formsettings.jsp
62	getlookup.jsp
63	horizontalRuleType.jsp
64	horizontalRuleTypeSubmit.jsp
65	include.jsp
66	linkforms.jsp
67	lookupType.jsp
68	lookupTypeSubmit.jsp
69	multicostsave.jsp
70	multilookup.jsp
71	multipleChoiceBox.jsp
72	multipleChoiceSection.jsp
73	multipleChoiceSectionSubmit.jsp
74	neweventcost.jsp
75	newSection.jsp
76	newSectionSubmit.jsp
77	panel.jsp
78	popupaddcrf.jsp
79	popupaddcrfsave.jsp
80	popupeditor.jsp
81	protocolcalendar.jsp
82	protocoltabs.jsp
83	rolesave.jsp
84	savedelmultiple.jsp
85	selectActionConditionValues.jsp
86	selectActionTargetFields.jsp
87	selecteventus.jsp
88	selectGroup.jsp
89	selectremoveusers.jsp
90	selectStudy.jsp
91	updateprotocol.jsp
92	urlsave.jsp
93	viewevent.jsp
94	viewGroup.jsp
95	viewOrg.jsp
96	viewStudyPopup.jsp
97	visitdetail.jsp
98	visitdetailsave.jsp


