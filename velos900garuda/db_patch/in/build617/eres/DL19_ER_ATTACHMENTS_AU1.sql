CREATE OR REPLACE TRIGGER "ERES"."ER_ATTACHMENTS_AU1" AFTER UPDATE ON ERES.ER_ATTACHMENTS
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO V_ROWID FROM DUAL;   
	 
	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'ER_ATTACHMENTS',:OLD.RID,:OLD.PK_ATTACHMENT,'U',:NEW.CREATOR); 

	IF NVL(:OLD.PK_ATTACHMENT,0) != NVL(:NEW.PK_ATTACHMENT,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'PK_ATTACHMENT',:OLD.PK_ATTACHMENT, :NEW.PK_ATTACHMENT,NULL,NULL);
  END IF;
  IF NVL(:OLD.ENTITY_ID,0) != NVL(:NEW.ENTITY_ID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ENTITY_ID',:OLD.ENTITY_ID, :NEW.ENTITY_ID,NULL,NULL);
  END IF;
  IF NVL(:OLD.ENTITY_TYPE,0) != NVL(:NEW.ENTITY_TYPE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ENTITY_TYPE',:OLD.ENTITY_TYPE, :NEW.ENTITY_TYPE,NULL,NULL);
  END IF;  
  IF NVL(:OLD.FK_ATTACHMENT_TYPE,0) !=  NVL(:NEW.FK_ATTACHMENT_TYPE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_ATTACHMENT_TYPE',:OLD.FK_ATTACHMENT_TYPE, :NEW.FK_ATTACHMENT_TYPE,NULL,NULL);
  END IF;
  IF NVL(:OLD.DCMS_FILE_ATTACHMENT_ID,0) != NVL(:NEW.DCMS_FILE_ATTACHMENT_ID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'DCMS_FILE_ATTACHMENT_ID',:OLD.DCMS_FILE_ATTACHMENT_ID, :NEW.DCMS_FILE_ATTACHMENT_ID,NULL,NULL);
  END IF;
   IF NVL(:OLD.CREATOR,0) !=NVL(:NEW.CREATOR,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
  END IF;  
  IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
      END IF;
   IF NVL(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'LAST_MODIFIED_DATE', TO_CHAR(:OLD.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;  
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'RID',:OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
	IF NVL(:OLD.DOCUMENT_TYPE,' ') != NVL(:NEW.DOCUMENT_TYPE,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'DOCUMENT_TYPE',:OLD.DOCUMENT_TYPE, :NEW.DOCUMENT_TYPE,NULL,NULL);
  END IF; 
  IF NVL(:OLD.ATTACHMENTS_TYPE_REM,' ') != NVL(:NEW.ATTACHMENTS_TYPE_REM,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ATTACHMENTS_TYPE_REM',:OLD.ATTACHMENTS_TYPE_REM, :NEW.ATTACHMENTS_TYPE_REM,NULL,NULL);
  END IF;  
  IF NVL(:OLD.ATTACHMENT_FILE_NAME,' ') != NVL(:NEW.ATTACHMENT_FILE_NAME,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ATTACHMENT_FILE_NAME',:OLD.ATTACHMENT_FILE_NAME, :NEW.ATTACHMENT_FILE_NAME,NULL,NULL);
  END IF;	
END ;
/