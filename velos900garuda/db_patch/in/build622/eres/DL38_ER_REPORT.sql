set define off;

delete from er_repxsl where pk_repxsl = 168;
commit;

Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
IDNOTES, CBBID, STORAGE_LOC, CBU_COLLECTION_SITE, CBU_INFO_NOTES_VISIBLE_TC, ELIGIBLE_STATUS, ELIGIBILITY_NOTES_VISIBLE_TC,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
heparin_thou_per,heparin_thou_ml,heparin_five_per,heparin_five_ml,heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) idm_attachments,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) hhs_attachments,CBB_ID FROM rep_cbu_details where pk_cord = ~1' where pk_report = 168;
COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
IDNOTES, CBBID, STORAGE_LOC, CBU_COLLECTION_SITE, CBU_INFO_NOTES_VISIBLE_TC, ELIGIBLE_STATUS, ELIGIBILITY_NOTES_VISIBLE_TC,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
heparin_thou_per,heparin_thou_ml,heparin_five_per,heparin_five_ml,heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) idm_attachments,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) hhs_attachments,CBB_ID FROM rep_cbu_details where pk_cord = ~1' where pk_report = 168;
COMMIT;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 169;
  if (v_record_exists = 0) then
   Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (169,'IDs Report','IDs Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);
COMMIT;	

Update ER_REPORT set REP_SQL = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
cbbid, storage_loc,cbu_collection_site,(select F_CORD_ID_ON_BAG(~1) from dual)as idonbag,(select F_CORD_ADD_IDS(~1) from dual) addids,CBB_ID FROM rep_cbu_details where pk_cord = ~1' where pk_report = 169;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
cbbid, storage_loc,cbu_collection_site,(select F_CORD_ID_ON_BAG(~1) from dual)as idonbag,(select F_CORD_ADD_IDS(~1) from dual) addids,CBB_ID FROM rep_cbu_details where pk_cord = ~1' where pk_report = 169;
	COMMIT;

  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 170;
  if (v_record_exists = 0) then
   Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (170,'Processing Information Report','Processing Information Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);
COMMIT;	

Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, cbbid, cbb_id, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, individual_frac, freezer_manufact, other_freezer_manufact,
bagtype, other_bag, storage_temperature, cryobag_manufac, ctrl_rate_freezing, max_vol, frozen_in, other_frozen_cont,
heparin_thou_per, heparin_thou_ml, heparin_five_per, heparin_five_ml, heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti, hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,
ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,five_dextrose_ml,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
othr_diluents_ml,spec_othr_diluents,no_of_segments,extr_dna,filter_paper,no_of_cell_mater_aliquots,rpc_pellets,
no_of_serum_mater_aliquots,no_of_extr_dna_mater_aliquots, no_of_plasma_mater_aliquots,serum_aliquotes,plasma_aliquotes,nonviable_aliquotes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) processinfo_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 170;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, cord_local_cbu_id, cord_isbi_din_code, cbbid, cbb_id, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, individual_frac, freezer_manufact, other_freezer_manufact,
bagtype, other_bag, storage_temperature, cryobag_manufac, ctrl_rate_freezing, max_vol, frozen_in, other_frozen_cont,
heparin_thou_per, heparin_thou_ml, heparin_five_per, heparin_five_ml, heparin_ten_per,heparin_ten_ml,heparin_six_per,heparin_six_ml,cpda_per,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti, hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,
ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,five_human_albu_ml,twentyfive_human_albu_per,twentyfive_human_albu_ml, plasmalyte_per,plasmalyte_ml,othr_cryoprotectant_per,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,five_dextrose_ml,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
othr_diluents_ml,spec_othr_diluents,no_of_segments,extr_dna,filter_paper,no_of_cell_mater_aliquots,rpc_pellets,
no_of_serum_mater_aliquots,no_of_extr_dna_mater_aliquots, no_of_plasma_mater_aliquots,serum_aliquotes,plasma_aliquotes,nonviable_aliquotes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) processinfo_attachments
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 170;
	COMMIT;

  end if;
end;
/

CREATE OR REPLACE
function f_cord_add_ids(pk_cord number) return SYS_REFCURSOR
IS
p_add_id SYS_REFCURSOR;
BEGIN
    OPEN p_add_id FOR SELECT F_CODELST_DESC(FK_CB_ADDITIONAL_ID_TYPE) AS IDTYPE,CB_ADDITIONAL_ID AS ADDIDVAL FROM CB_ADDITIONAL_IDS WHERE ENTITY_ID = pk_cord;
RETURN P_ADD_ID;
END;
/

CREATE OR REPLACE
function f_cord_id_on_bag(pk_cord number) return SYS_REFCURSOR
IS
p_id_on_bag SYS_REFCURSOR;
BEGIN
    OPEN p_id_on_bag FOR SELECT F_CODELST_DESC(FK_CODELST) AS IDONBAGVAL FROM CB_MULTIPLE_VALUES WHERE ENTITY_ID = pk_cord AND FK_CODELST_TYPE='all_ids';
RETURN P_ID_ON_BAG;
END;
/

create or replace
function f_get_attachments(pk_cord number,categry number) return SYS_REFCURSOR
IS
p_attachments SYS_REFCURSOR;
BEGIN
    OPEN P_ATTACHMENTS FOR SELECT to_char(ATTACHMENTS.CREATED_ON,'Mon DD, YYYY') AS CREATEDON, ATTACHMENTS.ATTACHMENT_FILE_NAME AS FILENAME,F_CODELST_DESC(UPLOADINFO.FK_CATEGORY) AS CATEGORYVAL FROM ER_ATTACHMENTS ATTACHMENTS LEFT OUTER JOIN CB_UPLOAD_INFO UPLOADINFO ON ATTACHMENTS.PK_ATTACHMENT=UPLOADINFO.FK_ATTACHMENTID WHERE ATTACHMENTS.ENTITY_ID =PK_CORD AND UPLOADINFO.FK_CATEGORY = CATEGRY;
RETURN P_ATTACHMENTS;
END;
/
