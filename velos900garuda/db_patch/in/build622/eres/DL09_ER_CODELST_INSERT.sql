set define off;


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_milestone'
    AND codelst_subtyp = 'cordshipped';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'order_milestone','cordshipped','CORD SHIPPED','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_milestone'
    AND codelst_subtyp = 'cordshipped';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'order_milestone','cordshipped','CORD SHIPPED','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


commit;