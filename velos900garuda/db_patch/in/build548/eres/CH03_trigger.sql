CREATE OR REPLACE TRIGGER "ER_STUDY_SITE_RIGHTS_AD0" AFTER DELETE ON ER_STUDY_SITE_RIGHTS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Gopu for Audit Report After Delete
DECLARE
   raid number(10);
   deleted_data varchar2(2000);
    usr VARCHAR(2000);

   

BEGIN

usr := Getuser(:OLD.last_modified_by);
   select seq_audit.nextval into raid from dual;
   audit_trail.record_transaction (raid, 'ER_STUDY_SITE_RIGHTS', :old.rid, 'D',usr);
   deleted_data :=
      to_char(:old.PK_STUDY_SITE_RIGHTS) || '|' ||
      to_char(:old.FK_SITE) || '|' ||
      to_char(:old.FK_STUDY) || '|' ||
      to_char(:old.FK_USER) || '|' ||
      to_char(:old.USER_STUDY_SITE_RIGHTS) || '|' ||
      to_char(:old.rid) || '|' ||
      to_char(:old.creator) || '|' ||
      to_char(:old.last_modified_by) || '|' ||
      to_char(:old.last_modified_date) || '|' ||
      to_char(:old.created_on) || '|' ||
      :old.ip_add;
    insert into audit_delete (raid, row_data) values (raid, deleted_data);
END;
/



  CREATE OR REPLACE TRIGGER "ERES"."ER_STUDYSITES_AD" 
AFTER DELETE
ON ER_STUDYSITES
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  orid number(10);
  usr VARCHAR(2000);
BEGIN
   
    usr := Getuser(:OLD.last_modified_by);
   orid:=nvl(:old.rid,0);
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYSITES', orid, 'D',usr);
 deleted_data :=TO_CHAR(:OLD.PK_STUDYSITES)||'|'||
 TO_CHAR(:OLD.FK_STUDY)||'|'||
 TO_CHAR(:OLD.FK_SITE)||'|'||
 TO_CHAR(:OLD.FK_CODELST_STUDYSITETYPE)||'|'||
 :OLD.STUDYSITE_LSAMPLESIZE||'|'||
 TO_CHAR(:OLD.STUDYSITE_PUBLIC)||'|'||
 TO_CHAR(:OLD.STUDYSITE_REPINCLUDE)||'|'||
 TO_CHAR(orid)||'|'||
 TO_CHAR(:OLD.CREATOR)||'|'||
 TO_CHAR(:OLD.CREATED_ON)||'|'||
 TO_CHAR(:OLD.LAST_MODIFIED_BY)||'|'||
 TO_CHAR(:OLD.LAST_MODIFIED_ON)||'|'||
 :OLD.IP_ADD||'|'||
 TO_CHAR(:OLD.STUDYSITE_ENRCOUNT);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);

END;
/

CREATE OR REPLACE TRIGGER "ERES"."ER_STUDYTEAM_AD0" 
AFTER DELETE
ON ER_STUDYTEAM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
  usr VARCHAR(2000);


begin

  usr := Getuser(:OLD.last_modified_by);

  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYTEAM', :old.rid, 'D', usr);
  deleted_data :=
  to_char(:old.pk_studyteam) || '|' ||
  to_char(:old.fk_codelst_tmrole) || '|' ||
  to_char(:old.fk_user) || '|' ||
  to_char(:old.fk_study) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.study_team_rights || '|' ||
  :old.ip_add || '|' ||
  :old.study_team_usr_type || '|' ||
  -- Added the column studyteam status for july-august enhancement
  :old.studyteam_status;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


 