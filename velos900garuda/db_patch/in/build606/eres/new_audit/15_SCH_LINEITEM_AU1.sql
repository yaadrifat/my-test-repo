--When SCH_LINEITEM table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ESCH"."SCH_LINEITEM_AU1" 
AFTER UPDATE ON ESCH.SCH_LINEITEM 
REFERENCING NEW AS NEW OLD AS OLD FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
	--This Block run when there is Delete operation perform on SCH_LINEITEM
    IF :NEW.LINEITEM_DELFLAG='Y' THEN
	  --Inserting row in AUDIT_ROW_MODULE table.Action - 'D' is used for Logical Deletion of the record.
      PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'SCH_LINEITEM',:OLD.RID,NULL,'D',:NEW.Creator);
	  
      --Inserting rows in AUDIT_Column_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'PK_LINEITEM',:OLD.PK_LINEITEM,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_BGTSECTION',:OLD.FK_BGTSECTION,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_DESC',:OLD.LINEITEM_DESC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_SPONSORUNIT',:OLD.LINEITEM_SPONSORUNIT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_CLINICNOFUNIT',:OLD.LINEITEM_CLINICNOFUNIT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_OTHERCOST',:OLD.LINEITEM_OTHERCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_DELFLAG',:OLD.LINEITEM_DELFLAG,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'RID',:OLD.RID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CREATOR',:OLD.CREATOR,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_NAME',:OLD.LINEITEM_NAME,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_NOTES',:OLD.LINEITEM_NOTES,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_INPERSEC',:OLD.LINEITEM_INPERSEC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_STDCARECOST',:OLD.LINEITEM_STDCARECOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_RESCOST',:OLD.LINEITEM_RESCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_INVCOST',:OLD.LINEITEM_INVCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_INCOSTDISC',:OLD.LINEITEM_INCOSTDISC,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_CPTCODE',:OLD.LINEITEM_CPTCODE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_REPEAT',:OLD.LINEITEM_REPEAT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_PARENTID',:OLD.LINEITEM_PARENTID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_APPLYINFUTURE',:OLD.LINEITEM_APPLYINFUTURE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_CODELST_CATEGORY',:OLD.FK_CODELST_CATEGORY,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_TMID',:OLD.LINEITEM_TMID,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_CDM',:OLD.LINEITEM_CDM,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_APPLYINDIRECTS',:OLD.LINEITEM_APPLYINDIRECTS,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_TOTALCOST',:OLD.LINEITEM_TOTALCOST,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_SPONSORAMOUNT',:OLD.LINEITEM_SPONSORAMOUNT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_VARIANCE',:OLD.LINEITEM_VARIANCE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_EVENT',:OLD.FK_EVENT,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_SEQ',:OLD.LINEITEM_SEQ,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_CODELST_COST_TYPE',:OLD.FK_CODELST_COST_TYPE,NULL,NULL,NULL);
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'SUBCOST_ITEM_FLAG',:OLD.SUBCOST_ITEM_FLAG,NULL,NULL,NULL);
   ELSE
   --This Block run when there is Update operation perform on SCH_LINEITEM 
	  --Inserting row in the table AUDIT_ROW_MODULE.
      PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'SCH_LINEITEM',:OLD.RID,NULL,'U',:NEW.Creator);
      
	  --Inserting row in the table AUDIT_COLUMN_MODULE.
	  IF NVL(:OLD.PK_LINEITEM,0) != NVL(:NEW.PK_LINEITEM,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'PK_LINEITEM',:OLD.PK_LINEITEM, :NEW.PK_LINEITEM,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_BGTSECTION,0) != NVL(:NEW.FK_BGTSECTION,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_BGTSECTION',:OLD.FK_BGTSECTION, :NEW.FK_BGTSECTION,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_DESC,' ') != NVL(:NEW.LINEITEM_DESC,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_DESC',:OLD.LINEITEM_DESC, :NEW.LINEITEM_DESC,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_SPONSORUNIT,' ') != NVL(:NEW.LINEITEM_SPONSORUNIT,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_SPONSORUNIT',:OLD.LINEITEM_SPONSORUNIT,:NEW.LINEITEM_SPONSORUNIT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_CLINICNOFUNIT,' ') != NVL(:NEW.LINEITEM_CLINICNOFUNIT,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_CLINICNOFUNIT',:OLD.LINEITEM_CLINICNOFUNIT, :NEW.LINEITEM_CLINICNOFUNIT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_OTHERCOST,' ') != NVL(:NEW.LINEITEM_OTHERCOST,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_OTHERCOST',:OLD.LINEITEM_OTHERCOST, :NEW.LINEITEM_OTHERCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_DELFLAG,' ') != NVL(:NEW.LINEITEM_DELFLAG,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_DELFLAG',:OLD.LINEITEM_DELFLAG, :NEW.LINEITEM_DELFLAG,NULL,NULL);
      END IF;
      IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'RID',:OLD.RID, :NEW.RID,NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
      END IF;
      IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:NEW.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
      END IF;
      IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_NAME,' ') != NVL(:NEW.LINEITEM_NAME,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_NAME',:OLD.LINEITEM_NAME, :NEW.LINEITEM_NAME,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_NOTES,' ') != NVL(:NEW.LINEITEM_NOTES,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_NOTES',:OLD.LINEITEM_NOTES, :NEW.LINEITEM_NOTES,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_INPERSEC,0) != NVL(:NEW.LINEITEM_INPERSEC,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_INPERSEC',:OLD.LINEITEM_INPERSEC, :NEW.LINEITEM_INPERSEC,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_STDCARECOST,0) != NVL(:NEW.LINEITEM_STDCARECOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_STDCARECOST',:OLD.LINEITEM_STDCARECOST, :NEW.LINEITEM_STDCARECOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_RESCOST,0) != NVL(:NEW.LINEITEM_RESCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_RESCOST',:OLD.LINEITEM_RESCOST, :NEW.LINEITEM_RESCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_INVCOST,0) != NVL(:NEW.LINEITEM_INVCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_INVCOST',:OLD.LINEITEM_INVCOST, :NEW.LINEITEM_INVCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_INCOSTDISC,0) != NVL(:NEW.LINEITEM_INCOSTDISC,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_INCOSTDISC',:OLD.LINEITEM_INCOSTDISC, :NEW.LINEITEM_INCOSTDISC,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_CPTCODE,' ') != NVL(:NEW.LINEITEM_CPTCODE,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_CPTCODE',:OLD.LINEITEM_CPTCODE, :NEW.LINEITEM_CPTCODE,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_REPEAT,0) != NVL(:NEW.LINEITEM_REPEAT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_REPEAT',:OLD.LINEITEM_REPEAT, :NEW.LINEITEM_REPEAT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_PARENTID,0) != NVL(:NEW.LINEITEM_PARENTID,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_PARENTID',:OLD.LINEITEM_PARENTID, :NEW.LINEITEM_PARENTID,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_APPLYINFUTURE,0) != NVL(:NEW.LINEITEM_APPLYINFUTURE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_APPLYINFUTURE',:OLD.LINEITEM_APPLYINFUTURE, :NEW.LINEITEM_APPLYINFUTURE,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_CODELST_CATEGORY,0) != NVL(:NEW.FK_CODELST_CATEGORY,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_CODELST_CATEGORY',:OLD.FK_CODELST_CATEGORY, :NEW.FK_CODELST_CATEGORY,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_TMID,' ') != NVL(:NEW.LINEITEM_TMID,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_TMID',:OLD.LINEITEM_TMID, :NEW.LINEITEM_TMID,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_CDM,' ') != NVL(:NEW.LINEITEM_CDM,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_CDM',:OLD.LINEITEM_CDM, :NEW.LINEITEM_CDM,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_APPLYINDIRECTS,' ') != NVL(:NEW.LINEITEM_APPLYINDIRECTS,' ') THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_APPLYINDIRECTS',:OLD.LINEITEM_APPLYINDIRECTS, :NEW.LINEITEM_APPLYINDIRECTS,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_TOTALCOST,0) != NVL(:NEW.LINEITEM_TOTALCOST,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_TOTALCOST',:OLD.LINEITEM_TOTALCOST, :NEW.LINEITEM_TOTALCOST,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_SPONSORAMOUNT,0) != NVL(:NEW.LINEITEM_SPONSORAMOUNT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_SPONSORAMOUNT',:OLD.LINEITEM_SPONSORAMOUNT, :NEW.LINEITEM_SPONSORAMOUNT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_VARIANCE,0) != NVL(:NEW.LINEITEM_VARIANCE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_VARIANCE',:OLD.LINEITEM_VARIANCE, :NEW.LINEITEM_VARIANCE,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_EVENT,0) != NVL(:NEW.FK_EVENT,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_EVENT',:OLD.FK_EVENT, :NEW.FK_EVENT,NULL,NULL);
      END IF;
      IF NVL(:OLD.LINEITEM_SEQ,0) != NVL(:NEW.LINEITEM_SEQ,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'LINEITEM_SEQ',:OLD.LINEITEM_SEQ, :NEW.LINEITEM_SEQ,NULL,NULL);
      END IF;
      IF NVL(:OLD.FK_CODELST_COST_TYPE,0) != NVL(:NEW.FK_CODELST_COST_TYPE,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'FK_CODELST_COST_TYPE',:OLD.FK_CODELST_COST_TYPE, :NEW.FK_CODELST_COST_TYPE,NULL,NULL);
      END IF;
      IF NVL(:OLD.SUBCOST_ITEM_FLAG,0) != NVL(:NEW.SUBCOST_ITEM_FLAG,0) THEN
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'SUBCOST_ITEM_FLAG',:OLD.SUBCOST_ITEM_FLAG, :NEW.SUBCOST_ITEM_FLAG,NULL,NULL);
      END IF;
    END IF;
END;
