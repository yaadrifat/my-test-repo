set define off;

--Sql for Table CB_CORD to add column PRE CBU Nucleated Cell Count Concentration.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'PRE_NUCL_CBU_CNT_CNCTRN'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add PRE_NUCL_CBU_CNT_CNCTRN number(10,2)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.PRE_NUCL_CBU_CNT_CNCTRN IS 'This Column will store the value of PRE CBU Nucleated Cell Count Concentration.'; 
commit;


--Sql for Table CB_CORD to add column Prep-Processing Testing Start Date.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'PRE_PRCSNG_START_DATE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add PRE_PRCSNG_START_DATE DATE');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.PRE_PRCSNG_START_DATE IS 'This Column will store the value of Pre Processing Start Date.'; 
commit;

--Sql for Table CB_CORD to add column COMP_CORD_ENTRY.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'COMP_CORD_ENTRY'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add COMP_CORD_ENTRY varchar2(1)');
  end if;
end;
/
COMMENT ON COLUMN CBB.COMP_CORD_ENTRY IS 'Flag to enable auto complete cord 
entry'; 
commit;


--Sql for Table CBB to add column COMP_ACK_RES_TASK.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'COMP_ACK_RES_TASK'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add COMP_ACK_RES_TASK varchar2(1)');
  end if;
end;
/
COMMENT ON COLUMN CBB.COMP_ACK_RES_TASK IS 'Flag to enable auto complete cord 
entry'; 
commit;


--Sql for Table CBB to add column FK_SHP_PWRK_LOC.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_SHP_PWRK_LOC'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add FK_SHP_PWRK_LOC number(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB.FK_SHP_PWRK_LOC IS 'Default papaer work shipping location'; 
commit;

--Sql for Table CBB to add column FK_SHP_PWRK_LOC.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'FK_CBU_PCK_ADD'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add FK_CBU_PCK_ADD number(10)');
  end if;
end;
/
COMMENT ON COLUMN CBB.FK_CBU_PCK_ADD IS 'Values reference from ER_CODELST'; 
commit;

--Sql for Table CBB to add column USE_OWN_DUMN.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'USE_OWN_DUMN'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add USE_OWN_DUMN varchar2(1 BYTE)');
  end if;
end;
/
COMMENT ON COLUMN CBB.USE_OWN_DUMN IS 'This column store the info that CBB using own declaration of urgent medical need'; 
commit;

--Sql for Table CBB to add column DOMESTIC_NMDP.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'DOMESTIC_NMDP'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add DOMESTIC_NMDP varchar2(1 BYTE)');
  end if;
end;
/
COMMENT ON COLUMN CBB.DOMESTIC_NMDP IS 'This column store the info that CBB is domestic or not'; 
commit;

--Sql for Table CBB to add column USE_FDOE.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'USE_FDOE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add USE_FDOE varchar2(1 BYTE)');
  end if;
end;
/
COMMENT ON COLUMN CBB.USE_FDOE IS 'This column store the info that CBB is FDOE or not'; 
commit;



--Sql for Table CB_CORD to add column FK_CBU_STOR_LOC
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FK_CBU_STOR_LOC'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FK_CBU_STOR_LOC number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FK_CBU_STOR_LOC IS 'Code Id for FK CBU Storage Location ID Id.'; 
commit;

--Sql for Table CB_CORD to add column FK_CBU_COLL_SITE
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FK_CBU_COLL_SITE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FK_CBU_COLL_SITE number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FK_CBU_COLL_SITE IS 'This field store FK of CBU Collection Site Id.'; 
commit;

--Sql for Table CB_CORD to add column FK_CBU_COLL_TYPE
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FK_CBU_COLL_TYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FK_CBU_COLL_TYPE number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FK_CBU_COLL_TYPE IS 'This field store FK of CBU Collection TYPE.'; 
commit;

--Sql for Table CB_CORD to add column FK_CBU_DEL_TYPE
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'FK_CBU_DEL_TYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CORD add FK_CBU_DEL_TYPE number(10,0)');
  end if;
end;
/
COMMENT ON COLUMN CB_CORD.FK_CBU_DEL_TYPE IS 'This field store FK of CBU Delivery TYPE.'; 
commit;

--Sql for Table CB_CORD to add column CB_NOTES.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_NOTES' AND column_name = 'COMMENTS'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_NOTES add COMMENTS VARCHAR2(25)');
  end if;
end;
/
COMMENT ON COLUMN CB_NOTES.COMMENTS IS 'This will store comment for note having flag for later'; 
commit;

--STARTS ADD THE COLUMN LIC_CREATOR TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'LIC_CREATOR'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (LIC_CREATOR  NUMBER(10,0))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN LIC_CREATOR TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.LIC_CREATOR  is 'Storing the licensure review creator ID.';
--end--

--STARTS ADD THE COLUMN LIC_CREATED_ON TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'LIC_CREATED_ON'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (LIC_CREATED_ON  DATE)';
  end if;
end;
/
--END--
--starts comment for THE COLUMN LIC_CREATED_ON TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.LIC_CREATED_ON  is 'Storing the licensure review created date.';
--end--



--STARTS ADD THE COLUMN ELIG_CREATOR TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'ELIG_CREATOR'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (ELIG_CREATOR  NUMBER(10,0))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN ELIG_CREATOR TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.ELIG_CREATOR  is 'Storing the ELIGIBILITY review creator ID.';
--end--

--STARTS ADD THE COLUMN ELIG_CREATED_ON TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'ELIG_CREATED_ON'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (ELIG_CREATED_ON  DATE)';
  end if;
end;
/
--END--
--starts comment for THE COLUMN ELIG_CREATED_ON TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.ELIG_CREATED_ON  is 'Storing the ELIGIBILITY review created date.';
--end--





--STARTS ADD THE COLUMN FINAL_REVIEW_CREATOR TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'FINAL_REVIEW_CREATOR'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (FINAL_REVIEW_CREATOR  NUMBER(10,0))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN FINAL_REVIEW_CREATOR TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.FINAL_REVIEW_CREATOR  is 'Storing the FINAL_REVIEW creator ID.';
--end--

--STARTS ADD THE COLUMN FINAL_REVIEW_CREATED_ON TO CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'FINAL_REVIEW_CREATED_ON'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW add (FINAL_REVIEW_CREATED_ON  DATE)';
  end if;
end;
/
--END--
--starts comment for THE COLUMN FINAL_REVIEW_CREATED_ON TO CB_CORD_FINAL_REVIEW TABLE--
comment on column CB_CORD_FINAL_REVIEW.FINAL_REVIEW_CREATED_ON  is 'Storing the FINAL_REVIEW created date.';
--end--



--STARTS Drop THE COLUMN FINAL_REVIEW_DATE from CB_CORD_FINAL_REVIEW--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'FINAL_REVIEW_DATE'; 

  if (v_column_exists = 1) then
      execute immediate 'alter table CB_CORD_FINAL_REVIEW drop column  FINAL_REVIEW_DATE';
  end if;
end;
/
--END--


--Sql for Table CBB to add column ATTENTION.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'ATTENTION'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB add ATTENTION varchar2(25 BYTE)');
  end if;
end;
/
COMMENT ON COLUMN CBB.ATTENTION IS 'This column store attention'; 
commit;


--STARTS ADDING COLUMN TO CB_CORD_COMPLETE_REQ_INFO TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_COMPLETE_REQ_INFO'
    AND column_name = 'COMPLETE_REQ_INFO_FLAG';
  if (v_column_exists = 0) then
	 execute immediate 'ALTER TABLE ERES.CB_CORD_COMPLETE_REQ_INFO ADD(COMPLETE_REQ_INFO_FLAG VARCHAR2(1 CHAR))';
  end if;
end;
/
--END--

commit;