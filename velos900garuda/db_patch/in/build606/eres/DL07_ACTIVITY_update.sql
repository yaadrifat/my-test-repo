--STARTS UPDATING RECORD FROM ACTIVITY TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ACTIVITY
    where ACTIVITY_NAME='ctshipmentbar';
  if (v_record_exists = 1) then
     UPDATE ACTIVITY SET ACTIVITY_DESC='Enter Shipment Info' WHERE ACTIVITY_NAME='ctshipmentbar';
	commit;
  end if;
end;
/
--END--