--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_01';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='A new CT - request has been requested for <CBU Registry ID>' WHERE codelst_type = 'alert'
    AND codelst_subtyp = 'alert_01';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_02';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='A new CT - request has been requested for <CBU Registry ID>' WHERE codelst_type = 'alert'
    AND codelst_subtyp = 'alert_02';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_03';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='A new HE - request has been requested for <CBU Registry ID>' WHERE codelst_type = 'alert'
    AND codelst_subtyp = 'alert_03';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_04';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='A new OR - request has been requested for <CBU Registry ID>' WHERE codelst_type = 'alert'
    AND codelst_subtyp = 'alert_04';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'all_ids'
    AND codelst_subtyp = 'isbt_din_id';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='ISBT Donation Identification Number' where codelst_type = 'all_ids'AND codelst_subtyp = 'isbt_din_id';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'num_of_bags'
    AND codelst_subtyp = '1_bag';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='1 bag' where codelst_type = 'num_of_bags'AND codelst_subtyp = '1_bag';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'num_of_bags'
    AND codelst_subtyp = '2_bags';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='2 bags' where codelst_type = 'num_of_bags'AND codelst_subtyp = '2_bags';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'incomplete'
    AND codelst_subtyp = 'phy_find_ass';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Physical Findings Assessment' where codelst_type = 'incomplete'AND codelst_subtyp = 'phy_find_ass';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'incomplete'
    AND codelst_subtyp = 'oth_med_rec';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Other Relevant Medical Records' where codelst_type = 'incomplete'AND codelst_subtyp = 'oth_med_rec';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'ineligible'
    AND codelst_subtyp = 'oth_med_rec';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Other Relevant Medical Records' where codelst_type = 'ineligible'AND codelst_subtyp = 'oth_med_rec';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_meth'
    AND codelst_subtyp = 'under_liq_nit';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Under Liquid Nitrogen' where codelst_type = 'storage_meth'AND codelst_subtyp = 'under_liq_nit';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_meth'
    AND codelst_subtyp = 'vpr_phse';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Vapor phase of liquid nitrogen' where codelst_type = 'storage_meth'AND codelst_subtyp = 'vpr_phse';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_meth'
    AND codelst_subtyp = 'vpr_phse';
  if (v_record_exists = 1) then
	delete from er_codelst where codelst_type='storage_meth' and codelst_subtyp='ele_freezer';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>-135';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='>=-135' where codelst_type = 'storage_temp'AND codelst_subtyp = '>-135';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>=-150to<=-135';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='>-150to<=-135' where codelst_type = 'storage_temp'AND codelst_subtyp = '>=-150to<=-135';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '<-150';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='<=-150' where codelst_type = 'storage_temp'AND codelst_subtyp = '<-150';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'hetrozygous �';
  if (v_record_exists = 1) then
	delete from er_codelst where codelst_type='hem_path_scrn' and codelst_subtyp='hetrozygous �';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_meth'
    AND codelst_subtyp = 'under_liq_nit';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'storage_meth'AND codelst_subtyp = 'under_liq_nit';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_meth'
    AND codelst_subtyp = 'vpr_phse';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'storage_meth'AND codelst_subtyp = 'vpr_phse';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'segment';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'samp_type'AND codelst_subtyp = 'segment';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'filtPap';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'samp_type'AND codelst_subtyp = 'filtPap';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'RBC';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'samp_type'AND codelst_subtyp = 'RBC';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'extDna';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'samp_type'AND codelst_subtyp = 'extDna';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'nonViab';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=5 where codelst_type = 'samp_type'AND codelst_subtyp = 'nonViab';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'plasAli';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=6 where codelst_type = 'samp_type'AND codelst_subtyp = 'plasAli';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'serAli';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=7 where codelst_type = 'samp_type'AND codelst_subtyp = 'serAli';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'samp_type'
    AND codelst_subtyp = 'viabCel';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=8 where codelst_type = 'samp_type'AND codelst_subtyp = 'viabCel';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'fung_cul'
    AND codelst_subtyp = 'posi';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'fung_cul'AND codelst_subtyp = 'posi';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'fung_cul'
    AND codelst_subtyp = 'negi';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'fung_cul'AND codelst_subtyp = 'negi';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'bact_cul'
    AND codelst_subtyp = 'posi';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'bact_cul'AND codelst_subtyp = 'posi';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'bact_cul'
    AND codelst_subtyp = 'negi';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'bact_cul'AND codelst_subtyp = 'negi';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'rh_type'
    AND codelst_subtyp = 'posi';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'rh_type'AND codelst_subtyp = 'posi';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'rh_type'
    AND codelst_subtyp = 'negi';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'rh_type'AND codelst_subtyp = 'negi';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'rh_type'
    AND codelst_subtyp = 'oth';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'rh_type'AND codelst_subtyp = 'oth';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'incomplete';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=5 where codelst_type = 'unlicensed'AND codelst_subtyp = 'incomplete';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'pre_lice';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=6 where codelst_type = 'unlicensed'AND codelst_subtyp = 'pre_lice';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'post_lice';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=7 where codelst_type = 'unlicensed'AND codelst_subtyp = 'post_lice';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'unlicensed'
    AND codelst_subtyp = 'not_applicable';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=8 where codelst_type = 'unlicensed'AND codelst_subtyp = 'not_applicable';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'frozen_in'
    AND codelst_subtyp = 'bags';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'frozen_in'AND codelst_subtyp = 'bags';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'frozen_in'
    AND codelst_subtyp = 'vials';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'frozen_in'AND codelst_subtyp = 'vials';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'frozen_in'
    AND codelst_subtyp = 'tubes';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'frozen_in'AND codelst_subtyp = 'tubes';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'frozen_in'
    AND codelst_subtyp = 'other';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'frozen_in'AND codelst_subtyp = 'other';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'freezer_manu'
    AND codelst_subtyp = 'bioarchive';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'freezer_manu'AND codelst_subtyp = 'bioarchive';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'freezer_manu'
    AND codelst_subtyp = 'MVE';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'freezer_manu'AND codelst_subtyp = 'MVE';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'freezer_manu'
    AND codelst_subtyp = 'tay_whar';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'freezer_manu'AND codelst_subtyp = 'tay_whar';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'freezer_manu'
    AND codelst_subtyp = 'forma_scienti';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'freezer_manu'AND codelst_subtyp = 'forma_scienti';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'freezer_manu'
    AND codelst_subtyp = 'other';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=5 where codelst_type = 'freezer_manu'AND codelst_subtyp = 'other';
	commit;
  end if;
end;
/
--END--





--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'normal';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'normal';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'hemo_trait';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'hemo_trait';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'beta_thalmia';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'beta_thalmia';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'sickle_beta';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'sickle_beta';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'homo_no_dis';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=5 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'homo_no_dis';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'hemo_sick_dis';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=6 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'hemo_sick_dis';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'alp_thal_sil';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=7 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'alp_thal_sil';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'alp_thal_trait';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=8 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'alp_thal_trait';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'alp_thal_sev';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=9 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'alp_thal_sev';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'mult_trait';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=10 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'mult_trait';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'not_done';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=11 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'not_done';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hem_path_scrn'
    AND codelst_subtyp = 'unknown';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=12 where codelst_type = 'hem_path_scrn'AND codelst_subtyp = 'unknown';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'minimum_decl'
    AND codelst_subtyp = 'meets';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'minimum_decl'AND codelst_subtyp = 'meets';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'minimum_decl'
    AND codelst_subtyp = 'doesntmeets';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'minimum_decl'AND codelst_subtyp = 'doesntmeets';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'timing_of_test'
    AND codelst_subtyp = 'pre_procesing';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'timing_of_test'AND codelst_subtyp = 'pre_procesing';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'timing_of_test'
    AND codelst_subtyp = 'post_procesing';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'timing_of_test'AND codelst_subtyp = 'post_procesing';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'timing_of_test'
    AND codelst_subtyp = 'post_proc_thaw';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'timing_of_test'AND codelst_subtyp = 'post_proc_thaw';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'timing_of_test'
    AND codelst_subtyp = 'new_test_date';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=4 where codelst_type = 'timing_of_test'AND codelst_subtyp = 'new_test_date';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'prod_modi'
    AND codelst_DESC = 'Plasma and RBC Reduced';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SUBTYP='plasma_rbc' where codelst_type = 'prod_modi'AND codelst_DESC = 'Plasma and RBC Reduced';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'prod_modi'
    AND CODELST_DESC = 'Red Cell Depletion Only';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Red Cell Depletion' where codelst_type = 'prod_modi'AND CODELST_DESC = 'Red Cell Depletion Only';
	commit;
  end if;
end;
/
--END--


--Processing--
--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'processing'
    AND codelst_subtyp = 'manual';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'processing'AND codelst_subtyp = 'manual';
	commit;
  end if;
end;
/
--END--
--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'processing'
    AND codelst_subtyp = 'automated';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'processing'AND codelst_subtyp = 'automated';
	commit;
  end if;
end;
/
--END--
--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'if_automated'
    AND codelst_subtyp = 'SEPAX';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'if_automated'AND codelst_subtyp = 'SEPAX';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'if_automated'
    AND codelst_subtyp = 'AXP';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'if_automated'AND codelst_subtyp = 'AXP';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'if_automated'
    AND codelst_subtyp = 'other';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'if_automated'AND codelst_subtyp = 'other';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'contr_freez'
    AND codelst_subtyp = 'yes';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'contr_freez'AND codelst_subtyp = 'yes';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'contr_freez'
    AND codelst_subtyp = 'no';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'contr_freez'AND codelst_subtyp = 'no';
	commit;
  end if;
end;
/
--END--

-- UPDATEING THE CODELIST VALUES --

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  codelst_type = 'test_source' AND codelst_subtyp = 'maternal';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Maternal'' where CODELST_TYPE=''test_source'' and CODELST_SUBTYP=''maternal''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  codelst_type = 'test_outcome' AND codelst_subtyp = 'not_done';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_SEQ = 4 where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP=''not_done''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  codelst_type = 'test_outcome' AND codelst_subtyp = 'indeter';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_SEQ = 3 where CODELST_TYPE=''test_outcome'' and CODELST_SUBTYP=''indeter''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  codelst_type = 'idmtest_subques' AND codelst_subtyp = 'some';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_SEQ = 2 where CODELST_TYPE=''idmtest_subques'' and CODELST_SUBTYP=''some''';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  codelst_type = 'idmtest_subques' AND codelst_subtyp = 'none';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_SEQ = 3 where CODELST_TYPE=''idmtest_subques'' and CODELST_SUBTYP=''none''';
  end if;
end;
/

commit;
--END--




