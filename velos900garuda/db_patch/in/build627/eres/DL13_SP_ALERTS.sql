create or replace
PROCEDURE SP_ALERTS(order_id number) AS
TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
cur_cv   CurTyp;  -- declare cursor variable
v_alertId number;
v_alert_title varchar2(4000);
v_alert_desc varchar2(4000);
v_alert_wording varchar2(4000);
v_alert_precondition varchar2(4000) :='';
v_col_name varchar2(4000);
v_alert_precond_cnt number :=0;
v_query varchar2(4000);
sql_fieldval varchar2(4000);
var_instance_cnt number;
v_userId number;
v_cordregid varchar2(255);
V_ORDERTYPE VARCHAR2(255);
v_sampleatlab varchar2(1);
V_ALERTNAME VARCHAR2(255);
v_resol number:=0;
v_resoldesc VARCHAR2(50 CHAR):='N/A';
v_orderId number;
v_schshipdate varchar2(50);
v_temp number;
BEGIN
select f_codelst_desc(order_type) into v_ordertype from er_order where pk_order=order_id;
FOR CUR_ALERTS IN(select pk_codelst,CODELST_SUBTYP,codelst_desc,codelst_custom_col1,codelst_custom_col from er_codelst where codelst_type in('alert','alert_resol') order by pk_codelst)
LOOP
  v_alertId := cur_alerts.pk_codelst;
  V_ALERTNAME:=cur_alerts.codelst_subtyp;
	v_alert_title := cur_alerts.codelst_desc;
	v_alert_desc := cur_alerts.codelst_custom_col1;
	v_alert_wording :=cur_alerts.codelst_custom_col;
  v_alert_precondition := getalertcondition(v_alertid,'PRE');
if v_alert_precondition<>' ' then
    dbms_output.put_line('alert Id :::'||v_alertid);
    dbms_output.put_line('alert pre condition is::'||v_alert_precondition);
   
    if v_alertname in('alert_01','alert_02','alert_03','alert_04','alert_15','alert_27') then
      v_query := 'select count(*) cnt from er_order where '||v_alert_precondition;
    elsif v_alertname in('alert_05','alert_06') then
      v_query := 'select count(*) cnt from er_order,er_order_header where er_order.fk_order_header=er_order_header.pk_order_header and '||v_alert_precondition;
    elsif v_alertname in('alert_07','alert_08','alert_09','alert_10','alert_11') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_shipment where er_order.fk_order_header=er_order_header.pk_order_header and cb_shipment.fk_order_id=er_order.pk_order and '||v_alert_precondition;
    elsif v_alertname='alert_19' then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and '||v_alert_precondition;
    elsif v_alertname IN('alert_23','alert_24','alert_25') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord,er_attachments,cb_upload_info where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and er_attachments.entity_id=cb_cord.pk_cord and er_attachments.entity_type=(select pk_codelst from er_codelst where codelst_type=''entity_type'' and codelst_subtyp=''CBU'') and er_attachments.pk_attachment=cb_upload_info.fk_attachmentid and '||v_alert_precondition;
    end if;
    v_query:=replace(v_query,'#keycolumn',order_id);
     dbms_output.put_line('^^^^^^^^^^^'||v_query);
    open cur_cv for v_query;
     LOOP
          FETCH cur_cv INTO sql_fieldval ;
          EXIT WHEN cur_cv%NOTFOUND;
           dbms_output.put_line('display result  ' || sql_fieldval );
           if sql_fieldval>0 then
            select nvl(assigned_to,0),nvl(crd.cord_registry_id,'N/A'),nvl(ord.order_sample_at_lab,'-'),nvl(ord.fk_order_resol_by_tc,0) into v_userid,v_cordregid,v_sampleatlab,v_resol from er_order ord,er_order_header ordhdr,cb_cord crd where ord.fk_order_header=ordhdr.pk_order_header and ordhdr.order_entityid=crd.pk_cord and pk_order=order_id;
            dbms_output.put_line('v_resol............'||v_resol);
           if v_resol<>0 then
           dbms_output.put_line('before getting resolution desc...............');
            select cbu_status_desc into v_resoldesc from cb_cbu_status where pk_cbu_status=v_resol;
            dbms_output.put_line('after getting resolution desc...............'||v_resoldesc);
           end if;
           select ord.pk_order,nvl(to_char(ship.sch_shipment_date,'Mon DD, YYYY'),'N/A') into v_temp,v_schshipdate from er_order ord left outer join (select fk_order_id,sch_shipment_date from cb_shipment where fk_order_id=order_id and fk_shipment_type=(select pk_codelst from er_codelst where codelst_type='shipment_type' and codelst_subtyp='CBU Shipment')) ship on(ship.fk_order_id=ord.pk_order) where ord.pk_order=order_id;
           v_alert_wording:=replace(v_alert_wording,'<CBU Registry ID>',v_cordregid);
           v_alert_wording:=replace(v_alert_wording,'<Request Type>',v_ordertype);
           v_alert_wording:=replace(v_alert_wording,'<Resolution Code>',v_resoldesc);
           v_alert_wording:=replace(v_alert_wording,'<Shipment Scheduled Date>',v_schshipdate);
           if v_sampleatlab='Y' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Lab');
           elsif v_sampleatlab='N' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Ship');
           end if;
           select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=order_id and alrt_inst.fk_userid=v_userid and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify');
                dbms_output.put_line('var_instance_cnt:::::'||var_instance_cnt);
                if var_instance_cnt=0 then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,order_id,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER'),v_userid,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify'),v_alert_wording);
                end if;
            for usrs in(select altr.FK_USER,altr.EMAIL_FLAG email_flag,altr.NOTIFY_FLAG notify_flag,altr.PROMPT_FLAG prompt_flag,addr.add_email mail from cb_user_alerts altr,er_user usr,er_add addr where altr.fk_user=usr.pk_user and usr.fk_peradd=addr.pk_add and fk_codelst_alert=v_alertid)
            loop
              dbms_output.put_line('users that has assigned alert is:::'||usrs.fk_user);
              if usrs.email_flag=1 then
              select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=order_id and alrt_inst.entity_type=(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER') and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail');
              if var_instance_cnt=0 then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,order_id,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER'),usrs.fk_user,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail'),usrs.mail,v_alert_wording);
              dbms_output.put_line('Email alerts implemented');
                insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
              end if;
              end if;
              end loop;
              commit;
           end if;
      END LOOP;
    dbms_output.put_line('alert wording is::'||v_alert_wording);
end if;
END LOOP;
end;
/


CREATE TABLE CB_ALERTS_MAINTENANCE(ENTITY_ID NUMBER(10,0),ENTITY_TYPE NUMBER(10,0));
COMMENT ON TABLE CB_ALERTS_MAINTENANCE IS 'uses to create alert notifications';



CREATE OR REPLACE TRIGGER "ERES"."ER_ORDER_ALERTS"
AFTER INSERT OR UPDATE ON ERES.ER_ORDER
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
INSERT INTO CB_ALERTS_MAINTENANCE(ENTITY_ID,ENTITY_TYPE) VALUES(:NEW.PK_ORDER,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='ORDER'));
END;
/

CREATE OR REPLACE TRIGGER "ERES"."CB_CORD_ALERTS" AFTER INSERT OR UPDATE ON ERES.CB_CORD
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
INSERT INTO CB_ALERTS_MAINTENANCE(ENTITY_ID,ENTITY_TYPE) VALUES(:NEW.PK_CORD,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU'));
end;
/

create or replace
PROCEDURE        "CB_ALERTS_MAINTENANCE_PROC" AS
  V_ENTITY_ID NUMBER;
  V_ENTITY_TYPE NUMBER;
  V_ENTITY_CORD NUMBER;
  V_ENTITY_ORDER NUMBER;
BEGIN
FOR ALERTS IN(SELECT ENTITY_ID,ENTITY_TYPE FROM CB_ALERTS_MAINTENANCE)
LOOP
V_ENTITY_TYPE:=ALERTS.ENTITY_TYPE;
V_ENTITY_ID:=ALERTS.ENTITY_ID;
dbms_output.put_line('V_ENTITY_TYPE:::'||V_ENTITY_TYPE);
dbms_output.put_line('V_ENTITY_ID:::'||V_ENTITY_ID);
SELECT PK_CODELST INTO V_ENTITY_ORDER FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='ORDER';
SELECT PK_CODELST INTO V_ENTITY_CORD FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU';
IF V_ENTITY_TYPE=V_ENTITY_ORDER THEN
dbms_output.put_line('order id:::'||V_ENTITY_ID);
  SP_ALERTS(V_ENTITY_ID);
  delete from cb_alerts_maintenance where entity_id=V_ENTITY_ID;
END IF;
IF V_ENTITY_TYPE=V_ENTITY_CORD THEN
dbms_output.put_line('cord id:::'||V_ENTITY_ID);
FOR ORDERS IN(SELECT PK_ORDER FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND ORDHDR.ORDER_ENTITYID=V_ENTITY_ID AND ORDHDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU'))
LOOP
dbms_output.put_line('ORDERS.PK_ORDER:::'||ORDERS.PK_ORDER);
  SP_ALERTS(ORDERS.PK_ORDER);
END LOOP;
delete from cb_alerts_maintenance where entity_id=V_ENTITY_ID;
END IF;
END LOOP;
END CB_ALERTS_MAINTENANCE_PROC;
/


begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_WORKFLOW_JOB');
 end;
 /


begin
DBMS_SCHEDULER.create_job (
    job_name        => 'Create_Workflow_job',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'begin sp_workflow_temp;CB_ALERTS_MAINTENANCE_PROC;end;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=SECONDLY;INTERVAL=10',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to create workflow.');
end;
/


begin
dbms_scheduler.run_job('CREATE_WORKFLOW_JOB');
end;
/







CREATE OR REPLACE PROCEDURE SP_UPDATE_CORD_STATUS IS
v_pk_cbu_status_av NUMBER;
v_pk_cbu_status_ro NUMBER;
v_sysdate DATE := sysdate;
BEGIN
--dbms_output.put_line('v_sysdate:::::::::'||v_sysdate);

select pk_cbu_status into v_pk_cbu_status_av from cb_cbu_status where cbu_status_code='AV';
select pk_cbu_status into v_pk_cbu_status_ro from cb_cbu_status where cbu_status_code='RO' and code_type='Response';
--dbms_output.put_line('v_pk_cbu_status::::::'||v_pk_cbu_status_av);
for cords in (select pk_cord from cb_cord where to_char(cord_avail_date,'DD-MON-YY')=to_char(v_sysdate,'DD-MON-YY') and fk_cord_cbu_status=v_pk_cbu_status_ro)
loop
  --dbms_output.put_line('pk_cord is::::'||cords.pk_cord);
  update cb_cord set FK_CORD_CBU_STATUS=v_pk_cbu_status_av where pk_cord=cords.pk_cord;
 END LOOP;

END SP_UPDATE_CORD_STATUS;
/


begin
DBMS_SCHEDULER.create_job (
    job_name        => 'UPDATE_CORD_STATUS_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'begin SP_UPDATE_CORD_STATUS;FOR ORDERS IN(SELECT PK_ORDER FROM ER_ORDER ORD WHERE ORD.ORDER_STATUS NOT IN(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_status'' and CODELST_SUBTYP IN(''close_ordr'',''CANCELLED''))) LOOP SP_ALERTS(ORDERS.PK_ORDER);END LOOP;end;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=daily;byhour=00;byminute=00;bysecond=1',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to update cord status.');
end;
/

begin
dbms_scheduler.run_job('UPDATE_CORD_STATUS_JOB');
end;
/

 
