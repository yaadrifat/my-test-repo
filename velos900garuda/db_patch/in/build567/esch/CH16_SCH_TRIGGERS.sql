CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_BI0" BEFORE INSERT ON EVENT_ASSOC
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;
BEGIN
 BEGIN
	v_new_creator := :NEW.creator;
	SELECT TO_CHAR(pk_user) ||', ' || trim(usr_lastname) ||', ' ||trim(usr_firstname)
	INTO usr FROM er_user WHERE pk_user = v_new_creator ;
	EXCEPTION WHEN NO_DATA_FOUND THEN
	USR := 'New User' ;
	END ;
	SELECT TRUNC(seq_rid.NEXTVAL)
	INTO erid FROM dual;
	:NEW.rid := erid ;
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
-- Added by Ganapathy on 06/23/05 for Audit inserting
--modified: JM
  insert_data:=:NEW.DURATION_UNIT||'|'|| :NEW.FK_VISIT||'|'|| :NEW.EVENT_ID||'|'||
	:NEW.CHAIN_ID||'|'||:NEW.EVENT_TYPE||'|'||:NEW.NAME||'|'||:NEW.NOTES||'|'||
	:NEW.COST||'|'||:NEW.COST_DESCRIPTION||'|'|| :NEW.DURATION||'|'|| :NEW.USER_ID||'|'||
	:NEW.LINKED_URI||'|'||:NEW.FUZZY_PERIOD||'|'||:NEW.MSG_TO||'|'||
	:NEW.DESCRIPTION||'|'|| :NEW.DISPLACEMENT||'|'|| :NEW.ORG_ID||'|'||:NEW.EVENT_MSG||'|'||
	:NEW.EVENT_RES||'|'|| :NEW.EVENT_FLAG||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	:NEW.PAT_DAYSBEFORE||'|'|| :NEW.PAT_DAYSAFTER||'|'|| :NEW.USR_DAYSBEFORE||'|'||
	:NEW.USR_DAYSAFTER||'|'||:NEW.PAT_MSGBEFORE||'|'||:NEW.PAT_MSGAFTER||'|'||
	:NEW.USR_MSGBEFORE||'|'||:NEW.USR_MSGAFTER||'|'|| TO_CHAR(:NEW.STATUS_DT,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	:NEW.STATUS_CHANGEBY||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||
	:NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	:NEW.IP_ADD||'|'||
	:NEW.ORIG_STUDY ||'|'||:NEW.ORIG_CAL ||'|'||:NEW.ORIG_EVENT ||'|'||
	:NEW.EVENT_CPTCODE||'|'||:NEW.EVENT_FUZZYAFTER||'|'||:NEW.EVENT_DURATIONAFTER||'|'||
	:NEW.EVENT_DURATIONBEFORE||'|'||:NEW.FK_CATLIB||'|'||:NEW.EVENT_CALASSOCTO||'|'||
	:NEW.EVENT_CALSCHDATE ||'|'|| :NEW.EVENT_CATEGORY ||'|'|| :NEW.EVENT_LIBRARY_TYPE ||'|'|| :NEW.EVENT_LINE_CATEGORY ||'|'||
	:NEW.SERVICE_SITE_ID ||'|'|| :NEW.FACILITY_ID ||'|'|| :NEW.FK_CODELST_COVERTYPE||'|'|| :NEW.COVERAGE_NOTES||'|'||
	:NEW.FK_CODELST_CALSTAT; 

	audit_trail.record_transaction(raid, 'EVENT_ASSOC', erid, 'I', USR );
	INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


create or replace
TRIGGER "ESCH"."EVENT_ASSOC_AU0" AFTER UPDATE ON ESCH.EVENT_ASSOC FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);

BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);

	audit_trail.record_transaction
		(raid, 'EVENT_ASSOC', :OLD.rid, 'U', usr);

	IF NVL(:OLD.event_id,0) !=
		NVL(:NEW.event_id,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_ID', :OLD.event_id, :NEW.event_id);
	END IF;
	IF NVL(:OLD.chain_id,0) !=
		NVL(:NEW.chain_id,0) THEN
		audit_trail.column_update
		(raid, 'CHAIN_ID', :OLD.chain_id, :NEW.chain_id);
	END IF;
	IF NVL(:OLD.event_type,' ') !=
		NVL(:NEW.event_type,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_TYPE', :OLD.event_type, :NEW.event_type);
	END IF;
	IF NVL(:OLD.NAME,' ') !=
		NVL(:NEW.NAME,' ') THEN
		audit_trail.column_update
		(raid, 'NAME', :OLD.NAME, :NEW.NAME);
	END IF;
	IF NVL(:OLD.notes,' ') !=
		NVL(:NEW.notes,' ') THEN
		audit_trail.column_update
		(raid, 'NOTES', :OLD.notes, :NEW.notes);
	END IF;
	IF NVL(:OLD.COST,0) !=
		NVL(:NEW.COST,0) THEN
		audit_trail.column_update
		(raid, 'COST', :OLD.COST, :NEW.COST);
	END IF;
	IF NVL(:OLD.cost_description,' ') !=
		NVL(:NEW.cost_description,' ') THEN
		audit_trail.column_update
		(raid, 'COST_DESCRIPTION', :OLD.cost_description, :NEW.cost_description);
	END IF;
	IF NVL(:OLD.duration,0) !=
		NVL(:NEW.duration,0) THEN
		audit_trail.column_update
		(raid, 'DURATION', :OLD.duration, :NEW.duration);
	END IF;
	IF NVL(:OLD.user_id,0) !=
		NVL(:NEW.user_id,0) THEN
		audit_trail.column_update
		(raid, 'USER_ID', :OLD.user_id, :NEW.user_id);
	END IF;
	IF NVL(:OLD.linked_uri,' ') !=
		NVL(:NEW.linked_uri,' ') THEN
		audit_trail.column_update
		(raid, 'LINKED_URI', :OLD.linked_uri, :NEW.linked_uri);
	END IF;
	IF NVL(:OLD.fuzzy_period,' ') !=
		NVL(:NEW.fuzzy_period,' ') THEN
		audit_trail.column_update
		(raid, 'FUZZY_PERIOD', :OLD.fuzzy_period, :NEW.fuzzy_period);
	END IF;
	IF NVL(:OLD.msg_to,' ') !=
		NVL(:NEW.msg_to,' ') THEN
		audit_trail.column_update
		(raid, 'MSG_TO', :OLD.msg_to, :NEW.msg_to);
	END IF;
	/*IF NVL(:OLD.status,' ') !=
		NVL(:NEW.status,' ') THEN
		audit_trail.column_update
		(raid, 'STATUS', :OLD.status, :NEW.status);
	END IF;*/
	IF NVL(:OLD.description,' ') !=
		NVL(:NEW.description,' ') THEN
		audit_trail.column_update
		(raid, 'DESCRIPTION', :OLD.description, :NEW.description);
	END IF;
	IF NVL(:OLD.displacement,0) !=
		NVL(:NEW.displacement,0) THEN
		audit_trail.column_update
		(raid, 'DISPLACEMENT', :OLD.displacement, :NEW.displacement);
	END IF;
	IF NVL(:OLD.org_id,0) !=
		NVL(:NEW.org_id,0) THEN
		audit_trail.column_update
		(raid, 'ORG_ID', :OLD.org_id, :NEW.org_id);
	END IF;

	--JM: added

	IF NVL(:OLD.orig_cal,0) !=
		NVL(:NEW.orig_cal,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_CAL', :OLD.orig_cal, :NEW.orig_cal);
	END IF;

	IF NVL(:OLD.orig_event,0) !=
		NVL(:NEW.orig_event,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_EVENT', :OLD.orig_event, :NEW.orig_event);
	END IF;

	IF NVL(:OLD.orig_study,0) !=
		NVL(:NEW.orig_study,0) THEN
		audit_trail.column_update
		(raid, 'ORIG_STUDY', :OLD.orig_study, :NEW.orig_study);
	END IF;
	--

	IF NVL(:OLD.event_msg,' ') !=
		NVL(:NEW.event_msg,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_MSG', :OLD.event_msg, :NEW.event_msg);
	END IF;
	IF NVL(:OLD.event_res,' ') !=
		NVL(:NEW.event_res,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_RES', :OLD.event_res, :NEW.event_res);
	END IF;

	IF NVL(:OLD.event_flag,0) !=
		NVL(:NEW.event_flag,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_FLAG', :OLD.event_flag, :NEW.event_flag);
	END IF;
	IF NVL(:OLD.pat_daysbefore,0) !=
		NVL(:NEW.pat_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSBEFORE', :OLD.pat_daysbefore, :NEW.pat_daysbefore);
	END IF;
	IF NVL(:OLD.pat_daysafter,0) !=
		NVL(:NEW.pat_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSAFTER', :OLD.pat_daysafter, :NEW.pat_daysafter);
	END IF;
	IF NVL(:OLD.usr_daysbefore,0) !=
		NVL(:NEW.usr_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSBEFORE', :OLD.usr_daysbefore, :NEW.usr_daysbefore);
	END IF;
	IF NVL(:OLD.usr_daysafter,0) !=
		NVL(:NEW.usr_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSAFTER', :OLD.usr_daysafter, :NEW.usr_daysafter);
	END IF;
	IF NVL(:OLD.pat_msgbefore,' ') !=
		NVL(:NEW.pat_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGBEFORE', :OLD.pat_msgbefore, :NEW.pat_msgbefore);
	END IF;
	IF NVL(:OLD.pat_msgafter,' ') !=
		NVL(:NEW.pat_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGAFTER', :OLD.pat_msgafter, :NEW.pat_msgafter);
	END IF;
	IF NVL(:OLD.usr_msgbefore,' ') !=
		NVL(:NEW.usr_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGBEFORE', :OLD.usr_msgbefore, :NEW.usr_msgbefore);
	END IF;
	IF NVL(:OLD.usr_msgafter,' ') !=
		NVL(:NEW.usr_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGAFTER', :OLD.usr_msgafter, :NEW.usr_msgafter);
	END IF;
	IF NVL(:OLD.status_dt,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.status_dt,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'STATUS_DT', to_char(:old.status_dt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.status_dt, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.status_changeby,0) !=
		NVL(:NEW.status_changeby,0) THEN
		audit_trail.column_update
		(raid, 'STATUS_CHANGEBY', :OLD.status_changeby, :NEW.status_changeby);
	END IF;
	IF NVL(:OLD.rid,0) !=
		NVL(:NEW.rid,0) THEN
		audit_trail.column_update
		(raid, 'RID',:OLD.rid, :NEW.rid);
	END IF;
	IF NVL(:OLD.last_modified_by,0) !=
		NVL(:NEW.last_modified_by,0) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_BY', :OLD.last_modified_by, :NEW.last_modified_by);
	END IF;
	IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_DATE', to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.ip_add,' ') !=
		NVL(:NEW.ip_add,' ') THEN
		audit_trail.column_update
		(raid, 'IP_ADD', :OLD.ip_add, :NEW.ip_add);
	END IF;
	IF NVL(:OLD.duration_unit,' ') !=
		NVL(:NEW.duration_unit,' ') THEN
		audit_trail.column_update
		(raid, 'DURATION_UNIT', :OLD.duration_unit, :NEW.duration_unit);
	END IF;
	IF NVL(:OLD.fk_visit,0) !=
		NVL(:NEW.fk_visit,0) THEN
		audit_trail.column_update
		(raid, 'FK_VISIT', :OLD.fk_visit, :NEW.fk_visit);
	END IF;
	IF NVL(:OLD.event_cptcode,' ') !=
		NVL(:NEW.event_cptcode,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CPTCODE', :OLD.event_cptcode, :NEW.event_cptcode);
	END IF;
	IF NVL(:OLD.event_fuzzyafter,' ') !=
		NVL(:NEW.event_fuzzyafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_FUZZYAFTER', :OLD.event_fuzzyafter, :NEW.event_fuzzyafter);
	END IF;
	IF NVL(:OLD.event_durationafter,' ') !=
		NVL(:NEW.event_durationafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONAFTER', :OLD.event_durationafter, :NEW.event_durationafter);
	END IF;
	IF NVL(:OLD.event_durationbefore,' ') !=
		NVL(:NEW.event_durationbefore,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONBEFORE', :OLD.event_durationbefore, :NEW.event_durationbefore);
	END IF;
	--JM : changed    IF NVL(:OLD.fk_catlib,' ') !=
	IF NVL(:OLD.fk_catlib,0) !=
		NVL(:NEW.fk_catlib,0) THEN
		audit_trail.column_update
		(raid, 'FK_CATLIB', :OLD.fk_catlib, :NEW.fk_catlib);
	END IF;
	IF NVL(:OLD.event_calassocto,' ') !=
		NVL(:NEW.event_calassocto,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CALASSOCTO', :OLD.event_calassocto, :NEW.event_calassocto);
	END IF;
	--JM: IF NVL(:OLD.event_calschdate,' ') !=
	IF NVL(:OLD.event_calschdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.event_calschdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'EVENT_CALSCHDATE', to_char(:old.event_calschdate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.event_calschdate, PKG_DATEUTIL.F_GET_DATEFORMAT) );
	END IF;

	--KM-28Mar08
	IF NVL(:OLD.EVENT_CATEGORY,' ') !=
		NVL(:NEW.EVENT_CATEGORY,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CATEGORY', :OLD.EVENT_CATEGORY, :NEW.EVENT_CATEGORY);
	END IF;

	-- KM - 21Aug09
	IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) !=
		NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LIBRARY_TYPE',:OLD.EVENT_LIBRARY_TYPE, :NEW.EVENT_LIBRARY_TYPE);
	END IF;

	IF NVL(:OLD.EVENT_LINE_CATEGORY,0) !=
		NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LINE_CATEGORY',:OLD.EVENT_LINE_CATEGORY, :NEW.EVENT_LINE_CATEGORY);
	END IF;
---------------------------------------
	IF NVL(:OLD.SERVICE_SITE_ID,0) !=
		NVL(:NEW.SERVICE_SITE_ID,0) THEN
		audit_trail.column_update
		(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
	END IF;

	IF NVL(:OLD.FACILITY_ID,0) !=
		NVL(:NEW.FACILITY_ID,0) THEN
		audit_trail.column_update
		(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
	END IF;

	IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
		NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
	END IF;  
  IF NVL(:OLD.COVERAGE_NOTES,0) !=                      --BK 23rd july 2010
		NVL(:NEW.COVERAGE_NOTES,0) THEN
		audit_trail.column_update
		(raid, 'COVERAGE_NOTES',:OLD.COVERAGE_NOTES, :NEW.COVERAGE_NOTES);
	END IF;
	--JM: 07FEB2011, #D-FIN9
	IF NVL(:OLD.FK_CODELST_CALSTAT,0) !=
		NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_CALSTAT', :OLD.FK_CODELST_CALSTAT, :NEW.FK_CODELST_CALSTAT);
	END IF;
END;
/
CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AD0" AFTER DELETE ON EVENT_ASSOC
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob; --KM

begin
	select seq_audit.nextval into raid from dual;

	audit_trail.record_transaction
		(raid, 'EVENT_ASSOC', :old.rid, 'D');

	deleted_data :=
		to_char(:old.event_id) || '|' ||
		to_char(:old.chain_id) || '|' ||
		:old.event_type || '|' ||
		:old.name || '|' ||
		:old.notes || '|' ||
		to_char(:old.cost) || '|' ||
		:old.cost_description || '|' ||
		to_char(:old.duration) || '|' ||
		to_char(:old.user_id) || '|' ||
		:old.linked_uri || '|' ||
		:old.fuzzy_period || '|' ||
		:old.msg_to || '|' ||
		--:old.status || '|' ||
		:old.description || '|' ||
		to_char(:old.displacement) || '|' ||
		to_char(:old.org_id) || '|' ||
		:old.event_msg || '|' ||
		:old.event_res || '|' ||
		to_char(:old.created_on) || '|' ||
		to_char(:old.event_flag) || '|' ||
		to_char(:old.pat_daysbefore) || '|' ||
		to_char(:old.pat_daysafter) || '|' ||
		to_char(:old.usr_daysbefore) || '|' ||
		to_char(:old.usr_daysafter) || '|' ||
		:old.pat_msgbefore || '|' ||
		:old.pat_msgafter || '|' ||
		:old.usr_msgbefore || '|' ||
		:old.usr_msgafter || '|' ||
		to_char(:old.status_dt) || '|' ||
		to_char(:old.status_changeby) || '|' ||
		to_char(:old.rid) || '|' ||
		to_char(:old.creator) || '|' ||
		to_char(:old.last_modified_by) || '|' ||
		to_char(:old.last_modified_date) || '|' ||
		:old.ip_add  || '|' ||
		:old.orig_study ||'|'||
		:old.orig_cal ||'|'||
		:old.orig_event ||'|'||
		:old.duration_unit || '|' ||
		to_char(:old.fk_visit) || '|' ||
		:old.event_cptcode || '|' ||
		:old.event_fuzzyafter || '|' ||
		:old.event_durationafter || '|' ||
		:old.event_durationbefore || '|' ||
		:old.fk_catlib || '|' ||
		:old.event_calassocto || '|' ||
		to_char(:old.event_calschdate) || '|' ||
		--KM-28Mar08
		:old.event_category || '|' ||
		to_char(:old.EVENT_LIBRARY_TYPE) || '|' ||
		to_char(:old.EVENT_LINE_CATEGORY) || '|' ||--KM: 21Aug09
		to_char(:old.SERVICE_SITE_ID) ||'|'|| to_char(:old.FACILITY_ID) ||'|'|| to_char(:old.FK_CODELST_COVERTYPE) ||'|'||
		to_char(:old.COVERAGE_NOTES)||'|'|| 
		:old.FK_CODELST_CALSTAT;

	insert into audit_delete
	(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));
end;
/


CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AI1" AFTER INSERT ON EVENT_ASSOC
FOR EACH ROW
DECLARE v_checkactive NUMBER(1) := 0;
         v_study NUMBER;
v_return number;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_ASSOC_AI1', pLEVEL  => Plog.LFATAL);

BEGIN
 IF UPPER(trim(:NEW.event_type)) = 'P' THEN

   INSERT INTO SCH_PROTSTAT (
     PK_PROTSTAT    ,
     FK_EVENT       ,
     --PROTSTAT       ,
     PROTSTAT_DT    ,
     PROTSTAT_BY    ,
     PROTSTAT_NOTE  ,
     CREATOR        , -- Amarnadh
     IP_ADD,
     FK_CODELST_CALSTAT
     )
     VALUES
    (sch_protstat_seq.NEXTVAL,
    :NEW.event_id ,
    --:NEW.status ,
    :NEW.status_dt ,
    :NEW.status_changeby ,
    :NEW.notes ,
    :NEW.creator ,
    :NEW.ip_Add,
    :NEW.FK_CODELST_CALSTAT ) ;


   -- by sonia sahni
   -- insert default notifications if study is already enrolling
   v_study := :NEW.CHAIN_ID;

	/*CCF-FIN1 User will specify the budget template to be used for creating default budget
	Removed PKG_BGT.sp_createProtocolBudget call from here Moved code to Java.
	This is done explicitely as Library calendars will not have default budgets anymore.
	The only place the procedure will be called from is study setup calendars
	*/
END IF;

if (:NEW.event_type='A') then
	begin
	    PKG_BGT.sp_addToProtocolBudget( :NEW.event_id,'S',:NEW.chain_id, :NEW.creator , :NEW.ip_Add , v_return , 0 ,:NEW.fk_visit,
		:NEW.name,:NEW.event_cptcode,:NEW.event_line_category,:NEW.description,:NEW.event_sequence);
	    --, to be added after Manimaran's patch - new.event_line_category
	end;
 end if;

END ;
/


CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AU1" AFTER UPDATE ON EVENT_ASSOC
FOR EACH ROW
declare
v_return number;
v_pk_lineitem number;

BEGIN
IF  (:OLD.FK_CODELST_CALSTAT <> :NEW.FK_CODELST_CALSTAT) THEN
 INSERT INTO SCH_PROTSTAT (
    PK_PROTSTAT    ,
 FK_EVENT       ,
 --PROTSTAT       ,
 PROTSTAT_DT    ,
 PROTSTAT_BY    ,
 PROTSTAT_NOTE  ,
 CREATOR,   ---Amarnadh
 IP_ADD, FK_CODELST_CALSTAT )
        VALUES
 (sch_protstat_seq.NEXTVAL,
  :NEW.event_id ,
  --:NEW.status ,
  :NEW.status_dt ,
  :NEW.status_changeby ,
  :NEW.notes ,
  :NEW.creator ,
  :NEW.ip_Add, :NEW.FK_CODELST_CALSTAT ) ;
END IF;
    --set default notifications for the protocol
 /*  if ( (:old.status <> :new.status) and :new.status = 'A') then
       pkg_alnot.set_alnot(NULL, :new.CHAIN_ID, :new.EVENT_ID, 'G' , :new.creator, sysdate, :new.ip_add);
  end if; */


 --for default budget:
if (:old.event_type='A' and nvl(:old.fk_visit,0) <> nvl(:NEW.fk_visit,0) and nvl(:old.displacement,-1) <> 0 ) then
    PKG_BGT.sp_addToProtocolBudget( :NEW.event_id,'S',:NEW.chain_id, :NEW.creator , :NEW.ip_Add , v_return , 0 ,:NEW.fk_visit,
    :NEW.name,:NEW.event_cptcode,:NEW.event_line_category,:NEW.description,:NEW.event_sequence);
end if;
begin
    -- get default budget lineitem info for this event
    if (:old.event_type='A' and ( (:NEW.name <> :old.name) or  ( nvl(:NEW.description,' ') <> nvl(:old.description,' ') ) or
       ( nvl(:NEW.event_cptcode,' ') <> nvl(:old.event_cptcode,' ') )  or
       ( nvl(:NEW.event_line_category,0) <> nvl(:old.event_line_category,0) )
       or( nvl(:NEW.event_sequence,0) <> nvl(:old.event_sequence,0) ))
    )then -- only for events
		--update the lineitem
        update sch_lineitem
        set last_modified_by = :NEW.last_modified_by ,last_modified_date = sysdate, IP_ADD = :NEW.ip_add,
        LINEITEM_NAME =  :NEW.name,LINEITEM_DESC= :NEW.description,LINEITEM_CPTCODE = :NEW.event_cptcode,
        lineitem_seq = :NEW.event_sequence
        where pk_lineitem in (select pk_lineitem
        from sch_lineitem l
        where l.fk_event = :old.event_id and l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_bgtcal =
        (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.chain_id 
        and nvl(BGTCAL_DELFLAG,'Z') <>'Y' and fk_budget = (
        select  pk_budget from sch_budget where budget_calendar = :old.chain_id
        and nvl(BUDGET_DELFLAG,'Z') <>'Y')
        )
        ) ) ;
    end if;
	exception when no_data_found then
    	v_pk_lineitem := 0;
end;
END ;
/


CREATE OR REPLACE TRIGGER "ESCH"."EVENT_DEF_BI0" BEFORE INSERT ON EVENT_DEF
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;

BEGIN
 BEGIN
  v_new_creator := :NEW.creator;
  SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
p('USR2' || USR);
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
p(':new.rid' || :NEW.rid);
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
p('raid' || raid);
  audit_trail.record_transaction
    (raid, 'EVENT_DEF', erid, 'I', USR );
-- Added by Ganapathy on 06/23/05 for Audit insert
insert_data:=:NEW.CALENDAR_SHAREDWITH||'|'||:NEW.DURATION_UNIT||'|'||
    :NEW.FK_VISIT||'|'|| :NEW.EVENT_ID||'|'|| :NEW.CHAIN_ID||'|'||:NEW.EVENT_TYPE||'|'||
    :NEW.NAME||'|'||:NEW.NOTES||'|'|| :NEW.COST||'|'||:NEW.COST_DESCRIPTION||'|'||
    :NEW.DURATION||'|'|| :NEW.USER_ID||'|'||:NEW.LINKED_URI||'|'||:NEW.FUZZY_PERIOD||'|'||
    :NEW.MSG_TO||'|'||:NEW.DESCRIPTION||'|'|| :NEW.DISPLACEMENT||'|'||
    :NEW.ORG_ID||'|'||:NEW.EVENT_MSG||'|'||:NEW.EVENT_RES||'|'|| :NEW.EVENT_FLAG||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.PAT_DAYSBEFORE||'|'||:NEW.PAT_DAYSAFTER||'|'||
    :NEW.USR_DAYSBEFORE||'|'|| :NEW.USR_DAYSAFTER||'|'||:NEW.PAT_MSGBEFORE||'|'||
    :NEW.PAT_MSGAFTER||'|'||:NEW.USR_MSGBEFORE||'|'||:NEW.USR_MSGAFTER||'|'||
    :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
     TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||
    :NEW.EVENT_FUZZYAFTER||'|'||:NEW.EVENT_DURATIONAFTER||'|'||	:NEW.EVENT_CPTCODE||'|'||:NEW.EVENT_DURATIONBEFORE ||'|'||
    :NEW.FK_CATLIB ||'|'|| :NEW.EVENT_CATEGORY ||'|'|| :NEW.EVENT_LIBRARY_TYPE ||'|'|| :NEW.EVENT_LINE_CATEGORY ||'|'||
    :NEW.SERVICE_SITE_ID ||'|'|| :NEW.FACILITY_ID ||'|'|| :NEW.FK_CODELST_COVERTYPE ||'|'|| :NEW.COVERAGE_NOTES||'|'||
    :NEW.FK_CODELST_CALSTAT; 

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


CREATE OR REPLACE TRIGGER EVENT_DEF_AU0 AFTER UPDATE ON EVENT_DEF FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);

	audit_trail.record_transaction
	(raid, 'EVENT_DEF', :OLD.rid, 'U', usr );

	IF NVL(:OLD.event_id,0) !=
		NVL(:NEW.event_id,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_ID',:OLD.event_id, :NEW.event_id);
	END IF;
	IF NVL(:OLD.chain_id,0) !=
		NVL(:NEW.chain_id,0) THEN
		audit_trail.column_update
		(raid, 'CHAIN_ID',:OLD.chain_id, :NEW.chain_id);
	END IF;
	IF NVL(:OLD.event_type,' ') !=
		NVL(:NEW.event_type,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_TYPE',:OLD.event_type, :NEW.event_type);
	END IF;
	IF NVL(:OLD.NAME,' ') !=
		NVL(:NEW.NAME,' ') THEN
		audit_trail.column_update
		(raid, 'NAME',:OLD.NAME, :NEW.NAME);
	END IF;
	IF NVL(:OLD.notes,' ') !=
		NVL(:NEW.notes,' ') THEN
		audit_trail.column_update
		(raid, 'NOTES',:OLD.notes, :NEW.notes);
	END IF;
	IF NVL(:OLD.COST,0) !=
		NVL(:NEW.COST,0) THEN
		audit_trail.column_update
		(raid, 'COST',:OLD.COST, :NEW.COST);
	END IF;
	IF NVL(:OLD.cost_description,' ') !=
		NVL(:NEW.cost_description,' ') THEN
		audit_trail.column_update
		(raid, 'COST_DESCRIPTION',:OLD.cost_description, :NEW.cost_description);
	END IF;
	IF NVL(:OLD.duration,0) !=
		NVL(:NEW.duration,0) THEN
		audit_trail.column_update
		(raid, 'DURATION',:OLD.duration, :NEW.duration);
	END IF;
	IF NVL(:OLD.user_id,0) !=
		NVL(:NEW.user_id,0) THEN
		audit_trail.column_update
		(raid, 'USER_ID',:OLD.user_id, :NEW.user_id);
	END IF;
	IF NVL(:OLD.linked_uri,' ') !=
		NVL(:NEW.linked_uri,' ') THEN
		audit_trail.column_update
		(raid, 'LINKED_URI',:OLD.linked_uri, :NEW.linked_uri);
	END IF;
	IF NVL(:OLD.fuzzy_period,' ') !=
		NVL(:NEW.fuzzy_period,' ') THEN
		audit_trail.column_update
		(raid, 'FUZZY_PERIOD',:OLD.fuzzy_period, :NEW.fuzzy_period);
	END IF;
	IF NVL(:OLD.msg_to,' ') !=
		NVL(:NEW.msg_to,' ') THEN
		audit_trail.column_update
		(raid, 'MSG_TO',:OLD.msg_to, :NEW.msg_to);
	END IF;
	/*
	--JM: 07Feb2011: #D-FIN9
	IF NVL(:OLD.status,' ') !=
		NVL(:NEW.status,' ') THEN
		audit_trail.column_update
		(raid, 'STATUS',:OLD.status, :NEW.status);
	END IF;*/
	IF NVL(:OLD.description,' ') !=
		NVL(:NEW.description,' ') THEN
		audit_trail.column_update
		(raid, 'DESCRIPTION',:OLD.description, :NEW.description);
	END IF;
	IF NVL(:OLD.displacement,0) !=
		NVL(:NEW.displacement,0) THEN
		audit_trail.column_update
		(raid, 'DISPLACEMENT',:OLD.displacement, :NEW.displacement);
	END IF;
	IF NVL(:OLD.org_id,0) !=
		NVL(:NEW.org_id,0) THEN
		audit_trail.column_update
		(raid, 'ORG_ID',:OLD.org_id, :NEW.org_id);
	END IF;
	IF NVL(:OLD.event_msg,' ') !=
		NVL(:NEW.event_msg,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_MSG',:OLD.event_msg, :NEW.event_msg);
	END IF;
	IF NVL(:OLD.event_res,' ') !=
		NVL(:NEW.event_res,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_RES',:OLD.event_res, :NEW.event_res);
	END IF;
	IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'CREATED_ON',to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.event_flag,0) !=
		NVL(:NEW.event_flag,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_FLAG',:OLD.event_flag, :NEW.event_flag);
	END IF;
	IF NVL(:OLD.pat_daysbefore,0) !=
		NVL(:NEW.pat_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSBEFORE',:OLD.pat_daysbefore, :NEW.pat_daysbefore);
	END IF;
	IF NVL(:OLD.pat_daysafter,0) !=
		NVL(:NEW.pat_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'PAT_DAYSAFTER',:OLD.pat_daysafter, :NEW.pat_daysafter);
	END IF;
	IF NVL(:OLD.usr_daysbefore,0) !=
		NVL(:NEW.usr_daysbefore,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSBEFORE',:OLD.usr_daysbefore, :NEW.usr_daysbefore);
	END IF;
	IF NVL(:OLD.usr_daysafter,0) !=
		NVL(:NEW.usr_daysafter,0) THEN
		audit_trail.column_update
		(raid, 'USR_DAYSAFTER',:OLD.usr_daysafter, :NEW.usr_daysafter);
	END IF;
	IF NVL(:OLD.pat_msgbefore,' ') !=
		NVL(:NEW.pat_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGBEFORE',:OLD.pat_msgbefore, :NEW.pat_msgbefore);
	END IF;
	IF NVL(:OLD.pat_msgafter,' ') !=
		NVL(:NEW.pat_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'PAT_MSGAFTER',:OLD.pat_msgafter, :NEW.pat_msgafter);
	END IF;
	IF NVL(:OLD.usr_msgbefore,' ') !=
		NVL(:NEW.usr_msgbefore,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGBEFORE',:OLD.usr_msgbefore, :NEW.usr_msgbefore);
	END IF;
	IF NVL(:OLD.usr_msgafter,' ') !=
		NVL(:NEW.usr_msgafter,' ') THEN
		audit_trail.column_update
		(raid, 'USR_MSGAFTER',:OLD.usr_msgafter, :NEW.usr_msgafter);
	END IF;
	IF NVL(:OLD.rid,0) !=
		NVL(:NEW.rid,0) THEN
		audit_trail.column_update
		(raid, 'RID',:OLD.rid, :NEW.rid);
	END IF;
	IF NVL(:OLD.last_modified_by,0) !=
		NVL(:NEW.last_modified_by,0) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_BY',:OLD.last_modified_by, :NEW.last_modified_by);
	END IF;
	IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
		NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
		audit_trail.column_update
		(raid, 'LAST_MODIFIED_DATE',to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
	END IF;
	IF NVL(:OLD.ip_add,' ') !=
		NVL(:NEW.ip_add,' ') THEN
		audit_trail.column_update
		(raid, 'IP_ADD',:OLD.ip_add, :NEW.ip_add);
	END IF;

--JM: newly added
	IF NVL(:OLD.calendar_sharedwith,' ') !=
		NVL(:NEW.calendar_sharedwith,' ') THEN
		audit_trail.column_update
		(raid, 'CALENDAR_SHAREDWITH',:OLD.calendar_sharedwith, :NEW.calendar_sharedwith);
	END IF;

	IF NVL(:OLD.duration_unit,' ') !=
		NVL(:NEW.duration_unit,' ') THEN
		audit_trail.column_update
		(raid, 'DURATION_UNIT',:OLD.duration_unit, :NEW.duration_unit);
	END IF;

	IF NVL(:OLD.fk_visit,0) !=
		NVL(:NEW.fk_visit,0) THEN
		audit_trail.column_update
		(raid, 'FK_VISIT',:OLD.fk_visit, :NEW.fk_visit);
	END IF;
------------

	IF NVL(:OLD.event_fuzzyafter,' ') !=
		NVL(:NEW.event_fuzzyafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_FUZZYAFTER',:OLD.event_fuzzyafter, :NEW.event_fuzzyafter);
	END IF;

	IF NVL(:OLD.event_durationafter,' ') !=
		NVL(:NEW.event_durationafter,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONAFTER',:OLD.event_durationafter, :NEW.event_durationafter);
	END IF;

	IF NVL(:OLD.event_cptcode,' ') !=
		NVL(:NEW.event_cptcode,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CPTCODE',:OLD.event_cptcode, :NEW.event_cptcode);
	END IF;

	IF NVL(:OLD.event_durationbefore,' ') !=
		NVL(:NEW.event_durationbefore,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_DURATIONBEFORE',:OLD.event_durationbefore, :NEW.event_durationbefore);
	END IF;

	IF NVL(:OLD.fk_catlib,0) !=
		NVL(:NEW.fk_catlib,0) THEN
		audit_trail.column_update
		(raid, 'FK_CATLIB',:OLD.fk_catlib, :NEW.fk_catlib);
	END IF;
-- Added by Manimaran for Enh#C7
	IF NVL(:OLD.EVENT_CATEGORY,' ') !=
		NVL(:NEW.EVENT_CATEGORY,' ') THEN
		audit_trail.column_update
		(raid, 'EVENT_CATEGORY',:OLD.EVENT_CATEGORY, :NEW.EVENT_CATEGORY);
	END IF;
-- KM - 07Aug09
	IF NVL(:OLD.EVENT_LIBRARY_TYPE,0) !=
		NVL(:NEW.EVENT_LIBRARY_TYPE,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LIBRARY_TYPE',:OLD.EVENT_LIBRARY_TYPE, :NEW.EVENT_LIBRARY_TYPE);
	END IF;

	IF NVL(:OLD.EVENT_LINE_CATEGORY,0) !=
		NVL(:NEW.EVENT_LINE_CATEGORY,0) THEN
		audit_trail.column_update
		(raid, 'EVENT_LINE_CATEGORY',:OLD.EVENT_LINE_CATEGORY, :NEW.EVENT_LINE_CATEGORY);
	END IF;
---------------------------------------
	IF NVL(:OLD.SERVICE_SITE_ID,0) !=
		NVL(:NEW.SERVICE_SITE_ID,0) THEN
		audit_trail.column_update
		(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
	END IF;

	IF NVL(:OLD.FACILITY_ID,0) !=
		NVL(:NEW.FACILITY_ID,0) THEN
		audit_trail.column_update
		(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
	END IF;

	IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
		NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
	END IF;
  
  --BK 23rd july 2010
    IF NVL(:OLD.COVERAGE_NOTES,0) !=
		NVL(:NEW.COVERAGE_NOTES,0) THEN
		audit_trail.column_update
		(raid, 'COVERAGE_NOTES',:OLD.COVERAGE_NOTES, :NEW.COVERAGE_NOTES);
	END IF;
--JM: 07Feb: #D-FIN9	
	IF NVL(:OLD.FK_CODELST_CALSTAT,0) !=
		NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
		audit_trail.column_update
		(raid, 'FK_CODELST_CALSTAT',:OLD.FK_CODELST_CALSTAT, :NEW.FK_CODELST_CALSTAT);
	END IF;
	
END;
/

CREATE OR REPLACE TRIGGER "ESCH"."EVENT_DEF_AD0" AFTER DELETE ON EVENT_DEF
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob; --KM

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'EVENT_DEF', :old.rid, 'D');

  deleted_data :=
   to_char(:old.event_id) || '|' ||
   to_char(:old.chain_id) || '|' ||
   :old.event_type || '|' ||
   :old.name || '|' ||
   :old.notes || '|' ||
   to_char(:old.cost) || '|' ||
   :old.cost_description || '|' ||
   to_char(:old.duration) || '|' ||
   to_char(:old.user_id) || '|' ||
   :old.linked_uri || '|' ||
   :old.fuzzy_period || '|' ||
   :old.msg_to || '|' ||
   --:old.status || '|' ||
   :old.description || '|' ||
   to_char(:old.displacement) || '|' ||
   to_char(:old.org_id) || '|' ||
   :old.event_msg || '|' ||
   :old.event_res || '|' ||
   to_char(:old.created_on) || '|' ||
   to_char(:old.event_flag) || '|' ||
   to_char(:old.pat_daysbefore) || '|' ||
   to_char(:old.pat_daysafter) || '|' ||
   to_char(:old.usr_daysbefore) || '|' ||
   to_char(:old.usr_daysafter) || '|' ||
   :old.pat_msgbefore || '|' ||
   :old.pat_msgafter || '|' ||
   :old.usr_msgbefore || '|' ||
   :old.usr_msgafter || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   :old.ip_add|| '|' ||
   :old.calendar_sharedwith||'|'||
   :old.duration_unit||'|'||
   :old.fk_visit||'|'||
--JM: 02/15/2007
   :old.event_fuzzyafter|| '|' ||
   :old.event_durationafter|| '|' ||
   :old.event_cptcode|| '|' ||
   :old.event_durationbefore|| '|' ||
   to_char(:old.fk_catlib) || '|' ||
   --KM-Mar28,08
   :old.event_category || '|' ||
   to_char(:old.EVENT_LIBRARY_TYPE) || '|' ||
   to_char(:old.EVENT_LINE_CATEGORY) || '|' || --KM: 07Aug09
   to_char(:old.SERVICE_SITE_ID) ||'|'||
   to_char(:old.FACILITY_ID) ||'|'||
   to_char(:old.FK_CODELST_COVERTYPE)||'|'||  --BK 23rd july 2010
   to_char(:old.COVERAGE_NOTES)||'|'||   
   to_char(:old.FK_CODELST_CALSTAT);

insert into audit_delete
(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));
end;
/


CREATE OR REPLACE TRIGGER "SCH_PROTSTAT_BI0"
BEFORE INSERT
ON SCH_PROTSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;
BEGIN
 BEGIN
   v_new_creator := :NEW.creator ;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_PROTSTAT', erid, 'I', USR );


      --   Added by Ganapathy on 06/23/05 for Audit insert
    insert_data:= :NEW.PK_PROTSTAT||'|'|| :NEW.FK_EVENT||'|'||
    TO_CHAR(:NEW.PROTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.PROTSTAT_BY||'|'||:NEW.PROTSTAT_NOTE||'|'||
    :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
     TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     :NEW.IP_ADD||'|'||:NEW.FK_CODELST_CALSTAT;
     INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


CREATE OR REPLACE TRIGGER SCH_PROTSTAT_AU0
AFTER UPDATE
ON SCH_PROTSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_PROTSTAT', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_protstat,0) !=
      NVL(:NEW.pk_protstat,0) THEN
      audit_trail.column_update
        (raid, 'PK_PROTSTAT',
        :OLD.pk_protstat, :NEW.pk_protstat);
   END IF;
   IF NVL(:OLD.fk_event,0) !=
      NVL(:NEW.fk_event,0) THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   /*IF NVL(:OLD.protstat,' ') !=
      NVL(:NEW.protstat,' ') THEN
      audit_trail.column_update
        (raid, 'PROTSTAT',
        :OLD.protstat, :NEW.protstat);
   END IF;*/
   IF NVL(:OLD.protstat_dt,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.protstat_dt,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'PROTSTAT_DT',
        to_char(:OLD.protstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.protstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.protstat_by,0) !=
      NVL(:NEW.protstat_by,0) THEN
      audit_trail.column_update
        (raid, 'PROTSTAT_BY',
        :OLD.protstat_by, :NEW.protstat_by);
   END IF;
   IF NVL(:OLD.protstat_note,' ') !=
      NVL(:NEW.protstat_note,' ') THEN
      audit_trail.column_update
        (raid, 'PROTSTAT_NOTE',
        :OLD.protstat_note, :NEW.protstat_note);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
             to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
             to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   --JM: 07FEB2011, #D-FIN9
   IF NVL(:OLD.FK_CODELST_CALSTAT,0) !=
      NVL(:NEW.FK_CODELST_CALSTAT,0) THEN
      audit_trail.column_update
        (raid, 'FK_CODELST_CALSTAT',
        :OLD.FK_CODELST_CALSTAT, :NEW.FK_CODELST_CALSTAT);
   END IF;

END;
/


CREATE OR REPLACE TRIGGER "SCH_PROTSTAT_AD0" 
AFTER DELETE
ON SCH_PROTSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_PROTSTAT', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_protstat) || '|' ||
   to_char(:old.fk_event) || '|' ||
   --:old.protstat || '|' ||
   to_char(:old.protstat_dt) || '|' ||
   to_char(:old.protstat_by) || '|' ||
   :old.protstat_note || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add|| '|' ||
   to_char(:old.FK_CODELST_CALSTAT) ;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


CREATE OR REPLACE TRIGGER "SCH_PROTSTAT_AI0" 
AFTER INSERT
ON SCH_PROTSTAT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;

v_codelst_id number; --JM: 07FEB2011, #D-FIN9

/******************************************************************************
   NAME:       ESCH.SCH_PROTSTAT_AI0
   PURPOSE:    To Reset the mail notification

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2/23/2007             1. Created this trigger.

   NOTES:

******************************************************************************/
BEGIN
   tmpVar := 0;
   
   
   select PK_CODELST into v_codelst_id from sch_codelst where CODELST_TYPE='calStatStd' and CODELST_SUBTYP='D'; 
   
--IF NVL(:NEW.PROTSTAT,' ') ='D'  THEN
IF NVL(:NEW.FK_CODELST_CALSTAT,0) =v_codelst_id  THEN
   UPDATE SCH_MSGTRAN SET msg_status=-2 WHERE fk_protocol =:NEW.fk_event;
   END IF;

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/