set define off;

CREATE OR REPLACE PACKAGE BODY        "PKG_ALNOT" 
AS
PROCEDURE set_alnot(
 p_patprot IN NUMBER ,
 p_study IN NUMBER ,
 p_protocol IN NUMBER,
 p_globflag IN VARCHAR2,
 p_creator IN NUMBER ,
 p_created_on IN DATE ,
 p_ipadd IN VARCHAR2)
AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 02/01/2002
   ** insert alnot settings for a study / patient enrolled in a study according to the parameters
   ** modified on 02/05/2002 : insert a global setting record for the study
*/
BEGIN
INSERT INTO SCH_ALERTNOTIFY
 (PK_ALNOT, --1
  FK_PATPROT , --2
  FK_CODELST_AN  , --3
  ALNOT_TYPE ,
  ALNOT_USERS ,
  ALNOT_MOBEVE ,
  ALNOT_FLAG ,
  CREATOR,  --- 8
  CREATED_ON ,
  IP_ADD,
  FK_STUDY,
  FK_PROTOCOL,
  ALNOT_GLOBALFLAG)
(SELECT
 SEQ_SCH_ALERTNOTIFY.NEXTVAL,
 p_patprot ,
 c.PK_CODELST,
 'A',
 NULL,
 NULL,
 '0' , --
  p_creator ,
  SYSDATE,
  p_ipadd ,
  p_study ,
  p_protocol,
  p_globflag
 FROM SCH_CODELST c
WHERE c.codelst_type = 'sch_alert'
);
--set a global setting record for this study/protocol
INSERT INTO SCH_ALERTNOTIFY
 (PK_ALNOT, --1
  ALNOT_TYPE ,
  ALNOT_FLAG ,
  CREATOR,  --- 8
  CREATED_ON ,
  IP_ADD,
  FK_STUDY,
  FK_PROTOCOL,
  ALNOT_GLOBALFLAG)
  VALUES ( SEQ_SCH_ALERTNOTIFY.NEXTVAL,
  'G',
  '1' , -- default to lock the settings, by default the lock will be on
  p_creator ,
  SYSDATE,
  p_ipadd ,
  p_study ,
  p_protocol,
  p_globflag);
END set_alnot;
PROCEDURE set_study_alnot( p_study IN NUMBER ,
 p_creator IN NUMBER ,
 p_created_on IN DATE ,
 p_ipadd IN VARCHAR2)
AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 02/01/2002
   ** insert alnot settings for a study , for each active protocol
*/
v_protocolid NUMBER;
v_codelst_id NUMBER;
BEGIN

  select PK_CODELST into v_codelst_id from sch_codelst where CODELST_TYPE='calStatStd' and CODELST_SUBTYP='A'; 

  FOR  i IN (SELECT EVENT_ID FROM EVENT_ASSOC
    WHERE  CHAIN_ID = P_STUDY AND UPPER(EVENT_TYPE) IN('p','P')
	AND EVENT_CALASSOCTO='P'
    --AND UPPER(status) = 'A' ORDER BY NAME)--JM: 09FEB2011,  #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS from EVENT_ASSOC table
    AND FK_CODELST_CALSTAT = v_codelst_id ORDER BY NAME)

    LOOP
      v_protocolid := i.event_id;
	 --patprotid will be null
	 set_alnot(NULL, p_study, v_protocolid, 'N' , p_creator, p_created_on, p_ipadd);
    END LOOP ;
 END set_study_alnot;
 PROCEDURE apply_alnot(
 p_study IN NUMBER ,
 p_protocol IN NUMBER,
 p_lastmodified_by IN NUMBER ,
 p_lastmodified_on IN DATE ,
 p_ipadd IN VARCHAR2)
AS
/**************************************************************************************************
   **
   ** Author: Sonia Sahni 02/05/2002
   ** update alnot settings for a study.
   ** updates alnot settings for patients registered in the study/protocol according to the flag
*/
v_patprot NUMBER;
v_globflag VARCHAR(2);
BEGIN
BEGIN
SELECT ALNOT_GLOBALFLAG
INTO v_globflag
FROM SCH_ALERTNOTIFY
WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND FK_PATPROT IS NULL AND ALNOT_TYPE = 'G' ;
P(v_globflag );
EXCEPTION WHEN NO_DATA_FOUND THEN v_globflag := 'N';
P(v_globflag );
END;
P(v_globflag );
UPDATE SCH_ALERTNOTIFY -- common for settings - GR, G, N, NR
SET ALNOT_GLOBALFLAG = v_globflag ,
last_modified_by = p_lastmodified_by ,last_modified_date = SYSDATE , ip_add =  p_ipadd
WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND FK_PATPROT IS NULL AND ALNOT_TYPE <> 'G' ;
UPDATE SCH_CRFNOTIFY
SET CRFNOT_GLOBALFLAG = v_globflag ,
last_modified_by = p_lastmodified_by ,last_modified_date = SYSDATE , ip_add =  p_ipadd
WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND FK_PATPROT IS NULL ;
IF v_globflag = 'GR'  OR v_globflag = 'NR'  THEN -- update settings for old registration records- GR, NR
  UPDATE SCH_ALERTNOTIFY s
  SET ALNOT_GLOBALFLAG = v_globflag ,
  last_modified_by = p_lastmodified_by ,last_modified_date = p_lastmodified_on , ip_add =  p_ipadd
  WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND FK_PATPROT IS NOT NULL AND
  EXISTS ( SELECT * FROM ER_PATPROT e
                WHERE e.FK_PROTOCOL = s.FK_PROTOCOL AND
                e.FK_STUDY = s.FK_STUDY AND
          	 e.PK_PATPROT = s.FK_PATPROT
                AND  e.PATPROT_STAT = 1 AND e.PATPROT_ENROLDT IS NOT NULL);
 UPDATE SCH_CRFNOTIFY s
 SET CRFNOT_GLOBALFLAG = v_globflag ,
 last_modified_by = p_lastmodified_by ,last_modified_date = SYSDATE , ip_add =  p_ipadd
 WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND FK_PATPROT IS NOT NULL AND
  EXISTS ( SELECT * FROM ER_PATPROT e
                WHERE e.FK_PROTOCOL = s.FK_PROTOCOL AND
                e.FK_STUDY = s.FK_STUDY AND
          	 e.PK_PATPROT = s.FK_PATPROT
                AND  e.PATPROT_STAT = 1 AND e.PATPROT_ENROLDT IS NOT NULL);
 END IF; -- endif for GR,NR
IF v_globflag = 'G' THEN --update previous registrations alnot settings with global settings,
                        -- set study's global settings
DELETE FROM SCH_ALERTNOTIFY s
WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND
FK_PATPROT IS NOT NULL AND
EXISTS ( SELECT * FROM ER_PATPROT e
        WHERE e.FK_PROTOCOL = s.FK_PROTOCOL AND
	   e.FK_STUDY = s.FK_STUDY AND
	   e.PK_PATPROT = s.FK_PATPROT
	   AND  e.PATPROT_STAT = 1 AND e.PATPROT_ENROLDT IS NOT NULL);  --delete all alnot settings for previous registrations;
-- also check for active registrations/patprots  only
-- delete old crf notifications for
DELETE FROM SCH_CRFNOTIFY s
WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND
FK_PATPROT IS NOT NULL AND
EXISTS ( SELECT * FROM ER_PATPROT e
        WHERE e.FK_PROTOCOL = s.FK_PROTOCOL AND
	   e.FK_STUDY = s.FK_STUDY AND
	   e.PK_PATPROT = s.FK_PATPROT
	   AND  e.PATPROT_STAT = 1 AND e.PATPROT_ENROLDT IS NOT NULL);
-- copy all settings for this study and protocol for each active registartion/patprot
FOR i IN (SELECT PK_PATPROT FROM ER_PATPROT WHERE FK_STUDY = p_study AND
          FK_PROTOCOL = p_protocol AND  PATPROT_STAT = 1 AND PATPROT_ENROLDT IS NOT NULL)
		LOOP
                 v_patprot := i.PK_PATPROT;
                 INSERT INTO SCH_ALERTNOTIFY  (PK_ALNOT, --1
                             FK_PATPROT , --2
                             FK_CODELST_AN  , --3
                             ALNOT_TYPE ,
                             ALNOT_USERS ,
                             ALNOT_MOBEVE ,
                             ALNOT_FLAG ,
                             CREATOR,  --- 8
                             CREATED_ON ,
                             IP_ADD,
                             FK_STUDY,
                             FK_PROTOCOL,
                             ALNOT_GLOBALFLAG)
                (SELECT      SEQ_SCH_ALERTNOTIFY.NEXTVAL,
                             v_patprot ,
                             FK_CODELST_AN  , --3
                             ALNOT_TYPE ,
                             ALNOT_USERS ,
                             ALNOT_MOBEVE ,
                             ALNOT_FLAG ,
					    p_lastmodified_by ,
                             p_lastmodified_on ,
                             p_ipadd ,
                             FK_STUDY,
                             FK_PROTOCOL,
                             ALNOT_GLOBALFLAG
                          FROM SCH_ALERTNOTIFY
                         WHERE FK_STUDY = p_study AND
                          FK_PROTOCOL = p_protocol AND fk_patprot IS NULL AND ALNOT_TYPE <> 'G'
);
-- copy all crfnotify settings for this study and protocol for each active registartion/patprot
 INSERT INTO SCH_CRFNOTIFY( PK_CRFNOT, FK_CRF,
                           FK_CODELST_NOTSTAT , CRFNOT_USERSTO ,
                           CRFNOT_NOTES , CREATOR, CREATED_ON ,
                           IP_ADD , FK_STUDY,  FK_PROTOCOL,
                           FK_PATPROT , CRFNOT_GLOBALFLAG  )
			   ( SELECT SEQ_SCH_CRFNOTIFY.NEXTVAL,
                     	  FK_CRF ,
                           FK_CODELST_NOTSTAT ,
                           CRFNOT_USERSTO ,
                           CRFNOT_NOTES ,
                           p_lastmodified_by ,
                           p_lastmodified_on ,
                           p_ipadd ,
                           FK_STUDY,
                           FK_PROTOCOL ,
                           v_patprot ,
                           CRFNOT_GLOBALFLAG FROM
           			  SCH_CRFNOTIFY
					  WHERE FK_STUDY = p_study AND
                          FK_PROTOCOL = p_protocol AND fk_patprot IS NULL );
		END LOOP;
END IF; -- endif for settings = 'G'
IF v_globflag = 'N' THEN
-- delete notification settings for existing registrations for this study and protocol
       DELETE FROM SCH_ALERTNOTIFY s
       WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND
	  ALNOT_TYPE = 'N' AND
       FK_PATPROT IS NOT NULL AND
       EXISTS ( SELECT * FROM ER_PATPROT e
                WHERE e.FK_PROTOCOL = s.FK_PROTOCOL AND
                e.FK_STUDY = s.FK_STUDY AND
          	 e.PK_PATPROT = s.FK_PATPROT
                AND  e.PATPROT_STAT = 1 AND e.PATPROT_ENROLDT IS NOT NULL);  --delete all alnot settings for previous registrations;
-- also check for active registrations/patprots  only
-- update records for alerts to reset ALNOT_FLAG to 0 : the defualt
     UPDATE SCH_ALERTNOTIFY s
	SET ALNOT_FLAG = 0,  ALNOT_GLOBALFLAG = 'N',  ALNOT_USERS = NULL,
 ALNOT_MOBEVE = NULL,
    last_modified_by = p_lastmodified_by ,last_modified_date = p_lastmodified_on , ip_add =  p_ipadd
     WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND
     ALNOT_TYPE ='A' AND FK_PATPROT IS NOT NULL AND
     EXISTS ( SELECT * FROM ER_PATPROT e
                WHERE e.FK_PROTOCOL = s.FK_PROTOCOL AND
                e.FK_STUDY = s.FK_STUDY AND
          	 e.PK_PATPROT = s.FK_PATPROT
                AND  e.PATPROT_STAT = 1 AND e.PATPROT_ENROLDT IS NOT NULL);
    --Delete existing crfnotifications
      DELETE FROM SCH_CRFNOTIFY s
      WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND
	 FK_PATPROT IS NOT NULL AND
      EXISTS ( SELECT * FROM ER_PATPROT e
                WHERE e.FK_PROTOCOL = s.FK_PROTOCOL AND
                e.FK_STUDY = s.FK_STUDY AND
          	 e.PK_PATPROT = s.FK_PATPROT
                AND  e.PATPROT_STAT = 1 AND e.PATPROT_ENROLDT IS NOT NULL);  --delete all alnot settings for previous registrations;
END IF; -- endif for settings = 'N'
COMMIT;
END apply_alnot;
PROCEDURE update_study_alnot(
 p_study IN NUMBER ,
 p_protocol IN NUMBER,
 p_lastmodified_by IN NUMBER ,
 p_ipadd IN VARCHAR2,
 p_globflag IN VARCHAR2,
 p_lockvalue IN NUMBER)
 AS
 /**************************************************************************************************
   **
   ** Author: Sonia Sahni 02/06/2002
   ** update alnot global setting record for a study.
 p_lockvalue : when value is 0 edit lock if off. user will be able to modify global settings
 for a study. when value is 1 edit lock is on, user will not be able to edit global settings.
 by default the lock will be on. In order to apply the defined alnot settings for the study
 to study/patients registered the lock must be turned on
*/
v_patprot NUMBER;
v_globflag VARCHAR(2);
BEGIN
UPDATE SCH_ALERTNOTIFY -- common for settings - GR, G, N, NR
SET ALNOT_GLOBALFLAG = p_globflag , ALNOT_FLAG = p_lockvalue ,
last_modified_by = p_lastmodified_by ,last_modified_date = SYSDATE , ip_add =  p_ipadd
WHERE FK_STUDY = p_study AND FK_PROTOCOL = p_protocol AND FK_PATPROT IS NULL AND ALNOT_TYPE = 'G' ;
P('*********updated' );
IF p_lockvalue = 1 THEN --apply settings according to global_flag
P('*********in if' );
apply_alnot( p_study, p_protocol, p_lastmodified_by, SYSDATE, p_ipadd );
-- change for new requirements, update refresh flag column in tabe event_Assoc
 UPDATE EVENT_ASSOC
 SET ORIG_CAL = 0
 WHERE  CHAIN_ID = p_study  AND
 TO_NUMBER(EVENT_ID) = p_protocol;
END IF;
P('*********end');
COMMIT;
END update_study_alnot;
PROCEDURE copy_crfnotify ( p_FK_CRF IN NUMBER ,
                           p_FK_CODELST_NOTSTAT     IN NUMBER ,
                           p_CRFNOT_USERSTO IN VARCHAR2,
                           p_CREATOR   IN NUMBER ,
                           p_IP_ADD  IN VARCHAR2,
                           p_FK_STUDY IN NUMBER ,
                           p_FK_PROTOCOL IN NUMBER ,
                           p_FK_PATPROT   IN NUMBER ,
                           p_CRFNOT_GLOBALFLAG IN VARCHAR2
 )
  AS
 /**************************************************************************************************
   **
   ** Author: Sonia Sahni 02/27/2002
   **  get crf number for fk_crf
       get all CRF IDs which have same CRF number for given fk_patprot
      copy the same crf notify setting for others
 */
 v_pkcrf NUMBER;
 v_crfnum VARCHAR(50);
 BEGIN
 IF (p_FK_CRF > 0) THEN --in case 'All' is not selected from add notification
    SELECT CRF_NUMBER
    INTO v_crfnum
    FROM SCH_CRF
    WHERE pk_crf = p_FK_CRF;
    FOR i IN (SELECT pk_crf FROM SCH_CRF
            WHERE FK_PATPROT = p_FK_PATPROT
            AND CRF_NUMBER = v_crfnum AND pk_crf <> p_FK_CRF)
     	LOOP
             v_pkcrf := i.pk_crf;
     	 INSERT INTO SCH_CRFNOTIFY (PK_CRFNOT,
                                 FK_CRF,
                                 FK_CODELST_NOTSTAT ,
                                 CRFNOT_USERSTO,
                                 CREATOR ,
                                 CREATED_ON             ,
                                 IP_ADD ,
                                 FK_STUDY,
                                 FK_PROTOCOL,
                                 FK_PATPROT ,
                                 CRFNOT_GLOBALFLAG)
              VALUES (      SEQ_SCH_CRFNOTIFY.NEXTVAL,
                     	  v_pkcrf ,
                           p_FK_CODELST_NOTSTAT ,
                           p_CRFNOT_USERSTO ,
                           p_CREATOR ,
					  SYSDATE,
                           p_IP_ADD ,
                           p_FK_STUDY,
                           p_FK_PROTOCOL ,
                           p_FK_PATPROT  ,
                           p_CRFNOT_GLOBALFLAG
	 )  ;
	END LOOP;
   COMMIT;
  END IF;
 END copy_crfnotify;
 PROCEDURE sp_adversevenotify (
     P_STUDY NUMBER,
     P_PATIENT NUMBER,
	P_ADVTYPE NUMBER,
	P_OUTCOME VARCHAR2,
	P_CREATOR NUMBER,
	P_IPADD VARCHAR2
)
AS
/*
This procedure will move the records to despatcher table to send mails in case of Serious
Adverse Event AND  OR outcome is DEATH
P_EVENT is event id
P_ADVTYPE is adverse event id and description to be fetched from Code List
P_OUTCOME is sequence of numbers
P_CREATOR is for auditing that who entered the Adverse event
P_IPADD is the IP address from where the Adverse Event was executed
 Modified By         Date     	Remarks
 Sonia               12Dec      To move the records to despatcher for further action to be taken
 Sonia               28 Dec      To get visit from sch_events1 instead of calculating dynamically
 Sonia               11 Jan      To move the records for pager/cell mail to despatcher for further action to be taken
 Sonia               26 Feb      To make it work on basis of patid and study (EPT requirements)
 Sonia               7 Sep       enrollment date is not mandatory for adverse events, removed check for the same
 Sonia Abrol 03/29/2005 to send email to only users who have access rights to patient's organization
*/
V_PATPROT	NUMBER;
V_ADVTYPFLAG	CHAR(1);
V_POS		NUMBER :=0 ;
V_POS1		NUMBER :=0 ;
V_USERs		VARCHAR2(1000);
V_MSG		VARCHAR2(4000);
V_MSGTYP	VARCHAR2(50);
V_STR		VARCHAR2(1000);
V_PARAMS	VARCHAR2(4000);
v_cnt NUMBER ;
v_patcode VARCHAR2(25);
v_fparam VARCHAR2(200);
v_eventname VARCHAR2(100);
v_schdate VARCHAR2(12);
v_visit NUMBER;
v_msgtemplate VARCHAR2(4000);
v_pkalnot NUMBER;
v_pkalnot_outcome NUMBER;
v_alnot_mobeve VARCHAR2(2000);
v_alnot_mobeve_outcome VARCHAR2(2000);
v_cellmsgtemplate VARCHAR2(4000);
v_cellmsg VARCHAR2(4000);
v_study NUMBER;
v_protocol NUMBER;
v_site_right NUMBER;
BEGIN
	/* Get the fk_patprot ID  into V_PATPROT from sch_events1 */
	SELECT pk_patprot, fk_study, fk_protocol INTO v_patprot, v_study, v_protocol
     FROM ER_PATPROT
	WHERE fk_study = P_STUDY AND fk_per = p_patient AND
	PATPROT_STAT = 1 ;

  --and PATPROT_ENROLDT is not null; commented by sonia, enrollment date is not mandatory for adverse events

	/*SELECT fk_patprot, description, to_char(ACTUAL_SCHDATE,'mm/dd/yyyy'), visit
	 into v_patprot, v_eventname ,v_schdate , v_visit
	 FROM sch_events1
	WHERE event_id = lpad(p_event,10,0) ;*/
--        Select  PKG_COMMON.SCH_GETPATCODE(v_patprot)
--	   into v_patcode
--   	   from dual;        -- v_patcode is param 1
             SELECT P.per_code
             INTO v_patcode
             FROM ER_PATPROT e , ER_PER P
             WHERE e.pk_patprot = v_patprot AND
              P.pk_per = e.fk_per;


	  -- v_visit := sch_getvisit (v_patprot, lpad(p_event,10,0) ) ;  -- get visit
	/*Get the Users, Flag to whether send notification and message type into V_USER, V_ADVTYPFLAG,
	  and v_msgtyp respectively incase of Adverse Event*/
BEGIN
	SELECT ALNOT_FLAG, ALNOT_USERS, trim(a.codelst_subtyp), PK_ALNOT, ALNOT_MOBEVE
	 INTO v_advtypflag, v_users, v_msgtyp, v_pkalnot,  v_alnot_mobeve
	 FROM SCH_ALERTNOTIFY, SCH_CODELST a, SCH_CODELST b
	WHERE fk_patprot = v_patprot AND fk_study = v_study AND fk_protocol =  v_protocol
	  AND fk_codelst_an = a.pk_codelst
	  AND a.codelst_subtyp = b.codelst_subtyp
	  AND b.pk_codelst = p_advtype
	  AND a.codelst_type = 'sch_alert'
	  AND b.codelst_type = 'adve_type' ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
     v_advtypflag := 0;
  END ;
	/*If the flag is set to send notification*/
        IF v_advtypflag = 1 THEN
		/* Get the message */
                        v_msgtemplate  :=    Pkg_Common.SCH_GETMAILMSG( v_msgtyp);
                        --v_fparam :=  v_patcode ||'~' || nvl(to_char(v_visit),'Not Known') ||'~'|| v_eventname ||'~'|| v_schdate ;
			 	    v_fparam :=  v_patcode;
                        v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate ,v_fparam);
	     --
			 --insert record for mobile/pager mail
        IF v_alnot_mobeve  IS NOT NULL THEN
	    v_cellmsgtemplate := Pkg_Common.SCH_GETCELLMAILMSG(v_msgtyp);
         v_cellmsg :=    Pkg_Common.SCH_GETMAIL(v_cellmsgtemplate ,v_fparam);
          --p(v_alnot_mobeve);
	   INSERT INTO SCH_DISPATCHMSG (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_PAT		       ,
				CREATOR                ,
				IP_ADD                 ,
				FK_PATPROT             )
			      VALUES (
			        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
			   	SYSDATE,
			 	0,
				'M', --M for mobile :)
				NULL ,
				v_cellmsg,
				v_pkalnot,
				p_creator,
				p_ipadd,
			     v_patprot) ;
	   END IF;
	--
		/*Move the records to despatcher to send email to the users*/
		v_str    := v_users || ',' ;
		v_params := v_users || ',' ;
		LOOP
			V_CNT := V_CNT + 1;
		       	V_POS1 := INSTR (V_STR, ',');
		       	V_PARAMS := SUBSTR (V_STR, 1, V_POS1 - 1);
		       	EXIT WHEN V_PARAMS IS NULL;

						--check for email recepient user's rights on patient's organization
				  IF v_patprot > 0 THEN
					BEGIN
						 --get user's rights to the patient's site
						SELECT pkg_user.f_chk_right_for_patprotsite(v_patprot,v_params)
						INTO v_site_right FROM dual;
					 EXCEPTION WHEN NO_DATA_FOUND THEN
					 		 v_site_right := 0;
					 END;
   			     ELSE
			 	 		v_site_right  := 1;
			    END IF;

			IF v_site_right > 0 THEN -- create  a mail only i user has any access rights to patients site
					INSERT INTO SCH_DISPATCHMSG (
						PK_MSG                 ,
						MSG_SENDON             ,
						MSG_STATUS             ,
						MSG_TYPE               ,
						FK_SCHEVENT               ,
						MSG_TEXT               ,
						FK_PAT		       ,
						CREATOR                ,
						IP_ADD                 ,
						FK_PATPROT             )
					      VALUES (
					        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
					   	SYSDATE ,
					 	0,
						'U',
						NULL,
						v_msg ,
						v_params,
						p_creator,
						p_ipadd,
					        v_patprot) ;
				END IF;

					V_STR := SUBSTR (V_STR, V_POS1 + 1);
		END LOOP ;
	END IF;
END sp_adversevenotify;
-------------------------
 PROCEDURE sp_deathnotify (
     P_FKADVERSE NUMBER,
	P_OUTCOME NUMBER,
	P_CREATOR NUMBER,
	P_IPADD VARCHAR2
)
AS
/*
This procedure will move the records to despatcher table to send mails in case of death outcome
Sonia Sahni

Modification History
Sonia               7 Sep       enrollment date is not mandatory for adverse events, removed check for the same
 Sonia Abrol 03/29/2005 to send email to only users who have access rights to patient's organization
*/
V_PATPROT	NUMBER;
V_OUTCOMEFLAG	CHAR(1);
V_USERs		VARCHAR2(1000);
V_MSG		VARCHAR2(4000);
V_MSGTYP	VARCHAR2(50);
V_STR		VARCHAR2(1000);
V_PARAMS	VARCHAR2(4000);
v_cnt  NUMBER :=0;
v_patcode VARCHAR2(25);
v_fparam VARCHAR2(200);
v_eventname VARCHAR2(100);
v_schdate VARCHAR2(12);
v_visit NUMBER;
v_msgtemplate VARCHAR2(4000);
v_pkalnot NUMBER;
v_pkalnot_outcome NUMBER;
v_alnot_mobeve VARCHAR2(2000);
v_alnot_mobeve_outcome VARCHAR2(2000);
v_cellmsgtemplate VARCHAR2(4000);
v_cellmsg VARCHAR2(4000);
v_study NUMBER;
v_per NUMBER;
v_protocol NUMBER;
V_POS1		NUMBER :=0 ;
v_site_right NUMBER;
BEGIN
	/* Get the fk_patprot ID  into V_PATPROT from sch_events1 */
	SELECT ad.fk_study, ad.fk_per, er.pk_patprot, er.fk_protocol
	INTO v_study, v_per, v_patprot, v_protocol
     FROM SCH_ADVERSEVE ad , ER_PATPROT er
	WHERE ad.pk_adveve = P_FKADVERSE AND
	er.fk_study = 	ad.fk_study AND er.fk_per = ad.fk_per AND
	er.PATPROT_STAT = 1 ;

	--and er.PATPROT_ENROLDT is not null; -- Sonia    7 Sep       enrollment date is not mandatory for adverse events, removed check for the same

--      Select  PKG_COMMON.SCH_GETPATCODE(v_patprot)
--	   into v_patcode
--   	   from dual;        -- v_patcode is param 1
             SELECT P.per_code
             INTO v_patcode
             FROM ER_PATPROT e , ER_PER P
             WHERE e.pk_patprot = v_patprot AND
              P.pk_per = e.fk_per;
	  -- v_visit := sch_getvisit (v_patprot, lpad(p_event,10,0) ) ;  -- get visit
         BEGIN
 		   SELECT ALNOT_FLAG, ALNOT_USERS, trim(a.codelst_subtyp),PK_ALNOT, ALNOT_MOBEVE
		 	 INTO v_outcomeflag, v_users, v_msgtyp, v_pkalnot_outcome ,v_alnot_mobeve_outcome
	          FROM SCH_ALERTNOTIFY, SCH_CODELST a
			WHERE fk_patprot = v_patprot AND fk_study = v_study AND fk_protocol = v_protocol AND
			fk_codelst_an = a.pk_codelst  AND
			  a.codelst_type = 'sch_alert' AND  a.codelst_subtyp = 'al_death' ;
	 EXCEPTION WHEN NO_DATA_FOUND THEN
	     v_outcomeflag:= 0;
	  END ;
			IF v_outcomeflag = 1 THEN
                        v_msgtemplate  :=    Pkg_Common.SCH_GETMAILMSG( v_msgtyp);
                        --v_fparam :=  v_patcode ||'~' || nvl(to_char(v_visit),'Not Known') ||'~'|| v_eventname ||'~'|| v_schdate ;
                        v_fparam :=  v_patcode ;
                        v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate ,v_fparam);
				    v_cellmsgtemplate := Pkg_Common.SCH_GETCELLMAILMSG(v_msgtyp);
                        v_cellmsg :=    Pkg_Common.SCH_GETMAIL(v_cellmsgtemplate ,v_fparam);
                       --p('v_fparam' || v_fparam);
        --              p('v_users' || v_users);
				/*Move the records to despatcher to send email to the users*/
				v_str    := v_users || ',' ;
				v_params := v_users || ',' ;
                  	     --
			 --insert record for mobile/pager mail
        IF v_alnot_mobeve_outcome IS NOT NULL THEN
          --p(v_alnot_mobeve);
	   INSERT INTO SCH_DISPATCHMSG (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_PAT		       ,
				CREATOR                ,
				IP_ADD                 ,
				FK_PATPROT             )
			      VALUES (
			        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
			   	SYSDATE,
			 	0,
				'M', --M for mobile :)
				NULL,
				v_cellmsg,
				v_pkalnot_outcome ,
				p_creator,
				p_ipadd,
		        v_patprot) ;
	   END IF;
				LOOP
					    V_CNT := V_CNT + 1;
				       	V_POS1 := INSTR (V_STR, ',');
				       	V_PARAMS := SUBSTR (V_STR, 1, V_POS1 - 1);
						EXIT WHEN V_PARAMS IS NULL;

--						 Plog.DEBUG(pCTX,'V_PARAMS' || V_PARAMS);
 	--					 Plog.DEBUG(pCTX,'V_patprot' || V_patprot);
				--check for email recepient user's rights on patient's organization
				  IF v_patprot > 0 THEN
					BEGIN
						 --get user's rights to the patient's site
						SELECT pkg_user.f_chk_right_for_patprotsite(v_patprot,v_params)
						INTO v_site_right FROM dual;
						 				--		 Plog.DEBUG(pCTX,'v_site_right' || v_site_right);
					 EXCEPTION WHEN NO_DATA_FOUND THEN
					 		 v_site_right := 0;
					 						-- Plog.DEBUG(pCTX,'v_site_right' || v_site_right);
					 END;
   			     ELSE
			 	 		v_site_right  := 1;
			    END IF;

			IF v_site_right > 0 THEN -- create  a mail only i user has any access rights to patients site
									 			--			 Plog.DEBUG(pCTX,'v_site_right greate than 0');
				INSERT INTO SCH_DISPATCHMSG (
						PK_MSG                 ,
						MSG_SENDON             ,
						MSG_STATUS             ,
						MSG_TYPE               ,
						FK_SCHEVENT               ,
						MSG_TEXT               ,
						FK_PAT		       ,
						CREATOR                ,
						IP_ADD                 ,
						FK_PATPROT             )
					      VALUES (
					        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
					   	SYSDATE ,
					 	0,
						'U',
						NULL ,
						v_msg ,
						v_params,
						p_creator,
						p_ipadd,
					     v_patprot) ;

				END IF;
					V_STR := SUBSTR (V_STR, V_POS1 + 1);
				END LOOP ;
			END IF;
			 --Plog.DEBUG(pCTX,'sp_deathnotify end');
END sp_deathnotify;

--//JM:-------------------------------
PROCEDURE Sp_Advnotify_For_Studylevel(
     P_STUDY NUMBER,
	 P_PATIENT NUMBER,
	 P_CREATOR NUMBER,
	P_IPADD VARCHAR2
)
AS
/*
This procedure will move the records to despatcher table to send mails in case of Serious
Adverse Event
P_Study is Study id
P_Patient is patient id associated to the study
P_CREATOR is for auditing that who entered the Adverse event
P_IPADD is the IP address from where the Adverse Event was executed
JM: 22Aug2006
*/
V_PATPROT	NUMBER;

V_POS		NUMBER :=0 ;
V_POS1		NUMBER :=0 ;
V_USERS		VARCHAR2(1000);
V_MSG		VARCHAR2(4000);
V_MSGTYP	VARCHAR2(50);
V_STR		VARCHAR2(1000);
V_PARAMS	VARCHAR2(4000);
v_cnt NUMBER ;
v_patcode VARCHAR2(25);
v_fparam VARCHAR2(200);
v_eventname VARCHAR2(100);
v_msgtemplate VARCHAR2(4000);
v_cellmsgtemplate VARCHAR2(4000);
v_cellmsg VARCHAR2(4000);
v_study NUMBER;

v_site_right NUMBER;
v_mobeve		VARCHAR2(2000);
v_settingModnumber NUMBER;

BEGIN

	SELECT pk_patprot, fk_study--, fk_protocol
	INTO v_patprot, v_study--, v_protocol
     FROM ER_PATPROT
	WHERE fk_study = P_STUDY AND fk_per = p_patient
	AND PATPROT_STAT = 1 ;

	v_settingModnumber := P_STUDY;

	plog.DEBUG(pctx,'P_STUDY:' || P_STUDY);

BEGIN
SELECT trim(a.codelst_subtyp) INTO v_msgtyp FROM SCH_CODELST a,
SCH_CODELST b WHERE a.codelst_type = 'sch_alert'
	AND b.codelst_type = 'adve_type' AND a.codelst_subtyp = b.codelst_subtyp;

EXCEPTION WHEN NO_DATA_FOUND THEN
		  v_msgtyp := '';
END;
	  plog.DEBUG(pctx,'v_msgtyp:' || v_msgtyp);


             SELECT P.per_code
             INTO v_patcode
             FROM ER_PATPROT e , ER_PER P
             WHERE e.pk_patprot = v_patprot AND
              P.pk_per = e.fk_per;

--JM:get the ae notify user

		 BEGIN
			SELECT settings_value INTO v_users FROM ER_SETTINGS WHERE settings_modnum = P_STUDY AND settings_modname = 3
			AND settings_keyword='SAE_NOTIFY_USER';

			SELECT settings_value INTO v_mobeve FROM ER_SETTINGS WHERE settings_modnum = P_STUDY AND settings_modname = 3
			AND settings_keyword='SAE_NOTIFY_EMAIL';
		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  		--	v_users  := NULL;
					v_mobeve  := NULL;
		 END;

	  plog.DEBUG(pctx,'v_users:' || v_users);
	  	  plog.DEBUG(pctx,'v_mobeve :' || v_mobeve );

	/* Get the message */
                       v_msgtemplate  :=    Pkg_Common.SCH_GETMAILMSG( v_msgtyp);

  	  	  plog.DEBUG(pctx,'v_msgtemplate  :' || v_msgtemplate  );
   	  	  plog.DEBUG(pctx,'v_patcode  :' || v_patcode );

		 	    	   v_fparam :=  v_patcode;
                       v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate ,v_fparam);
     --


	 --insert record for mobile/pager mail
	 IF v_mobeve IS NOT NULL THEN
	       	  v_cellmsgtemplate := Pkg_Common.SCH_GETCELLMAILMSG(v_msgtyp);
	          v_cellmsg :=    Pkg_Common.SCH_GETMAIL(v_cellmsgtemplate ,v_fparam);


   INSERT INTO SCH_DISPATCHMSG (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_PAT		       ,
				CREATOR                ,
				IP_ADD                 ,
				FK_PATPROT             )
			      VALUES (
			        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
			   	SYSDATE,
			 	0,
				'M', --M for mobile :)
				NULL ,
				v_cellmsg,
				NULL,--------------// v_pkalnot
				p_creator,
				p_ipadd,
			    v_patprot) ;
	   END IF;
	--

        IF v_users IS NOT NULL AND INSTR(LOWER(v_users),'null') <= 0 THEN
		/*Move the records to despatcher to send email to the users*/
  	  		   --plog.DEBUG(pctx,'insert null   patprot :' || v_patprot || '_vapar ' || v_params );

		v_str    := v_users || ',' ;
		v_params := v_users || ',' ;
		LOOP
			V_CNT := V_CNT + 1;
		       	V_POS1 := INSTR (V_STR, ',');
		       	V_PARAMS := SUBSTR (V_STR, 1, V_POS1 - 1);
		       	EXIT WHEN V_PARAMS IS NULL;

						--check for email recepient user's rights on patient's organization
				  IF v_patprot > 0 THEN
					BEGIN
						 --get user's rights to the patient's site
						SELECT pkg_user.f_chk_right_for_patprotsite(v_patprot,v_params)
						INTO v_site_right FROM dual;
					 EXCEPTION WHEN NO_DATA_FOUND THEN
					 		 v_site_right := 0;
					 END;
   			     ELSE
			 	 		v_site_right  := 1;
			    END IF;
  				-- plog.DEBUG(pctx,'site right  :' || v_site_right );
			IF v_site_right > 0 THEN -- create  a mail only i user has any access rights to patients site
					INSERT INTO SCH_DISPATCHMSG (
						PK_MSG                 ,
						MSG_SENDON             ,
						MSG_STATUS             ,
						MSG_TYPE               ,
						FK_SCHEVENT               ,
						MSG_TEXT               ,
						FK_PAT		       ,
						CREATOR                ,
						IP_ADD                 ,
						FK_PATPROT             )
					      VALUES (
					        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
					   	SYSDATE ,
					 	0,
						'U',
						NULL,
						v_msg ,
						v_params,
						p_creator,
						p_ipadd,
					    v_patprot) ;
			END IF;
				--   COMMIT;
					V_STR := SUBSTR (V_STR, V_POS1 + 1);
		END LOOP ;



	END IF;
END Sp_Advnotify_For_Studylevel;
--------------------------------
---//JM: 22August2006-------------
PROCEDURE Sp_Deathnotify_For_Studylevel (
    P_FKADVERSE NUMBER,
	P_CREATOR NUMBER,
	P_IPADD VARCHAR2
)
AS
/*
This procedure will move the records to despatcher table to send mails in case of death outcome
Sonia Sahni

Modification History
Sonia               7 Sep       enrollment date is not mandatory for adverse events, removed check for the same
 Sonia Abrol 03/29/2005 to send email to only users who have access rights to patient's organization
*/
V_PATPROT	NUMBER;
V_OUTCOMEFLAG	CHAR(1);
V_USERs		VARCHAR2(1000);
V_MSG		VARCHAR2(4000);
V_MSGTYP	VARCHAR2(50);
V_STR		VARCHAR2(1000);
V_PARAMS	VARCHAR2(4000);
v_cnt  NUMBER :=0;
v_patcode VARCHAR2(25);
v_fparam VARCHAR2(200);
v_eventname VARCHAR2(100);
v_schdate VARCHAR2(12);
v_visit NUMBER;
v_msgtemplate VARCHAR2(4000);
v_pkalnot NUMBER;
v_pkalnot_outcome NUMBER;
v_alnot_mobeve VARCHAR2(2000);
v_alnot_mobeve_outcome VARCHAR2(2000);
v_cellmsgtemplate VARCHAR2(4000);
v_cellmsg VARCHAR2(4000);
v_study NUMBER;
v_per NUMBER;
v_protocol NUMBER;
V_POS1		NUMBER :=0 ;
v_site_right NUMBER;

v_settingModnumber NUMBER;--JM
v_mobeve_outcome VARCHAR2(2000);--JM
BEGIN

	SELECT ad.fk_study, ad.fk_per, er.pk_patprot --, er.fk_protocol --//no need this column
	INTO v_study, v_per, v_patprot --, v_protocol --//no need this column
     FROM SCH_ADVERSEVE ad , ER_PATPROT er
	WHERE ad.pk_adveve = P_FKADVERSE AND
	er.fk_study = 	ad.fk_study AND er.fk_per = ad.fk_per AND
	er.PATPROT_STAT = 1 ;


    SELECT P.per_code
    INTO v_patcode
    FROM ER_PATPROT e , ER_PER P
    WHERE e.pk_patprot = v_patprot AND
     P.pk_per = e.fk_per;


--JM:get the death notify user

		 BEGIN

			SELECT settings_value INTO v_users FROM ER_SETTINGS WHERE settings_modnum = v_study AND settings_modname = 3
			AND settings_keyword='SAE_DEATH_NOTIFY_USER';

			SELECT settings_value INTO v_mobeve_outcome FROM ER_SETTINGS WHERE settings_modnum = v_study AND settings_modname = 3
			AND settings_keyword='SAE_DEATH_NOTIFY_EMAIL';
		 EXCEPTION WHEN NO_DATA_FOUND THEN
		  		--	v_users  := NULL;
					v_mobeve_outcome  := NULL;
		 END;

BEGIN

SELECT trim(codelst_subtyp) INTO v_msgtyp FROM SCH_CODELST
WHERE codelst_type = 'sch_alert' AND  codelst_subtyp = 'al_death';

EXCEPTION WHEN NO_DATA_FOUND THEN
		  v_msgtyp := 0;
END;
------
	            v_msgtemplate  :=    Pkg_Common.SCH_GETMAILMSG( v_msgtyp);
                v_fparam :=  v_patcode ;
                v_msg :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate ,v_fparam);

				v_cellmsgtemplate := Pkg_Common.SCH_GETCELLMAILMSG(v_msgtyp);
                v_cellmsg :=    Pkg_Common.SCH_GETMAIL(v_cellmsgtemplate ,v_fparam);

				v_str    := v_users || ',' ;
				v_params := v_users || ',' ;

			 --insert record for mobile/pager mail
       IF v_mobeve_outcome IS NOT NULL THEN
          --p(v_alnot_mobeve);
	   INSERT INTO SCH_DISPATCHMSG (
				PK_MSG                 ,
				MSG_SENDON             ,
				MSG_STATUS             ,
				MSG_TYPE               ,
				FK_SCHEVENT               ,
				MSG_TEXT               ,
				FK_PAT		       ,
				CREATOR                ,
				IP_ADD                 ,
				FK_PATPROT             )
			      VALUES (
			        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
			   	SYSDATE,
			 	0,
				'M', --M for mobile :)
				NULL,
				v_cellmsg,
				v_pkalnot_outcome ,
				p_creator,
				p_ipadd,
		        v_patprot) ;
	   END IF;

				 IF v_users IS NOT NULL THEN

				LOOP
					    V_CNT := V_CNT + 1;
				       	V_POS1 := INSTR (V_STR, ',');
				       	V_PARAMS := SUBSTR (V_STR, 1, V_POS1 - 1);
						EXIT WHEN V_PARAMS IS NULL;

--						 Plog.DEBUG(pCTX,'V_PARAMS' || V_PARAMS);
 	--					 Plog.DEBUG(pCTX,'V_patprot' || V_patprot);
				--check for email recepient user's rights on patient's organization
				  IF v_patprot > 0 THEN
					BEGIN
						 --get user's rights to the patient's site
						SELECT pkg_user.f_chk_right_for_patprotsite(v_patprot,v_params)
						INTO v_site_right FROM dual;
						 				--		 Plog.DEBUG(pCTX,'v_site_right' || v_site_right);
					 EXCEPTION WHEN NO_DATA_FOUND THEN
					 		 v_site_right := 0;
					 						-- Plog.DEBUG(pCTX,'v_site_right' || v_site_right);
					 END;
   			     ELSE
			 	 		v_site_right  := 1;
			    END IF;

			IF v_site_right > 0 THEN -- create  a mail only i user has any access rights to patients site
									 			--			 Plog.DEBUG(pCTX,'v_site_right greate than 0');
				INSERT INTO SCH_DISPATCHMSG (
						PK_MSG                 ,
						MSG_SENDON             ,
						MSG_STATUS             ,
						MSG_TYPE               ,
						FK_SCHEVENT               ,
						MSG_TEXT               ,
						FK_PAT		       ,
						CREATOR                ,
						IP_ADD                 ,
						FK_PATPROT             )
					      VALUES (
					        SCH_DISPATCHMSG_SEQ1.NEXTVAL ,
					   	SYSDATE ,
					 	0,
						'U',
						NULL ,
						v_msg ,
						v_params,
						p_creator,
						p_ipadd,
					     v_patprot) ;

				END IF;
					V_STR := SUBSTR (V_STR, V_POS1 + 1);
				END LOOP ;
			END IF;
			 --Plog.DEBUG(pCTX,'sp_deathnotify end');
END Sp_Deathnotify_For_Studylevel;
-----------------------

PROCEDURE sp_notifyadmin (
     P_USER NUMBER,
	P_IPADD VARCHAR2,
	P_VELOSUSER NUMBER,
	P_FAILDATE VARCHAR2,
     P_FAILTIME VARCHAR2,
	O_MAIL OUT VARCHAR2 ,
     O_TOUSER OUT NUMBER,
	O_TOUSERMAIL OUT VARCHAR2
)
AS
/*
This procedure will be used to create notifications for velos admin/ account admin for disabled user account due to
maximum failed login attempts
Sonia Sahni -  03/20/2003
*/
v_accuser NUMBER := 0;
v_adminuser NUMBER := 0;
v_touser NUMBER;
v_msgtemplate VARCHAR2(4000);
v_mail VARCHAR2(4000);
v_fparam VARCHAR2(1000);
v_logname VARCHAR2(20);
v_name VARCHAR2(65);
v_org VARCHAR2(65);
v_account NUMBER :=0 ;
v_def_admin BOOLEAN := FALSE;
v_adminemail VARCHAR2(100);
v_velosadminemail VARCHAR2(100);
BEGIN
-- check if the user is Account Admin
  BEGIN
      SELECT  a.AC_USRCREATOR  , u.pk_user , u. USR_LOGNAME , NVL(u.USR_FIRSTNAME,'')  || ' ' ||  NVL(u.USR_LASTNAME,'') , u.fk_account , admin_ad.ADD_EMAIL
      INTO v_adminuser , v_accuser, v_logname, v_name, v_account, v_adminemail
      FROM ER_ACCOUNT a , ER_USER ad, ER_USER u, ER_ADD admin_ad
      WHERE u.pk_user = p_user AND
      a.pk_account = u.fk_account AND
      ad.pk_user = a.AC_USRCREATOR AND
	 admin_ad.pk_add = ad.FK_PERADD ;
      SELECT  a.SITE_NAME
      INTO v_org
      FROM ER_USER u,  ER_SITE a
      WHERE u.pk_user = p_user AND
      a.pk_site = u.FK_SITEID   ;
	 v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('fail_acc');
	 v_fparam :=  v_logname ||'~'|| v_name ||'~'|| v_org ||'~'|| P_FAILDATE ||'~'|| P_FAILTIME;
	 v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);
      IF v_adminuser <> v_accuser THEN

	  SELECT ctrl_value INTO v_velosadminemail FROM er_ctrltab WHERE ctrl_key = 'eresuser';
        v_touser := v_adminuser;
	   v_def_admin  := FALSE;
	   O_TOUSERMAIL :=  v_velosadminemail;


       ELSE
	   v_touser :=  P_VELOSUSER;
	   v_def_admin := TRUE;
    	   --get velos admin email
--	   BEGIN
--     	SELECT ADD_EMAIL
--	     INTO v_velosadminemail
--   	     FROM er_add , er_user
--  	     WHERE pk_user = P_VELOSUSER AND er_add.pk_add = er_user.FK_PERADD ;
--		O_TOUSERMAIL := v_velosadminemail;
--	     EXCEPTION WHEN NO_DATA_FOUND THEN
--            O_TOUSERMAIL := 'customersupport@veloseresearch.com';
--           END;

	  SELECT ctrl_value INTO v_velosadminemail FROM er_ctrltab WHERE ctrl_key = 'eresuser';

	   O_TOUSERMAIL := v_velosadminemail;

      END IF;
    /*     if v_def_admin then
         --insert into dispatchmsg
     	Insert into sch_dispatchmsg (	PK_MSG ,MSG_SENDON ,MSG_STATUS,MSG_TYPE ,
						MSG_TEXT ,FK_PAT,IP_ADD,MSG_SUBJECT)
					      Values ( SCH_DISPATCHMSG_SEQ1.nextval ,sysdate ,0,
						'U',	v_mail ,v_touser,P_IPADD,'Failed Login Attempt') ;
        --deactivate user
       else
         -- get all admins from admin account
         for i in (
     	   select  FK_USER
             from er_usrgrp, er_grps
             where er_grps.fk_account = v_account and
             trim(lower(GRP_NAME)) = 'admin' and
             er_usrgrp. FK_GRP         = er_grps.PK_GRP )
          LOOP
         	  Insert into sch_dispatchmsg (	PK_MSG ,MSG_SENDON ,MSG_STATUS,MSG_TYPE ,
						MSG_TEXT ,FK_PAT,IP_ADD,MSG_SUBJECT)
					      Values ( SCH_DISPATCHMSG_SEQ1.nextval ,sysdate ,0,
					'U',	v_mail ,i.fk_user,P_IPADD,'Failed Login Attempt') ;
     	 END LOOP;
       end if; */
     o_mail := v_mail;
	o_touser := v_touser;

     COMMIT;
   EXCEPTION WHEN NO_DATA_FOUND THEN
         o_touser := 0;
   END;
END sp_notifyadmin;

/* By Sonia Abrol, 04/08/05
   to insert alert notification records for a patient enrollment, moving the logic from a er_patprot trigger, er_patprot_ai1, this will now also be called from update of fk_protocol in er_patprot
*/
 PROCEDURE set_alertnotify_for_enrollment (p_study NUMBER, p_protocol NUMBER, p_patprot NUMBER, p_creator  NUMBER,
 p_created_on  DATE,p_ip_add VARCHAR2)
 AS
  v_globflag      VARCHAR2 (3) ;
   v_editlock      NUMBER;
   v_calglobflag   VARCHAR2 (3) ;
   v_new_fk_study NUMBER;
   v_new_fk_protocol NUMBER;
   v_new_pk_patprot  NUMBER;
   v_new_creator NUMBER;
   v_new_created_on DATE;
   v_new_ip_add  VARCHAR2 (15) ;

 BEGIN
 	  /**************************************************************************************************
   **
   ** Author: Sonia Sahni 02/06/2002
   ** insert default alert notifications for patient enrollment
   ** according to the study's global settings - global flag and edit lock
*/

  v_new_fk_study := p_study;
  v_new_fk_protocol :=  p_protocol ;
  v_new_pk_patprot  := p_patprot;
  v_new_creator := p_creator;
  v_new_created_on := p_created_on;
  v_new_ip_add :=  p_ip_add;

  BEGIN
   SELECT ALNOT_GLOBALFLAG, ALNOT_FLAG
   INTO v_globflag , v_editlock
   FROM SCH_ALERTNOTIFY
   WHERE FK_STUDY = v_new_fk_study AND FK_PROTOCOL = v_new_fk_protocol AND
   FK_PATPROT IS NULL AND ALNOT_TYPE = 'G' ;

   EXCEPTION WHEN NO_DATA_FOUND THEN
    v_globflag := 'N' ;
    v_editlock := 1 ;
  END;

  v_calglobflag := v_globflag ;

  IF (v_globflag = 'G' OR v_globflag = 'GR') AND v_editlock = 0 THEN
      v_calglobflag := 'N';
  END IF;

  IF v_globflag = 'NR' AND v_editlock = 0 THEN
      v_calglobflag := 'N';
  END IF;

  IF (v_globflag = 'G' OR v_globflag = 'GR') AND v_editlock = 1 THEN
                   INSERT INTO SCH_ALERTNOTIFY (PK_ALNOT, --1
                             FK_PATPROT , --2
                             FK_CODELST_AN  , --3
                             ALNOT_TYPE ,
                             ALNOT_USERS ,
                             ALNOT_MOBEVE ,
                             ALNOT_FLAG ,
                             CREATOR,  --- 8
                             CREATED_ON ,
                             IP_ADD,
                             FK_STUDY,
                             FK_PROTOCOL,
                             ALNOT_GLOBALFLAG)
                (SELECT      SEQ_SCH_ALERTNOTIFY.NEXTVAL,
                              v_new_pk_patprot ,
                             FK_CODELST_AN  , --3
                             ALNOT_TYPE ,
                             ALNOT_USERS ,
                             ALNOT_MOBEVE ,
                             ALNOT_FLAG ,
					    v_new_creator,
                             v_new_created_on,
                             v_new_ip_add,
                             FK_STUDY,
                             FK_PROTOCOL,
                             ALNOT_GLOBALFLAG
                          FROM SCH_ALERTNOTIFY
                         WHERE FK_STUDY = v_new_fk_study AND
                          FK_PROTOCOL = v_new_fk_protocol AND fk_patprot IS NULL AND
					 ALNOT_TYPE <> 'G');

          			 -- copy all crfnotify settings for this study and protocol for each active registartion/patprot

 INSERT INTO SCH_CRFNOTIFY( PK_CRFNOT, FK_CRF,
                           FK_CODELST_NOTSTAT , CRFNOT_USERSTO ,
                           CRFNOT_NOTES , CREATOR, CREATED_ON ,
                           IP_ADD , FK_STUDY,  FK_PROTOCOL,
                           FK_PATPROT , CRFNOT_GLOBALFLAG  )
			   ( SELECT SEQ_SCH_CRFNOTIFY.NEXTVAL,
                     	  FK_CRF ,
                           FK_CODELST_NOTSTAT ,
                           CRFNOT_USERSTO ,
                           CRFNOT_NOTES ,
                           v_new_creator,
                           v_new_created_on,
                           v_new_ip_add,
					  FK_STUDY,
                           FK_PROTOCOL ,
                           v_new_pk_patprot ,
                           CRFNOT_GLOBALFLAG FROM
           			  SCH_CRFNOTIFY
					  WHERE FK_STUDY = v_new_fk_study  AND
                          FK_PROTOCOL = v_new_fk_protocol AND fk_patprot IS NULL );
  END IF;

   IF v_globflag = 'N' OR v_globflag = 'NR'  OR v_calglobflag = 'N' THEN

    INSERT INTO SCH_ALERTNOTIFY
    (PK_ALNOT, --1
    FK_PATPROT , --2
    FK_CODELST_AN  , --3
    ALNOT_TYPE ,
    ALNOT_USERS ,
    ALNOT_MOBEVE ,
    ALNOT_FLAG ,
    CREATOR,  --- 8
    CREATED_ON ,
    IP_ADD,
    FK_STUDY,
    FK_PROTOCOL,
    ALNOT_GLOBALFLAG)
   (SELECT
    SEQ_SCH_ALERTNOTIFY.NEXTVAL,
    v_new_pk_patprot,
    c.PK_CODELST,
    'A',
    NULL,
   NULL,
   '0' , --
   v_new_creator,
   v_new_created_on,
   v_new_ip_add,
v_new_fk_study,
  v_new_fk_protocol,
   v_calglobflag
  FROM SCH_CODELST c
  WHERE c.codelst_type = 'sch_alert'
);

END IF;

 END;

-------------------end of package
END Pkg_Alnot;
/