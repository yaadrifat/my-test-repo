LOAD DATA
INFILE *
INTO TABLE ER_LABEL_TEMPLATES 
APPEND
FIELDS TERMINATED BY ','
(PK_LT,TEMPLATE_NAME,TEMPLATE_TYPE,TEMPLATE_SUBTYPE,
 TEMP_FILE filler char,
"TEMPLATE_FORMAT" LOBFILE (TEMP_FILE) TERMINATED BY EOF NULLIF TEMP_FILE = 'NONE'
)
BEGINDATA 
10,PDF417 Barcode,specimen,default,temp_specimen_pdf417.txt
11,DataMatrix Barcode,specimen,default,temp_specimen_datamatrix.txt
12,PDF417 Barcode,storage,default,temp_storage_pdf417.txt
13,DataMatrix Barcode,storage,default,temp_storage_datamatrix.txt
