set define off;

DECLARE
countFlage NUMBER(5);
BEGIN
	SELECT COUNT(1) INTO countFlage FROM USER_TABLES WHERE TABLE_NAME='SCH_CODELST';
	if (countFlage > 0) then
		UPDATE SCH_CODELST SET CODELST_STUDY_ROLE = 'default_data' WHERE CODELST_TYPE='cost_desc';
		commit;
	end if;
END;
/