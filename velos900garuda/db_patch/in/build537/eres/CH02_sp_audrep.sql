set define off;
create or replace
PROCEDURE        "SP_AUDREP" (
   P_AUD          number,
   P_PARAMS         varchar,
   P_SXML     out   clob
)
is
 /****************************************************************************************************
   **
   ** Author: Charanjiv S Kalha 10/29/2001
   ** This procedure generates the XML from SQL's for the Audit module. The SQLs are stored
   ** in the AUDIT_REP.AUD_SQL column. The RID is passed as a parameter (p_aud), the parameters
   ** to the SQL (stored as ~1, ~2 and so on) are passed in comma (,) delimited string p_params.
   ** The procedure parses the string p_params and replaces the parameters in the SQL with the values.
   ** The XML is created with xmlgen.getXML procedure. and passed out in the p_sxml parameter.
   **
   ** Modification History
   **
   ** Modified By         Date         Remarks
   **
   **************************************************************************************************
   **
   */
   V_STR      varchar2 (2000)default P_PARAMS || ',';
   V_SQLSTR   varchar2 (4000);
   V_PARAMS   varchar2 (2000)default P_PARAMS || ',';
   V_POS      number         := 0;
   V_CNT      number         := 0;
   V_SQLXML   CLOB;
BEGIN
/*
Resets any XML options set before
*/
   XMLGEN.RESETOPTIONS;

   -- Get the Report SQL from the ER_REPORT Table for the preport id p_repid
   SELECT AUDIT_SQL
     INTO V_SQLSTR
     FROM AUDIT_REP
    WHERE RID = P_AUD;

   -- Parse the parameter string passed and separate the comma delimited paramters.
   -- Replace the variables in the SQL with the parameters
   LOOP
      exit when V_PARAMS Is null;
      V_CNT := V_CNT + 1;
      V_POS := instr (V_STR, ',');
      V_PARAMS := substr (V_STR, 1, V_POS - 1);
      V_SQLSTR := replace (V_SQLSTR, '~' || V_CNT, V_PARAMS);
      V_STR := substr (V_STR, V_POS + 1);
 	  DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,1,200)) ;
--  	  DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,-200)) ;

   END LOOP;

   --DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,1,255)) ;
   -- DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,245,255)) ;
   -- Execute the SQL created, pass it as a variable in the XMLGEN.getXML procedure
   -- This will return the XML for the SQL. Even if the SQL returns no rows the XML will be created
      P_SXML := dbms_XMLGEN.GETXML (V_SQLSTR);
      DBMS_OUTPUT.PUT_LINE ('XML ' || DBMS_LOB.SUBSTR (P_SXML,1, 200));
      DBMS_OUTPUT.PUT_LINE ('XML 2 ' || DBMS_LOB.SUBSTR (P_SXML,200, 200));
END;
/
 
