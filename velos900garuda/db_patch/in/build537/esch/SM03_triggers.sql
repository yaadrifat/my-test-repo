 --Trigger alterations for CCF-FIN1
 
drop trigger "ESCH"."EVENT_DEF_AI1";

drop trigger "ESCH"."EVENT_DEF_AU1";

DROP TRIGGER "ESCH"."EVENT_ASSOC_AI1" ;
/

CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AI1" AFTER INSERT ON EVENT_ASSOC
FOR EACH ROW
DECLARE v_checkactive NUMBER(1) := 0;
         v_study NUMBER;
v_return number;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_ASSOC_AI1', pLEVEL  => Plog.LFATAL);

BEGIN
 IF UPPER(trim(:NEW.event_type)) = 'P' THEN

   INSERT INTO SCH_PROTSTAT (
     PK_PROTSTAT    ,
     FK_EVENT       ,
     PROTSTAT       ,
     PROTSTAT_DT    ,
     PROTSTAT_BY    ,
     PROTSTAT_NOTE  ,
     CREATOR        , -- Amarnadh
     IP_ADD  )
     VALUES
    (sch_protstat_seq.NEXTVAL,
    :NEW.event_id ,
    :NEW.status ,
    :NEW.status_dt ,
    :NEW.status_changeby ,
    :NEW.notes ,
    :NEW.creator ,
    :NEW.ip_Add ) ;


   -- by sonia sahni
   -- insert default notifications if study is already enrolling
   v_study := :NEW.CHAIN_ID;

	/*CCF-FIN1 User will specify the budget template to be used for creating default budget
	Removed PKG_BGT.sp_createProtocolBudget call from here Moved code to Java.
	This is done explicitely as Library calendars will not have default budgets anymore.
	The only place the procedure will be called from is study setup calendars
	*/
END IF;

if (:NEW.event_type='A') then
	begin
	    PKG_BGT.sp_addToProtocolBudget( :NEW.event_id,'S',:NEW.chain_id, :NEW.creator , :NEW.ip_Add , v_return , 0 ,:NEW.fk_visit,
		:NEW.name,:NEW.event_cptcode,:NEW.event_line_category,:NEW.description,:NEW.event_sequence);
	    --, to be added after Manimaran's patch - new.event_line_category
	end;
 end if;

END ;
/


DROP TRIGGER "ESCH"."EVENT_ASSOC_AU1" ;
/

CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AU1" AFTER UPDATE ON EVENT_ASSOC
FOR EACH ROW
declare
v_return number;
v_pk_lineitem number;

BEGIN
IF  (:OLD.status <> :NEW.status) THEN
 INSERT INTO SCH_PROTSTAT (
    PK_PROTSTAT    ,
 FK_EVENT       ,
 PROTSTAT       ,
 PROTSTAT_DT    ,
 PROTSTAT_BY    ,
 PROTSTAT_NOTE  ,
 CREATOR,   ---Amarnadh
 IP_ADD )
        VALUES
 (sch_protstat_seq.NEXTVAL,
  :NEW.event_id ,
  :NEW.status ,
  :NEW.status_dt ,
  :NEW.status_changeby ,
  :NEW.notes ,
  :NEW.creator ,
  :NEW.ip_Add ) ;
END IF;
    --set default notifications for the protocol
 /*  if ( (:old.status <> :new.status) and :new.status = 'A') then
       pkg_alnot.set_alnot(NULL, :new.CHAIN_ID, :new.EVENT_ID, 'G' , :new.creator, sysdate, :new.ip_add);
  end if; */


 --for default budget:
if (:old.event_type='A' and nvl(:old.fk_visit,0) <> nvl(:NEW.fk_visit,0) and nvl(:old.displacement,-1) <> 0 ) then
    PKG_BGT.sp_addToProtocolBudget( :NEW.event_id,'S',:NEW.chain_id, :NEW.creator , :NEW.ip_Add , v_return , 0 ,:NEW.fk_visit,
    :NEW.name,:NEW.event_cptcode,:NEW.event_line_category,:NEW.description,:NEW.event_sequence);
end if;
begin
    -- get default budget lineitem info for this event
    if (:old.event_type='A' and ( (:NEW.name <> :old.name) or  ( nvl(:NEW.description,' ') <> nvl(:old.description,' ') ) or
       ( nvl(:NEW.event_cptcode,' ') <> nvl(:old.event_cptcode,' ') )  or
       ( nvl(:NEW.event_line_category,0) <> nvl(:old.event_line_category,0) )
       or( nvl(:NEW.event_sequence,0) <> nvl(:old.event_sequence,0) ))
    )then -- only for events
		--update the lineitem
        update sch_lineitem
        set last_modified_by = :NEW.last_modified_by ,last_modified_date = sysdate, IP_ADD = :NEW.ip_add,
        LINEITEM_NAME =  :NEW.name,LINEITEM_DESC= :NEW.description,LINEITEM_CPTCODE = :NEW.event_cptcode,
        lineitem_seq = :NEW.event_sequence
        where pk_lineitem in (select pk_lineitem
        from sch_lineitem l
        where l.fk_event = :old.event_id and l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_bgtcal =
        (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.chain_id and fk_budget = (
        select  pk_budget from sch_budget where budget_calendar = :old.chain_id)
        )
        ) ) ;
    end if;
	exception when no_data_found then
    	v_pk_lineitem := 0;
end;
END ;
/

DROP TRIGGER "ESCH"."SCH_EVENTCOST_AI0";

CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTCOST_AI0"
AFTER INSERT
ON SCH_EVENTCOST REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  v_name Varchar2(4000);
  v_protocol Number;
  v_creator Number;
  v_ipadd Varchar2(20);
  v_visit Number;
  v_cpt varchar2(50);
  v_desc  varchar2(4000);
v_line_category number;
  v_add_line boolean := false;
  v_pk_lineitem Number;
  v_calledfrom char(1);
  v_event_seq number;
  v_bgtTemplate number;
   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_EVENTCOST_AI0', pLEVEL  => Plog.LFATAL);
	v_res_costtype_pk number;
	v_apply_indirects number;
	v_soc_costpk number;

 BEGIN

  begin
	  SELECT pk_codelst INTO v_res_costtype_pk
	  FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
	  AND trim(LOWER(codelst_subtyp))='res_cost';
  exception when no_data_found then
	v_res_costtype_pk := 0;
 end;

  begin
	  SELECT pk_codelst INTO v_soc_costpk
	  FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'
            AND trim(LOWER(codelst_subtyp))='stdcare_cost';
  exception when no_data_found then
	v_soc_costpk := 0;
 end;

 BEGIN
     -- find out if cost record belongs to event_def  or event_assoc

     select name, chain_id, creator , ip_Add , fk_visit,
    event_cptcode,event_line_category,description,event_sequence
    into v_name,v_protocol,v_creator,v_ipadd,v_visit,v_cpt,v_line_category,v_desc,v_event_seq
    from event_def
    where event_id = :new.fk_event and event_type = 'A' and nvl(displacement,-99999999) <> 0;

  -- nvl for displacement because events added to 'no interval visit' has displacement as null

      v_add_line := true;

      v_calledfrom := 'L';



  exception when no_data_found then
    -- check in event_assoc

      begin

      select name, chain_id, creator , ip_Add , fk_visit,
    event_cptcode,event_line_category,description ,event_sequence, budget_template
    into v_name,v_protocol,v_creator,v_ipadd,v_visit,v_cpt,v_line_category,v_desc,v_event_seq, v_bgtTemplate
    from event_assoc
    where event_id = :new.fk_event and event_type = 'A' and nvl(displacement,-99999999) <> 0;

  -- nvl for displacement because events added to 'no interval visit' has displacement as null


        v_add_line := true;

        v_calledfrom := 'S';

    exception when others then
        v_add_line := false;
    end;


 END;

   if (nvl(v_line_category,0) <= 0 ) then
         SELECT  pk_codelst  INTO v_line_category FROM  SCH_CODELST
        WHERE trim (codelst_type) = 'category' AND trim (codelst_subtyp) = 'ctgry_patcare';
    end if;



 if (v_add_line = true  and v_bgtTemplate is not null) then 
 -- check if there is alineitem added for this event where cost is not set:
 -- CCF-FIN1 budget will be present if template is present

     begin
         select pk_lineitem
         into v_pk_lineitem
         from sch_lineitem l,sch_budget,sch_bgtcal,sch_bgtsection
         where l.fk_event = :new.fk_event and (to_number(nvl(l.lineitem_sponsorunit,0))= 0) and l.fk_bgtsection = pk_budgetsec and sch_bgtsection.fk_visit = v_visit
         and PK_BGTCAL = fk_bgtcal  and bgtcal_protid = v_protocol  and pk_budget = fk_budget and budget_calendar = v_protocol
         and rownum < 2;

     exception when no_data_found then
         v_pk_lineitem := 0;
     end ;


     if (v_pk_lineitem <= 0) then -- isnert lineitem
            PKG_BGT.sp_addToProtocolBudget( :new.fk_event,v_calledfrom,v_protocol, v_creator , v_ipAdd , v_pk_lineitem , 0 ,v_visit,
        v_name,v_cpt,v_line_category,v_desc,v_event_seq );

    --Plog.FATAL(pctx,'v_pk_lineitem  insrted');

     end if;

     --update cost

     if (v_soc_costpk = nvl(:new.fk_cost_desc,0) ) then
		v_apply_indirects := 0;
	 else
	 	v_apply_indirects := 1;
	 end if;


     update sch_lineitem
     set    LINEITEM_SPONSORUNIT    = :new.eventcost_value,
    last_modified_by = v_creator,fk_codelst_category = v_line_category,
     fk_codelst_cost_type = :new.fk_cost_desc ,lineitem_othercost = :new.eventcost_value,
      lineitem_applyindirects = v_apply_indirects
     where pk_lineitem = v_pk_lineitem;

    --Plog.FATAL(pctx,'v_pk_lineitem  updated');

 end if;

END;
/



