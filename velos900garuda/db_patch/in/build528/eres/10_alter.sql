alter table er_codelst add codelst_study_role Varchar2 (4000);

comment on column er_codelst.codelst_study_role  is 'This is used to store the logged in user''s Study Team Role';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,10,'10_alter.sql',sysdate,'8.9.0 Build#528');

commit;
