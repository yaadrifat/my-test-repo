DELETE FROM ER_REPXSL WHERE FK_REPORT = 138;
DELETE FROM ER_REPXSL WHERE FK_REPORT = 145;
DELETE FROM ER_REPXSL WHERE FK_REPORT = 155;
DELETE FROM ER_REPXSL WHERE FK_REPORT = 84;

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,6,'06_Del_ER_RepXSL.sql',sysdate,'8.9.0 Build#528');

commit;