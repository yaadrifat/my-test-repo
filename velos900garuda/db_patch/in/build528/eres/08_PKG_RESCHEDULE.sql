CREATE OR REPLACE PACKAGE "ERES"."PKG_RESCHEDULE" IS

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_RESCHEDULE', pLEVEL  => Plog.LFATAL);

/* author: Sonia Abrol
 date : 02/25/08 - package with utility procedures for patient schedule management

 There will be no commit statements in any of these procedures.
 Since these procedures can be called independently of in conjunction with other procedures, 'COMMIT' will be handled by calling code.

*/


/* SP_DISC_ALLSCHS   : Discontinue schedules for all patients on the given protocol. no commit added.
  commit to be handled by calling routine

   P_CURRENT_PROTOCOL : The Primary Key of protocol calendar
   P_DISCDATE   : Date when schedules are discontinued
   P_DISCREASON : Discontinuation Reason
   P_NEW_PROTOCOL : new protocol on which patients should be added. optional, pass 0 when patients need
        not be put on a new protocol
   P_NEW_SCHDATE_FLAG : Schedule start date option for new Protocol. optional, will be used only when  P_NEW_PROTOCOL is passed
          Possible values:

          E: Same as patient enrollment date. In this case, Generate option will only work for patients that have enrolled status added
          D: Date of Discontinuation for previous protocol
          C: Current Date
          S: Same as start date of the current protocol that is being discontinued

   P_MODIFIED_BY : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful
*/

PROCEDURE SP_DISC_ALLSCHS  (
   P_CURRENT_PROTOCOL    IN   NUMBER,
   P_DISCDATE   IN   DATE,
   P_DISCREASON IN VARCHAR2,
   P_NEW_PROTOCOL IN NUMBER,
   P_NEW_SCHDATE_FLAG IN VARCHAR2,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT NUMBER
);


/* SP_UPDATE_EVENTS  : Updates event statuses for all schedules generated for a Calendar :  will be used by both b and c checkbox options

   P_PROTOCOL : The Primary Key of protocol calendar
   P_SCHEDULE_TYPE_FLAG :flag to indicate whether the status should be updated for a 'Current' schedule or a 'discontinued' schedule.
          Possible values:  C: Currrent schedule, D: Discontinued schedule

          if P_SCHEDULE_TYPE_FLAG is 'D', and a patient has multiple discontinued schedules for the selected protocol,
           all those schedules will be affected by this update

   P_EVENT_RANGE_FLAG : Flag to indicate what scheduled events will be updated:
          Possible values: B: before the given date - P_EVENT_DATE
                           OB: on and before the given date - P_EVENT_DATE
                           A: After given date -P_EVENT_DATE
                           OA: on and After given date - P_EVENT_DATE
   P_EVENT_DATE : Event Date, to be used with parameter P_EVENT_RANGE_FLAG
   P_EVENT_STATUS : The codelist PK of the new status set for the selected events (using parameters P_EVENT_RANGE_FLAG and P_EVENT_DATE)
   P_EVENT_STATUS_DATE : The new 'event status date' for the selected events. If not passed, it will be defaulted to system date
      (using parameters P_EVENT_RANGE_FLAG and P_EVENT_DATE)
   P_MODIFIED_BY : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful
*/

PROCEDURE SP_UPDATE_EVENTS  (
   P_PROTOCOL    IN   NUMBER,
   P_SCHEDULE_TYPE_FLAG IN VARCHAR2,
   P_EVENT_RANGE_FLAG  IN VARCHAR2,
   P_EVENT_DATE  IN DATE,
   P_EVENT_STATUS IN NUMBER,
   P_EVENT_STATUS_DATE  IN DATE,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT NUMBER
);


/* SP_GENERATE_SCHEDULES : Put all patients on the selected study calendar and generate schedules. This feature will work for patients
that do not have any current schedules (or are not put on any protocol calendar on the study )

   P_PROTOCOL : The Primary Key of protocol calendar
   P_SCH_START_DATE : Schedule Start Date. This date will be used as the start date for all schedules
   P_USE_ENROLL_DATE : Flag to indicate whether 'enrollment date' should be used as start date for the schedules. Possible values:
     1:Use enrollment date,0: do not use enrollment date. when passed as 1,P_SCH_START_DATE will be ignored
     if P_USE_ENROLL_DATE is passed , schedules for only enrolled patients will be generated
   P_MODIFIED_BY : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful
*/

PROCEDURE SP_GENERATE_SCHEDULES  (
   P_PROTOCOL    IN   NUMBER,
   P_SCH_START_DATE IN DATE,
   P_USE_ENROLL_DATE IN NUMBER,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT NUMBER
);


/*
 Procedure to deactivate the current schedule. again, no commit added in the procedure, committing the transaction is calling
 routine's responsibility

 P_PROTOCOL : The Primary Key of protocol calendar
    P_ID : The PK of the patient or study. depending on the type of schedule being deactivated. Used to identify 'study admin schedule'.
      Can be passed NULL for patient schedule
    p_date :  The schedule start date for the given protocol /P_ID combination. Used to identify 'study admin schedule'.
      Can be passed NULL for patient schedule
    p_patprot : PK_PATPROT for the patient schedule. Can be passed as NULL for admin schedule
    p_sch_type : flag to indicate schedule type: S:Study Admin, P:Patient Schedule
   p_last_modified_by : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful

*/

PROCEDURE SP_DEACTIVATE_SCHEDULE(
p_protocol IN NUMBER,
p_id IN NUMBER,
p_date IN DATE,
p_patprot IN NUMBER,
p_sch_type CHAR :='',
p_last_modified_by IN NUMBER,
p_ipadd IN VARCHAR2,
o_success OUT NUMBER
);


/*
 Procedure to generate patient schedule. again, no commit added in the procedure, committing the transaction is calling
 routine's responsibility

 p_protocol   : The Primary Key of protocol calendar
 p_patientid : The Primary Key of Patient for patient schedule. for study admin schedule, pass 0
    p_date :  The schedule start date for the given protocol /p_patientid combination.
    p_patprotid  : PK_PATPROT for the patient schedule. for admin schedule pass PK of the study

   p_days : The 'Start Day' from where the schedule should be generated.
   p_dmlby : PK of user who initiated this process from application/database
   P_IPADD      : Ip address of the looged in user
   o_scuccess : Output parameter, indicates if the procedure was successful . Possible value: 0:successful;-1:unsuccessful

*/
  PROCEDURE sp_generate_schedule (
     p_protocol    IN   NUMBER,
      p_patientid   IN   NUMBER,
      p_date        IN   DATE,
      p_patprotid   IN   NUMBER,
      p_days        IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
     o_success OUT NUMBER
   );


function f_get_eventstatus_newpk return Number;

END PKG_RESCHEDULE ;
/

CREATE OR REPLACE PACKAGE BODY "ERES"."PKG_RESCHEDULE" IS

   PROCEDURE SP_DISC_ALLSCHS (
    P_CURRENT_PROTOCOL    IN   NUMBER,
   P_DISCDATE   IN   DATE,
   P_DISCREASON IN VARCHAR2,
   P_NEW_PROTOCOL IN NUMBER,
   P_NEW_SCHDATE_FLAG IN VARCHAr2,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2   ,
   o_success OUT Number
   )

   AS
   v_new_protocol Number;
   v_new_patprot Number;
   v_new_startdate Date;
   v_new_startday Number;
   v_NEW_PROTOCOL_enr Number;
    BEGIN

/*
    VARIOUS CASES Patient protoocl can be changed:
    1. if patient is not on any protocol, same record in er_patprot is updated
      In sch_events1, new schedule has status = 0

    2.Change patient's calendar/schedule start date/or simply discontinue,
    insert a new row in er_patprot with patprot_stat = 1, and update old row with discontinuation details and
    patprot_stat = 0      In sch_events1 disc old schedule, set status = 5. any messages in sch_msgtran, status set = 2

*/
 begin -- for transaction

    select decode(P_NEW_PROTOCOL,null,0,P_NEW_PROTOCOL)
    into v_NEW_PROTOCOL_enr
    from dual;

    if (v_NEW_PROTOCOL_enr <= 0) then
        v_new_startdate := null;
           v_new_startday := null;
       else
           v_new_startday := 1;
       end if;



    --get all current enrollments for the calendar to be discontinued:

    for i in (select pk_patprot ,fk_per,fk_study , patprot_start,patprot_enroldt from er_patprot
        where fk_protocol = P_CURRENT_PROTOCOL and patprot_stat = 1)
    loop
        --reset the value
        v_NEW_PROTOCOL := v_NEW_PROTOCOL_enr;
         v_new_startday := 1;

        -- discontinue schedule for old enrollment
        SP_DEACTIVATE_SCHEDULE(P_CURRENT_PROTOCOL,i.fk_per,i.patprot_start,i.pk_patprot,
            'P', P_MODIFIED_BY,P_IPADD,o_success );

        if (o_success = 0) then

            select seq_er_patprot.nextval
            into  v_new_patprot
            from dual;

            --update old patprot row with protocol discontinuation reasons

            update er_patprot
            set  PATPROT_DISCDT = P_DISCDATE, PATPROT_REASON = P_DISCREASON , patprot_stat = 0,
            last_modified_by = P_MODIFIED_BY, ip_add = P_IPADD
            where pk_patprot = i.pk_patprot;

            if (v_new_protocol >  0) then
                if (P_NEW_SCHDATE_FLAG = 'E') then
                     if (nvl(i.patprot_enroldt,to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) = to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) then
                         v_new_startdate  := null;
                        v_new_protocol := null;
                        v_new_startday := null;

                    else
                        v_new_startdate  := i.patprot_enroldt;

                    end if;

                elsif (P_NEW_SCHDATE_FLAG = 'D') then
                    v_new_startdate  := P_DISCDATE;
                elsif (P_NEW_SCHDATE_FLAG = 'C') then
                    v_new_startdate  := trunc(sysdate);
                elsif (P_NEW_SCHDATE_FLAG = 'S') then
                    v_new_startdate  := i.patprot_start;
                else
                    v_new_startdate  := null;
                    v_new_protocol := null;
                    v_new_startday := null;

                end if;
            else
                v_new_protocol := null;
                  end if;

            --insert new er_patprot record - need more columns
            Insert into er_patprot   (PK_PATPROT, FK_PROTOCOL, FK_STUDY, FK_USER, PATPROT_ENROLDT, FK_PER,
            PATPROT_START, PATPROT_NOTES, PATPROT_STAT,  CREATOR,
            CREATED_ON, IP_ADD, PATPROT_SCHDAY, FK_TIMEZONE, PATPROT_PATSTDID,
            PATPROT_CONSIGN, PATPROT_CONSIGNDT, PATPROT_RESIGNDT1, PATPROT_RESIGNDT2, FK_USERASSTO,
            FK_CODELSTLOC, PATPROT_RANDOM, PATPROT_PHYSICIAN, FK_CODELST_PTST_EVAL_FLAG, FK_CODELST_PTST_EVAL,
            FK_CODELST_PTST_INEVAL, FK_CODELST_PTST_SURVIVAL, FK_CODELST_PTST_DTH_STDREL, DEATH_STD_RLTD_OTHER,
            INFORM_CONSENT_VER, NEXT_FOLLOWUP_ON, DATE_OF_DEATH, FK_SITE_ENROLLING,PATPROT_TREATINGORG)
            Select v_new_patprot,v_NEW_PROTOCOL,  FK_STUDY,FK_USER, PATPROT_ENROLDT,
            FK_PER,v_new_startdate,PATPROT_NOTES,1,P_MODIFIED_BY,sysdate,p_ipadd,v_new_startday,FK_TIMEZONE,
            PATPROT_PATSTDID,PATPROT_CONSIGN, PATPROT_CONSIGNDT, PATPROT_RESIGNDT1, PATPROT_RESIGNDT2, FK_USERASSTO,
            FK_CODELSTLOC, PATPROT_RANDOM, PATPROT_PHYSICIAN, FK_CODELST_PTST_EVAL_FLAG, FK_CODELST_PTST_EVAL,
            FK_CODELST_PTST_INEVAL, FK_CODELST_PTST_SURVIVAL, FK_CODELST_PTST_DTH_STDREL, DEATH_STD_RLTD_OTHER,
            INFORM_CONSENT_VER, NEXT_FOLLOWUP_ON, DATE_OF_DEATH, FK_SITE_ENROLLING,PATPROT_TREATINGORG
             from er_patprot where pk_patprot = i.pk_patprot;


             --generate new schedule if protocol cal selected

             if (nvl(v_new_protocol ,0) > 0) then
                 sp_generate_schedule(v_new_protocol,i.fk_per,v_new_startdate,v_new_patprot,v_new_startday,P_MODIFIED_BY,p_ipadd,o_success);
             end if;

        end if; --o_success = 0

    end loop;
      o_success := 0;
  exception when OTHERS then
      plog.fatal(pctx,'Exception in SP_DISC_ALLSCHS:' || sqlerrm);
      o_success := -1;
  end;
  END SP_DISC_ALLSCHS;



PROCEDURE SP_UPDATE_EVENTS  (
   P_PROTOCOL    IN   NUMBER,
   P_SCHEDULE_TYPE_FLAG IN Varchar2,
   P_EVENT_RANGE_FLAG  IN Varchar2,
   P_EVENT_DATE  IN DATE,
   P_EVENT_STATUS IN NUMBER,
   P_EVENT_STATUS_DATE  IN DATE,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT Number
)
AS
  v_status Number;
  v_operator Varchar2(10);
  v_sql varchar2(4000);
  v_sql_oldstat varchar2(4000);
  v_sql_newstat varchar2(4000);
BEGIN


 if P_SCHEDULE_TYPE_FLAG = 'D' then
    v_status := 5 ;
 else --current schedule
     v_status := 0 ;
 end if;


 if P_EVENT_RANGE_FLAG = 'B' then
         v_operator := ' < ';
  elsif  P_EVENT_RANGE_FLAG = 'OB' then
         v_operator := ' <= ';
  elsif    P_EVENT_RANGE_FLAG = 'A' then
         v_operator := ' > ';
  else -- OA
        v_operator := ' >= ';
 end if;

 v_sql_oldstat := ' update sch_eventstat set EVENTSTAT_ENDDT = :dt, last_modified_by = :lm,ip_add = :ip ,
 last_modified_date =:lmd
 where  FK_EVENT in ( select event_id from sch_events1 where session_id = :protocol' ||
     ' and actual_schdate '|| v_operator ||' :evdate and status = :schstatus ) and EVENTSTAT_ENDDT is null ';

 v_sql := ' update sch_events1 set isconfirmed = :status , event_exeon = :dt , last_modified_by = :lm,ip_add = :ip,event_exeby = :eby,last_modified_date = :lmd where session_id = :protocol' ||
     ' and actual_schdate '|| v_operator ||' :evdate and status = :schstatus' ;



 v_sql_newstat := ' Insert into SCH_EVENTSTAT   (PK_EVENTSTAT, EVENTSTAT_DT, FK_EVENT,
    CREATOR, CREATED_ON, IP_ADD,    EVENTSTAT)
 Select sch_eventstat_seq.nextval, :statdt,event_id, :creator, :createdon, :ip,  :status from
    sch_events1 where session_id = :protocol' ||
     ' and actual_schdate '|| v_operator ||' :evdate and status = :schstatus  ';


 begin
    --update old status
    --JM: 29Oct2008: Modified the following line
    --execute immediate v_sql_oldstat using P_EVENT_STATUS_DATE,P_MODIFIED_BY,p_ipadd,sysdate, P_PROTOCOL,  P_EVENT_DATE ,v_status;
    execute immediate v_sql_oldstat using sysdate,P_MODIFIED_BY,p_ipadd,sysdate, P_PROTOCOL,  P_EVENT_DATE ,v_status;

     execute immediate v_sql using P_EVENT_STATUS,P_EVENT_STATUS_DATE,P_MODIFIED_BY,p_ipadd,P_MODIFIED_BY, sysdate, P_PROTOCOL,  P_EVENT_DATE ,v_status;

    --insert new status history

    execute immediate v_sql_newstat using P_EVENT_STATUS_DATE,
    P_MODIFIED_BY,sysdate,p_ipadd,P_EVENT_STATUS ,P_PROTOCOL,P_EVENT_DATE, v_status;

     o_success := 0;

 exception when OTHERS then
     plog.fatal(pctx,'Exception in SP_UPDATE_EVENTS:' || sqlerrm);
    plog.fatal(pctx,'Exception in SP_UPDATE_EVENTS:v_sql_newstat' || v_sql_newstat);
    plog.fatal(pctx,'Exception in SP_UPDATE_EVENTS:v_old_newstat' || v_sql_oldstat);
      o_success := -1;
 end;

END SP_UPDATE_EVENTS;


PROCEDURE SP_GENERATE_SCHEDULES  (
   P_PROTOCOL    IN   NUMBER,
   P_SCH_START_DATE IN DATE,
   P_USE_ENROLL_DATE IN NUMBER,
   P_MODIFIED_BY       IN   NUMBER ,
   P_IPADD       IN   VARCHAR2,
   o_success OUT Number
)
AS
    /* generates schedules for the patients that are not on any current protocol calendar*/
    v_study Number;
    v_start_date Date;

BEGIN

 begin
    -- get study
    select chain_id
    into v_study
    from EVENT_ASSOC
    where event_id = P_PROTOCOL AND UPPER (event_type) = 'P';

    for i in (select pk_patprot ,fk_per,fk_study , patprot_start,patprot_enroldt from er_patprot
        where fk_study = v_study and fk_protocol is null and patprot_stat = 1)
    loop
        --update this record

        if P_USE_ENROLL_DATE = 1 then
            v_start_date := i.patprot_enroldt;
        else
            v_start_date := P_SCH_START_DATE;
        end if;

        if (nvl(v_start_date,to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) <>  to_date(PKG_DATEUTIL.f_get_null_date_str,PKG_DATEUTIL.F_GET_DATEFORMAT)) then
             update er_patprot
            set  fk_protocol = P_PROTOCOL, patprot_start = v_start_date, PATPROT_SCHDAY = 1,
                    last_modified_by = P_MODIFIED_BY, ip_add = P_IPADD
            where pk_patprot = i.pk_patprot;


            -- generate schedule
            sp_generate_schedule(P_PROTOCOL,i.fk_per,v_start_date,i.pk_patprot,1,P_MODIFIED_BY,p_ipadd,o_success);

        end if;
    end loop;
    o_success := 0;
 exception when NO_DATA_FOUND then
     plog.fatal(pctx,'Exception in SP_GENERATE_SCHEDULES:' || sqlerrm);
      o_success := -1;
 end;

END SP_GENERATE_SCHEDULES;

PROCEDURE SP_DEACTIVATE_SCHEDULE(
p_protocol IN NUMBER,
p_id IN NUMBER,
p_date IN DATE,
p_patprot IN NUMBER,
p_sch_type CHAR :='',
p_last_modified_by IN NUMBER,
p_ipadd in Varchar2,
o_success OUT NUMBER
)
/**
Procedure to deactivate the current schedule
**/
AS
BEGIN
    begin
        IF (p_sch_type = 'P') THEN -- patient schedule

            --discontinue old schedule
            UPDATE SCH_EVENTS1 SET status = 5,
            last_modified_date = SYSDATE WHERE  fk_patprot = p_patprot;

            --delete generated emails where emails are not yet sent
            DELETE FROM  SCH_DISPATCHMSG
            WHERE fk_schevent is not null and fk_patprot = p_patprot and nvl(msg_status,0) = 0;

            --update flag for 'transient emails' so that they don't get sent in clubbed email
            UPDATE SCH_MSGTRAN
            SET msg_status= 2
            WHERE fk_patprot=p_patprot and fk_schevent is not null;
         ELSE  --study schedule

            DELETE FROM  SCH_DISPATCHMSG
            WHERE fk_schevent IN ( SELECT event_id
                                   FROM SCH_EVENTS1
                                    WHERE fk_study= (p_id)
                                    AND SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND
                              status=0) and nvl(msg_status,0) = 0;


            UPDATE SCH_EVENTS1 SET status = 5, last_modified_date = SYSDATE WHERE  fk_study= p_id  AND
            SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND status=0 ;

            UPDATE SCH_MSGTRAN
            SET msg_status= 2
            WHERE fk_schevent IN ( SELECT event_id
                                   FROM SCH_EVENTS1
                                    WHERE fk_study= (p_id)
                                    AND SESSION_ID= LPAD(TO_CHAR(p_protocol),10,'0') AND
                               status=0 ) ;

         END IF;

        o_success := 0;
    exception when OTHERS then
        o_success := -1;
        plog.fatal(pctx,'Exception in SP_DEACTIVATE_SCHEDULE:' || sqlerrm);
    end;

END;


PROCEDURE sp_generate_schedule(p_protocol    IN   NUMBER,
      p_patientid   IN   NUMBER,
      p_date        IN   DATE,
      p_patprotid   IN   NUMBER,
      p_days        IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
      o_success OUT Number
)
AS

v_study            NUMBER;
v_notdone          NUMBER;
vcursch            NUMBER          := 0;
v_calassoc         CHAR (1);
v_protocol_study   NUMBER;
v_patprot          NUMBER;
v_err VARCHAR(4000);
v_mindisp NUMBER;
v_days NUMBER;
BEGIN
	--Check if the schedule is generated from Study admin. If passed from Study admin calendar parameter will contains different values
	-- fk_patprot will contain study primary key if called from study admin calendar. If called from patient schedule, it will contain patprot Id.
	--PatientId is not needed for studyadmin schedule,so will contain 0
	BEGIN
		v_patprot := p_patprotid;

		SELECT event_calassocto, chain_id
		INTO v_calassoc, v_study
		FROM EVENT_ASSOC
		WHERE event_id = p_protocol AND UPPER (event_type) = 'P';

		v_protocol_study := v_study;

		IF (p_days=1) THEN
			SELECT MIN(displacement) INTO v_mindisp FROM EVENT_ASSOC WHERE chain_id=p_protocol AND UPPER(event_type)='A';

			IF (v_mindisp<0) THEN
				v_days:=v_mindisp;
			ELSE
				v_days:=p_days;
			END IF;
			--KM, 28Sep09 -- For No interval defined visits.
		ELSIF (p_days=0) THEN
			v_days := null;
		ELSE
			v_days:=p_days;
		END IF;


		IF (v_calassoc = 'S')  THEN
			SELECT COUNT (1)
			INTO vcursch
			FROM SCH_EVENTS1
			WHERE fk_study = v_study
			AND session_id=LPAD(TO_CHAR(p_protocol),10,'0')
			AND status = 0;

			v_patprot := 0;

		ELSE
			v_protocol_study := 0;

			SELECT COUNT (1)
			INTO vcursch
			FROM SCH_EVENTS1
			WHERE fk_patprot = v_patprot AND status = 0;
		END IF;


		IF vcursch = 0 THEN

			SELECT pk_codelst
			INTO v_notdone
			FROM SCH_CODELST
			WHERE codelst_subtyp = 'ev_notdone';

			INSERT INTO SCH_EVENTS1 ( EVENT_ID, LOCATION_ID, NOTES, DESCRIPTION, BOOKED_BY,
				START_DATE_TIME, END_DATE_TIME, PATIENT_ID, OBJECT_ID, STATUS, TYPE_ID,
				SESSION_ID, VISIT_TYPE_ID, FK_ASSOC, ISCONFIRMED, BOOKEDON, CREATOR , LAST_MODIFIED_BY ,
				LAST_MODIFIED_DATE ,CREATED_ON, IP_ADD, FK_PATPROT,ACTUAL_SCHDATE, VISIT,
				FK_VISIT,event_notes ,fk_study ,EVENT_SEQUENCE,
				SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE)
			SELECT lpad (to_char (SCH_EVENTS1_SEQ1.NEXTVAL), 10, '0'), '173', null,NAME, '0000000525' ,
				--((p_date + DISPLACEMENT) - 1) , --KM
				--KM-Next 2 lines commented for #4372
				--case when (select no_interval_flag FROM SCH_PROTOCOL_VISIT
				--where fk_protocol = p_protocol and pk_protocol_visit = fk_visit) = 1 then (p_date+ (select max(nvl(displacement,0)) from sch_protocol_visit where fk_protocol= p_protocol) ) else ((p_date + nvl(DISPLACEMENT,0)) - 1) end ,
				case when (select no_interval_flag FROM SCH_PROTOCOL_VISIT
				where fk_protocol = p_protocol and pk_protocol_visit = fk_visit) = 1 then (p_date) else ((p_date + nvl(DISPLACEMENT,0)) - 1) end ,
				--(( p_date + DISPLACEMENT) - 1) + DURATION,
				(( p_date + nvl(DISPLACEMENT,0)) - 1) + DURATION,
				lpad (to_char (p_patientid), 10, '0'), '0000001476', 0, '165',
				lpad (to_char (p_protocol), 10, '0'), FUZZY_PERIOD, EVENT_ID,
				v_notdone , p_date , p_dmlby , NULL, NULL , SYSDATE , p_ipadd , v_patprot,
				--((p_date + DISPLACEMENT) - 1),
				case when (select no_interval_flag FROM SCH_PROTOCOL_VISIT
				where fk_protocol = p_protocol and pk_protocol_visit = fk_visit ) = 1 then null else ((p_date + nvl(DISPLACEMENT,0)) - 1) end ,
				(select act_num from(select rownum act_num,a.* from( (SELECT pk_protocol_visit, visit_no, displacement FROM SCH_PROTOCOL_VISIT
				where fk_protocol = p_protocol order by displacement asc, visit_no)) a)b where pk_protocol_visit = fk_visit) visit, 
				fk_visit ,notes,v_protocol_study,assoc.EVENT_SEQUENCE,
				SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
				FROM EVENT_ASSOC  assoc
				WHERE CHAIN_ID = p_protocol
				--and EVENT_TYPE = 'A' and displacement <> 0 and DISPLACEMENT >=  v_days
				and EVENT_TYPE = 'A' and nvl(displacement,-1) <> 0 and ( DISPLACEMENT >=  v_days or DISPLACEMENT is null)
				and (HIDE_FLAG is null or HIDE_FLAG <>1)   ;
				--plog.fatal(pctx,'new proc:' );
				--
				--and nvl(displacement,-1) <> 0 added by sonia, without this cluase 'selected --events' were also pulled in schedule

			IF v_calassoc = 'S' THEN
				-- for admin schedule
				----KLUDGE     -- commit needs to be removed from pkg_gensch.sp_admin_msg_to_tran
				Pkg_Gensch.sp_admin_msg_to_tran (p_protocol, v_study , p_dmlby,    p_ipadd );
			ELSE -- patient schedule
				-- for clubbed notifications
				----KLUDGE     -- commit needs to be removed from pkg_gensch.sp_admin_msg_to_tran
				Pkg_Gensch.sp_addmsg_to_tran (p_patientid,v_study,p_dmlby,p_ipadd,v_patprot);

				Sp_Copyprotcrf (v_patprot, p_dmlby, p_ipadd);

				--change status of mails according to current alert/notify setting
				-- KLUDGE --commit needs to be removed from pkg_gensch.sp_setmailstat
				Pkg_Gensch.sp_setmailstat (v_patprot, p_dmlby, p_ipadd);
			END IF;

		END IF;
		o_success := 0;
		EXCEPTION  WHEN OTHERS    THEN
			v_err := SQLERRM;
			o_success := -1;
			plog.fatal(pctx,'Exception in sp_generate_schedule:' || v_err);

	END;
END sp_generate_schedule ;

function f_get_eventstatus_newpk return Number
as
v_newpk number;
    begin
    select sch_eventstat_seq.nextval
    into  v_newpk from dual;

    return v_newpk;

end;


END Pkg_Reschedule;
/

 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,8,'08_PKG_RESCHEDULE.sql',sysdate,'8.9.0 Build#528');

commit;



