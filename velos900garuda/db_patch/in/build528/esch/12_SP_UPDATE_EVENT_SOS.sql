CREATE OR REPLACE PROCEDURE  "ESCH"."SP_UPDATE_EVENT_SOS" (
   p_eventId NUMBER ,
   p_eventSOSId NUMBER
)
AS

/** Author: Sammie Mhalagi 05/03/2010
**  This procedure updates the events Site of Service.
** the input parameters are Event Id and Site of Service id 
**
** Modification History
**
*/

BEGIN
	IF (p_eventSOSId = 0) THEN
		Update sch_events1 set SERVICE_SITE_ID = NULL WHERE F_TO_NUMBER(EVENT_ID) = p_eventId;
	ELSE
		Update sch_events1 set SERVICE_SITE_ID = p_eventSOSId WHERE F_TO_NUMBER(EVENT_ID) = p_eventId;
	END IF;
	COMMIT;
END ;
/


CREATE SYNONYM ERES.SP_UPDATE_EVENT_SOS FOR SP_UPDATE_EVENT_SOS;

CREATE SYNONYM EPAT.SP_UPDATE_EVENT_SOS FOR SP_UPDATE_EVENT_SOS;

GRANT EXECUTE, DEBUG ON SP_UPDATE_EVENT_SOS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_UPDATE_EVENT_SOS TO ERES;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,12,'12_SP_UPDATE_EVENT_SOS.sql',sysdate,'8.9.0 Build#528');

commit;
