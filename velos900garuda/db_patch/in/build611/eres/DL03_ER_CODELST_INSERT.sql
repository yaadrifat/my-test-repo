set define off;


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_visible'
    AND codelst_subtyp = 'public';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'note_visible','public','Public','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_visible'
    AND codelst_subtyp = 'private';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'note_visible','private','Private','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_type'
    AND codelst_subtyp = 'CO';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_type','CO','Customer Order(CO)','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_type'
    AND codelst_subtyp = 'FO';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_type','FO','Fulfillment Order','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_type'
    AND codelst_subtyp = 'SO';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_type','SO','Supplier Order(SO)','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_stts'
    AND codelst_subtyp = 'OP';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_stts','OP','OPEN','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_stts'
    AND codelst_subtyp = 'PN';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_stts','PN','PENDING','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_stts'
    AND codelst_subtyp = 'RS';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_stts','RS','RESOLVED','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_stts'
    AND codelst_subtyp = 'RJ';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_stts','RJ','REJECTED','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_hdr_stts'
    AND codelst_subtyp = 'CL';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'order_hdr_stts','CL','CLOSED','N',NULL,SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--END--

insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'eligibile_cat','mrq_attach','Maternal Risk Questionnaire Attachment','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
commit;
