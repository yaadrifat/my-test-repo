<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">


<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

	</HEAD>

<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >

<tr class="reportGreyRow">
<td class="reportPanel"> 


Download the report in: 
<A href='{$wd}' >
Word Format
</A> 
<xsl:text>&#xa0;</xsl:text> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
Excel Format
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
Printer Friendly Format
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$pd}' >
PDF Format
</A> 

</td>

</tr>
</table>
</xsl:if>
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><xsl:value-of select="$repName" />
</b>
</td>
</tr>

<tr><td>&#xa0;</td></tr> 
<tr>
<td class="reportData" WIDTH="30%" ALIGN="CENTER">
Selected Filter(s): <xsl:value-of select="$argsStr" />
</td></tr>
</table>

<TABLE WIDTH="100%" BORDER="1">

<TR>
<TH class="reportHeading" WIDTH="12%" align="CENTER">
 <b>
Registry Id </b>
</TH>
<TH class="reportHeading" WIDTH="12%" align="CENTER">
<b>ISBT DIN
</b>
</TH>
<TH class="reportHeading" WIDTH="15%" align="CENTER" >
<b>ISBT Product Code
</b>
</TH>
<TH class="reportHeading" WIDTH="5%" align="CENTER"><b>TNC Frozen
</b>
</TH>
<TH class="reportHeading" WIDTH="20%" align="CENTER">
<b>Licensure Status
</b>
</TH>
<TH class="reportHeading" WIDTH="16%" align="CENTER">
<b>Eligible Status
</b>
</TH>
<TH class="reportHeading" WIDTH="10%" align="CENTER">
<b>External CBU ID
</b>
</TH>
<TH class="reportHeading" WIDTH="10%" align="CENTER">
<b>CBU Local ID
</b>
</TH>

</TR>
<xsl:apply-templates select="ROW"/>
</TABLE>
<hr class="thickLine" />
<TABLE WIDTH="100%" >

<TR>

<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
Report By:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
Date:<xsl:value-of select="$repDate" />
</TD>

</TR>
</TABLE>

<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 

<xsl:template match="ROW">
<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >

<TR class="reportEvenRow" valign = "top" >

<TD class="reportData" align="left" >
<xsl:value-of select="CORD_REGISTRY_ID" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_ISBI_DIN_CODE" />
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="CORD_ISIT_PRODUCT_CODE" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_TNC_FROZEN" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="LICENCE_STATUS" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="ELIGIBLE_STATUS" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_EXTERNAL_CBU_ID" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_LOCAL_CBU_ID" />
</TD>
</TR>
</xsl:when> 
<xsl:otherwise>
<TR class="reportOddRow">
<TD class="reportData" align="left" >
<xsl:value-of select="CORD_REGISTRY_ID" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_ISBI_DIN_CODE" />
</TD>
<TD class="reportData" align="left" >
<xsl:value-of select="CORD_ISIT_PRODUCT_CODE" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_TNC_FROZEN" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="LICENCE_STATUS" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="ELIGIBLE_STATUS" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_EXTERNAL_CBU_ID" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="CORD_LOCAL_CBU_ID" />
</TD>
</TR>
</xsl:otherwise>
</xsl:choose> 
</xsl:template> 
</xsl:stylesheet>