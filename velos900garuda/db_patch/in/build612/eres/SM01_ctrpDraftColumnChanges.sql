set define off;

ALTER TRIGGER ERES."ER_CTRP_DRAFT_BI0" DISABLE;

ALTER TRIGGER ERES."ER_CTRP_DRAFT_AU0" DISABLE;

ALTER TRIGGER ERES."ER_CTRP_DRAFT_AD0" DISABLE;

declare
v_column_exists number := 0;
begin

	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SUBMIT_ORG_NAME'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT rename column SUBMIT_ORG_NAME to SUBMIT_ORG_FK_SITE';
		dbms_output.put_line('Column SUBMIT_ORG_NAME renamed to SUBMIT_ORG_FK_SITE');
	else
		dbms_output.put_line('Column SUBMIT_ORG_NAME does not exist in table ER_CTRP_DRAFT');
	end if;
	
	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SPONSOR_CTRP_ID'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT rename column SPONSOR_CTRP_ID to SPONSOR_CTRPID';
		dbms_output.put_line('Column SPONSOR_CTRP_ID renamed to SPONSOR_CTRPID');
	else
		dbms_output.put_line('Column SPONSOR_CTRP_ID does not exist in table ER_CTRP_DRAFT');
	end if;
	
	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SPONSOR_PERSONAL_MIDDLE_NAME'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT rename column SPONSOR_PERSONAL_MIDDLE_NAME to SPONSOR_PERSONAL_FK_USER';
		dbms_output.put_line('Column SPONSOR_PERSONAL_MIDDLE_NAME renamed to SPONSOR_PERSONAL_FK_USER');
	else
		dbms_output.put_line('Column SPONSOR_PERSONAL_MIDDLE_NAME does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------

	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SUMM4_SPONSOR_NAME'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT rename column SUMM4_SPONSOR_NAME to SUMM4_SPONSOR_FK_SITE';
		dbms_output.put_line('Column SUMM4_SPONSOR_NAME renamed to SUMM4_SPONSOR_FK_SITE');
	else
		dbms_output.put_line('Column SUMM4_SPONSOR_NAME does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
	AND COLUMN_NAME = 'SPONSOR_NAME'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT rename column SPONSOR_NAME to SPONSOR_FK_SITE';
		dbms_output.put_line('Column SPONSOR_NAME renamed to SPONSOR_FK_SITE');
	else
		dbms_output.put_line('Column SPONSOR_NAME does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------
end;
/

Update ER_CTRP_DRAFT set SUBMIT_ORG_FK_SITE = null;

Update ER_CTRP_DRAFT set SUBMIT_ORG_CTRPID = null;

Update ER_CTRP_DRAFT set SPONSOR_CTRPID = null;

Update ER_CTRP_DRAFT set SPONSOR_PERSONAL_FK_USER = null;

Update ER_CTRP_DRAFT set SUMM4_SPONSOR_FK_SITE = null;

Update ER_CTRP_DRAFT set SPONSOR_FK_SITE = null;

Update ER_CTRP_DRAFT set RP_SPONSOR_CONTACT_ID = null;

commit;

COMMENT ON COLUMN ER_CTRP_DRAFT.SUBMIT_ORG_FK_SITE 
 IS 'Foreign Key for ER_SITE. Identifies Submitting Organization';

COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_PERSONAL_FK_USER 
 IS 'Foreign Key for ER_USER. Identifies Sponsor Personal Contact';
 
COMMENT ON COLUMN ER_CTRP_DRAFT.SUMM4_SPONSOR_FK_SITE 
 IS 'Foreign Key for ER_SITE. Identifies Summary4 Sponsor';

COMMENT ON COLUMN ER_CTRP_DRAFT.SPONSOR_FK_SITE 
 IS 'Foreign Key for ER_SITE. Identifies Sponsor';

declare
v_column_exists number := 0;
begin

	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SUBMIT_ORG_FK_SITE'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT modify (SUBMIT_ORG_FK_SITE NUMBER)';
		dbms_output.put_line('Column SUBMIT_ORG_FK_SITE is now a number');
	else
		dbms_output.put_line('Column SUBMIT_ORG_FK_SITE does not exist in table ER_CTRP_DRAFT');
	end if;
	
	--------------------------------------------------------------------------------------------------------------------------
		
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SUBMIT_ORG_CTRPID'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT modify (SUBMIT_ORG_CTRPID VARCHAR2(50))';
		dbms_output.put_line('Column SUBMIT_ORG_CTRPID is now a varchar2(50)');
	else
		dbms_output.put_line('Column SUBMIT_ORG_CTRPID does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SPONSOR_PERSONAL_FK_USER'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT modify (SPONSOR_PERSONAL_FK_USER NUMBER)';
		dbms_output.put_line('Column SPONSOR_PERSONAL_FK_USER is now a number');
	else
		dbms_output.put_line('Column SPONSOR_PERSONAL_FK_USER does not exist in table ER_CTRP_DRAFT');
	end if;
	
	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SUMM4_SPONSOR_FK_SITE'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT modify (SUMM4_SPONSOR_FK_SITE NUMBER)';
		dbms_output.put_line('Column SUMM4_SPONSOR_FK_SITE is now a number');
	else
		dbms_output.put_line('Column SUMM4_SPONSOR_FK_SITE does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SPONSOR_FK_SITE'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT modify (SPONSOR_FK_SITE NUMBER)';
		dbms_output.put_line('Column SPONSOR_FK_SITE is now a number');
	else
		dbms_output.put_line('Column SPONSOR_FK_SITE does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'SPONSOR_CTRPID'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT modify (SPONSOR_CTRPID VARCHAR2(50))';
		dbms_output.put_line('Column SPONSOR_CTRPID is now a varchar2(50)');
	else
		dbms_output.put_line('Column SPONSOR_CTRPID does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------
	
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRP_DRAFT'
    AND COLUMN_NAME = 'RP_SPONSOR_CONTACT_ID'; 
	
	if (v_column_exists = 1) then
		execute immediate 'alter table ER_CTRP_DRAFT modify (RP_SPONSOR_CONTACT_ID VARCHAR2(50))';
		dbms_output.put_line('Column RP_SPONSOR_CONTACT_ID is now a varchar2(50)');
	else
		dbms_output.put_line('Column RP_SPONSOR_CONTACT_ID does not exist in table ER_CTRP_DRAFT');
	end if;

	--------------------------------------------------------------------------------------------------------------------------

end;
/

