set define off;

-- Create Funding Guidelines Table
CREATE TABLE CB_FUNDING_GUIDELINES(
					PK_FUNDING_GUIDELINE NUMBER(10) Primary Key,
					FK_CBB_ID NUMBER(10),
					FUNDING_ID VARCHAR2(50),
					FK_FUND_CATEG NUMBER(10),
					FUNDING_BDATE DATE,
					DESCRIPTION VARCHAR2(255),
					FUNDING_STATUS VARCHAR2(25),
					CREATOR NUMBER(10),
					CREATED_ON DATE,
					IP_ADD VARCHAR2(15),
					LAST_MODIFIED_BY NUMBER(10),
					LAST_MODIFIED_ON DATE,
					RID NUMBER(10),
					DELETEDFLAG VARCHAR2(1)
				);

COMMENT ON TABLE "CB_FUNDING_GUIDELINES" IS 'This table will store the Funding Guidelines';
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."PK_FUNDING_GUIDELINE" IS 'Primary Key of the table'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."FK_CBB_ID" IS 'Reference to CBB'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."FK_FUND_CATEG" IS 'This column stores the fund race rollup possible values are defined in code list'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."FUNDING_BDATE" IS 'Date on which cord will be available'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."DESCRIPTION" IS 'Remarks/Comments given.'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."FUNDING_STATUS" IS 'Status of Funding Guidelin(active/inactive)';	 

COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."LAST_MODIFIED_ON" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_FUNDING_GUIDELINES"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE "SEQ_CB_FUNDING_GUIDELINES" MINVALUE 1 MAXVALUE 999999999999999999999999999 
INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;



													------------------End-----------------
													
													

-- Create Funding Guidelines Table
CREATE TABLE CB_FUNDING_SCHEDULE(
									PK_FUNDING_SCHEDULE NUMBER(10) Primary Key,
									FK_CBB_ID NUMBER(10),
								    DATE_TO_GENERATE DATE,
									CBB_REGISTRATION_DATE DATE,
									NOTIFICATION_BEFORE NUMBER,
									GENERATED_STATUS VARCHAR2(50),
									CREATOR NUMBER(10),
									CREATED_ON DATE,
									IP_ADD VARCHAR2(15),
									LAST_MODIFIED_BY NUMBER(10),
									LAST_MODIFIED_ON DATE,
									RID NUMBER(10),
									DELETEDFLAG VARCHAR2(1)
								);

COMMENT ON TABLE "CB_FUNDING_SCHEDULE" IS 'This table will store the Funding Schedule';
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."PK_FUNDING_SCHEDULE" IS 'Primary Key of the table'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."FK_CBB_ID" IS 'Reference to CBB'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."DATE_TO_GENERATE" IS 'Date on which Report will be generated'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."CBB_REGISTRATION_DATE" IS 'Date on which cord will be available'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."NOTIFICATION_BEFORE" IS 'Remarks/Comments given.'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."GENERATED_STATUS" IS 'Status of Funding Guidelin(active/inactive)';	 

COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."LAST_MODIFIED_ON" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_FUNDING_SCHEDULE"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 


CREATE SEQUENCE "SEQ_CB_FUNDING_SCHEDULE" MINVALUE 1 MAXVALUE 999999999999999999999999999 
INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;



CREATE TABLE ER_ORDER_USERS(
	PK_ORDER_USER NUMBER(10,0),
	USER_LOGINID VARCHAR2(50 BYTE),
	USER_FNAME VARCHAR2(50 BYTE),
	USER_LNAME VARCHAR2(50 BYTE),
	USER_MAILID VARCHAR2(50 BYTE),
	USER_CONTACTNO VARCHAR2(50 BYTE),
	USER_COMMENTS VARCHAR2(255 BYTE),
	CREATOR	NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15 BYTE),
	DELETEDFLAG NUMBER(10,0),
	RID NUMBER(10,0)
);

COMMENT ON TABLE ER_ORDER_USERS IS 'Stores Case Management User details';

COMMENT ON COLUMN ER_ORDER_USERS.PK_ORDER_USER IS 'Stores sequence generated unique value';
COMMENT ON COLUMN ER_ORDER_USERS.USER_LOGINID IS 'Stores Case Management Login Id of the user';
COMMENT ON COLUMN ER_ORDER_USERS.USER_FNAME IS 'Stores User first name';
COMMENT ON COLUMN ER_ORDER_USERS.USER_LNAME IS 'Stores User last name';
COMMENT ON COLUMN ER_ORDER_USERS.USER_MAILID IS 'Stores User mail id';
COMMENT ON COLUMN ER_ORDER_USERS.USER_CONTACTNO IS 'Stores User contact no';
COMMENT ON COLUMN ER_ORDER_USERS.USER_COMMENTS IS 'Stores comments if any';
COMMENT ON COLUMN ER_ORDER_USERS.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN ER_ORDER_USERS.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN ER_ORDER_USERS.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN ER_ORDER_USERS.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN ER_ORDER_USERS.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN ER_ORDER_USERS.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN ER_ORDER_USERS.RID IS 'Uses for Audit trail. Stores sequence generated unique value';



CREATE SEQUENCE SEQ_ER_ORDER_USERS MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;




CREATE TABLE "CB_DUMN"
   (    "PK_DUMN" NUMBER(10,0) Primary Key,
    "MRQ_QUES_1" VARCHAR2(1 BYTE),
    "MRQ_QUES_1_A" VARCHAR2(1 BYTE),
    "MRQ_QUES_1_B" VARCHAR2(1 BYTE),
    "MRQ_QUES_1_AD_DETAIL" VARCHAR2(100 CHAR),
    "MRQ_QUES_2" VARCHAR2(1 BYTE),
    "MRQ_QUES_2_AD_DETAIL" VARCHAR2(100 CHAR),
    "PHY_FIND_QUES_3" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_A" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_B" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_C" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_D" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_AD_DETAIL" VARCHAR2(100 CHAR),
    "PHY_FIND_QUES_4" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_4_AD_DETAIL" VARCHAR2(100 CHAR),
    "PHY_FIND_QUES_5" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_A" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_B" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_C" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_D" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_E" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_F" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_G" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_H" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_I" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_AD_DETAIL" VARCHAR2(100 CHAR),
    "IDM_QUES_6" VARCHAR2(1 BYTE),
    "IDM_QUES_6_A" VARCHAR2(1 BYTE),
    "IDM_QUES_6_B" VARCHAR2(1 BYTE),
    "IDM_QUES_6_AD_DETAIL" VARCHAR2(100 CHAR),
    "IDM_QUES_7" VARCHAR2(1 BYTE),
    "IDM_QUES_7_A" VARCHAR2(1 BYTE),
    "IDM_QUES_7_B" VARCHAR2(1 BYTE),
    "IDM_QUES_7_AD_DETAIL" VARCHAR2(100 CHAR),
    "IDM_QUES_8" VARCHAR2(1 BYTE),
    "IDM_QUES_8_AD_DETAIL" VARCHAR2(100 CHAR),
    "OTHER_QUES_9" VARCHAR2(1 BYTE),
    "OTHER_QUES_9_A" VARCHAR2(1 BYTE),
    "OTHER_QUES_9_B" VARCHAR2(1 BYTE),
    "OTHER_QUES_10" VARCHAR2(1 BYTE),
    "OTHER_QUES_10_AD_DETAIL" VARCHAR2(100 CHAR),
    "CREATOR" NUMBER(10,0),
    "CREATED_ON" DATE,
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "IP_ADD" VARCHAR2(15 BYTE),
    "DELETEDFLAG" NUMBER(22,0),
    "RID" NUMBER(15,0),
    "ENTITY_ID" NUMBER(10,0),
    "ENTITY_TYPE" NUMBER(20,0));

   COMMENT ON TABLE "CB_DUMN"  IS 'This table stores question response realted to dumn.';

   COMMENT ON COLUMN "CB_DUMN"."PK_DUMN" IS 'Primary Key';

 COMMENT ON COLUMN "CB_DUMN"."MRQ_QUES_1" IS 'This column will store MRQ Question 1';

 COMMENT ON COLUMN "CB_DUMN"."MRQ_QUES_1_A" IS 'This column will store MRQ Question 1 A';

 COMMENT ON COLUMN "CB_DUMN"."MRQ_QUES_1_B" IS 'This column will store MRQ Question 1 B';

 COMMENT ON COLUMN "CB_DUMN"."MRQ_QUES_1_AD_DETAIL" IS 'This column will store MRQ Question AD details';

 COMMENT ON COLUMN "CB_DUMN"."MRQ_QUES_2" IS 'This column will store MRQ Question 2';

 COMMENT ON COLUMN "CB_DUMN"."MRQ_QUES_2_AD_DETAIL" IS 'This column will store MRQ Question 2 AD details';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_3" IS 'This column will store Physical finding Question 3';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_3_A" IS 'This column will store Physical finding Question 3 A';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_3_B" IS 'This column will store Physical finding Question 3 B';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_3_C" IS 'This column will store Physical finding Question 3 C';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_3_D" IS 'This column will store Physical finding Question 3 D';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_3_AD_DETAIL" IS 'This column will store Physical finding Question 3 AD Detail';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_4" IS 'This column will store Physical finding Question 4';
 
 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_4_AD_DETAIL" IS 'This column will store Physical finding Question 4 AD Detail';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_A" IS 'This column will store Physical finding Question 5 A';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_B" IS 'This column will store Physical finding Question 5 B';
 
 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_C" IS 'This column will store Physical finding Question 5 C';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_D" IS 'This column will store Physical finding Question 5 D';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_E" IS 'This column will store Physical finding Question 5 E';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_F" IS 'This column will store Physical finding Question 5 F';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_G" IS 'This column will store Physical finding Question 5 G';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_H" IS 'This column will store Physical finding Question 5 H';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_I" IS 'This column will store Physical finding Question 5 I';

 COMMENT ON COLUMN "CB_DUMN"."PHY_FIND_QUES_5_AD_DETAIL" IS 'This column will store Physical finding Question 5 AD Detail';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_6" IS 'This column will store IDM Question 6';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_6_A" IS 'This column will store IDM Question 6 A';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_6_AD_DETAIL" IS 'This column will store IDM Question 6 AD Detail';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_7_A" IS 'This column will store IDM Question 7 A';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_7_B" IS 'This column will store IDM Question 7 B';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_7_AD_DETAIL" IS 'This column will store IDM Question 7 AD Detail';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_8" IS 'This column will store IDM Question 8';

 COMMENT ON COLUMN "CB_DUMN"."IDM_QUES_8_AD_DETAIL" IS 'This column will store IDM Question 8 AD Detail';

 COMMENT ON COLUMN "CB_DUMN"."OTHER_QUES_9" IS 'This column will store Other  Question 9';

 COMMENT ON COLUMN "CB_DUMN"."OTHER_QUES_9_A" IS 'This column will store Other  Question 9 A';

 COMMENT ON COLUMN "CB_DUMN"."OTHER_QUES_9_B" IS 'This column will store Other  Question 9 B';

 COMMENT ON COLUMN "CB_DUMN"."OTHER_QUES_10" IS 'This column will store Other  Question 10';

 COMMENT ON COLUMN "CB_DUMN"."OTHER_QUES_10_AD_DETAIL" IS 'This column will store Other  Question 10 AD Detail';

   COMMENT ON COLUMN "CB_DUMN"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';

   COMMENT ON COLUMN "CB_DUMN"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';

   COMMENT ON COLUMN "CB_DUMN"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';

   COMMENT ON COLUMN "CB_DUMN"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';

   COMMENT ON COLUMN "CB_DUMN"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';

   COMMENT ON COLUMN "CB_DUMN"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';

   COMMENT ON COLUMN "CB_DUMN"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

   COMMENT ON COLUMN "CB_DUMN"."ENTITY_ID" IS 'pk of the entity';

   COMMENT ON COLUMN "CB_DUMN"."ENTITY_TYPE" IS 'Id of the entity like CBU, Doner etc';

    CREATE SEQUENCE SEQ_CB_DUMN MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;


    CREATE TABLE "CB_FINAL_DECL_ELIG"
   (    "PK_FINAL_DECL_ELIG" NUMBER(10,0) Primary Key,
    "MRQ_QUES_1" VARCHAR2(1 BYTE),
    "MRQ_QUES_1_A" VARCHAR2(1 BYTE),
    "MRQ_QUES_1_B" VARCHAR2(1 BYTE),
    "MRQ_QUES_1_AD_DETAIL" VARCHAR2(100 CHAR),
    "MRQ_QUES_2" VARCHAR2(1 BYTE),
    "MRQ_QUES_2_AD_DETAIL" VARCHAR2(100 CHAR),
    "PHY_FIND_QUES_3" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_A" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_B" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_C" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_D" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_3_AD_DETAIL" VARCHAR2(100 CHAR),
    "PHY_FIND_QUES_4" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_4_AD_DETAIL" VARCHAR2(100 CHAR),
    "PHY_FIND_QUES_5" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_A" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_B" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_C" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_D" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_E" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_F" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_G" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_H" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_I" VARCHAR2(1 BYTE),
    "PHY_FIND_QUES_5_AD_DETAIL" VARCHAR2(100 CHAR),
    "IDM_QUES_6" VARCHAR2(1 BYTE),
    "IDM_QUES_6_A" VARCHAR2(1 BYTE),
    "IDM_QUES_6_B" VARCHAR2(1 BYTE),
    "IDM_QUES_6_AD_DETAIL" VARCHAR2(100 CHAR),
    "IDM_QUES_7" VARCHAR2(1 BYTE),
    "IDM_QUES_7_A" VARCHAR2(1 BYTE),
    "IDM_QUES_7_B" VARCHAR2(1 BYTE),
    "IDM_QUES_7_AD_DETAIL" VARCHAR2(100 CHAR),
    "IDM_QUES_8" VARCHAR2(1 BYTE),
    "IDM_QUES_8_AD_DETAIL" VARCHAR2(100 CHAR),
    "CREATOR" NUMBER(10,0),
    "CREATED_ON" DATE,
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "IP_ADD" VARCHAR2(15 BYTE),
    "DELETEDFLAG" NUMBER(22,0),
    "RID" NUMBER(15,0),
    "ENTITY_ID" NUMBER(10,0),
    "ENTITY_TYPE" NUMBER(20,0));

    COMMENT ON TABLE "CB_FINAL_DECL_ELIG"  IS 'This table stores question response realted to final declaration of eligibility.';

    COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PK_FINAL_DECL_ELIG" IS 'Primary Key';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."MRQ_QUES_1" IS 'This column will store MRQ Question 1';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."MRQ_QUES_1_A" IS 'This column will store MRQ Question 1 A';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."MRQ_QUES_1_B" IS 'This column will store MRQ Question 1 B';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."MRQ_QUES_1_AD_DETAIL" IS 'This column will store MRQ Question AD details';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."MRQ_QUES_2" IS 'This column will store MRQ Question 2';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."MRQ_QUES_2_AD_DETAIL" IS 'This column will store MRQ Question 2 AD details';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_3" IS 'This column will store Physical finding Question 3';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_3_A" IS 'This column will store Physical finding Question 3 A';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_3_B" IS 'This column will store Physical finding Question 3 B';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_3_C" IS 'This column will store Physical finding Question 3 C';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_3_D" IS 'This column will store Physical finding Question 3 D';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_3_AD_DETAIL" IS 'This column will store Physical finding Question 3 AD Detail';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_4" IS 'This column will store Physical finding Question 4';
 
 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_4_AD_DETAIL" IS 'This column will store Physical finding Question 4 AD Detail';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_A" IS 'This column will store Physical finding Question 5 A';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_B" IS 'This column will store Physical finding Question 5 B';
 
 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_C" IS 'This column will store Physical finding Question 5 C';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_D" IS 'This column will store Physical finding Question 5 D';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_E" IS 'This column will store Physical finding Question 5 E';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_F" IS 'This column will store Physical finding Question 5 F';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_G" IS 'This column will store Physical finding Question 5 G';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_H" IS 'This column will store Physical finding Question 5 H';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_I" IS 'This column will store Physical finding Question 5 I';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."PHY_FIND_QUES_5_AD_DETAIL" IS 'This column will store Physical finding Question 5 AD Detail';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_6" IS 'This column will store IDM Question 6';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_6_A" IS 'This column will store IDM Question 6 A';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_6_B" IS 'This column will store IDM Question 6 B';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_6_AD_DETAIL" IS 'This column will store IDM Question 6 AD Detail';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_7_A" IS 'This column will store IDM Question 7 A';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_7_B" IS 'This column will store IDM Question 7 B';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_7_AD_DETAIL" IS 'This column will store IDM Question 7 AD Detail';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_8" IS 'This column will store IDM Question 8';

 COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IDM_QUES_8_AD_DETAIL" IS 'This column will store IDM Question 8 AD Detail';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."ENTITY_ID" IS 'pk of the entity';

   COMMENT ON COLUMN "CB_FINAL_DECL_ELIG"."ENTITY_TYPE" IS 'Id of the entity like CBU, Doner etc';

   CREATE SEQUENCE SEQ_CB_FINAL_DECL_ELIG MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

commit;

DECLARE
  v_table_exists number := 0;  
BEGIN
	SELECT COUNT(*) into v_table_exists
	FROM USER_SEQUENCES 
	WHERE SEQUENCE_NAME='SEQ_CB_APP_MESSAGES';

	if (v_table_exists = 0) then
		execute immediate 'CREATE SEQUENCE "ERES"."SEQ_CB_APP_MESSAGES" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE';
		dbms_output.put_line('Sequence SEQ_CB_APP_MESSAGES is created in eres schema');
	else
		dbms_output.put_line('Sequence SEQ_CB_APP_MESSAGES exists in eres schema');
	end if;
end;
/


DECLARE
  v_table_exists number := 0;  
BEGIN
	Select count(*) into v_table_exists
    from USER_TABLES
    where TABLE_NAME = 'CB_APP_MESSAGES'; 

	if (v_table_exists = 0) then
		execute immediate 'CREATE TABLE eres.CB_APP_MESSAGES (
			PK_APP_MESSAGES NUMBER(10,0) primary key,
			MESSAGE_DESC VARCHAR2(225),
			MESSAGE_STATUS NUMBER(10,0),
			MESSAGE_DISP_AT NUMBER(10,0),
			FK_SITE NUMBER(10,0),
			FK_GRPS NUMBER(10,0),
			MSG_START_DT DATE,
			MSG_END_DT DATE,
			MSG_DISP_ORDER NUMBER(10,0),
			DELETED_FLAG VARCHAR2(1 BYTE) Default 0,
			RID NUMBER(10,0),
			IP_ADD VARCHAR2(15),
			CREATOR NUMBER(10,0),
			CREATED_ON DATE Default sysdate,
			LAST_MODIFIED_BY NUMBER(10,0),
			LAST_MODIFIED_DATE DATE)';
		dbms_output.put_line('Table CB_APP_MESSAGES is created in eres schema');
	else
		dbms_output.put_line('Table CB_APP_MESSAGES exists in eres schema');
	end if;
end;
/
			

COMMENT ON COLUMN CB_APP_MESSAGES.PK_APP_MESSAGES
 IS 'Primary key';
COMMENT ON COLUMN CB_APP_MESSAGES.MESSAGE_DESC 
 IS 'Store the messages description';
COMMENT ON COLUMN CB_APP_MESSAGES.MESSAGE_STATUS
 IS 'Foreign Key for ER_CODELST FOR MSG_STATUS';
COMMENT ON COLUMN CB_APP_MESSAGES.MESSAGE_DISP_AT
 IS 'Foreign Key for ER_CODELST FOR MSG_DISP_AT';
COMMENT ON COLUMN CB_APP_MESSAGES.FK_SITE
 IS 'Foreign Key for ER_SITE';
COMMENT ON COLUMN CB_APP_MESSAGES.FK_GRPS
 IS 'Foreign Key for ER_GRPS';
COMMENT ON COLUMN CB_APP_MESSAGES.MSG_START_DT
 IS 'Messages active start date';
COMMENT ON COLUMN CB_APP_MESSAGES.MSG_END_DT
 IS 'Messages active end date';
COMMENT ON COLUMN CB_APP_MESSAGES.MSG_DISP_ORDER
 IS 'Messages display order';
COMMENT ON COLUMN CB_APP_MESSAGES.DELETED_FLAG 
 IS 'Identifies Deletion flag 1-Yes (Deleted), 0-No (Not deleted)';
COMMENT ON COLUMN CB_APP_MESSAGES.RID 
 IS 'This column is used for the audit trail. Uniquely identifies the row in the database';
COMMENT ON COLUMN CB_APP_MESSAGES.IP_ADD 
 IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine';
COMMENT ON COLUMN CB_APP_MESSAGES.CREATOR 
 IS 'This column is used for Audit Trail. Identifies the user who created this row. Stores PK of table ER_USER';
COMMENT ON COLUMN CB_APP_MESSAGES.CREATED_ON 
 IS 'This column is used for the audit trail. Stores the date on which this row was created';
COMMENT ON COLUMN CB_APP_MESSAGES.LAST_MODIFIED_BY 
 IS 'This column is used for the audit trail. Identifies the user who last modified this row. Stores PK of table ER_USER';
COMMENT ON COLUMN CB_APP_MESSAGES.LAST_MODIFIED_DATE 
 IS 'This column is used for the audit trail. Stores the date on which this row was last modified.';

commit;


--CB_ASSESSMENT Table sequence
DECLARE
  v_table_exists number := 0;  
BEGIN
	SELECT COUNT(*) into v_table_exists
	FROM USER_SEQUENCES 
	WHERE SEQUENCE_NAME='SEQ_CB_ASSESSMENT';

	if (v_table_exists = 0) then
		execute immediate 'CREATE  SEQUENCE SEQ_CB_ASSESSMENT MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1';
		dbms_output.put_line('Sequence SEQ_CB_ASSESSMENT is created in eres schema');
	else
		dbms_output.put_line('Sequence SEQ_CB_ASSESSMENT exists in eres schema');
	end if;
end;
/


--CB_ASSESSMENT Table
DECLARE
  v_table_exists number := 0;  
BEGIN
	Select count(*) into v_table_exists
    from USER_TABLES
    where TABLE_NAME = 'CB_ASSESSMENT'; 

	if (v_table_exists = 0) then
	     execute immediate 'CREATE TABLE CB_ASSESSMENT(
				PK_ASSESSMENT NUMBER(10,0) PRIMARY KEY,
				ENTITY_ID NUMBER(10,0),
				ENTITY_TYPE NUMBER(10,0),
				SUB_ENTITY_ID NUMBER(10,0),
				SUB_ENTITY_TYPE NUMBER(10,0),
				ASSESSMENT_REMARKS VARCHAR2(4000 Byte),
				FLAG_FOR_LATER VARCHAR2(2),
				ASSESSMENT_FOR_RESPONSE NUMBER(10,0),
				TC_VISIBILITY_FLAG VARCHAR2(2),
				ASSESSMENT_REASON NUMBER(10,0),
				ASSESSMENT_REASON_REMARKS VARCHAR2(4000 Byte),
				AVAILABILITY_DATE DATE,
				SENT_TO_REVIEW_FLAG VARCHAR2(2),
				CONSULTE_FLAG VARCHAR2(2),
				ASSESSMENT_REVIEWED VARCHAR2(2),
				COLUMN_REFERENCE varchar2(100),		
				CREATOR NUMBER(10),
				CREATED_ON DATE,
				IP_ADD VARCHAR2(15),
				LAST_MODIFIED_BY NUMBER(10),
				LAST_MODIFIED_DATE DATE,
				RID NUMBER(10),
				DELETEDFLAG VARCHAR2(1))';
		dbms_output.put_line('Table CB_ASSESSMENT is created in eres schema');
	else
		dbms_output.put_line('Table CB_ASSESSMENT exists in eres schema');
	end if;
end;
/

-- COMMENTS FOR THE CB_ASSESSMENT TABLE
COMMENT ON TABLE "CB_ASSESSMENT" IS 'Table to store ASSESSMENT INFORMATION';
COMMENT ON COLUMN "CB_ASSESSMENT"."PK_ASSESSMENT" IS 'Primary key column';
COMMENT ON COLUMN "CB_ASSESSMENT"."ENTITY_ID" IS 'storeS pk_cord , pk_donor etc';
COMMENT ON COLUMN "CB_ASSESSMENT"."ENTITY_TYPE" IS 'stores pk_patlab, pk_note etc,';
COMMENT ON COLUMN "CB_ASSESSMENT"."SUB_ENTITY_ID" IS 'stores pk_patlab, pk_note etc.';
COMMENT ON COLUMN "CB_ASSESSMENT"."SUB_ENTITY_TYPE" IS 'stores IDM TEST , NOTE, MRQ TEST etc.';
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_REMARKS" IS 'stores assesments comments.';
COMMENT ON COLUMN "CB_ASSESSMENT"."FLAG_FOR_LATER" IS 'stores flag for later flag value either 0 or 1.';
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_FOR_RESPONSE" IS 'stores pk_codelst like NA,NCD,TU  etc.';
COMMENT ON COLUMN "CB_ASSESSMENT"."TC_VISIBILITY_FLAG" IS 'stores visible for TC flag value either 0 or 1';
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_REASON" IS 'stores pk_codelst for assessment reasons.';
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_REASON_REMARKS" IS 'stores assessment reason remarks etc.';
COMMENT ON COLUMN "CB_ASSESSMENT"."AVAILABILITY_DATE" IS 'stores available date etc.';
COMMENT ON COLUMN "CB_ASSESSMENT"."SENT_TO_REVIEW_FLAG" IS 'stores send review flag 0 or 1.';
COMMENT ON COLUMN "CB_ASSESSMENT"."CONSULTE_FLAG" IS 'stores consult  flag 0 or 1.';
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_REVIEWED" IS 'stores 0 or 1 for confirming assessment reviewed';
COMMENT ON COLUMN "CB_ASSESSMENT"."CREATOR"  IS 'This column is used for Audit Trail. Identifies the user who created this row. Stores PK of table ER_USER';
COMMENT ON COLUMN "CB_ASSESSMENT"."CREATED_ON"  IS 'This column is used for the audit trail. Stores the date on which this row was created';
COMMENT ON COLUMN "CB_ASSESSMENT"."IP_ADD"  IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine';
COMMENT ON COLUMN "CB_ASSESSMENT"."LAST_MODIFIED_BY"  IS 'This column is used for the audit trail. Identifies the user who last modified this row. Stores PK of table ER_USER';
COMMENT ON COLUMN "CB_ASSESSMENT"."LAST_MODIFIED_DATE"  IS 'This column is used for the audit trail. Stores the date on which this row was last modified.';
COMMENT ON COLUMN "CB_ASSESSMENT"."RID"  IS 'This column is used for the audit trail. Uniquely identifies the row in the database';
COMMENT ON COLUMN "CB_ASSESSMENT"."DELETEDFLAG"  IS 'Identifies Deletion flag 1-Yes (Deleted), 0-No (Not deleted)';
COMMENT ON COLUMN "CB_ASSESSMENT"."COLUMN_REFERENCE"  IS 'Stores a reference Name';

commit;


