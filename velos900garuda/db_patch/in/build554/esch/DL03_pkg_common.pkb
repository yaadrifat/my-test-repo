CREATE OR REPLACE PACKAGE BODY        "PKG_COMMON" AS

FUNCTION SCH_GETPATCODE  (p_patprot NUMBER)
    RETURN  VARCHAR2
IS
       patcode VARCHAR2 (20);
        BEGIN
             SELECT P.per_code
             INTO patcode
             FROM ER_PATPROT e , ER_PER P
             WHERE e.pk_patprot = p_patprot AND
              P.pk_per = e.fk_per;
            P(patcode);
             RETURN patcode ;
  END SCH_GETPATCODE;

FUNCTION Validatename (
   EVENTID     NUMBER,
   USERID      NUMBER,
   EVENTNAME   VARCHAR2,
   EVENTTYPE   VARCHAR2
)
   RETURN VARCHAR2
IS
/*
** The function checks if the event name is already existing or not. It returns a -1 if a duplicate
** is found. -1 is passed in the EVENTID parameter if the name is to be checked during an Insert.
** To check during an UPDATE the event_id is passed in the EVENTID parameter. This is used to check the name
** in all other events other than this particular EVENT_ID
**
** Return Value
** -1 on finding a duplicate
**  0 on not finding a duplicate
*/
   EVENTCOUNT   NUMBER;
BEGIN
 -- If EVENTID is -1 then its a check during a INSERT
    IF (EVENTID = -1) THEN
      IF ((EVENTTYPE = 'P')
   OR (EVENTTYPE = 'E')
          OR (EVENTTYPE = 'L')) THEN
         SELECT COUNT (EVENT_ID)
           INTO EVENTCOUNT
           FROM EVENT_DEF
          WHERE USER_ID = USERID
            AND EVENT_TYPE = EVENTTYPE
 -- modified by manimaran on 8th May 2005 for May Enhancement
            AND trim(LOWER(NAME)) = trim(LOWER(EVENTNAME));

         IF (EVENTCOUNT > 0) THEN
            RETURN (-1);
         END IF;
      END IF;
   --
   -- ELSE if the EVENTID is not equal to -1, then its a check during an UPDATE.
   -- The EVENTID contains the event is of the event whoes name has to be checked
   --
   ELSE
      IF ((EVENTTYPE = 'P')
          OR (EVENTTYPE = 'E')
          OR (EVENTTYPE = 'L')) THEN
         SELECT COUNT (EVENT_ID)
           INTO EVENTCOUNT
           FROM EVENT_DEF
          WHERE USER_ID = USERID
            AND EVENT_TYPE = EVENTTYPE
 -- modified by manimaran on 8th May 2005 for May Enhancement
            AND trim(LOWER(NAME)) = trim(LOWER(EVENTNAME))
            AND EVENT_ID != EVENTID;

         IF (EVENTCOUNT > 0) THEN
            RETURN (-1);
         END IF;
      END IF;
   END IF;

   RETURN (0);
END Validatename;

/*YK - 15NOV2010 Changes for BUG # 4538  */
FUNCTION Validatenameassoc (
   EVENTID     NUMBER,
   USERID      NUMBER,
   EVENTNAME   VARCHAR2,
   EVENTTYPE   VARCHAR2
)
   RETURN VARCHAR2
IS
/*
** The function checks if the event name is already existing or not. It returns a -1 if a duplicate
** is found. -1 is passed in the EVENTID parameter if the name is to be checked during an Insert.
** To check during an UPDATE the event_id is passed in the EVENTID parameter. This is used to check the name
** in all other events other than this particular EVENT_ID
**
** Return Value
** -1 on finding a duplicate
**  0 on not finding a duplicate
*/
   EVENTCOUNT   NUMBER;
BEGIN
 -- If EVENTID is -1 then its a check during a INSERT
    IF (EVENTID = -1) THEN
      IF ((EVENTTYPE = 'P')
   OR (EVENTTYPE = 'E')
          OR (EVENTTYPE = 'L')) THEN
         SELECT COUNT (EVENT_ID)
           INTO EVENTCOUNT
           FROM EVENT_ASSOC
          WHERE USER_ID = USERID
            AND EVENT_TYPE = EVENTTYPE
 -- modified by manimaran on 8th May 2005 for May Enhancement
            AND trim(LOWER(NAME)) = trim(LOWER(EVENTNAME));

         IF (EVENTCOUNT > 0) THEN
            RETURN (-1);
         END IF;
      END IF;
   --
   -- ELSE if the EVENTID is not equal to -1, then its a check during an UPDATE.
   -- The EVENTID contains the event is of the event whoes name has to be checked
   --
   ELSE
      IF ((EVENTTYPE = 'P')
          OR (EVENTTYPE = 'E')
          OR (EVENTTYPE = 'L')) THEN
         SELECT COUNT (EVENT_ID)
           INTO EVENTCOUNT
           FROM EVENT_ASSOC
          WHERE USER_ID = USERID
            AND EVENT_TYPE = EVENTTYPE
 -- modified by manimaran on 8th May 2005 for May Enhancement
            AND trim(LOWER(NAME)) = trim(LOWER(EVENTNAME))
            AND EVENT_ID != EVENTID;

         IF (EVENTCOUNT > 0) THEN
            RETURN (-1);
         END IF;
      END IF;
   END IF;

   RETURN (0);
END Validatenameassoc;
/*YK - 15NOV2010 Changes for BUG # 4538  */


FUNCTION SCH_GETMAIL (p_message VARCHAR2, p_params VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
       V_POS      NUMBER         := 0;
       V_CNT      NUMBER         := 0;
       V_PARAMS   VARCHAR2 (2000)DEFAULT p_params || '~';
       V_STR      VARCHAR2 (2000)DEFAULT P_PARAMS || '~';
        BEGIN
         v_msg :=  p_message;
     /*replace the placeholders in message string in order of parameters*/
            LOOP
               EXIT WHEN V_PARAMS IS NULL;
               V_CNT := V_CNT + 1;
               V_POS := INSTR (V_STR, '~');
               V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
               v_msg := REPLACE (v_msg, '~' || V_CNT || '~', V_PARAMS);
               V_STR := SUBSTR (V_STR, V_POS + 1);
         END LOOP;

            /*Test*/
      --p(v_msg);
              RETURN v_msg;
END SCH_GETMAIL;

FUNCTION SCH_GETCELLMAILMSG (p_mesgtype VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
        BEGIN
             SELECT msgtxt_short
             INTO v_msg
             FROM sch_msgtxt
             WHERE UPPER(msgtxt_type) = UPPER(p_mesgtype) ;
          --  p(v_msg);
             RETURN v_msg;
END SCH_GETCELLMAILMSG;

FUNCTION SCH_GETMAILMSG (p_mesgtype VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
        BEGIN
             SELECT dbms_lob.SUBSTR(msgtxt_long,4000,1)
             INTO v_msg
             FROM sch_msgtxt
             WHERE UPPER(msgtxt_type) = UPPER(p_mesgtype) ;
          --  p(v_msg);
             RETURN v_msg;
END SCH_GETMAILMSG ;

END Pkg_Common ;
/


CREATE SYNONYM ERES.PKG_COMMON FOR PKG_COMMON;


CREATE SYNONYM EPAT.PKG_COMMON FOR PKG_COMMON;


GRANT EXECUTE, DEBUG ON PKG_COMMON TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_COMMON TO ERES;

