--Procedure for updating null Module_IDs/User IDs in audit_row_module table
CREATE OR REPLACE
PROCEDURE "SP_AUDIT_ROW_MODULE_UPDATE"
AS
   CURSOR c_audit_row_module IS SELECT pk_row_id,table_name,rid,module_id,user_id FROM AUDIT_ROW_MODULE WHERE module_id IS NULL ORDER BY pk_row_id;
   r_audit_row_module c_audit_row_module%ROWTYPE;
BEGIN
	OPEN c_audit_row_module;
	LOOP
		FETCH c_audit_row_module INTO r_audit_row_module;
		EXIT WHEN c_audit_row_module%NOTFOUND;
		BEGIN
			--Update Module ID for table name SCH_BGTSECTION in AUDIT_ROW_MODULE table
			IF r_audit_row_module.table_name = 'SCH_BGTSECTION' THEN
				UPDATE AUDIT_ROW_MODULE
				SET module_id = (SELECT fk_budget FROM SCH_BGTCAL WHERE pk_bgtcal = 
				(SELECT fk_bgtcal FROM SCH_BGTSECTION WHERE rid = r_audit_row_module.rid)) 
				WHERE pk_row_id = r_audit_row_module.pk_row_id;
			--Update Module ID for table name SCH_LINEITEM in AUDIT_ROW_MODULE table
			ELSIF r_audit_row_module.table_name = 'SCH_LINEITEM' THEN
				UPDATE AUDIT_ROW_MODULE
				SET module_id = (SELECT fk_budget FROM SCH_BGTCAL WHERE PK_bgtcal=(SELECT fk_bgtcal FROM sch_bgtsection WHERE PK_BUDGETSEC = (SELECT fk_bgtsection FROM sch_lineitem WHERE rid = r_audit_row_module.rid )))
				WHERE pk_row_id = r_audit_row_module.pk_row_id;
			--Update Module ID And User ID for table name SCH_BGTAPNDX in AUDIT_ROW_MODULE table
			ELSIF r_audit_row_module.table_name = 'SCH_BGTAPNDX' THEN
				Update AUDIT_ROW_MODULE
				SET module_id = (SELECT fk_budget FROM sch_bgtapndx WHERE rid = r_audit_row_module.rid),
					user_id = (SELECT creator FROM sch_bgtapndx WHERE rid = r_audit_row_module.rid)
				WHERE pk_row_id = r_audit_row_module.pk_row_id;
			END IF;
		END;
	END LOOP;
	COMMIT;
	CLOSE c_audit_row_module;
END;
