set define off;


Insert into ER_MAILCONFIG(
	PK_MAILCONFIG,
	MAILCONFIG_MAILCODE,
	MAILCONFIG_SUBJECT,
	MAILCONFIG_CONTENT,
	MAILCONFIG_CONTENTTYPE,
	MAILCONFIG_FROM,
	MAILCONFIG_BCC,
	MAILCONFIG_CC,
	DELETEDFLAG,
	FK_CODELSTORGID,
	RID,
	MAILCONFIG_HASATTACHEMENT,
	CREATED_ON,
	CREATOR,
	LAST_MODIFIED_DATE,
	LAST_MODIFIED_BY,
	IP_ADD) 
	values (SEQ_ER_MAILCONFIG.nextval,'IMPORTNOTI',
	'CORD IMPORT JOB#[ID] COMPLETE',
	CHR(13) || 
	'Dear [USER],</br>' || CHR(13) ||	
	CHR(13) || CHR(13) || 	
	'The Cord Import Process that you started has been completed now.</br>' || CHR(13) || CHR(13) || 
	'Please Use This Link <a href="[URL]">Cord Import</a>  to verify the process.</br>' || CHR(13) || 
	CHR(13) || 
	'Kind Regards,</br>' || CHR(13) || 
	'[CM]' || 
	CHR(13),
	'text/html',
	'admin@aithent.com',
	'admin@aithent.com',
	0,0,0,1,0,sysdate,null,sysdate,null,null
	);

commit;
