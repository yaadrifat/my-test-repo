
--STARTS INSERTING RECORD INTO UI_PAGE TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_PAGE
    where PAGE_NAME = 'Pending_Order_Det';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_PAGE(PK_PAGE,PAGE_CODE,PAGE_NAME) 
  VALUES(SEQ_UI_PAGE.nextval,'27','Pending_Order_Det');
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO UI_WIDGET_CONTAINER TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET_CONTAINER
    where CONTAINER_NAME = 'Pending Order Details';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET_CONTAINER(PK_CONTAINER,FK_PAGE_ID,CONTAINER_NAME) 
  VALUES(SEQ_UI_WIDGET_CONTAINER.nextval,(SELECT PK_PAGE FROM UI_PAGE WHERE PAGE_NAME='Pending_Order_Det'),'Pending Order Details');
	commit;
  end if;
end;
/
--END--



--================================Current Request Progress==========================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'openOrdercbuInfo';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Current Request Progress','openOrdercbuInfo','Current Request Progress',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrdercbuInfo');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrdercbuInfo'));
	commit;
  end if;
end;
/
--END--

--================================Current Request Progress==========================================================

--=============================Patient - Transplant Center - Case Manager ==========================================

--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'openOrderrecTcInfo';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Patient - Transplant Center - Case Manager','openOrderrecTcInfo','Patient - Transplant Center - Case Manager',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrderrecTcInfo');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrderrecTcInfo'));
	commit;
  end if;
end;
/
--END--
--=============================Patient - Transplant Center - Case Manager ==========================================

--=============================CBU Availability Confirmation =======================================================

--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'cbuavailconfbarDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'CBU Availability Confirmation','cbuavailconfbarDiv','CBU Availability Confirmation',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cbuavailconfbarDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cbuavailconfbarDiv'));
	commit;
  end if;
end;
/
--END--

--=============================CBU Availability Confirmation =======================================================

--=============================Required Clinical Information  ======================================================

--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'reqclincinfobarDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Required Clinical Information','reqclincinfobarDiv','Required Clinical Information',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='reqclincinfobarDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='reqclincinfobarDiv'));
	commit;
  end if;
end;
/
--END--


--=============================Required Clinical Information  ======================================================

--=============================CT-Shipment Information   ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'ctshipmentbarDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'CT-Shipment Information','ctshipmentbarDiv','CT-Shipment Information',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='ctshipmentbarDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='ctshipmentbarDiv'));
	commit;
  end if;
end;
/
--END--


--=============================CT-Shipment Information   ======================================================

--=============================TC Order Details    ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'cttcorderdetailsDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'TC Order Details ','cttcorderdetailsDiv','TC Order Details ',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cttcorderdetailsDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cttcorderdetailsDiv'));
	commit;
  end if;
end;
/
--END--


--=============================TC Order Details    ======================================================

--=============================Additional HLA Typing by CBB ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'additityingandtestDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Additional HLA Typing by CBB','additityingandtestDiv','Additional HLA Typing by CBB',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='additityingandtestDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='additityingandtestDiv'));
	commit;
  end if;
end;
/
--END--


--=============================Additional HLA Typing by CBB     ======================================================


--=============================Resolution  ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'ctresolutionbarDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Resolution ','ctresolutionbarDiv','Resolution ',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='ctresolutionbarDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='ctresolutionbarDiv'));
	commit;
  end if;
end;
/
--END--


--=============================Resolution     ======================================================

--=============================Progress Notes   ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'openOrdernotes';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Progress Notes ','openOrdernotes','Progress Notes ',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrdernotes');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrdernotes'));
	commit;
  end if;
end;
/
--END--


--=============================Progress Notes     ======================================================


--=============================Request & CBU History   ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'openOrdercbuhis';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Request & CBU History','openOrdercbuhis','Request & CBU History',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrdercbuhis');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='openOrdercbuhis'));
	commit;
  end if;
end;
/
--END--


--=============================Request & CBU History    ======================================================


--=============================TC Order Details (OR Order)   ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'ortcorderdetailsDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'TC Order Details (OR Order)','ortcorderdetailsDiv','TC Order Details (OR Order)',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='ortcorderdetailsDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='ortcorderdetailsDiv'));
	commit;
  end if;
end;
/
--END--


--=============================TC Order Details (OR Order)    ======================================================


--=============================Final CBU Review    ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'finalcbureviewbarDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Final CBU Review ','finalcbureviewbarDiv','Final CBU Review ',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='finalcbureviewbarDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='finalcbureviewbarDiv'));
	commit;
  end if;
end;
/
--END--


--=============================Final CBU Review     ======================================================


--=============================NMDP Research Sample    ======================================================
--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'nmdpresearchSampleDiv';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'NMDP Research Sample ','nmdpresearchSampleDiv','NMDP Research Sample',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='nmdpresearchSampleDiv');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Pending Order Details'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='nmdpresearchSampleDiv'));
	commit;
  end if;
end;
/
--END--


--=============================NMDP Research Sample   ======================================================


