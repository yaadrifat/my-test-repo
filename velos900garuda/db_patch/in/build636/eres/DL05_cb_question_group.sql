--STARTS INSERTING RECORD INTO CB_QUESTION_GROUP TABLE--
--Empty Header--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = '';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--END--
--Screening Tests--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = 'Screening Tests';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'Screening Tests',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--Confirmatory / Supplemental Tests--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = 'Confirmatory / Supplemental Tests';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'Confirmatory / Supplemental Tests',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--Additional / Miscellaneous IDM Tests--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = 'Additional / Miscellaneous IDM Tests';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'Additional / Miscellaneous IDM Tests',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

