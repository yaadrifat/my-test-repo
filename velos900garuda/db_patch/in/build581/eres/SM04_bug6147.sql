set define off;

update er_browserconf set browserconf_seq = browserconf_seq+1 where fk_browser =
(select pk_browser from er_browser where BROWSER_MODULE='studyPatient' and BROWSER_NAME ='Enrolled Patient Browser');

commit;