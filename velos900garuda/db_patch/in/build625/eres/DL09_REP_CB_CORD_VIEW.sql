


CREATE OR REPLACE FORCE VIEW "ERES"."REP_CB_CORD_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "PK_ROW_ID", "MODULE_ID")
AS
  SELECT cord.CORD_REGISTRY_ID,
    cord.CORD_LOCAL_CBU_ID,
    cord.Registry_Maternal_Id,
    cord.maternal_local_id,
    cord.Created_On,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || ER_USER.USR_LASTNAME
    FROM Er_User
    WHERE Pk_User = cord.CREATOR
    ) AS CREATOR,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || ER_USER.USR_LASTNAME
    FROM Er_User
    WHERE Pk_User = ARM.USER_ID
    ) AS LAST_MODIFIED_BY,
    cord.Last_Modified_Date,
    arm.PK_ROW_ID,
    ARM.MODULE_ID
  FROM Audit_Row_Module arm,
    cb_cord cord
  WHERE cord.pk_cord = arm.module_id
  AND arm.TABLE_NAME ='CB_CORD';

commit;






