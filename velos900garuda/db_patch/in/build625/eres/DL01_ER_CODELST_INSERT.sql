--SQL for mail configuration
-- forentry in  ER_CODELST

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
   WHERE codelst_type='ADD_CHG_EMAIL' AND codelst_subtyp='PICK_UP';
   
    if (v_record_exists = 0) then
insert into er_codelst(pk_codelst,codelst_subtyp,codelst_type,codelst_desc)
values(seq_er_codelst.nextval,'PICK_UP','ADD_CHG_EMAIL','cordcourierreq@nmdp.org');
	commit;
  end if;
END;
/