=====================================================================================================================================
Garuda :

	"Additional Configurations for CDR-1.doc" is a document which contains information how to configure below components.
	This document contains configuration details for the following components.
	1)Hibernate Database Configurations
	2)Document Configuration Management System Settings
	3)eMail Configuration Settings
	4)Work flow Integration Settings
	
	
=====================================================================================================================================
eResearch:

Note:   We need to execute the DL11_ER_CodelstCtrpCountryupdate.sql  after the execution of  patch   IH01_insertCodelstCtrpCountry.sql. Since the base records of CTRP country codes are inserted in the  IH01_insertCodelstCtrpCountry.sql.
So the sequence of patch execution should be should be 

1.IH01_insertCodelstCtrpCountry.sql
2.DL11_ER_CodelstCtrpCountryupdate.sql
	
=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	labelBundle.properties
2	LC.java
3	LC.jsp
4	MC.java
5	MC.jsp
6	messageBundle.properties

Following Files have been Modified for mouse over/balloon help issues.

TITLE:
1. acctformdetails.jsp
2. formdatarecords.jsp
3. getmultilookup.jsp
4. milestone.jsp
5. ongoingBrowser.jsp
6. patformdetails.jsp
7. patstudyformdetails.jsp
8. studyFormDD.jsp
9. studyformdetails.jsp

ALT:
1. accountsave.jsp
2. centerwatchlatestdrugs.jsp
3. commentType.jsp
4. editBox.jsp
5. editBoxSection.jsp
6. ereslogin.jsp
7. fetchCoverage.jsp
8. forgotmail.jsp
9. forgotpassword.jsp
10. forgotpwd.jsp
11. formfldbrowser.jsp
12. getlookup.jsp
13. getmultilookup.jsp
14. multipleChoiceSection.jsp
15. patientbudget.jsp
16. patientbudgetView.jsp
17. patientlogin.jsp
18. protocoltabs.jsp
19. register.jsp
20. studyprotocols.jsp

All the reports XSLs have been localized.
Count: 125 reports XSLs

We need to execute the following in the sequence: 
1. BT01_data.sql
2. loadxsl.bat
=====================================================================================================================================