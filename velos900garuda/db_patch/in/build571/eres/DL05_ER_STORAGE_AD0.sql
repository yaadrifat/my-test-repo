CREATE OR REPLACE TRIGGER "ER_STORAGE_AD0"
AFTER DELETE
ON ER_STORAGE
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
  usr VARCHAR(2000);


begin

  usr := Getuser(:old.last_modified_by);

  select seq_audit.nextval into raid from dual;

audit_trail.record_transaction
    (raid, 'ER_STORAGE', :old.rid, 'D', usr);
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_STORAGE) || '|' ||
:old.STORAGE_ID || '|' ||
:old.STORAGE_NAME || '|' ||
to_char(:old.FK_CODELST_STORAGE_TYPE) || '|' ||
to_char(:old.FK_STORAGE) || '|' ||
to_char(:old.STORAGE_CAP_NUMBER) || '|' ||
to_char(:old.FK_CODELST_CAPACITY_UNITS) || '|' ||
--:old.STORAGE_STATUS || '|' ||--KM
to_char(:old.STORAGE_DIM1_CELLNUM) || '|' ||
:old.STORAGE_DIM1_NAMING || '|' ||
:old.STORAGE_DIM1_ORDER || '|' ||
to_char(:old.STORAGE_DIM2_CELLNUM) || '|' ||
:old.STORAGE_DIM2_NAMING || '|' ||
:old.STORAGE_DIM2_ORDER || '|' ||
to_char(:old.STORAGE_AVAIL_UNIT_COUNT) || '|' ||
to_char(:old.STORAGE_COORDINATE_X) || '|' ||
to_char(:old.STORAGE_COORDINATE_Y) || '|' ||
:old.STORAGE_LABEL || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
:old.IP_ADD || '|' ||
TO_CHAR(:OLD.FK_ACCOUNT)|| '|' ||
to_char(:old.STORAGE_ISTEMPLATE)|| '|' ||
to_char(:old.STORAGE_TEMPLATE_TYPE)|| '|' ||
:old.STORAGE_ALTERNALEID|| '|' ||
to_char(:old.STORAGE_UNIT_CLASS)|| '|' ||
to_char(:old.STORAGE_KIT_CATEGORY)|| '|' ||
to_char(:old.FK_STORAGE_KIT);--YK Bug#5827

insert into AUDIT_DELETE
(raid, row_data) values (raid, deleted_data);
end;
/
 