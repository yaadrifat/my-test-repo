create or replace TRIGGER ER_SPECIMEN_BI0 BEFORE INSERT ON ER_SPECIMEN
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_SPECIMEN',erid, 'I',usr);

insert_data:=

:NEW.PK_SPECIMEN || '|' ||
:NEW.SPEC_ID|| '|' ||
:NEW.SPEC_ALTERNATE_ID || '|' ||
:NEW.SPEC_DESCRIPTION || '|' ||
:NEW.SPEC_ORIGINAL_QUANTITY || '|' ||
:NEW.SPEC_QUANTITY_UNITS || '|' ||
:NEW.SPEC_TYPE || '|' ||
:NEW.FK_STUDY || '|' ||
TO_CHAR(:NEW.SPEC_COLLECTION_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)|| '|' ||
:NEW.FK_PER || '|' ||
:NEW.SPEC_OWNER_FK_USER || '|' ||
:NEW.FK_STORAGE || '|' ||
:NEW.FK_SITE || '|' ||
:NEW.FK_VISIT || '|' ||
:NEW.FK_SCH_EVENTS1 || '|' ||
:NEW.FK_SPECIMEN || '|' ||
:NEW.SPEC_STORAGE_TEMP || '|' ||
:NEW.SPEC_STORAGE_TEMP_UNIT || '|' ||
:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
||'|'||:NEW.IP_ADD||'|'||:NEW.FK_ACCOUNT||'|'||:NEW.SPEC_PROCESSING_TYPE||'|'||:NEW.FK_STORAGE_KIT_COMPONENT  --KM -013008
||'|'||:NEW.FK_USER_PATH||'|'||:NEW.FK_USER_SURG||'|'||:NEW.FK_USER_COLLTECH||'|'||:NEW.FK_USER_PROCTECH
||'|'||TO_CHAR(:NEW.SPEC_REMOVAL_TIME,PKG_DATEUTIL.F_GET_DATETIMEFORMAT) ||'|'|| TO_CHAR(:NEW.SPEC_FREEZE_TIME,PKG_DATEUTIL.F_GET_DATETIMEFORMAT)||'|'||
:NEW.spec_anatomic_site||'|'||
:NEW.spec_tissue_side||'|'||
:NEW.spec_pathology_stat||'|'||
:NEW.fk_storage_kit||'|'||
:NEW.spec_expected_quantity||'|'||
:NEW.spec_base_orig_quantity||'|'||
:NEW.spec_expectedq_units||'|'||
:NEW.spec_base_origq_units ||'|'||
:NEW.spec_disposition||'|'||  --AK(Bug#5828-16thMar11)
:NEW.spec_envt_cons;

INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/