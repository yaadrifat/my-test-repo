set define off;

UPDATE ER_BrowserConf
SET browserconf_settings ='{"key":"PERSON_CODE", "label":"Patient ID",
"sortable":true, "resizeable":true}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='PERSON_CODE'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;
  
UPDATE ER_BrowserConf
SET browserconf_settings =
'{"key":"CHECK_ITEM", "label":"<input type =''checkbox'' name=''checkallrow'' onClick = ''checkAll()''/>",
"format":"checkList"}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='CHECK_DATA'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;



UPDATE ER_BrowserConf
SET browserconf_settings =
'{"key":"EVENT_SCHDATE", "label":"Scheduled Date", "resizeable":true,"sortable":true}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='EVENT_SCHDATE'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;
  
  
  
  
UPDATE ER_BrowserConf
SET browserconf_settings =
'{"key":"EVENT_NAME", "label":"Event", "sortable":true, "resizeable":true}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='EVENT_NAME'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
commit;


UPDATE ER_BrowserConf
SET browserconf_settings =
'{"key":"STORAGE_KIT", "label":"Storage kit",  "resizeable":true,"sortable":true}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='STORAGE_KIT'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
commit;


UPDATE ER_BrowserConf
SET browserconf_settings ='{"key":"STUDY_NUMBER", "label":"Study Number", "sortable":true,
"resizeable":true}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='STUDY_NUMBER'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;
  
  
  UPDATE ER_BrowserConf
SET browserconf_settings ='{"key":"PROTOCOL_NAME", "label":"Calendar", "sortable":true, 
"resizeable":true}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='PROTOCOL_NAME'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;
  
  
  
  UPDATE ER_BrowserConf
SET browserconf_settings ='{"key":"VISIT_NAME", "label":"Visit", "sortable":true,
"resizeable":true}'
WHERE pk_browserconf     =
  (SELECT pk_browserconf
  FROM ER_BrowserConf
  WHERE browserconf_colname='VISIT_NAME'
  AND fk_browser           =
    (SELECT pk_browser
    FROM er_browser
    WHERE BROWSER_MODULE = 'preparea'
    AND BROWSER_NAME     = 'Preparation Area'
    )
  );
  commit;

