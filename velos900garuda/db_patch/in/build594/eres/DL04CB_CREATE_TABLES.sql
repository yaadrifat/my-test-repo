----------------------------------------------CB_RECEIPANT_INFO Table-------------------------------------------------

CREATE TABLE CB_RECEIPANT_INFO(
	PK_RECEIPANT NUMBER(10,0) PRIMARY KEY,
	RECEIPANT_ID VARCHAR2(20 BYTE),
	FK_PERSON_ID NUMBER(10,0),
	FK_ORDER_ID NUMBER(10,0),
	CURRENT_DIAGNOSIS VARCHAR(20 BYTE),
	FK_HLA_ID NUMBER(10,0),
	TRANS_CENTER_ID NUMBER(10,0),
	CREATOR NUMBER(10),
	CREATED_ON DATE,
	IP_ADD VARCHAR2(15),
	LAST_MODIFIED_BY NUMBER(10),
	LAST_MODIFIED_ON DATE,
	RID NUMBER(10),
	DELETEDFLAG VARCHAR2(1)
);
COMMENT ON TABLE "CB_RECEIPANT_INFO" IS 'Table to store Receipants information';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."PK_RECEIPANT" IS 'Primary key column';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."RECEIPANT_ID" IS 'The NMDP Recipient identification number';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."FK_PERSON_ID" IS 'foreign key column for epat.person table';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."FK_ORDER_ID" IS 'foreign key column for er_order table';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."CURRENT_DIAGNOSIS" IS 'The recipientís disease diagnosis';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."FK_HLA_ID" IS 'foreign key column for cb_hla table';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."TRANS_CENTER_ID" IS 'foreign key column';
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. '; 
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."LAST_MODIFIED_ON" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_RECEIPANT_INFO"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 

----------------------------------------------CB_RECEIPANT_INFO Table Ends here-------------------------------------------------



CREATE  SEQUENCE SEQ_CB_RECEIPANT_INFO MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

----------------------------------------------CB_REQUEST Table--------------------------------------------------




CREATE TABLE CB_REQUEST(
	PK_REQUEST NUMBER(10,0) PRIMARY KEY,
	FK_ORDER_ID NUMBER(10,0),
	REQUEST_STATUS VARCHAR2(20 BYTE),
	LAST_REQ_STAT_DATE DATE,
	REQ_LAST_REVIEWED_BY NUMBER(10,0),
	REQ_LAST_REVIEWED_DATE DATE,
	REQ_ASSIGNED_TO NUMBER(10,0),
	REQ_ASSIGNED_DATE DATE,
	CREATOR NUMBER(10),
	CREATED_ON DATE,
	IP_ADD VARCHAR2(15),
	LAST_MODIFIED_BY NUMBER(10),
	LAST_MODIFIED_ON DATE,
	RID NUMBER(10),
	DELETEDFLAG VARCHAR2(1)
);
COMMENT ON TABLE "CB_REQUEST" IS 'Table to store Requests information';
COMMENT ON COLUMN "CB_REQUEST"."PK_REQUEST" IS 'Primary key column';
COMMENT ON COLUMN "CB_REQUEST"."FK_ORDER_ID" IS 'foreign key column with reference to er_order table';
COMMENT ON COLUMN "CB_REQUEST"."REQUEST_STATUS" IS 'stores the request status';
COMMENT ON COLUMN "CB_REQUEST"."LAST_REQ_STAT_DATE" IS 'stores last request status date';
COMMENT ON COLUMN "CB_REQUEST"."REQ_LAST_REVIEWED_BY" IS 'Name of person that last reviewed any of the CBU information/file.';
COMMENT ON COLUMN "CB_REQUEST"."REQ_LAST_REVIEWED_DATE" IS 'Date of when the CBU  file was last reviewed by';
COMMENT ON COLUMN "CB_REQUEST"."REQ_ASSIGNED_TO" IS 'Cord Blood Bank assigns a search request to a user';
COMMENT ON COLUMN "CB_REQUEST"."REQ_ASSIGNED_DATE" IS 'A date is entered automatically into this field by the system when the Assigned To field is populated';
COMMENT ON COLUMN "CB_REQUEST"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. '; 
COMMENT ON COLUMN "CB_REQUEST"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_REQUEST"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_REQUEST"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_REQUEST"."LAST_MODIFIED_ON" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_REQUEST"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_REQUEST"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 

----------------------------------------------CB_REQUEST Table Ends here--------------------------------------------------


CREATE SEQUENCE SEQ_CB_REQUEST MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

----------------------------------------------CB_SHIPMENT Table--------------------------------------------------


CREATE TABLE CB_SHIPMENT(
	PK_SHIPMENT NUMBER(10,0) PRIMARY KEY,
	FK_ORDER_ID NUMBER(10,0),
	SHIPMENT_DATE DATE,
	ADDI_SHIPMENT_DATE DATE,
	SHIPMENT_COMPANY VARCHAR2(20 BYTE),
	SHIPMENT_TRACKING_NO NUMBER(10,0),
	ADDI_SHIPMENT_TRACKING_NO NUMBER(10,0),
	CBB_ID NUMBER(10,0),
	CREATOR NUMBER(10),
	CREATED_ON DATE,
	IP_ADD VARCHAR2(15),
	LAST_MODIFIED_BY NUMBER(10),
	LAST_MODIFIED_ON DATE,
	RID NUMBER(10),
	DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_SHIPMENT" IS 'Table to store shipment information';
COMMENT ON COLUMN "CB_SHIPMENT"."PK_SHIPMENT" IS 'Primary key column';
COMMENT ON COLUMN "CB_SHIPMENT"."FK_ORDER_ID" IS 'Foreign key column with reference to er_order table';
COMMENT ON COLUMN "CB_SHIPMENT"."SHIPMENT_DATE" IS 'date the sample was shipped to NMDP contract lab.';
COMMENT ON COLUMN "CB_SHIPMENT"."ADDI_SHIPMENT_DATE" IS 'date the additional sample was shipped to the NMDP contract lab';
COMMENT ON COLUMN "CB_SHIPMENT"."SHIPMENT_COMPANY" IS 'Default the shipper information pulled from the CBB Profile section.';
COMMENT ON COLUMN "CB_SHIPMENT"."SHIPMENT_TRACKING_NO" IS 'Enter the sample shipment tracking number';
COMMENT ON COLUMN "CB_SHIPMENT"."ADDI_SHIPMENT_TRACKING_NO" IS 'Enter the additional sample shipment tracking number';
COMMENT ON COLUMN "CB_SHIPMENT"."CBB_ID" IS 'Foreign key column with reference to cbb table';
COMMENT ON COLUMN "CB_SHIPMENT"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. '; 
COMMENT ON COLUMN "CB_SHIPMENT"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_SHIPMENT"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_SHIPMENT"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_SHIPMENT"."LAST_MODIFIED_ON" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_SHIPMENT"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_SHIPMENT"."DELETEDFLAG" IS 'This column denotes whether record is deleted.'; 

----------------------------------------------CB_SHIPMENT Table Ends here--------------------------------------------------

----------------------------------------------SEQ_CB_SHIPMENT sequence--------------------------------------------------

CREATE SEQUENCE SEQ_CB_SHIPMENT MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;