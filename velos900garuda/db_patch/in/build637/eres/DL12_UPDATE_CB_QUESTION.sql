--START UPDATING THE CB_QUESTIONS TABLE--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'cmv_total_ind';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_temp_over_ten_days';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained temperature higher than 100.5°F (38.06°C) for more than 10 days?' where QUES_CODE = 'unexpln_temp_over_ten_days';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_4mth_illness_symptom_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 4 months, have you experienced two or more of the following: a fever (>100.5° F or 38.06° C), headache, muscle weakness, skin rash on trunk of the body, or swollen lymph glands?' where QUES_CODE = 'pst_4mth_illness_symptom_ind';
commit;
  end if;
end;
/  
--END--
