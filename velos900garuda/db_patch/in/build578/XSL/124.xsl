<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByCategory" match="ROW" use="BGTSECTION_NAME" />
<xsl:key name="RecordsByCategoryLineitem" match="ROW" use="concat(BGTSECTION_NAME, ' ', LINEITEM_NAME)" />

<xsl:key name="RecordsByCategorySOC" match="ROW" use="concat(BGTSECTION_NAME, ' ', STANDARD_OF_CARE)" /> 

<xsl:key name="RecordsBySOC" match="ROW" use="STANDARD_OF_CARE" /> 

<xsl:key name="RecordsByPersonnel" match="ROW" use="CATEGORY_SUBTYP" />

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:param name="budgetTemplate"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

    </HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >
<tr class="reportGreyRow">
<td class="reportPanel"> 
Download the report in: 
<A href='{$wd}' >
Word Format
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
Excel Format
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
Printer Friendly Format
</A> 
</td>
</tr>
</table>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>
<hr class="thickLine" />


<TABLE WIDTH="100%" >
<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Name: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_NAME" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Version: </TD><TD class="reportData" align="left"><b><xsl:value-of select="//BUDGET_VERSION" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Description: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_DESC" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="left" width="22%">
VELLABEL[Std_Study] Number: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_NUMBER" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="LEFT" width="22%">
VELLABEL[Std_Study] Title: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_TITLE" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="LEFT" width="22%">
Protocol Calendar: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_CALENDAR" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="left" width="22%">
Organization: </TD><TD class="reportData"><b><xsl:value-of select="//SITE_NAME" /></b>
</TD>
</TR>
</TABLE>
<hr class="thinLine" />

<TABLE WIDTH="100%" BORDER="1">
<TR>
<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER">Section</TH>
    <TH class="reportHeading" WIDTH="15%" ALIGN="CENTER" >Type</TH>
	<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER" >Direct Cost/VELLABEL[Pat_Patient]</TH>
	<TH class="reportHeading" WIDTH="15%" ALIGN="CENTER" >Total Cost/VELLABEL[Pat_Patient]</TH>
    <TH class="reportHeading" WIDTH="15%" ALIGN="CENTER" >Cost/All VELLABEL[Pat_Patients]</TH>
    <xsl:if test="$budgetTemplate='C'">
    <TH class="reportHeading" WIDTH="15%" ALIGN="CENTER" >Sponsor Amount</TH>
    <TH class="reportHeading" WIDTH="15%" ALIGN="CENTER" >Variance</TH>    
    </xsl:if>
</TR>
<xsl:for-each select="ROW[count(. | key('RecordsByCategory', BGTSECTION_NAME)[1])=1]">

<TR>
<TD class="reportData" colspan="7">
<b><xsl:value-of select="BGTSECTION_NAME" /></b>
</TD>
</TR>

<xsl:variable name="str" select="key('RecordsByCategory', BGTSECTION_NAME)" />
<xsl:for-each select="key('RecordsByCategory', BGTSECTION_NAME)">

</xsl:for-each>


<tr>
<td>&#xa0;</td>
<td WIDTH="15%" align="left" class="reportGrouping">Section Research Total </td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>

<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/LINEITEM_SPONSORAMOUNT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/LINEITEM_SPONSORAMOUNT) - (  sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_PER_PAT) ) ,'##,###,###,###,###,##0.00')"/></td>
</xsl:if>
</tr>

<tr>
<td>&#xa0;</td>
<td WIDTH="15%" align="left" class="reportGrouping">Section SOC Total </td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>

<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/LINEITEM_SPONSORAMOUNT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/LINEITEM_SPONSORAMOUNT) -( sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_PER_PAT)   )  ,'##,###,###,###,###,##0.00')"/></td>
</xsl:if>
</tr>

<tr>
<td>&#xa0;</td>
<td WIDTH="15%" align="left" class="reportGrouping">Section Other Cost Total </td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>

<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/LINEITEM_SPONSORAMOUNT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/LINEITEM_SPONSORAMOUNT) - (  sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_PER_PAT) ) ,'##,###,###,###,###,##0.00')"/></td>
</xsl:if>
</tr>

   
    
</xsl:for-each>
<tr>
    <td class="reportGrouping"><font color="red">Grand Totals</font></td>
</tr>
<tr>
<td>&#xa0;</td>
<td WIDTH="15%" align="left" class="reportGrouping"><font color="red">Grand Research Total </font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>



<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/LINEITEM_SPONSORAMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/LINEITEM_SPONSORAMOUNT) - ( sum(key('RecordsBySOC', 'No')/TOTAL_COST_PER_PAT)  )   ,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
</tr>

<tr>
<td>&#xa0;</td>
<td WIDTH="15%" align="left" class="reportGrouping"><font color="red">Grand SOC Total </font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>


<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/LINEITEM_SPONSORAMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/LINEITEM_SPONSORAMOUNT) - ( sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_PER_PAT) ) ,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
</tr>

<tr>
<td>&#xa0;</td>
<td WIDTH="15%" align="left" class="reportGrouping"><font color="red">Grand Other Cost Total </font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>



<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/LINEITEM_SPONSORAMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/LINEITEM_SPONSORAMOUNT) - ( sum(key('RecordsBySOC', 'Other')/TOTAL_COST_PER_PAT)  )   ,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
</tr>


<tr>
<td>&#xa0;</td>
<td WIDTH="15%" align="left" class="reportGrouping"><font color="red">Grand Total </font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>


<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//LINEITEM_SPONSORAMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//LINEITEM_SPONSORAMOUNT)- ( sum(//TOTAL_COST_PER_PAT) ) ,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
</tr>





</TABLE>

<hr class="thickLine" />
<TABLE WIDTH="100%" >

<TR>
<TD>Last Modified by: <xsl:value-of select="//LAST_MODIFIED_BY"/></TD>
<TD>Date Last Modified: <xsl:value-of select="//LAST_MODIFIED_DATE"/></TD>
</TR>
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
Report By:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
Date:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>