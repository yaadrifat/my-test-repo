CREATE OR REPLACE TRIGGER "ERES"."CBU_UNIT_REPORT_HLA_AU1" AFTER UPDATE ON ERES.CBU_UNIT_REPORT_HLA
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */

  BEGIN
    PKG_AUDIT_TRAIL_MODULE.SP_row_insert (V_ROWID,'CBU_UNIT_REPORT_HLA',:OLD.rid,:OLD.PK_CBU_UNIT_REPORT_HLA,'U',:NEW.LAST_MODIFIED_BY);
  	If Nvl(:Old.Pk_Cbu_Unit_Report_Hla,0) != Nvl(:New.Pk_Cbu_Unit_Report_Hla,0) Then
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','PK_CBU_UNIT_REPORT_HLA', :OLD.PK_CBU_UNIT_REPORT_HLA, :NEW.PK_CBU_UNIT_REPORT_HLA,NULL,NULL);
    END IF;	
	IF NVL(:old.FK_CORD_CDR_CBU_ID,' ') != NVL(:new.FK_CORD_CDR_CBU_ID,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','FK_CORD_CDR_CBU_ID', :OLD.FK_CORD_CDR_CBU_ID, :NEW.FK_CORD_CDR_CBU_ID,NULL,NULL);
    END IF;
	IF NVL(:old.CORD_HLA_TWICE,' ') != NVL(:new.CORD_HLA_TWICE,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CORD_HLA_TWICE', :OLD.CORD_HLA_TWICE, :NEW.CORD_HLA_TWICE,NULL,NULL);
    END IF;
  IF NVL(:OLD.CORD_HLA_SECOND_LAB,' ') != NVL(:NEW.CORD_HLA_SECOND_LAB,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CORD_HLA_SECOND_LAB', :OLD.CORD_HLA_SECOND_LAB, :NEW.CORD_HLA_SECOND_LAB,NULL,NULL);
    END IF;  
	IF NVL(:OLD.CORD_HLA_CONTI_SEG,' ') != NVL(:NEW.CORD_HLA_CONTI_SEG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CORD_HLA_CONTI_SEG', :OLD.CORD_HLA_CONTI_SEG, :NEW.CORD_HLA_CONTI_SEG,NULL,NULL);
    END IF;
	IF NVL(:old.CORD_HLA_CONTI_SEG_BEF_REL,' ') != NVL(:new.CORD_HLA_CONTI_SEG_BEF_REL,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CORD_HLA_CONTI_SEG_BEF_REL', :OLD.CORD_HLA_CONTI_SEG_BEF_REL, :NEW.CORD_HLA_CONTI_SEG_BEF_REL,NULL,NULL);
    END IF;
	IF NVL(:old.CORD_HLA_IND_BEF_REL,' ') != NVL(:new.CORD_HLA_IND_BEF_REL,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CORD_HLA_IND_BEF_REL', :OLD.CORD_HLA_IND_BEF_REL, :NEW.CORD_HLA_IND_BEF_REL,NULL,NULL);
    END IF;
	IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CREATOR', :OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
    END IF;
	IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
 IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','IP_ADD', :old.ip_add, :new.ip_add,NULL,NULL);
    END IF;
IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN    
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
	IF NVL(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','LAST_MODIFIED_DATE', TO_CHAR(:OLD.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;	
	IF NVL(:old.rid,0) != NVL(:new.rid,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','RID', :OLD.rid, :NEW.rid,NULL,NULL);
    END IF;
	IF NVL(:old.DELETEDFLAG,' ') != NVL(:new.DELETEDFLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
    END IF;
	IF NVL(:old.CORD_HLA_CONTI_SEG_NUM,0) != NVL(:new.CORD_HLA_CONTI_SEG_NUM,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CORD_HLA_CONTI_SEG_NUM', :OLD.CORD_HLA_CONTI_SEG_NUM, :NEW.CORD_HLA_CONTI_SEG_NUM,NULL,NULL);
    END IF;
	IF NVL(:old.CORD_IND_HLA_SAMPLES,0) != NVL(:new.CORD_IND_HLA_SAMPLES,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','CORD_IND_HLA_SAMPLES', :OLD.CORD_IND_HLA_SAMPLES, :NEW.CORD_IND_HLA_SAMPLES,NULL,NULL);
    END IF;
	IF NVL(:old.FK_CORD_EXT_INFO,0) != NVL(:new.FK_CORD_EXT_INFO,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CBU_UNIT_REPORT_HLA','FK_CORD_EXT_INFO', :OLD.FK_CORD_EXT_INFO, :NEW.FK_CORD_EXT_INFO,NULL,NULL);
    END IF;
END;
/