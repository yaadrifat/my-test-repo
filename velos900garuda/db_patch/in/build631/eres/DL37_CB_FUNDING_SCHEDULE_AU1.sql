CREATE OR REPLACE TRIGGER "ERES"."CB_FUNDING_SCHEDULE_AU1"
AFTER UPDATE ON ERES.CB_FUNDING_SCHEDULE
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */	
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    
	IF :NEW.DELETEDFLAG = 'Y' THEN
	  
	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_FUNDING_SCHEDULE',:OLD.rid,:OLD.PK_FUNDING_SCHEDULE,'D',:NEW.LAST_MODIFIED_BY);	
  
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','PK_FUNDING_SCHEDULE',:OLD.PK_FUNDING_SCHEDULE,NULL,NULL,NULL);
	 PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','FK_CBB_ID',:OLD.FK_CBB_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','DATE_TO_GENERATE', to_char(:OLD.DATE_TO_GENERATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','CBB_REGISTRATION_DATE', to_char(:OLD.CBB_REGISTRATION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','NOTIFICATION_BEFORE',:OLD.NOTIFICATION_BEFORE,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','GENERATED_STATUS',:OLD.GENERATED_STATUS,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','CREATOR',:OLD.CREATOR,NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','CREATED_ON', to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','LAST_MODIFIED_DATE', to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','RID',:OLD.RID,NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','DELETEDFLAG',:OLD.DELETEDFLAG,NULL,NULL,NULL);
   ELSE
	
	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_FUNDING_SCHEDULE',:OLD.RID,:OLD.PK_FUNDING_SCHEDULE,'U',:NEW.LAST_MODIFIED_BY); 
	
	IF NVL(:OLD.PK_FUNDING_SCHEDULE,0) != NVL(:NEW.PK_FUNDING_SCHEDULE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','PK_FUNDING_SCHEDULE', :OLD.PK_FUNDING_SCHEDULE, :NEW.PK_FUNDING_SCHEDULE,NULL,NULL);
  END IF;  
	IF NVL(:OLD.FK_CBB_ID,0) != NVL(:NEW.FK_CBB_ID,0) THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','FK_CBB_ID',:OLD.FK_CBB_ID, :NEW.FK_CBB_ID,NULL,NULL);
  END IF; 
  IF NVL(:OLD.DATE_TO_GENERATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) != NVL(:NEW.DATE_TO_GENERATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','DATE_TO_GENERATE',
       TO_CHAR(:OLD.DATE_TO_GENERATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.DATE_TO_GENERATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;	
 IF NVL(:OLD.CBB_REGISTRATION_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) != NVL(:NEW.CBB_REGISTRATION_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','CBB_REGISTRATION_DATE',
       TO_CHAR(:OLD.CBB_REGISTRATION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CBB_REGISTRATION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF NVL(:OLD.NOTIFICATION_BEFORE,0) != NVL(:NEW.NOTIFICATION_BEFORE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','NOTIFICATION_BEFORE',:OLD.NOTIFICATION_BEFORE, :NEW.NOTIFICATION_BEFORE,NULL,NULL);
  END IF; 
  IF NVL(:OLD.GENERATED_STATUS,' ') != NVL(:NEW.GENERATED_STATUS,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','GENERATED_STATUS',:OLD.GENERATED_STATUS, :NEW.GENERATED_STATUS,NULL,NULL);
  END IF;  
   IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE', 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
  END IF;
  IF NVL(:OLD.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) != NVL(:NEW.CREATED_ON,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF; 	
	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
	END IF;   
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;  	
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);     
    END IF;	
	IF NVL(:OLD.DELETEDFLAG,' ') != NVL(:NEW.DELETEDFLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_SCHEDULE','DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);     
    END IF;       
  END IF;  
END ;
/