package com.velos.jms;

import java.util.Enumeration;
import java.util.Hashtable;

public class PublisherExceptionListener implements javax.jms.ExceptionListener {

    Publisher publisher = null;

    static Hashtable htPublishers = new Hashtable();

    public PublisherExceptionListener() {
    }

    public static void addPublisher(Publisher publisher) {
        String tName = publisher.getTopicName();

        if (htPublishers.containsKey(tName)) {

        } else {
            htPublishers.put(tName, publisher);
        }

        System.out.println("in PublisherExceptionListner.. addPublisher  ");

    }

    public static void removePublisher(Publisher publisher) {
        String tName = publisher.getTopicName();
        System.out.println("in PublisherExceptionListner.. removePublisher  ");

        if (htPublishers.containsKey(tName)) {
            htPublishers.remove(tName);
        }

    }

    /**
     * Handle asynchronous problem with the connection. (as specified in the
     * javax.jms.ExceptionListener interface).
     */
    public void onException(javax.jms.JMSException jsme) {
        System.out.println("in PublisherExceptionListner... got exception  ");
        onInterpretException(jsme);
    }

    public void onInterpretException(javax.jms.JMSException jsme) {
        // to handle when connection exception occurs for QueryGen subscriber

        // See if connection was dropped.

        // Tell the user that there is a problem.

        // See if the error is a dropped connection. If so, try to reconnect.
        // NOTE: the test is against Progress SonicMQ error codes.

        for (Enumeration enu = htPublishers.elements(); enu.hasMoreElements();) {
            Publisher publisher = (Publisher) enu.nextElement();
            if (publisher != null) {

                if (publisher.isConnectionDropped(jsme)) {

                    System.out
                            .println("in PublisherExceptionListner.... got exception Connection dropped ");
                    System.out
                            .println("in PublisherExceptionListner.......... trying to re connect ");

                    // Reestablish the connection

                    publisher.closeJMSConnection();
                    publisher.setBJMSConnected(false);
                    publisher.createJMSConnection();

                    System.out
                            .println("in PublisherExceptionListner.......... re connected ");

                    String tName = publisher.getTopicName();
                    publisher.reInit();
                    publisher.createJMSTopicPublisher();
                    publisher.publishQueuedMessages();

                    System.out
                            .println("in PublisherExceptionListner.......... publishQueuedMessages ");

                }
            }

        }

    }

}