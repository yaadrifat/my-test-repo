/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Thu Jan 29
 * 09:21:03 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2011)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.BrowserRows;

// /__astcwz_class_idt#(1014!n)
public class DBLogger {

    public static int impexId;

    // will be used for imports

    public static String siteCode;

    // /__astcwz_opern_idt#(1080)
    public static int log2DB(String severity, String messageCode,
            String messageDesc, String module) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int returnVal = 0;
        try {
            // conn = EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_IMPEXLOGGING.SP_RECORDLOG(?,?,?,?,?,?,?)}");

            cstmt.setInt(1, impexId);
            cstmt.setString(2, severity);
            cstmt.setString(3, messageCode);
            cstmt.setString(4, messageDesc);
            cstmt.setString(5, module);
            cstmt.setString(6, siteCode);

            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.execute();

            returnVal = cstmt.getInt(7);
            return returnVal;

        } catch (SQLException ex) {
            System.out.println("Exception in calling DBLogger:log2DB : " + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }

        }
        return returnVal;

    }

    public static int log2DB(String messageDesc, String module) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int returnVal = 0;
        try {
            // conn = EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_IMPEXLOGGING.SP_RECORDLOGSITE(?,?,?,?,?)}");

            cstmt.setInt(1, impexId);
            cstmt.setString(2, messageDesc);
            cstmt.setString(3, module);
            cstmt.setString(4, siteCode);

            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);
            cstmt.execute();

            returnVal = cstmt.getInt(5);
            return returnVal;

        } catch (SQLException ex) {
            System.out.println("Exception in calling DBLogger:log2DB : " + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }

        }
        return returnVal;

    }

    public static int log2DB(String messageDesc) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int returnVal = 0;
        try {
            // conn = EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_IMPEXLOGGING.SP_RECORDLOGSITE(?,?,?,?)}");

            cstmt.setInt(1, impexId);
            cstmt.setString(2, messageDesc);
            cstmt.setString(3, siteCode);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();

            returnVal = cstmt.getInt(4);
            return returnVal;

        } catch (SQLException ex) {
            System.out.println("Exception in calling DBLogger:log2DB : " + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }

        }
        return returnVal;

    }

    public static int log2DB(String severity, String messageCode,
            String messageDesc) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int returnVal = 0;
        try {
            // conn = EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_IMPEXLOGGING.SP_RECORDLOGSITE(?,?,?,?,?,?)}");

            cstmt.setInt(1, impexId);
            cstmt.setString(2, severity);
            cstmt.setString(3, messageCode);
            cstmt.setString(4, messageDesc);
            cstmt.setString(5, siteCode);

            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.execute();

            returnVal = cstmt.getInt(6);
            return returnVal;

        } catch (SQLException ex) {
            System.out.println("Exception in calling DBLogger:log2DB : " + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                System.out.println("Exception in calling DBLogger:log2DB : "
                        + e);
            }

        }
        return returnVal;

    }

    // /__astcwz_opern_idt#(1081)
    public static String generateLogFileData(int expId) {

        String logSQL = " Select PK_LOG ,FK_REQLOG , to_char(LOG_DATETIME,pkg_dateutil.f_get_datetimeformat) LOG_DATETIME , LOG_SEVERITY , LOG_MESSAGECODE  , LOG_DESC , LOG_MODULE from erv_explog  where FK_REQLOG = ? Order by PK_LOG ";

        PreparedStatement pstmt = null;
        Connection conn = null;

        String pkLog;
        String logDate;
        String logSeverity;
        String logMsgCode;
        String logDesc;
        String logModule;

        StringBuffer sbLog = new StringBuffer();

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmt = conn.prepareStatement(logSQL);

            pstmt.setInt(1, expId);

            ResultSet rs = pstmt.executeQuery();

            sbLog.append("\nLOG: \n");

            sbLog
                    .append("\nLOG ID \t TIMESTAMP \t SEVERITY \t CODE \t DESCRIPTION \t MODULE \t\n");

            System.out
                    .println("DBUtil.generateLogFileData --> Query excecuted");

            while (rs.next()) {

                pkLog = rs.getString("PK_LOG");
                logDate = rs.getString("LOG_DATETIME");
                logSeverity = rs.getString("LOG_SEVERITY");
                logMsgCode = rs.getString("LOG_MESSAGECODE");
                logDesc = rs.getString("LOG_DESC");
                logModule = rs.getString("LOG_MODULE");

                sbLog.append(pkLog + "\t" + logDate + "\t" + logSeverity + "\t"
                        + logMsgCode + "\t" + logDesc + "\t" + logModule
                        + "\t\n");

            }

            return sbLog.toString();

        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            System.out.println("DBUtil.generateLogFileData --> "
                    + ex.toString());
            return "";
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

    // /__astcwz_opern_idt#(1082)
    public void viewCompleteLog() {

    }

    public static BrowserRows getLogFileData(int expId) {

        String logSQL = " Select PK_LOG ,FK_REQLOG , to_char(LOG_DATETIME,pkg_dateutil.f_get_datetimeformat) LOG_DATETIME , LOG_SEVERITY , LOG_MESSAGECODE  , LOG_DESC , LOG_MODULE from erv_explog  where FK_REQLOG = ? Order by PK_LOG ";

        PreparedStatement pstmt = null;
        Connection conn = null;

        String pkLog;
        String logDate;
        String logSeverity;
        String logMsgCode;
        String logDesc;
        String logModule;
        int rows = 0;
        Hashtable ht = new Hashtable();

        BrowserRows br = new BrowserRows();

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmt = conn.prepareStatement(logSQL);

            pstmt.setInt(1, expId);

            ResultSet rs = pstmt.executeQuery();

            br.setBColumns("PK_LOG");
            br.setBColumns("LOG_DATETIME");
            br.setBColumns("LOG_SEVERITY");
            br.setBColumns("LOG_MESSAGECODE");
            br.setBColumns("LOG_DESC");
            br.setBColumns("LOG_MODULE");

            while (rs.next()) {

                pkLog = rs.getString("PK_LOG");
                logDate = rs.getString("LOG_DATETIME");
                logSeverity = rs.getString("LOG_SEVERITY");
                logMsgCode = rs.getString("LOG_MESSAGECODE");
                logDesc = rs.getString("LOG_DESC");
                logModule = rs.getString("LOG_MODULE");

                ArrayList ar = new ArrayList();

                ar.add(pkLog);
                ar.add(logDate);
                ar.add(logSeverity);
                ar.add(logMsgCode);
                ar.add(logDesc);
                ar.add(logModule);
                ar.add(pkLog);
                rows++;
                ht.put(new Integer(rows), ar);

            }

            br.setBValues(ht);
            br.setHasPrevious(false);
            br.setHasMore(false);
            br.setRowReturned(rows);
            br.setShowPages(1);
            br.setStartPage(1);
            br.setFirstRec(1);
            br.setLastRec(rows);

            return br;

        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            System.out.println("DBUtil.getLogFileData --> " + ex.toString());
            return br;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

    public static BrowserRows getLogFileData(int expId, String aSiteCode) {

        String logSQL = " Select PK_LOG ,FK_REQLOG , to_char(LOG_DATETIME,pkg_dateutil.f_get_datetimeformat) LOG_DATETIME , LOG_SEVERITY , LOG_MESSAGECODE  , LOG_DESC , LOG_MODULE from erv_explog  where FK_REQLOG = ? and SITE_CODE = ? Order by PK_LOG ";

        PreparedStatement pstmt = null;
        Connection conn = null;

        String pkLog;
        String logDate;
        String logSeverity;
        String logMsgCode;
        String logDesc;
        String logModule;
        int rows = 0;
        Hashtable ht = new Hashtable();

        BrowserRows br = new BrowserRows();

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmt = conn.prepareStatement(logSQL);

            pstmt.setInt(1, expId);
            pstmt.setString(2, aSiteCode);

            ResultSet rs = pstmt.executeQuery();

            br.setBColumns("PK_LOG");
            br.setBColumns("LOG_DATETIME");
            br.setBColumns("LOG_SEVERITY");
            br.setBColumns("LOG_MESSAGECODE");
            br.setBColumns("LOG_DESC");
            br.setBColumns("LOG_MODULE");

            while (rs.next()) {

                pkLog = rs.getString("PK_LOG");
                logDate = rs.getString("LOG_DATETIME");
                logSeverity = rs.getString("LOG_SEVERITY");
                logMsgCode = rs.getString("LOG_MESSAGECODE");
                logDesc = rs.getString("LOG_DESC");
                logModule = rs.getString("LOG_MODULE");

                ArrayList ar = new ArrayList();

                ar.add(pkLog);
                ar.add(logDate);
                ar.add(logSeverity);
                ar.add(logMsgCode);
                ar.add(logDesc);
                ar.add(logModule);
                ar.add(pkLog);
                rows++;
                ht.put(new Integer(rows), ar);

            }

            br.setBValues(ht);
            br.setHasPrevious(false);
            br.setHasMore(false);
            br.setRowReturned(rows);
            br.setShowPages(1);
            br.setStartPage(1);
            br.setFirstRec(1);
            br.setLastRec(rows);

            return br;

        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            System.out.println("DBUtil.getLogFileData --> " + ex.toString());
            return br;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

}
