/**
 * __astcwz_cmpnt_det: Author: Konesa CodeWizard Date and Time Created: Tue Feb
 * 10 13:44:10 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2102)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

// /__astcwz_class_idt#(2092!n)
public class FTPInfo {
    // /__astcwz_attrb_idt#(2086)
    private String ftpIp;

    // /__astcwz_attrb_idt#(2087)
    private String ftpUser;

    // /__astcwz_attrb_idt#(2088)
    private String ftpPass;

    // /__astcwz_attrb_idt#(2089)
    private String ftpFolder;

    // /__astcwz_default_constructor
    public FTPInfo() {
    }

    // /__astcwz_opern_idt#()
    public String getFtpIp() {
        return ftpIp;
    }

    // /__astcwz_opern_idt#()
    public String getFtpUser() {
        return ftpUser;
    }

    // /__astcwz_opern_idt#()
    public String getFtpPass() {
        return ftpPass;
    }

    // /__astcwz_opern_idt#()
    public String getFtpFolder() {
        return ftpFolder;
    }

    // /__astcwz_opern_idt#()
    public void setFtpIp(String aftpIp) {
        ftpIp = aftpIp;
    }

    // /__astcwz_opern_idt#()
    public void setFtpUser(String aftpUser) {
        ftpUser = aftpUser;
    }

    // /__astcwz_opern_idt#()
    public void setFtpPass(String aftpPass) {
        ftpPass = aftpPass;
    }

    // /__astcwz_opern_idt#()
    public void setFtpFolder(String aftpFolder) {
        ftpFolder = aftpFolder;
    }

}
