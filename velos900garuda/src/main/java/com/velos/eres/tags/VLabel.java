package com.velos.eres.tags;

import com.velos.eres.business.common.Style;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

/**
 * VLabel is designed to generate XSL for form field 'Labels'. The class is also used to generate XSL for 'Comment' type
 * fields.
 * @author Sonia Sahni
 * @vesion %I%, %G%
 */
/*******************************************************************************
 * **************************History Modified by :Sonia Sahni Modified
 * On:06/21/2004 Comment: 1. Added JavaDoc comments for some methods 2. Modified
 * drawXSL (), see method history.
 * *************************END****************************************
 */

// /__astcwz_class_idt#(1464!n)
public class VLabel extends VTag {
    // /__astcwz_attrb_idt#(1466)
    private String labelFor;

    private String text;

    private Style displayStyle;

    private boolean isRequired;

    private boolean softCheckStar;

    private boolean forRequired;

    // preserve user entered format
    private boolean userFormat;

    // display help icon or not
    private boolean helpIconRequired;

    /** The the width of the TD in which field label will be displayed */

    private String LabelDisplayWidth;

    // /__astcwz_default_constructor
    public VLabel() {
        this.labelFor = "";
        this.text = "";
        this.displayStyle = new Style();
        LabelDisplayWidth = "";

    }

    public VLabel(String labelFor, String text, Style displayStyle) {
        this.labelFor = labelFor;
        this.text = text;
        this.displayStyle = displayStyle;
        LabelDisplayWidth = "";

    }

    public VLabel(String text) {
        this.text = text;
        this.displayStyle = new Style();
        this.labelFor = "";
        LabelDisplayWidth = "";

    }

    public boolean getIsRequired() {
        return this.isRequired;
    }

    public void setIsRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public boolean getSoftCheckStar() {
        return this.softCheckStar;
    }

    public void setSoftCheckStar(boolean softCheckStar) {
        this.softCheckStar = softCheckStar;
    }

    public boolean getForRequired() {
        return this.forRequired;
    }

    public void setForRequired(boolean forRequired) {
        this.forRequired = forRequired;
    }

    public boolean getHelpIconRequired() {
        return this.helpIconRequired;
    }

    public void setHelpIconRequired(boolean helpIconRequired) {
        this.helpIconRequired = helpIconRequired;
    }

    public String getLabelFor() {
        return this.labelFor;
    }

    public String getLabelForTag() {
        if (!EJBUtil.isEmpty(this.labelFor))
            return " for = \"" + this.labelFor + "\"";
        else
            return "";
    }
    
    public String getLabelIDTag() {
        if (!EJBUtil.isEmpty(this.labelFor))
            return " id = \"" + this.labelFor + "_id" +"\"";
        else
            return "";
    }
    

    public void setLabelFor(String alabelFor) {
        labelFor = alabelFor;
    }

    /**
     * Gets Label text.
     * 
     * @return Gets text for the Label.
     */
    public String getText() {
        return this.text;
    }

    /**
     * Sets Label text.
     * 
     * @param text
     *            Display text for the Label
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets Display Style string. It is used to define the style attribute of a
     * Label.
     * 
     * @return Display Style string
     */
    public Style getDisplayStyle() {
        return this.displayStyle;
    }

    /**
     * Sets Display Style string. It is used to define the style attribute of a
     * Label.
     * 
     * @param displayStyle
     *            Display Style string
     */
    public void setDisplayStyle(Style displayStyle) {
        this.displayStyle = displayStyle;
    }

    /**
     * Gets user format flag. The flag identifies if the label should preserve
     * user formatting (tabs, spaces,newline,etc)
     * 
     * @return Boolean flag to identify if the label should preserve user
     *         formatting
     */

    public boolean getUserFormat() {
        return this.userFormat;
    }

    /**
     * Sets user format flag. The flag identifies if the label should preserve
     * user formatting (tabs, spaces,newline,etc)
     * 
     * @param userFormat
     *            Boolean flag to identify if the label should preserve user
     *            formatting
     */
    public void setUserFormat(boolean userFormat) {
        this.userFormat = userFormat;
    }

    /**
     * Returns the value of LabelDisplayWidth.
     */
    public String getLabelDisplayWidth() {
        return LabelDisplayWidth;
    }

    /**
     * Sets the value of LabelDisplayWidth.
     * 
     * @param LabelDisplayWidth
     *            The value to assign LabelDisplayWidth.
     */
    public void setLabelDisplayWidth(String LabelDisplayWidth) {
        this.LabelDisplayWidth = LabelDisplayWidth;
    }

    /**
     * Generates and returns XSL for the Label object
     * 
     * @return XSL string for the Label object
     */
    /***************************************************************************
     * **************************History Modified by :Sonia Sahni Modified
     * On:06/21/2004 Comment: 1. Added JavaDoc comment 2. Added a CSS class for
     * <PRE> in Label XSL. (used fo comment type label)
     * *************************END****************************************
     */
    public String drawXSL() {

        StringBuffer xslStr = new StringBuffer();

        xslStr.append("<label ");
        xslStr.append(getTagClass());
        xslStr.append(getStyle());
        xslStr.append(getToolTip());
        xslStr.append(getId() + " "+ getLabelIDTag() );
        if (forRequired) {
            xslStr.append(getLabelForTag());
        }
        xslStr.append(" >");
        xslStr.append((getDisplayStyle()).getDisplayStartTags());
        /*
         * if (userFormat) { xslStr.append("<pre class = \"dynFormPRE\"> "); }
         */
        xslStr.append(StringUtil.unEscapeSpecialChar(getText()));

        /*
         * if (userFormat) { xslStr.append("</pre>"); }
         */

        xslStr.append((getDisplayStyle()).getDisplayEndTags());
        if (isRequired) {
            xslStr.append("<font class = \"mandatory\">*</font>");
        }
        if (softCheckStar) {
            xslStr.append("<font class = \"green\">*</font>");
        }

        xslStr.append("</label>");

        xslStr.append("  &#xa0;&#xa0;&#xa0;");
        return xslStr.toString();
    }

    /**
     * drawXSL() overloaded to generate XSL with or without a
     * <td> tag. Generates and returns XSL for the Label object
     * 
     * @param withTD
     *            boolean flag to specify if XSL should be enclosed within a
     *            <TD>.
     * @return XSL string for the Label object
     */
    public String drawXSL(boolean withTD) {
        if (withTD) {
            StringBuffer xslStr = new StringBuffer();

            // check whether the field label has to be expanded

            String expLabel = getDisplayStyle().getExpLabel();

            String labelDisplayWidthStr = "";

            labelDisplayWidthStr = getLabelDisplayWidth();

            if (!StringUtil.isEmpty(labelDisplayWidthStr)) {
                xslStr.append("<td width= \'" + labelDisplayWidthStr + "%\'  "
                        + ((getDisplayStyle()).getAlignVal()) + " >");
            } else if (StringUtil.isEmpty(labelDisplayWidthStr)
                    && expLabel.equals("1")) {
                xslStr.append("<td " + ((getDisplayStyle()).getAlignVal())
                        + " >");
            } else {
                xslStr.append("<td width='15%' "
                        + ((getDisplayStyle()).getAlignVal()) + " >");
            }

            xslStr.append(drawXSL());

            //Bug #3870
            String helpXslStr = "";
            helpXslStr = getHelpIcon();
            
            if (helpXslStr.length() > 0){
            	String trimPart="</label>";
            	String firstPart = xslStr.substring(0, xslStr.indexOf(trimPart));
            	xslStr = new StringBuffer();
            	xslStr = xslStr.append(firstPart)
            			.append("  &#xa0;&#xa0;&#xa0;")
            			.append(helpXslStr)
            			.append(trimPart);
            	xslStr = xslStr;
            }
            // if (helpIconRequired)
            // {
            //xslStr.append(getHelpIcon());
            // }

            xslStr.append("</td>");
            return xslStr.toString();
        } else {
        	//Bug #7613
        	StringBuffer xslStr = new StringBuffer();
        	xslStr.append(drawXSL());
        	
            String helpXslStr = "";
            helpXslStr = getHelpIcon();
            
            if (helpXslStr.length() > 0){
            	String trimPart="</label>";
            	String firstPart = xslStr.substring(0, xslStr.indexOf(trimPart));
            	xslStr = new StringBuffer();
            	xslStr = xslStr.append(firstPart)
            			.append("  &#xa0;&#xa0;&#xa0;")
            			.append(helpXslStr)
            			.append(trimPart);
            	xslStr = xslStr;
            }
            return xslStr.toString();
        }

    }

}