package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class StudyViewDao extends CommonDAO implements java.io.Serializable {

    private ArrayList svPk;

    private ArrayList svStudyId;

    private ArrayList svUser;

    private ArrayList svStudynum;

    private ArrayList svStudyTitle;

    private ArrayList svUserLName;

    private ArrayList svUserFName;

    private ArrayList svUserAccount;

    public StudyViewDao() {

        svPk = new ArrayList();
        svStudyId = new ArrayList();
        svUser = new ArrayList();
        svStudynum = new ArrayList();
        svStudyTitle = new ArrayList();
        svUserLName = new ArrayList();
        svUserFName = new ArrayList();
        svUserAccount = new ArrayList();
    }

    // GETTER SETTER METHODS

    public ArrayList getSvPk() {
        return this.svPk;
    }

    public void setSvPk(ArrayList svPk) {
        this.svPk = svPk;
    }

    public ArrayList getSvStudyId() {
        return this.svStudyId;
    }

    public void setSvStudyId(ArrayList svStudyId) {
        this.svStudyId = svStudyId;
    }

    public ArrayList getSvStudynum() {
        return this.svStudynum;
    }

    public void setSvStudynum(ArrayList svStudynum) {
        this.svStudynum = svStudynum;
    }

    public ArrayList getSvStudyTitle() {
        return this.svStudyTitle;
    }

    public void setSvStudyTitle(ArrayList svStudyTitle) {
        this.svStudyTitle = svStudyTitle;
    }

    public ArrayList getSvUser() {
        return this.svUser;
    }

    public void setSvUser(ArrayList svUser) {
        this.svUser = svUser;
    }

    public ArrayList getSvUserFName() {
        return this.svUserFName;
    }

    public void setSvUserFName(ArrayList svUserFName) {
        this.svUserFName = svUserFName;
    }

    public ArrayList getSvUserLName() {
        return this.svUserLName;
    }

    public void setSvUserLName(ArrayList svUserLName) {
        this.svUserLName = svUserLName;
    }

    public void setSvUser(Integer svUser) {
        this.svUser.add(svUser);
    }

    public void setSvStudyTitle(String svStudyTitle) {
        this.svStudyTitle.add(svStudyTitle);
    }

    public void setSvStudynum(String svStudynum) {
        this.svStudynum.add(svStudynum);
    }

    public void setSvStudyId(Integer svStudyId) {
        this.svStudyId.add(svStudyId);
    }

    public void setSvPk(Integer svPk) {
        this.svPk.add(svPk);
    }

    public void setSvUserFName(String svUserFName) {
        this.svUserFName.add(svUserFName);
    }

    public void setSvUserLName(String svUserLName) {
        this.svUserLName.add(svUserLName);
    }

    public ArrayList getSvUserAccount() {
        return this.svUserAccount;
    }

    public void setSvUserAccount(ArrayList svUserAccount) {
        this.svUserAccount = svUserAccount;
    }

    public void setSvUserAccount(Integer svUserAccount) {
        this.svUserAccount.add(svUserAccount);
    }

    // END OF GETTER SETTER METHODS

    public void getStudyViewByUserId(int userId) {
        // //////////////
        Connection conn = null;
        PreparedStatement pstmt = null;
        // ////////////
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select pk_studyview, fk_study,STUDYVIEW_NUM ,STUDYVIEW_TITLE  from er_studyview where fk_user = ?");

            pstmt.setInt(1, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setSvPk(new Integer(rs.getInt("pk_studyview")));
                setSvStudyId(new Integer(rs.getInt("fk_study")));
                setSvStudynum(rs.getString("STUDYVIEW_NUM"));
                setSvStudyTitle(rs.getString("STUDYVIEW_TITLE"));

            }

        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM STUDY VIEW" + ex);
            Rlog.fatal("studyview", "EXCEPTION IN FETCHING FROM STUDY VIEW"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // /////////////

    }

    public void getStudyViewByUserId(int userId, int studycount) {
        // //////////////
        Connection conn = null;
        PreparedStatement pstmt = null;
        // ////////////
        try {
            conn = getConnection();

            String sql = "Select * from "
                    + "(Select pk_studyview, fk_study,STUDYVIEW_NUM ,"
                    + " STUDYVIEW_TITLE  "
                    + " from "
                    + " er_studyview where fk_user = ? order by CREATED_ON desc ) a "
                    + " where rownum <= ?";

            pstmt = conn.prepareStatement(sql);

            Rlog.debug("studyview", "sql is " + sql);

            pstmt.setInt(1, userId);
            pstmt.setInt(2, studycount);

            Rlog.debug("studyview", "sql is " + "Before fetching the query");

            ResultSet rs = pstmt.executeQuery();

            Rlog.debug("studyview", "sql is " + "After fetching the query");

            while (rs.next()) {
                setSvPk(new Integer(rs.getInt("pk_studyview")));
                setSvStudyId(new Integer(rs.getInt("fk_study")));
                setSvStudynum(rs.getString("STUDYVIEW_NUM"));
                setSvStudyTitle(rs.getString("STUDYVIEW_TITLE"));

            }

        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM STUDY VIEW" + ex);
            Rlog.fatal("studyview", "EXCEPTION IN FETCHING FROM STUDY VIEW"
                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // /////////////

    }

    public void getUserByStudyView(int studyId) {
        // //////////////
        Connection conn = null;
        PreparedStatement pstmt = null;
        // ////////////
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select DISTINCT er_user.USR_LASTNAME, er_user.USR_FIRSTNAME, er_user.fk_account from er_user, er_studyview where er_studyview.fk_study = ? and er_user.pk_user = er_studyview.fk_user ORDER BY er_user.USR_FIRSTNAME,er_user.USR_LASTNAME ");

            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                // setSvPk(new Integer(rs.getInt(3)));
                setSvUserFName(rs.getString(2));
                setSvUserLName(rs.getString(1));
                setSvUserAccount(new Integer(rs.getInt(3)));

            }

        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM STUDY VIEW" + ex);
            Rlog.fatal("studyview", "EXCEPTION IN FETCHING FROM STUDY VIEW"
                    + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // /////////////

    }

    public int getStudyViewIdByStudy(int studyId) {
        int svPk = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        // ////////////
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("Select pk_studyview from er_studyview where fk_study = ?  and fk_user is null");
            pstmt.setInt(1, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                svPk = rs.getInt("pk_studyview");

            }
            return svPk;

        } catch (SQLException ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM STUDY VIEW" + ex);
            Rlog.fatal("studyview", "EXCEPTION IN FETCHING FROM STUDY VIEW"
                    + ex);
            return 0;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // /////////////

    }

}
