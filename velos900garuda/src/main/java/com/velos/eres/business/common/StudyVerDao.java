/*
 * Classname			StudyVer.class
 * 
 * Version information 	1.0
 *
 * Date					12/03/2002				
 * 
 * Copyright notice		Velos, Inc.
 * by Arvind Kumar		
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * StudyVerDao for Study Versions
 * 
 * @author Arvind
 * @version : 1.0 12/03/2002
 */

public class StudyVerDao extends CommonDAO implements java.io.Serializable {
    private ArrayList studyVerIds;

    private ArrayList studyVerStudyIds;

    private ArrayList studyVerNumbers;

    private ArrayList studyVerStatus;

    private ArrayList studyVerNotes;
    
    
    //JM: 11/17/2005  
    private ArrayList studyVerDates;
    
    private ArrayList studyVerCategories;
    
    private ArrayList studyVerCategoryIds;

    private ArrayList studyVerTypes;

    /**
     * Stores a collection of Version Status Description (Description from
     * er_codelst)
     */
    private ArrayList studyVerStatusDescs;

    /** Stores a collection of Version Status Pks ( from er_status_history) */
    private ArrayList studyVerStatusPKs;

    private ArrayList creators;

    private ArrayList ipAdds;

    private ArrayList secCounts;

    private ArrayList apndxCounts;

    private int cRows;

    /**
     * Default Constructor
     */

    /*
     * Modified by Sonia Sahni 08/11/04, added initialization for
     * studyVerStatusDesc,studyVerStatusPKs
     */

    public StudyVerDao() {
        studyVerIds = new ArrayList();
        studyVerStudyIds = new ArrayList();
        studyVerNumbers = new ArrayList();
        studyVerStatus = new ArrayList();
        studyVerNotes = new ArrayList();
        
        studyVerDates = new  ArrayList();
        studyVerCategories = new ArrayList();
        studyVerCategoryIds = new ArrayList();
        studyVerTypes = new ArrayList();
        
        secCounts = new ArrayList();
        apndxCounts = new ArrayList();
        studyVerStatusDescs = new ArrayList();
        studyVerStatusPKs = new ArrayList();

        creators = new ArrayList();
        ipAdds = new ArrayList();

    }

    // Getter and Setter methods

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public ArrayList getSecCounts() {
        return this.secCounts;
    }

    public void setSecCounts(ArrayList secCounts) {
        this.secCounts = secCounts;
    }

    public ArrayList getApndxCounts() {
        return this.apndxCounts;
    }

    public void setApndxCounts(ArrayList apndxCounts) {
        this.apndxCounts = apndxCounts;
    }

    public ArrayList getStudyVerIds() {
        return this.studyVerIds;
    }

    public void setStudyVerIds(ArrayList studyVerIds) {
        this.studyVerIds = studyVerIds;
    }

    public ArrayList getStudyVerStudyIds() {
        return this.studyVerStudyIds;
    }

    public void setStudyVerStudyIds(ArrayList studyVerStudyIds) {
        this.studyVerStudyIds = studyVerStudyIds;
    }

    public ArrayList getStudyVerNumbers() {
        return this.studyVerNumbers;
    }

    public void setStudyVerNumbers(ArrayList studyVerNumbers) {
        this.studyVerNumbers = studyVerNumbers;
    }

    public ArrayList getStudyVerStatus() {
        return this.studyVerStatus;
    }

    public void setStudyVerStatus(ArrayList studyVerStatus) {
        this.studyVerStatus = studyVerStatus;
    }

    public ArrayList getStudyVerNotes() {
        return this.studyVerNotes;
    }

    public void setStudyVerNotes(ArrayList studyVerNotes) {
        this.studyVerNotes = studyVerNotes;
    }

    public ArrayList getCreators() {
        return this.creators;
    }

    public void setCreators(ArrayList creators) {
        this.creators = creators;
    }

    public ArrayList getIpAdds() {
        return this.ipAdds;
    }

    public void setIpAdds(ArrayList ipAdds) {
        this.ipAdds = ipAdds;
    }

    //

    public void setSecCounts(Integer secCount) {
        secCounts.add(secCount);
    }

    public void setApndxCounts(Integer apndxCount) {
        apndxCounts.add(apndxCount);
    }

    public void setStudyVerIds(Integer verId) {
        studyVerIds.add(verId);
    }

    public void setStudyVerStudyIds(Integer studyId) {
        studyVerStudyIds.add(studyId);
    }

    public void setStudyVerNumbers(String verNo) {
        studyVerNumbers.add(verNo);
    }

    public void setStudyVerStatus(String verStatus) {
        studyVerStatus.add(verStatus);
    }

    public void setStudyVerNotes(String verNote) {
        studyVerNotes.add(verNote);
    }

    public void setCreators(Integer creator) {
        creators.add(creator);
    }

    public void setIpAdds(String ipAdd) {
        ipAdds.add(ipAdd);
    }
    
//JM:
    public void setStudyVerDates(String verDate) {
    	studyVerDates.add(verDate);
    }

    public void setStudyVerCategories(String verCategories) {
    	studyVerCategories.add(verCategories);
    }

    public void setStudyVerTypes(String verTypes) {
    	studyVerTypes.add(verTypes);
    }
//
    

    

    /**
     * appends a study version status description to class member ArrayList
     * studyVerStatusDescs
     * 
     * @param studyVerStatusDesc
     *            Study Version Description
     */
    public void setStudyVerStatusDescs(String studyVerStatusDesc) {
        studyVerStatusDescs.add(studyVerStatusDesc);
    }

    /**
     * Returns an ArrayList of Study Version Descriptions
     * 
     * @return ArrayList of Study Version Descriptions
     */

    public ArrayList getStudyVerStatusDescs() {
        return this.studyVerStatusDescs;
    }

    /**
     * Sets an ArrayList of Study Version Descriptions to class member ArrayList
     * studyVerStatusDescs
     * 
     * @param studyVerStatusDesc
     *            ArrayList of Study Version Descriptions
     */
    public void setStudyVerStatusDescs(ArrayList studyVerStatusDescs) {
        this.studyVerStatusDescs = studyVerStatusDescs;
    }

    /**
     * appends a study version Status PK to class member ArrayList
     * studyVerStatusPKs
     * 
     * @param studyVerStatusPK
     *            Study Version Status PK
     */
    public void setStudyVerStatusPKs(String studyVerStatusPK) {
        studyVerStatusPKs.add(studyVerStatusPK);
    }

    /**
     * Returns an ArrayList of Study Version Status PKs
     * 
     * @return ArrayList of Study Version Status Pks
     */

    public ArrayList getStudyVerStatusPKs() {
        return this.studyVerStatusPKs;
    }

    /**
     * Sets an ArrayList of Study Version Status Pks to class member ArrayList
     * studyVerStatusPKs
     * 
     * @param studyVerStatusPKs
     *            ArrayList of Study Version Status Pks
     */
    public void setStudyVerStatusPKs(ArrayList studyVerStatusPKs) {
        this.studyVerStatusPKs = studyVerStatusPKs;
    }
    
  //JM 
    /**
     * Returns an ArrayList of Study Version Dates
     * 
     * @return ArrayList of Study Version Dates
     */

    public ArrayList getStudyVerDates() {
        return this.studyVerDates;
    }

    /**
     * Sets an ArrayList of Study Version Dates to class member ArrayList
     * 
     * 
     * @param studyVerDates
     *            ArrayList of Study Version Dates
     */
    public void setStudyVerDates(ArrayList studyVerDates) {
        this.studyVerDates = studyVerDates;
    }
    
    
    
    
    /**
     * Returns an ArrayList of Study Version Categories
     * 
     * @return ArrayList of Study Version Categories
     */

    public ArrayList getStudyVerCategories() {
        return this.studyVerCategories;
    }

    /**
     * Sets an ArrayList of Study Version Categories to class member ArrayList
     * 
     * 
     * @param Categories
     *            ArrayList of Study Version Categories
     */
    public void setStudyVerCategories(ArrayList studyVerCategories) {
        this.studyVerCategories = studyVerCategories;
    }
      
    public ArrayList getStudyVerCategoryIds() {
        return this.studyVerCategoryIds;
    }

    private void setStudyVerCategoryIds(String categoryId) {
        studyVerCategoryIds.add(categoryId);
    }


     public ArrayList getStudyVerTypes(){
    	  
    	  return this.studyVerTypes;
    	  
      }
      
      public void setStudyVerTypes(ArrayList studyVerTypes) {
          this.studyVerTypes = studyVerTypes;
      }
      
      //JM
 
    // end of getter and setter methods

    /**
     * Populates the Data Access object with all study versions
     * 
     * @param StudyId -
     *            Id of the Study
     */

    /*
     * Changed by Sonia Sahni 08/11/04, Version's latest Status will be fetched
     * from er_status_history Now we are maintaining version's status history
     * also.STUDYVER_STATUS will not be used anymore.
     */

    //public void getAllVers(int studyId) { 
      public void getAllVers(int studyId){
          int rows = 0;
          PreparedStatement pstmt = null;
          Connection conn = null;
          
      /*    
          String sWhere = "";
          String vcWhere = "";
          String vtWhere = "";
          String vstWhere = "";
          String obWhere = "";
          String otWhere = "";
          
          */
        //  StringBuffer sqlBuffer = new StringBuffer();
          try {
              conn = getConnection();
              // Modified by Ganapathy on 05/25/05 for may enhancement-2
              String mysql = "SELECT PK_STUDYVER,"
                  + " (select count(*) from er_studysec where fk_studyver = pk_studyver ) secCount, "
                  + " (select count(*) from er_studyapndx where fk_studyver = pk_studyver) apndxCount, "
                  + " FK_STUDY,"
                  + " STUDYVER_NUMBER,STUDYVER_DATE,"
                  + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_CATEGORY) as STUDYVER_CATEGORY,"
                  + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_TYPE) as STUDYVER_TYPE,"
                  + " STUDYVER_NOTES,"
                  + " CREATOR,"
                  + " IP_ADD, CODELST_DESC STATUS_CODEDESC, CODELST_SUBTYP STATUS_CODESUBTYP,PK_STATUS, a.STUDYVER_CATEGORY as STUDYVER_CATID "
                  + " FROM ER_STUDYVER a , ( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from "
                  + " er_studyver i, ER_STATUS_HISTORY, er_codelst c Where i.fk_study = ? and i.PK_STUDYVER =  STATUS_MODPK and STATUS_MODTABLE = ? and "
                  + " STATUS_END_DATE is null and c.pk_codelst = FK_CODELST_STAT ) stat"
                  + " WHERE FK_STUDY = ? and STATUS_MODPK (+) = pk_studyver " ;
              
              Rlog.debug("StudyVer", "StudyVerDao.getAllVers SQL " + mysql);
              pstmt = conn.prepareStatement(mysql);//JM
              pstmt.setInt(1, studyId);
              pstmt.setString(2, "er_studyver");
              pstmt.setInt(3, studyId);

              ResultSet rs = pstmt.executeQuery();

              while (rs.next()) {

                  setStudyVerIds(new Integer(rs.getInt("PK_STUDYVER")));
                  setSecCounts(new Integer(rs.getInt("secCount")));
                  setApndxCounts(new Integer(rs.getInt("apndxCount")));
                  setStudyVerStudyIds(new Integer(rs.getInt("FK_STUDY")));
                  setStudyVerNumbers(rs.getString("STUDYVER_NUMBER"));
                  setStudyVerStatus(rs.getString("STATUS_CODESUBTYP"));
                  setStudyVerNotes(rs.getString("STUDYVER_NOTES"));
                  setCreators(new Integer(rs.getInt("CREATOR")));
                  setIpAdds(rs.getString("IP_ADD"));
                  setStudyVerStatusDescs(rs.getString("STATUS_CODEDESC"));
                  setStudyVerStatusPKs(rs.getString("PK_STATUS"));
                  
                  //JM: 
                  setStudyVerDates(rs.getString("STUDYVER_DATE"));
                  setStudyVerCategories(rs.getString("STUDYVER_CATEGORY"));
                  setStudyVerTypes(rs.getString("STUDYVER_TYPE"));
                  setStudyVerCategoryIds(rs.getString("STUDYVER_CATID"));

                  rows++;
                  Rlog.debug("StudyVer", "StudyVerDao.getAllVers " + rows);
              }

              setCRows(rows);
          } catch (SQLException ex) {
              Rlog.fatal("StudyVer",
                      "StudyVerDao.getAllVers  EXCEPTION IN FETCHING FROM er_studyver table"
                              + ex);
          } finally {
              try {
                  if (pstmt != null)
                      pstmt.close();
              } catch (Exception e) {
              }
              try {
                  if (conn != null)
                      conn.close();
              } catch (Exception e) {
              }

          }

      }


/*
      public void getAllStudyVers(int studyId,String studyVerNumber,String verCat, String verType, String verStat, String orderBy, String orderType){
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        
        
        String sWhere = "";
        String vcWhere = "";
        String vtWhere = "";
        String vstWhere = "";
        String obWhere = "";
        String otWhere = "";
        
        StringBuffer sqlBuffer = new StringBuffer();
        try {
            conn = getConnection();

            String mysqlSelect = "SELECT PK_STUDYVER,"
                    + " (select count(*) from er_studysec where fk_studyver = pk_studyver ) secCount, "
                    + " (select count(*) from er_studyapndx where fk_studyver = pk_studyver) apndxCount, "
                    + " FK_STUDY,"
                    + " STUDYVER_NUMBER,STUDYVER_DATE,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_CATEGORY) as STUDYVER_CATEGORY,"
                    + " (select codelst_desc from er_codelst where pk_codelst=a.STUDYVER_TYPE) as STUDYVER_TYPE,"
                    + " STUDYVER_NOTES,"
                    + " CREATOR,"
                    + " IP_ADD, CODELST_DESC STATUS_CODEDESC, CODELST_SUBTYP STATUS_CODESUBTYP,PK_STATUS "
                    + " FROM ER_STUDYVER a , ( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from "
                    + " er_studyver i, ER_STATUS_HISTORY, er_codelst c Where i.fk_study = ? and i.PK_STUDYVER =  STATUS_MODPK and STATUS_MODTABLE = ? and "
                    + " STATUS_END_DATE is null and c.pk_codelst = FK_CODELST_STAT ) stat"
                    + " WHERE FK_STUDY = ? and STATUS_MODPK (+) = pk_studyver " ;
            
       	 
				if ((studyVerNumber!= null) && (! studyVerNumber.trim().equals("") && ! studyVerNumber.trim().equals("null")))
				{
					sWhere = " and upper(a.STUDYVER_NUMBER) like upper('";							
					sWhere+=studyVerNumber;
					sWhere+="%')";
				}
				
				if ((verCat!= null) && (! verCat.trim().equals("") && ! verCat.trim().equals("null")))
				{
					vcWhere = " and upper(a.STUDYVER_CATEGORY) = upper(";
					vcWhere+=verCat;
					vcWhere+=")";
				}
				
				if ((verType!= null) && (! verType.trim().equals("") && ! verType.trim().equals("null")))
				{
					vtWhere = " and a.STUDYVER_TYPE = UPPER(" ;
					vtWhere+=verType;
					vtWhere+=")";
				}
				
				if ((verStat!= null) && (! verStat.trim().equals("") && ! verStat.trim().equals("null")))
				{
					vstWhere = " and FK_CODELST_STAT = UPPER(" ;
					vstWhere+=verStat;
					vstWhere+=")";
				}
				if ((orderBy!= null) && (! orderBy.trim().equals("") && ! orderBy.trim().equals("null")))
				{
					obWhere = " order by " +orderBy+ " " + orderType;
					
				}
				else{
						  
					obWhere = " order by PK_STUDYVER DESC ";
				}
            
				sqlBuffer.append(mysqlSelect);
				
	            if(sWhere!=null){
	            	sqlBuffer.append(sWhere);
	            }
	            if(vcWhere !=null){
	            	sqlBuffer.append(vcWhere );
	            }
	            if(vtWhere!=null){
	            	sqlBuffer.append(vtWhere);
	            }
	            if(vstWhere !=null){
	            	sqlBuffer.append(vstWhere );
	            }
	            if(orderBy!=null){
	            	sqlBuffer.append(obWhere);
	            }
	            
            
	            String mysqlFinal=sqlBuffer.toString();

            Rlog.debug("StudyVer", "StudyVerDao.getAllVers SQL " + mysqlFinal);
            pstmt = conn.prepareStatement(mysqlFinal);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, "er_studyver");
            pstmt.setInt(3, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudyVerIds(new Integer(rs.getInt("PK_STUDYVER")));
                setSecCounts(new Integer(rs.getInt("secCount")));
                setApndxCounts(new Integer(rs.getInt("apndxCount")));
                setStudyVerStudyIds(new Integer(rs.getInt("FK_STUDY")));
                setStudyVerNumbers(rs.getString("STUDYVER_NUMBER"));
                setStudyVerStatus(rs.getString("STATUS_CODESUBTYP"));
                setStudyVerNotes(rs.getString("STUDYVER_NOTES"));
                setCreators(new Integer(rs.getInt("CREATOR")));
                setIpAdds(rs.getString("IP_ADD"));
                setStudyVerStatusDescs(rs.getString("STATUS_CODEDESC"));
                setStudyVerStatusPKs(rs.getString("PK_STATUS"));
                
                //JM: 
                setStudyVerDates(rs.getString("STUDYVER_DATE"));
                setStudyVerCategories(rs.getString("STUDYVER_CATEGORY"));
                setStudyVerTypes(rs.getString("STUDYVER_TYPE"));

                rows++;
                Rlog.debug("StudyVer", "StudyVerDao.getAllVers " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("StudyVer",
                    "StudyVerDao.getAllStudyVers  EXCEPTION IN FETCHING FROM er_studyver table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
*/
    /**
     * Populates the Data Access Object with study versions with latest status
     * as the specified status. <br>
     * Status is passed as codelst_subtyp for codelst_type = 'versionStatus'
     * 
     * @param studyId
     *            Study Id
     * @param status
     *            Version Status : codelst_subtyp for codelst_type =
     *            'versionStatus'
     */
    /* Modified by sonia sahni, changed to get the status from status history */
    public void getAllVers(int studyId, String status) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            String sql = "SELECT PK_STUDYVER,STUDYVER_NUMBER "
                    + " FROM ER_STUDYVER , ER_STATUS_HISTORY, er_codelst c  "
                    + " WHERE FK_STUDY = ?  and PK_STUDYVER =  STATUS_MODPK and "
                    + " STATUS_MODTABLE = ? and   STATUS_END_DATE is null and "
                    + " c.pk_codelst = FK_CODELST_STAT and CODELST_SUBTYP = ? order by PK_STUDYVER ASC";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setString(2, "er_studyver");
            pstmt.setString(3, status);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setStudyVerIds(new Integer(rs.getInt("PK_STUDYVER")));
                setStudyVerNumbers(rs.getString("STUDYVER_NUMBER"));

                rows++;
                Rlog.debug("StudyVer",
                        "StudyVerDao.getAllVers(int studyId, String status) "
                                + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "StudyVer",
                            "StudyVerDao.getAllVers(int studyId, String status)  EXCEPTION IN FETCHING FROM er_studyver table"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}
