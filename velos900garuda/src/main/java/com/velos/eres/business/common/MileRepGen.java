/*
 * Classname : MileRepGen
 * 
 * Version information: 1.0
 *
 * Date 07/22/2002
 * 
 * Copyright notice: Velos Inc.
 */

package com.velos.eres.business.common;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.MileRepThread;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.report.ReportJBNew;
//import com.velos.eres.web.savedRep.SavedRepJB;
import com.velos.eres.web.site.SiteJB;
//import com.velos.eres.web.study.StudyJB;

/**
 * MileRepGen class for background processing and generation of milestone
 * reports
 * 
 * @author Sonika Talwar
 * @verison 1.0 07/22/2002
 */

public class MileRepGen extends CommonDAO implements java.io.Serializable {

    public void callGenRep(int userId, String ipAdd, int acc, int repId,
            String repName, String genRepName, String dtFrom, String dtTo,
            String studyPk, String patientId, String uName,
            String repArgsDisplay, String orgId) {
        Rlog.debug("common", "IN callGenRep of MileRepGen line 1");

        MileRepThread p = new MileRepThread(userId, ipAdd, acc, repId, repName,
                genRepName, dtFrom, dtTo, studyPk, patientId, uName,
                repArgsDisplay, orgId);
        p.start();

    }

    public void generateReport(int userId, String ipAdd, int acc, int repId,
            String repName, String genRepName, String dtFrom, String dtTo,
            String studyPk, String patientId, String uName,
            String repArgsDisplay, String organizationId) {
        Rlog.debug("common", "MileRepGen.generateReport line 1");

        String intervalType = "";
        String repDate = "";
        String repTitle = "&nbsp;";
        String format = "";
        int repXslId = 0;
        int studyId = EJBUtil.stringToNum(studyPk);

        //StudyJB studyB = new StudyJB();
        /*studyB.setId(studyId);
        studyB.getStudyDetails();
        String studyNumber = studyB.getStudyNumber();
        String studyTitle = studyB.getStudyTitle();*/
        int patId = EJBUtil.stringToNum(patientId);

        int orgId = EJBUtil.stringToNum(organizationId);

        String params = "";
        String repType = "";
        String hdr_file_name = "";
        String ftr_file_name = "";
        String filePath = "";
        String hdrfile = "";
        String ftrfile = "";
        String hdrFilePath = "";
        String ftrFilePath = "";

        Calendar now = Calendar.getInstance();
        repDate = "" + now.get(now.DAY_OF_MONTH) + now.get(now.MONTH)
                + (now.get(now.YEAR) - 1900);

        Calendar calToday = Calendar.getInstance();
        format = "MM/dd/yyyy HH:mm";

        SimpleDateFormat todayFormat = new SimpleDateFormat(format);
        repDate = todayFormat.format(calToday.getTime());

        ReportJBNew repB = new ReportJBNew();

        ReportDaoNew rD = new ReportDaoNew();
        Rlog.debug("common", "MileRepGen.generateReport line 5 repId " + repId);
        Rlog.debug("common", "MileRepGen.generateReport line 6 repArgsDisplay "
                + repArgsDisplay);

        switch (repId) {

        case 67: // Milestones Summary (Study)
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Status Reports";
            intervalType = "";
            rD = repB.getRepMileXml("S", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 68: // Milestones Summary (All Patients)
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Status Reports";
            intervalType = "";
            rD = repB.getRepMileXml("P", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 69: // Milestones Summary (Patient)
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Status Reports";
            intervalType = "";
            rD = repB.getRepMileXml("P", studyId, patId, dtFrom, dtTo,
                    intervalType, 0);
            break;

        case 70: // Milestones by Month
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Status Reports";
            intervalType = "M";
            rD = repB.getRepMileXml("F", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 71: // Milestones by Quarter
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Status Reports";
            intervalType = "Q";
            rD = repB.getRepMileXml("F", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 72: // Milestones by Year
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Status Reports";
            intervalType = "Y";
            rD = repB.getRepMileXml("F", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 73: // Payments Received by Month
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestones/Payments Received Reports";
            intervalType = "M";
            rD = repB.getRepMileXml("R", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 74: // Payments Received by Quarter
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestones/Payments Received Reports";
            intervalType = "Q";
            rD = repB.getRepMileXml("R", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 75: // Payments Received by Year
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestones/Payments Received Reports";
            intervalType = "Y";
            rD = repB.getRepMileXml("R", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 76: // Milestones/Payment Discrepancy by Month
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestones/Payments Received Reports";
            intervalType = "M";
            rD = repB.getRepMileXml("R", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 77: // Milestones/Payment Discrepancy by Quarter
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestones/Payments Received Reports";
            intervalType = "Q";
            rD = repB.getRepMileXml("R", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 78: // Milestones/Payment Discrepancy by Year
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestones/Payments Received Reports";
            intervalType = "Y";
            rD = repB.getRepMileXml("R", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 48: // Milestone Forecast by Month
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Projection Reports";
            intervalType = "M";
            rD = repB.getRepMileXml("FR", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 49: // Milestone Forecast by Quarter
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Projection Reports";
            intervalType = "Q";
            rD = repB.getRepMileXml("FR", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;

        case 50: // Milestone Forecast by Year
            repXslId = repId;
            repArgsDisplay = repArgsDisplay;
            repType = "Milestone Projection Reports";
            intervalType = "Y";
            rD = repB.getRepMileXml("FR", studyId, 0, dtFrom, dtTo,
                    intervalType, orgId);
            break;
        default:
            ;
        }

        ArrayList repXml = rD.getRepXml();

        rD = repB.getRepXsl(repXslId);
        ArrayList repXsl = rD.getXsls();

        Object temp = null;
        String xml = null;
        String xsl = null;

        if (repXml.size() > 0) {
            temp = repXml.get(0);
        }

        if (!(temp == null)) {
            xml = temp.toString();
        } else {
            Rlog.fatal("common", "error in getting xml");
            return;
        }

        if (repXsl.size() > 0) {
            temp = repXsl.get(0);
        }

        if (!(temp == null)) {
            xsl = temp.toString();
        } else {
            Rlog.fatal("common", "error in getting xsl");
            return;
        }

        if ((xml.length()) == 0) { // no data found
            Rlog.debug("common",
                    "MileRepGen.generateReport line 10 no data found");
            return;
        }

        // get hdr and ftr
        rD = repB.getRepHdFtr(repId, acc, userId);

        byte[] hdrByteArray = rD.getHdrFile();
        byte[] ftrByteArray = rD.getFtrFile();
        String hdrflag, ftrflag;

        hdr_file_name = "temph[" + now.get(now.DAY_OF_MONTH)
                + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)
                + now.get(now.HOUR_OF_DAY) + now.get(now.MINUTE)
                + now.get(now.SECOND) + "].jpg";
        Configuration
                .readReportParam(Configuration.ERES_HOME + "eresearch.xml");
        filePath = Configuration.REPTEMPFILEPATH;
        Rlog.debug("common", "MileRepGen.generateReport line 12 filePath "
                + filePath);

        hdrfile = filePath + "/" + hdr_file_name;
        ftr_file_name = "tempf[" + now.get(now.DAY_OF_MONTH)
                + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)
                + now.get(now.HOUR_OF_DAY) + now.get(now.MINUTE)
                + now.get(now.SECOND) + "].jpg";
        ftrfile = filePath + "/" + ftr_file_name;

        hdrFilePath = "../temp/" + hdr_file_name;
        ftrFilePath = "../temp/" + ftr_file_name;

        // check for byte array
        if (!(hdrByteArray == null)) {
            hdrflag = "1";
            ByteArrayInputStream fin = new ByteArrayInputStream(hdrByteArray);

            BufferedInputStream fbin = new BufferedInputStream(fin);

            try {
                File fo = new File(hdrfile);

                FileOutputStream fout = new FileOutputStream(fo);
                Rlog.debug("common", "MileRepGen.generateReport line 14");
                int c;
                while ((c = fbin.read()) != -1) {
                    fout.write(c);
                }
                fbin.close();
                fout.close();
            } catch (FileNotFoundException fe) {
                Rlog.fatal("common", "File header file error " + fe);
                return;
            } catch (IOException ie) {
                Rlog.fatal("common", "File header file error " + ie);
                return;
            }
        } else {
            hdrflag = "0";
        }

        Rlog.debug("common", "MileRepGen.generateReport line 15");
        // check for length of byte array
        if (!(ftrByteArray == null)) {
            ftrflag = "1";
            try {
                Rlog.debug("common", "MileRepGen.generateReport line 16");
                ByteArrayInputStream fin1 = new ByteArrayInputStream(
                        ftrByteArray);
                BufferedInputStream fbin1 = new BufferedInputStream(fin1);
                File fo1 = new File(ftrfile);
                FileOutputStream fout1 = new FileOutputStream(fo1);
                Rlog.debug("common", "MileRepGen.generateReport line 17");
                int c1;
                while ((c1 = fbin1.read()) != -1) {

                    fout1.write(c1);
                }

                fbin1.close();
                fout1.close();
            } catch (FileNotFoundException fe) {
                Rlog.fatal("common", "File header file error " + fe);
                return;
            } catch (IOException ie) {
                Rlog.fatal("common", "File header file error " + ie);
                return;
            }

        } else {
            ftrflag = "0";
        }

        // get the folder name
        Configuration.readAppendixParam(Configuration.ERES_HOME
                + "eresearch.xml");
        String fileDnPath = Configuration.DOWNLOADSERVLET;

        Configuration
                .readReportParam(Configuration.ERES_HOME + "eresearch.xml");
        filePath = Configuration.REPDWNLDPATH;
        Rlog.debug("common", "MileRepGen.generateReport line 19 filePath "
                + filePath);
        // make the file name
        String fileName = "reporthtml" + "[" + now.get(now.DAY_OF_MONTH)
                + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)
                + now.get(now.HOUR_OF_DAY) + now.get(now.MINUTE)
                + now.get(now.SECOND) + "].html";
        // make the complete file name
        String htmlFile = filePath + "/" + fileName;
        try {

            // first save the output in html file
            TransformerFactory tFactory1 = TransformerFactory.newInstance();
            Reader mR1 = new StringReader(xml);
            Reader sR1 = new StringReader(xsl);
            Source xmlSource1 = new StreamSource(mR1);
            Source xslSource1 = new StreamSource(sR1);
            Rlog.debug("common", "MileRepGen.generateReport line 21");
            Transformer transformer1 = tFactory1.newTransformer(xslSource1);
            // Set the params
            transformer1.setParameter("hdrFileName", hdrFilePath);
            transformer1.setParameter("ftrFileName", ftrFilePath);
            transformer1.setParameter("repTitle", repTitle);
            transformer1.setParameter("repName", repName);
            transformer1.setParameter("repBy", uName);
            transformer1.setParameter("repDate", repDate);
            transformer1.setParameter("argsStr", repArgsDisplay);
            transformer1.setParameter("cond", "F");
            transformer1.setParameter("wd", "");
            transformer1.setParameter("xd", "");
            transformer1.setParameter("hd", "");
            transformer1.setParameter("hdrflag", hdrflag);
            transformer1.setParameter("ftrflag", ftrflag);
            transformer1.setParameter("dl", "");

            // Perform the transformation, sending the output to html file
            transformer1.transform(xmlSource1, new StreamResult(htmlFile));
            Rlog.debug("common", "MileRepGen.generateReport line 22");
            // now send it to console
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Reader mR = new StringReader(xml);
            Reader sR = new StringReader(xsl);
            Source xmlSource = new StreamSource(mR);
            Source xslSource = new StreamSource(sR);
            // Generate the transformer.
            Transformer transformer = tFactory.newTransformer(xslSource);

            // Set the param for header and footer
            transformer.setParameter("hdrFileName", hdrFilePath);
            transformer.setParameter("ftrFileName", ftrFilePath);
            transformer.setParameter("repTitle", repTitle);
            transformer.setParameter("repName", repName);
            transformer.setParameter("repBy", uName);
            transformer.setParameter("repDate", repDate);
            transformer.setParameter("argsStr", repArgsDisplay);
            transformer.setParameter("cond", "T");
            transformer.setParameter("wd", "repGetWord.jsp?htmlFile="
                    + htmlFile + "&fileDnPath=" + fileDnPath + "&filePath="
                    + filePath);
            transformer.setParameter("xd", "repGetExcel.jsp?htmlFile="
                    + htmlFile + "&fileDnPath=" + fileDnPath + "&filePath="
                    + filePath);
            transformer.setParameter("hd", "repGetHtml.jsp?htmlFileName="
                    + fileName + "&fileDnPath=" + fileDnPath + "&filePath="
                    + filePath);
            transformer.setParameter("hdrflag", hdrflag);
            transformer.setParameter("ftrflag", ftrflag);
            transformer.setParameter("dl", fileDnPath);
            Rlog.debug("common", "MileRepGen.generateReport line 23");
            StringWriter sw = new StringWriter();
            transformer.transform(xmlSource, new StreamResult(sw));

            String rephtml = sw.toString();

            // Rlog.debug("common","GENERATED HTML " + rephtml);

          //  SavedRepJB savedrepB = new SavedRepJB();
            Rlog.debug("common", "MileRepGen.generateReport line 24");
           /* savedrepB.setSavedRepId(repId);
            savedrepB.setSavedRepStudyId(studyPk);
            savedrepB.setSavedRepReportId((new Integer(repId)).toString());
            savedrepB.setSavedRepRepName(genRepName);*/
            Rlog.debug("common", "MileRepGen.generateReport line 25");
            String user = ((new Integer(userId)).toString());
            /*savedrepB.setSavedRepUserId(user);
            savedrepB.setCreator(user);
            savedrepB.setIpAdd(ipAdd);*/

            if (orgId > 0) {
                String orgName = "";
                SiteJB siteB = new SiteJB();
                siteB.setSiteId(orgId);
                siteB.getSiteDetails();
                orgName = siteB.getSiteName();
                repArgsDisplay = repArgsDisplay + " Organization: " + orgName;
            }

            //savedrepB.setSavedRepFilter("Date Range: " + repArgsDisplay);
           // savedrepB.setSavedRepAsOfDate(repDate);
            Rlog.debug("common", "patientId " + patientId);

            if (!(patientId.equals(""))) {
               // savedrepB.setSavedRepPatId(patientId);
            }

           // int savedrepId = savedrepB.setSavedRepDetails();
            SavedRepDao sd = new SavedRepDao();

            //int ret = sd.updateReportContent(savedrepId, rephtml);
           // Rlog.debug("common", "clob updated return value " + ret);

           /* if (ret == 1) // send mail to user about generated report
            {
                SendReportMail(userId, studyNumber, studyTitle, repName,
                        genRepName, repDate);
            }
*/
        } catch (Exception e) {
            Rlog.fatal("common", "error in generateReport in MileRepGen " + e);
            e.printStackTrace();
            return;
        }

    } // generateReport end

    public void SendReportMail(int userId, String studyNum, String studyTitle,
            String repName, String genRepName, String reqOnAt) {
        Rlog.debug("common", "In SendReportMail userId");
        Connection conn = null;
        PreparedStatement pstmt = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        String msg = null;
        String param = studyNum + "~" + studyTitle + "~" + repName + "~"
                + genRepName + "~" + reqOnAt;

        Rlog.debug("common", "In SendReportMail param " + param);

        try {
            conn = getConnection();

            Rlog.debug("common", "userId in SendReportMail " + userId);

            pstmt = conn
                    .prepareStatement("select ESCH.PKG_COMMON.SCH_GETMAILMSG('rep_mail') as msg from dual");
            Rlog.debug("common",
                    "In After prepareStatement 2 in SendReportMail");
            rs = pstmt.executeQuery();
            rs.next();
            msg = rs.getString("msg");

            String lsql = "select ESCH.PKG_COMMON.SCH_GETMAIL(?,?) as msg from dual";

            pstmt = conn.prepareStatement(lsql);

            pstmt.setString(1, msg);
            pstmt.setString(2, param);

            rs = pstmt.executeQuery();
            rs.next();
            msg = rs.getString("msg");

            Rlog.debug("common",
                    "In After prepareStatement 4 in SendReportMail");

            cstmt = conn.prepareCall("{call pkg_milnot.SP_MILEMAIL(?,?,?)}");
            Rlog.debug("common", "In After prepareCallin SendReportMail");
            cstmt.setString(1, msg);
            cstmt.setString(2, ((new Integer(userId)).toString()));
            cstmt.setInt(3, 0);
            cstmt.execute();

        } catch (Exception e) {
            Rlog.fatal("common", "MileRepGen.SendReportMail " + e);
            return;
        }
        finally{
            try { if (pstmt != null) { pstmt.close(); } } catch(Exception e) {}
            try { if (cstmt != null) { cstmt.close(); } } catch(Exception e) {}
            try { if (conn != null)  { conn.close();  } } catch(Exception e) {}
        }
    }

} // main end

