/*
 * Classname : 			FormLibDao
 * 
 * Version information: 1.0 
 *
 * Date: 				07/18/2003
 * 
 * Copyright notice: 	Velos, Inc
 *
 * Author: 				Anu Khanna
 */

package com.velos.eres.business.common;

/* Import Statements */
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The Form Library Dao.<br>
 * <br>
 * This class has various methods to get resultsets of user data for different
 * parameters.
 * 
 * @author Anu Khanna
 * @vesion 1.0
 */
/*
 * Sonia Abrol Date : 04/06/05 copyFormsFromLib() : Generate Form Fields Action
 * Script for the copied form
 */

public class FormLibDao extends CommonDAO implements java.io.Serializable {

    private ArrayList arrFormLibIds;

    private ArrayList arrCatLibIds;

    private ArrayList arrAccountIds;

    private ArrayList arrSharedWithIds;

    private ArrayList arrFormTypes;

    private ArrayList arrFormNames;

    private ArrayList arrDescriptions;

    private ArrayList arrStatus;

    private ArrayList arrSharedWith;

    private ArrayList arrCodeLst;

    private ArrayList arrSecName;

    private ArrayList arrSecId;

    private ArrayList arrFieldName;

    private ArrayList arrFieldId;

    private ArrayList arrFieldUniId;

    private ArrayList arrFieldType;

    private ArrayList arrRepeatField;

    private ArrayList arrSecFormat;

    private ArrayList arrFormFldId;

    private ArrayList arrFormFldSeqs;

    private ArrayList arrFormFldNums;

    private ArrayList arrFormFldDataTypes;

    private ArrayList arrFormSecOfflines;

    private ArrayList arrFormFldOfflines;

    private String[] saFormLibIds;

    private String[] saCatLibIds;

    private String[] saAccountIds;

    private String[] saSharedWithIds;

    private String sharedWithIds;

    private String creator;

    private String ipAdd;

    private String lastModifiedBy;

    private String recordType;

    private String sharedWith;

    private int cRows;

    private int formLibId;

    public FormLibDao() {
        arrFormLibIds = new ArrayList();
        arrCatLibIds = new ArrayList();
        arrAccountIds = new ArrayList();
        arrSharedWithIds = new ArrayList();
        arrFormTypes = new ArrayList();
        arrFormNames = new ArrayList();
        arrDescriptions = new ArrayList();
        arrStatus = new ArrayList();
        arrSharedWith = new ArrayList();
        arrCodeLst = new ArrayList();
        arrSecName = new ArrayList();
        arrSecId = new ArrayList();
        arrFieldName = new ArrayList();
        arrFieldId = new ArrayList();
        arrFieldUniId = new ArrayList();
        arrFieldType = new ArrayList();
        arrRepeatField = new ArrayList();
        arrSecFormat = new ArrayList();
        arrFormFldId = new ArrayList();
        arrFormFldSeqs = new ArrayList();
        arrFormFldNums = new ArrayList();
        String ipAdd;
        String recordType;
        arrFormFldDataTypes = new ArrayList();
        arrFormSecOfflines = new ArrayList();
        arrFormFldOfflines = new ArrayList();

        int creator;
        int lastModifiedBy;
        int field;
        int cRows;

    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public int getFormLibId() {
        return this.formLibId;
    }

    public ArrayList getArrFormLibIds() {
        return this.arrFormLibIds;
    }

    public void setArrFormLibIds(ArrayList arrFormLibIds) {
        this.arrFormLibIds = arrFormLibIds;
    }

    public ArrayList getArrCatLibIds() {
        return this.arrCatLibIds;
    }

    public void setArrCatLibIds(ArrayList arrCatLibIds) {
        this.arrCatLibIds = arrCatLibIds;
    }

    public ArrayList getFormFldSeqs() {
        return this.arrFormFldSeqs;
    }

    public void setFormFldSeqs(ArrayList arrFormFldSeqs) {
        this.arrFormFldSeqs = arrFormFldSeqs;
    }

    public ArrayList getArrAccountIds() {
        return this.arrAccountIds;
    }

    public void setArrAccountIds(ArrayList arrAccountIds) {
        this.arrAccountIds = arrAccountIds;
    }

    public ArrayList getArrSharedWithIds() {
        return this.arrSharedWithIds;
    }

    public void setArrSharedWithIds(ArrayList arrSharedWithIds) {
        this.arrSharedWithIds = arrSharedWithIds;
    }

    public ArrayList getFormTypes() {
        return this.arrFormTypes;
    }

    public void setFormTypes(ArrayList arrFormTypes) {
        this.arrFormTypes = arrFormTypes;
    }

    public ArrayList getFormNames() {
        return this.arrFormNames;
    }

    public void setFormNames(ArrayList arrFormNames) {
        this.arrFormNames = arrFormNames;
    }

    public ArrayList getDescriptions() {
        return this.arrDescriptions;
    }

    public void setDescriptions(ArrayList arrDescriptions) {
        this.arrDescriptions = arrDescriptions;
    }

    public ArrayList getStatus() {
        return this.arrStatus;
    }

    public void setStatus(ArrayList arrStatus) {
        this.arrStatus = arrStatus;
    }

    public ArrayList getCodeLst() {
        return this.arrCodeLst;
    }

    public void setCodeLst(ArrayList arrCodeLst) {
        this.arrCodeLst = arrCodeLst;
    }

    public ArrayList getSharedWith() {
        return this.arrSharedWith;
    }

    public void setSharedWith(ArrayList arrSharedWith) {
        this.arrSharedWith = arrSharedWith;
    }

    public ArrayList getSecNames() {
        return this.arrSecName;
    }

    public void setSecName(ArrayList arrSecName) {
        this.arrSecName = arrSecName;
    }

    public ArrayList getSecIds() {
        return this.arrSecId;
    }

    public void setSecIds(ArrayList arrSecId) {
        this.arrSecId = arrSecId;
    }

    public ArrayList getFieldNames() {
        return this.arrFieldName;
    }

    public void setFieldNames(ArrayList arrFieldName) {
        this.arrFieldName = arrFieldName;
    }

    public ArrayList getFieldIds() {
        return this.arrFieldId;
    }

    public void setFieldIds(ArrayList arrFieldId) {
        this.arrFieldId = arrFieldId;
    }

    public ArrayList getFieldUniIds() {
        return this.arrFieldUniId;
    }

    public void setFieldUniIds(ArrayList arrFieldUniId) {
        this.arrFieldUniId = arrFieldUniId;
    }

    public ArrayList getFieldTypes() {
        return this.arrFieldType;
    }

    public void setFieldTypes(ArrayList arrFieldType) {
        this.arrFieldType = arrFieldType;
    }

    public ArrayList getRepeatFields() {
        return this.arrRepeatField;
    }

    public void setRepeatFields(ArrayList arrRepeatField) {
        this.arrRepeatField = arrRepeatField;
    }

    public ArrayList getSecFormats() {
        return this.arrSecFormat;
    }

    public void setSecFormats(ArrayList arrSecFormat) {
        this.arrSecFormat = arrSecFormat;
    }

    public ArrayList getFormFldIds() {
        return this.arrFormFldId;
    }

    public void setFormFldIds(ArrayList arrFormFldId) {
        this.arrFormFldId = arrFormFldId;
    }

    public ArrayList getFormFldNums() {
        return this.arrFormFldNums;
    }

    public void setFormFldNums(ArrayList arrFormFldNums) {
        this.arrFormFldNums = arrFormFldNums;
    }

    /* getter setter for string type */

    public void setFormLibIds(String[] saFormLibIds) {
        this.saFormLibIds = saFormLibIds;
    }

    public String[] getCatLibIds() {
        return this.saCatLibIds;
    }

    public void setCatLibIds(String[] saCatLibIds) {
        this.saCatLibIds = saCatLibIds;
    }

    public String[] getAccountIds() {
        return this.saAccountIds;
    }

    public void setAccountIds(String[] saAccountIds) {
        this.saAccountIds = saAccountIds;
    }

    public String getFormLibSharedWith() {
        return this.sharedWith;
    }

    public void setFormLibSharedWith(String sharedWith) {
        this.sharedWith = sharedWith;
    }

    public String getSharedWithIds() {
        return this.sharedWithIds;
    }

    public void setSharedWithIds(String sharedWithIds) {
        this.sharedWithIds = sharedWithIds;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setFldRespIds(Integer formLibId) {
        arrFormLibIds.add(formLibId);
    }

    public void setCatLibIds(Integer catLib) {
        arrCatLibIds.add(catLib);
    }

    public void setAccountIds(Integer accountId) {
        arrAccountIds.add(accountId);
    }

    public void setStatus(Integer status) {
        arrStatus.add(status);
    }

    public void setCodeLst(String codeLst) {
        arrCodeLst.add(codeLst);
    }

    public void setArrFormLibIds(String formLibId) {
        arrFormLibIds.add(formLibId);
    }

    public void setArrCatLibIds(String catLibId) {
        arrCatLibIds.add(catLibId);
    }

    public void setFormNames(String formName) {
        arrFormNames.add(formName);
    }

    public void setFormTypes(String formType) {
        arrFormTypes.add(formType);
    }

    public void setDescriptions(String desc) {
        arrDescriptions.add(desc);
    }

    public void setSharedWith(String sharedWith) {
        arrSharedWith.add(sharedWith);
    }

    public void setSecNames(String secName) {
        // arrSecName.add(secName);
        arrSecName.add(StringUtil.escapeSpecialCharHTML(secName));

    }

    public void setSecIds(Integer secId) {
        arrSecId.add(secId);
    }

    public void setFieldNames(String fieldName) {
        arrFieldName.add(fieldName);
    }

    public void setFieldIds(Integer fieldId) {
        arrFieldId.add(fieldId);
    }

    public void setFormFldSeqs(Integer seq) {
        arrFormFldSeqs.add(seq);
    }

    public void setFieldUniIds(String fieldUniId) {
        arrFieldUniId.add(fieldUniId);
    }

    public void setFieldTypes(String fieldType) {
        arrFieldType.add(fieldType);
    }

    public void setRepeatFields(Integer repeatField) {
        arrRepeatField.add(repeatField);
    }

    public void setSecFormats(String secFormat) {
        arrSecFormat.add(secFormat);
    }

    public void setFormFldIds(Integer formFldId) {
        arrFormFldId.add(formFldId);
    }

    public void setFormFldNum(Integer formFldNum) {
        arrFormFldNums.add(formFldNum);
    }

    public ArrayList getArrFormFldDataTypes() {
        return arrFormFldDataTypes;
    }

    public void setArrFormFldDataTypes(ArrayList arrFormFldDataTypes) {
        this.arrFormFldDataTypes = arrFormFldDataTypes;
    }

    public void setArrFormFldDataTypes(String arrFormFldDataType) {
        this.arrFormFldDataTypes.add(arrFormFldDataType);
    }

    public ArrayList getArrFormSecOfflines() {
        return arrFormSecOfflines;
    }

    public void setArrFormSecOfflines(ArrayList arrFormSecOfflines) {
        this.arrFormSecOfflines = arrFormSecOfflines;
    }

    public void setArrFormSecOfflines(Integer arrFormSecOffline) {
        this.arrFormSecOfflines.add(arrFormSecOffline);
    }

    public ArrayList getArrFormFldOfflines() {
        return arrFormFldOfflines;
    }

    public void setArrFormFldOfflines(ArrayList arrFormFldOfflines) {
        this.arrFormFldOfflines = arrFormFldOfflines;
    }

    public void setArrFormFldOfflines(Integer arrFormFldOffline) {
        this.arrFormFldOfflines.add(arrFormFldOffline);
    }

    // ///////////////////////////////////////////////////////////////
    public int insertFormSharedWith(int formId, String sharedWithIds,
            String type, String user, String ipAdd, String mode) {
        Rlog.debug("formlib", "form sharedwith inside insertFormSharedWith");
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog
                    .debug("formlib",
                            "In insertFormSharedWith calling SP insertFormSharedWith() ");
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_FORMSHAREWITH(?,?,?,?,?,?,?)}");

            cstmt.setInt(1, formId);
            cstmt.setString(2, sharedWithIds);
            cstmt.setString(3, type);
            cstmt.setString(4, user);
            cstmt.setString(5, ipAdd);
            cstmt.setString(6, mode);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(7);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "EXCEPTION in insertFormSharedWith, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void getFormDetails(int accountId, int userId, String formName,
            int formType) {

        Rlog.debug("formlib", "Inside getFormDetails in FormLibDao formType "
                + formType);
        int ret = 1;
        int rows = 0;
        PreparedStatement stmt = null;
        Connection conn = null;
        String sql = "";
        String str1, str2, str3, str4, str5, str6;
        String mainsql = "";
        try {

            conn = getConnection();

            str1 = " select pk_formlib , catlib_name,form_name, "
                    + " fk_catlib, NVL(form_desc,'-') form_desc,form_status,cd.codelst_desc form_statusdesc, form_sharedwith  "
                    + " from  er_formlib fl, er_catlib cl, er_codelst cd "
                    + " where cd.pk_codelst=fl.form_status "
                    + " and fl.fk_account = ? " + " and  fl.form_linkto = 'L' "
                    + " and  fl.record_type <> 'D' "
                    + " and fl.fk_account=cl.fk_account "
                    + " and fl.fk_catlib = cl.pk_catlib  "
                    + " and cl.catlib_type='T' " + " and exists "
                    + " (select osh.fk_object " + " from er_objectshare osh "
                    + " where " + " fl.pk_formlib = osh.fk_object "
                    + " and osh.objectshare_type ='U' "
                    + " and osh.object_number = 1  "
                    + " and osh.fk_objectshare_id = ?)";

            if (formName.equals("")) {
                str2 = " ";
            }

            else {
                str2 = " and lower(fl.form_name) like lower('%" + formName
                        + "%') ";
            }

            // -2 is used when we have to show the form names in copy form page
            if (formType == 0 || formType == -1 || formType == -2) {
                str3 = " ";
            }

            else {
                str3 = " and fl.fk_catlib=" + formType;
            }

            if (formType == 0 || formType == -1) {

                str4 = "  UNION  (   SELECT  NULL, CATLIB_NAME  ,NULL, "
                        + "  PK_CATLIB 	, NULL , NULL, NULL, NULL  "
                        + "  FROM ER_CATLIB "
                        + "  WHERE FK_ACCOUNT =  "
                        + accountId
                        + " "
                        + "  AND CATLIB_TYPE='T' "
                        + "  AND RECORD_TYPE <> 'D' "
                        + "  AND ER_CATLIB.PK_CATLIB NOT IN (SELECT  ER_FORMLIB.FK_CATLIB "
                        + "  FROM ER_FORMLIB WHERE FK_ACCOUNT = " + accountId
                        + " )) ";

            } else {
                str4 = " ";
            }

            str5 = " ORDER BY (fk_catlib)";

            Rlog.debug("formlib", "stmt" + str1 + str2 + str3 + str4 + str5);

            ResultSet rs = null;
            try {
                mainsql = str1 + str2 + str3 + str4 + str5;
                stmt = conn.prepareStatement(mainsql);
                stmt.setInt(1, accountId);
                stmt.setInt(2, userId);
                stmt.execute();

                rs = stmt.getResultSet();

            } catch (Exception ex) {
                Rlog.fatal("formlib", "FormLib. getFormDetails exception" + ex);
            }

            if (rs != null) {

                while (rs.next()) {
                    setArrFormLibIds(rs.getString("PK_FORMLIB"));
                    setFormTypes(rs.getString("CATLIB_NAME"));
                    setFormNames(rs.getString("FORM_NAME"));
                    setDescriptions(rs.getString("FORM_DESC"));
                    setStatus(new Integer(rs.getInt("FORM_STATUS")));
                    setCodeLst(rs.getString("form_statusdesc"));
                    setSharedWith(rs.getString("FORM_SHAREDWITH"));
                    setArrCatLibIds(rs.getString("FK_CATLIB"));
                    rows++;
                }
                setCRows(rows);
            }
        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "FormlibDao.getFormDetails EXCEPTION IN FETCHING FROM table"
                            + ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    //Added by IA bug 2614 multiple organizations
    
    public void getFormDetails(int accountId, int userId, String formName,
            int formType, String orgId) {

        Rlog.debug("formlib", "Inside getFormDetails in FormLibDao formType "
                + formType);
        int ret = 1;
        int rows = 0;
        PreparedStatement stmt = null;
        Connection conn = null;
        String sql = "";
        String str1, str2, str3, str4, str5, str6;
        String mainsql = "";
        try {

            conn = getConnection();

            str1 = " select pk_formlib , catlib_name,form_name, "
                    + " fk_catlib, NVL(form_desc,'-') form_desc,form_status,cd.codelst_desc form_statusdesc, form_sharedwith  "
                    + " from  er_formlib fl, er_catlib cl, er_codelst cd "
                    + " where cd.pk_codelst=fl.form_status "
                    + " and fl.fk_account = ? " + " and  fl.form_linkto = 'L' "
                    + " and  fl.record_type <> 'D' "
                    + " and fl.fk_account=cl.fk_account "
                    + " and fl.fk_catlib = cl.pk_catlib  "
                    + " and cl.catlib_type='T' " + " and exists "
                    + " (select osh.fk_object " + " from er_objectshare osh "
                    + " where " + " fl.pk_formlib = osh.fk_object "
                    + " and osh.object_number = 1  and (( osh.objectshare_type ='U' "
                    + " and osh.fk_objectshare_id = ?) or ( osh.objectshare_type ='O' "
                    + " and osh.fk_objectshare_id in( " + orgId + "))) )";
                    		

            if (formName.equals("")) {
                str2 = " ";
            }

            else {
                str2 = " and lower(fl.form_name) like lower('%" + formName
                        + "%') ";
            }

            // -2 is used when we have to show the form names in copy form page
            if (formType == 0 || formType == -1 || formType == -2) {
                str3 = " ";
            }

            else {
                str3 = " and fl.fk_catlib=" + formType;
            }

            if (formType == 0 || formType == -1) {

                str4 = "  UNION  (   SELECT  NULL, CATLIB_NAME  ,NULL, "
                        + "  PK_CATLIB 	, NULL , NULL, NULL, NULL  "
                        + "  FROM ER_CATLIB "
                        + "  WHERE FK_ACCOUNT =  "
                        + accountId
                        + " "
                        + "  AND CATLIB_TYPE='T' "
                        + "  AND RECORD_TYPE <> 'D' "
                        + "  AND ER_CATLIB.PK_CATLIB NOT IN (SELECT  ER_FORMLIB.FK_CATLIB "
                        + "  FROM ER_FORMLIB WHERE FK_ACCOUNT = " + accountId
                        + " )) ";

            } else {
                str4 = " ";
            }

            str5 = " ORDER BY (fk_catlib)";

            Rlog.debug("formlib", "stmt" + str1 + str2 + str3 + str4 + str5);

            ResultSet rs = null;
            try {
                mainsql = str1 + str2 + str3 + str4 + str5;
                stmt = conn.prepareStatement(mainsql);
                stmt.setInt(1, accountId);
                stmt.setInt(2, userId);
                stmt.execute();

                rs = stmt.getResultSet();

            } catch (Exception ex) {
                Rlog.fatal("formlib", "FormLib. getFormDetails exception" + ex);
            }

            if (rs != null) {

                while (rs.next()) {
                    setArrFormLibIds(rs.getString("PK_FORMLIB"));
                    setFormTypes(rs.getString("CATLIB_NAME"));
                    setFormNames(rs.getString("FORM_NAME"));
                    setDescriptions(rs.getString("FORM_DESC"));
                    setStatus(new Integer(rs.getInt("FORM_STATUS")));
                    setCodeLst(rs.getString("form_statusdesc"));
                    setSharedWith(rs.getString("FORM_SHAREDWITH"));
                    setArrCatLibIds(rs.getString("FK_CATLIB"));
                    rows++;
                }
                setCRows(rows);
            }
        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "FormlibDao.getFormDetails EXCEPTION IN FETCHING FROM table"
                            + ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

   //end added
    
    
    
    
    /**
     * This method calls a stored procedure to copy a field passed to this
     * method as a <br>
     * parameter
     * 
     * @param pkForm
     * @param pkSection
     * @param pkField
     * @param user
     * @param ipadd
     * @return int
     */
    public int[] insertCopyFieldToForm(int pkForm, int pkSection, int pkField,
            String user, String ipAdd) {
    	int[] retArray = new int[2];
        int retFieldId = 0;
        int retFormFldId = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("formlib",
                    "In insertCopyFieldToForm insertCopyFieldToForm() ");
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_COPYFIELD_TOFORM(?,?,?,?,?,?,?)}");
            cstmt.setInt(1, pkForm);
            cstmt.setInt(2, pkSection);
            cstmt.setInt(3, pkField);
            cstmt.setString(4, user);
            cstmt.setString(5, ipAdd);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);

            cstmt.execute();
            retFieldId = cstmt.getInt(6);
            retFormFldId = cstmt.getInt(7);
            retArray[0] = retFieldId;
            retArray[1] = retFormFldId;
            return retArray;

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "EXCEPTION in insertCopyFieldToForm, excecuting Stored Procedure "
                            + e);
            retArray[0] = -1;
            return retArray;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public void getSectionFieldInfo(int formId) {

        Rlog.debug("formlib", "inside getSectionFieldInfo in FormLibDao");
        int ret = 1;
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            Rlog.debug("formlib", "In getSectionFieldInfo () ");
            sql = " select pk_formfld, formsec_name , pk_formsec, fld_name, pk_field, "
                    + " fld_uniqueid, fld_type, formsec_repno,formsec_fmt, "
                    + " formsec_seq , formfld_seq ,"
                    + "  ( select count(pk_field) from erv_formflds where pk_formsec = i.pk_formsec  "
                    + "   and pk_formlib = ?  ) numformfields , fld_datatype, formsec_offline, formfld_offline  "
                    + " from erv_formflds i  "
                    + " where pk_formlib= ? "
                    + " union select null, formsec_name, pk_formsec, null , "
                    + " null, null, null, formsec_repno, formsec_fmt, "
                    + " formsec_seq , null , null , null, formsec_offline, null"
                    + " from er_formsec where nvl(record_type, 'Z') <> 'D' and fk_formlib= ? "
                    + " and pk_formsec not in (select pk_formsec "
                    + " from erv_formflds where pk_formlib= ?) "
                    + " order by  formsec_seq , pk_formsec, formfld_seq ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, formId);
            pstmt.setInt(2, formId);
            pstmt.setInt(3, formId);
            pstmt.setInt(4, formId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setFormFldIds(new Integer(rs.getInt("PK_FORMFLD")));
                setSecNames(rs.getString("FORMSEC_NAME"));
                setSecIds(new Integer(rs.getInt("PK_FORMSEC")));
                setFieldNames(rs.getString("FLD_NAME"));
                setFieldIds(new Integer(rs.getInt("PK_FIELD")));
                setFieldUniIds(rs.getString("FLD_UNIQUEID"));
                setFieldTypes(rs.getString("FLD_TYPE"));
                setRepeatFields(new Integer(rs.getInt("FORMSEC_REPNO")));
                setSecFormats(rs.getString("FORMSEC_FMT"));
                setFormFldSeqs(new Integer(rs.getInt("FORMFLD_SEQ")));
                setFormFldNum(new Integer(rs.getInt("numformfields")));
                setArrFormFldDataTypes(rs.getString("FLD_DATATYPE"));
                setArrFormSecOfflines(new Integer(rs.getInt("FORMSEC_OFFLINE")));
                setArrFormFldOfflines(new Integer(rs.getInt("FORMFLD_OFFLINE")));

                rows++;
            }
            setCRows(rows);
            Rlog.debug("formlib", "number of rows" + rows);

        } catch (SQLException ex) {
            Rlog.fatal("formlib",
                    "getSectionFieldInfo.getSectionFieldInfo EXCEPTION IN FETCHING FROM table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /*
     * Sonia Abrol Date : 04/06/05 Generate Form Fields Action Script for the
     * copied form
     */

    /*
     * Modified by Sonia Abrol, 09/06/05, check the form preserver format
     * setting, if form should not be refreshed, then do not generate interfield
     * action javascript
     */
    public int copyFormsFromLib(String[] idArray, String formType,
            String formName, String creator, String ipAdd) {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;
        String formDesc = null;
        String flag = "L";
        int studyId = -1;
        FieldActionDao fldActionDao = new FieldActionDao();

        int formRefreshValue = 1;
        int saveFormatting = 0;
        ArrayList arValues = new ArrayList();

        Rlog.fatal("formlib", "Inside the Copy Form From Lib in formLibDao ");

        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIdArray = new ARRAY(inIds, conn, idArray);

            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_COPY_MULTIPLE_FORMS(?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setArray(1, inIdArray);
            cstmt.setString(2, formDesc);
            cstmt.setInt(3, Integer.parseInt(formType));
            cstmt.setString(4, flag);
            cstmt.setInt(5, studyId);
            cstmt.setInt(6, Integer.parseInt(creator));
            cstmt.setString(7, formName);
            cstmt.setString(8, null); // display type
            cstmt.setString(9, ipAdd);
            cstmt.registerOutParameter(10, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(10);

            /*
             * Generate Form Fields Action Script for the copied form
             */

            if (idArray.length == 1) {

                saveFormatting = getFormSaveFormat(Integer.parseInt(idArray[0]));

                if (saveFormatting == 1) {
                    formRefreshValue = 0;
                } else {
                    formRefreshValue = 1;
                }

            }

            if (formRefreshValue == 1) {
                fldActionDao.generateFieldActionsScript(String.valueOf(ret));
            }

            Rlog.debug("formlib", "Inside the copyForms, return value" + ret);
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog.fatal("formlib",
                    "EXCEPTION in copyMultipleFields, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * This method calls a stored procedure
     * pkg_user.SP_CREATE_FORMSHAREWITH_DATA() to create er_objectshare data for
     * all the forms who have the user primary organization in sharewith to
     * create er_objectshare data for all the forms who have the new user
     * account
     * 
     * @param userId
     * @param accountId
     * @param siteId
     * @param creator
     * @param ipAdd
     * @return 0 if successful, -1 if error
     */

    public int createFormShareWithData(int userId, int accountId, int siteId,
            int creator, String ipAdd) {
        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = 0;

        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_CREATE_FORMSHAREWITH_DATA(?,?,?,?,?,?)}");
            cstmt.setInt(1, userId);
            cstmt.setInt(2, accountId);
            cstmt.setInt(3, siteId);
            cstmt.setInt(4, creator);
            cstmt.setString(5, ipAdd);

            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(6);

            return ret;

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "formlib",
                            "EXCEPTION in createFormShareWithData, excecuting Stored Procedure SP_CREATE_FORMSHAREWITH_DATA"
                                    + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public int updateInsertForStatusChange(int formLibId, int newStatus,
            int creator, String ipAdd)

    {
        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = 0;

        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_UPDT_INSRT_FORSTATUSCHNG(?,?,?,?,?)}");
            cstmt.setInt(1, formLibId);
            cstmt.setInt(2, newStatus);
            cstmt.setInt(3, creator);
            cstmt.setString(4, ipAdd);

            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(5);

            return ret;

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "formlib",
                            "EXCEPTION in updateInsertForStatusChange, excecuting Stored Procedure SP_UPDT_INSRT_FORSTATUSCHNG"
                                    + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public int insertFormDefaultData(int formId) {
        Rlog.debug("formlib", "inside insertFormDefaultData");

        CallableStatement cstmt = null;
        Connection conn = null;
        int formFldId = 0;

        try {
            conn = getConnection();
            Rlog
                    .debug("formlib",
                            "In insertFormDefaultData calling SP insertFormDefaultData() ");
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_FORMDEFAULTDATA(?,?,?)}");

            cstmt.setInt(1, formId);
            cstmt.registerOutParameter(2, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
            cstmt.execute();
            formFldId = cstmt.getInt(3);

        } catch (Exception e) {
            Rlog.fatal("formlib",
                    "EXCEPTION in insertFormDefaultData, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        return formFldId;
    }

    // //////////////////////

    public int getFormSaveFormat(int form) {
        int formSaveFormat = 0;
        int rows = 0;
        PreparedStatement stmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            sql = " select form_saveformat from er_formlib where pk_formlib = ? ";

            ResultSet rs = null;
            try {

                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, form);
                stmt.execute();

                rs = stmt.getResultSet();

            } catch (Exception ex) {
                Rlog.fatal("formlib", "FormLib.getFormSaveFormat exception"
                        + ex);
            }

            if (rs != null) {

                while (rs.next()) {
                    formSaveFormat = rs.getInt("form_saveformat");

                }
            }
            return formSaveFormat;
        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "FormlibDao.getFormDetails EXCEPTION IN FETCHING FROM table"
                            + ex);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            return formSaveFormat;
        }
    }
    
        public String getFormXml(int form) {
            int rows = 0;
            PreparedStatement stmt = null;
            Connection conn = null;
            String sql = "";
            Clob clob_xml=null;
            try {

                conn = getConnection();
                sql = " select a.form_xml.getClobVal() as form_xml from er_formlib a where pk_formlib = ? ";

                ResultSet rs = null;
                try {

                    stmt = conn.prepareStatement(sql);
                    stmt.setInt(1, form);
                    stmt.execute();

                    rs = stmt.getResultSet();

                } catch (Exception ex) {
                    Rlog.fatal("formlib", "FormLib.getFormxml exception"
                            + ex);
                }

                if (rs != null) {

                    while (rs.next()) {
                        clob_xml=rs.getClob("form_xml");

                    }
                }
                return clob_xml.getSubString(new Integer(1).longValue(),new Long(clob_xml.length()).intValue());
            } catch (Exception ex) {
                Rlog.fatal("formlib",
                        "FormlibDao.getFormxml EXCEPTION IN FETCHING FROM table"
                                + ex);
                ex.printStackTrace();
                return "";
            } finally {
                try {
                    if (stmt != null)
                        stmt.close();
                } catch (Exception e) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (Exception e) {
                }
                
            }

        }


    
        public String getFormXsl(int form) {
            int rows = 0;
            PreparedStatement stmt = null;
            Connection conn = null;
            String sql = "";
            Clob clob_xml=null;
            try {

                conn = getConnection();
                sql = " select form_xsl from er_formlib  where pk_formlib = ? ";

                ResultSet rs = null;
                try {

                    stmt = conn.prepareStatement(sql);
                    stmt.setInt(1, form);
                    stmt.execute();

                    rs = stmt.getResultSet();

                } catch (Exception ex) {
                    Rlog.fatal("formlib", "FormLib.getFormXSL exception"
                            + ex);
                }

                if (rs != null) {

                    while (rs.next()) {
                        clob_xml=rs.getClob("form_xsl");

                    }
                }
                return clob_xml.getSubString(new Integer(1).longValue(),new Long(clob_xml.length()).intValue());
            } catch (Exception ex) {
                Rlog.fatal("formlib",
                        "FormlibDao.getFormxsl EXCEPTION IN FETCHING FROM table"
                                + ex);
                ex.printStackTrace();
                return "";
            } finally {
                try {
                    if (stmt != null)
                        stmt.close();
                } catch (Exception e) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (Exception e) {
                }
                
            }

        }
        
        private static String uniqueLibraryFormNameSql = " SELECT COUNT(*) FROM ER_FORMLIB "+
        		" WHERE LOWER(trim(form_name)) = LOWER(trim(?)) "+
        		" AND fk_account = ? AND form_linkto = 'L' "+
        		" AND NVL(ER_FORMLIB.record_type,'Z') <> 'D' ";

        public int uniqueLibraryFormName(int accountId, String formName) {
            int ret = -1;
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = getConnection();
                pstmt = conn.prepareStatement(uniqueLibraryFormNameSql);
                pstmt.setString(1, formName);
                pstmt.setInt(2, accountId);
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                	ret = rs.getInt(1);
                }
            }  catch (SQLException ex) {
                Rlog.fatal("formlib", "In uniqueLibraryFormName(): "+ex);
            } finally {
                try { if (pstmt != null) { pstmt.close(); }
                } catch (Exception e) {}
                try { if (conn != null) { conn.close(); }
                } catch (Exception e) {}
            }
            return ret;
        }

	
		//Apr17,06-Bug 2563:Added by Manimaran to associate same form names to different studies while updating. 
		public int  uniqueLinkedFormName(int accountid,int iformLibId,String formlibname,String formlinkto, int studyId){
        int ret = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";

        try {

            conn = getConnection();
            sql="select pk_lf from er_formlib a,er_linkedforms b where a.fk_account=? and a.pk_formlib=b.fk_formlib  and a.form_name=? and a.form_linkto=? and b.record_type != 'D' and b.fk_study=? and a.pk_formlib!=?";
	    
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, accountid);
            pstmt.setString(2, formlibname);
            pstmt.setString(3, formlinkto);
			pstmt.setInt(4, studyId);
			pstmt.setInt(5,iformLibId);
            
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	ret++;
            }
    		if (ret>0)
			{
				ret=-3;
			}
			return ret;
			
        } catch (SQLException ex) {
            Rlog.fatal("formlib",
                    "FormlibDao.uniqueLinkedFormName EXCEPTION IN FETCHING FROM table"
                            + ex);
            return -2;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

	}
		
		
		//Added by Manimaran for November Enhancement F6
		public String getFormLibVersionNumber(int formlibverPK) {
			String formLibVer = "";
	        PreparedStatement stmt = null;
	        Connection conn = null;
	        String sql = "";
	        try {

	            conn = getConnection();
	            sql = " select formlibver_number from er_formlibver where pk_formlibver = ? ";
	            
	            //JM: 122206
	         //   sql = " select max(FORMLIBVER_NUMBER) formlibver_number from ER_FORMLIBVER where FK_FORMLIB = ? ";

	            ResultSet rs = null;
	            try {

	                stmt = conn.prepareStatement(sql);
	                stmt.setInt(1, formlibverPK);
	                stmt.execute();

	                rs = stmt.getResultSet();

	            } catch (Exception ex) {
	                Rlog.fatal("formlib", "FormLib.getFormLibVersionNumber exception"
	                        + ex);
	            }

	            if (rs != null) {

	                while (rs.next()) {
	                	formLibVer = rs.getString("formlibver_number");

	                }
	            }
	            return formLibVer;
	        } catch (Exception ex) {
	            Rlog.fatal("formlib",
	                    "FormlibDao.getFormLibVersionNumber EXCEPTION IN FETCHING FROM table"
	                            + ex);
	        } finally {
	            try {
	                if (stmt != null)
	                    stmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	            return formLibVer;
	        }
	    }
		
  //get latest form version
//		Added by Sonia Abrol 11/22/06
		public String getLatestFormLibVersionPK(int form) {
			String formLibVerPK = "";
	        PreparedStatement stmt = null;
	        Connection conn = null;
	        String sql = "";
	        try {

	            conn = getConnection();
	            sql = " select max(pk_formlibver) from er_formlibver where fk_formlib = ? ";

	            ResultSet rs = null;
	            try {

	                stmt = conn.prepareStatement(sql);
	                stmt.setInt(1, form);
	                stmt.execute();

	                rs = stmt.getResultSet();

	            } catch (Exception ex) {
	                Rlog.fatal("formlib", "Exception in FormLib.getLatestFormLibVersionPK "
	                        + ex);
	            }

	            if (rs != null) {

	                while (rs.next()) {
	                	formLibVerPK = rs.getString(1);

	                }
	            }
	            return formLibVerPK;
	        } catch (Exception ex) {
	            Rlog.fatal("formlib",
	                    "Exception in FormLib.getLatestFormLibVersionPK EXCEPTION IN FETCHING FROM table"
	                            + ex);
	        } finally {
	            try {
	                if (stmt != null)
	                    stmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	            return formLibVerPK;
	        }
	    }


		///////
		
		public boolean shouldGenerateActiovationScript(int form) {
	        int count = 0;
	        
	        PreparedStatement stmt = null;
	        Connection conn = null;
	        String sql = "";
	        boolean flag = false;
	        
	        try {

	            conn = getConnection();
	            sql = "select count(*) from er_formlib where pk_formlib = ? and (form_custom_js is null or dbms_lob.getlength(form_activation_js) = 0 ) and pk_formlib in (select fk_form from ER_FLDACTION  where fk_form = pk_formlib)";

	            ResultSet rs = null;
	            try {

	                stmt = conn.prepareStatement(sql);
	                stmt.setInt(1, form);
	                stmt.execute();

	                rs = stmt.getResultSet();

	            } catch (Exception ex) {
	                Rlog.fatal("formlib", "FormLib.shouldGenerateActiovationScript exception"
	                        + ex);
	            }

	            if (rs != null) {

	                while (rs.next()) {
	                    count = rs.getInt(1);

	                }
	                
	                if (count == 1)
	                {
	                	
	                	flag = true;
	                }
	                
	            }
	            return flag;
	        } catch (Exception ex) {
	            Rlog.fatal("formlib",
	                    "FormlibDao.shouldGenerateActiovationScript EXCEPTION IN FETCHING FROM table"
	                            + ex);
	            return flag; 
	        } finally {
	            try {
	                if (stmt != null)
	                    stmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	            
	        }
	    }

		
		
		///////////////
//		Added by Sonia Abrol 11/22/06
		public int getPatFilledFormPKForCreator(int form,int patientPK,int creator) {
			int filledformid = 0;
	        PreparedStatement stmt = null;
	        Connection conn = null;
	        String sql = "";
	        try {

	            conn = getConnection();
	            
	            //rownum less than 2 will make sure it returns 1 record
	            
	            sql = " select pk_patforms from er_patforms where fk_formlib = ? and fk_per = ? and creator = ? and nvl(record_type,'N') <> 'D' and rownum < 2 order by created_on ";

	            ResultSet rs = null;
	            try {

	                stmt = conn.prepareStatement(sql);
	                
	                stmt.setInt(1, form);
	                stmt.setInt(2, patientPK);
	                stmt.setInt(3, creator);
	                
	                stmt.execute();

	                rs = stmt.getResultSet();

	            } catch (Exception ex) {
	                Rlog.fatal("formlib", "Exception in FormLib.getPatFilledFormPKForCreator "
	                        + ex);
	            }

	            if (rs != null) {

	                while (rs.next()) {
	                	filledformid = rs.getInt(1);

	                }
	            }
	            return filledformid;
	        } catch (Exception ex) {
	            Rlog.fatal("formlib",
	                    "Exception in FormLib.getPatFilledFormPKForCreator EXCEPTION IN FETCHING FROM table"
	                            + ex);
	            return filledformid;
	        } finally {
	            try {
	                if (stmt != null)
	                    stmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	             
	        }
	    }

		
		public String validateFormField(int formId,String systemID,int studyID,int patientID,int accountID,String datavalue) {
		
			PreparedStatement stmt = null;
		    ResultSet rs = null;
			Connection conn = null;
	        String mainsql="";
	        String formvalidate = "";

	        try{
				
				  conn = getConnection();			
				  
				  mainsql = "select Pkg_Filledform.fn_formfld_validate_partial(?,?,?,?,?,?) as returnval from dual";
	              
				  stmt = conn.prepareStatement(mainsql);			  
				  
				  stmt.setInt(1,formId);
	              stmt.setString(2,systemID);
				  stmt.setInt(3,studyID);
				  stmt.setInt(4,patientID);
				  stmt.setInt(5,accountID);
				  stmt.setString(6,datavalue);
				  
				  stmt.execute();
				  rs = stmt.getResultSet();
				  
				  while(rs.next())
				  {				  
					  formvalidate =(String )rs.getObject(1);  
					 				  
				  }
		        	  			
			}
			catch(Exception e){
				e.printStackTrace();
				Rlog.fatal("formlib",
	                    "Exception in FormLib.validateFormField exception in fetching from function"+e);
				e.printStackTrace();
				
			}
			finally {
			    try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
                try { if (conn != null) { conn.close(); } } catch(Exception e) {}
			}
			return formvalidate;
		
		}

		
		public int validateFormField(int formID,int responseID,String systemID,int studyID,int patientID,int accountID,String datavalue,String operator) {
			
			PreparedStatement stmt = null;
		    ResultSet rs = null;
			Connection conn = null;
	        String mainsql="";
	        int formvalidate = 0;

	        try{
				
				  conn = getConnection();			
				  
				  mainsql = "select Pkg_Filledform.fn_formfld_validate(?,?,?,?,?,?,?,?) as returnVal from dual";
	              
				  stmt = conn.prepareStatement(mainsql);			  
				  
	              stmt.setInt(1,formID);
				  stmt.setInt(2,responseID);
				  stmt.setString(3,systemID);
				  stmt.setInt(4,studyID);
				  stmt.setInt(5,patientID);
				  stmt.setInt(6,accountID);
				  stmt.setString(7,datavalue);
				  stmt.setString(8,operator);
				  
				  stmt.execute();
				  rs = stmt.getResultSet();
				  
				  while(rs.next())
				  {				  
					  formvalidate =rs.getInt("returnVal");  
					 				  
				  }
		        	  			
			}
			catch(Exception e){
				e.printStackTrace();
				Rlog.fatal("formlib",
	                    "Exception in FormLib.validateFormField exception in fetching from function"+e);
				e.printStackTrace();
				
			}
            finally {
                try { if (stmt != null) { stmt.close(); } } catch(Exception e) {}
                try { if (conn != null) { conn.close(); } } catch(Exception e) {}
            }
			return formvalidate;
		
		}

		/**
		 * get Patfilled Form PK for Hashtable of parameters. if hastable is empty, no extra filter will be applied 
		 * htparam Hashtable 
		 * supported key: "status"
		 * */
		public int getPatFilledFormPKForCreatorForParam(int form,int patientPK,int creator,Hashtable htParam) {
			int filledformid = 0;
	        PreparedStatement stmt = null;
	        Connection conn = null;
	        String sql = "";
	        String status = "";
	        
	        try {

	            conn = getConnection();
	            
	            //rownum less than 2 will make sure it returns 1 record
	            
	            sql = " select pk_patforms from er_patforms where fk_formlib = ? and fk_per = ? and creator = ? and nvl(record_type,'N') <> 'D' ";
	            
	            if (htParam.containsKey("status"))
	            {
	            	status = (String) htParam.get("status");
	            	
	            	if (! StringUtil.isEmpty(status))
	            	{
	            		sql = sql + " and form_completed = ? ";
	            		
	            	}
	            }
	            
	            
	            
	            sql = sql + " and rownum < 2 order by created_on ";

	            ResultSet rs = null;
	            try {

	                stmt = conn.prepareStatement(sql);
	                
	                stmt.setInt(1, form);
	                stmt.setInt(2, patientPK);
	                stmt.setInt(3, creator);
	                
	                if (! StringUtil.isEmpty(status))
	            	{
		                stmt.setInt(4, StringUtil.stringToNum(status));
	            	}
	                
	                stmt.execute();

	                rs = stmt.getResultSet();

	            } catch (Exception ex) {
	                Rlog.fatal("formlib", "Exception in FormLib.getPatFilledFormPKForCreator "
	                        + ex);
	            }

	            if (rs != null) {

	                while (rs.next()) {
	                	filledformid = rs.getInt(1);

	                }
	            }
	            return filledformid;
	        } catch (Exception ex) {
	            Rlog.fatal("formlib",
	                    "Exception in FormLib.getPatFilledFormPKForCreator EXCEPTION IN FETCHING FROM table"
	                            + ex);
	            return filledformid;
	        } finally {
	            try {
	                if (stmt != null)
	                    stmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	             
	        }
	    }

}
