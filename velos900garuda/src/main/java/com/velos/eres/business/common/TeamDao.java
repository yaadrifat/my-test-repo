/*
 * Classname			TeamDao
 * 
 * Version information 	1.0
 *
 * Date					04/17/2001
 * 
 * Copyright notice		Velos, Inc.
 *r
 * Author 				Sajal
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * SiteDao for getting Site records based on some retrieval criteria
 * 
 * @author Sajal
 * @version : 1.0 03/26/2001
 */

public class TeamDao extends CommonDAO implements java.io.Serializable {

    /*
     * CLASS METHODS
     * 
     * public ArrayList getTeamIds() public void setTeamIds(ArrayList teamIds)
     * public ArrayList getTeamRoles() public void setTeamRoles(ArrayList
     * teamRoles) public ArrayList getTeamUserLastNames() public void
     * setTeamUserLastNames(ArrayList teamUserLastNames) public ArrayList
     * getTeamUserFirstNames() public void setTeamUserFirstNames(ArrayList
     * teamUserFirstNames) public ArrayList getTeamRights() public void
     * setTeamRights(ArrayList teamRights) public int getCRows() public void
     * setCRows(int cRows) public void setTeamIds(Integer teamId) public void
     * setTeamRoles(String teamRole) public void setTeamUserNames(String
     * teamUserName) public void setTeamRights(String teamRight) public void
     * getTeamValues(int teamId)
     * 
     * END OF CLASS METHODS
     */

    /*
     * Array list to save the Team Ids (primary key)
     */
    private ArrayList teamIds;

    /*
     * Array list to save the Team Roles
     */
    private ArrayList teamRoles;

    /*
     * Array list to save the Team Role Ids
     */
    private ArrayList teamRoleIds;

    /*
     * Array list to save the Team User Last Names
     */
    private ArrayList teamUserLastNames;

    /*
     * Array list to save the Team User First Names
     */
    private ArrayList teamUserFirstNames;

    /*
     * Array list to save the Team User First Names
     */
    private ArrayList usrSiteNames;

    /*
     * Array list to save the Team Rights
     */
    private ArrayList teamRights;

    /*
     * Array list to store whether the user is internal or external 1 is stored
     * for internal users and 2 for external
     */
    private ArrayList inexUsers;

    /*
     * Array list to store user type
     */
    private ArrayList teamUserTypes;

    /*
     * This stores the number of elements in the arraylists
     */
    private int cRows;

    /* for user id */

    private ArrayList userIds;

    /* for user site ids */

    private ArrayList siteIds;
    
    /* for team member status */
    
    private ArrayList teamStatus;

    /* for team status id */
    
    private ArrayList teamStatusIds;
    
    /* for User Address */
    
    private ArrayList userAddresses;
    
    /* for User phone */
    
    private ArrayList userPhones;
    
    /* for User email */
    
    private ArrayList userEmails;
    
    /*for study statuses*/
    private ArrayList studyStatuses;//virendra
    
    
   
	private ArrayList  userTypes;
    /**
     * Defines a teamDao object with empty array lists
     */
    public TeamDao() {
        teamIds = new ArrayList();
        teamRoles = new ArrayList();
        teamRoleIds = new ArrayList();
        teamUserLastNames = new ArrayList();
        teamUserFirstNames = new ArrayList();
        usrSiteNames = new ArrayList();
        teamRights = new ArrayList();
        inexUsers = new ArrayList();
        userIds = new ArrayList();
        teamUserTypes = new ArrayList();
        siteIds = new ArrayList();
        teamStatus=new ArrayList();
        teamStatusIds=new ArrayList();
        //		Added by IA 11.03.2006 
        userAddresses =new ArrayList();
        userPhones=new ArrayList();
        userEmails=new ArrayList();
        
        studyStatuses=new ArrayList();//virendra
        userTypes=new ArrayList();
        //end added
        
    }

    // Getter and Setter methods

    /**
     * Returns an Arraylist containing Integer objects with Team Ids
     * 
     * @return an Arraylist containing Integer objects with Team Ids
     */
    public ArrayList getTeamIds() {
        return this.teamIds;
    }

    /**
     * Sets the Arraylist containing the Team Ids with the ArrayList passed as
     * the parameter
     * 
     * @param teamIds
     *            An ArrayList with Integer objects having the Team Ids
     */
    public void setTeamIds(ArrayList teamIds) {
        this.teamIds = teamIds;
    }

    /**
     * Returns an Arraylist containing String objects with Team Roles
     * 
     * @return an Arraylist containing the String objects with Team Roles
     */
    public ArrayList getTeamRoles() {
        return this.teamRoles;
    }

    /**
     * Sets the Arraylist containing the Team Roles with the ArrayList passed as
     * the parameter
     * 
     * @param teamRoles
     *            An ArrayList with String objects having the Team Roles
     */
    public void setTeamRoles(ArrayList teamRoles) {
        this.teamRoles = teamRoles;
    }

    /**
     * Returns an Arraylist containing String objects with Team Role Ids
     * 
     * @return an Arraylist containing the String objects with Team Role Ids
     */
    public ArrayList getTeamRoleIds() {
        return this.teamRoleIds;
    }

    /**
     * Sets the Arraylist containing the Team Role Ids with the ArrayList passed
     * as the parameter
     * 
     * @param teamRoleIds
     *            An ArrayList with String objects having the Team Role Ids
     */
    public void setTeamRoleIds(ArrayList teamRoleIds) {
        this.teamRoleIds = teamRoleIds;
    }

    /**
     * Returns an Arraylist containing the String objects with Team User Last
     * Names
     * 
     * @return an Arraylist containing the String objects with Team User Last
     *         Names
     */
    public ArrayList getTeamUserLastNames() {
        return this.teamUserLastNames;
    }

    /**
     * Sets the Arraylist containing the Team User Last Names with the ArrayList
     * passed as the parameter
     * 
     * @param teamUserLastNames
     *            An ArrayList with String objects having the Team User Last
     *            Names
     */
    public void setTeamUserLastNames(ArrayList teamUserLastNames) {
        this.teamUserLastNames = teamUserLastNames;
    }

    /**
     * Returns an Arraylist containing the String objects with Team User First
     * Names
     * 
     * @return an Arraylist containing the String objects with Team User First
     *         Names
     */
    public ArrayList getTeamUserFirstNames() {
        return this.teamUserFirstNames;
    }

    /**
     * Sets the Arraylist containing the Team User First Names with the
     * ArrayList passed as the parameter
     * 
     * @param teamUserFirstNames
     *            An ArrayList with String objects having the Team User First
     *            Names
     */
    public void setTeamUserFirstNames(ArrayList teamUserFirstNames) {
        this.teamUserFirstNames = teamUserFirstNames;
    }

    /**
     * Returns an Arraylist containing the String objects with Team User First
     * Names
     * 
     * @return an Arraylist containing the String objects with Team User First
     *         Names
     */
    public ArrayList getUsrSiteNames() {
        return this.usrSiteNames;
    }

    /**
     * Sets the Arraylist containing the Team User First Names with the
     * ArrayList passed as the parameter
     * 
     * @param teamUserFirstNames
     *            An ArrayList with String objects having the Team User First
     *            Names
     */
    public void setUsrSiteNames(ArrayList usrSiteNames) {
        this.usrSiteNames = usrSiteNames;
    }

    /**
     * Returns an Arraylist containing the String objects with Team Rights
     * 
     * @return an Arraylist containing the String objects with Team Rights
     */
    public ArrayList getTeamRights() {
        return this.teamRights;
    }

    /**
     * Sets the Arraylist containing the Team Rights with the ArrayList passed
     * as the parameter
     * 
     * @param teamRights
     *            An ArrayList with String objects having the Team Rights
     */
    public void setTeamRights(ArrayList teamRights) {
        this.teamRights = teamRights;
    }

    /**
     * Returns an Arraylist containing the type of user (internal or external)
     * 
     * @return an Arraylist containing the type of user (internal or external)
     */
    public ArrayList getInexUsers() {
        return this.inexUsers;
    }

    /**
     * Sets the Arraylist containing the type of user (internal or external)
     * with the ArrayList passed as the parameter
     * 
     * @param teamRights
     *            An ArrayList with String objects having the type of user
     *            (internal or external)
     */
    public void setInexUsers(ArrayList inexUsers) {
        this.inexUsers = inexUsers;
    }

    /**
     * Returns the number of elements in the ArrayLists
     * 
     * @return the number of elements in the ArrayLists
     */
    public int getCRows() {
        return this.cRows;
    }

    /**
     * Sets the number of elements in the ArrayLists
     * 
     * @param cRows
     *            the number of elements in the ArrayLists
     */
    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    /**
     * Returns an Arraylist containing Integer objects with site Ids
     * 
     * @return an Arraylist containing Integer objects with site Ids
     */
    public ArrayList getSiteIds() {
        return this.siteIds;
    }

    /**
     * Sets the Arraylist containing the site Ids with the ArrayList passed as
     * the parameter
     * 
     * @param siteIds
     *            An ArrayList with Integer objects having the Team Ids
     */
    public void setSiteIds(ArrayList siteIds) {
        this.siteIds = siteIds;
    }

    /**
     * Adds the object to the Arraylist containing the Team Ids
     * 
     * @param teamId
     *            An Integer object that needs to be added to the Arraylist
     *            containing the Team Ids
     */
    public void setTeamIds(Integer teamId) {
        teamIds.add(teamId);
    }

    /**
     * Adds the object to the Arraylist containing the site Ids
     * 
     * @param siteId
     *            An Integer object that needs to be added to the Arraylist
     *            containing the Team Ids
     */
    public void setSiteIds(Integer siteId) {
        siteIds.add(siteId);
    }

    /**
     * Adds the object to the Arraylist containing the Team Roles
     * 
     * @param teamRole
     *            A String object that needs to be added to the Arraylist
     *            containing the Team Roles
     */
    public void setTeamRoles(String teamRole) {
        teamRoles.add(teamRole);
    }

    /**
     * Adds the object to the Arraylist containing the Team Role Ids
     * 
     * @param teamRoleId
     *            A String object that needs to be added to the Arraylist
     *            containing the Team Role Ids
     */
    public void setTeamRoleIds(String teamRoleId) {
        teamRoleIds.add(teamRoleId);
    }

    /**
     * Adds the object to the Arraylist containing the Team User Last Names
     * 
     * @param teamUserLastName
     *            A String object that needs to be added to the Arraylist
     *            containing the Team User Last Names
     */
    public void setTeamUserLastNames(String teamUserLastName) {
        teamUserLastNames.add(teamUserLastName);
    }

    /**
     * Adds the object to the Arraylist containing the Team User First Names
     * 
     * @param teamUserFirstName
     *            A String object that needs to be added to the Arraylist
     *            containing the Team User First Names
     */
    public void setTeamUserFirstNames(String teamUserFirstName) {
        teamUserFirstNames.add(teamUserFirstName);
    }

    /**
     * Adds the object to the Arraylist containing the Team User First Names
     * 
     * @param teamUserFirstName
     *            A String object that needs to be added to the Arraylist
     *            containing the Team User First Names
     */
    public void setUsrSiteNames(String usrSiteName) {
        usrSiteNames.add(usrSiteName);
    }

    /**
     * Adds the object to the Arraylist containing the Team Rights
     * 
     * @param teamRight
     *            A String object that needs to be addedto the Arraylist
     *            containing the Team Rights
     */
    public void setTeamRights(String teamRight) {
        teamRights.add(teamRight);
    }

    /**
     * Adds the object to the Arraylist containing the type of user (internal or
     * external)
     * 
     * @param inexUser
     *            A String object that needs to be addedto the Arraylist
     *            containing the type of user (internal or external)
     */
    public void setInexUsers(String inexUser) {
        inexUsers.add(inexUser);
    }

    public ArrayList getUserIds() {
        return this.userIds;
    }

    public void setUserIds(Integer userIds) {
        this.userIds.add(userIds);
    }

    /**
     * Returns an Arraylist containing userTeamType
     * 
     * @return an Arraylist containing userTeamType
     */
    public ArrayList getTeamUserTypes() {
        return this.teamUserTypes;
    }

    /**
     * Sets the Arraylist containing the userTeamType with the ArrayList passed
     * as the parameter
     * 
     * @param userTeamType
     *            An ArrayList with Integer objects having the Team Ids
     */
    public void setTeamUserTypes(ArrayList teamUserTypes) {
        this.teamUserTypes = teamUserTypes;
    }

    /**
     * Adds the object to the Arraylist containing the Team User Last Names
     * 
     * @param teamUserLastName
     *            A String object that needs to be added to the Arraylist
     *            containing the Team User Last Names
     */
    public void setTeamUserTypes(String teamUserType) {
        teamUserTypes.add(teamUserType);
    }

  
    /**
     * Returns an Arraylist containing the String objects with Team Status
     * 
     * @return an Arraylist containing the String objects with Team Status
     */
    public ArrayList getTeamStatus() {
        return this.teamStatus;
    }

    /**
     * Sets the Arraylist containing the Team Status with the ArrayList passed
     * as the parameter
     * 
     * @param teamRights
     *            An ArrayList with String objects having the Team Status
     */
    public void setTeamStatus(ArrayList teamStatus) {
        this.teamStatus = teamStatus;
    }

    
    /**
     * Adds the object to the Arraylist containing the Team Status
     * 
     * @param teamStatus
     *            A String object that needs to be addedto the Arraylist
     *            containing the Team Status
     */
    public void setTeamStatus(String teamStat) {
        teamStatus.add(teamStat);
    }
    
    
    /**
     * Returns an Arraylist containing Integer objects with Team Status Ids
     * 
     * @return an Arraylist containing Integer objects with Team Status Ids
     */
    public ArrayList getTeamStatusIds() {
        return this.teamStatusIds;
    }

    /**
     * Sets the Arraylist containing the Team Ids with the ArrayList passed as
     * the parameter
     * 
     * @param teamIds
     *            An ArrayList with Integer objects having the Team Status Ids
     */
    public void setTeamStatusIds(ArrayList teamStatusIds) {
        this.teamStatusIds = teamStatusIds;
    }
    
    
    /**
     * Adds the object to the Arraylist containing the Team Status Ids
     * 
     * @param teamStatusId
     *            An Integer object that needs to be added to the Arraylist
     *            containing the Team Status Ids
     */
    public void setTeamStatusIds(Integer teamStatusId) {
        teamStatusIds.add(teamStatusId);
    }
    
    //Added by IA 11.03.2006
    
    /**
     * Returns an Arraylist containing with User Address
     */
    public ArrayList getUserAddress() {
        return this.userAddresses;
    }

    /**
     * Sets the Arraylist containing the User Address with the ArrayList passed as
     * the parameter
     **/ 
    public void setUserAddress(ArrayList userAddres) {
        this.userAddresses = userAddres;
    }
    
    
    /**
     * Adds the object to the Arraylist containing the User Address
     * 
     */
    public void setUserAddress(String userAddress) {
    	userAddresses.add(userAddress);
    }
    
    /**
     * Returns an Arraylist containing with User Phone
     */
    public ArrayList getUserPhone() {
        return this.userPhones;
    }

    /**
     * Sets the Arraylist containing the User Phone with the ArrayList passed as
     * the parameter
     **/ 
    public void setUserPhone(ArrayList userPhone) {
        this.userPhones = userPhone;
    }
    
    
    /**
     * Adds the object to the Arraylist containing the User Phone
     * 
     */
    public void setUserPhone(String userPhone) {
    	userPhones.add(userPhone);
    }
    
    /**
     * Returns an Arraylist containing with User email
     */
    public ArrayList getUserEmail() {
        return this.userEmails;
    }

    /**
     * Sets the Arraylist containing the User email with the ArrayList passed as
     * the parameter
     **/ 
    public void setUserEmail(ArrayList userEmail) {
        this.userEmails= userEmail;
    }
    
    
    /**
     * Adds the object to the Arraylist containing the User email
     * 
     */
    public void setUserEmail(String userEmail) {
    	userEmails.add(userEmail);
    }
    //virendra
    
    public ArrayList getStudyStatus() {
		return studyStatuses;
	}

	public void setStudyStatus(ArrayList studyStatus) {
		this.studyStatuses = studyStatus;
	}
	 public void setStudyStatus(String studyStatus) {
		 studyStatuses.add(studyStatus);
	    }


    public ArrayList getUserType() {
        return this.userTypes;
    }
    
    /**
     * Sets the Arraylist containing the User email with the ArrayList passed as
     * the parameter
     **/ 
    public void setUserType(ArrayList userType) {
        this.userTypes= userType;
    }
     
    
    /**
     * Adds the object to the Arraylist containing the User email
     * 
     */
    public void setUserType(String userType) {
    	userTypes.add(userType);
    }
    //End added
    
    // end of getter and setter methods

    /**
     * Gets the Team details corresponding to some teamId in ArrayLists
     * 
     * @param teamId
     *            The teamId for which the Team details are to be fetched
     */

    public void getTeamValues(int studyId, int accId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select fk_user, PK_STUDYTEAM, "
                            + "FK_CODELST_TMROLE as teamRoleId, "
                            + "(select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER) as site_id, "
                            + "(select CODELST_DESC from er_codelst where  PK_CODELST = FK_CODELST_TMROLE) as teamRole, "
                            + "(select USR_LASTNAME from er_user where pk_user = FK_USER) as user_last_name, "
                            + "(select USR_FIRSTNAME from er_user where pk_user = FK_USER) as user_first_name, "
                            + "(select SITE_NAME from er_site where PK_SITE  = (select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER)) as site_name, "
                            + "STUDY_TEAM_RIGHTS, "
                            + "'1' as inexUser , nvl(STUDY_TEAM_USR_TYPE,'D') STUDY_TEAM_USR_TYPE  "
                            + "from	er_studyteam "
                            + "where FK_STUDY = ?  and nvl(STUDY_TEAM_USR_TYPE,'D') = 'D' "
                            + "and fk_user in (select pk_user from er_user where fk_account = ?) "
                            + "union "
                            + "select fk_user, PK_STUDYTEAM, "
                            + "FK_CODELST_TMROLE as teamRoleId, "
                            + "(select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER) as site_id, "
                            + "(select CODELST_DESC from er_codelst where  PK_CODELST = FK_CODELST_TMROLE) as teamRole, "
                            + "(select USR_LASTNAME from er_user where pk_user = FK_USER) as user_last_name, "
                            + "(select USR_FIRSTNAME from er_user where pk_user = FK_USER) as user_first_name, "
                            + "(select SITE_NAME from er_site where PK_SITE  = (select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER)) as site_name, "
                            + "STUDY_TEAM_RIGHTS, "
                            + "'2' as inexUser, nvl(STUDY_TEAM_USR_TYPE, 'D') STUDY_TEAM_USR_TYPE "
                            + "from	er_studyteam "
                            + "where FK_STUDY = ? "
                            + "and fk_user not in (select pk_user from er_user where fk_account = ?)"
                            + "order by inexUser, site_id, site_name ASC");
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, accId);
            pstmt.setInt(3, studyId);
            pstmt.setInt(4, accId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setTeamIds(new Integer(rs.getInt("PK_STUDYTEAM")));
                setTeamRoleIds((new Integer(rs.getInt("teamRoleId")))
                        .toString());
                setSiteIds(new Integer(rs.getInt("site_id")));
                setTeamRoles(rs.getString("teamRole"));
                setTeamUserLastNames(rs.getString("user_last_name"));
                setTeamUserFirstNames(rs.getString("user_first_name"));
                setUsrSiteNames(rs.getString("site_name"));
                setTeamRights(rs.getString("STUDY_TEAM_RIGHTS"));
                setInexUsers(rs.getString("inexUser"));
                setUserIds(new Integer(rs.getInt("fk_user")));
                setTeamUserTypes(rs.getString("STUDY_TEAM_USR_TYPE"));
                rows++;

                Rlog.debug("team", "TeamDao.getTeamValues rows " + rows);

            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("team",
                    "TeamDao.getTeamValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    /* modified by sonia abrol 10/25/07
     * to return the super user group right if the user is also a super user
     * also changed the ordre by to return the study team row as first row. 
     * in case of super user, pk_studyteam will be 0*/
    public void getTeamRights(int studyId, int userId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        
        String teamType = "";
        int teamrole  = 0;
        String teamRoleString = "";
        
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select PK_STUDYTEAM, FK_CODELST_TMROLE, (select CODELST_SUBTYP from er_codelst where PK_CODELST = FK_CODELST_TMROLE) as role_desc," +
                    		" STUDY_TEAM_RIGHTS ,'D' as teamtype "
                            + " from  er_studyteam  where FK_USER = ? and FK_STUDY = ?  and nvl(STUDY_TEAM_USR_TYPE, 'D') <> 'X' " 
                            + " union select 0,0,'', pkg_superuser.F_get_Superuser_right(?), 'S' as teamtype from dual where  pkg_superuser.F_Is_Superuser(?,?) > 0 " +
                            " Order by pk_studyteam desc" );
            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);
            
            pstmt.setInt(3, userId);
            pstmt.setInt(4, userId);
            pstmt.setInt(5, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setTeamIds(new Integer(rs.getInt("PK_STUDYTEAM")));
                                
                teamType = rs.getString("teamtype");
                
                if (teamType.equals("S"))
                {
                	//get ROle
                	
                	teamrole = GroupDao.getUserDefaultGroupStudyTeamRole(userId);
                	
                	if (teamrole == 0)
                	{
                		teamRoleString = "";
                	}else
                	{
                		teamRoleString = String.valueOf(teamrole);
                	}
                	
                	setTeamRoleIds(teamRoleString);
                	
                }
                else
                {
                	setTeamRoleIds((new Integer(rs.getInt("FK_CODELST_TMROLE"))).toString());
                }	
                
                setTeamRoles(rs.getString("role_desc"));
                setTeamRights(rs.getString("STUDY_TEAM_RIGHTS"));
                
                
                rows++;

                Rlog.debug("team", "TeamDao.getTeamRights rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("team",
                    "TeamDao.getTeamRights EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Retrieves users who have superuser access to a study
     * 
     * @param int
     *            studyId - study Id of the study
     */

    public void getSuperUserTeam(int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        
        ArrayList acclist = new ArrayList();
    	int accId = 0;   	
    	
        try {
        	StudyDao sDao = new StudyDao();
        	sDao.getStudy(studyId);
        	
        	acclist= sDao.getStudyAccountIds();
        	accId= StringUtil.stringToNum(""+acclist.get(0));
        	
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select distinct pk_user,  USR_LASTNAME , USR_FIRSTNAME ,site_name  "
                            + " from	er_user, er_site "
                            + " where  er_user.fk_account =? and pkg_superuser.F_Is_Superuser(pk_user,?) > 0 "
                            + "  and fk_siteid = pk_site "
                            + " and usr_stat<>'D' order by lower(site_name) asc");
            pstmt.setInt(1, accId);
            pstmt.setInt(2, studyId);
            
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUserIds(new Integer(rs.getInt("pk_user")));
                setTeamUserLastNames(rs.getString("usr_lastname"));
                setTeamUserFirstNames(rs.getString("usr_firstname"));
                setUsrSiteNames(rs.getString("site_name"));
                //setTeamRights(rs.getString("STUDY_TEAM_RIGHTS"));

                rows++;

                Rlog.debug("team", "TeamDao.getSuperUserTeam rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("team", "TeamDao.getSuperUserTeam EXCEPTION " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Retrieves users who have superuser access for budgets
     */

    public void getBgtSuperUserTeam() {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select distinct pk_user,  USR_LASTNAME , USR_FIRSTNAME ,site_name  "
                        + " from er_user, er_site "
                        + " where pkg_superuser.F_Is_BgtSuperuser(pk_user) > 0 "
                        + "  and fk_siteid = pk_site "
                        + " and usr_stat<>'D' order by lower(site_name) asc");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setUserIds(new Integer(rs.getInt("pk_user")));
                setTeamUserLastNames(rs.getString("usr_lastname"));
                setTeamUserFirstNames(rs.getString("usr_firstname"));
                setUsrSiteNames(rs.getString("site_name"));
                //setTeamRights(rs.getString("STUDY_TEAM_RIGHTS"));

                rows++;

                Rlog.debug("team", "TeamDao.getBgtSuperUserTeam rows " + rows);

            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("team", "TeamDao.getBgtSuperUserTeam EXCEPTION " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Retrieves study team infomation for an organization
     * 
     * @param int
     *            studyId - study Id of the study
     * @param int
     *            accId - account Id of the user
     * @param int
     *            orId - organization Id
     */
    //  Query modified by Manimaran for the July-August Enhancement S4.
    // Query modified by Gopu for fix the bugzilla Issues #2711
    public void getTeamValuesBySite(int studyId, int accId, int orgId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql1 = "";
        String sql2 = "";
        String sql3 = "";
        String sql = "";

        try {
            conn = getConnection();

            sql1 = "select fk_user, PK_STUDYTEAM, "
                    + "FK_CODELST_TMROLE as teamRoleId, "
                    + "(select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER) as site_id, "
                    + "(select CODELST_DESC from er_codelst where  PK_CODELST = FK_CODELST_TMROLE) as teamRole, "
                    + "(select USR_LASTNAME from er_user where pk_user = FK_USER) as user_last_name, "
                    + "(select USR_FIRSTNAME from er_user where pk_user = FK_USER) as user_first_name, "
                    + "(select SITE_NAME from er_site where PK_SITE  = (select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER)) as site_name, "
                    + "STUDY_TEAM_RIGHTS, "
                    + " '1' as inexUser , STUDY_TEAM_USR_TYPE, "
                    + " CODELST_DESC,PK_STATUS,"
                   //+ " CODELST_SUBTYP,PK_STATUS,"
                    + "( select ADD_PHONE FROM ER_ADD WHERE PK_ADD IN  (select FK_PERADD from er_user where pk_user = FK_USER )  )as user_phone, "					//Added by IA 11.03.2006 phone, email, address
                    + "( select ADD_EMAIL FROM ER_ADD WHERE PK_ADD IN  (select FK_PERADD from er_user where pk_user = FK_USER ) ) as user_email, "
                    + "( select USR_TYPE from er_user where pk_user = FK_USER) as user_type"
                    + " from er_studyteam,"
                    + " (Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from  er_studyteam i, ER_STATUS_HISTORY, "
                    +" er_codelst c Where i.fk_study = ? and i.PK_STUDYTEAM =  STATUS_MODPK and STATUS_MODTABLE =  'er_studyteam'"   
                    +" and  status_iscurrent=1 and c.pk_codelst = FK_CODELST_STAT ) "
                    
                    + " where FK_STUDY = ? and STATUS_MODPK (+) = pk_studyteam and STUDY_TEAM_USR_TYPE in('D','X') "
                    + " and fk_user in (select pk_user from er_user where fk_account = ? ";
            if (orgId != 0) {
                sql2 = " and fk_siteid = " + orgId;
            }
            sql3 = ") "
                    + " union "
                    + " select fk_user, PK_STUDYTEAM, "
                    + " FK_CODELST_TMROLE as teamRoleId, "
                    + " (select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER) as site_id, "
                    + " (select CODELST_DESC from er_codelst where  PK_CODELST = FK_CODELST_TMROLE) as teamRole, "
                    + " (select USR_LASTNAME from er_user where pk_user = FK_USER) as user_last_name, "
                    + " (select USR_FIRSTNAME from er_user where pk_user = FK_USER) as user_first_name, "
                    + "(select SITE_NAME from er_site where PK_SITE  = (select FK_SITEID from er_user where er_user.pk_user = er_studyteam.FK_USER)) as site_name, "
                    + "STUDY_TEAM_RIGHTS, "
                    + "'2' as inexUser,  STUDY_TEAM_USR_TYPE,"
                    + " CODELST_DESC,PK_STATUS,"
                    //+ " CODELST_SUBTYP,PK_STATUS, "
                    + "( select ADD_PHONE FROM ER_ADD WHERE PK_ADD IN  (select FK_PERADD from er_user where pk_user = FK_USER ) )as user_phone, "					//Added by IA 11.03.2006 phone, email, address
                    + "( select ADD_EMAIL FROM ER_ADD WHERE PK_ADD IN  (select FK_PERADD from er_user where pk_user = FK_USER ) ) as user_email, "
                    + "( select USR_TYPE from er_user where pk_user = FK_USER) as user_type"
                    + " from	er_studyteam, "
                    + "( Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS,STATUS_MODPK, FK_CODELST_STAT  from  er_studyteam i, ER_STATUS_HISTORY, "
                    +" er_codelst c Where i.fk_study = ? and i.PK_STUDYTEAM =  STATUS_MODPK and STATUS_MODTABLE =  'er_studyteam'"   
                    +" and status_iscurrent=1 and c.pk_codelst = FK_CODELST_STAT ) "
                    + " where FK_STUDY = ?  and STATUS_MODPK (+) = pk_studyteam"
                    + " and fk_user not in (select pk_user from er_user where fk_account = ?)"
                    + " order by inexUser, site_id, site_name ASC";

            sql = sql1 + sql2 + sql3;              
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyId);
            pstmt.setInt(3, accId);
            pstmt.setInt(4, studyId);
            pstmt.setInt(5, studyId);
            pstmt.setInt(6, accId); 
                      	
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setTeamIds(new Integer(rs.getInt("PK_STUDYTEAM")));
                setTeamRoleIds((new Integer(rs.getInt("teamRoleId"))).toString());
                setSiteIds(new Integer(rs.getInt("site_id")));
                setTeamRoles(rs.getString("teamRole"));
                setTeamUserLastNames(rs.getString("user_last_name"));
                setTeamUserFirstNames(rs.getString("user_first_name"));
                setUsrSiteNames(rs.getString("site_name"));
                setTeamRights(rs.getString("STUDY_TEAM_RIGHTS"));
                setInexUsers(rs.getString("inexUser"));
                setUserIds(new Integer(rs.getInt("fk_user")));
                setTeamUserTypes(rs.getString("STUDY_TEAM_USR_TYPE"));
                //setTeamStatus(rs.getString("CODELST_SUBTYP"));
                setTeamStatus(rs.getString("CODELST_DESC"));
                setTeamStatusIds(new Integer(rs.getInt("PK_STATUS")));
                setUserPhone(rs.getString("user_phone"));
                setUserEmail(rs.getString("user_email"));
                setUserType(rs.getString("user_type"));
                
                rows++;

                Rlog.debug("team", "TeamDao.getTeamValuesbySite rows " + rows);

            }

            setCRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("team",
                    "TeamDao.getTeamValues EXCEPTION IN FETCHING FROM User table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // /
    /**
     * Find whether user is present in the study team for a particular study
     * 
     * @param int
     *            studyId - study Id of the study
     * @param int
     *            userId - user Id of the user
     * @param int
     *            orId - organization Id
     */
    public void findUserInTeam(int studyId, int userId) {

        String subType = null;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "";
        try {

            conn = getConnection();
            sql = "select count(*) count "
                    + " from ER_STUDY_SITE_RIGHTS where " + " FK_USER = ?  "
                    + " and FK_STUDY = ? ";

            Rlog.debug("team", "inside method findUserInTeam sql" + sql);

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, userId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setCRows(rs.getInt("count"));
            }

        } catch (SQLException ex) {
            Rlog.fatal("team", "error in findUserInTeam" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    //JM: 
    /** Add users to multiple studies    
     * 
     * @param String[]
     * 					userIds - String array of user ids
     * @param String[]
     * 					studyIds - String array of study ids
     * @param String[]
     * 					roles - String array of roles 
     * @param int
     * 				user - logged in user id
     * @param ipAdd
     * 				ipAdd - ipAdd of the logged in user 
     * 
     * @return int
     */
    public int addUsersToMultipleStudies(String[] userIds, String[] studyIds, String[] roles, int user, String ipAdd){
    	
        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
       
            ArrayDescriptor adUserId = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY userId = new ARRAY(adUserId, conn, userIds);
            
            ArrayDescriptor adstudyId = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY studyId = new ARRAY(adstudyId, conn, studyIds);
            
            ArrayDescriptor adRole = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
            ARRAY role = new ARRAY(adRole, conn, roles);
            
            cstmt = conn.prepareCall("{call sp_addUsersToMultipleStudies(?,?,?,?,?,?)}");
            
            cstmt.setArray(1, userId);
            cstmt.setArray(2, studyId);
            cstmt.setArray(3, role);
            cstmt.setInt(4,user);
            cstmt.setString(5,ipAdd);            
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();
            ret = cstmt.getInt(6);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("Team","EXCEPTION in addUsersToMultipleStudies()"+ e);
            return 0;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
   
    

    // end of class
}
