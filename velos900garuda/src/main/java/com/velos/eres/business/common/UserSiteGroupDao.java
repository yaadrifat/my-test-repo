/*
 * Classname			UserSiteGroupDao
 * 
 * Version information	1.0
 *
 * Date				01/30/2012
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.business.grpRights.impl.GrpRightsBean;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * UserSiteGroupDao class
 * 
 * @author Yogesh Kumar
 * @version 1.0 01/30/2012
 */

public class UserSiteGroupDao extends CommonDAO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7841490993182286406L;
	
	private int userId; // pk_user
	private ArrayList pkUsrSiteGrp; // pk_VALUE
	private ArrayList userSiteIds; // pk_usersite
    private ArrayList userFkGroupIds; // pk_usergroup

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ArrayList getPkUsrSiteGrp() {
		return pkUsrSiteGrp;
	}

	public void setPkUsrSiteGrp(ArrayList pkUsrSiteGrp) {
		this.pkUsrSiteGrp = pkUsrSiteGrp;
	}
	
	public void setPkUsrSiteGrp(String pkUsrSiteGrp) {
        this.pkUsrSiteGrp.add(pkUsrSiteGrp);
    }
	
	public ArrayList getUserSiteIds() {
		return userSiteIds;
	}

	public void setUserSiteIds(ArrayList userSiteIds) {
		this.userSiteIds = userSiteIds;
	}

	public void setUserSiteIds(String userSiteIds) {
        this.userSiteIds.add(userSiteIds);
    }
	
	public ArrayList getUserFkGroupIds() {
		return userFkGroupIds;
	}

	public void setUserFkGroupIds(ArrayList userFkGroupIds) {
		this.userFkGroupIds = userFkGroupIds;
	}

	public void setUserFkGroupIds(String userFkGroupIds) {
        this.userFkGroupIds.add(userFkGroupIds);
    }
	
	public UserSiteGroupDao()
	{
		userId = 0;
		pkUsrSiteGrp = new ArrayList();
		userSiteIds = new ArrayList();
		userFkGroupIds = new ArrayList();
		
	}
	public void getUsrSiteGrp(int userid) {
        PreparedStatement pstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("userSiteGroup", "In UserSiteGroupDao getUsrSiteGrp() ");
            
            pstmt = conn.prepareStatement("SELECT PK_USR_SITE_GRP,FK_GRP_ID,FK_USER_SITE from ER_USRSITE_GRP WHERE FK_USER=? order by FK_USER_SITE");
            pstmt.setInt(1, userid);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setPkUsrSiteGrp(rs.getString("PK_USR_SITE_GRP"));
                setUserSiteIds(rs.getString("FK_USER_SITE"));
                setUserFkGroupIds(rs.getString("FK_GRP_ID"));
            }
        } catch (Exception e) {
            Rlog.fatal("userSiteGroup",
                    "EXCEPTION in updateUsrSiteGrp, excecuting Stored Procedure "
                            + e);
        } finally {
            try {
                if (pstmt != null)
                	pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
	public int updateUsrSiteGrp(int userid, String[] userpkSite,String[] fkGroupIds,String[] pkUserSiteGrp,String ipAdd,String creator) {
        int ret = 1;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("userSiteGroup", "In UserSiteGroupDao updateUsrSiteGrp() ");

            ArrayDescriptor inPkUserSites = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);

            ARRAY pkUserSitesArray = new ARRAY(inPkUserSites, conn, userpkSite);

            ArrayDescriptor infkGroupIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY fkGroupIdsArray = new ARRAY(infkGroupIds, conn, fkGroupIds);
            
            ArrayDescriptor inpkUserSiteGrp = ArrayDescriptor.createDescriptor(
            		"ARRAY_STRING", conn);
            ARRAY pkUserSiteGrpArray = new ARRAY(inpkUserSiteGrp, conn, pkUserSiteGrp);

            cstmt = conn
                    .prepareCall("{call PKG_USER.SP_ATTACH_USER_SITE_GROUPS(?,?,?,?,?,?,?)}");
            cstmt.setInt(1, userid);
            cstmt.setArray(2, pkUserSitesArray);
            cstmt.setArray(3, fkGroupIdsArray);
            cstmt.setArray(4, pkUserSiteGrpArray);
            cstmt.setString(5, ipAdd);
            cstmt.setString(6, creator);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(7);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("userSiteGroup",
                    "EXCEPTION in updateUsrSiteGrp, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
	
    /**
     * Created by : Ankit Kumar
     * Created On : 18-Apr-2012 
     * Enhancement: Garuda Security Imp
     * Purpose    : Compare and return maximum rights of Groups within Organization.
     * @return    : GrpRightsBean;
     */
	public GrpRightsBean getMaxGrpRights(long userId, long siteId) {
		
        PreparedStatement pstmt = null;
        Connection conn = null;
        GrpRightsBean gsk = new GrpRightsBean();

        String grpRight = null;
        CtrlDao cntrl;
        ArrayList cVal, cDesc, cSeq;
        cVal = new ArrayList();
        cVal = new ArrayList();
        cSeq = new ArrayList();
        int sLength = 0;
        int counter = 0;

        cntrl = new CtrlDao();
        
        try {
            conn = getConnection();
            Rlog.debug("userSiteGroup", "In UserSiteGroupDao getMaxGrpRights() ");
            List<StringBuilder> builders = new LinkedList<StringBuilder>();
            pstmt = conn.prepareStatement("SELECT GRP_RIGHTS FROM ER_GRPS G " +
            		"LEFT JOIN ER_USRSITE_GRP U ON U.FK_GRP_ID=G.PK_GRP LEFT JOIN " +
            		"ER_USERSITE EU ON EU.PK_USERSITE=U.FK_USER_SITE WHERE U.FK_USER = ? " +
            		"AND EU.FK_SITE = ? ORDER BY GRP_RIGHTS");
            pstmt.setLong(1, userId);
            pstmt.setLong(2, siteId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	builders.add(new StringBuilder(rs.getString("GRP_RIGHTS")));
            }
            if(builders!=null && builders.size() > 1)
            	grpRight = compare(builders);  // Compare and return the max rights.  
            else if(builders!=null && builders.size() == 1)
            	grpRight = builders.get(0).toString();
            
            gsk.setGrpRights(grpRight); // Setting the Max Rights
            if (!(StringUtil.isEmpty(grpRight))) {
                sLength = grpRight.length();
            }
            for (counter = 0; counter <= (sLength - 1); counter++) {
                gsk.setFtrRights(String.valueOf(grpRight.charAt(counter)));
                gsk.setGrSeq(Integer.toString(counter));
            }

            cntrl.getControlValues("app_rights");
            cVal = cntrl.getCValue();
            cDesc = cntrl.getCDesc();
            gsk.setGrValue(cVal);
            gsk.setGrDesc(cDesc);
            
        } catch (Exception e) {
            Rlog.fatal("userSiteGroup",
                    "EXCEPTION in getMaxGrpRights, excecuting Stored Procedure "
                            + e);
        } finally {
            try {
                if (pstmt != null)
                	pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return gsk;
    }
	public static String compare(List<StringBuilder> builders) {
			int length = builders.size();
			StringBuilder a, b = null;
			StringBuilder smaller, larger;
			a = builders.get(0);
			for (int i = 0; i < length - 1; i++) {
			b = builders.get(i + 1);
	
			if (a.length() <= b.length()) {
			smaller = a;
			larger = b;
	
			} else {
			smaller = b;
			larger = a;
			}
	
			a = returnedChar(smaller, larger);
	
			}
			return a.toString();
	}
	private static StringBuilder returnedChar(StringBuilder smaller, StringBuilder larger) {
			StringBuilder greater = new StringBuilder("");
			String extra;
			for (int i = 0; i < smaller.length(); i++) {
			if (smaller.charAt(i) > larger.charAt(i)) {
			greater = greater.append(smaller.charAt(i));
			} else {
			greater = greater.append(larger.charAt(i));
			}
			}
			int pos = larger.length() - (smaller.length());
			extra = larger.substring((larger.length()-pos),larger.length());
			return greater.append(extra);
	}

} // end of main class

