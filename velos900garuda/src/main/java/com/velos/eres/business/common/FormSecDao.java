/*
 * Classname			FieldSecDao.class
 * 
 * Version information 	1.0
 *
 * Date					18/07/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * FieldSecDao for getting FieldSec records
 * 
 * @author Sonia Kaura
 * @version : 1.0 18/07/2003
 */

public class FormSecDao extends CommonDAO implements java.io.Serializable {

    private ArrayList formSecId;

    private ArrayList formLibId;

    private ArrayList formSecName;

    private ArrayList formSecSeq;

    private ArrayList formSecFmt;

    private ArrayList formSecRepNo;

    private ArrayList recordType;

    private ArrayList creator;

    private ArrayList modifiedBy;

    private ArrayList ipAdd;

    private ArrayList offlineFlag;

    private int rows;

    public FormSecDao() {
        formSecId = new ArrayList();
        formLibId = new ArrayList();
        formSecName = new ArrayList();
        formSecSeq = new ArrayList();
        formSecFmt = new ArrayList();
        formSecRepNo = new ArrayList();
        recordType = new ArrayList();
        creator = new ArrayList();
        modifiedBy = new ArrayList();
        ipAdd = new ArrayList();
        offlineFlag = new ArrayList();
    }

    // //////////////////////////////////////////////////////////////////////////////

    public ArrayList getFormSecId() {
        return this.formSecId;
    }

    public void setFormSecId(ArrayList formSecId) {
        this.formSecId = formSecId;
    }

    public ArrayList getFormLibId() {
        return this.formLibId;
    }

    public void setFormLibId(ArrayList formLibId) {
        this.formLibId = formLibId;
    }

    public ArrayList getFormSecName() {
        return this.formSecName;
    }

    public void setFormSecName(ArrayList formSecName) {
        this.formSecName = formSecName;
    }

    public void setOfflineFlag(ArrayList offlineFlag) {
        this.offlineFlag = offlineFlag;
    }

    public ArrayList getOfflineFlag() {
        return this.offlineFlag;
    }

    public ArrayList getFormSecSeq() {
        return this.formSecSeq;
    }

    public void setFormSecSeq(ArrayList formSecSeq) {
        this.formSecSeq = formSecSeq;
    }

    public ArrayList getFormSecFmt() {
        return this.formSecFmt;
    }

    public void setFormSecFmt(ArrayList formSecFmt) {
        this.formSecFmt = formSecFmt;
    }

    public ArrayList getFormSecRepNo() {
        return this.formSecRepNo;
    }

    public void setFormSecRepNo(ArrayList formSecRepNo) {

        this.formSecRepNo = formSecRepNo;
    }

    public ArrayList getRecordType() {
        return this.recordType;
    }

    public void setRecordType(ArrayList recordType) {
        this.recordType = recordType;
    }

    public ArrayList getCreator() {
        return this.creator;
    }

    public void setCreator(ArrayList creator) {
        this.creator = creator;
    }

    public ArrayList getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(ArrayList modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public ArrayList getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(ArrayList ipAdd) {
        this.ipAdd = ipAdd;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int i) {
        rows = i;
    }

    // /SINGLE GETTERS AND SETTERS
    // /////////////////////////////////////////////////////////////////////////////////

    public Object getFormSecId(int i) {

        return this.formSecId.get(i);
    }

    public void setFormSecId(Integer formSecId) {
        this.formSecId.add(formSecId);
    }

    // ///////////
    public Object getFormLibId(int i) {

        return this.formLibId.get(i);

    }

    public void setFormLibId(String formLibId) {
        this.formLibId.add(formLibId);
    }

    // ////////////
    public Object getFormSecName(int i) {
        return this.formSecName.get(i);
    }

    public Object getOfflineFlag(int i) {
        return this.offlineFlag.get(i);
    }

    public void setFormSecName(String formSecName) {
        this.formSecName.add(formSecName);
    }

    public void setOfflineFlag(String offlineFlag) {
        this.offlineFlag.add(offlineFlag);
    }

    // ///////////////

    public Object getFormSecSeq(int i) {
        return this.formSecSeq.get(i);
    }

    public void setFormSecSeq(Integer formSecSeq) {
        this.formSecSeq.add(formSecSeq);
    }

    // //////////////

    public Object getFormSecFmt(int i) {
        return this.formSecFmt.get(i);
    }

    public void setFormSecFmt(String formSecFmt) {
        this.formSecFmt.add(formSecFmt);
    }

    // ///////////

    public Object getFormSecRepNo(int i) {
        return this.formSecRepNo.get(i);
    }

    public void setFormSecRepNo(Integer formSecRepNo) {
        this.formSecRepNo.add(formSecRepNo);
    }

    // //////
    public Object getRecordType(int i) {
        return this.recordType.get(i);
    }

    public void setRecordType(String recordType) {
        this.recordType.add(recordType);
    }

    // /////
    public Object getCreator(int i) {

        return this.creator.get(i);

    }

    public void setCreator(String creator) {
        this.creator.add(creator);
    }

    // //////////////

    public Object getModifiedBy(int i) {
        return this.modifiedBy.get(i);
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy.add(modifiedBy);
    }

    // //////

    public Object getIpAdd(int i) {
        return this.ipAdd.get(i);
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd.add(ipAdd);
    }

    // ////////////////////////////////////////////////

    /**
     * 
     * 
     * @param formLibId
     * @param formSecName
     * @param formSecSeq
     * @param formSecFmt
     * @param formSecRepNo
     * @param recordType
     * @param creator
     * @param modifiedBy
     * @param ipAdd
     */

    public void insertNewSections(ArrayList formLibId, ArrayList formSecName,
            ArrayList formSecSeq, ArrayList formSecFmt, ArrayList formSecRepNo,
            ArrayList recordType, ArrayList creator, ArrayList modifiedBy,
            ArrayList ipAdd)

    {

        setFormLibId(formLibId);
        setFormSecName(formSecName);
        setFormSecSeq(formSecSeq);
        setFormSecFmt(formSecFmt);
        setFormSecRepNo(formSecRepNo);
        setRecordType(recordType);
        setCreator(creator);
        setIpAdd(ipAdd);
        setRows(ipAdd.size());

    }

    // ////////////////////////////

    /**
     * 
     * 
     * @param formLibId
     */
    public void getAllSectionsOfAForm(int formLibId) {

        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement(" SELECT   FORMSEC_SEQ ,FORMSEC_NAME , "
                            + " 	FORMSEC_REPNO  , FORMSEC_FMT , PK_FORMSEC, FORMSEC_OFFLINE "
                            + "	FROM ER_FORMSEC "
                            + "	WHERE FK_FORMLIB=? AND RECORD_TYPE <> 'D' "
                            + "  ORDER BY FORMSEC_SEQ ");

            pstmt.setInt(1, formLibId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setFormSecSeq(new Integer(rs.getInt("FORMSEC_SEQ")));

                setFormSecName(rs.getString("FORMSEC_NAME"));

                setFormSecRepNo(new Integer(rs.getInt("FORMSEC_REPNO")));

                setFormSecFmt(rs.getString("FORMSEC_FMT"));

                setFormSecId(new Integer(rs.getInt("PK_FORMSEC")));

                setOfflineFlag(rs.getString("FORMSEC_OFFLINE"));

                rows++;

            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("formsection",
                    "FormSecDao.getAllSectionsOfAForm EXCEPTION IN FETCHING FROM ER_FORMSEC TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * This method retrieves sections according to the offfine flag
     * 
     * @param formLibId
     * @param offline
     *            flag
     */
    public void getAllSectionsOfAForm(int formLibId, int offline) {

        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement(" SELECT   FORMSEC_SEQ ,FORMSEC_NAME , "
                            + " 	FORMSEC_REPNO  , FORMSEC_FMT , PK_FORMSEC "
                            + "	FROM ER_FORMSEC "
                            + "	WHERE FK_FORMLIB=? AND NVL(FORMSEC_OFFLINE,0) = ? AND RECORD_TYPE <> 'D' "
                            + "  ORDER BY FORMSEC_SEQ ");

            pstmt.setInt(1, formLibId);
            pstmt.setInt(2, offline);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setFormSecSeq(new Integer(rs.getInt("FORMSEC_SEQ")));

                setFormSecName(rs.getString("FORMSEC_NAME"));

                setFormSecRepNo(new Integer(rs.getInt("FORMSEC_REPNO")));

                setFormSecFmt(rs.getString("FORMSEC_FMT"));

                setFormSecId(new Integer(rs.getInt("PK_FORMSEC")));
                rows++;

            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog.fatal("formsection",
                    "FormSecDao.getAllSectionsOfAForm EXCEPTION IN FETCHING FROM ER_FORMSEC TABLE"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * 
     * 
     * @param formSecId
     * @param formLibId
     * @param formSecName
     * @param formSecSeq
     * @param formSecFmt
     * @param formSecRepNo
     * @param recordType
     * @param creator
     * @param modifiedBy
     * @param ipAdd
     */
    public void updateNewSections(ArrayList formSecId, ArrayList formLibId,
            ArrayList formSecName, ArrayList formSecSeq, ArrayList formSecFmt,
            ArrayList formSecRepNo, ArrayList recordType, ArrayList creator,
            ArrayList modifiedBy, ArrayList ipAdd)

    {

        setFormSecId(formSecId);
        setFormLibId(formLibId);
        setFormSecName(formSecName);
        setFormSecSeq(formSecSeq);
        setFormSecFmt(formSecFmt);
        setFormSecRepNo(formSecRepNo);
        setRecordType(recordType);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setRows(ipAdd.size());

    }

    public int updateDefaultFormSec(int pkFormSec, int pkForm, String user,
            String ipAdd) {

        int retValue = 0;

        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("formsection",
                    "In updateDefaultFormSec updateDefaultFormSec() ");
            cstmt = conn
                    .prepareCall("{call PKG_FORM.SP_DEF_SEC_DELETE (?,?,?,?,?)}");
            cstmt.setInt(1, pkFormSec);
            cstmt.setInt(2, pkForm);
            cstmt.setString(3, user);
            cstmt.setString(4, ipAdd);
            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();
            retValue = cstmt.getInt(5);

            return retValue;

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "EXCEPTION in updateDefaultFormSec, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /*
     * * Function to set the browserflag of field to be displayed * in browser
     * to 0 when the section repeat flag is greater than 0 * By Anu
     */

    public int updateBrowserFlagForRepFld(FormSecDao formSecDao) {

        ArrayList formSecIds = new ArrayList();
        ArrayList arrSecIds = new ArrayList();
        String secIds = "";
        int i = 1;
        formSecIds = formSecDao.getFormSecId();
        Rlog.debug("formsection", "formSecIds" + formSecIds);
        int size = formSecIds.size();
        Rlog.debug("formsection", "size" + size);

        for (i = 0; i < size; i++) {
            if (!(formSecDao.getFormSecRepNo(i).toString()).equals("0")) {

                if (i == 0) {
                    secIds = formSecDao.getFormSecId(i).toString();
                    Rlog.debug("formsection", "secids in i=0" + secIds);
                } else {
                    if (secIds.equals("")) {
                        secIds = formSecDao.getFormSecId(i).toString();
                        Rlog.debug("formsection", "secids in equals " + secIds);
                    } else {
                        secIds = secIds + ","
                                + formSecDao.getFormSecId(i).toString();
                        Rlog.debug("formsection", "secids in else" + secIds);
                    }
                }

            }
        }
        Rlog.debug("formsection", "secids" + secIds);
        if (!secIds.equals("")) {
            int rows = 0;
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = getConnection();

                Rlog.debug("formsection", "before statement");

                pstmt = conn
                        .prepareStatement(" UPDATE ER_FORMFLD SET FORMFLD_BROWSERFLG=0 "
                                + " 	WHERE FK_FORMSEC IN("
                                + secIds
                                + ") "
                                + "	and  formfld_browserflg = 1");

                ResultSet rs = pstmt.executeQuery();

            } catch (SQLException ex) {
                Rlog
                        .fatal(
                                "formsection",
                                "FormSecDao.updateBrowserFlagForRepFld EXCEPTION IN FETCHING FROM ER_FORMfld TABLE"
                                        + ex);
                return -1;

            } finally {
                try {
                    if (pstmt != null)
                        pstmt.close();
                } catch (Exception e) {
                }
                try {
                    if (conn != null)
                        conn.close();
                } catch (Exception e) {
                }

            }
        }

        return 1;
    }

    /*
     * This Function sets the browser flag to 1 for the data entry date field
     * when its repeat number is set to 0
     */

    public int updateBrowserFlag(int pkFormSec) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            Rlog.debug("formsection", "%%%  in Dao pkFormSec" + pkFormSec);
            pstmt = conn
                    .prepareStatement(" UPDATE ER_FORMFLD SET FORMFLD_BROWSERFLG=1 "
                            + " WHERE PK_FORMFLD = ( SELECT PK_FORMFLD FROM "
                            + " ER_FORMFLD,ER_FLDLIB,ER_FORMSEC "
                            + " WHERE PK_FORMSEC = "
                            + pkFormSec
                            + " "
                            + " AND FLD_SYSTEMID ='er_def_date_01' "
                            + " AND PK_FORMSEC=FK_FORMSEC "
                            + " AND PK_FIELD=FK_FIELD )");

            ResultSet rs = pstmt.executeQuery();

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "formsection",
                            "FormSecDao.UPDATE FORMLFD_BROWSERFLG EXCEPTION IN FETCHING FROM ER_FORMfld TABLE"
                                    + ex);
            return -1;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        return 1;
    }

    /*
     * Method to update all the sequences of sections in a form(when a duplicate
     * seq is entered) by incrementing the formsec_seq of the duplicate and all
     * other section sequences whose seq are greater than the duplicate seq
     */

    public int updateSectionSeq(int formLibId) {
        int retValue = 0;
        Rlog.debug("formsection", "In updateSectionSeq  formLibId" + formLibId);

        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("formsection", "In updateSectionSeq  ");
            cstmt = conn
                    .prepareCall("{call PKG_SECFLD.SP_UPDATE_SECTION_SEQ (?,?)}");
            cstmt.setInt(1, formLibId);

            cstmt.registerOutParameter(2, java.sql.Types.INTEGER);

            cstmt.execute();
            retValue = cstmt.getInt(2);

            return retValue;

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "EXCEPTION in updateSectionSeq, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

}