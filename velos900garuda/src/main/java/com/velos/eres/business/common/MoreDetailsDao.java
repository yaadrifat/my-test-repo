package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * StudyIdDao for getting StudyId records
 * 
 * @author Sonia Sahni
 * @version : 1.0 09/24/2003
 */

public class MoreDetailsDao extends CommonDAO implements java.io.Serializable {
    private ArrayList id;

    private ArrayList mdElementIds;

    private ArrayList mdElementDescs;

    private ArrayList mdElementValues;

    private ArrayList mdModIds;

    private ArrayList recordTypes;

    private ArrayList dispTypes;

    private ArrayList dispDatas;

    private int cRows;
    public MoreDetailsDao() {
        id = new ArrayList();
        mdElementIds = new ArrayList();
        mdElementDescs = new ArrayList();
        mdElementValues = new ArrayList();
        mdModIds = new ArrayList();
        recordTypes = new ArrayList();
        dispTypes = new ArrayList();
        dispDatas = new ArrayList();

    }

    /**
	 * @return the dispDatas
	 */
	public ArrayList getDispDatas() {
		return dispDatas;
	}

	/**
	 * @param dispDatas the dispDatas to set
	 */
	public void setDispDatas(ArrayList dispDatas) {
		this.dispDatas = dispDatas;
	}
	/**
	 * @param dispDatas the dispData to set
	 */
	public void setDispDatas(String dispData) {
		this.dispDatas.add(dispData);
	}
	/**
	 * @return the dispTypes
	 */
	public ArrayList getDispTypes() {
		return dispTypes;
	}

	/**
	 * @param dispTypes the dispTypes to set
	 */
	public void setDispTypes(ArrayList dispTypes) {
		this.dispTypes = dispTypes;
	}
	/**
	 * @param dispTypes the dispTypes to set
	 */
	public void setDispTypes(String dispType) {
		this.dispTypes.add(dispType);
	}

	/**
	 * @return the mdElementDescs
	 */
	public ArrayList getMdElementDescs() {
		return mdElementDescs;
	}

	/**
	 * @param mdElementDescs the mdElementDescs to set
	 */
	public void setMdElementDescs(ArrayList mdElementDescs) {
		this.mdElementDescs = mdElementDescs;
	}
	/**
	 * @param mdElementDescs the mdElementDescs to set
	 */
	public void setMdElementDescs(String mdElementDesc) {
		this.mdElementDescs.add(mdElementDesc);
	}
	/**
	 * @return the mdElementIds
	 */
	public ArrayList getMdElementIds() {
		return mdElementIds;
	}

	/**
	 * @param mdElementIds the mdElementIds to set
	 */
	public void setMdElementIds(ArrayList mdElementIds) {
		this.mdElementIds = mdElementIds;
	}
	/**
	 * @param mdElementIds the mdElementIds to set
	 */
	public void setMdElementIds(Integer mdElementId) {
		this.mdElementIds.add(mdElementId);
	}

	/**
	 * @return the mdElementValues
	 */
	public ArrayList getMdElementValues() {
		return mdElementValues;
	}

	/**
	 * @param mdElementValues the mdElementValues to set
	 */
	public void setMdElementValues(ArrayList mdElementValues) {
		this.mdElementValues = mdElementValues;
	}
	/**
	 * @param mdElementValues the mdElementValues to set
	 */
	public void setMdElementValues(String mdElementValue) {
		this.mdElementValues.add(mdElementValue);
	}
	/**
	 * @return the mdModIds
	 */
	public ArrayList getMdModIds() {
		return mdModIds;
	}

	/**
	 * @param mdModIds the mdModIds to set
	 */
	public void setMdModIds(ArrayList mdModIds) {
		this.mdModIds = mdModIds;
	}
	/**
	 * @param mdModIds the mdModIds to set
	 */
	public void setMdModIds(String mdModId) {
		this.mdModIds.add(mdModId);
	}

	/**
	 * @return the recordTypes
	 */
	public ArrayList getRecordTypes() {
		return recordTypes;
	}

	/**
	 * @param recordTypes the recordTypes to set
	 */
	public void setRecordTypes(ArrayList recordTypes) {
		this.recordTypes = recordTypes;
	}
	/**
	 * @param recordTypes the recordTypes to set
	 */
	public void setRecordTypes(String recordType) {
		this.recordTypes.add(recordType);
	}
	
    // Getter and Setter methods
    public void setId(ArrayList id) {
        this.id = id;
    }
    
     public ArrayList getId() {
        return id;
    }

   
    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setId(Integer id) {
        this.id.add(id);
    }

   
    // end of getter and setter methods
   
    public void getMoreDetails(int modId,String modName, String defUserGroup ) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        try {
            conn = getConnection();
            
            int grpId = 0;
            
            grpId = StringUtil.stringToNum(defUserGroup);

            sbSQL
                    .append(" Select pk_moredetails, pk_codelst, codelst_desc, md_modelementdata,'M' record_type, codelst_seq,codelst_custom_col");
            sbSQL.append(",codelst_custom_col1 from  er_moredetails, er_codelst ");
            sbSQL
                    .append(" where fk_modpk = ? and  pk_codelst = md_modelementpk  and ");
            sbSQL.append(" codelst_type = ? ");
            sbSQL.append("  UNION ");
            sbSQL
                    .append("  Select 0 pk_moredetails , pk_codelst, codelst_desc,' ' md_modelementdata ,'N' record_type, codelst_seq,codelst_custom_col");
            sbSQL
                    .append(",codelst_custom_col1 from  er_codelst  where pk_codelst not in ");
            sbSQL
                    .append("(Select md_modelementpk from er_moredetails where   fk_modpk = ? ) and ");
            sbSQL.append(" codelst_type = ? ");
            
            if (grpId > 0) //append group check
            {
            	sbSQL.append(" and not exists (select * from er_codelst_hide h where h.codelst_type = ? and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ");
            	
            }
            
            sbSQL.append(" order by codelst_seq  ");

            pstmt = conn.prepareStatement(sbSQL.toString());

            
            pstmt.setInt(1, modId);
            pstmt.setString(2, modName);
            pstmt.setInt(3, modId);
            pstmt.setString(4, modName);
            
            if (grpId > 0) //append group check
            {
            	pstmt.setString(5, modName);
            	pstmt.setInt(6, grpId);
            }

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setId(new Integer(rs.getInt("pk_moredetails")));
                this.setMdElementIds((new Integer(rs.getInt("pk_codelst"))));
                this.setMdElementDescs((rs.getString("codelst_desc")));
                this.setMdElementValues((rs.getString("md_modelementdata")));
                setRecordTypes(rs.getString("record_type"));
                setDispTypes(rs.getString("codelst_custom_col"));
                setDispDatas(rs.getString("codelst_custom_col1"));
                rows++;
               
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("moredetails",
                    "moreDetailsDao.getMoreDetails EXCEPTION IN FETCHING rows" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of class
}
