/* class MileApndxBean.java
 * version 4.0
 * date 04.06.02
 * Author Dinesh Khurana
 * Copy right notice Velos Inc, 
 */

package com.velos.eres.business.mileApndx.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "ER_MILEAPNDX")
public class MileApndxBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3617855295888439092L;

    /**
     * the mileApndxId
     */

    public int mileApndxId;

    /**
     * studyId
     */

    public Integer studyId;

    /**
     * milestoneId
     */

    public Integer milestoneId;

    /**
     * milestone desc
     */

    public String desc;

    /**
     * milestone uri
     */

    public String uri;

    /**
     * milestone type
     */

    public String type;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_MILEAPNDX", allocationSize=1)
    @Column(name = "PK_MILEAPNDX")
    public int getMileApndxId() {
        return mileApndxId;
    }

    public void setMileApndxId(int id) {
        mileApndxId = id;
    }

    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);
    }

    public void setStudyId(String studyId) {
        if (studyId != null)
            this.studyId = Integer.valueOf(studyId);
    }

    @Column(name = "FK_MILESTONE")
    public String getMilestoneId() {
        return StringUtil.integerToString(this.milestoneId);
    }

    public void setMilestoneId(String milestoneId) {
        if (milestoneId != null) {
            this.milestoneId = Integer.valueOf(milestoneId);
        }
    }

    @Column(name = "MILEAPNDX_DESC")
    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Column(name = "MILEAPNDX_URI")
    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Column(name = "MILEAPNDX_TYPE")
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "Creator")
    public String getCreator() {
    	
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null)
        	this.creator = Integer.valueOf(creator);
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null)
        	this.modifiedBy = Integer.valueOf(modifiedBy);
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // /end of setter getter methods

    /*
     * public void setMileApndxStateKeeper(MileApndxStateKeeper mask) {
     * 
     * try { Connection conn = null; conn = getConnection(); mileApndxId =
     * GenerateId.getId("SEQ_ER_MILEAPNDX", conn); conn.close();
     * 
     * setStudyId(mask.getStudyId()); setMilestoneId(mask.getMilestoneId());
     * setDesc(mask.getDesc()); setUri(mask.getUri()); setType(mask.getType());
     * setCreator(mask.getCreator()); setModifiedBy(mask.getModifiedBy());
     * setIpAdd(mask.getIpAdd()); } catch (Exception e) {
     * Rlog.fatal("mileapndx", "exception in setMileApndxStateKeeper" + e); } }
     */

    public int updateMileApndx(MileApndxBean mask) {

        try {

            setStudyId(mask.getStudyId());
            setMilestoneId(mask.getMilestoneId());
            setDesc(mask.getDesc());
            setUri(mask.getUri());
            setType(mask.getType());
            setCreator(mask.getCreator());
            setModifiedBy(mask.getModifiedBy());
            setIpAdd(mask.getIpAdd());

        } catch (Exception e) {
            Rlog.fatal("mileapndx", "exception in setMileApndxStateKeeper" + e);
            return -1;
        }
        return 0;
    }

    public MileApndxBean() {

    }

    public MileApndxBean(int mileApndxId, String studyId, String milestoneId,
            String desc, String uri, String type, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setMileApndxId(mileApndxId);
        setStudyId(studyId);
        setMilestoneId(milestoneId);
        setDesc(desc);
        setUri(uri);
        setType(type);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /*
     * public MileApndxStateKeeper getMileApndxStateKeeper() { return new
     * MileApndxStateKeeper(getMileApndxId(), getStudyId(), getMilestoneId(),
     * getDesc(), getUri(), getType(), getCreator(), getModifiedBy(),
     * getIpAdd()); }
     */

}
