/*
 * Classname			StudyNotifyBean.class
 * 
 * Version information
 *
 * Date					02/26/2001 
 * 
 * Copyright notice
 */

package com.velos.eres.business.studyNotify.impl;

/**
 * @ejbHomeJNDIname ejb.StudyNotify
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The StudyNotify BMP entity bean.<br>
 * <br>
 * 
 * @author Sajal
 */
@Entity
@Table(name = "er_studynotify")
public class StudyNotifyBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 116673881059493097L;

    /**
     * the studyNotify Id
     */
    public int studyNotifyId;

    /**
     * the Therapeutic area from codelist
     */
    public Integer studyNotifyCodelstTarea;

    /**
     * the studyNotify User Id
     */
    public Integer studyNotifyUserId;

    /**
     * the studyNotify type
     */
    public String studyNotifyType;

    /**
     * the studyNotify keywords
     */
    public String studyNotifyKeywords;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYNOTIFY", allocationSize=1)
    @Column(name = "PK_STUDYNOTIFY")
    public int getStudyNotifyId() {
        return this.studyNotifyId;
    }

    public void setStudyNotifyId(int id) {
        this.studyNotifyId = id;
    }

    @Column(name = "FK_CODELST_TAREA")
    public String getStudyNotifyCodelstTarea() {
        return StringUtil.integerToString(this.studyNotifyCodelstTarea);
    }

    public void setStudyNotifyCodelstTarea(String studyNotifyCodelstTarea) {
        if (studyNotifyCodelstTarea != null) {
            this.studyNotifyCodelstTarea = Integer
                    .valueOf(studyNotifyCodelstTarea);
        }
    }

    @Column(name = "FK_USER")
    public String getStudyNotifyUserId() {
        return StringUtil.integerToString(this.studyNotifyUserId);
    }

    public void setStudyNotifyUserId(String studyNotifyUserId) {
        if (studyNotifyUserId != null) {
            this.studyNotifyUserId = Integer.valueOf(studyNotifyUserId);
        }
    }

    @Column(name = "STUDYNOTIFY_TYPE")
    public String getStudyNotifyType() {
        return this.studyNotifyType;
    }

    public void setStudyNotifyType(String studyNotifyType) {
        this.studyNotifyType = studyNotifyType;
    }

    @Column(name = "STUDYNOTIFY_KEYWRDS")
    public String getStudyNotifyKeywords() {
        return this.studyNotifyKeywords;
    }

    public void setStudyNotifyKeywords(String studyNotifyKeywords) {
        this.studyNotifyKeywords = studyNotifyKeywords;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    public int updateStudyNotify(StudyNotifyBean snsk) {
        try {
        	setCreator(snsk.getCreator());
            setStudyNotifyCodelstTarea(snsk.getStudyNotifyCodelstTarea());
            setStudyNotifyUserId(snsk.getStudyNotifyUserId());
            setStudyNotifyType(snsk.getStudyNotifyType());
            setStudyNotifyKeywords(snsk.getStudyNotifyKeywords());
            Rlog.debug("studyNotify", "StudyNotifyBean.updateStudyNotify");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyNotify",
                    " error in StudyNotifyBean.updateStudyNotify");
            return -2;
        }
    }

    public StudyNotifyBean() {

    }

    public StudyNotifyBean(int studyNotifyId, String studyNotifyCodelstTarea,
            String studyNotifyUserId, String studyNotifyType,
            String studyNotifyKeywords, String creator, String modifiedBy,
            String ipAdd) {
        // TODO Auto-generated constructor stub
        setStudyNotifyId(studyNotifyId);
        setStudyNotifyCodelstTarea(studyNotifyCodelstTarea);
        setStudyNotifyUserId(studyNotifyUserId);
        setStudyNotifyType(studyNotifyType);
        setStudyNotifyKeywords(studyNotifyKeywords);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
