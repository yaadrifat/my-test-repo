/*
 * Classname : StudyViewFile
 * 
 * Version information: 1.0
 *
 * Copyright notice: Velos, Inc
 * date: 08/11/2001
 *
 * Author: sonia sahni
 *
 */

package com.velos.eres.business.common;

/* import statements */
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import oracle.jdbc.driver.OracleResultSet;
import oracle.sql.BLOB;

import com.velos.eres.service.util.Rlog;

/* import statements */

/**
 * 
 * 
 * @author Sonia Sahni
 * @vesion 1.0
 */

public class StudyViewFile extends CommonDAO implements java.io.Serializable {

    // End of Getter Setter methods

    /**
     * Default Constructor
     * 
     * 
     */

    public StudyViewFile() {
    }

    public boolean saveFile(String filPath, int studyViewPK) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;

        // set code type
        try {
            conn = getConnection();

            conn.setAutoCommit(false);

            // Debug.println("GOT FILE PATH " + filPath);
            Rlog.debug("studyview", "GOT FILE PATH " + filPath);

            File file = new File(filPath);
            InputStream istr = new FileInputStream(file);

            // Debug.println("FILE OBJ CREATED" + file);
            Rlog.debug("studyview", "FILE OBJ CREATED" + file);

            // insert empty blob for the first time
            pstmt = conn
                    .prepareStatement("Update ER_STUDYVIEW set  STUDYVIEW = empty_blob() where PK_STUDYVIEW = ?");

            // pstmt.setBinaryStream (1, istr, (int)file.length ());

            // pstmt.setInt (1,filekb );

            pstmt.setInt(1, studyViewPK);
            pstmt.execute();

            pstmt.close();

            // get the empty blob from the row saved above

            Statement b_stmt = conn.createStatement();
            ResultSet savedBlob = b_stmt
                    .executeQuery("SELECT STUDYVIEW from ER_STUDYVIEW WHERE PK_STUDYVIEW  = "
                            + studyViewPK + " FOR UPDATE");

            // Retrieve BLOB streams and load the files

            if (savedBlob.next()) {

                // Debug.println("got blobrow");
                Rlog.debug("studyview", "got blobrow");
                // Get the BLOB locator and open output stream for the BLOB
                BLOB filBLOB = ((OracleResultSet) savedBlob).getBLOB(1);
                OutputStream l_blobOutputStream = filBLOB
                        .getBinaryOutputStream();

                byte[] l_buffer = new byte[10 * 1024];

                int l_nread = 0; // Number of bytes read
                while ((l_nread = istr.read(l_buffer)) != -1)
                    // Read from file
                    l_blobOutputStream.write(l_buffer, 0, l_nread); // Write to
                // BLOB

                // Close both streams
                istr.close();
                l_blobOutputStream.close();
            }
            conn.commit();
            savedBlob.close();
            b_stmt.close();
            // ////

            // delete the temporary file that was created on the server
            // Debug.println("Trying to delete temp file");
            if (file.delete()) {
                // Debug.println("Temporary File Deleted");
                Rlog.debug("studyview", "Temporary File Deleted");
            }

            return true;
        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN SAVING FILE" + ex);
            Rlog.fatal("studyview", "EXCEPTION IN SAVING FILE" + ex);
            return false;
        } finally {
            // try{
            // if (pstmt != null) pstmt.close();
            // }catch (Exception e) { }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    public String readFile(String path, int studyViewPK) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sb = new StringBuffer();
        BufferedInputStream bInput;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("Select STUDYVIEW from ER_STUDYVIEW where PK_STUDYVIEW = ? ");

            pstmt.setInt(1, studyViewPK);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                InputStream dbFile = (rs.getBlob(1)).getBinaryStream();

                bInput = new BufferedInputStream(dbFile);
                int streamValue = 0;
                int streamSize = 0;
                while ((streamValue = bInput.read()) != -1) {
                    streamSize++;
                }

                byte bFile[] = new byte[streamSize];
                ResultSet rs1 = pstmt.executeQuery();

                if (rs1.next()) {

                    InputStream dbFile1 = (rs.getBlob(1)).getBinaryStream();
                    BufferedInputStream bInput1 = new BufferedInputStream(
                            dbFile1);

                    int sajal = bInput1.read(bFile, 0, streamSize);
                    sb.append(new String(bFile));
                    bFile = null;
                }
            }

            return sb.toString();

        } catch (Exception ex) {
            return null;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

}
