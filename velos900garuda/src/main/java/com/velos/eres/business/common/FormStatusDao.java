/*
 * Classname			FormStatusDao
 *
 * Version information 	1.0
 *
 * Date					08/21/2003
 *
 * Copyright notice		Velos, Inc.
 *
 * Author 				Salil
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * FormStatusDao for getting 'Status of Forms'
 *
 * @author Salil
 * @version : 1.0 08/21/2003
 */

public class FormStatusDao extends CommonDAO implements java.io.Serializable {
    private ArrayList formStDate;

    private ArrayList formEndDate;

    private ArrayList formNotes;

    private ArrayList formStatus;

    private int cRows;

    // getter

    public int getCRows() {
        return this.cRows;
    }

    public ArrayList getFormStDate() {
        return this.formStDate;
    }

    public ArrayList getFormEndDate() {
        return this.formEndDate;
    }

    public ArrayList getFormNotes() {
        return this.formNotes;
    }

    public ArrayList getFormStatus() {
        return this.formStatus;
    }

    // / setters

    public void setCRows(int row) {
        this.cRows = row;
    }

    public void setFormStDate(ArrayList formStDate) {
        this.formStDate = formStDate;
    }

    public void setFormEndDate(ArrayList formEndDate) {
        this.formEndDate = formEndDate;
    }

    public void setFormNotes(ArrayList formNotes) {
        this.formNotes = formNotes;
    }

    public void setFormStatus(ArrayList formStatus) {
        this.formStatus = formStatus;
    }

    public void setFormStDate(String formStDate) {
        this.formStDate.add(formStDate);
    }

    public void setFormEndDate(String formEndDate) {
        this.formEndDate.add(formEndDate);
    }

    public void setFormNotes(String formNotes) {
        Rlog.debug("formstat", "In setFormNotes in FormStatusDao " + formNotes);
        this.formNotes.add(formNotes);
        Rlog.debug("formstat", "In setFormNotes in FormStatusDao after adding"
                + formNotes);
    }

    public void setFormStatus(String formStatus) {
        this.formStatus.add(formStatus);
    }

    // end of setters and getters

    // constructor

    public FormStatusDao() {
        Rlog.debug("formstat", "In constructor begining");
        formStDate = new ArrayList();
        formEndDate = new ArrayList();
        formNotes = new ArrayList();
        formStatus = new ArrayList();
        Rlog.debug("formstat", "In constructor end");
    }

    public void getFormStatusHistory(int formId) {

        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
            		// Query modified by Gopu to fix the bugzilla issue #2592
 
            		.prepareStatement("select (select codelst_desc from er_codelst where pk_codelst=fk_codelst_stat) as form_status,pk_formstat,to_char(formstat_stdate,PKG_DATEUTIL.F_GET_DATEFORMAT) as formstat_startdate,to_char(formstat_enddate,PKG_DATEUTIL.F_GET_DATEFORMAT) as formstat_enddate,formstat_notes from er_formstat where fk_formlib=? order by pk_formstat desc");
            pstmt.setInt(1, formId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                rows++;
                setFormNotes(rs.getString("formstat_notes"));
                setFormStatus(rs.getString("form_status"));
                setFormStDate(rs.getString("formstat_startdate"));
                setFormEndDate(rs.getString("formstat_enddate"));
            }

            setCRows(rows);

        } catch (SQLException ex) {

            Rlog.fatal("formstat",
                    "EXCEPTION IN getFormStatusHistory in FormStatDao " + ex);

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}