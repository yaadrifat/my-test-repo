/*
 * Classname			FormField.class
 * 
 * Version information 	1.0
 *
 * Date					24/07/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * FormFieldDao for getting FormField records
 * 
 * @author Sonia Kaura
 * @version : 1.0 05/07/2003
 */

public class FormFieldDao extends CommonDAO implements java.io.Serializable {

    private ArrayList formFldId;

    private ArrayList formFldMandatory;

    private ArrayList formFldBrowserFlg;

    private ArrayList formFldSeq;

    private ArrayList formSecId;;

    private int rows; // to get the number of rows of the incoming result set

    public void FormFieldDao() {

        formFldId = new ArrayList();
        formFldMandatory = new ArrayList();
        ;
        formFldBrowserFlg = new ArrayList();
        ;
        formFldSeq = new ArrayList();
        ;
        formSecId = new ArrayList();
        ;

    }

    // /////////////////////////////////////
    // SETTERS AND GETTERS

    public ArrayList getFormFldId() {
        return this.formFldId;
    }

    public void setFormFldId(ArrayList formFldId) {
        this.formFldId = formFldId;
    }

    public ArrayList getFormFldMandatory() {
        return this.formFldMandatory;
    }

    public void setFormFldMandatory(ArrayList formFldMandatory) {
        this.formFldMandatory = formFldMandatory;
    }

    public ArrayList getFormFldBrowserFlg() {
        return this.formFldBrowserFlg;
    }

    public void setFormFldBrowserFlg(ArrayList formFldBrowserFlg) {
        this.formFldBrowserFlg = formFldBrowserFlg;
    }

    public ArrayList getFormFldSeq() {
        return this.formFldSeq;
    }

    public void setFormFldSeq(ArrayList formFldSeq) {
        this.formFldSeq = formFldSeq;
    }

    public int getRows() {
        return this.rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public ArrayList getFormSecId() {
        return this.formSecId;
    }

    public void setFormSecId(ArrayList formSecId) {
        this.formSecId = formSecId;
    }

    // // // // // /
    // ////////////////////////////////////////////////////////////////////

    // END OF SETTERS AND GETTERS

    // //////////////////////////////////////////////////////////
    // SINGLE ELEMENT GETTERS AND SETTERS

    public Object getFormFldId(int i) {
        return this.formFldId.get(i);
    }

    public void setFormFldId(Integer formFldId) {
        this.formFldId.add(formFldId);
    }

    public Object getFormFldMandatory(int i) {
        return this.formFldMandatory.get(i);
    }

    public void setFormFldMandatory(String formFldMandatory) {
        this.formFldMandatory.add(formFldMandatory);
    }

    public Object getFormFldBrowserFlg(int i) {
        return this.formFldBrowserFlg.get(i);
    }

    public void setFormFldBrowserFlg(String formFldBrowserFlg) {
        this.formFldBrowserFlg.add(formFldBrowserFlg);
    }

    public Object getFormFldSeq(int i) {
        return this.formFldSeq.get(i);
    }

    public void setFormFldSeq(String formFldSeq) {
        this.formFldSeq.add(formFldSeq);
    }

    public Object getFormSecId(int i) {
        return this.formSecId.get(i);
    }

    public void setFormSecId(String formSecId) {
        this.formSecId.add(formSecId);
    }

    // /////////////////////////////////////////////////////////////////////

    public void getMandatoryAndSequenceOfField(int fldId, int formSecId) {

        int rows = 0;

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("SELECT  PK_FORMFLD, FK_FORMSEC  , FORMFLD_MANDATORY , FORMFLD_SEQ  "
                            + " FORMFLD_BROWSERFLG FROM ER_FORMFLD "
                            + " WHERE FK_FIELD = ? AND  FK_FORMSEC= ? "
                            + " AND RECORD_TYPE <> 'D'");

            pstmt.setInt(1, fldId);
            pstmt.setInt(2, formSecId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setFormFldId(new Integer(rs.getInt("PK_FORMFLD")));
                setFormSecId((new Integer(rs.getInt("FK_FORMSEC"))).toString());
                setFormFldMandatory((new Integer(rs.getInt("FORMFLD_MANDATORY")))
                        .toString());
                setFormFldSeq((new Integer(rs.getInt("FORMFLD_SEQ")))
                        .toString());
                setFormFldBrowserFlg((new Integer(rs
                        .getInt("FORMFLD_BROWSERFLG"))).toString());
                rows++;
                Rlog.debug("formfield",
                        "FormFldDao.getMandatoryAndSequenceOfField rows "
                                + rows);
            }

            setRows(rows);

        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "formfield",
                            "FormFldDao.getMandatoryAndSequenceOfField EXCEPTION IN FETCHING FROM ER_FORMFLD TABLE"
                                    + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public int repeatLookupField(int pkField, int formField, int section) {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            Rlog.debug("fieldlib", "In repeatLookupField() ");

            cstmt = conn
                    .prepareCall("{call PKG_LOOKUP.SP_REPEAT_LOOKUP(?,?,?,?)}");

            cstmt.setInt(1, pkField);
            cstmt.setInt(2, formField);
            cstmt.setInt(3, section);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(4);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("fieldlib",
                    "EXCEPTION in repeatLookupField, excecuting Stored Procedure "
                            + e);
            return -1;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

}
