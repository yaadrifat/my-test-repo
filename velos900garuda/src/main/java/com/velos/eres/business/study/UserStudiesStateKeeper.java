//sonia sahni  : dt: 03/15/2001

package com.velos.eres.business.study;

import java.util.*;
import com.velos.eres.service.util.*;

/**
 * GrpRightsStateKeeper class resulting from implementation of the StateHolder
 * pattern
 */

public class UserStudiesStateKeeper implements java.io.Serializable {

    /**
     * Array List of StudyIds
     */
    private ArrayList uStudies;

    /**
     * the Array List of Study Titles
     */
    private ArrayList uTitles;

    /**
     * the Array List of Study Numbers
     */
    private ArrayList uStudyNumbers;

    /**
     * Default constructor of Group Rights State Keeper
     * 
     */
    public UserStudiesStateKeeper() {
        uStudies = new ArrayList();
        uTitles = new ArrayList();
        uStudyNumbers = new ArrayList();

    }

    /**
     * Sets the Values in User Study Rights State Keeper
     * 
     */
    public UserStudiesStateKeeper(ArrayList uStudies, ArrayList uTitles,
            ArrayList uStudyNumbers) {
        Rlog.debug("study", "USER STUDIES STATEKEEPER Start Constructor ");
        setUStudies(uStudies);
        setUTitles(uTitles);
        setUStudyNumbers(uStudyNumbers);
        Rlog.debug("study", "USER STUDIES STATEKEEPER End of Constructor ");
    }

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return
     */

    public ArrayList getUStudies() {
        return this.uStudies;
    }

    public void setUStudies(ArrayList uStudies) {
        this.uStudies = uStudies;
    }

    public ArrayList getUTitles() {
        return this.uTitles;
    }

    public void setUTitles(ArrayList uTitles) {
        this.uTitles = uTitles;
    }

    public ArrayList getUStudyNumbers() {
        return this.uStudyNumbers;
    }

    public void setUStudyNumbers(ArrayList uStudyNumber) {
        this.uStudyNumbers = uStudyNumber;
    }

    public void setUStudyNumbers(String uStudyNumber) {
        Rlog.debug("study", "Setting the Title in State Keeper" + uStudyNumber);
        try {
            this.uStudyNumbers.add(uStudyNumber);
        } catch (Exception e) {
            Rlog.fatal("study", "EXCEPTION IN String Value Set"
                    + this.uStudyNumbers + "*" + e);
        }
        Rlog.debug("study", "String Value Set" + this.uStudyNumbers);

    }

    public void setUTitles(String uTitle) {
        Rlog.debug("study", "Setting the Title in State Keeper" + uTitle);
        try {
            this.uTitles.add(uTitle);
        } catch (Exception e) {
            Rlog.fatal("study", "EXCEPTION IN String Value Set" + this.uTitles
                    + "*" + e);
        }
        Rlog.debug("study", "String Value Set" + this.uTitles);

    }

    public void setUStudies(int uStudy) {
        Rlog.debug("study", "Setting the Study in State Keeper" + uStudy);
        try {
            this.uStudies.add(String.valueOf(uStudy));
        } catch (Exception e) {
            Rlog.fatal("study", "EXCEPTION IN String Value Set" + this.uStudies
                    + "*" + e);
        }
        Rlog.debug("study", "String Value Set" + this.uStudies);

    }

    public void setUserStudyDetails(int uStudy, String uTitle,
            String uStudyNumber) {
        setUStudies(uStudy);
        setUTitles(uTitle);
        setUStudyNumbers(uStudyNumber);

    }

    // END OF GETTER SETTER METHODS

}
