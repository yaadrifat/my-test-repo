/*
 * Classname			StorageKitDao.class
 *
 * Version information 	1.0
 *
 * Date					08/21/2009
 *
 * Copyright notice		Velos, Inc.
 *
 * Author 				Jnanamay Majumdar
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;


/**
 * StorageKitDao for getting Storage kit records
 *
 * @author    Jnanamay Majumdar
 * @version : 1.0 08/21/2009
 */


public class StorageKitDao extends CommonDAO implements java.io.Serializable {

	private ArrayList pkStorageKits;
	private ArrayList fkStorages;
	private ArrayList defSpecimenTypes;
	private ArrayList defProcTypes;
	private ArrayList defProcSeqs;
	private ArrayList defSpecQuants;
	private ArrayList defSpecQuantUnits;
	private ArrayList kitSpecDispositions;
	private ArrayList kitSpecEnvtCons;


	public StorageKitDao() {

    	pkStorageKits = new ArrayList();
    	fkStorages = new ArrayList();
    	defSpecimenTypes = new ArrayList();
    	defProcTypes = new ArrayList();
    	defProcSeqs = new ArrayList();
    	defSpecQuants = new ArrayList();
    	defSpecQuantUnits = new ArrayList();
    	kitSpecDispositions = new ArrayList();
    	kitSpecEnvtCons = new ArrayList();
    }


	//getter and setter methods
	public ArrayList getPkStorageKits() {
		return pkStorageKits;
	}

	public void setPkStorageKits(ArrayList pkStorageKits) {
		this.pkStorageKits = pkStorageKits;
	}

	public void setPkStorageKits(Integer pkStorageKits) {
	        this.pkStorageKits.add(pkStorageKits);
	}


	public ArrayList getFkStorages() {
		return fkStorages;
	}
	public void setFkStorages(ArrayList fkStorages) {
		this.fkStorages = fkStorages;
	}
	public void setFkStorages(Integer fkStorages) {
        this.fkStorages.add(fkStorages);
	}


	public ArrayList getDefSpecimenTypes() {
		return defSpecimenTypes;
	}
	public void setDefSpecimenTypes(ArrayList defSpecimenTypes) {
		this.defSpecimenTypes = defSpecimenTypes;
	}
	public void setDefSpecimenTypes(Integer defSpecimenTypes) {
        this.defSpecimenTypes.add(defSpecimenTypes);
	}

	public ArrayList getDefProcTypes() {
		return defProcTypes;
	}
	public void setDefProcTypes(ArrayList defProcTypes) {
		this.defProcTypes = defProcTypes;
	}
	public void setDefProcTypes(String defProcTypes) {
		this.defProcTypes.add(defProcTypes);
	}


	public ArrayList getDefProcSeqs() {
		return defProcSeqs;
	}
	public void setDefProcSeqs(ArrayList defProcSeqs) {
		this.defProcSeqs = defProcSeqs;
	}
	public void setDefProcSeqs(String defProcSeqs) {
		this.defProcSeqs.add(defProcSeqs);
	}


	public ArrayList getDefSpecQuants() {
		return defSpecQuants;
	}
	public void setDefSpecQuants(ArrayList defSpecQuants) {
		this.defSpecQuants = defSpecQuants;
	}
	public void setDefSpecQuants(Float defSpecQuants) {
        this.defSpecQuants.add(defSpecQuants);
	}


	public ArrayList getDefSpecQuantUnits() {
		return defSpecQuantUnits;
	}
	public void setDefSpecQuantUnits(ArrayList defSpecQuantUnits) {
		this.defSpecQuantUnits = defSpecQuantUnits;
	}
	public void setDefSpecQuantUnits(Integer defSpecQuantUnits) {
        this.defSpecQuantUnits.add(defSpecQuantUnits);
	}


	public ArrayList getKitSpecDispositions() {
		return kitSpecDispositions;
	}
	public void setKitSpecDispositions(ArrayList kitSpecDispositions) {
		this.kitSpecDispositions = kitSpecDispositions;
	}
	public void setKitSpecDispositions(Integer kitSpecDispositions) {
        this.kitSpecDispositions.add(kitSpecDispositions);
	}

	public ArrayList getKitSpecEnvtCons() {
		return kitSpecEnvtCons;
	}
	public void setKitSpecEnvtCons(ArrayList kitSpecEnvtCons) {
		this.kitSpecEnvtCons = kitSpecEnvtCons;
	}
	public void setKitSpecEnvtCons(String kitSpecEnvtCons) {
		this.kitSpecEnvtCons.add(kitSpecEnvtCons);
	}



//end of getter and setter methods


	public void getStorageKitAttributes(int pkStorageKit) {

		PreparedStatement pstmt = null;
	    Connection conn = null;
	    try {
	        conn = getConnection();
	        pstmt = conn.prepareStatement("select PK_STORAGE_KIT, FK_STORAGE, DEF_SPECIMEN_TYPE, DEF_PROCESSING_TYPE," +
	        		" DEF_SPEC_PROCESSING_SEQ, DEF_SPEC_QUANTITY, DEF_SPEC_QUANTITY_UNIT," +
	        		" KIT_SPEC_DISPOSITION, KIT_SPEC_ENVT_CONS " +
	        		" from er_storage_kit where  fk_storage=?");



	        pstmt.setInt(1, pkStorageKit);
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next()) {


	        	setPkStorageKits(new Integer(rs.getInt("PK_STORAGE_KIT")));
	        	setFkStorages(new Integer(rs.getInt("FK_STORAGE")));
	        	setDefSpecimenTypes(new Integer(rs.getInt("DEF_SPECIMEN_TYPE")));
	        	setDefProcTypes(rs.getString("DEF_PROCESSING_TYPE"));
	        	setDefProcSeqs(rs.getString("DEF_SPEC_PROCESSING_SEQ"));
	        	setDefSpecQuants(new Float(rs.getFloat("DEF_SPEC_QUANTITY")));
	        	setDefSpecQuantUnits(new Integer(rs.getInt("DEF_SPEC_QUANTITY_UNIT")));
	        	setKitSpecDispositions(new Integer(rs.getInt("KIT_SPEC_DISPOSITION")));
	        	setKitSpecEnvtCons(rs.getString("KIT_SPEC_ENVT_CONS"));


	        }

	   } catch (SQLException ex) {
	        Rlog.fatal("storagekit",
	                "StorageKitDao.getStorageKitAttributes EXCEPTION IN FETCHING FROM storage table"
	                        + ex);
	    } finally {
	        try {
	            if (pstmt != null)
	                pstmt.close();
	        } catch (Exception e) {
	        }
	        try {
	            if (conn != null)
	                conn.close();
	        } catch (Exception e) {
	        }

	    }

	}



	//end of class
}
