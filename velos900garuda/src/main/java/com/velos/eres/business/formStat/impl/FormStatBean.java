/**
 * This is the FormStatHome for the FormStatBean
 * @author Salil
 * Date 07/29/2003
 **/

package com.velos.eres.business.formStat.impl;

/* Import Statements */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

@Entity
@Table(name = "er_formstat")
public class FormStatBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3257568403752302133L;

    // These are Data Members
    // this is the primary key
    public int formStatId;

    // this is the reference to the form library
    public Integer formLib;

    // this is the refernce to the code list table
    public Integer codeLst;

    // This is s a refernce to the person changing the status
    public Integer changedBy;

    // This is the start date of the changed status
    public java.util.Date stDate;

    // This is the end date of the status
    public java.util.Date endDate;

    // These are the notes about the change in form status
    public String notes;

    // This is to Store the type of Record New /Modified /Deleted
    public String recordType;

    // This is the refernce to the creator
    public Integer creator;

    // This is the refernce to the person who last modified the form status
    public Integer lastModifiedBy;

    // This is the ip address of the machine that changed the form status
    public String ipAdd;

    // these are the getter functions

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name="SEQ_GEN", sequenceName="SEQ_ER_FORMSTAT", allocationSize=1)
    @Column(name = "PK_FORMSTAT")
    public int getFormStatId() {
        return formStatId;
    }

    public void setFormStatId(int formStatId) {
        this.formStatId = formStatId;
    }

    @Column(name = "FK_FORMLIB")
    public String getFormLib() {
        return StringUtil.integerToString(formLib);
    }

    public void setFormLib(String formLib) {
        if (formLib.length() > 0) {
            this.formLib = Integer.valueOf(formLib);
        }

    }

    @Column(name = "FK_CODELST_STAT")
    public String getCodeLst() {
        return StringUtil.integerToString(this.codeLst);
    }

    public void setCodeLst(String codeLst) {
        if (codeLst.length() > 0) {
            this.codeLst = Integer.valueOf(codeLst);
        }

    }

    @Column(name = "FORMSTAT_CHANGEDBY")
    public String getChangedBy() {
        return StringUtil.integerToString(this.changedBy);
    }

    public void setChangedBy(String changedBy) {
        String changedB = changedBy.trim();
        if (changedB.length() > 0) {
            this.changedBy = Integer.valueOf(changedB);
        }

    }

    @Transient
    public String getStDate() {
        String stDate;

        stDate = "";

        if (this.stDate != null) {
            stDate = DateUtil.dateToString(getStDatePersistent());

        }

        return stDate;
    }

    public void setStDate(String stDate) {

        if (!StringUtil.isEmpty(stDate)) {
            setStDatePersistent(DateUtil.stringToDate(stDate, null));
        }

    }

    @Column(name = "FORMSTAT_STDATE")
    public Date getStDatePersistent() {
        return this.stDate;
    }

    public void setStDatePersistent(Date stDate) {
        this.stDate = stDate;
    }

    @Transient
    public String getEndDate() {
        String endDate;

        endDate = "";

        if (this.endDate != null) {
            endDate = DateUtil.dateToString(getEndDatePersistent());

        }

        return endDate;
    }

    public void setEndDate(String endDate) {
        DateUtil eDate = null;
        if (!StringUtil.isEmpty(endDate)) {
            setEndDatePersistent(DateUtil.stringToDate(endDate, null));
        } else
            setEndDatePersistent(null);

    }

    @Column(name = "FORMSTAT_ENDDATE")
    public Date getEndDatePersistent() {
        return endDate;
    }

    public void setEndDatePersistent(Date endDate) {

        this.endDate = endDate;

    }

    @Column(name = "FORMSTAT_NOTES")
    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "RECORD_TYPE")
    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator.length() > 0) {
            this.creator = Integer.valueOf(creator);
        }

    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return StringUtil.integerToString(this.lastModifiedBy);
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        if (!StringUtil.isEmpty(lastModifiedBy)) {
            this.lastModifiedBy = Integer.valueOf(lastModifiedBy);
        } else
            this.lastModifiedBy = null;

    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        if (ipAdd.length() > 0) {
            this.ipAdd = ipAdd;
        }

    }

    /*
     * public FormStatStateKeeper getFormStatDetails() { return new
     * FormStatStateKeeper(getFormStatId(), getFormLib(), getCodeLst(),
     * getChangedBy(), getRecordType(), getStDate(), getEndDate(), getNotes(),
     * getCreator(), getLastModifiedBy(), getIpAdd()); }
     */

    public int updateFormStatDetails(FormStatBean fssk) {
        try {
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean");
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean formstatId"
                            + fssk.getFormStatId());
            setFormStatId(fssk.getFormStatId());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean formstatId"
                            + getFormStatId());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean formlibId"
                            + fssk.getFormLib());
            setFormLib(fssk.getFormLib());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean formstatId"
                            + getFormStatId());
            setCodeLst(fssk.getCodeLst());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean codeLst"
                            + fssk.getCodeLst());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean codeLst"
                            + getCodeLst());
            setChangedBy(fssk.getChangedBy());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean changedby"
                            + fssk.getChangedBy());
            setIpAdd(fssk.getIpAdd());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean ipadd"
                            + fssk.getIpAdd());
            setNotes(fssk.getNotes());
            setRecordType(fssk.getRecordType());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean StDAte"
                            + fssk.getStDate());
            setStDate(fssk.getStDate());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean enddate"
                            + fssk.getEndDate());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean endDate"
                            + getEndDate());
            setEndDate(fssk.getEndDate());
            Rlog.debug("formstat",
                    "Inside updateFormStatDetails in FormStatBean codeLst"
                            + fssk.getCreator());
            setCreator(fssk.getCreator());
            //KM-#3634
            setLastModifiedBy(fssk.getLastModifiedBy());
            return 0;
        } catch (Exception e) {

            Rlog.fatal("formstat",
                    "Exception in updating form status in updateFormStatDetails in FormStatBean"
                            + e);
            return -2;
        }
    }

    public FormStatBean() {
    }

    public FormStatBean(int formStatId, String formLib, String codeLst,
            String changedBy, String stDate, String endDate, String notes,
            String recordType, String creator, String lastModifiedBy,
            String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setFormStatId(formStatId);
        setFormLib(formLib);
        setCodeLst(codeLst);
        setChangedBy(changedBy);
        setStDate(stDate);
        setEndDate(endDate);
        setNotes(notes);
        setRecordType(recordType);
        setCreator(creator);
        setLastModifiedBy(lastModifiedBy);
        setIpAdd(ipAdd);
    }

    /*
     * public void setFormStatStateKeeper(FormStatStateKeeper fssk) { GenerateId
     * genId = null;
     *
     * try { Connection conn = null; conn = getConnection();
     * Rlog.debug("formStat", "Connection :" + conn); setFormStatId =
     * genId.getId("SEQ_ER_FORMSTAT", conn); conn.close();
     * Rlog.debug("formStat", "notes :" + fssk.getNotes());
     * setNotes(fssk.getNotes()); Rlog.debug("formStat", "notes :" +
     * getNotes()); Rlog.debug("formStat", "stdate :" + fssk.getNotes());
     * setStDate(fssk.getStDate()); Rlog.debug("formStat", "stdate :" +
     * fssk.getStDate()); Rlog.debug("formStat", "end date :" +
     * fssk.getEndDate()); setEndDate(fssk.getEndDate()); Rlog.debug("formStat",
     * "end date :" + fssk.getEndDate()); Rlog.debug("formStat", "codelst :" +
     * fssk.getCodeLst()); setCodeLst(fssk.getCodeLst()); Rlog.debug("formStat",
     * "codelst :" + getCodeLst()); setChangedBy(fssk.getChangedBy());
     * setCreator(fssk.getCreator()); setIpAdd(fssk.getIpAdd());
     * Rlog.debug("formStat", "ipadd :" + fssk.getIpAdd());
     * Rlog.debug("formStat", "form lib id :" + fssk.getFormLib());
     * setFormLib(fssk.getFormLib()); Rlog.debug("formStat", "form lib id :" +
     * getFormLib()); setRecordType(fssk.getRecordType()); } catch (Exception e) {
     *
     * Rlog.fatal("formstat", "Exception in setting form status in
     * setFormStatStateKeeper in FormStatBean" + e); } }
     */
}

// end of class
