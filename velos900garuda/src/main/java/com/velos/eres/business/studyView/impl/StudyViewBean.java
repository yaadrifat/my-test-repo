/*
 * Classname			studyViewBean.class
 * 
 * Version information
 *
 * Date					08/11/2001
 * 
 * Copyright notice Aithent
 */

package com.velos.eres.business.studyView.impl;

/**
 * 
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "er_studyview")
public class StudyViewBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3258416140152616504L;

    /**
     * the StudyView Id
     */
    public int id;

    /**
     * studyView STudy User
     */
    public Integer stdUser;

    /**
     * Study Id
     */
    public Integer stdStudy;

    /**
     * studyView study Number
     */

    public String stdStudyNum;

    /**
     * studyView study Title
     */

    public String stdTitle;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    ;

    // GETTER SETTER METHODS
    public StudyViewBean() {

    }

    public StudyViewBean(int id, String stdUser, String stdStudy,
            String stdStudyNum, String stdTitle, String creator,
            String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setStdUser(stdUser);
        setStdStudy(stdStudy);
        setStdStudyNum(stdStudyNum);
        setStdTitle(stdTitle);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYVIEW", allocationSize=1)
    @Column(name = "PK_STUDYVIEW")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "FK_STUDY")
    public String getStdStudy() {
        return StringUtil.integerToString(this.stdStudy);
    }

    public void setStdStudy(String stdStudy) {
        if (stdStudy != null) {
            this.stdStudy = Integer.valueOf(stdStudy);
        }
    }

    @Column(name = "STUDYVIEW_NUM")
    public String getStdStudyNum() {
        return this.stdStudyNum;
    }

    public void setStdStudyNum(String stdStudyNum) {
        this.stdStudyNum = stdStudyNum;
    }

    @Column(name = "STUDYVIEW_TITLE")
    public String getStdTitle() {
        return this.stdTitle;
    }

    public void setStdTitle(String stdTitle) {
        this.stdTitle = stdTitle;
    }

    @Column(name = "FK_USER")
    public String getStdUser() {
        return StringUtil.integerToString(this.stdUser);
    }

    public void setStdUser(String stdUser) {
        if (stdUser != null)
            this.stdUser = Integer.valueOf(stdUser);

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * sets up a state holder containing details of the study studyView
     */

    public int updateStudyView(StudyViewBean ask) {
        try {

            setStdStudy(ask.getStdStudy());
            setStdUser(ask.getStdUser());
            setStdStudyNum(ask.getStdStudyNum());
            setStdTitle(ask.getStdTitle());
            setCreator(ask.getCreator());
            setModifiedBy(ask.getModifiedBy());
            setIpAdd(ask.getIpAdd());
            Rlog.debug("studyView", "StudyViewBean.updateStudyView");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyView", " error in StudyViewBean.updateStudyView");
            return -2;
        }
    }

}// end of class
