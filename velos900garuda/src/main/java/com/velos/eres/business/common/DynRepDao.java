 //Vishal dt 03/29/2001

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.GenerateId;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
//import com.velos.eres.web.dynrep.DynRepJB;
//import com.velos.eres.web.dynrep.holder.FieldContainer;
//import com.velos.eres.web.dynrep.holder.FilterContainer;
//import com.velos.eres.web.dynrep.holder.ReportHolder;
//import com.velos.eres.web.dynrep.holder.SortContainer;
//import com.velos.eres.web.dynrep.holder.SummaryHolder;
//import com.velos.eres.web.linkedForms.LinkedFormsJB;

/**
 * Description of the Class
 *
 * @author vabrol
 * @created October 25, 2004
 */
/*
 * Modified by Sonia Abrol on 01/11/05
 *
 * 1. Added a new method resetFilterDetails() 2. Modified getFilter() for
 * multiform 3. Modified fillReportContainer() to populate filters and filter
 * details,sort arrays, field info
 */
/*
 * Modified by Sonia Abrol on 01/24/05
 *
 * 3. Changes for Core Table Lookups in AdHoc Queries
 */
/*
 * Modified by Sonia Abrol, 01/31/05, added mapform column - mp_uid for field
 * unique ids
 */
/*
 * Modified by Sonia Abrol, 02/09/05 to add the form names in same order as
 * formId string
 */
/*
 * Modified by Sonia Abrol, 04/14/05, added new attributes, repUseUniqueIds,
 * repUseDataValues, repUseUniqueId, repUseDataValue
 */
public class DynRepDao extends CommonDAO implements java.io.Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3978707276888289584L;

    private ArrayList mapCols;

    private ArrayList mapOrigIds;

    private ArrayList mapDispNames;

    private ArrayList mapFormTypes;

    private ArrayList mapColSecIds;

    private ArrayList mapColSecNames;

    private ArrayList mapColIds;

    private ArrayList mapColLens;

    private ArrayList mapColSecRepNo;

    private ArrayList mapColTypes;

    private ArrayList mapColTypesNoRepeatFields;

    /** keyword defined for a core lookup field. Arraylist will not be populatd for forms*/
    private ArrayList fieldKeyword;

    /**
     * the system id of the field. will be used to identify if the field is
     * regular or repeat field
     */
    private ArrayList mapSysIds;

    /** ArrayList of field unique ids */
    private ArrayList mapUids;

    /** The table name associated with the query's PK column. */
    private String mapTableName;

    /** The query's PK column. */
    private String mapPrimaryKeyCol;

    /** The query's Filter column. */
    private String mapFilterCol;

    /**
     * The column name for pk_study. A value in this field indicates that the
     * data should be checked for study access rights
     */
    private String mapStudyColumn;

    private ArrayList dataCollection;

    private ArrayList dataList;

    private ArrayList repColId;

    private ArrayList repCol;

    private ArrayList repColSeq;

    private ArrayList repColWidth;

    private ArrayList repColFormat;

    private ArrayList repColDispName;

    private ArrayList repColName;

    private ArrayList repColType;

    private ArrayList repColFormId;

    private ArrayList fltrColIds;

    private ArrayList filtCol;

    private ArrayList filtQf;

    private ArrayList filtData;

    private ArrayList filtExclude;

    private ArrayList filtBbrac;

    private ArrayList filtEbrac;

    private ArrayList filtOper;

    private ArrayList filtSeq;

    private String filtName;

    private String filtId;

    private String fltrDtId;

    private String usr;

    private String ipAdd;

    private String repId;

    private HashMap attributes;

    private HashMap FldAttr;

    private HashMap SortAttr;

    private HashMap TypeAttr;

    private HashMap RepAttr;

    private HashMap FltrAttr;

    private ArrayList fltrIds;

    private ArrayList fltrNames;

    private ArrayList fltrStrs;

    private String fltrStr;

    private String fkForm;

    /**
     * the type of date range applied on the filter. Possible values: A:All,
     * M:Month,Y:Year,DR:given range, PS:patient study status
     */
    private ArrayList flrDateRangeType;

    /**
     * the 'from' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    private ArrayList fltrDateRangeFrom;

    /**
     * the 'to' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    private ArrayList fltrDateRangeTo;

    // Added for multiform
    //private ReportHolder reportContainer;

    private ArrayList formIdList;

    private ArrayList formNameList;

    // Added to hold filter formIds, introduced with multiform, using vector
    // because we need to search
    // on elements using indexOf(startpos,endpos)

    private Vector fltrFormIds;

    private Vector fltrParentFilterIds;

    /** For form types for filters */
    private ArrayList filterFormTypes;

    /**
     * Flag for the report, to Use Unique Field ID as Display Name. However,
     * user will have an option to have a different setting for each field.
     * possible values: 0:true,1:false
     */
    private String repUseUniqueId;

    /**
     * Flag for the report, to Display Data value of multiple choice fields.
     * However, user will have an option to have a different setting for each
     * field. possible values: 0:true,1:false (default, display value will be
     * shown)
     */
    private String repUseDataValue;

    /**
     * For a field, Flag to Use 'Unique Field ID' as Display Name. possible
     * values: 0:true,1:false
     */
    private ArrayList useUniqueIds;

    /**
     * For a field, Flag to Display Data value of multiple choice fields.
     * possible values: 0:true,1:false (default, display value will be shown)
     */

    private ArrayList useDataValues;

    private String ignoreFilters;

    /**
     *Flag to calculate summary information for this field - min/max/median/avg. Possible values-0:do not calculate, 1:calculate
     */
    private ArrayList calcSums;


    /**
     *Flag to calculate count/percentage information. Possible values-0:do not calculate,1:calculate
     */
    private ArrayList calcPers;

    ArrayList fieldRepeatNumber;

    /**
     * Constructor for the DynRepDao object
     */

    /** contains the summary information of report columns (the column that were selected fro summary information)*/
	 //private SummaryHolder repSummaryHolder;

	 private String formLinkedType;

    public DynRepDao() {
        mapCols = new ArrayList();
        mapOrigIds = new ArrayList();
        mapDispNames = new ArrayList();
        mapFormTypes = new ArrayList();
        mapColTypes = new ArrayList();
        mapColSecIds = new ArrayList();
        mapColSecNames = new ArrayList();
        mapColIds = new ArrayList();
        mapColLens = new ArrayList();
        mapUids = new ArrayList();
        mapColTypesNoRepeatFields = new ArrayList();
        mapSysIds = new ArrayList();

        dataCollection = new ArrayList();
        dataList = new ArrayList();
        repColId = new ArrayList();
        repCol = new ArrayList();
        repColSeq = new ArrayList();
        repColWidth = new ArrayList();
        repColFormat = new ArrayList();
        repColDispName = new ArrayList();
        repColName = new ArrayList();
        repColType = new ArrayList();
        repColFormId = new ArrayList();
        fltrColIds = new ArrayList();
        filtCol = new ArrayList();
        filtQf = new ArrayList();
        filtData = new ArrayList();
        filtExclude = new ArrayList();
        filtBbrac = new ArrayList();
        filtEbrac = new ArrayList();
        filtOper = new ArrayList();
        filtSeq = new ArrayList();

        attributes = new HashMap();
        FldAttr = new HashMap();
        FltrAttr = new HashMap();
        RepAttr = new HashMap();
        SortAttr = new HashMap();
        TypeAttr = new HashMap();

        filtName = "";
        filtId = "";
        fltrDtId = "";
        fltrStr = "";
        fkForm = "";
        fltrIds = new ArrayList();
        fltrNames = new ArrayList();
        fltrStrs = new ArrayList();

      //  reportContainer = new ReportHolder();
        formIdList = new ArrayList();
        formNameList = new ArrayList();

        fltrFormIds = new Vector(30);
        fltrParentFilterIds = new Vector(30);

        mapTableName = "";
        mapPrimaryKeyCol = "";
        mapStudyColumn = "";
        filterFormTypes = new ArrayList();
        useUniqueIds = new ArrayList();
        useDataValues = new ArrayList();
        repUseUniqueId = "";
        repUseDataValue = "";

        flrDateRangeType = new ArrayList();
        fltrDateRangeFrom = new ArrayList();
        fltrDateRangeTo = new ArrayList();

        ignoreFilters = "";

        calcSums = new ArrayList();
        calcPers  = new ArrayList();
        //repSummaryHolder = new SummaryHolder();
        mapColSecRepNo = new ArrayList();
        fieldRepeatNumber = new ArrayList();

        formLinkedType="";
        fieldKeyword  = new ArrayList();
    }



  /*  public SummaryHolder getRepSummaryHolder() {
		return repSummaryHolder;
	}



	public void setRepSummaryHolder(SummaryHolder repSummaryHolder) {
		this.repSummaryHolder = repSummaryHolder;
	}*/



	/**
     * Sets the mapCols attribute of the DynRepDao object
     *
     * @param mapCols
     *            The new mapCols value
     */
    public void setMapCols(ArrayList mapCols) {
        this.mapCols = mapCols;
    }

    /**
     * Sets the mapCols attribute of the DynRepDao object
     *
     * @param mapCol
     *            The new mapCols value
     */
    public void setMapCols(String mapCol) {
        mapCols.add(mapCol);
    }

    /**
     * Sets the mapOrigIds attribute of the DynRepDao object
     *
     * @param mapOrigIds
     *            The new mapOrigIds value
     */
    public void setMapOrigIds(ArrayList mapOrigIds) {
        this.mapOrigIds = mapOrigIds;
    }

    /**
     * Sets the mapOrigIds attribute of the DynRepDao object
     *
     * @param mapOrigId
     *            The new mapOrigIds value
     */
    public void setMapOrigIds(String mapOrigId) {
        mapOrigIds.add(mapOrigId);
    }

    /**
     * Sets the mapDispNames attribute of the DynRepDao object
     *
     * @param mapDispNames
     *            The new mapDispNames value
     */
    public void setMapDispNames(ArrayList mapDispNames) {
        this.mapDispNames = mapDispNames;
    }

    /**
     * Sets the mapDispNames attribute of the DynRepDao object
     *
     * @param mapDispName
     *            The new mapDispNames value
     */
    public void setMapDispNames(String mapDispName) {
        mapDispNames.add(mapDispName);
    }

    /**
     * Sets the mapFormTypes attribute of the DynRepDao object
     *
     * @param mapFormTypes
     *            The new mapFormTypes value
     */
    public void setMapFormTypes(ArrayList mapFormTypes) {
        this.mapFormTypes = mapFormTypes;
    }

    /**
     * Sets the mapFormTypes attribute of the DynRepDao object
     *
     * @param mapFormType
     *            The new mapFormTypes value
     */
    public void setMapFormTypes(String mapFormType) {
        this.mapFormTypes.add(mapFormType);
    }

    /**
     * Sets the mapColTypes attribute of the DynRepDao object
     *
     * @param mapColTypes
     *            The new mapColTypes value
     */
    public void setMapColTypes(ArrayList mapColTypes) {
        this.mapColTypes = mapColTypes;
    }

    /**
     * Sets the mapColTypes attribute of the DynRepDao object
     *
     * @param mapColType
     *            The new mapColTypes value
     */
    public void setMapColTypes(String mapColType) {
        this.mapColTypes.add(mapColType);
    }

    /**
     * Sets the mapColSecIds attribute of the DynRepDao object
     *
     * @param mapColSecIds
     *            The new mapColSecIds value
     */
    public void setMapColSecIds(ArrayList mapColSecIds) {
        this.mapColSecIds = mapColSecIds;
    }

    /**
     * Sets the mapColSecIds attribute of the DynRepDao object
     *
     * @param mapColSecId
     *            The new mapColSecIds value
     */
    public void setMapColSecIds(String mapColSecId) {
        this.mapColSecIds.add(mapColSecId);
    }

    /**
     * Sets the mapColSecNames attribute of the DynRepDao object
     *
     * @param mapColSecNames
     *            The new mapColSecNames value
     */
    public void setMapColSecNames(ArrayList mapColSecNames) {
        this.mapColSecNames = mapColSecNames;
    }

    /**
     * Sets the mapColSecNames attribute of the DynRepDao object
     *
     * @param mapColSecName
     *            The new mapColSecNames value
     */
    public void setMapColSecNames(String mapColSecName) {
        this.mapColSecNames.add(mapColSecName);
    }

    /**
     * Gets the mapCols attribute of the DynRepDao object
     *
     * @return The mapCols value
     */
    public ArrayList getMapCols() {
        return mapCols;
    }

    /**
     * Gets the mapOrigIds attribute of the DynRepDao object
     *
     * @return The mapOrigIds value
     */
    public ArrayList getMapOrigIds() {
        return mapOrigIds;
    }

    /**
     * Gets the mapDispNames attribute of the DynRepDao object
     *
     * @return The mapDispNames value
     */
    public ArrayList getMapDispNames() {
        return mapDispNames;
    }

    /**
     * Gets the mapFormTypes attribute of the DynRepDao object
     *
     * @return The mapFormTypes value
     */
    public ArrayList getMapFormTypes() {
        return mapFormTypes;
    }

    /**
     * Gets the mapColTypes attribute of the DynRepDao object
     *
     * @return The mapColTypes value
     */
    public ArrayList getMapColTypes() {
        return mapColTypes;
    }

    /**
     * Gets the mapColSecIds attribute of the DynRepDao object
     *
     * @return The mapColSecIds value
     */
    public ArrayList getMapColSecIds() {
        return mapColSecIds;
    }

    /**
     * Gets the mapColSecNames attribute of the DynRepDao object
     *
     * @return The mapColSecNames value
     */
    public ArrayList getMapColSecNames() {
        return mapColSecNames;
    }

    /*
     * public void setDataCollection(ArrayLit dataCollection) {
     * this.dataCollection =dataCollection; }
     */
    /**
     * Sets the dataCollection attribute of the DynRepDao object
     *
     * @param data
     *            The new dataCollection value
     */
    public void setDataCollection(ArrayList data) {
        this.dataCollection.add(data);
    }

    /**
     * Sets the dataList attribute of the DynRepDao object
     *
     * @param dataList
     *            The new dataList value
     */
    public void setDataList(ArrayList dataList) {
        this.dataList = dataList;
    }

    /**
     * Sets the dataList attribute of the DynRepDao object
     *
     * @param data
     *            The new dataList value
     */
    public void setDataList(String data) {
        this.dataList.add(data);
    }

    /**
     * Description of the Method
     */
    public void resetList() {
        // reset the arraylis to hold next row
        this.dataList = new ArrayList();
    }

    /**
     * Gets the dataCollection attribute of the DynRepDao object
     *
     * @return The dataCollection value
     */
    public ArrayList getDataCollection() {
        return dataCollection;
    }

    /**
     * Gets the dataList attribute of the DynRepDao object
     *
     * @return The dataList value
     */
    public ArrayList getDataList() {
        return dataList;
    }

    /**
     * Sets the repColId attribute of the DynRepDao object
     *
     * @param repColId
     *            The new repColId value
     */
    public void setRepColId(ArrayList repColId) {
        this.repColId = repColId;
    }

    /**
     * Sets the repColId attribute of the DynRepDao object
     *
     * @param ColId
     *            The new repColId value
     */
    public void setRepColId(String ColId) {
        this.repColId.add(ColId);
    }

    /**
     * Sets the repCol attribute of the DynRepDao object
     *
     * @param repCol
     *            The new repCol value
     */
    public void setRepCol(ArrayList repCol) {
        this.repCol = repCol;
    }

    /**
     * Sets the repCol attribute of the DynRepDao object
     *
     * @param repCol
     *            The new repCol value
     */
    public void setRepCol(String repCol) {
        this.repCol.add(repCol);
    }

    /**
     * Sets the repColSeq attribute of the DynRepDao object
     *
     * @param repColSeq
     *            The new repColSeq value
     */
    public void setRepColSeq(ArrayList repColSeq) {
        this.repColSeq = repColSeq;
    }

    /**
     * Sets the repColSeq attribute of the DynRepDao object
     *
     * @param repColSeq
     *            The new repColSeq value
     */
    public void setRepColSeq(String repColSeq) {
        this.repColSeq.add(repColSeq);
    }

    /**
     * Sets the repColWidth attribute of the DynRepDao object
     *
     * @param repColWidth
     *            The new repColWidth value
     */
    public void setRepColWidth(ArrayList repColWidth) {
        this.repColWidth = repColWidth;
    }

    /**
     * Sets the repColWidth attribute of the DynRepDao object
     *
     * @param repColWidth
     *            The new repColWidth value
     */
    public void setRepColWidth(String repColWidth) {
        this.repColWidth.add(repColWidth);
    }

    /**
     * Sets the repColFormat attribute of the DynRepDao object
     *
     * @param repColFormat
     *            The new repColFormat value
     */
    public void setRepColFormat(ArrayList repColFormat) {
        this.repColFormat = repColFormat;
    }

    /**
     * Sets the repColFormat attribute of the DynRepDao object
     *
     * @param repColFormat
     *            The new repColFormat value
     */
    public void setRepColFormat(String repColFormat) {
        this.repColFormat.add(repColFormat);
    }

    /**
     * Sets the repColDispName attribute of the DynRepDao object
     *
     * @param repColDispName
     *            The new repColDispName value
     */
    public void setRepColDispName(ArrayList repColDispName) {
        this.repColDispName = repColDispName;
    }

    /**
     * Sets the repColDispName attribute of the DynRepDao object
     *
     * @param repColDispName
     *            The new repColDispName value
     */
    public void setRepColDispName(String repColDispName) {
        this.repColDispName.add(repColDispName);
    }

    /**
     * Sets the repColName attribute of the DynRepDao object
     *
     * @param repColName
     *            The new repColName value
     */
    public void setRepColName(ArrayList repColName) {
        this.repColName = repColName;
    }

    /**
     * Sets the repColName attribute of the DynRepDao object
     *
     * @param repColName
     *            The new repColName value
     */
    public void setRepColName(String repColName) {
        this.repColName.add(repColName);
    }

    /**
     * Sets the repColType attribute of the DynRepDao object
     *
     * @param repColType
     *            The new repColType value
     */
    public void setRepColType(ArrayList repColType) {
        this.repColType = repColType;
    }

    /**
     * Sets the repColType attribute of the DynRepDao object
     *
     * @param repColType
     *            The new repColType value
     */
    public void setRepColType(String repColType) {
        this.repColType.add(repColType);
    }

    /**
     * Gets the repColId attribute of the DynRepDao object
     *
     * @return The repColId value
     */
    public ArrayList getRepColId() {
        return repColId;
    }

    /**
     * Gets the repCol attribute of the DynRepDao object
     *
     * @return The repCol value
     */
    public ArrayList getRepCol() {
        return repCol;
    }

    /**
     * Gets the repColSeq attribute of the DynRepDao object
     *
     * @return The repColSeq value
     */
    public ArrayList getRepColSeq() {
        return repColSeq;
    }

    /**
     * Gets the repColWidth attribute of the DynRepDao object
     *
     * @return The repColWidth value
     */
    public ArrayList getRepColWidth() {
        return repColWidth;
    }

    /**
     * Gets the repColFormat attribute of the DynRepDao object
     *
     * @return The repColFormat value
     */
    public ArrayList getRepColFormat() {
        return repColFormat;
    }

    /**
     * Gets the repColDispName attribute of the DynRepDao object
     *
     * @return The repColDispName value
     */
    public ArrayList getRepColDispName() {
        return repColDispName;
    }

    /**
     * Gets the repColName attribute of the DynRepDao object
     *
     * @return The repColName value
     */
    public ArrayList getRepColName() {
        return repColName;
    }

    /**
     * Gets the repColType attribute of the DynRepDao object
     *
     * @return The repColType value
     */
    public ArrayList getRepColType() {
        return repColType;
    }

    /**
     * Sets the fltrColIds attribute of the DynRepDao object
     *
     * @param fltrColIds
     *            The new fltrColIds value
     */
    public void setFltrColIds(ArrayList fltrColIds) {
        this.fltrColIds = fltrColIds;
    }

    /**
     * Sets the fltrColIds attribute of the DynRepDao object
     *
     * @param fltrColId
     *            The new fltrColIds value
     */
    public void setFltrColIds(String fltrColId) {
        this.fltrColIds.add(fltrColId);
    }

    /**
     * Sets the filtCol attribute of the DynRepDao object
     *
     * @param filtCol
     *            The new filtCol value
     */
    public void setFiltCol(ArrayList filtCol) {
        this.filtCol = filtCol;
    }

    /**
     * Sets the filtCol attribute of the DynRepDao object
     *
     * @param fltrCol
     *            The new filtCol value
     */
    public void setFiltCol(String fltrCol) {
        this.filtCol.add(fltrCol);
    }

    /**
     * Sets the filtQf attribute of the DynRepDao object
     *
     * @param filtQf
     *            The new filtQf value
     */
    public void setFiltQf(ArrayList filtQf) {
        this.filtQf = filtQf;
    }

    /**
     * Sets the filtQf attribute of the DynRepDao object
     *
     * @param fltrQf
     *            The new filtQf value
     */
    public void setFiltQf(String fltrQf) {
        this.filtQf.add(fltrQf);
    }

    /**
     * Sets the filtData attribute of the DynRepDao object
     *
     * @param filtData
     *            The new filtData value
     */
    public void setFiltData(ArrayList filtData) {
        this.filtData = filtData;
    }

    /**
     * Sets the filtData attribute of the DynRepDao object
     *
     * @param fltrData
     *            The new filtData value
     */
    public void setFiltData(String fltrData) {
        this.filtData.add(fltrData);
    }

    /**
     * Sets the filtExclude attribute of the DynRepDao object
     *
     * @param filtExclude
     *            The new filtExclude value
     */
    public void setFiltExclude(ArrayList filtExclude) {
        this.filtExclude = filtExclude;
    }

    /**
     * Sets the filtExclude attribute of the DynRepDao object
     *
     * @param fltrExclude
     *            The new filtExclude value
     */
    public void setFiltExclude(String fltrExclude) {
        this.filtExclude.add(fltrExclude);
    }

    /**
     * Sets the filtBbrac attribute of the DynRepDao object
     *
     * @param filtBbrac
     *            The new filtBbrac value
     */
    public void setFiltBbrac(ArrayList filtBbrac) {
        this.filtBbrac = filtBbrac;
    }

    /**
     * Sets the filtBbrac attribute of the DynRepDao object
     *
     * @param fltrBbrac
     *            The new filtBbrac value
     */
    public void setFiltBbrac(String fltrBbrac) {
        this.filtBbrac.add(fltrBbrac);
    }

    /**
     * Sets the filtEbrac attribute of the DynRepDao object
     *
     * @param filtEbrac
     *            The new filtEbrac value
     */
    public void setFiltEbrac(ArrayList filtEbrac) {
        this.filtEbrac = filtEbrac;
    }

    /**
     * Sets the filtEbrac attribute of the DynRepDao object
     *
     * @param fltrEbrac
     *            The new filtEbrac value
     */
    public void setFiltEbrac(String fltrEbrac) {
        this.filtEbrac.add(fltrEbrac);
    }

    /**
     * Sets the filtOper attribute of the DynRepDao object
     *
     * @param filtOper
     *            The new filtOper value
     */
    public void setFiltOper(ArrayList filtOper) {
        this.filtOper = filtOper;
    }

    /**
     * Sets the filtOper attribute of the DynRepDao object
     *
     * @param fltrOper
     *            The new filtOper value
     */
    public void setFiltOper(String fltrOper) {
        this.filtOper.add(fltrOper);
    }

    /**
     * Sets the filtSeq attribute of the DynRepDao object
     *
     * @param filtSeq
     *            The new filtSeq value
     */
    public void setFiltSeq(ArrayList filtSeq) {
        this.filtSeq = filtSeq;
    }

    /**
     * Sets the filtSeq attribute of the DynRepDao object
     *
     * @param fltrSeq
     *            The new filtSeq value
     */
    public void setFiltSeq(String fltrSeq) {
        this.filtSeq.add(fltrSeq);
    }

    /**
     * Sets the filtName attribute of the DynRepDao object
     *
     * @param filtName
     *            The new filtName value
     */
    public void setFiltName(String filtName) {
        this.filtName = filtName;
    }

    /**
     * Sets the filtId attribute of the DynRepDao object
     *
     * @param filtId
     *            The new filtId value
     */
    public void setFiltId(String filtId) {
        this.filtId = filtId;
    }

    /**
     * Gets the fltrColIds attribute of the DynRepDao object
     *
     * @return The fltrColIds value
     */
    public ArrayList getFltrColIds() {
        return fltrColIds;
    }

    /**
     * Gets the filtCol attribute of the DynRepDao object
     *
     * @return The filtCol value
     */
    public ArrayList getFiltCol() {
        return filtCol;
    }

    /**
     * Gets the filtQf attribute of the DynRepDao object
     *
     * @return The filtQf value
     */
    public ArrayList getFiltQf() {
        return filtQf;
    }

    /**
     * Gets the filtData attribute of the DynRepDao object
     *
     * @return The filtData value
     */
    public ArrayList getFiltData() {
        return filtData;
    }

    /**
     * Gets the filtExclude attribute of the DynRepDao object
     *
     * @return The filtExclude value
     */
    public ArrayList getFiltExclude() {
        return filtExclude;
    }

    /**
     * Gets the filtBbrac attribute of the DynRepDao object
     *
     * @return The filtBbrac value
     */
    public ArrayList getFiltBbrac() {
        return filtBbrac;
    }

    /**
     * Gets the filtEbrac attribute of the DynRepDao object
     *
     * @return The filtEbrac value
     */
    public ArrayList getFiltEbrac() {
        return filtEbrac;
    }

    /**
     * Gets the filtOper attribute of the DynRepDao object
     *
     * @return The filtOper value
     */
    public ArrayList getFiltOper() {
        return filtOper;
    }

    /**
     * Gets the filtSeq attribute of the DynRepDao object
     *
     * @return The filtSeq value
     */
    public ArrayList getFiltSeq() {
        return filtSeq;
    }

    /**
     * Gets the filtName attribute of the DynRepDao object
     *
     * @return The filtName value
     */
    public String getFiltName() {
        return filtName;
    }

    /**
     * Gets the filtId attribute of the DynRepDao object
     *
     * @return The filtId value
     */
    public String getFiltId() {
        return filtId;
    }

    /**
     * Sets the usr attribute of the DynRepDao object
     *
     * @param usr
     *            The new usr value
     */
    public void setUsr(String usr) {
        this.usr = usr;
    }

    /**
     * Sets the ipAdd attribute of the DynRepDao object
     *
     * @param ipAdd
     *            The new ipAdd value
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Gets the usr attribute of the DynRepDao object
     *
     * @return The usr value
     */
    public String getUsr() {
        return usr;
    }

    /**
     * Gets the ipAdd attribute of the DynRepDao object
     *
     * @return The ipAdd value
     */
    public String getIpAdd() {
        return ipAdd;
    }

    /**
     * Sets the repId attribute of the DynRepDao object
     *
     * @param repId
     *            The new repId value
     */
    public void setRepId(String repId) {
        this.repId = repId;
    }

    /**
     * Gets the repId attribute of the DynRepDao object
     *
     * @return The repId value
     */
    public String getRepId() {
        return repId;
    }

    /**
     * Sets the attributes attribute of the DynRepDao object
     *
     * @param attributes
     *            The new attributes value
     */
    public void setAttributes(HashMap attributes) {
        this.attributes = attributes;
    }

    /**
     * Gets the attributes attribute of the DynRepDao object
     *
     * @return The attributes value
     */
    public HashMap getAttributes() {
        return attributes;
    }

    /**
     * Sets the fltrDtId attribute of the DynRepDao object
     *
     * @param fltDtId
     *            The new fltrDtId value
     */
    public void setFltrDtId(String fltDtId) {
        this.fltrDtId = fltrDtId;
    }

    /**
     * Gets the fltrDtId attribute of the DynRepDao object
     *
     * @return The fltrDtId value
     */
    public String getFltrDtId() {
        return fltrDtId;
    }

    /**
     * Sets the fltrIds attribute of the DynRepDao object
     *
     * @param fltrIds
     *            The new fltrIds value
     */
    public void setFltrIds(ArrayList fltrIds) {
        this.fltrIds = fltrIds;
    }

    /**
     * Sets the fltrIds attribute of the DynRepDao object
     *
     * @param fltrId
     *            The new fltrIds value
     */
    public void setFltrIds(String fltrId) {
        this.fltrIds.add(fltrId);
    }

    /**
     * Sets the fltrNames attribute of the DynRepDao object
     *
     * @param fltrNames
     *            The new fltrNames value
     */
    public void setFltrNames(ArrayList fltrNames) {
        this.fltrNames = fltrNames;
    }

    /**
     * Sets the fltrNames attribute of the DynRepDao object
     *
     * @param fltrName
     *            The new fltrNames value
     */
    public void setFltrNames(String fltrName) {
        this.fltrNames.add(fltrName);
    }

    /**
     * Sets the fltrStrs attribute of the DynRepDao object
     *
     * @param fltrStrs
     *            The new fltrStrs value
     */
    public void setFltrStrs(ArrayList fltrStrs) {
        this.fltrStrs = fltrStrs;
    }

    /**
     * Sets the fltrStrs attribute of the DynRepDao object
     *
     * @param fltrStr
     *            The new fltrStrs value
     */
    public void setFltrStrs(String fltrStr) {
        this.fltrStrs.add(fltrStr);
    }

    /**
     * Gets the fltrIds attribute of the DynRepDao object
     *
     * @return The fltrIds value
     */
    public ArrayList getFltrIds() {
        return fltrIds;
    }

    /**
     * Gets the fltrNames attribute of the DynRepDao object
     *
     * @return The fltrNames value
     */
    public ArrayList getFltrNames() {
        return fltrNames;
    }

    /**
     * Gets the fltrStrs attribute of the DynRepDao object
     *
     * @return The fltrStrs value
     */
    public ArrayList getFltrStrs() {
        return fltrStrs;
    }

    /**
     * Sets the fltrStr attribute of the DynRepDao object
     *
     * @param fltrStr
     *            The new fltrStr value
     */
    public void setFltrStr(String fltrStr) {
        this.fltrStr = fltrStr;
    }

    /**
     * Gets the fltrStr attribute of the DynRepDao object
     *
     * @return The fltrStr value
     */
    public String getFltrStr() {
        return fltrStr;
    }

    /**
     * Sets the fkForm attribute of the DynRepDao object
     *
     * @param fkForm
     *            The new fkForm value
     */
    public void setFkForm(String fkForm) {
        this.fkForm = fkForm;
    }

    /**
     * Gets the fkForm attribute of the DynRepDao object
     *
     * @return The fkForm value
     */
    public String getFkForm() {
        return fkForm;
    }

    /**
     * Returns the value of mapColIds.
     *
     * @return The mapColIds value
     */
    public ArrayList getMapColIds() {
        return mapColIds;
    }

    /**
     * Sets the value of mapColIds.
     *
     * @param mapColIds
     *            The value to assign mapColIds.
     */
    public void setMapColIds(ArrayList mapColIds) {
        this.mapColIds = mapColIds;
    }

    /**
     * Sets the mapColIds attribute of the DynRepDao object
     *
     * @param mapColId
     *            The new mapColIds value
     */
    public void setMapColIds(String mapColId) {
        this.mapColIds.add(mapColId);
    }

    /**
     * Returns the value of mapColLens.
     *
     * @return The mapColLens value
     */
    public ArrayList getMapColLens() {
        return mapColLens;
    }

    /**
     * Sets the value of mapColLens.
     *
     * @param mapColLens
     *            The value to assign mapColLens.
     */
    public void setMapColLens(ArrayList mapColLens) {
        this.mapColLens = mapColLens;
    }

    /**
     * Sets the mapColLens attribute of the DynRepDao object
     *
     * @param mapColLen
     *            The new mapColLens value
     */
    public void setMapColLens(String mapColLen) {
        this.mapColLens.add(mapColLen);
    }

    /**
     * Returns the value of FldAttr.
     *
     * @return The fldAttr value
     */
    public HashMap getFldAttr() {
        return FldAttr;
    }

    /**
     * Sets the value of FldAttr.
     *
     * @param FldAttr
     *            The value to assign FldAttr.
     */
    public void setFldAttr(HashMap FldAttr) {
        this.FldAttr = FldAttr;
    }

    /**
     * Returns the value of SortAttr.
     *
     * @return The sortAttr value
     */
    public HashMap getSortAttr() {
        return SortAttr;
    }

    /**
     * Sets the value of SortAttr.
     *
     * @param SortAttr
     *            The value to assign SortAttr.
     */
    public void setSortAttr(HashMap SortAttr) {
        this.SortAttr = SortAttr;
    }

    /**
     * Returns the value of TypeAttr.
     *
     * @return The typeAttr value
     */
    public HashMap getTypeAttr() {
        return TypeAttr;
    }

    /**
     * Sets the value of TypeAttr.
     *
     * @param TypeAttr
     *            The value to assign TypeAttr.
     */
    public void setTypeAttr(HashMap TypeAttr) {
        this.TypeAttr = TypeAttr;
    }

    /**
     * Returns the value of RepAttr.
     *
     * @return The repAttr value
     */
    public HashMap getRepAttr() {
        return RepAttr;
    }

    /**
     * Sets the value of RepAttr.
     *
     * @param RepAttr
     *            The value to assign RepAttr.
     */
    public void setRepAttr(HashMap RepAttr) {
        this.RepAttr = RepAttr;
    }

    /**
     * Returns the value of FltrAttr.
     *
     * @return The fltrAttr value
     */
    public HashMap getFltrAttr() {
        return FltrAttr;
    }

    /**
     * Sets the value of FltrAttr.
     *
     * @param FltrAttr
     *            The value to assign FltrAttr.
     */
    public void setFltrAttr(HashMap FltrAttr) {
        this.FltrAttr = FltrAttr;
    }

    /**
     * Returns the value of reportContainer.
     *
     * @return The reportContainer value
     */
    /*public ReportHolder getReportContainer() {
        return reportContainer;
    }

    *//**
     * Sets the value of reportContainer.
     *
     * @param reportContainer
     *            The value to assign reportContainer.
     *//*
    public void setReportContainer(ReportHolder reportContainer) {
        this.reportContainer = reportContainer;
    }
*/
    /**
     * Returns the value of formIdList.
     *
     * @return The formIdList value
     */
    public ArrayList getFormIdList() {
        return formIdList;
    }

    /**
     * Sets the value of formIdList.
     *
     * @param formIdList
     *            The value to assign formIdList.
     */
    public void setFormIdList(ArrayList formIdList) {
        this.formIdList = formIdList;
    }

    /**
     * Returns the value of formNameList.
     *
     * @return The formNameList value
     */
    public ArrayList getFormNameList() {
        return formNameList;
    }

    /**
     * Sets the value of formNameList.
     *
     * @param formNameList
     *            The value to assign formNameList.
     */
    public void setFormNameList(ArrayList formNameList) {
        this.formNameList = formNameList;
    }

    /**
     * Returns the value of repColFormId.
     */
    public ArrayList getRepColFormId() {
        return repColFormId;
    }

    /**
     * Sets the value of repColFormId.
     *
     * @param repColFormId
     *            The value to assign repColFormId.
     */
    public void setRepColFormId(ArrayList repColFormId) {
        this.repColFormId = repColFormId;
    }

    /**
     * Sets the value of repColFormId.
     *
     * @param repColFormId
     *            The value to assign repColFormId.
     */
    public void setRepColFormId(String repColFormId) {
        this.repColFormId.add(repColFormId);
    }

    /**
     * Returns the value of fltrFormIds.
     */
    public Vector getFltrFormIds() {
        return fltrFormIds;
    }

    /**
     * Sets the value of fltrFormIds.
     *
     * @param fltrFormIds
     *            The value to assign fltrFormIds.
     */
    public void setFltrFormIds(Vector fltrFormIds) {
        this.fltrFormIds = fltrFormIds;
    }

    /**
     * Appends a fltrFormId to attribute fltrFormId.
     *
     * @param fltrFormIds
     *            The form id associated with the filter
     */
    public void setFltrFormIds(String fltrFormId) {
        this.fltrFormIds.add(fltrFormId);
    }

    /**
     * Returns the value of fltrParentFilterId.s
     */
    public Vector getFltrParentFilterIds() {
        return fltrParentFilterIds;
    }

    /**
     * Sets the value of fltrParentFilterId.s
     *
     * @param fltrParentFilterId
     *            The value to assign fltrParentFilterId.s
     */
    public void setFltrParentFilterIds(Vector fltrParentFilterIds) {
        this.fltrParentFilterIds = fltrParentFilterIds;
    }

    /**
     * Appends a fltrParentFilterId to fltrParentFilterIds.
     *
     * @param fltrParentFilterId
     *            The value to assign fltrParentFilterIds.
     */
    public void setFltrParentFilterIds(String fltrParentFilterId) {
        this.fltrParentFilterIds.add(fltrParentFilterId);
    }

    /**
     * Returns the value of mapTableName.
     */
    public String getMapTableName() {
        return mapTableName;
    }

    /**
     * Sets the value of mapTableName.
     *
     * @param mapTableName
     *            The value to assign mapTableName.
     */
    public void setMapTableName(String mapTableName) {
        this.mapTableName = mapTableName;
    }

    /**
     * Returns the value of mapPrimaryKeyCol.
     */
    public String getMapPrimaryKeyCol() {
        return mapPrimaryKeyCol;
    }

    /**
     * Sets the value of mapPrimaryKeyCol.
     *
     * @param mapPrimaryKeyCol
     *            The value to assign mapPrimaryKeyCol.
     */
    public void setMapPrimaryKeyCol(String mapPrimaryKeyCol) {
        this.mapPrimaryKeyCol = mapPrimaryKeyCol;
    }

    /**
     * Returns the value of mapFilterCol.
     */
    public String getMapFilterCol() {
        return mapFilterCol;
    }

    /**
     * Sets the value of mapFilterCol.
     *
     * @param mapFilterCol
     *            The value to assign mapFilterCol.
     */
    public void setMapFilterCol(String mapFilterCol) {
        this.mapFilterCol = mapFilterCol;
    }

    /**
     * Returns the value of filterFormTypes.
     */
    public ArrayList getFilterFormTypes() {
        return filterFormTypes;
    }

    /**
     * Sets the value of filterFormTypes.
     *
     * @param filterFormTypes
     *            The value to assign filterFormTypes.
     */
    public void setFilterFormTypes(ArrayList filterFormTypes) {
        this.filterFormTypes = filterFormTypes;
    }

    /**
     * Adds a filter formtype to filterFormTypes.
     *
     * @param filterFormType
     *            The value to add to filterFormTypes.
     */
    public void setFilterFormTypes(String filterFormType) {
        this.filterFormTypes.add(filterFormType);
    }

    /**
     * Returns the value of mapUids.
     */
    public ArrayList getMapUids() {
        return mapUids;
    }

    /**
     * Sets the value of mapUids.
     *
     * @param mapUids
     *            The value to assign mapUids.
     */
    public void setMapUids(ArrayList mapUids) {
        this.mapUids = mapUids;
    }

    /**
     * Adds a mapUid to mapUids.
     *
     * @param mapUids
     *            The value to add to mapUids.
     */
    public void setMapUids(String mapUid) {
        this.mapUids.add(mapUid);
    }

    /**
     * Returns the value of mapStudyColumn.
     */
    public String getMapStudyColumn() {
        return mapStudyColumn;
    }

    /**
     * Sets the value of mapStudyColumn.
     *
     * @param mapStudyColumn
     *            The value to assign mapStudyColumn.
     */
    public void setMapStudyColumn(String mapStudyColumn) {
        this.mapStudyColumn = mapStudyColumn;
    }

    /**
     * Returns the value of repUseUniqueId.
     */
    public String getRepUseUniqueId() {
        return repUseUniqueId;
    }

    /**
     * Sets the value of repUseUniqueId.
     *
     * @param repUseUniqueId
     *            The value to assign repUseUniqueId.
     */
    public void setRepUseUniqueId(String repUseUniqueId) {
        this.repUseUniqueId = repUseUniqueId;
    }

    /**
     * Returns the value of repUseDataValue.
     */
    public String getRepUseDataValue() {
        return repUseDataValue;
    }

    /**
     * Sets the value of repUseDataValue.
     *
     * @param repUseDataValue
     *            The value to assign repUseDataValue.
     */
    public void setRepUseDataValue(String repUseDataValue) {
        this.repUseDataValue = repUseDataValue;
    }

    /**
     * Returns the value of useUniqueIds.
     */
    public ArrayList getUseUniqueIds() {
        return useUniqueIds;
    }

    /**
     * Sets the value of useUniqueIds.
     *
     * @param useUniqueIds
     *            The value to assign useUniqueIds.
     */
    public void setUseUniqueIds(ArrayList useUniqueIds) {
        this.useUniqueIds = useUniqueIds;

    }

    /**
     * Sets the value of useUniqueIds.
     *
     * @param useUniqueIds
     *            The value to assign useUniqueIds.
     */
    public void setUseUniqueIds(String useUniqueId) {
        this.useUniqueIds.add(useUniqueId);

    }

    /**
     * Returns the value of useDataValues.
     */
    public ArrayList getUseDataValues() {
        return useDataValues;
    }

    /**
     * Sets the value of useDataValues.
     *
     * @param useDataValues
     *            The value to assign useDataValues.
     */
    public void setUseDataValues(ArrayList useDataValues) {
        this.useDataValues = useDataValues;
    }

    /**
     * Sets the value of useDataValues.
     *
     * @param useDataValue
     *            The value to add to useDataValues.
     */
    public void setUseDataValues(String useDataValue) {
        this.useDataValues.add(useDataValue);
    }

    /**
     * Returns the value of mapColTypesNoRepeatFields.
     */
    public ArrayList getMapColTypesNoRepeatFields() {
        return mapColTypesNoRepeatFields;
    }

    /**
     * Sets the value of mapColTypesNoRepeatFields.
     *
     * @param mapColTypesNoRepeatFields
     *            The value to assign mapColTypesNoRepeatFields.
     */
    public void setMapColTypesNoRepeatFields(ArrayList mapColTypesNoRepeatFields) {
        this.mapColTypesNoRepeatFields = mapColTypesNoRepeatFields;
    }

    /**
     * Sets the value of mapColTypesNoRepeatFields.
     *
     * @param mapColTypesNoRepeatFields
     *            The value to assign mapColTypesNoRepeatFields.
     */
    public void setMapColTypesNoRepeatFields(String mapColTypesNoRepeatField) {
        this.mapColTypesNoRepeatFields.add(mapColTypesNoRepeatField);
    }

    /**
     * Returns the value of mapSysIds.
     */
    public ArrayList getMapSysIds() {
        return mapSysIds;
    }

    /**
     * Sets the value of mapSysIds.
     *
     * @param mapSysIds
     *            The value to assign mapSysIds.
     */
    public void setMapSysIds(ArrayList mapSysIds) {
        this.mapSysIds = mapSysIds;
    }

    /**
     * Sets the value of mapSysIds.
     *
     * @param mapSysIds
     *            The value to assign mapSysIds.
     */
    public void setMapSysIds(String mapSysId) {
        this.mapSysIds.add(mapSysId);
    }

    /**
     * Returns the value of flrDateRangeType.
     */
    public ArrayList getFlrDateRangeType() {
        return flrDateRangeType;
    }

    /**
     * Sets the value of flrDateRangeType.
     *
     * @param flrDateRangeType
     *            The value to assign flrDateRangeType.
     */
    public void setFlrDateRangeType(ArrayList flrDateRangeType) {
        this.flrDateRangeType = flrDateRangeType;
    }

    /**
     * Adds the value to flrDateRangeType.
     *
     * @param sflrDateRangeType
     *            The value to add to flrDateRangeType.
     */
    public void setFlrDateRangeType(String sflrDateRangeType) {
        this.flrDateRangeType.add(sflrDateRangeType);
    }

    /**
     * Returns the value of fltrDateRangeFrom.
     */
    public ArrayList getFltrDateRangeFrom() {
        return fltrDateRangeFrom;
    }

    /**
     * Sets the value of fltrDateRangeFrom.
     *
     * @param fltrDateRangeFrom
     *            The value to assign fltrDateRangeFrom.
     */
    public void setFltrDateRangeFrom(ArrayList fltrDateRangeFrom) {
        this.fltrDateRangeFrom = fltrDateRangeFrom;
    }

    /**
     * Adds the value to fltrDateRangeFrom.
     *
     * @param sfltrDateRangeFrom
     *            The value to add to fltrDateRangeFrom.
     */
    public void setFltrDateRangeFrom(String sfltrDateRangeFrom) {
        this.fltrDateRangeFrom.add(sfltrDateRangeFrom);
    }

    /**
     * Returns the value of fltrDateRangeTo.
     */
    public ArrayList getFltrDateRangeTo() {
        return fltrDateRangeTo;
    }

    /**
     * Sets the value of fltrDateRangeTo.
     *
     * @param fltrDateRangeTo
     *            The value to assign fltrDateRangeTo.
     */
    public void setFltrDateRangeTo(ArrayList fltrDateRangeTo) {
        this.fltrDateRangeTo = fltrDateRangeTo;
    }

    /**
     * adds the value to fltrDateRangeTo.
     *
     * @param sfltrDateRangeTo
     *            The value to add to fltrDateRangeTo.
     */
    public void setFltrDateRangeTo(String sfltrDateRangeTo) {
        this.fltrDateRangeTo.add(sfltrDateRangeTo);
    }

    /**
     * Returns the value of ignoreFilters.
     */
    public String getIgnoreFilters() {
        return ignoreFilters;
    }

    /**
     * Sets the value of ignoreFilters.
     *
     * @param ignoreFilters
     *            The value to assign ignoreFilters.
     */
    public void setIgnoreFilters(String ignoreFilters) {
        this.ignoreFilters = ignoreFilters;
    }

    public ArrayList getCalcPers() {
		return calcPers;
	}



	public void setCalcPers(ArrayList calcPers) {
		this.calcPers = calcPers;
	}



	public ArrayList getCalcSums() {
		return calcSums;
	}



	public void setCalcSums(ArrayList calcSums) {
		this.calcSums = calcSums;
	}

	public void setCalcSums(String calcSum) {
		this.calcSums.add(calcSum);
	}


public void setCalcPers(String calcPer) {
		this.calcPers.add(calcPer);
	}
/**
 * @return the formLinkedType
 */
public String getFormLinkedType() {
	return formLinkedType;
}



/**
 * @param formLinkedType the formLinkedType to set
 */
public void setFormLinkedType(String formLinkedType) {
	this.formLinkedType = formLinkedType;
}

    /**
     * Gets the mapData attribute of the DynRepDao object
     *
     * @param formId
     *            Description of the Parameter
     */
    /*
     * Modified by Sonia Abrol, 01/31/05, added mapform column - mp_uid for
     * field unique ids
     */
    /* MOdified by Sonia Abrol, 04/12/05, added logic to populate mapStudyColumn */
    /*
     * Modified by Sonia Abrol, to populate hashtable ignoreFilters for core
     * lookups
     */
    public void getMapData(String formId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int id = 0;
        //LinkedFormsJB lfJB = new LinkedFormsJB();
        String formType = "";
        String formTypeGeneral = "";
        String studyNumSql = "";

        Rlog
                .debug("dynrep",
                        "formId received in DynRepDao(after EJBUTIL Conversion):}"
                                + id);
        try {
            // check if the formID contains a "C". In that case its not a form
            // but a lookup view id
            if (!formId.startsWith("C")) {
                // if it does not start with a"C" its a form

                id = EJBUtil.stringToNum(formId);

                // get the form type
               // lfJB.findByFormId(id);
                //formType = lfJB.getLFDisplayType();

                if (StringUtil.isEmpty(formType))
                    formType = "";

                formType = formType.trim();
                // append the default columns for this form

                if (formType.equals("S") || formType.equals("SA")) {
                    formTypeGeneral = "S";
                } else if (formType.equals("SP") || formType.equals("PA")
                        || formType.equals("PS") || formType.equals("PR")) {
                    formTypeGeneral = "P";
                } else if (formType.equals("A")) {
                    formTypeGeneral = "A";
                } else {
                    formTypeGeneral = formType;
                }

                if (formType.equals("S") || formType.equals("SA")
                        || formType.equals("SP") || formType.equals("PS")
                        || formType.equals("PR")) {
                    if (formType.equals("S") || formType.equals("SA")) {
                        studyNumSql = "(SELECT study_number FROM ER_STUDY WHERE pk_study=ID)";
                    } else if (formType.equals("SP") || formType.equals("PS")
                            || formType.equals("PR")) {
                        studyNumSql = "(SELECT study_number FROM ER_STUDY where pk_study = (select fk_study from er_patprot where pk_patprot = fk_patprot ))";
                    }
                    // study forms
                    setMapCols(studyNumSql);
                    setMapOrigIds("vel_studynumber");
                    setMapDispNames(LC.L_Study_Number);
                    setMapFormTypes(formTypeGeneral);
                    setMapColTypes("ET");
                    setMapColSecIds("0");
                    setMapColSecNames(LC.L_Default);
                    setMapColIds("0");
                    setMapColLens("100");
                    setMapUids(LC.L_Study_Number);
                    setMapColTypesNoRepeatFields("ET"); // right now it gets all
                    // the data,
                    // it will be filtered in preProcessMap
                    setMapSysIds("vel_studynumber");
                    setMapColSecRepNo("0");

                }

                // all patient forms
                if (formType.equals("SP") || formType.equals("PA")
                        || formType.equals("PS") || formType.equals("PR")) {
                    // study forms
                    setMapCols("(SELECT per_code FROM ER_PER WHERE pk_per=ID )");
                    setMapOrigIds("velpat_code");
                    setMapDispNames(LC.L_Patient_Id);
                    setMapFormTypes("P");
                    setMapColTypes("ET");
                    setMapColSecIds("0");
                    setMapColSecNames(LC.L_Default);
                    setMapColIds("0");
                    setMapColLens("100");
                    setMapUids(LC.L_Patient_Id);
                    setMapColTypesNoRepeatFields("ET"); // right now it gets all
                    // the data,
                    // it will be filtered in preProcessMap
                    setMapSysIds("velpat_code");
                    setMapColSecRepNo("0");
                }

                // all patient study forms
                if (formType.equals("SP") || formType.equals("PS")
                        || formType.equals("PR")) {
                    // patient study forms
                    setMapCols("(select patprot_patstdid from er_patprot where pk_patprot = fk_patprot)");
                    setMapOrigIds("velpat_studyid");
                    setMapDispNames(LC.L_Patient_StudyId);
                    setMapFormTypes("P");
                    setMapColTypes("ET");
                    setMapColSecIds("0");
                    setMapColSecNames(LC.L_Default);
                    setMapColIds("0");
                    setMapColLens("100");
                    setMapUids(LC.L_Patient_StudyId);
                    setMapColTypesNoRepeatFields("ET"); // right now it gets all
                    // the data,
                    // it will be filtered in preProcessMap
                    setMapSysIds("velpat_studyid");
                    setMapColSecRepNo("0");


                    // FOR new CRF Implementation
                    //get Visit Name
                    setMapCols("visit_name");
                    setMapOrigIds("velpat_visitname");
                    setMapDispNames(LC.L_Visit_Name);
                    setMapFormTypes("P");
                    setMapColTypes("ET");
                    setMapColSecIds("0");
                    setMapColSecNames(LC.L_Default);
                    setMapColIds("0");
                    setMapColLens("100");
                    setMapUids(LC.L_Visit_Name);
                    setMapColTypesNoRepeatFields("ET");
                    setMapSysIds("velpat_visitname");
                    setMapColSecRepNo("0");

                    //get Event Name
                    setMapCols("event_name");
                    setMapOrigIds("velpat_eventname");
                    setMapDispNames(LC.L_Event_Name);
                    setMapFormTypes("P");
                    setMapColTypes("ET");
                    setMapColSecIds("0");
                    setMapColSecNames(LC.L_Default);
                    setMapColIds("0");
                    setMapColLens("100");
                    setMapUids(LC.L_Event_Name);
                    setMapColTypesNoRepeatFields("ET");
                    setMapSysIds("velpat_eventname");
                    setMapColSecRepNo("0");
                    //get calendar name

                    setMapCols("protocol_name");
                    setMapOrigIds("velpat_protname");
                    setMapDispNames(LC.L_Protocol_Name);
                    setMapFormTypes("P");
                    setMapColTypes("ET");
                    setMapColSecIds("0");
                    setMapColSecNames(LC.L_Default);
                    setMapColIds("0");
                    setMapColLens("100");
                    setMapUids(LC.L_Protocol_Name);
                    setMapColTypesNoRepeatFields("ET");
                    setMapSysIds("velpat_protname");
                    setMapColSecRepNo("0");


                }

                // for all forms



                setMapCols("(select codelst_desc from er_codelst where pk_codelst = form_completed)");
                setMapOrigIds("vel_formstatus");
                setMapDispNames(LC.L_Form_ResponseStatus);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("ET");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Form_ResponseStatus);
                setMapColTypesNoRepeatFields("ET");
                setMapSysIds("vel_formstatus");
                setMapColSecRepNo("0");

                //filled form id
                setMapCols("fk_filledform");
                setMapOrigIds("vel_responseId");
                setMapDispNames(LC.L_Response_Id);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("EN");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Response_Id);
                setMapColTypesNoRepeatFields("EN");
                setMapSysIds("vel_responseId");
                setMapColSecRepNo("0");

                //creator
                setMapCols("(usr_lst(creator))");
                setMapOrigIds("vel_creator");
                setMapDispNames(LC.L_Created_By);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("ET");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Created_By);
                setMapColTypesNoRepeatFields("ET");
                setMapSysIds("vel_creator");
                setMapColSecRepNo("0");

                //created on
                setMapCols("created_on");
                setMapOrigIds("velcreated_on");
                setMapDispNames(LC.L_Created_On);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("ED");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Created_On);
                setMapColTypesNoRepeatFields("ED");
                setMapSysIds("velcreated_on");
                setMapColSecRepNo("0");

                //last modified by

                setMapCols("(usr_lst(last_modified_by))");
                setMapOrigIds("vel_lastmod_by");
                setMapDispNames(LC.L_Last_ModifiedBy);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("ET");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Last_ModifiedBy);
                setMapColTypesNoRepeatFields("ET");
                setMapSysIds("vel_lastmod_by");
                setMapColSecRepNo("0");

                //Last modified on
                setMapCols("last_modified_date");
                setMapOrigIds("vel_lastmod_on");
                setMapDispNames(LC.L_Last_ModifiedOn);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("ED");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Last_ModifiedOn);
                setMapColTypesNoRepeatFields("ED");
                setMapSysIds("vel_lastmod_on");
                setMapColSecRepNo("0");


                setMapCols("form_version");
                setMapOrigIds("vel_version_number");
                setMapDispNames(LC.L_Form_VersionNumber);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("ET");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Form_VersionNumber);
                setMapColTypesNoRepeatFields("ET");
                setMapSysIds("vel_version_number");
                setMapColSecRepNo("0");

                //get specimen number
                setMapCols(" pkg_specimen.f_get_specimen_info(fk_specimen) ");
                setMapOrigIds("vel_specid");
                setMapDispNames(LC.L_Specimen_Id);
                setMapFormTypes(formTypeGeneral);
                setMapColTypes("ET");
                setMapColSecIds("0");
                setMapColSecNames(LC.L_Default);
                setMapColIds("0");
                setMapColLens("100");
                setMapUids(LC.L_Specimen_Id);
                setMapColTypesNoRepeatFields("ET");
                setMapSysIds("vel_specid");
                setMapColSecRepNo("0");
                // right now it gets all the
                // data,
                // it will be filtered in preProcessMap

                // ////////////
                conn = getConnection();
                String sql = "select pk_mp,mp_mapcolname,mp_origsysid,mp_dispname,mp_formtype,mp_flddatatype,mp_pksec,mp_secname,mp_pkfld,mp_fldcharsno,mp_uid,mp_systemid, formsec_repno from er_mapform, er_formsec where fk_form=? and mp_pksec = pk_formsec ORDER BY mp_pksec, mp_sequence";
                Rlog.debug("dynrep", "sql" + sql);
                pstmt = conn.prepareStatement(sql);
                Rlog.debug("dynrep", "pstmt" + pstmt);
                pstmt.setInt(1, id);
                Rlog.debug("dynrep", "id" + id);
                rs = pstmt.executeQuery();
                Rlog.debug("dynrep", "rs-Using Classes in libext" + rs);
                while (rs.next()) {
                    setMapCols(rs.getString("mp_mapcolname"));
                    String origSysId = rs.getString("mp_origsysid");
                    setMapOrigIds(origSysId);
                	if ("er_def_date_01".equals(origSysId)){
                		setMapDispNames(LC.L_Data_EntryDate);
	                }else{               
	                	setMapDispNames(rs.getString("mp_dispname"));
	                }                	
                    setMapFormTypes(rs.getString("mp_formtype"));
                    setMapColTypes(rs.getString("mp_flddatatype"));
                    setMapColSecIds(rs.getString("mp_pksec"));
                    setMapColSecNames(rs.getString("mp_secname"));
                    setMapColIds(rs.getString("mp_pkfld"));
                    setMapColLens(rs.getString("mp_fldcharsno"));
                    setMapUids(rs.getString("mp_uid"));
                    setMapColTypesNoRepeatFields(rs.getString("mp_flddatatype")); // right
                    // now
                    // it
                    // gets
                    // all
                    // the
                    // data,
                    // it will be filtered in preProcessMap

                    setMapSysIds(rs.getString("mp_systemid"));
                    setMapColSecRepNo(rs.getString("formsec_repno"));

                }

                setMapPrimaryKeyCol("ID");
                setMapFilterCol("FK_FORM");
                setMapTableName("ER_FORMSLINEAR");

            } else // its a core table lookup view
            {
                if (formId.length() > 1) {
                    // extract the lookup view id
                    formId = formId.substring(1);

                    LookupDao ldao = new LookupDao();
                    int lkpViewColCount = 0;
                    String dataType = "";
                    ArrayList coldataTypes = new ArrayList();
                    ArrayList arIsDisplay = new ArrayList();
                    ArrayList arCols = new ArrayList();
                    ArrayList arNames = new ArrayList();
                    String isDisplay = "";
                    ArrayList colKeywords = new ArrayList();
                    ArrayList colTableNames = new ArrayList();
                    String keyWord = "";
                    String ignoreFilters = "";

                    // get the lookup view data
                    ldao.getReturnValues(formId);
                    ignoreFilters = ldao.getLkpIgnoreFilters(Integer
                            .parseInt(formId));
                    setIgnoreFilters(ignoreFilters);

                    arIsDisplay = ldao.getLViewIsDisplay();
                    colKeywords = ldao.getLViewRetKeywords(); // store the
                    // keyword
                    arCols = ldao.getLViewColumns(); // store column
                    arNames = ldao.getLViewRetColumns();
                    coldataTypes = ldao.getLViewColDataTypes();
                    colTableNames = ldao.getLViewTableNames();

                    lkpViewColCount = arCols.size();

                    // set the mapform Arrays

                    Rlog.debug("dynrep", "lookup count columns:"
                            + lkpViewColCount);

                    Rlog.debug("dynrep",
                            "lookup count columns arIsDisplay.size:"
                                    + arIsDisplay.size());
                    Rlog.debug("dynrep",
                            "lookup count columns colKeywords.size:"
                                    + colKeywords.size());
                    Rlog.debug("dynrep", "lookup count columns arCols.size:"
                            + arCols.size());
                    Rlog.debug("dynrep",
                            "lookup count columns coldataTypes.size:"
                                    + coldataTypes.size());
                    Rlog.debug("dynrep",
                            "lookup count columns colTableNames.size:"
                                    + colTableNames.size());
                    Rlog.debug("dynrep", "lookup count columns arNames.size:"
                            + arNames.size());

                    for (int i = 0; i < lkpViewColCount; i++) {
                        isDisplay = (String) arIsDisplay.get(i);
                        Rlog.debug("dynrep", "lookup data isDisplay:"
                                + isDisplay);
                        keyWord = (String) colKeywords.get(i);



                        Rlog.debug("dynrep", "lookup data keyWord:" + keyWord);

                        if (keyWord.toUpperCase().equals("LKP_PK")) {
                            setMapPrimaryKeyCol((String) arCols.get(i));

                            Rlog.debug("dynrep",
                                    "lookup data (String) arCols.get(i):"
                                            + arCols.get(i));

                            setMapFilterCol((String) arCols.get(i));
                            Rlog.debug("dynrep",
                                    "lookup data (String) arCols.get(i):"
                                            + arCols.get(i));

                            setMapTableName((String) colTableNames.get(i));

                            Rlog.debug("dynrep",
                                    "lookup data (String)  colTableNames.get(i):"
                                            + colTableNames.get(i));

                        }

                        // see if the column keyword = LKP_STUDY_CHKRIGHT, if
                        // yes,
                        // the study team rights needs to be checked for this
                        // column
                        if (keyWord.toUpperCase().equals("LKP_STUDY_CHKRIGHT")) {
                            setMapStudyColumn((String) arCols.get(i));
                        }



                        if (isDisplay.toUpperCase().equals("Y")) {

                            setMapCols((String) arCols.get(i));
                            setMapDispNames((String) arNames.get(i));

                            //    add to fieldKeyword
                            setFieldKeyword(keyWord);

                            Rlog.debug("dynrep",
                                    "lookup data (String)  arNames.get(i):"
                                            + arNames.get(i));

                            dataType = (String) coldataTypes.get(i);

                            Rlog
                                    .debug("dynrep",
                                            "lookup data (String)  dataType"
                                                    + dataType);

                            if (dataType.toLowerCase().compareTo("varchar2") == 0) {
                                setMapColTypes("ET");
                                setMapColTypesNoRepeatFields("ET");
                            } else if (dataType.toLowerCase().compareTo("date") == 0) {
                                setMapColTypes("ED");
                                setMapColTypesNoRepeatFields("ED");
                            } else if (dataType.toLowerCase().compareTo("number") == 0) {
                                setMapColTypes("EN");
                                setMapColTypesNoRepeatFields("EN");
                            } else if (dataType.toLowerCase().compareTo("clob") == 0) {
                                setMapColTypes("CL");
                                setMapColTypesNoRepeatFields("CL");
                            }else {
                                setMapColTypes("ET");
                                setMapColTypesNoRepeatFields("ET");
                            }

                            setMapOrigIds("");
                            setMapFormTypes("C");
                            setMapColSecIds("0");
                            setMapColSecNames("");
                            setMapColIds("");
                            setMapColLens("");
                            setMapUids((String) arNames.get(i));
                            setMapSysIds("");
                            setMapColSecRepNo("0");
                        }// end of if for display
                    }// end of for

                }

            }

        } catch (Exception e) {
            Rlog.fatal("dynrep",
                    "{SQLEXCEPTION IN dynrepDao:getMapData(formId)}" + e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Description of the Method
     */
    public void preProcessMap() {
        ArrayList mapColsTmp = new ArrayList();
        ArrayList mapOrigIdsTmp = new ArrayList();
        ArrayList mapDispNameTmp = new ArrayList();
        ArrayList mapColSecIdsTmp = new ArrayList();
        ArrayList mapColSecNamesTmp = new ArrayList();
        ArrayList mapColTypesTmp = new ArrayList();
        ArrayList mapUidsTmp = new ArrayList();
         ArrayList mapRepeatNoTmp = new ArrayList();
        // ArrayList mapSysIdsTmp = new ArrayList();

        int index = -1;
        String sysId = "";
        String origSysId = "";
        String uniqueId = "";
        String existingUniqueId = "";

        for (int i = 0; i < mapOrigIds.size(); i++) {
            index = mapOrigIdsTmp.indexOf(mapOrigIds.get(i));

            sysId = (String) mapSysIds.get(i);
            origSysId = (String) mapOrigIds.get(i);
            existingUniqueId = "";

            if (sysId.equals(origSysId)) {
                // this is an original field and not a repeat field, get its
                // unique id
                uniqueId = (String) mapUids.get(i);

            } else {
                uniqueId = "";
            }

            if (index >= 0) {
                mapOrigIdsTmp.set(index, mapOrigIds.get(i));
                mapRepeatNoTmp.set(index, mapColSecRepNo.get(i));

                mapColsTmp.set(index, ((String) mapColsTmp.get(index)) + "|"
                        + ((String) mapCols.get(i)));
                mapDispNameTmp.set(index, mapDispNames.get(i));
                mapColSecIdsTmp.set(index, mapColSecIds.get(i));
                mapColSecNamesTmp.set(index, mapColSecNames.get(i));
                mapColTypesTmp.set(index, mapColTypes.get(i));

                existingUniqueId = (String) mapUidsTmp.get(index);
                if (StringUtil.isEmpty(existingUniqueId)
                        && (!StringUtil.isEmpty(uniqueId))) {
                    mapUidsTmp.set(index, uniqueId); // to get the original
                    // id and not that of
                    // repeat fields
                }
            } else {
                mapOrigIdsTmp.add(mapOrigIds.get(i));
                mapRepeatNoTmp.add(mapColSecRepNo.get(i));

                mapColsTmp.add(mapCols.get(i));
                mapDispNameTmp.add(mapDispNames.get(i));
                mapColSecIdsTmp.add(mapColSecIds.get(i));
                mapColSecNamesTmp.add(mapColSecNames.get(i));
                mapColTypesTmp.add(mapColTypes.get(i));
                mapUidsTmp.add(uniqueId);
            }
        }
        mapCols = mapColsTmp;
        mapDispNames = mapDispNameTmp;
        mapOrigIds = mapOrigIdsTmp;
        mapColSecIds = mapColSecIdsTmp;
        mapColSecNames = mapColSecNamesTmp;
        mapColTypesNoRepeatFields = mapColTypesTmp;
        mapUids = mapUidsTmp;
        mapColSecRepNo = mapRepeatNoTmp;
    }

    public void getReportData(String sql, String[] fldNames, String formType,
            ArrayList fldTypes,Hashtable moreParameters) {

        this.setRepColType(fldTypes);
        getReportData(sql, fldNames, formType ,moreParameters);

    }

    /**
     * Retrives the data for the specified SQL
     *
     * @param sql
     *            Description of the Parameter
     * @param fldNames
     *            Description of the Parameter
     * @param formType
     *            Description of the Parameter
     */

    public void getReportData(String sql, String[] fldNames, String formType , Hashtable moreParameters) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ArrayList dataList = new ArrayList();
        ArrayList tempList = new ArrayList();
        ResultSet rs = null;
        String tempStr = "";

        //SummaryHolder sHolder = new SummaryHolder();

        ArrayList  arCalcSum = new ArrayList();
        ArrayList  arCalcPer = new ArrayList();

        String strCalc = "";
        String datavalue = "";
        String strPer = "";
        String fileNameForMappingHash = "";
        Hashtable htRowIDMapping  = new Hashtable();
        Hashtable htIDRowCount = new Hashtable();
        String formId = "";
        String ID = "";
        ArrayList arKeys = new ArrayList();
        Hashtable htFinal = new Hashtable();
        ArrayList arMaskedColumnList = new ArrayList();


        Rlog.debug("dynrep", "SQL  received in DynrepDao.getReportData" + sql);
        try {

            conn = getConnection();
            String temp = "";
            String type = "";
            String tempValue = "";
            Rlog.debug("dynrep",
                    "Column Types  received in DynrepDao.getReportData"
                            + repColType);
            int typeListSize = repColType.size();
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            int colType= 0;
            String expFormat = "";
            String maskedReferenceColumn = "";
            boolean anyMaskedColumn=false;
            String columnName="";
            int maskRight = 0;
            boolean bMaskCol = false;

            if (moreParameters != null)
            {

            	 if (moreParameters.containsKey("fldCalcSum"))
            	 {
            	 	arCalcSum = (ArrayList) moreParameters.get("fldCalcSum");

            	 }

            	  if (moreParameters.containsKey("fldCalcPer"))
            	 {
            	 	arCalcPer = (ArrayList) moreParameters.get("fldCalcPer");

            	 }

            	  if (moreParameters.containsKey("formId"))
             	 {
            		  formId = (String) moreParameters.get("formId");

             	 }

            	 if (moreParameters.containsKey("fileNameForMappingHash"))
             	 {
            		 fileNameForMappingHash = (String) moreParameters.get("fileNameForMappingHash");



             	 }
            	 if (moreParameters.containsKey("expFormat"))
             	 {
            		 expFormat = (String) moreParameters.get("expFormat");


             	 }

            	 if (moreParameters.containsKey("arMaskedColumnList"))
             	 {
            		 arMaskedColumnList = (ArrayList) moreParameters.get("arMaskedColumnList");

            		 if (arMaskedColumnList != null && arMaskedColumnList.size() > 0)
            			 {
            			 	anyMaskedColumn = true;
            			 }


             	 }

            	 if (moreParameters.containsKey("maskedReferenceColumn"))
             	 {
            		 maskedReferenceColumn = (String) moreParameters.get("maskedReferenceColumn");


             	 }



            }


            while (rs.next()) {
                resetList();
                // fetch on basis of col number, in case of core lkp tables, col
                // names could be different

                ID = rs.getString(1); // for ID
                setDataList(ID);
                setDataList(rs.getString(2));// for FK_PATPROT
                ResultSetMetaData rsmd = rs.getMetaData();
                for (int i = 0; i < fldNames.length; i++) {
                    // tempList.clear();
                    temp = fldNames[i];
                    bMaskCol = false;

                    if (temp == null) {
                        temp = "";
                    }
                    if (temp.length() > 30) {
                        temp = temp.substring(0, 30);
                    }
                    if (temp.indexOf("?") >= 0) {
                        temp = temp.replace('?', 'q');
                    }
                    if (temp.indexOf("\\") >= 0) {
                        temp = temp.replace('\\', 'S');
                    }

                    // setDataList(rs.getString(temp));
                    if (i < typeListSize)
                    {
                        type = (String) repColType.get(i);
                    }
                    else
                    {
                        type = "";
                    }

                    colType = rsmd.getColumnType(i+3) ;

                    columnName = rsmd.getColumnName(i+3);



                    //System.out.println("type" + rsmd.getColumnType(i+3) + Types.CLOB);

                    // check if its a clob column
                    if (colType == Types.CLOB)
                    {
                    	 Clob clobData = null;

                    	 clobData = rs.getClob(i+3);
                    	 String clobStr = "";
                    	 if (clobData != null) {
                    		 clobStr = clobData.getSubString(1, (int) clobData.length());
                         }

                    	  setDataList(clobStr);
                    }
                    else //for all other column types
                    {
                    	//check if summary is not passed - for sas crash
                    	if (arCalcSum.size() > 0)
                    	{
	                    	strCalc = (String) arCalcSum.get(i);
	                    	strPer = (String) arCalcPer.get(i);
                    	}

                    	if (StringUtil.isEmpty(strPer))
                    	{
                    		strPer = "0";
                    	}

                    	if (StringUtil.isEmpty(strCalc))
                    	{
                    		strCalc = "0";
                    	}


                    	datavalue = rs.getString(i + 3);
                    	
                    	//strip script
                    	datavalue = StringUtil.stripScript(datavalue);

                    	if (anyMaskedColumn )
                    	{
                    		//System.out.println("columnName" + columnName);
                    		//System.out.println("arMaskedColumnList.indexOf(columnName)" + arMaskedColumnList.indexOf(columnName));

                    		if (arMaskedColumnList.indexOf(columnName) >=0)
                    		{
                    			//get the value of masked reference field
                    			maskRight = rs.getInt(maskedReferenceColumn);
                    			if (maskRight < 4)
                    			{
                    				bMaskCol = true;

                    			}
                    		}

                    	}


	                    if ( colType == Types.DATE   )
	                    {
	                    	//just adding a comment so that file is included for next release
	                    	String dtStr = "";

	                    	//System.out.println("datavalue"+datavalue);
	                    	dtStr = DateUtil.format2DateFormat(datavalue);

	                    	if((! StringUtil.isEmpty(dtStr)) && dtStr.length() > 10)
	                    	{
	                    		if (dtStr.indexOf("00:00:00") > 9)
	                    		{
	                    			dtStr = dtStr.replace(" 00:00:00","");
	                    		}


	                    	}
	                    	//System.out.println("colTyp + colType + "datavalue" + datavalue + "con" + dtStr );

	                    	if (StringUtil.isEmpty(dtStr))
	                    	{
	                    		dtStr = "&#160;";
	                    	}

	                    	if (bMaskCol )
	                    	{
	                    		dtStr="****";
	                    	}

	                        setDataList(dtStr);
	                    }
	                    else
	                    {
	                    	if (bMaskCol )
	                    	{
	                    		datavalue="****";
	                    	}

	                        setDataList(datavalue);

	                        if (strCalc.equals("1") && strPer.equals("0") && (! StringUtil.isEmpty(datavalue)))
	                        {
	                        	//sHolder.setHtHolders(i,datavalue,"sum");

	                        }else if (strCalc.equals("1") && strPer.equals("1") && (! StringUtil.isEmpty(datavalue)))
	                        {
	                        	//sHolder.setHtHolders(i,datavalue,"both");
	                        }
	                        else if (strCalc.equals("0") && strPer.equals("1") && (! StringUtil.isEmpty(datavalue)))
	                        {
	                        	//sHolder.setHtHolders(i,datavalue,"per");
	                        }



	                    }



                    } // for all other column types
                }
                setDataCollection(getDataList());
               if (expFormat.equals("L") || expFormat.equals("X"))
           		 {
	                Integer count = new Integer(0);
	                int curIDCOUNT = 0;

	                if (htIDRowCount.containsKey(ID))
	                {
	                	count = (Integer) htIDRowCount.get(ID);
	                }
	                curIDCOUNT = count.intValue() + 1 ;
	                htIDRowCount.put(ID,new Integer(curIDCOUNT));
	                htRowIDMapping.put((ID + "*" + curIDCOUNT), getDataList());
	                arKeys.add(ID + "*" + curIDCOUNT); //to maintain original order
	              }

            } //for each row
            //setRepSummaryHolder(sHolder);

            //serialize

            if (expFormat.equals("L") || expFormat.equals("X"))
            {


	            htFinal.put("keyObj",arKeys);
	            htFinal.put("data",htRowIDMapping);
	            EJBUtil.serializeMe(htFinal,fileNameForMappingHash);
	            htRowIDMapping = null;
            }

            //sHolder.printHolders();
        } catch (SQLException e) {
            Rlog.fatal("dynrep", "{SQLEXCEPTION IN dynrepDao:getReportData()}"
                    + e);
            e.printStackTrace();
            dataList = null;

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the filter attribute of the DynRepDao object
     *
     * @param repId
     *            Description of the Parameter
     */
    /*
     * Modified by Sonia Abrol on 01/11/05, to read fk_form,fk_parentfilter
     * column intoduced with multiform
     */
    public void getFilter(String repId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Clob clobData = null;
        int id = EJBUtil.stringToNum(repId);
        String formType = "";
        String formId = "";
        Rlog.debug("dynrep",
                "repId received in getFlter() in DynRepDao(after EJBUTIL Conversion):}"
                        + id);
        try {
            conn = getConnection();
            String sql = "select pk_dynrepfilter,dynrepfilter_name,dynrepfilter_filter,nvl(fk_form,0) as fk_form ,fk_parentfilter,form_type, daterange_type,daterange_from,daterange_to from er_dynrepfilter where fk_dynrep=? order by lower(dynrepfilter_name) asc, fk_form desc";
            Rlog.debug("dynrep", "sql in DynRepDao:setFilter" + sql);
            pstmt = conn.prepareStatement(sql);
            Rlog.debug("dynrep", "pstmt" + pstmt);
            pstmt.setInt(1, id);
            Rlog.debug("dynrep", "id in dynrepDao:getfilter" + id);
            rs = pstmt.executeQuery();
            Rlog.debug("dynrep", "rs-Using Classes in libext" + rs);

            while (rs.next()) {
                setFltrIds(EJBUtil.integerToString(new Integer(rs
                        .getInt("pk_dynrepfilter"))));
                setFltrNames(rs.getString("dynrepfilter_name"));
                clobData = rs.getClob("dynrepfilter_filter");
                if (clobData != null) {
                    setFltrStrs(clobData.getSubString(1, (int) clobData
                            .length()));
                } else {
                    setFltrStrs("");
                }
                formId = rs.getString("fk_form");
                setFltrParentFilterIds(rs.getString("fk_parentfilter"));

                formType = rs.getString("form_type");

                setFilterFormTypes(formType);

                if (StringUtil.isEmpty(formType)) {
                    formType = "F"; // default to app forms
                }

                if (formType.equals("C")) // core table lookup
                {
                    formId = formType + formId;
                }
                setFltrFormIds(formId);

                setFlrDateRangeType(rs.getString("daterange_type"));
                setFltrDateRangeFrom(rs.getString("daterange_from"));
                setFltrDateRangeTo(rs.getString("daterange_to"));

            }
        } catch (SQLException e) {
            Rlog.fatal("dynrep", "{SQLEXCEPTION IN dynrepDao:getFilter(reoId)}"
                    + e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the filterDetails attribute of the DynRepDao object
     *
     * @param fltrId
     *            Description of the Parameter
     */
    public void getFilterDetails(String fltrId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int id = EJBUtil.stringToNum(fltrId);
        Rlog.debug("dynrep",
                "fltrId received in getFlterdetails() in DynRepDao(after EJBUTIL Conversion):}"
                        + id);
        try {
            conn = getConnection();

            String sql = "select pk_dynrepfilterdt,dynrepfilterdt_col,dynrepfilterdt_qf,dynrepfilterdt_data,dynrepfilterdt_exclude,"
                    + "dynrepfilterdt_bbrac,dynrepfilterdt_ebrac,dynrepfilterdt_seq,dynrepfilterdt_oper from er_dynrepfilterdt where fk_dynrepfilter=? order by pk_dynrepfilterdt";
            Rlog.debug("dynrep", "sql in DynRepDao:setFilterDetails" + sql);
            pstmt = conn.prepareStatement(sql);
            Rlog.debug("dynrep", "pstmt in dynrepdao:getFilterDetails" + pstmt);
            pstmt.setInt(1, id);
            Rlog.debug("dynrep", "id in dynrepDao:getfilter" + id);
            rs = pstmt.executeQuery();
            Rlog.debug("dynrep", "rs-Using Classes in libext" + rs);

            while (rs.next()) {
                setFltrColIds(EJBUtil.integerToString(new Integer(rs
                        .getInt("pk_dynrepfilterdt"))));
                setFiltCol(rs.getString("dynrepfilterdt_col"));
                setFiltQf(rs.getString("dynrepfilterdt_qf"));
                setFiltData(rs.getString("dynrepfilterdt_data"));
                setFiltExclude(rs.getString("dynrepfilterdt_exclude"));
                setFiltBbrac(rs.getString("dynrepfilterdt_bbrac"));
                setFiltEbrac(rs.getString("dynrepfilterdt_ebrac"));
                setFiltOper(rs.getString("dynrepfilterdt_oper"));
                setFiltSeq(EJBUtil.integerToString(new Integer(rs
                        .getInt("dynrepfilterdt_seq"))));
            }

        } catch (SQLException e) {
            Rlog.fatal("dynrep",
                    "{SQLEXCEPTION IN dynrepDao:getFilterdetails(fltrId)}" + e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    // method to populate HashMap

    /**
     * Description of the Method
     *
     * @param repIdStr
     *            Description of the Parameter
     */
    public void populateHash(String repIdStr) {
        String repName = "";
        String repHdr = "";
        String repFtr = "";
        String repType = "";
        String repFilter = "";
        String shareWith = "";
        String selStudy = "";
        String repOrder = "";
        String repDesc = "";
        String selPat = "";
        String formId = "";
        HashMap Attributes = new HashMap();
        HashMap FldAttrLocal = new HashMap();
        HashMap SortAttrLocal = new HashMap();
        ;
        HashMap TypeAttrLocal = new HashMap();
        ;
        HashMap RepAttrLocal = new HashMap();
        ;
        HashMap FltrAttrLocal = new HashMap();
        ;

        ArrayList lfltrIds = new ArrayList();
        ArrayList lfltrNames = new ArrayList();
        //DynRepJB dynrepB = new DynRepJB();
        getFilter(repIdStr);
        lfltrIds = getFltrIds();
        lfltrNames = getFltrNames();
        int repId = (new Integer(repIdStr)).intValue();
        //dynrepB.setId(repId);
       // dynrepB.getDynRepDetails();
       // repName = dynrepB.getRepName();
        repName = (repName == null) ? "" : repName;
       // repHdr = dynrepB.getRepHdr();
        repHdr = (repHdr == null) ? "" : repHdr;
       // repFtr = dynrepB.getRepFtr();
        repFtr = (repFtr == null) ? "" : repFtr;
       // repDesc = dynrepB.getRepDesc();
        repDesc = (repDesc == null) ? "" : repDesc;
       // shareWith = dynrepB.getShareWith();
        shareWith = (shareWith == null) ? "" : shareWith;
        RepAttrLocal.put("repName", repName);
        RepAttrLocal.put("repHdr", repHdr);
        RepAttrLocal.put("repFtr", repFtr);
        RepAttrLocal.put("repDesc", repDesc);
        RepAttrLocal.put("shareWith", shareWith);

       // repType = dynrepB.getRepType();
        repType = (repType == null) ? "" : repType;
       // repFilter = dynrepB.getRepFilter();
        repFilter = (repFilter == null) ? "" : repFilter;
      //  selStudy = dynrepB.getStudyId();
        selStudy = (selStudy == null) ? "" : selStudy;
     //   selPat = dynrepB.getPerId();
        selPat = (selPat == null) ? "" : selPat;
     //   formId = dynrepB.getFormIdStr();
        formId = (formId == null) ? "" : formId;

        TypeAttrLocal.put("dynType", repType);
        TypeAttrLocal.put("studyId", selStudy);
        TypeAttrLocal.put("perId", selPat);
        TypeAttrLocal.put("formId", formId);
        TypeAttrLocal.put("repMode", "M");

        // get the form fields
        getMapData(formId);
        // dyndao.getMapData(formId);
        preProcessMap();
        ArrayList lmapCols = getMapCols();
        ArrayList lmapDispNames = getMapDispNames();

        // end getting form fields

     //   repOrder = dynrepB.getRepOrder();
        repOrder = (repOrder == null) ? "" : repOrder;
        SortAttrLocal.put("repOrder", repOrder);
        getReportDetails(repIdStr);
        FldAttrLocal.put("sessColId", getRepColId());
        FldAttrLocal.put("sessCol", getRepCol());
        FldAttrLocal.put("sessColSeq", getRepColSeq());
        FldAttrLocal.put("sessColDisp", getRepColDispName());
        FldAttrLocal.put("sessColWidth", getRepColWidth());
        FldAttrLocal.put("sessColFormat", getRepColFormat());
        FldAttrLocal.put("sessColName", getRepColName());
        FldAttrLocal.put("sessColType", getRepColType());
        FldAttrLocal.put("prevSessColId", getRepColId());

        if (lfltrIds != null) {
            if (lfltrIds.size() > 0) {
                FltrAttrLocal.put("MultiFltrIds", lfltrIds);
            }
            if (lfltrIds.size() > 0) {
                FltrAttrLocal.put("MultiFltrNames", lfltrNames);
            }
        }
        if (lmapCols != null) {

            if (lmapDispNames.size() > 0) {
                FltrAttrLocal.put("fldName", ((String[]) lmapDispNames
                        .toArray(new String[lmapDispNames.size()])));
            }
            if (lmapCols.size() > 0) {
                FltrAttrLocal.put("fldCol", ((String[]) lmapCols
                        .toArray(new String[lmapCols.size()])));
            }
        }
        Attributes.put("TypeAttr", TypeAttrLocal);
        Attributes.put("FldAttr", FldAttrLocal);
        Attributes.put("SortAttr", SortAttrLocal);
        Attributes.put("RepAttr", RepAttrLocal);
        Attributes.put("FltrAttr", FltrAttrLocal);
        setFldAttr(FldAttrLocal);
        setFltrAttr(FltrAttrLocal);
        setSortAttr(SortAttrLocal);
        setRepAttr(RepAttrLocal);
        setTypeAttr(TypeAttrLocal);

        // if (FltrAttrLocal.containsKey("MultiFltrIds"))
        // AttrLocalibutes.put("FltrAttrLocal",FltrAttrLocal);

        setAttributes(Attributes);

    }

    /**
     * Gets the reportDetails attribute of the DynRepDao object
     *
     * @param repIdStr
     *            Description of the Parameter
     */
    public void getReportDetails(String repIdStr) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ArrayList dataList = new ArrayList();
        ArrayList tempList = new ArrayList();
        ResultSet rs = null;
        String tempStr = "";
        String sql = "";
        int repId = (new Integer(repIdStr)).intValue();
        try {
            conn = getConnection();
            String temp = "";
            sql = "select pk_dynrepview,dynrepview_col,dynrepview_seq,dynrepview_width,dynrepview_format,dynrepview_dispname,dynrepview_colname,"
                    + " dynrepview_coldatatype,fk_form,useuniqueid,usedataval ,calcsum,calcper,repeat_number from er_dynrepview where fk_dynrep="
                    + repId + " order by dynrepview_seq ";
            Rlog.debug("dynrep", "sql in getReportDetails" + sql);
            pstmt = conn.prepareStatement(sql);
            Rlog.debug("dynrep", "pstmt in getReportDetails" + pstmt);
            rs = pstmt.executeQuery();
            Rlog.debug("dynrep", "rs in getReportDetails" + rs);
            while (rs.next()) {
                setRepColId((new Integer(rs.getInt("pk_dynrepview")))
                        .toString());
                setRepCol(rs.getString("dynrepview_col"));
                setRepColSeq((new Integer(rs.getInt("dynrepview_seq")))
                        .toString());
                setRepColWidth(rs.getString("dynrepview_width"));
                setRepColFormat(rs.getString("dynrepview_format"));
                setRepColDispName(rs.getString("dynrepview_dispname"));
                setRepColName(rs.getString("dynrepview_colname"));
                setRepColType(rs.getString("dynrepview_coldatatype"));
                setRepColFormId(rs.getString("fk_form"));
                setUseUniqueIds(rs.getString("useuniqueid"));
                setUseDataValues(rs.getString("usedataval"));
                setCalcSums(rs.getString("calcsum"));
                setCalcPers(rs.getString("calcper"));
                setFieldRepeatNumber(rs.getString("repeat_number"));

            }

        } catch (SQLException e) {
            Rlog.fatal("dynrep",
                    "{SQLEXCEPTION IN dynrepDao:getReportDetails()}" + e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets the reportDetails attribute of the DynRepDao object
     *
     * @param repIdStr
     *            Description of the Parameter
     * @param type
     *            Type of the report
     */
    /*
     * modified by Sonia Abrol,1/17/05 changed order by to order by
     * fk_form,dynrepview_seq
     */
    /*
     * modified by Sonia Abrol,1/27/05 added logic for form_type to handle core
     * lookup forms
     * modified by Sonia Abrol, 12/28/06, added logic for calculate Summary and calculate Percentage fields
     */
    public void getReportDetails(String repIdStr, String type) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ArrayList dataList = new ArrayList();
        ArrayList tempList = new ArrayList();
        ResultSet rs = null;
        String tempStr = "";
        String sql = "";
        int repId = (new Integer(repIdStr)).intValue();
        String repColFormType = "";
        String repColForm = "";

        try {
            conn = getConnection();
            String temp = "";
            sql = "select pk_dynrepview,dynrepview_col,dynrepview_seq,dynrepview_width,dynrepview_format,dynrepview_dispname,dynrepview_colname,"
                    + " dynrepview_coldatatype,fk_form,form_type,useuniqueid,usedataval ,calcsum,calcper,repeat_number from er_dynrepview where fk_dynrep="
                    + repId + " order by fk_form,dynrepview_seq ";


            pstmt = conn.prepareStatement(sql);

            rs = pstmt.executeQuery();
            Rlog.debug("dynrep", "rs in getReportDetails" + rs);
            while (rs.next()) {
                setRepColId((new Integer(rs.getInt("pk_dynrepview")))
                        .toString());
                setRepCol(rs.getString("dynrepview_col"));
                setRepColSeq((new Integer(rs.getInt("dynrepview_seq")))
                        .toString());
                setRepColWidth(rs.getString("dynrepview_width"));
                setRepColFormat(rs.getString("dynrepview_format"));
                setRepColDispName(rs.getString("dynrepview_dispname"));
                setRepColName(rs.getString("dynrepview_colname"));
                setRepColType(rs.getString("dynrepview_coldatatype"));

                repColForm = rs.getString("fk_form");
                repColFormType = rs.getString("form_type");

                setUseUniqueIds(rs.getString("useuniqueid"));
                setUseDataValues(rs.getString("usedataval"));

                setCalcSums(rs.getString("calcsum"));
                setCalcPers(rs.getString("calcper"));

                setFieldRepeatNumber(rs.getString("repeat_number"));


                if (StringUtil.isEmpty(repColFormType)) {
                    repColFormType = "F"; // default to app forms
                }

                if (repColFormType.equals("C")) // core table lookup
                {
                    repColForm = repColFormType + repColForm;
                }
                setRepColFormId(repColForm);

            }

        } catch (SQLException e) {
            Rlog.fatal("dynrep",
                    "{SQLEXCEPTION IN dynrepDao:getReportDetails()}" + e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * Sets the filterDetails attribute of the DynRepDao object
     *
     * @param dynDao
     *            The new filterDetails value
     * @param mode
     *            The new filterDetails value
     * @return Description of the Return Value
     */
    /*
     * Modified by Sonia Abrol, 01/17/05, added code to delete existing filter
     * details if 'column' is deselected
     */
    public int setFilterDetails(DynRepDao dynDao, String mode) {
        ArrayList afiltCol = dynDao.getFiltCol();
        ArrayList afiltQf = dynDao.getFiltQf();
        ArrayList afiltData = dynDao.getFiltData();
        ArrayList afiltExclude = dynDao.getFiltExclude();
        ArrayList afiltBbrac = dynDao.getFiltBbrac();
        ArrayList afiltEbrac = dynDao.getFiltEbrac();
        ArrayList afiltOper = dynDao.getFiltOper();
        ArrayList afiltSeq = dynDao.getFiltSeq();
        ArrayList afiltColId = dynDao.getFltrColIds();
        String afiltName = dynDao.getFiltName();
        String afiltId = dynDao.getFiltId();
        String aipAdd = dynDao.getIpAdd();
        String ausr = dynDao.getUsr();
        String arepId = dynDao.getRepId();

        String sql = "";

        String filtCol = "";
        PreparedStatement pstmt = null;
        PreparedStatement delPstmt = null;

        Connection conn = null;
        int id = 0;
        int filterPk = 0;
        int status = 0;
        filterPk = EJBUtil.stringToNum(afiltId);
        try {
            conn = getConnection();
            String temp = "";

            delPstmt = conn
                    .prepareStatement("Delete from er_dynrepfilterdt where pk_dynrepfilterdt = ?");

            int lfltrColId = -1;
            for (int i = 0; i < afiltColId.size(); i++) {
                lfltrColId = EJBUtil.stringToNum((String) afiltColId.get(i));
                if (lfltrColId == 0) {
                    sql = "insert into er_dynrepfilterdt(dynrepfilterdt_col,dynrepfilterdt_qf,dynrepfilterdt_data,dynrepfilterdt_exclude,"
                            + "dynrepfilterdt_bbrac,dynrepfilterdt_ebrac,dynrepfilterdt_oper,dynrepfilterdt_seq,fk_dynrepfilter,"
                            + " creator,ip_add,pk_dynrepfilterdt) values (?,?,?,?,?,?,?,?,?,?,?,?)";
                    id = GenerateId.getId("seq_er_dynrepfilterdt", conn);
                } else if (lfltrColId > 0) {
                    sql = "update er_dynrepfilterdt set dynrepfilterdt_col=?,dynrepfilterdt_qf=?,dynrepfilterdt_data=?,dynrepfilterdt_exclude=?,"
                            + "dynrepfilterdt_bbrac=?,dynrepfilterdt_ebrac=?,dynrepfilterdt_oper=?,dynrepfilterdt_seq=?,fk_dynrepfilter=?,"
                            + " last_modified_by=?,ip_add=? where pk_dynrepfilterdt=?";
                    id = lfltrColId;
                }
                Rlog.debug("dynrep", "sql in setFilterDetails" + sql);
                pstmt = conn.prepareStatement(sql);
                Rlog.debug("dynrep", "pstmt in setFilterDetails" + pstmt);

                pstmt.clearParameters();

                filtCol = (String) afiltCol.get(i);
                if (filtCol == null) {
                    filtCol = "";
                }
                if (filtCol.length() == 0) {

                    // if it was an existing filter detail and user tried to
                    // remove it
                    // by deselecting column name
                    if (lfltrColId > 0) {
                        Rlog
                                .debug("dynrep",
                                        "No column name selected, have to delete the filter");
                        delPstmt.setInt(1, lfltrColId);
                        delPstmt.executeUpdate();
                    }

                    continue;
                }
                Rlog.debug("dynrep", "loop setFilterDetails" + i);
                Rlog.debug("dynrep", "fileCol in setFilterDetails" + filtCol);
                pstmt.setString(1, filtCol);
                Rlog.debug("dynrep", "filtQFin setFilterDetails"
                        + (String) afiltQf.get(i));
                pstmt.setString(2, (String) afiltQf.get(i));
                Rlog.debug("dynrep", "filtdata setFilterDetails"
                        + (String) afiltData.get(i));
                pstmt.setString(3, (String) afiltData.get(i));
                Rlog.debug("dynrep", "filtexclude  in setFilterDetails"
                        + (String) afiltExclude.get(i));
                pstmt.setString(4, (String) afiltExclude.get(i));
                Rlog.debug("dynrep", "filBBracket  in setFilterDetails"
                        + (String) afiltBbrac.get(i));
                pstmt.setString(5, (String) afiltBbrac.get(i));
                Rlog.debug("dynrep", "filEBracket  in setFilterDetails"
                        + (String) afiltEbrac.get(i));
                pstmt.setString(6, (String) afiltEbrac.get(i));
                Rlog.debug("dynrep", "filoperracket  in setFilterDetails"
                        + (String) afiltOper.get(i));
                pstmt.setString(7, (String) afiltOper.get(i));
                // pstmt.setInt(8,((Integer)afiltSeq.get(i)).intValue());
                pstmt.setInt(8, i);
                Rlog
                        .debug("dynrep", "filterPk  in setFilterDetails"
                                + filterPk);
                pstmt.setInt(9, filterPk);
                // Rlog.debug("dynrep","filtername in
                // setFilterDetails"+afiltName);
                // pstmt.setString(10,afiltName);
                Rlog.debug("dynrep", "usrame  in setFilterDetails" + ausr);
                pstmt.setInt(10, EJBUtil.stringToNum(ausr));
                Rlog.debug("dynrep", "iop  in setFilterDetails" + aipAdd);
                pstmt.setString(11, aipAdd);
                Rlog.debug("dynrep", "id  in setFilterDetails" + id);
                pstmt.setInt(12, id);
                pstmt.executeUpdate();

            }
            conn.commit();

        } catch (Exception e) {
            Rlog.fatal("dynrep", "{EXCEPTION IN dynrepDao:setFilterDetails()}"
                    + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -1;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (delPstmt != null) {
                    delPstmt.close();
                }

            } catch (Exception e) {
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return 1;
    }

    /**
     * Description of the Method
     *
     * @param fltrIds
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    /*
     * Modified by Sonia Abrol, 05/31/05, to delete child filter rows from
     * er_dynrepfilter
     */
    public int removeFilters(ArrayList fltrIds) {
        int count = 0;
        String fltrId = "";
        String fltrIdList = "";
        String sql = "";
        String sqlChild = "";
        PreparedStatement pstmt = null;
        Connection conn = null;
        int id = 0;
        int filterPk = 0;
        int status = 0;
        if (fltrIds != null) {
            count = fltrIds.size();
        }
        for (int i = 0; i < count; i++) {
            fltrId = (String) fltrIds.get(i);
            fltrId = (fltrId == null) ? "" : fltrId;
            if (fltrIdList.length() == 0) {
                fltrIdList = fltrIdList + fltrId;
            } else {
                fltrIdList = fltrIdList + "," + fltrId;
            }
        }
        fltrIdList = "(" + fltrIdList + ")";
        try {
            conn = getConnection();
            String temp = "";

            sql = "delete from er_dynrepfilterdt where fk_dynrepfilter in"
                    + fltrIdList;

            pstmt = conn.prepareStatement(sql);
            pstmt.clearParameters();
            pstmt.executeUpdate();

            sqlChild = "delete from er_dynrepfilter where fk_parentfilter in"
                    + fltrIdList;

            pstmt = conn.prepareStatement(sqlChild);
            pstmt.clearParameters();
            pstmt.executeUpdate();

            conn.commit();

        } catch (Exception e) {
            Rlog
                    .fatal("dynrep", "{EXCEPTION IN dynrepDao:removeFilters()}"
                            + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -1;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return 1;
    }

    // this method is not being used right now,it generates the filter string
    // from selection on main screen
    /**
     * Sets the filter attribute of the DynRepDao object
     */
    public void setFilter() {
        ArrayList lfiltCol = getFiltCol();
        ArrayList lfiltQf = getFiltQf();
        ArrayList lfiltData = getFiltData();
        ArrayList lfiltExclude = getFiltExclude();
        ArrayList lfiltBbrac = getFiltBbrac();
        ArrayList lfiltEbrac = getFiltEbrac();
        ArrayList lfiltOper = getFiltOper();
        ArrayList lfiltSeq = getFiltSeq();
        StringTokenizer st = null;
        String formId = getFkForm();
        formId = (formId == null) ? "" : formId;
        int startcount = 0;
        int endcount = 0;
        int totcount = 0;
        String filterTemp = "";
        String filter = "";
        String criteriaStr = "";
        String extend = "";
        String tempStr = "";
        String startBrac = "";
        String fldName = "";
        String criteria = "";
        String fltdata = "";
        String endBrac = "";
        totcount = lfiltCol.size();
        for (int i = 0; i < totcount; i++) {
            filterTemp = "";
            criteriaStr = "";
            if (((String) lfiltExclude.get(i)) != null) {
                continue;
            }
            startBrac = (String) lfiltBbrac.get(i);
            startBrac = (startBrac == null) ? "" : startBrac;
            if (startBrac.length() > 0) {
                startcount++;
            }
            fldName = (String) lfiltCol.get(i);
            fldName = (fldName == null) ? "" : fldName;
            criteria = (String) lfiltQf.get(i);
            criteria = (criteria == null) ? "" : criteria;
            fltdata = (String) lfiltData.get(i);
            fltdata = (fltdata == null) ? "" : fltdata;
            endBrac = (String) lfiltEbrac.get(i);
            endBrac = (endBrac == null) ? "" : endBrac;
            if (endBrac.length() > 0) {
                endcount++;
            }
            if (i > 0) {
                extend = (String) lfiltOper.get(i - 1);
            }
            if ((fldName.length() > 0)
                    && (fltdata.length() == 0 && criteria.length() == 0)) {
                continue;
            }
            if ((fldName.length() == 0 && fltdata.length() == 0)
                    && (criteria.length() > 0)) {
                continue;
            }
            if ((fldName.length() == 0 && criteria.length() == 0)
                    && (fltdata.length() > 0)) {
                continue;
            }
            if (startBrac.length() > 0) {
                filterTemp = " " + extend + " (";
            } else {
                filterTemp = " " + extend + " ";
            }
            if ((criteria.trim()).equals("contains")) {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + st.nextToken()
                                    + ") like lower('%" + fltdata + "%')";
                        } else {
                            criteriaStr = criteriaStr + " or lower("
                                    + st.nextToken() + ") like lower('%"
                                    + fltdata + "%')";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") like lower('%"
                            + fltdata + "%')";
                }
            }
            if (criteria.equals("isequalto")) {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {

                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + st.nextToken()
                                    + ") = lower('" + fltdata + "')";
                        } else {
                            criteriaStr = criteriaStr + " or lower("
                                    + st.nextToken() + ") = lower('" + fltdata
                                    + "')";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") = lower('" + fltdata
                            + "')";
                }
            }
            if (criteria.equals("start")) {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + st.nextToken()
                                    + ") like lower('" + fltdata + "%')";
                        } else {
                            criteriaStr = criteriaStr + " or lower("
                                    + st.nextToken() + ") like lower('"
                                    + fltdata + "%')";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") like lower('"
                            + fltdata + "%')";
                }
            }
            if (criteria.equals("ends")) {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + st.nextToken()
                                    + ") like lower('%" + fltdata + "')";
                        } else {
                            criteriaStr = criteriaStr + " or lower("
                                    + st.nextToken() + ") like lower('%"
                                    + fltdata + "')";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") like lower('%"
                            + fltdata + "')";
                }
            }
            if (criteria == "isgt") {
                st = new StringTokenizer(fldName, "|");
                while (st.hasMoreTokens()) {
                    if (criteriaStr.length() == 0) {
                        criteriaStr = "(lower(" + st.nextToken()
                                + ") > lower('%" + fltdata + "%')";
                    } else {
                        criteriaStr = criteriaStr + " or lower("
                                + st.nextToken() + ") > lower('%" + fltdata
                                + "%')";
                    }
                }
                criteriaStr = criteriaStr + ")";
            } else {
                criteriaStr = "lower(" + fldName + ") > lower('%" + fltdata
                        + "%')";
            }

            if (criteria == "islt") {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + st.nextToken()
                                    + ") < lower('%" + fltdata + "%')";
                        } else {
                            criteriaStr = criteriaStr + " or lower("
                                    + st.nextToken() + ") < lower('%" + fltdata
                                    + "%')";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") < lower('%" + fltdata
                            + "%')";
                }
            }
            if (criteria == "isgte") {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + st.nextToken()
                                    + ") >= lower('%" + fltdata + "%')";
                        } else {
                            criteriaStr = criteriaStr + " or lower("
                                    + st.nextToken() + ") >= lower('%"
                                    + fltdata + "%')";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") >= lower('%"
                            + fltdata + "%')";
                }
            }
            if (criteria == "islte") {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + st.nextToken()
                                    + ") <= lower('%" + fltdata + "%')";
                        } else {
                            criteriaStr = criteriaStr + "or lower("
                                    + st.nextToken() + ") <= lower('%"
                                    + fltdata + "%')";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") <= lower('%"
                            + fltdata + "%')";
                }
            }

            if (criteria == "first") {
                criteriaStr = "filldate=(select min(filldate) from er_formslinear where fk_form="
                        + formId + ")";
            }
            if (criteria == "latest") {
                criteriaStr = "filldate=(select max(filldate) from er_formslinear where fk_form="
                        + formId + ")";
            }

            if (criteria == "lowest") {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        tempStr = st.nextToken();
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + tempStr
                                    + ") =(select min(lower(" + tempStr
                                    + ")) from er_formslinear where fk_form="
                                    + formId + ")";
                        } else {
                            criteriaStr = criteriaStr + " or lower(" + tempStr
                                    + ") =(select min(lower(" + tempStr
                                    + ")) from er_formslinear where fk_form="
                                    + formId + ")";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") =(select min(lower("
                            + fldName + ")) from er_formslinear where fk_form="
                            + formId + ")";
                }
                tempStr = "";
            }
            if (criteria == "highest") {
                if (fldName.indexOf("|") > -1) {
                    st = new StringTokenizer(fldName, "|");
                    while (st.hasMoreTokens()) {
                        tempStr = st.nextToken();
                        if (criteriaStr.length() == 0) {
                            criteriaStr = "(lower(" + tempStr
                                    + ") =(select max(lower(" + tempStr
                                    + ")) from er_formslinear where fk_form="
                                    + formId + ")";
                        } else {
                            criteriaStr = criteriaStr + " or lower(" + tempStr
                                    + ") =(select max(lower(" + tempStr
                                    + ")) from er_formslinear where fk_form="
                                    + formId + ")";
                        }
                    }
                    criteriaStr = criteriaStr + ")";
                } else {
                    criteriaStr = "lower(" + fldName + ") =(select max(lower("
                            + fldName + ")) from er_formslinear where fk_form="
                            + formId + ")";
                }
            }
            if (criteriaStr.length() > 0) {
                filter = filter + filterTemp + criteriaStr;
                if (endBrac.length() > 0) {
                    filter = filter + " )";
                }
            }
            Rlog.debug("dynrep", "Filter in dynrepDao is" + filter);
            setFltrStr(filter);

        }
        // end for

    }

    // end Method

    // This method is created to retrieve section names of selected fields
    // before displaying.

    /**
     * Gets the sectionDetails attribute of the DynRepDao object
     *
     * @param paramCols
     *            Description of the Parameter
     * @param formId
     *            Description of the Parameter
     */
    public void getSectionDetails(ArrayList paramCols, String formId) {
        String colStr = "";
        String tempVal = "";
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if ( ! formId.startsWith("C") )
        {
		        for (int count = 0; count < paramCols.size(); count++) {
		            tempVal = (String) paramCols.get(count);
		            if (tempVal.indexOf("|") > 0) {
		                tempVal = tempVal.substring(0, (tempVal.indexOf("|")));
		            }

		            if (colStr.length() == 0) {
		                colStr = "'" + tempVal + "'";
		            } else {
		                colStr = colStr + ",'" + tempVal + "'";
		            }
		        }
		        colStr = "(" + colStr + ")";
		        String sql = "select pk_mp,mp_mapcolname,mp_origsysid,mp_dispname,mp_formtype,mp_flddatatype,mp_pksec,mp_secname,mp_pkfld,mp_fldcharsno,mp_uid,mp_systemid,formsec_repno from er_mapform ,er_formsec where mp_mapcolname in "
		                + colStr
		                + " and fk_form="
		                + formId
		                + " and pk_formsec = mp_pksec  ORDER BY mp_pksec, mp_sequence";
		        Rlog.debug("dynrep", "SQL in dynrepdao:getsectiondetails" + sql);
		        try {
		            conn = getConnection();
		            pstmt = conn.prepareStatement(sql);
		            Rlog.debug("dynrep", "pstmt" + pstmt);
		            rs = pstmt.executeQuery();
		            while (rs.next()) {
		                setMapCols(rs.getString("mp_mapcolname"));
		                String origSysId = rs.getString("mp_origsysid");
						setMapOrigIds(origSysId);
                    	if ("er_def_date_01".equals(origSysId)){
                    		setMapDispNames(LC.L_Data_EntryDate);
		                }else{               
		                	setMapDispNames(rs.getString("mp_dispname"));
		                }
		                setMapFormTypes(rs.getString("mp_formtype"));
		                setMapColTypes(rs.getString("mp_flddatatype"));
		                setMapColSecIds(rs.getString("mp_pksec"));
		                setMapColSecNames(rs.getString("mp_secname"));
		                setMapColIds(rs.getString("mp_pkfld"));
		                setMapColLens(rs.getString("mp_fldcharsno"));
		                setMapUids(rs.getString("mp_uid"));
		                setMapColSecRepNo(rs.getString("formsec_repno"));
		                // it will be filtered in preProcessMap
		                setMapSysIds(rs.getString("mp_systemid"));
		            }

        } catch (SQLException e) {
            Rlog.fatal("dynrep",
                    "{SQLEXCEPTION IN dynrepDao:setSectionDetails()}" + e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

       } // formID check
    }

    // end method getSectionDetails

    /**
     * Method to retrieve information of multi report from the database and
     * populate report container
     *
     * @param repIdStr
     *            Report id as String
     */
    public void fillReportContainer(String repIdStr) {
      //  ReportHolder container = new ReportHolder();
        String[] repOrderArray = null;
        String repName = "";
        String repHdr = "";
        String repFtr = "";
        String repType = "";
        String repFilter = "";
        String shareWith = "";
        String selStudy = "";
        String repOrder = "";
        String repDesc = "";
        String selPat = "";
        String formId = "";
        String formName = "";
        ArrayList lfltrIds = new ArrayList();
        ArrayList lfltrNames = new ArrayList();
       // DynRepJB dynrepB = new DynRepJB();

        int mainFilterCounter = 0;
        int choppedFilterCounter = 0;

        String mainFilterId = "";
        String mainFilterString = "";
        String mainFilterName = "";

        String dataOrderStr = "";
        String tempStr = "";

        String formIdCompleteStr = "";
        String[] formArrayComplete;

        // get filters associated with the report

        getFilter(repIdStr);

        // get only those filter rows where fk_form is null, these are main
        // filter rows

        Rlog
                .debug("dynrep",
                        "*************Starting to get Report Filters*******************");
        Rlog.debug("dynrep", "*************fltrFormIds.size()**"
                + fltrFormIds.size());

        while (mainFilterCounter >= 0
                && mainFilterCounter <= fltrFormIds.size()) {
            mainFilterCounter = fltrFormIds.indexOf("0", mainFilterCounter);

            Rlog.debug("dynrep", "*************mainFilterCounter)**"
                    + mainFilterCounter);

            if (mainFilterCounter >= 0) {
               // FilterContainer fltrContainer = new FilterContainer();

                mainFilterId = (String) fltrIds.get(mainFilterCounter);
                mainFilterString = (String) fltrStrs.get(mainFilterCounter);
                mainFilterName = (String) fltrNames.get(mainFilterCounter);

                Rlog.debug("dynrep", "*************mainFilterId)**"
                        + mainFilterId);
                Rlog.debug("dynrep", "*************mainFilterString)**"
                        + mainFilterString);

               // fltrContainer.setFilterId(mainFilterId);
               // fltrContainer.setFilterName(mainFilterName);

               // fltrContainer.setFilterDateRangeType((String) flrDateRangeType
                      //  .get(mainFilterCounter));
                //fltrContainer.setFilterDateRangeTo((String) fltrDateRangeTo
                        //.get(mainFilterCounter));
             //   fltrContainer.setFilterDateRangeFrom((String) fltrDateRangeFrom
                   //     .get(mainFilterCounter));

                // for multiple filters, set the value to container
                //container.setFilterIds(mainFilterId);
               // container.setFilterNames(mainFilterName);

               // fltrContainer.setFilterStr(mainFilterString);
                // already saved filter, so mode is "M";added to support
                // multiple filters
               // fltrContainer.setFilterMode("M");

                // get filter details for this filter

                // resetFilterDetails(); //reset filter detail arraylists.

                DynRepDao dRepDaoFilter = new DynRepDao();

                dRepDaoFilter.getFilterDetails(mainFilterId);

                // set the details on the FilterContainer obj

            /*    fltrContainer.setFilterDbId(dRepDaoFilter.getFltrColIds());
                fltrContainer.setFilterCols(dRepDaoFilter.getFiltCol());
                fltrContainer.setFilterQualifier(dRepDaoFilter.getFiltQf());
                fltrContainer.setFilterData(dRepDaoFilter.getFiltData());
                fltrContainer.setFilterExclude(dRepDaoFilter.getFiltExclude());
                fltrContainer.setFilterBbrac(dRepDaoFilter.getFiltBbrac());
                fltrContainer.setFilterEbrac(dRepDaoFilter.getFiltEbrac());
                fltrContainer.setFilterOper(dRepDaoFilter.getFiltOper());*/

                // get chopped filter data for this main filter

                Rlog.debug("dynrep",
                        "*************fltrParentFilterIds.size()**"
                                + fltrParentFilterIds.size());

                while (choppedFilterCounter >= 0
                        && choppedFilterCounter <= fltrParentFilterIds.size()) {
                    choppedFilterCounter = fltrParentFilterIds.indexOf(
                            mainFilterId, choppedFilterCounter);

                    if (choppedFilterCounter >= 0) {
                       // fltrContainer.setFormIds((String) fltrFormIds
                            //    .get(choppedFilterCounter));
                       // fltrContainer.setChoppedFilters((String) fltrStrs
                       //         .get(choppedFilterCounter));
                      //  fltrContainer.setChoppedFilterIds((String) fltrIds
                             //   .get(choppedFilterCounter));

                        Rlog
                                .debug(
                                        "dynrep",
                                        "*************fltrFormIds.get(choppedFilterCounter)**"
                                                + fltrFormIds
                                                        .get(choppedFilterCounter));
                        choppedFilterCounter++;

                    }

                } // end of while for chopped filter

                choppedFilterCounter = 0;
                mainFilterCounter++;

                // add the FilterContainer to report holder object

                Rlog.debug("dynrep", "********STARTING DISPLAY *****");
               // fltrContainer.Display();
                Rlog.debug("dynrep", "********END DISPLAY *****");
              //  container.setFilterObject(mainFilterId, fltrContainer);

            }
        }

        // lfltrIds=getFltrIds();
        // lfltrNames=getFltrNames();
        int repId = (new Integer(repIdStr)).intValue();
      //  dynrepB.setId(repId);
      //  dynrepB.getDynRepDetails();

      //  repName = dynrepB.getRepName();
        repName = (repName == null) ? "" : repName;



       // repHdr = dynrepB.getRepHdr();
        repHdr = (repHdr == null) ? "" : repHdr;

       // repFtr = dynrepB.getRepFtr();
        repFtr = (repFtr == null) ? "" : repFtr;

      //  repDesc = dynrepB.getRepDesc();
        repDesc = (repDesc == null) ? "" : repDesc;

       // repOrder = dynrepB.getRepOrder();
        repOrder = (repOrder == null) ? "" : repOrder;

      //  shareWith = dynrepB.getShareWith();
        shareWith = (shareWith == null) ? "" : shareWith;

      //  repType = dynrepB.getRepType();
        repType = (repType == null) ? "" : repType;

      //  repFilter = dynrepB.getRepFilter();
        repFilter = (repFilter == null) ? "" : repFilter;

      //  selStudy = dynrepB.getStudyId();
        selStudy = (selStudy == null) ? "" : selStudy;

      //  selPat = dynrepB.getPerId();
        selPat = (selPat == null) ? "" : selPat;

      //  formIdCompleteStr = dynrepB.getFormIdStr();
        formIdCompleteStr = (formIdCompleteStr == null) ? ""
                : formIdCompleteStr;

      //  container.setReportType(repType);

        // split the formId string, separate the core form lookup ids and create
        // new formID string

        if (formIdCompleteStr.indexOf(";") >= 0) {
            formArrayComplete = StringUtil.strSplit(formIdCompleteStr, ";",
                    false);
            for (int i = 0; i < formArrayComplete.length; i++) {

                if (!formArrayComplete[i].startsWith("C")) {
                    formId = formId + ";" + formArrayComplete[i];
                } else {

              //      container
                //            .setSelectedCoreLookupformIds(formArrayComplete[i]);
                }

            }

            if (formId.startsWith(";")) {
                formId = formId.substring(1);
            }
            Rlog.debug("dynrep", "********formID*****" + formId + "*");

        } else {
            if (!formIdCompleteStr.startsWith("C")) {
                formId = formIdCompleteStr;
            } else {
              //  container.setSelectedCoreLookupformIds(formIdCompleteStr);
            }
            Rlog.debug("dynrep", "in else********formID*****" + formId + "*");
        }

      //  Rlog.debug("dynrep", "container.getSelectedCoreLookupformIds"
           //     + container.getSelectedCoreLookupformIds().toString() + "**");
        // ///////////////////////////////

        ArrayList tempList = new ArrayList();

        if (!StringUtil.isEmpty(formId)) {
            getFormNames(formId, ";");

            tempList = getFormNameList();
            for (int i = 0; i < tempList.size(); i++) {
                if (formName.length() == 0)
                    formName = (String) tempList.get(i);
                else
                    formName = formName + "{VELSEP}" + (String) tempList.get(i);
            }
        }

       /* container.setReportName(repName);
        container.setReportHeader(repHdr);
        container.setReportFooter(repFtr);
        container.setReportDesc(repDesc);
        container.setShareWith(shareWith);

        container.setStudyId(selStudy);
        container.setPatientId(selPat);
        container.setFormIdStr(formId);
        container.setFormNameStr(formName);
        container.setReportMode("M");
        container.setReportId(repIdStr);
        container.setRepUseUniqueId(dynrepB.getRepUseUniqueId());
        container.setRepUseDataValue(dynrepB.getRepUseDataValue());

        container.DisplayDetail();

        ArrayList formIds = container.getFormIds();*/

        /* Create sort objects for each form */
        if (repOrder.length() > 0) {
            String[] dataOrderArray = StringUtil.strSplit(repOrder, "[VELSEP]",
                    false);
            String sortFormId = "";
            int sortFormIdPos = -1;

            if (dataOrderArray != null) {
                for (int i = 0; i < dataOrderArray.length; i++) {
                    dataOrderStr = dataOrderArray[i];

                    Rlog.debug("dynrep", "dataOrderStr" + dataOrderStr);

                    if ((dataOrderStr.trim()).indexOf("NS[*]") >= 0)
                        continue;
                    //SortContainer sortContainer = new SortContainer();

                    dataOrderStr = StringUtil.replace(dataOrderStr, "[*]", "*"); // for
                    // formid

                    // get the sortFormId

                    sortFormIdPos = dataOrderStr.indexOf("*");
                    if (sortFormIdPos >= 0) {
                        sortFormId = dataOrderStr.substring(sortFormIdPos + 1);
                        dataOrderStr = dataOrderStr.substring(0, sortFormIdPos);
                        Rlog.debug("dynrep", "dataOrderStr,sortFormId"
                                + sortFormId);
                        Rlog.debug("dynrep", "dataOrderStr,after sortFormId"
                                + dataOrderStr);

                    } else {
                        //sortFormId = (String) formIds.get(i); // backward
                        // compatability
                        Rlog.debug("dynrep", "old string,old form style"
                                + sortFormId);
                    }

                    //sortContainer.setSortStr(dataOrderStr);
                    Rlog.debug("dynrep", "processed dataOrderStr"
                            + dataOrderStr);
                    // break the sort string to respective arraylists

                    ArrayList tempName = new ArrayList();
                    ArrayList tempOrder = new ArrayList();
                    // [$$] is replaced with ~ because of imporoper token
                    // results from tokenzer using [$$]
                    dataOrderStr = StringUtil
                            .replace(dataOrderStr, "[$$]", "~");

                    StringTokenizer st = new StringTokenizer(dataOrderStr, "~");
                    StringTokenizer stTemp = null;
                    while (st.hasMoreTokens()) {
                        tempStr = (String) st.nextToken();
                        if ((tempStr.trim()).length() > 3) {
                            tempName.add(tempStr.substring(0, tempStr
                                    .indexOf("[$]")));
                            tempOrder.add(tempStr.substring((tempStr
                                    .indexOf("[$]")) + 3));
                        }
                    }

                   /* sortContainer.setSortFields(tempName);
                    sortContainer.setSortOrderBy(tempOrder);

                    container.setSortObject(sortFormId, sortContainer);*/
                }// end for

            }
        }// if for (repOrder.length()>0)

        /* End create Sort Object */

        /* Create Field Object for each form */
        getReportDetails(repIdStr, "M");
       // FieldContainer fldContainer = new FieldContainer();
        ArrayList tempFormIds = getRepColFormId();
        Rlog.debug("dynrep", "Form List" + tempFormIds);
       // if (tempFormIds.size() > 0)
        //    fldContainer = new FieldContainer();
        String tempFormId = "", prevFormId = "";
        String ignoreFilter = "";
        for (int i = 0; i < tempFormIds.size(); i++) {
            tempFormId = (String) tempFormIds.get(i);
            Rlog.debug("dynrep", "TempFormId " + tempFormId
                    + " AND prevFormId " + prevFormId);
            if (!tempFormId.equals(prevFormId)) {

                // get the full column list for the form

                //DynRepDao dyndao = dynrepB.getMapData(prevFormId);
                //ignoreFilter = dyndao.getIgnoreFilters();
                if (!StringUtil.isEmpty(ignoreFilter)) {
                //    container.setIgnoreFilters(prevFormId, ignoreFilter);
                }
                ArrayList mapCols = new ArrayList();
                ArrayList mapDispNames = new ArrayList();
                ArrayList mapColTypes = new ArrayList();
                ArrayList mapColPks = new ArrayList();
                ArrayList mapColTypesNoRepeat = new ArrayList();


                Rlog.debug("dynrep", "test 1...after dynrepdao.getMapData");

                if (!prevFormId.startsWith("C")) {

                 //   dyndao.preProcessMap();

                }
              /*  mapCols = dyndao.getMapCols();
                mapDispNames = dyndao.getMapDispNames();
                mapColTypes = dyndao.getMapColTypes();
                mapColPks = dyndao.getMapColIds();
                mapColTypesNoRepeat = dyndao.getMapColTypesNoRepeatFields();
*/

                Rlog.debug("dynrep", "test 4...after mapCols" + mapCols.size());

               /* fldContainer.setFieldList(mapCols);
                fldContainer.setFieldNameList(mapDispNames);
                fldContainer.setFieldTypeList(mapColTypes);
                fldContainer.setFieldPkList(mapColPks);
                fldContainer.setFieldTypesNoRepeatFields(mapColTypesNoRepeat);

                fldContainer.setPkTableName(dyndao.getMapTableName());
                fldContainer.setMainFilterColName(dyndao.getMapFilterCol());
                fldContainer.setPkColName(dyndao.getMapPrimaryKeyCol());
                fldContainer.setMapStudyColumn(dyndao.getMapStudyColumn());
                fldContainer.setMapColSecRepNo( dyndao.getMapColSecRepNo());

                fldContainer.setFieldKeyword(dyndao.getFieldKeyword());

                if (!StringUtil.isEmpty(fldContainer.getMapStudyColumn())) {
                    container.setCheckForStudyRights(true);
                }
                container.setFldObjects(prevFormId, fldContainer);

                Rlog.debug("dynrep", "Create new Field Object");
                fldContainer = new FieldContainer();
            }
            fldContainer.setFieldDbId((String) repColId.get(i));
            fldContainer.setFieldPrevColId((String) repColId.get(i));
            fldContainer.setFieldColId((String) repCol.get(i));
            fldContainer.setFieldSequence((String) repColSeq.get(i));
            fldContainer.setFieldDisplayName((String) repColDispName.get(i));
            fldContainer.setFieldWidth((String) repColWidth.get(i));
            fldContainer.setFieldFormat((String) repColFormat.get(i));
            fldContainer.setFieldName((String) repColName.get(i));
            fldContainer.setFieldType((String) repColType.get(i));

            fldContainer.setUseUniqueIds((String) useUniqueIds.get(i));
            fldContainer.setUseDataValues((String) useDataValues.get(i));
            fldContainer.setCalcPers((String) calcPers.get(i));
            fldContainer.setCalcSums((String) calcSums.get(i));

            fldContainer.setFieldRepeatNumber( (String) fieldRepeatNumber.get(i));
*/
            prevFormId = tempFormId;
            /* run a check for last form/data row */
        }

        // get the full column list for the form
        //DynRepDao dyndao = dynrepB.getMapData(prevFormId);

      //  ignoreFilter = dyndao.getIgnoreFilters();
        if (!StringUtil.isEmpty(ignoreFilter)) {
           // container.setIgnoreFilters(prevFormId, ignoreFilter);
        }

        Rlog.debug("dynrep", "test 5...after dynrepdao.getMapData");

        ArrayList mapCols = new ArrayList();
        ArrayList mapDispNames = new ArrayList();
        ArrayList mapColTypes = new ArrayList();
        ArrayList mapColPks = new ArrayList();
        ArrayList mapColTypesNoRepeat = new ArrayList();

        if (!prevFormId.startsWith("C")) {
            Rlog.debug("dynrep", "test 6...before dynrepdao.preProcessMap");
            //dyndao.preProcessMap();
            Rlog.debug("dynrep", "test 7...after dynrepdao.preProcessMap");
        }

       // mapCols = dyndao.getMapCols();
       // mapDispNames = dyndao.getMapDispNames();
       // mapColTypes = dyndao.getMapColTypes();
      //  mapColPks = dyndao.getMapColIds();
       // mapColTypesNoRepeat = dyndao.getMapColTypesNoRepeatFields();

        Rlog.debug("dynrep", "test 8...after mapCols" + mapCols.size());
        }

        /*fldContainer.setFieldList(mapCols);
        fldContainer.setFieldNameList(mapDispNames);
        fldContainer.setFieldTypeList(mapColTypes);
        fldContainer.setFieldPkList(mapColPks);
        fldContainer.setFieldTypesNoRepeatFields(mapColTypesNoRepeat);

        fldContainer.setPkTableName(dyndao.getMapTableName());
        fldContainer.setMainFilterColName(dyndao.getMapFilterCol());
        fldContainer.setPkColName(dyndao.getMapPrimaryKeyCol());
        fldContainer.setMapStudyColumn(dyndao.getMapStudyColumn());
        fldContainer.setMapColSecRepNo( dyndao.getMapColSecRepNo());

        fldContainer.setFieldKeyword(dyndao.getFieldKeyword());

        if (!StringUtil.isEmpty(fldContainer.getMapStudyColumn())) {
            container.setCheckForStudyRights(true);
        }

        container.setFldObjects(prevFormId, fldContainer);
        

        setReportContainer(container);
*/
    }

    /**
     * Gets the formNames attribute of the DynRepDao object
     *
     * @param formIdStr
     *            Description of the Parameter
     * @param delimiter
     *            Description of the Parameter
     */
    /*
     * Modified by Sonia Abrol, to add the form names in same order as formId
     * string
     */
    public void getFormNames(String formIdStr, String delimiter) {
        String[] arrayOfStr;
        String whereString = "", tempStr = "", formStr = "";
        String sql = "select pk_formlib,form_name from er_formlib";
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        Hashtable htNames = new Hashtable();

        if (StringUtil.isEmpty(formIdStr)) {
            return;
        }

        // parse the formIdStr to seperate indvidual formIds
        arrayOfStr = StringUtil.strSplit(formIdStr, delimiter, false);
        for (int i = 0; i < arrayOfStr.length; i++) {
            if (!StringUtil.isEmpty(arrayOfStr[i])) {
                formIdList.add(arrayOfStr[i]);
                if (whereString.length() == 0) {
                    whereString = arrayOfStr[i];
                } else {
                    whereString = whereString + "," + arrayOfStr[i];
                }
            }
        }
        if (whereString.length() > 0) {
            sql = sql + " where pk_formlib in (" + whereString + ") ";
        } else {
            return; // no form was filtered
        }
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                // add the data to a hashtable
                htNames.put(rs.getString("pk_formlib"), rs
                        .getString("form_name"));

            }

            // now iterate through the arrayOfStr and insert the formnames in
            // appropriate order

            for (int i = 0; i < arrayOfStr.length; i++) {
                if (htNames.containsKey(arrayOfStr[i])) {
                    formNameList.add((String) htNames.get(arrayOfStr[i]));
                }
            }

        } catch (SQLException e) {
            Rlog.fatal("dynrep", "{SQLEXCEPTION IN dynrepDao:getFormNames()}"
                    + e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }

    }

    /**
     * Resets all Filter Detail attributes. Usage: when getting filter detail
     * attributes for more than one filter in a loop, use this method prior to
     * calling getFilterDetails() for each filter
     */
    public void resetFilterDetails() {

        fltrColIds.clear();
        filtCol.clear();
        filtQf.clear();
        filtData.clear();
        filtExclude.clear();
        filtBbrac.clear();
        filtEbrac.clear();
        filtOper.clear();
        filtSeq.clear();
    }
//JM: 07July2006, copying report template in Ad-Hoc Query
    /**
     * Description of the Method
     *
     * @param repName
     * 			  new name of the report template being created
     *       @param id
     *            id of the report template to  be copied
     * @return Description of the Return Value
     */

    public int copyDynRep(String repName, int id, String usrId){
        int ret = 0;
        Connection conn = null;
        CallableStatement cstmt = null;

        try {
            conn = getConnection();

            cstmt = conn
                 .prepareCall("{call sp_copyDynRptTmplt(?,?,?,?)}");
            cstmt.setString(1, repName);
            cstmt.setInt(2, id);
            cstmt.setString(3, usrId);
            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);
            cstmt.execute();
            ret = cstmt.getInt(4);

            return ret;

        } catch (Exception e) {
            Rlog
                    .fatal("dynrep", "{EXCEPTION IN dynrepDao:copyDynRep()}"
                            + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -1;
        } finally {
            try {
                if (cstmt != null) {
                	cstmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        //return ret;
    }

    public void getFormDefinition(int formId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int id = 0;

                try {
                conn = getConnection();
                String sql = "SELECT pk_mp,mp_mapcolname,mp_origsysid,mp_dispname,mp_formtype,mp_flddatatype,mp_pksec,mp_secname,mp_pkfld,mp_fldcharsno,mp_uid,mp_systemid,lf_displaytype FROM ER_MAPFORM map,ER_LINKEDFORMS lnk "+
                			 " WHERE fk_form=? AND map.fk_form=lnk.fk_formlib ORDER BY mp_pksec, mp_sequence";
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, formId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    setMapCols(rs.getString("mp_mapcolname"));
                    String origSysId = rs.getString("mp_origsysid");
                    setMapOrigIds(origSysId);
	                if ("er_def_date_01".equals(origSysId)){
	                	setMapDispNames(LC.L_Data_EntryDate);
	                }else{               
	                	setMapDispNames(rs.getString("mp_dispname"));
	                }
                    setMapFormTypes(rs.getString("mp_formtype"));
                    setMapColTypes(rs.getString("mp_flddatatype"));
                    setMapColSecIds(rs.getString("mp_pksec"));
                    setMapColSecNames(rs.getString("mp_secname"));
                    setMapColIds(rs.getString("mp_pkfld"));
                    setMapColLens(rs.getString("mp_fldcharsno"));
                    setMapUids(rs.getString("mp_uid"));
                    setMapColTypesNoRepeatFields(rs.getString("mp_flddatatype"));
                    setMapSysIds(rs.getString("mp_systemid"));
                    this.setFormLinkedType(rs.getString("lf_displaytype"));
                }


            }
         catch (Exception e) {
            Rlog.fatal("dynrep",
                    "{SQLEXCEPTION IN dynrepDao:getFormDefinition(formId)}" + e);
              e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }




	public ArrayList getMapColSecRepNo() {
		return mapColSecRepNo;
	}



	public void setMapColSecRepNo(ArrayList mapColSecRepNo) {
		this.mapColSecRepNo = mapColSecRepNo;
	}

	public void setMapColSecRepNo( String repNo) {
		this.mapColSecRepNo.add(repNo);
	}



	public ArrayList getFieldRepeatNumber() {
		return fieldRepeatNumber;
	}



	public void setFieldRepeatNumber(ArrayList fieldRepeatNumber) {
		this.fieldRepeatNumber = fieldRepeatNumber;
	}


	public void setFieldRepeatNumber(String fieldRepeatNum) {
		this.fieldRepeatNumber.add(fieldRepeatNum);
	}

	public ArrayList getFieldKeyword() {
		return fieldKeyword;
	}



	public void setFieldKeyword(ArrayList fieldKeyword) {
		this.fieldKeyword = fieldKeyword;
	}

	public void setFieldKeyword(String fldKeyword) {
		this.fieldKeyword.add(fldKeyword);
	}
}
// end of class

