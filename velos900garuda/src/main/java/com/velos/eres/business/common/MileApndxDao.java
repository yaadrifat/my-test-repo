/*
 * Classname : MileApndxDao
 * 
 * Version information: 4.0 
 *
 * Date: 06/08/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh khurana
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class MileApndxDao extends CommonDAO implements java.io.Serializable {
    private ArrayList mileApndxIds;

    private ArrayList studyIds;

    private ArrayList milestoneIds;

    private ArrayList mileApndxDescs;

    private ArrayList mileApndxUris;

    private ArrayList mileApndxTypes;

    private int cRows;

    public MileApndxDao() {

        mileApndxIds = new ArrayList();
        studyIds = new ArrayList();
        milestoneIds = new ArrayList();
        mileApndxDescs = new ArrayList();
        mileApndxUris = new ArrayList();
        mileApndxTypes = new ArrayList();
    }

    public ArrayList getMileApndxIds() {
        return this.mileApndxIds;
    }

    public void setMileApndxIds(ArrayList mileApndxIds) {
        this.mileApndxIds = mileApndxIds;
    }

    public ArrayList getStudyIds() {
        return this.studyIds;
    }

    public void setStudyIds(ArrayList studyIds) {
        this.studyIds = studyIds;
    }

    public ArrayList getMilestoneIds() {
        return this.milestoneIds;
    }

    public void setMilestoneIds(ArrayList milestoneIds) {
        this.milestoneIds = milestoneIds;
    }

    public ArrayList getMileApndxDescs() {
        return this.mileApndxDescs;
    }

    public void setMileApndxDescs(ArrayList mileApndxDescs) {
        this.mileApndxDescs = mileApndxDescs;
    }

    public ArrayList getMileApndxUris() {
        return this.mileApndxUris;
    }

    public void setMileApndxUris(ArrayList mileApndxUris) {
        this.mileApndxUris = mileApndxUris;
    }

    public ArrayList getMileApndxTypes() {
        return this.mileApndxTypes;
    }

    public void setMileApndxTypes(ArrayList mileApndxTypes) {
        this.mileApndxTypes = mileApndxTypes;
    }

    // **************

    public void setMileApndxIds(String mileApndxId) {
        mileApndxIds.add(mileApndxId);
    }

    public void setStudyIds(String studyId) {
        studyIds.add(studyId);
    }

    public void setMilestoneIds(String milestoneId) {
        milestoneIds.add(milestoneId);
    }

    public void setMileApndxDescs(String mileApndxDesc) {
        mileApndxDescs.add(mileApndxDesc);
    }

    public void setMileApndxUris(String mileApndxUri) {
        mileApndxUris.add(mileApndxUri);
    }

    public void setMileApndxTypes(String mileApndxType) {
        mileApndxTypes.add(mileApndxType);
    }

    public void getMileApndxUris(int studyId) {
        Connection conn = null;
        String sqlString = null;
        PreparedStatement pstmt = null;

        try {

            conn = getConnection();
            sqlString = "select PK_MILEAPNDX,"
                    + "FK_MILESTONE,"
                    + "MILEAPNDX_DESC,"
                    + "MILEAPNDX_URI,"
                    + "MILEAPNDX_TYPE"
                    + " from er_mileapndx where FK_STUDY = ? and MILEAPNDX_TYPE='U'";
            Rlog.debug("mileapndx", "sql is" + sqlString);

            pstmt = conn.prepareStatement(sqlString);
            Rlog.debug("mileapndx", "after prepareStatement");
            pstmt.setInt(1, studyId);
            Rlog.debug("mileapndx", "after setting the study");

            ResultSet rs = pstmt.executeQuery();
            Rlog.debug("mileapndx", "after execute" + rs);

            while (rs.next()) {
                Rlog.debug("mileapndx", "inside loop");
                setMileApndxIds(rs.getString("PK_MILEAPNDX"));
                setMilestoneIds(rs.getString("FK_MILESTONE"));
                setMileApndxDescs(rs.getString("MILEAPNDX_DESC"));
                setMileApndxUris(rs.getString("MILEAPNDX_URI"));
                setMileApndxTypes(rs.getString("MILEAPNDX_TYPE"));

            }
        } catch (SQLException e) {
            Rlog.fatal("mileapndx",
                    "error in database fetch operation in getMileApndxDetails");
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "some error in getMileApndxDetails");

        } finally {
            try {
                pstmt.close();
            } catch (SQLException e) {
                Rlog.debug("mileapndx",
                        "Could not close the prepared statement");
            }
            
            try {
                conn.close();
            } catch (SQLException e) {
                Rlog.debug("mileapndx",
                        "Could not close the connection");
            }

        }

    }

    public void getMileApndxFiles(int studyId) {
        Connection conn = null;
        String sqlString = null;
        PreparedStatement pstmt = null;

        try {

            conn = getConnection();
            sqlString = "select PK_MILEAPNDX,"
                    + "FK_MILESTONE,"
                    + "MILEAPNDX_DESC,"
                    + "MILEAPNDX_URI, "
                    + "MILEAPNDX_TYPE"
                    + " from er_mileapndx where FK_STUDY = ? and MILEAPNDX_TYPE='F'";

            pstmt = conn.prepareStatement(sqlString);
            pstmt.setInt(1, studyId);
            ResultSet rs = rs = pstmt.executeQuery();

            while (rs.next()) {

                setMileApndxIds(rs.getString("PK_MILEAPNDX"));
                setMilestoneIds(rs.getString("FK_MILESTONE"));
                setMileApndxDescs(rs.getString("MILEAPNDX_DESC"));
                setMileApndxUris(rs.getString("MILEAPNDX_URI"));
                setMileApndxTypes(rs.getString("MILEAPNDX_TYPE"));

            }
        } catch (SQLException e) {
            Rlog.fatal("mileapndx",
                    "error in database fetch operation in getMileApndxFiles");
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "some error in getMileApndxFiles");

        } finally {
        	try {
                pstmt.close();
            } catch (SQLException e) {
                Rlog.debug("mileapndx",
                        "Could not close the prepared statement");
            }
            
            try {
                conn.close();
            } catch (SQLException e) {
                Rlog.debug("mileapndx",
                        "Could not close the connection");
            }

        }

    }

}
