package com.velos.eres.business.submission.impl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * @ejbHomeJNDIname ejb.SubmissionStatusBean
 */

@Entity
@Table(name = "er_submission_status")
@NamedQueries( {
    @NamedQuery(name = "findCurrentByFkSubmission",
            query = "SELECT OBJECT(ss) FROM SubmissionStatusBean ss where "+
            " ss.fkSubmission = :fkSubmission and ss.fkSubmissionBoard is null and "+
            " ss.isCurrent = 1"),
    @NamedQuery(name = "findCurrentByFkSubmissionAndBoard",
            query = "SELECT OBJECT(ss) FROM SubmissionStatusBean ss where "+
            " ss.fkSubmission = :fkSubmission and ss.fkSubmissionBoard = :fkSubmissionBoard and "+
            " ss.isCurrent = 1")
})
public class SubmissionStatusBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final int  submissionNotesMax = 4000;
    
    private Integer id;
    private Integer fkSubmission;
    private Integer fkSubmissionBoard;
    private Integer submissionStatus;
    private Date    submissionStatusDate;
    private Integer submissionEnteredBy;
    private Integer submissionAssignedTo;
    private Integer submissionCompletedBy;
    private String  submissionNotes;
    private Integer isCurrent;
    private Integer creator;
    private Integer lastModifiedBy;
    private Date    lastModifiedDate;
    private String  ipAdd;

    
    public SubmissionStatusBean() {}
    
    public SubmissionStatusBean(Integer id, Integer fkSubmission, Integer fkSubmissionBoard,
            Integer submissionStatus, Date submissionStatusDate, Integer submissionEnteredBy,
            Integer submissionAssignedTo, Integer submissionCompletedBy, String submissionNotes,
            Integer isCurrent, Integer creator, Integer lastModifiedBy, String ipAdd) {
        setId(id); setFkSubmission(fkSubmission); setFkSubmissionBoard(fkSubmissionBoard);
        setSubmissionStatus(submissionStatus); setSubmissionStatusDate(submissionStatusDate);
        setSubmissionEnteredBy(submissionEnteredBy); setSubmissionAssignedTo(submissionAssignedTo);
        setSubmissionCompletedBy(submissionCompletedBy); setSubmissionNotes(submissionNotes);
        setIsCurrent(isCurrent); setCreator(creator);
        setLastModifiedBy(lastModifiedBy); setIpAdd(ipAdd);
    }
    
    public void setDetails(SubmissionStatusBean anotherBean) {
        setFkSubmission(anotherBean.getFkSubmission());
        setFkSubmissionBoard(anotherBean.getFkSubmissionBoard());
        setSubmissionStatus(anotherBean.getSubmissionStatus());
        setSubmissionStatusDate(anotherBean.getSubmissionStatusDate());
        setSubmissionEnteredBy(anotherBean.getSubmissionEnteredBy());
        setSubmissionAssignedTo(anotherBean.getSubmissionAssignedTo());
        setSubmissionCompletedBy(anotherBean.getSubmissionCompletedBy());
        setSubmissionNotes(anotherBean.getSubmissionNotes());
        setIsCurrent(anotherBean.getIsCurrent());
        setCreator(anotherBean.getCreator());
        setLastModifiedBy(anotherBean.getLastModifiedBy());
        setIpAdd(anotherBean.getIpAdd());
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_SUBMISSION_STATUS", allocationSize=1)
    @Column(name = "PK_SUBMISSION_STATUS")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FK_SUBMISSION")
    public Integer getFkSubmission() {
        return fkSubmission;
    }

    public void setFkSubmission(Integer fkSubmission) {
        this.fkSubmission = fkSubmission;
    }

    @Column(name = "FK_SUBMISSION_BOARD")
    public Integer getFkSubmissionBoard() {
        return fkSubmissionBoard;
    }

    public void setFkSubmissionBoard(Integer fkSubmissionBoard) {
        this.fkSubmissionBoard = fkSubmissionBoard;
    }

    @Column(name = "SUBMISSION_STATUS")
    public Integer getSubmissionStatus() {
        return submissionStatus;
    }

    public void setSubmissionStatus(Integer submissionStatus) {
        this.submissionStatus = submissionStatus;
    }

    @Column(name = "SUBMISSION_STATUS_DATE")
    public Date getSubmissionStatusDate() {
        return submissionStatusDate;
    }

    public void setSubmissionStatusDate(Date submissionStatusDate) {
        this.submissionStatusDate = submissionStatusDate;
    }

    @Column(name = "SUBMISSION_ENTERED_BY")
    public Integer getSubmissionEnteredBy() {
        return submissionEnteredBy;
    }

    public void setSubmissionEnteredBy(Integer submissionEnteredBy) {
        this.submissionEnteredBy = submissionEnteredBy;
    }

    @Column(name = "SUBMISSION_ASSIGNED_TO")
    public Integer getSubmissionAssignedTo() {
        return submissionAssignedTo;
    }

    public void setSubmissionAssignedTo(Integer submissionAssignedTo) {
        this.submissionAssignedTo = submissionAssignedTo;
    }

    @Column(name = "SUBMISSION_COMPLETED_BY")
    public Integer getSubmissionCompletedBy() {
        return submissionCompletedBy;
    }

    public void setSubmissionCompletedBy(Integer submissionCompletedBy) {
        this.submissionCompletedBy = submissionCompletedBy;
    }

    @Column(name = "SUBMISSION_NOTES")
    public String getSubmissionNotes() {
        return submissionNotes;
    }

    public void setSubmissionNotes(String submissionNotes) {
        if (submissionNotes != null && submissionNotes.length() > submissionNotesMax) {
            this.submissionNotes = submissionNotes.substring(0, submissionNotesMax-1);
        } else {
            this.submissionNotes = submissionNotes;
        }
    }

    @Column(name = "IS_CURRENT")
    public Integer getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(Integer isCurrent) {
        this.isCurrent = isCurrent;
    }

    @Column(name = "CREATOR")
    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Column(name = "LAST_MODIFIED_BY")
    public Integer getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(Integer lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Column(name = "LAST_MODIFIED_DATE")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }


}
