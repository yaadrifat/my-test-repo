/*
 * Classname			PatProtBean
 * 
 * Version information : 1.0
 *
 * Date					06/16/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.business.patProt.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * CMP entity bean for Patient Study Enrollment
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Business layer.
 * </p>
 * 
 * @author Sajal
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:08/18/2004 Comment: 1. Added new attribute
 * patProtRandomNumber,patProtPhysician and respective getter/setters 2.
 * Modified methods for the new attributes 3. Modified JavaDoc comments
 * *************************END****************************************
 */

@Entity
@Table(name = "er_patprot")
@NamedQueries( {
        @NamedQuery(name = "findByEnrollDate", query = "SELECT OBJECT(pp) FROM PatProtBean pp  where fk_study = :fk_study  AND fk_per = :fk_per and patprot_enroldt = to_date(:enroldt,:date_format) and patprot_stat = 1 "),
        @NamedQuery(name = "findByEnroll", query = "SELECT OBJECT(pp) FROM PatProtBean pp  where fk_study = :fk_study  AND fk_per = :fk_per and patprot_stat = 1 order by pk_patprot desc") })
public class PatProtBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3257847675411247673L;

    /*
     * CLASS METHODS
     * 
     * public int getPatProtId() public String getPatProtProtocolId() public
     * void setPatProtProtocolId(String patProtProtocolId) public String
     * getPatProtStudyId() public void setPatProtStudyId(String patProtStudyId)
     * public String getPatProtUserId() public void setPatProtUserId(String
     * patProtUserId) public String getPatProtEnrolDt() public void
     * setPatProtEnrolDt(String patProtEnrolDt) public String
     * getPatProtPersonId() public void setPatProtPersonId(String
     * patProtPersonId) public String getPatProtStartDt() public void
     * setPatProtStartDt(String patProtStartDt) public String getPatProtNotes()
     * public void setPatProtNotes(String patProtNotes) public String
     * getPatProtStat() public void setPatProtStat(String patProtStat) public
     * PatProtPK ejbCreate(PatProtStateKeeper ppsk) throws CreateException,
     * RemoteException public PatProtStateKeeper getPatProtStateKeeper() public
     * void setPatProtStateKeeper(PatProtStateKeeper ppsk) public int
     * updatePatProt(PatProtStateKeeper ppsk){ public void ejbActivate() public
     * void ejbLoad() public void ejbStore() public void ejbPassivate() public
     * void ejbRemove() public void setEntityContext(EntityContext ctx) public
     * void unsetEntityContext()
     * 
     * END OF CLASS METHODS
     */

    /**
     * PatProt Id
     */
    public int patProtId;

    /**
     * PatProt Protocol Id
     */
    public Integer patProtProtocolId;

    /**
     * PatProt Study Id
     */
    public Integer patProtStudyId;

    /**
     * PatProt User Id
     */
    public Integer patProtUserId;

    /**
     * PatProt Enrollment date
     */
    public Date patProtEnrolDt = null;

    /**
     * PatProt Person Id
     */
    public Integer patProtPersonId;

    /**
     * PatProt Start Date
     */
    public Date patProtStartDt = null;

    /**
     * PatProt Notes
     */
    public String patProtNotes;

    /**
     * PatProt Status
     */
    public Integer patProtStat;

    /**
     * Time Zone
     */
    public Integer timeZoneId;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    /**
     * Pat Prot Schedule Start Day
     */
    public Integer patProtStartDay;

    /**
     * PatProt reason for protocol change
     */
    public String patProtReason;

    /**
     * PatProt Discontinuation date
     */
    public Date patProtDiscDt = null;

    /**
     * EntityContext object for the Bean
     */

    /**
     * Patient's study id
     */
    public String patStudyId;

    /**
     * Informed Consent signed on date
     * 
     * @deprecated deprecated since patient status enhacements, Aug 04
     */
    public Date consignDt = null;

    /**
     * Informed Consent resigned on date 1
     * 
     * @deprecated deprecated since patient status enhacements, Aug 04
     */
    public Date reSignDt1 = null;

    /**
     * Informed Consent resigned on date 2
     * 
     * @deprecated deprecated since patient status enhacements, Aug 04
     */
    public Date reSignDt2 = null;

    /**
     * Patient signed the informed consent: 0-signed , 1-not signed
     * 
     * @deprecated deprecated since patient status enhacements, Aug 04
     */
    public Integer consign;

    /**
     * Patient assigned to
     */
    public Integer assignTo;

    /**
     * Patient's treatment location
     */
    public Integer treatmentLoc;

    /**
     * Patient Randomization Number
     */
    public String patProtRandomNumber;

    /**
     * Patient Physician
     */
    public Integer patProtPhysician;

    // //

    /**
     * Patient evaluation flag
     */
    public Integer ptstEvalFlag;

    /**
     * Patient evaluation status
     */
    public Integer ptstEval;

    /**
     * Patient inevaluation status
     */
    public Integer ptstInEval;

    /**
     * Patient survival status
     */
    public Integer ptstSurvival;

    /**
     * Patient death related to study
     */
    public Integer ptstDthStdRel;

    /**
     * Patient reason for deatj related to study
     */
    public String ptstDthStdRelOther;

    /**
     * Patient consent verison
     */
    public String ptstConsentVer;

    /**
     * Patient next followup date
     */
    public Date ptstNextFlwup = null;

    /**
     * Patient's death date
     */
    public Date ptstDeathDate = null;
    
    /**
     * Patient's enrollling organization
     */
    public String enrollingSite = "";
    
    /**
     * Patient's Treating Organization
     */
    public String patOrg = ""; //JM: 07Aug2006


    // GETTER SETTER METHODS

    /**
     * @return PatProt Id
     */

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PATPROT", allocationSize=1)
    @Column(name = "PK_PATPROT")
    public int getPatProtId() {
        return this.patProtId;
    }

    public void setPatProtId(int patProtId) {
        this.patProtId = patProtId;

    }

    /**
     * 
     * 
     * @return PatProt Protocol Id
     */
    @Column(name = "FK_PROTOCOL")
    public String getPatProtProtocolId() {
        return StringUtil.integerToString(this.patProtProtocolId);
    }

    /**
     * 
     * 
     * @param patProtProtocolId
     *            PatProt Protocol Id
     */
    public void setPatProtProtocolId(String pid) {

        if (pid != null) {
            this.patProtProtocolId = StringUtil.stringToInteger(pid);
            Rlog.debug("patProt",
                    "PatProtBean.setPatProtEnrolDt() patProtProtocolId " + pid);
        } else {
            this.patProtProtocolId = null;
            Rlog
                    .debug("patProt",
                            "PatProtBean.setPatProtEnrolDt() patProtProtocolId I AM NULL ***********");
        }
    }

    /**
     * 
     * 
     * @return PatProt Study Id
     */
    @Column(name = "FK_STUDY")
    public String getPatProtStudyId() {
        return StringUtil.integerToString(this.patProtStudyId);
    }

    /**
     * 
     * 
     * @param patProtStudyId
     *            PatProt Study Id
     */
    public void setPatProtStudyId(String patProtStudyId) {

        this.patProtStudyId = StringUtil.stringToInteger(patProtStudyId);

    }

    /**
     * 
     * 
     * @return PatProt User Id
     */
    @Column(name = "FK_USER")
    public String getPatProtUserId() {
        return StringUtil.integerToString(this.patProtUserId);
    }

    /**
     * 
     * 
     * @param patProtUserId
     *            PatProt User Id
     */
    public void setPatProtUserId(String patProtUserId) {

        this.patProtUserId = StringUtil.stringToInteger(patProtUserId);

    }

    /**
     * @return PatProt Enrollment date
     */

    public String returnPatProtEnrolDt() {

        return DateUtil.dateToString(getPatProtEnrolDt());
    }

    /**
     * @param patProtEnrolDt
     *            The value that is required to be registered as PatProt
     *            Enrollment date
     */
    public void updatePatProtEnrolDt(String patProtEnrolDt) {

        setPatProtEnrolDt(DateUtil.stringToDate(patProtEnrolDt, null));

    }

    /**
     * @return PatProt Enrollment date
     */
    @Column(name = "PATPROT_ENROLDT")
    public Date getPatProtEnrolDt() {
        return this.patProtEnrolDt;
    }

    public void setPatProtEnrolDt(Date patProtEnrolDt) {
        this.patProtEnrolDt = patProtEnrolDt;
    }

    /**
     * 
     * 
     * @return PatProt Person Id
     */
    @Column(name = "FK_PER")
    public String getPatProtPersonId() {
        return StringUtil.integerToString(this.patProtPersonId);
    }

    /**
     * 
     * 
     * @param patProtPersonId
     *            PatProt Person Id
     */
    public void setPatProtPersonId(String patProtPersonId) {

        this.patProtPersonId = StringUtil.stringToInteger(patProtPersonId);

    }

    /**
     * @return PatProt Start date
     */
    @Transient
    public String getPatProtStartDt() {
        Rlog.debug("patProt",
                "PatProtBean.getPatProtStartDt() this.patProtStartDt "
                        + this.patProtStartDt);
        return DateUtil.dateToString(getPatProtStartDtPersistent());
    }

    /**
     * @param patProtStartDt
     *            The value that is required to be registered as PatProt Start
     *            date
     */
    public void setPatProtStartDt(String patProtStartDt) {

        Rlog.debug("patProt", "PatProtBean.setPatProtStartDt() patProtStartDt "
                + patProtStartDt);
        setPatProtStartDtPersistent(DateUtil.stringToDate(patProtStartDt,
                null));

    }

    /**
     * @return PatProt Start date
     */
    @Column(name = "PATPROT_START")
    public Date getPatProtStartDtPersistent() {
        return this.patProtStartDt;
    }

    public void setPatProtStartDtPersistent(Date patProtStartDt) {
        this.patProtStartDt = patProtStartDt;
    }

    /**
     * @return PatProt Notes
     */

    @Column(name = "PATPROT_NOTES")
    public String getPatProtNotes() {
        return this.patProtNotes;
    }

    /**
     * @param patProtNotes
     *            The value that is required to be registered as PatProt Notes
     */
    public void setPatProtNotes(String patProtNotes) {
        this.patProtNotes = patProtNotes;
    }

    /**
     * 
     * 
     * @return PatProt Status
     */
    @Column(name = "PATPROT_STAT")
    public String getPatProtStat() {
        return StringUtil.integerToString(this.patProtStat);
    }

    /**
     * 
     * 
     * @param patProtStat
     *            PatProt Status
     */
    public void setPatProtStat(String patProtStat) {

        this.patProtStat = StringUtil.stringToInteger(patProtStat);

    }

    /**
     * 
     * 
     * @return Time Zone
     */
    @Column(name = "fk_timezone")
    public String getTimeZoneId() {
        return StringUtil.integerToString(this.timeZoneId);
    }

    /**
     * 
     * 
     * @param Time
     *            Zone
     */
    public void setTimeZoneId(String timeZoneId) {

        this.timeZoneId = StringUtil.stringToInteger(timeZoneId);

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {

        this.creator = StringUtil.stringToInteger(creator);

    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = StringUtil.stringToInteger(modifiedBy);

    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "PATPROT_REASON")
    public String getPatProtReason() {
        return this.patProtReason;
    }

    public void setPatProtReason(String patProtReason) {
        this.patProtReason = patProtReason;
    }

    @Column(name = "PATPROT_SCHDAY")
    public String getPatProtStartDay() {
        return StringUtil.integerToString(this.patProtStartDay);
    }

    public void setPatProtStartDay(String patProtStartDay) {

        this.patProtStartDay = StringUtil.stringToInteger(patProtStartDay);

    }

    @Transient
    public String getPatProtDiscDt() {
        return DateUtil.dateToString(getPatProtDiscDtPersistent());
    }

    public void setPatProtDiscDt(String patProtDiscDt) {

        setPatProtDiscDtPersistent(DateUtil.stringToDate(patProtDiscDt,
                null));
    }

    @Column(name = "PATPROT_DISCDT")
    public Date getPatProtDiscDtPersistent() {
        return this.patProtDiscDt;
    }

    public void setPatProtDiscDtPersistent(Date patProtDiscDt) {

        this.patProtDiscDt = patProtDiscDt;
    }

    @Column(name = "patprot_patstdid")
    public String getPatStudyId() {
        return this.patStudyId;
    }

    public void setPatStudyId(String patStudyId) {
        this.patStudyId = patStudyId;
    }

    @Transient
    public String getConsignDt() {
        return DateUtil.dateToString(getConsignDtPersistent());
    }

    @Column(name = "patprot_consigndt")
    public Date getConsignDtPersistent() {
        return this.consignDt;
    }

    public void setConsignDt(String consignDt) {
        setConsignDtPersistent(DateUtil.stringToDate(consignDt, null));

    }

    public void setConsignDtPersistent(Date consignDt) {
        this.consignDt = consignDt;

    }

    @Transient
    public String getReSignDt1() {
        return DateUtil.dateToString(getReSignDt1Persistent());
    }

    @Column(name = "patprot_resigndt1")
    public Date getReSignDt1Persistent() {
        return this.reSignDt1;
    }

    public void setReSignDt1(String reSignDt1) {
        setReSignDt1Persistent(DateUtil.stringToDate(reSignDt1, null));
    }

    public void setReSignDt1Persistent(Date reSignDt1) {
        this.reSignDt1 = reSignDt1;
    }

    @Transient
    public String getReSignDt2() {
        return DateUtil.dateToString(getReSignDt2Persistent());
    }

    @Column(name = "patprot_resigndt2")
    public Date getReSignDt2Persistent() {
        return this.reSignDt2;
    }

    public void setReSignDt2(String reSignDt2) {
        setReSignDt2Persistent(DateUtil.stringToDate(reSignDt2, null));
    }

    public void setReSignDt2Persistent(Date reSignDt2) {
        this.reSignDt2 = reSignDt2;
    }

    @Column(name = "patprot_consign")
    public String getConsign() {
        return StringUtil.integerToString(this.consign);
    }

    public void setConsign(String consign) {

        this.consign = StringUtil.stringToInteger(consign);

    }

    @Column(name = "FK_USERASSTO")
    public String getAssignTo() {
        Rlog.debug("patProt", "PatProtBean.getAssignTo " + this.assignTo);
        return StringUtil.integerToString(this.assignTo);
    }

    public void setAssignTo(String assignTo) {
        Rlog.debug("patProt", "PatProtBean.setAssignTo" + assignTo);
        this.assignTo = StringUtil.stringToInteger(assignTo);

    }

    @Column(name = "FK_CODELSTLOC")
    public String getTreatmentLoc() {
        Rlog
                .debug("patProt", "PatProtBean.getTreatmentLoc"
                        + this.treatmentLoc);
        return StringUtil.integerToString(this.treatmentLoc);
    }

    public void setTreatmentLoc(String treatmentLoc) {
        Rlog.debug("patProt", "PatProtBean.setTreatmentLoc" + treatmentLoc);
        this.treatmentLoc = StringUtil.stringToInteger(treatmentLoc);

    }

    /**
     * Returns patient Randomization Number
     * 
     * @return Patient Randomization Number
     */
    @Column(name = "PATPROT_RANDOM")
    public String getPatProtRandomNumber() {
        return patProtRandomNumber;
    }

    /**
     * Sets patient Randomization Number
     * 
     * @param patProtRandomNumber
     *            Patient Randomization Number
     */
    public void setPatProtRandomNumber(String patProtRandomNumber) {
        this.patProtRandomNumber = patProtRandomNumber;
    }

    /**
     * Returns Patient Enrollment Physician id
     * 
     * @return Patient Enrollment Physician id
     */
    @Column(name = "PATPROT_PHYSICIAN")
    public String getPatProtPhysician() {
        return StringUtil.integerToString(this.patProtPhysician);
    }

    /**
     * Sets Patient Enrollment Physician id
     * 
     * @param patProtPhysician
     *            The Patient Enrollment Physician id.
     */
    public void setPatProtPhysician(String patProtPhysician) {

        if (!StringUtil.isEmpty(patProtPhysician)) {
            try {
                this.patProtPhysician = Integer.valueOf(patProtPhysician);
            } catch (Exception ex) {
                Rlog.debug("patProt",
                        "In PatProtBean.setPatProtPhysician(): Not a valid number"
                                + ex);
                this.patProtPhysician = null;
            }
        } else {
            this.patProtPhysician = null;
        }

    }

    // //
    @Column(name = "FK_CODELST_PTST_EVAL_FLAG")
    public String getPtstEvalFlag() {
        return StringUtil.integerToString(ptstEvalFlag);
    }

    public void setPtstEvalFlag(String ptstEvalFlag) {
        this.ptstEvalFlag = StringUtil.stringToInteger(ptstEvalFlag);
    }

    @Column(name = "FK_CODELST_PTST_EVAL")
    public String getPtstEval() {
        return StringUtil.integerToString(ptstEval);
    }

    public void setPtstEval(String ptstEval) {
        this.ptstEval = StringUtil.stringToInteger(ptstEval);
    }

    @Column(name = "FK_CODELST_PTST_INEVAL")
    public String getPtstInEval() {
        return StringUtil.integerToString(ptstInEval);
    }

    public void setPtstInEval(String ptstInEval) {
        this.ptstInEval = StringUtil.stringToInteger(ptstInEval);
    }

    @Column(name = "FK_CODELST_PTST_SURVIVAL")
    public String getPtstSurvival() {
        return StringUtil.integerToString(ptstSurvival);
    }

    public void setPtstSurvival(String ptstSurvival) {
        this.ptstSurvival = StringUtil.stringToInteger(ptstSurvival);
    }

    @Column(name = "FK_CODELST_PTST_DTH_STDREL")
    public String getPtstDthStdRel() {
        return StringUtil.integerToString(ptstDthStdRel);
    }

    public void setPtstDthStdRel(String ptstDthStdRel) {
        this.ptstDthStdRel = StringUtil.stringToInteger(ptstDthStdRel);
    }

    @Column(name = "DEATH_STD_RLTD_OTHER")
    public String getPtstDthStdRelOther() {
        return ptstDthStdRelOther;
    }

    public void setPtstDthStdRelOther(String ptstDthStdRelOther) {
        this.ptstDthStdRelOther = ptstDthStdRelOther;
    }

    @Column(name = "INFORM_CONSENT_VER")
    public String getPtstConsentVer() {
        return ptstConsentVer;
    }

    public void setPtstConsentVer(String ptstConsentVer) {
        this.ptstConsentVer = ptstConsentVer;
    }

    @Transient
    public String getPtstNextFlwup() {
        return DateUtil.dateToString(getPtstNextFlwupPersistent());
    }

    @Column(name = "NEXT_FOLLOWUP_ON")
    public Date getPtstNextFlwupPersistent() {
        return ptstNextFlwup;
    }

    public void setPtstNextFlwup(String ptstNextFlwup) {
        setPtstNextFlwupPersistent(DateUtil.stringToDate(ptstNextFlwup,
                null));
    }

    public void setPtstNextFlwupPersistent(Date ptstNextFlwup) {
        this.ptstNextFlwup = ptstNextFlwup;
    }

    @Transient
    public String getPtstDeathDate() {
        return DateUtil.dateToString(getPtstDeathDatePersistent());
    }

    @Column(name = "DATE_OF_DEATH")
    public Date getPtstDeathDatePersistent() {
        return ptstDeathDate;
    }

    public void setPtstDeathDate(String ptstDeathDate) {
        setPtstDeathDatePersistent(DateUtil.stringToDate(ptstDeathDate,
                null));
    }

    public void setPtstDeathDatePersistent(Date ptstDeathDate) {
        this.ptstDeathDate = ptstDeathDate;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Gets Patient Enrollment details
     * 
     * @return PatProtStateKeeper with patient enrollment details from the
     *         bean's attributes
     * @see com.velos.eres.business.patProt.PatProtStateKeeper
     */
    /*
     * public PatProtStateKeeper getPatProtStateKeeper() { return (new
     * PatProtStateKeeper(getPatProtId(), getPatProtProtocolId(),
     * getPatProtStudyId(), getPatProtUserId(), getPatProtEnrolDt(),
     * getPatProtPersonId(), getPatProtStartDt(), getPatProtNotes(),
     * getPatProtStat(), getTimeZoneId(), getCreator(), getModifiedBy(),
     * getIpAdd(), getPatProtStartDay(), getPatProtReason(), getPatProtDiscDt(),
     * getPatStudyId(), getConsignDt(), getReSignDt1(), getReSignDt2(),
     * getConsign(), getAssignTo(), getTreatmentLoc(), getPatProtRandomNumber(),
     * getPatProtPhysician(), getPtstEvalFlag(), getPtstEval(), getPtstInEval(),
     * getPtstSurvival(), getPtstDthStdRel(), getPtstDthStdRelOther(),
     * getPtstConsentVer(), getPtstNextFlwup(), getPtstDeathDate())); }
     */

    /**
     * Sets the bean's attributes with the values from the PatProtStateKeeper
     * object
     * 
     * @param ppsk
     *            A PatProtStateKeeper with patient enrollment data
     * @see com.velos.eres.business.patProt.PatProtStateKeeper
     */
    /*
     * public void setPatProtStateKeeper(PatProtStateKeeper ppsk) { GenerateId
     * id = null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("patProt", "PatProtBean.setPatProtStateKeeper() Connection :" +
     * conn); this.patProtId = id.getId("SEQ_ER_PATPROT", conn);
     * Rlog.debug("patProt", "PatProtBean.setPatProtStateKeeper() patProtId :" +
     * patProtId); conn.close();
     * setPatProtProtocolId(ppsk.getPatProtProtocolId());
     * setPatProtStudyId(ppsk.getPatProtStudyId());
     * setPatProtUserId(ppsk.getPatProtUserId());
     * setPatProtEnrolDt(ppsk.getPatProtEnrolDt());
     * setPatProtPersonId(ppsk.getPatProtPersonId());
     * setPatProtStartDt(ppsk.getPatProtStartDt());
     * setPatProtNotes(ppsk.getPatProtNotes());
     * setPatProtStat(ppsk.getPatProtStat());
     * setTimeZoneId(ppsk.getTimeZoneId()); setCreator(ppsk.getCreator());
     * setModifiedBy(ppsk.getModifiedBy()); setIpAdd(ppsk.getIpAdd());
     * setPatProtReason(ppsk.getPatProtReason());
     * setPatProtDiscDt(ppsk.getPatProtDiscDt());
     * setPatProtStartDay(ppsk.getPatProtStartDay());
     * setPatStudyId(ppsk.getPatStudyId()); setConsignDt(ppsk.getConsignDt());
     * setReSignDt1(ppsk.getReSignDt1()); setReSignDt2(ppsk.getReSignDt2());
     * setConsign(ppsk.getConsign()); Rlog.debug("patProt",
     * "PatProtBean.setPatProtStateKeeper() patProtStat@@@@ :" + consign);
     * setAssignTo(ppsk.getAssignTo()); Rlog.debug("patProt",
     * "PatProtBean.setPatProtStateKeeper() assignTo@@@@ :" + assignTo);
     * setTreatmentLoc(ppsk.getTreatmentLoc()); Rlog.debug("patProt",
     * "PatProtBean.setPatProtStateKeeper() treamentLoc@@@@ :" + treatmentLoc);
     * setPatProtRandomNumber(ppsk.getPatProtRandomNumber());
     * setPatProtPhysician(ppsk.getPatProtPhysician());
     * setPtstEvalFlag(ppsk.getPtstEvalFlag()); setPtstEval(ppsk.getPtstEval());
     * setPtstInEval(ppsk.getPtstInEval());
     * setPtstSurvival(ppsk.getPtstSurvival());
     * setPtstDthStdRel(ppsk.getPtstDthStdRel());
     * setPtstDthStdRelOther(ppsk.getPtstDthStdRelOther());
     * setPtstConsentVer(ppsk.getPtstConsentVer());
     * setPtstNextFlwup(ppsk.getPtstNextFlwup());
     * setPtstDeathDate(ppsk.getPtstDeathDate()); } catch (Exception e) {
     * Rlog.fatal("patProt", "Error in setPatProtStateKeeper() in PatProtBean " +
     * e); } }
     */

    /**
     * Updates patient ernrollment details
     * 
     * @param ppsk
     *            A PatProtStateKeeper object with the enrollment data
     * 
     * @return 0 - if update is successful <br>
     *         -2 - if update is unsuccessful
     * @see com.velos.eres.business.patProt.PatProtStateKeeper
     * 
     */
    public int updatePatProt(PatProtBean ppsk) {
        try {
            Rlog.debug("patProt",
                    "PatProtBean.setPatProtEnrolDt() patProtProtocolId ppsk.getPatProtProtocolId()"
                            + ppsk.getPatProtProtocolId());

            setPatProtProtocolId(ppsk.getPatProtProtocolId());
            setPatProtStudyId(ppsk.getPatProtStudyId());
            setPatProtUserId(ppsk.getPatProtUserId());
            updatePatProtEnrolDt(ppsk.returnPatProtEnrolDt());
            setPatProtPersonId(ppsk.getPatProtPersonId());
            setPatProtStartDt(ppsk.getPatProtStartDt());
            setPatProtNotes(ppsk.getPatProtNotes());
            setPatProtStat(ppsk.getPatProtStat());
            setTimeZoneId(ppsk.getTimeZoneId());
            setCreator(ppsk.getCreator());
            setModifiedBy(ppsk.getModifiedBy());
            setIpAdd(ppsk.getIpAdd());
            setPatProtReason(ppsk.getPatProtReason());
            setPatProtDiscDt(ppsk.getPatProtDiscDt());
            setPatProtStartDay(ppsk.getPatProtStartDay());
            setPatStudyId(ppsk.getPatStudyId());
            setConsignDt(ppsk.getConsignDt());
            setReSignDt1(ppsk.getReSignDt1());
            setReSignDt2(ppsk.getReSignDt2());
            setConsign(ppsk.getConsign());
            Rlog.debug("patProt", "PatProtBean.ppsk.getConsign()"
                    + ppsk.getConsign());
            setAssignTo(ppsk.getAssignTo());
            Rlog.debug("patProt", "PatProtBean.ppsk.getAssignTo()"
                    + ppsk.getAssignTo());
            setTreatmentLoc(ppsk.getTreatmentLoc());
            Rlog.debug("patProt", "PatProtBean.ppsk.getTreatmentLoc()"
                    + ppsk.getTreatmentLoc());
            setPatProtRandomNumber(ppsk.getPatProtRandomNumber());
            setPatProtPhysician(ppsk.getPatProtPhysician());
            Rlog.debug("patProt", "PatProtBean.updatePatProt");
            setPtstEvalFlag(ppsk.getPtstEvalFlag());
            setPtstEval(ppsk.getPtstEval());
            setPtstInEval(ppsk.getPtstInEval());
            setPtstSurvival(ppsk.getPtstSurvival());
            setPtstDthStdRel(ppsk.getPtstDthStdRel());
            setPtstDthStdRelOther(ppsk.getPtstDthStdRelOther());
            setPtstConsentVer(ppsk.getPtstConsentVer());
            setPtstNextFlwup(ppsk.getPtstNextFlwup());
            setPtstDeathDate(ppsk.getPtstDeathDate());
            setEnrollingSite(ppsk.getEnrollingSite());
            setpatOrg(ppsk.getpatOrg());//
            return 0;
        } catch (Exception e) {
            Rlog.fatal("patProt", " error in PatProtBean.updatePatProt");
            return -2;
        }
    }

    /**
     * Basic constructor for PatProtBean
     */
    public PatProtBean() {
    }

    public PatProtBean(int patProtId, String patProtProtocolId,
            String patProtStudyId, String patProtUserId, String patProtEnrolDt,
            String patProtPersonId, String patProtStartDt, String patProtNotes,
            String patProtStat, String timeZoneId, String creator,
            String modifiedBy, String ipAdd, String patProtStartDay,
            String patProtReason, String patProtDiscDt, String patStudyId,
            String consignDt, String reSignDt1, String reSignDt2,
            String consign, String assignTo, String treatmentLoc,
            String patProtRandomNumber, String patProtPhysician,
            String ptstEvalFlag, String ptstEval, String ptstInEval,
            String ptstSurvival, String ptstDthStdRel,
            String ptstDthStdRelOther, String ptstConsentVer,
            String ptstNextFlwup, String ptstDeathDate,String enrollingSite,String patOrg) {
        super();
        // TODO Auto-generated constructor stub
        setPatProtId(patProtId);
        setPatProtProtocolId(patProtProtocolId);
        setPatProtStudyId(patProtStudyId);
        setPatProtUserId(patProtUserId);
        updatePatProtEnrolDt(patProtEnrolDt);
        setPatProtPersonId(patProtPersonId);
        setPatProtStartDt(patProtStartDt);
        setPatProtNotes(patProtNotes);
        setPatProtStat(patProtStat);
        setTimeZoneId(timeZoneId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPatProtStartDay(patProtStartDay);
        setPatProtReason(patProtReason);
        setPatProtDiscDt(patProtDiscDt);
        setPatStudyId(patStudyId);
        setConsignDt(consignDt);
        setReSignDt1(reSignDt1);
        setReSignDt2(reSignDt2);
        setConsign(consign);
        setAssignTo(assignTo);
        setTreatmentLoc(treatmentLoc);
        setPatProtRandomNumber(patProtRandomNumber);
        setPatProtPhysician(patProtPhysician);
        setPtstEvalFlag(ptstEvalFlag);
        setPtstEval(ptstEval);
        setPtstInEval(ptstInEval);
        setPtstSurvival(ptstSurvival);
        setPtstDthStdRel(ptstDthStdRel);
        setPtstDthStdRelOther(ptstDthStdRelOther);
        setPtstConsentVer(ptstConsentVer);
        setPtstNextFlwup(ptstNextFlwup);
        setPtstDeathDate(ptstDeathDate);
        setEnrollingSite(enrollingSite);
        setpatOrg(patOrg);//JM
    }

    @Column(name = "FK_SITE_ENROLLING")
	public String getEnrollingSite() {
		return enrollingSite;
	}

	public void setEnrollingSite(String enrollingSite) {
		this.enrollingSite = enrollingSite;
	}
	
//JM: 07Aug2006
	@Column(name = "PATPROT_TREATINGORG")
	public String getpatOrg() {
		return patOrg;
	}

	public void setpatOrg(String patOrg) {
		this.patOrg = patOrg;
	}
	


}// end of class
