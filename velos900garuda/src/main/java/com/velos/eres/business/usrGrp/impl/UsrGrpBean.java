/*
 * Classname : UsrGrpBean
 * 
 * Version information: 1.0
 *
 * Date: 03/12/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sajal
 */

package com.velos.eres.business.usrGrp.impl;

/* Import Statements */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The UsrGrp CMP entity bean.<br>
 * <br>
 * 
 * @author Sajal
 * @vesion 1.0 03/12/2001
 * @ejbHome UsrGrpHome
 * @ejbRemote UsrGrpRObj
 */
@Entity
@Table(name = "er_usrgrp")
public class UsrGrpBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3833467292830085426L;

    /**
     * the usergroup Id
     */
    public int usrGrpId;

    /**
     * the usergroup user id
     */
    public Integer usrGrpUserId;

    /**
     * the usergroup group id
     */
    public Integer usrGrpGroupId;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return unique Id for one User-Group combination
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USRGRP", allocationSize=1)
    @Column(name = "pk_usrgrp")
    public int getUsrGrpId() {
        return this.usrGrpId;
    }

    public void setUsrGrpId(int userGrpId) {
        this.usrGrpId = userGrpId;
    }

    /**
     * 
     * 
     * @return User Id
     */
    @Column(name = "fk_user")
    public String getUsrGrpUserId() {
        return StringUtil.integerToString(this.usrGrpUserId);
    }

    /**
     * 
     * 
     * @param usrGrpUserId
     *            unique Id for one User-Group combination
     */
    public void setUsrGrpUserId(String usrGrpUserId) {
        if (usrGrpUserId != null) {
            this.usrGrpUserId = Integer.valueOf(usrGrpUserId);
        }
    }

    /**
     * 
     * 
     * @return Group Id
     */
    @Column(name = "fk_grp")
    public String getUsrGrpGroupId() {
        return StringUtil.integerToString(this.usrGrpGroupId);
    }

    /**
     * 
     * 
     * @param usrGrpGroupId
     *            Group Id
     */
    public void setUsrGrpGroupId(String usrGrpGroupId) {
        if (usrGrpGroupId != null) {
            this.usrGrpGroupId = Integer.valueOf(usrGrpGroupId);
        }
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this study
     */

    /*
     * public UsrGrpStateKeeper getUsrGrpStateKeeper() { Rlog.debug("usrgrp",
     * "UsrGrpBean.getUsrGrpStateKeeper"); return (new
     * UsrGrpStateKeeper(getUsrGrpId(), getUsrGrpUserId(), getUsrGrpGroupId(),
     * getCreator(), getModifiedBy(), getIpAdd())); }
     */

    /**
     * sets up a state keeper containing details of the usrGrp
     */

    /*
     * public void setUsrGrpStateKeeper(UsrGrpStateKeeper ugsk) { GenerateId
     * usrGrpId = null; try { Connection conn = null; conn = getConnection();
     * Rlog.debug("usrgrp", "UsrGrpBean.setUsrGrpStateKeeper() Connection :" +
     * conn); this.usrGrpId = usrGrpId.getId("SEQ_ER_USRGRP", conn);
     * Rlog.debug("usrgrp", "UsrGrpBean.setUsrGrpStateKeeper() usrGrpId :" +
     * this.usrGrpId); conn.close(); setUsrGrpUserId(ugsk.getUsrGrpUserId());
     * Rlog.debug("usrgrp", "UsrGrpBean.setUsrGrpStateKeeper() usrGrpUserId :" +
     * usrGrpUserId); setUsrGrpGroupId(ugsk.getUsrGrpGroupId());
     * Rlog.debug("usrgrp", "UsrGrpBean.setUsrGrpStateKeeper() usrGrpGroupId :" +
     * usrGrpGroupId); setCreator(ugsk.getCreator());
     * setModifiedBy(ugsk.getModifiedBy()); setIpAdd(ugsk.getIpAdd()); } catch
     * (Exception e) { Rlog.fatal("usrgrp", "Error in setUsrGrpStateKeeper() in
     * UsrGrpBean " + e); } }
     */
    /**
     * updates the User Group Record
     * 
     * @param ugsk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateUsrGrp(UsrGrpBean ugsk) {
        try {
            setUsrGrpUserId(ugsk.getUsrGrpUserId());
            setUsrGrpGroupId(ugsk.getUsrGrpGroupId());
            Rlog.debug("usrgrp", "UsrGrpBean.updateUsrGrp");
            setCreator(ugsk.getCreator());
            setModifiedBy(ugsk.getModifiedBy());
            setIpAdd(ugsk.getIpAdd());
            setCreator(ugsk.getCreator());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("usrgrp", " error in UsrGrpBean.updateUsrGrp");
            return -2;
        }
    }

    public UsrGrpBean() {

    }

    public UsrGrpBean(int usrGrpId, String usrGrpUserId, String usrGrpGroupId,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setUsrGrpId(usrGrpId);
        setUsrGrpUserId(usrGrpUserId);
        setUsrGrpGroupId(usrGrpGroupId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
