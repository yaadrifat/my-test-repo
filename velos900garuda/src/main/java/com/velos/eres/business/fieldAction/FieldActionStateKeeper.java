/*

 * Classname			FieldActionStateKeeper

 * 

 * Version information	1.0

 *

 * Date					05/12/2004

 * 

 * Copyright notice		Velos, Inc.

 */

package com.velos.eres.business.fieldAction;

/* Import Statements */

import java.util.ArrayList;
import java.util.Hashtable;

/* End of Import Statements */

/**
 * 
 * FieldActionStateKeeper class resulting from implementation of the StateKeeper
 * pattern
 * 
 * @author Sonia Sahni
 * 
 * @version 1.0 05/12/2004
 * 
 */

public class FieldActionStateKeeper implements java.io.Serializable

{

    /**
     * 
     * the field Action Id
     * 
     */

    private int fldActionId;

    /**
     * 
     * the form
     * 
     */

    private String fldForm;

    /**
     * 
     * the action type
     * 
     */

    private String fldActionType;

    /**
     * 
     * the field Action Condition
     * 
     */

    private String fldActionCondition;

    /**
     * 
     * the field Action Information
     */

    private Hashtable fldActionInfo;

    /**
     * 
     * the field Id
     * 
     */

    private String fldId;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    // Start of Getter setter methods

    /**
     * Returns the value of fldActionId.
     */
    public int getFldActionId() {
        return fldActionId;
    }

    /**
     * Sets the value of fldActionId.
     * 
     * @param fldActionId
     *            The value to assign fldActionId.
     */
    public void setFldActionId(int fldActionId) {
        this.fldActionId = fldActionId;
    }

    /**
     * Returns the value of fldForm.
     */
    public String getFldForm() {
        return fldForm;
    }

    /**
     * Sets the value of fldForm.
     * 
     * @param fldForm
     *            The value to assign fldForm.
     */
    public void setFldForm(String fldForm) {
        this.fldForm = fldForm;
    }

    /**
     * Returns the value of fldActionType.
     */
    public String getFldActionType() {
        return fldActionType;
    }

    /**
     * Sets the value of fldActionType.
     * 
     * @param fldActionType
     *            The value to assign fldActionType.
     */
    public void setFldActionType(String fldActionType) {
        this.fldActionType = fldActionType;
    }

    /**
     * Returns the value of fldActionCondition.
     */
    public String getFldActionCondition() {
        return fldActionCondition;
    }

    /**
     * Sets the value of fldActionCondition.
     * 
     * @param fldActionCondition
     *            The value to assign fldActionCondition.
     */
    public void setFldActionCondition(String fldActionCondition) {
        this.fldActionCondition = fldActionCondition;
    }

    /**
     * Returns the value of fldActionInfo.
     */
    public Hashtable getFldActionInfo() {
        return fldActionInfo;
    }

    /**
     * Sets the value of fldActionInfo.
     * 
     * @param fldActionInfo
     *            The value to assign fldActionInfo.
     */
    public void setFldActionInfo(Hashtable fldActionInfo) {
        this.fldActionInfo = fldActionInfo;
    }

    /**
     * Sets the value of fldActionInfo.
     * 
     * @param String
     *            keyWord
     * @param ArrayList
     *            values
     */
    public void setFldActionInfo(String keyWord, ArrayList values) {
        this.fldActionInfo.put(keyWord, values);
    }

    /**
     * Returns the value of fldActionInfo Key.
     */
    public ArrayList getFldActionInfo(String keyWord) {
        ArrayList arrValues = new ArrayList();

        if (fldActionInfo.containsKey(keyWord)) {
            arrValues = (ArrayList) fldActionInfo.get(keyWord);
        }
        return arrValues;
    }

    /**
     * Returns the value of fldId.
     */
    public String getFldId() {
        return fldId;
    }

    /**
     * Sets the value of fldId.
     * 
     * @param fldId
     *            The value to assign fldId.
     */
    public void setFldId(String fldId) {
        this.fldId = fldId;
    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    // END OF GETTER SETTER METHODS

    /**
     * 
     * Default Constructor
     * 
     * 
     * 
     */

    public FieldActionStateKeeper() {
    }

    public FieldActionStateKeeper(int fldActionId, String fldForm,
            String fldActionType, String fldActionCondition,
            Hashtable fldActionInfo, String creator, String modifiedBy,
            String ipAdd, String fldId) {
        setFldActionId(fldActionId);
        setFldForm(fldForm);
        setFldActionType(fldActionType);
        setFldActionCondition(fldActionCondition);
        setFldActionInfo(fldActionInfo);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setFldId(fldId);

    }

    /**
     * 
     * Returns FieldActionStateKeeper which holds allthe details of the
     * fieldAction
     * 
     * @return FieldActionStateKeeper
     * 
     */

    public FieldActionStateKeeper getFieldActionStateKeeper()

    {
        return new FieldActionStateKeeper(fldActionId, fldForm, fldActionType,
                fldActionCondition, fldActionInfo, creator, modifiedBy, ipAdd,
                fldId);
    }

    /**
     * 
     * Setter for all attributes.<br>
     * 
     * Receives a FieldActionStateKeeper and assigns attributes to class
     * variables
     * 
     * @param fsk
     *            the FieldActionStateKeeper to set to.
     * 
     */

    public void setFieldActionStateKeeper(FieldActionStateKeeper fsk) {
        setFldActionId(fsk.getFldActionId());
        setFldForm(fsk.getFldForm());
        setFldActionType(fsk.getFldActionType());
        setFldActionCondition(fsk.getFldActionCondition());
        setFldActionInfo(fsk.getFldActionInfo());
        setCreator(fsk.getCreator());
        setModifiedBy(fsk.getModifiedBy());
        setIpAdd(fsk.getIpAdd());
        setFldId(fsk.getFldId());
    }

    /**
     * 
     * Overriden for unit testing purposes
     * 
     * @param anObject
     *            Object on which to check equality.
     * 
     */

    public boolean equals(Object anObject)

    {
        if (!(anObject instanceof FieldActionStateKeeper))

            return false;

        FieldActionStateKeeper aBean = (FieldActionStateKeeper) anObject;

        return (aBean.getFldActionId() == getFldActionId()
                && aBean.getFldForm().equals(getFldForm())
                && aBean.getFldActionType().equals(getFldActionType())
                && aBean.getFldActionCondition()
                        .equals(getFldActionCondition())
                && aBean.getFldActionInfo().equals(getFldActionInfo())
                && aBean.getCreator().equals(getCreator())
                && aBean.getModifiedBy().equals(getModifiedBy())
                && aBean.getIpAdd().equals(getIpAdd()) && aBean.getFldId()
                .equals(getFldId()));

    }

}
