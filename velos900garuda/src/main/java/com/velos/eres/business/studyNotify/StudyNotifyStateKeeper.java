/*
 * Classname : StudyNotifyStateKeeper.java
 * 
 * Version information
 *
 * Date 03/16/2001
 * 
 * Copyright notice: Aithent
 */


package com.velos.eres.business.studyNotify;
import java.util.*;
//import com.velos.eres.service.util.Debug;
import com.velos.eres.service.util.Rlog;

/**
 * StudyNotifyStateKeeper class resulting from implementation of the StateKeeper pattern
 * @author Sajal
 */

public class StudyNotifyStateKeeper implements java.io.Serializable {

	/**
	 * the studyNotify Id
	 */
	private int studyNotifyId ;

	/**
	 * the Therapeutic area from codelist 
	 */
	private String studyNotifyCodelstTarea ;

	/**
	 * the studyNotify User Id 
	 */
	private String studyNotifyUserId ;

	/**
	 * the studyNotify type
	 */
	private String studyNotifyType ;
	
	/**
	 * the studyNotify keywords
	 */
	private String studyNotifyKeywords ;
	
	/*
	 * creator
	 */
	private String creator ;
	
	/*
	 * last modified by
	 */
	private String modifiedBy ;
	
	/*
	 * IP Address
	 */
	private String ipAdd ;	
	
	
	
//GETTER SETTER METHODS
	
	public int getStudyNotifyId(){
		return this.studyNotifyId;
	}

	public void setStudyNotifyId(int studyNotifyId){
		this.studyNotifyId = studyNotifyId;
	}

	public String getStudyNotifyCodelstTarea(){
		return this.studyNotifyCodelstTarea;
	}

	public void setStudyNotifyCodelstTarea(String studyNotifyCodelstTarea){
		this.studyNotifyCodelstTarea = studyNotifyCodelstTarea;
	}
	
	public String getStudyNotifyUserId(){
		return this.studyNotifyUserId;
	}

	public void setStudyNotifyUserId(String studyNotifyUserId){
		this.studyNotifyUserId = studyNotifyUserId;
	}

	public String getStudyNotifyType(){
		return this.studyNotifyType;
	}

	public void setStudyNotifyType(String studyNotifyType){
		this.studyNotifyType = studyNotifyType;
	}
	
	public String getStudyNotifyKeywords(){
		return this.studyNotifyKeywords;
	}

	public void setStudyNotifyKeywords(String studyNotifyKeywords){
		this.studyNotifyKeywords = studyNotifyKeywords;
	}
	
	/**
	 * @return     Creator
	 */	
	public String getCreator() {
		return this.creator;
	}

	/**
	 * @param   creator  The value that is required to be registered as Creator of the record
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	/**
	 * @return     Last Modified By
	 */	
	public String getModifiedBy() {
		return this.modifiedBy;
	}

	/**
	 * @param   modifiedBy  The value that is required to be registered as record last modified by
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	/**
	 * @return     IP Address 
	 */	
	public String getIpAdd() {
		return this.ipAdd;
	}

	/**
	 * @param   ipAdd  The value that is required to be registered as the IP Address
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}			
	

// END OF GETTER SETTER METHODS

	public StudyNotifyStateKeeper(){
	}

	public StudyNotifyStateKeeper(int studyNotifyId, String studyNotifyCodelstTarea, 
					String studyNotifyUserId, String studyNotifyType, String studyNotifyKeywords, 
					String creator, String modifiedBy, String ipAdd){
		Rlog.debug("studyNotify","StudyNotifyStateKeeper constructor ");
		setStudyNotifyId(studyNotifyId);
		setStudyNotifyCodelstTarea(studyNotifyCodelstTarea);
		setStudyNotifyUserId(studyNotifyUserId);
		setStudyNotifyType(studyNotifyType);
		setStudyNotifyKeywords(studyNotifyKeywords);
		Rlog.debug("studyNotify","StudyNotifyStateKeeper constructor creator = " + creator);
		setCreator(creator);
		Rlog.debug("studyNotify","StudyNotifyStateKeeper constructor modifiedBy = " + modifiedBy);		
		setModifiedBy(modifiedBy);
		Rlog.debug("studyNotify","StudyNotifyStateKeeper constructor ipAdd = " + ipAdd);		
		setIpAdd(ipAdd);		
   	}


    public StudyNotifyStateKeeper getStudyNotifyStateKeeper(){
		Rlog.debug("studyNotify","StudyNotifyStateKeeper.getStudyNotifyStateKeeper() ");
        return new StudyNotifyStateKeeper(studyNotifyId, studyNotifyCodelstTarea, studyNotifyUserId, 
						studyNotifyType, studyNotifyKeywords, creator, modifiedBy, ipAdd);
    }

    /**
     * Setter for all attributes.<br>
     * Receives a StudyNotifyStateKeeper and assigns attributes to local variables
     * @param snsk The StudyNotifyStateKeeper to set to.
     */
    public void setStudyNotifyStateKeeper(StudyNotifyStateKeeper snsk){
		setStudyNotifyId(snsk.getStudyNotifyId());
		setStudyNotifyCodelstTarea(snsk.getStudyNotifyCodelstTarea());
		setStudyNotifyUserId(snsk.getStudyNotifyUserId());
		setStudyNotifyType(snsk.getStudyNotifyType());
		setStudyNotifyKeywords(snsk.getStudyNotifyKeywords());
		setCreator(snsk.getCreator());
		setModifiedBy(snsk.getModifiedBy());
		setIpAdd(snsk.getIpAdd());		
		Rlog.debug("studyNotify","StudyNotifyStateKeeper.setStudyNotifyStateKeeper() ");
    }

    /**
     * Overriden for unit testing purposes
     * @param anObject Object on which to check equality.
     */
    public boolean equals(Object anObject)
    {
		Rlog.debug("studyNotify","StudyNotifyStateKeeper.equals(Object) ");
        if (!(anObject instanceof StudyNotifyStateKeeper))
            return false;
        StudyNotifyStateKeeper aBean = (StudyNotifyStateKeeper)anObject;
        return aBean.getStudyNotifyId() == getStudyNotifyId() && 
			aBean.getStudyNotifyCodelstTarea().equals(getStudyNotifyCodelstTarea()) && 
			aBean.getStudyNotifyUserId().equals(getStudyNotifyUserId()) && 
			aBean.getStudyNotifyType().equals(getStudyNotifyType()) && 
			aBean.getStudyNotifyKeywords().equals(getStudyNotifyKeywords());
    }

	
}

