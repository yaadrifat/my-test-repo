/*
 * Classname : UserBean
 * 
 * Version information: 1.0
 *
 * Date: 02/26/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.eres.business.user.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The User CMP entity bean.<br>
 * <br>
 * 
 * @author Dinesh
 * @vesion 1.0 02/26/2001
 * @ejbHome UserHome
 * @ejbRemote UserRObj
 */
@Entity
@Table(name = "er_user")
@NamedQueries( {
        @NamedQuery(name = "findValidUser", query = "SELECT OBJECT(user) FROM UserBean user where trim(usr_logname)=:user_logname  AND trim(usr_pwd)= :user_pwd"),
        @NamedQuery(name = "findUser", query = "SELECT OBJECT(user) FROM UserBean user where trim(usr_logname)=:user_logname") })
public class UserBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3256725069878539570L;

    /**
     * the user Id
     */
    public Integer userId;

    /**
     * the user codelst jobtype
     */
    public Integer userCodelstJobtype;

    /**
     * the user accountId
     */

    public Integer userAccountId;

    /**
     * the user siteId
     */

    public Integer userSiteId;

    /**
     * the user permanent AddressId
     */
    public Integer userPerAddressId;

    /**
     * the user lastName
     */
    public String userLastName;

    /**
     * the user firstName
     */

    public String userFirstName;

    /**
     * the user mid Name
     */

    public String userMidName;

    /**
     * the user work experience
     */

    public String userWrkExp;

    /**
     * the user phase ivaluation
     */

    public String userPhaseInv;

    /**
     * the user session Time out
     */

    public Integer userSessionTime;

    /**
     * the user login Name
     */

    public String userLoginName;

    /**
     * the user password
     */

    public String userPwd;

    /**
     * the user Secret Question
     */

    public String userSecQues;

    /**
     * the user Answer
     */

    public String userAnswer;

    /**
     * the user status
     */

    public String userStatus;

    /**
     * the user codelist primary Speciality
     */

    public Integer userCodelstSpl;

    /**
     * the user Default group
     */
    public Integer userGrpDefault;

    /**
     * Time Zone
     */
    public Integer timeZoneId;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /*
     * Pwd Expiry Date
     */
    public Date userPwdExpiryDate = null;

    /*
     * Pwd Expiry Days
     */
    public Integer userPwdExpiryDays;

    /*
     * Pwd Reminder to be sent on
     */
    public Date userPwdReminderDate = null;

    /*
     * E-signature
     */
    public String userESign;

    /*
     * E-signature Expiry Date
     */
    public Date userESignExpiryDate;

    /*
     * Pwd Expiry Days
     */
    public Integer userESignExpiryDays;

    /*
     * Pwd Reminder to be sent on
     */
    public Date userESignReminderDate;

    /*
     * User Login attempt count
     */
    public Integer userAttemptCount;

    /*
     * User Login attempt count
     */
    public Integer userLoginFlag;

    /*
     * User Site Flag, A-access to all child organizations,S-access to specified
     * organizations
     * 
     */
    public String userSiteFlag;

    /*
     * User Type, N - Non system user,S - System User
     * 
     */
    public String userType;
    
    public Integer userLoginModule;
    public String  userLoginModuleMap;

    public Integer userSkin;
    
    public Integer userTheme;
    
    public Integer userHidden;//KM
    
    //Bug-13541 Ankit
    private String userCurrntSsID;
    
    public String userTitle;
    
    public String tableRows;
    
    // GETTER SETTER METHODS

   

	/**
     * 
     * 
     * @return the Id of the User
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_USER", allocationSize=1)
    @Column(name = "PK_USER")
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer user) {
        this.userId = user;
    }

    /**
     * 
     * 
     * @return the CodeList Id of the User JobType
     */
    @Column(name = "FK_CODELST_JOBTYPE")
    public String getUserCodelstJobtype() {
        return StringUtil.integerToString(this.userCodelstJobtype);

    }

    /**
     * 
     * 
     * @param userCodelstJobtype
     *            the CodeList Id of the User JobType
     */
    public void setUserCodelstJobtype(String userCodelstJobtype) {

        this.userCodelstJobtype = StringUtil.stringToInteger(userCodelstJobtype);
    }

    /**
     * 
     * 
     * @return the Account Id of the User
     */
    @Column(name = "FK_ACCOUNT")
    public String getUserAccountId() {
        return StringUtil.integerToString(this.userAccountId);
    }

    /**
     * 
     * 
     * @param userAccountId
     *            the Account Id of the User
     */
    public void setUserAccountId(String userAccountId) {

        this.userAccountId = StringUtil.stringToInteger(userAccountId);
    }

    /**
     * 
     * 
     * @return the Site Id of the User Site
     */
    @Column(name = "FK_SITEID")
    public String getUserSiteId() {
        return StringUtil.integerToString(this.userSiteId);
    }

    /**
     * 
     * 
     * @param userSiteId
     *            the Site Id of the User Site
     */
    public void setUserSiteId(String userSiteId) {

        this.userSiteId = StringUtil.stringToInteger(userSiteId);
    }

    /**
     * 
     * 
     * @return the Address Id of the User
     */
    @Column(name = "FK_PERADD")
    public String getUserPerAddressId() {
        return StringUtil.integerToString(this.userPerAddressId);
    }

    /**
     * 
     * 
     * @param userPerAddressId
     *            the Address Id of the User
     */
    public void setUserPerAddressId(String userPerAddressId) {

        this.userPerAddressId = StringUtil.stringToInteger(userPerAddressId);
    }

    /**
     * 
     * 
     * @return user Last Name
     */
    @Column(name = "USR_LASTNAME")
    public String getUserLastName() {
        return this.userLastName;
    }

    /**
     * 
     * 
     * @param userLastName
     *            user Last Name
     */
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    /**
     * 
     * 
     * @return user First Name
     */
    @Column(name = "USR_FIRSTNAME")
    public String getUserFirstName() {
        return this.userFirstName;
    }

    /**
     * 
     * 
     * @param userFirstName
     *            the user First Name
     */
    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    /**
     * 
     * 
     * @return user Middle Name
     */
    @Column(name = "USR_MIDNAME")
    public String getUserMidName() {
        return this.userMidName;
    }

    /**
     * 
     * 
     * @param userMidName
     *            user Middle Name
     */
    public void setUserMidName(String userMidName) {
        this.userMidName = userMidName;
    }

    /**
     * 
     * 
     * @return User Work Experience
     */
    @Column(name = "USR_WRKEXP")
    public String getUserWrkExp() {
        return this.userWrkExp;
    }

    /**
     * 
     * 
     * @param userWrkExp
     *            User Work Experience
     */
    public void setUserWrkExp(String userWrkExp) {
        this.userWrkExp = userWrkExp;
    }

    /**
     * 
     * 
     * @return the phases user has been involved in
     */
    @Column(name = "USR_PAHSEINV")
    public String getUserPhaseInv() {
        return this.userPhaseInv;
    }

    /**
     * 
     * 
     * @param userPhaseInv
     *            the phases user has been involved in
     */
    public void setUserPhaseInv(String userPhaseInv) {
        this.userPhaseInv = userPhaseInv;
    }

    /**
     * 
     * 
     * @return the Session Time Out period of the user
     */
    @Column(name = "USR_SESSTIME")
    public String getUserSessionTime() {
        return StringUtil.integerToString(this.userSessionTime);
    }

    /**
     * 
     * 
     * @param userSessionTime
     *            the Session Time Out period of the user
     */

    /**
     * 
     * 
     * @param userSessionTime
     *            the Session Time Out period of the user
     */
    public void setUserSessionTime(String userSessionTime) {

        this.userSessionTime = StringUtil.stringToInteger(userSessionTime);
    }

    /**
     * 
     * 
     * @return User Login Name
     */
    @Column(name = "USR_LOGNAME")
    public String getUserLoginName() {
        return this.userLoginName;
    }

    /**
     * 
     * 
     * @param userLoginName
     *            User Login Name
     */
    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    /**
     * 
     * 
     * @return User Password
     */
    @Column(name = "USR_PWD")
    public String getUserPwdPersist() {

        return (this.userPwd);

    }

    /**
     * 
     * 
     * @param userPwd
     *            User Password
     */
    public void setUserPwdPersist(String userPwd) {

        this.userPwd = userPwd;

    }

    @Transient
    public String getUserPwd() {
        String ret = "";
        if (this.userPwd != null) {
            //return Security.decrypt(this.userPwd);
        	return this.userPwd;
        } else
            return ret;
    }

    /**
     * 
     * 
     * @param userPwd
     *            User Password
     */
    public void setUserPwd(String userPwd) {
        if (userPwd != null) {
                 this.userPwd = (userPwd);
        }
    }

    /**
     * 
     * 
     * @return User Secret Question
     */
    @Column(name = "USR_SECQUES")
    public String getUserSecQues() {
        return this.userSecQues;
    }

    /**
     * 
     * 
     * @param userSecQues
     *            User Secret Question
     */
    public void setUserSecQues(String userSecQues) {
        this.userSecQues = userSecQues;
    }

    /**
     * 
     * 
     * @return Answer to the User Secret Question
     */
    @Column(name = "USR_ANS")
    public String getUserAnswer() {
        return this.userAnswer;
    }

    /**
     * 
     * 
     * @param userAnswer
     *            Answer to the User Secret Question
     */
    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    /**
     * 
     * 
     * @return User Status
     */
    @Column(name = "USR_STAT")
    public String getUserStatus() {
        return this.userStatus;
    }

    /**
     * 
     * 
     * @param userStatus
     *            User Status
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * 
     * 
     * @return CodeList Id of the User Speciality
     */
    @Column(name = "FK_CODELST_SPL")
    public String getUserCodelstSpl() {
        return StringUtil.integerToString(this.userCodelstSpl);
    }

    /**
     * 
     * 
     * @param userCodelstSpl
     *            CodeList Id of the User Speciality
     */
    public void setUserCodelstSpl(String userCodelstSpl) {

        this.userCodelstSpl = StringUtil.stringToInteger(userCodelstSpl);
    }

    /**
     * 
     * 
     * @return Id of the User Default Group
     */
    @Column(name = "FK_GRP_DEFAULT")
    public String getUserGrpDefault() {
        return StringUtil.integerToString(this.userGrpDefault);
    }

    /**
     * 
     * 
     * @param userGrpDefault
     *            Id of the User Default Group
     */
    public void setUserGrpDefault(String userGrpDefault) {

        this.userGrpDefault = StringUtil.stringToInteger(userGrpDefault);
    }

    /**
     * 
     * 
     * @return Time Zone of the User Default Group
     */
    @Column(name = "FK_TIMEZONE")
    public String getTimeZoneId() {
        return StringUtil.integerToString(this.timeZoneId);
    }

    /**
     * 
     * 
     * @param Time
     *            Zone Id of the User Default Group
     */
    public void setTimeZoneId(String timeZoneId) {

        this.timeZoneId = StringUtil.stringToInteger(timeZoneId);
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    // Modified By Amarnadh for issue #3197
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }
    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }
     //  Amarnadh 
    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
       
    public void setModifiedBy(String modifiedBy) {
    	
    	if ( modifiedBy != null)
    	{
    		this.modifiedBy = StringUtil.stringToInteger(modifiedBy);
    	}
     }
    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return Pwd Expiry Date
     */
    @Column(name = "USR_PWDXPIRY")
    public Date getUserPwdExpDate() {
        return this.userPwdExpiryDate;
    }

    /**
     * @param userPwdExpiryDate
     *            The value that is required to be registered as the Pwd Expiry
     *            Date
     */
    public void setUserPwdExpDate(Date userPwdExpiryDate) {
        this.userPwdExpiryDate = userPwdExpiryDate;
    }

    @Transient
    public String getUserPwdExpiryDate() {
        return DateUtil.dateToString(getUserPwdExpDate());
    }

    /**
     * @param userPwdExpiryDate
     *            The value that is required to be registered as the Pwd Expiry
     *            Date
     */
    public void setUserPwdExpiryDate(String userPwdExpiryDate) {
        setUserPwdExpDate(DateUtil
                .stringToDate(userPwdExpiryDate, null));
    }

    /**
     * @return Pwd Expiry Days
     */
    @Column(name = "USR_PWDDAYS")
    public String getUserPwdExpiryDays() {
        return StringUtil.integerToString(this.userPwdExpiryDays);
    }

    /**
     * @param userPwdExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserPwdExpiryDays(String userPwdExpiryDays) {

        this.userPwdExpiryDays = StringUtil.stringToInteger(userPwdExpiryDays);
    }

    /**
     * @return Pwd Reminder to be sent
     */
    @Column(name = "USR_PWDREMIND")
    public Date getUserPwdRemindDate() {
        return (this.userPwdReminderDate);
    }

    /**
     * @param userPwdReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserPwdRemindDate(Date userPwdReminderDate) {
        this.userPwdReminderDate = userPwdReminderDate;
    }

    @Transient
    public String getUserPwdReminderDate() {
        return DateUtil.dateToString(getUserPwdRemindDate());
    }

    /**
     * @param userPwdReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserPwdReminderDate(String userPwdReminderDate) {
        setUserPwdRemindDate(DateUtil.stringToDate(userPwdReminderDate,
                null));
    }

    /**
     * @return E-signature
     */
    @Column(name = "USR_ES")
    public String getUserESignPersist() {
        return this.userESign;

    }

    /**
     * @param userESign
     *            The value that is required to be registered as the E-signature
     */
    public void setUserESignPersist(String userESign) {
        if (userESign != null) {
            this.userESign = userESign;
        }
    }

    @Transient
    public String getUserESign() {
        String ret = "";
        if (this.getUserESignPersist() != null) {
            return Security.decrypt(this.getUserESignPersist());
        } else
            return ret;
    }

    /**
     * @param userESign
     *            The value that is required to be registered as the E-signature
     */
    public void setUserESign(String userESign) {
        if (userESign != null) {
            setUserESignPersist(Security.encrypt(userESign));
        }
    }

    /**
     * @return E-signature Expiry Date
     */
    @Column(name = "USR_ESXPIRY")
    public Date getUserESignExpDate() {
        return (this.userESignExpiryDate);
    }

    /**
     * @param userESignExpiryDate
     *            The value that is required to be registered as the E-signature
     *            Expiry Date
     */
    public void setUserESignExpDate(Date userESignExpiryDate) {
        this.userESignExpiryDate = userESignExpiryDate;
    }

    @Transient
    public String getUserESignExpiryDate() {
        return DateUtil.dateToString(getUserESignExpDate());
    }

    /**
     * @param userESignExpiryDate
     *            The value that is required to be registered as the E-signature
     *            Expiry Date
     */
    public void setUserESignExpiryDate(String userESignExpiryDate) {
        setUserESignExpDate(DateUtil.stringToDate(userESignExpiryDate,
                null));
    }

    /**
     * @return Pwd Expiry Days
     */
    @Column(name = "USR_ESDAYS")
    public String getUserESignExpiryDays() {
        return StringUtil.integerToString(this.userESignExpiryDays);
    }

    /**
     * @param userESignExpiryDays
     *            The value that is required to be registered as the Pwd Expiry
     *            Days
     */
    public void setUserESignExpiryDays(String userESignExpiryDays) {

        if (!StringUtil.isEmpty(userESignExpiryDays)) {
            this.userESignExpiryDays = StringUtil
                    .stringToInteger(userESignExpiryDays);
        }
    }

    /**
     * @return Pwd Reminder to be sent on
     */
    @Column(name = "USR_ESREMIND")
    public Date getUserESignRemindDate() {
        return (this.userESignReminderDate);
    }

    /**
     * @param userESignReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserESignRemindDate(Date userESignReminderDate) {
        this.userESignReminderDate = userESignReminderDate;
    }

    @Transient
    public String getUserESignReminderDate() {
        return DateUtil.dateToString(getUserESignRemindDate());
    }

    /**
     * @param userESignReminderDate
     *            The value that is required to be registered as the Pwd
     *            Reminder to be sent on
     */
    public void setUserESignReminderDate(String userESignReminderDate) {
        setUserESignRemindDate(DateUtil.stringToDate(userESignReminderDate,
                null));
    }

    @Column(name = "USR_LOGGEDINFLAG")
    public String getUserLoginFlag() {
        return StringUtil.integerToString(this.userLoginFlag);
    }

    public void setUserLoginFlag(String userLoginFlag) {

        this.userLoginFlag = StringUtil.stringToInteger(userLoginFlag);

    }

    @Column(name = "USR_ATTEMPTCNT")
    public String getUserAttemptCount() {
        return StringUtil.integerToString(this.userAttemptCount);
    }

    public void setUserAttemptCount(String userAttemptCount) {

        this.userAttemptCount = StringUtil.stringToInteger(userAttemptCount);

    }

    @Column(name = "USR_SITEFLAG")
    public String getUserSiteFlag() {
        return this.userSiteFlag;
    }

    public void setUserSiteFlag(String userSiteFlag) {
        this.userSiteFlag = userSiteFlag;
    }

    @Column(name = "USR_TYPE")
    public String getUserType() {
        Rlog.debug("user", "in UserBean getUserType " + this.userType);
        return this.userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
    /**
	 * @return the userLoginModule
	 */
    @Column(name = "FK_LOGINMODULE")
	public String getUserLoginModule() {
		return StringUtil.integerToString(userLoginModule);
	}

	/**
	 * @param userLoginModule the userLoginModule to set
	 */
	public void setUserLoginModule(String userLoginModule) {
		this.userLoginModule = StringUtil.stringToInteger(userLoginModule);
	}

	/**
	 * @return the userLoginModuleMap
	 */
	@Column(name = "USR_LOGINMODULEMAP")
	public String getUserLoginModuleMap() {
		return userLoginModuleMap;
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserLoginModuleMap(String userLoginModuleMap) {
		this.userLoginModuleMap = userLoginModuleMap;
	}

	@Column(name = "FK_CODELST_SKIN")
	public String getUserSkin() {
		return StringUtil.integerToString(userSkin);
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserSkin(String userSkin) {
	  if (userSkin != null) {
		this.userSkin = StringUtil.stringToInteger(userSkin);
		
	  }
	}
	
	@Column(name = "FK_CODELST_THEME")
	public String getUserTheme() {
		return StringUtil.integerToString(userTheme);
	}

	/**
	 * @param userLoginModuleMap the userLoginModuleMap to set
	 */
	public void setUserTheme(String userTheme) {
	    if (userTheme != null) {
		
		this.userTheme = StringUtil.stringToInteger(userTheme);
		}
	}
	
	//Added by Manimaran for the Enh.#U11
	@Column(name = "USER_HIDDEN")
	public String getUserHidden() {
		return StringUtil.integerToString(this.userHidden);
	}

	public void setUserHidden(String userHidden) {
		
			String UH = "";
			
			if (StringUtil.isEmpty(userHidden))
	    	{
				UH = "0";
	    	}
	    	else
	    	{
	    		UH = userHidden;
	    	}
			
			
		this.userHidden = StringUtil.stringToInteger(UH);

	}
    @Column(name = "USR_CURRNTSESSID")
    public String getUserCurrntSsID() {
	return userCurrntSsID;
    }

    public void setUserCurrntSsID(String userCurrntSsID) {
    this.userCurrntSsID = userCurrntSsID;
    }
    	
    @Column(name = "USR_TITLE")
    public String getUserTitle() {
        return this.userTitle;
    }

    public void setUserTitle(String userTitle) {
        this.userTitle = userTitle;
    }
	
    @Column(name = "FK_CODELST_TABLEROWS")
    public String getTableRows() {
        return this.tableRows;
    }

    public void setTableRows(String tableRows) {
        this.tableRows = tableRows;
    }
    
    // END OF GETTER SETTER METHODS

    /**
     * 
     * /** updates the user details
     * 
     * @param usk
     * @return 0 if successful; -2 for exception
     */
    public int updateUser(UserBean usk) {
        try {

            setUserCodelstJobtype(usk.getUserCodelstJobtype());
            setUserAccountId(usk.getUserAccountId());
            setUserSiteId(usk.getUserSiteId());
            setUserPerAddressId(usk.getUserPerAddressId());
            setUserLastName(usk.getUserLastName());
            setUserFirstName(usk.getUserFirstName());
            setUserMidName(usk.getUserMidName());
            setUserWrkExp(usk.getUserWrkExp());
            setUserPhaseInv(usk.getUserPhaseInv());
            setUserSessionTime(usk.getUserSessionTime());
            setUserLoginName(usk.getUserLoginName());
            setUserPwd(usk.getUserPwd());
            setUserSecQues(usk.getUserSecQues());
            setUserAnswer(usk.getUserAnswer());
            setUserStatus(usk.getUserStatus());
            setUserCodelstSpl(usk.getUserCodelstSpl());
            setUserGrpDefault(usk.getUserGrpDefault());
            setTimeZoneId(usk.getTimeZoneId());
            setCreator(usk.getCreator());
            System.out.println("modifiedBy..@@"+usk.getModifiedBy());
            setModifiedBy(usk.getModifiedBy());
            setIpAdd(usk.getIpAdd());
            setUserPwdExpiryDate(usk.getUserPwdExpiryDate());
            setUserPwdExpiryDays(usk.getUserPwdExpiryDays());
            setUserPwdReminderDate(usk.getUserPwdReminderDate());
            setUserESign(usk.getUserESign());
            setUserESignExpiryDate(usk.getUserESignExpiryDate());
            setUserESignExpiryDays(usk.getUserESignExpiryDays());
            setUserESignReminderDate(usk.getUserESignReminderDate());
            setUserLoginFlag(usk.getUserLoginFlag());
            setUserAttemptCount(usk.getUserAttemptCount());
            setUserSiteFlag(usk.getUserSiteFlag());
            setUserType(usk.getUserType());
            setUserSkin(usk.getUserSkin());
            setUserTheme(usk.getUserTheme());
            this.setUserLoginModule(usk.getUserLoginModule()); 
            this.setUserLoginModuleMap(usk.getUserLoginModuleMap());
            setUserHidden(usk.getUserHidden());//KM
            setUserTitle(usk.getUserTitle());
            setTableRows(usk.getTableRows());
	        setUserCurrntSsID(usk.getUserCurrntSsID());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("user", " error in UserBean.updateUser : " + e);
            return -2;
        }
    }

    public UserBean() {

    }

    public UserBean(Integer userId, String userCodelstJobtype,
            String userAccountId, String userSiteId, String userPerAddressId,
            String userLastName, String userFirstName, String userMidName,
            String userWrkExp, String userPhaseInv, String userSessionTime,
            String userLoginName, String userPwd, String userSecQues,
            String userAnswer, String userStatus, String userCodelstSpl,
            String userGrpDefault, String timeZoneId, String creator,
            String modifiedBy, String ipAdd, String userPwdExpiryDate,
            String userPwdExpiryDays, String userPwdReminderDate,
            String userESign, String userESignExpiryDate,
            String userESignExpiryDays, String userESignReminderDate,
            String userAttemptCount, String userLoginFlag, String userSiteFlag,
            String userType,String userLoginModule,String userLoginModuleMap, String userSkin,
            String userTheme, String userHidden, String userTitle, String tableRows) {

        this.userId = userId;
        setUserCodelstJobtype(userCodelstJobtype);
        setUserAccountId(userAccountId);
        setUserSiteId(userSiteId);
        setUserPerAddressId(userPerAddressId);
        setUserLastName(userLastName);
        setUserFirstName(userFirstName);
        setUserMidName(userMidName);
        setUserWrkExp(userWrkExp);
        setUserPhaseInv(userPhaseInv);
        setUserSessionTime(userSessionTime);
        setUserLoginName(userLoginName);
        setUserPwd(userPwd);
        setUserSecQues(userSecQues);
        setUserAnswer(userAnswer);
        setUserStatus(userStatus);
        setUserCodelstSpl(userCodelstSpl);
        setUserGrpDefault(userGrpDefault);
        setTimeZoneId(timeZoneId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setUserPwdExpiryDate(userPwdExpiryDate);
        setUserPwdExpiryDays(userPwdExpiryDays);
        setUserPwdReminderDate(userPwdReminderDate);
        setUserESign(userESign);
        setUserESignExpiryDate(userESignExpiryDate);
        setUserESignExpiryDays(userESignExpiryDays);
        setUserESignReminderDate(userESignReminderDate);
        setUserAttemptCount(userAttemptCount);
        setUserLoginFlag(userLoginFlag);
        setUserSiteFlag(userSiteFlag);
        setUserType(userType);
        setUserSkin(userSkin);
        setUserTheme(userTheme);
        this.setUserLoginModule(userLoginModule); 
        this.setUserLoginModuleMap(userLoginModuleMap);
        setUserHidden(userHidden);
        setUserTitle(userTitle);
        setTableRows(tableRows);
    }
    
    // Overloaded constructor of UserBean, Bug-13541 Ankit
    public UserBean(Integer userId, String userCodelstJobtype,
            String userAccountId, String userSiteId, String userPerAddressId,
            String userLastName, String userFirstName, String userMidName,
            String userWrkExp, String userPhaseInv, String userSessionTime,
            String userLoginName, String userPwd, String userSecQues,
            String userAnswer, String userStatus, String userCodelstSpl,
            String userGrpDefault, String timeZoneId, String creator,
            String modifiedBy, String ipAdd, String userPwdExpiryDate,
            String userPwdExpiryDays, String userPwdReminderDate,
            String userESign, String userESignExpiryDate,
            String userESignExpiryDays, String userESignReminderDate,
            String userAttemptCount, String userLoginFlag, String userSiteFlag,
            String userType,String userLoginModule,String userLoginModuleMap, String userSkin,
            String userTheme, String userHidden, String userTitle, String tableRows,String userCurrntSsID) {

        this.userId = userId;
        setUserCodelstJobtype(userCodelstJobtype);
        setUserAccountId(userAccountId);
        setUserSiteId(userSiteId);
        setUserPerAddressId(userPerAddressId);
        setUserLastName(userLastName);
        setUserFirstName(userFirstName);
        setUserMidName(userMidName);
        setUserWrkExp(userWrkExp);
        setUserPhaseInv(userPhaseInv);
        setUserSessionTime(userSessionTime);
        setUserLoginName(userLoginName);
        setUserPwd(userPwd);
        setUserSecQues(userSecQues);
        setUserAnswer(userAnswer);
        setUserStatus(userStatus);
        setUserCodelstSpl(userCodelstSpl);
        setUserGrpDefault(userGrpDefault);
        setTimeZoneId(timeZoneId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setUserPwdExpiryDate(userPwdExpiryDate);
        setUserPwdExpiryDays(userPwdExpiryDays);
        setUserPwdReminderDate(userPwdReminderDate);
        setUserESign(userESign);
        setUserESignExpiryDate(userESignExpiryDate);
        setUserESignExpiryDays(userESignExpiryDays);
        setUserESignReminderDate(userESignReminderDate);
        setUserAttemptCount(userAttemptCount);
        setUserLoginFlag(userLoginFlag);
        setUserSiteFlag(userSiteFlag);
        setUserType(userType);
        setUserSkin(userSkin);
        setUserTheme(userTheme);
        this.setUserLoginModule(userLoginModule); 
        this.setUserLoginModuleMap(userLoginModuleMap);
        setUserHidden(userHidden);
        setUserTitle(userTitle);
        setTableRows(tableRows);
	    setUserCurrntSsID(userCurrntSsID);
    }    

}// end of class
