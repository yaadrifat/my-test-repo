/*
 * Classname : SaveFormDao
 * 
 * Version information: 1.0 
 *
 * Date: 07/23/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

/*
 * -- get forms
 *  ---form id/dates/all forms and form types
 *  --loop
 *  ----- get all form responses
 *  ----- get XML for one form from result set 
 *  ----- get linear data for all
 *  ----- compare XML with linear
 * --      log discrepancies
 * --notify
 * 
 * 
 * 
 * */
package com.velos.eres.business.common;

/* Import Statements */
import java.io.StringReader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.NodeList;

//import com.velos.eres.business.saveForm.SaveFormStateKeeper;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The Save Form Compare Utility Dao .<br>
 * <br>
 * This class has methods for the utility to compare form form XML with er_formslinear
 * 
 * @author Sonia Abrol
 * @vesion 1.0
 */

 public class FormCompareUtilityDao extends CommonDAO implements java.io.Serializable {
    int formId;

    Hashtable htSysIdColNameMapping ;
   
    Hashtable htColDsipNameMapping ;

    Hashtable htColUIDMapping ;


	ArrayList colSysIds;

    ArrayList colTypes;

    ArrayList colNames;

    ArrayList colOrigSysIds;

    //SaveFormStateKeeper houseKeeper;

    // These three variables are maintained to
    // keep track of possible responses for a multiple choice field.
    ArrayList colOrigSysIdWValueSet;

    ArrayList colResponseDispValSet;
    
    ArrayList lockdownStatuses ;
    
    int filledFormStat;
    int lockDownStat;
    
    String htmlStr="";

    Hashtable htOtherAttributes ;
	
    //additional optional parameters supplied to DAO methods
    Hashtable htMoreParams;

    ArrayList colDisplayNames;
    
    private String insertSqlForCompareLog = " INSERT INTO ER_RESP_COMPLOG ( pk_complog , fk_form , form_type ,records_processed) values (?,?,?,?)";
    
    private String insertSqlForPatCompareLogDet = " INSERT INTO ER_RESP_COMPLOGDET ( pk_complogdet , fk_resp_complog , fk_per ," +
    		" fk_patprot, fk_filledform,field_name_section, field_uid, xml_val,lin_val,fill_date_xml, " +
    		" formresp_lmd, formresp_created_on ) " +
    		"values (?,?,?,?,?,?,?,?,?,?,?,?)";

	public FormCompareUtilityDao() {
        colSysIds = new ArrayList();
        colTypes = new ArrayList();
        colNames = new ArrayList();
        colOrigSysIds = new ArrayList();
        colOrigSysIdWValueSet = new ArrayList();
        colResponseDispValSet = new ArrayList();
        htOtherAttributes = new Hashtable();
        lockdownStatuses =  new ArrayList();
        htMoreParams= new Hashtable();
        colDisplayNames =  new ArrayList();
        htSysIdColNameMapping = new Hashtable();
        htColDsipNameMapping = new Hashtable();
        htColUIDMapping = new Hashtable();
    }

    public Hashtable getHtSysIdColDsipNameMapping() {
		return htColDsipNameMapping;
	}

	public void setHtSysIdColDsipNameMapping(Hashtable htColDsipNameMapping) {
		this.htColDsipNameMapping = htColDsipNameMapping;
	}

	public Hashtable getHtColUIDMapping() {
		return htColUIDMapping;
	}

	public void setHtColUIDMapping(Hashtable htColUIDMapping) {
		this.htColUIDMapping = htColUIDMapping;
	}

	
	// Getter-Setters
    public ArrayList getColNames() {
        return colNames;
    }

    public void setColNames(ArrayList colNames) {
        this.colNames = colNames;
    }

    public void setColNames(String colName) {
        this.colNames.add(colName);
    }

    public ArrayList getColSysIds() {
        return colSysIds;
    }

    public void setColSysIds(ArrayList colSysIds) {
        this.colSysIds = colSysIds;
    }

    public void setColSysIds(String colSysId) {
        this.colSysIds.add(colSysId);
    }

    public ArrayList getColTypes() {
        return colTypes;
    }

    public void setColTypes(ArrayList colTypes) {
        this.colTypes = colTypes;
    }

    public void setColTypes(String colType) {
        this.colTypes.add(colType);
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

   /* public SaveFormStateKeeper getHouseKeeper() {
        return houseKeeper;
    }

    public void setHouseKeeper(SaveFormStateKeeper houseKeeper) {
        this.houseKeeper = houseKeeper;
    }
*/
    public ArrayList getColOrigSysIds() {
        return colOrigSysIds;
    }

    public void setColOrigSysIds(ArrayList colOrigSysIds) {
        this.colOrigSysIds = colOrigSysIds;
    }

    public void setColOrigSysIds(String colOrigSysId) {
        this.colOrigSysIds.add(colOrigSysId);
    }

    public ArrayList getColOrigSysIdWValueSet() {
        return colOrigSysIdWValueSet;
    }

    public void setColOrigSysIdWValueSet(ArrayList colOrigSysIdWValueSet) {
        this.colOrigSysIdWValueSet = colOrigSysIdWValueSet;
    }

    public void setColOrigSysIdWValueSet(String colOrigSysIdWValue) {
        this.colOrigSysIdWValueSet.add(colOrigSysIdWValue);
    }

    public ArrayList getColResponseDispValSet() {
        return colResponseDispValSet;
    }

    public void setColResponseDispValSet(ArrayList colResponseDispValSet) {
        this.colResponseDispValSet = colResponseDispValSet;
    }

    public void setColResponseDispValSet(String colResponseDispVal) {
        this.colResponseDispValSet.add(colResponseDispVal);
    }
    /**
	 * @return the filledFormStat
	 */
	public int getFilledFormStat() {
		return filledFormStat;
	}

	/**
	 * @param filledFormStat the filledFormStat to set
	 */
	public void setFilledFormStat(int filledFormStat) {
		this.filledFormStat = filledFormStat;
	}
	/**
	 * @return the htmlStr
	 */
	public String getHtmlStr() {
		return htmlStr;
	}

	/**
	 * @param htmlStr the htmlStr to set
	 */
	public void setHtmlStr(String htmlStr) {
		this.htmlStr = htmlStr;
	}
	/**
	 * @return the lockDownStat
	 */
	public int getLockDownStat() {
		return lockDownStat;
	}

	/**
	 * @param lockDownStat the lockDownStat to set
	 */
	public void setLockDownStat(int lockDownStat) {
		this.lockDownStat = lockDownStat;
	}

    // End Getter-Setters

    public void getFormDetails() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("SELECT mp_mapcolname,mp_systemid,mp_flddatatype,mp_origsysid,mp_dispname, mp_secname, mp_uid" +
                    		" FROM ER_MAPFORM WHERE fk_form=? order by pk_mp");
            pstmt.setInt(1, this.formId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                this.setColNames(rs.getString("mp_mapcolname"));
                this.setColTypes(rs.getString("mp_flddatatype"));
                this.setColSysIds(rs.getString("mp_systemid"));
                this.setColOrigSysIds(rs.getString("mp_origsysid"));
                
                this.htSysIdColNameMapping.put(rs.getString("mp_mapcolname"), rs.getString("mp_systemid"));
                this.htColDsipNameMapping.put(rs.getString("mp_mapcolname"), rs.getString("mp_secname") +":" + rs.getString("mp_dispname")) ;
                this.htColUIDMapping.put(rs.getString("mp_mapcolname"), rs.getString("mp_uid"));
                
            }
        } catch (SQLException ex) {
            Rlog.fatal("saveforms", "EXCEPTION IN  FormCompareUtility.getFormDetails"
                    + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    public void comparePatientFormResponses(Hashtable htParam) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        
        PreparedStatement pstmtLog = null;
        
        boolean dateFromFilter = false;
        String sDateFrom = "";
        
        StringBuffer sbSQL = new StringBuffer();
        String formXmlStr = "";
        ArrayList arDataInfo = new ArrayList();
        
        String dataColSQL = "";
        int v_datacol_count = 0;
        
        int colCounter = 0;

         

        String dataStr = "";
        String colStr = "";
        String colVal = "";
        int pkFormsLinear = 0;
        int pkPatforms = 0;
        
        int logPk = 0;
        
        PreparedStatement  ptmtFormData = null;
        

        //System.out.println("this.formId"+this.formId);
        
        
        
        getFormDetails(); //load form details - needed only one per form
        
        sbSQL.append("SELECT e.PK_PATFORMS,e.FK_PER , e.FK_PATPROT ,  to_char(e.PATFORMS_FILLDATE ,pkg_dateutil.f_get_datetimeformat) PATFORMS_FILLDATE,e.PATFORMS_XML.getClobVal() PATFORMS_XML ,e.FORM_COMPLETED ,e.RECORD_TYPE,e.CREATOR,e.LAST_MODIFIED_BY,e.ISVALID," +
        		"to_char(e.LAST_MODIFIED_DATE,pkg_dateutil.f_get_datetimeformat) LAST_MODIFIED_DATE," +
        		"to_char(e.CREATED_ON ,pkg_dateutil.f_get_datetimeformat) CREATED_ON ,e.FK_FORMLIBVER,e.FK_SCH_EVENTS1,e.FK_SPECIMEN " +
        		"FROM ER_patforms e WHERE fk_formlib =? and nvl(record_type,'N') <> 'D' ");
        
        ExpToFormDao expD = new ExpToFormDao();
        
        
        arDataInfo = expD.getFormDataInfo(this.formId, "L",new Integer(0));
            
     // get column list SQL for form data
        if (arDataInfo.size() == 2) {
            dataColSQL = (String) arDataInfo.get(0);
            v_datacol_count = ((Integer) arDataInfo.get(1))
                    .intValue();
            
            dataColSQL = dataColSQL + " and fk_filledform = ?";
            
        }
        
                
         

        
        if (htParam != null)
        {
        	
        	if (htParam.containsKey("dateFrom"))
        	{
        		dateFromFilter = true;
        		
        		sDateFrom =  (String)htParam.get("dateFrom");
        		
                sbSQL.append(" and ( trunc(created_on) >= trunc(sysdate) - ? or trunc(nvl(last_modified_date,created_on)) >= trunc(sysdate) - ? ) ");
        		
        	}
        }
        
        try {
            conn = getConnection();
        
            //log the request
            
            pstmtLog = conn.prepareStatement(insertSqlForCompareLog);
            
            logPk = this.getSequencePK("SEQ_ER_RESP_COMPLOG");
            
            pstmtLog.setInt(1, logPk);
            pstmtLog.setInt(2, this.formId);
            pstmtLog.setString(3, "P");
            pstmtLog.setInt(4, 0);
            
            ResultSet rsLog = pstmtLog.executeQuery();
            
            //New Request Logged. Use logPk to refer to any detail rows related to this request
            
            ptmtFormData = conn.prepareStatement(dataColSQL);
            
            
            pstmt = conn.prepareStatement(sbSQL.toString());

            pstmt.setInt(1, this.formId);
            
            if (dateFromFilter)
        	{
            	pstmt.setString(2, sDateFrom);
            	pstmt.setString(3, sDateFrom);
            	
        	}

            
            
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	 HashMap<String, String> hmResponseInfo = new HashMap<String, String>();
            	
            	           	
            	 hmResponseInfo.put("pk_patforms",rs.getString("PK_PATFORMS"));
            	 hmResponseInfo.put("fk_per",rs.getString("FK_PER"));
            	 hmResponseInfo.put("fk_patprot",rs.getString("fk_patprot"));
            	 hmResponseInfo.put("patforms_filldate",rs.getString("PATFORMS_FILLDATE"));
            	 hmResponseInfo.put("created_on",rs.getString("created_on")); 
            	 hmResponseInfo.put("last_modified_date",rs.getString("last_modified_date"));
            	 
            	 
            	 Clob xmlCLob = null;

                 xmlCLob = rs.getClob("PATFORMS_XML");

                 if (!(xmlCLob == null)) {
                     formXmlStr = xmlCLob.getSubString(1, (int) xmlCLob.length());
                 }

                 //got the XML - now compare with forms linear
                 pkPatforms = rs.getInt("PK_PATFORMS");
                  
                 
                 
                 ptmtFormData.setInt(1,pkPatforms );
                 
                 ResultSet rsetFormData = ptmtFormData.executeQuery();
                 
                 Hashtable htLinValues = new Hashtable();
                 
                 while (rsetFormData.next()) // loop for every form instance
                 {
                     try {

                         colCounter = 1;

                                                   
                         colStr = "";

                         // append the data in arrayList
                         while (colCounter <= v_datacol_count) {
                           
                        	 colVal = rsetFormData.getString(colCounter);

                             if (colVal == null)
                                 colVal = "";

                             htLinValues.put("col"+colCounter, colVal);
                             
                             colCounter++;

                         } // end of while for colCounter.next()

                         // get other columns for form


                         pkFormsLinear = rsetFormData.getInt("pk_formslinear");

                          
                         // ready with all column values for this row

                         // update the blank form xml
                         compareXMLWithLinearAndLogErrors(formXmlStr , htLinValues, hmResponseInfo,logPk);
                         
                         updateProcessCount(logPk);

                     }
                     catch (SQLException ex) {
                         Rlog.fatal("formCompareUtility", "EXCEPTION IN  FormCompareUtility.comparePatientFormResponses"+ ex);
                         ex.printStackTrace();
                     } 
                 } // end of while for rsetFormData.next()
                 
                 
                 rsetFormData.close();
        
            }// end of while for form responses
                 
            //update the end time for comparison
            updateLogEndTime(logPk);
            
            rs.close();
            
        } catch (SQLException ex) {
            Rlog.fatal("formCompareUtility", "EXCEPTION IN  FormCompareUtility.comparePatientFormResponses"
                    + ex);
            ex.printStackTrace();
        } finally {
            
        	try {
                if (pstmtLog  != null)
                	pstmtLog.close();
            } catch (Exception e) {
            }
            
        	try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            
            try {
                if (ptmtFormData != null)
                	ptmtFormData.close();
            } catch (Exception e) {
            }
            
            
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

       
 
	public Hashtable getHtOtherAttributes() {
		return htOtherAttributes;
	}

	public void setHtOtherAttributes(Hashtable htOtherAttributes) {
		this.htOtherAttributes = htOtherAttributes;
	}

	public void setHtOtherAttributes(String key,String value) {
		this.htOtherAttributes.put(key,value);
	}

	public ArrayList getLockdownStatuses() {
		return lockdownStatuses;
	}

	public void setLockdownStatuses(ArrayList lockdownStatuses) {
		this.lockdownStatuses = lockdownStatuses;
	}
	
	public void setLockdownStatuses(Integer lockdownStatus) {
		this.lockdownStatuses.add(lockdownStatus);
	}
	
 
	public Hashtable getHtMoreParams() {
		return htMoreParams;
	}

	public void setHtMoreParams(Hashtable htMoreParams) {
		this.htMoreParams = htMoreParams;
	}
	
	
	 
	public ArrayList getColDisplayNames() {
		return colDisplayNames;
	}

	public void setColDisplayNames(ArrayList colDisplayNames) {
		this.colDisplayNames = colDisplayNames;
	}

	public void setColDisplayNames(String colDisplayName) {
		this.colDisplayNames.add(colDisplayName);
	}

    public Hashtable getHtSysIdColNameMapping() {
		return htSysIdColNameMapping;
	}

	public void setHtSysIdColNameMapping(Hashtable htSysIdColNameMapping) {
		this.htSysIdColNameMapping = htSysIdColNameMapping;
	}
	
	public String getSystemIdForCol(String colname)
	{
		
		String sysid = "";
		
		Hashtable htMapping = new Hashtable();
		
		htMapping =  this.getHtSysIdColNameMapping();
		
		if (htMapping != null && htMapping.containsKey(colname))
				{
			
					sysid = (String) htMapping.get(colname);
				}
		return sysid;
		
	}

	/* Utility method to compare saved XML with data saved in linear format */
	public void compareXMLWithLinearAndLogErrors(String formXmlStr , Hashtable htLinValues, HashMap hmResponseInformation, int logPk)
	{
		Hashtable htErrors = new Hashtable();

		//process each value of XML and compare with value in the Hashtable;
		
		int detLogPk = 0;
		int paramCt = 0;
        String paramStr = null;
        String paramValueStr = null;

        String attrColCountVal = "";
        String attrDispVal = "";
        String attrTypeVal = "";
        String multiAtrrType = "";
        String selectedChoice = "";

        paramCt = htLinValues.size();
        String linesnumber = "";
        String checkboxData = "";
        int sepIndex = -1;
        Hashtable htMapping = new Hashtable();
        Hashtable htDispnameMapping = new Hashtable();
        String fieldNameAndLocation = "";
        
        String linColName="";
        Connection conn = null;
        PreparedStatement pstmtLog = null;
        String fldUID = "";
        
        try {
        	
        	
        	
        	conn = getConnection();
        	
        	pstmtLog = conn.prepareStatement(this.insertSqlForPatCompareLogDet);
        	
        	DOMParser parser = new DOMParser();

            parser.setPreserveWhitespace(false);

            parser.parse(new StringReader(formXmlStr));

            XMLDocument doc = parser.getDocument();

            Enumeration e = htLinValues.keys();
            
                        
            htMapping = this.getHtSysIdColNameMapping();
            htDispnameMapping = this.getHtSysIdColDsipNameMapping();
            
            String patprotpk = "";
            
            patprotpk = (String) hmResponseInformation.get("fk_patprot");
            
            if (StringUtil.isEmpty(patprotpk))
            {
            	patprotpk = null;
            }
            
            while( e. hasMoreElements()) 
            {
            	linColName = (String) e.nextElement();
                
                paramValueStr = (String) htLinValues.get(linColName);

                paramStr = (String) htMapping.get(linColName);
                
                fieldNameAndLocation = (String) htDispnameMapping.get(linColName);
                
                fldUID = (String) htColUIDMapping.get(linColName);
                
                String savedValueStr = "";
                
                NodeList nl = doc.getElementsByTagName(paramStr);
                
                /*System.out.println("******************");
                System.out.println("linColName" + linColName);
           
                System.out.println("paramStr" + paramStr); */
                
                for (int n = 0; n < nl.getLength(); n++) {
                    XMLElement elem = (XMLElement) nl.item(n);
                    XMLNode textNode = (XMLNode) elem.getFirstChild();

                    attrColCountVal = elem.getAttribute("colcount");
                    attrTypeVal = elem.getAttribute("type");

                    linesnumber = elem.getAttribute("linesno");

                    if (attrTypeVal.equals("MR") || attrTypeVal.equals("MC")) {
                        multiAtrrType = "checked";
                    } else if (attrTypeVal.equals("MD")) {
                        multiAtrrType = "selected";
                    }

                    if (StringUtil.isEmpty(attrColCountVal)) {
                        //if (textNode != null)
                            //textNode.setNodeValue(paramValueStr);
                    	if (textNode != null)
                    	{
                    	   savedValueStr = textNode.getNodeValue();
                    	}   
                    	   
                    	 
                    } else // multichoice field
                    {

                    	// check if the paramValueStr has [VELSEP1], is yes, extract the value before [VELSEP1]. This will extract display value from the string
                    	sepIndex = -1;

                    	sepIndex = paramValueStr.indexOf("[VELSEP1]");
                    	if (sepIndex > -1)
                    	{
                    		paramValueStr = paramValueStr.substring(0,sepIndex);
                    	}

                        // get decendent elements

                        NodeList nlresp = elem.getElementsByTagName("resp");
                        
                        
                        for (int p = 0; p < nlresp.getLength(); p++) {
                            XMLElement elemresp = (XMLElement) nlresp.item(p);
                            XMLNode textNoderesp = (XMLNode) elemresp
                                    .getFirstChild();

                            attrDispVal = elemresp.getAttribute("dispval");

                            selectedChoice = elemresp.getAttribute(multiAtrrType);
                            
                                if (attrTypeVal.equals("MC")) {

                                if (! StringUtil.isEmpty(selectedChoice) && selectedChoice.equals("1")) // option is checked
                                    {
                                     // incase of checkboxes, make the comma
                                        // separated string of selected values
                                        if (savedValueStr.equals("")) {
                                        	savedValueStr = "[" + attrDispVal
                                                    + "]";
                                        } else {
                                        	savedValueStr = savedValueStr + ",["
                                                    + attrDispVal + "]";
                                        }
                                    } 
                            } // for checkbox
                                else if (attrTypeVal.equals("MD") || attrTypeVal.equals("MR")) {
                                	if (! StringUtil.isEmpty(selectedChoice) && selectedChoice.equals("1")) // option is checked
                                    {
                                		savedValueStr = attrDispVal;
                                    }  
                                } // end for radio and dropdown

                            
                        }// end of multi choice field

                                             
                    }
                    
                    /*System.out.println("paramValueStr" + paramValueStr);
                    System.out.println("savedValueStr" + savedValueStr); */
                  
                    if (StringUtil.isEmpty(paramValueStr))
                    {
                    	
                    	paramValueStr = "";
                    }
                    if (StringUtil.isEmpty(savedValueStr))
                    {
                    	savedValueStr = "";
                    }
                    
                    if (! paramValueStr.equals(savedValueStr)) //write in detail log
                    {
                    	 	
                        detLogPk = this.getSequencePK("SEQ_ER_RESP_COMPLOGDET");
                        
                                             	
                    	pstmtLog.setInt(1, detLogPk);//PK
                    	pstmtLog.setInt(2, logPk);//FK to Log table
                    	
                    	pstmtLog.setString(3, (String) hmResponseInformation.get("fk_per") ); //FK_PER
                    	pstmtLog.setString(4, (String) patprotpk);
                    	pstmtLog.setString(5, (String) hmResponseInformation.get("pk_patforms"));
                    	
                    	pstmtLog.setString(6, fieldNameAndLocation);
                    	pstmtLog.setString(7, fldUID);
                    	pstmtLog.setString(8, savedValueStr);
                    	pstmtLog.setString(9, paramValueStr);
                    	pstmtLog.setTimestamp(10,  DateUtil.stringToTimeStamp((String)hmResponseInformation.get("patforms_filldate"),""));
 
                    	pstmtLog.setTimestamp(11, DateUtil.stringToTimeStamp((String)hmResponseInformation.get("last_modified_date"),""));
                    	pstmtLog.setTimestamp(12, DateUtil.stringToTimeStamp((String)hmResponseInformation.get("created_on"),"" ));
                    	
                    	
                   	   
                    	pstmtLog.executeQuery();
                    }
                    
                }

                 
            }

             
        } // end of try
        catch (Exception e) {
        	Rlog.fatal("formCompareUtility", "EXCEPTION IN  FormCompareUtility.compareXMLWithLinearAndLogErrors EXCEPTION : " + e);
            e.printStackTrace(System.out);
            
        }
        finally {
            
        	try {
                if (pstmtLog  != null)
                	pstmtLog.close();
            } catch (Exception e) {
            
            }
            
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

            
        } //finally
  
        
	}
	
    /**
     * Gets PK for er_resp_complog
     *
     * 
     * @return returns the Primary key
     */
    public int getSequencePK(String sequenceObjectName) {

        Connection conn = null;
        Statement stmt = null;

        int seq= 0;

        try {
            conn = getConnection();

            stmt= conn.createStatement();
            String query = "select " + sequenceObjectName+".nextval from dual";

             
            ResultSet rset = stmt.executeQuery(query);
            
             

            while (rset.next()) {

                
                seq = rset.getInt(1);

               
            }
            return seq;
        }

        catch (Exception ex) {
            Rlog.fatal("formCompareUtility", "EXCEPTION IN  FormCompareUtility.getRespLogSequencePK EXCEPTION : " + ex);
            return 0;
        } finally {
            try {
                if (stmt != null)
                	stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method
    }


    /**
     * Updates the number of records processed for a comparison request
     *
     * 
     * 
     */
    public int updateProcessCount(int logPk) {

        Connection conn = null;
        PreparedStatement stmt = null;

        int seq= 0;
        int ret  = 0;
        

        try {
            conn = getConnection();

            String query = "update er_resp_complog set records_processed = records_processed+1 where pk_complog = ? ";

            stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, logPk );
            
            ret  = stmt.executeUpdate();
            
         return ret;
        }

        catch (Exception ex) {
            Rlog.fatal("formCompareUtility", "EXCEPTION IN  FormCompareUtility.updateProcessCount EXCEPTION : " + ex);
            return 0;
        } finally {
            try {
                if (stmt != null)
                	stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method
    }

    /**
     * Updates the end date for a comparison request
     *
     * 
     * 
     */
    public int updateLogEndTime(int logPk) {

        Connection conn = null;
        PreparedStatement stmt = null;

        int seq= 0;
        int ret  = 0;
        

        try {
            conn = getConnection();

            String query = "update er_resp_complog set comp_end_date  = sysdate where pk_complog = ? ";

            stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, logPk );
            
            ret  = stmt.executeUpdate();
            
         return ret;
        }

        catch (Exception ex) {
            Rlog.fatal("formCompareUtility", "EXCEPTION IN  FormCompareUtility.updateLogEndTime EXCEPTION : " + ex);
            return 0;
        } finally {
            try {
                if (stmt != null)
                	stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        // end of method
    }
    
}// end of class
