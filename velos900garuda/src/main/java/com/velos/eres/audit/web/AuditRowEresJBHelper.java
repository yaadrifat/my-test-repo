package com.velos.eres.audit.web;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.eres.audit.service.AuditRowEresAgent;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;

/**
 * A helper class for AuditRowJB. The purpose of this class is to reduce the code size of AuditRowJB
 * by delegating some methods. This class object requires a reference to a AuditRowJB object. When
 * executing a method, this class fills the AuditRowJB object with the data retrieved from DB.
 * 
 * @author Isaac Huang
 */
public class AuditRowEresJBHelper {
	private AuditRowEresJB AuditRowJB = null;
	
	@SuppressWarnings("unused")
	private AuditRowEresJBHelper() {}

	public AuditRowEresJBHelper(AuditRowEresJB AuditRowJB) {
		this.AuditRowJB = AuditRowJB;
	}
	
	private AuditRowEresAgent getAuditRowEresAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AuditRowEresAgent)ic.lookup(JNDINames.AuditRowEresAgentHome);
	}

	public int getRID(int pkey, String tableName, String pkColumnName) {
		AuditRowEresAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEresAgent();
			output = auditAgent.getRID(pkey, tableName, pkColumnName);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.getRID for pKey="+pkey+" :tableName="+tableName+" "+e);
			output = -1;
		}
		return output;
	}
	
	public int findLatestRaid(int rid, int userId) {
		AuditRowEresAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEresAgent();
			output = auditAgent.findLatestRaid(rid, userId);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.findLatestRaid "+e);
			output = -1;
		}
		return output;
	}
	
	public int setReasonForChange(int raid, String remarks) {
		AuditRowEresAgent auditAgent = null;
		int output = 0;
		
		try {
			auditAgent = this.getAuditRowEresAgent();
			if (raid > 0)
				output = auditAgent.setReasonForChange(raid,remarks);
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowJBHelper.setReasonForChange for raid="+raid+" "+e);
			output = -1;
		}
		return output;
	}
}
