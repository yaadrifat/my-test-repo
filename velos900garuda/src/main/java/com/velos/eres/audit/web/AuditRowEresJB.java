package com.velos.eres.audit.web;

import java.util.ArrayList;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.eres.business.common.PatTXArmDao;
import com.velos.eres.audit.service.AuditRowEresAgent;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
//import com.velos.eres.web.patTXArm.PatTXArmJB;

public class AuditRowEresJB {
	private Integer id = 0;
    private String tableName;
	private Integer RID;
    private String action;
    private Date timeStamp;
    private String userName;	
		
 // Getters and setters
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getTableName() {
		return tableName;
	}

	public void setRID(Integer rid) {
		this.RID = rid;
	}
	public Integer getRID() {
		return RID;
	}

	public void setAction(String action) {
		this.action = action;
	}
	public String getAction() {
		return action;
	}
	
	public void setTimeStamp(Date timeStamp){
		this.timeStamp=timeStamp;
	}
	public Date getTimeStamp(){
		return this.timeStamp;
	}
	
	public void setUserName(String userName){
		this.userName=userName;
	}
	public String getUserName(){
		return this.userName;
	}
	
	public AuditRowEresJB() {
	
	}
	
	public int setReasonForChangeOfTxArm(int txArmId, int userId, String remarks){
		int output =0;
		int rid = getMeRID(txArmId, "ER_PATTXARM", "PK_PATTXARM");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	public int setReasonForChangeOfAllTxArms(int patProtId, int userId, String remarks){
		int output =0;
		
		//PatTXArmJB patTXArmJB = new PatTXArmJB();
		PatTXArmDao pDao = new PatTXArmDao();
		//pDao = patTXArmJB.getPatTrtmtArms(patProtId);
		
		ArrayList patArmsIds = pDao.getPatTXArmIds();
		
		for (int cnt=0; cnt < patArmsIds.size()-1; cnt++){
			int txArmId = StringUtil.stringToNum(""+patArmsIds.get(cnt));
			if (txArmId > 0){
				output = setReasonForChangeOfTxArm(txArmId, userId, remarks);
			}			
		}
		return output;
	}
	
	public int setReasonForChangeOfPatProt(int patProtId, int userId, String remarks){
		int output =0;
		int rid = getMeRID(patProtId, "ER_PATPROT", "PK_PATPROT");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	public int setReasonForChangeOfPatLab(int patLabId, int userId, String remarks){
		int output =0;
		int rid = getMeRID(patLabId, "ER_PATLABS", "PK_PATLABS");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	public int setReasonForChangeOfPatientStudyStat(int patStudyStatId, int userId, String remarks){
		int output =0;
		int rid = getMeRID(patStudyStatId, "ER_PATSTUDYSTAT", "PK_PATSTUDYSTAT");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}

	public int setReasonForChangeOfFormResponse(int formResponseId, int userId, String remarks){
		int output =0;
		int rid = getMeRID(formResponseId, "ER_PATFORMS", "PK_PATFORMS");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	public int setReasonForChangeOfPatient(int patientId, int userId, String remarks){
		int output =0;
		int rid = getMeRID(patientId, "ER_PER", "PK_PER");
		output = this.setReasonForChange(rid, userId, remarks); 		
		return output;
	}
	
	private int setReasonForChange(int rid, int userId, String remarks) {
		int output =0;
		int raid =0;
		try{
			raid = findLatestRaid(rid, userId);

			AuditRowEresJBHelper helper = new AuditRowEresJBHelper(this);
            output = helper.setReasonForChange(raid, remarks);
		} catch (Exception e){
			Rlog.fatal("auditRow","EXCEPTION IN setReasonForChange "+ e);
			output = -2;
		}		
		return output;	
	}
	
	private int findLatestRaid(int rid, int userId) {
		int output = 0;
		AuditRowEresJBHelper helper = new AuditRowEresJBHelper(this);
        output = helper.findLatestRaid(rid, userId);
        return output;
	}

	private int getMeRID(int pkey, String tableName, String pkColumnName) {
		int output = 0;
		try {
            AuditRowEresJBHelper helper = new AuditRowEresJBHelper(this);
            output = helper.getRID(pkey, tableName, pkColumnName);
            return output;
        }
        catch (Exception e) {
            Rlog.fatal("auditRow","EXCEPTION IN READING "+ tableName +" table"+ e);
            return -2;
        }
	}
	
	private AuditRowEresAgent getAuditRowEresAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AuditRowEresAgent)ic.lookup(JNDINames.AuditRowEresAgentHome);
	}
}
