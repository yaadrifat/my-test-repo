/*
 * Classname : PrefAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 06/02/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.service.prefAgent.impl;

/* Import Statements */
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.pref.impl.PrefBean;
import com.velos.eres.service.prefAgent.PrefAgentRObj;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP - PrefBean.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @see PrefBean
 * @version 1.0 06/02/2001
 * @ejbHome PrefAgentHome
 * @ejbRemote PrefAgentRObj
 */
@Stateless
public class PrefAgentBean implements PrefAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     * Looks up on the Pref id parameter passed in and constructs and returns a
     * Pref state keeper. containing all the details of the usrGrp<br>
     * 
     * @param usrGrpId
     *            the Pref id
     */
    public PrefBean getPrefDetails(int prefPKId) {

        try {
            return (PrefBean) em.find(PrefBean.class, new Integer(prefPKId));
        } catch (Exception e) {
            Rlog
                    .fatal("pref", "Error in getPrefDetails() in PrefAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Creates a Pref record.
     * 
     * @param an
     *            prefstate keeper containing pref attributes to be set.
     * @return statekeeper ; e\Exception : return null
     */

    public PrefBean setPrefDetails(PrefBean psk) {

        try {
            return psk;

        } catch (Exception e) {
            Rlog.fatal("pref", "Error in setPrefDetails() in PrefAgentBean "
                    + e);
        }
        return null;
    }

    /**
     * Updates a Pref record.
     * 
     * @param an
     *            pref state keeper containing pref attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updatePref(PrefBean ugsk) {

        try {
            // Code is removed from here because insert/update to this table
            // are through trigger.
            return 0;
        } catch (Exception e) {
            Rlog.debug("pref", "Error in updatePref in PrefAgentBean" + e);
            return -2;
        }

    }

    public int removePref(int prefPKId) {

        try {

            /*
             * Rlog.debug("pref", "in try bloc of usrGrp agent:remove pref");
             * 
             * PrefHome prefHome = EJBUtil.getPrefHome();
             * 
             * PrefPK pkPref;
             * 
             * pkPref = new PrefPK(prefPKId);
             * 
             * prefRObj = prefHome.findByPrimaryKey(pkPref);
             * 
             * prefRObj.remove(); Rlog.debug("pref", "Removed pref row = " +
             * prefPKId); return 0;
             */
            return 0;
        } catch (Exception e) {
            Rlog.debug("pref", "Exception in remove PREF " + e);
            return -1;

        }

    }

    public boolean patientCodeExists(String patientCode, String siteId) {
        boolean codeExists = false;
        try {
            Rlog.debug("pref",
                    "In patientCodeExists in PrefAgentBean line number 0");
            PatientDao patientDao = new PatientDao();
            Rlog.debug("pref",
                    "In patientCodeExists in PrefAgentBean line number 1");
            codeExists = patientDao.patientCodeExists(patientCode, siteId);
            Rlog.debug("pref",
                    "In patientCodeExists in PrefAgentBean line number 2");
        } catch (Exception e) {
        	e.printStackTrace();
            Rlog.fatal("pref",
                    "Exception In patientCodeExists in PrefAgentBean " + e);
        }
        return codeExists;

    }
    

    public boolean patientCodeExistsOneOrg(String patientCode, String siteId) {
        boolean codeExists = false;
        try {
            Rlog.debug("pref",
                    "In patientCodeExists in PrefAgentBean line number 0");
            PatientDao patientDao = new PatientDao();
            Rlog.debug("pref",
                    "In patientCodeExists in PrefAgentBean line number 1");
            codeExists = patientDao.patientCodeExistsOneOrg(patientCode, siteId);
            Rlog.debug("pref",
                    "In patientCodeExists in PrefAgentBean line number 2");
        } catch (Exception e) {
        	e.printStackTrace();
            Rlog.fatal("pref",
                    "Exception In patientCodeExists in PrefAgentBean " + e);
        }
        return codeExists;

    }
    
    
    public boolean patientCheck(String fname,String lname,String date, String siteId,String patId) {
        boolean patExists = false;
        try {
            
            PatientDao patientDao = new PatientDao();
            
            patExists = patientDao.patientCheck(fname,lname,date,siteId,patId);
            
        } catch (Exception e) {
        	e.printStackTrace();
            Rlog.fatal("pref",
                    "Exception In patientCheck in PrefAgentBean " + e);
        }
        return patExists;

    }

}// end of class

