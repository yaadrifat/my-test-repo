/*
 * Classname : StudyIdAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 09/28/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol
 */

package com.velos.eres.service.labAgent.impl;

/* Import Statements */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.common.LabDao;
import com.velos.eres.business.lab.impl.LabBean;
import com.velos.eres.service.labAgent.LabAgentRObj;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * LabAgentBean.<br>
 * <br>
 * 
 * @author Vishal Abrol
 * @see LabAgentBean
 * @version 1.0 09/22/2003
 * @ejbHome LabAgentHome
 * @ejbRemote LabAgentRObj
 */
@Stateless
public class LabAgentBean implements LabAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     * Looks up on the id parameter passed in and constructs and returns a lab
     * state keeper. containing all the details of the studyId <br>
     * 
     * @param id
     *            the lab id
     */

    public LabBean getLabDetails(int id) {

        try {

            return (LabBean) em.find(LabBean.class, new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("lab", "Exception in getLabDetails() in LabAgentBean"
                    + e);
            return null;
        }

    }

    /**
     * Creates Lab record.
     * 
     * @param a
     *            State Keeper containing the lab attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setLabDetails(LabBean lsk) {
        try {
            em.merge(lsk);
            return lsk.getId();
        } catch (Exception e) {
            Rlog.fatal("lab", "Error in setLabDetails() in LabAgentBean " + e);
            return -2;
        }

    }

    /**
     * Updates a lab record.
     * 
     * @param a
     *            lab state keeper containing studyId attributes to be set.
     * @return int for successful:0 ; e\Exception : -2
     */
    public int updateLab(LabBean lsk) {

        LabBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = (LabBean) em.find(LabBean.class, new Integer(lsk
                    .getId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateLab(lsk);

        } catch (Exception e) {
            Rlog.fatal("lab", "Error in updatelab in LabAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a lab record.
     * 
     * @param a
     *            lab Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeLab(int id) {
        int output;
        LabBean labRObj = null;

        try {

            labRObj = (LabBean) em.find(LabBean.class, new Integer(id));
            em.remove(labRObj);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("lab", "Exception in removeLab" + e);
            return -1;

        }

    }

    /**
     * Removes a lab record.
     * 
     * @param a
     *            lab Primary Key
     * @return int for successful:0; e\Exception : -2
     */
    public int updateClobData(int labId, String result, String notes) {
        int ret;
        try {
            Rlog.debug("lab", "In LabAgentBean:updateClobData ");
            LabDao labdao = new LabDao();

            ret = labdao.updateClobData(labId, result, notes);

        } catch (Exception e) {
            Rlog.fatal("lab", "Exception In LabagentBean:updateClobData " + e);
            return -2;
        }

        return ret;

    }

    public LabDao getData(String groupId) {
        LabDao labdao = new LabDao();
        try {
            Rlog.debug("lab", "labdao" + labdao);
            labdao.getData(groupId);
            return labdao;

        } catch (Exception e) {
            Rlog.fatal("lab", "Exception In getdata(groupid) in LabAgentBean"
                    + e);

        }
        return labdao;
    }

    public LabDao getData() {
        LabDao labdao = new LabDao();
        try {
            labdao.getData();
            return labdao;
        } catch (Exception e) {
            Rlog.fatal("lab", "Exception In getdata() in LabAgentBean" + e);

        }
        return labdao;
    }

    public LabDao groupPullDown(String groupId) {
        try {
            LabDao labdao = new LabDao();
            labdao.groupPullDown(groupId);
            return labdao;
        } catch (Exception e) {
            Rlog.fatal("lab", "Exception In getdata() in LabAgentBean" + e);
            return null;
        }

    }

    public int insertLab(LabDao labdao) {
        try {
            Rlog.debug("lab", "LabAgentBean:insertLab- 0");
            LabDao labDao = new LabDao();
            labDao.insertLab(labdao);

        } catch (Exception e) {
            Rlog.fatal("lab", "LabAgentBean:insertLab-Exception In insertLab "
                    + e);
            return -2;
        }

        return 1;
    }

    public int deleteLab(String[] deleteIds, String tableName, String colName,
            String type) {
        LabDao labDao = new LabDao();
        int ret = 0;
        try {
            Rlog.debug("lab", "LabAgentBean:deleteLab- 0");
            ret = labDao.deleteLab(deleteIds, tableName, colName, type);

        } catch (Exception e) {
            Rlog.fatal("lab", "LabAgentBean:deleteLab-Exception In insertLab "
                    + e);
            return -1;
        }

        return ret;

    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int deleteLab(String[] deleteIds, String tableName, String colName,
            String type,Hashtable<String, String> auditInfo) {
        LabDao labDao = new LabDao();
        int ret = 0;
        Connection conn = null;
        
        try {
        	StringBuffer buf = new StringBuffer(); 
           	for (int i = 0; i < deleteIds.length; i++) {
    			if (i > 0) {
    				buf.append(",");
    			}
    			if (deleteIds[i] != null) {
    				buf.append(deleteIds[i]);
    			}
    		}
        	String condition ="pk_patlabs IN (" + buf.toString()+ ")";
        	
        	Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues(tableName, condition, "eres");
        	conn = AuditUtils.insertAuditRow(tableName, rowValues, LC.L_MngLabs, auditInfo, "eres");
            ret = labDao.deleteLab(deleteIds, tableName, colName, type);
            
            if(ret!=-1){
            	conn.commit();
            }
        } catch (Exception e) {
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}	
        	
            Rlog.fatal("lab", "LabAgentBean:deleteLab-Exception In insertLab "
                    + e);
            return -1;
        }finally{
        	try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        	
        }

        return ret;

    }

    public LabDao retLabUnit(String[] testIds) {
        LabDao labDao = new LabDao();
        try {
            Rlog.debug("lab", "LabAgentBean:retLabUnit-Start");
            labDao.retLabUnit(testIds);
            return labDao;
        } catch (Exception e) {
            Rlog.fatal("lab",
                    "LabAgentBean:retLabUnit-Exception In retLabUnit " + e);

        }

        return labDao;
    }

    public LabDao retLabUnit(String testId) {
        LabDao labDao = new LabDao();
        try {
            Rlog.debug("lab", "LabAgentBean:retLabUnit-Start");
            labDao.retLabUnit(testId);
            return labDao;
        } catch (Exception e) {
            Rlog.fatal("lab",
                    "LabAgentBean:retLabUnit-Exception In retLabUnit " + e);

        }

        return labDao;
    }

    public LabDao getClobData(String testId) {
        LabDao labDao = new LabDao();
        try {
            Rlog.debug("lab", "LabAgentBean:getClobData-Start");
            labDao.getClobData(testId);
            return labDao;
        } catch (Exception e) {
            Rlog.fatal("lab",
                    "LabAgentBean:getClobData-Exception In getClobData " + e);

        }

        return labDao;

    }

}
