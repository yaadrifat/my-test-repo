/*
 * Classname			Security
 * 
 * Version information  1.0
 *
 * Date					03/21/2001	
 * 
 * Copyright notice		Velos Inc.
 * 
 * Author 				Dinesh
 */

package com.velos.eres.service.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cryptix.util.core.Hex;

/**
 * This class is used to encrypt or decrypt the password before storing into the
 * database
 * 
 * @author Dinesh
 * @version 1.0 03/21/2001
 */

public final class Security {
    /* function for encrypting */
    public static String encrypt(String pwd) {
        char ca[] = {};
        String s="";
        int len = pwd.length();
        if (len > 0) {
            ca = pwd.toCharArray();
            int i;
            for (i = 0; i < len; i++)
                ca[i] += ca[i];
            s = new String(ca);
           
                
    }
        return s;
    }
    public static String encryptSHA(String pwd) {
        
 	   String s=getMessageDigest(pwd);
         return s;
              
     }    

    /* function for decrypting */
    public static String decrypt(String pwd) {
        pwd = pwd.trim();
        char ca[] = {};
        String s;
        int len = pwd.length();
        if (len > 0) {
            ca = pwd.toCharArray();
            int i;
            for (i = 0; i < len; i++)
                ca[i] = (char) ((int) ca[i] / 2);
            s = new String(ca);
            return s;
        } else {
            return "error";
        }
    }
    public static String getMessageDigest(String message) {

        String msg = message;
        byte[] message_digest = null;
        String hex_string = "";

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] buffer = msg.getBytes();
            int len = buffer.length;
            md.update(buffer, 0, len);
            message_digest = md.digest();
            hex_string = Hex.toString(message_digest);
        } catch (NoSuchAlgorithmException ae) {
            ae.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        return hex_string;
    }

}