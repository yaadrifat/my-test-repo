/* class MileApndxAgentBean.java
 * version 4.0
 * date 04.05.02
 * Author Dinesh Khurana
 * Copy right notice Velos Inc,
 */

package com.velos.eres.service.mileApndxAgent.impl;

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.mileApndx.impl.MileApndxBean;
import com.velos.eres.service.mileApndxAgent.MileApndxAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

@Stateless
public class MileApndxAgentBean implements MileApndxAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
	
    @Resource
	private SessionContext context;

    public int setMileApndxDetails(MileApndxBean mask) {
        try {
            MileApndxBean mileABean = new MileApndxBean();
            mileABean.updateMileApndx(mask);
            em.persist(mileABean);
            return mileABean.getMileApndxId();
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "Exception in setMileApndxDetails" + e);
            e.printStackTrace();
            return -1;
        }
    }

    public MileApndxBean getMileApndxDetails(int mileApndxId) {
        try {
            return (MileApndxBean) em.find(MileApndxBean.class, new Integer(
                    mileApndxId));

        } catch (Exception e) {
            Rlog.fatal("mileapndx", "Exception in getMileApndxDetails" + e);
            return null;
        }
    }

    public int updateMileApndx(MileApndxBean mask) {
        int output = 0;
        try {
            MileApndxBean mileApndxRObj = (MileApndxBean) em.find(
                    MileApndxBean.class, mask.getMileApndxId());
            output = mileApndxRObj.updateMileApndx(mask);
            em.merge(mileApndxRObj);
            return output;
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "Excepltion in updateMileApndxDetails" + e);
            e.printStackTrace();
            return -1;
        }
    }

    public int removeMileApndx(int mileApndxId) {
        try {
            MileApndxBean mileApndxRObj = (MileApndxBean) em.find(
                    MileApndxBean.class, new Integer(mileApndxId));
            em.remove(mileApndxRObj);
            return 0;
        } catch (Exception e) {
            Rlog
                    .fatal("mileapndx", "Exception in removeMileApndxDetails()"
                            + e);
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeMileApndx(int mileApndxId,Hashtable<String, String> auditInfo) {
        
    	AuditBean audit=null;
    	try {
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_MILEAPNDX","eres","PK_MILEAPNDX="+mileApndxId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_MILEAPNDX",String.valueOf(mileApndxId),rid,userID,currdate,app_module,ipAdd,reason);
        	MileApndxBean mileApndxRObj = (MileApndxBean) em.find(
                    MileApndxBean.class, new Integer(mileApndxId));
            
        	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(mileApndxRObj);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog
                    .fatal("mileapndx", "Exception in removeMileApndx(int mileApndxId,Hashtable<String, String> auditInfo)"
                            + e);
            return -1;
        }
    }
}
