package com.velos.eres.service.util;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class VelosAuthenticator extends Authenticator {
    protected String m_strUser = null;

    protected String m_strPassword = null;

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(m_strUser, m_strPassword);
    }

    public void setUser(String strUser) {
        m_strUser = strUser;
    }

    public void setPassword(String strPassword) {
        m_strPassword = strPassword;
    }
}
