/*
 * Classname : UsrGrpAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 03/12/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sajal
 */

package com.velos.eres.service.usrGrpAgent;

/* Import Statements */
import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.usrGrp.impl.UsrGrpBean;

/* End of Import Statements */

/**
 * Remote interface for UsrGrpAgent session EJB
 * 
 * @author sajal
 * @version : 1.0 03/12/2001
 */
@Remote
public interface UsrGrpAgentRObj {
    /**
     * gets the usrGrp record
     */
    UsrGrpBean getUsrGrpDetails(int usrGrpId);

    /**
     * creates a new Uer Group Record
     */
    public int setUsrGrpDetails(UsrGrpBean ugsk);

    /**
     * updates Uer Group Record
     */
    public int updateUsrGrp(UsrGrpBean ugsk);

    /**
     * remove Usr Group Record
     */
    public int removeUsrFromGrp(int usrGrpId);
    //  Overloaded for INF-18183 -- AGodara
    public int removeUsrFromGrp(int usrGrpId,Hashtable<String, String> userInfo);
}
