package com.velos.eres.service.siteAgent.impl;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Stateless
//@Remote ({SiteAgentRObj.class})
public class SiteAgentBean implements SiteAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Calls getSiteDetails() on entity bean - SiteBean Looks up on the Site id
     * parameter passed in and constructs and returns a Site state holder.
     * containing all the summary details of the u<br>
     * 
     * @param siteId
     *            the Site id
     * @see SiteBean
     */
    public SiteBean getSiteDetails(Integer siteId) {

        SiteBean siteBean = null;

        try {
            // SitePK pksite;
            // pksite = new SitePK(siteId);
            // SiteHome siteHome = EJBUtil.getSiteHome();
            siteBean = em.find(
                    com.velos.eres.business.site.impl.SiteBean.class, siteId);
            Rlog.debug("site", "SiteBean" + siteBean);
            return siteBean;
            // toreturn = siteBean.getSiteStateKeeper();

        } catch (Exception e) {
            Rlog
                    .debug("site", "Error in getSiteDetails() in SiteAgentBean"
                            + e);
            e.printStackTrace();
            return siteBean;
        }

    }

    /**
     * Creates a new a Site. Calls setSiteDetails() on entity bean SiteBean
     * 
     * @param an
     *            site state holder containing site attributes to be set.
     * 
     * @return void
     * @see SiteBean
     */

    public int setSiteDetails(SiteBean site) {
        try {
            Enumeration enum_velos;
            String siteIdentifier = "";
            int accId = 0;
            int nextSeqForSite = 0;
            CodeDao cdao = new CodeDao();
            String sitePrefix = "";
            String siteSuffix = "";
            String siteNumberFormat = "";
            ArrayList cdDesc = new ArrayList();
            ArrayList cdCustom = new ArrayList();
            ArrayList cdCustom1 = new ArrayList();

            SiteBean siteBean = new SiteBean();
           
            // SiteHome siteHome = EJBUtil.getSiteHome();

            accId = StringUtil.stringToNum(site.getSiteAccountId());

          

            siteIdentifier = site.getSiteIdentifier();
          
			

            // get the next available sequence for this site

            nextSeqForSite = SiteDao.getNextSiteSequence(accId);

          

            site.setSiteSequence(String.valueOf(nextSeqForSite));

           

            // generate a siteIdentifier if the siteIdentifier is null

            if (StringUtil.isEmpty(siteIdentifier)) {
                cdao.getCodelstData(accId, "siteid_prefix");

                // get the prefix and the format
                cdDesc = cdao.getCDesc();
                cdCustom = cdao.getCodeCustom();
                cdCustom1 = cdao.getCodeCustom1();

                if (cdDesc.size() > 0) {
                    sitePrefix = (String) cdDesc.get(0);
                    siteNumberFormat = (String) cdCustom.get(0);
                    siteSuffix = (String) cdCustom1.get(0);
                }
                siteIdentifier = StringUtil.getFormattedString(sitePrefix,
                        siteSuffix, siteNumberFormat, nextSeqForSite);
                Rlog.debug("site", "SiteAgentBean.setSiteDetails"
                        + siteIdentifier);
                if (siteIdentifier.length() > 25)
                    siteIdentifier = siteIdentifier.substring((siteIdentifier
                            .length() - 25));
                Rlog.debug("site", "SiteAgentBean.setSiteDetails"
                        + siteIdentifier);
                // set the siteIdentifier to statekeeper
                site.setSiteIdentifier(siteIdentifier);
            }
            
			
            if (!StringUtil.isEmpty(siteIdentifier)) {

                // check if the site identifier value passed is unique
                // enum_velos =
                // siteBean.findBySiteIdentifier(accId,siteIdentifier);
                Query querySiteIdentifier = em
                        .createNamedQuery("findBySiteIdentifier");
                querySiteIdentifier.setParameter("fk_account", accId);
                querySiteIdentifier.setParameter("siteId", siteIdentifier);
                ArrayList list = (ArrayList) querySiteIdentifier
                        .getResultList();
                if (list == null)
                    list = new ArrayList();
             

                if (list.size() > 0) {
                    // site identifier is not unique
                    Rlog
                            .debug("site",
                                    "SiteAgentBean.setSiteDetails : the site identifier is not unique");
                    // SiteBean sBean=(SiteBean)list.get(0);
                    // SiteStateKeeper stkeep=sBean.getSiteStateKeeper();
                    // Rlog.debug("site","Site"+stkeep);
                    return -3;
                }

            }

            // SiteRObj st = siteHome.create(site);
        
		

            // siteBean.setSiteStateKeeper(site);
            siteBean.updateSite(site);
            
            em.persist(siteBean);
            Rlog.debug("site", "SiteAgentBean.setSiteDetails SiteStateKeeper "
                    + site);
            return (siteBean.getSiteId());
        } catch (Exception e) {
            Rlog.fatal("site", "Error in setSiteDetails() in SiteAgentBean "
                    + e);
            return -2;
        }

    }

    /**
     * Updates Site Details. Calls updateSite() on entity bean SiteBean
     * 
     * @param an
     *            site state holder containing site attributes to be set.
     * 
     * @return int 0 for successful ; -2 for exception
     * @see SiteBean
     */

    public int updateSite(SiteBean ssk) {

    /* Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 */	
        int output;
        ArrayList enum_velos;
        String siteIdentifier = "";
        String oldSiteIdentifier = "";
        String siteName ="";
        String oldSiteName="";
		String siteNotes = "";
		String oldSiteNotes = "";

        try {

            // pksite = new SitePK(ssk.getSiteId());
            // SiteHome siteHome = EJBUtil.getSiteHome();
            SiteBean siteBean = (SiteBean) em.find(SiteBean.class, ssk
                    .getSiteId());
            // retrieved = siteHome.findByPrimaryKey(pksite);

            if (siteBean == null) {
                return -2;
            }

            siteIdentifier = ssk.getSiteIdentifier();
            siteIdentifier = siteIdentifier.trim();
            
            oldSiteIdentifier = siteBean.getSiteIdentifier();
            if (StringUtil.isEmpty(oldSiteIdentifier)) {
                oldSiteIdentifier = "";
            } else {
                oldSiteIdentifier = oldSiteIdentifier.trim();
            }

            if (!oldSiteIdentifier.equalsIgnoreCase(siteIdentifier)) {
                if (!StringUtil.isEmpty(siteIdentifier)) {
                    // check if the site identifier value passed is unique
                    // enum_velos = siteBean.findBySiteIdentifier(EJBUtil
                    // .stringToNum(ssk.getSiteAccountId()),
                    // siteIdentifier);
                    Query querySiteIdentifier = em
                            .createNamedQuery("findBySiteIdentifier");
                    querySiteIdentifier.setParameter("fk_account", ssk
                            .getSiteAccountId());
                    querySiteIdentifier.setParameter("siteId", siteIdentifier);
                    enum_velos = (ArrayList) querySiteIdentifier
                            .getResultList();
                    if (enum_velos!=null) {
                        int size=enum_velos.size();
                        if (size>0){
                        Rlog
                                .debug("site",
                                        "SiteAgentBean.updateSite : the site identifier is not unique");
                        // site identifier is not unique
                        return -3;
                        }
                    }

                }
            }

			siteNotes = ssk.getSiteNotes();
			oldSiteNotes = siteBean.getSiteNotes();
			siteNotes  = siteNotes.trim();

            
            //Added by Manimaran to fix the Bug2729
            siteName = ssk.getSiteName();
            siteName = siteName.trim();
            

			
            oldSiteName = siteBean.getSiteName();
            if (StringUtil.isEmpty(oldSiteName)) {
            	oldSiteName = "";
            } else {
            	oldSiteName = oldSiteName.trim();
            }

            if (!oldSiteName.equalsIgnoreCase(siteName)) {
                if (!StringUtil.isEmpty(siteName)) {
                    // check if the site name value passed is unique
                    // enum_velos = siteBean.findBySiteName(EJBUtil
                    // .stringToNum(ssk.getSiteAccountId()),
                    // siteName);
                    Query querySiteName = em
                            .createNamedQuery("findBySiteName");
                    querySiteName.setParameter("fk_account", ssk
                            .getSiteAccountId());
                    querySiteName.setParameter("siteName", siteName);
                    enum_velos = (ArrayList) querySiteName
                            .getResultList();
                    if (enum_velos!=null) {
                        int size=enum_velos.size();
                        if (size>0){
                        Rlog
                                .debug("site",
                                        "SiteAgentBean.updateSite : the site name is not unique");
                        // site name is not unique
                        return -4;
                        }
                    }

                }
            }
            
            
            output = siteBean.updateSite(ssk);
            em.merge(siteBean);

        } catch (Exception e) {
            Rlog.debug("site", "Error in updateSite in SiteAgentBean" + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int findBySiteIdentifier(String accId, String siteId) {

        try {
            Query querySiteIdentifier = em
                    .createNamedQuery("findBySiteIdentifier");
            querySiteIdentifier.setParameter("fk_account", accId);
            querySiteIdentifier.setParameter("siteId", siteId);
            ArrayList list = (ArrayList) querySiteIdentifier.getResultList();
            if (list == null)
                list = new ArrayList();
            return list.size();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return -1;
        }

    }

    //Added by Manimaran to fix the Bug2729.
    public int findBySiteName(String accId, String siteName) {

        try {
            Query querySiteName = em
                    .createNamedQuery("findBySiteName");
            querySiteName.setParameter("fk_account", accId);
            querySiteName.setParameter("siteName", siteName);
            ArrayList list = (ArrayList) querySiteName.getResultList();
            if (list == null)
                list = new ArrayList();
            return list.size();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return -1;
        }

    }

    
    /**
     * Calls getByAccountId() on SiteDao
     * 
     * @param Account
     *            Id.
     * 
     * @return SiteDao with resultset
     * @see SiteDao
     */
    public SiteDao getByAccountId(int accountId) {
        try {
            Rlog.debug("site", "In getByAccountId in SiteAgentBean - 0");
            SiteDao siteDao = new SiteDao();
            Rlog.debug("site", "In getByAccountId in SiteAgentBean - 1");
            siteDao.getSiteValues(accountId);
            Rlog.debug("site", "In getByAccountId in SiteAgentBean - 2");
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Exception In findByAccountId in SiteAgentBean "
                    + e);
        }
        return null;

    }

    /**
     * Calls getSiteValuesForStudy() on SiteDao
     * 
     * @param Study
     *            Id
     * 
     * @return SiteDao with resultset
     * @see SiteDao
     */

    public SiteDao getSiteValuesForStudy(int studyId) {
        try {
            Rlog.debug("site", "In getSiteValuesForStudy in SiteAgentBean - 0");
            SiteDao siteDao = new SiteDao();
            Rlog.debug("site", "In getSiteValuesForStudy in SiteAgentBean - 1");
            siteDao.getSiteValuesForStudy(studyId);
            Rlog.debug("site", "In getSiteValuesForStudy in SiteAgentBean - 2");
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site",
                    "Exception In getSiteValuesForStudy in SiteAgentBean " + e);
        }
        return null;

    }

    /**
     * Calls getByAccountId(accountId, siteType) on SiteDao
     * 
     * @param Account
     *            Id.
     * @param Site
     *            Type.
     * @return SiteDao with resultset
     * @see SiteDao
     */

    public SiteDao getByAccountId(int accountId, String siteType) {
        try {
            SiteDao siteDao = new SiteDao();
            siteDao.getSiteValues(accountId, siteType);
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Exception In findByAccountId in SiteAgentBean "
                    + e);
        }
        return null;

    }

    // modified on 29th march for organization sorting, bug 2072

    public SiteDao getByAccountId(int accId, String orderBy, String orderType) {
        try {
            Rlog.debug("site", "In getByAccountId in SiteAgentBean - 0");
            SiteDao siteDao = new SiteDao();
            Rlog.debug("site", "In getByAccountId in SiteAgentBean - 1");

            siteDao.getSiteValues(accId, orderBy, orderType);
            Rlog.debug("site", "In getByAccountId in SiteAgentBean - 2");
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Exception In findByAccountId in SiteAgentBean "
                    + e);
        }
        return null;

    }
    
    /**
     * Calls getBySiteTypeCodeId(accountId, siteType) on SiteDao
     * 
     * @param Account
     *            Id.
     * @param Site
     *            Type.
     * @return SiteDao with resultset
     * @see SiteDao
     */

    public SiteDao getBySiteTypeCodeId(int accountId, String siteType) {
        try {
            SiteDao siteDao = new SiteDao();
            siteDao.getBySiteTypeCodeId(accountId, siteType);
            return siteDao;
        } catch (Exception e) {
            Rlog.fatal("site", "Exception In findByAccountId in SiteAgentBean "
                    + e);
        }
        return null;

    }
    
    /**
     * Get the list of sites that a user has access to. This method
     * calls getSitesByUser() of SiteDao and returns SiteDao with ArrayList of siteIds.
     * 
     * @param userId
     */
    public SiteDao getSitesByUser(int userId) {
    	SiteDao siteDao = new SiteDao();
    	siteDao.getSitesByUser(userId);
    	return siteDao;
    }

}
