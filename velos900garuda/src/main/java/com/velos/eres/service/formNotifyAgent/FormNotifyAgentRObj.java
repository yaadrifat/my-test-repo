/*
 * Classname : FormNotifyAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 24/06/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.formNotifyAgent;

/* Import Statements */

import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.common.FormNotifyDao;
import com.velos.eres.business.formNotify.impl.FormNotifyBean;

/* End of Import Statements */

/**
 * Remote interface for FormNotifyAgent session EJB
 * 
 * @author SoniaKaura
 * @version : 1.0 24/06/2003
 */

@Remote
public interface FormNotifyAgentRObj {
    /**
     * gets the Form Notify record
     */
    FormNotifyBean getFormNotifyDetails(int formNotifyId);

    /**
     * creates a new Form Notify Record
     */
    public int setFormNotifyDetails(FormNotifyBean formsk);

    /**
     * updates the Form Notify Record
     */
    public int updateFormNotify(FormNotifyBean formsk);

    /**
     * remove Form NotifyRecord
     */
    public int removeFormNotify(int formNotifyId);

 // Overloaded for INF-18183 ::: AGodara
    public int removeFormNotify(int formNotifyId,Hashtable<String, String> auditInfo);
    
    /**
     * get the DAO from the table
     * 
     */

    public FormNotifyDao getAllFormNotifications(int formLibId);

}
