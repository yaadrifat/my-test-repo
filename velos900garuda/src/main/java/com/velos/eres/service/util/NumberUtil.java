package com.velos.eres.service.util;

import java.util.Formatter;

public class NumberUtil {

	//Function to round off the number
	public static Formatter roundOffNo(double val)
	{
			Formatter fmt=new Formatter(); 
			fmt.format("%.2f",val);
		    return fmt;
    }
	
	public static boolean isInteger(String input)   
	{   
		try {   
			Integer.parseInt(input);   
			return true;   
		} catch( Exception e ){   
			return false;   
		}   
	} 	
}
