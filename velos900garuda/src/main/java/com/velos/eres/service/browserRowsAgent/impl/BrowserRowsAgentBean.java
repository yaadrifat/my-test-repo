/*
 * Classname			BrowserRowsAgentBean
 * 
 * Version information 	1.0
 *
 * Date					12/23/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.browserRowsAgent.impl;

import java.util.Hashtable;

import javax.ejb.Stateless;

import com.velos.eres.service.browserRowsAgent.BrowserRowsAgentRObj;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB <br>
 * <br>
 * 
 * @author Sonia Sahni
 * @version 1.0 12/23/2003
 */
@Stateless
public class BrowserRowsAgentBean implements BrowserRowsAgentRObj {

    /**
     * 
     * getPageRows Calls getPageRows on BrowserRows
     */

    public BrowserRows getPageRows(long page, long rowsRequired, String bSql,
            long totalPages, String countSql, String orderBy, String order) {
        BrowserRows br = new BrowserRows();

        try {

            br.getPageRows(page, rowsRequired, bSql, totalPages, countSql,
                    orderBy, order);

        } catch (Exception e) {
            Rlog.fatal("browserRows",
                    "Exception in getPageRows() in BrowserRowsAgentBean" + e);
        }
        return br;
    }

    /**
     * 
     * getSchPageRows Calls getSchPageRows on BrowserRows
     */

    public BrowserRows getSchPageRows(long page, long rowsRequired,
            String bSql, long totalPages, String countSql, String orderBy,
            String order) {
        BrowserRows br = new BrowserRows();

        try {

            br.getSchPageRows(page, rowsRequired, bSql, totalPages, countSql,
                    orderBy, order);

        } catch (Exception e) {
            Rlog.fatal("browserRows",
                    "Exception in getPageRows() in BrowserRowsAgentBean" + e);
        }
        return br;
    }

    public Hashtable getSchedulePageRows(long page, long rowsRequired,
            String bSql, long totalPages, String countSql, String orderBy,
            String order, String schWhere) {
        // Hashtable htReturn = new Hashtable();
        Hashtable htCRF = new Hashtable();
        // Hashtable htVisitCRF = new Hashtable();

        BrowserRows br = new BrowserRows();

        try {

            htCRF = br.getSchedulePageRows(page, rowsRequired, bSql,
                    totalPages, countSql, orderBy, order, schWhere);

            // add Browser Rows OBJ in the returned hashtable

            htCRF.put("schedule", br);

        } catch (Exception e) {
            Rlog.fatal("browserRows",
                    "Exception in getPageRows() in BrowserRowsAgentBean" + e);
        }

        return htCRF;

    }

}// end of class
