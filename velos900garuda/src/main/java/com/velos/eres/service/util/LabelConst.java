package com.velos.eres.service.util;

/**
 * @deprecated
 * Use LC.java instead.
 * 
 * Class to hold all the strings of keys in labelBundle.properties file.
 * Make all key strings public static final.
 * Start each key with a category prefix, and arrange them alphabetically.
 */
public class LabelConst {

    public static final String Evt_CoverageType = "Evt_CoverageType";
    public static final String Pat_Patient = "Pat_Patient";
    public static final String Reason_For_Change_CT = "Reason_For_Change_CT";
    public static final String Std_Study = "Std_Study";
    public static final String Std_Studies = "Std_Studies";
    public static final String Std_Study_ies="Std_Study_ies";
    public static final String Pat_Patients="Pat_Patients";

}
