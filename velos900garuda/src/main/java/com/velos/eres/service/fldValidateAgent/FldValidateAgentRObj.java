/*
 * Classname : FldValidateAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 11/05/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.fldValidateAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.eres.business.fldValidate.impl.FldValidateBean;

/* End of Import Statements */

/**
 * Remote interface for FldValidateAgent session EJB
 * 
 * @author SoniaKaura
 * @version : 1.0 11/05/2003
 */
@Remote
public interface FldValidateAgentRObj {
    /**
     * gets the FldValidate record
     */
    FldValidateBean getFldValidateDetails(int fldValidateId);

    /**
     * creates a new FldValidate Record
     */
    public int setFldValidateDetails(FldValidateBean fldValidatesk);

    /**
     * updates the FldValidate Record
     */
    public int updateFldValidate(FldValidateBean fldValidatesk);

    /**
     * remove FldValidate Record
     */
    public int removeFldValidate(int fldValidateId);

    FldValidateBean findByFieldId(int fldlibId);

}
