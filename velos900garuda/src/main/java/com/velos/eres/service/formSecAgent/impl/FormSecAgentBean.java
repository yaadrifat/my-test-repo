/*
 * Classname : FormSecAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 17/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.formSecAgent.impl;

/* Import Statements */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.FieldActionDao;
import com.velos.eres.business.common.FormSecDao;
import com.velos.eres.business.formNotify.impl.FormNotifyBean;
import com.velos.eres.business.formSec.impl.FormSecBean;
import com.velos.eres.service.formSecAgent.FormSecAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.util.Hashtable;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.business.audit.impl.AuditBean;
/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FormSecAgentBean.<br>
 * <br>
 * 
 * @author SoniaKaura
 * @see FormNotifyBean
 * @version 1.0 17/07/2003
 * @ejbHome FormSecAgentHome
 * @ejbRemote FormFeildAgentRObj
 */
@Stateless
public class FormSecAgentBean implements FormSecAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * 
     * Looks up on the FormSec id parameter passed in and constructs and returns
     * a FormSec state keeper. containing all the details of the Form Section<br>
     * 
     * @param formSecId
     *            the FormSec id
     */

    public FormSecBean getFormSecDetails(int formSecId) {
        FormSecBean retrieved = null;

        try {

            retrieved = (FormSecBean) em.find(FormSecBean.class, new Integer(
                    formSecId));

            return retrieved;

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Error in getFormSecDetails() in FormSecAgentBean" + e);
        }

        return retrieved;

    }

    /**
     * Creates FormSec record.
     * 
     * @param a
     *            State Keeper containing the FormSec attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setFormSecDetails(FormSecBean formSecsk) {
        try {
            FormSecBean fb = new FormSecBean();
            fb.updateFormSec(formSecsk);
            em.persist(fb);
            return fb.getFormSecId();
        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Error in setFormSecDetails() in FormSecAgentBean " + e);
            return -2;
        }

    }

    /**
     * Updates a FormSec record.
     * 
     * @param a
     *            FormSec state keeper containing FormSec attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updateFormSec(FormSecBean formSecsk) {

        FormSecBean retrieved = null;
        // FormSecRObj remoteObj = null;
        int returnVal; // Entity Bean Remote Object
        String formSecRepNo = "";
        int output;

        try {

            formSecRepNo = formSecsk.getFormSecRepNo();

            retrieved = (FormSecBean) em.find(FormSecBean.class, new Integer(
                    formSecsk.getFormSecId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFormSec(formSecsk);
            em.merge(retrieved);
            if (formSecRepNo.equals("0")) {
                Rlog.debug("formsection", "FormSecAgentBean.%%% inside if");
                updateBrowserFlag(formSecsk.getFormSecId());

            }

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Error in updateFormSec in FormSecAgentBean" + e);
            return -2;
        }

        return output;
    }

    //Overloaded for INF-18183 ::: Ankit
    public int updateFormSec(FormSecBean formSecsk, Hashtable<String, String> auditInfo) {

        FormSecBean retrieved = null;
        // FormSecRObj remoteObj = null;
        int returnVal; // Entity Bean Remote Object
        String formSecRepNo = "";
        int output;

        try {

            formSecRepNo = formSecsk.getFormSecRepNo();

            retrieved = (FormSecBean) em.find(FormSecBean.class, new Integer(
                    formSecsk.getFormSecId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFormSec(formSecsk);
            if(output == 0){
	            AuditBean audit=null;
	            String currdate =DateUtil.getCurrentDate();
	            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
	            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
	            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
	            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
	            String getRidValue= AuditUtils.getRidValue("er_formsec","eres","pk_formsec="+retrieved.getFormSecId());/*Fetches the RID/PK_VALUE*/ 
	            audit = new AuditBean("ER_FORMSEC",String.valueOf(retrieved.getFormSecId()),getRidValue,
	        			userID,currdate,moduleName,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
	        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            }
            
            em.merge(retrieved);
            if (formSecRepNo.equals("0")) {
                Rlog.debug("formsection", "FormSecAgentBean.%%% inside if");
                updateBrowserFlag(formSecsk.getFormSecId());

            }

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("formsection",
                    "Error in updateFormSec in FormSecAgentBean" + e);
            return -2;
        }

        return output;
    }
    
    /**
     * Removes a FormSec record.
     * 
     * @param a
     *            FormSec Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeFormSec(int formSecId) {
        int output;
        FormSecBean retrieved = null;

        try {

            retrieved = (FormSecBean) em.find(FormSecBean.class, new Integer(
                    formSecId));

            em.remove(retrieved);
            Rlog.debug("formsection", "Removed FormSec row with FormSecID = "
                    + formSecId);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formsection", "Exception in removeFormSec " + e);
            return -1;

        }

    }

    /**
     * Inserts multiple sections to the ER_FORMSEC
     * 
     * @param formSecDao
     * @return int
     */
    public int insertNewSections(FormSecDao formSecDao) {
        try {

            int rows = formSecDao.getRows();
            int ret = 0;
            int formLibId = 0;
            int seq = 0;
            int retVal = 0;

            for (int i = 0; i < rows; i++) {

                FormSecBean formSecsk = new FormSecBean();
                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+i );

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormLibId(i) );
                formSecsk.setFormLibId((String) formSecDao.getFormLibId(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecName(i));
                formSecsk.setFormSecName((String) formSecDao.getFormSecName(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecSeq(i));
                formSecsk.setFormSecSeq((String) formSecDao.getFormSecSeq(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecFmt(i));
                formSecsk.setFormSecFmt((String) formSecDao.getFormSecFmt(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecRepNo(i));
                formSecsk.setFormSecRepNo((String) formSecDao
                        .getFormSecRepNo(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getRecordType(i));
                formSecsk.setRecordType((String) formSecDao.getRecordType(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getCreator(i));
                formSecsk.setCreator((String) formSecDao.getCreator(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getIpAdd(i));
                formSecsk.setIpAdd((String) formSecDao.getIpAdd(i));
                ret = setFormSecDetails(formSecsk);
                Rlog.debug("formsection",
                        "FormSecAgentBean.INSIDE ROWS AFTER SETTING###" + ret);

            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Exception In insertNewSections in FormSecAgentBean " + e);
            return -2;
        }

    }

    /**
     * 
     * 
     * @param formLibId
     * @return FormSecDao
     */
    public FormSecDao getAllSectionsOfAForm(int formLibId) {

        try {
            Rlog.debug("formsection",
                    "FormSecAgentBean.getFieldsFromSearch starting");
            FormSecDao formsecDao = new FormSecDao();
            formsecDao.getAllSectionsOfAForm(formLibId);
            return formsecDao;
        } catch (Exception e) {
            Rlog
                    .fatal("formsection",
                            "Exception In getFieldsFromSearch in FormSecAgentBean "
                                    + e);
        }
        return null;
    }

    /**
     * Updates multiple sections of the ER_FORMSEC
     * 
     * @param formSecDao
     * @return int
     */
    public int updateNewSections(FormSecDao formSecDao) {
        try {
            Rlog.debug("formsection",
                    "FormSecAgentBean.updateNewSections starting");
            int rows = formSecDao.getRows();
            int ret = 0;
            int formLibId = 0;
            int seq = 0;

            for (int i = 0; i < rows; i++) {

                FormSecBean formSecsk = new FormSecBean();
                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+i );

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+ (String)formSecDao.getFormSecId(i) );
                formSecsk.setFormSecId(StringUtil.stringToNum(formSecDao
                        .getFormSecId(i).toString()));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormLibId(i) );
                formSecsk.setFormLibId((String) formSecDao.getFormLibId(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecName(i));
                formSecsk.setFormSecName((String) formSecDao.getFormSecName(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecSeq(i));
                formSecsk.setFormSecSeq((String) formSecDao.getFormSecSeq(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecFmt(i));
                formSecsk.setFormSecFmt((String) formSecDao.getFormSecFmt(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getFormSecRepNo(i));
                formSecsk.setFormSecRepNo((String) formSecDao
                        .getFormSecRepNo(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getRecordType(i));
                formSecsk.setRecordType((String) formSecDao.getRecordType(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getModifiedBy(i));
                formSecsk.setModifiedBy((String) formSecDao.getModifiedBy(i));

                // Rlog.debug("formsection","FormSecAgentBean.INSIDE ROWS
                // starting###"+(String) formSecDao.getIpAdd(i));
                formSecsk.setIpAdd((String) formSecDao.getIpAdd(i));
                getFormSecDetails(formSecsk.getFormSecId());
                ret = updateFormSec(formSecsk);
                Rlog.debug("formsection",
                        "FormSecAgentBean.INSIDE ROWS AFTER SETTING###" + ret);
            }

            return ret;
        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Exception In updateNewSections in FormSecAgentBean " + e);
            return -2;
        }

    }

    public int updateDefaultFormSec(int pkFormSec, int pkForm, String user,
            String ipAdd) {

        int ret = 0;
        try {
            Rlog.debug("formsection",
                    "In updateDefaultFormSec in FormLibAgentBean - 0");
            FormSecDao formSecDao = new FormSecDao();
            ret = formSecDao.updateDefaultFormSec(pkFormSec, pkForm, user,
                    ipAdd);

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Exception In updateDefaultFormSec in FormSecAgentBean "
                            + e);
            return -2;
        }

        return ret;
    }

    /*
     * This Function sets the browser flag to 1 for the data entry date field
     * when its repeat number is set to 0
     */
    public int updateBrowserFlag(int pkFormSec) {

        int ret = 0;
        try {
            Rlog.debug("formsection",
                    "In updateBrowserFlag in FormLibAgentBean - 0");
            FormSecDao formSecDao = new FormSecDao();
            ret = formSecDao.updateBrowserFlag(pkFormSec);

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Exception In updateBrowserFlag in FormSecAgentBean " + e);
            return -2;
        }

        return ret;
    }

    public FormSecBean findByFormIdAndSeq(int formLibId, int seq) {

        FormSecBean retrieved = null;

        try {

            Query query = em.createNamedQuery("findByFormIdAndSeq");
            query.setParameter("fk_formlib", formLibId);
            query.setParameter("formsec_seq", seq);
            retrieved = (FormSecBean) query.getSingleResult();

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Exception in findByFormIdAndSeq in formsecAgentBean " + e);
        }

        return retrieved;
    }
    
    /**
     * returns true if field as any inter field action associated with the field
     * 
     * @param sectionId
     * 
     *            
     */

    public boolean sectionHasInterFieldActions(String sectionId) 
    {

        try {
            Rlog.debug("formsection","In sectionHasInterFieldActions in FormSecAgentBean - 0");
             
            return FieldActionDao.sectionHasInterFieldActions(sectionId);

        } catch (Exception e) {
            Rlog.fatal("formsection",
                    "Exception In sectionHasInterFieldActions in FormSecAgentBean " + e);
            return true;
        }

    	
    }

}
