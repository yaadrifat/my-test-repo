
package com.velos.eres.service.specimenApndxAgent.impl;

import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.SpecimenApndxDao;
import com.velos.eres.business.common.StorageAllowedItemsDao;
import com.velos.eres.business.mileApndx.impl.MileApndxBean;
import com.velos.eres.business.specimenApndx.impl.SpecimenApndxBean;
import com.velos.eres.service.specimenApndxAgent.SpecimenApndxAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.audit.impl.AuditBean;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;


@Stateless
public class SpecimenApndxAgentBean implements SpecimenApndxAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    public int setSpecimenApndxDetails(SpecimenApndxBean pask) {
        SpecimenApndxBean specApndxBean=new SpecimenApndxBean();
        try {
            specApndxBean.updateSpecimenApndx(pask);
            em.persist(specApndxBean);
            return specApndxBean.getSpecApndxId();
        } catch (Exception e) {
            Rlog.fatal("specapndx", "Exception in setSpecimenApndxDetails........." + e);
            return -1;
        }
    }
  
    public SpecimenApndxBean getSpecimenApndxDetails(int specApndxId) {
        try {
        	        	
            return (SpecimenApndxBean) em.find(SpecimenApndxBean.class, new Integer(
                    specApndxId));
        } catch (Exception e) {
            Rlog.fatal("specapndx", "Exception in getSpecimenApndxDetails" + e);
            return null;
        }
    }

    public int updateSpecimenApndx(SpecimenApndxBean pask) {
        int output = 0;
        SpecimenApndxBean specApndx = null;
        try {
        	
        	SpecimenApndxBean specimenApndxBean = new SpecimenApndxBean();
            specApndx = (SpecimenApndxBean) em.find(SpecimenApndxBean.class, pask.getSpecApndxId());
            output=specApndx.updateSpecimenApndx(pask);
            em.merge(specApndx);
            return output;
        } catch (Exception e) {
            Rlog.fatal("specapndx", "Excepltion in updateSpecimenApndxDetails" + e);
            return -1;
        }
    }
  
    
    public int removeSpecApndx(int specApndxId) {
        SpecimenApndxBean specApndx = null;
          try {
        	  
        	  
           specApndx = (SpecimenApndxBean)em.find(SpecimenApndxBean.class,new Integer(specApndxId));
           em.remove(specApndx);
            return 0;
        } catch (Exception e) {
        	
            Rlog.fatal("specapndx", "Exception in removeSpecApndx" + e);
            return -1;
        }
    }
    
  
    // removeSpecApndx() Overloaded for INF-18183 ::: Raviesh
    public int removeSpecApndx(int specApndxId,Hashtable<String, String> args) {
        SpecimenApndxBean specApndx = null;
          try {
        	  
        	  AuditBean audit=null; //Audit Bean for AppDelete
              String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
              String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
              String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
              String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
              String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
              
              String getRidValue= AuditUtils.getRidValue("ER_SPECIMEN_APPNDX","eres","PK_SPECIMEN_APPNDX="+specApndxId);/*Fetches the RID/PK_VALUE*/ 
          	audit = new AuditBean("ER_SPECIMEN_APPNDX",String.valueOf(specApndxId),getRidValue,
          			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
          	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/                    	  
           specApndx = (SpecimenApndxBean)em.find(SpecimenApndxBean.class,new Integer(specApndxId));
           em.remove(specApndx);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("specapndx", "Exception in removeSpecApndx" + e);
            return -1;
        }
    }
    
    
 public SpecimenApndxDao getSpecimenApndxUris(int pkSpecimen) {
        
        try {
        	
        	SpecimenApndxDao specimenApndxDao = new SpecimenApndxDao();
        	specimenApndxDao.getSpecimenApndxUris(pkSpecimen);
            return specimenApndxDao ;
            
        } catch (Exception e) {
            Rlog.fatal("specapndx", "Exception In  getSpecimenApndxUris in SpecimenApndxAgentBean" + e);
        }
        return null;
    }


   
}