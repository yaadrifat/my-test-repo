/*
 * Classname			StudySiteApndxAgentBean.class
 * 
 * Version information
 *
 * Date					10/29/2004
 * 
 * Copyright notice Aithent
 */

package com.velos.eres.service.studySiteApndxAgent.impl;

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.StudySiteApndxDao;
import com.velos.eres.business.studySiteApndx.impl.StudySiteApndxBean;
import com.velos.eres.service.studySiteApndxAgent.StudySiteApndxAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author
 */

@Stateless
public class StudySiteApndxAgentBean implements StudySiteApndxAgentRObj

{
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
	
    @Resource
	private SessionContext context;

    public StudySiteApndxBean getStudySiteApndxDetails(int studySiteApndxId) {
        StudySiteApndxBean app_ro = null; // Study Site Apndx Entity Bean
        // Remote Object

        try {
            Rlog.debug("studySiteApndx",
                    "IN TRY OF SESSION BEAN getStudySiteApndxDetails");

            app_ro = (StudySiteApndxBean) em.find(StudySiteApndxBean.class,
                    studySiteApndxId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return app_ro;
    }

    /**
     * Sets a StudySiteApndx.
     * 
     * @param StudySiteApndx
     *            a state holder containing study attributes to be set.
     * @return String
     */
    public int setStudySiteApndxDetails(StudySiteApndxBean ssk) {
        int StudySiteApndxID = 0;
        StudySiteApndxBean ssbnew = new StudySiteApndxBean();
        try {

            ssbnew.updateStudySiteApndx(ssk);
            em.persist(ssbnew);

            StudySiteApndxID = ssbnew.getStudySiteApndxId();

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return StudySiteApndxID;
    }

    public int updateStudySiteApndx(StudySiteApndxBean appsk) {
        StudySiteApndxBean retrieved = null; // User Entity Bean Remote
        // Object
        int output;
        try {

            retrieved = (StudySiteApndxBean) em.find(StudySiteApndxBean.class,
                    appsk.getStudySiteApndxId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateStudySiteApndx(appsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("studySiteApndx",
                    "Error in updateStudySiteApndx in StudySiteApndxAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public StudySiteApndxDao getStudySitesApndxValues(int studySiteId,
            String type) {
        try {
            Rlog
                    .debug("studySiteApndx",
                            "In getStudySitesApndxValues in StudySiteApndxAgentBean line number 1");
            StudySiteApndxDao studySiteApndxDao = new StudySiteApndxDao();

            studySiteApndxDao.getStudySitesApndxValues(studySiteId, type);
            Rlog
                    .debug("studySiteApndx",
                            "In getStudySitesApndxValues in StudySiteApndxAgentBean line number 2");
            return studySiteApndxDao;
        } catch (Exception e) {
            Rlog.fatal("studySiteApndx",
                    "Exception In getStudySitesApndxValues in StudySiteApndxAgentBean "
                            + e);
            return null;
        }
    } // end of getByStudyIdAndType

    public int removeStudySiteApndx(int studySiteApndxId) {
        int output;
        StudySiteApndxBean app_ro = null; // Study Appendix Entity Bean Remote
        // Object
        try {

            app_ro = (StudySiteApndxBean) em.find(StudySiteApndxBean.class,
                    studySiteApndxId);
            em.remove(app_ro);

            Rlog.debug("studySiteApndx",
                    "Removed study site appendix row with studySiteApndxId="
                            + studySiteApndxId);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeStudySiteApndx(int studySiteApndxId,Hashtable<String, String> auditInfo) {
       
        StudySiteApndxBean app_ro = null; // Study Appendix Entity Bean Remote Object
        AuditBean audit=null;
       
        try {
        
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_STUDYSITES_APNDX","eres","PK_STUDYSITES_APNDX="+studySiteApndxId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_STUDYSITES_APNDX",String.valueOf(studySiteApndxId),rid,userID,currdate,app_module,ipAdd,reason);
        	
            app_ro = (StudySiteApndxBean) em.find(StudySiteApndxBean.class,
                    studySiteApndxId);
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(app_ro);

            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            e.printStackTrace();
            return -1;
        }
    }


}// end of class

