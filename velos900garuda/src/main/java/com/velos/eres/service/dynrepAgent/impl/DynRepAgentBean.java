/*
 * Classname : DynRepAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/05/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol
 */

package com.velos.eres.service.dynrepAgent.impl;

/* Import Statements */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.DynRepDao;
import com.velos.eres.business.dynrep.impl.DynRepBean;
import com.velos.eres.service.dynrepAgent.DynRepAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * LabAgentBean.<br>
 * <br>
 * 
 * @author Vishal Abrol
 * @see Dyn
 * @version 1.0 11/05/2003
 * @ejbHome LabAgentHome
 * @ejbRemote LabAgentRObj
 */

@Stateless
public class DynRepAgentBean implements DynRepAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    public DynRepBean getDynRepDetails(int id) {
        DynRepBean retrieved = null;

        try {
            retrieved = (DynRepBean) em.find(DynRepBean.class, new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("dynrep", "Exception in getDynRepDetails() in DynRepAgentBean" + e);
        }

        return retrieved;

    }

    /**
     * Creates dynrep record.
     * 
     * @param a
     *            State Keeper containing the dynrep attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setDynRepDetails(DynRepBean dsk) {
        try {
            DynRepBean db = new DynRepBean();

// JM: 13July2006
            Query query = em.createNamedQuery("findDynRepName");
            query.setParameter("repName", dsk.getRepName());
            query.setParameter("accountId", dsk.getAccId());
            
            ArrayList list1 = (ArrayList) query.getResultList();
            if (list1 == null)
                list1 = new ArrayList();
            //if same dynRep exists
            if (list1.size() > 0) {
                Rlog.debug("DynRep","DynRepAgentBean.setDynRepDetails : given report template name already exist"); 
                return -3;
            }
///////
            
            db.updateDynRep(dsk);
            em.persist(db);
            return db.getId();
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in setDynRepDetails() in DynRepAgentBean " + e);
            return -2;
        }

    }

    /**
     * Updates a dynrep record.
     * 
     * @param a
     *            dynrep state keeper containing studyId attributes to be set.
     * @return int for successful:0 ; e\Exception : -2
     */
    public int updateDynRep(DynRepBean dsk) {

        DynRepBean retrieved = null; 
        int output;

        try {

            retrieved = (DynRepBean) em.find(DynRepBean.class, new Integer(dsk
                    .getId()));           
            
            if (retrieved == null) {
                return -2;
            }
            
// JM: 13July2006: Checking for duplicate template names 
            
            try {
                if (!((retrieved.getRepName()).toUpperCase())
                        .equals((dsk.getRepName()).toUpperCase())) {
                	Query query = em.createNamedQuery("findDynRepName");
                    query.setParameter("repName", dsk.getRepName());
                    query.setParameter("accountId", dsk.getAccId());
                    
                    ArrayList list1 = (ArrayList) query.getResultList();
                    if (list1 == null)
                        list1 = new ArrayList();
                    //if same dynRep exists
                    if (list1.size() > 0) {
                        Rlog.debug("DynRep", "DynRepAgentBean.updateDynRep : given report template name already exist"); 
                        return -3;
                    }

                }
     
 
                } catch (Exception e) {
                Rlog.debug("DynRep","This given report template name does not exist in database" + e);
            }
            
            
// JM: 13July2006:           
            
            
            output = retrieved.updateDynRep(dsk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog
                    .fatal("dynrep", "Error in updateDynRep in DynRepAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a dynrep record.
     * 
     * @param a
     *            dynrep Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeDynRep(int id) {
        int output;
        DynRepBean retrieved = null;

        try {

            retrieved = (DynRepBean) em.find(DynRepBean.class, new Integer(id));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Exception in removeDynrep" + e);
            return -1;

        }

    }
    
    // removeDynRep() Overloaded for INF-18183 ::: Raviesh
    public int removeDynRep(int id,Hashtable<String, String> args) {
        int output;
        DynRepBean retrieved = null;
        try {
        	  AuditBean audit=null; //Audit Bean for AppDelete
              String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
              String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
              String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
              String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
              String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
              
              String getRidValue= AuditUtils.getRidValue("ER_DYNREP","eres","PK_DYNREP="+id);/*Fetches the RID/PK_VALUE*/ 
          	audit = new AuditBean("ER_DYNREP",String.valueOf(id),getRidValue,
          			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
          	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/              
          	retrieved = (DynRepBean) em.find(DynRepBean.class, new Integer(id));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("dynrep", "Exception in removeDynrep" + e);
            return -1;
        }
    }

    public DynRepDao getMapData(String formId) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep", "DynRepDoa in DynRepAgentBean:getMaPData"
                    + dyndao);
            dyndao.getMapData(formId);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In getMAdata(formid) in DynRepAgentBean" + e);

        }
        return dyndao;
    }

    public DynRepDao getReportData(String sql, String[] fldNames,
            String formType  , Hashtable moreParameters ) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep", "DynRepDao in DynRepAgentBean:getReportData"
                    + dyndao);
            dyndao.getReportData(sql, fldNames, formType  ,moreParameters);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In getReportdata(sql,fldNames) in DynRepAgentBean"
                            + e);
           e.printStackTrace(); 

        }
        return dyndao;
    }

    public DynRepDao getReportData(String sql, String[] fldNames,
            String formType, ArrayList fldTypes , Hashtable moreParameters) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep", "DynRepDao in DynRepAgentBean:getReportData"
                    + dyndao);
            dyndao.getReportData(sql, fldNames, formType, fldTypes,moreParameters);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In getReportdata(sql,fldNames) in DynRepAgentBean"
                            + e);
            e.printStackTrace();

        }
        return dyndao;
    }

    public DynRepDao getReportDetails(String repIdStr) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep",
                    "DynRepDao in DynRepAgentBean:getReportDetails" + dyndao);
            dyndao.getReportDetails(repIdStr);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In getReportdetails(repIdStr) in DynRepAgentBean"
                            + e);

        }
        return dyndao;

    }

    public DynRepDao populateHash(String repIdStr) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep", "DynRepDao in DynRepAgentBean:populateHash"
                    + dyndao);
            dyndao.populateHash(repIdStr);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In populateHash(repIdStr) in DynRepAgentBean"
                            + e);

        }
        return dyndao;

    }

    public int setFilterDetails(DynRepDao dynDao, String mode) {
        DynRepDao dyndao = new DynRepDao();
        int ret = 0;
        try {
            Rlog.debug("dynrep",
                    "DynRepDao in DynRepAgentBean:setFilterDetails" + dyndao);
            ret = dyndao.setFilterDetails(dynDao, mode);

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In setFilterDetails in DynRepAgentBean" + e);
            return -1;

        }
        return ret;

    }

    public DynRepDao getFilter(String repIdStr) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep", "DynRepDao in DynRepAgentBean:getFilter"
                    + dyndao);
            dyndao.getFilter(repIdStr);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In getFilter(repIdStr) in DynRepAgentBean" + e);

        }
        return dyndao;

    }

    public DynRepDao getFilterDetails(String fltrId) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep",
                    "DynRepDao in DynRepAgentBean:getFilterDetails" + dyndao);
            dyndao.getFilterDetails(fltrId);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In getFilterDetails(fltrId) in DynRepAgentBean"
                            + e);

        }
        return dyndao;

    }
    
    public int removeFilters(ArrayList fltrIds) {
        int ret = -1;
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep",
                    "fltrIds in DynRepAgentBean:RemoveFilterDetails" + fltrIds);
            ret = dyndao.removeFilters(fltrIds);

        } catch (Exception e) {
            Rlog
                    .fatal("dyndao",
                            "Exception In removeFilters(fltrId) in DynRepAgentBean"
                                    + e);
            return -2;

        }
        return ret;

    }
    
    // Overloaded for INF-18183 ::: Akshi
    public int removeFilters(ArrayList fltrIds,Hashtable<String, String> args) {
        int ret = -1;
        String condition="";
        DynRepDao dyndao = new DynRepDao();
        try {
        	AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
           
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/    
            
            for(int i=0; i<fltrIds.size(); i++)
        		condition = condition + ", " + fltrIds.get(i);  
        	
        	condition = "PK_DYNREPFILTER IN ("+condition.substring(1)+")";

        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("ER_DYNREPFILTER",condition,"eres");/*Fetches the RID/PK_VALUE*/
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		//String getRidValue= AuditUtils.getRidValues("ER_PATPROT","PK_PATPROT="+condition);/*Fetches the RID/PK_VALUE*/ 
            for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_DYNREPFILTER",pkVal,ridVal,
        			userID,currdate,appmodule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
    			Rlog.debug("dynrep",
                    "fltrIds in DynRepAgentBean:RemoveFilterDetails" + fltrIds);
            ret = dyndao.removeFilters(fltrIds);
            if(ret!=1)
            {context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return -1 else, rollback */
        }
            catch (Exception e) {
        	context.setRollbackOnly();
            Rlog
                    .fatal("dyndao",
                            "Exception In removeFilters(fltrId) in DynRepAgentBean"
                                    + e);
            return -2;
        }
        return ret;
    }

    public DynRepDao getSectionDetails(ArrayList paramCols, String formId) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep",
                    "DynRepDao in DynRepAgentBean:getSectionDetails" + dyndao);
            dyndao.getSectionDetails(paramCols, formId);
            return dyndao;

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "dyndao",
                            "Exception In getSectionDetails(ArrayList paramCols,String formId) in DynRepAgentBean"
                                    + e);

        }
        return dyndao;

    }

    public DynRepDao fillReportContainer(String repIdStr) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog
                    .debug("dynrep",
                            "DynRepDao in DynRepAgentBean:fillReportContainer"
                                    + dyndao);
            dyndao.fillReportContainer(repIdStr);
            return dyndao;

        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In fillReportContainer(repIdStr) in DynRepAgentBean"
                            + e);

        }
        return dyndao;

    }

    public DynRepDao getFormNames(String formIdStr, String delimiter) {
        DynRepDao dyndao = new DynRepDao();
        try {
            Rlog.debug("dynrep", "DynRepDao in DynRepAgentBean:getFormNames"
                    + dyndao);
            dyndao.getFormNames(formIdStr, delimiter);
            return dyndao;
        } catch (Exception e) {
            Rlog.fatal("dyndao",
                    "Exception In getFormNames(repIdStr) in DynRepAgentBean"
                            + e);
        }
        return dyndao;

    }
    
    //JM: 07July2006
    public int copyDynRep(String repName, int id, String usrId){
    	int retnumber = -1 ;
    	
    	DynRepDao dynDao = new DynRepDao();
    	try{
    		Rlog.debug("dynrep", "DynRepDao in DynRepAgentBean:copyDynRep"
                    + dynDao);
    		retnumber = dynDao.copyDynRep(repName,id, usrId);    		
    		
            return retnumber;
    		
    	}catch (Exception e){
    		Rlog.fatal("dyndao",
                    "Exception In copyDynRep(id) in DynRepAgentBean"
                            + e);
    	}
    	return retnumber;
    }

}
