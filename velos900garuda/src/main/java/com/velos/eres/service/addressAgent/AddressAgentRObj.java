/*
 * Classname			AddressAgentRObj.class
 * 
 * Version information   
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.addressAgent;

import javax.ejb.Remote;

import com.velos.eres.business.address.impl.AddressBean;

/**
 * Remote interface for AddressAgent session EJB
 * 
 * @author sajal
 */

@Remote
public interface AddressAgentRObj {
    /**
     * gets the address details
     */
    AddressBean getAddressDetails(int addId);

    /**
     * sets the address details
     */
    public int setAddressDetails(AddressBean addsk);

    public int updateAddress(AddressBean addsk);

    /** deletes an address */
    public int removeAddress(int id);

}
