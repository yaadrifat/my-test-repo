/*
 * Classname			SiteAgentRObj
 * 
 * Version information 	1.0
 *
 * Date					02/27/2001
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.siteAgent;

import javax.ejb.Remote;

import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.site.impl.SiteBean;

/**
 * Remote interface for SiteAgent session EJB
 * 
 * @author Dinesh Khurana
 */
@Remote
public interface SiteAgentRObj {

    /**
     * gets the site details
     */
    public SiteBean getSiteDetails(Integer siteId);

    /**
     * sets the site details
     */
    public int setSiteDetails(SiteBean site);

    public int updateSite(SiteBean ssk);

    public SiteDao getByAccountId(int accId);

    public SiteDao getByAccountId(int accId, String siteType);
   
    // modified on 29th March for organization sorting, bug 2072

    public SiteDao getByAccountId(int accId, String orderBy, String orderType);

    public SiteDao getBySiteTypeCodeId(int accId, String siteType);
    
    public SiteDao getSiteValuesForStudy(int studyId);
    
    public int findBySiteIdentifier(String accId, String siteId);
    
    //Added by Manimaran to fix the Bug2729
    public int findBySiteName(String accId, String siteName);
    
    public SiteDao getSitesByUser(int userId);
}
