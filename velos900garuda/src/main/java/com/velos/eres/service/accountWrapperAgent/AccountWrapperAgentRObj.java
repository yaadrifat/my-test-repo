/*
 * Classname				AccountWrapperAgentRObj
 * 
 * Version information  	1.0 
 *
 * Date						03/14/2001
 * 
 * Copyright notice			Velos Inc
 *
 * Author					Sajal
 */

package com.velos.eres.service.accountWrapperAgent;

import javax.ejb.Remote;

/**
 * Remote interface for AccountWrapperAgent session EJB
 * 
 * @author sajal
 */
@Remote
public interface AccountWrapperAgentRObj {

    /**
     * creates a new account
     */
    // public void createAccount(AccountWrapperStateKeeper awsk);
    /**
     * creates a new user
     */
    // public int createUser(AccountWrapperStateKeeper awsk);
    /**
     * update an existing user
     */

    // public int updateUser(AccountWrapperStateKeeper awsk);
    /**
     * creates a new site
     */

    // public int createSite(SiteBean ssk, AddressBean ask);
    /**
     * update an existing site
     */

    // public int updateSite(SiteBean ssk, AddressBean ask);
}
