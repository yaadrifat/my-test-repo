/*
 * Classname			SessionMaint
 * 
 * Version information  1.0
 *
 * 
 * Copyright notice		Velos Inc.
 * 
 * Author 				From SCP
 */

package com.velos.eres.service.util;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.velos.eres.web.user.UserJB;

// Session validation Bean
public class SessionMaint {
    // checks the session is valid for the user
    // if the user has logged in returns true else false
    public boolean isValidSession(HttpSession session) {
        String userIdObj;
        // if session itself not valid return false
        if (session == null)
            return false;

        // get the user id(that is set when user has logged in) from session
        userIdObj = (String) session.getAttribute("userId");
        if (userIdObj == null)
            return false;

        int userId = StringUtil.stringToNum(userIdObj);

        // if valid user return true else false
        if (userId > 0) {
            return true;
        } else {
            return false;
        }

    }
    
    // Ankit: Bug-13541
    public synchronized boolean inValidSession(HttpSession session) {
    	String adminUser = (String) session.getAttribute("adminUser");
    	if (StringUtil.isEmpty(adminUser)){
	    	String portalUser = (String) session.getAttribute("portalUser");
	    	if (StringUtil.isEmpty(portalUser)){
		    	String userIdObj;
		        UserJB userJB = (UserJB) session.getAttribute("currentUser");	
		   		userIdObj = (String) session.getAttribute("userId");
		   	    int userId = StringUtil.stringToNum(userIdObj);
		   		UserJB userB = new UserJB();
		   		userB.setUserId(userId);
		   		userB.getUserDetails();
		   		if (userB.getUserCurrntSsID()==null || !(userB.getUserCurrntSsID().equalsIgnoreCase(session.getId()))){	
		   			session.invalidate();
		            return false;
		   		}
		   		else
		   			return true;
	    	}else{
	    		//Patient portal user
	    		session.invalidate();
	            return false;
	    	}
    	}else
   			return true;
    }

    // validates the session and redirect to timeout page if the session is not
    // valid.
    public void validateSession(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        if (session == null || isValidSession(session) == false) {
            String errorPage = "http://" + request.getRemoteAddr()
                    + request.getContextPath() + "/jsp/timeout.jsp";
            response.sendRedirect(response.encodeRedirectURL(errorPage));
        }
    }
}