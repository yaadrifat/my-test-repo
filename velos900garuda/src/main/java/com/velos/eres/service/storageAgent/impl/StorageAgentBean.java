/*
 * Classname			StorageAgentBean
 *
 * Version information :
 *
 * Date					25/06/2007
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.storageAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.storage.impl.StorageBean;
import com.velos.eres.service.storageAgent.StorageAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.business.common.StorageDao;


/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP.
 *
 * @author Manimaran
 * @version 1.0, 06/26/2007
 * @ejbHome StorageAgentHome
 * @ejbRemote StorageAgentRObj
 */

@Stateless
public class StorageAgentBean implements StorageAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    public StorageBean getStorageDetails(int pkStorage) {
        StorageBean ap = null;
        try {

            ap = (StorageBean) em.find(StorageBean.class, new Integer(pkStorage));

            CommonDAO cdao = new CommonDAO();
            cdao.populateClob("er_storage","storage_notes"," where pk_storage = " + pkStorage );
            ap.setStorageNotes(cdao.getClobData());

        } catch (Exception e) {
            Rlog.fatal("storage",
                    "Error in getStorageDetails() in StorageAgentBean" + e);
        }

        return ap;
    }

    public int setStorageDetails(StorageBean admin) {

        	StorageBean ad = new StorageBean();
        	int rowfound = 0;

        	try {
                Rlog.debug("storage", "in try bloc of storage agent");


                try {

                    Query queryIdentifier = em.createNamedQuery("findStorageId");
                    queryIdentifier.setParameter("storageId", (admin
                            .getStorageId()));
                    //KM-Account Id added for new requirement change.
                    queryIdentifier.setParameter("accountId", (admin
                            .getAccountId()));

                    ArrayList list = (ArrayList) queryIdentifier
                            .getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        rowfound = 1;
                        return -4; // storage id exists
                    }

                } catch (Exception ex) {
                    Rlog.fatal("storage",
                            "In setStorageDetails call to  FIND STORAGE ID EXCEPTION"
                                    + ex);
                    rowfound = 0;
                }


                //check on name not needed
                /*
                 * try {

                    Query queryIdentifier = em.createNamedQuery("findStorageName");
                    queryIdentifier.setParameter("storageName", (admin
                            .getStorageName()).toUpperCase());
                    queryIdentifier.setParameter("storageId", (admin
                            .getStorageId()));
                    ArrayList list = (ArrayList) queryIdentifier
                            .getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        rowfound = 1;
                        return -3; // storage name exists
                    }

                } catch (Exception ex) {
                    Rlog.fatal("storage",
                            "In setStorageDetails call to  FIND STORAGE NAME EXCEPTION"
                                    + ex);
                    rowfound = 0; // storage name does not exist
                }*/

        	ad.updateStorage(admin);
            em.persist(ad);
            return ad.getPkStorage();
        } catch (Exception e) {
            Rlog.fatal("storage",
                    "Error in setStorageDetails() in StorageAgentBean " + e);
        }
        return 0;
    }

    public int updateStorage(StorageBean ap) {
    	StorageBean adm = null;
    	int rowfound = 0;
        int output;
        try {
            adm = (StorageBean) em.find(StorageBean.class, new Integer(
                    ap.getPkStorage()));
            if (adm == null) {
                return -2;
            }


            if (!(adm.getStorageId()).equals(ap
                    .getStorageId())) {
                // storage id has changed

            	try {

                    Query queryIdentifier = em.createNamedQuery("findStorageId");
                    queryIdentifier.setParameter("storageId", (ap
                            .getStorageId()));
                    //KM-Account Id added for new requirement change.-17JUL08
                    queryIdentifier.setParameter("accountId", (ap
                            .getAccountId()));

                    ArrayList list = (ArrayList) queryIdentifier
                            .getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        rowfound = 1;
                        return -4; // storage id exists
                    }

                } catch (Exception ex) {
                    Rlog.fatal("storage",
                            "In setStorageDetails call to  FIND STORAGE ID EXCEPTION"
                                    + ex);
                    rowfound = 0; // storage name does not exist
                }
            } // end check if storage id is changed


            //     check on name not needed

            /*if (!((adm.getStorageName()).toUpperCase()).equals((ap
                    .getStorageName()).toUpperCase())) {
                // storage name has changed

            	try {

                    Query queryIdentifier = em.createNamedQuery("findStorageName");
                    queryIdentifier.setParameter("storageName", (ap
                            .getStorageName()).toUpperCase());
                    ArrayList list = (ArrayList) queryIdentifier
                            .getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        rowfound = 1;
                        return -3; // storage name exists
                    }

                } catch (Exception ex) {
                    Rlog.fatal("storage",
                            "In setStorageDetails call to  FIND STORAGE NAME EXCEPTION"
                                    + ex);
                    rowfound = 0; // storage name does not exist
                }

            } // end check if storage name is changed
       */

            output = adm.updateStorage(ap);
            em.merge(adm);
        } catch (Exception e) {
            Rlog.debug("storage", "Error in updateStorage in StorageAgentBean"
                    + e);
            return -2;
        }
        return output;
    }


    public int deleteStorages(String[] deleteIds,int flag, int user) {
        StorageDao storageDao = new StorageDao();
        int ret = 0;
        try {
            Rlog.debug("storage", "StorageAgentBean:deleteStorage- 0");
            ret = storageDao.deleteStorages(deleteIds,flag, user);

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageAgentBean:deleteStorages-Exception In deleteStorages "
                    + e);
            return -1;
        }

        return ret;

    }
    // Overloaded for INF-18183 ::: Akshi
    public int deleteStorages(String[] deleteIds,int flag, int user,Hashtable<String, String> args) {
        StorageDao storageDao = new StorageDao();
        int ret = 0;
        String condition="";
        try {
        	AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
           
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            
            for(int i=0; i<deleteIds.length; i++)
        		condition = condition + ", " + deleteIds[i];  
        	
        	condition = "PK_STORAGE IN ("+condition.substring(1)+")";

        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("ER_STORAGE",condition,"eres");/*Fetches the RID/PK_VALUE*/
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		
                        
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_STORAGE",pkVal,ridVal,
        			userID,currdate,appmodule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
            Rlog.debug("storage", "StorageAgentBean:deleteStorage- 0");
            ret = storageDao.deleteStorages(deleteIds,flag, user);
            
            if(ret==-1){context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return 1 else, rollback */
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("storage", "StorageAgentBean:deleteStorages-Exception In deleteStorages "
                    + e);
            return -1;
        }

        return ret;

    }

    public int getCount(String storageName) {
        int count = 0;
        try {
            Rlog.debug("storage", "In getCount line number 0");
            StorageDao storageDao = new StorageDao();
            Rlog.debug("storage", "In getCount line number 1");
            count = storageDao.getCount(storageName);
            Rlog.debug("storage", "In getCount line number 2");
        } catch (Exception e) {
            Rlog.fatal("storage", "Exception In getCount in StorageAgentBean " + e);
        }
        return count;
    }


    public int getCountId(String storageId, String accId) {//KM
        int count = 0;
        try {
            Rlog.debug("storage", "In getCountId line number 0");
            StorageDao storageDao = new StorageDao();
            Rlog.debug("storage", "In getCountId line number 1");
            count = storageDao.getCountId(storageId,accId);
            Rlog.debug("storage", "In getCountId line number 2");
        } catch (Exception e) {
            Rlog.fatal("storage", "Exception In getCountId in StorageAgentBean " + e);
        }
        return count;
    }

    public String getStorageIdAuto() {

    	StorageDao storageDao = new StorageDao();
        String StorageId = "";
        try {
            Rlog.debug("storage", "StorageAgentBean:getStorageIdAuto ...");
            StorageId = storageDao.getStorageIdAuto();

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageAgentBean:getStorageIdAuto-Exception In getStorageIdAuto "
                    + e);
            return StorageId;
        }
        return StorageId;

    }

    public String getStorageIdAuto(String fkCodelstStorageType, int accId) {
        StorageDao storageDao = new StorageDao();
        String storageId = storageDao.getStorageIdAuto(fkCodelstStorageType, accId);
        return storageId == null ? "" : storageId;
    }

    public StorageDao getStorageValue(int pkStorage) {

        try {

            StorageDao storageDao = new StorageDao();
            storageDao.getStorageValue(pkStorage);
            return storageDao;

        } catch (Exception e) {
            Rlog.fatal("storage", "Exception In getCountId in StorageAgentBean " + e);
        }
        return null;
    }

    //JM: 28Dec2007
    public String findAllChildStorageUnitIds(String storageId){


    	StorageDao storageDao = new StorageDao();
        String retString = "";
        try {
            Rlog.debug("storage", "StorageAgentBean:findAllChildStorageUnitIds ...");
            retString = storageDao.findAllChildStorageUnitIds(storageId);

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageAgentBean:findAllChildStorageUnitIds-Exception In findAllChildStorageUnitIds "
                    + e);
            return retString;
        }
        return retString;

    }

    //JM: 22Jan2008
    public String findParentStorageType(String storageId){

    	StorageDao storageDao = new StorageDao();
        String retString = "";
        try {
            Rlog.debug("storage", "StorageAgentBean:findParentStorageType ...");
            retString = storageDao.findParentStorageType(storageId);

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageAgentBean:findParentStorageType-Exception In findParentStorageType "
                    + e);
            return retString;
        }
        return retString;



    }

//  Added by Manimaran to copy StorageUnit and Templates.
    public int copyStorageUnitsAndTemplates(String[] strgPks,String[] copyCounts,String[] strgIds,String[] strgNames,String[] templates,String usr, String ipAdd) {
        StorageDao storageDao = new StorageDao();
        int ret = 0;
        try {
            Rlog.debug("storage", "StorageAgentBean:deleteStorage- 0");
            ret = storageDao.copyStorageUnitsAndTemplates(strgPks,copyCounts,strgIds,strgNames,templates,usr,ipAdd);

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageAgentBean:deleteStorages-Exception In deleteStorages "
                    + e);
            return -1;
        }

        return ret;

    }

    /**
     * Searches storages on the basis of arguments passed in Hashtable args
     * */
    public StorageDao searchStorage(Hashtable args,int account)
    {
        StorageDao storageDao = new StorageDao();

        try {

        	 storageDao.searchStorage(args,account);

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageAgentBean:searchStorage-Exception In searchStorage "
                    + e);
            e.printStackTrace();
            return storageDao;
        }

        return storageDao;



    }

    /**
     * gets immediate children storages on the basis of storage pk
     * */
    public StorageDao getImmediateChildren(int storagePK)
    {
        StorageDao storageDao = new StorageDao();

        try {

        	 storageDao.getImmediateChildren( storagePK);

        } catch (Exception e) {
            Rlog.fatal("storage", "StorageAgentBean:getImmediateChildren-Exception In getImmediateChildren "
                    + e);
            e.printStackTrace();
            return storageDao;
        }

        return storageDao;



    }

  //JM: 21Aug2009
    public StorageDao getAllChildren(int pkStorageKit) {

        try {

            StorageDao storageDao = new StorageDao();
            storageDao.getAllChildren(pkStorageKit);
            return storageDao;

        } catch (Exception e) {
            Rlog.fatal("storage", "Exception In getAllChildren in StorageAgentBean " + e);
        }
        return null;
    }

    public StorageDao getSpecimensAndPatients(int pkStorage, int account) {
        try {
            StorageDao storageDao = new StorageDao();
            storageDao.getSpecimensAndPatients(pkStorage, account);
            return storageDao;
        } catch (Exception e) {
            Rlog.fatal("storage", "Exception in getSpecimensAndPatients in StorageAgentBean " + e);
        }
        return null;
    }

}
