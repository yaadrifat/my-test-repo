package com.velos.eres.service.util;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.velos.eres.business.common.LkpFilterDao;

/**
 * Utility class to register all known SQL filters so that we can reject any unknown filter request
 * in order to prevent SQL injection from getlookup.jsp and getmultilookup.jsp.
 */
public class FilterUtil {
    public static final String fakeFilter = "1=2";
    public static final String emptyString = "";
    public static final String NUMBER = "NUMBER";
    public static final String NUMBER_LIST = "NUMBER_LIST";
    public static final String CHAR2 = "CHAR2";
    public static final String VARCHAR = "VARCHAR";
    public static final String VARCHAR_NO_SPACES = "VARCHAR_NO_SPACES";
    /**
     * The literalHash contains static strings which can be safely plugged into a SQL.
     * It is loaded at the startup time. Only accept safe strings in this hash.
     */
    private static HashMap literalHash = null;
    
    /**
     * The paramHash contains key, SQL, and an arbitrary number of parameter values.
     * This hash is used for the dfilter values that take on the format "key|value1|value2|...".
     * For example, if the key is "addeventkit", value1 corresponds to parameter ?1 in
     * the string paramHash.get("addeventkit").
     * 
     * It is possible to write a custom method to do the replacement. The custom method 
     * could be in validateDFilter() or another method called by validateDFilter(). 
     * Make sure to sanitize every parameter value according to its data type.
     * 
     * To add a new filter to paramHash, insert a record into ER_LKPFILTER and refresh cache.
     */
    private static HashMap paramHash = null;
    
    /**
     * Make the constructor private to disallow instantiation. All methods in this class
     * should be static and called statically.
     */
    private FilterUtil() {}
    
    /**
     * Make a DB call to populate paramHash from ER_LKPFILTER.
     * This method is called at startup and also when a cache refresh is requested.
     */
    public static void fillParamHashFromLkpFilterDao() {
        if (paramHash == null) { paramHash = new HashMap(); } 
        else { paramHash.clear(); }
        LkpFilterDao lkpFilterDao = new LkpFilterDao();
        lkpFilterDao.getLkpFilterData();
        ArrayList<String> lkpFilterKey = lkpFilterDao.getLkpFilterKey();
        ArrayList<String> lkpFilterSQL = lkpFilterDao.getLkpFilterSQL();
        ArrayList<String> lkpFilterParamType = lkpFilterDao.getLkpFilterParamType();
        for (int iX=0; iX<lkpFilterKey.size(); iX++) {
            LkpFilterObject lfObj = new LkpFilterObject(lkpFilterKey.get(iX),
                    lkpFilterSQL.get(iX), lkpFilterParamType.get(iX));
            paramHash.put(lkpFilterKey.get(iX), lfObj);
        }
    }
    
    static {
        // Load literalHash at startup
        literalHash = new HashMap();
        literalHash.put("codelst_type='study_division'", "Y");
        literalHash.put("codelst_type='tarea'", "Y");
        literalHash.put("codelst_type='disease_site'", "Y");
        literalHash.put("codelst_type='patStatus'", "Y");
        literalHash.put("codelst_type='research_type'", "Y");
        literalHash.put("codelst_type='studystat'", "Y");
        literalHash.put("codelst_type='sponsor'", "Y");
        literalHash.put("storage_subtyp!='Kit' and STORAGE_ISTEMPLATE!='Yes'", "Y");
        
        // Load paramHash at startup
        fillParamHashFromLkpFilterDao();
    }
    
    /**
     * The main method to be called by getlookup.jsp and getmultilookup.jsp.
     * The incoming HTTP request should only contain minimum key info and parameter values, not SQL.
     * The output of this method should not be passed back and forth between the client and server.
     * It should remain on the server side. If any parameter value is suspicious, return a 1=2 which
     * will result in no match found for the client.
     * 
     * @param request - incoming HTTP request
     * @return - Authorized SQL filter to be passed directly to BrowserRows. 
     */
    public static String validateDFilter(HttpServletRequest request) {
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return fakeFilter; }
        String str = request.getParameter("dfilter");
        if (str == null) { return null; }
        if (str.length() == 0) { return str; }
        if ("Y".equals(literalHash.get(str))) { return str; }
        String[] parts = str.split("\\|");
        String sql = null;
        if ("study".equals(parts[0])) {
            String accId = StringUtil.trueValue((String)tSession.getAttribute("accountId"));
            if (accId == null) { return fakeFilter; }
            return " fk_account= " + accId;
        }
        if ("formQuery".equals(parts[0])) {
            String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
            if (userId == null) { return fakeFilter; }
            return "(pkg_util.f_getStudyRight(pkg_studystat.F_GET_USERRIGHTS_FOR_STUDY("+userId+
            ", pk_study),(select CTRL_SEQ from er_ctrltab where CTRL_KEY = 'study_rights' and upper(ctrl_value) = 'STUDYMPAT')) > 0)";            
        }
        if ("linkFormType".equals(parts[0])) {
            sql = getLinkFormTypeFilter(request, parts);
            return sql != null ? sql : fakeFilter;
        }
        LkpFilterObject lfObj = null;
        if ("lookupType".equals(parts[0])) {
            if ((lfObj = (LkpFilterObject)paramHash.get(parts[0])) == null) { return fakeFilter; }
            if ((sql = lfObj.getLkpFilterSQL()) == null) { return fakeFilter; }
            String accId = StringUtil.trueValue((String)tSession.getAttribute("accountId"));
            String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
            return sql.replaceFirst("[?][1]\\b", accId).replaceFirst("[?][2]\\b", userId);
        }
        if ("mdynreptype".equals(parts[0]) || "portalDesign".equals(parts[0])) {
            sql = getWhereClauseForFormsLookup(request, parts);
            return sql != null ? sql : fakeFilter;
        }
        
        // All the rest of filters configured in ER_LKPFILTER come here 
        if ((lfObj = (LkpFilterObject)paramHash.get(parts[0])) == null) { return fakeFilter; }
        if ((sql = lfObj.getLkpFilterSQL()) == null) { return fakeFilter; }
        if (parts.length == 1) { return sql; }
        String lfParamType = lfObj.getLkpFilterParamType();
        if (lfParamType == null) {
        	//YK 10Mar2011 Bug#5892
         	sql=sql.replaceFirst("[?][1]\\b", parts[1]);  
        	return sql!= null ? sql : fakeFilter; 
        }
        String[] rules = lfParamType.split(",");
        if (rules.length != parts.length-1) {
            Rlog.fatal("getmultilookup", "Numbers of parameters do not match.");
            return fakeFilter;
        }
        for (int iX=0; iX<parts.length-1; iX++) {
            sql = sql.replaceAll("[?]["+(iX+1)+"]\\b", sanitizeParam(parts[iX+1], rules[iX]));
        }
        return sql;
    }
    
    public static String sanitizeText(String input) {
        if (input == null) { return null; }
        if (input.length() == 0) { return emptyString; }
        return sanitizeParam(input, VARCHAR);
    }
    
    /**
     * The custom method to handle the "linkFormType" request.
     * 
     * @param request - incoming HTTP request
     * @param parts - incoming dfilter value split by pipe (|) into String[]. part[0] is always the request identifier.
     * @return - Authorized SQL filter to be passed directly to BrowserRows. 
     */
    private static String getLinkFormTypeFilter(HttpServletRequest request, String[] parts) {
        String type = sanitizeParam(parts[1], CHAR2);
        String linkedStudy = sanitizeParam(parts[2], NUMBER);
        
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return null; }
        String accId = StringUtil.trueValue((String)tSession.getAttribute("accountId"));
        String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
        
        String typeFilter = "";
        String studyFilter = "";
        if ("SA".equals(type)) {
            typeFilter = "'SA'";
        } else if ("S".equals(type)) {
            typeFilter = "'S','SA'";
            studyFilter = "  and (b.fk_study is null or b.fk_study = "+ linkedStudy +")  ";
        } else if ("A".equals(type)) {
            typeFilter = "'A'";
        } else if ("PA".equals(type)) {
            typeFilter = "'PA'";
        } else if ("SP".equals(type)) {
            typeFilter = "'SP','PS','PR','PA'";
            studyFilter = "  and (b.fk_study is null or b.fk_study = "+ linkedStudy +")  ";
        } else if ("PS".equals(type)) {
            typeFilter = "'PA','PS','PR'";
        } else if ("PR".equals(type)) {
            typeFilter = "'PA','PS','PR'";
        }

        String s1 = " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
        "(c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp = 'A' and codelst_type='frmstat')) "+
        " and (c.formstat_enddate is null) and  "+
        " b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
        "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
        " AND (LF_DISPLAYTYPE in  ("+ typeFilter+") " + studyFilter + " ) [VELFILTERSTR] union " +
        "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
        ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status ,lf_displaytype "+ 
        " from er_formlib a,er_linkedforms b,er_formstat c " +
        "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
        " (c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp = 'A' and codelst_type='frmstat')) "+
        " and (c.formstat_enddate is null) and  "+
        " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
        " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
        " AND ( LF_DISPLAYTYPE in  ("+ typeFilter+")  " + studyFilter + " ) " ;
        return s1;
    }
    
    /**
     * The custom method to handle the "mdynreptype" request. The parts array must be 6 in length. 
     * 
     * @param request - incoming HTTP request
     * @param parts - incoming dfilter value split by pipe (|) into String[]. part[0] is always the request identifier.
     * @return - Authorized SQL filter to be passed directly to BrowserRows. 
     */
    private static String getWhereClauseForFormsLookup(HttpServletRequest request, String[] parts) {
        if (parts == null || parts.length < 6) { return fakeFilter; }
        HttpSession tSession = request.getSession(false);
        if (tSession == null) { return fakeFilter; }
        String accId = StringUtil.trueValue((String)tSession.getAttribute("accountId"));
        String userId = StringUtil.trueValue((String)tSession.getAttribute("userId"));
        String uniqueFilterString = getUniqueFilterForFormsLookup(parts);
        String formtype = sanitizeParam(parts[1], CHAR2);
        String study = sanitizeParam(parts[4], NUMBER);
        String pat = sanitizeParam(parts[5], NUMBER);
        String whereClause = null;
        if ("A".equals(formtype)) {
           whereClause = " b.fk_account = "+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D' and "+ 
               "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
           "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
           " and (c.formstat_enddate is null) and  "+
           "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
           "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
           " AND ((LF_DISPLAYTYPE = 'A' )) " + uniqueFilterString +  "[VELFILTERSTR]  union " +
           "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
           ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
           " from er_formlib a,er_linkedforms b,er_formstat c " +
           " where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
           " ((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
           "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
           " and (c.formstat_enddate is null) and  "+
           " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
           " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
           " AND ((LF_DISPLAYTYPE = 'A' )) "  + uniqueFilterString   ;
        } else if ("S".equals(formtype)) {
            if ( Integer.parseInt(study) == 0) {
                whereClause = " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D' and "+ 
                "(fk_study is null)  and "+
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                " AND ((LF_DISPLAYTYPE = 'SA' )) " + uniqueFilterString +  " [VELFILTERSTR]  union  " +
                "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype"+
                " from er_formlib a,er_linkedforms b,er_formstat c " +
                "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                " AND ((LF_DISPLAYTYPE = 'SA' )) "   + uniqueFilterString ;
            } else {
                whereClause = " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                "((fk_study="+ study+ " and LF_DISPLAYTYPE = 'S') or (LF_DISPLAYTYPE = 'SA' ) ) and "+
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                "  " + uniqueFilterString +  " [VELFILTERSTR]  union  "+
                "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
                " from er_formlib a,er_linkedforms b,er_formstat c " +
                "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
                "((fk_study="+ study + " and LF_DISPLAYTYPE = 'S') or (LF_DISPLAYTYPE = 'SA' ) ) and "+ 
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                " AND ((LF_DISPLAYTYPE = 'S' ) or (LF_DISPLAYTYPE = 'SA' )) " + uniqueFilterString   ;
            }
        } else if ("P".equals(formtype)) {
            if (Integer.parseInt(pat) == 0) {
                if (Integer.parseInt(study) == 0) {
                    whereClause =  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                    " fk_study is null  and "+
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                    " b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                    " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR')) " + uniqueFilterString +  " [VELFILTERSTR]  union  " +
                    "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                    ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
                    " from er_formlib a,er_linkedforms b,er_formstat c " +
                    "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+ 
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                    " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                    " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR' )) " + uniqueFilterString ; 
                    //******************************endhere**************************************************//  
                } else {
                    whereClause =  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                    "((fk_study="+ study + " and LF_DISPLAYTYPE = 'SP' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR' ) or (LF_DISPLAYTYPE = 'PA' ) ) and "+
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                    " b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) " +
                    "  " + uniqueFilterString +  " [VELFILTERSTR]  union  "+
                    "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                    ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status,lf_displaytype "+
                    " from er_formlib a,er_linkedforms b,er_formstat c " +
                    "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D'and "+
                    "((fk_study="+ study + " and LF_DISPLAYTYPE = 'SP' ) or (LF_DISPLAYTYPE = 'PS' ) or (LF_DISPLAYTYPE = 'PR') or (LF_DISPLAYTYPE = 'PA' ) ) and "+ 
                    "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                    "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                    " and (c.formstat_enddate is null) and  "+
                    " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                    " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) " + uniqueFilterString  ; 
                    //******************************endhere**************************************************//  
                }
            } else {
                whereClause =  " b.fk_account ="+ accId+"  and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and  b.record_type<>'D'and "+ 
                "((fk_study is null) or (fk_study in (select fk_study from er_patprot where fk_per="+pat+" and patprot_stat=1))) and "+
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                "b.fk_formlib not in (select fk_formlib from er_formorgacc where fk_site not in (select fk_site from er_usersite where fk_user=" +userId +" and usersite_right <> 0)) and "+ 
                "b.fk_formlib not in (select fk_formlib from er_formgrpacc where fk_group not in (select fk_grp from er_usrgrp where fk_user="+userId +" )) "+
                " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS') or (LF_DISPLAYTYPE = 'PR' ) or (LF_DISPLAYTYPE = 'SP') ) " +
                " " + uniqueFilterString +  " [VELFILTERSTR]  union  " +
                "select PK_FORMLIB,FORM_NAME,FORM_DESC,DECODE (trim(LF_DISPLAYTYPE) ,'PA','All Patients','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','S','Study','SA','All Studies','SP','Patient (Specific Study)','-') " +
                ",(SELECT codelst_desc  FROM ER_CODELST WHERE pk_codelst=c.fk_codelst_stat) AS form_status ,lf_displaytype"+
                " from er_formlib a,er_linkedforms b,er_formstat c " +
                "where b.fk_account = "+accId+" and pk_formlib = b.fk_formlib and b.fk_formlib=c.fk_formlib and b.record_type<>'D' and "+
                "((fk_study is null) or (fk_study in (select fk_study from er_patprot where fk_per="+pat+" and patprot_stat=1))) and "+ 
                "((c.fk_codelst_stat in (select pk_codelst from er_codelst where  codelst_subtyp in ('A','D','O') and codelst_type='frmstat')) "+
                "   OR  (c.fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='L'  AND codelst_type='frmstat')))"+ 
                " and (c.formstat_enddate is null) and  "+
                " (b.fk_formlib in (select fk_formlib from er_formgrpacc where fk_group in (select fk_grp from er_usrgrp where fk_user="+userId+" )) "+
                " or b.fk_formlib in (select fk_formlib from er_formorgacc where fk_site in (select fk_site from er_usersite where fk_user="+userId+" and usersite_right <> 0))) "+
                " AND ((LF_DISPLAYTYPE = 'PA' ) or (LF_DISPLAYTYPE = 'PS') or (LF_DISPLAYTYPE = 'PR') or (LF_DISPLAYTYPE = 'SP')) " + uniqueFilterString  ; 
                //******************************endhere**************************************************//
            }
        }
        return whereClause;
    }
    
    /**
     * Private method used only within getWhereClauseForFormsLookup().
     * 
     * @param parts - incoming dfilter value split by pipe (|) into String[]. part[0] is always the request identifier.
     * @return - string that handles form field filter
     */
    private static String getUniqueFilterForFormsLookup(String[] parts) {
        String uniqueSearchType = sanitizeParam(parts[2], CHAR2);
        String searchUniqueId = sanitizeParam(parts[3], VARCHAR_NO_SPACES);
        String uniqueFilterString = null;
        if (searchUniqueId != null && searchUniqueId.length() > 0) {
            uniqueFilterString = " and exists (select * from erv_formflds v where v.pk_formlib = b.fk_formlib and lower(trim(v.fld_uniqueid))  " ;
            if ("E".equals(uniqueSearchType)) {
                uniqueFilterString += " = lower(trim('" + searchUniqueId + "') ))" ;
            }
            else {
                uniqueFilterString += " like lower(trim('%" + searchUniqueId + "%') ))" ;
            }
        } else {
            uniqueFilterString = "";
        }
        return uniqueFilterString;
    }
    
    /**
     * 
     * @param str - String to sanitize
     * @param type - one of predefined data type: NUMBER, NUMBER_LIST, etc.
     * @return - sanitize string or (if suspicious) emptyString
     */
    private static String sanitizeParam(String str, String type) {
        if (str == null || type == null) { return emptyString; }
        type = type.trim().toUpperCase();
        // If NUMBER, reject the string if there is any char which is not a number, period or space.
        if (NUMBER.equals(type)) {
            String tmp = str.replaceAll("[^0-9. ]", "@");
            if (tmp.indexOf("@") > -1) { return emptyString; }
            return str;
        }
        // If NUMBER_LIST, reject the string if there is any char which is not a number, comma, period or space.
        if (NUMBER_LIST.equals(type)) {
            String tmp = str.replaceAll("[^0-9,. ]", "@");
            if (tmp.indexOf("@") > -1) { return emptyString; }
            return str;
        }
        // If CHAR2, reject the string if it contains a whitespace or if it does not contain
        // one or two alphanumeric characters.
        if (CHAR2.equals(type)) {
            if (str.matches(".*\\s+.*") || str.length() > 2) { return emptyString; }
            if (!str.matches("[A-Za-z0-9]{1,2}")) { return emptyString; }
            return str;
        }
        // If VARCHAR_NO_SPACES, reject the string if it contains a whitespace. Also, make a single quote two
        // consecutive single quotes and wipe out some special characters.
        if (VARCHAR_NO_SPACES.equals(type)) {
            if (str.matches(".*\\s+.*")) { return emptyString; }
            return str.replaceAll("'", "''").replaceAll("[*()%]", "");
        }
        // If VARCHAR_NO_SPACES, reject the string if it contains a whitespace. Also, make a single quote two
        // consecutive single quotes and wipe out some special characters.
        if (VARCHAR.equals(type)) {
            return str.replaceAll("'", "''").replaceAll("[*()%]", "");
        }
        return emptyString;
    }

}
