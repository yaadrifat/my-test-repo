/*
 * Classname : PersonAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 06/03/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.service.personAgent.impl;

/* Import Statements */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ejb.SessionContext;
import javax.annotation.Resource;

import com.aithent.audittrail.reports.AuditUtils;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.PatientDao;
import com.velos.eres.business.perApndx.impl.PerApndxBean;
import com.velos.eres.business.person.impl.PersonBean;

import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP - PersonBean.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @see PersonBean
 * @version 1.0 06/03/2001
 * @ejbHome PersonAgentHome
 * @ejbRemote PersonAgentRObj
 */

@Stateless
public class PersonAgentBean implements PersonAgentRObj {
    @PersistenceContext(unitName = "epat")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * 
     * Looks up on the Person id parameter passed in and constructs and returns
     * a Person state keeper. containing all the details of the usrGrp<br>
     * 
     * @param usrGrpId
     *            the Person id
     */
    public PersonBean getPersonDetails(int personPKId) {
        if (personPKId == 0) {
            return null;
        }
        PersonBean pb = null;

        try {

            pb = (PersonBean) em
                    .find(PersonBean.class, new Integer(personPKId));
            
            //JM: 04Oct2006
            CommonDAO cDao = new CommonDAO();
            cDao.populateClob( "person","person_notes_clob"," where pk_person = " + personPKId);
            pb.setPersonNotes(cDao.getClobData());
            


        } catch (Exception e) {
            Rlog.fatal("person",
                    "Error in getPersonDetails() in PersonAgentBean" + e);
        }

        return pb;
    }

    /**
     * Creates a Person record.
     * 
     * @param an
     *            personstate keeper containing person attributes to be set.
     * @return statekeeper ; e\Exception : return null
     */

    public PersonBean setPersonDetails(PersonBean psk) {
        PersonBean pBean = new PersonBean();
        try {
            pBean.updatePerson(psk);            
            em.persist(pBean);

            Rlog.debug("person",
                    "PersonAgentBean.setPersonDetails PersonStateKeeper - PK"
                            + pBean.getPersonPKId());
            return pBean;

        } catch (Exception e) {
            Rlog.fatal("person",
                    "Error in setPersonDetails() in PersonAgentBean " + e);
            return null;
        }

    }

    /**
     * Updates a Person record.
     * 
     * @param an
     *            person state keeper containing person attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */
    public int updatePerson(PersonBean pb) {
        int output;
        PersonBean pbean = null;
        try {
            pbean = (PersonBean) em.find(PersonBean.class, new Integer(pb
                    .getPersonPKId()));

            if (pbean == null) {
                return -2;
            }
            output = pbean.updatePerson(pb);
            em.merge(pbean);

        } catch (Exception e) {
            Rlog
                    .debug("person", "Error in updatePerson in PersonAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public int removePerson(int personPKId) {
        int output;
        PersonBean pbean = null;

        try {

            Rlog.debug("person", "in try bloc of usrGrp agent:remove person");
            pbean = (PersonBean) em.find(PersonBean.class, new Integer(
                    personPKId));
            em.remove(pbean);

            return 0;
        } catch (Exception e) {
            Rlog.debug("person", "Exception in remove PREF " + e);
            return -1;

        }

    }

    public PatientDao getPatients(int usrOrganization, String patientID,
            int patStatus) {
        try {
            PatientDao patientDao = new PatientDao();
            patientDao.getPatients(usrOrganization, patientID, patStatus);
            return patientDao;
        } catch (Exception e) {
            Rlog.fatal("person", "Exception In getPatients in PersonAgentBean "
                    + e);
        }
        return null;

    }

    public PatientDao searchPatients(String siteId, String studyId,
            String patientCode) {
        try {
            PatientDao patientDao = new PatientDao();
            patientDao.searchPatients(siteId, studyId, patientCode);
            return patientDao;
        } catch (Exception e) {
            Rlog.fatal("person",
                    "Exception In searchPatients in PersonAgentBean " + e);
        }
        return null;

    }

    public PatientDao searchPatientsForEnr(String siteId, String studyId,
            String patientCode, String age, String genderVal, String regBy,
            String pstat, String pName, String pStudyExists) {
        try {

            PatientDao patientDao = new PatientDao();
            patientDao.searchPatientsForEnr(siteId, studyId, patientCode, age,
                    genderVal, regBy, pstat, pName, pStudyExists);
            return patientDao;

        } catch (Exception e) {
            Rlog
                    .fatal("person",
                            "Exception In searchPatientsForEnr in PersonAgentBean "
                                    + e);
        }
        return null;

    }

    public int getPatientCompleteDetailsAccessRight(int userId, int groupId,
            int patientId) {
        int returnRight = 0;
        try {

            PatientDao patientDao = new PatientDao();
            returnRight = patientDao.getPatientCompleteDetailsAccessRight(
                    userId, groupId, patientId);
            return returnRight;

        } catch (Exception e) {
            Rlog.fatal("person",
                    "Exception In getPatientCompleteDetailsAccessRight in PersonAgentBean "
                            + e);

        }
        return 0;

    }

    // to delete a patient
    // km
 // Overloaded for INF-18183 ::: Akshi
    public void deletePatient(int patientId,Hashtable<String, String> args) {
    	
    	Connection conn = null;    
    	
    	try {
        
    		String app_module=(String)args.get(AuditUtils.APP_MODULE); //Fetches the APP_MODULE for deletion from the Hashtable
        	String condition ="PK_PERSON="+patientId;
        	Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("PERSON", condition, "epat");
        	conn = AuditUtils.insertAuditRow("PERSON", rowValues, app_module, args, "epat");
        	
        	PatientDao patientDao = new PatientDao();
            int ret=patientDao.deletePatient(patientId);
            if(ret == 0){
            	conn.commit();
            }
        } catch (Exception e) {
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            Rlog.fatal("person",
                    "Exception In deletePatient in PersonStudyAgentBean " + e);
        }finally{
        	try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    public void deletePatient(int patientId) {
        try {
            Rlog.debug("person",
                    "In deletePatient in PersonAgentBean line number 0");
            PatientDao patientDao = new PatientDao();
            Rlog.debug("person",
                    "In deletePatient in PersonAgentBean line number 1");
            patientDao.deletePatient(patientId);
            Rlog.debug("person",
                    "In deletePatient in PersonAgentBean line number 2");
        } catch (Exception e) {
            Rlog.fatal("person",
                    "Exception In deletePatient in PersonStudyAgentBean " + e);
        }
    }
 

    // km

}// end of class

