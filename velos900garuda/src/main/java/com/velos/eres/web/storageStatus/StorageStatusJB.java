/*
 * Classname : 				StorageStatusJB
 *
 * Version information : 	1.0
 *
 * Date 					07/13/2007
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Manimaran
 */

package com.velos.eres.web.storageStatus;

/* IMPORT STATEMENTS */
import java.util.Hashtable;
import com.velos.eres.business.common.StorageStatusDao;
import com.velos.eres.business.storageStatus.impl.StorageStatusBean;
import com.velos.eres.service.storageStatusAgent.StorageStatusAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.common.CommonDAO;

/* END OF IMPORT STATEMENTS */

public class StorageStatusJB {
    private int pkStorageStat;
    private String fkStorage;
    private String ssStartDate;

//  JM: 29Sep2007: SS_END_DATE removed
    //private String ssEndDate;

    private String fkCodelstStorageStat;
    private String ssNotes;
    private String fkUser;
    private String ssTrackingNum;
    private String fkStudy;
    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;


//  GETTERS AND SETTERS

    /**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the fkStorage
	 */
	public String getFkStorage() {
		return fkStorage;
	}

	/**
	 * @param fkStorage the fkStorage to set
	 */
	public void setFkStorage(String fkStorage) {
		this.fkStorage = fkStorage;
	}

	/**
	 * @return the fkStudy
	 */
	public String getFkStudy() {
		return fkStudy;
	}

	/**
	 * @param fkStudy the fkStudy to set
	 */
	public void setFkStudy(String fkStudy) {
		this.fkStudy = fkStudy;
	}

	/**
	 * @return the fkUser
	 */
	public String getFkUser() {
		return fkUser;
	}

	/**
	 * @param fkUser the fkUser to set
	 */
	public void setFkUser(String fkUser) {
		this.fkUser = fkUser;
	}

	/**
	 * @return the ipAdd
	 */
	public String getIpAdd() {
		return ipAdd;
	}

	/**
	 * @param ipAdd the ipAdd to set
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the pkStorageStat
	 */
	public int getPkStorageStat() {
		return pkStorageStat;
	}

	/**
	 * @param pkStorageStat the pkStorageStat to set
	 */
	public void setPkStorageStat(int pkStorageStat) {
		this.pkStorageStat = pkStorageStat;
	}

	/**
	 * @return the ssEndDate
	 */
	/*public String getSsEndDate() {
		return ssEndDate;
	}*/

	/**
	 * @param ssEndDate the ssEndDate to set
	 */
	/*public void setSsEndDate(String ssEndDate) {
		this.ssEndDate = ssEndDate;
	}*/

	/**
	 * @return the ssNotes
	 */
	public String getSsNotes() {
		return ssNotes;
	}

	/**
	 * @param ssNotes the ssNotes to set
	 */
	public void setSsNotes(String ssNotes) {
		this.ssNotes = ssNotes;
	}

	/**
	 * @return the ssStartDate
	 */
	public String getSsStartDate() {
		return ssStartDate;
	}

	/**
	 * @param ssStartDate the ssStartDate to set
	 */
	public void setSsStartDate(String ssStartDate) {
		this.ssStartDate = ssStartDate;
	}

	/**
	 * @return the ssTrackingNum
	 */
	public String getSsTrackingNum() {
		return ssTrackingNum;
	}

	/**
	 * @param ssTrackingNum the ssTrackingNum to set
	 */
	public void setSsTrackingNum(String ssTrackingNum) {
		this.ssTrackingNum = ssTrackingNum;
	}

	/**
	 * @return the fkCodelstStorageStat
	 */
	public String getFkCodelstStorageStat() {
		return fkCodelstStorageStat;
	}

	/**
	 * @param fkCodelstStorageStat the fkCodelstStorageStat to set
	 */
	public void setFkCodelstStorageStat(String fkCodelstStorageStat) {
		this.fkCodelstStorageStat = fkCodelstStorageStat;
	}




//	 END OF THE GETTER AND SETTER METHODS

	/**
     * Constructor
     *
     * @param pkStorageStat:
     *            Unique Id constructor
     *
     */

    public StorageStatusJB(int pkStorageStat) {
        setPkStorageStat(pkStorageStat);
    }

    /**
     * Default Constructor
     */

    public StorageStatusJB() {
        Rlog.debug("storageStatus", "StorageStatusJB.StorageStatusJB() ");
    }

	public StorageStatusJB(int pkStorageStat, String fkStorage, String ssStartDate,String fkCodelstStorageStat, String ssNotes, String fkUser, String ssTrackingNum, String fkStudy, String creator, String modifiedBy, String ipAdd) {

		setPkStorageStat(pkStorageStat);
		setFkStorage(fkStorage);
		setSsStartDate(ssStartDate);
		//setSsEndDate(ssEndDate);
		setFkCodelstStorageStat(fkCodelstStorageStat);
		setSsNotes(ssNotes);
		setFkUser(fkUser);
		setSsTrackingNum(ssTrackingNum);
		setFkStudy(fkStudy);
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
	}


	  /**
     * Retrieves the details of an storage in StorageStateKeeper Object
     *
     * @return StorageStateKeeper consisting of the storage details
     * @see StorageStateKeeper
     */
    public StorageStatusBean getStorageStatusDetails() {
    	StorageStatusBean ssk = null;
        try {
            StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();
            ssk = storageStatusAgentRObj.getStorageStatusDetails(this.pkStorageStat);
            Rlog.debug("storageStatus", "In StorageStatusJB.getStorageStatusDetails() " + ssk);
        } catch (Exception e) {
            Rlog
                    .fatal("storageStatus", "Error in getStorageStatusDetails() in StorageStatusJB "
                            + e);
        }
        if (ssk != null) {
            this.pkStorageStat = ssk.getPkStorageStat();
            this.fkStorage = ssk.getFkStorage();
            this.ssStartDate = ssk.getSsStartDate();
            //this.ssEndDate = ssk.getSsEndDate();
            this.fkCodelstStorageStat = ssk.getFkCodelstStorageStat();
    		this.ssNotes = ssk.getSsNotes();
    		this.fkUser = ssk.getFkUser();
    		this.ssTrackingNum = ssk.getSsTrackingNum();
    		this.fkStudy = ssk.getFkStudy();
    	    this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();

        }
        return ssk;
    }


    public int setStorageStatusDetails() {
        int output = 0;
        try {
        	StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();
            output = storageStatusAgentRObj.setStorageStatusDetails(this
                    .createStorageStatusStateKeeper());

            this.setPkStorageStat(output);
            CommonDAO cDao = new CommonDAO();//KM-to fix the issue 3171
            if(this.getSsNotes()!=null)
            cDao.updateClob(this.getSsNotes(),"er_storage_status","ss_notes"," where pk_storage_status = " + output);

        }catch (Exception e) {
            Rlog
                    .fatal("storageStatus", "Error in setStorageStatusDetails() in StorageStatusJB "
                            + e);
          return -2;
        }
        return output;
    }


    public int updateStorageStatus() {
        int output;
        try {
        	StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();
            output = storageStatusAgentRObj.updateStorageStatus(this
                    .createStorageStatusStateKeeper());

            CommonDAO cDao = new CommonDAO();//KM-to fix the issue 3171
            if(this.getSsNotes()!=null)
            cDao.updateClob(this.getSsNotes(),"er_storage_status","ss_notes"," where pk_storage_status = " + this.getPkStorageStat());

        } catch (Exception e) {
            Rlog.debug("storageStatus",
                    "EXCEPTION IN SETTING STORAGE STATUS DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    public StorageStatusBean createStorageStatusStateKeeper() {
        Rlog.debug("storageStatus", "StorageStatusJB.createStorageStatusStateKeeper ");
        return new StorageStatusBean(this.pkStorageStat, this.fkStorage, this.ssStartDate, this.fkCodelstStorageStat,
        		this.ssNotes, this.fkUser, this.ssTrackingNum,
        		this.fkStudy, this.creator,  this.modifiedBy,  this.ipAdd);
    }


//  JM: 06Sep2007
    public StorageStatusDao getStorageStausValues(int storageId) {

    	StorageStatusDao storStatDao = new StorageStatusDao();
    	try {
    		StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();

    		storStatDao = storageStatusAgentRObj.getStorageStausValues(storageId);
            return storStatDao;

        } catch (Exception e) {
            Rlog.fatal("storageStatus", "Exception In getStorageStausValues in StorageStatusJB " + e);
        }
        return storStatDao;


    }

    //Added by Manimaran for deletion of storage status.
    public int delStorageStatus(int pkStorStat) {
        int ret = 0;
    	try {
        	StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();
            Rlog.debug("storagestatus", "StorageStatusJB.delStorageStatus after remote");
            ret = storageStatusAgentRObj.delStorageStatus(pkStorStat);
        } catch (Exception e) {
            Rlog.fatal("study", "Error in delStorageStatus in StorageStatusJB " + e);
            return -1;
        }
        return ret;
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int delStorageStatus(int pkStorStat,Hashtable<String, String> auditInfo) {
        
    	int ret = 0;
    	try {
        	StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();
            ret = storageStatusAgentRObj.delStorageStatus(pkStorStat,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("study", "Error in delStorageStatus(int pkStorStat,Hashtable<String, String> auditInfo) in StorageStatusJB " + e);
            return -1;
        }
        return ret;
    }    

  //JM: 23Jul2009: #4147
    public boolean isStorageAllocated(int storageId, int accId){
    	boolean ret = false;
    	try {

    		StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();
            ret = storageStatusAgentRObj.isStorageAllocated(storageId,accId);
    	} catch (Exception e) {
    		Rlog.fatal("storageStatus", "Exception In isStorageAllocated in StorageStatusJB " + e);
    		return false;
    	}
    	if (ret){
    		return true;
    	}else{
    		return false;
    	}
    }

    public int getDefStatStudyId(int storageId) {
    	int retStudyId=0;

    	try{
    		StorageStatusAgentRObj storageStatusAgentRObj = EJBUtil.getStorageStatusAgentHome();
    		Rlog.debug("storagestatus", "StorageStatusJB.getDefStatStudyId after remote");
    		retStudyId = storageStatusAgentRObj.getDefStatStudyId(storageId);
    	}catch(Exception e){
    		Rlog.fatal("storageStatus", "Exception in getDefStatStudyId in StorageStatusJB " + e);

    	}
    	return retStudyId;

    }



}// end of class

