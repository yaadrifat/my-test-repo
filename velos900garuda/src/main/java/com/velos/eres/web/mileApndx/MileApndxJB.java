/*
 * Classname : MileApndx JB.java
 * 
 * Version information
 *
 * Date 05/28/2002
 * 
 * Copyright notice: Aithent
 */
package com.velos.eres.web.mileApndx;
import java.util.Hashtable;
import com.velos.eres.business.mileApndx.impl.MileApndxBean;
import com.velos.eres.service.mileApndxAgent.MileApndxAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.esch.service.bgtApndxAgent.BgtApndxAgentRObj;
/**
 * Client side bean for the MileStone Appendix
 * 
 * @author Dinesh Khurana
 * 
 */
public class MileApndxJB {
    /**
     * the mileApndxId
     */
    private int mileApndxId;
    /**
     * studyId
     */
    private String studyId;
    /**
     * milestoneId
     */
    private String milestoneId;
    /**
     * milestone desc
     */
    private String desc;
    /**
     * milestone uri
     */
    private String uri;
    /**
     * milestone type
     */
    private String type;
    /*
     * creator
     */
    private String creator;
    /*
     * last modified by
     */
    private String modifiedBy;
    /*
     * IP Address
     */
    private String ipAdd;
    // GETTER SETTER METHODS
    public int getMileApndxId() {
        return mileApndxId;
    }
    public void setMileApndxId(int id) {
        mileApndxId = id;
    }
    public String getStudyId() {
        return this.studyId;
    }
    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }
    public String getMilestoneId() {
        return this.milestoneId;
    }
    public void setMilestoneId(String milestoneId) {
        this.milestoneId = milestoneId;
    }
    public String getDesc() {
        return this.desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getUri() {
        return this.uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getCreator() {
        return this.creator;
    }
    public void setCreator(String creator) {
        this.creator = creator;
    }
    public String getModifiedBy() {
        return this.modifiedBy;
    }
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    public String getIpAdd() {
        return this.ipAdd;
    }
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    // END OF GETTER SETTER METHODS
    // CONSTRUCTOR TO CREATE appendix OBJ WITH ID
    public MileApndxJB(String id) {
        Rlog.debug("mileapndx", "MileApndx JB Create with id");
        setMileApndxId(EJBUtil.stringToNum(id));
    }
    // DEFAULT CONSTRUCTOR
    public MileApndxJB() {
        Rlog.debug("mileapndx", "MileApndx JB  : Default Constructor");
    }
    public MileApndxBean getMileApndxDetails() {
        MileApndxBean mask = null;
        try {
            MileApndxAgentRObj mileApndxRObj = EJBUtil.getMileApndxAgentHome();
            mask = mileApndxRObj.getMileApndxDetails(this.mileApndxId);
        } catch (Exception e) {
            Rlog.debug("mileapndx",
                    "Exception in the function getMileApndxDetails");
        }
        if (mask != null) {
            mileApndxId = mask.getMileApndxId();
            studyId = mask.getStudyId();
            milestoneId = mask.getMilestoneId();
            desc = mask.getDesc();
            uri = mask.getUri();
            type = mask.getType();
            creator = mask.getCreator();
            modifiedBy = mask.getModifiedBy();
            ipAdd = mask.getIpAdd();
        }
        return mask;
    }
    public int setMileApndxDetails() {
        int mileapndxId = 0;
        try {
            MileApndxAgentRObj mileApndxAgentRobj = EJBUtil
                    .getMileApndxAgentHome();
            mileapndxId = mileApndxAgentRobj.setMileApndxDetails(this
                    .createMileApndxStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "Exception in setMileApndxDetails()");
            e.printStackTrace();
            return -1;
        }
        return mileapndxId;
    }
    public int removeMileApndx(int mileApndxId) {
        int output = 0;
        try {
            MileApndxAgentRObj mileApndxAgentRobj = EJBUtil
                    .getMileApndxAgentHome();
            output = mileApndxAgentRobj.removeMileApndx(mileApndxId);
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "Exception in removing mileApndx");
            return -1;
        }
        return output;
    }
 // Overloaded for INF-18183 ::: AGodara
    public int removeMileApndx(int mileApndxId,Hashtable<String, String> auditInfo) {
        int output = 0;
        try {
            MileApndxAgentRObj mileApndxAgentRobj = EJBUtil
                    .getMileApndxAgentHome();
            output = mileApndxAgentRobj.removeMileApndx(mileApndxId,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "Exception in removing mileApndx removeMileApndx(int mileApndxId,Hashtable<String, String> auditInfo)");
            return -1;
        }
        return output;
    }
    
    public int updateMileApndx() {
        int output = 0;
        try {
            MileApndxAgentRObj mileApndxAgentRObj = EJBUtil
                    .getMileApndxAgentHome();
            output = mileApndxAgentRObj.updateMileApndx(this
                    .createMileApndxStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("mileapndx", "Exception in updating MileApndxDetails()");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    public MileApndxBean createMileApndxStateKeeper() {
        return new MileApndxBean(mileApndxId, studyId, milestoneId, desc, uri,
                type, creator, modifiedBy, ipAdd);
    }
    /**
     * 
     * 
     * generates a String of XML for the customer details entry form.<br>
     * <br>
     * 
     * 
     * Note that it is planned to encapsulate this detail in another object.
     * 
     * 
     */
    public String toXML()
    {
        return null;
    }
    // end of method
}// end of class
