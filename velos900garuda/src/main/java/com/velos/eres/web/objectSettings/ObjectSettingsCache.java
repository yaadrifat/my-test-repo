/*
 * Classname : ObjectSettingsCache
 *
 * Version information: 1.0
 *
 * Date: 05/04/2009
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.eres.web.objectSettings;

import com.velos.eres.business.common.ObjectSettingsDao;
import com.velos.eres.web.objectSettings.ObjectSettings;
import com.velos.eres.service.util.Rlog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

public class ObjectSettingsCache {

    private static ObjectSettingsCache objectSettingsCache = new ObjectSettingsCache();

    //-> private static ArrayList<ObjectSettings> detailsList;
    private static Hashtable<String,ArrayList<ObjectSettings>> hashSettingsByObjectName;

    // private constructor will prevent instantiation from other classes
    private ObjectSettingsCache () {
        populateObjectSettings();
    }

    private synchronized void clearCache() {
        /*  //->
        if (detailsList != null) {
            detailsList.clear();
            detailsList = null;
        }
        */
        if (hashSettingsByObjectName != null) {
            hashSettingsByObjectName.clear();
            hashSettingsByObjectName = null;
        }
    }
    /**
     * Clear cache and reload all the rows of the er_object_settings table into memory.
     */
    public synchronized void populateObjectSettings() {
        // fetches all the settings from the er_object_settings table (including default settings)
        this.clearCache();
        ObjectSettingsDao objectSettingDao = new ObjectSettingsDao();
        objectSettingDao.getObjectSettings();

        ArrayList objList = objectSettingDao.getPkObjSetting();
        int oSize = (objList == null) ? 0 : objList.size();

        if (oSize == 0) {
            return;
        }
        
        ArrayList<ObjectSettings> detailsList = new ArrayList<ObjectSettings>(oSize);
        //-> detailsList = new ArrayList<ObjectSettings>(oSize);
        for (int i=0;  i< objList.size(); i++) {
            ObjectSettings detailsObj = new ObjectSettings();
            detailsObj.setPkObjSetting((String)objectSettingDao.getPkObjSetting().get(i));
            detailsObj.setObjType((String)objectSettingDao.getObjType().get(i));
            detailsObj.setObjSubType((String)objectSettingDao.getObjSubType().get(i));
            detailsObj.setObjName((String)objectSettingDao.getObjName().get(i));
            detailsObj.setObjSequence((String)objectSettingDao.getObjSequence().get(i));
            detailsObj.setObjVisible((String)objectSettingDao.getObjVisible().get(i));
            detailsObj.setObjDispTxt((String)objectSettingDao.getObjDispTxt().get(i));
            detailsObj.setObjAcc((String)objectSettingDao.getObjAcc().get(i));
            detailsList.add(detailsObj);
        }

        hashSettingsByObjectName = new Hashtable<String,ArrayList<ObjectSettings>>();
        ArrayList<ObjectSettings> hashValueByObjectName = new ArrayList<ObjectSettings>(); 

        int prevAccId = 0;
        String prevObjName = "";
        if (detailsList.get(0).getObjName() != null 
                && ((String)detailsList.get(0).getObjName()).length() > 0) {
            prevObjName = new String((String)detailsList.get(0).getObjName());
        }
        for (int i=0;  i<detailsList.size(); i++) {
            int thisAcctId = -1;
            try {
                thisAcctId = Integer.parseInt((String) objectSettingDao.getObjAcc().get(i));
            } catch(Exception ex) {
                Rlog.fatal("ObjectSettingsCache", "parse exception in constructor at "+i+": "+ ex);
                continue;
            }
            String thisObjName = "";
            if (detailsList.get(i).getObjName() != null 
                    && ((String)detailsList.get(i).getObjName()).length() > 0) {
                thisObjName = new String((String)detailsList.get(i).getObjName());
            }
            if (thisAcctId != prevAccId || !thisObjName.equals(prevObjName)) {
                hashSettingsByObjectName.put(formAcctObjNameKey(prevAccId, prevObjName), hashValueByObjectName);
                hashValueByObjectName = new ArrayList<ObjectSettings>(); 
            }
            hashValueByObjectName.add(detailsList.get(i));
            prevAccId = thisAcctId;
            prevObjName = thisObjName;
        } //end of detailsList loop
        hashSettingsByObjectName.put(formAcctObjNameKey(prevAccId, prevObjName), hashValueByObjectName);
    }
    
    private String formAcctObjNameKey(int account, String objName) {
        StringBuffer sb = new StringBuffer();
        sb.append(account).append("-").append(objName==null ? "" : objName);
        return sb.toString();
    }

    /**
     * Get the reference to the singleton object of this class.
     * @return The singleton object of this class
     */
    public static ObjectSettingsCache getObjectSettingsCache() {
        return objectSettingsCache;
    }
    
    /**
     * Main API method of this class to get a sorted ArrayList of the records of ObjectSettings
     * for the specified account and object name. If the target records are missing, the default
     * values for account=0 will be returned.
     *  
     * @param int account
     * @param String objName
     * @return ArrayList of object settings sorted by the sequence number
     */
    public ArrayList<ObjectSettings> getAccountObjects(int account, String objName) {
        String key = formAcctObjNameKey(account, objName);
        ArrayList<ObjectSettings> list = hashSettingsByObjectName.get(key);
        ArrayList<ObjectSettings> defaultList = hashSettingsByObjectName.get(formAcctObjNameKey(0, objName)); 
        if (defaultList == null) { defaultList = new ArrayList<ObjectSettings>(); }
        if (list == null || list.size() == 0) {
            return defaultList;
        }
        if (list.size() >= defaultList.size()) return list;

        // If custom settings are missing some subtypes as compared to the default,
        // copy the missing subtypes from the default
        ArrayList<ObjectSettings> newList = new ArrayList<ObjectSettings>(defaultList.size());
        boolean[] skipFromDefault = new boolean[defaultList.size()];
        for (int iX=0; iX<defaultList.size(); iX++) {
            for (int iY=0; iY<list.size(); iY++) {
                if (defaultList.get(iX).getObjSubType().equals(list.get(iY).getObjSubType())) {
                    skipFromDefault[iX] = true;
                    break;
                }
            }
        }
        for (int iX=0; iX<defaultList.size(); iX++) {
            if (skipFromDefault[iX]) { continue; }
            newList.add(defaultList.get(iX));
        }
        for (int iY=0; iY<list.size(); iY++) {
            newList.add(list.get(iY));
        }
        Collections.sort(newList);
        return newList;
    }

}
