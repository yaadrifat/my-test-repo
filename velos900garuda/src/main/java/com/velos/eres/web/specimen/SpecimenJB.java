/*
 * Classname : 				SpecimenJB
 *
 * Version information : 	1.0
 *
 * Date 					07/10/2007
 *
 * Copyright notice: 		Velos Inc
 *
 * Author 					Jnanamay Majumdar
 */

package com.velos.eres.web.specimen;

/* IMPORT STATEMENTS */
import java.util.Hashtable;
import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.SpecimenDao;
import com.velos.eres.business.specimen.impl.SpecimenBean;
import com.velos.eres.service.specimenAgent.SpecimenAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
/* END OF IMPORT STATEMENTS */

public class SpecimenJB {
	    private  int pkSpecimen;
	    private  String specimenId;
	    private  String specAltId;
	    private  String specDesc;
	    private  String specOrgQnty;
	    private  String spectQuntyUnits;
	    private  String specBaseOrgQnty;
	    private  String specBaseOrigQuntyUnits;
	    private  String  specType;
	    private  String  fkStudy;
	    private  String specCollDate;
	    private  String  fkPer;
	    private  String  specOwnerFkUser;
	    private  String  fkStorage;
	    private  String  fkSite;
	    private  String fkVisit;
	    private  String fkSchEvents;
	    private  String specNote;
	    private  String fkSpec;
	    private  String specStorageTmp;
	    private  String specStorageTmpUnit;
	    
	    //private  String specimenStat;
//JM: 19Sep2007: fk_account added to have things account specific
	    private  String fkAccount;


	    //KM: Added for child specimens.
	    private String specProcType;

	    private String fkStrgKitComp;


    /**
     * creator
     */
	    private String creator;

    /**
     * last modified by
     */
	    private String modifiedBy;

    /**
     * IP Address
     */
      private String ipAdd;

      private String specUserPathologist;

      private  String specUserSurgeon;

      private  String specUserCollectingTech;
      private  String specUserProcessingTech;
      private  String specRemovalDatetime;
      private   String specFreezeDatetime;

      /* JM: 20May2009: added, enhacement #INVP2.7.1 */
      private String specAnatomicSite;
      private String specTissueSide;
      private String specPathologyStat;
      private String fkStorageId;
      private String specExpectQuant;

      private String specBaseOrigQuant;
      private String specExpectedQUnit;
      private String specBaseOrigQUnit;
      /**
       *  specimen disposition for Invp 2.8
       */
      private String specDisposition;
      private String specEnvtCons;




//  GETTERS AND SETTERS

    /**
	 * @return the pkSpecimen
	 */
	public int getPkSpecimen() {
		return pkSpecimen;
	}

	/**
	 * @param pkSpecimen the pkSpecimen to set
	 */
	public void setPkSpecimen(int pkSpecimen) {
		this.pkSpecimen = pkSpecimen;
	}

	/**
	 * @return the specimenId
	 */
	public String getSpecimenId() {
		return specimenId;
	}


	/**
	 * @param specimenId the specimenId to set
	 */
	public void setSpecimenId(String specimenId) {
		this.specimenId = specimenId;
	}


	/**
	 * @return the getSpecAltId
	 */
	public String getSpecAltId() {
		return specAltId;
	}


	/**
	 * @param specAltId the specAltId to set
	 */
	public void setSpecAltId(String specAltId) {
		this.specAltId = specAltId;
	}

	/**
	 * @return the specDesc
	 */
	public String getSpecDesc() {
		return specDesc;
	}

	/**
	 * @param specDesc the specDesc to set
	 */
	public void setSpecDesc(String specDesc) {
		this.specDesc = specDesc;
	}


	/**
	 * @return the specOrgQnty
	 */
	public String getSpecOrgQnty() {
		return specOrgQnty;
	}

	/**
	 * @param specOrgQnty the specOrgQnty to set
	 */
	public void setSpecOrgQnty(String specOrgQnty) {
		this.specOrgQnty = specOrgQnty;
	}

	/**
	 * @return the getBaseSpecOrgQnty
	 */
	public String getBaseSpecOrgQnty() {
		return specBaseOrgQnty;
	}

	/**
	 * @param specBaseOrgQnty the setSpecBaseOrgQnty to set
	 */
	public void setSpecBaseOrgQnty(String specBaseOrgQnty) {
		this.specBaseOrgQnty = specBaseOrgQnty;
	}

	
	/**
	 * @return the spectQuntyUnits
	 */
	public String getSpectQuntyUnits() {
		return spectQuntyUnits;
	}

	/**
	 * @param spectQuntyUnits the spectQuntyUnits to set
	 */
	public void setSpectQuntyUnits(String spectQuntyUnits) {
		this.spectQuntyUnits = spectQuntyUnits;
	}

	/**
	 * @return the specType
	 */
	public String getSpecType() {
		return specType;
	}

	/**
	 * @param specType the specType to set
	 */
	public void setSpecType(String specType) {
		this.specType = specType;
	}

	/**
	 * @return the fkStudy
	 */
	public String getFkStudy() {
		return fkStudy;
	}


	public void setFkStudy(String study) {
		this.fkStudy = study;
	}
	/**
	 * @param specCollDate, the specCollDate to set
	 */
	public void setSpecCollDate(String specCollDate) {
		this.specCollDate = specCollDate;
	}

	/**
	 * @return the specCollDate
	 */
	public String getSpecCollDate() {
		return specCollDate;
	}

	/**
	 * @param setFkPer, the setFkPer to set
	 */
	public void setFkPer(String fkPer) {
		this.fkPer = fkPer;
	}

	/**
	 * @return the fkPer
	 */
	public String getFkPer() {
		return fkPer;
	}

	/**
	 * @param specOwnerFkUser, the specOwnerFkUser to set
	 */
	public void setSpecOwnerFkUser(String specOwnerFkUser) {
		this.specOwnerFkUser = specOwnerFkUser;
	}

	/**
	 * @return the specOwnerFkUser
	 */
	public String getSpecOwnerFkUser() {
		return specOwnerFkUser;
	}

	/**
	 * @param fkStorage, the fkStorage to set
	 */
	public void setFkStorage(String fkStorage) {
		this.fkStorage = fkStorage;
	}

	/**
	 * @return the fkStorage
	 */
	public String getFkStorage() {
		return fkStorage;
	}

	/**
	 * @param fkSite, the fkSite to set
	 */
	public void setFkSite(String fkSite) {
		this.fkSite = fkSite;
	}

	/**
	 * @return the fkSite
	 */
	public String getFkSite() {
		return fkSite;
	}

	/**
	 * @param fkVisit the fkVisit to set
	 */
	public void setFkVisit(String fkVisit) {
		this.fkVisit = fkVisit;
	}

	/**
	 * @return the fkVisit
	 */
	public String getFkVisit() {
		return fkVisit;
	}


	public void setFkSchEvents(String schEvnt) {
		this.fkSchEvents = schEvnt;
	}


	/**
	 * @return the fkSchEvents
	 */
	public String getFkSchEvents() {
		return fkSchEvents;
	}


	////////////////////////////////////////////////////


	public void setSpecNote(String specNote){
    	this.specNote = specNote;

    }


	public String getSpecNote(){
		return this.specNote;
	}



	////////////////////////////////////////////////




	/**
	 * @param fkSpec, the fkSpec to set
	 */
	public void setFkSpec(String fkSpec) {
		this.fkSpec = fkSpec;
	}

	/**
	 * @return the storageAvailUnitCnt
	 */
	public String getFkSpec() {
		return fkSpec;
	}

	/**
	 * @param specStorageTmp, the specStorageTmp to set
	 */
	public void setSpecStorageTmp(String specStorageTmp) {
		this.specStorageTmp = specStorageTmp;
	}

	/**
	 * @return the specStorageTmp
	 */
	public String getSpecStorageTmp() {
		return specStorageTmp;
	}

	/**
	 * @param specStorageTmpUnit, the specStorageTmpUnit to set
	 */
	public void setSpecStorageTmpUnit(String specStorageTmpUnit) {
		this.specStorageTmpUnit = specStorageTmpUnit;
	}

	/**
	 * @return the specStorageTmpUnit
	 */
	public String getSpecStorageTmpUnit() {
		return specStorageTmpUnit;
	}





	/**
	 * @param specStorageTmpUnit, the specStorageTmpUnit to set
	 */
	/*
	public void setSpecimenStatus(String specimenStat) {
		this.specimenStat = specimenStat;
	}
	*/
	/**
	 * @return the specStorageTmpUnit
	 */
	/*
	public String getSpecimenStatus() {
		return specimenStat;
	}
	*/

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the ipAdd
	 */
	public String getIpAdd() {
		return ipAdd;
	}

	/**
	 * @param ipAdd the ipAdd to set
	 */
	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}



	 /**
	    * @return Fk Account
	    */

	   public String getFkAccount() {
	       return fkAccount;
	   }

	   /**
	    * @param fkAccount
	    *            The Fk Account of the logged in user
	    */
	  public void setFkAccount(String fkAccount) {
	       this.fkAccount = fkAccount;
	   }

	  //KM:Added for Child Specimens.
	  /**
	   * @param specProcType, the specProcType to set
	   */
	  public void setSpecProcType(String specProcType) {
		  this.specProcType = specProcType;
	  }

	  /**
	   * @return the specProcType
	   */
	  public String getSpecProcType() {
		  return specProcType;
	  }


	  /**
	   * @param fkStrgKitComp, the fkStrgKitComp to set
	   */
	  public void setFkStrgKitComp(String fkStrgKitComp) {
		  this.fkStrgKitComp = fkStrgKitComp;
	  }

	  /**
	   * @return the fkStrgKitComp
	   */
	  public String getFkStrgKitComp() {
		  return fkStrgKitComp;
	  }


//	 END OF THE GETTER AND SETTER METHODS

	/**
     * Constructor
     *
     * @param pkSpecimen:
     *            Unique Id constructor
     *
     */

    public void SpecimenJB(int pkSpecimen) {
    	setPkSpecimen(pkSpecimen);
    }

    /**
     * Default Constructor
     */

    public SpecimenJB() {
        Rlog.debug("specimen", "SpecimenJB.SpecimenJB() ");
    }
//Parameterized constructor

		public SpecimenJB(int  pkSpecimen,    String specimenId, String specAltId, String specDesc,  String specOrgQnty, String spectQuntyUnits,
			    String  specType,  String  fkStudy,  String  specCollDate, String  fkPer,  String  specOwnerFkUser, String  fkStorage,
			    String  fkSite,  String fkVisit, String fkSchEvents, String specNote, String fkSpec,  String specStorageTmp, String specStorageTmpUnit,
			    String creator,String modifiedBy, String ipAdd, String fkAccount, String specProcType, String fkStrgKitComp,
			    String surgeon,String procTech,String path, String collTech,String removalTime,String freezeTime, String specAnatomicSite,
			    String specTissueSide,String specPathologyStat, String fkStorageId, String specExpectQuant,
			    String specBaseOrigQuant,  String specExpectedQUnit, String specBaseOrigQUnit, String specDisposition,String specEnvtCons) {


		setPkSpecimen(pkSpecimen);
    	setSpecimenId(specimenId);
    	setSpecAltId(specAltId);
    	setSpecDesc(specDesc);
    	setSpecOrgQnty(specOrgQnty);
    	setSpectQuntyUnits(spectQuntyUnits);
    	setSpecType(specType);
    	setFkStudy(fkStudy);
    	setSpecCollDate(specCollDate);
    	setFkPer(fkPer);
    	setSpecOwnerFkUser(specOwnerFkUser);
    	setFkStorage(fkStorage);
    	setFkSite(fkSite);
    	setFkVisit(fkVisit);
    	setFkSchEvents(fkSchEvents);
    	setSpecNote(specNote);
    	setFkSpec(fkSpec);
    	setSpecStorageTmp(specStorageTmp);
    	setSpecStorageTmpUnit(specStorageTmpUnit);
    	//setSpecimenStatus(specimenStat);
    	setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setFkAccount(fkAccount);
        setSpecProcType(specProcType);
        setFkStrgKitComp(fkStrgKitComp);
        setSpecUserSurgeon(surgeon);
        setSpecUserProcessingTech(procTech);
        setSpecUserPathologist(path);
        setSpecUserCollectingTech(collTech);
        setSpecRemovalDatetime(removalTime);
        setSpecFreezeDatetime(freezeTime);

		setSpecAnatomicSite(specAnatomicSite);
		setSpecTissueSide(specTissueSide);
		setSpecPathologyStat(specPathologyStat);
		setFkStorageId(fkStorageId);
		setSpecExpectQuant(specExpectQuant);
		setSpecBaseOrigQuant(specBaseOrigQuant);
		setSpecExpectedQUnits(specExpectedQUnit);
		setSpecBaseOrigQUnit(specBaseOrigQUnit);
		//Invp 2.8
		setSpecDisposition(specDisposition);
		setSpecEnvtCons(specEnvtCons);



	}


	  /**
     * Retrieves the details of a specimen in SpecimenStateKeeper Object
     *
     * @return SpecimenStateKeeper consisting of the specimen details
     * @see SpecimenStateKeeper
     */
    public SpecimenBean getSpecimenDetails() {
    	SpecimenBean spsk = null;
        try {
            SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
            spsk = specimenAgentRObj.getSpecimenDetails(this.pkSpecimen);
            Rlog.debug("specimen", "In SpecimenJB.getSpecimenDetails() " + spsk);
        } catch (Exception e) {
            Rlog.fatal("specimen", "Error in getSpecimenDetails() in SpecimenJB " + e);
        }
        if (spsk != null) {


        	this.pkSpecimen = spsk.getPkSpecimen();
    	    this.specimenId = spsk.getSpecimenId();
    	    this.specAltId = spsk.getSpecAltId();
    	    this.specDesc = spsk.getSpecDesc();
    	    this.specOrgQnty= spsk.getSpecOrgQnty();
    	    this.spectQuntyUnits = spsk.getSpectQuntyUnits();
    	    this.specType = spsk.getSpecType();
    	    this.fkStudy = spsk.getFkStudy();
    	    this.specCollDate =spsk.getSpecCollDate();
    	    this.fkPer =spsk.getFkPer();
    	    this.specOwnerFkUser = spsk.getSpecOwnerFkUser();
    	    this.fkStorage = spsk.getFkStorage() ;
    	    this.fkSite = spsk.getFkSite();
    	    this.fkVisit = spsk.getFkVisit();
    	    this.fkSchEvents = spsk.getFkSchEvents();
    	    this.specNote = spsk.getSpecNote();
    	    this.fkSpec = spsk.getFkSpec();
    	    this.specStorageTmp = spsk.getSpecStorageTmp();
    	    this.specStorageTmpUnit = spsk.getSpecStorageTmpUnit();
    	    //this.specimenStat = spsk.getSpecimenStatus();
            this.creator = spsk.getCreator();
            this.modifiedBy = spsk.getModifiedBy();
            this.ipAdd = spsk.getIpAdd();
            this.fkAccount = spsk.getFkAccount();
            this.specProcType = spsk.getSpecProcType();
            this.fkStrgKitComp = spsk.getFkStrgKitComp();

            setSpecUserSurgeon(spsk.getSpecUserSurgeon());
            setSpecUserProcessingTech(spsk.getSpecUserProcessingTech());
            setSpecUserPathologist(spsk.getSpecUserPathologist());
            setSpecUserCollectingTech(spsk.getSpecUserCollectingTech());
            setSpecRemovalDatetime(spsk.getSpecRemovalDatetimeDt());
            setSpecFreezeDatetime(spsk.getSpecFreezeDatetimeDt());

            setSpecAnatomicSite(spsk.getSpecAnatomicSite());
    		setSpecTissueSide(spsk.getSpecTissueSide());
    		setSpecPathologyStat(spsk.getSpecPathologyStat());
    		setFkStorageId(spsk.getFkStorageId());
    		setSpecExpectQuant(spsk.getSpecExpectQuant());
    		setSpecBaseOrigQuant(spsk.getSpecBaseOrigQuant());
      		setSpecExpectedQUnits(spsk.getSpecExpectedQUnits());
      		setSpecBaseOrigQUnit(spsk.getSpecBaseOrigQUnit());
      		//Invp 2.8
      		setSpecDisposition(spsk.getSpecDisposition());
      		setSpecEnvtCons(spsk.getSpecEnvtCons());

        }
        return spsk;
    }


    public int setSpecimenDetails() {
        int output = 0;
        try {
        	SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
            output = specimenAgentRObj.setSpecimenDetails(this.createSpecimenStateKeeper());
            this.setPkSpecimen(output);

            CommonDAO cDao = new CommonDAO();

            Rlog.debug("specimen", "specimen notes clob goes here ..." + this.getSpecNote());
            if(this.getSpecNote()!=null)
            cDao.updateClob(this.getSpecNote(),"er_specimen","SPEC_NOTES"," where PK_SPECIMEN = " + output);
            Rlog.debug("specimen", "SpecimenJB.setSpecimenDetails()");

        } catch (Exception e) {
            Rlog.fatal("specimen", "Error in setSpecimenDetails() in SpecimenJB "   + e);
            return -2;
        }
        return output;
    }


    public int updateSpecimen() {
        int output;
        try {
        	SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
            output = specimenAgentRObj.updateSpecimen(this.createSpecimenStateKeeper());


            CommonDAO cDao = new CommonDAO();
            Rlog.debug("specimen", "specimen notes clob goes here ..." + this.getSpecNote());
            if (this.getSpecNote() != null) {
                cDao.updateClob(this.getSpecNote(),"er_specimen","SPEC_NOTES"," where PK_SPECIMEN = " + this.getPkSpecimen());
            }
            Rlog.debug("specimen", "SpecimenJB.updateSpecimen()");

        } catch (Exception e) {
            Rlog.debug("specimen", "Exception setting specimen details to the specimen SB" + e.getMessage());
            return -2;
        }
        return output;
    }

    public SpecimenBean createSpecimenStateKeeper() {

    	Rlog.debug("specimen", "SpecimenJB.createSpecimenStateKeeper");

        return new SpecimenBean(this.pkSpecimen, this.specimenId, this.specAltId,
        	    this.specDesc, this.specOrgQnty, this.spectQuntyUnits,
        	    this.specType, this.fkStudy,this.specCollDate,
        	    this.fkPer, this.specOwnerFkUser, this.fkStorage,
        	    this.fkSite, this.fkVisit, this.fkSchEvents, this.specNote,
        	    this.fkSpec, this.specStorageTmp,this.specStorageTmpUnit,
        		this.creator, this.modifiedBy, this.ipAdd, this.fkAccount,this.specProcType, this.fkStrgKitComp,
        		this.specUserSurgeon, this.specUserProcessingTech,this.specUserPathologist,this.specUserCollectingTech,
        		this.specRemovalDatetime,this.specFreezeDatetime,this.specAnatomicSite, this.specTissueSide,
        	    this.specPathologyStat, this.fkStorageId, this.specExpectQuant,
        	    this.specBaseOrigQuant,  this.specExpectedQUnit, this.specBaseOrigQUnit,this.specDisposition,this.specEnvtCons);


    }


    public int deleteSpecimens(String[] deleteIds) {
        int ret = 0;
        try {
            Rlog.debug("specimen", "SpecimenJB:deleteSpecimen starting");

            SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
           ret = specimenAgentRObj.deleteSpecimens(deleteIds);

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenJB:deleteSpecimen-Error " + e);
            return -1;
        }
        return ret;

    }
    // Overloaded for INF-18183 ::: AGodara
    public int deleteSpecimens(String[] deleteIds,Hashtable<String, String> auditInfo) {
        int ret = 0;
        try {
            SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
            ret = specimenAgentRObj.deleteSpecimens(deleteIds,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenJB:deleteSpecimens(String[] deleteIds,Hashtable<String, String> auditInfo)-Error " + e);
            return -1;
        }
        return ret;
    }


    public String getSpecimenIdAuto() {

    	 String id = "";
         try {
             Rlog.debug("specimen", "SpecimenJB:getSpecimenIdAuto ....");

             SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
             id = specimenAgentRObj.getSpecimenIdAuto();

         } catch (Exception e) {
             Rlog.fatal("specimen", "SpecimenJB:getSpecimenIdAuto -Error " + e);
             return id;
         }
         return id;

     }

    public String getSpecimenIdAuto(String studyId, String patientId) {

        String id = "";
        try {
            Rlog.debug("specimen", "SpecimenJB:getSpecimenIdAuto ....");

            SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
            id = specimenAgentRObj.getSpecimenIdAuto(studyId, patientId, 
                    EJBUtil.stringToNum(this.fkAccount));

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenJB:getSpecimenIdAuto -Error " + e);
            return id;
        }
        return id;

    }

    //JM: 30Jan2008:
    public SpecimenDao getSpecimenChilds(int specId, int accId){

    	SpecimenDao specimenDao = new SpecimenDao();
    	try {
    		SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
    		specimenDao= specimenAgentRObj.getSpecimenChilds( specId, accId);
            return specimenDao;

        } catch (Exception e) {
        	Rlog.fatal("specimen", "SpecimenJB:getSpecimenChilds -Error " + e);
        }
        return specimenDao;
    }

    public String getParentSpecimenId(int specId) {

        String parentSpecId = "";


        try {
            Rlog.debug("specimen", "SpecimenJB:getSpecimenIdAuto ....");

            SpecimenAgentRObj specimenAgentRObj = EJBUtil.getSpecimenAgentHome();
            parentSpecId = specimenAgentRObj.getParentSpecimenId(specId);

        } catch (Exception e) {
            Rlog.fatal("specimen", "SpecimenJB:getParentSpecimenId -Error " + e);
            return parentSpecId;
        }
        return parentSpecId;

    }

	public String getSpecFreezeDatetime() {
		return specFreezeDatetime;
	}

	public void setSpecFreezeDatetime(String specFreezeDatetime) {
		this.specFreezeDatetime = specFreezeDatetime;
	}

	public String getSpecRemovalDatetime() {
		return specRemovalDatetime;
	}

	public void setSpecRemovalDatetime(String specRemovalDatetime) {
		this.specRemovalDatetime = specRemovalDatetime;
	}

	public String getSpecUserCollectingTech() {
		return specUserCollectingTech;
	}

	public void setSpecUserCollectingTech(String specUserCollectingTech) {
		this.specUserCollectingTech = specUserCollectingTech;
	}

	public String getSpecUserPathologist() {
		return specUserPathologist;
	}

	public void setSpecUserPathologist(String specUserPathologist) {
		this.specUserPathologist = specUserPathologist;
	}

	public String getSpecUserProcessingTech() {
		return specUserProcessingTech;
	}

	public void setSpecUserProcessingTech(String specUserProcessingTech) {
		this.specUserProcessingTech = specUserProcessingTech;
	}

	public String getSpecUserSurgeon() {
		return specUserSurgeon;
	}

	public void setSpecUserSurgeon(String specUserSurgeon) {
		this.specUserSurgeon = specUserSurgeon;
	}


	/* JM: 20May2009: added, enhacement #INVP2.7.1 */

    public String getSpecAnatomicSite() {
    	return specAnatomicSite;
    }
    public void setSpecAnatomicSite(String specAnatomicSite) {
    	this.specAnatomicSite = specAnatomicSite;
    }

    public String getSpecTissueSide(){
 	   return specTissueSide;
    }
    public void setSpecTissueSide(String specTissueSide) {
 	   this.specTissueSide = specTissueSide;
    }

    public String getSpecPathologyStat(){
 	   return specPathologyStat;
    }
    public void setSpecPathologyStat(String specPathologyStat) {
 	   this.specPathologyStat = specPathologyStat;
    }

    public String getFkStorageId(){
 	   return fkStorageId;
    }
    public void setFkStorageId(String fkStorageId) {
 	   this.fkStorageId = fkStorageId;
    }

    public String getSpecExpectQuant(){
		return specExpectQuant;
	}
    public void setSpecExpectQuant(String specExpectQuant){
    	this.specExpectQuant = specExpectQuant;
    }


    public String getSpecBaseOrigQuant(){
		return specBaseOrigQuant;
	}
    public void setSpecBaseOrigQuant(String specBaseOrigQuant){
    	this.specBaseOrigQuant = specBaseOrigQuant;
    }



    public String getSpecExpectedQUnits(){
  	   return specExpectedQUnit;
    }
    public void setSpecExpectedQUnits(String specExpectedQUnit) {
  	   this.specExpectedQUnit = specExpectedQUnit;
    }



    public String getSpecBaseOrigQUnit(){
   	   return specBaseOrigQUnit;
    }
    public void setSpecBaseOrigQUnit(String specBaseOrigQUnit) {
   	   this.specBaseOrigQUnit = specBaseOrigQUnit;
    }

    /* JM: 20May2009: added, enhacement #INVP2.7.1 */

    //Added for Invp 2.8 
	public String getSpecDisposition() {
		return specDisposition;
	}

	public void setSpecDisposition(String specDisposition) {
		this.specDisposition = specDisposition;
	}

	public String getSpecEnvtCons() {
		return specEnvtCons;
	}

	public void setSpecEnvtCons(String specEnvtCons) {
		this.specEnvtCons = specEnvtCons;
	}
	
}// end of class

