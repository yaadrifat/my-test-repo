/*
 * Classname : StudySiteApndxJB.java
 * 
 * Version information
 *
 * Date 03/02/2001
 * 
 * Copyright notice: Aithent
 */

package com.velos.eres.web.studySiteApndx;

import java.util.Hashtable;
import com.velos.eres.business.common.StudySiteApndxDao;
import com.velos.eres.business.studySiteApndx.impl.StudySiteApndxBean;
import com.velos.eres.service.studySiteApndxAgent.StudySiteApndxAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the Study Site Appendix
 * 
 * @author Anu Khannna
 */
public class StudySiteApndxJB {
    /**
     * the Study Site Appendix Id
     */
    private int studySiteApndxId;

    /**
     * the Study SiteId
     */
    private String studySiteId;

    /**
     * appendix type
     */
    private String type;

    /**
     * the url/file name
     */
    private String name;

    /**
     * appendix description
     */
    private String desc;

    /**
     * appendix file size
     */
    private String fileSize;

    /**
     * creator
     */
    private String creator;

    /**
     * last modified by
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * @return study site apndx id
     */
    public int getStudySiteApndxId()

    {
        return this.studySiteApndxId;
    }

    /**
     * @param studySiteApndxId
     */

    public void setStudySiteApndxId(int studySiteApndxId) {
        this.studySiteApndxId = studySiteApndxId;
    }

    /**
     * @return study site id
     */
    public String getStudySiteId()

    {
        return this.studySiteId;
    }

    /**
     * @param studySiteId
     */
    public void setStudySiteId(String studySiteId) {
        this.studySiteId = studySiteId;
    }

    /**
     * @return appendx type
     */
    public String getAppendixType() {
        return this.type;
    }

    /**
     * @param type
     */
    public void setAppendixType(String type) {
        this.type = type;
    }

    /**
     * @return appendix name
     */
    public String getAppendixName() {
        return this.name;
    }

    /**
     * @param name
     */
    public void setAppendixName(String name) {
        this.name = name;
    }

    /**
     * @return appendix description
     */
    public String getAppendixDescription() {
        return this.desc;
    }

    /**
     * @param desc
     */
    public void setAppendixDescription(String desc) {
        this.desc = desc;
    }

    /**
     * @return file size
     */
    public String getFileSize() {
        return this.fileSize;
    }

    /**
     * @param fileSize
     */
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    public StudySiteApndxJB(int studySiteApndxId) {
        setStudySiteApndxId(studySiteApndxId);
    }

    // DEFAULT CONSTRUCTOR

    public StudySiteApndxJB() {

        Rlog.debug("studySiteApndx", "StudySiteApndx JB : Default Constructor");

    }

    /**
     * 
     */

    public StudySiteApndxBean getStudySiteApndxDetails() {
        StudySiteApndxBean ssk = null;
        try {
            StudySiteApndxAgentRObj aa_ro = EJBUtil
                    .getStudySiteApndxAgentHome();

            ssk = aa_ro.getStudySiteApndxDetails(this.studySiteApndxId);
        }

        catch (Exception e) {
            Rlog.debug("studySiteApndx", "EXception in calling session bean");

        }

        if (ssk != null) {
            this.studySiteApndxId = ssk.getStudySiteApndxId();
            this.type = ssk.getAppendixType();
            this.name = ssk.getAppendixName();
            this.desc = ssk.getAppendixDescription();
            this.fileSize = ssk.getFileSize();
            this.creator = ssk.getCreator();
            this.modifiedBy = ssk.getModifiedBy();
            this.ipAdd = ssk.getIpAdd();
        }
        return ssk;
    }

    public int setStudySiteApndxDetails() {
        int apndxId = 0;
        try {
            StudySiteApndxAgentRObj aa_ro = EJBUtil
                    .getStudySiteApndxAgentHome();

            apndxId = aa_ro.setStudySiteApndxDetails(this
                    .createStudySiteApndxStateKeeper());
        }

        catch (Exception e) {
            Rlog.debug("studySiteApndx",
                    "in session bean - set appendix details");
            return -1;
        }

        return apndxId;

    }

    public int updateStudySiteApndx() {
        int output;
        try {
            StudySiteApndxAgentRObj studySiteApndxAgentRObj = EJBUtil
                    .getStudySiteApndxAgentHome();

            output = studySiteApndxAgentRObj.updateStudySiteApndx(this
                    .createStudySiteApndxStateKeeper());

        }

        catch (Exception e) {

            Rlog.debug("studySiteApndx",
                    "EXCEPTION IN SETTING APPENDIX DETAILS TO  SESSION BEAN");

            e.printStackTrace();

            return -2;

        }

        return output;

    }

    // places bean attributes into a CustomerStateHolder.

    public StudySiteApndxBean createStudySiteApndxStateKeeper() {
        return new StudySiteApndxBean(this.studySiteApndxId, this.studySiteId,
                this.type, this.name, this.desc, this.fileSize, this.creator,
                this.modifiedBy, this.ipAdd);
    }

    /**
     * Method to get values from studySiteApndx corresponding to given
     * studySiteId and type
     */

    public StudySiteApndxDao getStudySitesApndxValues(int studySiteId,
            String type) {
        StudySiteApndxDao studySiteApndxDao = new StudySiteApndxDao();
        try {
            Rlog.debug("studySiteApndx",
                    "StudySiteApndxJB.getStudySitesApndxValues starting");

            StudySiteApndxAgentRObj studySiteApndxAgentRObj = EJBUtil
                    .getStudySiteApndxAgentHome();

            studySiteApndxDao = studySiteApndxAgentRObj
                    .getStudySitesApndxValues(studySiteId, type);

            return studySiteApndxDao;

        } catch (Exception e) {

            Rlog.fatal("studySiteApndx",
                    "Error in getStudySitesApndxValues(studySiteId, type) in StudySiteApndxJB "
                            + e);

            return null;

        }

    }

    public int removeStudySiteApndx() {

        int output;
        try {
            StudySiteApndxAgentRObj aa_ro = EJBUtil
                    .getStudySiteApndxAgentHome();

            output = aa_ro.removeStudySiteApndx(this.studySiteApndxId);
            return output;
        } catch (Exception e) {
            Rlog.debug("studySiteApndx",
                    "in session bean - remove appendix details");
            return -1;

        }
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeStudySiteApndx(Hashtable<String, String> auditInfo) {

        int output;
        try {
            StudySiteApndxAgentRObj aa_ro = EJBUtil
                    .getStudySiteApndxAgentHome();
            output = aa_ro.removeStudySiteApndx(this.studySiteApndxId,auditInfo);
            return output;
        } catch (Exception e) {
            Rlog.debug("studySiteApndx",
                    "JB removeStudySiteApndx(Hashtable<String, String> auditInfo)");
            return -1;
        }
    }

}// end of class
