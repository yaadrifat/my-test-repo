package com.velos.eres.web.dynrep.holder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import com.velos.eres.business.common.LookupDao;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * Description of the Class
 * 
 * @author vabrol
 * @created October 20, 2004
 */
/*
 * Modified by Sonia Abrol, 04/14/05, added new attributes, repUseUniqueId,
 * repUseDataValue
 */
/* Modified by Sonia Abrol 07/18/05, added method removeObsoleteDependentObjects */

public class ReportHolder implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3761972678709752116L;

    String reportId;

    String reportName;

    String reportType;

    String reportHeader;

    String reportFooter;

    String studyId;

    String patientId;

    String reportDesc;

    String formIdStr;

    String formNameStr;

    /*
     * ArrayList to contain selected forms information
     */
    ArrayList formIds;

    ArrayList formNames;

    /**
     * ArrayList of form types, Possible values- F:Forms, C:Core Tables Each
     * element will correspond to a formId in the ArrayList formIds
     */
    ArrayList formTypes;

    String delimiter;

    String prevFormSelection;

    String shareWith;

    String reportMode;

    ArrayList filterIds;

    ArrayList filterNames;

    /**
     * ArrayList to hold core lookup view ids. This will be populated when the
     * object is created and then later on used to append to form Ids list
     */
    ArrayList coreLookupViewIds;

    /**
     * ArrayList to hold core lookup view names. This will be populated when the
     * object is created and then later on used to append to form Names list
     */

    ArrayList coreLookupViewNames;

    /*
     * Variable stores the total number of fields selected in all the forms in
     * this report
     */
    int selectedFieldCount;

    /*
     * ArrayList stores all the fields available in forms This list is
     * customized for display and add formName to field list to display on the
     * screen.
     */
    ArrayList masterFieldId;

    ArrayList masterFieldName;

    ArrayList masterFieldType;

    /*
     * Hash map to store field,filter and sort details
     */
    HashMap fldObjects;

    HashMap filterObjects;

    HashMap sortObjects;

    /*
     * Stores the combined order by string defined for all the forms.
     */
    String orderMergedStr;

    /**
     * Stores: Key:formId, Value- a hastable of form primary key and table
     * information
     */
    private Hashtable htFormTablePKInfo;

    /**
     * Holds the 'selected' core table lookup view ids prefixed with a 'C' By
     * 'selected' we mean, the ones from which user selected any fields to
     * display in the report
     */
    private ArrayList selectedCoreLookupformIds;

    /**
     * Flag to indicate if study team rights needs are to be checked for the
     * report
     */

    private boolean checkForStudyRights;

    /**
     * Flag for the report, to Use Unique Field ID as Display Name. However,
     * user will have an option to have a different setting for each field.
     * possible values: 0:true,1:false
     */
    private String repUseUniqueId;

    /**
     * Flag for the report, to Display Data value of multiple choice fields.
     * However, user will have an option to have a different setting for each
     * field. possible values: 0:true,1:false (default, display value will be
     * shown)
     */
    private String repUseDataValue;

    /**
     * Hashtable to keep a mapping of selected lookup forms and if any filters
     * that should be ignored. A value will be added only if there are any
     * filters to be ignored
     */
    private Hashtable ignoreFilters;

    /**
     * Constructor for the DynRepDao object
     */

    /**
     * Constructor for the ReportHolder object
     */
    public ReportHolder() {
        reportName = "";
        reportType = "";
        reportHeader = "";
        reportFooter = "";
        studyId = "";
        patientId = "";
        reportDesc = "";
        formIdStr = "";
        formNameStr = "";
        delimiter = ";";
        prevFormSelection = "";
        shareWith = "";
        reportMode = "";
        reportId = "";
        formIds = new ArrayList();
        formNames = new ArrayList();

        masterFieldId = new ArrayList();
        masterFieldName = new ArrayList();
        masterFieldType = new ArrayList();
        filterIds = new ArrayList();
        filterNames = new ArrayList();

        fldObjects = new HashMap();
        filterObjects = new HashMap();
        sortObjects = new HashMap();

        selectedFieldCount = 0;

        formTypes = new ArrayList();
        coreLookupViewIds = new ArrayList();
        coreLookupViewNames = new ArrayList();

        htFormTablePKInfo = new Hashtable();

        selectedCoreLookupformIds = new ArrayList();

        checkForStudyRights = false;
        repUseUniqueId = "";
        repUseDataValue = "";
        ignoreFilters = new Hashtable();
    }

    /**
     * Returns the value of reportId.
     * 
     * @return The reportId value
     */
    public String getReportId() {
        return reportId;
    }

    /**
     * Sets the value of reportId.
     * 
     * @param reportId
     *            The value to assign reportId.
     */
    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    /**
     * Returns the value of reportName.
     * 
     * @return The reportName value
     */
    public String getReportName() {
        return reportName;
    }

    /**
     * Sets the value of reportName.
     * 
     * @param reportName
     *            The value to assign reportName.
     */
    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    /**
     * Returns the value of reportType.
     * 
     * @return The reportType value
     */
    public String getReportType() {
        return reportType;
    }

    /**
     * Sets the value of reportType.
     * 
     * @param reportType
     *            The value to assign reportType.
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;

        // populate default core lookup data
        populateCoreLookupInfo();
    }

    /**
     * Returns the value of reportHeader.
     * 
     * @return The reportHeader value
     */
    public String getReportHeader() {
        return reportHeader;
    }

    /**
     * Sets the value of reportHeader.
     * 
     * @param reportHeader
     *            The value to assign reportHeader.
     */
    public void setReportHeader(String reportHeader) {
        this.reportHeader = reportHeader;
    }

    /**
     * Returns the value of reportFooter.
     * 
     * @return The reportFooter value
     */
    public String getReportFooter() {
        return reportFooter;
    }

    /**
     * Sets the value of reportFooter.
     * 
     * @param reportFooter
     *            The value to assign reportFooter.
     */
    public void setReportFooter(String reportFooter) {
        this.reportFooter = reportFooter;
    }

    /**
     * Returns the value of studyId.
     * 
     * @return The studyId value
     */
    public String getStudyId() {
        return studyId;
    }

    /**
     * Sets the value of studyId.
     * 
     * @param studyId
     *            The value to assign studyId.
     */
    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    /**
     * Returns the value of patientId.
     * 
     * @return The patientId value
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * Sets the value of patientId.
     * 
     * @param patientId
     *            The value to assign patientId.
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    /**
     * Returns the value of reportDesc.
     * 
     * @return The reportDesc value
     */
    public String getReportDesc() {
        return reportDesc;
    }

    /**
     * Sets the value of reportDesc.
     * 
     * @param reportDesc
     *            The value to assign reportDesc.
     */
    public void setReportDesc(String reportDesc) {
        this.reportDesc = reportDesc;
    }

    /**
     * Returns the value of formIds.
     * 
     * @return The formIds value
     */
    public ArrayList getFormIds() {
        return formIds;
    }

    /**
     * Sets the value of formIds.
     * 
     * @param formIds
     *            The value to assign formIds.
     */
    public void setFormIds(ArrayList formIds) {
        this.formIds = formIds;
    }

    /**
     * Sets the value of formIds.
     * 
     * @param formId
     *            The value to add to formIds.
     */
    public void setFormIds(String formId) {
        this.formIds.add(formId);
    }

    /**
     * Returns the value of formIdStr.
     * 
     * @return The formIdStr value
     */
    public String getFormIdStr() {
        return formIdStr;
    }

    /**
     * Sets the value of formIdStr.
     * 
     * @param formIdStr
     *            The value to assign formIdStr.
     */
    public void setFormIdStr(String formIdStr) {
        this.formIdStr = formIdStr;
        formIds.clear();
        addFormIds();
    }

    /**
     * Returns the value of formNameStr.
     * 
     * @return The formNameStr value
     */
    public String getFormNameStr() {
        return formNameStr;
    }

    /**
     * Sets the value of formNameStr.
     * 
     * @param formNameStr
     *            The value to assign formNameStr.
     */
    public void setFormNameStr(String formNameStr) {
        this.formNameStr = formNameStr;
        formNames.clear();
        addFormNames();
    }

    /**
     * Returns the value of formNames.
     * 
     * @return The formNames value
     */
    public ArrayList getFormNames() {
        return formNames;
    }

    /**
     * Sets the value of formNames.
     * 
     * @param formNames
     *            The value to assign formNames.
     */
    public void setFormNames(ArrayList formNames) {
        this.formNames = formNames;
    }

    /**
     * Sets the value of formNames.
     * 
     * @param formName
     *            The value to add to formNames.
     */
    public void setFormNames(String formName) {
        this.formNames.add(formName);
    }

    /**
     * Returns the value of prevFormSelection.
     * 
     * @return The prevFormSelection value
     */
    public String getPrevFormSelection() {
        return prevFormSelection;
    }

    /**
     * Sets the value of prevFormSelection.
     * 
     * @param prevFormSelection
     *            The value to assign prevFormSelection.
     */
    public void setPrevFormSelection(String prevFormSelection) {
        this.prevFormSelection = prevFormSelection;
    }

    /**
     * Returns the value of shareWith.
     * 
     * @return The shareWith value
     */
    public String getShareWith() {
        return shareWith;
    }

    /**
     * Sets the value of shareWith.
     * 
     * @param shareWith
     *            The value to assign shareWith.
     */
    public void setShareWith(String shareWith) {
        this.shareWith = shareWith;
    }

    /**
     * Returns the value of reportMode.
     * 
     * @return The reportMode value
     */
    public String getReportMode() {
        return reportMode;
    }

    /**
     * Sets the value of reportMode.
     * 
     * @param reportMode
     *            The value to assign reportMode.
     */
    public void setReportMode(String reportMode) {
        this.reportMode = reportMode;
    }

    /**
     * Returns the value of fldObjects.
     * 
     * @return The fldObjects value
     */
    public HashMap getFldObjects() {
        return fldObjects;
    }

    /**
     * Sets the value of fldObjects.
     * 
     * @param fldObjects
     *            The value to assign fldObjects.
     */
    public void setFldObjects(HashMap fldObjects) {
        this.fldObjects = fldObjects;
    }

    /**
     * Sets the value of fldObjects.
     * 
     * @param key
     *            The new fldObjects value
     * @param value
     *            The new fldObjects value
     */
    public void setFldObjects(String key, FieldContainer value) {
        this.fldObjects.put(key, value);
    }

    /**
     * Returns the value of fldObjects.
     * 
     * @param key
     *            Description of the Parameter
     * @return The fldObject value
     */
    public FieldContainer getFldObject(String key) {
        return (FieldContainer) this.fldObjects.get(key);
    }

    /**
     * Create a new Field Objects for the key passed
     * 
     * @param key
     *            formId passed as key
     * @return refrence to the new Field Container Object
     */
    public FieldContainer newFldObject(String key) {
        FieldContainer container = new FieldContainer();
        setFldObjects(key, container);
        return container;
    }

    /**
     * Returns the value of filterObjects.
     * 
     * @return The filterObjects value
     */
    public HashMap getFilterObjects() {
        return filterObjects;
    }

    /**
     * Sets the value of filterObjects.
     * 
     * @param filterObjects
     *            The value to assign filterObjects.
     */
    public void setFilterObjects(HashMap filterObjects) {
        this.filterObjects = filterObjects;
    }

    /**
     * Sets the value of filterObjects.
     * 
     * @param key
     *            The new filterObject value
     * @param value
     *            The new filterObject value
     */
    public void setFilterObject(String key, FilterContainer value) {
        this.filterObjects.put(key, value);
    }

    /**
     * Gets the filterObject attribute of the ReportHolder object
     * 
     * @param key
     *            Description of the Parameter
     * @return The filterObject value
     */
    public FilterContainer getFilterObject(String key) {
        return (FilterContainer) this.filterObjects.get(key);
    }

    /**
     * Create a new Filter Object for the key
     * 
     * @param key -
     *            passed filterId to attach the object to
     * @return Description of the Return Value
     */

    public FilterContainer newFilterObject(String key) {
        FilterContainer container = new FilterContainer();
        setFilterObject(key, container);
        return container;
    }

    /**
     * Returns the value of sortObjects.
     * 
     * @return The sortObjects value
     */
    public HashMap getSortObjects() {
        return sortObjects;
    }

    /**
     * Sets the value of sortObjects.
     * 
     * @param sortObjects
     *            The value to assign sortObjects.
     */
    public void setSortObjects(HashMap sortObjects) {
        this.sortObjects = sortObjects;
    }

    /**
     * Sets the value of sortObjects.
     * 
     * @param key
     *            Key for sortObject
     * @param value
     *            SortObject
     */
    public void setSortObject(String key, SortContainer value) {
        this.sortObjects.put(key, value);
    }

    /**
     * retrieve the SortObjects for the key specified.
     * 
     * @param key
     *            key for which sortobject is retrieved.
     * @return SortContainer for the key
     */
    public SortContainer getSortObject(String key) {
        return (SortContainer) this.sortObjects.get(key);
    }

    /**
     * Create a new SortObject for the specified key
     * 
     * @param key
     *            for which the object is created.
     * @return SortContainer object
     */
    public SortContainer newSortObject(String key) {
        SortContainer sortContainer = new SortContainer();
        setSortObject(key, sortContainer);
        return sortContainer;
    }

    /**
     * Returns the value of filterIds.
     * 
     * @return The filterIds value
     */
    public ArrayList getFilterIds() {
        return filterIds;
    }

    /**
     * Sets the value of filterIds.
     * 
     * @param filterIds
     *            The value to assign filterIds.
     */
    public void setFilterIds(ArrayList filterIds) {
        this.filterIds = filterIds;
    }

    /**
     * Sets the value of filterIds.
     * 
     * @param filterId
     *            The new filterIds value
     */
    public void setFilterIds(String filterId) {
        this.filterIds.add(filterId);
    }

    /**
     * Returns the value of filterNames.
     * 
     * @return The filterNames value
     */
    public ArrayList getFilterNames() {
        return filterNames;
    }

    /**
     * Sets the value of filterNames.
     * 
     * @param filterNames
     *            The value to assign filterNames.
     */
    public void setFilterNames(ArrayList filterNames) {
        this.filterNames = filterNames;
    }

    /**
     * Sets the value of filterNames.
     * 
     * @param filterName
     *            The new filterNames value
     */
    public void setFilterNames(String filterName) {
        this.filterNames.add(filterName);
    }

    /**
     * Returns the value of masterFieldId.
     * 
     * @return The masterFieldId value
     */
    public ArrayList getMasterFieldId() {
        return masterFieldId;
    }

    /**
     * Sets the value of masterFieldId.
     * 
     * @param masterFieldId
     *            The value to assign masterFieldId.
     */
    public void setMasterFieldId(ArrayList masterFieldId) {
        this.masterFieldId = masterFieldId;
    }

    /**
     * Returns the value of masterFieldName.
     * 
     * @return The masterFieldName value
     */
    public ArrayList getMasterFieldName() {
        return masterFieldName;
    }

    /**
     * Sets the value of masterFieldName.
     * 
     * @param masterFieldName
     *            The value to assign masterFieldName.
     */
    public void setMasterFieldName(ArrayList masterFieldName) {
        this.masterFieldName = masterFieldName;
    }

    /**
     * Returns the value of masterFieldType.
     * 
     * @return The masterFieldType value
     */
    public ArrayList getMasterFieldType() {
        return masterFieldType;
    }

    /**
     * Sets the value of masterFieldType.
     * 
     * @param masterFieldType
     *            The value to assign masterFieldType.
     */
    public void setMasterFieldType(ArrayList masterFieldType) {
        this.masterFieldType = masterFieldType;
    }

    /**
     * Sets the value of masterFieldType.
     * 
     * @param fieldType
     *            The new masterFieldType value
     */
    public void setMasterFieldType(String fieldType) {
        this.masterFieldType.add(masterFieldType);
    }

    /**
     * Returns the value of orderMergedStr.
     * 
     * @return The orderMergedStr value
     */
    public String getOrderMergedStr() {
        return orderMergedStr;
    }

    /**
     * Sets the value of orderMergedStr.
     * 
     * @param orderMergedStr
     *            The value to assign orderMergedStr.
     */
    public void setOrderMergedStr(String orderMergedStr) {
        this.orderMergedStr = orderMergedStr;
    }

    /**
     * Returns the value of formTypes.
     */
    public ArrayList getFormTypes() {
        return formTypes;
    }

    /**
     * Sets the value of formTypes.
     * 
     * @param formTypes
     *            ArrayList of formTypes to identify the form type.
     */
    public void setFormTypes(ArrayList formTypes) {
        this.formTypes = formTypes;
    }

    /**
     * Adds the value of a form Type to the ArrayList formTypes.
     * 
     * @param formType
     *            form Type to identify the form.
     */
    public void setFormTypes(String formType) {
        this.formTypes.add(formType);
    }

    /**
     * Returns the value of coreLookupViewIds.
     */
    public ArrayList getCoreLookupViewIds() {
        return coreLookupViewIds;
    }

    /**
     * Sets the value of coreLookupViewIds.
     * 
     * @param coreLookupViewIds
     *            The value to assign coreLookupViewIds.
     */
    public void setCoreLookupViewIds(ArrayList coreLookupViewIds) {
        this.coreLookupViewIds = coreLookupViewIds;
    }

    /**
     * Returns the value of coreLookupViewNames.
     */
    public ArrayList getCoreLookupViewNames() {
        return coreLookupViewNames;
    }

    /**
     * Returns a list of all coreLookupViewNames.
     */
    public String getCoreLookupViewNamesString() {
        String strNames = "";

        if (coreLookupViewNames != null) {
            for (int i = 0; i < coreLookupViewNames.size(); i++) {

                strNames = strNames + ", " + (String) coreLookupViewNames.get(i);

            }
        }

        if (!StringUtil.isEmpty(strNames)) {
            strNames = strNames.substring(1); // extract after first ";"
        }

        return strNames;
    }

    /**
     * Sets the value of coreLookupViewNames.
     * 
     * @param coreLookupViewNames
     *            The value to assign coreLookupViewNames.
     */
    public void setCoreLookupViewNames(ArrayList coreLookupViewNames) {
        this.coreLookupViewNames = coreLookupViewNames;
    }

    /**
     * Adds a core view lookup name to the ArrayListcoreLookupViewNames.
     * 
     * @param coreLookupViewName
     *            The value to add to coreLookupViewNames.
     */
    public void setCoreLookupViewNames(String coreLookupViewName) {
        this.coreLookupViewNames.add(coreLookupViewName);
    }

    /**
     * Adds a core view lookup to ArrayList coreLookupViewIds.
     * 
     * @param coreLookupViewId
     *            The value to add coreLookupViewIds.
     */
    public void setCoreLookupViewIds(String coreLookupViewId) {
        this.coreLookupViewIds.add(coreLookupViewId);
    }

    /**
     * Returns the value of htFormTablePKInfo.
     */
    public Hashtable getHtFormTablePKInfo() {
        return htFormTablePKInfo;
    }

    /**
     * Returns the value for the passed key.
     * 
     * @param formId
     *            The Key for which the hashtable of PK Values is required
     * @return A Hashtable of PKInfo for the form, The Keys in this Hashtable
     *         are <br>: 1. mapPKTableName : The main table name associated
     *         with the form <br>
     *         2. mapTablePK: The PK column for the form, This column will be
     *         selected to filter out records <br>
     *         3. mapTableFilterCol: The Filter column for the form. This will
     *         be the main filter column <br>
     */
    public Hashtable getHtFormTablePKInfo(String formId) {
        if (htFormTablePKInfo.containsKey(formId)) {
            return (Hashtable) htFormTablePKInfo.get(formId);
        } else {
            return null;
        }
    }

    /**
     * Returns the value of checkForStudyRights.
     */
    public boolean getCheckForStudyRights() {
        return checkForStudyRights;
    }

    /**
     * Sets the value of checkForStudyRights.
     * 
     * @param checkForStudyRights
     *            The value to assign checkForStudyRights.
     */
    public void setCheckForStudyRights(boolean checkForStudyRights) {
        this.checkForStudyRights = checkForStudyRights;
    }

    /**
     * Sets the value of htFormTablePKInfo.
     * 
     * @param htFormTablePKInfo
     *            The value to assign htFormTablePKInfo.
     */
    public void setHtFormTablePKInfo(Hashtable htFormTablePKInfo) {
        this.htFormTablePKInfo = htFormTablePKInfo;
    }

    /**
     * Adds a PKInfo Hashtable for a formId Key to the Hashtable
     * htFormTablePKInfo
     * 
     * @param formIdKey
     *            The Key for Hashtable htFormTablePKInfo, contains a
     *            formId/lookupViewid.
     * @param htPKInfo
     *            The PKInfo Hashtable for formId Key
     */
    public void setHtFormTablePKInfo(String formIdKey, Hashtable htPKInfo) {
        this.htFormTablePKInfo.put(formIdKey, htPKInfo);
    }

    /**
     * Calculate the number of fields selected in the report from different
     * forms
     * 
     * @return The selectedFieldCount value
     */

    /**
     * Returns the value of selectedCoreLookupformIds.
     */
    public ArrayList getSelectedCoreLookupformIds() {
        return selectedCoreLookupformIds;
    }

    /**
     * Sets the value of selectedCoreLookupformIds.
     * 
     * @param selectedCoreLookupformIds
     *            The value to assign selectedCoreLookupformIds.
     */
    public void setSelectedCoreLookupformIds(ArrayList selectedCoreLookupformIds) {
        this.selectedCoreLookupformIds = selectedCoreLookupformIds;
    }

    /**
     * Adds a prefixed core lookupview id to selectedCoreLookupformIds.
     * 
     * @param selectedCoreLookupformId
     *            The value to add to selectedCoreLookupformIds.
     */
    public void setSelectedCoreLookupformIds(String selectedCoreLookupformId) {
        // check if the selectedCoreLookupformId does not exist in the
        // arrayList, then add it

        if (!selectedCoreLookupformIds.contains(selectedCoreLookupformId)) {
            this.selectedCoreLookupformIds.add(selectedCoreLookupformId);
        }
    }

    /**
     * Returns the value of repUseUniqueId.
     */
    public String getRepUseUniqueId() {
        return repUseUniqueId;
    }

    /**
     * Sets the value of repUseUniqueId.
     * 
     * @param repUseUniqueId
     *            The value to assign repUseUniqueId.
     */
    public void setRepUseUniqueId(String repUseUniqueId) {
        this.repUseUniqueId = repUseUniqueId;
    }

    /**
     * Returns the value of repUseDataValue.
     */
    public String getRepUseDataValue() {
        return repUseDataValue;
    }

    /**
     * Sets the value of repUseDataValue.
     * 
     * @param repUseDataValue
     *            The value to assign repUseDataValue.
     */
    public void setRepUseDataValue(String repUseDataValue) {
        this.repUseDataValue = repUseDataValue;
    }

    /**
     * Returns the value of ignoreFilters.
     */
    public Hashtable getIgnoreFilters() {
        return ignoreFilters;
    }

    /**
     * Sets the value of ignoreFilters.
     * 
     * @param ignoreFilters
     *            The value to assign ignoreFilters.
     */
    public void setIgnoreFilters(Hashtable ignoreFilters) {
        this.ignoreFilters = ignoreFilters;
    }

    /**
     * Adds to ignoreFilters.
     * 
     * @param String
     *            formkey The key
     * @param String
     *            filterCodes The codes for the value
     */
    public void setIgnoreFilters(String formkey, String filterCodes) {
        this.ignoreFilters.put(formkey, filterCodes);
    }

    /**
     * Returns the value of ignoreFilters for a form
     */
    public String getIgnoreFilters(String form) {
        String value = "";
        if (ignoreFilters.containsKey(form)) {
            value = (String) ignoreFilters.get(form);
            if (StringUtil.isEmpty(value)) {
                value = "";
            }
        }
        return value;
    }

    public int removeSelectedCoreLookupformId(String lkpViewId) {
        int pos = -1;

        pos = selectedCoreLookupformIds.indexOf(lkpViewId);

        if (pos >= 0) {
            if (pos <= selectedCoreLookupformIds.size()) {

                try {
                    selectedCoreLookupformIds.remove(pos);
                } catch (IndexOutOfBoundsException ix) {

                    Rlog.fatal("dynrep",
                            "Could not remove from selectedCoreLookupformIds, Exception:"
                                    + ix);
                    return -1;

                }

            }
        }
        return 0;
    }

    public int getSelectedFieldCount() {
        selectedFieldCount = 0;
        Collection lnset = fldObjects.values();
        Iterator itr = lnset.iterator();
        while (itr.hasNext()) {
            FieldContainer fldcon = (FieldContainer) itr.next();
            selectedFieldCount = selectedFieldCount
                    + fldcon.getFieldColId().size();
        }

        return selectedFieldCount;
    }

    /**
     * Parses the FormIds delimited list and stores in formIds List
     */
    public void addFormIds() {
        String[] formArray;
        String[] formNameArray;

        String coreView = "";

        if (formIdStr == null) {
            Rlog.fatal("dynrep", "Cannot add null String to Form List");
        }

        // before adding form ids, add the core lookup view ids.

        //JM: shuffled on  28July2006 : just reverse of the above comment happens
        if (!StringUtil.isEmpty(formIdStr)) {
            if (formIdStr.indexOf(delimiter) >= 0) {
                formArray = StringUtil.strSplit(formIdStr, delimiter, false);
                for (int i = 0; i < formArray.length; i++) {
                    if (formIds.indexOf(formArray[i]) < 0) {
                        setFormIds(formArray[i]);
                        setFormTypes("F");
                    }
                }

            } else {
                setFormIds(formIdStr);
                setFormTypes("F");
            }
        }
        
        for (int i = 0; i < coreLookupViewIds.size(); i++) {
            coreView = (String) coreLookupViewIds.get(i);

            if (!StringUtil.isEmpty(coreView)) {
                // All core View ids will preceede with a "C"
                setFormIds("C" + coreView);
                setFormTypes("C");

            }

        }
//JM: 28July2006: Move the segment above the core look up
        /*
        if (!StringUtil.isEmpty(formIdStr)) {
            if (formIdStr.indexOf(delimiter) >= 0) {
                formArray = StringUtil.strSplit(formIdStr, delimiter, false);
                for (int i = 0; i < formArray.length; i++) {
                    if (formIds.indexOf(formArray[i]) < 0) {
                        setFormIds(formArray[i]);
                        setFormTypes("F");
                    }
                }

            } else {
                setFormIds(formIdStr);
                setFormTypes("F");
            }
        }*/
    }

    /**
     * Parse the formName String and add to ArrayList
     * 
     */
    /* Modified by Sonia Abrol, 09/25/06, removed the check - if form name already exists. This was causing crashes 
     * when two different forms (of different form types) have same form name   */
    
    public void addFormNames() {
        String[] formNameArray;
        String coreViewName = "";
        String nameDelim = "{VELSEP}";

        if (formNameStr == null) {
            Rlog.fatal("dynrep", "Cannot add null String to Form List");
        }
        // before adding form Names, add the core lookup view names.

        //JM: 14Jul2006: 
        if (!StringUtil.isEmpty(formNameStr)) {

            if (formNameStr.indexOf(nameDelim) >= 0) {
                formNameArray = StringUtil.strSplit(formNameStr, nameDelim,
                        false);
                for (int i = 0; i < formNameArray.length; i++) {
                        setFormNames(formNameArray[i]);
                    
                }
            } else {

                setFormNames(formNameStr);
            }

        }
        
        for (int i = 0; i < coreLookupViewNames.size(); i++) {
            coreViewName = (String) coreLookupViewNames.get(i);

            if (!StringUtil.isEmpty(coreViewName)) {
                setFormNames(coreViewName);

            }

        }
        

    }

    /**
     * Combine all the fields available to create a merged list to display on
     * Filter screen
     */
    public void compileFormFields() {    	
    	String compString = "";//JM:27Jul2006
    	
        int size = 0;
        String formId = "";
        String formName = "";
        int index = -1;
        ArrayList tempIds;
        ArrayList tempNames;
        ArrayList tempTypes;
        masterFieldId.clear();
        masterFieldName.clear();
        masterFieldType.clear();
        Collection lnset1 = fldObjects.keySet();
        Iterator itr3 = lnset1.iterator();
        while (itr3.hasNext()) {
            formId = (String) itr3.next();
            
            Hashtable htPKInfo = new Hashtable();

            index = formIds.indexOf(formId);
            if (index >= 0) {
                formName = (String) formNames.get(index);
            }
            FieldContainer fldCon = (FieldContainer) fldObjects.get(formId);
            
            Rlog.debug("dynrep", formId
                    + "fldCon.getFieldColId()).size()**********"
                    + fldCon.getFieldColId().size());

            if ((fldCon.getFieldColId()).size() > 0) {
                tempIds = fldCon.getFieldList();      
                tempNames = fldCon.getFieldNameList();
                tempTypes = fldCon.getFieldTypesNoRepeatFields();

                // tempTypes = fldCon.getFieldTypeList();
                // masterFieldId.add("");
                // masterFieldName.add(formName);
                // masterFieldType.add("");
                size = tempIds.size();
                if (size > 0) {

                    // add the PK info

                    htPKInfo.put("mapTablePK", fldCon.getPkColName());
                    htPKInfo.put("mapPKTableName", fldCon.getPkTableName());
                    htPKInfo.put("mapTableFilterCol", fldCon.getMainFilterColName());
                    htPKInfo.put("mapStudyColumn", fldCon.getMapStudyColumn());

                    if (!StringUtil.isEmpty(fldCon.getMapStudyColumn())) {
                        setCheckForStudyRights(true);
                    }

                    setHtFormTablePKInfo(formId, htPKInfo);
                    
                    for (int i = 0; i < size; i++) {
                    	
                    	//JM: 27July2006
                    	if (!compString.equals(formId)){
                    		
                    		
                    		masterFieldId.add("nVal");
                    		
                    		//modified by sonia abrol,12/05/06, we also need to update  masterFieldType
                    		masterFieldType.add("");
                    		
                    		if (formId.substring(0, 1).equals("C")){                    		
                    		masterFieldName.add("***Table:" + " " + formName + "***");
                    		}
                    		else
                    		{                    			
                    		masterFieldName.add("***Form:" + " " + formName + "***");
                    		}
                    		compString = formId;
                    		
                    	}
                    	//JM: 27July2006                	
                    	
                    	masterFieldId.add(formId + "[VELSEP]" + tempIds.get(i));
                    	// masterFieldName.add("---"+tempNames.get(i));
                        masterFieldName.add(tempNames.get(i));
                        masterFieldType.add(tempTypes.get(i));
                    }

                }
            }// end if fldCon.getFieldColId()).size
            else {
                // check if the form is in htFormTablePKInfo, if yes then remove
                // it
                if (htFormTablePKInfo.containsKey(formId)) {
                    htFormTablePKInfo.remove(formId);
                }

            }
            // fldcon.Display();
        }
        Rlog.debug("dynrep", "masterIdList" + masterFieldId
                + "\n masterFieldName" + masterFieldName
                + "\n masterFieldTypes" + masterFieldType);
    }

    /**
     * Method to combine the Sort String for different forms delimiter by
     * [VELSEP]
     */
    public void createSortString() {
        String formId = "";
        String tempStr = "";
        SortContainer sortContainer;
        orderMergedStr = "";
        for (int i = 0; i < formIds.size(); i++) {
            formId = (String) formIds.get(i);
            if (sortObjects.containsKey(formId)) {
                sortContainer = (SortContainer) sortObjects.get(formId);
                if ((sortContainer.getSortFields().size()) > 0) {
                    tempStr = sortContainer.getSortStr();

                    if (StringUtil.isEmpty(tempStr))
                        tempStr = "NS";

                    // append formId

                    tempStr = tempStr + "[*]" + formId;

                    // tempStr = (tempStr == null) ? "" : tempStr;

                    if (orderMergedStr.length() > 0) {
                        orderMergedStr = orderMergedStr + "[VELSEP]" + tempStr;
                    } else {
                        orderMergedStr = tempStr;
                    }
                } else {
                    // else for sortContainer!=null
                    if (orderMergedStr.length() > 0) {
                        orderMergedStr = orderMergedStr + "[VELSEP]" + "NS"
                                + "[*]" + formId;
                    } else {
                        orderMergedStr = "NS" + "[*]" + formId;
                    }
                }
            } else {
                if (orderMergedStr.length() > 0) {
                    orderMergedStr = orderMergedStr + "[VELSEP]" + "NS" + "[*]"
                            + formId;
                } else {
                    orderMergedStr = "NS" + "[*]" + formId;
                }
            }

        }
    }

    /**
     * Reset FormLists
     */
    public void reset() {
        formIds.clear();
        formNames.clear();
    }

    /**
     * Returns a comma separated string of all formIds + all selected Core
     * Lookup Viewe Ids
     */
    public String getAllFormIdStr() {
        String allStr = "";

        allStr = formIdStr;

        if (StringUtil.isEmpty(allStr)) {
            allStr = "";
        }

        for (int i = 0; i < selectedCoreLookupformIds.size(); i++) {
            allStr = allStr + ";" + selectedCoreLookupformIds.get(i);
        }

        if (allStr.startsWith(";")) {
            allStr = allStr.substring(1);
        }

        return allStr;

    }

    /**
     * Sonia Abrol, 07/18/05 Call this method to remove obsolete filter, field
     * and sort objects from the ReportHolder objects. These objects will get
     * obsolete when in New mode selected forms are changed and for the old
     * forms the field, filter or sort objects were already created
     */
    public void removeObsoleteDependentObjects() {
        // iterate through fldObjects and remove obsolete field objects

        String formId = "";
        String filterId = "";

        Collection colFields = fldObjects.keySet();
        Iterator itrFields = colFields.iterator();

        Collection colFilters = filterObjects.keySet();
        Iterator itrFilters = colFilters.iterator();

        Collection colSort = sortObjects.keySet();
        Iterator itrSort = colSort.iterator();
        int idx = 0;

        while (itrFields.hasNext()) {
            formId = (String) itrFields.next();
            // check if this form is selected, if not delete the resspective
            // object
            if (!formIds.contains(formId)) {
                itrFields.remove();
                Rlog.debug("dynrep", "Removed field for old form : " + formId);
            }
        }

        while (itrFilters.hasNext()) {
            filterId = (String) itrFilters.next();

            FilterContainer fltcon = (FilterContainer) filterObjects
                    .get(filterId);
            ArrayList fltForms = new ArrayList();

            fltForms = fltcon.getFormIds();

            if (fltForms.size() == 0) {
                itrFilters.remove();
                Rlog.debug("dynrep", "Removed filter for old form's filter : "
                        + filterId);

                idx = filterIds.indexOf(filterId);

                if (idx >= 0) {
                    filterIds.remove(idx);
                    filterNames.remove(idx);
                }

            } else if (fltForms.size() > 0) {

                for (int i = 0; i < fltForms.size(); i++) {
                    // check if this form is selected, if not delete the
                    // resspective object
                    formId = (String) fltForms.get(i);
                    Rlog.debug("dynrep",
                            " filter for old form's filter,formID : " + formId);
                    if (!formIds.contains(formId)) {
                        itrFilters.remove();
                        Rlog.debug("dynrep",
                                "Removed filter for old form's filter : "
                                        + filterId);

                        idx = filterIds.indexOf(filterId);

                        if (idx >= 0) {
                            filterIds.remove(idx);
                            filterNames.remove(idx);
                        }
                    }
                }

            }
        }

        while (itrSort.hasNext()) {
            formId = (String) itrSort.next();
            // check if this form is selected, if not delete the resspective
            // object
            if (!formIds.contains(formId)) {
                itrSort.remove();
                Rlog.debug("dynrep", "Removed sort for old form : " + formId);
            }
        }

    }

    /**
     * Description of the Method
     */
    public void Display() {
        System.out
                .println("****************START DISPLAY******************************");
        System.out
                .println("****************START Field Objects DISPLAY******************************");

        Collection lnset1 = fldObjects.keySet();
        Iterator itr3 = lnset1.iterator();
        while (itr3.hasNext()) {
            System.out.println("Field key Object" + itr3.next());

        }
        lnset1 = fldObjects.values();
        itr3 = lnset1.iterator();
        while (itr3.hasNext()) {
            FieldContainer fldcon = (FieldContainer) itr3.next();
            System.out.println("Field Objects" + fldcon);
            fldcon.Display();
        }

        System.out
                .println("******************END FIELD OBJECT DISPLAY****************************");

        System.out
                .println("****************START FILTER OBJECTS DISPLAY******************************");
        lnset1 = filterObjects.keySet();
        itr3 = lnset1.iterator();
        while (itr3.hasNext()) {
            System.out.println("Filter key Object" + itr3.next());

        }

        lnset1 = filterObjects.values();
        itr3 = lnset1.iterator();
        while (itr3.hasNext()) {
            FilterContainer fltcon = (FilterContainer) itr3.next();
            System.out.println("Filter Object" + fltcon);
            fltcon.Display();
        }

        System.out
                .println("******************END FILTER OBJECT DISPLAY****************************");

        System.out
                .println("****************START SORT OBJECTS DISPLAY******************************");

        lnset1 = sortObjects.keySet();
        itr3 = lnset1.iterator();
        while (itr3.hasNext()) {
            System.out.println("Sort key Object" + itr3.next());

        }
        lnset1 = sortObjects.values();
        itr3 = lnset1.iterator();
        while (itr3.hasNext()) {
            SortContainer sortcon = (SortContainer) itr3.next();
            System.out.println("Sort Object" + sortcon);
            sortcon.Display();
        }

        System.out
                .println("******************END SORT OBJECT DISPLAY****************************");
        System.out.println("coreViewIds: " + coreLookupViewIds.toString()
                + "\n");
        System.out.println("coreLookupViewNames: "
                + coreLookupViewNames.toString() + "\n");

        System.out.println("selectedCoreLookupformIds: "
                + selectedCoreLookupformIds.toString() + "\n");
        System.out.println("getCheckForStudyRights: "
                + getCheckForStudyRights() + "\n");

        System.out
                .println("******************DISPLAY CORE VIEW INFO****************************");

        System.out
                .println("******************END DISPLAY****************************");
    }

    /**
     * Description of the Method
     */
    public void DisplayDetail() {
        System.out
                .println("******************Report Details*******************");
        System.out.println("reportName: " + reportName + "\n");
        System.out.println("reportType: " + reportType + "\n");
        System.out.println("reportHeader: " + reportHeader + "\n");
        System.out.println("reportFooter: " + reportFooter + "\n");
        System.out.println("studyId: " + studyId + "\n");
        System.out.println("patientId:  " + patientId + "\n");
        System.out.println("reportDesc: " + reportDesc + "\n");
        System.out.println("formIdStr: " + formIdStr + "\n");
        System.out.println("formNameStr: " + formNameStr + "\n");
        System.out.println("formIds: " + formIds + "\n");
        System.out.println("formNames: " + formNames + "\n");
        System.out.println("prevFormSelection: " + prevFormSelection + "\n");
        System.out.println("shareWith: " + shareWith + "\n");
        System.out.println("reportMode: " + reportMode + "\n");
        System.out.println("reportId: " + reportId);
        System.out
                .println("******************Report Details END*******************");
    }

    /** Method to populate core lookup information. */
    public void populateCoreLookupInfo() {
        LookupDao ldao = new LookupDao();
        ArrayList arViewIds = new ArrayList();

        String dynRepView = "";

        coreLookupViewIds.clear();
        coreLookupViewNames.clear();

        String repType = "";
        repType = getReportType();

        if (StringUtil.isEmpty(repType)) {
            dynRepView = "dyn_p";
        } else if (repType.equals("S")) {
            dynRepView = "dyn_s";
        } else if (repType.equals("P")) {
            dynRepView = "dyn_p";
        } else if (repType.equals("A")) {
            dynRepView = "dyn_a";
        }

        if (!StringUtil.isEmpty(dynRepView)) {

            ldao.getAllViewsForLkpType(0, dynRepView);
            arViewIds = ldao.getLViewIds();

            if (arViewIds != null && arViewIds.size() > 0) {
                for (int i = 0; i < arViewIds.size(); i++) {
                    setCoreLookupViewIds(((Integer) arViewIds.get(i))
                            .toString());
                }

                setCoreLookupViewNames(ldao.getLViewNames());

            }

        }
    }
}
