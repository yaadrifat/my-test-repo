/*
 * Classname : 				CatLibJB
 * 
 * Version information : 	1.0
 *
 * Date 					06/25/2003
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Anu
 */

package com.velos.eres.web.catLib;

/* IMPORT STATEMENTS */

import com.velos.eres.business.catLib.impl.CatLibBean;
import com.velos.eres.business.common.CatLibDao;
import com.velos.eres.service.catLibAgent.CatLibAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for Category module. This class is used for any kind of
 * manipulation for the Category.
 * 
 * @author Anu
 * @version 1.0, 06/25/2003
 */

public class CatLibJB {

    /*
     * category Id
     */
    private int catLibId;

    /*
     * category account
     */
    private String catLibAcc;

    /*
     * category type
     */
    private String catLibType;

    /*
     * category name
     */
    private String catLibName;

    /*
     * category description
     */
    private String catLibDesc;

    /*
     * category creator
     */
    private String catLibCreator;

    /*
     * category last modified by
     */
    private String catLibLastModBy;

    /*
     * IP address
     */
    private String catLibIPAdd;

    /*
     * Record Type
     */
    private String recordType;

    // GETTER SETTER METHODS

    public int getCatLibId() {
        return this.catLibId;
    }

    public void setCatLibId(int catLibId) {
        this.catLibId = catLibId;
    }

    public String getCatLibAcc() {
        return this.catLibAcc;
    }

    public void setCatLibAcc(String catLibAcc) {
        this.catLibAcc = catLibAcc;
    }

    public String getCatLibType() {
        return this.catLibType;
    }

    public void setCatLibType(String catLibType) {
        this.catLibType = catLibType;

    }

    public String getCatLibName() {
        return this.catLibName;
    }

    public void setCatLibName(String catLibName) {
        this.catLibName = catLibName;
    }

    public String getCatLibDesc() {
        return this.catLibDesc;
    }

    public void setCatLibDesc(String catLibDesc) {
        this.catLibDesc = catLibDesc;
    }

    public String getCatLibCreator() {
        return this.catLibCreator;
    }

    public void setCatLibCreator(String catLibCreator) {
        this.catLibCreator = catLibCreator;
    }

    public String getLastModifiedBy() {
        return this.catLibLastModBy;
    }

    public void setLastModifiedBy(String catLibLastModBy) {
        this.catLibLastModBy = catLibLastModBy;
    }

    public String getIPAdd() {
        return this.catLibIPAdd;
    }

    public void setIPAdd(String catLibIPAdd) {
        this.catLibIPAdd = catLibIPAdd;
    }

    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Defines an CatLibJB object with the specified category Id
     */
    public CatLibJB(int catLibId) {
        setCatLibId(catLibId);
    }

    /**
     * Defines an CatLibJB object with default values for fields
     */
    public CatLibJB() {
        Rlog.debug("catlib", "CatLibJB.CatLibJB() ");
    };

    /**
     * Defines an CatLibJB object with the specified values for the fields
     * 
     */
    public CatLibJB(int catLibId, String catLibAcc, String catLibType,
            String catLibName, String catLibDesc, String catLibCreator,
            String catLibLastModBy, String catLibIPAdd, String recordType) {
        setCatLibId(catLibId);
        setCatLibAcc(catLibAcc);
        setCatLibType(catLibType);
        setCatLibName(catLibName);
        setCatLibDesc(catLibDesc);
        setCatLibCreator(catLibCreator);
        setLastModifiedBy(catLibLastModBy);
        setIPAdd(catLibIPAdd);
        setRecordType(recordType);
        Rlog.debug("catlib", "CatLibJB.CatLibJB(all parameters)");
    }

    /**
     * Retrieves the details of an category in CatLibStateKeeper Object
     * 
     * @return CatLibStateKeeper consisting of the category details
     * @see CatLibStateKeeper
     */
    public CatLibBean getCatLibDetails() {
        CatLibBean clsk = null;
        try {
            CatLibAgentRObj catLibAgentRObj = EJBUtil.getCatLibAgentHome();
            clsk = catLibAgentRObj.getCatLibDetails(this.catLibId);
            Rlog.debug("catlib", "In CatLibJB.getCatLibDetails() " + clsk);
        } catch (Exception e) {
            Rlog
                    .fatal("catlib", "Error in getCatLibDetails() in CatLibJB "
                            + e);
        }
        if (clsk != null) {
            this.catLibId = clsk.getCatLibId();
            this.catLibAcc = clsk.getCatLibAcc();
            this.catLibType = clsk.getCatLibType();
            this.catLibName = clsk.getCatLibName();
            this.catLibDesc = clsk.getCatLibDesc();
            this.catLibCreator = clsk.getCatLibCreator();
            this.catLibLastModBy = clsk.getLastModifiedBy();
            this.catLibIPAdd = clsk.getIPAdd();
            this.recordType = clsk.getRecordType();
        }
        return clsk;
    }

    public void setCatLibDetails() {
        int output = 0;
        try {
            CatLibAgentRObj catLibAgentRObj = EJBUtil.getCatLibAgentHome();
            output = catLibAgentRObj.setCatLibDetails(this
                    .createCatLibStateKeeper());
            this.setCatLibId(output);
            Rlog.debug("catlib", "CatLibJB.setCatLibDetails()");

        } catch (Exception e) {
            Rlog
                    .fatal("catlib", "Error in setCatLibDetails() in CatLibJB "
                            + e);

        }
    }

    /**
     * Saves the modified details of the category to the database
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updateCatLib() {
        int output;
        try {
            CatLibAgentRObj catLibAgentRObj = EJBUtil.getCatLibAgentHome();
            output = catLibAgentRObj.updateCatLib(this
                    .createCatLibStateKeeper());
        } catch (Exception e) {
            Rlog.debug("catlib",
                    "EXCEPTION IN SETTING CATEGORY DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    /**
     * Uses the values of the current CatLibJB object to create an
     * CatLibStateKeeper Object
     * 
     * @return CatLibStateKeeper object with all the details of the category
     * @see CatLibtStateKeeper
     */
    public CatLibBean createCatLibStateKeeper() {
        Rlog.debug("catlib", "CatLibJB.createCatLibStateKeeper ");
        return new CatLibBean(this.catLibId, this.catLibAcc, this.catLibType,
                this.catLibName, this.catLibDesc, this.catLibCreator,
                this.catLibLastModBy, this.catLibIPAdd, this.recordType);
    }

    public CatLibDao getCategoriesWithAllOption(int accountId, String type,
            int forLastAll) {
        CatLibDao catLibDao = new CatLibDao();
        try {
            Rlog
                    .debug("catlib",
                            "CatLibJB.getCategoriesWithAllOption starting");
            CatLibAgentRObj catLibAgentRObj = EJBUtil.getCatLibAgentHome();
            catLibDao = catLibAgentRObj.getCategoriesWithAllOption(accountId,
                    type, forLastAll);

            return catLibDao;
        } catch (Exception e) {
            Rlog.fatal("catlib",
                    "getCategoriesWithAllOption(int accountId, String type) in CatLibJB "
                            + e);
        }
        return catLibDao;
    }

    public CatLibDao getAllCategories(int accountId, String type) {
        CatLibDao catLibDao = new CatLibDao();
        try {
            Rlog.debug("catlib", "CatLibJB.getAllCategories starting");
            CatLibAgentRObj catLibAgentRObj = EJBUtil.getCatLibAgentHome();
            catLibDao = catLibAgentRObj.getAllCategories(accountId, type);

            return catLibDao;
        } catch (Exception e) {
            Rlog.fatal("catlib",
                    "getAllCategories(int accountId, String type,  int forLastAll) in CatLibJB "
                            + e);
        }
        return catLibDao;
    }

}
