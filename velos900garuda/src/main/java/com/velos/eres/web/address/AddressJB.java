/*

 * Classname : AddressJB.java

 * 

 * Version information

 *

 * Date 02/26/2001

 * 

 * Copyright notice: Aithent 

 */

package com.velos.eres.web.address;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * 
 * Client side bean for Address
 * 
 * 
 * 
 * @author Sajal
 * 
 * @version 1.0 02/26/2001
 * 
 */

public class AddressJB {

    /**
     * 
     * the address Id
     * 
     */

    private int addId;

    /**
     * 
     * the address type
     * 
     */

    private String addType;

    /**
     * 
     * the address primary
     * 
     */

    private String addPri;

    /**
     * 
     * the address city
     * 
     */

    private String addCity;

    /**
     * 
     * the address state
     * 
     */

    private String addState;

    /**
     * 
     * the address zip code
     * 
     */

    private String addZip;

    /**
     * 
     * the address country
     * 
     */

    private String addCountry;

    /**
     * 
     * the address county
     * 
     */

    private String addCounty;

    /**
     * 
     * the address effective date
     * 
     */

    private String addEffDate;

    /**
     * 
     * the address phone
     * 
     */

    private String addPhone;

    /**
     * 
     * the address phone extension
     * 
     */

    private String addPhoneExt;
    
 	/**
     * 
     * the address email
     * 
     */
    
    
    private String addEmail;

    /**
     * 
     * the address pager
     * 
     */

    private String addPager;

    /**
     * 
     * the address pager
     * 
     */

    private String addMobile;

    /**
     * 
     * the address fax
     * 
     */

    private String addFax;

    /* * 
     * TTY
     * */
    private String addTty;

    /* * 
     * URL
     * */
    private String addUrl;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * 
     * 
     * @return Address Id
     */
    public int getAddId()

    {

        return this.addId;

    }

    /**
     * 
     * 
     * @param addId
     *            Address Id
     */
    public void setAddId(int addId)

    {

        this.addId = addId;

    }

    /**
     * 
     * 
     * @return Address Type
     */
    public String getAddType()

    {

        return this.addType;

    }

    /**
     * 
     * 
     * @param addType
     *            Address Type
     */
    public void setAddType(String addType)

    {

        this.addType = addType;

    }

    /**
     * 
     * 
     * @return Primary Address
     */
    public String getAddPri()

    {

        return this.addPri;

    }

    /**
     * 
     * 
     * @param addPri
     *            Primary Address
     */
    public void setAddPri(String addPri)

    {

        this.addPri = addPri;

    }

    /**
     * 
     * 
     * @return City
     */
    public String getAddCity()

    {

        return this.addCity;

    }

    /**
     * 
     * 
     * @param addCity
     *            City
     */
    public void setAddCity(String addCity)

    {

        this.addCity = addCity;

    }

    /**
     * 
     * 
     * @return State
     */
    public String getAddState()

    {

        return this.addState;

    }

    /**
     * 
     * 
     * @param addState
     *            State
     */
    public void setAddState(String addState)

    {

        this.addState = addState;

    }

    /**
     * 
     * 
     * @return Zip
     */
    public String getAddZip()

    {

        return this.addZip;

    }

    /**
     * 
     * 
     * @param addZip
     *            Zip
     */
    public void setAddZip(String addZip)

    {

        this.addZip = addZip;

    }

    public String getAddCountry()

    {

        return this.addCountry;

    }

    public void setAddCountry(String addCountry)

    {

        this.addCountry = addCountry;

    }

    public String getAddCounty()

    {

        return this.addCounty;

    }

    /**
     * 
     * 
     * @param addCounty
     *            Country
     */
    public void setAddCounty(String addCounty)

    {

        this.addCounty = addCounty;

    }

    public String getAddEffDate()

    {

        return this.addEffDate;

    }

    /**
     * 
     * 
     * @param addEffDate
     *            Effective Date of Address
     */
    public void setAddEffDate(String addEffDate)

    {

        this.addEffDate = addEffDate;

    }

    /**
     * 
     * 
     * @return Phone #
     */
    public String getAddPhone()

    {

        return this.addPhone;

    }

    /**
     * 
     * 
     * @param addPhone
     *            Phone #
     */
    public void setAddPhone(String addPhone)

    {

        this.addPhone = addPhone;

    }

    public String getAddPhoneExt() {
		return this.addPhoneExt;
	}

	public void setAddPhoneExt(String addPhoneExt) {
		this.addPhoneExt = addPhoneExt;
	}
    
    /**
     * 
     * 
     * @return Email address
     */
    public String getAddEmail()

    {

        return this.addEmail;

    }

    /**
     * 
     * 
     * @param addEmail
     *            Email Address
     */
    public void setAddEmail(String addEmail)

    {

        this.addEmail = addEmail;

    }

    /**
     * 
     * 
     * @return Pager #
     */
    public String getAddPager()

    {

        return this.addPager;

    }

    /**
     * 
     * 
     * @param addPager
     *            Pager #
     */
    public void setAddPager(String addPager)

    {

        this.addPager = addPager;

    }

    /**
     * 
     * 
     * @return Mobile #
     */
    public String getAddMobile()

    {

        return this.addMobile;

    }

    /**
     * 
     * 
     * @param addMobile
     *            Mobile #
     */
    public void setAddMobile(String addMobile)

    {

        this.addMobile = addMobile;

    }

    /**
     * 
     * 
     * @return Fax #
     */
    public String getAddFax()

    {

        return this.addFax;

    }

    /**
     * 
     * 
     * @param addFax
     *            Fax #
     */
    public void setAddFax(String addFax)

    {

        this.addFax = addFax;

    }

    /**
     * @return TTY
     */
    public String getAddTty() {
        return this.addTty;
    }
    /**
     * @param addTty
     *    TTY specified as part of address
     */
    public void setAddTty(String addTty) {
        this.addTty = addTty;
    }
    
    /**
     * @return URL
     */
    public String getAddUrl() {
        return this.addUrl;
    }
    /**
     * @param addUrl
     *    URL specified as part of address
     */
    public void setAddUrl(String addUrl) {
        this.addUrl = addUrl;
    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    // END OF GETTER SETTER METHODS

    /**
     * Defines an AddressJB object with the specified Address Id
     * 
     * @param addId
     *            Address Id
     */
    public AddressJB(int addId)

    {

        setAddId(addId);

        Rlog.debug("address", "Constructor of AddressJB with addressId ");

    }

    /**
     * Defines an AddressJB object with default values
     * 
     */
    public AddressJB()

    {

        Rlog.debug("address", "Blank constructor of AddressJB");

    };

    /**
     * Defines an AddressJB object with the specified Address details
     * 
     * 
     * @param addId
     *            Address Id
     * @param addType
     *            Address Type
     * @param addPri
     *            Primary Address
     * @param addCity
     *            City
     * @param addState
     *            State
     * @param addZip
     *            Zip
     * @param addCountry
     *            Country
     * @param addCounty
     *            County
     * @param addEffDate
     *            Effective Date of Address
     * @param addPhone
     *            Phone #
     * @param addEmail
     *            Email Address
     * @param addPager
     *            Pager #
     * @param addMobile
     *            Mobile #
     * @param addFax
     *            Fax #
     */
    public AddressJB(int addId, String addType, String addPri, String addCity,

    String addState, String addZip, String addCountry, String addCounty,

    String addEffDate, String addPhone, String addPhoneExt, String addEmail, String addPager,

    String addMobile, String addFax, String addTty, String addUrl,
    
    String creator, String modifiedBy, String ipAdd)

    {

        setAddId(addId);

        setAddType(addType);

        setAddPri(addPri);

        setAddCity(addCity);

        setAddState(addState);

        setAddZip(addZip);

        setAddCountry(addCountry);

        setAddCounty(addCounty);

        setAddEffDate(addEffDate);

        setAddPhone(addPhone);
        
        setAddPhoneExt(addPhoneExt);
        
        setAddEmail(addEmail);

        setAddPager(addPager);

        setAddMobile(addMobile);

        setAddFax(addFax);
        
        setAddTty(addTty);
        
        setAddUrl(addUrl);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);

        Rlog.debug("address", "Full arguments constructor of AddressJB");

    }

    /**
     * 
     * 
     * @return AddressBean, an object which holds the address details
     */
    public AddressBean getAddressDetails()

    {

        AddressBean addsk = null;

        try {

            // Context initial = new InitialContext();

            // AddressAgentHome addressAgentHome =
            // (AddressAgentHome)PortableRemoteObject.narrow(initial.lookup("AddressAgentBean"),
            // AddressAgentHome.class);

            AddressAgentRObj addressAgentRObj = EJBUtil.getAddressAgentHome();
            addsk = addressAgentRObj.getAddressDetails(this.addId);

            Rlog
                    .debug("address",
                            "AddressJB.getAddressDetails() AddressStateKeeper "
                                    + addsk);

        } catch (Exception e) {

            Rlog.fatal("address", "Error in getAddressDetails() in AddressJB "
                    + e);

        }
        if (addsk != null) {
            this.addId = addsk.getAddId();
            this.addType = addsk.getAddType();
            this.addPri = addsk.getAddPri();
            this.addCity = addsk.getAddCity();
            this.addState = addsk.getAddState();
            this.addZip = addsk.getAddZip();
            this.addCountry = addsk.getAddCountry();
            this.addCounty = addsk.getAddCounty();
            this.addEffDate = addsk.getAddEffDate();
            this.addPhone = addsk.getAddPhone();
            this.addPhoneExt = addsk.getAddPhoneExt();
            this.addEmail = addsk.getAddEmail();
            this.addPager = addsk.getAddPager();
            this.addMobile = addsk.getAddMobile();
            this.addFax = addsk.getAddFax();
            this.addTty = addsk.getAddTty();
            this.addUrl = addsk.getAddUrl();
            this.creator = addsk.getCreator();
            this.modifiedBy = addsk.getModifiedBy();
            this.ipAdd = addsk.getIpAdd();
        }

        return addsk;

    }

    /**
     * 
     * 
     */
    public void setAddressDetails()

    {

        try {

            // Context initial = new InitialContext();

            AddressAgentRObj addressAgentRObj = EJBUtil.getAddressAgentHome();
            this.setAddId(addressAgentRObj.setAddressDetails(this
                    .createAddressStateKeeper()));

            Rlog.debug("address", "AddressJB.setAddressDetails()");

        } catch (Exception e) {

            Rlog.fatal("address", "Error in setAddressDetails() in AddressJB "
                    + e);

        }

    }

    /**
     * Updates address details
     * 
     * @return 0 incase of successful updation and -2 in case of error
     */
    public int updateAddress() {

        int output;

        try {

            AddressAgentRObj addressAgentRObj = EJBUtil.getAddressAgentHome();
            output = addressAgentRObj.updateAddress(this
                    .createAddressStateKeeper());

        } catch (Exception e) {

            Rlog.fatal("address",
                    "EXCEPTION IN SETTING ADDRESS DETAILS TO  SESSION BEAN "
                            + e);
            return -2;

        }

        return output;

    }

    /**
     * 
     * 
     * @return AddressBean, an object which holds the address details
     */
    public AddressBean createAddressStateKeeper()

    {

        Rlog.debug("address", "AddressJB.createAddressStateKeeper ");

        return new AddressBean(this.addId, this.addType, this.addPri,

        this.addCity, this.addState, this.addZip, this.addCountry,

        this.addCounty, this.addEffDate, this.addPhone, this.addPhoneExt, this.addEmail,

        this.addPager, this.addMobile, this.addFax, this.addTty, this.addUrl, 
        
        this.creator, this.modifiedBy, this.ipAdd);

    }

    /*
     * 
     * generates a String of XML for the address details entry form.<br><br>
     * 
     * 
     * 
     * 
     * 
     * 
     * public String toXML() {
     * 
     * Rlog.debug("address","AddressJB.toXML()");
     * 
     * return new String ("<?xml version=\"1.0\"?>" + "<?cocoon-process
     * type=\"xslt\"?>" + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\"
     * type=\"text/xsl\"?>" + "<?xml-stylesheet
     * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" +
     * 
     * //to be done "<form action=\"addresssummary.jsp\">" + "<head>" +
     * 
     * //to be done "<title>Address Summary </title>" + "</head>" + "<input
     * type=\"hidden\" name=\"addId\" value=\"" + this.getAddId() + "\"
     * size=\"10\"/>" + "<input type=\"text\" name=\"addType\" value=\"" +
     * this.getAddType() + "\" size=\"10\"/>" + "<input type=\"text\" name=\"
     * addPri\" value=\"" + this.getAddPri() + "\" size=\"50\"/>" + "<input
     * type=\"text\" name=\" addCity\" value=\"" + this.getAddCity()+ "\"
     * size=\"30\"/>" + "<input type=\"text\" name=\" addState\" value=\"" +
     * this.getAddState() + "\" size=\"30\"/>" + "<input type=\"text\" name=\"
     * addZip\" value=\"" + this.getAddZip() + "\" size=\"15\"/>" + "<input
     * type=\"text\" name=\" addCountry\" value=\"" + this.getAddCountry()+ "\"
     * size=\"30\"/>" + "<input type=\"text\" name=\" addCounty\" value=\"" +
     * this. getAddCounty()+ "\" size=\"30\"/>" + "<input type=\"text\" name=\"
     * addEffDate\" value=\"" + this.getAddEffDate()+ "\" size=\"10\"/>" + "<input
     * type=\"text\" name=\" addPhone\" value=\"" + this.getAddPhone() + "\"
     * size=\"100\"/>" + "<input type=\"text\" name=\" addEmail\" value=\"" +
     * this.getAddEmail() + "\" size=\"100\"/>" + "<input type=\"text\" name=\"
     * addPager\" value=\"" + this.getAddPager() + "\" size=\"100\"/>" + "<input
     * type=\"text\" name=\" addMobile\" value=\"" + this.getAddMobile() + "\"
     * size=\"100\"/>" + "<input type=\"text\" name=\" addFax\" value=\"" +
     * this.getAddFax() + "\" size=\"100\"/>" + "</form>" );
     * 
     * 
     * 
     * }//end of method
     * 
     */

}// end of class

