/**
 * Classname : FormQueryJB
 *
 * Version information: 1.0
 *
 * Date: 01/03/2005
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.formQuery;

/* IMPORT STATEMENTS */

import java.util.ArrayList;

import com.velos.eres.business.common.FormQueryDao;
import com.velos.eres.business.formQuery.impl.FormQueryBean;
import com.velos.eres.service.formQueryAgent.FormQueryAgentRObj;
import com.velos.eres.service.formQueryAgent.impl.FormQueryAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for FormQuery
 *
 * @author Anu Khanna
 * @version 1.0 01/03/2005
 */

public class FormQueryJB {

    /**
     * The primary key:
     */

    private int formQueryId;

    /**
     * query module id
     */
    private String queryModuleId;

    /**
     * query module linked to id from codelst
     */
    private String queryModuleLnkTo;

    /**
     * field id
     */

    private String fieldId;

    /**
     * query name
     */
    private String queryName;

    /**
     * query desc
     */

    private String queryDesc;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    /**
     * Get the Form Query ID
     *
     * @return int formQuery id
     */
    public int getFormQueryId() {
        return this.formQueryId;
    }

    /**
     * sets FormQuery ID
     *
     * @param formQueryId
     */
    public void setFormQueryId(int formQueryId) {
        this.formQueryId = formQueryId;
    }

    /**
     * Get query module id
     *
     * @return String queryModuleId
     */

    public String getQueryModuleId() {
        return this.queryModuleId;
    }

    /**
     * sets queryModuleId
     *
     * @param queryModuleId
     */
    public void setQueryModuleId(String queryModuleId) {
        this.queryModuleId = queryModuleId;
    }

    /**
     * Get query module linked to
     *
     * @return String queryModuleLnkTo
     */

    public String getQueryModuleLnkTo() {
        return this.queryModuleLnkTo;
    }

    /**
     * sets queryModuleLnkTo
     *
     * @param queryModuleLnkTo
     */
    public void setQueryModuleLnkTo(String queryModuleLnkTo) {
        this.queryModuleLnkTo = queryModuleLnkTo;
    }

    /**
     * Get field id
     *
     * @return String fieldId
     */

    public String getFieldId() {
        return this.fieldId;
    }

    /**
     * sets field id
     *
     * @param fieldId
     */
    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    /**
     * Get query name
     *
     * @return String queryName
     */

    public String getQueryName() {
        return this.queryName;
    }

    /**
     * sets query name
     *
     * @param queryName
     */

    public void setQueryName(String queryName) {
        this.queryName = queryName;

    }

    /**
     * Get query description
     *
     * @return String queryDesc
     */

    public String getQueryDesc() {
        return this.queryDesc;
    }

    /**
     * sets query Description
     *
     * @param queryDesc
     */

    public void setQueryDesc(String queryDesc) {
        this.queryDesc = queryDesc;

    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     *
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     *
     * @param formQueryId:
     *            Unique Id constructor
     *
     */

    public FormQueryJB(int formQueryId) {
        setFormQueryId(formQueryId);
    }

    /**
     * Default Constructor
     */

    public FormQueryJB() {
        Rlog.debug("formQuery", "FormQueryJB.FormQueryJB() ");
    }

    public FormQueryJB(int formQueryId, String queryModuleId,
            String queryModuleLnkTo, String fieldId, String queryName,
            String queryDesc, String creator, String modifiedBy, String ipAdd) {
        setFormQueryId(formQueryId);
        setQueryModuleId(queryModuleId);
        setQueryModuleLnkTo(queryModuleLnkTo);
        setFieldId(fieldId);
        setQueryName(queryName);
        setQueryDesc(queryDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getFormQueryDetails of FormQueryId Session Bean: FormQueryAgentBean
     *
     * @return FormQueryStatKeeper
     * @see FormQueryAgentBean
     */
    public FormQueryBean getFormQueryDetails() {
        FormQueryBean formQuerysk = null;
        try {
            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            formQuerysk = formQueryAgentRObj
                    .getFormQueryDetails(this.formQueryId);
            Rlog.debug("formQuery",
                    "FormQueryJB.getFormQueryDetails() FormQueryStateKeeper "
                            + formQuerysk);
        } catch (Exception e) {
            Rlog.fatal("formQuery", "Error in FormQuery getFormQueryDetails"
                    + e);
        }

        if (formQuerysk != null) {
            setFormQueryId(formQuerysk.getFormQueryId());
            setQueryModuleId(formQuerysk.getQueryModuleId());
            setQueryModuleLnkTo(formQuerysk.getQueryModuleLnkTo());
            setFieldId(formQuerysk.getFieldId());
            setQueryName(formQuerysk.getQueryName());
            setQueryDesc(formQuerysk.getQueryDesc());
            setCreator(formQuerysk.getCreator());
            setModifiedBy(formQuerysk.getModifiedBy());
            setIpAdd(formQuerysk.getIpAdd());

        }

        return formQuerysk;

    }

    /**
     * Calls setFormQueryDetails() of Form Query Session Bean:
     * FormQueryAgentBean
     *
     */

    public int setFormQueryDetails() {
        int toReturn;
        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            FormQueryBean tempStateKeeper = new FormQueryBean();

            tempStateKeeper = this.createFormQueryStateKeeper();

            toReturn = formQueryAgentRObj.setFormQueryDetails(tempStateKeeper);
            this.setFormQueryId(toReturn);

            Rlog.debug("formQuery", "FormQueryJB.setFormQueryDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("formQuery",
                    "Error in setFormQueryDetails() in FormQueryJB " + e);
            return -2;
        }
    }

    /**
     *
     * @return a statekeeper object for the FormQuery Record with the current
     *         values of the bean
     */
    public FormQueryBean createFormQueryStateKeeper() {

        return new FormQueryBean(formQueryId, queryModuleId, queryModuleLnkTo,
                fieldId, queryName, queryDesc, creator, modifiedBy, ipAdd);

    }

    /**
     * Calls updateFormQuery() of FormQuery Session Bean: FormQueryAgentBean
     *
     * @return The status as an integer
     */
    public int updateFormQuery() {
        int output;
        try {
            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();
            output = formQueryAgentRObj.updateFormQuery(this
                    .createFormQueryStateKeeper());
        } catch (Exception e) {
            Rlog.debug("formQuery",
                    "EXCEPTION IN SETTING FormQuery DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeFormQuery() {

        int output;

        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            output = formQueryAgentRObj.removeFormQuery(this.formQueryId);
            return output;

        } catch (Exception e) {
            Rlog.fatal("formQuery", "in FormQueryJB removeFormQuery() method"
                    + e);
            return -1;
        }

    }

    //KM-#4016
    public FormQueryDao insertIntoFormQuery(int filledFormId, int formId, int calledFrom,
            int queryType, String[] queryTypeIds, String[] fldSysIds,
            String[] notes, String[] status, String[] comments,
            String[] modes, String[] pkQueryStats, int creator, String ipAdd) {
        FormQueryDao formQueryDao = new FormQueryDao();
        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            formQueryDao = formQueryAgentRObj.insertIntoFormQuery(filledFormId, formId,
                    calledFrom, queryType, queryTypeIds, fldSysIds, notes, 
                    status, comments, modes, pkQueryStats, creator, ipAdd);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery", "insertIntoFormQuery in FormQueryJB " + e);
        }
        return formQueryDao;
    }

    public FormQueryDao getQueriesForForm(int formId, int queryModuleId, int moduleLinkedTo) {
        FormQueryDao formQueryDao = new FormQueryDao();
        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();
            formQueryDao = formQueryAgentRObj.getQueriesForForm(formId, queryModuleId,
                    moduleLinkedTo);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery", "insertIntoFormQuery in FormQueryJB " + e);
        }
        return formQueryDao;
    }

    public FormQueryDao getQueryHistoryForField(int formQueryId) {
        FormQueryDao formQueryDao = new FormQueryDao();
        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            formQueryDao = formQueryAgentRObj
                    .getQueryHistoryForField(formQueryId);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery", "getQueryHistoryForField in FormQueryJB "
                    + e);
        }
        return formQueryDao;
    }

    public int insertQueryResponse(int filledformId, int calledFrom, int fldId,
            int queryType, int queryTypeId, int queryStatusId, String comments,
            String enteredOn, int enteredBy, int creator, String ipAdd)

    {

        int ret = 0;
        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            ret = formQueryAgentRObj.insertQueryResponse(filledformId,
                    calledFrom, fldId, queryType, queryTypeId, queryStatusId,
                    comments, enteredOn, enteredBy, creator, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("formQuery", "getQueryHistoryForField in FormQueryJB "
                    + e);
        }
        return ret;
    }

    public FormQueryDao getSystemFormQueryStat(int filledFormId,
            String fldSysIds) {
        FormQueryDao formQueryDao = new FormQueryDao();
        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            formQueryDao = formQueryAgentRObj.getSystemFormQueryStat(
                    filledFormId, fldSysIds);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery", "getSystemFormQueryStat in FormQueryJB "
                    + e);
        }
        return formQueryDao;
    }
    
    public FormQueryDao getSystemFormQueryStat(int filledFormId,
            ArrayList fldSysIds, ArrayList fldReasons) {
        FormQueryDao formQueryDao = new FormQueryDao();
        try {

            FormQueryAgentRObj formQueryAgentRObj = EJBUtil
                    .getFormQueryAgentHome();

            formQueryDao = formQueryAgentRObj.getSystemFormQueryStat(
                    filledFormId, fldSysIds, fldReasons);
            return formQueryDao;
        } catch (Exception e) {
            Rlog.fatal("formQuery", "getSystemFormQueryStat in FormQueryJB "
                    + e);
        }
        return formQueryDao;
    }

    public FormQueryDao getSystemQueryNotes(int formQueryId) {
        FormQueryDao formQueryDao = null;
        try {
            FormQueryAgentRObj formQueryAgentRObj = EJBUtil.getFormQueryAgentHome();
            formQueryDao = formQueryAgentRObj.getSystemQueryNotes(formQueryId);
        } catch (Exception e) {
            Rlog.fatal("formQuery", "getSystemFormQueryStat in FormQueryJB "
                    + e);
        }
        if (formQueryDao == null) { formQueryDao = new FormQueryDao(); }
        return formQueryDao;
    }
}
