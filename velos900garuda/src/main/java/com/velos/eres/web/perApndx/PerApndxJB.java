/*
 * Classname : PerApndx JB.java
 * @author Sonika Talwar
 * @Version 1.0
 * Date Aug 28, 2002  
 */

package com.velos.eres.web.perApndx;

import java.util.Hashtable;

import com.velos.eres.business.common.PerApndxDao;
import com.velos.eres.business.perApndx.impl.PerApndxBean;
import com.velos.eres.service.perApndxAgent.PerApndxAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the Patient Appendix
 */

public class PerApndxJB {

    /**
     * the perApndxId
     */
    private int perApndxId;

    /**
     * the patient Id
     */
    private String perApndxPerId;

    /**
     * Appendix date
     */
    private String perApndxDate;

    /**
     * desc
     */
    private String perApndxDesc;

    /**
     * uri
     */
    private String perApndxUri;

    /**
     * type
     */
    private String perApndxType;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public int getPerApndxId() {
        return perApndxId;
    }

    public void setPerApndxId(int id) {
        perApndxId = id;
    }

    public String getPerApndxPerId() {
        return this.perApndxPerId;
    }

    public void setPerApndxPerId(String perApndxPerId) {
        this.perApndxPerId = perApndxPerId;
    }

    public String getPerApndxDate() {
        return this.perApndxDate;
    }

    public void setPerApndxDate(String perApndxDate) {
        this.perApndxDate = perApndxDate;
    }

    public String getPerApndxDesc() {
        return this.perApndxDesc;
    }

    public void setPerApndxDesc(String perApndxDesc) {
        this.perApndxDesc = perApndxDesc;
    }

    public String getPerApndxUri() {
        return this.perApndxUri;
    }

    public void setPerApndxUri(String perApndxUri) {
        this.perApndxUri = perApndxUri;
    }

    public String getPerApndxType() {
        return this.perApndxType;
    }

    public void setPerApndxType(String perApndxType) {
        this.perApndxType = perApndxType;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    // CONSTRUCTOR TO CREATE patient appendix OBJ WITH ID

    public PerApndxJB(String id) {
        Rlog.debug("perapndx", "PerApndx JB Create with id");
        setPerApndxId(EJBUtil.stringToNum(id));
    }

    // DEFAULT CONSTRUCTOR

    public PerApndxJB() {

        Rlog.debug("perapndx", "PerApndx JB  : Default Constructor");

    }

    public PerApndxBean getPerApndxDetails() {
        PerApndxBean pask = null;

        try {

            PerApndxAgentRObj perApndxRObj = EJBUtil.getPerApndxAgentHome();
            pask = perApndxRObj.getPerApndxDetails(this.perApndxId);

        } catch (Exception e) {
            Rlog.debug("perapndx",
                    "Exception in the function getPerApndxDetails");
        }
        if (pask != null) {
            perApndxId = pask.getId();
            perApndxPerId = pask.getFkPer();
            perApndxDate = pask.getPerApndxDate();
            perApndxDesc = pask.getPerApndxDesc();
            perApndxUri = pask.getPerApndxUri();
            perApndxType = pask.getPerApndxType();
            creator = pask.getCreator();
            modifiedBy = pask.getModifiedBy();
            ipAdd = pask.getIpAdd();
        }
        return pask;
    }

    public int setPerApndxDetails() {
        int perApndxId = 0;
        try {

            PerApndxAgentRObj perApndxAgentRobj = EJBUtil
                    .getPerApndxAgentHome();
            perApndxId = perApndxAgentRobj.setPerApndxDetails(this
                    .createPerApndxStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Exception in setPerApndxDetails()" + e);
            return -1;
        }

        return perApndxId;
    }

    public int removePerApndx(int perApndxId) {

        int output = 0;

        try {

            PerApndxAgentRObj perApndxAgentRobj = EJBUtil
                    .getPerApndxAgentHome();
            output = perApndxAgentRobj.removePerApndx(perApndxId);

        } catch (Exception e) {
            Rlog.fatal("perapndx", "Exception in removePerApndx");
            return -1;
        }
        return output;
    }
 // Overloaded for INF-18183 AGodara
    public int removePerApndx(int perApndxId,Hashtable<String, String> auditInfo) {

        int output = 0;
        try {
            PerApndxAgentRObj perApndxAgentRobj = EJBUtil
                    .getPerApndxAgentHome();
            output = perApndxAgentRobj.removePerApndx(perApndxId,auditInfo);
        } catch (Exception e) {
            Rlog.fatal("perapndx", "removePerApndx(int perApndxId,Hashtable<String, String> auditInfo)");
            return -1;
        }
        return output;
    }

    public int updatePerApndx() {
        int output = 0;
        try {

            PerApndxAgentRObj perApndxAgentRObj = EJBUtil
                    .getPerApndxAgentHome();
            output = perApndxAgentRObj.updatePerApndx(this
                    .createPerApndxStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Exception in updating PerApndxDetails()");
            return -2;
        }

        return output;
    }

    public PerApndxBean createPerApndxStateKeeper() {

        return new PerApndxBean(perApndxId,this.perApndxPerId, perApndxDate, perApndxDesc,
                perApndxUri, perApndxType, creator, modifiedBy, ipAdd);
    }

    public PerApndxDao getPerApndxUris(int patId) {
        PerApndxDao perApndxDao = new PerApndxDao();
        try {

            PerApndxAgentRObj perApndxAgent = EJBUtil.getPerApndxAgentHome();
            Rlog.debug("perapndx", "PerApndxJB.getPerApndxUris after remote");
            perApndxDao = perApndxAgent.getPerApndxUris(patId);
            Rlog.debug("perapndx", "PerApndxJB.getPerApndxUris after Dao");
            return perApndxDao;
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Error in getPerApndxUris in PerApndxJB "
                    + e);
        }
        return perApndxDao;
    }

    public PerApndxDao getPerApndxFiles(int patId) {
        PerApndxDao perApndxDao = new PerApndxDao();
        try {

            PerApndxAgentRObj perApndxAgent = EJBUtil.getPerApndxAgentHome();
            Rlog.debug("perapndx", "PerApndxJB.getPerApndxFiles after remote");
            perApndxDao = perApndxAgent.getPerApndxFiles(patId);
            Rlog.debug("perapndx", "PerApndxJB.getPerApndxFiles after Dao");
            return perApndxDao;
        } catch (Exception e) {
            Rlog.fatal("perapndx", "Error in getPerApndxFiles in PerApndxJB "
                    + e);
        }
        return perApndxDao;
    }

    /**
     * 
     * 
     * generates a String of XML for the customer details entry form.<br>
     * <br>
     * 
     * 
     * Note that it is planned to encapsulate this detail in another object.
     * 
     * 
     */

    public String toXML()

    {
        return null;
    }

    // end of method

}// end of class
