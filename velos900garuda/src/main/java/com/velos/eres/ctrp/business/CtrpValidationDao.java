package com.velos.eres.ctrp.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.velos.eres.business.address.impl.AddressBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.business.site.impl.SiteBean;
//import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.user.impl.UserBean;
//import com.velos.eres.ctrp.service.CtrpDraftAgent;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
//import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.LC;
import com.velos.eres.service.util.MC;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.VelosResourceBundle;

public class CtrpValidationDao implements Serializable {

	Integer ctrpDraftId;
	LinkedHashMap<String,ArrayList<CtrpValidationJB>> validationMsgMap;
	
	public static final String TYPE_TEXT = "text";
	public static final String TYPE_RADIO = "radio";
	public static final String TYPE_SELECT = "select";
	public static final String TYPE_TEXT_AREA = "textArea";
	
	public static final String asterisk = "<FONT class='Mandatory' color='red'>* </FONT>";
	public static final String IMG_VALID = "<img src='../images/jpg/Done.gif'/>";
	public static final String IMG_INVALID = "<img src='../images/jpg/delete_pg.png'/>";
	
	// TO-DO isDraftValid=true
	public static boolean isDraftValid=false;
	
	private String getValueInvalidImg(){
		isDraftValid=false;
		return IMG_INVALID;
	}
	
	
	CtrpValidationJB validationObj = null;

	public Integer getCtrpDraftId() {
		return ctrpDraftId;
	}
	public void setCtrpDraftId(Integer ctrpDraftId) {
		this.ctrpDraftId = ctrpDraftId;
	}

	public LinkedHashMap<String, ArrayList<CtrpValidationJB>> getValidationMsgMap() {
		return validationMsgMap;
	}
	
	public void setValidationMsgMap(LinkedHashMap<String, ArrayList<CtrpValidationJB>> validationMsgMap) {
		this.validationMsgMap = validationMsgMap;
	}
	
	public void setValidationValues(String secName, ArrayList<CtrpValidationJB> secValues) {
		validationMsgMap.put(secName, secValues);
	}
	

	/**
	 * Constructor for the CtrpValidationDao object
	 */
	public CtrpValidationDao() {
		ctrpDraftId = new Integer(0);
		validationMsgMap = new LinkedHashMap<String,ArrayList<CtrpValidationJB>>();
	}

	/*
	 * Method: runCtrpDraftValidation 
	 * Functionality: Looks at the validity of
	 * the CTRP draft and then runs validations. Depending upon 'Trial
	 * Submission Category' calls corresponding functions
	 */
	public Integer runCtrpDraftValidation() {
		
		Integer output = -1;
		Integer ctrpDraftId = this.getCtrpDraftId();
		
		if (ctrpDraftId == null || ctrpDraftId < 1) {
			return output;
		}
		try {
			//CtrpDraftAgent ctrpDraftAgent = EJBUtil.getCtrpDraftAgentHome();
			//CtrpDraftBean draftBean = ctrpDraftAgent.findByDraftId(ctrpDraftId);
			//Integer studyId = draftBean.getFkStudy();
			//Integer studySubmCategory = draftBean.getCtrpDraftType();// 1-Industrial,0-Non-Industrial
			
			/*if (draftBean.getId() < 1) {
			throw (new Exception("Serious Error Is Occured: Draft not present"));	
			}
			if (studyId < 1) {
				throw (new Exception("Serious Error Is Occured: Trail/Study not present"));
			}

			if (studySubmCategory == null || studySubmCategory < 0) {
				throw (new Exception("Serious Error Is Occured: submission category not present"));
			}

			switch (studySubmCategory) {
			case 0:					// Non-Industrial Draft
				output = runCtrpDraftValidationNonIndustrial(draftBean);
				break;
			case 1:					// Industrial Draft
				output = runCtrpDraftValidationIndustrial(draftBean);
				break;
			}*/
		} catch (Exception e) {
			Rlog.fatal("ctrp","Exception in CtrpValidationDao.runCtrpDraftValidation: "+ e);
			output = -1;
		}
		return output;
	}

	/*
	 * Method: runCtrpDraftValidationIndustrial Parameter: CtrpDraftBean
	 * Functionality: Runs validations for draft where 'Trial Submission
	 * Category' = 'Industrial'
	 */
	private Integer runCtrpDraftValidationIndustrial(CtrpDraftBean draftBean) {
		Integer output = 0;
		Integer studyId = draftBean.getFkStudy();
		try {
			//StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
			//StudyBean studyBean = studyAgent.getStudyDetails(studyId);

			validateSectTrailSubmType(draftBean);
			validateSectTrialIdentifiers(draftBean, studyId);
			//validateSectTrialDetails(draftBean, studyBean);
			validateSectStatusDates(draftBean);
		} catch (Exception e) {
			Rlog.fatal("ctrp","Exception in CtrpValidationDao.runCtrpDraftValidationNonIndustrial: "+ e);
			output = -1;
		}
		return output;
	}

	/*
	 * Method: runCtrpDraftValidationNonIndustrial Parameter: CtrpDraftBean
	 * Functionality: Runs validations for draft where 'Trial Submission
	 * Category' = 'Non Industrial'
	 */
	private Integer runCtrpDraftValidationNonIndustrial(CtrpDraftBean draftBean) {

		Integer output = 0;
		Integer studyId = draftBean.getFkStudy();
		//StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
		//StudyBean studyBean = studyAgent.getStudyDetails(studyId);
		try {

			validateSectTrailSubmType(draftBean);
			validateSectTrialIdentifiers(draftBean, studyId);
			//validateSectTrialDetails(draftBean, studyBean);
			if (draftBean.getDraftXMLReqd()!= null && draftBean.getDraftXMLReqd()==1) {	
				validateRegulatoryInfoCtrpDraft(draftBean);
			}
			validateSectStatusDates(draftBean);

		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpValidationDao.runCtrpDraftValidationNonIndustrial: "
							+ e);
			output = -1;
		}
		return output;
	}

	
	
	// Validation for CTRP draft section "Trial Submission Type" 
	private void validateSectTrailSubmType(CtrpDraftBean draftBean) {
		
		ArrayList<CtrpValidationJB> validationMsgSubmType = new ArrayList<CtrpValidationJB>();
		validationObj = null;
		
		Integer studySubmCategory = draftBean.getCtrpDraftType();// 1-Industrial,0-Non-Industrial
		Integer fkStudySubmType = draftBean.getFkCodelstSubmissionType();// O,A,U
		CodeDao cdDao = new CodeDao();

		String studySubmSubType = cdDao.getCodeSubtype(fkStudySubmType);
		Integer draftXMLReqd = draftBean.getDraftXMLReqd();// 1-Yes, 0-No
		String amendNumber = draftBean.getAmendNumber();
		String amendDate = ""+ draftBean.getAmendDate();

		
		if (fkStudySubmType == null || fkStudySubmType < 1) {
			validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftSectSubmType+asterisk,"",TYPE_SELECT,getValueInvalidImg()));
		} else if (studySubmSubType != null){
			validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftSectSubmType+asterisk,studySubmSubType+"",TYPE_SELECT,IMG_VALID));
		}
		
		
		// For Non-Industrial Trials
		if (studySubmCategory == 0) {

			if ("A".equals(studySubmSubType) || "O".equals(studySubmSubType)) {
					if (draftXMLReqd == null || draftXMLReqd < 0) {
						validationMsgSubmType.add( new CtrpValidationJB(LC.CTRP_DraftXMLRequired+asterisk,"",TYPE_RADIO,getValueInvalidImg()));
					}else{
						validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftXMLRequired+asterisk,draftXMLReqd+"",TYPE_RADIO,IMG_VALID));
					}
			}
			if ("A".equals(studySubmSubType)) {
				if (amendNumber == null || amendNumber.length() == 0) {
					validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftAmendmentNumber+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftAmendmentNumber+asterisk,(amendNumber+""),TYPE_TEXT,IMG_VALID));
				}
				
				if (amendDate == null || amendDate.length() == 0) {
					validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftAmendmentDate+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftAmendmentDate+asterisk,(amendDate+""),TYPE_TEXT,IMG_VALID));
				}
			}else if("O".equals(studySubmSubType)){
				validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftAmendmentNumber+asterisk,(amendNumber+""),TYPE_TEXT,IMG_VALID));
				validationMsgSubmType.add(new CtrpValidationJB(LC.CTRP_DraftAmendmentDate+asterisk,(amendDate+""),TYPE_TEXT,IMG_VALID));
			}
			
			
		} else if (studySubmCategory == 1) { // For Industrial Trials
			
			Integer userId = draftBean.getFkTrialOwner();
			
			validationMsgSubmType.add(new CtrpValidationJB("Trail Owner :"+asterisk,"",TYPE_TEXT,IMG_VALID));
			
			if (userId == null || userId < 1) {
				validationMsgSubmType.add(new CtrpValidationJB(LC.L_First_Name+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				validationMsgSubmType.add(new CtrpValidationJB(LC.L_Last_Name+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				validationMsgSubmType.add(new CtrpValidationJB(LC.L_Email_Addr+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			} else {
				
				UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
				AddressAgentRObj addressAgent = EJBUtil.getAddressAgentHome();
				
				UserBean userBean = userAgent.getUserDetails(userId);
				AddressBean addressBean = addressAgent.getAddressDetails(StringUtil.stringToInteger(userBean.getUserPerAddressId()));
				
				String userFirstName = userBean.getUserFirstName();
				String userLastName = userBean.getUserLastName();
				String userEmail = addressBean.getAddEmail();
				
				validationMsgSubmType.add(new CtrpValidationJB("Trail Owner :"+asterisk,"",TYPE_TEXT,IMG_VALID));
				
				if (userFirstName != null && userFirstName.length() <= 0) {
					validationMsgSubmType.add(new CtrpValidationJB(LC.L_First_Name+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgSubmType.add(new CtrpValidationJB(LC.L_First_Name+asterisk,userFirstName,TYPE_TEXT,IMG_VALID));
				}
				
				if (userLastName != null && userLastName.length() <= 0) {
					validationMsgSubmType.add(new CtrpValidationJB(LC.L_Last_Name+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgSubmType.add(new CtrpValidationJB(LC.L_Last_Name+asterisk,userLastName,TYPE_TEXT,IMG_VALID));
				}
				
				if (userEmail != null && userEmail.length() <= 0) {
					validationMsgSubmType.add(new CtrpValidationJB(LC.L_Email_Addr+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgSubmType.add(new CtrpValidationJB(LC.L_Email_Addr+asterisk,userEmail,TYPE_TEXT,IMG_VALID));
				}
			}

		}
		//Addition of validation message to HashMap with key as a Section Name 
		setValidationValues(LC.CTRP_DraftSectSubmType,validationMsgSubmType);

	}
	
	
	// Validation for CTRP draft section "Trial Identifiers"
	private void validateSectTrialIdentifiers(CtrpDraftBean draftBean,int studyId) {

		ArrayList<CtrpValidationJB> validationMsgIdentifiers = new ArrayList<CtrpValidationJB>();
		
		CodeDao cdDao = new CodeDao();
		//StudyAgent studyAgent = EJBUtil.getStudyAgentHome();
		//StudyBean studyBean = studyAgent.getStudyDetails(studyId);

		Integer studySubmCategory = draftBean.getCtrpDraftType();// 1-Industrial,0-Non-Industrial
		Integer fkStudySubmType = draftBean.getFkCodelstSubmissionType();// O,A,U
		//String uniqueStudyId = studyBean.getStudyNumber();
		String nciStudyId = (draftBean.getNciStudyId())==null?"":draftBean.getNciStudyId();
		String leadOrgStudyId = draftBean.getLeadOrgStudyId();
		String studySubmSubType = cdDao.getCodeSubtype(fkStudySubmType);
		String nctNumber = (draftBean.getNctNumber())==null?"": draftBean.getNctNumber();
		String otherNumber = (draftBean.getOtherNumber())==null?"":draftBean.getOtherNumber();

		// Unique Trial Identifier
		/*if (uniqueStudyId == null || uniqueStudyId.length() == 0) {
			validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftUniqTrialId+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
		}else{
			validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftUniqTrialId+asterisk,uniqueStudyId,TYPE_TEXT,IMG_VALID));
		}*/

		// Non-Industrial Trails
		if (studySubmCategory == 0) {
			
			if ("A".equals(studySubmSubType)) {
				
				if (nciStudyId.length() == 0) {
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId+asterisk,nciStudyId,TYPE_TEXT,IMG_VALID));
				}
				
				if (leadOrgStudyId == null || leadOrgStudyId.length() == 0) {
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId+asterisk,leadOrgStudyId,TYPE_TEXT,IMG_VALID));
				}
			} else if ("U".equals(studySubmSubType)){
				
				if (nciStudyId.length() == 0) {
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId+asterisk,nciStudyId,TYPE_TEXT,IMG_VALID));
				}
				
				validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId,leadOrgStudyId,TYPE_TEXT,IMG_VALID));
				
			}else if ("O".equals(studySubmSubType)){
				
				validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId,nciStudyId,TYPE_TEXT,IMG_VALID));
				
				if (leadOrgStudyId == null || leadOrgStudyId.length() == 0) {
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId+asterisk,leadOrgStudyId,TYPE_TEXT,IMG_VALID));
				}
			}else{
				//NCI Trial Identifier 	
				validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId,nciStudyId,TYPE_TEXT,IMG_VALID));
				//Lead Organization Trial Identifier
				validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId,leadOrgStudyId,TYPE_TEXT,IMG_VALID));
			}
			
			//NCT Number
			validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCTNumber,nctNumber,TYPE_TEXT,IMG_VALID));
			//Other Identifier
			validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftOtherTrialId,otherNumber,TYPE_TEXT,MC.CTRP_DraftOtherIdInfo+IMG_VALID));
			
				
		}else if (studySubmCategory == 1) { 		// Section - Trial Identifiers Industrial

			
			if ("U".equals(studySubmSubType)) {
				if (nciStudyId.length() == 0) {
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId+asterisk,nciStudyId,TYPE_TEXT,IMG_VALID));
				}
			}else if ("O".equals(studySubmSubType)) {
				validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCITrialId+asterisk,nciStudyId,TYPE_TEXT,IMG_VALID));
			}

			if (leadOrgStudyId == null || leadOrgStudyId.length() == 0) {
				validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftLeadOrgTrialId+asterisk,leadOrgStudyId,TYPE_TEXT,IMG_VALID));
			}

			String submitOrgStudyId = draftBean.getSubmitOrgStudyId();
			if (submitOrgStudyId == null || submitOrgStudyId.length() == 0) {
				validationMsgIdentifiers.add(new CtrpValidationJB("Submitting Organization Local Identifier"+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgIdentifiers.add(new CtrpValidationJB("Submitting Organization Local Identifier"+asterisk,submitOrgStudyId,TYPE_TEXT,IMG_VALID));
			}
			
			validationMsgIdentifiers.add(new CtrpValidationJB(LC.CTRP_DraftNCTNumber,nctNumber,TYPE_TEXT,IMG_VALID));
		}
		
		//Addition of validation message to HashMap with key as a Section Name 
		setValidationValues(LC.CTRP_DraftSectTrialIdentfiers,validationMsgIdentifiers);
	}

	
	
	
	
	// Validation for CTRP draft section "Trial details" 
	/*private void validateSectTrialDetails(CtrpDraftBean draftBean,
			StudyBean studyBean) {
		
		ArrayList<CtrpValidationJB> validationMsgTrialDetails =  new ArrayList<CtrpValidationJB>();
		CodeDao cdDao = new CodeDao();
		
		Integer studySubmCategory = draftBean.getCtrpDraftType();// 1-Industrial,0-Non-Industrial
		Integer fkStudyPhase = draftBean.getFkCodelstPhase();
		String studyTitle = studyBean.getStudyTitle();
		Integer draftPilot = draftBean.getDraftPilot();	
		Integer fkStudySubmType = draftBean.getFkCodelstSubmissionType();
		String studySubmSubType = cdDao.getCodeSubtype(fkStudySubmType);// O,A,U
		Integer studyPurpose = draftBean.getStudyPurpose();
		Integer draftStudyType = draftBean.getStudyType();
		String studyPurposeOther = draftBean.getStudyPurposeOther();
		String nctNumber = draftBean.getNctNumber();
		
		String draftStudyTypeStr="";
		switch (draftStudyType) { 
			case 1:	draftStudyTypeStr  = LC.CTRP_DraftInterventional;
					break;
			case 0: draftStudyTypeStr = LC.CTRP_DraftObservational;
					break;
        }
		
		
		String studyPhaseSubType = (fkStudyPhase == null) ? "" : cdDao
				.getCodeSubtype(fkStudyPhase);
		
		String studyPurposeSubType = (studyPurpose == null) ? "" : cdDao
				.getCodeSubtype(studyPurpose);
		
		String studyPurposeDesc = (studyPurpose == null) ? "" : cdDao.getCodeDescription(studyPurpose);
		
		// Non-Industrial Trails
		if (studySubmCategory == 0) {
			
			if("A".equalsIgnoreCase(studySubmSubType)){
			
				if (studyTitle == null || studyTitle.length() == 0) {
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Title+asterisk,"",TYPE_TEXT_AREA,getValueInvalidImg()));
				}else{
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Title+asterisk,studyTitle,TYPE_TEXT_AREA,IMG_VALID));
				}
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Title,studyTitle,TYPE_TEXT_AREA,IMG_VALID));
			}
			
			
			if (studyPhaseSubType == null || studyPhaseSubType.length()==0) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Phase+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Phase+asterisk,studyPhaseSubType,TYPE_TEXT,IMG_VALID));
			}
			
			if("NA".equalsIgnoreCase(studyPhaseSubType)){
				if (draftPilot == null) {
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPilotTrial+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPilotTrial+asterisk,draftPilot+"",TYPE_RADIO,IMG_VALID));
				}
			}
			
			if (draftStudyType == null ) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftTrialType+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftTrialType+asterisk,draftStudyTypeStr,TYPE_TEXT,IMG_VALID));
			}
			
			if (studyPurpose == null || studyPurpose==0) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPurpose+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPurpose+asterisk,studyPurposeDesc,TYPE_TEXT,IMG_VALID));
			}
			
			
			
		}else if (studySubmCategory == 1) {			// Section - Trial Identifiers Industrial	
			
			Integer interventionType = draftBean.getFkCodelstInterventionType();
					
			String interventionTypeDesc = (interventionType == null) ? "" : cdDao
					.getCodeDescription(interventionType);
			String interventionName = (draftBean.getInterventionName())==null?"":draftBean.getInterventionName();
			
			String diseaseName = (studyBean.getDisSite())==null?"":studyBean.getDisSite();
			//its a CSV so proceed accordingly
			//String diseaseNameDesc = (cdDao.getCodeDescription(StringUtil.stringToInteger(diseaseName)))==null?"":cdDao.getCodeDescription(StringUtil.stringToInteger(diseaseName));
			
			if (studyTitle == null || studyTitle.length() == 0) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Title+asterisk,"",TYPE_TEXT_AREA,getValueInvalidImg()));
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Title+asterisk,studyTitle,TYPE_TEXT_AREA,IMG_VALID));
			}
			
			if (studyPhaseSubType == null || studyPhaseSubType.length()==0) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Phase+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.L_Phase+asterisk,studyPhaseSubType,TYPE_TEXT,IMG_VALID));
			}
			
			if("NA".equalsIgnoreCase(studyPhaseSubType)){
				if (draftPilot == null) {
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPilotTrial+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPilotTrial+asterisk,draftPilot+"",TYPE_RADIO,IMG_VALID));
				}
			}
			
			if (draftStudyType == null ) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftTrialType+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftTrialType+asterisk,draftStudyTypeStr,TYPE_TEXT,IMG_VALID));
			}
		
			
			if (nctNumber == null || nctNumber.length()== 0) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftIntervenType+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			
				if (interventionName==null || interventionName.length()==0) {
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftIntervenName+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftIntervenName+asterisk,interventionName,TYPE_TEXT,IMG_VALID));
				}
				
				if (diseaseName==null || diseaseName.length()==0) {
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftDiseaseName+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftDiseaseName+asterisk,diseaseName,TYPE_TEXT,IMG_VALID));
				}
				if (studyPurpose == null || studyPurpose==0) {
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPurpose+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPurpose+asterisk,studyPurposeDesc,TYPE_TEXT,IMG_VALID));
				}
				
			
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftIntervenType+asterisk,interventionTypeDesc,TYPE_TEXT,IMG_VALID));
			}
			
			validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftIntervenName,interventionName,TYPE_TEXT,IMG_VALID));
			validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftDiseaseName,diseaseName,TYPE_TEXT,IMG_VALID));
			validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPurpose,studyPurposeDesc,TYPE_TEXT,IMG_VALID));
		}
		
		
		
		// validation common to Industrial/Non-Industrial
		if("other".equalsIgnoreCase(studyPurposeSubType)){
			if (studyPurposeOther == null || studyPurposeOther.length()==0) {
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPurposeOther+asterisk,"",TYPE_TEXT_AREA,getValueInvalidImg()));
			}else{
				validationMsgTrialDetails.add(new CtrpValidationJB(LC.CTRP_DraftPurposeOther+asterisk,studyPurposeOther,TYPE_TEXT_AREA,IMG_VALID));
			}
		}
		
		//Addition of validation message to HashMap with key as a Section Name 
		setValidationValues(LC.CTRP_DraftSectTrialDetails,validationMsgTrialDetails);
	//here
		}
	*/
	
	// Validation for CTRP draft section "Regulatory Information" 
	private void validateRegulatoryInfoCtrpDraft(CtrpDraftBean draftBean) {
		CodeDao cd = new CodeDao();
		ArrayList<CtrpValidationJB> validationMsgRegulatoryInfo = new ArrayList<CtrpValidationJB>();
		
		
		Integer oversightCountry = draftBean.getOversightCountry();
		String oversightCountryDesc = (cd.getCodeDescription(oversightCountry)==null)?"":cd.getCodeDescription(oversightCountry);
		
		Integer oversightOrg = draftBean.getOversightOrganization();
		String oversightOrgDesc = (cd.getCodeDescription(oversightOrg)== null)? "": cd.getCodeDescription(oversightOrg);
		
		Integer fdaIntvenIndicator = draftBean.getFdaIntvenIndicator();
		Integer section801Indicator = draftBean.getSection801Indicator();
		Integer delayPostIndicator = draftBean.getDelayPostIndicator();
		Integer dmAppointIndicator = draftBean.getDmAppointIndicator();
		
		if (oversightCountry == null || oversightCountry==0) {
			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftTrialAuthorityCountry+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
		}else{
			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftTrialAuthorityCountry+asterisk,oversightCountryDesc,TYPE_TEXT,IMG_VALID));
		}
		
		if (oversightOrg == null || oversightOrg==0) {
			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftTrialAuthorityOrgName+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
		}else{
			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftTrialAuthorityOrgName+asterisk,oversightOrgDesc,TYPE_TEXT,IMG_VALID));
		}
		
		if (fdaIntvenIndicator == null || fdaIntvenIndicator==0) {
			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftFdaRegIntvnIndicator+asterisk,"",TYPE_RADIO,getValueInvalidImg()));
		}else{

			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftFdaRegIntvnIndicator+asterisk,fdaIntvenIndicator+"",TYPE_RADIO,IMG_VALID));
			if(fdaIntvenIndicator==1){
				if(section801Indicator==null){
					validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftSection801Indicator+asterisk,"",TYPE_RADIO,getValueInvalidImg()));
				}else{
					validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftSection801Indicator+asterisk,section801Indicator+"",TYPE_RADIO,IMG_VALID));
					if(delayPostIndicator==null){
						validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftDelayPostIndicator+asterisk,"",TYPE_RADIO,getValueInvalidImg()));
					}else{
						validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftDelayPostIndicator+asterisk,delayPostIndicator+"",TYPE_RADIO,IMG_VALID));
					}
				}
				
			}
		}
		
		if (dmAppointIndicator == null) {
			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftDMAppointIndicator+asterisk,"",TYPE_RADIO,getValueInvalidImg()));
		}else{
			validationMsgRegulatoryInfo.add(new CtrpValidationJB(LC.CTRP_DraftDMAppointIndicator+asterisk,dmAppointIndicator+"",TYPE_RADIO,IMG_VALID));
		}
		//Addition of validation message to HashMap with key as a Section Name 
		setValidationValues(LC.CTRP_DraftSectRegInfo,validationMsgRegulatoryInfo);
	}
	
	
	// Validation for CTRP draft section "Status Dates" 
	private void validateSectStatusDates(CtrpDraftBean draftBean) {
		
		CodeDao cdDao = new CodeDao();
		ArrayList<CtrpValidationJB> validationMsgStatusDates = new ArrayList<CtrpValidationJB>();
		
		Integer studySubmCategory = draftBean.getCtrpDraftType();// 1-Industrial,0-Non-Industrial
		Integer fkCodelstCtrpStudyStatus = draftBean.getFkCodelstCtrpStudyStatus();
		String studyStatusSubType = (fkCodelstCtrpStudyStatus == null) ? "":cdDao.getCodeSubtype(fkCodelstCtrpStudyStatus);
		
		if (fkCodelstCtrpStudyStatus == null|| fkCodelstCtrpStudyStatus < 1) {
			validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftCurrTrialStatus+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
		}else{
			validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftCurrTrialStatus+asterisk,studyStatusSubType+"",TYPE_TEXT,IMG_VALID));
		}
		
		
		if (studySubmCategory == 0) {
			
			
			String ctrpStudyStatusDate = DateUtil.dateToString(draftBean.getCtrpStudyStatusDate());
			if (ctrpStudyStatusDate == null	|| ctrpStudyStatusDate.length() == 0) {
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftCurrTrialStatusDate+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftCurrTrialStatusDate+asterisk,ctrpStudyStatusDate+"",TYPE_TEXT,IMG_VALID));
			}
			
			
			// Withdrawn, Administratively Complete, Complete,
			// Temporarily Closed to Accrual and Intervention, Temporarily
			// Closed to Accrual
			String studyStopReason = (draftBean.getCtrpStudyStopReason())==null?"":draftBean.getCtrpStudyStopReason();
			if (("withdrawn".equals(studyStatusSubType) || "adminComplete".equals(studyStatusSubType))
					|| ("complete".equals(studyStatusSubType) || "tempClosAccIntv".equals(studyStatusSubType))
					|| "tempClosedAcc".equals(studyStatusSubType)) {
				
				
				if (studyStopReason == null || studyStopReason.length() == 0) {
					validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftWhyTrialStopped+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
				}else{
					validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftWhyTrialStopped+asterisk,studyStopReason+"",TYPE_TEXT,IMG_VALID));
				}
			}else{
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftWhyTrialStopped+asterisk,studyStopReason+"",TYPE_TEXT,IMG_VALID));
			}

			
			String ctrpStudyStartDate = DateUtil.dateToString(draftBean.getCtrpStudyStartDate());
			if (ctrpStudyStartDate == null || ctrpStudyStartDate.length() == 0) {
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftTrialStartDate+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftTrialStartDate+asterisk,ctrpStudyStartDate+"",TYPE_TEXT,IMG_VALID));
			}

			String ctrpStudyCompDate = DateUtil.dateToString(draftBean.getCtrpStudyCompDate());
			if (ctrpStudyCompDate == null || ctrpStudyCompDate.length() == 0) {
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftPrimaryCompletionDate+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftPrimaryCompletionDate+asterisk,ctrpStudyCompDate+"",TYPE_TEXT,IMG_VALID));
			}

		} else if (studySubmCategory == 1) {		//Industrial Trails
		
			String ctrpStudyStatusDate = DateUtil.dateToString(draftBean.getCtrpStudyStatusDate());
			if (ctrpStudyStatusDate == null	|| ctrpStudyStatusDate.length() == 0) {
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftSiteRecruitStatusDate+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			}else{
				validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftSiteRecruitStatusDate+asterisk,ctrpStudyStatusDate+"",TYPE_TEXT,IMG_VALID));
			}
			
			 String openAccrualDate = DateUtil.dateToString(draftBean.getCtrpStudyAccOpenDate());
			 String closeAccrualDate = DateUtil.dateToString(draftBean.getCtrpStudyAccClosedDate()); 
			
			 if(closeAccrualDate.length()==0 && openAccrualDate.length()==0){
				 validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftOpenAccrualDate+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			 }else{
				 validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftOpenAccrualDate+asterisk,openAccrualDate+"",TYPE_TEXT,IMG_VALID));
			 }
			 
			 validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftCloseAccrualDate,closeAccrualDate+"",TYPE_TEXT,IMG_VALID));
			 
			
			 Integer siteTargetAccrual = draftBean.getSiteTargetAccrual();
			 if(draftBean.getSubmitNCIDesignated()==1 && siteTargetAccrual == null ){
				 validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftSiteTargetAccrual+asterisk,"",TYPE_TEXT,getValueInvalidImg()));
			 }else{
				 validationMsgStatusDates.add(new CtrpValidationJB(LC.CTRP_DraftSiteTargetAccrual+asterisk,siteTargetAccrual+"",TYPE_TEXT,IMG_VALID));
			 }
			 
		}
		//Addition of validation message to HashMap with key as a Section Name 
		setValidationValues(LC.CTRP_DraftSectStatusDates,validationMsgStatusDates);
	}

	
	
	
	
	
	// Validation for CTRP draft section "Lead Organization/ Principal Investigator" 
	
	private void validateSectLeadOrg_PriInvs(CtrpDraftBean draftBean) {
		
		ArrayList<CtrpValidationJB> validationMsgLeadOrg_PriInvs = null;

		Integer studySubmCategory = draftBean.getCtrpDraftType();// 1-Industrial,0-Non-Industrial		
		String leadOrgId = draftBean.getLeadOrgCtrpId();
		String leadOrgName = "";//draftBean.getLeadOrgName();
		Integer leadOrgType = draftBean.getLeadOrgType();
		Integer fkLeadOrgAddress = draftBean.getFkAddLeadOrg();

		
		// Non-Industrial Trails
		if (studySubmCategory == 0) {
			
			
		
			if (leadOrgId == null || StringUtil.stringToNum(leadOrgId) < 1) {
				
				
				
				
				
				
				
				
				
				
				if (fkLeadOrgAddress == null || fkLeadOrgAddress < 1) {
					Object[] params = { LC.L_Information };
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString("CTRP_DraftLeadOrgSpecifics",
									params), "Fail", "Missing");
				} else {
					if (leadOrgName == null || leadOrgName.length() == 0) {
						Object[] params = { LC.L_Name };
						this.addValidationError("Id", VelosResourceBundle
								.getLabelString("CTRP_DraftLeadOrgSpecifics",
										params), "Fail", "Incomplete");
					}

					this.validateAddress(fkLeadOrgAddress);

					if (leadOrgType == null || leadOrgType < 1) {
						Object[] params = { LC.L_Type };
						this.addValidationError("Id", VelosResourceBundle
								.getLabelString("CTRP_DraftLeadOrgSpecifics",
										params), "Fail", "Incomplete");
					}
				}
			}

			Integer piId = 0;//draftBean.getPiCtrpId();
			String piFirstName = draftBean.getPiFirstName();
			String piMiddleName = "";//draftBean.getPiMiddleName();
			String piLastName = draftBean.getPiLastName();
			Integer fkPiAddress = 0;//draftBean.getFkAddPI();

			if (piId == null || piId < 1) {
				if (fkPiAddress == null || fkPiAddress < 1) {
					Object[] params = { LC.L_Information };
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString("CTRP_DraftPiSpecifics", params),
							"Fail", "Missing");
				} else {
					if (piFirstName == null || piFirstName.length() == 0) {
						Object[] params = { LC.L_First_Name };
						this.addValidationError("Id",
								VelosResourceBundle.getLabelString(
										"CTRP_DraftPiSpecifics", params),
								"Fail", "Incomplete");
					}
					/* A subtle warning about Middle Name */
					if (piMiddleName == null || piMiddleName.length() == 0) {
						Object[] params = { LC.L_Middle_Name };
						this.addValidationError("Id",
								VelosResourceBundle.getLabelString(
										"CTRP_DraftPiSpecifics", params), "-",
								"Missing");
					}
					if (piLastName == null || piLastName.length() == 0) {
						Object[] params = { LC.L_Last_Name };
						this.addValidationError("Id",
								VelosResourceBundle.getLabelString(
										"CTRP_DraftPiSpecifics", params),
								"Fail", "Incomplete");
					}

					this.validateAddress(fkPiAddress);
				}
			}

		} else if (studySubmCategory == 1) {
			
			/* //Section - Lead Organization/ Submitting Organization/ Principal
			 Investigator -Industrial Trails Integer leadOrgId =
			 draftBean.getLeadOrgCtrpId(); String leadOrgName =
			 draftBean.getLeadOrgName(); Integer leadOrgType =
			 draftBean.getLeadOrgType(); Integer fkLeadOrgAddress =
			 draftBean.getFkAddLeadOrg();
			 
			 if (leadOrgId == null || leadOrgId < 1) { if(fkLeadOrgAddress <
			 1){ Object[] params = {LC.L_Information};
			 this.addValidationError(
			 "Id",VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics"
			 ,params),"Fail","Missing"); }else{ if (leadOrgName == null ||
			 leadOrgName.length() == 0) { Object[] params = {LC.L_Name};
			 this.addValidationError
			 ("Id",VelosResourceBundle.getLabelString("CTRP_DraftLeadOrgSpecifics"
			 , params),"Fail","Incomplete"); }
			 
			 validAddress = this.validateAddress(fkLeadOrgAddress);
			 
			 if (leadOrgType == null || leadOrgType < 1){ Object[] params =
			 {LC.L_Type};
			 this.addValidationError("Id",VelosResourceBundle.getLabelString
			 ("CTRP_DraftLeadOrgSpecifics", params),"-","Incomplete"); } } }
			 
			 Integer submitOrgId = draftBean.getSubmitOrgCtrpId(); String
			 submitOrgName = draftBean.getSubmitOrgName(); Integer
			 submitOrgType = draftBean.getSubmitOrgType(); Integer
			 fkSubmitOrgAddress = draftBean.getFkAddSubmitOrg();
			 
			 if (submitOrgId == null || submitOrgId < 1) {
			 if(fkSubmitOrgAddress < 1){ Object[] params = {LC.L_Information};
			 this.addValidationError("Id",VelosResourceBundle.getLabelString(
			 "CTRP_DraftSubmitOrgSpecifics",params),"Fail","Missing"); }else{
			 if (submitOrgName == null || submitOrgName.length() == 0) {
			 Object[] params = {LC.L_Name};
			 this.addValidationError("Id",VelosResourceBundle
			 .getLabelString("CTRP_DraftSubmitOrgSpecifics",
			 params),"Fail","Incomplete"); }
			 
			 validAddress = this.validateAddress(fkSubmitOrgAddress);
			 
			 if (submitOrgType == null || submitOrgType < 1){ Object[] params
			 = {LC.L_Type};
			 this.addValidationError("Id",VelosResourceBundle.getLabelString
			 ("CTRP_DraftSubmitOrgSpecifics", params),"-","Incomplete"); } } }
			 Integer submitOrgNciDesignated =
			 draftBean.getSubmitNCIDesignated(); if (submitOrgNciDesignated ==
			 null || submitOrgNciDesignated < 0) {
			 this.addValidationError("Id"
			 ,LC.CTRP_DraftIsSubmOrgNCIDesignated,"Fail","Missing"); }
			* 
			 * Integer piId = draftBean.getPiCtrpId(); String piFirstName =
			 * draftBean.getPiFirstName(); String piMiddleName =
			 * draftBean.getPiMiddleName(); String piLastName =
			 * draftBean.getPiLastName(); Integer fkPiAddress =
			 * draftBean.getFkAddPI();
			 * 
			 * if (piId < 1) { if(fkPiAddress < 1){ Object[] params =
			 * {LC.L_Information};
			 * this.addValidationError("Id",VelosResourceBundle
			 * .getLabelString("CTRP_DraftSitePiSpecifics"
			 * ,params),"Fail","Missing"); }else{ if (piFirstName == null ||
			 * piFirstName.length() == 0){ Object[] params = {LC.L_First_Name};
			 * this.addValidationError("Id",VelosResourceBundle.getLabelString(
			 * "CTRP_DraftSitePiSpecifics", params),"Fail","Incomplete"); } A
			 * subtle warning about Middle Name if (piMiddleName == null ||
			 * piMiddleName.length() == 0){ Object[] params ={LC.L_Middle_Name};
			 * this.addValidationError("Id",VelosResourceBundle.getLabelString(
			 * "CTRP_DraftSitePiSpecifics", params),"-","Missing"); } if
			 * (piLastName == null || piLastName.length() == 0){ Object[] params
			 * = {LC.L_Last_Name};
			 * this.addValidationError("Id",VelosResourceBundle
			 * .getLabelString("CTRP_DraftSitePiSpecifics",
			 * params),"Fail","Incomplete"); }
			 * 
			 * validAddress = this.validateAddress(fkPiAddress); } }
			*/
		}

	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * Method: validateCtrpDocuments Parameter: studyId Functionality: Checks
	 * for existence of CTRP documents under Study 'Version/Documents' tab and
	 * their validity
	 */
	private Integer validateCtrpDocuments(Integer studyId) {
		Integer output = 0;
		try {
			output = 1;
		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpValidationDao.validateIndIdes: " + e);
			output = -1;
		}
		return output;
	}

	/*
	 * Method: validateIndIdes Parameter: studyId Functionality: Checks for
	 * existence of IND/IDE numbers specified under 'Study Summary' tab and
	 * their validity
	 */
	private Integer validateIndIdes(Integer studyId) {
		Integer output = 0;
		try {
			StudyINDIDEDAO indIdeDao = new StudyINDIDEDAO();
			indIdeDao.getStudyIndIdeInfo(studyId);

			ArrayList<Integer> idIndIdeList = indIdeDao.getIdIndIdeList();
			if (idIndIdeList == null || idIndIdeList.size() <= 0) {
				return output;
			}

			ArrayList<Integer> typeIndIdeList = indIdeDao.getTypeIndIdeList();
			ArrayList<String> numberIndIdeList = indIdeDao
					.getNumberIndIdeList();
			ArrayList<Integer> grantorList = indIdeDao.getGrantorList();
			ArrayList<Integer> holderList = indIdeDao.getHolderList();
			ArrayList<Integer> programCodeList = indIdeDao.getTypeIndIdeList();
			ArrayList<Integer> expandedAccessList = indIdeDao
					.getExpandedAccessList();
			ArrayList<Integer> accessTypeList = indIdeDao.getAccessTypeList();
			ArrayList<Integer> exemptIndIdeList = indIdeDao
					.getExemptIndIdeList();

			String numberIndIde = "";
			Integer typeIndIde = 0, grantor = 0, holder = 0, programCode = 0, expandedAccess = 0, accessType = 0, exemptIndIde = 0;

			for (int i = 0; i < idIndIdeList.size(); i++) {
				typeIndIde = typeIndIdeList.get(i);
				if (typeIndIde == null || typeIndIde < 1) {
					this.addValidationError("Id", LC.L_IndIde_Type, "Fail",
							"Missing");
				} else {
					if (typeIndIde != 1 && typeIndIde != 2) {// Invalid Type
						this.addValidationError("Id", LC.L_IndIde_Type, "-",
								"Invalid");
					}
				}
				numberIndIde = numberIndIdeList.get(i);
				if (numberIndIde == null || numberIndIde.length() < 1) {
					this.addValidationError("Id", LC.L_IndIde_Number, "Fail",
							"Missing");
				}
				grantor = grantorList.get(i);
				if (grantor == null || grantor < 1) {
					this.addValidationError("Id", LC.L_IndIde_Grantor, "Fail",
							"Missing");
				}
				holder = holderList.get(i);
				if (holder == null || holder < 1) {
					this.addValidationError("Id", LC.L_IndIde_HldrType, "Fail",
							"Missing");
				}
				programCode = programCodeList.get(i);
				if (programCode == null || programCode < 1) {
					this.addValidationError("Id", LC.L_NCI_Div_Prom_Code, "-",
							"Missing");
				}
				expandedAccess = expandedAccessList.get(i);
				if (expandedAccess == null || expandedAccess < 0) {
					Object[] params = { LC.L_Expanded_Access };
					this.addValidationError("Id",
							VelosResourceBundle.getLabelString(
									"CTRP_DraftIndIdeSpecifics", params), "-",
							"Missing");
				}
				accessType = accessTypeList.get(i);
				if (accessType == null || accessType < 1) {
					Object[] params = { LC.L_Expanded_AccType };
					this.addValidationError("Id",
							VelosResourceBundle.getLabelString(
									"CTRP_DraftIndIdeSpecifics", params), "-",
							"Missing");
				}
				exemptIndIde = exemptIndIdeList.get(i);
				if (exemptIndIde == null || exemptIndIde < 0) {
					Object[] params = { LC.L_Exempt };
					this.addValidationError("Id",
							VelosResourceBundle.getLabelString(
									"CTRP_DraftIndIdeSpecifics", params), "-",
							"Missing");
				}
			}
			output = 1;
		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpValidationDao.validateIndIdes: " + e);
			output = -1;
		}
		return output;
	}

	private void addValidationError(String string, String lIndIdeType,
			String string2, String string3) {
		// TODO Auto-generated method stub
		
	}
	/*
	 * Method: validateNihGrants Parameter: studyId Functionality: Checks for
	 * existence of NIH Grants specified under 'Study Summary' tab and their
	 * validity
	 */
	private Integer validateNihGrants(Integer studyId) {
		Integer output = 0;
		try {
			StudyNIHGrantDao nihGrantDao = new StudyNIHGrantDao();
			nihGrantDao.getStudyNIHGrantRecords(studyId);

			ArrayList<Integer> pkStudyNIhGrants = nihGrantDao
					.getPkStudyNIhGrants();
			if (pkStudyNIhGrants == null || pkStudyNIhGrants.size() <= 0) {
				return output;
			}

			ArrayList<Integer> fundMechsms = nihGrantDao.getFundMechsms();
			ArrayList<Integer> institudeCodes = nihGrantDao.getInstitudeCodes();
			ArrayList<Integer> programCodes = nihGrantDao.getProgramCodes();
			ArrayList<String> nihGrantSerials = nihGrantDao
					.getNihGrantSerials();

			String nihGrantSerial = "";
			Integer fundMechsm = 0, institudeCode = 0, programCode = 0;

			for (int i = 0; i < pkStudyNIhGrants.size(); i++) {
				fundMechsm = fundMechsms.get(i);
				if (fundMechsm == null || fundMechsm < 1) {
					Object[] params = { LC.L_Funding_Mechsm };
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString("CTRP_DraftNihGrantSpecifics",
									params), "Fail", "Missing");
				}
				institudeCode = institudeCodes.get(i);
				if (institudeCode == null || institudeCode < 1) {
					Object[] params = { LC.L_Institute_Code };
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString("CTRP_DraftNihGrantSpecifics",
									params), "Fail", "Missing");
				}
				programCode = programCodes.get(i);
				if (programCode == null || programCode < 1) {
					Object[] params = { LC.L_NCI_Div_Prom_Code };
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString("CTRP_DraftNihGrantSpecifics",
									params), "Fail", "Missing");
				}
				nihGrantSerial = nihGrantSerials.get(i);
				if (nihGrantSerial == null || nihGrantSerial.length() <= 0) {
					Object[] params = { LC.L_Serial_Number };
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString("CTRP_DraftNihGrantSpecifics",
									params), "Fail", "Missing");
				}
			}
			output = 1;
		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpValidationDao.validateIndIdes: " + e);
			output = -1;
		}
		return output;

	}

	/*
	 * Method: validateAddress Parameter: fkAddress Functionality: Checks for
	 * existence of Address fields and their validity
	 */
	private Integer validateAddress(Integer fkAddress) {
		Integer output = 1;
		try {
			AddressAgentRObj addressAgent = EJBUtil.getAddressAgentHome();
			AddressBean addressBean = addressAgent.getAddressDetails(fkAddress);

			Integer fkCodelstAddressType = StringUtil
					.stringToInteger(addressBean.getAddType());
			CodeDao cdAddress = new CodeDao();
			String addressTypeSubType = cdAddress
					.getCodeSubtype(fkCodelstAddressType);

			String key = "";
			if ("leadOrg".equals(addressTypeSubType)) {
				key = "CTRP_DraftLeadOrgSpecifics";
			}

			if ("submittingOrg".equals(addressTypeSubType)) {
				key = "CTRP_DraftLeadOrgSpecifics";
			}

			if ("pi".equals(addressTypeSubType)) {
				key = "CTRP_DraftPiSpecifics";
			}

			if ("sponsor".equals(addressTypeSubType)) {
				key = "CTRP_DraftSponsorSpecifics";
			}

			if ("sponsorContact".equals(addressTypeSubType)) {
				key = "CTRP_DraftSubmitOrgSpecifics";
			}

			if ("summ4Sponsor".equals(addressTypeSubType)) {
				key = "CTRP_DraftSumm4SponsorSpecifics";
			}

			String streetAddress = addressBean.getAddPri();
			String city = addressBean.getAddCity();
			String state = addressBean.getAddState();
			String zip = addressBean.getAddZip();
			String country = addressBean.getAddCountry();
			String email = addressBean.getAddEmail();

			if (streetAddress == null || streetAddress.length() == 0) {
				Object[] params = { LC.L_Street_Address };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[0]), "Fail", "Incomplete");
				output = 0;
			}
			if (city == null || city.length() == 0) {
				Object[] params = { LC.L_City };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[1]), "Fail", "Incomplete");
				output = 0;
			}
			if (state == null || state.length() == 0) {
				Object[] params = { LC.CTRP_DraftStateProvince };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[2]), "Fail", "Incomplete");
				output = 0;
			}
			if (zip == null || zip.length() == 0) {
				Object[] params = { LC.L_Zip };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[3]), "Fail", "Incomplete");
				output = 0;
			}
			if (country == null || country.length() == 0) {
				Object[] params = { LC.L_Country };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[4]), "Fail", "Incomplete");
				output = 0;
			}
			if (email == null || email.length() == 0) {
				Object[] params = { LC.L_Email };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[5]), "Fail", "Incomplete");
				output = 0;
			}

			String phone = addressBean.getAddPhone();
			String tty = addressBean.getAddTty();
			String fax = addressBean.getAddFax();
			String url = addressBean.getAddUrl();

			if (phone == null || phone.length() == 0) {
				Object[] params = { LC.L_Phone };
				if ("pi".equals(addressTypeSubType)
						|| "sponsorContact".equals(addressTypeSubType)) {
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString(key, params[6]), "Fail", "Missing");
				} else {
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString(key, params[6]), "-", "Missing");
				}
			}
			if (tty == null || tty.length() == 0) {
				Object[] params = { LC.L_TTY };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[7]), "-", "Missing");
			}
			if (fax == null || fax.length() == 0) {
				Object[] params = { LC.L_Fax };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[8]), "-", "Missing");
			}
			if (url == null || url.length() == 0) {
				Object[] params = { LC.L_Url };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString(key, params[9]), "-", "Missing");
			}
		} catch (Exception e) {
			Rlog.fatal("ctrp",
					"Exception in CtrpValidationDao.validateAddress: " + e);
			output = -1;
		}
		return output;
	}

	

	

	
	
	// Validation for CTRP draft section "Sponsor/Responsible" for
	// Non-Industrial Trails
	private void validateSectSponser_ResParty(CtrpDraftBean draftBean) {

		String sponsorId = draftBean.getSponsorCtrpId();
		Integer fkSponsorSite = draftBean.getFkSiteSponsor();
		Integer fkSponsorAddress = draftBean.getFkAddSponsor();

		if (sponsorId == null || sponsorId.length() < 1) {
			if (fkSponsorAddress == null || fkSponsorAddress < 1) {
				Object[] params = { LC.L_Information };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString("CTRP_DraftSponsorSpecifics", params),
						"Fail", "Missing");
			} else {
				if (fkSponsorSite == null || fkSponsorSite > 0) {
					SiteAgentRObj siteAgent = EJBUtil.getSiteAgentHome();
					SiteBean siteBean = siteAgent.getSiteDetails(fkSponsorSite);
					String sponsorName = siteBean.getSiteName();
					
					if (sponsorName == null || sponsorName.length() == 0) {
						Object[] params = { LC.L_Name };
						this.addValidationError("Id", VelosResourceBundle
								.getLabelString("CTRP_DraftSponsorSpecifics",
										params), "Fail", "Incomplete");
					}
				}
			//	this.validateAddress(draftBean.getFkAddPI());
			}
		}

		Integer respParty = draftBean.getResponsibleParty();
		if (respParty == null) {
			this.addValidationError("Id", LC.CTRP_DraftResponsibleParty,
					"Fail", "Missing");
		} else {
			if (respParty == 0) {// Sponsor
				String contactId = draftBean.getRpSponsorContactId();
				Integer contactType = draftBean.getRpSponsorContactType();

				if (contactType == null) {
					this.addValidationError("Id",
							LC.CTRP_DraftSponsorContactType, "Fail", "Missing");
				} else {
					switch (contactType) {
					case 1:// Personal
						Integer fkContact = draftBean.getFkUserSponsorPersonal();
						UserAgentRObj userAgent = EJBUtil.getUserAgentHome();
						UserBean userBean = new  UserBean(); 
											
						if (fkContact == null || fkContact > 0) {
							userBean = userAgent.getUserDetails(fkContact);
						}
						String contactFirstName = userBean.getUserFirstName();
						String contactMiddleName = userBean.getUserMidName();
						String contactLastName = userBean.getUserLastName();
						Integer fkContactAddress = draftBean
								.getFkAddSponsorPersonal();

						if (contactId == null || contactId.length() < 1) {
							if (fkContactAddress == null
									|| fkContactAddress < 1) {
								Object[] params = { LC.L_Information };
								this
										.addValidationError(
												"Id",
												VelosResourceBundle
														.getLabelString(
																"CTRP_DraftPersonalContactSpecifics",
																params),
												"Fail", "Missing");
							} else {
								if (contactFirstName == null
										|| contactFirstName.length() == 0) {
									Object[] params = { LC.L_First_Name };
									this
											.addValidationError(
													"Id",
													VelosResourceBundle
															.getLabelString(
																	"CTRP_DraftPersonalContactSpecifics",
																	params),
													"-", "Missing");
								}
								/* A subtle warning about Middle Name */
								if (contactMiddleName == null
										|| contactMiddleName.length() == 0) {
									Object[] params = { LC.L_Middle_Name };
									this
											.addValidationError(
													"Id",
													VelosResourceBundle
															.getLabelString(
																	"CTRP_DraftPersonalContactSpecifics",
																	params),
													"-", "Missing");
								}
								if (contactLastName == null
										|| contactLastName.length() == 0) {
									Object[] params = { LC.L_Last_Name };
									this
											.addValidationError(
													"Id",
													VelosResourceBundle
															.getLabelString(
																	"CTRP_DraftPersonalContactSpecifics",
																	params),
													"Fail", "Incomplete");
								}

								this.validateAddress(fkContactAddress);
							}
						}
						break;
					case 0:// Generic
						if (contactId == null || contactId.length() < 1) {
							this.addValidationError("Id",
									LC.CTRP_DraftGenericContactId, "Fail",
									"Missing");
						}
						String contactTitle = draftBean
								.getSponsorGenericTitle();
						if (contactTitle == null || contactTitle.length() == 0) {
							this.addValidationError("Id",
									LC.CTRP_DraftGenericContactTitle, "-",
									"Missing");
						}
						break;
					}
				}
			}
		}

		String rpEmail = draftBean.getRpEmail();
		String rpPhone = draftBean.getRpPhone();
		Integer rpExtn = draftBean.getRpExtn();
		if (rpEmail == null || rpEmail.length() == 0) {
			Object[] params = { LC.L_Email };
			this.addValidationError("Id", VelosResourceBundle.getLabelString(
					"CTRP_DraftRespPartySpecifics", params), "Fail", "Missing");
		}
		if (rpPhone == null || rpPhone.length() == 0) {
			Object[] params = { LC.L_Phone };
			this.addValidationError("Id", VelosResourceBundle.getLabelString(
					"CTRP_DraftRespPartySpecifics", params), "Fail", "Missing");
		}
		if (rpExtn == null || rpExtn < 1) {
			Object[] params = { LC.L_Phone_Extn };
			this.addValidationError("Id", VelosResourceBundle.getLabelString(
					"CTRP_DraftRespPartySpecifics", params), "Fail", "Missing");
		}

	}

	// Validation for CTRP draft section "Summary4 Information" for
	// Industrial/Non-Industrial Trails
	private void validateSectSummary4(CtrpDraftBean draftBean) {

		Integer studySubmCategory = draftBean.getCtrpDraftType();// 1-Industrial,
		// 0-Non-Industrial
		// Non-Industrial Trails
		if (studySubmCategory == 0) {

			Integer summ4SponsorType = draftBean.getSumm4SponsorType();
			if (summ4SponsorType == null || summ4SponsorType < 1) {
				Object[] params = { LC.L_Type };
				this.addValidationError("Id", VelosResourceBundle
						.getLabelString("CTRP_DraftSumm4SponsorSpecifics",
								params), "Fail", "Missing");
			}

			String summ4SponsorId = draftBean.getSumm4SponsorId();
			Integer fkSumm4SponsorSite = draftBean.getFkSiteSumm4Sponsor();
			Integer fkSumm4SponsorAddress = draftBean.getfkAddSumm4Sponsor();

			if (summ4SponsorId == null || summ4SponsorId.length() == 0) {
				if (fkSumm4SponsorAddress == null || fkSumm4SponsorAddress < 1) {
					Object[] params = { LC.L_Information };
					this.addValidationError("Id", VelosResourceBundle
							.getLabelString("CTRP_DraftSumm4SponsorSpecifics",
									params), "Fail", "Missing");
				} else {
					if (fkSumm4SponsorSite == null || fkSumm4SponsorSite > 0) {
						SiteAgentRObj siteAgent = EJBUtil.getSiteAgentHome();
						SiteBean siteBean = siteAgent.getSiteDetails(fkSumm4SponsorSite);
						String summ4SponsorName = siteBean.getSiteName();
						
						if (summ4SponsorName == null || summ4SponsorName.length() == 0) {
							Object[] params = { LC.L_Name };
							this.addValidationError("Id", VelosResourceBundle
									.getLabelString(
											"CTRP_DraftSumm4SponsorSpecifics",
											params), "Fail", "Incomplete");
						}
					}

					this.validateAddress(fkSumm4SponsorAddress);
				}
			}

			String summ4ProgramCode = draftBean.getSponsorProgramCode();
			if (summ4ProgramCode == null || summ4ProgramCode.length() == 0) {
				this.addValidationError("Id", LC.CTRP_DraftProgramCode, "-",
						"Missing");
			}

		} else if (studySubmCategory == 1) {
			/*
			 * //Section - Summary4 Information -Industrial Trails Integer
			 * summ4SponsorType = draftBean.getSumm4SponsorType(); if
			 * (summ4SponsorType < 1){ Object[] params ={LC.L_Type};
			 * this.addValidationError("Id",VelosResourceBundle.getLabelString(
			 * "CTRP_DraftSumm4SponsorSpecifics",params),"Fail","Missing"); }
			 * 
			 * String summ4SponsorId = draftBean.getSumm4SponsorId(); String
			 * summ4SponsorName = draftBean.getSumm4SponsorName(); Integer
			 * fkSumm4SponsorAddress = draftBean.getfkAddSumm4Sponsor();
			 * 
			 * if (summ4SponsorId == null || summ4SponsorId.length() == 0) {
			 * if(fkSumm4SponsorAddress == null || fkSumm4SponsorAddress < 1){
			 * Object[] params = {LC.L_Information};
			 * this.addValidationError("Id"
			 * ,VelosResourceBundle.getLabelString("CTRP_DraftSumm4SponsorSpecifics"
			 * ,params),"Fail","Missing"); }else{ if (summ4SponsorName == null
			 * || summ4SponsorName.length() == 0){ Object[] params ={LC.L_Name};
			 * this.addValidationError("Id",VelosResourceBundle.getLabelString(
			 * "CTRP_DraftSumm4SponsorSpecifics",params),"Fail","Incomplete"); }
			 * 
			 * validAddress = this.validateAddress(fkSumm4SponsorAddress); } }
			 * 
			 * String summ4ProgramCode = draftBean.getSponsorProgramCode(); if
			 * (summ4ProgramCode == null || summ4ProgramCode.length() == 0){ if
			 * (submitOrgNciDesignated != null && submitOrgNciDesignated == 1) {
			 * this.addValidationError("Id",LC.CTRP_DraftSiteProgramCode,"Fail",
			 * "Missing"); }else{
			 * this.addValidationError("Id",LC.CTRP_DraftSiteProgramCode
			 * ,"-","Missing"); } }
			 */

		}

	}

	// Validation for CTRP draft section "NIH Grant Information" for
	// Non-Industrial Trails
	private void validateSectNIHGrantInfo(CtrpDraftBean draftBean, int studyId) {
		if (validateNihGrants(studyId) < 1) {
			this.addValidationError("Id", LC.L_NIH_Grant_Information, "Fail",
					"Missing");
		}

	}

	
	// Validation for CTRP draft section "FDA IND/IDE Information" for
	// Non-Industrial Trails
	private void validateSectIndIde(CtrpDraftBean draftBean, int studyId) {

		if (validateIndIdes(studyId) < 1) {
			this.addValidationError("Id", LC.L_IndIde_Info, "Fail", "Missing");
		}

	}

	}
