package com.velos.epat.audit.web;

import java.util.ArrayList;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

//import com.velos.epat.audit.service.AuditRowEpatAgent;
import com.velos.epat.business.common.AuditRowEpatDao;
import com.velos.eres.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class AuditRowEpatJB {
	private Integer id = 0;
    private String tableName;
	private Integer RID;
    private String action;
    private Date timeStamp;
    private String userName;	
		
 // Getters and setters
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getTableName() {
		return tableName;
	}

	public void setRID(Integer rid) {
		this.RID = rid;
	}
	public Integer getRID() {
		return RID;
	}

	public void setAction(String action) {
		this.action = action;
	}
	public String getAction() {
		return action;
	}
	
	public void setTimeStamp(Date timeStamp){
		this.timeStamp=timeStamp;
	}
	public Date getTimeStamp(){
		return this.timeStamp;
	}
	
	public void setUserName(String userName){
		this.userName=userName;
	}
	public String getUserName(){
		return this.userName;
	}
	
	public AuditRowEpatJB() {
	
	}
	
	public int setReasonForChangeOfPatient(int patientId, int userId, String remarks, int lastRaid){
		int output =0;
		int rid = getMeRID(patientId, "PERSON", "PK_PERSON");
		
		AuditRowEpatDao auditDao = new AuditRowEpatDao();
        ArrayList listRaids = auditDao.getAllLatestRaids(rid, userId, lastRaid);
        
        if (null != listRaids){
	        for (int i=0; i<listRaids.size();i++){
	        	int raid = StringUtil.stringToNum(""+listRaids.get(i));
	        	AuditRowEpatJBHelper helper = new AuditRowEpatJBHelper(this);
	            output = helper.setReasonForChange(raid, remarks);
	        }
        } else{
        	output = -1;
        }
		//AuditRowEresJB auditB = new AuditRowEresJB();
		//output = auditB.setReasonForChangeOfPatient(patientId, userId, remarks);
		return output;
	}
	
	private int setReasonForChange(int rid, int userId, String remarks) {
		int output =0;
		int raid =0;
		try{
			raid = findLatestRaid(rid, userId);

			AuditRowEpatJBHelper helper = new AuditRowEpatJBHelper(this);
            output = helper.setReasonForChange(raid, remarks);
		} catch (Exception e){
			Rlog.fatal("auditRow","EXCEPTION IN setReasonForChange "+ e);
			output = -2;
		}
		return output;	
	}
	
	public int findLatestRaidForPatient(int patientId){
		int output =0;
		int rid = getMeRID(patientId, "PERSON", "PK_PERSON");
		output = findLatestRaid(rid);
		return output;
	}
	
	private int findLatestRaid(int rid) {
		int output = 0;
		AuditRowEpatJBHelper helper = new AuditRowEpatJBHelper(this);
        output = helper.findLatestRaid(rid);
        return output;
	}
	
	private int findLatestRaid(int rid, int userId) {
		int output = 0;
		AuditRowEpatJBHelper helper = new AuditRowEpatJBHelper(this);
        output = helper.findLatestRaid(rid, userId);
        return output;
	}
	
	private int getMeRID(int pkey, String tableName, String pkColumnName) {
		int output = 0;
		try {
            AuditRowEpatJBHelper helper = new AuditRowEpatJBHelper(this);
            output = helper.getRID(pkey, tableName, pkColumnName);
            return output;
        }
        catch (Exception e) {
            Rlog.fatal("auditRow","getMeRID EXCEPTION IN READING "+ tableName +" table"+ e);
            return -2;
        }
	}
	
	/*private AuditRowEpatAgent getAuditRowEpatAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AuditRowEpatAgent)ic.lookup(JNDINames.AuditRowEpatAgentHome);
	}*/
}
