/**
 * 
 */
package com.velos.services.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlElement;

/**Identifies a visit in eResearch
 * @author dylan
 *
 */
@XmlRootElement(name="Visit")
@XmlAccessorType(XmlAccessType.FIELD)
public class Visit extends ServiceObject {

	protected VisitIdentifier visitIdentifier;
	protected String visitName;
	protected String visitDescription;
	protected Integer visitSequence;
	protected Duration absoluteInterval;
	protected Duration relativeVisitInterval;
	protected Duration relativeVisitName;
	protected List<Event> events;
	protected Duration visitWindowBefore;
	protected Duration visitWindowAfter;
	protected boolean noIntervalDefined;
	protected String VisitWindow;
	
	protected Date visitSuggestedDate;
	protected Date visitScheduledDate;
	
	public VisitIdentifier getVisitIdentifier() {
		return visitIdentifier;
	}

	public void setVisitIdentifier(VisitIdentifier visitIdentifier) {
		this.visitIdentifier = visitIdentifier;
	}

	public Integer getVisitSequence() {
		return visitSequence;
	}

	public void setVisitSequence(Integer visitSequence) {
		this.visitSequence = visitSequence;
	}

	public Duration getVisitWindowBefore() {
		return visitWindowBefore;
	}

	public void setVisitWindowBefore(Duration visitWindowBefore) {
		this.visitWindowBefore = visitWindowBefore;
	}

	public Duration getVisitWindowAfter() {
		return visitWindowAfter;
	}

	public void setVisitWindowAfter(Duration visitWindowAfter) {
		this.visitWindowAfter = visitWindowAfter;
	}

	public boolean isNoIntervalDefined() {
		return noIntervalDefined;
	}

	public void setNoIntervalDefined(boolean noIntervalDefined) {
		this.noIntervalDefined = noIntervalDefined;
	}

	public Visit(){
		
	}

	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public String getVisitDescription() {
		return visitDescription;
	}

	public void setVisitDescription(String visitDescription) {
		this.visitDescription = visitDescription;
	}

	public Duration getAbsoluteInterval() {
		return absoluteInterval;
	}

	public void setAbsoluteInterval(Duration absoluteInterval) {
		this.absoluteInterval = absoluteInterval;
	}

	public Duration getRelativeVisitInterval() {
		return relativeVisitInterval;
	}

	public void setRelativeVisitInterval(Duration relativeVisitInterval) {
		this.relativeVisitInterval = relativeVisitInterval;
	}

	public Duration getRelativeVisitName() {
		return relativeVisitName;
	}

	public void setRelativeVisitName(Duration relativeVisitName) {
		this.relativeVisitName = relativeVisitName;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public String getVisitWindow() {
		return VisitWindow;
	}

	public void setVisitWindow(String visitWindow) {
		VisitWindow = visitWindow;
	}

	public Date getVisitSuggestedDate() {
		return visitSuggestedDate;
	}

	public void setVisitSuggestedDate(Date visitSuggestedDate) {
		this.visitSuggestedDate = visitSuggestedDate;
	}

	public Date getVisitScheduledDate() {
		return visitScheduledDate;
	}

	public void setVisitScheduledDate(Date visitScheduledDate) {
		this.visitScheduledDate = visitScheduledDate;
	}
	
	
}
