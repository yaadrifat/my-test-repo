
package com.velos.services.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Code")

/**
 * Class used for transferring information about codes values. Code values
 * are the those that map to the er_codelst table.
 * 
 * In general, when sending data into the service (through create or
 * update methods) only the {@link #code} value must be passed.
 */
public class Code 
	implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2981827419922964310L;

	protected String type;

	/**
	 * code field is the actual value of the code. cannot  be null.
	 * 
	 */
	@XmlElement(required=true)
    protected String code;

    protected String description;

    public Code() {
    
    }
    
    public Code(String type, String code) {
		super();
		this.type = type;
		this.code = code;
	}
    public Code(String type, String code, String description) {
		super();
		this.type = type;
		this.code = code;
		this.description = description;
	}

	/**
     * Gets the value of the type property.
     * Type corresponds to er_codelst.codelst_type field.
     * 
     * Not a required field.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the value of the type property.
     * Type corresponds to er_codelst.codelst_type field.
     * 
     * Not a required field.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the code property.
     * Code corresponds to er_codelst.codelst_subtyp field
     * 
     * This is a required field. All Codes will contain.
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @NotNull
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * @see #getCode()
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * Description corresponds to er_codelst.codelst_desc field
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * @see #getDescription()
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }
    
    
    public String toString(){
    	return "Code: " + code + " Desc: " + description + " Type: " + type;
    }

}
