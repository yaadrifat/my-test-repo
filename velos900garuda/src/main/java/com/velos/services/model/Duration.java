/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Transfer object for portraying time duration information.
 * This is most often used for describing intervals in Calendars and Schedules.
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Duration")

public class Duration implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4463807315597025469L;

	/**
	 * Enumeration for units of time.
	 * @author dylan
	 *
	 */
	public enum TimeUnits {
		DAY,
		WEEK,
		MONTH,
		YEAR
	}
	
	protected int value;
	protected TimeUnits unit;
	
	public Duration(){
		
	}
	
	public Duration(int value, TimeUnits unit) {
		super();
		this.value = value;
		this.unit = unit;
	}
	
	public Duration(int value, String unitString)
	{
		super(); 
		this.value = value; 
		this.unit = decodeUnit(unitString); 
		 
	}


	@NotNull
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@NotNull
	public TimeUnits getUnit() {
		return unit;
	}

	public void setUnit(TimeUnits unit) {
		this.unit = unit;
	}
	
	public void setUnit(String unit)
	{
			this.unit = decodeUnit(unit); 		
	}
	
	private TimeUnits decodeUnit(String unit)
	{
		if(unit == null ) return null; 
		if(unit.equals("D")) return TimeUnits.DAY; 
		if(unit.equals("W")) return TimeUnits.WEEK;
		if(unit.equals("M")) return TimeUnits.MONTH; 
		if(unit.equals("Y")) return TimeUnits.YEAR; 
		return null; 
	}
	public static String encodeUnit(TimeUnits timeunits)
	{
		if(timeunits == null ) return null; 
		if(timeunits.equals(TimeUnits.DAY)) return "D"; 
		if(timeunits.equals(TimeUnits.WEEK)) return "W";
		if(timeunits.equals(TimeUnits.MONTH)) return "M"; 
		if(timeunits.equals(TimeUnits.YEAR)) return "Y"; 
		return null; 
	}
	
	
}
