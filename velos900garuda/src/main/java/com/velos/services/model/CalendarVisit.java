/**
 * Created On May 6, 2011
 */
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

/**
 * @author Kanwaldeep
 *
 */
@XmlRootElement(name="Visit")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarVisit extends ServiceObject{
	
	private static final long serialVersionUID = -2136767714648967858L;
	@XmlElement(required=true)	
	protected String visitName;
	protected VisitIdentifier visitIdentifier; 
	protected String visitDescription;
	protected Integer visitSequence;
	protected Duration absoluteInterval;
	protected Duration relativeVisitInterval;
//	protected Duration relativeVisitName;
	protected String relativeVisitName; 
	protected CalendarEvents events;
	protected Duration visitWindowBefore;
	protected Duration visitWindowAfter;
	protected boolean noIntervalDefined;
	

	public CalendarVisit(){
		
	}
	
	public Integer getVisitSequence() {
		return visitSequence;
	}

	public void setVisitSequence(Integer visitSequence) {
		this.visitSequence = visitSequence;
	}

	@Valid
	public Duration getVisitWindowBefore() {
		return visitWindowBefore;
	}

	public void setVisitWindowBefore(Duration visitWindowBefore) {
		this.visitWindowBefore = visitWindowBefore;
	}

	@Valid
	public Duration getVisitWindowAfter() {
		return visitWindowAfter;
	}

	public void setVisitWindowAfter(Duration visitWindowAfter) {
		this.visitWindowAfter = visitWindowAfter;
	}

	public boolean isNoIntervalDefined() {
		return noIntervalDefined;
	}

	public void setNoIntervalDefined(boolean noIntervalDefined) {
		this.noIntervalDefined = noIntervalDefined;
	}
	
	@NotNull
	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public String getVisitDescription() {
		return visitDescription;
	}

	public void setVisitDescription(String visitDescription) {
		this.visitDescription = visitDescription;
	}

	@Valid
	public Duration getAbsoluteInterval() {
		return absoluteInterval;
	}

	public void setAbsoluteInterval(Duration absoluteInterval) {
		this.absoluteInterval = absoluteInterval;
	}

	@Valid
	public Duration getRelativeVisitInterval() {
		return relativeVisitInterval;
	}

	public void setRelativeVisitInterval(Duration relativeVisitInterval) {
		this.relativeVisitInterval = relativeVisitInterval;
	}

//	public Duration getRelativeVisitName() {
//		return relativeVisitName;
//	}
//
//	public void setRelativeVisitName(Duration relativeVisitName) {
//		this.relativeVisitName = relativeVisitName;
//	}
	
	public String getRelativeVisitName() {
		return relativeVisitName;
	}

	public void setRelativeVisitName(String relativeVisitName) {
		this.relativeVisitName = relativeVisitName;
	}

	public CalendarEvents getEvents() {
		return events;
	}

	public void setEvents(CalendarEvents events) {
		this.events = events;
	}
	
	public VisitIdentifier getVisitIdentifier() {
		return visitIdentifier;
	}

	public void setVisitIdentifier(VisitIdentifier visitIdentifier) {
		this.visitIdentifier = visitIdentifier;
	}
	
	 public ParentIdentifier getParentIdentifier() {
	    return super.parentIdentifier; 
	   }
	    
	    public void setParentIdentifier(ParentIdentifier calendarIdentifer) {
	      
	        super.parentIdentifier = calendarIdentifer;
	    }

}
