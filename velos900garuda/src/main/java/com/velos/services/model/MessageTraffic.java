package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="HeartBeatCount")


public class MessageTraffic implements Serializable{
	/**
     * 
     */
    private static final long serialVersionUID = 5175773824153262803L;
    
    /**
	 * Object for Message Traffic with properties as All Messages Count,
	 * Success Messages Count, Failed Message Count.
	 */
	protected Integer messageCountAll;
	protected Integer messageCountSuccess;
	protected Integer messageCountFailure;
	
	public Integer getMessageCountAll() {
		return messageCountAll;
	}
	public void setMessageCountAll(Integer messageCountAll) {
		this.messageCountAll = messageCountAll;
	}
	public Integer getMessageCountSuccess() {
		return messageCountSuccess;
	}
	public void setMessageCountSuccess(Integer messageCountSuccess) {
		this.messageCountSuccess = messageCountSuccess;
	}
	public Integer getMessageCountFailure() {
		return messageCountFailure;
	}
	public void setMessageCountFailure(Integer messageCountFailure) {
		this.messageCountFailure = messageCountFailure;
	}
	
}