/**
 * 
 */
package com.velos.services.model;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * An Event represents a Calendar event. Events 
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Event")
public class Event extends ServiceObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1534561290482142120L;
	protected EventIdentifier eventIdentifier;
	protected String eventName;
	protected String description;
	protected String CPTCode;
	protected Integer sequence;
	protected List<Cost> costs;
	protected OrganizationIdentifier facility;
	protected OrganizationIdentifier siteOfService;
	protected Code coverageType;
	protected Duration eventWindowBefore;
	protected Duration eventWindowAfter;
	protected String notes;
	protected String eventWindow;

	protected Date eventSuggestedDate;
	protected Date eventScheduledDate;
	protected EventStatus eventStatus;
	
	public EventIdentifier getEventIdentifier() {
		return eventIdentifier;
	}


	public void setEventIdentifier(EventIdentifier eventIdentifier) {
		this.eventIdentifier = eventIdentifier;
	}


	/**
	 * 
	 */
	public Event() {
		// TODO Auto-generated constructor stub
	}


	public String getEventName() {
		return eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCPTCode() {
		return CPTCode;
	}


	public void setCPTCode(String cPTCode) {
		CPTCode = cPTCode;
	}


	public Integer getSequence() {
		return sequence;
	}


	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}


	public List<Cost> getCosts() {
		return costs;
	}


	public void setCosts(List<Cost> costs) {
		this.costs = costs;
	}


	public OrganizationIdentifier getFacility() {
		return facility;
	}


	public void setFacility(OrganizationIdentifier facility) {
		this.facility = facility;
	}


	public OrganizationIdentifier getSiteOfService() {
		return siteOfService;
	}


	public void setSiteOfService(OrganizationIdentifier siteOfService) {
		this.siteOfService = siteOfService;
	}


	public Code getCoverageType() {
		return coverageType;
	}


	public void setCoverageType(Code coverageType) {
		this.coverageType = coverageType;
	}


	public Duration getEventWindowBefore() {
		return eventWindowBefore;
	}


	public void setEventWindowBefore(Duration eventWindowBefore) {
		this.eventWindowBefore = eventWindowBefore;
	}


	public Duration getEventWindowAfter() {
		return eventWindowAfter;
	}


	public void setEventWindowAfter(Duration eventWindowAfter) {
		this.eventWindowAfter = eventWindowAfter;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public String getEventWindow() {
		return eventWindow;
	}


	public void setEventWindow(String eventWindow) {
		this.eventWindow = eventWindow;
	}


	public Date getEventSuggestedDate() {
		return eventSuggestedDate;
	}


	public void setEventSuggestedDate(Date eventSuggestedDate) {
		this.eventSuggestedDate = eventSuggestedDate;
	}


	public Date getEventScheduledDate() {
		return eventScheduledDate;
	}


	public void setEventScheduledDate(Date eventScheduledDate) {
		this.eventScheduledDate = eventScheduledDate;
	}


	public EventStatus getEventStatus() {
		return eventStatus;
	}


	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	}


	


	
}
