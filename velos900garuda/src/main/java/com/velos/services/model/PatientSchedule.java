package com.velos.services.model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**model class for patient schedule. depicts
 *  a schedule a patient has, on a study
 * @author virendra
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PatientSchedule")
public class PatientSchedule extends ServiceObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public PatientSchedule(){}
	
	protected String patientId;
	protected StudyPatient studyPatient;
	//Virendra:Fixed #6061
	protected String scheduleName;
	protected ArrayList<Visit> visits;
	protected boolean isCurrent;
	
	
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public StudyPatient getStudyPatient() {
		return studyPatient;
	}
	public void setStudyPatient(StudyPatient studyPatient) {
		this.studyPatient = studyPatient;
	}

	public String getScheduleName() {
		return scheduleName;
	}
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}
	public ArrayList<Visit> getVisits() {
		return visits;
	}
	public void setVisits(ArrayList<Visit> visits) {
		this.visits = visits;
	}
	public boolean isCurrent() {
		return isCurrent;
	}
	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
}