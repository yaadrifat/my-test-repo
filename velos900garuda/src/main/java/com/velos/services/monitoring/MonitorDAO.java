/**
 * 
 */
package com.velos.services.monitoring;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.service.util.StringUtil;
import com.velos.eres.business.common.CommonDAO;
import com.velos.services.Issue;
import com.velos.services.OperationException;
import com.velos.services.model.NVPair;
import com.velos.services.model.StudySummary;
//import com.velos.services.study.MoreStudyDetailsDAO;

/**
 * @author dylan
 *
 */
public class MonitorDAO extends CommonDAO{


	/**
	 * Data Access Object related to database operations/methods of monitoring services.
	 */
	private static final long serialVersionUID = -3026533389581280946L;
	private static Logger logger = Logger.getLogger(MonitorDAO.class);


	private static final String SQL_MESSAGE_BY_DATE = 
		"select usersession.fk_user, usersession.ip_add, usersession.success_flag, " +
		" usersessionlog.pk_usl,  usersessionlog.usl_url, usr_logname," +
		" usersessionlog.usl_accesstime, usersessionlog.usl_urlparam,	usersessionlog.usl_modname " +
		" from er_usersession usersession, er_usersessionlog usersessionlog, er_user" +
		" where	usersessionlog.fk_usersession = usersession.pk_usersession and" +
		" usersession.fk_user = pk_user and" +
		" usersessionlog.usl_accesstime between ? and ? and" +
		" usersessionlog.usl_modname = ?";

	public static List<MessageLog> fetchMessageLogsByDateRange(
			Date fromDate, 
			Date toDate,
			String moduleName)
	throws OperationException{
		PreparedStatement pstmt = null;
		Connection conn = null;
		try
		{
			java.sql.Date toSQLDate = new java.sql.Date(toDate.getTime());
			java.sql.Date fromSQLDate = new java.sql.Date(fromDate.getTime());
			conn = getConnection();
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.monitoringMonitorDAO.fetchMessageLogsByDateRange sql:" + SQL_MESSAGE_BY_DATE);

			pstmt = conn.prepareStatement(SQL_MESSAGE_BY_DATE);
			pstmt.setDate(1, fromSQLDate);
			pstmt.setDate(2, toSQLDate);
			pstmt.setString(3, moduleName);
			ResultSet rs = pstmt.executeQuery();
			List<MessageLog> messages = new ArrayList<MessageLog>();

			while (rs.next()) {
				String messageId = StringUtil.integerToString(rs.getInt("pk_usl"));
//				String usr_logname = rs.getString("usr_logname");
				Integer fk_user = rs.getInt("fk_user");
				String ip_add = rs.getString("ip_add");
				//String usl_url = rs.getString("usl_url");
				java.sql.Date usl_accesstime = rs.getDate("usl_accesstime");
				String usl_urlparam = rs.getString("usl_urlparam");
				//String usl_modname = rs.getString("usl_modname");
				
				MessageLog messageLog = new MessageLog();
				messageLog.setModule(moduleName);
				messageLog.setMessageId(messageId);
				messageLog.setIdentity(fk_user);
				messageLog.setOperation(usl_urlparam);
				messageLog.setRoute(MessageLog.ROUTE_INCOMING);
				messageLog.setTime(usl_accesstime);
				messageLog.setEndpoint(ip_add);
				
				messages.add(messageLog);
			}
			return messages;
		}
		catch (SQLException ex) {
			logger.error("com.velos.services.monitoringMonitorDAO.fetchMessageLogsByDateRange EXCEPTION IN FETCHING FROM er_studystatus"
					+ ex);
			throw new OperationException(ex);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}

	}
	/**
	 * Gets the runtime status, returns true if database connection active,
	 * else false(if inactive)
	 * @return
	 * @throws OperationException
	 */
	public static Boolean getRuntimeStatus() throws OperationException{
		Connection conn= null;
		Boolean isRuntimeStatus = false;
		try {
			conn = getConnection();
			if(conn != null){
				isRuntimeStatus = true;
			}
			if (logger.isDebugEnabled()) logger.debug("com.velos.services.monitoringMonitorDAO.getRuntimeStatus():getConnection()" + conn);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isRuntimeStatus = false;
		}
		finally {
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return isRuntimeStatus;
		
	} 
	
}

