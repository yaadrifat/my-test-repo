package com.velos.services.util;

public class JNDINames {
	public final static String prefix = "velos/";
	public final static String suffix = "/remote";
	
	public final static String MessageServiceImpl = prefix+"MessageServiceImpl"+suffix;
	public final static String ObjectMapServiceImpl = prefix+"ObjectMapServiceImpl"+suffix;
	public final static String SessionServiceImpl = prefix+"SessionServiceImpl"+suffix;
	public final static String StudyServiceImpl = prefix+"StudyServiceImpl"+suffix;
}
