/**
 * Created On Jun 8, 2011
 */
package com.velos.services.calendar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
/*import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.eventcost.impl.EventcostBean;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventcostAgent.EventcostAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
*/import com.velos.services.AbstractService;
import com.velos.services.CodeNotFoundException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.Cost;
import com.velos.services.model.Costs;
import com.velos.services.model.Duration;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**Helper class to facilitate operations for Event
 * in a Study Calendar
 * @author Kanwaldeep
 *
 */
public class CalendarEventHelper {
	
	private static Logger logger = Logger.getLogger(CalendarEventHelper.class); 
	/**
	 * 
	 * @param studyCalendarEvents
	 * @param calendarPK
	 * @param visitPK
	 * @param visitDisplacement
	 * @param parameters
	 * @throws OperationException
	 */
	public void addEventsToVisit(CalendarEvents studyCalendarEvents,
			Integer calendarPK, Integer visitPK, String visitDisplacement, Map<String, Object> parameters) throws OperationException {
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder"); 
		//EventdefAgentRObj  eventDefAgent= (EventdefAgentRObj) parameters.get("EventdefAgentRObj"); 
		//EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService"); 
		
		//look for duplicate event names from request
		List<String> eventsToAdd = new ArrayList<String>(); 
		
		
		int costMax = 0; 
		
		int maxSequence = 0; 
		
		for (CalendarEvent calendarEvent : studyCalendarEvents.getEvent()) {
			//Integer maxSequenceEvent = eventDefAgent.getMaxSeqForVisitEvents(
				//	visitPK, calendarEvent.getEventName(), "event_assoc");
			/*if (maxSequenceEvent == -3) {
				response.getIssues().add(
						new Issue(IssueTypes.STUDY_CALENDAR_ERROR_CREATE_EVENT,
								"Event already exists in the visit "));
				throw new OperationException();
				
			}*/
			
			if(eventsToAdd.contains(calendarEvent.getEventName().toUpperCase()))
			{
				Issue issue = new Issue(IssueTypes.DUPLICATE_EVENT_NAME, "For Event Name : " + calendarEvent.getEventName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
				
			}
			
			eventsToAdd.add(calendarEvent.getEventName().toUpperCase()); 
			
			
		//	if(maxSequence == 0)maxSequence =  maxSequenceEvent; 
			
			//EventAssocBean eventAssocBean = new EventAssocBean();
			/*try {
				StudyCalendarDAO dao = new StudyCalendarDAO(); 
				int costID = dao.getCostIDforEventPool(calendarEvent.getEventName(), calendarPK); 
				if(costID == 0 )// not in event pool for Calendar add to pool
				{
					EventAssocBean poolEventAssocBean = new EventAssocBean(); 
					if(costMax == 0)// just call once when we are adding events 
					{
						costMax = eventAssocAgent.getMaxCost(EJBUtil.integerToString(calendarPK)); 
					}
					calendarEventIntoBean(calendarEvent, poolEventAssocBean, calendarPK, null, true, EJBUtil.integerToString(costMax++), null, parameters,0); 
					eventAssocAgent.setEventAssocDetails(poolEventAssocBean); 
					costID = costMax; 
				}
				calendarEventIntoBean(calendarEvent, eventAssocBean,
						calendarPK, visitPK, true, EJBUtil.integerToString(costID),visitDisplacement, parameters, maxSequence);
			} catch (MultipleObjectsFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer eventPK = 0;
			eventPK = eventAssocAgent.setEventAssocDetails(eventAssocBean);
*/
			/*if (eventPK == -2) {
				response.getIssues().add(
						new Issue(
								IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT,
								"Error creating event"));
				throw new OperationRolledBackException(response.getIssues());
			}*/
			//objectMapService.getOrCreateObjectMapFromPK(
			//		ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, eventPK);
			// Integer eventPK = eventAssocBean.getEvent_id();
			if (calendarEvent.getCosts() != null) {
				boolean isCostsAddedToEvent = false;
				Costs eventCosts = calendarEvent.getCosts();
				//isCostsAddedToEvent = addEventCostsToEvent(eventCosts, eventPK,
				//		true, parameters);
				if (!isCostsAddedToEvent) {
					response.getIssues()
							.add(new Issue(
									IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT_COST,
									"Error creating event costs"));
					throw new OperationRolledBackException(response.getIssues());
				}
			}
		}
	}
	/**
	 * 
	 * @param calendarEvent
	 * @param persistEventAssocBean
	 * @param calendarPK
	 * @param visitPK
	 * @param isNew
	 * @param costID
	 * @param visitDisplacement
	 * @param parameters
	 * @param maxSequence
	 * @throws OperationException
	 * @throws MultipleObjectsFoundException
	 */
	/*private void calendarEventIntoBean(CalendarEvent calendarEvent,
			EventAssocBean persistEventAssocBean,Integer calendarPK ,Integer visitPK, boolean isNew, String costID, String visitDisplacement,Map<String, Object> parameters, int maxSequence) throws OperationException, MultipleObjectsFoundException {
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder"); 
		persistEventAssocBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService"); 
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		EventAssocAgentRObj eventAssocAgent = (EventAssocAgentRObj) parameters.get("EventAssocAgentRObj");
		
		
		if (isNew){ 
			persistEventAssocBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		}
		persistEventAssocBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
		//kanwal added to fix cost and displacement
		persistEventAssocBean.setDisplacement(visitDisplacement); 
		persistEventAssocBean.setCost(costID); 
		//kanwal added eventDuration 
		TimeUnits durationUnit = TimeUnits.DAY; 
		Integer durationValue = 0; 
		
		Duration eventDuration = calendarEvent.getEventDuration();
		if(eventDuration != null){
			durationUnit = eventDuration.getUnit();
			durationValue = eventDuration.getValue();

			if(durationUnit != null && durationValue != null ){

				if(!durationUnit.equals(TimeUnits.DAY)){

					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter event duration after unit as 'DAY'"));
					throw new OperationException();

				}				
			}
		}
		
		String encodedUnit = Duration.encodeUnit(durationUnit);
		persistEventAssocBean.setDuration(EJBUtil.integerToString(durationValue));
		persistEventAssocBean.setDurationUnit(encodedUnit);
		
		
		//Hardcoded for event type
		persistEventAssocBean.setEvent_type(CodeCache.CODE_EVENT_TYPE);
		persistEventAssocBean.setEventVisit(EJBUtil.integerToString(visitPK));
		
		persistEventAssocBean.setCptCode(calendarEvent.getCPTCode());
		
		
		persistEventAssocBean.setChain_id(EJBUtil.integerToString(calendarPK));
		if(calendarEvent.getCoverageType() != null)
		{
			Integer coverageTypePk = 0;
			//CodeCache codes = CodeCache.getInstance();
			try{
				coverageTypePk = AbstractService.dereferenceSchCode(calendarEvent.getCoverageType(), CodeCache.CODE_TYPE_COVERAGE_TYPE, callingUser);
				persistEventAssocBean.setEventCoverageType(EJBUtil.integerToString(coverageTypePk));
			}catch(CodeNotFoundException e){
				response.getIssues().add(
						new Issue(
								IssueTypes.CODE_NOT_FOUND, 
								"CoverageType code not found"));
				throw new OperationException();
			}
		
		
			if(coverageTypePk == null || coverageTypePk == 0 ){
				response.getIssues().add(
						new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"CoverageType code not found"));
				throw new OperationException();
			
			}
		}
		persistEventAssocBean.setDescription(calendarEvent.getDescription());
		
		//EventIdentifier eventIdentifier = calendarEvent.getEventIdentifier();
		
		persistEventAssocBean.setName(calendarEvent.getEventName());
		//Set Defaults 
		TimeUnits durationUnitAfter = TimeUnits.DAY; 
		Integer durationValueAfter = 0; 
		
		Duration eventDurationAfter = calendarEvent.getEventWindowAfter();
		if(eventDurationAfter != null){
			durationUnitAfter = eventDurationAfter.getUnit();
			durationValueAfter = eventDurationAfter.getValue();

			if(durationUnitAfter != null && durationValueAfter != null ){

				if(!durationUnitAfter.equals(TimeUnits.DAY)){

					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter event duration after unit as 'DAY'"));
					throw new OperationException();

				}				
			}
		}
		
		String encodedUnitAfter = Duration.encodeUnit(durationUnitAfter);
		persistEventAssocBean.setFuzzyAfter(EJBUtil.integerToString(durationValueAfter));
		persistEventAssocBean.setDurationUnitAfter(encodedUnitAfter);
		
		TimeUnits durationUnitBefore = TimeUnits.DAY; 
		int durationValueBefore = 0; 
		
		Duration eventDurationBefore = calendarEvent.getEventWindowBefore();
		if(eventDurationBefore != null){
			durationUnitBefore = eventDurationBefore.getUnit();
			durationValueBefore = eventDurationBefore.getValue(); 
			if(durationUnitBefore != null){
				//Integer durationValueBefore = eventDurationBefore.getValue();

				if(!durationUnitBefore.equals(TimeUnits.DAY)){

					response.getIssues().add(
							new Issue(
									IssueTypes.DATA_VALIDATION, 
									"Please enter event duration Before unit as 'DAY'"));
					throw new OperationException();

				}
				
			}
		}
		
		String encodedUnitBefore = Duration.encodeUnit(durationUnitBefore);
		persistEventAssocBean.setDurationUnitBefore(encodedUnitBefore);
		persistEventAssocBean.setFuzzy_period(EJBUtil.integerToString(durationValueBefore)); 
		
		OrganizationIdentifier organizationIdentifier = calendarEvent.getFacility();
		if(organizationIdentifier != null)
		{
			Integer eventSitePK = 0;
			try {
				eventSitePK = ObjectLocator.sitePKFromIdentifier(callingUser, organizationIdentifier, sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(eventSitePK == 0 || eventSitePK == null){

				response.getIssues().add(
						new Issue(
								IssueTypes.ORGANIZATION_NOT_FOUND, 
						"Event site not found"));
				throw new OperationException();

			}
			persistEventAssocBean.setEventFacilityId(EJBUtil.integerToString(eventSitePK));
		}
		persistEventAssocBean.setNotes(calendarEvent.getNotes());
		Integer sequence = calendarEvent.getSequence();
		//Virendra added for shuffling eventSequence 
		if(visitPK != null){
//			Integer maxSequence = eventAssocAgent.getMaxSeqForVisitEvents(visitPK, calendarEvent.getEventName(), "event_assoc");
			
		if(sequence == null || sequence== 0 || sequence > maxSequence ){
			persistEventAssocBean.setEventSequence(EJBUtil.integerToString(++maxSequence));
		}
		else{
			if( sequence <= maxSequence){
				persistEventAssocBean.setEventSequence(EJBUtil.integerToString(sequence));
				StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
				
				//Integer eventIdForMaxSeqEvent = studyCalendarDAO.getEventIdForVisitFromSequence(visitPK, maxSequence, "event_assoc");
				for(int i =sequence ; i <= maxSequence; i++){
					Integer eventId = studyCalendarDAO.getEventIdForVisitFromSequence(visitPK, i, "event_assoc");
					EventAssocBean bean = eventAssocAgent.getEventAssocDetails(eventId);
					bean.setEventSequence(EJBUtil.integerToString(i+1));
					bean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
					eventAssocAgent.updateEventAssoc(bean);
				}
				
			}
		}
		}
	
		OrganizationIdentifier siteOfService = calendarEvent.getSiteOfService();
		if(siteOfService != null)
		{
			Integer eventSiteOfServicePK = 0;
			try {
				eventSiteOfServicePK = ObjectLocator.sitePKFromIdentifier(callingUser, siteOfService, sessionContext, objectMapService);
			} catch (MultipleObjectsFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(eventSiteOfServicePK == 0 || eventSiteOfServicePK == null){

				response.getIssues().add(
						new Issue(
								IssueTypes.ORGANIZATION_NOT_FOUND, 
						"Event site of service not found"));
				throw new OperationException();
			}
		}
	}*/
	/**
	 * 
	 * @param eventCosts
	 * @param eventPK
	 * @param isNew
	 * @param parameters
	 * @return
	 * @throws OperationException
	 */

	private boolean addEventCostsToEvent(Costs eventCosts,
			Integer eventPK, boolean isNew, Map<String, Object> parameters) throws OperationException {
		boolean isEventCostAdded =  false;
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder");
		ObjectMapService objectMapService = (ObjectMapService) parameters.get("ObjectMapService"); 
		SessionContext sessionContext = (SessionContext) parameters.get("sessionContext"); 
		//EventcostAgentRObj eventCostAgent =(EventcostAgentRObj) parameters.get("EventcostAgentRObj"); 
		try {
			for (Cost eventCost : eventCosts.getCost()) {
				//EventcostBean persistEventCostBean = new EventcostBean();
				//calendarEventCostIntoBean(persistEventCostBean, eventCost, eventPK,
						//true, parameters);
			//	Integer intCalendarEventCostPK = eventCostAgent.setEventcostDetails(persistEventCostBean);
				
				/*if(intCalendarEventCostPK == -2){
					sessionContext.setRollbackOnly();
					response.getIssues().add(new Issue(IssueTypes.ERROR_CREATING_STUDY_CALENDAR_EVENT_COST));
					throw new OperationException("Error Creating Study Calendar event cost");
					
				}*/
				//ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT_COST, intCalendarEventCostPK);
			}
			isEventCostAdded = true;
		} catch(Throwable t){
			sessionContext.setRollbackOnly();
		//	addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return isEventCostAdded;
			
	}
	/**
	 * 
	 * @param persistEventCostBean
	 * @param eventCost
	 * @param eventPK
	 * @param isNew
	 * @param parameters
	 * @throws OperationException
	 */
	/*private void calendarEventCostIntoBean(EventcostBean persistEventCostBean, Cost eventCost, Integer eventPK, boolean isNew, Map<String, Object> parameters) throws OperationException{
		ResponseHolder response = (ResponseHolder) parameters.get("ResponseHolder");
		persistEventCostBean.setIpAdd(AbstractService.IP_ADDRESS_FIELD_VALUE);
		UserBean callingUser = (UserBean) parameters.get("callingUser");
		if (isNew){ 
			persistEventCostBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		}
		persistEventCostBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
		persistEventCostBean.setEventId(EJBUtil.integerToString(eventPK));
		persistEventCostBean.setEventcostValue((eventCost.getCost()).toString());
		
		Integer costDescriptionTypePk= 0;
		try{
			costDescriptionTypePk = AbstractService.dereferenceSchCode(eventCost.getCostType(), CodeCache.CODE_TYPE_COST_DESC, callingUser);
			persistEventCostBean.setEventcostDescId(EJBUtil.integerToString(costDescriptionTypePk));
			
		}catch(CodeNotFoundException e){
			response.getIssues().add(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Cost description type code not found"));
			throw new OperationException();
		}
		
		Integer costCurrencyPK = 0;
		try{
			costCurrencyPK = AbstractService.dereferenceSchCode(eventCost.getCurrency(), CodeCache.CODE_TYPE_CURRENCY, callingUser);
			persistEventCostBean.setCurrencyId(EJBUtil.integerToString(costDescriptionTypePk));
			
		}catch(CodeNotFoundException e){
			response.getIssues().add(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Currency code not found"));
			throw new OperationException();
		}
	}*/
	
	

}
