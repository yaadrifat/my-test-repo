package com.velos.services.client;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
//import com.velos.services.studypatient.StudyPatientService;
/***
 * Client Class/Remote object for StudyPatient Services 
 * @author Virendra
 *
 */
public class StudyPatientClient{
	/**
	 * Invokes the remote object.
	 * @return remote StudyPatient service
	 * @throws OperationException
	 */
	
	/*private static StudyPatientService getStudyPatientRemote()
	throws OperationException{
		
	StudyPatientService StudyPatientRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		StudyPatientRemote =
			(StudyPatientService) ic.lookup(StudyPatientService.class.getName());
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return StudyPatientRemote;
	}*/
	/**
	 * Calls getStudyPatient on getStudyPatient Remote object
	 * to return List of StudyPatients 
	 * @param studyId
	 * @return List<StudyPatient>
	 * @throws OperationException
	 */
	/*public static List<StudyPatient> getStudyPatients(StudyIdentifier studyId) 
	throws OperationException{
		StudyPatientService studyPatientService = getStudyPatientRemote();
		return studyPatientService.getStudyPatients(studyId);
	}*/
}