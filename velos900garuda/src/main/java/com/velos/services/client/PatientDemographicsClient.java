package com.velos.services.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
//import com.velos.services.patientdemographics.PatientDemographicsService;
/***
 * Client Class/Remote object for PatientDemographics Services 
 * @author Virendra
 *
 */
public class PatientDemographicsClient{
	/**
	 * Invokes the remote object.
	 * @return remote PatientDemographics service
	 * @throws OperationException
	 */
	
	/*private static PatientDemographicsService getPatientDemoRemote()
	throws OperationException{
		
		PatientDemographicsService PatientDemoRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		PatientDemoRemote =
			(PatientDemographicsService) ic.lookup(PatientDemographicsService.class.getName());
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return PatientDemoRemote;
	}*/
	/**
	 * Client method calls PatientDemographics Service 
	 * to get Patient Demographics object
	 * @param patientId
	 * @return
	 * @throws OperationException
	 */
	/*public static PatientDemographics getPatientDemographics(PatientIdentifier patientId) 
	throws OperationException{
		ResponseHolder response = new ResponseHolder();
		//PatientDemographicsService patientDemoService = getPatientDemoRemote();
		if(patientDemoService == null){
			Issue issue = new Issue(IssueTypes.EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES, 
					"Exception getting remote patient demographics service:"+patientDemoService);
			response.addIssue(issue);
			throw new OperationException();
		}
		return patientDemoService.getPatientDemographics(patientId);
	}*/
}