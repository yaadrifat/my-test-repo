/**
 * 
 */
package com.velos.services.client;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * @author dylan
 *
 */
public class TestCallbackHandler implements CallbackHandler {

	/* (non-Javadoc)
	 * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
	 */
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		for (Callback callback : callbacks){
			 if (callback instanceof NameCallback){
				 ((NameCallback)callback).setName("duketest");
			 }
			 if (callback instanceof PasswordCallback){
				 ((PasswordCallback)callback).setPassword("velos123".toCharArray());
			 }
		}

	}

}
