/**
 * 
 */
package com.velos.services;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CRUDAction")
public enum CRUDAction{
	CREATE,
	RETRIEVE,
	UPDATE,
	REMOVE,
	
}