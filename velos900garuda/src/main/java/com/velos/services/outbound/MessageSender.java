package com.velos.services.outbound;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jms.JMSException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;



/**The MessageSender class handles sending messages to JMS Topics and updating database accordingly. The MessageSender class implements Runnable - Multiple  messages can be sent at a time through multiple threads.
 * @author Kanwaldeep
 *
 */
public class MessageSender implements Runnable {
	private static Logger logger = Logger.getLogger(MessageSender.class); 
	private static boolean debugEnabled = logger.isDebugEnabled(); 
	private Changes changesMessage; 
	private List<Integer> pkList;
	private MessagePublisher publisher; 
	private Class[] classesToBound;  
	private boolean isMessageSent = false; 
	
	
	/**
	 * @param changesMessage
	 * @param pkList
	 * @param publisher
	 * @param classesToBound
	 */
	public MessageSender(Changes changesMessage, List<Integer> pkList, MessagePublisher publisher, Class[] classesToBound){
		this.changesMessage = changesMessage; 
		this.pkList = pkList; 
		this.publisher = publisher;
		this.classesToBound = classesToBound;
		
	}

	public void run() {
		// Set processed flag to in- process
		MessagingDAO dao = new MessagingDAO(); 
		dao.updateStatus(MessagingConstants.MESSAGE_STATUS_IN_PROCESS, pkList);
		// sendMessage	to Topic
		String errorMessage =""; 
		boolean moveToHistory = true; 
		int processedflag = 0; 
		try{
				sendMessage(publisher, changesMessage, classesToBound);
				//update status to success
				processedflag = MessagingConstants.MESSAGE_STATUS_PROCESSED; 
		}catch(OutBoundException oe)
		{
			
			if(oe.getErrorCode() < MessagingConstants.OUTBOUND_EXCEPTION_CONTROLLER_LIMIT)
			{
				// We still want to keep this message in Queue so that it can be processed in next Batch
					moveToHistory = false; 
					dao.updateStatus(MessagingConstants.MESSAGE_STATUS_RE_PROCESS, pkList); 
			}else{
				//For Failed messages from exceptions which cannot be recovered.
				//update status to failure
				processedflag = MessagingConstants.MESSAGE_STATUS_ERRORED_OUT; 
				logger.error("Message Sent failed ", oe); 
				errorMessage = oe.getMessage(); 				
			}
			
		}		
		catch(Exception e)
		{
			//update status to failure
			processedflag = MessagingConstants.MESSAGE_STATUS_ERRORED_OUT; 
			logger.error("Message Sent failed ", e); 
			errorMessage = e.getMessage(); 
			
		}
		// move records from table to history table
		if(moveToHistory) dao.moveToHistoryTable(pkList, errorMessage, processedflag); 
			
	}
	
	
	/**This sends message to Topic specified by MessagePublisher.
	 * @param publisher - {@link com.velos.services.outbound.MessagePublisher} 
	 * @param changesMessage - {@link com.velos.services.outbound.Changes}
	 * @param classesToBound - Class[] array for JAXBContext.
	 */
	public void sendMessage(MessagePublisher publisher, Changes changesMessage,Class[] classesToBound) throws OutBoundException
	{
		
			    try {			    
			   		JAXBContext context = JAXBContext.newInstance(classesToBound);			
				    Marshaller m = context.createMarshaller();
				    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				    StringWriter s = new StringWriter(); 
					m.marshal(changesMessage, s); 
					if(debugEnabled) logger.debug("OutBound Message generated " + s); 
					publisher.publishMessage(s.toString()); 
					isMessageSent = true; 				
				} catch (JAXBException e) {
					logger.error("Failed to generate XML ", e); 
					throw new OutBoundException(MessagingConstants.UNABLE_TO_MARSHALL_TO_XML, e.getMessage())	; 	
				}
		
		
		
	}
	
	/**
	 * @return boolean value to indicate whether message is sent successfully or not.
	 */
	public boolean isMessageSentSuccessfully()
	{
		return isMessageSent; 
	}
	
	
	
	

}
