/**
 * Created On Mar 29, 2011
 */
package com.velos.services.outbound;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;


import com.velos.eres.business.common.CommonDAO;

/**
 * @author Kanwaldeep
 *
 */
public class MessagingIdentifierHelperDAO extends CommonDAO{
	private static Logger logger = Logger.getLogger(MessagingIdentifierHelperDAO.class); 
	
	private String GET_STUDY_SQL = "select study_number from er_study where pk_study = ?"; 
	
	public String getStudyNumber(int pkStudy)
	{
		PreparedStatement stmt = null; 
		ResultSet rs = null; 
		Connection conn = null ;
		String studyNumber = null; 
		try
		{
			conn = getConnection(); 
			stmt = conn.prepareStatement(GET_STUDY_SQL); 
			stmt.setInt(1, pkStudy); 
			
			rs = stmt.executeQuery(); 
			
			if(rs.next())
			{
				studyNumber = rs.getString("study_number"); 
			}
			
		}catch(SQLException sqe)
		{
			logger.error("Unable to get Study Number for study "+sqe); 
		}
		
		return studyNumber; 
	}

}
