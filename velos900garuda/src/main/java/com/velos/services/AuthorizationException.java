/**
 * 
 */
package com.velos.services;


/**
 * @author dylan
 *
 */
public class AuthorizationException extends OperationException{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6872583670816723920L;

	public AuthorizationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuthorizationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public AuthorizationException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public AuthorizationException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}


}
