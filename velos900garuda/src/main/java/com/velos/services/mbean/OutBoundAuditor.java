/**
 * Created On Mar 31, 2011
 */
package com.velos.services.mbean;

import org.jboss.system.ServiceMBeanSupport;

import com.velos.services.outbound.Auditor;

/**
 * @author Kanwaldeep
 *
 */
public class OutBoundAuditor extends ServiceMBeanSupport implements OutBoundAuditorMBean{
	
	 public void startService() throws Exception {
	       Auditor.startAuditor(); 	 
	  } 
	 
	 public void stopService() throws Exception{
		 Auditor.stopAuditor(); 
	 }

}
