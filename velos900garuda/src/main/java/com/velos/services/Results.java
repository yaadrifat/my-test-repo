package com.velos.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class Results implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = -1079172409451215587L;
    protected List<CompletedAction> result = new ArrayList<CompletedAction>();
 
    public Results() {}
    
    public void addAction(CompletedAction action){
        this.result.add(action);
    }
    
    public void addAll(List<CompletedAction> action) {
        this.setResult(action);
    }
    
    public List<CompletedAction> getActions() {
        return this.getResult();
    }
    
    public List<CompletedAction> getResult() {
        return this.result;
    }
    
    public void setResult(List<CompletedAction> result) {
        this.result = result;
    }
    
}
