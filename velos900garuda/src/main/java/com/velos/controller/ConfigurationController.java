/*
$Id: ConfigurationController.java,v 1.1 2008/01/11 19:39:13 vabrol Exp $ */
package com.velos.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.velos.eres.business.common.BrowserDao;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.StringUtil;

import java.util.ArrayList;

public class ConfigurationController extends HttpServlet {

	private ServletContext context;
	private ArrayList settingNames;
	private ArrayList settingValues;

	public void init(ServletConfig config) throws ServletException {
		this.context = config.getServletContext();
		settingNames=new ArrayList();
		settingValues=new ArrayList();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String retVal="";
		
		
		String module=request.getParameter("module");
		module=(module==null)?"":module;
		
		module=module.toLowerCase().trim();
		
		//key type can have get,save,delete				
		String reqType = request.getParameter("reqType");
		reqType = (reqType == null) ? "" : reqType;
		
		reqType=reqType.toLowerCase().trim();
		
		
		String reqKey = request.getParameter("reqKey");
		reqKey = (reqKey == null) ? "" : reqKey;
		
		reqKey=reqKey.toLowerCase().trim();
		
		if (reqType.startsWith("save"))
		{
			if (reqKey.equals("bsetting")) {	
		 retVal=new Integer(saveSettings(module,request)).toString();
			} 
			if (reqKey.equals("search")) {	
				 retVal=new Integer(saveSearch(module,request)).toString();
					}
		 
		}
		if (reqType.startsWith("get"))
		{
		if (reqKey.equals("search")) {	
		 retVal=getSearch(module,request);
		} 
			
		}
		if (reqType.startsWith("delete"))
		{
		if (reqKey.equals("search")) {	
		 retVal=new Integer(deleteSearch(module,request)).toString();
		} 
			
		}
				
		response.getWriter().write(retVal);
		
			
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		this.doGet(request, response);
	}
	
	private String getBrowserConfig()
	{
		return "{colArray:[\"PERSON_CODE\",\"PLNAME\",\"PERSON_DOB\",\"PERSON_GENDER\",\"PERSON_STATUS\",\"PK_PERSON\"]," +
				"colSet:[key:\"PERSON_CODE\", label:\"Person Code\",formatter:myCustomFormatter, sortable:true, resizeable:true},"+
                               " {key:\"PFNAME\", label:\"Person First Name\", sortable:true, resizeable:true, hideable:true, className:'hide'},"+
                               " {key:\"PLNAME\", label:\"Person Last Name\", sortable:true, resizeable:true, hideable:true},"+
                                 "{key:\"PERSON_DOB\", text:\"Date of Birth\", label:\"DOB\", sortable:true, resizeable:true, hideable:true},"+
                                 " {key:\"PERSON_GENDER\", text:\"Gender\" ,label:\"Gender\", sortable:true, resizeable:true, hideable:true},"+
                               " {key:\"PERSON_STATUS\",text:\"Patient Status\", label:\"Patient Status\", sortable:true, resizeable:true, hideable:true}"+
                            "];}";
	}
	
	private int saveSettings(String module,HttpServletRequest request)
	{
		String settingName;
		String settingValue;
		
		this.reset();
		
		String userId=request.getParameter("userId");
		userId=StringUtil.trueValue(userId);
		
		
		for (Enumeration attrEnm=request.getParameterNames();attrEnm.hasMoreElements();)
		{
		  settingName=((String)attrEnm.nextElement());
		  if (settingName.toLowerCase().startsWith(module)) {
			  settingNames.add(settingName);
			  settingValue=StringUtil.trueValue((String)request.getParameter(settingName));
			  settingValues.add(settingValue);
		  }
		}
		BrowserDao bDao=new BrowserDao();
		bDao.setBrowserSettings(EJBUtil.stringToNum(userId), "U", settingNames, settingValues);
		
		return 1;
	}
	
	private int saveSearch(String module,HttpServletRequest request)
	{
		int retVal=0;
		
		String isDefault=StringUtil.trueValue(request.getParameter("default"));
		isDefault=(isDefault.length()==0)?"N":isDefault;
		
		String name=StringUtil.trueValue(request.getParameter("name"));
		
		String criteria=StringUtil.trueValue(request.getParameter("criteria"));
		
		String userId=StringUtil.trueValue(request.getParameter("userId"));
		
		 
		try{
		BrowserDao bDao=new BrowserDao();
		retVal=bDao.saveSearch(module, name, criteria, isDefault, userId,"I",0);
		}
		catch(Exception e) {
			e.printStackTrace();
			return -1;
		}
		return retVal;
	}
	private int deleteSearch(String module,HttpServletRequest request){
		int retVal=0;
		
		String userId=StringUtil.trueValue(request.getParameter("userId"));
		String name=StringUtil.trueValue(request.getParameter("name"));
		try{
			BrowserDao bDao=new BrowserDao();
			retVal=bDao.deleteSearch(module, name,userId,0);
			}
			catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		return retVal;
		
	}
	
	private String getSearch(String module,HttpServletRequest request){
		String retVal="";
		String userId=StringUtil.trueValue(request.getParameter("userId"));
		try{
			BrowserDao bDao=new BrowserDao();
			bDao.getBrowserSavedSearch(module,userId);
			retVal=bDao.createBrowserSavedSearchDD(module+"_search id="+module+"_search ");
			
			}
			catch(Exception e) {
				e.printStackTrace();
				return "";
			}
			return retVal;
		
	}
	
	
	private void reset()
	{
		settingNames.clear();
		settingValues.clear();
	}

}
