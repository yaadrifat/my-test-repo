package com.velos.base;

/**
 * An Interface Service.
 */
public abstract class InterfaceService implements Runnable {

    private boolean keepRunning;

    /** Creates a new instance of Server */
    public InterfaceService() {

    }

    public abstract void run();

    /**
     * Returns true if the thread should continue to run, false otherwise (ie if
     * stop() has been called).
     */
    protected boolean keepRunning() {
        return keepRunning;
    }

    /**
     * Starts the server listening for connections in a new thread. This
     * continues until <code>stop()</code> is called.
     */
    public void start() {
        Thread thd = new Thread(this);
        this.keepRunning = true;
        thd.start();
    }

    /**
     * Stops the server from listening for new connections, and closes all
     * existing Connections.
     */
    public void stop() {
        this.keepRunning = false;

    }

    public void sleep() {
        Thread thd = new Thread(this);
        try {
            thd.sleep(3000);
        } catch (Exception e) {
        }
    }

}
