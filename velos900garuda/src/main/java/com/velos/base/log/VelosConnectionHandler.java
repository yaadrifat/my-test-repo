
package com.velos.base.log;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

import org.apache.log4j.jdbcplus.JDBCPoolConnectionHandler;

import com.velos.base.dbutil.DBEngine;

 
public class VelosConnectionHandler implements JDBCPoolConnectionHandler {
  Connection con = null;
  DBEngine dbEngine=null;

  

  public Connection getConnection() {
      DBEngine.getEngine();
      con=dbEngine.getConnection("logger");
      System.out.println("Return Connection"+con);
      return con;
  }

  public Connection getConnection(String _url, String _username, String _password) {
    try {
        DBEngine.getEngine();
        getConnection();
        
    } catch (Exception e) {
      e.printStackTrace();
    }

    return con;
  }
  
  public void freeConnection(Connection conn) {
      dbEngine.returnConnection(conn);
      
  }
}

