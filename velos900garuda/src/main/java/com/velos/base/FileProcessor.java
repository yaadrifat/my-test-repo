package com.velos.base;

import java.sql.Connection;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.velos.base.dbutil.DBEngine;

/**
 * FileProcessor handles all the parser/extension retrieval for an event and
 * route the data set appropriately. for failed records, messages are logged to
 * .bad file.
 * 
 * @author Vishal Abrol
 * @version 1.0
 */
public class FileProcessor {
    private static Logger logger = Logger.getLogger(FileProcessor.class
            .getName());
    private static Logger log_cat = Logger.getLogger("log_cat");
    String encode = "";

    String eventType = "";

    String filePath = "";

    String fileName = "";

    String dlm = ";";

    InterfaceConfig config = null;

    VelosIO velIO = null;

    String badFilePath = "";

    String fileHeader = "";
    DataHolder dataHolder=DataHolder.getDataHolder();
    
    public FileProcessor() {
        logger.debug("File Processor Constructed but no values specified.");
    }

    /**
     * Constructor
     * 
     * @param String -
     *            encoding used in the data set
     * @param event -
     *            event for the current data set.
     * @param <code>InterfaceConfig</code> contains all the configuration
     * @param string -
     *            dataset as a File Path as a string
     * @param String
     *            -fileName to process.
     * 
     */
    public FileProcessor(String encode, String event, InterfaceConfig config,
            String filePath, String fileName) {
        this.encode = encode;
        this.eventType = event;
        this.config = config;
        this.fileName = fileName;
        this.filePath = filePath;
        logger.info("File Processor Initialized");
    }

    /**
     * Initialize the FileProcessor with specified parameters
     * 
     * @param String -
     *            encoding used in the data set
     * @param event -
     *            event for the current data set.
     * @param <code>InterfaceConfig</code> contains all the configuration
     * @param string -
     *            dataset as a File Path as a string
     * @param String
     *            -fileName to process.
     */
    public void Initialize(String encode, String event, InterfaceConfig config,
            String filePath, String fileName) {
        this.encode = encode;
        this.eventType = event;
        this.config = config;
        this.fileName = fileName;
        this.filePath = filePath;
        logger.info("File Processor Initialized and values set.");
    }

    /**
     * Method parses the dataset with appropriate Parser specified, send the
     * data elements to implemented Application extension.
     */
    public synchronized int processFile() {
        Parser parser = null,validateParser=null;
        ArrayList msgList = new ArrayList();
        Application app = null;
        int counter = 0;
        String encoding = "", enclosed = "", header = "";
        DefaultApplication defaultApp = new DefaultApplication();
        int out = 0;
        // this.encode=encode;
        // this.eventType=event;
        logger.debug("Delegating request for Database Connection");
        Connection conn = DBEngine.getConnection();
        if (conn == null) {
            dataHolder.setMSG_LEVEL("BAD");
            logger.info("Unable to establish connection to the database."
                    + "Please check the connection settings.");
            dataHolder.setMSG_LEVEL("");
            return -3;
        }
        logger.debug("Connection received in FilePrcoessor" + conn);
        // config.LoadEvent(eventType);
        encoding = config.getEncoding();
        encoding = (encoding == null) ? "" : encoding;
        enclosed = config.getEnclosed();
        enclosed = (enclosed == null) ? "" : enclosed;
        badFilePath = config.getBadPath();
        badFilePath = (badFilePath.length() > 0) ? badFilePath
                + VelosIO.getFileName(fileName) + "_" + eventType +"_"+(new java.util.Date()).getTime()+ ".bad"
                : filePath + "\\bad\\" + VelosIO.getFileName(fileName) + "_"
                        + eventType +"_"+(new java.util.Date()).getTime()+ ".bad";
         dataHolder.setBAD_FILE(VelosIO.getFileName(fileName) + "_" + eventType+"_"+(new java.util.Date()).getTime()+ ".bad");       
        logger.info("bad file name " + badFilePath);
        try {
            parser = config.getParser(encoding);
          } catch (ApplicationException ae) {
            new ApplicationException(
                    "Error encountered while retrieving the parser");
            return -3;
        } catch (ClassNotFoundException ce) {
            new ApplicationException("FileProcessor:ClassCastException thrown "
                    + ce.getMessage());
            return -3;
        } catch (InstantiationException iae) {
            new ApplicationException(
                    "FileProcessor:Error in Instantiating the Parser specified. "
                            + iae.getMessage());
            return -3;
        } catch (IllegalAccessException ile) {
            new ApplicationException(
                    "FileProcessor:Access Denied to the parser "
                            + ile.getMessage());
            return -3;
        }
               

        
            //PreValidation Routine
            counter=0;
            int maxCycle=0;
            int msgSize=0;
            int finalCount=0;
            boolean rejectFlag=false;
           
            if ((config.getMaxCounter()>=config.getValidateMsgCount()) || (config.getMaxCounter()==-1))
                maxCycle=config.getMaxCounter();
            if ((config.getMaxCounter()<config.getValidateMsgCount()))
                maxCycle=config.getValidateMsgCount();
            
            if (config.getPreValidate().equals("Y")) {
                dataHolder.setProcessMessage("PreValidating the File.... (It may take a long time to prevalidate based on file size and configuration)");
                logger.info("PreValidating the File.... (It may take a long time to prevalidate based on file size and configuration)");
                try{
                validateParser = config.getParser(encoding);
                }catch(Exception e)
                {
                    
                }
                validateParser.setProperties(filePath + "\\" + fileName, config.getDlm());
             //while (!(parser.EOF()) && ((counter<(maxCycle)) && (maxCycle!=-1)) ) {
                 
                 while (!(validateParser.EOF()) && (  ((counter<(maxCycle)) && (maxCycle!=-1)  ) || ( maxCycle == -1)  )){
                 //System.out.println(counter);
                msgList = validateParser.getTokens();
                logger.debug("Pre Validating Row:" + validateParser.getCurrentBuffer());
                try {
                    if (msgList!=null && msgList.size()>0) {
                  //      if ((config.getValidateMsgCount()>counter) &&  ((config.getValidateMsgCount()>0) && config.getValidateMsgCount()!=-1))
                            
                   if ((config.getValidateMsgCount()>counter &&  config.getValidateMsgCount()>0 && config.getValidateMsgCount()!=-1) || ( config.getValidateMsgCount()==-1) )   
                        {
                           if (msgList.size()< config.getMappingCount())
                           {
                               rejectFlag=true;
                               break;
                           }
                        }
                        //if msgList.size()<config.
                      if ((config.getEnablemsgcount().equals("Y")) && (config.getMaxCounter()==-1) || (config.getMaxCounter()>counter) )
                          finalCount++;
                        counter++;
                    }
                    

                } catch (Exception e) {
                    
                    e.printStackTrace();
                }
            }
          //System.out.println("Counter Stopped at :" +finalCount);
          dataHolder.setPreCount(finalCount);
          if (rejectFlag)
           {
              dataHolder.setProcessMessage("File was rejected by PreValidator.");
             //Do cleanup,file rejected
              validateParser.close();
             DBEngine.returnConnection(conn);
             rejectFlag=false;
             return -5;
             
           }
           
           //Close the handle to the file
           validateParser.close();
            } //Check PreValidation Flag 
            //End Prevalidation

            logger.debug("Parser initialized" + parser);
            parser.setProperties(filePath + "\\" + fileName, config.getDlm());

            if (parser != null) {
                try {
                    app = config.getExtension(eventType);
                } catch (Exception ae) {

                }
                
               dataHolder.setProcessMessage("Event:"+eventType+"		        Message:File Processing in Progress......");
            
            // open a file stream for writing bad records
            if (((config.getHeader()).toLowerCase()).equals("y")) {
                parser.getTokens();
                this.fileHeader = parser.getCurrentBuffer();
            }
            counter = 1;
            if (app != null) {
                while (!(parser.EOF())) {
                    msgList = parser.getTokens();
                    logger.debug("Processing Row" + parser.getCurrentBuffer());
                    try {
                        if (msgList.size() > 0) {
                            dataHolder.addToMsgCount();
                            logger.info("Processing Record: " + counter);
                            out = app.processMessage(msgList, config, conn);
                            counter++;
                        }
                        if (out < 0) {

                            logMessage(parser.getCurrentBuffer());
                            out=0;
                        }

                    } catch (Exception e) {
                    }
                }
            } else {
                while (!(parser.EOF())) {
                    msgList = parser.getTokens();
                    try {
                        if (msgList.size() > 0) {
                            /*logger.info("============================");
                            logger.info("Processing Record: " + counter);
                            logger.info("============================");*/
                            dataHolder.addToMsgCount();
                            logger.info("Processing Record: " + counter);
                            out = (defaultApp).processMessage(msgList, config,
                                    conn);
                            counter++;
                        }
                        if (out < 0) {
                            logMessage(parser.getCurrentBuffer());
                            out=0;
                        }
                    } catch (ApplicationException e) {

                    }

                }
                
            }
            
            /*logger
                    .info("===========================================================");
            logger
                    .info("                     Summary                                ");
            logger
                    .info("===========================================================");
            logger
                    .info("                                                            ");
            logger.info("File Processed :  " + fileName);
            logger.info("Total Records  : " + (counter - 1));
            logger
                    .info("                                                            ");
            logger
                    .info("===========================================================");
            logger
                    .info("                                                            ");
            logger
                    .info("============================================================");*/
            
           
            
            
            try {
                parser.close();
               if (validateParser!=null) validateParser.close();
                conn.commit();
                if (velIO!=null) velIO.close();
                DBEngine.returnConnection(conn);
               
            } catch (Exception e) {
                // Rlog.fatal("common","Error in Closing connection in ");
                e.printStackTrace();
            }
        } else {
            logger
                    .fatal("Parser is not available to process the file. Please contact Velos Support.");
            return -3;
        }
        return 1;
    }

    /**
     * Log the message to *.bad file
     * 
     * @param String -
     *            message to log.
     */
    public void logMessage(String currentBuffer) {
        dataHolder.addToBadCount();
        dataHolder.setMSG_LEVEL("BAD");
        if (velIO == null) {
            // Rlog.debug("common","Create New Object for VELIO");
            velIO = new VelosIO(badFilePath, 2);
            if (this.fileHeader.length() > 0)
                velIO.writeLine(this.fileHeader);
        }
        logger.info("Bad Record, streaming to the bad file");
        if (currentBuffer != null)
            velIO.writeLine(currentBuffer);
        dataHolder.setMSG_LEVEL(""); 
    }

    public void logStatus(int status) {

    }

}
