/**
 * 
 */
package com.velos.remoteservice.forms;

import java.io.InputStream;
import java.util.List;

/*import com.velos.eres.web.person.PersonJB;
import com.velos.eres.web.study.StudyJB;*/
import com.velos.remoteservice.IRemoteService;
import com.velos.remoteservice.RemoteMemberNotImplementedException;

/**
 * This interface defines methods for an entry-point into the Velos
 * Consumption Framework plugins. Implementations can be created to integrate
 * to remote services that serve consent forms. Methods included query
 * a remote system for a list of forms available as well for retrieving
 * payloads from those systems.
 * 
 * The framework is agnostic about the protocols used to query and retreive
 * content.
 * 
 * @author dylan
 *
 */
public interface IConsentFormService extends IRemoteService{
	
	/**
	 * Retrieve a list of documents for documents related to a patient
	 * and a study.
	 * 
	 * @param person patient for the search of documents
	 * @param study study for the search of documents
	 * @param patientProtocolId patientPrtocolId 
	 * 
	 * @return list of documents
	 * @throws RemoteMemberNotImplementedException
	 */
	public List<DocumentMetaData> getStudyPatientDocs(
			//PersonJB patient, 
			//StudyJB study, 
			String patientProtocolId)
			
		throws RemoteMemberNotImplementedException;

	/**
	 * Retrieve a list of documents related to a study, but which has
	 * no real connection to a single patient.
	 * @param study
	 * @return
	 * @throws RemoteMemberNotImplementedException
	 */
	/*public List<DocumentMetaData> getStudyDocs(StudyJB study)
		throws RemoteMemberNotImplementedException;
*/
	/**
	 * Retrieve a document 
	 * @param document
	 * @return
	 * @throws RemoteMemberNotImplementedException
	 */
	public InputStream getDocument(DocumentMetaData document)
		throws RemoteMemberNotImplementedException;

	    
}
