package com.velos.esch.audit.web;

import java.util.ArrayList;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.esch.service.util.JNDINames;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
//import com.velos.esch.audit.service.AuditRowEschAgent;
import com.velos.esch.business.common.EventdefDao;
//import com.velos.services.patientschedule.PatientScheduleDAO;

public class AuditRowEschJB {
	private Integer id = 0;
    private String tableName;
	private Integer RID;
    private String action;
    private Date timeStamp;
    private String userName;	
		
 // Getters and setters
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getTableName() {
		return tableName;
	}

	public void setRID(Integer rid) {
		this.RID = rid;
	}
	public Integer getRID() {
		return RID;
	}

	public void setAction(String action) {
		this.action = action;
	}
	public String getAction() {
		return action;
	}
	
	public void setTimeStamp(Date timeStamp){
		this.timeStamp=timeStamp;
	}
	public Date getTimeStamp(){
		return this.timeStamp;
	}
	
	public void setUserName(String userName){
		this.userName=userName;
	}
	public String getUserName(){
		return this.userName;
	}
	
	public AuditRowEschJB() {
	
	}
	
	public int setReasonForChangeOfSchedule(int eventId, int userId, String remarks){
		int output =0;
		int rid = getMeRID(eventId, "SCH_EVENTS1", "EVENT_ID");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	public int setReasonForChangeOfScheduleEvent(int eventId, int userId, String remarks){
		int output =0;	
		int rid = getMeRID(eventId, "SCH_EVENTS1", "EVENT_ID");
		output = this.setReasonForChange(rid, userId, remarks); 
		//setReasonForChangeOfScheduleEventStatus(eventId, userId, remarks);
		return output;
	}
	
	public int setReasonForChangeOfScheduleEvents(ArrayList eventIds, int userId, String remarks){
		int output =0;	
		for (int i=0; i < eventIds.size(); i++){
			int eventId = ((Integer)eventIds.get(i)).intValue();
			int rid = getMeRID(eventId, "SCH_EVENTS1", "EVENT_ID");
			output = this.setReasonForChange(rid, userId, remarks); 			
		}
		return output;
	}

	public int setReasonForChangeOfScheduleEventStatus(int eventId, int userId, String remarks,
			boolean excludeCurrent){
		int output =0;
		String strEventId =""+eventId;
		
		for (int i=0;i < (10-(""+eventId).length());i++){
			strEventId = "0" + strEventId;
		}
		EventdefDao eventDefDao = new EventdefDao();
		if (excludeCurrent)
			eventDefDao.getEventStatusHistoryExcludeCurrent(strEventId);
		else
			eventDefDao.getEventStatusHistory(strEventId);
		
		ArrayList evtStatusPKs = eventDefDao.getEventStatIds();
		if (null != evtStatusPKs && evtStatusPKs.size() > 0){
			int evtStatusPK = StringUtil.stringToNum(""+evtStatusPKs.get(0));
			int rid = getMeRID(evtStatusPK, "SCH_EVENTSTAT", "PK_EVENTSTAT");
			output = this.setReasonForChange(rid, userId, remarks);
		}
		return output;
	}
	
	public int setReasonForChangeOfDeactiveEvents(int patientId, int protId, java.sql.Date bookedOn, 
			int userId, String remarks){
		int output =0;
		//PatientScheduleDAO schDao = new PatientScheduleDAO();	
		//ArrayList schEventIds = schDao.getDeactivatedEvents(patientId, protId, bookedOn);
		
		/*if (schEventIds != null){
			for (int cnt=0; cnt < schEventIds.size()-1; cnt++){
				int schEventId = StringUtil.stringToNum(""+schEventIds.get(cnt));
				if (schEventId > 0){
					output = setReasonForChangeOfScheduleEvent(schEventId, userId, remarks);
				}			
			}
		}*/
		return output;
	}

	public int setReasonForChangeOfAdverseEvent(int advId, int userId, String remarks){
		int output =0;	
		int rid = getMeRID(advId, "SCH_ADVERSEVE", "PK_ADVEVE");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	public int setReasonForChangeOfAdverseEventInfo(int advInfoId, int userId, String remarks){
		int output =0;	
		int rid = getMeRID(advInfoId, "SCH_ADVINFO", "PK_ADVINFO");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	public int setReasonForChangeOfAdverseEventNotify(int advNotifyId, int userId, String remarks){
		int output =0;	
		int rid = getMeRID(advNotifyId, "SCH_ADVNOTIFY", "PK_ADVNOTIFY");
		output = this.setReasonForChange(rid, userId, remarks); 
		return output;
	}
	
	private int setReasonForChange(int rid, int userId, String remarks) {
		int output =0;
		int raid =0;
		try{
			raid = findLatestRaid(rid, userId);

			AuditRowEschJBHelper helper = new AuditRowEschJBHelper(this);
          //  output = helper.setReasonForChange(raid, remarks);
		} catch (Exception e){
			Rlog.fatal("auditRow","EXCEPTION IN setReasonForChange "+ e);
			output = -2;
		}		
		return output;	
	}
	
	private int findLatestRaid(int rid, int userId) {
		int output = 0;
		AuditRowEschJBHelper helper = new AuditRowEschJBHelper(this);
       // output = helper.findLatestRaid(rid, userId);
        return output;
	}

	private int getMeRID(int pkey, String tableName, String pkColumnName) {
		int output = 0;
		try {
            AuditRowEschJBHelper helper = new AuditRowEschJBHelper(this);
           // output = helper.getRID(pkey, tableName, pkColumnName);
            return output;
        }
        catch (Exception e) {
            Rlog.fatal("auditRow","EXCEPTION IN READING "+ tableName +" table"+ e);
            return -2;
        }
	}
	
	/*private AuditRowEschAgent getAuditRowEschAgent() throws NamingException {
		InitialContext ic = new InitialContext();
		return (AuditRowEschAgent)ic.lookup(JNDINames.AuditRowEschAgentHome);
	}*/
}
