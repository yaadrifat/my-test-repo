/*
 * $Id: EJBUtil.java,v 1.17 2014/11/15 06:22:25 jkhurana Exp $
 * Copyright 2000 Velos, Inc. All rights reserved.
 */

package com.velos.esch.service.util;

/* Import all the exceptions can be thrown */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.velos.eres.service.util.LC;
/*import com.velos.esch.service.advEveAgent.AdvEveAgentRObj;
import com.velos.esch.service.advInfoAgent.AdvInfoAgentRObj;
import com.velos.esch.service.advNotifyAgent.AdvNotifyAgentRObj;*/
import com.velos.esch.service.advEveAgent.AdvEveAgentRObj;
import com.velos.esch.service.alertNotifyAgent.AlertNotifyAgentRObj;
import com.velos.esch.service.bgtApndxAgent.BgtApndxAgentRObj;
/*import com.velos.esch.service.bgtSectionAgent.BgtSectionAgentRObj;
import com.velos.esch.service.budgetAgent.BudgetAgentRObj;
import com.velos.esch.service.budgetUsersAgent.BudgetUsersAgentRObj;
import com.velos.esch.service.budgetcalAgent.BudgetcalAgentRObj;
import com.velos.esch.service.crfAgent.CrfAgentRObj;
import com.velos.esch.service.crfNotifyAgent.CrfNotifyAgentRObj;
import com.velos.esch.service.crfStatAgent.CrfStatAgentRObj;
import com.velos.esch.service.crflibAgent.CrflibAgentRObj;*/
import com.velos.esch.service.docrefAgent.DocrefAgentRObj;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.eventCrfAgent.EventCrfAgentRObj;
/*import com.velos.esch.service.eventKitAgent.EventKitAgentRObj;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventcostAgent.EventcostAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
*/
import com.velos.esch.service.eventdocAgent.EventdocAgentRObj;
/*import com.velos.esch.service.eventresourceAgent.EventResourceAgentRObj;
import com.velos.esch.service.eventuserAgent.EventuserAgentRObj;*/
import com.velos.esch.service.lineitemAgent.LineitemAgentRObj;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
/*import com.velos.esch.service.eventstatAgent.EventStatAgentRObj;
import com.velos.esch.service.portalFormsAgent.PortalFormsAgentRObj;*/
import com.velos.esch.service.subCostItemAgent.SubCostItemAgentRObj;
import com.velos.esch.service.subCostItemVisitAgent.SubCostItemVisitAgentRObj;
//import com.velos.esch.audit.service.AuditRowEschAgent;

/**
 * This is a utility class for obtaining EJB references. All modules Home
 * objects should be referred from this location.
 */

public final class EJBUtil {
	
    static int cReturn;

    static float cFloatReturn;

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static String integerToString(Integer i) {
        String returnString = null;
        try {
            if (i == null) {
                return returnString;
            }
            returnString = i.toString();
            return returnString;
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
            return returnString;
        }
    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static Integer stringToInteger(String str) {
        Integer returnInteger = null;
        try {

            if (str != null) {
                str = str.trim();
                if (str.length() > 0) {
                    returnInteger = Integer.valueOf(str);
                }
            }
            return returnInteger;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "{EXCEPTION IN CONVERTING TO Integer in sch EJBUtil:}" + e);
            return returnInteger;
        }
    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static String floatToString(Float i) {
        String returnString = null;
        try {
            Rlog.debug("common", "{Float VALUE:}" + i);
            if (i == null) {
                Rlog.debug("common", "Float is null}");
                return returnString;
            }
            returnString = i.toString();
            Rlog.debug("common", "{String VALUE:}" + returnString);
            return returnString;
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
            return returnString;
        }
    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static int stringToNum(String str) {
        int iReturn = 0;
        try {
            Rlog.debug("common", "{STRING VALUE:}" + str);
            if (str == null) {
                return cReturn;
            } else if (str.equals("")) {
                return cReturn;
            } else {
                iReturn = Integer.parseInt(str);
                Rlog.debug("common", "{INT VALUE:}" + iReturn);
                return iReturn;
            }
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO NUMBER:}" + e);
            return iReturn;
        }
    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static float stringToFloat(String str) {
        float iReturn = 0;
        try {
            Rlog.debug("common", "{STRING VALUE:}" + str);
            if (str == null) {
                return cFloatReturn;
            } else if (str == "") {
                return cFloatReturn;
            } else {
                iReturn = Float.parseFloat(str);
                Rlog.debug("common", "{Float VALUE:}" + iReturn);
                return iReturn;
            }
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO FLOAT:}" + e);
            return iReturn;
        }
    }

    /*
     * if the appServerParam are set in configuration class then return it,
     * otherwise read it from xml file and store in appServerParam then return.
     */

    public static Hashtable getContextProp() {
        try {
            Rlog.debug("common", "Conf param = "
                    + Configuration.getServerParam());
            if (Configuration.getServerParam() == null)
                Configuration.readSettings();
            Rlog.debug("common", "Conf param = "
                    + Configuration.getServerParam());
        } catch (Exception e) {
            Rlog.fatal("common", "EJBUtil:getContextProp:general ex" + e);
        }
        return Configuration.getServerParam();
    }

    public static String getActualPath(String dir, String fileName) {
        String path;
        dir = dir.trim();
        fileName = fileName.trim();

        File f = new File(dir, fileName);

        try {
            path = f.getAbsolutePath();
        } catch (Exception e) {
            path = null;
        }
        return path;
    }

    /** *********GET CONNECTION FROM RESOURCE********* */

    public static Connection getConnection() {
        InitialContext iCtx = null;
        Connection conn = null;
        Configuration conf = null;

        Rlog.debug("common", "GetConnection Function Call");
        try {
            iCtx = new InitialContext(EJBUtil.getContextProp());
            if (Configuration.dsJndiName == null) {
                Configuration.readSettings();
            }
            Rlog
                    .debug("common", "JNDI NAME DS NAME"
                            + Configuration.dsJndiName);
            DataSource ds = (DataSource) iCtx.lookup(Configuration.dsJndiName);
            Rlog.debug("common", "DATASOURCE " + ds);
            return ds.getConnection();
        } catch (NamingException ne) {
            Rlog.fatal("common", ":getConnection():Naming exception:" + ne);
            return null;
        } catch (Exception se) {
            Rlog.fatal("common", ":getConnection():Sql exception:" + se);
            return null;
        } finally {
            try {
                if (iCtx != null)
                    iCtx.close();
            } catch (NamingException ne) {
                Rlog.fatal("common",
                        ":getConnection():context close exception:" + ne);
            }
        }
    }

    /** **********READ ENV VARIABLES************ */

    public static String getEnvVariable(String Name) throws Exception {

        String iline = "";
        String envValue = "";
        int i = 0;
        int sType = 0;
        Process theProcess = null;
        BufferedReader stdInput = null;

        try {
            
            // Figure out what kind of system we are running on
            // 0 = windows
            // 1 = unix
            sType = getSystemType();

             

            if (sType == 0) {
                String envPath = "cmd /C echo %" + Name + "%";
              
                theProcess = Runtime.getRuntime().exec(envPath);
            } else {
                // theProcess = Runtime.getRuntime().exec("env $"+Name);
                theProcess = Runtime.getRuntime().exec("printenv " + Name);
            }
            stdInput = new BufferedReader(new InputStreamReader(theProcess
                    .getInputStream()));

            envValue = stdInput.readLine();

            stdInput.close();
            theProcess.destroy();
             
            return envValue;

        } catch (IOException ioe) {

            throw new Exception(ioe.getMessage());
        }

    }

    private static int getSystemType() {

        Properties p = System.getProperties();
        String os = p.getProperty("os.name");
        int i = 0;
        int osType = 0;

        if ((i = os.indexOf("Win")) != -1) {
            osType = 0;
        } else {
            osType = 1;
        }

        return osType;

    }

    public static EventdefAgentRObj getEventdefAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventdefAgentRObj) initial.lookup(JNDINames.EventdefAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Error in getEventdefAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static EventAssocAgentRObj getEventAssocAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventAssocAgentRObj) initial.lookup(JNDINames.EventAssocAgentHome);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getEventAssocAgentHome() in EJBUtil " + e);
            return null;
        }
    }
/*
    public static EventcostAgentRObj getEventcostAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventcostAgentRObj) initial.lookup(JNDINames.EventcostAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getEventcostAgentHome() in EJBUtil " + e);
            return null;
        }
    }
*/
    public static EventdocAgentRObj getEventdocAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventdocAgentRObj) initial.lookup(JNDINames.EventdocAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getEventdocAgentHome() in EJBUtil " + e);
            return null;
        }
    }
/*
    public static CrfAgentRObj getCrfAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (CrfAgentRObj) initial.lookup(JNDINames.CrfAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc", "Error in getCrfAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static CrfStatAgentRObj getCrfStatAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (CrfStatAgentRObj) initial.lookup(JNDINames.CrfStatAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getCrfStatAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static CrfNotifyAgentRObj getCrfNotifyAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (CrfNotifyAgentRObj) initial.lookup(JNDINames.CrfNotifyAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getCrfNotifyAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static CrflibAgentRObj getCrflibAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (CrflibAgentRObj) initial.lookup(JNDINames.CrflibAgentHome);

        } catch (Exception e) {
            Rlog.fatal("crflib", "Error in getCrflibAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }*/

    public static AlertNotifyAgentRObj getAlertNotifyAgentHome() {
        try {
            InitialContext initial = new InitialContext();

            return (AlertNotifyAgentRObj) initial
                    .lookup(JNDINames.AlertNotifyAgentHome);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAlertNotifyAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static AdvEveAgentRObj getAdvEveAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (AdvEveAgentRObj) initial.lookup(JNDINames.AdvEveAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAdvEveAgentHome() in EJBUtil " + e);
            return null;
        }
    }
    /*
    public static AdvInfoAgentRObj getAdvInfoAgentHome() {
        try {

            InitialContext initial = new InitialContext();
            return (AdvInfoAgentRObj) initial.lookup(JNDINames.AdvInfoAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAdvInfoAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static AdvNotifyAgentRObj getAdvNotifyAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (AdvNotifyAgentRObj) initial.lookup(JNDINames.AdvNotifyAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getAdvNotifyAgentHome() in EJBUtil " + e);
            return null;
        }
    }*/

    public static DocrefAgentRObj getDocrefAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (DocrefAgentRObj) initial.lookup(JNDINames.DocrefAgentHome);

        } catch (Exception e) {
            Rlog
                    .fatal("eventassoc", "Error in getDocrefAgentHome() in EJBUtil "
                            + e);
            return null;
        }
    }

    /*public static EventuserAgentRObj getEventuserAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventuserAgentRObj) initial.lookup(JNDINames.EventuserAgentHome);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in getEventuserAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static BudgetAgentRObj getBudgetAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (BudgetAgentRObj) initial.lookup(JNDINames.BudgetAgentHome);

        } catch (Exception e) {
            Rlog.fatal("budget", "Error in getBudgetAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static BudgetcalAgentRObj getBudgetcalAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (BudgetcalAgentRObj) initial.lookup(JNDINames.BudgetcalAgentHome);

        } catch (Exception e) {
            Rlog.fatal("budget", "Error in getBudgetcalAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static BudgetUsersAgentRObj getBudgetUsersAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (BudgetUsersAgentRObj) initial.lookup(JNDINames.BudgetUsersAgentHome);

        } catch (Exception e) {
            Rlog.fatal("BudgetUsers",
                    "Error in getBudgetUsersAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static BgtSectionAgentRObj getBgtSectionAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (BgtSectionAgentRObj) initial.lookup(JNDINames.BgtSectionAgentHome);

        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Error in getBgtSectionAgentHome() in EJBUtil " + e);
            return null;
        }
    }
*/
    public static BgtApndxAgentRObj getBgtApndxAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (BgtApndxAgentRObj) initial.lookup(JNDINames.BgtApndxAgentHome);

        } catch (Exception e) {
            Rlog.fatal("BgtApndx",
                    "Error in getBgtApndxAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static LineitemAgentRObj getLineitemAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (LineitemAgentRObj) initial.lookup(JNDINames.LineitemAgentHome);

        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "Error in getLineitemAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static ProtVisitAgentRObj getProtVisitAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (ProtVisitAgentRObj) initial.lookup(JNDINames.ProtVisitAgentHome);

        } catch (Exception e) {
            Rlog.fatal("protvisit",
                    "Error in getProtVisitAgentHome() in EJBUtil " + e);
            return null;
        }
    }



    //  Added by Manimaran on 26,June 2006 for May June Requirement
    public static EventCrfAgentRObj getEventCrfAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (EventCrfAgentRObj) initial.lookup(JNDINames.EventCrfAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getEventCrfAgentHome() in EJBUtil "
                    + e);

            return null;

        }

    }
/*

    //JM: 15Feb2008:
    public static EventResourceAgentRObj getEventResourceAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventResourceAgentRObj) initial.lookup(JNDINames.EventResourceAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventresource",
                    "Error in getEventResourceAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static EventKitAgentRObj getEventKitAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventKitAgentRObj) initial.lookup(JNDINames.EventKitAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventkit",
                    "Error in getEventKitAgentHome() in EJBUtil " + e);
            return null;
        }
    }


    //JM: 28Aug2008:
    public static EventStatAgentRObj getEventStatAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EventStatAgentRObj) initial.lookup(JNDINames.EventStatAgentHome);

        } catch (Exception e) {
            Rlog.fatal("eventstat",
                    "Error in getEventStatAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static PortalFormsAgentRObj getPortalFormsAgentHome() {
    	try {
    		InitialContext initial = new InitialContext();
    		return (PortalFormsAgentRObj) initial.lookup(JNDINames.PortalFormsAgentHome);

    	} catch (Exception e) {
    		Rlog.fatal("portalForms",
    				"Error in getPortalFormsAgentHome() in EJBUtil " + e);
    		return null;
    	}
    }*/

    //KM:19Jan11 - D-FIN11
    public static SubCostItemAgentRObj getSubCostItemAgentHome() {
    	try {
    		InitialContext initial = new InitialContext();
    		return (SubCostItemAgentRObj) initial.lookup(JNDINames.SubCostItemAgentHome);

    	} catch (Exception e) {
    		Rlog.fatal("subjectcost",
    				"Error in getSubCostItemAgentHome() in EJBUtil " + e);
    		return null;
    	}
    } 

    public static SubCostItemVisitAgentRObj getSubCostItemVisitAgentHome() {
    	try {
    		InitialContext initial = new InitialContext();
    		return (SubCostItemVisitAgentRObj) initial.lookup(JNDINames.SubCostItemVisitAgentHome);

    	} catch (Exception e) {
    		Rlog.fatal("subjectcost",
    				"Error in getSubCostItemVisitAgentHome() in EJBUtil " + e);
    		return null;
    	}
    }
    
    /*public static AuditRowEschAgent getAuditRowEschAgentBean() {
    	try {
    		InitialContext initial = new InitialContext();
    		return (AuditRowEschAgent) initial.lookup(JNDINames.AuditRowEschAgentHome);
    	} catch (Exception e) {
    		Rlog.fatal("common", "gen ex=" + e);
    		return null;
    	}
    }   */
    /*
     * Method to create a drop down @param name of the HTML element select
     * @param already selected value @param Ids of data value of dropdown @param
     * Desc - display value of dropdown
     */
    public static String createPullDown(String dName, int selValue,
            ArrayList id, ArrayList desc) {
        String strCodeId;
        Integer integerCodeId;

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;
            int intcode = 0;

            // check whether ID is of type Integer or String
            if (id.size() > 0) {
                objType = (((Object) id.get(counter)).getClass()).getName();
            }

            if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                isString = true;
            }

            mainStr.append("<SELECT NAME=" + dName + ">");

            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= desc.size() - 1; counter++) {

                if (isString) {
                    strCodeId = (String) id.get(counter);
                    intcode = stringToNum(strCodeId);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    intcode = integerCodeId.intValue();

                }

                if (selValue == intcode) {
                    mainStr.append("<OPTION value = " + intcode + " SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = " + intcode + ">"
                            + desc.get(counter) + "</OPTION>");
                }
            }
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0")) {
                mainStr
                		.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDown()" + e;
        }
        return mainStr.toString();
    }

    // END OF CLASS
}