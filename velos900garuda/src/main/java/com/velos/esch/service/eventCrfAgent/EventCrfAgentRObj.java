/*
 * Classname			EventCrfAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.esch.service.eventCrfAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.esch.business.common.CrfDao;
import com.velos.esch.business.eventCrf.impl.EventCrfBean;

/**
 * Remote interface for EventCrfAgentBean session EJB
 * 
 * @author Manimaran
 * @version 1.0, 06/25/2003
 */
@Remote
public interface EventCrfAgentRObj {
    public EventCrfBean getEventCrfDetails(int eventCrfId);

    public int setEventCrfDetails(EventCrfBean eventcrf);

    public int updateEventCrf(EventCrfBean eventcrf);
    
    public CrfDao getEventCrfForms(int eventId,String dispType);
    
    // Overloaded for INF-18183 ::: Akshi
    public void deleteEventCrf(int eventId,Hashtable<String, String> args);

    public void deleteEventCrf(int eventId);
 
    /** Returns 'Visit Mark Done' prompt Flag = 1 if all the CRfs are filled for the scheduled event record 
     *  and if the event has no marked
     * status. It will return 0 if the event already has a status. 
     * @param schEventId PK to identify scheduled event (sch_events1 pk)
     * @return 1 if application should prompt for marking 'events done', 0 - for no prompts
     */
    public int getEventMarkDoneFlagWhenAllCRFsFilled(int schEventId);
}
