/*
 * Classname : EventAssocAgentBean
 *
 * Version information: 1.0
 *
 * Date: 06/12/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.eventassocAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ejb.SessionContext;
import javax.annotation.Resource;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.common.EventAssocDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.common.ScheduleDao;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;
import com.velos.eres.business.common.TeamDao;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * EventAssocBean.<br>
 * <br>
 *
 */

@Stateless
@Remote( { EventAssocAgentRObj.class })
public class EventAssocAgentBean implements EventAssocAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * Calls getEventAssocDetails() on EventAssoc Entity Bean EventAssocBean.
     * Looks up on the EventAssoc id parameter passed in and constructs and
     * returns a EventAssoc state holder. containing all the summary details of
     * the EventAssoc<br>
     *
     * @param eventassocId
     *            the EventAssoc id
     * @ee EventAssocBean
     */
    public EventAssocBean getEventAssocDetails(int event_id) {

        EventAssocBean eventassoc = null;

        try {
            Rlog.debug("user", "in getEventAssocDetails agentbean event_id## "
                    + event_id);
            eventassoc = (EventAssocBean) em.find(EventAssocBean.class,
                    new Integer(event_id));
        } catch (Exception e) {
            Rlog.debug("fatal",
                    "Exception in EventAssocAgentBean.getEventAssocDetails "
                            + e);
        }
        return eventassoc;
    }

    /**
     * Calls setEventAssocDetails() on EventAssoc Entity Bean
     * EventAssocBean.Creates a new EventAssoc with the values in the
     * StateKeeper object.
     *
     * @param eventassoc
     *            a eventassoc state keeper containing eventassoc attributes for
     *            the new EventAssoc.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setEventAssocDetails(EventAssocBean edsk) {
        try {
            EventAssocBean eventAssoc = new EventAssocBean();
            
            
            //JM: 10Apr2008: blocked and modified
            //eventAssoc.updateEventAssoc(eventAssoc);
            eventAssoc.updateEventAssoc(edsk);

            em.persist(eventAssoc);
            Rlog
                    .debug("eventassoc",
                            "EventAssocAgentBean.setEventAssocDetails EventAssocStateKeeper ");
            if (eventAssoc.getEvent_type().equalsIgnoreCase("L")
                    || eventAssoc.getEvent_type().equalsIgnoreCase("P")) {
                eventAssoc.setChain_id(Integer.toString(eventAssoc
                        .getEvent_id()));
                em.merge(eventAssoc);
            }

            //return (edsk.getEvent_id());
            //JM: 17Apr2008 commented and modified
            return (eventAssoc.getEvent_id());
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in setEventAssocDetails() in EventAssocAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateEventAssoc(EventAssocBean edsk) {
        int output;
        EventAssocBean eventassoc = null;

        try {
        	 
        	//JM: 02DEC2010, #4538        	
        	EventdefDao eventdefdao = new EventdefDao();
        	 int count = eventdefdao.ValidateEventName(edsk.getEvent_id(),
                     EJBUtil.stringToNum(edsk.getUser_id()), edsk.getName(),
                     edsk.getEvent_type(), edsk.getChain_id(), edsk.getCalAssocTo());
        	 if (count == -1) {
                 return -1;
             }
        	
        	 
        	
            eventassoc = em.find(EventAssocBean.class, edsk.getEvent_id());
            if (eventassoc == null) {
                return -2;
            }
            output = eventassoc.updateEventAssoc(edsk);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in updateEventAssoc in EventAssocAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    public int createEventsAssoc(String[] eIdList, String[] vIdList, String[] seqList,
    		String[] dispList, String[] hideList,
    		int userId, String ipAdd) {
    	EventAssocDao eventAssocDao = new EventAssocDao();
    	return eventAssocDao.createEventsAssoc(eIdList, vIdList, seqList, dispList, hideList, userId, ipAdd);
    }


    public int removeEventAssocChild(int event_id) {

        int output = 0;
        EventAssocBean assocBean = null;

        try {
            assocBean = em.find(EventAssocBean.class, event_id);
            em.remove(assocBean);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in removeEventAssocChild  in EventAssocAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeEventAssocChilds(int event_ids[]) {
        int output = 0;
        try {

            for (int count = 0; count < event_ids.length; count++) {
                EventAssocBean assocBean = null;
                assocBean = em.find(EventAssocBean.class, event_ids[count]);
                em.remove(assocBean);
            }
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in removeEventAssocChilds in EventAssocAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeEventAssocHeader(int chain_id) {
        int output = 0;
        Enumeration event_ids;
        try {

            Query query = em.createNamedQuery("findEventIdsAssoc");
            query.setParameter("chainId", chain_id);
            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    EventAssocBean eventassoc = (EventAssocBean) list.get(i);
                    em.remove(eventassoc);
                }
            }
            // //////

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in removeEventAssocHeader  in EventAssocAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public int removeEventAssocHeaders(int chain_ids[]) {

        int output = 0;
        Enumeration event_ids;

        try {
            Query query = em.createNamedQuery("findEventIdsAssoc");

            for (int count = 0; count < chain_ids.length; count++) {
                query.setParameter("chainId", chain_ids[count]);
                ArrayList list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        EventAssocBean eventassoc = (EventAssocBean) list
                                .get(i);
                        em.remove(eventassoc);
                    }
                }
            }

        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in removeEventAssocHeaders  in EventAssocAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    public int AddProtToStudy(String protocol_id, String study_id,
            String userId, String ipAdd,String type) {

        int output;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            output = eventassocdao.AddProtToStudy(protocol_id, study_id,
                    userId, ipAdd, type);
        }

        catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in AddProtToStudy  in EventAssocAgentBean"
                            + e);
            return -2;
        }
        return output;
    }
       
    public int createProtocolBudget(int protocol_id, int bgtTemplate, String bgtName,
            int userId, String ipAdd, int studyId) {

        int output;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            output = eventassocdao.createProtocolBudget(protocol_id, bgtTemplate, bgtName,
                    userId, ipAdd, studyId);
        }

        catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in createProtocolBudget in EventAssocAgentBean"
                            + e);
            return -2;
        }
        return output;
    }
    
    public int copyStudyProtocol(String protocolId, String protocolName,
            String userId, String ipAdd,String accId) {

        int output;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            output = eventassocdao.copyStudyProtocol(protocolId, protocolName,
                    userId, ipAdd,accId);
        }

        catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in copyStudyProtocol  in EventAssocAgentBean" + e);
            return -2;
        }
        return output;
    }
//  JM: 04May2007, active protocol calendars only
    public EventAssocDao getStudyProtsActive(int study_id) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getStudyProtsActive(study_id);
            Rlog
                    .debug("eventassoc",
                            "In getStudyProtsActive in EventAssocAgentBean ...");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Exception In getAllLibAndEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }



    public EventAssocDao getStudyProts(int study_id) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getStudyProts(study_id);
            Rlog
                    .debug("eventassoc",
                            "In getAllLibAndEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Exception In getAllLibAndEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    public EventAssocDao getStudyAdminProts(int study_id,String status) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getStudyAdminProts(study_id,status);
            Rlog
                    .debug("eventassoc",
                            "In getAllLibAndEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Exception In getAllLibAndEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }


    public EventAssocDao getStudyProts(int study_id, String status) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getStudyProts(study_id, status);
            return eventassocDao;
        } catch (Exception e) {
            Rlog
                    .fatal(
                            "eventassoc",
                            "Exception In getStudyProts(int study_id, String status) in EventAssocAgentBean "
                                    + e);
        }
        return null;

    }

    public EventAssocDao getStudyProtsWithAlnotStat(int study_id) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getStudyProtsWithAlnotStat(study_id);
            Rlog
                    .debug("eventassoc",
                            "In getStudyProtsWithAlnotStat in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Exception In getStudyProtsWithAlnotStat in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    public EventAssocDao getStudyProts(int study_id, int budgetId) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getStudyProts(study_id, budgetId);
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Exception In getAllLibAndEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    /*
     * events_disps array has org_id,disp,cost for all the events
     */

    public void AddEventsToProtocol(String chain_id, String[][] events_disps,
            int fromDisp, int toDisp, String userId, String ipAdd) {
        int output;

        String crfinfo = "S";
        Integer event_id;
        int i = 0;

        EventdefDao eventDao = new EventdefDao();
        EventAssocDao eventassocDao = new EventAssocDao();
        eventassocDao.EditAssocEvent(chain_id, fromDisp, toDisp);

        for (i = 0; i < events_disps.length; i++) {

            Rlog.debug("EventAssoc", "Inside SessionBean:AddEventsToProtocol"
                    + events_disps[i][0]);
            event_id = new Integer(events_disps[i][0]);
            EventAssocBean eask = new EventAssocBean();
            try {

                // the org_id field stores the event_id of event_def table
                // incase the calendar has
                // been selected from library or an event has been added to the
                // calendar from library
                // but incase of copying a study calendar, it stores the
                // event_id of event_Assoc table
                // so we will get an exception when we try to look in for the pk
                // in event_def table
                // thus incase of exception, use event_assoc table for copying
                EventdefAgentRObj eventDefRObj=EJBUtil.getEventdefAgentHome();
                EventdefBean edsk = eventDefRObj.getEventdefDetails(event_id.intValue());

                eask.setEvent_id(edsk.getEvent_id());
                eask.setChain_id(edsk.getChain_id());
                eask.setEvent_type(edsk.getEvent_type());
                eask.setName(edsk.getName());
                eask.setNotes(edsk.getNotes());
                eask.setCost(events_disps[i][2]);
                eask.setCost_description(edsk.getCost_description());
                eask.setDuration(edsk.getDuration());
                eask.setUser_id(edsk.getUser_id());
                eask.setLinked_uri(edsk.getLinked_uri());
                eask.setFuzzy_period(edsk.getFuzzy_period());
                eask.setMsg_to(edsk.getMsg_to());                
                eask.setDescription(edsk.getDescription());
                eask.setDisplacement(edsk.getDisplacement());
                eask.setOrg_id(edsk.getOrg_id());
                eask.setEvent_msg(edsk.getEvent_msg());

                // SV, 11/05, part of the reason for bug #1827 : message created
                // for a new study event propagates with days stored in all
                // fields. All the following fields were copied from
                // patdaysbefore!@
                // Also, this method of trying to update eventdef first, and if
                // that fails, then update assoc!? wouldn't this add some
                // overhead??!
                eask.setPatDaysBefore(edsk.getPatDaysBefore());
                eask.setPatDaysAfter(edsk.getPatDaysAfter());
                eask.setUsrDaysBefore(edsk.getUsrDaysBefore());
                eask.setUsrDaysAfter(edsk.getUsrDaysAfter());
                eask.setPatMsgBefore(edsk.getPatMsgBefore());
                eask.setPatMsgAfter(edsk.getPatMsgAfter());
                eask.setUsrMsgBefore(edsk.getUsrMsgBefore());
                eask.setUsrMsgAfter(edsk.getUsrMsgAfter());
                eask.setCreator(userId);
                eask.setIpAdd(ipAdd);

                eask.setChain_id(chain_id);
                eask.setDisplacement(events_disps[i][1]);
                eask.setEvent_type("A");
                eask.setOrg_id(event_id.intValue());
                eask.setDurationUnit(edsk.getDurationUnit());//JM: 27May2008, added for, issue #3205
                //soniaabrol, propagation issues:
                eask.setCptCode(edsk.getCptCode());
                eask.setDurationUnitAfter(edsk.getDurationUnitAfter());
                eask.setDurationUnitBefore(edsk.getDurationUnitBefore());
                eask.setFuzzyAfter(edsk.getFuzzyAfter());
                eask.setEventCalType(edsk.getEventCalType());

                eask.setEventCategory(events_disps[i][4]);//KM
                //KM-#4560
                eask.setEventLibType(edsk.getEventLibType());
                eask.setEventLineCategory(edsk.getEventLineCategory());
                
                eask.setEventSOSId(edsk.getEventSOSId());
                eask.setEventFacilityId(edsk.getEventFacilityId());
                eask.setEventCoverageType(edsk.getEventCoverageType());

                try {
                    if (!(StringUtil.isEmpty(events_disps[i][3])))
                    eask.setEventVisit(events_disps[i][3]);
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }

            }// try
            catch (Exception e) {
                // e.printStackTrace();
                // the org_id field stores the event_id of event_def table
                // incase the calendar has
                // been selected from library or an event has been added to the
                // calendar from library
                // but incase of copying a study calendar, it stores the
                // event_id of event_Assoc table
                // so we will get an exception when we try to look in for the pk
                // in event_def table
                // thus incase of exception, use event_assoc table for copying

                try {

                    EventAssocBean easkOld = em.find(EventAssocBean.class,
                            event_id);

                    if (easkOld != null) {
                        eask.setEvent_id(easkOld.getEvent_id());
                        eask.setChain_id(easkOld.getChain_id());
                        eask.setEvent_type(easkOld.getEvent_type());
                        eask.setName(easkOld.getName());
                        eask.setNotes(easkOld.getNotes());
                        eask.setCost(events_disps[i][2]);
                        eask.setCost_description(easkOld.getCost_description());
                        eask.setDuration(easkOld.getDuration());
                        eask.setUser_id(easkOld.getUser_id());
                        eask.setLinked_uri(easkOld.getLinked_uri());
                        eask.setFuzzy_period(easkOld.getFuzzy_period());
                        eask.setMsg_to(easkOld.getMsg_to());                        
                        eask.setDescription(easkOld.getDescription());
                        eask.setDisplacement(easkOld.getDisplacement());
                        eask.setOrg_id(easkOld.getOrg_id());
                        eask.setEvent_msg(easkOld.getEvent_msg());
                        // SV, 11/05, part of the reason for bug #1827 : message
                        // created for a new study event propagates with days
                        // stored
                        // in all fields. All the following fields were copied
                        // from
                        // patdaysbefore!@

                        eask.setPatDaysBefore(easkOld.getPatDaysBefore());
                        eask.setPatDaysAfter(easkOld.getPatDaysAfter());
                        eask.setUsrDaysBefore(easkOld.getUsrDaysBefore());
                        eask.setUsrDaysAfter(easkOld.getUsrDaysAfter());
                        eask.setPatMsgBefore(easkOld.getPatMsgBefore());
                        eask.setPatMsgAfter(easkOld.getPatMsgAfter());
                        eask.setUsrMsgBefore(easkOld.getUsrMsgBefore());
                        eask.setUsrMsgAfter(easkOld.getUsrMsgAfter());
                        eask.setCreator(userId);
                        eask.setIpAdd(ipAdd);

                        eask.setChain_id(chain_id);
                        eask.setDisplacement(events_disps[i][1]);
                        eask.setEvent_type("A");
                        eask.setOrg_id(event_id.intValue());
                        eask.setDurationUnit(easkOld.getDurationUnit());//JM: 27May2008, added for, issue #3205

//                      soniaabrol, propagation issues:
                        eask.setCptCode(easkOld.getCptCode());
                        eask.setDurationUnitAfter(easkOld.getDurationUnitAfter());
                        eask.setDurationUnitBefore(easkOld.getDurationUnitBefore());
                        eask.setFuzzyAfter(easkOld.getFuzzyAfter());
                        eask.setEventCalType(easkOld.getEventCalType());
                        eask.setEventLibType(easkOld.getEventLibType());
                        eask.setEventLineCategory(easkOld.getEventLineCategory());

                        eask.setEventSOSId(easkOld.getEventSOSId());
                        eask.setEventFacilityId(easkOld.getEventFacilityId());
                        eask.setEventCoverageType(easkOld.getEventCoverageType());
                        
                        try {
                            if (!(StringUtil.isEmpty(events_disps[i][3])))
                            eask.setEventVisit(events_disps[i][3]);
                            }
                            catch(Exception es)
                            {
                                es.printStackTrace();
                            }

                    }
                }// try
                catch (Exception ex) {
                    Rlog.fatal("eventassoc",
                            "Error in AddEventsToProtocol in EventAssocAgentBean"
                                    + ex);
                    ex.printStackTrace();
                    return;
                }

            }

            try {
                EventAssocBean newBean = new EventAssocBean();
                newBean.updateEventAssoc(eask);
                em.persist(newBean);

                Integer oldID = new Integer(newBean.getEvent_id());
                eventDao.CopyEventDetails(events_disps[i][0], oldID.toString(),
                        crfinfo, userId, ipAdd);
                // SV, 10/27/04, moved it up to JB
                // eventDao.UpdateEventVisitIds(chain_id, userId, ipAdd); //SV,
                // 10/04, cal-enh

            } catch (Exception exp) {
                Rlog.fatal("eventassoc",
                        "Error in AddEventsToProtocol in copyeventdetails in EventAssocAgentBean"
                                + exp);
                exp.printStackTrace();
                return;

            }

        }// for loop
    }

    public int DeleteProtocolEvents(String chain_id) {

        int output;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            // output=eventassocdao.DeleteProtocolEvents(chain_id);
            // SV, 8/19/04, renamed delete method in eventAssocdao to be
            // consistent with what it does and EventdefDao.
            output = eventassocdao.DeleteDuplicateEvents(chain_id);
        }

        catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in CopyProtoco  in EventAssocAgentBean" + e);
            return -2;
        }
        return output;
    }

    // SV, 8/19/04, new function to handle when calendar duration is reduced.
    public int DeleteProtocolEventsPastDuration(String chain_id, int duration) {

        int output;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            output = eventassocdao.DeleteProtocolEvents(chain_id, duration);
        }

        catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in DeleteProtocolEventsPastDuration  in EventAssocAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Delete calendar associated with a study
     *
     * @param studyId
     * @param calendarId
     * @returns 0:successful, -1:error, -2 if patients enrolled in the calendar,
     *          -3 if budget linked to the calendar
     */

    public int studyCalendarDelete(int studyId, int calendarId) {
        int output;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            output = eventassocdao.studyCalendarDelete(studyId, calendarId);
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in studyCalendarDelete in EventAssocAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int studyCalendarDelete(int studyId, int calendarId, Hashtable<String, String> auditInfo) {
        int output;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            /*AUDIT FOR DELETED BY BEGINS*/
            AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            String getRidValue= AuditUtils.getRidValue("EVENT_ASSOC","esch","event_id="+calendarId);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("EVENT_ASSOC",String.valueOf(calendarId),getRidValue,
        			userID,currdate,moduleName,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
            output = eventassocdao.studyCalendarDelete(studyId, calendarId);
            if(output!=0){context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return 0 else, rollback */
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("eventassoc",
                    "Error in studyCalendarDelete in EventAssocAgentBean" + e);
            return -2;
        }
        return output;

    }

    public EventAssocDao getFilteredEvents(int user_id, int protocol_id) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getFilteredEvents(user_id, protocol_id);
            Rlog
                    .debug("EventAssoc",
                            "In getAllLibAndEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getFilteredEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    public EventAssocDao getAllProtAndEvents(int event_id) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getAllProtAndEvents(event_id);
            Rlog
                    .debug("EventAssoc",
                            "In getAllProtAndEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getAllProtAndEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    public EventAssocDao getPrevStatus(int protocolId) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getPrevStatus(protocolId);
            Rlog.debug("eventassoc",
                    "In getPrevStatus in EventassocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Exception In getPrevStatus in EventassocAgentBean " + e);
        }
        return null;

    }

    //Modified by Manimaran to fix the issue #3394
    public EventAssocDao getProtSelectedEvents(int protocolId, String orderType, String orderBy) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getProtSelectedEvents(protocolId,orderType,orderBy);
            Rlog
                    .debug("EventAssoc",
                            "In getProtSelectedEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getProtSelectedEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    public EventAssocDao getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao
                    .getFilteredRangeEvents(protocolId, beginDisp, endDisp);
            Rlog
                    .debug("EventAssoc",
                            "In getFilteredRangeEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getFilteredRangeEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    public int RemoveProtEventInstance(int eventId, String whichTable) {

        int output;

        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            output = eventassocdao.RemoveProtEventInstance(eventId, whichTable);
        }

        catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in RemoveProtEventInstance  in EventAssocAgentBean"
                            + e);
            return -1;
        }
        return output;
    }

    public EventAssocDao getProtRowEvents(int protocolId, int rownum,
            int beginDisp, int endDisp) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getProtRowEvents(protocolId, rownum, beginDisp,
                    endDisp);
            Rlog.debug("eventassoc",
                    "In getProtRowEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog
                    .fatal("EventAssoc",
                            "Exception In getProtRowEvents in EventAssocAgentBean "
                                    + e);
        }
        return null;

    }

    public EventAssocDao fetchCalTemplate(String protocolId) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.fetchCalTemplate(protocolId);
            Rlog.debug("eventassoc",
                    "In fetchCalTemplate in EventassocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog
                    .fatal("EventAssoc",
                            "Exception In fetchCalTemplate in EventAssocAgentBean "
                                    + e);
        }
        return null;

    }

    public EventInfoDao getAlertNotifyValues(int patprotId) {
        try {
            EventInfoDao eventinfoDao = new EventInfoDao();
            eventinfoDao.getAlertNotifyValues(patprotId);
            Rlog
                    .debug("eventassoc",
                            "In getAlertNotifyValues in EventassocAgentBean line number 2");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getAlertNotifyValues in EventassocAgentBean "
                            + e);
        }
        return null;

    }

    public int getMaxCost(String chainId) {
        int cost;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            cost = eventassocdao.getMaxCost(chainId);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "EventAssocAgentBean.GetMaxCost" + e);
            return -2;
        }
        return cost;
    }

    // /// Changes Made for enhancements Phase II
    // / By Sonia Kaura
    // / Date : 11/3/2003

    public EventAssocDao getAllProtSelectedEvents(int protocolId, String visitId, String evtName) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getAllProtSelectedEvents(protocolId,visitId,evtName);
            Rlog
                    .debug("EventAssoc",
                            "In getAllProtSelectedEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getAllProtSelectedEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }
        //YK: FOR PCAL-20461
    public EventAssocDao getAllProtSelectedVisitsEvents(int protocolId, String visitId, String evtName) {
    	try {
    		EventAssocDao eventassocDao = new EventAssocDao();
    		eventassocDao.getAllProtSelectedVisitsEvents(protocolId,visitId,evtName);
    		Rlog
    		.debug("EventAssoc",
    		"In getAllProtSelectedVisitsEvents in EventAssocAgentBean line number 2");
    		return eventassocDao;
    	} catch (Exception e) {
    		Rlog.fatal("EventAssoc",
    				"Exception In getAllProtSelectedVisitsEvents in EventAssocAgentBean "
    				+ e);
    	}
    	return null;
    	
    }

    public EventAssocDao getProtSelectedAndGroupedEvents(int protocolId) {
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getProtSelectedAndGroupedEvents(protocolId);
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc", "In EventAssocAgentBean.getProtSelectedAndGroupedEvents:  "+e);
            return null;
        }
    }
   
    /*YK :: DFIN12 FIX FOR BUG#5954-- RESOLVES  #5947 #5943 BUGS ALSO*/
      public EventAssocDao getEventVisitMileStoneGrid(int protocolId) {
    	try {
    		EventAssocDao eventassocDao = new EventAssocDao();
    		eventassocDao.getEventVisitMileStoneGrid(protocolId);
    		return eventassocDao;
    	} catch (Exception e) {
    		Rlog.fatal("EventAssoc", "In EventAssocAgentBean.getEventVisitMileStoneGrid:  "+e);
    		return null;
    	}
    }

    /* Sonia Abrol, 10/30/06*/
    public int refreshCalendarMessages(String protocolId, String studyId, String modifiedBy,
            String ipAdd)
    {
    	int success = 0;
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            success  = eventassocDao.refreshCalendarMessages(protocolId,studyId, modifiedBy,
                    ipAdd);
            Rlog.debug("EventAssoc",
                            "In refreshCalendarMessages in EventAssocAgentBean -executed method");
            return success ;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In refreshCalendarMessages in EventAssocAgentBean "
                            + e);
        }
        return success ;

    }


    /**
     *JM: 10Apr2008: for #C8 March2008-enh.
     * Parameter protocolId
     *
     *
     */

    public EventAssocDao getProtVisitAndEvents(int protocolId, String search,String resetSort) {/*YK 04Jan- SUT_1_Requirement_18489*/
        try {
            EventAssocDao eventassocDao = new EventAssocDao();
            eventassocDao.getProtVisitAndEvents(protocolId, search, resetSort);/*YK 04Jan- SUT_1_Requirement_18489*/
            Rlog
                    .debug("EventAssoc",
                            "In getProtVisitAndEvents in EventAssocAgentBean line number 2");
            return eventassocDao;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getProtVisitAndEvents in EventAssocAgentBean "
                            + e);
        }
        return null;

    }

    //JM: 10April2008: #C8
    public int getMaxSeqForVisitEvents(int visitId, String eventName, String tableName) {
        int ret=0;
        try {
            EventAssocDao eventassocdao = new EventAssocDao();
            ret = eventassocdao.getMaxSeqForVisitEvents(visitId, eventName, tableName);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "EventAssocAgentBean.getMaxSeqForVisitEvents" + e);
            return -2;
        }
        return ret;
    }



    //JM: 17Apr2008
    public int CopyEventDetails(String old_event_id, String new_event_id, String crfInfo, String userId, String ipAdd){
        int ret=0;
        try {
        	EventdefDao eventdefDao = new EventdefDao();
        	//EventAssocDao eventassocdao = new EventAssocDao();
        	eventdefDao.CopyEventDetails( old_event_id,  new_event_id,  crfInfo,  userId,  ipAdd);
        } catch (Exception e) {
            Rlog.fatal("eventassoc", "Exception In CopyEventDetails() in eventAssocBean" + e);
            return -2;
        }
        return ret;
    }


  //JM: 23Apr2008, added for, Enh. #C9
    public void updateOfflnFlgForEventsVisits(int event_id, String offlineFlag) {

    	try{

        EventAssocDao evtAsscDao = new EventAssocDao();
        evtAsscDao.updateOfflnFlgForEventsVisits(event_id, offlineFlag);
    	} catch (Exception e) {
            Rlog.fatal("eventassoc", "Exception In updateOfflnFlgForEventsVisits() in eventAssocAgentBean" + e);
            return;
        }



    }


    //JM: 25Apr2008, added for, Enh. #C9
    public int hideUnhideEventsVisits(String[] eventIds, String hide_flag, String[] flags) {

    	int output = 0;
        try {

        	EventAssocDao eventassocdao = new EventAssocDao();
            output = eventassocdao.hideUnhideEventsVisits(eventIds, hide_flag, flags );
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in hideUnhideEventsVisits()  in eventAssocAgentBean " + e);
            return -1;
        }
        return output;


    }

  //JM: 23Apr2008, added for, Enh. #C9
    public void updatePatientSchedulesNow(int calId, int checkedVal, int statusId, java.sql.Date selDate, int userId, String ipAdd  ){
    	try{

        EventAssocDao evtAsscDao = new EventAssocDao();
        evtAsscDao.updatePatientSchedulesNow(calId, checkedVal, statusId, selDate,userId, ipAdd);
    	} catch (Exception e) {
            Rlog.fatal("eventassoc", "Exception In updatePatientSchedulesNow() in eventAssocAgentBean" + e);
            return;
        }

    }

    /*
     * events_disps array has org_id, disp, cost for all the events
     * JM: 06May2008, added for, Enh. #PS16
     */
    //public void updateUnscheduledEvents(String chain_id, String[][] events_disps, int fromDisp, int toDisp, int seq, String userId, String ipAdd) {

    public String[]  updateUnscheduledEvents(String chain_id, String[][] events_disps, int fromDisp, int toDisp, int seq, String userId, String ipAdd) {
        int output;

        String crfinfo = "S";
        Integer event_id;
        int i = 0;


        String eventType = "U";
        String eventflg = "0";

        int evIdRet =0;
        String[] strArrEventIds = new String[events_disps.length] ;


        EventdefDao eventDao = new EventdefDao();
        EventAssocDao evtAsscDao = new EventAssocDao();

        for (i = 0; i < events_disps.length; i++) {

            event_id = new Integer(events_disps[i][0]);


            EventAssocBean eask = new EventAssocBean();


            try {

                EventdefAgentRObj eventDefRObj=EJBUtil.getEventdefAgentHome();
                EventdefBean edsk = eventDefRObj.getEventdefDetails(event_id.intValue());

                eask.setEvent_id(edsk.getEvent_id());
                eask.setChain_id(chain_id);
                eask.setEvent_type(eventType);
                eask.setName(edsk.getName());
                eask.setNotes(edsk.getNotes());
                eask.setCost(events_disps[i][2]);
                eask.setCost_description(edsk.getCost_description());
                eask.setDuration(edsk.getDuration());
                eask.setUser_id(edsk.getUser_id());
                eask.setLinked_uri(edsk.getLinked_uri());
                eask.setFuzzy_period(edsk.getFuzzy_period());
                eask.setMsg_to(edsk.getMsg_to());               
                eask.setDescription(edsk.getDescription());
                eask.setDisplacement(events_disps[i][1]);
                eask.setOrg_id(event_id.intValue());
                eask.setEvent_msg(edsk.getEvent_msg());
                eask.setEventFlag(eventflg);
                eask.setPatDaysBefore(edsk.getPatDaysBefore());
                eask.setPatDaysAfter(edsk.getPatDaysAfter());
                eask.setUsrDaysBefore(edsk.getUsrDaysBefore());
                eask.setUsrDaysAfter(edsk.getUsrDaysAfter());
                eask.setPatMsgBefore(edsk.getPatMsgBefore());
                eask.setPatMsgAfter(edsk.getPatMsgAfter());
                eask.setUsrMsgBefore(edsk.getUsrMsgBefore());
                eask.setUsrMsgAfter(edsk.getUsrMsgAfter());
                eask.setCreator(userId);
                eask.setIpAdd(ipAdd);
                eask.setDurationUnit(edsk.getDurationUnit());//JM: 27May2008, added for, issue #3205
                eask.setCptCode(edsk.getCptCode());
                eask.setDurationUnitAfter(edsk.getDurationUnitAfter());
                eask.setDurationUnitBefore(edsk.getDurationUnitBefore());
                eask.setFuzzyAfter(edsk.getFuzzyAfter());
                eask.setEventCalType(edsk.getEventCalType());
                eask.setEventCategory(events_disps[i][4]);

                eask.setEventSequence(""+(seq+i+1));
                if (!(StringUtil.isEmpty(events_disps[i][3])))
                	eask.setEventVisit(events_disps[i][3]);
                
                eask.setEventSOSId(edsk.getEventSOSId());
                eask.setEventFacilityId(edsk.getEventFacilityId());
                eask.setEventCoverageType(edsk.getEventCoverageType());
                	
                EventAssocBean newBean = new EventAssocBean();
                newBean.updateEventAssoc(eask);
                em.persist(newBean);

                em.merge(newBean);/////////////////////////////////added now..........

                evIdRet = newBean.getEvent_id();
                strArrEventIds[i]= ""+evIdRet;

                Integer oldID = new Integer(newBean.getEvent_id());
                eventDao.CopyEventDetails(events_disps[i][0], oldID.toString(),crfinfo, userId, ipAdd);

            } catch (Exception exp) {
                Rlog.fatal("eventassoc",
                        "Error in updateUnscheduledEvents in copyeventdetails in EventAssocAgentBean"
                                + exp);
                exp.printStackTrace();


            }

        }// for loop


        return strArrEventIds;

        //EventAssocDao evtAsscDao = new EventAssocDao();
        //evtAsscDao.updateUnscheduledEvents1(EJBUtil.stringToNum(chain_id),EJBUtil.stringToNum(userId), ipAdd, strArrEventIds);


    }

    //JM: 06May2008, added for, Enh. #PS16
    public void updateUnscheduledEvents1(int patId, int patProtId, int calId, int userId, String ipAdd, String[] strs  ){
    	try{

        EventAssocDao evtAsscDao = new EventAssocDao();
        evtAsscDao.updateUnscheduledEvents1(patId, patProtId, calId, userId, ipAdd, strs);
    	} catch (Exception e) {
            Rlog.fatal("eventassoc", "Exception In updateUnscheduledEvents1() in eventAssocAgentBean" + e);
            return;
        }

    }

    public  Hashtable getVisitEventForScheduledEvent(String schevents1_pk) {
    	Hashtable htR = new Hashtable();
        try {

            ScheduleDao sDao = new ScheduleDao();
            htR =  sDao.getVisitEventForScheduledEvent(schevents1_pk);

            return htR;
        } catch (Exception e) {
            Rlog.fatal("EventAssoc",
                    "Exception In getVisitEventForScheduledEvent in EventAssocAgentBean "
                            + e);
        }
        return htR;

    }



  //JM: 16July2008, added for the issue #3586

    public int deleteEventStatusHistory(int eventStatusHistId){

    	int retme = 0;
        try {
            EventAssocDao evaDao = new EventAssocDao();
            retme = evaDao.deleteEventStatusHistory(eventStatusHistId);

        } catch (Exception e) {
        	Rlog.fatal("eventassoc","inside EventAssocJB::deleteEventStatusHistory()" + e);
			return -2;
        }

        return retme;
    }

    // Overloaded for INF-18183 ::: Akshi
    public int deleteEventStatusHistory(int eventStatusHistId,Hashtable<String, String> args){

    	int retme = 0;
        try {
        	
        	 AuditBean audit=null;
             String currdate =DateUtil.getCurrentDate();
             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
             String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
             String getRidValue= AuditUtils.getRidValue("SCH_EVENTSTAT","esch","PK_EVENTSTAT="+eventStatusHistId);/*Fetches the RID/PK_VALUE*/ 
             audit = new AuditBean("SCH_EVENTSTAT",String.valueOf(eventStatusHistId),getRidValue,
         			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
         	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        	    	
        	
            EventAssocDao evaDao = new EventAssocDao();
            retme = evaDao.deleteEventStatusHistory(eventStatusHistId);
            if(retme!=1)
            {context.setRollbackOnly();};
            	

        } catch (Exception e) {
        	context.setRollbackOnly();
        	Rlog.fatal("eventassoc","inside EventAssocJB::deleteEventStatusHistory()" + e);
			return -2;
        }

        return retme;
    }
    public void updateCurrentStatus(int evtId){


        try {
            EventAssocDao evaDao = new EventAssocDao();
            evaDao.updateCurrentStatus( evtId);

        } catch (Exception e) {
        	Rlog.fatal("eventassoc","inside EventAssocJB::updateCurrentStatus()" + e);
			return ;
        }

    }
    
    public int getEventFormAccees(int crfLinkedForm, int assocId, int portalId){

    	int retme = 0;
        try {
            EventAssocDao evaDao = new EventAssocDao();
            retme = evaDao.getEventFormAccees(crfLinkedForm, assocId, portalId);

        } catch (Exception e) {
        	Rlog.fatal("eventassoc","inside EventAssocJB::getEventFormAccees()" + e);
			return -2;
        }

        return retme;
    }
    //Added by Virendra to create new calendar from eSP
    public int setEventAssocCalendarDetails(EventAssocBean edsk) {
        try {
            EventAssocBean eventAssoc = new EventAssocBean();
           
            eventAssoc.updateEventAssoc(edsk);

            em.persist(eventAssoc);
            Rlog
                    .debug("eventassoc",
                            "EventAssocAgentBean.setEventAssocCalendarDetails EventAssocStateKeeper ");
            em.merge(eventAssoc);

            return (eventAssoc.getEvent_id());
        } catch (Exception e) {
            Rlog.fatal("eventassoc",
                    "Error in setEventAssocCalendarDetails() in EventAssocAgentBean "
                            + e);
        }
        return 0;
    }
    
    /**
     * Created by : Ankit Kumar
     * Created On : 07/14/2011 
     * Enhancement: FIN-CostType-1
     * Purpose    : Identify the role assigned to the user/team on study.
     * @return    : roleCodePk;
     */
    public  String getStudyUserRole(String studyId, String userId){
    	
    	ArrayList tId = new ArrayList();
    	String roleCodePk = "";
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(userId));
		tId = teamDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamDao.getTeamRoleIds();
		
				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);
		
					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}
		
			}
			else
			{
				roleCodePk ="";
			}
			return roleCodePk;
    }
    //Yk: Added for Enhancement :PCAL-19743
    public EventAssocDao getProtSelectedAndVisitEvents(int protocolId) {
    	try {
    		EventAssocDao eventassocDao = new EventAssocDao();
    		eventassocDao.getProtSelectedAndVisitEvents(protocolId);
    		return eventassocDao;
    	} catch (Exception e) {
    		Rlog.fatal("EventAssoc", "In EventAssocAgentBean.getProtSelectedAndVisitEvents:  "+e);
    		return null;
    	}
    }
   

}// end of class
