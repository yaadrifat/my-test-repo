/*
 * Classname : AdvEveAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/28/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.advEveAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.common.NCILibDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.advEve.impl.AdvEveBean;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.esch.business.common.AdvEveDao;
import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.service.advEveAgent.AdvEveAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Stateless session EJB acting as a facade for the CMP entity bean AdvEveBean
 * and other adverse event services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Service layer.
 * </p>
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:06/11/2004 Comment: 1. Modified JavaDoc comments
 * *************************END****************************************
 */
@Stateless
public class AdvEveAgentBean implements AdvEveAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    /*
     * * returns an AdvEveStateKeeper object with Adverse Event details. <p>
     * Calls getAdvEveDetails() on Entity Bean AdvEveBean and creates and
     * returns an AdvEveStateKeeper. containing all the details of the AdvEve<br>
     * </p>
     * 
     * @param advEveId the adverse event id
     * 
     * @see com.velos.esch.business.advEve.AdvEveStateKeeper
     */
    public AdvEveBean getAdvEveDetails(int advEveId) {

        AdvEveBean retrieved = null;

        try {

            return (AdvEveBean) em
                    .find(AdvEveBean.class, new Integer(advEveId));
        } catch (Exception e) {
            System.out.print("Error in getAdvEveDetails() in AdvEveAgentBean"
                    + e);
            return retrieved;
        }

    }

    /**
     * Calls setAdvEveDetails() on Entity Bean AdvEveBean and creates a new
     * Adverse Event.
     * 
     * @param edsk
     *            AdvEveStateKeeper object with Adverse Event details
     * @return success flag
     *         <ul>
     *         <li> id of new adverse event</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @see com.velos.esch.business.advEve.AdvEveStateKeeper
     * 
     */
    public int setAdvEveDetails(AdvEveBean edsk) {
        try {

            AdvEveBean adv = new AdvEveBean();
            adv.updateAdvEve(edsk);
            em.persist(adv);

            return (adv.getAdvEveId());
        } catch (Exception e) {
            System.out.print("Error in setAdvEveDetails() in AdvEveAgentBean "
                    + e);
        }
        return 0;
    }

    /**
     * Calls updateAdvEve() on Entity Bean AdvEveBean and updates an Adverse
     * Event.
     * 
     * @param edsk
     *            AdvEveStateKeeper object with Adverse Event details
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @see com.velos.esch.business.advEve.AdvEveStateKeeper
     * 
     */
    public int updateAdvEve(AdvEveBean edsk) {
        AdvEveBean retrieved = null; // AdvEve Entity Bean Remote Object
        int output;
        try {

            retrieved = (AdvEveBean) em.find(AdvEveBean.class, new Integer(edsk
                    .getAdvEveId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateAdvEve(edsk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog
                    .debug("advEve", "Error in updateAdvEve in AdvEveAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Returns an EventInfoDao object with patient adverse events. Calls
     * getAdverseEventNames() on the EventInfoDao
     * 
     * @param patProtID
     *            patient enrollment id
     * @return returns EventInfoDao
     * @see com.velos.esch.business.common.EventInfoDao
     */
    public EventInfoDao getAdverseEventNames(int patProtID) {
        try {
            Rlog
                    .debug("eventinfo",
                            "In getAdverseEventNames in EventdefAgentBean line number 0");
            EventInfoDao eventinfoDao = new EventInfoDao();
            Rlog
                    .debug("eventinfo",
                            "In getAdverseEventNames in EventdefAgentBean line number 1");
            eventinfoDao.getAdverseEventNames(patProtID);
            Rlog
                    .debug("eventinfo",
                            "In getAdverseEventNames in EventdefAgentBean line number 2");
            return eventinfoDao;
        } catch (Exception e) {
            Rlog.fatal("eventdef",
                    "Exception In getAdverseEventNames in EventdefAgentBean "
                            + e);
        }
        return null;

    }

    /**
     * Returns an NCILibDao object with adverse events toxicity names. Calls
     * getNCILib() on NCILibDao object.
     * 
     * @param nciVersion
     * @return returns NCILibDao
     * @see com.velos.eres.business.common.NCILibDao
     */
    public NCILibDao getNCILib(String nciVersion) {
        try {

            NCILibDao nciDao = new NCILibDao();
            nciDao.getNCILib(nciVersion);
            Rlog.debug("eventinfo", "In getNCILib in AdvEventAgentBean");
            return nciDao;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In getNCILib() in AdvEventAgentBean" + e);
        }
        return null;

    }

    /**
     * Returns an NCILibDao object with patient adverse events toxicity
     * information. Calls getToxicityGrade() on NCILibDao object
     * 
     * @param toxicityGroup
     *            toxicity group
     * @param labDate
     *            lab test date
     * @param labUnit
     *            lab unit
     * @param labSite
     *            lab site
     * @param labResult
     *            lab result
     * @param labLLN
     *            lab LLN
     * @param labULN
     *            lab ULN
     * @param userRangeFlag
     *            flag whether to user user specified range or lab facility's
     *            range
     * @param nciVersion
     *            NCI CDE version
     * @param patient
     *            patient
     * @return returns NCILibDao
     * @see com.velos.eres.business.common.NCILibDao
     */
    public NCILibDao getToxicityGrade(int toxicityGroup, String labDate,
            String labUnit, int labSite, String labResult, int labLLN,
            int labULN, int userRangeFlag, String nciVersion, int patient) {
        try {

            NCILibDao nciDao = new NCILibDao();
            nciDao.getToxicityGrade(toxicityGroup, labDate, labUnit, labSite,
                    labResult, labLLN, labULN, userRangeFlag, nciVersion,
                    patient);
            Rlog.debug("common", "In getToxicityGrade() in AdvEventAgentBean");
            return nciDao;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In getToxicityGrade() in AdvEventAgentBean" + e);
        }
        return null;

    }

    // method added by J.Majumdar dt. 19Jan05
    public int reomoveAdvEve(int advId) {

        int ret = -1;
        // DynRepDao dyndao=new DynRepDao();
        AdvEveDao advevedao = new AdvEveDao();
        try {
            Rlog.debug("advevedao", "advId in AdvEveAgentBean:reomoveAdvEve"
                    + advId);
            ret = advevedao.reomoveAdvEve(advId);

        } catch (Exception e) {
            Rlog.fatal("advevedao",
                    "Exception In reomoveAdvEve(int advId) in AdvEveAgentBean"
                            + e);
            return -2;

        }
        return ret;

    }
 // Overloaded for INF-18183 ::: AGodara
    public int reomoveAdvEve(int advId,Hashtable<String, String> auditInfo) {

        int ret = -1;
        AdvEveDao advevedao = new AdvEveDao();
        AuditBean audit=null;
        
        try {
        	String currdate =DateUtil.getCurrentDate();
        	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("SCH_ADVERSEVE","esch","PK_ADVEVE="+advId); //Fetches the RID
            
            //POPULATE THE AUDIT BEAN 
    		audit = new AuditBean("SCH_ADVERSEVE",String.valueOf(advId),rid,userID,currdate,app_module,ipAdd,reason);
         	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
        	ret = advevedao.reomoveAdvEve(advId);
        	if(ret==-1){
        		context.setRollbackOnly();
             }
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("advevedao",
                    "reomoveAdvEve(int advId,Hashtable<String, String> auditInfo)"
                            + e);
            return -2;
        }
        return ret;
    }
}// end of class
