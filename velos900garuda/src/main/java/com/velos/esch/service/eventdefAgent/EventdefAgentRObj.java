/*
 * Classname : EventdefAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 05/14/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.eventdefAgent;

/* Import Statements */
import java.util.ArrayList;
import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.common.EventInfoDao;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.eventdef.impl.EventdefBean;
import com.velos.esch.service.util.EJBUtil;

/* End of Import Statements */

/**
 * Remote interface for EventdefAgent session EJB
 *
 * @author dinesh/ Vikas Chawla
 */

@Remote
public interface EventdefAgentRObj {

    EventdefBean getEventdefDetails(int eventdefId);

    public int setEventdefDetails(EventdefBean account);

    public int updateEventdef(EventdefBean usk);
    
    public int createEventsDef(String[] eIdList, String[] vIdList, String[] seqList,
    		String[] dispList, String[] hideList,
    		int userId, String ipAdd);
    
    public int removeEventdefChild(int event_id);

    public int removeEventdefHeader(int chain_id);

    public int removeEventdefChild(int event_id, Hashtable <String, String> usrDetail); //Overloaded for INF-18183 ::: Ankit

    public int removeEventdefHeader(int chain_id, Hashtable <String, String> usrDetail); //Overloaded for INF-18183 ::: Ankit

    public int removeEventdefChilds(int event_ids[]);

    public int removeEventdefHeaders(int chain_ids[]);

    public EventdefDao getAllLibAndEvents(int user_id);

    public EventdefDao getFilteredEvents(int user_id, int protocol_id);

    public int CopyProtocol(String protocol_id, String name, String calType, String userId,
            String ipAdd);

    //KM-02sep09
    public int CopyEventToCat(int event_id, int whichTable, int category_id, int evtLibType,
            String name, String userId, String ipAdd);

    public int GenerateSchedule(int protocol_id, int patient_id,
            java.sql.Date st_date, int patProtId, Integer days, String modifiedBy,
            String ipAdd);

    public int DeactivateEvents(int protocol_id, int patient_id,
            java.sql.Date st_date,String type);

    public int MarkDone(int event_id, String notes, java.sql.Date exe_date,
            int exe_by, int status, int oldStatus, String ipAdd, String mode,
            int eventSOSId, int eventCoverType, String reasonForCoverageChange);

    public int DeleteProtocolEvents(String chain_id);

    public int DeleteProtocolEventsPastDuration(String chain_id, int duration); // SV,
                                                                                // 8/19/04,
                                                                                // delete
                                                                                // events
                                                                                // when
                                                                                // duration

    // is shortened.

    public EventdefDao FetchSchedule(int protocol_id, int patient_id,
            String pro_start_date);

    public EventdefDao FetchFilteredSchedule(int protocol_id, int patient_id,
            String pro_start_date, String visit, String month, String year,
            String event, String status);

    public EventdefDao getTotalVisit(int protocol_id, int patient_id,
            String pro_start_date);

    public EventdefDao getSchEvents(int protocol_id, int patient_id,
            String pro_start_date);

    //KM
    public EventdefDao getHeaderList(int user_id, String type, int evtLibType);
    //Added by IA multiple organizations bug 2613
    public EventdefDao getHeaderList(int account_id, int user_id, String type, String orgId);

    //end added

    public EventdefDao getHeaderList(int account_id, int user_id, String type);


    // public EventdefDao getHeaderList(int user_id,String type,int budgetId)
    // ;
    public EventdefDao getHeaderList(int account_id, int user_id, String type,
            int budgetId);

    public void AddEventsToProtocol(String chain_id, String[][] events_disps,
            int fromDisp, int toDisp, String userId, String ipAdd);

    public EventdefDao getAllProtAndEvents(int event_id);

    public EventdefDao getUserProtAndEvents(int user_id);

    public EventInfoDao getEventDefInfo(int eventId);

    //Modified by Manimaran to fix the issue #3394
    public EventdefDao getProtSelectedEvents(int protocolId,String orderType,String orderBy);

    public EventdefDao getProtSelectedAndGroupedEvents(int protocolId);
     //Yk: Added for Enhancement :PCAL-19743
    public EventdefDao getProtSelectedAndVisitEvents(int protocolId);

    // Sonia Kaura
    //public EventdefDao getAllProtSelectedEvents(int protocolId);
    //JM: 16Apr2008
    public EventdefDao getAllProtSelectedEvents(int protocolId, String visitId, String evtName);
        //YK: FOR PCAL-20461
    public EventdefDao getAllProtSelectedVisitsEvents(int protocolId, String visitId, String evtName);





    public EventdefDao getFilteredRangeEvents(int protocolId, int beginDisp,
            int endDisp);

    //public int RemoveProtEventInstance(int eventId, String whichTable);
    public int RemoveProtEventInstance(String[] deleteIds, String whichTable);//KM-#c7
    
    public int RemoveProtEventInstance(String[] deleteIds, String whichTable, Hashtable<String, String> args);//Overloaded for INF-18183 ::: Ankit

    public int fetchEventIdByVisitAndDisplacement(String anEventId, String whichTable, String fkVisit, String displacement);

    public int fetchEventIdByVisit(String anEventId, String whichTable, String fkVisit);

    //KM-#5949
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user);//JM: 31Mar2008
    
    public int deleteEvtOrVisits(String[] deleteIds, String whichTable, String[] flags, int user,Hashtable<String, String> auditInfo);//Overloaded for INF-18183 ::: Ankit

    public EventdefDao getProtVisitAndEvents(int protocolId, String search, String resetSort) ;//JM: 10April2008 /*YK 04Jan- SUT_1_Requirement_18489*/

    public int getMaxSeqForVisitEvents(int visitId, String eventName, String tableName);//JM: 10April2008

    public EventdefDao getProtRowEvents(int protocolId, int rownum,
            int beginDisp, int endDisp);

    public EventdefDao FetchPreviousSchedule(int studyId, int patientId);

    public EventdefDao getEventStatusHistory(String eventId);

    public ArrayList findAllMovingEvents(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag, String calAssoc ,int synchSugDate);
    
    public int ChangeActualDate(String prevDate, String actualDate,
            String eventId, String patprotId, String statusFlag, String ipAdd,
            String modifiedBy,String calAssoc ,int synchSugDate);

    public EventInfoDao getAdverseEvents(int fkStudy, int fkPer);

    public EventInfoDao getAdvInfo(int fkAdverseEvent, String flag);

    public EventInfoDao getAdvNotify(int fkAdverseEvent);

    public EventdefDao FetchSchedule(int eventId);

    public EventdefDao getVisitEvents(int patProtId, int visit);

    public EventdefDao getVisitEvents(int patProtId, int visit,String type,int protocolId);

    public EventdefDao getVisitEvents(int patProtId, int status, int visit,
            String exeon);
    public EventdefDao getVisitEvents(int patProtId, int status, int visit,
            String exeon,String type,int protocolId);
    public int copyNotifySetting(int p_old_fkpatprot, int p_new_fkpatprot,
            int p_copyflag, int p_creator, String p_ip);

    public int getMaxCost(String chainId);

    public EventdefDao getScheduleEventsVisits(int patprot);

    public EventdefDao getStudyScheduleEventsVisits(int studyId,int protocolId) ;

    public int calendarDelete(int calId);
    
    //Overloaded for INF-18183 ::: Ankit
    public int calendarDelete(int calId, Hashtable<String, String> auditInfo);

    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd);
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd);
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon, String[] arrNote,
            String[] arrSos, String[] arrCoverage, String[] reasonForCoverageChng,
            String ipAdd,String type);
    
    public int saveMultipleEventsVisits(int enrollId, int userId,
            String[] arrEventIds, String[] arrStatusId,
            String[] arrOldStatusId, String[] arrExeon,
            String ipAdd,String type);
    
    public int copyProtToLib(String protocol_id, String study_id,String name,String desc,String type,String shareWith,
            String userId, String ipAdd) ;


    public int propagateEventUpdates(int protocolId, int eventId,
            String tableName, int primary_key, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType);


    //JM: 17Apr2008
    public int CopyEventDetails(String old_event_id, String new_event_id, String crfInfo, String userId, String ipAdd);


    public int updatePatTxtArmsForTheNewPatProt(int patProtIdOld, int patProtId);

}