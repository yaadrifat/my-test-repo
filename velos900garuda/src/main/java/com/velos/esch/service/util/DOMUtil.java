package com.velos.esch.service.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.xml.parser.Resolver;
import com.sun.xml.tree.XmlDocument;

public class DOMUtil {
    public static Document readDocument(String filename) throws IOException,
            SAXException {

        // if you are using the Sun parser, use these lines
        InputSource input = Resolver.createInputSource(new File(filename));
        XmlDocument doc = XmlDocument.createXmlDocument(input, false);
        doc.getDocumentElement().normalize();
        return doc;

    }

    public static Node findNode(Node node, String name) {
        if (node.getNodeName().equals(name))
            return node;
        if (node.hasChildNodes()) {
            NodeList list = node.getChildNodes();
            int size = list.getLength();
            for (int i = 0; i < size; i++) {
                Node found = findNode(list.item(i), name);
                if (found != null)
                    return found;
            }
        }
        return null;
    }

    public static String getNodeAttribute(Node node, String name) {
        if (node instanceof Element) {
            Element element = (Element) node;
            return element.getAttribute(name);
        }
        return null;
    }

    public static void printSubtree(PrintWriter writer, Node root, Node node) {
        if (node instanceof Element) {
            if (node != root)
                writer.print("\n<" + node.getNodeName() + ">");
            if (node.hasChildNodes()) {
                NodeList list = node.getChildNodes();
                int size = list.getLength();
                for (int i = 0; i < size; i++) {
                    printSubtree(writer, root, list.item(i));
                }
            }
            if (node != root)
                writer.print("</" + node.getNodeName() + ">");
        } else if (node instanceof Text) {
            writer.print(node.getNodeValue().trim());
        }
    }
}
