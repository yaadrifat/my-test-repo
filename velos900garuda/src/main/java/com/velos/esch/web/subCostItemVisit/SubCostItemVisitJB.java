/*

 * Classname : 				SubCostItemVisitJB

 * 

 * Version information : 	1.0

 *

 * Date 					01/21/11

 * 

 * Copyright notice: 		Velos Inc

 * 

 * Author 					Manimaran

 */

package com.velos.esch.web.subCostItemVisit;

import com.velos.esch.business.subCostItemVisit.impl.SubCostItemVisitBean;
import com.velos.esch.service.subCostItemVisitAgent.SubCostItemVisitAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

public class SubCostItemVisitJB {
	
	/**
	 * subCostItemVisitId
	 */
	private int subCostItemVisitId;
	
	/**
	 * fkSubCostItem
	 */
	private String fkSubCostItem;
	
	/**
	 * fkProtocolVisit
	 */
	private String fkProtocolVisit;
	
	/**
     * creator
     */
    private String creator;
    
    /**
     * last modifiedBy
     */
    private String modifiedBy;
    
    /**
     * IP Address
     */
    private String ipAdd;


    public int getSubCostItemVisitId() {
		return this.subCostItemVisitId;
	}

	public void setSubCostItemVisitId(int subCostItemVisitId) {
		this.subCostItemVisitId = subCostItemVisitId;
	}

	public String getFkSubCostItem() {
		return this.fkSubCostItem;
	}

	public void setFkSubCostItem(String fkSubCostItem) {
		this.fkSubCostItem = fkSubCostItem;
	}

	public String getFkProtocolVisit() {
		return this.fkProtocolVisit;
	}

	public void setFkProtocolVisit(String fkProtocolVisit) {
		this.fkProtocolVisit = fkProtocolVisit;
	}

	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getIpAdd() {
		return this.ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
    
	
	public SubCostItemVisitJB(int subCostItemVisitId) {
		setSubCostItemVisitId(subCostItemVisitId);
    }
	
	// DEFAULT CONSTRUCTOR
    public SubCostItemVisitJB() {
    }
	

	public SubCostItemVisitJB(int subCostItemVisitId, String fkSubCostItem,
			String fkProtocolVisit, String creator, String modifiedBy,
			String ipAdd) {
		setSubCostItemVisitId(subCostItemVisitId);
		setFkSubCostItem(fkSubCostItem);
		setFkProtocolVisit(fkProtocolVisit);
		setCreator(creator);
		setModifiedBy(modifiedBy);
		setIpAdd(ipAdd);
	}
	
	
	public SubCostItemVisitBean getSubCostItemVisitDetails() {

    	SubCostItemVisitBean scsk = null;

        try {

        	SubCostItemVisitAgentRObj subCostAgent = EJBUtil
                    .getSubCostItemVisitAgentHome();
            scsk = subCostAgent.getSubCostItemVisitDetails(this.subCostItemVisitId);
        } catch (Exception e) {
            Rlog.fatal("subcostitemvisit",
                    "EXception in calling session bean in subject cost item visit" + e);

        }

        if (scsk != null) {
    		this.subCostItemVisitId = scsk.getSubCostItemVisitId();
    		this.fkSubCostItem = scsk.getFkSubCostItem();
    		this.fkProtocolVisit = scsk.getFkProtocolVisit();
    		this.creator = scsk.getCreator();
    		this.modifiedBy = scsk.getModifiedBy();
    		this.ipAdd = scsk.getIpAdd();
        }
        return scsk;
    }
	
	
	public int setSubCostItemVisitDetails() {
        int output = 0;
        try {
        	SubCostItemVisitAgentRObj subCostAgent = EJBUtil.getSubCostItemVisitAgentHome();
            output = subCostAgent.setSubCostItemVisitDetails(this
                    .createSubCostItemVisitStateKeeper());
        	           
            this.setSubCostItemVisitId(output);
            Rlog.debug("subcostitemvisit", "SubCostItemVisitJB.setSubCostItemVisitDetails()");

        }catch (Exception e) {
            Rlog
                    .fatal("subcostitemvisit", "Error in setSubCostItemVisitDetails() in SubCostItemVisitJB "
                            + e);
          return -2;
        }
        return output;
    }
	
	
	public int updateSubCostItemVisit() {
        int output;
        try {
        	SubCostItemVisitAgentRObj subCostAgent = EJBUtil.getSubCostItemVisitAgentHome();
            output = subCostAgent.updateSubCostItemVisit(this
                    .createSubCostItemVisitStateKeeper());
        } catch (Exception e) {
            Rlog.debug("subcostitemvisit",
                    "EXCEPTION IN SETTING SUBJECT COST ITEM VISIT DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    
	public SubCostItemVisitBean createSubCostItemVisitStateKeeper() {
        return new SubCostItemVisitBean(this.subCostItemVisitId, this.fkSubCostItem,
        		this.fkProtocolVisit, this.creator, this.modifiedBy, this.ipAdd);
    }
	
	 public int getExistingItemsByFkItemAndFkVisit(Integer fkItem, Integer fkVisit) {
        int output = 0;
        try {
        	SubCostItemVisitAgentRObj subCostAgent = EJBUtil.getSubCostItemVisitAgentHome();
            Integer eOut = 
            	subCostAgent.getExistingItemsByFkItemAndFkVisit(fkItem, fkVisit);
            if (eOut.intValue() > 0) { return eOut.intValue(); }
        } catch(Exception e) {
            Rlog.fatal("subCostItem", "Error in SubCostItemJB.getExistingItemsByFkItemAndFkVisit "+e);
            output = -1;
        }
        return output;
    }
}
