/* 
 * Classname			BgtApndxJB.class
 * 
 * Version information
 *
 * Date					03/25/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.web.bgtApndx;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.esch.business.bgtApndx.impl.BgtApndxBean;
import com.velos.esch.business.common.BgtApndxDao;
import com.velos.esch.service.bgtApndxAgent.BgtApndxAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for Budget Appendix
 * 
 * @author Sajal
 * @version 1.0 03/21/2002
 */

public class BgtApndxJB {
    /**
     * the budget appendix Id
     */
    private int bgtApndxId;

    /**
     * the Budget
     */
    private String bgtApndxBudget;

    /**
     * the budget appendix description
     */
    private String bgtApndxDesc;

    /**
     * the budget appendix URI
     */
    private String bgtApndxUri;

    /**
     * the budget appendix type
     */

    private String bgtApndxType;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * @param bgtApndxId
     *            The value that is required to be registered as primary key of
     *            budget appendix id
     */

    public void setBgtApndxId(int bgtApndxId) {
        this.bgtApndxId = bgtApndxId;
    }

    /**
     * @return budget appendix id
     */
    public int getBgtApndxId() {
        return this.bgtApndxId;
    }

    /**
     * @param bgtApndxBudget
     *            The value that is required to be registered as the budget for
     *            which appendix are being saved
     */

    public void setBgtApndxBudget(String bgtApndxBudget) {
        this.bgtApndxBudget = bgtApndxBudget;
    }

    /**
     * @return budget id
     */
    public String getBgtApndxBudget() {
        return this.bgtApndxBudget;
    }

    /**
     * @param bgtApndxDesc
     *            The value that is required to be registered as the description
     *            for the budget appendix
     */

    public void setBgtApndxDesc(String bgtApndxDesc) {
        this.bgtApndxDesc = bgtApndxDesc;
    }

    /**
     * @return budget description
     */
    public String getBgtApndxDesc() {
        return this.bgtApndxDesc;
    }

    /**
     * @param bgtApndxUri
     *            The value that is required to be registered as the URI of the
     *            file or URL of the study appendix.
     */

    public void setBgtApndxUri(String bgtApndxUri) {
        this.bgtApndxUri = bgtApndxUri;
    }

    /**
     * @return budget appendix URI
     */

    public String getBgtApndxUri() {
        return this.bgtApndxUri;
    }

    /**
     * @param bgtApndxType
     *            The value that is required to be registered as type of
     *            appendix (F - file, U - URL)
     */

    public void setBgtApndxType(String bgtApndxType) {
        this.bgtApndxType = bgtApndxType;
    }

    /**
     * @return budget appendix type
     */
    public String getBgtApndxType() {
        return this.bgtApndxType;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return last modified by
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return IP
     */
    public String getIpAdd() {
        return this.ipAdd;

    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts bgtApndxId
     * 
     * @param bgtApndxId
     *            Id of the Budget Appendix
     */
    public BgtApndxJB(int bgtApndxId) {
        setBgtApndxId(bgtApndxId);
    }

    /**
     * Default Constructor
     * 
     */
    public BgtApndxJB() {
        Rlog.debug("BgtApndx", "BgtApndxJB.BgtApndxJB() ");
    }

    public BgtApndxJB(int bgtApndxId, String bgtApndxBudget,
            String bgtApndxDesc, String bgtApndxUri, String bgtApndxType) {
        setBgtApndxId(bgtApndxId);
        setBgtApndxBudget(bgtApndxBudget);
        setBgtApndxDesc(bgtApndxDesc);
        setBgtApndxUri(bgtApndxUri);
        setBgtApndxType(bgtApndxType);

        Rlog.debug("BgtApndx", "BgtApndxJB.BgtApndxJB()(all parameters)");
    }

    /**
     * Calls getBgtApndxDetails() of BgtApndx Session Bean: BgtApndxAgentBean
     * 
     * @return The Details of the budget appendix in the bgtApndxStateKeeper
     *         Object
     * @See BgtApndxAgentBean
     */

    public BgtApndxBean getBgtApndxDetails() {
        BgtApndxBean bask = null;

        try {

            BgtApndxAgentRObj bgtApndxAgent = EJBUtil.getBgtApndxAgentHome();
            bask = bgtApndxAgent.getBgtApndxDetails(this.bgtApndxId);
            Rlog.debug("BgtApndx",
                    "BgtApndxJB.getBgtApndxDetails() BgtApndxStateKeeper "
                            + bask);
        } catch (Exception e) {
            Rlog.debug("BgtApndx",
                    "Error in getBgtApndxDetails() in BgtApndxJB " + e);
        }
        if (bask != null) {
            this.bgtApndxId = bask.getBgtApndxId();
            this.bgtApndxBudget = bask.getBgtApndxBudget();
            this.bgtApndxDesc = bask.getBgtApndxDesc();
            this.bgtApndxUri = bask.getBgtApndxUri();
            this.bgtApndxType = bask.getBgtApndxType();
            this.creator = bask.getCreator();
            this.modifiedBy = bask.getModifiedBy();
            this.ipAdd = bask.getIpAdd();
        }
        return bask;
    }

    /**
     * Calls setBgtApndxDetails() of Budget Appendix Session Bean:
     * BgtApndxAgentBean
     * 
     */

    public int setBgtApndxDetails() {

        try {
            BgtApndxAgentRObj bgtApndxAgentRObj = EJBUtil
                    .getBgtApndxAgentHome();
            this.setBgtApndxId(bgtApndxAgentRObj.setBgtApndxDetails(this
                    .createBgtApndxStateKeeper()));
            Rlog.debug("BgtApndx", "BgtApndxJB.setBgtApndxDetails()");
            return this.getBgtApndxId();
        } catch (Exception e) {
            Rlog.debug("BgtApndx",
                    "Error in setBgtApndxDetails() in BgtApndxJB " + e);
            return -2;
        }
    }

    /**
     * Calls updateBgtApndx() of Budget Appendix Session Bean: BgtApndxAgentBean
     * 
     * @return -2 in case of exception
     */
    public int updateBgtApndx() {
        int output;
        try {
            BgtApndxAgentRObj bgtApndxAgentRObj = EJBUtil
                    .getBgtApndxAgentHome();
            output = bgtApndxAgentRObj.updateBgtApndx(this
                    .createBgtApndxStateKeeper());
        } catch (Exception e) {
            Rlog
                    .debug("BgtApndx",
                            "EXCEPTION IN SETTING BUDGET Appendix DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * @return the Budget Appendix StateKeeper Object with the current values of
     *         the Bean
     */

    public BgtApndxBean createBgtApndxStateKeeper() {
        Rlog.debug("BgtApndx", "BgtApndxJB.createBgtApndxStateKeeper ");

        return new BgtApndxBean(bgtApndxId, bgtApndxBudget, bgtApndxDesc,
                bgtApndxUri, bgtApndxType, creator, modifiedBy, ipAdd);
    }

    /**
     * Gets all URLs linked with the budget
     * 
     * @param budgetId
     */
    public BgtApndxDao getBgtApndxUrls(int budgetId) {
        BgtApndxDao bgtApndxDao = new BgtApndxDao();
        try {
            BgtApndxAgentRObj bgtApndxAgentRObj = EJBUtil
                    .getBgtApndxAgentHome();
            bgtApndxDao = bgtApndxAgentRObj.getBgtApndxUrls(budgetId);
        } catch (Exception e) {
            Rlog.fatal("BgtApndx", "EXCEPTION IN getting Urls in budget ");
            e.printStackTrace();
            return bgtApndxDao;
        }
        return bgtApndxDao;
    }

    /**
     * Gets all Files linked with the budget
     * 
     * @param budgetId
     */
    public BgtApndxDao getBgtApndxFiles(int budgetId) {
        BgtApndxDao bgtApndxDao = new BgtApndxDao();
        try {
            BgtApndxAgentRObj bgtApndxAgentRObj = EJBUtil
                    .getBgtApndxAgentHome();
            bgtApndxDao = bgtApndxAgentRObj.getBgtApndxFiles(budgetId);
        } catch (Exception e) {
            Rlog.fatal("BgtApndx", "EXCEPTION IN getting Files in budget ");
            e.printStackTrace();
            return bgtApndxDao;
        }
        return bgtApndxDao;
    }

    /**
     * Delete user from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */

    public int bgtApndxDelete(int bgtApndxId) {
        int ret = 0;
        try {
            Rlog.debug("BgtApndx", "In BgtApndxBean bgtApndxDelete()");
            BgtApndxAgentRObj bgtApndxAgentRObj = EJBUtil
                    .getBgtApndxAgentHome();

            ret = bgtApndxAgentRObj.bgtApndxDelete(bgtApndxId);
            return ret;
        }

        catch (Exception e) {
            Rlog.fatal("BgtApndx", "In BgtApndxBean - delete file/url" + e);
            return -2;
        }

    }
    
    // Overloaded for INF-18183 ::: AGodara 
    public int bgtApndxDelete(int bgtApndxId,Hashtable<String, String> auditInfo) {
        int ret = 0;
        try {
            BgtApndxAgentRObj bgtApndxAgentRObj = EJBUtil
                    .getBgtApndxAgentHome();
            ret = bgtApndxAgentRObj.bgtApndxDelete(bgtApndxId,auditInfo);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("BgtApndx", "In BgtApndxBean - delete file/url bgtApndxDelete(int bgtApndxId,Hashtable<String, String> auditInfo)" + e);
            return -2;
        }
    }

}// end of class
