package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.velos.eres.service.util.StringUtil;
import com.velos.eres.service.util.Rlog;

public class AuditRowEschDao extends CommonDAO implements java.io.Serializable {
	
	public AuditRowEschDao() {    	
    
    }
    public int getRID(int pkey, String tableName, String pkColumnName) {
    	 PreparedStatement pstmt = null;
         Connection conn = null;
         int retInt=0;
         String sql="";
         try{
        	 if (StringUtil.isEmpty(tableName) ||
        		StringUtil.isEmpty(pkColumnName) || pkey < 0){
        		 retInt = -1;
        	 }else{
	        	 conn = getConnection();
	        	 sql="SELECT RID FROM esch."+ tableName +" WHERE "+pkColumnName+" = " + pkey +"";
	        	 pstmt = conn.prepareStatement(sql);
	             ResultSet rs = pstmt.executeQuery();

	             while (rs.next()) {
	            	 retInt = (new Integer(rs.getInt("RID")));
	            	 break;
	             }
        	 }
        	 return retInt;
         }catch (SQLException ex) {
             Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN READING "+ tableName +" table "+ ex);
             return retInt;
         } finally {
             try {
                 if (pstmt != null)
                     pstmt.close();
             } catch (Exception e) {
             }
             try {
                 if (conn != null)
                     conn.close();
             } catch (Exception e) {
             }
         }
    }
    
    public int setReasonForChange(int raid, String remarks) {
   	 	PreparedStatement pstmt = null;
        Connection conn = null;
        int ret=0;
        String sql="";
        try{
       	 if (StringUtil.isEmpty(remarks) || raid < 0){
       		 ret = -1;
       	 }else{
        	 conn = getConnection();
        	 sql="UPDATE esch.AUDIT_COLUMN set REMARKS ='"+ remarks +"' WHERE RAID ="+raid
        	 +" AND (REMARKS IS NULL OR LENGTH(REMARKS)=0)";
        	 pstmt = conn.prepareStatement(sql);
       		 ret=pstmt.executeUpdate();
       		 conn.commit();
       		 ret = 1;
       	 }
       	 return ret;
        }catch (SQLException ex) {
            Rlog.fatal("","AuditRowEschDao.getRID EXCEPTION IN UPDATING AUDIT_COLUMN table " + ex);
            return ret;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
   	
   }
}  
