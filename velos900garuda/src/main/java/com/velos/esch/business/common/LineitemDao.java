/*
 * Classname	LineitemDao.class
 * 
 * Version information 	1.0
 *
 * Date	04/09/2002
 * 
 * Author -- Sajal
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

public class LineitemDao extends CommonDAO implements java.io.Serializable {
    private ArrayList lineitemIds;

    private ArrayList lineitemBgtSectionIds;

    private ArrayList lineitemNames;

    private ArrayList lineitemDescs;

    private ArrayList lineitemSponsorUnits;

    private ArrayList lineitemClinicNOfUnits;

    private ArrayList lineitemOtherCosts;

    private ArrayList lineitemDelFlags;

    private ArrayList lineitemInPerSecs;

    private ArrayList bgtSectionIds;

    private ArrayList bgtSectionBgtcalIds;

    private ArrayList bgtSectionNames;

    private ArrayList bgtSectionVisits;

    private ArrayList bgtSectionDelFlags;

    private ArrayList bgtSectionSequences;

    private ArrayList bgtSectionPatNos;

    private ArrayList bgtSectionPersonlFlags;

    private ArrayList bgtSectionTypes;

    private ArrayList lineitemStdCareCosts;

    private ArrayList lineitemResCosts;

    private ArrayList lineitemInvCosts;

    private ArrayList lineitemInCostDiscs;

    private ArrayList lineitemCptCodes;

    private ArrayList lineitemRepeats;

    private ArrayList lineitemNotes;

    private ArrayList lineitemInclusions;

    private ArrayList applyInFuture;

    private ArrayList lineitemCategories;

    private ArrayList lineitemTMIDs;

    private ArrayList lineitemCDMs;

    private ArrayList lineitemAppIndirects;
    
    private ArrayList lineitemTotalCosts;
    
    //added by IA 11.03.2006
    
    private ArrayList lineItemSponsorAmounts;
    
    private ArrayList lineItemVariances;
    
    private ArrayList srtCostVariances;
    
    private ArrayList socCostSponsors;
    
    private ArrayList socCostVariances;
    
    private ArrayList srtCostSponsors;
   
    
    //end added
    
    //Added by IA 9/18/2006
    
    private ArrayList srtCostTotals;
    
    private ArrayList srtCostGrandTotals;
    
    private ArrayList socCostTotals;
    
    private ArrayList socCostGrandTotals;
    //Edn Added

    private ArrayList arFkEvents;
    
    private ArrayList arFKVisits;
    
    private ArrayList arLineItemSequence;
    private ArrayList arLineItemCostType;
    
    private int bRows;

    public LineitemDao() {
    	
    	arFkEvents = new ArrayList();
    	arFKVisits = new ArrayList();
    	
        lineitemIds = new ArrayList();
        lineitemBgtSectionIds = new ArrayList();
        lineitemNames = new ArrayList();
        lineitemDescs = new ArrayList();
        lineitemSponsorUnits = new ArrayList();
        lineitemClinicNOfUnits = new ArrayList();
        lineitemOtherCosts = new ArrayList();
        lineitemDelFlags = new ArrayList();
        lineitemInPerSecs = new ArrayList();
        bgtSectionIds = new ArrayList();
        bgtSectionBgtcalIds = new ArrayList();
        bgtSectionNames = new ArrayList();
        bgtSectionVisits = new ArrayList();
        bgtSectionDelFlags = new ArrayList();
        bgtSectionSequences = new ArrayList();
        bgtSectionPatNos = new ArrayList();
        bgtSectionPersonlFlags = new ArrayList();
        bgtSectionTypes = new ArrayList();

        lineitemStdCareCosts = new ArrayList();
        lineitemResCosts = new ArrayList();
        lineitemInvCosts = new ArrayList();
        lineitemInCostDiscs = new ArrayList();
        lineitemCptCodes = new ArrayList();
        lineitemRepeats = new ArrayList();
        lineitemNotes = new ArrayList();
        lineitemInclusions = new ArrayList();
        applyInFuture = new ArrayList();
        lineitemCategories = new ArrayList();
        lineitemTMIDs = new ArrayList();
        lineitemCDMs = new ArrayList();
        lineitemAppIndirects = new ArrayList();
        lineitemTotalCosts = new ArrayList();
        
        //Added by IA 11.03.2006
                
        lineItemSponsorAmounts = new ArrayList();
        lineItemVariances = new ArrayList();

        srtCostVariances  = new ArrayList();
        socCostSponsors  = new ArrayList();
        socCostVariances  = new ArrayList();
        srtCostSponsors  = new ArrayList();
            
        //End Added
        
        
        //Added by IA 9/18/2006
        srtCostTotals = new ArrayList();
        srtCostGrandTotals  = new ArrayList();
        socCostTotals  = new ArrayList();
        socCostGrandTotals  = new ArrayList();
        
        //End Added IA 9/18/2006
        
        arLineItemSequence = new ArrayList();
        arLineItemCostType= new ArrayList();
        
    }

    
    // Getter and Setter methods

    public ArrayList getLineitemIds() {
        return this.lineitemIds;
    }

    public void setLineitemIds(ArrayList lineitemIds) {
        this.lineitemIds = lineitemIds;
    }

    public ArrayList getLineitemBgtSectionIds() {
        return this.lineitemBgtSectionIds;
    }

    public void setLineitemBgtSectionIds(ArrayList lineitemBgtSectionIds) {
        this.lineitemBgtSectionIds = lineitemBgtSectionIds;
    }

    public ArrayList getLineitemNames() {
        return this.lineitemNames;
    }

    public void setLineitemNames(ArrayList lineitemNames) {
        this.lineitemNames = lineitemNames;
    }

    public ArrayList getLineitemDescs() {
        return this.lineitemDescs;
    }

    public void setLineitemDescs(ArrayList lineitemDescs) {
        this.lineitemDescs = lineitemDescs;
    }

    public Object getLineitemDescs(int i) {

        return this.lineitemDescs.get(i);
    }

    public ArrayList getLineitemSponsorUnits() {
        return this.lineitemSponsorUnits;
    }

    public void setLineitemSponsorUnits(ArrayList lineitemSponsorUnits) {
        this.lineitemSponsorUnits = lineitemSponsorUnits;
    }

    public ArrayList getLineitemClinicNOfUnits() {
        return this.lineitemClinicNOfUnits;
    }

    public void setLineitemClinicNOfUnits(ArrayList lineitemClinicNOfUnits) {
        this.lineitemClinicNOfUnits = lineitemClinicNOfUnits;
    }

    public ArrayList getLineitemOtherCosts() {
        return this.lineitemOtherCosts;
    }

    public void setLineitemOtherCosts(ArrayList lineitemOtherCosts) {
        this.lineitemOtherCosts = lineitemOtherCosts;
    }

    public ArrayList getLineitemDelFlags() {
        return this.lineitemDelFlags;
    }

    public void setLineitemDelFlags(ArrayList lineitemDelFlags) {
        this.lineitemDelFlags = lineitemDelFlags;
    }

    public ArrayList getLineitemInPerSecs() {
        return this.lineitemInPerSecs;
    }

    public void setLineitemInPerSecs(ArrayList lineitemInPerSecs) {
        this.lineitemInPerSecs = lineitemInPerSecs;
    }

    public ArrayList getBgtSectionIds() {
        return this.bgtSectionIds;
    }

    public void setBgtSectionIds(ArrayList bgtSectionIds) {
        this.bgtSectionIds = bgtSectionIds;
    }

    public ArrayList getBgtSectionBgtcalIds() {
        return this.bgtSectionBgtcalIds;
    }

    public void setBgtSectionBgtcalIds(ArrayList bgtSectionBgtcalIds) {
        this.bgtSectionBgtcalIds = bgtSectionBgtcalIds;
    }

    public ArrayList getBgtSectionNames() {
        return this.bgtSectionNames;
    }

    public void setBgtSectionNames(ArrayList bgtSectionNames) {
        this.bgtSectionNames = bgtSectionNames;
    }

    public ArrayList getBgtSectionVisits() {
        return this.bgtSectionVisits;
    }

    public void setBgtSectionVisits(ArrayList bgtSectionVisits) {
        this.bgtSectionVisits = bgtSectionVisits;
    }

    public ArrayList getBgtSectionDelFlags() {
        return this.bgtSectionDelFlags;
    }

    public void setBgtSectionDelFlags(ArrayList bgtSectionDelFlags) {
        this.bgtSectionDelFlags = bgtSectionDelFlags;
    }

    public ArrayList getBgtSectionSequences() {
        return this.bgtSectionSequences;
    }

    public void setBgtSectionSequences(ArrayList bgtSectionSequences) {
        this.bgtSectionSequences = bgtSectionSequences;
    }

    public ArrayList getBgtSectionPatNos() {
        return this.bgtSectionPatNos;
    }

    public void setBgtSectionPatNos(ArrayList bgtSectionPatNos) {
        this.bgtSectionPatNos = bgtSectionPatNos;
    }

    public ArrayList getBgtSectionPersonlFlags() {
        return this.bgtSectionPersonlFlags;
    }

    public void setBgtSectionPersonlFlags(ArrayList bgtSectionPersonlFlags) {
        this.bgtSectionPersonlFlags = bgtSectionPersonlFlags;
    }

    public ArrayList getBgtSectionTypes() {
        return this.bgtSectionTypes;
    }

    public void setBgtSectionTypes(ArrayList bgtSectionTypes) {
        this.bgtSectionTypes = bgtSectionTypes;
    }

    public ArrayList getApplyInFuture() {
        return this.applyInFuture;
    }

    public void setApplyInFuture(ArrayList applyInFuture) {
        this.applyInFuture = applyInFuture;
    }

    public void setApplyInFuture(String applyInFutur) {
        applyInFuture.add(applyInFutur);
    }

    public void setLineitemIds(Integer lineitemId) {
        lineitemIds.add(lineitemId);
    }

    public void setLineitemBgtSectionIds(String lineitemBgtSectionId) {
        lineitemBgtSectionIds.add(lineitemBgtSectionId);
    }

    public void setLineitemNames(String lineitemName) {
        lineitemNames.add(lineitemName);
    }

    public void setLineitemDescs(String lineitemDesc) {
        lineitemDescs.add(lineitemDesc);
    }

    public void setLineitemInPerSecs(String lineitemInPerSec) {
        lineitemInPerSecs.add(lineitemInPerSec);
    }

    public void setLineitemSponsorUnits(String lineitemSponsorUnit) {
        lineitemSponsorUnits.add(lineitemSponsorUnit);
    }

    public void setLineitemClinicNOfUnits(String lineitemClinicNOfUnit) {
        lineitemClinicNOfUnits.add(lineitemClinicNOfUnit);
    }

    public void setLineitemOtherCosts(String lineitemOtherCost) {
        lineitemOtherCosts.add(lineitemOtherCost);
    }

    public void setLineitemDelFlags(String lineitemDelFlag) {
        lineitemDelFlags.add(lineitemDelFlag);
    }

    public void setBgtSectionIds(Integer bgtSectionId) {
        bgtSectionIds.add(bgtSectionId);
    }

    public void setBgtSectionBgtcalIds(String bgtSectionBgtcalId) {
        bgtSectionBgtcalIds.add(bgtSectionBgtcalId);
    }

    public void setBgtSectionNames(String bgtSectionName) {
        bgtSectionNames.add(bgtSectionName);
    }

    public void setBgtSectionVisits(String bgtSectionVisit) {
        bgtSectionVisits.add(bgtSectionVisit);
    }

    public void setBgtSectionDelFlags(String bgtSectionDelFlag) {
        bgtSectionDelFlags.add(bgtSectionDelFlag);
    }

    public void setBgtSectionSequences(String bgtSectionSequence) {
        bgtSectionSequences.add(bgtSectionSequence);
    }

    public ArrayList getLineitemStdCareCosts() {
        return this.lineitemStdCareCosts;
    }

    public void setLineitemStdCareCosts(ArrayList lineitemStdCareCosts) {
        this.lineitemStdCareCosts = lineitemStdCareCosts;
    }

    public void setLineitemStdCareCosts(String lineitemStdCareCost) {
        lineitemStdCareCosts.add(lineitemStdCareCost);
    }

    public ArrayList getLineitemResCosts() {
        return this.lineitemResCosts;
    }

    public void setLineitemResCosts(ArrayList lineitemResCosts) {
        this.lineitemResCosts = lineitemResCosts;
    }

    public void setLineitemResCosts(String lineitemResCost) {
        lineitemResCosts.add(lineitemResCost);
    }

    public ArrayList getLineitemInvCosts() {
        return this.lineitemInvCosts;
    }

    public void setLineitemInvCosts(ArrayList lineitemInvCosts) {
        this.lineitemInvCosts = lineitemInvCosts;
    }

    public void setLineitemInvCosts(String lineitemInvCost) {
        lineitemInvCosts.add(lineitemInvCost);
    }

    public ArrayList getLineitemInCostDiscs() {
        return this.lineitemInCostDiscs;
    }

    public void setLineitemInCostDiscs(ArrayList lineitemInCostDiscs) {
        this.lineitemInCostDiscs = lineitemInCostDiscs;
    }

    public void setLineitemInCostDiscs(String lineitemInCostDisc) {
        lineitemInCostDiscs.add(lineitemInCostDisc);
    }

    public ArrayList getLineitemCptCodes() {
        return this.lineitemCptCodes;
    }

    public void setLineitemCptCodes(ArrayList lineitemCptCodes) {
        this.lineitemCptCodes = lineitemCptCodes;
    }

    public void setLineitemCptCodes(String lineitemCptCode) {
        lineitemCptCodes.add(lineitemCptCode);
    }

    public ArrayList getLineitemRepeats() {
        return this.lineitemRepeats;
    }

    public void setLineitemRepeats(ArrayList lineitemRepeats) {
        this.lineitemRepeats = lineitemRepeats;
    }

    public void setLineitemRepeats(String lineitemRepeat) {
        lineitemRepeats.add(lineitemRepeat);
    }

    public void setBgtSectionPatNos(String bgtSectionPatNo) {
        bgtSectionPatNos.add(bgtSectionPatNo);
    }

    public ArrayList getLineitemCategories() {
        return this.lineitemCategories;
    }

    public void setLineitemCategories(ArrayList lineitemCategories) {
        this.lineitemCategories = lineitemCategories;
    }

    public void setLineitemCategories(String lineitemCategory) {
        lineitemCategories.add(lineitemCategory);
    }

    public ArrayList getLineitemTMIDs() {
        return this.lineitemTMIDs;
    }

    public void setLineitemTMIDs(ArrayList lineitemTMIDs) {
        this.lineitemTMIDs = lineitemTMIDs;
    }

    public void setLineitemTMIDs(String lineitemTMID) {
        lineitemTMIDs.add(lineitemTMID);
    }

    public ArrayList getLineitemCDMs() {
        return this.lineitemCDMs;
    }

    public void setLineitemCDMs(ArrayList lineitemCDMs) {
        this.lineitemCDMs = lineitemCDMs;
    }

    public void setLineitemCDMs(String lineitemCDM) {
        lineitemCDMs.add(lineitemCDM);
    }

    public void setBgtSectionPersonlFlags(String bgtSectionPersonlFlag) {
        bgtSectionPersonlFlags.add(bgtSectionPersonlFlag);
    }

    public void setBgtSectionTypes(String bgtSectionType) {
        bgtSectionTypes.add(bgtSectionType);
    }

    public ArrayList getLineitemNotes() {
        return this.lineitemNotes;
    }

    public void setLineitemNotes(ArrayList lineitemNotes) {
        this.lineitemNotes = lineitemNotes;
    }

    public void setLineitemNotes(String lineitemNote) {
        lineitemNotes.add(lineitemNote);
    }

    public ArrayList getLineitemInclusions() {
        return this.lineitemInclusions;
    }

    public void setLineitemInclusions(ArrayList lineitemInclusions) {
        this.lineitemInclusions = lineitemInclusions;
    }

    public void setLineitemInclusions(String lineitemInclusion) {
        lineitemInclusions.add(lineitemInclusion);
    }

    public ArrayList getLineitemAppIndirects() {
        return this.lineitemAppIndirects;
    }

    public void setLineitemAppIndirects(ArrayList lineitemAppIndirects) {
        this.lineitemAppIndirects = lineitemAppIndirects;
    }

    public void setLineitemAppIndirects(String lineitemAppIndirect) {
        lineitemAppIndirects.add(lineitemAppIndirect);
    }

    public void setBRows(int bRows) {
        this.bRows = bRows;
    }

    public int getBRows() {

        return this.bRows;
    }
    
  //  lineitemTotalCosts
    
    public ArrayList getLineitemTotalCost() {
        return this.lineitemTotalCosts;
    }

    public void setLineitemTotalCost(ArrayList lineitemTotalCosts) {
        this.lineitemTotalCosts = lineitemTotalCosts;
    }

    public void setLineitemTotalCost(String lineitemTotalCost) {
    	lineitemTotalCosts.add(lineitemTotalCost);
    }

    //Added by IA 11.03.2006 comparative Budget
    
    public ArrayList getLineItemSponsorAmount() {
        return this.lineItemSponsorAmounts;
    }

    public void setLineItemSponsorAmount(ArrayList lineItemSponsorAmounts) {
        this.lineItemSponsorAmounts = lineItemSponsorAmounts;
    }

    public void setLineItemSponsorAmount(String lineItemSponsorAmount) {
    	lineItemSponsorAmounts.add(lineItemSponsorAmount);
    }
    
    public ArrayList getLineItemVariance() {
        return this.lineItemVariances;
    }

    public void setLineItemVariance(ArrayList lineItemVariances) {
        this.lineItemVariances = lineItemVariances;
    }

    public void setLineItemVariance(String lineItemVariance) {
    	lineItemVariances.add(lineItemVariance);
    }
    
    
    
    //End Added
    

    //Added by IA 9/18/2006
    public ArrayList getSrtCostTotal() {
        return this.srtCostTotals;
    }

    public void setSrtCostTotal(ArrayList srtCostTotals) {
        this.srtCostTotals = srtCostTotals;
    }
    
    public void setSrtCostTotal(String srtCostTotal) {
    	srtCostTotals.add(srtCostTotal);
    	
    }

    public ArrayList getSrtCostGrandTotal() {
        return this.srtCostGrandTotals;
    }

    public void setSrtCostGrandTotal(ArrayList srtCostGrandTotals) {
        this.srtCostGrandTotals = srtCostGrandTotals;
    }
    
    public void setSrtCostGrandTotal(String srtCostGrandTotal) {
    	srtCostGrandTotals.add(srtCostGrandTotal);
    }
    
    public ArrayList getSocCostTotal() {
        return this.socCostTotals;
    }

    public void setSocCostTotal(ArrayList socCostTotals) {
        this.socCostTotals = socCostTotals;
    }

    
    public void setSocCostTotal(String socCostTotal) {
    	socCostTotals.add(socCostTotal);
    }
    
    public ArrayList getSocCostGrandTotal() {
        return this.socCostGrandTotals;
    }

    public void setSocCostGrandTotal(ArrayList socCostGrandTotals) {
        this.socCostGrandTotals = socCostGrandTotals;
    }

    public void setSocCostGrandTotal(String socCostGrandTotal) {
    	socCostGrandTotals.add(socCostGrandTotal);
    }
    
    //End Added
    
    
    //added by IA 11.03.2006

    public ArrayList getSrtCostSponsor() {
        return this.srtCostSponsors;
    }

    public void setSrtCostSponsor(ArrayList srtCostSponsors) {
        this.srtCostSponsors = srtCostSponsors;
    }

    public void setSrtCostSponsor(String srtCostSponsor) {
    	srtCostSponsors.add(srtCostSponsor);
    }

    public ArrayList getSrtCostVariance() {
        return this.srtCostVariances;
    }

    public void setSrtCostVariance(ArrayList srtCostVariances) {
        this.srtCostVariances = srtCostVariances;
    }

    public void setSrtCostVariance(String srtCostVariance) {
    	srtCostVariances.add(srtCostVariance);
    }
    

    public ArrayList getSocCostSponsor() {
        return this.socCostSponsors;
    }

    public void setSocCostSponsor(ArrayList socCostSponsors) {
        this.socCostSponsors = socCostSponsors;
    }

    public void setSocCostSponsor(String socCostSponsor) {
    	socCostSponsors.add(socCostSponsor);
    }
    
    
    public ArrayList getSocCostVariance() {
        return this.socCostVariances;
    }

    public void setSocCostVariance(ArrayList socCostVariances) {
        this.socCostVariances = socCostVariances;
    }

    public void setSocCostVariance(String socCostVariance) {
    	socCostVariances.add(socCostVariance);
    }
  
	
    //End added

    // end of getter and setter methods

    /**
     * Get the detials of all the lineitems associated to a budget calendar
     * 
     * @param bgtcalId
     */

    public void getLineitemsOfBgtcal(int bgtcalId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select li.PK_LINEITEM, "
                    + "li.LINEITEM_NAME,  "
                    + "li.LINEITEM_DESC,  "
                    + "li.LINEITEM_NOTES,  "
                    + "li.LINEITEM_SPONSORUNIT,  "
                    + "li.LINEITEM_CLINICNOFUNIT,  "
                    + "li.LINEITEM_OTHERCOST,  "
                    + "li.LINEITEM_DELFLAG,  "
                    + "li.LINEITEM_STDCARECOST,  "
                    + "li.LINEITEM_RESCOST,  "
                    + "li.LINEITEM_INVCOST,  "
                    + "li.LINEITEM_INCOSTDISC,  "
                    + "li.LINEITEM_CPTCODE,  "
                    + "li.LINEITEM_REPEAT,  "
                    + "li.LINEITEM_INPERSEC,  "
                    + "li.FK_CODELST_CATEGORY,  "
                    + "li.LINEITEM_APPLYINDIRECTS,  "
                    + "li.LINEITEM_TOTALCOST,  "
                    + "li.LINEITEM_SPONSORAMOUNT,  " //Added By IA 11.03.2006
                    + "li.LINEITEM_VARIANCE,  " //Added by IA 11.03.2006
                    + "bs.PK_BUDGETSEC,  "
                    + "bs.BGTSECTION_NAME,  "
                    + "bs.BGTSECTION_VISIT,  "
                    + "bs.BGTSECTION_DELFLAG,  "
                    + "bs.BGTSECTION_SEQUENCE,  "
                    + "bs.BGTSECTION_PATNO,  "
                    + "bs.BGTSECTION_PERSONLFLAG,  "
                    + "bs.BGTSECTION_TYPE,  "
                    + "bs.SRT_COST_TOTAL,  "
                    + "bs.SRT_COST_GRANDTOTAL,  "
                    + "bs.SOC_COST_TOTAL,  "
                    + "bs.SOC_COST_GRANDTOTAL,  "
                    + "bs.SOC_COST_SPONSOR,  "//Added by IA 11.03.2006 Comparative Budget
                    + "bs.SOC_COST_VARIANCE,  "
                    + "bs.SRT_COST_VARIANCE,  "
                    + "bs.SRT_COST_SPONSOR , bs.fk_visit,li.fk_event  "// End Added by IA 11.03.2006 Comparative Budget
                    + "from (select * from sch_lineitem where (LINEITEM_DELFLAG <> 'Y'  "
                    + "or LINEITEM_DELFLAG is null)  ) li, sch_bgtsection bs  "
                    + "where bs.FK_BGTCAL = ? "
                    + "and bs.PK_BUDGETSEC = li.FK_BGTSECTION (+) "
                    + "and (nvl(bs.BGTSECTION_DELFLAG,'Z') <> 'Y' and nvl(bs.BGTSECTION_DELFLAG,'Z') <> 'P' and nvl(bs.BGTSECTION_DELFLAG,'Z') <> 'R'"
                    + "or bs.BGTSECTION_DELFLAG is null) "
                    + "AND NVL(SUBCOST_ITEM_FLAG,0) != 1 "
                    + "order by BGTSECTION_SEQUENCE, lineitem_seq, lower(LINEITEM_NAME) ";
            
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, bgtcalId);

            Rlog.debug("Lineitem", "LineitemDao.getLineitemsOfBgtcal SQL**** "
                    + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLineitemIds(new Integer(rs.getInt("PK_LINEITEM")));

                setLineitemNames(rs.getString("LINEITEM_NAME"));
                setLineitemDescs(rs.getString("LINEITEM_DESC"));
                setLineitemNotes(rs.getString("LINEITEM_Notes"));

                setLineitemSponsorUnits(rs.getString("LINEITEM_SPONSORUNIT"));

                setLineitemClinicNOfUnits(rs
                        .getString("LINEITEM_CLINICNOFUNIT"));

                setLineitemOtherCosts(rs.getString("LINEITEM_OTHERCOST"));

                setLineitemDelFlags(rs.getString("LINEITEM_DELFLAG"));
                setLineitemInPerSecs(rs.getString("LINEITEM_INPERSEC"));

                setLineitemStdCareCosts(rs.getString("LINEITEM_STDCARECOST"));

                setLineitemResCosts(rs.getString("LINEITEM_RESCOST"));

                setLineitemInvCosts(rs.getString("LINEITEM_INVCOST"));

                setLineitemInCostDiscs(rs.getString("LINEITEM_INCOSTDISC"));
                setLineitemCptCodes(rs.getString("LINEITEM_CPTCODE"));
                setLineitemRepeats(rs.getString("LINEITEM_REPEAT"));

                setLineitemCategories(rs.getString("FK_CODELST_CATEGORY"));
                setLineitemAppIndirects(rs.getString("LINEITEM_APPLYINDIRECTS"));
                
                setLineitemTotalCost(rs.getString("LINEITEM_TOTALCOST"));
                
                //Added by IA 11.03.2006 Comparative Budget
                
                
                setLineItemSponsorAmount(rs.getString("LINEITEM_SPONSORAMOUNT"));
                
                setLineItemVariance(rs.getString("LINEITEM_VARIANCE"));
 
                setSrtCostSponsor(rs.getString("SRT_COST_SPONSOR"));
                
                setSrtCostVariance(rs.getString("SRT_COST_VARIANCE"));
                
                setSocCostSponsor(rs.getString("SOC_COST_SPONSOR"));
                
                setSocCostVariance(rs.getString("SOC_COST_VARIANCE"));

                
                //End added
                
                setBgtSectionIds(new Integer(rs.getInt("PK_BUDGETSEC")));

                setBgtSectionNames(rs.getString("BGTSECTION_NAME"));

                setBgtSectionVisits(rs.getString("BGTSECTION_VISIT"));

                setBgtSectionDelFlags(rs.getString("BGTSECTION_DELFLAG"));

                setBgtSectionSequences(rs.getString("BGTSECTION_SEQUENCE"));

                setBgtSectionPatNos(rs.getString("BGTSECTION_PATNO"));
                setBgtSectionPersonlFlags(rs
                        .getString("BGTSECTION_PERSONLFLAG"));
                setBgtSectionTypes(rs.getString("BGTSECTION_TYPE"));


                
                //Added by IA 9/18/2006
                
                setSrtCostTotal(rs.getString("SRT_COST_TOTAL"));
                
                setSrtCostGrandTotal(rs.getString("SRT_COST_GRANDTOTAL"));
                
                setSocCostTotal(rs.getString("SOC_COST_TOTAL"));
                
                setSocCostGrandTotal(rs.getString("SOC_COST_GRANDTOTAL"));
                
                //End Added By IA 9/18/2006
                
                setArFKVisits(rs.getString("FK_VISIT"));
                setArFkEvents(rs.getString("FK_EVENT"));
                
                rows++;

            }
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Lineitem",
                    "LineitemDao.getLineitemsOfBgtcal EXCEPTION IN FETCHING FROM "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Inserts lineitems to a section depending on its repeat line type
     * 
     * @param String
     *            Array containing lineitems names
     * @param String
     *            Array containing lineitems desciptions
     * @param String
     *            Array containing lineitems cptCodes
     * @param String
     *            Array containing lineitems repeat options
     * @param String
     *            Array containing lineitems categories
     * @param String
     *            Array containing lineitems tmids
     * @param String
     *            Array containing lineitems cdms
     * @param String
     *            containing budget section id
     * @param String
     *            containing budget cal id
     * @param String
     *            containing creator
     * @param String
     *            containing ip address
     * @return returns pk_lineitem of the last record inserted if successful
     *         else returns -1;
     */

    public int setRepeatLineitems(String[] lineitemNames,
            String[] lineitemDescs, String[] lineitemCpts,
            String[] lineitemRepeatOpts, String[] categories, String[] tmids,
            String[] cdms, String bgtSectionId, String budgetCalId,
            String ipAdd, String usr) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = -1;

        try {

            conn = getConnection();

            ArrayDescriptor dNames = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inNames = new ARRAY(dNames, conn, lineitemNames);

            ArrayDescriptor dDescs = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inDescs = new ARRAY(dDescs, conn, lineitemDescs);

            ArrayDescriptor dCpt = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCpts = new ARRAY(dCpt, conn, lineitemCpts);

            ArrayDescriptor dCategory = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCategory = new ARRAY(dCategory, conn, categories);

            ArrayDescriptor dTMID = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inTMID = new ARRAY(dTMID, conn, tmids);

            ArrayDescriptor dCDM = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCDM = new ARRAY(dCDM, conn, cdms);

            ArrayDescriptor dRepeat = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inRepeat = new ARRAY(dRepeat, conn, lineitemRepeatOpts);

            cstmt = conn
                    .prepareCall("{call pkg_bgt.sp_insert_repeat_line_items(?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, StringUtil.stringToNum(budgetCalId));

            cstmt.setArray(2, inNames);

            cstmt.setArray(3, inDescs);

            cstmt.setArray(4, inCpts);

            cstmt.setArray(5, inRepeat);

            cstmt.setArray(6, inCategory);

            cstmt.setArray(7, inTMID);

            cstmt.setArray(8, inCDM);

            cstmt.setInt(9, StringUtil.stringToNum(usr));

            cstmt.setString(10, ipAdd);

            cstmt.registerOutParameter(11, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(11);
            Rlog.debug("Lineitem", "in SP ret" + ret);
            return ret;

        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "lineitem",
                            "In insertRepeatLineItems in libeitemdao line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    /**
     * Inserts personnel cost type lineitems
     * 
     * @param int
     *            containing budget id
     * @param String
     *            Array containing lineitem names
     * @param String
     *            Array containing rates which is inserted in
     *            lineitem_sponsororunit
     * @param String
     *            Array containing lineitems inclusion type, 1- in personnel
     *            cost , 0- in all sections. this is inserted in
     *            lineitem_inpersec column
     * @param String
     *            Array containing notes
     * @param String
     *            Array containing categories
     * @param String
     *            Array containing tmids
     * @param String
     *            Array containing cdms
     * @param String
     *            containing creator
     * @param String
     *            containing ip address
     * @return returns pk_lineitem of the last record inserted if successful
     *         else returns -1;
     */

    public int insertPersonnelCost(int budgetId, String[] perTypes,
            String[] rates, String[] inclusions, String[] notes,
            String[] categories, String[] tmids, String[] cdms, int creator,
            String ipAdd) {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor dTypes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inTypes = new ARRAY(dTypes, conn, perTypes);

            ArrayDescriptor dRates = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inRates = new ARRAY(dRates, conn, rates);

            ArrayDescriptor dInclusion = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inInclusion = new ARRAY(dInclusion, conn, inclusions);

            ArrayDescriptor dNotes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inNotes = new ARRAY(dNotes, conn, notes);

            ArrayDescriptor dCategory = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCategory = new ARRAY(dCategory, conn, categories);

            ArrayDescriptor dTMID = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inTMID = new ARRAY(dTMID, conn, tmids);

            ArrayDescriptor dCDM = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCDM = new ARRAY(dCDM, conn, cdms);

            cstmt = conn
                    .prepareCall("{call PKG_BGT.SP_INSERT_PERSONNEL_COST(?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, budgetId);

            cstmt.setArray(2, inTypes);

            cstmt.setArray(3, inRates);

            cstmt.setArray(4, inInclusion);

            cstmt.setArray(5, inNotes);

            cstmt.setArray(6, inCategory);

            cstmt.setArray(7, inTMID);

            cstmt.setArray(8, inCDM);

            cstmt.setInt(9, creator);

            cstmt.setString(10, ipAdd);

            cstmt.registerOutParameter(11, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(11);

            return ret;

        }

        catch (SQLException ex) {
            Rlog.fatal("lineitem",
                    "In insertPersonnelCost in lineitemdao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    /*
     * Get all lineitems of default section @param int budget cal id
     */

    public void getLineitemsOfDefaultSection(int budgetCalId) {

        Rlog.debug("Lineitem", "LineitemDao.inside  budget id " + budgetCalId);
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select pk_lineitem, " + " lineitem_name, "
                    + " lineitem_sponsorunit, " + " lineitem_inpersec, "
                    + " lineitem_applyinfuture, " + " fk_codelst_category, "
                    + " lineitem_tmid, " + " lineitem_cdm, "
                    + " lineitem_notes, lineitem_totalcost "                 
                    + " from sch_bgtsection s, sch_bgtcal c, sch_lineitem l "
                    + " where " + " c.pk_bgtcal = s.fk_bgtcal "
                    + " and c.pk_bgtcal = ? "
                    + " and l.fk_bgtsection = s.pk_budgetsec "
                    + " and nvl(bgtsection_delflag,'Z') ='P' "
                    + " and nvl(lineitem_delflag,'Z') <> 'Y' order by lineitem_seq, lower(LINEITEM_NAME) ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetCalId);

            Rlog.debug("Lineitem",
                    "LineitemDao.getLineitemsOfDefaultSection SQL**** "
                            + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLineitemIds(new Integer(rs.getInt("PK_LINEITEM")));

                setLineitemNames(rs.getString("LINEITEM_NAME"));

                setLineitemSponsorUnits(rs.getString("LINEITEM_SPONSORUNIT"));

                setLineitemInclusions(rs.getString("LINEITEM_INPERSEC"));

                setLineitemNotes(rs.getString("LINEITEM_NOTES"));

                setApplyInFuture(rs.getString("LINEITEM_APPLYINFUTURE"));

                setLineitemCategories(rs.getString("FK_CODELST_CATEGORY"));

                setLineitemTMIDs(rs.getString("LINEITEM_TMID"));

                setLineitemCDMs(rs.getString("LINEITEM_CDM"));
                
                setLineitemTotalCost(rs.getString("LINEITEM_TOTALCOST"));

                rows++;

            }
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Lineitem",
                    "LineitemDao.getLineitemsOfDefaultSection EXCEPTION IN FETCHING FROM "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Update personnel cost type lineitems depending on inclusion and future
     * apply type
     * 
     * @param int
     *            containing budget cal id
     * @param String
     *            Array containing default section's lineitem ids
     * @param String
     *            Array containing lineitem names
     * @param String
     *            Array containing rates which is inserted in
     *            lineitem_sponsororunit
     * @param String
     *            Array containing lineitems inclusion type, 1- in personnel
     *            cost , 0- in all sections. this is inserted in
     *            lineitem_inpersec column
     * @param String
     *            Array containing notes
     * @param String
     *            containing creator
     * @param String
     *            containing ip address
     * @return returns pk_lineitem of the last record inserted if successful
     *         else returns -1;
     */

    public int updatePersonnelCost(int budgetCalId, String[] parentIds,
            String[] personnelTypes, String[] rates, String[] inclusions,
            String[] notes, String[] applyTypes, String[] categories,
            String[] tmids, String[] cdms, int creator, String ipAdd) {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor dParentIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inParentIds = new ARRAY(dParentIds, conn, parentIds);

            ArrayDescriptor dTypes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inTypes = new ARRAY(dTypes, conn, personnelTypes);

            ArrayDescriptor dRates = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inRates = new ARRAY(dRates, conn, rates);

            ArrayDescriptor dInclusion = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inInclusion = new ARRAY(dInclusion, conn, inclusions);

            ArrayDescriptor dNotes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inNotes = new ARRAY(dNotes, conn, notes);

            ArrayDescriptor dApplyTypes = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inApplyTypes = new ARRAY(dApplyTypes, conn, applyTypes);

            ArrayDescriptor dCategory = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCategory = new ARRAY(dCategory, conn, categories);

            ArrayDescriptor dTMID = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inTMID = new ARRAY(dTMID, conn, tmids);

            ArrayDescriptor dCDM = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCDM = new ARRAY(dCDM, conn, cdms);

            cstmt = conn
                    .prepareCall("{call PKG_BGT.SP_UPDATE_PERSONNEL_COST(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, budgetCalId);

            cstmt.setArray(2, inParentIds);

            cstmt.setArray(3, inTypes);

            cstmt.setArray(4, inRates);

            cstmt.setArray(5, inInclusion);

            cstmt.setArray(6, inNotes);

            cstmt.setArray(7, inApplyTypes);

            cstmt.setArray(8, inCategory);

            cstmt.setArray(9, inTMID);

            cstmt.setArray(10, inCDM);

            cstmt.setInt(11, creator);

            cstmt.setString(12, ipAdd);

            cstmt.registerOutParameter(13, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(13);
            Rlog.debug("Lineitem", "LineitemDao.updatePersonnelCost after ret"
                    + ret);

            return ret;

        }

        catch (SQLException ex) {
            Rlog.fatal("lineitem",
                    "In updatePersonnelCost in lineitemdao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    /*
     * Method to logically delete lineitems in personnel cost @param int
     * lineitem id @param int last modified by @param String ip address
     */
    public int deleteLineitemPerCost(int lineitemId, int applyType,
            int lastModifiedBy, String ipAdd)

    {

        int ret = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_BGT.SP_DELETE_LINEITEM_PER_COST(?,?,?,?,?)}");

            cstmt.setInt(1, lineitemId);

            cstmt.setInt(2, applyType);

            cstmt.setInt(3, lastModifiedBy);

            cstmt.setString(4, ipAdd);

            cstmt.registerOutParameter(5, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(5);

            return ret;

        }

        catch (SQLException ex) {
        	ret = -2;
            Rlog
                    .fatal(
                            "lineitem",
                            "In deleteLineitemPerCost in lineitemdao line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    /**
     * Inserts lineitems to a section depending on its repeat line type
     * 
     * @param String
     *            containing budget cal id
     * @param String
     *            Array containing lineitem ids of repeat default section
     * @param String
     *            Array containing lineitems names
     * @param String
     *            Array containing lineitems desciptions
     * @param String
     *            Array containing lineitems cptCodes
     * @param String
     *            Array containing lineitems repeat options
     * @param String
     *            containing creator
     * @param String
     *            containing ip address
     * @return returns pk_lineitem of the last record inserted if successful
     *         else returns -1;
     */

    public int updateRepeatLineitems(int budgetCalId, String[] parentIds,
            String[] lineitemNames, String[] lineitemDescs,
            String[] lineitemCpts, String[] lineitemRepeat,
            String[] applyTypes, String[] categories, String[] tmids,
            String[] cdms, int creator, String ipAdd) {

        CallableStatement cstmt = null;
        Connection conn = null;
        int ret = -1;
        for (int j = 0; j < lineitemNames.length; j++) {
            Rlog.debug("Lineitem", " lineitemNames:" + lineitemNames[j]);
            Rlog.debug("Lineitem", " lineitemCpts:" + lineitemCpts[j]);
        }
        try {

            conn = getConnection();

            ArrayDescriptor dIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIds = new ARRAY(dIds, conn, parentIds);

            ArrayDescriptor dNames = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inNames = new ARRAY(dNames, conn, lineitemNames);

            ArrayDescriptor dDescs = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inDescs = new ARRAY(dDescs, conn, lineitemDescs);

            ArrayDescriptor dCpt = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCpts = new ARRAY(dCpt, conn, lineitemCpts);

            ArrayDescriptor dRepeat = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inRepeat = new ARRAY(dRepeat, conn, lineitemRepeat);

            ArrayDescriptor dApply = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inApply = new ARRAY(dApply, conn, applyTypes);

            ArrayDescriptor dCategory = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCategory = new ARRAY(dCategory, conn, categories);

            ArrayDescriptor dTMID = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inTMID = new ARRAY(dTMID, conn, tmids);

            ArrayDescriptor dCDM = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inCDM = new ARRAY(dCDM, conn, cdms);

            cstmt = conn
                    .prepareCall("{call pkg_bgt.sp_update_repeat_line_items(?,?,?,?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, budgetCalId);

            cstmt.setArray(2, inIds);

            cstmt.setArray(3, inNames);

            cstmt.setArray(4, inDescs);

            cstmt.setArray(5, inCpts);

            cstmt.setArray(6, inRepeat);

            cstmt.setArray(7, inApply);

            cstmt.setArray(8, inCategory);

            cstmt.setArray(9, inTMID);

            cstmt.setArray(10, inCDM);

            cstmt.setInt(11, creator);

            cstmt.setString(12, ipAdd);

            cstmt.registerOutParameter(13, java.sql.Types.INTEGER);

            cstmt.execute();

            ret = cstmt.getInt(13);
            Rlog.debug("Lineitem", "in SP updaterep ret" + ret);
            return ret;

        }

        catch (SQLException ex) {
            Rlog
                    .fatal(
                            "lineitem",
                            "In updateRepeatLineItems in lineitemdao line EXCEPTION IN calling the procedure"
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;

    }

    /*
     * Get all lineitems of Repeat Default Section @param int budget cal id
     */

    public void getLineitemsOfRepeatDefaultSec(int budgetCalId) {

        Rlog.debug("Lineitem",
                "LineitemDao.inside getLineitemsOfRepeatDefaultSec budget id "
                        + budgetCalId);
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select pk_lineitem, " + " lineitem_name, "
                    + " lineitem_desc, " + " lineitem_cptcode, "
                    + " lineitem_repeat, " + " lineitem_applyinfuture, "
                    + " fk_codelst_category, " + " lineitem_tmid, "
                    + " lineitem_cdm "
                    + " from sch_bgtsection s, sch_bgtcal c, sch_lineitem l "
                    + " where " + " c.pk_bgtcal = s.fk_bgtcal "
                    + " and c.pk_bgtcal = ? "
                    + " and l.fk_bgtsection = s.pk_budgetsec "
                    + " and nvl(bgtsection_delflag,'Z') ='R' "
                    + " and nvl(lineitem_delflag,'Z') <> 'Y' order by lineitem_seq, lower(LINEITEM_NAME)  ";

            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, budgetCalId);

            Rlog.debug("Lineitem",
                    "LineitemDao.getLineitemsOfRepeatDefaultSec SQL**** "
                            + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLineitemIds(new Integer(rs.getInt("PK_LINEITEM")));

                setLineitemNames(rs.getString("LINEITEM_NAME"));

                setLineitemDescs(rs.getString("LINEITEM_DESC"));

                setLineitemCptCodes(rs.getString("LINEITEM_CPTCODE"));

                setLineitemRepeats(rs.getString("LINEITEM_REPEAT"));

                setApplyInFuture(rs.getString("LINEITEM_APPLYINFUTURE"));

                setLineitemCategories(rs.getString("FK_CODELST_CATEGORY"));

                setLineitemTMIDs(rs.getString("LINEITEM_TMID"));

                setLineitemCDMs(rs.getString("LINEITEM_CDM"));

                rows++;

            }
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Lineitem",
                    "LineitemDao.getLineitemsOfRepeatDefaultSec EXCEPTION IN FETCHING FROM "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // ///////////////////////
    /*
     * Delete lineitems @param String array of lineitemIds @param int last
     * modified by @param String ip address @return returns 1 if successful else
     * returns -1;
     */

    public int deleteSelectedLineitems(ArrayList lineitemIds,
            int lastModifiedBy, String ipAdd) {

        Rlog.debug("Lineitem", "LineitemDao.inside  deleteSelectedLineitems");

        int count = 0;
        int rows = 0;
        String Id = "", IdList = "";

        if (lineitemIds != null) {
            count = lineitemIds.size();
        }

        for (int i = 0; i < count; i++) {
            Id = (String) lineitemIds.get(i);
            Id = (Id == null) ? "" : Id;
            if (IdList.length() == 0)
                IdList = IdList + Id;
            else
                IdList = IdList + "," + Id;
        }

        IdList = "(" + IdList + ")";

        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = " Update sch_lineitem " + " set lineitem_delflag = 'Y', "
                    + " last_modified_by = " + lastModifiedBy + ","
                    + " ip_add = '" + ipAdd + "' where pk_lineitem in "
                    + IdList;

            Rlog.debug("Lineitem", "sql in deleteSelectedLineitems" + sqlStr);
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.executeUpdate();
            conn.commit();

        } catch (Exception e) {
            Rlog.fatal("Lineitem",
                    "EXCEPTION IN LineitemDao:deleteSelectedLineitems()" + e);
            try {
                conn.rollback();
            } catch (Exception ex) {

            }
            return -1;

        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }

            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return 1;
    }
    
    public void clearSubTotals(String[] lineItemIds) {
    	
        Rlog.debug("Lineitem", "LineitemDao.inside  clearSubTotals");
        
        int ret = 0;
    
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();

            ArrayDescriptor inIds = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY inIdArray = new ARRAY(inIds, conn, lineItemIds);

     
            cstmt = conn.prepareCall("{call PKG_BGT.sp_clear_subTotals(?)}");

            cstmt.setArray(1, inIdArray);
            
            cstmt.execute();
        
        
        }
        catch (SQLException ex) {
            Rlog.fatal("lineitem","In clearSubTotals in lineitemdao line EXCEPTION IN calling the procedure"+ ex);
        } 
        finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        }

	public ArrayList getArFkEvents() {
		return arFkEvents;
	}

	public void setArFkEvents(ArrayList arFkEvents) {
		this.arFkEvents = arFkEvents;
	}

	public void setArFkEvents(String arFkEvent) {
		this.arFkEvents.add(arFkEvent);
	}

	public ArrayList getArFKVisits() {
		return arFKVisits;
	}

	public void setArFKVisits(ArrayList arFKVisits) {
		this.arFKVisits = arFKVisits;
	}
	

	public void setArFKVisits(String arFKVisit) {
		this.arFKVisits.add(arFKVisit);
	}

	
    public void getSectionLineitems(int sectionPK) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select li.PK_LINEITEM, "
                    + "li.LINEITEM_NAME,  "
                    + "li.LINEITEM_DESC,  "
                    + "li.LINEITEM_NOTES,  "
                    + "li.LINEITEM_SPONSORUNIT,  "
                    + "li.LINEITEM_CLINICNOFUNIT,  "
                    + "li.LINEITEM_OTHERCOST,  "
                    + "li.LINEITEM_DELFLAG,  "
                    + "li.LINEITEM_STDCARECOST,  "
                    + "li.LINEITEM_RESCOST,  "
                    + "li.LINEITEM_INVCOST,  "
                    + "li.LINEITEM_INCOSTDISC,  "
                    + "li.LINEITEM_CPTCODE,  "
                    + "li.LINEITEM_REPEAT,  "
                    + "li.LINEITEM_INPERSEC,  "
                    + "li.FK_CODELST_CATEGORY,  "
                    + "li.LINEITEM_APPLYINDIRECTS,  "
                    + "li.LINEITEM_TOTALCOST,  "
                    + "li.LINEITEM_SPONSORAMOUNT,  " //Added By IA 11.03.2006
                    + "li.LINEITEM_VARIANCE,  " //Added by IA 11.03.2006
                    + "bs.PK_BUDGETSEC,  "
                    + "bs.BGTSECTION_NAME,  "
                    + "bs.BGTSECTION_VISIT,  "
                    + "bs.BGTSECTION_DELFLAG,  "
                    + "bs.BGTSECTION_SEQUENCE,  "
                    + "bs.BGTSECTION_PATNO,  "
                    + "bs.BGTSECTION_PERSONLFLAG,  "
                    + "bs.BGTSECTION_TYPE,  "
                     + "bs.SOC_COST_SPONSOR,  "//Added by IA 11.03.2006 Comparative Budget
                    + "bs.SOC_COST_VARIANCE,  "
                    + "bs.SRT_COST_VARIANCE,  "
                    + "bs.SRT_COST_SPONSOR , bs.fk_visit,li.fk_event,li.lineitem_seq,li.fk_codelst_cost_type,li.lineitem_cdm, li.lineitem_tmid  "// End Added by IA 11.03.2006 Comparative Budget
                    + "from  sch_lineitem  li, sch_bgtsection bs  "
                    + "where li.FK_BGTSECTION = ? and (LINEITEM_DELFLAG <> 'Y'  "
                    + "or LINEITEM_DELFLAG is null) and FK_BGTSECTION = pk_budgetsec "
                    + " And (nvl(bs.BGTSECTION_DELFLAG,'Z') <> 'Y' and nvl(bs.BGTSECTION_DELFLAG,'Z') <> 'P' and nvl(bs.BGTSECTION_DELFLAG,'Z') <> 'R'"
                    + "or bs.BGTSECTION_DELFLAG is null) "
                    + "AND NVL(SUBCOST_ITEM_FLAG,0) != 1 "
                    + "order by BGTSECTION_SEQUENCE,lineitem_seq, lower(LINEITEM_NAME) ";
            
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, sectionPK);

            Rlog.debug("Lineitem", "LineitemDao.getSectionLineitems SQL**** "
                    + sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLineitemIds(new Integer(rs.getInt("PK_LINEITEM")));

                setLineitemNames(rs.getString("LINEITEM_NAME"));
                setLineitemDescs(rs.getString("LINEITEM_DESC"));
                setLineitemNotes(rs.getString("LINEITEM_Notes"));

                setLineitemSponsorUnits(rs.getString("LINEITEM_SPONSORUNIT"));

                setLineitemClinicNOfUnits(rs
                        .getString("LINEITEM_CLINICNOFUNIT"));

                setLineitemOtherCosts(rs.getString("LINEITEM_OTHERCOST"));

                setLineitemDelFlags(rs.getString("LINEITEM_DELFLAG"));
                setLineitemInPerSecs(rs.getString("LINEITEM_INPERSEC"));

                setLineitemStdCareCosts(rs.getString("LINEITEM_STDCARECOST"));

                setLineitemResCosts(rs.getString("LINEITEM_RESCOST"));

                setLineitemInvCosts(rs.getString("LINEITEM_INVCOST"));

                setLineitemInCostDiscs(rs.getString("LINEITEM_INCOSTDISC"));
                setLineitemCptCodes(rs.getString("LINEITEM_CPTCODE"));
                setLineitemRepeats(rs.getString("LINEITEM_REPEAT"));

                setLineitemCategories(rs.getString("FK_CODELST_CATEGORY"));
                setLineitemAppIndirects(rs.getString("LINEITEM_APPLYINDIRECTS"));
                
                setLineitemTotalCost(rs.getString("LINEITEM_TOTALCOST"));
                
                //Added by IA 11.03.2006 Comparative Budget
                
                
                setLineItemSponsorAmount(rs.getString("LINEITEM_SPONSORAMOUNT"));
                
                setLineItemVariance(rs.getString("LINEITEM_VARIANCE"));
 
                setSrtCostSponsor(rs.getString("SRT_COST_SPONSOR"));
                
                setSrtCostVariance(rs.getString("SRT_COST_VARIANCE"));
                
                setSocCostSponsor(rs.getString("SOC_COST_SPONSOR"));
                
                setSocCostVariance(rs.getString("SOC_COST_VARIANCE"));

                
                //End added
                
                setBgtSectionIds(new Integer(rs.getInt("PK_BUDGETSEC")));

                setBgtSectionNames(rs.getString("BGTSECTION_NAME"));

                setBgtSectionVisits(rs.getString("BGTSECTION_VISIT"));

                setBgtSectionDelFlags(rs.getString("BGTSECTION_DELFLAG"));

                setBgtSectionSequences(rs.getString("BGTSECTION_SEQUENCE"));

                setBgtSectionPatNos(rs.getString("BGTSECTION_PATNO"));
                setBgtSectionPersonlFlags(rs.getString("BGTSECTION_PERSONLFLAG"));
                setBgtSectionTypes(rs.getString("BGTSECTION_TYPE"));


                setArFKVisits(rs.getString("FK_VISIT"));
                setArFkEvents(rs.getString("FK_EVENT"));
                
                setArLineItemSequence( rs.getString("lineitem_seq"));
                
                setArLineItemCostType( rs.getString("fk_codelst_cost_type") );
                
                setLineitemCDMs(rs.getString("lineitem_cdm") );
                setLineitemTMIDs(rs.getString("lineitem_tmid") );

                
                rows++;

            }
            setBRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("Lineitem",
                    "LineitemDao.getSectionLineitems EXCEPTION IN FETCHING FROM "
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }


	public ArrayList getArLineItemSequence() {
		return arLineItemSequence;
	}


	public void setArLineItemSequence(ArrayList arLineItemSequence) {
		this.arLineItemSequence = arLineItemSequence;
	}

	public void setArLineItemSequence(String lineItemSequence) {
		this.arLineItemSequence.add(lineItemSequence);
	}

	public ArrayList getArLineItemCostType() {
		return arLineItemCostType;
	}


	public void setArLineItemCostType(ArrayList arLineItemCostType) {
		this.arLineItemCostType = arLineItemCostType;
	}

	
	public void setArLineItemCostType(String lineItemCostType) {
		this.arLineItemCostType.add(lineItemCostType);
	}

	
}
 
