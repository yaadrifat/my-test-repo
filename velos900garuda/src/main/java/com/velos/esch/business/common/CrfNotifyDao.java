/*
 * Classname	CrfNotifyDao.class
 * 
 * Version information 	1.0
 *
 * Date	12/12/2001
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.esch.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//import com.velos.esch.business.crfNotify.impl.CrfNotifyBean;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * CrfNotifyDao for getting CrfNotify records
 * 
 * @author Sajal
 * @version : 1.0 12/12/2001
 */

public class CrfNotifyDao extends CommonDAO implements java.io.Serializable {
    private ArrayList crfNotIds;

    private ArrayList crfNotCrfIds;

    private ArrayList crfNotStats;

    private ArrayList crfNotUsers;

    private ArrayList crfNotNotes;

    private ArrayList crfNotStudys;

    private ArrayList crfNotProtocols;

    private ArrayList crfNotPatProts;

    private ArrayList crfNotGlobalFlags;

    private ArrayList crfNumbers;

    private int cRows;

    public CrfNotifyDao() {
        crfNotIds = new ArrayList();
        crfNotCrfIds = new ArrayList();
        crfNotStats = new ArrayList();
        crfNotUsers = new ArrayList();
        crfNotNotes = new ArrayList();
        crfNotStudys = new ArrayList();
        crfNotProtocols = new ArrayList();
        crfNotPatProts = new ArrayList();
        crfNotGlobalFlags = new ArrayList();
        crfNumbers = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getCrfNotIds() {
        return this.crfNotIds;
    }

    public void setCrfNotIds(ArrayList crfNotIds) {
        this.crfNotIds = crfNotIds;
    }

    public ArrayList getCrfNotCrfIds() {
        return this.crfNotCrfIds;
    }

    public void setCrfNotCrfIds(ArrayList crfNotCrfIds) {
        this.crfNotCrfIds = crfNotCrfIds;
    }

    public ArrayList getCrfNotStats() {
        return this.crfNotStats;
    }

    public void setCrfNotStats(ArrayList crfNotStats) {
        this.crfNotStats = crfNotStats;
    }

    public ArrayList getCrfNotUsers() {
        return this.crfNotUsers;
    }

    public void setCrfNotUsers(ArrayList crfNotUsers) {
        this.crfNotUsers = crfNotUsers;
    }

    public ArrayList getCrfNotNotes() {
        return this.crfNotNotes;
    }

    public void setCrfNotNotes(ArrayList crfNotNotes) {
        this.crfNotNotes = crfNotNotes;
    }

    public ArrayList getCrfNotStudys() {
        return this.crfNotStudys;
    }

    public void setCrfNotStudys(ArrayList crfNotStudys) {
        this.crfNotStudys = crfNotStudys;
    }

    public ArrayList getCrfNotProtocols() {
        return this.crfNotProtocols;
    }

    public void setCrfNotProtocols(ArrayList crfNotProtocols) {
        this.crfNotProtocols = crfNotProtocols;
    }

    public ArrayList getCrfNotPatProts() {
        return this.crfNotPatProts;
    }

    public void setCrfNotPatProts(ArrayList crfNotPatProts) {
        this.crfNotPatProts = crfNotPatProts;
    }

    public ArrayList getCrfNotGlobalFlags() {
        return this.crfNotGlobalFlags;
    }

    public void setCrfNotGlobalFlags(ArrayList crfNotGlobalFlags) {
        this.crfNotGlobalFlags = crfNotGlobalFlags;
    }

    public ArrayList getCrfNumbers() {
        return this.crfNumbers;
    }

    public void setCrfNumbers(ArrayList crfNumbers) {
        this.crfNumbers = crfNumbers;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setCrfNotIds(Integer crfNotId) {
        crfNotIds.add(crfNotId);
    }

    public void setCrfNotCrfIds(String crfNotCrfId) {
        crfNotCrfIds.add(crfNotCrfId);
    }

    public void setCrfNotStats(String crfNotStat) {
        crfNotStats.add(crfNotStat);
    }

    public void setCrfNotUsers(String crfNotUser) {
        crfNotUsers.add(crfNotUser);
    }

    public void setCrfNotNotes(String crfNotNote) {
        crfNotNotes.add(crfNotNote);
    }

    public void setCrfNotStudys(String crfNotStudy) {
        crfNotStudys.add(crfNotStudy);
    }

    public void setCrfNotProtocols(String crfNotProtocol) {
        crfNotProtocols.add(crfNotProtocol);
    }

    public void setCrfNotPatProts(String crfNotPatProt) {
        crfNotPatProts.add(crfNotPatProt);
    }

    public void setCrfNotGlobalFlags(String crfNotGlobalFlag) {
        crfNotGlobalFlags.add(crfNotGlobalFlag);
    }

    public void setCrfNumbers(String crfNumber) {
        crfNumbers.add(crfNumber);
    }

    // end of getter and setter methods

    /**
     * Gets the notification details of all the notifications associated to
     * crfId and sets the values in the class attributes
     * 
     * @param crfId
     *            Id of the CRF
     */

    public void getCrfNotifyValues(int patProtId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn
                    .prepareStatement("select PK_CRFNOT, "
                            + "FK_CRF, "
                            + "(select crf_number from sch_crf where pk_crf = fk_crf) crf_number, "
                            + "(select codelst_desc from sch_codelst where pk_codelst = FK_CODELST_NOTSTAT) as status, "
                            + "(select usr_lst(CRFNOT_USERSTO) from dual) users, "
                            + "CRFNOT_NOTES, " + "FK_STUDY, " + "FK_PROTOCOL, "
                            + "FK_PATPROT, " + "CRFNOT_GLOBALFLAG "
                            + "from sch_crfnotify " + "where FK_PATPROT = ? "
                            + "order by status");
            pstmt.setInt(1, patProtId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setCrfNotIds(new Integer(rs.getInt("PK_CRFNOT")));
                setCrfNotCrfIds((new Integer(rs.getInt("FK_CRF"))).toString());
                setCrfNumbers(rs.getString("crf_number"));
                setCrfNotStats(rs.getString("status"));
                setCrfNotUsers(rs.getString("users"));
                setCrfNotNotes(rs.getString("CRFNOT_NOTES"));
                setCrfNotStudys((new Integer(rs.getInt("FK_STUDY"))).toString());
                setCrfNotProtocols((new Integer(rs.getInt("FK_PROTOCOL")))
                        .toString());
                setCrfNotPatProts((new Integer(rs.getInt("FK_PATPROT")))
                        .toString());
                setCrfNotGlobalFlags(rs.getString("CRFNOT_GLOBALFLAG"));
                rows++;
                Rlog.debug("crfNotify", "CrfNotifyDao.getCrfNotifyValues rows "
                        + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("crfNotify",
                    "CrfNotifyDao.getCrfNotifyValues EXCEPTION IN FETCHING FROM CrfNotify table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    public void getCrfNotifyValues(int FK_STUDY, int FK_PROTOCOL) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn
                    .prepareStatement("select PK_CRFNOT, "
                            + "FK_CRF, "
                            + "(select crf_number from sch_crf where pk_crf = fk_crf) crf_number, "
                            + "(select codelst_desc from sch_codelst where pk_codelst = FK_CODELST_NOTSTAT) as status, "
                            + "(select usr_lst(CRFNOT_USERSTO) from dual) users, "
                            + "CRFNOT_NOTES, "
                            + "FK_STUDY, "
                            + "FK_PROTOCOL, "
                            + "FK_PATPROT, "
                            + "CRFNOT_GLOBALFLAG "
                            + "from sch_crfnotify "
                            + "where FK_STUDY = ? and FK_PROTOCOL = ? and FK_PATPROT is null "
                            + "order by status");

            pstmt.setInt(1, FK_STUDY);
            pstmt.setInt(2, FK_PROTOCOL);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setCrfNotIds(new Integer(rs.getInt("PK_CRFNOT")));
                setCrfNotCrfIds((new Integer(rs.getInt("FK_CRF"))).toString());
                setCrfNumbers(rs.getString("crf_number"));
                setCrfNotStats(rs.getString("status"));
                setCrfNotUsers(rs.getString("users"));
                setCrfNotNotes(rs.getString("CRFNOT_NOTES"));
                setCrfNotStudys((new Integer(rs.getInt("FK_STUDY"))).toString());
                setCrfNotProtocols((new Integer(rs.getInt("FK_PROTOCOL")))
                        .toString());
                setCrfNotPatProts((new Integer(rs.getInt("FK_PATPROT")))
                        .toString());
                setCrfNotGlobalFlags(rs.getString("CRFNOT_GLOBALFLAG"));
                rows++;
                Rlog.debug("crfNotify", "CrfNotifyDao.getCrfNotifyValues rows "
                        + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("crfNotify",
                    "CrfNotifyDao.getCrfNotifyValues EXCEPTION IN FETCHING FROM CrfNotify table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

   /* public void copyCrfNotifyDetails(CrfNotifyBean cnsk) {
        CallableStatement cstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            cstmt = conn
                    .prepareCall("{call pkg_alnot.copy_crfnotify (?,?,?,?,?,?,?,?,?)}");
            cstmt.setInt(1, StringUtil.stringToNum(cnsk.getCrfNotifyCrfId()));
            cstmt.setInt(2, StringUtil.stringToNum(cnsk
                    .getCrfNotifyCodelstNotStatId()));
            cstmt.setString(3, cnsk.getCrfNotifyUsersTo());
            cstmt.setInt(4, StringUtil.stringToNum(cnsk.getCreator()));
            cstmt.setString(5, cnsk.getIpAdd());
            cstmt.setInt(6, StringUtil.stringToNum(cnsk.getCrfNotStudyId()));
            cstmt.setInt(7, StringUtil.stringToNum(cnsk.getCrfNotProtocolId()));
            cstmt.setInt(8, StringUtil.stringToNum(cnsk.getCrfNotPatProtId()));
            cstmt.setString(9, cnsk.getCrfNotGlobalFlag());

            cstmt.execute();
        } catch (SQLException ex) {
            Rlog
                    .fatal(
                            "crfNotify",
                            "CrfNotifyDao.copyCrfNotifyDetails EXCEPTION IN call the stored procedure package "
                                    + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }*/
}
