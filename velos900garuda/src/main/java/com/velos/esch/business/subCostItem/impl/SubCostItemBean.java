 /*
 * Classname			SubCostItemBean
 * 
 * Version information  1.0
 *
 * Date					01/19/2011 
 * 
 * Copyright notice		Velos Inc
 */
package com.velos.esch.business.subCostItem.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * The Subject Cost Item BMP entity bean.<br>
 * <br>
 * 
 * @author Manimaran
 * @version 1.0 01/19/2011
 */
@Entity
@Table(name = "sch_subcost_item")
public class SubCostItemBean implements Serializable {
	
	private static final long serialVersionUID = 3977581398557275957L;

	/**
	 * subCostItemId
	 */
	public Integer subCostItemId;
	
	/**
	 * fkCalendar
	 */
	public Integer fkCalendar;
	
	/**
	 * subCostItemName
	 */
	public String subCostItemName;
	
	/**
	 * subCostItemCost
	 */
	public float subCostItemCost;
	
	/**
	 * subCostItemUnit
	 */
	public int subCostItemUnit;
	
	/**
	 * subCostItemSeq
	 */
	public Integer subCostItemSeq;
	
	/**
	 * fkCodelstCategory
	 */
	public Integer fkCodelstCategory;
	
	/**
	 * fkCodelstCostType 
	 */
	public String fkCodelstCostType;
	
	/*YK Bug 5821 & # 5818
	 * SUBCOST_ITEM_DELFLAG Removed from Table Not required.
	 * */
	
	/**
     * creator
     */
    public String creator;
    
    /**
     * last modifiedBy
     */
    public String modifiedBy;
    
    /**
     * IP Address
     */
    public String ipAdd;
    
    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_SUBCOST_ITEM", allocationSize=1)
    @Column(name = "PK_SUBCOST_ITEM")
	public int getSubCostItemId() {
		return (this.subCostItemId==null)? 0: this.subCostItemId;
	}

	public void setSubCostItemId(int subCostItemId) {
		this.subCostItemId = subCostItemId;
	}

	
	@Column(name = "FK_CALENDAR")
	public Integer getFkCalendar() {
		return this.fkCalendar;
	}

	public void setFkCalendar(int fkCalendar) {
		this.fkCalendar = fkCalendar;
	}

	@Column(name = "SUBCOST_ITEM_NAME")
	public String getSubCostItemName() {
		return this.subCostItemName;
	}

	public void setSubCostItemName(String subCostItemName) {
		this.subCostItemName = subCostItemName;
	}

	@Column(name = "SUBCOST_ITEM_COST")
	public String getSubCostItemCost() {
		return StringUtil.floatToString(this.subCostItemCost);
	}

	public void setSubCostItemCost(String subCostItemCost) {
		if(subCostItemCost != null) {
			this.subCostItemCost = Float.valueOf(subCostItemCost);
		}
	}

	@Column(name = "SUBCOST_ITEM_UNIT")
	public String getSubCostItemUnit() {
		return StringUtil.integerToString(this.subCostItemUnit);
	}

	public void setSubCostItemUnit(String subCostItemUnit) {
		if(subCostItemUnit != null) {
			this.subCostItemUnit = Integer.valueOf(subCostItemUnit);
		}
	}

	@Column(name = "SUBCOST_ITEM_SEQ")
	public String getSubCostItemSeq() {
		return StringUtil.integerToString(this.subCostItemSeq);
	}

	public void setSubCostItemSeq(String subCostItemSeq) {
		if(subCostItemSeq != null) {
			this.subCostItemSeq = Integer.valueOf(subCostItemSeq);
		}
	}

	@Column(name = "FK_CODELST_CATEGORY")
	public Integer getFkCodelstCategory() {
		return this.fkCodelstCategory;
	}

	public void setFkCodelstCategory(int fkCodelstCategory) {
		this.fkCodelstCategory = Integer.valueOf(fkCodelstCategory);
	}

	@Column(name = "FK_CODELST_COST_TYPE")
	public String getFkCodelstCostType() {
		return this.fkCodelstCostType;
	}

	public void setFkCodelstCostType(String fkCodelstCostType) {
		if (fkCodelstCostType != null){
			this.fkCodelstCostType = fkCodelstCostType;
		}
	}

	
	@Column(name = "CREATOR")
    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = creator;
        }
    }

    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = modifiedBy;
        }
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

  
    public int updateSubCostItem(SubCostItemBean scsk) {
        try {
        	setSubCostItemId(scsk.getSubCostItemId());
        	setFkCalendar(scsk.getFkCalendar());
        	setSubCostItemName(scsk.getSubCostItemName());
        	setSubCostItemCost( scsk.getSubCostItemCost());
        	setSubCostItemUnit( scsk.getSubCostItemUnit());
        	setSubCostItemSeq(scsk.getSubCostItemSeq());
        	setFkCodelstCategory(scsk.getFkCodelstCategory());
        	setFkCodelstCostType(scsk.getFkCodelstCostType());
        	setCreator(scsk.getCreator());
        	setModifiedBy(scsk.getModifiedBy());
        	setIpAdd(scsk.getIpAdd());
            return getSubCostItemId();
        } catch (Exception e) {
        	
            Rlog.fatal("sucost", " error in SubCostItemBean.updateSubCostItem");
            return -2;
        }
    }
      
    public SubCostItemBean(){
    }
    
    public SubCostItemBean(int subCostItemId, int fkCalendar, String subCostItemName,
            String subCostItemCost, String subCostItemUnit, String subCostItemSeq, Integer fkCodelstCategory,
            String fkCodelstCostType, String creator,
            String modifiedBy, String ipAdd) {
        super();
        
        setSubCostItemId(subCostItemId);
    	setFkCalendar(fkCalendar);
    	setSubCostItemName(subCostItemName);
    	setSubCostItemCost(subCostItemCost);
    	setSubCostItemUnit(subCostItemUnit);
    	setSubCostItemSeq(subCostItemSeq);
    	setFkCodelstCategory(fkCodelstCategory);
    	setFkCodelstCostType(fkCodelstCostType);
    	setCreator(creator);
    	setModifiedBy(modifiedBy);
    	setIpAdd(ipAdd);
    
    }
}