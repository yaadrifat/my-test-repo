/**
 * Extends HttpServlet class, used to upload a file to the database. The file can be uploaded only to a Blob type column.
 *
 * @author Sajal
 *
 * @version 1.0
 *
 */

package com.aithent.file.uploadDownload;

// import statements
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.velos.eres.business.common.StudyVerDao;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.web.appendix.AppendixJB;
import com.velos.eres.web.studyVer.StudyVerJB;
import com.velos.eres.web.user.UserJB;

/**
 * Extends HttpServlet class, used to upload a file to the database. The file
 * can be uploaded only to a Blob type column.
 *
 */
public class StudyApndx extends FileUploadServlet {

    public void init(ServletConfig sc) throws ServletException {

    }

    /**
     * Overrides the doPost method of HttpServlet. Extracts the parameters sent
     * by the servlet and uploads the specified file with the specified name
     * from the temporary path specified in the XML file to the table specified.
     *
     * @param httpservletrequest
     *            an HttpServletRequest object that contains the request the
     *            client has made of the servlet
     *
     * @param httpservletresponse
     *            an HttpServletResponse object that contains the response the
     *            servlet sends to the client
     *
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        String ipAdd = req.getRemoteAddr();
        if (!req.getContentType().toLowerCase().startsWith(
                "multipart/form-data")) {
            // Since this servlet only handles File Upload, bail out
            // Note: this isn't strictly legal - I should send
            // a custom error message
            return;
        }
        int ind = req.getContentType().indexOf("boundary=");
        if (ind == -1) {
            return;
        }
        String boundary = req.getContentType().substring(ind + 9);
        if (boundary == null) {
            return;
        }
        Hashtable _table = null;
        try {
            _table = parseMulti(boundary, req.getInputStream(), out);
        } catch (Throwable t) {
            t.printStackTrace(new PrintStream(res.getOutputStream()));
            return;
        }

        String[] eSign = new String[1];
        String[] userId = new String[1];
        String[] nextPage = new String[1];
        String[] successPage = new String[1];

        eSign = (String[]) _table.get("eSign");
        userId = (String[]) _table.get("userId");
        nextPage = (String[]) _table.get("nextPage");
        successPage = (String[]) _table.get("successPage");
        String[] studyId = (String[]) _table.get("studyId");

        UserJB userB = new UserJB();
        userB.setUserId(EJBUtil.stringToNum(userId[0]));
        userB.getUserDetails();
        String actualESign = userB.getUserESign();
        out = res.getOutputStream();

        if (!eSign[0].equals(actualESign)) {
            res.sendRedirect("../../velos/jsp/incorrectesign.jsp");
            return;
        }

        // First, store all upload files in a temp directory
        Hashtable fileHash = saveFilesInTemp(_table, userId[0]);

        // Found out what version-category combinations already exist for this study
        StudyVerDao studyVerDao = new StudyVerDao();
        studyVerDao.getAllVers(EJBUtil.stringToNum(studyId[0]));
        ArrayList verNumberList = studyVerDao.getStudyVerNumbers();
        ArrayList verCategoryList = studyVerDao.getStudyVerCategoryIds();
        ArrayList verIdList = studyVerDao.getStudyVerIds();
        Hashtable<String, String> verNumCategoryHash = new Hashtable<String, String>();
        for (int iX=0; iX<verNumberList.size(); iX++) {
            String key = ((String)verNumberList.get(iX))+"-"+((String)verCategoryList.get(iX));
            verNumCategoryHash.put(key, String.valueOf((Integer)verIdList.get(iX)));
        }

        // Loop thru all temp files and save in DB one by one
        int incorrectFile = 0;
        int outOfSpace = 0;
        int studyVerError = 0;
        int fileHashIndex = 0;
        Hashtable<String, String> newlyCreatedVer = null; // key=verNumber-category; value=verId
        while (true) {
            String _ser_file_name = (String)fileHash.get("ser_file_name"+(++fileHashIndex));
            if (_ser_file_name == null || _ser_file_name.length() == 0) { break; }
            String _file_name = (String)fileHash.get("file_name"+fileHashIndex);

            AppendixJB studyApndxJB = new AppendixJB();
            //KM-#3634
            studyApndxJB.setCreator(userId[0]);
            int studyApndxId = studyApndxJB.setAppendixDetails();
            pkBase[0] = String.valueOf(studyApndxId);

            // super.doPost(req, res);
            // -- save in DB here
            Hashtable dbHash = saveFileInDB(_table, fileHash, studyApndxId, fileHashIndex, incorrectFile, outOfSpace);
            boolean _deleteFlag = ((Boolean)dbHash.get("deleteFlag")).booleanValue();
            incorrectFile += ((Integer)dbHash.get("incorrectFile")).intValue();
            outOfSpace += ((Integer)dbHash.get("outOfSpace")).intValue();

            if (_deleteFlag == false) {

                String[] desc = (String[]) fileHash.get("desc"+fileHashIndex);
                String[] type = (String[]) _table.get("type");
                String[] pubflag = (String[]) _table.get("pubflag");
                String[] studyVer = (String[]) _table.get("studyVer"); // this gets stored in pk_studyver

                if (desc == null) { desc = new String[1]; }
                if (type == null) { type = new String[1]; }
                if (pubflag == null) {
                    pubflag = new String[1];
                    pubflag[0] = "N";
                }

                // if studyVer is "null", it came from the old way of uploading one file at a time
                if (studyVer == null || (studyVer.length>0 && studyVer[0].toLowerCase().equals("null"))) {
                    studyVer = new String[1];

                    // decide whether to create a new study version
                    boolean proceedToCreate = false;
                    String[] verNumber = (String[]) fileHash.get("verNumber"+fileHashIndex);
                    String[] dStudyvercat = (String[]) fileHash.get("dStudyvercat"+fileHashIndex);
                    String thisKey = verNumber[0]+"-"+dStudyvercat[0];
                    if (newlyCreatedVer != null && newlyCreatedVer.containsKey(thisKey)) {
                        studyVer[0] = newlyCreatedVer.get(thisKey);
                    } else if (verNumCategoryHash.containsKey(thisKey)) {
                        studyVer[0] = verNumCategoryHash.get(thisKey);
                    } else { // Could not find version in the new hash or DB hash => create a new one
                        proceedToCreate = true;
                    }

                    if (proceedToCreate) {
                        // create a new study version
                        StudyVerJB studyVerJB = new StudyVerJB();
                        int sId = 0;
                        try { sId = Integer.parseInt(studyId[0]); } catch(Exception e) {}
                        String[] versionDate = (String[]) fileHash.get("versionDate"+fileHashIndex);
                        String[] dStudyvertype = (String[]) fileHash.get("dStudyvertype"+fileHashIndex);
                        int newStudyVerId = studyVerJB.newStudyVersion(sId, verNumber[0], "", versionDate[0],
                                dStudyvercat[0], dStudyvertype[0], EJBUtil.stringToNum(userId[0]));
                        if (newStudyVerId < 1) {
                            Rlog.fatal("FileUpload", "New study version did not get created - error code="+newStudyVerId);
                            studyVerError++;
                            continue;
                        }
                        studyVer[0] = String.valueOf(newStudyVerId);
                        newlyCreatedVer = new Hashtable<String, String>();
                        newlyCreatedVer.put(thisKey,studyVer[0]);
                    }
                } // End of if studyVer is "null"

                Long _uploadFileSize = (Long)dbHash.get("uploadFileSize");

                studyApndxJB.setId(studyApndxId);
                studyApndxJB.getAppendixDetails();
                studyApndxJB.setAppendixDescription(desc[0]);
                studyApndxJB.setAppendixType(type[0]);
                studyApndxJB.setAppendixPubFlag(pubflag[0]);
                studyApndxJB.setCreator(userId[0]);
                studyApndxJB.setAppendixStudyVer(studyVer[0]);
                studyApndxJB.setAppendixStudy(studyId[0]);
                studyApndxJB.setIpAdd(ipAdd);

                studyApndxJB.setAppendixUrl_File(_file_name);
                studyApndxJB.setAppendixSavedFileName(_ser_file_name);
                studyApndxJB.setFileSize(_uploadFileSize.toString());
                //KM-#3634
                studyApndxJB.setModifiedBy(userId[0]);
                studyApndxJB.updateAppendix();
            } else {
                studyApndxJB.setId(studyApndxId);
                studyApndxJB.removeAppendix();
            }

        }
        if (nextPage[0] != null && nextPage[0].indexOf("studyVerBrowser.jsp") != -1) {
            res.sendRedirect("../../"+successPage[0]+"?outOfSpace="+outOfSpace+"&incorrectFile="+incorrectFile+"&studyVerError="+studyVerError+"&fileHashIndex="+fileHashIndex);
            return;
        }
        res.sendRedirect("../../"+nextPage[0]+"&incorrectFile="+incorrectFile+"&outOfSpace="+outOfSpace);

    }

    /**
     *
     *
     * Obtain information on this servlet.
     *
     *
     * @return String describing this servlet.
     *
     *
     */

    public String getServletInfo() {

        return "File upload servlet -- used to receive files";

    }

}
