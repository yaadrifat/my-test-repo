<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.MC" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="sectionB" scope="session" class="com.velos.eres.web.section.SectionJB"/>
<jsp:useBean id="codelstB" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,java.util.ArrayList"%>
<%
   String msg=null;
   String sectionId=null;
   String sectionSequence=null;	
   int length = EJBUtil.stringToNum(request.getParameter("length"));
   int id;
   String tab = request.getParameter("selectedTab");	
   String src = request.getParameter("srcmenu");
   String studyVerId = request.getParameter("studyVerId");      
   HttpSession tSession = request.getSession(true); 
 String eSign = request.getParameter("eSign");
    
	if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
   
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");

   ArrayList sectionIds = new ArrayList();
   ArrayList sectionSequences = new ArrayList();

   for(int i=0; i<length;i++) {
	sectionId = request.getParameter("sectionId" + i);
	sectionSequence = request.getParameter("secSequence" + i);
	sectionIds.add(sectionId);
	sectionSequences.add(sectionSequence);
   } 

   	sectionB.setModifiedBy(usr);
	sectionB.setIpAdd(ipAdd);
	int ret = sectionB.updateSectionSequence(sectionIds, sectionSequences);

%>
  
 
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>

<META HTTP-EQUIV=Refresh CONTENT="3; URL=sectionBrowserNew.jsp?srcmenu=<%=src%>&mode=M&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>">

<%
}//end of if for eSign check
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





