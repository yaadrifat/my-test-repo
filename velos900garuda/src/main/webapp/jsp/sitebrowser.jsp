<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/><html>

<head>

<title><%=MC.M_MngAcc_Org%><%--Manage Account >> Organizations*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<% String src;

src= request.getParameter("srcmenu");


%>



<jsp:include page="panel.jsp" flush="true" > 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   
<script> 

//modified on 29th March for organization sorting, bug #2072
function setOrder(formObj,orderBy) //orderBy column number 
{

	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";

	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
	}

	orderType=formObj.orderType.value;
	formObj.orderBy.value = orderBy;
	lsrc = formObj.srcmenu.value;	
	formObj.action="sitebrowser.jsp?mode=M&srcmenu="+lsrc+"&orderBy="+orderBy+"&orderType="+orderType;		
	formObj.submit(); 	

}

</script>


<body>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@page import="com.velos.eres.service.util.*"%>
<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="1"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 

<Form name="sitebrowser" method="post" action="sitebrowser.jsp" onsubmit="" flush="true">

  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{

	String uName = (String) tSession.getValue("userName");

	 int pageRight = 0;

	   GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));

 if (pageRight > 0 )

	{
		


	   int accountId=0;   

//modified on 29th March for organization sorting, bug #2072

		String orderBy="";
		orderBy=request.getParameter("orderBy");

		if (orderBy==null) orderBy="SITE_NAME";
				String orderType = "";
				orderType = request.getParameter("orderType");
				if (orderType == null)
			{
				orderType = "asc";
			} 
		


	   String acc = (String) tSession.getValue("accountId");

	   accountId = EJBUtil.stringToNum(acc);


	  // SiteDao siteDao = siteB.getByAccountId(accountId);


	  //modified on 29th March for organization sorting, bug 2072	  
	   SiteDao siteDao = siteB.getByAccountId(accountId,orderBy,orderType);

	   ArrayList siteIds = siteDao.getSiteIds(); 

	   ArrayList siteTypes = siteDao.getSiteTypes();

	   ArrayList siteNames = siteDao.getSiteNames();

	   ArrayList siteInfos = siteDao.getSiteInfos();

	   ArrayList siteParents = siteDao.getSiteParents();

	   ArrayList siteStats = siteDao.getSiteStats();



	   String siteType = null;

	   String siteName = null;

	   String siteInfo = null;

	   String siteParent = null;

	   String siteStat = null;


	   int siteId=0;



	   int len = siteIds.size();

	   int counter = 0;

%>

  
  
<input type="hidden" name="orderType" value="<%=orderType%>"> 
<input type="hidden" name="srcmenu" value="<%=src%>">
<input type="hidden" name="orderBy" value="<%=orderBy%>">

    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr valign="center"> 
        <td width="70%"> 
          <P class="sectionheadings"><%=MC.M_Cur_AddedOrgs%><%--Currently added Organizations are*****--%>:</P>
        </td>
        <td width="30%" valign="center" align="right"> 
           <A  class="rhsFont" href="sitedetails.jsp?mode=N&srcmenu=<%=src%>"><%=MC.M_AddNew_Org_Upper%><%--ADD A NEW ORGANIZATION*****--%> </A>  
        </td>
      </tr>
    </table>
    <table width="99%" class="outline midAlign">
      <tr> 
		<!--//modified on 29th March for organization sorting, bug #2072 -->

        <th width="40%" onClick="setOrder(document.sitebrowser,'lower(SITE_NAME)')"> <%=LC.L_Organization_Name%><%--Organization Name*****--%> &loz;</th>
        <th width="20%" onClick="setOrder(document.sitebrowser,'lower(SITE_TYPE)')"> <%=LC.L_Type%><%--Type*****--%> &loz;</th>
        <th width="40%" onClick="setOrder(document.sitebrowser,'lower(PARENT_SITE)')"> <%=LC.L_Parent_Org%><%--Parent Organization*****--%> &loz;</th>
      </tr>
      <%

    for(counter = 0;counter<len;counter++)

	{	siteId=EJBUtil.stringToNum(((siteIds.get(counter)) == null)?"-":(siteIds.get(counter)).toString());



		siteType=((siteTypes.get(counter)) == null)?"-":(siteTypes.get(counter)).toString();



		siteName=((siteNames.get(counter)) == null)?"-":(siteNames.get(counter)).toString();



		siteParent=((siteParents.get(counter)) ==null)?"-":(siteParents.get(counter)).toString();



		siteStat=((siteStats.get(counter)) == null)?"-":(siteStats.get(counter)).toString();

		if (siteStat.equals("A")){

		   siteStat = "Active";

		}

		else if (siteStat.equals("I")){

		   siteStat = "InActive";

		}


		if ((counter%2)==0) {  %>
      <tr class="browserEvenRow"> 
        <%}	else{ %>

      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td> <A href="../jsp/sitedetails.jsp?siteId=<%=siteId%>&mode=M&srcmenu=<%=src%>">
          <%= siteName%> </A> </td>
        <td> <%= siteType%> </td>
        <td> <%= siteParent%> </td>
      </tr>

	   <%

		}

%>
    </table>
    <div class="tmpHeight"></div>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>

