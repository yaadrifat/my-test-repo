<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Form_Lib%><%--Form Library*****--%></title>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="formfieldB" scope="request" class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="fldLibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
	
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  



<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	

	String formFldId= "";
	String formId="";
	String secId= "";
   	String fieldId= "";
	String mode= "";
	String calledFrom = "";
	String selectedTab="";
	String modBy="";
	String codeStatus="" ; 
	String lnkFrom="";
	String studyId= "";
	int ret=0;
	int refreshSec = 1;
	boolean hasInterFieldActions = false;
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		formFldId= request.getParameter("formFldId");
		formId=request.getParameter("formId");
		mode= request.getParameter("mode");
		calledFrom = request.getParameter("calledFrom");
		selectedTab= request.getParameter("selectedTab");
		modBy=(String) tSession.getAttribute("userId");
	
		codeStatus = request.getParameter("codeStatus");
		lnkFrom = request.getParameter("lnkFrom");
		studyId = request.getParameter("studyId");
		
			%>
	<FORM name="deleteField" id="delFieldFrm" method="post" action="fielddelete.jsp" onSubmit="if (validate(document.deleteField)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	<%  if ( mode.equals("initial") )
			{      
	%>			
	<P class="defComments"><%=MC.M_EsignToProc_WithFldDel%><%--Please enter e-Signature to proceed with Field Delete.*****--%></P>
				<jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="delFieldFrm"/>
						<jsp:param name="showDiscard" value="N"/>
				</jsp:include>

 		 <input type="hidden" name="formFldId" value="<%=formFldId%>">
		 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
		  <input type="hidden" name="codeStatus" value="<%=codeStatus%>">
		   <input type="hidden" name="lnkFrom" value="<%=lnkFrom%>">
		    <input type="hidden" name="studyId" value="<%=studyId%>">
		 
		 
		 <input type="hidden" name="formId" value="<%=formId%>">
		 <Input type="hidden" name="mode" value="visited" >
		 <Input type="hidden" name="srcmenu" value="<%=src%>" >
		  <Input type="hidden" name="selectedTab" value="<%=selectedTab%>" >
  	 
  	
	 
	</FORM>
<%  
		}
		else
		{
	
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	
						
					formFldId=  request.getParameter("formFldId");	
					formId=  request.getParameter("formId");	
					calledFrom = request.getParameter("calledFrom");
					selectedTab = request.getParameter("selectedTab");
					
					formfieldB.setFormFieldId(Integer.parseInt(formFldId));
					formfieldB.getFormFieldDetails();
					
					hasInterFieldActions = fldLibB.hasInterFieldActions(formfieldB.getFieldId());
					
					if (!hasInterFieldActions)
					{
						formfieldB.setModifiedBy(modBy);
						formfieldB.setRecordType("D");
						
						//Modified for INF-18183 ::: Ankit
						String moduleName = ("L".equals(calledFrom))? LC.L_Form_Lib : LC.L_Form_Mgmt;
						ret =formfieldB.updateFormField(AuditUtils.createArgs(session,"",moduleName));
					}
					else
					{
						ret = -4;
					}	
					
					if (ret==-2 || ret == -4) 
					{%>	

						<br><br><br><br><br><br>
	
						<table width=100%>
						<tr>
						<td align=center><p class = "successfulmsg"><%=MC.M_Data_CntDel%><%--Data cannot be deleted.*****--%></p>
						
						<% if (ret == -4 ) { 
							refreshSec = 4;
						%>
							<p class = "successfulmsg"><%=MC.M_FldAct_RemFldTryAgain%><%--This field is part of one or more Inter Field Actions. Please remove the field from the Inter Field Action(s) and then try again.*****--%></p>
							
						<%
						} //ret == -4
					} else 
					{
							refreshSec = 1;
					 %>
						<P class="successfulmsg"><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </P>
					
					<%}%>							
						<META HTTP-EQUIV=Refresh CONTENT="<%=refreshSec%>; URL=formfldbrowser.jsp?studyId=<%=studyId%>&lnkFrom=<%=lnkFrom%>&codeStatus=<%=codeStatus%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&mode=M&formId=<%=formId%>&selectedTab=<%=selectedTab%>">
				
				
			 			</td>
						</tr>
						<tr height=20></tr>
						<tr>
						<td align=center>
						</td>		
						</tr>		
						</table>				
						<br><br><br><br><br> <p class = "successfulmsg" align = center>  </p>
				<% 
				} 
		
		}
		
		 				
  
	}//end esign
		
	else
	{ %> 
	 	<jsp:include page="timeout.html" flush="true"/> 
 <% } %>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>
</body>
</HTML>


