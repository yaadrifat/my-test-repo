<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Bug#9941 29-May-2012 -Sudhir-->
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<%--
<DIV class="browserDefault" id = "div1"> 
--%>
  <jsp:useBean id="grp" scope="request" class="com.velos.eres.web.group.GroupJB"/>
  <jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
  <jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

  int ienet = 2;
  String agent1 ;
  agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;


  
String src = null;
src = request.getParameter("src");
String fromPage = request.getParameter("fromPage");
String eSign = request.getParameter("eSign");


HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%   
//   	String oldESign = (String) tSession.getValue("eSign");
//	if(!oldESign.equals(eSign)) {
//%>

<%
//	} else {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");      
	String totRows = request.getParameter("totrows");
	int ret = 0;
	int grpId  =0;

	String grpName[] =null;

	String grpDesc[] =null;

	String groupName="";

	String groupDesc="";


	String accId="";

	String mode="";

	String msg="";

	String rights="";
	boolean proceed=true;
	
	int count=0;


	grpId = Integer.parseInt(request.getParameter("groupId"));

	mode = request.getParameter("mode");
	

	grpName = request.getParameterValues("groupName");

	grpDesc = request.getParameterValues("groupDesc");

	accId = request.getParameter("groupAccId"); 
	
	for(int test=0 ;test<EJBUtil.stringToNum(totRows);test++){		
	groupName=grpName[test];

		if((groupName!= null) && (groupName.trim()!= "")){
		count = grp.getCount(groupName, accId);}
		if (count>0){
		proceed=false;
		break;
		}
	}

       if(proceed==false) {
%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "sectionHeadings" align = center> <%=MC.M_OneGrpNameExst_EtrNew %><%-- The One of the Group Name that you have entered already 
  exists. Please enter a new name.*****--%> </p>
<form method="POST">
  <table align=center>
    <tr width="100%"> 
      <td width="100%" >
      <!-- Bug#9941 29-May-2012 -Sudhir-->	  
	  <button onclick="window.history.back();return false;"><%=LC.L_Back%></button> 
	  <%--
        <input type="button" name="return" value="Return" onClick="window.history.back()">
		--%>
      </td>
    </tr>
  </table>
</form>
<%
	} else {	

	ctrl.getControlValues("app_rights");

	int rows = ctrl.getCRows();

	for(int i=0;i<rows;i++){ 

	rights = rights +"0";
	
	}
	
	for(int i=0; i<EJBUtil.stringToNum(totRows);i++)
	{	
	groupName=(String)grpName[i];

	if(!(groupName.equals(""))){	
	
	groupDesc=grpDesc[i];
	grp.setGroupName(groupName);
	grp.setGroupDesc(groupDesc);
	grp.setGroupAccId(accId);
	grp.setCreator(usr); 
	grp.setIpAdd(ipAdd);	
	grp.setGroupSuperUserFlag("0");
	grp.setGroupHidden("0");//KM
	grp.setGroupDetails();

	int id = grp.getGroupId();
	
	grpRights.setId(id);
	grpRights.getGrpRightsDetails();//commented by vishal,caused nullpointerexception

	grpRights.setFtrRights(rights);
	grpRights.setIpAdd(ipAdd);
	grpRights.setModifiedBy(usr);
		
	grpRights.updateGrpRights();  	///Assigning rights to the group as not accessible

	}/*end of if for  name is not null */
	} 
	
	
%>  
<br>
<br>
<br>
<br>
<br> 
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p> 
<% } %>


<% if (fromPage.equals("accountbrowser")) {%>
	<META HTTP-EQUIV=Refresh CONTENT="3; URL=accountbrowser.jsp?srcmenu=<%=src%>">
<%} else if (fromPage.equals("groupbrowser")) {%>


<% 
if(proceed != false)
{ 
%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=groupbrowserpg.jsp?srcmenu=<%=src%>">
<%
}
%>	

<%}%>


<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
<%-- Ankit: Bug#10497 Date:10-July-2012 --%>
<jsp:include page="bottompanel.jsp" flush="true"/>
</BODY>
</HTML>
