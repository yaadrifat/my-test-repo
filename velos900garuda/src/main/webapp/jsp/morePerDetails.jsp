<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.person.PersonJB" %>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.eres.web.userSite.UserSiteJB" %>

<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_More_PatDets%><%--More <%=LC.Pat_Patient%> Details*****--%></TITLE>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>


//KM-Modified for 3917
function setValue(formobj,count,cbcount){
if(formobj.alternateId.value == undefined)
     value=formobj.alternateId[count].value;
else
    value=formobj.alternateId.value;

totalcbcount=formobj.cbcount.value;
if (value=="Y") {
if(formobj.alternateId.value == undefined)
	formobj.alternateId[count].value="N";
else
	formobj.alternateId.value="N";
if (totalcbcount==1) formobj.alternate.checked=false;
else formobj.alternate[cbcount-1].checked=false;
}
if ((value.length==0) || (value=="N"))  {
if (formobj.alternateId.value == undefined)
	formobj.alternateId[count].value="Y";
else
	formobj.alternateId.value="Y";
if (totalcbcount==1) formobj.alternate.checked=true;
else formobj.alternate[cbcount-1].checked=true;
}
}

function setDD(formobj)
{
	optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
	arrayofStrings=optvalue.split("||");
	if (arrayofStrings.length>1) {
	for (var j=0;j<arrayofStrings.length;j++)
	{
	var ddStr=arrayofStrings[j];
	arrayofDD=ddStr.split(":");
	var ddcount=arrayofDD[0];
	var selvalue=arrayofDD[1];
	var opt = formobj.alternateId[ddcount].options;
	for (var i=0;i<opt.length;i++){
	if (opt[i].value==selvalue){
	formobj.alternateId[ddcount].selectedIndex=i ;
	}
	}
	}
	} else
	{
	var ddStr=arrayofStrings[0];
	arrayofDD=ddStr.split(":");
	var ddcount=arrayofDD[0];
	var selvalue=arrayofDD[1];
	var opt = formobj.alternateId.options;
	if (opt == undefined)
    	opt = formobj.alternateId[ddcount].options;
	for (var i=0;i<opt.length;i++){
	if (opt[i].value==selvalue){
	    if (formobj.alternateId.options != undefined)
			formobj.alternateId.selectedIndex=i ;
		else
			formobj.alternateId[ddcount].selectedIndex=i ;
	}
	}
	}// end else
	}//optvalue.length>0
}

function validate(formobj) {

 	 if (!(validate_col('e-Signature',formobj.eSign))) return false

     if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }

	return true;
}


</script>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;" onLoad="setDD(document.perdetails)">
<%
	} else {
%>
<body onLoad="setDD(document.perdetails)">
<%
	}
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,java.lang.reflect.*"%>
<%@ page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="pId" scope="request" class="com.velos.eres.web.perId.PerIdJB"/>

<DIV class="popDefault" id="div1">
<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
		 <jsp:include page="include.jsp" flush="true"/>

<%
	 String accountId = (String) tSession.getValue("accountId");
	 String userIdFromSession = (String) tSession.getAttribute("userId");
	 String perId = request.getParameter("perId");
	 int pageRights = EJBUtil.stringToNum(request.getParameter("pageRights"));

 	 String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	 //get patient ids
	 PerIdDao pidDao = new PerIdDao();


	 pidDao = pId.getPerIds(EJBUtil.stringToNum(perId),defUserGroup);

 	 ArrayList perIdType  = new ArrayList();
	 ArrayList perIdTypesDesc = new ArrayList();
	 ArrayList id = new ArrayList();
	 ArrayList alternateId = new ArrayList();
 	 ArrayList recordType = new ArrayList();
	 ArrayList dispType=new ArrayList();
	 ArrayList dispData=new ArrayList();

	 id = pidDao.getId();
	 perIdType =  pidDao.getPerIdType();
	 perIdTypesDesc = pidDao.getPerIdTypesDesc();
	 alternateId = pidDao.getAlternateId();
	 recordType = pidDao.getRecordType ();
	 dispType=pidDao.getDispType();
	 dispData=pidDao.getDispData();


	 String strPerIdTypesDesc ;
 	 String strAlternateId ;
  	 String strRecordType ;
  	 Integer intPerIdType;
   	 Integer intId;
	 String disptype="";
	 String dataType="";
	 String ddStr="";
	 String dispdata="";

	 // Access rights check
	 PersonJB personJB = new PersonJB();
	 personJB.setPersonPKId(StringUtil.stringToNum(perId));
	 personJB.getPersonDetails();
	 int patCompleteDetailsRight = personJB.getPatientCompleteDetailsAccessRight(
			 StringUtil.stringToNum(userIdFromSession),
			 StringUtil.stringToNum(defUserGroup),
			 StringUtil.stringToNum(perId));
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	 pageRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	 UserSiteJB userSiteJB = new UserSiteJB();
	 int orgRight = userSiteJB.getUserPatientFacilityRight(
			 StringUtil.stringToNum(userIdFromSession),
			 StringUtil.stringToNum(perId));
	 if (patCompleteDetailsRight < 4 || pageRights < 4 || orgRight < 4) {
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%
		 return;
	 }
%>
<Form  id="perdetfrm" name="perdetails" action="updateMorePerDetails.jsp" method="post" onSubmit="if (validate(document.perdetails)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >

<P class = "sectionHeadings">
<%=LC.L_More_PatDets%><%--More <%=LC.Pat_Patient%> Details*****--%>
</P>
<table width="100%">

<%
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= perIdType.size() -1 ; counter++)
		{
				strPerIdTypesDesc = (String) perIdTypesDesc.get(counter);
 	 			strAlternateId = (String) alternateId.get(counter);
 	 			disptype=(String) dispType.get(counter);
				dispdata=(String) dispData.get(counter);

				if (disptype==null) disptype="";
				if (dispdata==null) dispdata="";

 	 			if (strAlternateId == null)
 	 				strAlternateId = "";

  	 			strRecordType = (String) recordType.get(counter);
				intPerIdType = (Integer) perIdType.get(counter) ;
   	 			intId = (Integer) id.get(counter) ;

	%>

	<tr>
		     <td class=tdDefault width="20%" >
				<%= strPerIdTypesDesc %>
			 </td>
		     <td class=tdDefault width="40%" >
				<% if ((disptype.toLowerCase()).equals("chkbox")){
				cbcount=cbcount+1;
				%>
				<input type = "hidden" name = "alternateId" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
						<%	 if ((strAlternateId.trim()).equals("Y")){%>
				 <input type="checkbox" name="alternate" value="<%=strAlternateId.trim()%>" onClick="setValue(document.perdetails,<%=counter%>,<%=cbcount%>)" checked>
				  <% }else{%>
				  <input type="checkbox"  name="alternate" value="<%=strAlternateId.trim()%>" onClick="setValue(document.perdetails,<%=counter%>,<%=cbcount%>)">

				<%}} else if ((disptype.toLowerCase()).equals("dropdown")) {
				if (ddStr.length()==0) ddStr=(counter)+":"+strAlternateId;
					else ddStr=ddStr+"||"+(counter)+":"+strAlternateId;
				%>
				 <%=dispdata%>
				 <%}else if ((disptype.toLowerCase()).equals("date")) {
				 %>
<%-- INF-20084 Datepicker-- AGodara --%>
				 	<input name="alternateId" type="text" class="datefield" size="10" readOnly  value="<%=strAlternateId.trim()%>">
				<%}else{%>
				<input type = "text" name = "alternateId" value = "<%=strAlternateId.trim()%>" size = "25" maxlength = "100" >
				<%}%>
				<input type = "hidden" name = "recordType" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id" value = "<%=intId%>" >
				<input type = "hidden" name = "perIdType" value = "<%=intPerIdType%>" >
				<input type = "hidden" name = "perId" value = "<%=perId%>" >
			 </td>
			 <td width="30%">&nbsp;
			 </td>
	</tr>
	<%
		}
	%>
	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">


	<%

	if(pageRights >= 6 ){%>

<tr>
<td colspan="2">

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="perdetfrm"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="noBR" value="Y"/>

</jsp:include>

</td>
</tr>

		<%}%>
</table>
	</form>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%}%>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
