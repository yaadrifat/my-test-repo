<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_MngPat_EnrlDets%><%-- Manage <%=LC.Pat_Patient%> >> Enrollment Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.EventAssocDao,java.util.*,java.io.*,com.velos.eres.service.util.*"%>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="studyProt" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="codeLstB" scope="page" class="com.velos.eres.web.codelst.CodelstJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="patTXArmJB" scope="request" class="com.velos.eres.web.patTXArm.PatTXArmJB"/>
<jsp:useBean id="commonB" scope="page" class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />
	

<script>
function f_deletePatTX(pattxarmid,studyId,name,pageRight,orgRight)
{
 if (f_check_perm_org(pageRight,orgRight,'E')) 
	{
	var paramArray = [name];
	if (confirm(getLocalizedMessageString("M_Want_DelTmtArm",paramArray))) {/*if (confirm("Do you want to delete treatment arm '"+name+"'?")) {*****/
     windowName= window.open("pattxarmdelete.jsp?pattxArmId="+pattxarmid+"&studyId="+studyId,'pattxtdelete',"toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,top=150, left=200,width=650,height=350");
	 windowName.focus();
   }
   else{
   return false;
   }
   }
   else{
   return false;
   }
}

	function openStatus(patId,patStatPk,enrlDate,pgRight,study,orgRight) 
{	
	if(enrlDate == "")
	{
		mode = 'N';
	}
	else{
		mode = 'M';
	}
	
	var changeStatusMode;
	var permissionCode;
	
	if (mode == 'N')
	{
		changeStatusMode = "yes";
	
	}
	else
	{
		changeStatusMode = "no"
	}
	
	if (mode == 'M')
	{
		permissionCode = 'E'
	}
	else
	{
		permissionCode = mode;
	}

	if (f_check_perm_org(pgRight,orgRight, permissionCode) == true) {

		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode="+changeStatusMode+"&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=560");
		windowName.focus();
	}
}

function openWinTX(patProtId, studyId, patArmsId, mode, studyNumber, patientCode, pageRight, orgRight, lnkMode)
{
	
	 //KM-3991	
     if (patProtId =="" || patProtId == 0) {
    	 alert("<%=MC.M_ThereNoPatStd_ArmAdd%>");/*alert("There is no <%=LC.Pat_Patient%> <%=LC.Std_Study_Lower%> status added for this <%=LC.Pat_Patient%>, Treatment Arm cannot be added");*****/
	    return false;
	 }


if (f_check_perm_org(pageRight,orgRight,lnkMode))       
	{
	windowName= window.open("pattrtarm.jsp?studyId="+studyId+"&patProtId=" + patProtId+"&patTXArmId="+patArmsId+"&mode="+mode+"&studyNumber="+encodeString(studyNumber)+"&patientCode="+encodeString(patientCode ),"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=560");
	windowName.focus();
	}
	else{
	return false;
	}
}

</script>	
<% String src;

src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>


<%

   HttpSession tSession = request.getSession(true); 

  if (sessionmaint.isValidSession(tSession))
	{

	String mode = null;
	String uName = (String) tSession.getValue("userName");

	String userIdFromSession = (String) tSession.getValue("userId");
	//check whether the user has right to edit the patients data of the patient org
	String patientId="";
	patientId = request.getParameter("pkey");
	
	person.setPersonPKId(EJBUtil.stringToNum(patientId));
	person.getPersonDetails();		
	

	String acc = (String) tSession.getValue("accountId");
	
	String patientCode = StringUtil.decodeString(request.getParameter("patientCode"));
	
	String patStudiesEnrollingOrg = request.getParameter("patStudiesEnrollingOrg"); // this param is passed from patientstudies
	
	if (StringUtil.isEmpty(patStudiesEnrollingOrg))
	{
		patStudiesEnrollingOrg = person.getPersonLocation();
		
		//if patStudiesEnrollingOrg is not passed, set it to patient's default value 
	}
	
	if (StringUtil.isEmpty(patientCode))
	{
		patientCode = person.getPersonPId();
	}	

	String studyId = request.getParameter("studyId");
	if(studyId == null || studyId.equals("null")){
		studyId = (String) tSession.getValue("studyId");
	 } else {
		tSession.setAttribute("studyId",studyId);		
 	 }	
	
	//find patient's current enrollment
	int patProtId = 0;
	patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(patientId));
	// 
	patProtId   = patEnrollB.getPatProtId();
		
	
	

	String studyTitle = "";
	String studyNumber = "";
	
  	studyB.setId(EJBUtil.stringToNum(studyId));
    studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	
   	
//	Check if the Organization study combination is open for enrolling
	StudyStatusDao statDao=null;
	int iStudyId=EJBUtil.stringToNum(studyId);
	int index=-1,stopEnrollCount=0,activeClsCount=0,activeCount=0,siteCount=0;
	
	String studyAccrualFlag="";
	Hashtable statTable=null;
	if (iStudyId>0)
	{
	 SettingsDao settingsDao=commonB.retrieveSettings(iStudyId,3);
	 ArrayList settingsValue=settingsDao.getSettingValue();
	 ArrayList settingsKeyword=settingsDao.getSettingKeyword();
	 index=settingsKeyword.indexOf("STUDY_ACCRUAL_FLAG");
		if (index>=0)
		{
		 studyAccrualFlag=(String)settingsValue.get(index);
		 studyAccrualFlag=(studyAccrualFlag==null)?"":studyAccrualFlag;
		}
		else
		{
			studyAccrualFlag="D";
		}
		
	  if ( (studyAccrualFlag.equals("D")))  {
	   //Disable this for now, with Default settings everything will work as it was in ver 6.2    
	  //stopEnrollCount=studyStatB.getCountByOrgStudyStat(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(organization),"'active_cls'","A");
	   stopEnrollCount=0;
	  }else if (studyAccrualFlag.equals("O") && patProtId ==0)
	  {
		 statDao=studyStatB.getDAOByOrgStudyStat(iStudyId,EJBUtil.stringToNum(patStudiesEnrollingOrg),"'active_cls','active','prmnt_cls'","A");
		 ArrayList statList = new ArrayList();
		 
		 statList=statDao.getStudyStatList();
		 
		 if (statList.size()==0) stopEnrollCount=0;
		 
		 if (statList.indexOf("active_cls")>=0)
			 stopEnrollCount=1;
		 
		 if (statList.indexOf("active")<0)
			 stopEnrollCount=1;
		 
		 if (statList.indexOf("prmnt_cls")>0)
			 stopEnrollCount=1;
		
		 //apraoch changed by SOnia for issue
		 //now we do have organization for enrollment
		  
		 /*statDao=studyStatB.getDAOByOrgStudyStat(iStudyId,0,"'active_cls','active'","studyprot");
		 statTable=statDao.getStudyStatCountMap();
		 		 	 
		 if (statTable.size()==0) stopEnrollCount=1;
		 
		 activeClsCount=EJBUtil.stringToNum((String)statTable.get("active_cls"));
		 activeCount=EJBUtil.stringToNum((String)statTable.get("active"));
		 siteCount=EJBUtil.stringToNum((String)statTable.get("sitecount"));
		 
		 if ((activeCount>0) && (activeClsCount<siteCount))
			 stopEnrollCount=0;
		 
		 if (activeClsCount==siteCount)
			 stopEnrollCount=1; */
		 
		 
		 
		 
	  }
	}
		
	//End check for Organization study combination is open for enrolling
   	if (stopEnrollCount>0)
		{			
			%>
				<TABLE width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl">
			 	<tr>
					<td align="center">
			     	<P class = "successfulmsg">
			     		<%=MC.M_EnrlNotPrmtd_SelDiffOrg%><%-- Enrollment is not permitted at this time. The <%=LC.Std_Study_Lower%> may not be open at this organization, may be closed or temporarily on hold. <br>Please select a different organization or contact the <%=LC.Std_Study%> Administrator for more information.*****--%>
				 	</P>
				</td>
			   </tr>
			 </table>
			
			<TABLE  width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl">
				<tr>
					<td align=center> 
					<button onclick="window.history.back();"><%=LC.L_Back%></button>
					</td> 
				</tr>
   				</table>
   				<script>
if (!(typeof(document.getElementById("notification"))=="undefined") && (document.getElementById("notification")!=null))	
toggleNotification("off","");
</script>
			<%	
						return;
			
		}

//VA 08282008   	else {

	int pageRight = 0;
	
	 //******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
   	TeamDao teamDao = new TeamDao();
   	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
   	ArrayList tId = teamDao.getTeamIds();
   	int patdetright = 0;
   	if (tId.size() == 0) { 
   		pageRight=0 ;
   	}else {
   		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
   		 
	 	ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();
					 
				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();
				
   		 
   		tSession.setAttribute("studyRights",stdRights);
   		if ((stdRights.getFtrRights().size()) == 0){
   		 	pageRight= 0;
   			patdetright = 0;
   		}else{
   			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
   			patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));
   			tSession.setAttribute("studyManagePat",new Integer(pageRight));
   			tSession.setAttribute("studyViewPatDet",new Integer(patdetright));
    		}
   	}
	
	
	
	String organization = person.getPersonLocation();  
	int orgRight = 0;
	

	orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), EJBUtil.stringToNum(patientId) , EJBUtil.stringToNum(studyId) );
	if (orgRight > 0)
	{
		System.out.println("patient enroll  orgRight" + orgRight);
		orgRight = 7;
	}
	

	
 	if ( ( pageRight >= 4 ) && ( orgRight >= 4) )

	{

	//include patienttabs if user has rights to MANAGE PATIENTS                             

	    String enrollDate  ="";
		String protStDate ="";
		String notes ="";
		String patEnrollBy ;
		
		String protocolId ="";
		String enrollByName = "";
		
		
  	    String regDate = "";
		int siteId = 0;
		
		String patStudyId = "";
		String randomizationNumber = "";
		siteId = EJBUtil.stringToNum(person.getPersonLocation());
		
		String patAssignedTo = "";
		String patAssignedToName = "";
		String patPhysician = "";
		String patPhysicianName = "";
		String patTreatLoc = "";
		String patTreatLocDesc = "";
		String warningMessage = "";
		String enrlDate = "";

		regDate = person.getPersonRegDate();
		int num;
		
				%>

<DIV class="BrowserTopn" id="div1"> 

		<jsp:include page="patienttabs.jsp" flush="true"> 
			<jsp:param name="studyId" value="<%=studyId%>"/>
			<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
			<jsp:param name="page" value="patientEnroll"/>
		</jsp:include>
	 </div>
<DIV class="tabFormBotN tabFormBotN_PSc_1" id="div2"> 

	<%
		
		if ( patProtId <= 0 ) //patient is not yet enrolled
		{
			
			//reset the session attributes in case of new enrollment
			tSession.setAttribute("studyId","");
			tSession.setAttribute("enrollId","");
			tSession.setAttribute("studyRights",null);		
		}
		else //patient is enrolled
		{
			tSession.setAttribute("enrollId",String.valueOf(patProtId));
			
			enrollDate =patEnrollB.getPatProtEnrolDt();
			patientId = patEnrollB.getPatProtPersonId();
			protStDate = patEnrollB.getPatProtStartDt();
			notes = patEnrollB.getPatProtNotes();
			notes = (notes == null )?"":(notes) ;	

			patEnrollBy = patEnrollB.getPatProtUserId();
			protocolId =patEnrollB.getPatProtProtocolId();
			studyId = patEnrollB.getPatProtStudyId();

			patTreatLoc = patEnrollB.getTreatmentLoc();
			patPhysician = patEnrollB.getPatProtPhysician();
			patAssignedTo = patEnrollB.getAssignTo();
			randomizationNumber = patEnrollB.getPatProtRandomNumber();
				
			if (EJBUtil.isEmpty(patPhysician))
				patPhysician = "";
			if (EJBUtil.isEmpty(patAssignedTo))
				patAssignedTo = "";
			
				if (!EJBUtil.isEmpty(patAssignedTo))		
				{
		  			userB.setUserId(EJBUtil.stringToNum(patAssignedTo));
					userB.getUserDetails();
					patAssignedToName = userB.getUserFirstName() + " " + userB.getUserLastName();
				}	
				if (!EJBUtil.isEmpty(patPhysician))		
				{
		  			userB.setUserId(EJBUtil.stringToNum(patPhysician));
					userB.getUserDetails();
					patPhysicianName  = userB.getUserFirstName() + " " + userB.getUserLastName();
				}
	
			if (! EJBUtil.isEmpty(patTreatLoc))
			{
			 patTreatLocDesc = codeLstB.getCodeDescription(EJBUtil.stringToNum(patTreatLoc));
			}

			patStudyId = patEnrollB.getPatStudyId();
			if (patStudyId==null) {
				patStudyId="";
			}
			
			
			if (EJBUtil.isEmpty(randomizationNumber ))
				randomizationNumber = "";

			//get reg by name 
			
			 if (! EJBUtil.isEmpty(patEnrollBy))
			 {	
			  userB.setUserId(EJBUtil.stringToNum(patEnrollBy));
			  userB.getUserDetails();
			  enrollByName = userB.getUserFirstName() + " " + userB.getUserLastName();
			  }
		    			
		}	
		
%>

<Form name="enroll" method="post" action="" onsubmit="">



<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="pataccount" Value="<%=acc%>">
<Input type="hidden" name="patientId" value="<%=patientId%>">
<Input type="hidden" name="studyId" value="<%=studyId%>">
<Input type="hidden" name="patRegDate" value="<%=regDate%>">
<Input type="hidden" name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>">
<Input type="hidden" name="selectedTab" value="<%=selectedTab%>">
<Input type="hidden" name="calledFrom" value="enroll">



<%

  int personPK = EJBUtil.stringToNum(request.getParameter("pkey"));

%>

<Input type="hidden" name="personPK" value=<%=personPK%>>

<table  width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl  midAlign">
  <tr> 

<%	
studyTitle = StringUtil.escapeSpecialCharJS(studyTitle); 

	
%>

<script language=javascript>
	var varViewTitle = htmlEncode('<%=studyTitle%>');
</script>

	<td><font size="1">
	<%=LC.L_Study_Number%><%-- <%=LC.Std_Study%> Number*****--%>: <a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><%=studyNumber%></a> &nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onmouseover="return overlib(varViewTitle,CAPTION,'<%=LC.L_Study_Title%><%--Study Title*****--%>');" onmouseout="return nd();"><img border="0" src="./images/View.gif"/></a>  &nbsp;&nbsp;&nbsp;&nbsp;

    </td>

  </tr>

 <tr> 
</table>


	<%


 	if ( ( pageRight >= 4 ) && ( orgRight >= 4) )
	{

	if(EJBUtil.isEmpty(enrollDate))
	{
		enrlDate = "";
		
	}
	else{
	 enrlDate = enrollDate;
	}
	 
	 //Added by Manimaran for September Enhancement S8.
	 PatStudyStatDao ps = new PatStudyStatDao();
	 ps.getPatStatHistory(EJBUtil.stringToNum(patientId), EJBUtil.stringToNum(studyId));
	 ArrayList patStatPks = new ArrayList();
	 ArrayList stats = new ArrayList(); //KM
	 ArrayList currentStats =  new ArrayList();//KM
	 patStatPks = ps.getPatientStatusPk();
	 stats = ps.getPatientStatusDescs();
	 currentStats = ps.getCurrentStats();
	 String patStatPk = "";
	 String prevPatStudyStatPK = "";
	 String currentVal="";
	 String currentStatus="";
         int len=patStatPks.size();
	 for (int i=0;i<len;i++) {
	     currentVal = (currentStats.get(i) == null)?"":(currentStats.get(i)).toString();
	     if (currentVal.equals("1")) {
			  currentStatus = (String)stats.get(i).toString();
	     }
    	}			 
	 
	 if(enrlDate.equals("")){
	 	 	//get latest status
		  	if (patStatPks.size() > 0 )
		  	{
		  		prevPatStudyStatPK = (patStatPks.get(0)).toString();
		  	}
	}//if not enrolled
		
		//else get the pkStudyStat where status is enrolled
		else{
			int pkPatStudyStat = 0;
			/*CodeDao cd = new CodeDao();
			int enrolledPatPk = cd.getCodeId("patStatus","enrolled");
			out.println("enrolledPatPk"+enrolledPatPk);*/
			
			pkPatStudyStat = patB.getPkForStatusCodelstSubType(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(patientId),"enrolled");
			prevPatStudyStatPK = String.valueOf(pkPatStudyStat);
		}
	%>
	 <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign">
	 <tr>
	<td width="100%"><p class = "sectionheadings" ><%=LC.L_Pat_StatusDets%><%-- <%=LC.Pat_Patient%> Status Details*****--%> &nbsp;&nbsp;&nbsp;<A id="status" onclick="openStatus('<%=patientId%>','<%=prevPatStudyStatPK%>','<%=enrlDate%>','<%=pageRight%>','<%=studyId%>','<%=orgRight%>')" href=#><%=LC.L_Edit_Dets%><%-- Edit Details*****--%></A>
		</p></td>
	 </tr>
	 </table>
<table  width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign">
   <tr>
	  <td width ="15%"> <b><%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%>:</b></td>
	  <td width ="15%"><%=patStudyId%> </td>
 	  <td width ="15%"> <b><%=LC.L_Randomization%><%-- Randomization*****--%> #:</b></td>
	  <td width ="15%"><%=randomizationNumber%> </td>
	   <td width ="15%">	<b><%=LC.L_Assigned_To%><%-- Assigned To*****--%>: </b>	</td>
		<td width="23%"> 	 <%=patAssignedToName%></td>
  </tr>
  <tr> 
    <td >      <b><%=LC.L_Enrollment_Date%><%-- Enrollment Date*****--%> </b>   </td>
   <td >	<%=enrollDate%>	</td>
   <td >       <b><%=LC.L_Enrolled_By%><%-- Enrolled By*****--%> </b>   </td>
    <td >          <%=enrollByName%>    </td>
    <td > 		<b><%=LC.L_Physician%><%-- Physician*****--%></b> 	</td>
	<td > 	 	<%=patPhysicianName%>	 </td>
 </tr>
 <tr>
	
		
	 </tr>
	<tr>	
	  <td>	<b><%=LC.L_Treatment_Loc%><%-- Treatment Location*****--%></b> 	  </td>
	  <td >		<%=patTreatLocDesc %>		</td>
	   <td >		<b><%=LC.L_Current_Status%><%-- Current Status*****--%></b> 	  </td>
	  <td >		<%=currentStatus%>		</td>
	</tr>
	<!-- Added by Manimaran for September Enhancement S8 -->
	
</table>
<%
	if ( patProtId <= 0 ) //patient is not yet enrolled
     {	 
		warningMessage	= MC.M_CautionPatReg_IncompStd;/*warningMessage	= "Caution: Please note that if you exit this screen without recording any status, the "+LC.Pat_Patient_Lower+" registration process is incomplete and "+LC.Pat_Patient_Lower+" will not be linked to this "+LC.Std_Study+".";*****/
	%>	
 	 <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl  midAlign">
 		 <tr>
 	 		<td><P class="redMessage">
 	 		<%=warningMessage%></P>
 	 		</td>
 	 	</tr>
 	 </table> 
 	<%
 	}
	
	PatTXArmDao pDao = new PatTXArmDao();
	pDao = patTXArmJB.getPatTrtmtArms(patProtId);
	ArrayList patArmsIds = pDao.getPatTXArmIds();
	ArrayList txNames = pDao.getTXNames();
	ArrayList drugsInfo = pDao.getDrugsInfo();
	ArrayList startDates = pDao.getStartDates();
	ArrayList endDates = pDao.getEndDates();
	ArrayList txnotes = pDao.getNotes();
	
	String patArmsId = "";
	String txName = "";
	String drugInfo = "";
	String startDate = "";
	String endDate = "";
	String txnote = "";
	int startDateLen = 0;
    int endDateLen = 0;
	
   %> 	
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr height="8"><td></td></tr>
   	  <tr>     
       <td width = "70%"> <P class = "defComments"><%=MC.M_ListDisp_PatTreatArm%><%-- The list below displays <%=LC.Pat_Patient_Lower%>'s Treatment Arms*****--%>:</P></td>
	   <td width="30%" align="center"> 
   <A onclick="openWinTX('<%=patProtId%>','<%=studyId%>',0,'N','<%=studyNumber%>','<%=patientCode%>','<%=pageRight%>','<%=orgRight%>','N')"  href ="#" Title="<%=MC.M_AddNew_TreatArm%><%-- Add New Treatment Arm*****--%>">
	    		<%=MC.M_AddNew_TreatArm%><%--ADD NEW TREATMENT ARM*****--%>
	    	</A></td>
			</tr>
			</table>
   <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midAlign" >
       <tr> 
        	<th width="18%"> <%=LC.L_Treatment_Arm%><%-- Treatment Arm*****--%> </th>
			<th width="18%"> <%=LC.L_Drug_Info%><%-- Drug Information*****--%></th>
        	<th width="18%"> <%=LC.L_Status_Date%><%-- Status Date*****--%></th>
			<th width="18%"> <%=LC.L_End_Date%><%-- End Date*****--%> </th>
	    	<th width="18%"> <%=LC.L_Notes%><%-- Notes*****--%> </th>
	    	<th width="10%"><%=LC.L_Delete%><%--Delete*****--%></th>
      </tr>
       <%
	    for(int counter = 0;counter<patArmsIds.size();counter++)
		{	
		patArmsId = (patArmsIds.get(counter)).toString();
		txName=((txNames.get(counter)) == null)?"-":(txNames.get(counter)).toString();
		startDate=((startDates.get(counter)) == null)?"-":(startDates.get(counter)).toString();
		startDateLen=startDate.length();

		if (startDateLen > 1) {
			startDate = DateUtil.dateToString(java.sql.Date.valueOf(startDate.substring(0,10)));

		}
		

		endDate=((endDates.get(counter)) == null)?"-":(endDates.get(counter)).toString();
		 endDateLen=endDate.length();
			if (endDateLen > 1) {
			endDate = DateUtil.dateToString(java.sql.Date.valueOf(endDate.substring(0,10)));
		}

		drugInfo=((drugsInfo.get(counter)) == null)?"-":(drugsInfo.get(counter)).toString();
		txnote=((txnotes.get(counter)) == null)?"-":(txnotes.get(counter)).toString();

	 

			if ((counter%2)==0) {
  %>

      <tr class="browserEvenRow"> 
    <%
		}
	else{
  %>
      <tr class="browserOddRow"> 
     <%
		}
  %>

		<td align=center ><a onclick="openWinTX('<%=patProtId%>','<%=studyId%>','<%=patArmsId%>','M','<%=studyNumber%>','<%=patientCode%>','<%=pageRight%>','<%=orgRight%>','E')"   href="#"><%=txName%></a></td> 
		<td align=center><%=drugInfo%> </td>  
        <td align=center><%=startDate%> </td>
        <td align=center><%=endDate%> </td>         	
     	<td align=left><%=txnote%></td>   
		<td align="center"><A href="#" onClick="return f_deletePatTX('<%=patArmsId%>','<%=studyId%>','<%=txName%>','<%=pageRight%>','<%=orgRight%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>
        </tr>

      <%
		}

		%>
				</table>
   	 <br>
   	 <% System.out.println("patStudiesEnrollingOrg" + patStudiesEnrollingOrg);%>
 <jsp:include page="patstudyhistorycommon.jsp" flush="true">
     	<jsp:param name="studyId" value="<%=studyId%>"/>
		<jsp:param name="patientId" value="<%=patientId%>"/>
		<jsp:param name="pageRight" value="<%=pageRight%>"/>
		<jsp:param name="orgRight" value="<%=orgRight%>"/>
		<jsp:param name="patProtId" value="<%=patProtId%>"/>
		<jsp:param name="patStudiesEnrollingOrg" value="<%=patStudiesEnrollingOrg%>"/>
		
</jsp:include>
	 
</Form>

<%
	
} //end of if body for page right

else



{



%>

<jsp:include page="accessdenied.jsp" flush="true"/> 



<%

} //end of else body for page right

}//end of if body for session
else
{
	%>

	<jsp:include page="accessdenied.jsp" flush="true"/> 



	<%	
}
  	}
else

{

%>

<jsp:include page="timeout.html" flush="true"/> 

<%

}
   
%>

<div>

<jsp:include page="bottompanel.jsp" flush="true"/> 



</div>

</div>

<DIV class="mainMenu" id = "emenu">

  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->   

</DIV>
<script defer>
if (typeof(windowName)!="undefined")
{
	
	setTimeout("windowName.focus();",1);
	
	
}

</script>
</body>

</html>



