<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Manage_Org%><%--Manage Organizations*****--%></title>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	
 function  validate(formobj){
    if (!(validate_col('e-Sign',formobj.eSign))) return false
   
   	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}
}

//Added by Manimaran for June Enhancement #GL2
function checkAll(formobj){
    	normalTotCnt = formobj.normalTotCnt.value;
		primOrgTotCnt = formobj.primOrgTotCnt.value;
		if(formobj.All.checked) {
          
			if (normalTotCnt==0) {
				formobj.newr.checked = true;
			    formobj.edit.checked = true;
		        formobj.view.checked = true;
			    row = formobj.rowCnt.value;
				if (row > 0) {
					formobj.rights[row].value = 7;
			    } else {
				  formobj.rights.value = 7;
				}
		   } else {
  		
				for (i=0;i<=normalTotCnt;i++) {
				  formobj.newr[i].checked = true;
				  formobj.edit[i].checked = true;
				  formobj.view[i].checked = true;
				  row = formobj.rowCnt[i].value;
				  formobj.rights[row].value = 7;
		       }	  	  
		  }


		
			if (primOrgTotCnt==0) {
				formobj.newr_P.checked = true;
			    formobj.edit_P.checked = true;
		        formobj.view_P.checked = true;
			    row = formobj.rowCnt_P.value;
				if (row > 0) {
					formobj.rights[row].value = 7;
			    } else {
				  formobj.rights.value = 7;
				}
		   } else {
  		
				for (i=0;i<=primOrgTotCnt;i++) {
				  formobj.newr_P[i].checked = true;
				  formobj.edit_P[i].checked = true;
				  formobj.view_P[i].checked = true;
				  row = formobj.rowCnt_P[i].value;
				  formobj.rights[row].value = 7;
		       }	  	  
		  }


       }
       else {  
  		
		   if (normalTotCnt==0) {
				formobj.newr.checked = false;
			    formobj.edit.checked = false;
		        formobj.view.checked = false;
			    row = formobj.rowCnt.value;
				if (row > 0) {
					formobj.rights[row].value = 0;
			    } else {
				  formobj.rights.value = 0;
				}
		   } else {
  		
				for (i=0;i<=normalTotCnt;i++) {
				  formobj.newr[i].checked = false;
				  formobj.edit[i].checked = false;
				  formobj.view[i].checked = false;
				  row = formobj.rowCnt[i].value;
				  formobj.rights[row].value = 0;
		       }	  	  
		  }
      
			if (primOrgTotCnt==0) {
				formobj.newr_P.checked = false;
			    formobj.edit_P.checked = false;
		        formobj.view_P.checked = false;
			    row = formobj.rowCnt_P.value;
				if (row > 0) {
					formobj.rights[row].value = 0;
			    } else {
				  formobj.rights.value = 0;
				}
		   } else {
  		
				for (i=0;i<=primOrgTotCnt;i++) {
				  formobj.newr_P[i].checked = false;
				  formobj.edit_P[i].checked = false;
				  formobj.view_P[i].checked = false;
				  row = formobj.rowCnt_P[i].value;
				  formobj.rights[row].value = 0;
		       }	  	  
		  }
     }
  }


function changeRights(obj,row,formobj,rightsRow)	{
	  selrow = row ;
	  totrows = formobj.totalrows.value;
	  primOrgCnt = formobj.primOrgTotCnt.value;
	  normalCnt =  formobj.normalTotCnt.value;

	  if (totrows > 1) 	rights =formobj.rights[rightsRow].value;
	  else 	rights =formobj.rights.value;
      
	  objName = obj.name;
       if (totrows>1){
	if (obj.checked) {         
       	if (objName == "newr"){
			rights = parseInt(rights) + 1;
			if (normalCnt==0) {
				if (!formobj.view.checked){ 
					formobj.view.checked = true;
					rights = parseInt(rights) + 4;		
				}
			} else {
				if (!formobj.view[selrow].checked){ 
					formobj.view[selrow].checked = true;
					rights = parseInt(rights) + 4;					
				}
			}
		}

       	if (objName == "newr_P"){		 
			rights = parseInt(rights) + 1;
			if (primOrgCnt==0) {
				if (!formobj.view_P.checked){
     				formobj.view_P.checked = true;
     				rights = parseInt(rights) + 4;
     			}
			} else {
    			if (!formobj.view_P[selrow].checked){ 
     				formobj.view_P[selrow].checked = true;
     				rights = parseInt(rights) + 4;
     			} 
			}
		}		

     	if (objName == "edit")	{
			rights = parseInt(rights) + 2;
			if (normalCnt == 0) {
			    if (!formobj.view.checked)	{ 
     				formobj.view.checked = true;
     				rights = parseInt(rights) + 4;
     			}
			}else {
     			if (!formobj.view[selrow].checked)	{ 
     				formobj.view[selrow].checked = true;
     				rights = parseInt(rights) + 4;
     			}
			}
		}
		
     	if (objName == "edit_P")	{
			rights = parseInt(rights) + 2;
			if (primOrgCnt==0) {
    			if (!formobj.view_P.checked)	{ 
    				formobj.view_P.checked = true;
    				rights = parseInt(rights) + 4;
    			}
			} else {
    			if (!formobj.view_P[selrow].checked)	{ 
    				formobj.view_P[selrow].checked = true;
    				rights = parseInt(rights) + 4;
    			}
			}
		}
		
     	if (objName == "view") rights = parseInt(rights) + 4;
     	if (objName == "view_P") rights = parseInt(rights) + 4;		

		if (totrows > 1 )
			formobj.rights[rightsRow].value = rights;
		else
			formobj.rights.value = rights;
  }else	{
       	if (objName == "newr") rights = parseInt(rights) - 1;
       	if (objName == "newr_P") rights = parseInt(rights) - 1;		
       	if (objName == "edit") rights = parseInt(rights) - 2;
       	if (objName == "edit_P") rights = parseInt(rights) - 2;		
       	if (objName == "view")	{
			if (normalCnt==0) {	
    			if (formobj.newr.checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view.checked = true;		
    			}	
    		    else if (formobj.edit.checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view.checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			} else {
    			if (formobj.newr[selrow].checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view[selrow].checked = true;		
    			}	
    		    else if (formobj.edit[selrow].checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view[selrow].checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			}						
		}

       	if (objName == "view_P")	{
			if (primOrgCnt==0) {	
    			if (formobj.newr_P.checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P.checked = true;		
    			}	
    		    else if (formobj.edit_P.checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P.checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			}else {
    			if (formobj.newr_P[selrow].checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P[selrow].checked = true;		
    			}	
    		    else if (formobj.edit_P[selrow].checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P[selrow].checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			}			
		}


	if (totrows > 1 ) {
		formobj.rights[rightsRow].value = rights;
    } 
	else {
		formobj.rights.value = rights; 
  	 }
  }
  } else if (totrows==1) {
  	if (obj.checked) {         
       	if (objName == "newr"){
			rights = parseInt(rights) + 1;
			if (normalCnt==0) {
				if (!formobj.view.checked){ 
					formobj.view.checked = true;
					rights = parseInt(rights) + 4;		
				}
			} else {
				if (!formobj.view.checked){ 
					formobj.view.checked = true;
					rights = parseInt(rights) + 4;					
				}
			}
		}

       	if (objName == "newr_P"){		 
			rights = parseInt(rights) + 1;
			if (primOrgCnt==0) {
				if (!formobj.view_P.checked){
     				formobj.view_P.checked = true;
     				rights = parseInt(rights) + 4;
     			}
			} else {
    			if (!formobj.view_P.checked){ 
     				formobj.view_P.checked = true;
     				rights = parseInt(rights) + 4;
     			} 
			}
		}		

     	if (objName == "edit")	{
			rights = parseInt(rights) + 2;
			if (normalCnt == 0) {
			    if (!formobj.view.checked)	{ 
     				formobj.view.checked = true;
     				rights = parseInt(rights) + 4;
     			}
			}else {
     			if (!formobj.view.checked)	{ 
     				formobj.view.checked = true;
     				rights = parseInt(rights) + 4;
     			}
			}
		}
		
     	if (objName == "edit_P")	{
			rights = parseInt(rights) + 2;
			if (primOrgCnt==0) {
    			if (!formobj.view_P.checked)	{ 
    				formobj.view_P.checked = true;
    				rights = parseInt(rights) + 4;
    			}
			} else {
    			if (!formobj.view_P.checked)	{ 
    				formobj.view_P.checked = true;
    				rights = parseInt(rights) + 4;
    			}
			}
		}
		
     	if (objName == "view") rights = parseInt(rights) + 4;
     	if (objName == "view_P") rights = parseInt(rights) + 4;		

			formobj.rights.value = rights;
  }else	{
       	if (objName == "newr") rights = parseInt(rights) - 1;
       	if (objName == "newr_P") rights = parseInt(rights) - 1;		
       	if (objName == "edit") rights = parseInt(rights) - 2;
       	if (objName == "edit_P") rights = parseInt(rights) - 2;		
       	if (objName == "view")	{
			if (normalCnt==0) {	
    			if (formobj.newr.checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view.checked = true;		
    			}	
    		    else if (formobj.edit.checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view.checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			} else {
    			if (formobj.newr.checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view.checked = true;		
    			}	
    		    else if (formobj.edit.checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view.checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			}						
		}

       	if (objName == "view_P")	{
			if (primOrgCnt==0) {	
    			if (formobj.newr_P.checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P.checked = true;		
    			}	
    		    else if (formobj.edit_P.checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P.checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			}else {
    			if (formobj.newr_P.checked) {
    				alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P.checked = true;		
    			}	
    		    else if (formobj.edit_P.checked)	{
    		    	alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/
    				formobj.view_P.checked = true;		
    			} else{		
    			  rights = parseInt(rights) - 4;
    			}
			}			
		}


		formobj.rights.value = rights; 
  	
  }
  }
}

function chkAllChild(formobj) {
//if user clicks on 'User has access to all child organizations' radio button then
//check new,edit & view rights for all the children of Primary Organization
  primOrgCnt = formobj.primOrgTotCnt.value;
  
  if (primOrgCnt==0) {
	  formobj.newr_P.checked = true;
  	  formobj.edit_P.checked = true;
  	  formobj.view_P.checked = true;
	  row = formobj.rowCnt_P.value;
	  if (row > 0) {
		  formobj.rights[row].value = 7;
	  } else {
		  formobj.rights.value = 7;
	  }
  } else {
  		
		for (i=0;i<=primOrgCnt;i++) {
		  formobj.newr_P[i].checked = true;
		  formobj.edit_P[i].checked = true;
		  formobj.view_P[i].checked = true;
		  row = formobj.rowCnt_P[i].value;
		  formobj.rights[row].value = 7;
		}	  	  
  }
}

</SCRIPT>

<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<%@ page language = "java" import="java.util.*,java.io.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.userSite.*,com.velos.eres.web.user.UserJB"%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<div class="popDefault" style="width:480px">
<%		
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/> 
<jsp:include page="include.jsp" flush="true"/>
<%
     int accountId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
 	 int userId = EJBUtil.stringToNum(request.getParameter("userId"));
	 
	 String sessUserId =	(String) tSession.getValue("userId");
	 int curUser = EJBUtil.stringToNum(sessUserId);
	 
	 userB.setUserId(userId);
	 userB.getUserDetails(); 
	 
	 if (StringUtil.stringToNum(userB.getUserAccountId()) != accountId) {
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
			<div class = "myHomebottomPanel"> 
			<jsp:include page="bottompanel.jsp" flush="true"/>
			</div>
			</div>
			</body>
			</html>
		<%
			return;
	 } // End of same account check
	 
 	 String  userLastName = userB.getUserLastName();
	 String  userFirstName = userB.getUserFirstName();
	 
	 /////////////////////////////
	  int pageRightMngOrg = 0, modCDRPFseq=0;
	  int pageRightModCdrPf = 0;
	  char cdrpfRight='0';
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	 String modRight = (String) tSession.getValue("modRight");
           int modlen = modRight.length();
	 modCtlDao.getControlValues("module");
	 ArrayList modCtlDaofeature =  modCtlDao.getCValue();
	 ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();
	 
	 modCDRPFseq = modCtlDaofeature.indexOf("MODCDRPF");
	 pageRightModCdrPf = ((Integer) modCtlDaoftrSeq.get(modCDRPFseq)).intValue();
	 cdrpfRight = modRight.charAt(pageRightModCdrPf - 1);
	 
	 pageRightMngOrg = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));

	 	 
	
%>
<%Object[] arguments= {userFirstName,userLastName}; %>
	<P class="sectionHeadings"><%=VelosResourceBundle.getMessageString("M_Usr_MngOrgFor",arguments)%><%--Users >> Manage Organizations for '<%=userFirstName%> <%=userLastName%>'*****--%></P>
<%
	 int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
	 String userSiteFlag = userB.getUserSiteFlag();
	 if (userSiteFlag==null) userSiteFlag="S"; //set this as default
%>   
   <Form name="siteRights" id="siteRightsFrm" method="post" action="updatesiterights.jsp" onSubmit="if (validate(document.siteRights)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
   <input type="hidden" name="userPrimOrg" value=<%=userPrimOrg%>>
   <input type="hidden" name="userId" value=<%=userId%>>   
   <table width="100%" cellpadding="0" cellspacing="0">
   <tr>
   <td>
   <%if (userSiteFlag.equals("A")) {%>
	   <input type="radio" name="siteFlag" value="A" CHECKED onclick="chkAllChild(document.siteRights)"><%=MC.M_UsrAces_AllChildOrgs%><%--User has access to all child organizations*****--%>
   <%}else{%> 
	   <input type="radio" name="siteFlag" value="A" onclick="chkAllChild(document.siteRights)" ><%=MC.M_UsrAces_AllChildOrgs%><%--User has access to all child organizations*****--%>
	<%}%>
   </td>
   </tr>
   <tr>
   <td> 
   <%if (userSiteFlag.equals("S")) {%>
   <input type="radio" name="siteFlag"  value="S" CHECKED><%=MC.M_UsrAces_ToOrg%><%--User has access to only specified organizations*****--%>
    <%}else{%>
   <input type="radio" name="siteFlag" value="S"><%=MC.M_UsrAces_ToOrg%><%--User has access to only specified organizations*****--%>	
	<%}%> 
   </td>
   </tr>
   <input type="hidden" name="cdrpfRight" id="cdrpfRight" value="<%=cdrpfRight%>"/>
   <%if ((String.valueOf(cdrpfRight).compareTo("1") == 0)){ %>
    <tr><td align="right"><a href="manageUserSitesGrps.jsp?userId=<%=userId%>" ><%=MC.M_EditOrgGroupAccess %></a></td></tr>
    <%} %>
   </table>
   <br>
<%   
     int userSiteId = 0;
     int userSiteSiteId = 0;
     int userSiteRight = 0;
     String userSiteName = "";
     int userSiteIndent = 0;
	 int userSiteParent = 0;
  	 String spaces = "";
	 String newChkName = "";
 	 String editChkName = "";
 	 String viewChkName = "";
	 String rowCntName = "";
	 int selRow = 0;
	 boolean primFlag;
	 
	 int primOrgCounter = -1;
	 int normalCounter = -1;
     
     ArrayList userSiteIds = new ArrayList();		
     ArrayList userSiteSiteIds = new ArrayList();	
     ArrayList userSiteRights = new ArrayList();					
     ArrayList userSiteNames = new ArrayList();
     ArrayList userSiteIndents = new ArrayList();
	 ArrayList userSiteParents = new ArrayList();
     	 
     UserSiteDao userSiteDao = userSiteB.getUserSiteTree(accountId,userId);
     userSiteIds = userSiteDao.getUserSiteIds();
     userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
     userSiteNames = userSiteDao.getUserSiteNames();
     userSiteRights = userSiteDao.getUserSiteRights();
     userSiteIndents = userSiteDao.getUserSiteIndents();
     userSiteParents = userSiteDao.getUserSiteParents();	 
     
     int len = userSiteIds.size();
     %>
     <Input type="hidden" name="totalrows" value= <%=len%> >
 	  <table width="100%" cellspacing="0" cellpadding="0">
	  <tr>
	  <TH><%=LC.L_Organization%><%--Organization*****--%></TH>
      <TH><%=LC.L_New%><%--New*****--%></TH>
      <TH><%=LC.L_Edit%><%--Edit*****--%></TH>
      <TH><%=LC.L_View%><%--View*****--%></TH>
	  </tr>
	  <!-- Modified by Amar for Bugzilla issue #3023 -->
	  <tr><td align="left"><input type="checkbox" name="All"  onClick="checkAll(document.siteRights)">  <%=LC.L_SelOrDeSel_All%><%--Select / Deselect All*****--%></td> </tr>
     <%
     for (int counter=0;counter<len;counter++) {
	 //pkUserSiteId
     	userSiteId=EJBUtil.stringToNum(((userSiteIds.get(counter)) == null)?"-":(userSiteIds.get(counter)).toString());
	//fk_site
     	userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(counter)) == null)?"-":(userSiteSiteIds.get(counter)).toString());
     	userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(counter)) == null)?"-":(userSiteRights.get(counter)).toString());
   		userSiteIndent = EJBUtil.stringToNum(((userSiteIndents.get(counter)) == null)?"-":(userSiteIndents.get(counter)).toString());
   		userSiteName = ((userSiteNames.get(counter)) == null)?"-":(userSiteNames.get(counter)).toString();
		userSiteParent = EJBUtil.stringToNum(((userSiteParents.get(counter)) == null)?"-":(userSiteParents.get(counter)).toString());		

		spaces = "";
   		for (int i=1;i<=userSiteIndent;i++) {
   			spaces = spaces + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
   		}   			
   		userSiteName = spaces + userSiteName; //for indentation
		
		//create checkbox names dynamically, 
		//add _P if the site is child of User Primary Organization
		
		if (userPrimOrg == userSiteParent) {
			primFlag = true;
			primOrgCounter ++;
			newChkName = "newr" + "_P";
			editChkName = "edit" + "_P";
			viewChkName = "view" + "_P";						
			rowCntName = "rowCnt" + "_P";
		} else {
			primFlag = false;
			normalCounter ++;
			newChkName = "newr";
			editChkName = "edit";
			viewChkName = "view";
			rowCntName = "rowCnt";		
		}
		
		if (primFlag) selRow = primOrgCounter;
		else selRow = normalCounter;
     %>
	 
     	<tr>
	        <Input type="hidden" name="rights" value=<%=userSiteRight%> >
	        <Input type="hidden" name="pkUserSite" value=<%=userSiteId%> >								
	        <Input type="hidden" name="<%=rowCntName%>" value=<%=counter%> >
			
     		<td><%=userSiteName%></td>
			
			<%if (userSiteRight==7){%>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" CHECKED>
			</td>

		   <%}else if (userSiteRight==1){ %>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>">
			</td>
		   <%}else if (userSiteRight==3) {%>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" >
			</td>
			<%}else if (userSiteRight==2) {%>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" >
			</td>
			<%}else if (userSiteRight==4) {%>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
			</td>
			<%}else if (userSiteRight==5) {%>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
			</td>
			<%}else if (userSiteRight==6) {%>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" CHECKED>
			</td>
			<%}else if (userSiteRight==0) {%>
			<td align="center">
			   <Input type="checkbox" name="<%=newChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=editChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>,<%=primOrgCounter%>,<%=normalCounter%>)" >
			</td>
			<td align="center">
			   <Input type="checkbox" name="<%=viewChkName%>" onclick="changeRights(this,<%=selRow%>,document.siteRights,<%=counter%>),<%=primOrgCounter%>,<%=normalCounter%>" >
			</td>
			<%}%>								
     	</tr>
     <%
     } //end of for
%>	 
<input type="hidden" name="primOrgTotCnt" value=<%=primOrgCounter%>>
<input type="hidden" name="normalTotCnt" value=<%=normalCounter%>>
	
<%if  ( pageRightMngOrg >= 6 )
{ %>
<tr><td width="100%" height="10"></td></tr>
<tr><td width="100%" colspan="5">
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="siteRightsFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td></tr>
<%}%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%



} else {  //else of if body for session

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>

</div>
</body>
</html>
