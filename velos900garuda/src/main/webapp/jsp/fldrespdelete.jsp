<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Fld_ResponseDel%><%--Field Response Delete*****--%> </title>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page import="com.aithent.audittrail.reports.AuditUtils"%>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.fieldLib.impl.FieldLibBean"%><%@page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="fldRespJB" scope="request" class="com.velos.eres.web.fldResp.FldRespJB"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>

<BODY> 
<br>

<DIV class="popDefault" id="div1">
<% 
	String mode= "";
	String fieldLibId="";
	String fieldRespId="";
	String formId="";
	String codeStatus = "";
	String formFldId = "";
	int pageRight=0;
	String creator="";
	String ipAdd="";

	int ret=0;
	HttpSession tSession = request.getSession(true); 
 	if (sessionmaint.isValidSession(tSession))
	{ 	
	  GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	  pageRight = 4;
	  // no relation at-all between rights to Delete response record for 
	  // the field in the Form Library and the rights given for the Field Library.
	  // Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB")); (bug# 2125)
	  if (pageRight >=4)
	  {				
		mode= request.getParameter("mode");
		if (mode==null) mode="initial";
		String calledFrom = request.getParameter("calledFrom");
		String lnkFrom = request.getParameter("lnkFrom");					
		fieldRespId=  request.getParameter("fieldRespId");	
		fieldLibId=request.getParameter("fieldLibId");		
		formId=  request.getParameter("formId");
		codeStatus=  request.getParameter("codeStatus");				
		formFldId=  request.getParameter("formFldId");		
		String studyId = request.getParameter("studyId");
		
%>
		<FORM name="deleteFieldResp" id="delFldResp" method="post" action="fldrespdelete.jsp" onSubmit="if (validate(document.deleteFieldResp)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<br><br>
		<%  if ( mode.equals("initial") )
			{ %>     
				<P class="defComments"><%=MC.M_EsignToProc_RespDelFromFld%><%--Please enter e-Signature to proceed with Response Delete from Field.*****--%></P>
				<jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="delFldResp"/>
						<jsp:param name="showDiscard" value="N"/>
				</jsp:include>
		
				<Input type="hidden" name="mode" value="final" >
				<Input type="hidden" name="fieldRespId" value="<%=fieldRespId%>" >
				<Input type="hidden" name="fieldLibId" value="<%=fieldLibId%>" >
				<Input type="hidden" name="formId" value="<%=formId%>" >
				<Input type="hidden" name="codeStatus" value="<%=codeStatus%>" >
				<Input type="hidden" name="formFldId" value="<%=formFldId%>" >
				<Input type="hidden" name="calledFrom" value=<%=calledFrom%> >	  	  
				<Input type="hidden" name="lnkFrom" value=<%=lnkFrom%> >	
				<Input type="hidden" name="studyId" value=<%=studyId%> >					
																
		</FORM>
		<%  
			}
		else
		{
				String eSign = request.getParameter("eSign");	
				String oldESign = (String) tSession.getValue("eSign");
				if(!oldESign.equals(eSign)) 
				{ 
		%>
	 			  <jsp:include page="incorrectesign.jsp" flush="true"/>	
		<%
				} 
				else 
				{

					creator=(String) tSession.getAttribute("userId");
					ipAdd=(String)tSession.getAttribute("ipAdd");					
					fieldRespId=  request.getParameter("fieldRespId");
						
					
					//JM: bloked the following for #3309

					//fldRespJB.setFldRespId(Integer.parseInt(fieldRespId));					
					//fldRespJB.getFldRespDetails();				
					//fldRespJB.setRecordType("D");
					//fldRespJB.setLastModifiedBy(creator);
					//fldRespJB.setIPAdd(ipAdd);
					//ret =fldRespJB.updateFldResp();

					//JM: 24Jun2008, added for the issue #3309 Need to delete record physically from ER_FLDRESP on deletion. 
                    // Modified for INF-18183 ::: Raviesh
					ret =fldRespJB.deleteFldResp(Integer.parseInt(fieldRespId),AuditUtils.createArgs(session,"",LC.L_Form_Mgmt));	
					
					if (ret==-2) 
					{%>	

						<br><br><br><br><br><br>
	
						<table width=100%>
						<tr>
						<td align=center><p class = "successfulmsg"><%=MC.M_Data_NotDelSucc%><%--Data not deleted successfully.*****--%> </p>
					<%} else 
					{					
					 %>
						<br><br><br><br><br><br>
						<P class="successfulmsg" align="center"><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </P>
												
						<% if (formId.equals("null")) { //called from library %> 
						   <META HTTP-EQUIV=Refresh CONTENT="1; URL=multipleChoiceBox.jsp?fieldLibId=<%=fieldLibId%>&mode=M&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>" >
						<%} else { //called from Form 
							//set the modified by for the formfld so that the last_modified date is triggered 
							//and it updates the repeated responses also
							FieldLibBean fieldLsk = new FieldLibBean();							
							formFieldJB.setFormFieldId(EJBUtil.stringToNum(formFldId));
							formFieldJB.getFormFieldDetails();
							fieldLibId=formFieldJB.getFieldId();							
							fieldLibJB.setFieldLibId(EJBUtil.stringToNum(fieldLibId) );
							fieldLsk = fieldLibJB.getFieldLibDetails();
							fieldLsk.setFormId(formId );
							fieldLsk.setFormFldId(formFldId );
							fieldLsk.setIpAdd(ipAdd);
							fieldLsk.setRecordType("M");
							fieldLsk.setModifiedBy( creator) ;
							fieldLibJB.updateToFormField(fieldLsk);
						
    						//set xsl_refresh flag to 1 for the form so that changes are reflected in the preview mode				
             				formlibB.setFormLibId(EJBUtil.stringToNum(formId)) ;
             				formlibB.getFormLibDetails();             				
             				formlibB.setFormLibXslRefresh("1");
             				formlibB.setRecordType("M");
             				formlibB.setLastModifiedBy( creator) ;
             				formlibB.setIpAdd(ipAdd) ;
             				formlibB.updateFormLib(); %>						
							<META HTTP-EQUIV=Refresh CONTENT="1; URL=multipleChoiceSection.jsp?&mode=M&formId=<%=formId%>&formFldId=<%=formFldId%>&codeStatus=<%=codeStatus%>&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&calledFrom=<%=calledFrom%>&lnkFrom=<%=lnkFrom%>&studyId=<%=studyId%>" >			
						<%}%>
					<%}%>
					</p>
			 			</td>
						</tr>
						<tr height=20></tr>
						<tr>
						<td align=center>
						</td>		
						</tr>		
						</table>				
						<br><br><br><br><br> <p class = "successfulmsg" align = center>  </p>
				<% 
				} //esign %>
		<%
		} //mode 	
 
 	} //pageright
	else

		{

		%>

			<jsp:include page="accessdenied.jsp" flush="true"/>

		<%

		} //end of else body for page right
	
  } //session
 else
	{ %> 
	 	<jsp:include page="timeout.html" flush="true"/> 
 <% } //end session%>	
	 
</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</HTML>


