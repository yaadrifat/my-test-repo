var APPMODE_SAVE ="SAVE";
var APPMODE_ADD="ADD";
var APPMODE_DELETE="DELETE";
var APPMODE_READ="READ";
var errMsgNoRights ='Error: User does not have rights to ';
var switchwf_flag=0;
var loadedDivName = "";
var flagCbuInfo = true;
var flagFinalElig = true;
var flagmIdm = true;
var flagOtherData = true;
var flagCordId = true;
var flagCordProc = true;
var flagHla = true;
var flagHealthHistory = true;
var flagNoteList = true;
var flagLabSummary = true;
var flagmStaticPanel = true;
var completeReq = false;
var	defaultPageNojs = "1";
var eventCode="";
var $j=jQuery;
$j.ajaxSetup({ cache: false });



function checkAppRights(rights,mode){
    if(mode == APPMODE_READ && rights.indexOf("R") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_ADD && rights.indexOf("A") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_SAVE && rights.indexOf("M") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }else if(mode == APPMODE_DELETE && rights.indexOf("D") == -1 ){
			alert(errMsgNoRights + mode);
			return false;      		
    }
   
    return true;
}


//JavaScript Document
//code inside this will happen after page is loaded  
/*$(document).ready(function(){     
// find element with ID of "target" and put file contents into it
	//alert(" script.js ready");
	
	
	$('#header').load('html/inc_header.html');
	$('#breadcrumb').load('html/inc_breadcrumb.html');
	
	$('#footer').load('cb_inc_footer.jsp');
	
	

});
*/
/**** For showing add widget modal  *********/

function showAddWidgetModal(url)
{
	var position = $j("#widgetId").position();
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result, status, error){
	        	
	        	$j("#modelPopup1").html(result);
	        	$j("#modelPopup1").dialog(
	     			   {autoOpen: true,
	     				title: 'Add Widgets',
	     				closeText: '',
						closeOnEscape: false ,
	     				position: [(position.left-300),(position.top+25)],
	    				modal: true, width:411, height:200,
	     				close: function() {
	     					//$j(".ui-dialog-content").html("");
	     					//jQuery("#subeditpop").attr("id","subeditpop_old");
	     	        		jQuery("#modelPopup1").dialog("destroy");
	     			    }
	     			   }
	     			  );        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}
		
/*****  Add widget functions            ******/

function exportData(tableId){
	// alert("export -> " + tableId);
	 $j('#'+tableId).table2CSV({
		separator : '\t'
		});
}

function showData(data){
	//alert("showData")
	//alert("data " + data);
	//window.location='data:application/vnd.ms-excel;charset=utf8,' + encodeURIComponent(data) + ', #Content-Disposition:filename=export.xls,';	
	
	window.location='data:application/vnd.ms-excel;charset=utf8;Content-Type=application/vnd.ms-excel;Content-Disposition:attachment;filename=export.xls,' + encodeURIComponent(data) ;
 return true;
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function clearFields(){
	$j('#username').val("");
	$j('#password').val("");
	$j("#errormessagetd").html("");
}
function updateBreadcrumb(dispname,link ){
	//alert("update bread");
	if( $j('#breadcrumblink').text().indexOf( dispname ) == -1 ){

		$j('#breadcrumblink').append("&nbsp;&gt;&gt; <a  href='#' onclick='"+link+"';>" +dispname +"</a>");
	}
}
 
function changeBreadcrumb(pagename){
	var breadcrumbVal = '&nbsp;&nbsp;<a href="#" onclick="javascript:showHighlight(\'mnuHome\');checkLogin();" >Home </a>';
	if(pagename=="volunteer"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >New Volunteer</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="respondent"){
		showHighlight('mnuResp');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getRespondents\');">Respondents</a>&nbsp;';
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >Edit Respondent</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="editVolunteer"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getVolunteerlst\');">Volunteers</a>&nbsp;';
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" >Edit Volunteer</a>&nbsp;';
		$j('#breadcrumblink').html(breadcrumbVal);
	}else if(pagename=="Volunteers"){
		showHighlight('mnuVol');
		breadcrumbVal += '&nbsp;&nbsp;&gt;&gt;&nbsp;<a href="#" onclick="loadPage(\'getVolunteerlst\');">Volunteers</a>&nbsp;';		
		$j('#breadcrumblink').html(breadcrumbVal);
	}
	
	
}



function updateDisplayName(type, name){
	//alert("update user sel");
	$j("#pageType").val(type);
	$j("#pageName").val(name);
	var userSelection = type + " : " + name;
	$j('#userSelection').html(userSelection);
}


function getHome(){
	$j('#header').load('cb_inc_blankheader.jsp');
	
	$j('#header').removeClass('headerStyle').addClass('blankheader');
	$j('#nav').css('height','32px');
	$j('#nav').load('cb_inc_nav.jsp');
	$j('#breadcrumb').load('html/inc_breadcrumb.html');
	$j('#footer').load('cb_inc_footer.jsp');
	$j('#menu').removeClass('hidden').addClass('visible');
	
	$j('#breadcrumb').removeClass('hidden').addClass('visible');
	$j('#accordion').removeClass('hidden').addClass('visible');
	$j('#div_accordion').removeClass('col_0').addClass('col_15');
	$j('#div_main').removeClass('col_100').addClass('col_85');
	
	loadPage('home');
	$j('.jclock').jclock();
}
function forgotpassword(){
	$j("#errormesssagetd").html("");
	var data = "forgotuserName="+$j("#forgotuserName").val();+"&email="+$j("#email").val();
	//alert("context path " + contextpath);
	$j.ajax({
		type: "POST",
		url : "getforgotpassword",
		data : $j("#forgotform").serialize(),
		success : function(data) {
			$j("#errormesssagetd").html(data.errorMessage);
		},
		error:function() { 
			alert("Error ");
		}
		
	});
	return false;
}/*
function checkLogin(){
	//alert("check login");
	if($('#username').val()!= "" && $('#password').val()!=""){
		//alert(" inside if");
		var errorMessage = "";
		var navHtml="";
		var userNavRight = "";
		var errorMessage = validateUserDetails("getLoginDetails?userName="+$('#username').val()+"&password="+$('#password').val());
		var userNavDetails = readXml1("navigation.xml");
		if(errorMessage != "" && errorMessage!=null)
		{
		alert(errorMessage);
		}
		if(errorMessage == "" || errorMessage==null){  
			if($('#username').val()=="pfuser")
			{
				userNavRight = 2;
			}else if($('#username').val()=="cdruser")
			{
				userNavRight = 4;
			}else if($('#username').val()=="reviewuser")
			{
				userNavRight = 16;
			}else if($('#username').val()=="dataentryuser")
			{
				userNavRight = 8;
			}	
			navHtml += '<ul id="accordion" >';			
			$(userNavDetails).find("heading").each(function() {
				if((parseInt($(this).find("userright").text())& userNavRight) > 0){
					navHtml += '<li><a href="#"><span class="ui-icon ui-icon-triangle-1-e"></span>'+$(this).find("headingname").text()+'</a><ul>';
					$(this).find("nav").each(function(){
						navHtml += '<li><a href="#" onclick="javascript:showHighlight(\''+$(this).find("link").text()+'\');loadPage(\''+$(this).find("link").text()+'\')">'+$(this).find("navname").text()+'</a></li>';
					});
					navHtml += '</ul></li>';
				}
			});
			navHtml += "</ul>";
			
			
			$('#header').load('cb_inc_blankheader.jsp');
	
			$('#header').removeClass('headerStyle').addClass('blankheader');
			$('#nav').css('height','32px');
			$('#nav').load('cb_inc_nav.jsp');
			$( "button,a", "#addWidgetTable" ).button();			 
			$('#breadcrumb').load('html/inc_breadcrumb.html');
			$('#menu').removeClass('hidden').addClass('visible');
			$('#footer').load('cb_inc_footer.jsp');
			
			$('#breadcrumb').removeClass('hidden').addClass('visible');
			$('#leftNav').html(navHtml);	
			$( "input:submit, a, button", "#accordion" ).button();
			//$('#accordion').removeClass('hidden').addClass('visible');
			//$( "#accordion" ).accordion();
			//$( "#accordion" ).removeClass('ui-widget ui-helper-reset');			
			$('#div_accordion').removeClass('col_0').addClass('col_15');
			$('#accordion ul').hide();
			$('#accordion li a ').click(

				    function() {
					    $(this).next().slideToggle('normal');               
					    $(this).find(".ui-icon").each(function(){
					    	if($(this).is(".ui-icon-triangle-1-s")){
					    		$(this).removeClass("ui-icon-triangle-1-s").addClass("ui-icon-triangle-1-e");
					    	}
					    	else if($(this).is(".ui-icon-triangle-1-e")){
					    		$(this).removeClass("ui-icon-triangle-1-e").addClass("ui-icon-triangle-1-s");
					    	}
					    });					   

				    });
			
			
			
	$('#div_main').removeClass('col_100').addClass('col_80');
		//	loadPage('home');
			
			//$('#main').html(" ");
			
			//$('#footer').load('html/inc_footer.html');
			//alert("wait");
		//	 $('.jclock').jclock();
		}else{
			$("#errormessagetd").html(errorMessage);
			return false;
		}	
	}
	 $('#fimg').removeClass('hidden').addClass('visible');
	return true;
	//alert(" end of check ");
}
 */
function validateUserDetails(url){
	//alert("validateUserDetails ");
	var errorMessage = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert(result);
	        	//alert(result.errorMessage);
	        	errorMessage = result.errorMessage;
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
	 return errorMessage;
}

/*this method is for check value exist in database or not*/
function validateDbData(url){
	if(url.indexOf("null")==-1){
		var dbValue;
		$j.ajax({
		        type: "GET",
		        url: url,
		        async:false,
		        success: function (result){			     
			       dbValue = result.dbValue;			     
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		            alert(request.responseText);
		        }
	
			});
		return dbValue;
	}else{
		return true;
	}
}

function validateDbAdditionalData(url){
	var flag = true;
	if(url.indexOf("null")==-1){
		var dbValue;
		$j.ajax({
		        type: "GET",
		        url: url,
		        async:false,
		        success: function (result){
			       dbValue = result.dbValue;
			       if(dbValue == true){
			    	   flag = false;
			       }
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		            alert(request.responseText);
		        }
	
			});
		 return flag;
	}else{
		return flag;
	}
}


function validateValues(id1,id2){
	
	var val1 = $j("#"+id1).val();
	var val2 = $j("#"+id2).val();	
	
	return ((val1==val2)?false:true);
}
/*-----*/

function showPage(frmName){
	var pageName = frmName;
	$j('.ui-dialog').html("");
	$j('.ui-datepicker').html("");
	$j('.tabledisplay').html("");
	$j('#main').html("");
	$j('.tabledisplay').html("");
	$j('#main').load(pageName);
}

function showModal(title,url,height,width)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
	if($j('#modelPopup1').html()==null){
		var obj="<div id='modelPopup1'></div>";
		$j('body').append(obj);
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup1").dialog("destroy");
				    }
				   }
				  ); 
		$j('div.ui-dialog-titlebar').css('height', '35px');
		$j("#modelPopup1").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup1").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
	resetSessionWarningOnTimer();
}

function showModalWithData(title,url,height,width,formId,divname)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
		$j("#"+divname).html(progressMsg);
		$j("#"+divname).dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height					
				   }
				  ); 
		$j("#"+divname).dialog("open");
		$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result, status, error){
        	$j("#"+divname).html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
	resetSessionWarningOnTimer();
}


function showModalOne(title,url,height,width)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup2").html(progressMsg);
		$j("#modelPopup2").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup2").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup2").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup2").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}


function closeModals(divname){
	var flag;
	if(divname=="dialogForCRI"){
		flag=$j("#dialogForCRI").find("#criChangeFlag").val();
	}
	if(divname=="dialogForCRI" && flag=="true"){
		setTimeout(function(){
			showprogressMgs();
		},0);
		setTimeout(function(){
			var cordid = $j("#dialogForCRI").find("#cordId").val();
			var orderid = $j("#dialogForCRI").find("#criOrderId").val();
			var ordertype = $j("#dialogForCRI").find("#criOrderType").val();
			if(orderid != null && orderid != "" && typeof(orderid) != "undefined" && ordertype != null && ordertype != "" && typeof(ordertype) != "undefined"){
				var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
				loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
				setpfprogressbar();
				getprogressbarcolor();
			}else{
				var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
				loadPageByGetRequset(urla,'cordSelectedData');
			}
			var div = document.getElementById(divname);
			var divPar = div.parentNode;
				divPar.parentNode.removeChild(divPar);
		},100);
		setTimeout(function(){
			closeprogressMsg();
		},0);
	}else{
		if(divname=="dialogForCRI"){
			$j("#dialogForCRI").find("#criChangeFlag").val("");
		}
		var div = document.getElementById(divname);
		var divPar = div.parentNode;
			divPar.parentNode.removeChild(divPar);
	}
}
function maximizePage(){
	/*
	window.moveTo(0,0);
	if($j.browser.msie){
		window.resizeTo(screen.availWidth-3, screen.availHeight);
	}else{
		window.resizeTo(screen.availWidth-10, screen.availHeight-10);
	}
	//window.resizeTo(screen.availWidth, screen.availHeight);
    */
}
function hideDataDiv(){
	$j('#wrapper').scroll(function(){

		if($j('.dataEnteringDiv').is(':visible') || $j('.datepic').is(':visible')){
			$j('.dataEnteringDiv').hide();
			$j('.datepic').datepicker('hide');
		}
	});
}
function showModals(title,url,height,width,divname,maximize,minimize,sidex,sidey){
	
	//$j('.progress-indicator').css( 'display', 'block' );
	if(maximize==null || maximize=="" || maximize==undefined){
		maximize = false;
	}
	if(minimize==null || minimize=="" || minimize==undefined){
		minimize = false;
	}
	
	//$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height=="" || height==undefined){
		height = 650;
	}
	if(width==null || width=="" || width==undefined){
		width = 750;
	}
	if(divname=='MRQModalForm' || divname=='IDMModalForm'||divname=='FMHQModalForm'||divname=='cbuMinimumCriteria'||divname=='dialogForCRI'){
		var formArry='MRQModalForm,IDMModalForm,FMHQModalForm';
			formArry = formArry.split(',');
		var formsName = "";
		for(var i=0;i<formArry.length;i++){
			if($j("#"+formArry[i]).html()!=null){
				formsName = formsName+formArry[i];
			}
		}
		if(formsName!="" && formsName!=null){
			jConfirm('You are trying to open more than one form. In this case previous form will close and you will lose the data. Do you want to proceed?', 'Confirm',
					function(r) {
						if (r == false) {			         						
						}else if(r == true){
							formsName = formsName.split(",");
							for(var i=0;i<formsName.length;i++){
								closeModals(formsName[i]);
							}
								showModalscontent(title,url,height,width,divname,maximize,minimize,sidex,sidey);
								if(divname=='cbuMinimumCriteria'){
									$j('#ui-dialog-title-cbuMinimumCriteria').width('95%');
								}else if(divname=='dialogForCRI'){
									$j('#ui-dialog-title-dialogForCRI').width('98%');
								}
						}
					});	
		}else{
			showModalscontent(title,url,height,width,divname,maximize,minimize,sidex,sidey);
		}
	}else{
		showModalscontent(title,url,height,width,divname,maximize,minimize,sidex,sidey);
	}
}

function showModalscontent(title,url,height,width,divname,maximize,minimize,sidex,sidey){
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img class=\"asIsImage\" src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	if($j('#'+divname).html()==null){
			var obj="<div id='"+divname+"'></div>";
			$j('body').append(obj);
		}
	$j("#"+divname).html(progressMsg);
	$j("#"+divname).dialog({
				   autoOpen: false,
				   title: title,
				   resizable: true,
				   closeText: '',
				   closeOnEscape: false ,
				   modal: false, 
				   width : width,
				   height : height,
				   position: "center",
				   open: function (event, ui) {
					   if(sidex!='' && sidex!=null && sidex!='undefined'&& sidex=='x'){
								$j("#"+divname).css('overflow-x', 'hidden');
							  }
					   if(sidey!='' && sidey!=null && sidey!='undefined'&&sidey=='y'){
							$j("#"+divname).css('overflow-y', 'hidden');
						  }
					   },
				   close: function() {
					   var flag;
						if(divname=="dialogForCRI"){
							flag=$j("#dialogForCRI").find("#criChangeFlag").val();
						}
					   if(divname=="dialogForCRI" && flag=="true"){
						   setTimeout(function(){
						  		showprogressMgs();
						  	},0);
							setTimeout(function(){
									var cordid = $j("#dialogForCRI").find("#cordId").val();
									var orderid = $j("#dialogForCRI").find("#criOrderId").val();
									var ordertype = $j("#dialogForCRI").find("#criOrderType").val();
									if(orderid != null && orderid != "" && typeof(orderid) != "undefined" && ordertype != null && ordertype != "" && typeof(ordertype) != "undefined"){
										var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
										loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
										setpfprogressbar();
										getprogressbarcolor();
									}else{
										var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
							     		loadPageByGetRequset(urla,'cordSelectedData');
							        }
								var div = document.getElementById(divname);
								var divPar = div.parentNode;
								    divPar.parentNode.removeChild(divPar);
							},100);
							setTimeout(function(){
								closeprogressMsg();
							},0);
					   }else{
						   var div = document.getElementById(divname);
						   var divPar = div.parentNode;
							   divPar.parentNode.removeChild(divPar);
					   }
				    }
			   }).dialogExtend({
			      "maximize" : maximize,
			      "minimize" : minimize,
			      "dblclick" : "collapse"
			   });
			$j("#"+divname).dialog("open");
			$j.ajax({
		        type: "POST",
		        url: url,
		       // async:false,
		        success: function (result, status, error){
			       $j("#"+divname).html(result);
		        },
		        error: function (request, status, error) {
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});
		//	$j('.progress-indicator').css( 'display', 'none' );	
			resetSessionWarningOnTimer();
	}

var cbu_characteristics="";
var cbu_charcteristicflag=true;

var cbu_id="";
var cbu_idflag=true;

var cbu_labsummary="";
var cbu_labsummaryflag=true;

var cbu_procinfo="";
var cbu_procinfoflag=true;

var cbu_eligibility="";
var cbu_eligibilityflag=true;

var cbu_hla="";
var cbu_hlaflag=true;

var cbu_idm="";
var cbu_idmflag=true;

var cbu_healthistory="";
var cbu_healthistoryflag=true;

var cbu_othertestresult="";
var cbu_othertestresultflag=true;

var cbu_clinicalnotes="";
var cbu_clinicalnotesflag=true;

var cbu_history="";
var cbu_historyflag=true;

var htmresult="";
var temp="";
var mode="";
function maximizeScreen(id,cordId,element,titleTxt){
	titleTxt = $j(element).text()+" "+titleTxt;
	showprogressMgs();
	if(!flagFinalElig){
		flagFinalElig=true;
		mode="maximize";
	}	
	populateVarData(id,cordId,element);
	var result = getWidgetVar(id);
	var str="<form>";
	var str1="</form>";
	var res = str + result + str1;
	$j("#modelPopup1").html(res);
	$j("#modelPopup1").find('.ui-icon').each(function(){
		 if($j(this).is(".ui-icon-triangle-1-s")){
  		    $j(this).removeClass("ui-icon-triangle-1-s");
  	     }
		 if($j(this).is(".ui-icon-print")){
	    	$j(this).removeClass("ui-icon-print");
	     }
		 if($j(this).is(".ui-icon-newwin")){
	    	$j(this).removeClass("ui-icon-newwin");
	     }
		 if($j(this).is(".ui-icon-plusthick")){
	    	$j(this).removeClass("ui-icon-plusthick");
	     }
		 if($j(this).is(".ui-icon-triangle-1-e")){
	    	$j(this).removeClass("ui-icon-triangle-1-e");
	     }
		 if($j(this).is(".ui-icon-minusthick")){
	    	$j(this).removeClass("ui-icon-minusthick");
	     }
        $j(this).removeClass("ui-icon");		 
	});
	
	$j("#modelPopup1").find('div').each(function(){
		$j(this).removeAttr("onclick");
	});
	closeprogressMsg();
	$j("#modelPopup1").dialog(
     			   {autoOpen: false,
     				resizable: true,
     				closeText: '',
     				title:titleTxt,
					closeOnEscape: false ,
     				modal: true,width : 750, height : 650,
     				close: function() {     					
     	        		jQuery("#modelPopup1").dialog("destroy");
     			    }
     			   }
     			  ); 
	$j('div.ui-dialog-titlebar').css('height', '35px');
	$j("#modelPopup1").dialog("open");
      if(id=="historydiv"){
	    $j("#modelPopup1").find(".cbu-histry").attr("id","cbu-historyid");
	    $j("#modelPopup1").find(".cbu-req-history").attr("id","cbu-req-historyid");
	   }
	   if(id=="eligibilityInfo"){
    	  disableFormFlds('modelPopup1');
  	  }      
}
function populateVarData(id,cordId,element){
	var ParamObj={
			  maxwidget:"maxwidget",
			  widgetid:""					   
	     }
	switch(id){
	      case "cbuCharacteristics":
	            if(flagCbuInfo){
		           ParamObj.widgetid="cbuCharacteristics";
		           loadClinicalSubModuleData(cordId,'1','loadCbuCharacteristicDiv','cbuCharacteristicsparent','50','',ParamObj);
	             }
	            break;
	      case "cbuIds":
	            if(flagCordId){
		            ParamObj.widgetid="cbuIds";
		            loadClinicalSubModuleData(cordId,'5','loadCordIdsDataDiv','cbuIdsparent','2','',ParamObj);
	            }
	            break;
	      case "labsummarydiv":
	            if(flagLabSummary){
		            ParamObj.widgetid="labsummarydiv";
		            loadClinicalSubModuleData(cordId,'10','loadLabSummaryDiv','labsummarydivparent','98','',ParamObj);
	             }
	            break;
	      case "cbuprocessinginfodiv":      
	            if(flagCordProc){
		           ParamObj.widgetid="cbuprocessinginfodiv";
		           loadClinicalSubModuleData(cordId,'6','loadProcessingProcedureDataDiv','cbuprocessinginfodivparant','554','',ParamObj);
	            }
	            break;
	      case "eligibilityInfo":      
	           if(flagFinalElig){
		          ParamObj.widgetid="eligibilityInfo";
		          loadClinicalSubModuleData(cordId,'2','loadEligibilityDiv','eligibilityInfoparent','130','',ParamObj);
	           }
	           break;
	      case "cbuHla":     
	           if(flagHla){
		        ParamObj.widgetid="cbuHla";
		        loadClinicalSubModuleData(cordId,'7','loadHlaDataDiv','cbuHlaparent','38','',ParamObj);
	           }
	           break;
	      case "infectDisease":    
	            if(flagmIdm){
		          ParamObj.widgetid="infectDisease";
		          loadClinicalSubModuleData(cordId,'3','loadIdmDataDiv','infectDiseaseparent','290','8',ParamObj);
	             }
	            break;
	      case "healthHistory":     
	            if(flagHealthHistory){
		           ParamObj.widgetid="healthHistory";
		           loadClinicalSubModuleData(cordId,'8','loadFormsDataDiv','healthHistoryparent','290','6',ParamObj);
	             }
	             break;
	      case "otherTestResult":       
	            if(flagOtherData){
		        ParamObj.widgetid="otherTestResult";
		        loadClinicalSubModuleData(cordId,'4','loadOthersDataDiv','otherTestResultparent','2','',ParamObj);
	           }
	            break;
	      case "clinicalNotes":     
	           if(flagNoteList){
		          ParamObj.widgetid="clinicalNotes";
		          loadClinicalSubModuleData(cordId,'9','loadWholeNotesDiv','clinicalNotesparent','2','',ParamObj)
	             }
	            break;
	      case "historydiv": 	 		  
		        if(element.find('.ui-icon').is(".ui-icon-plusthick")){
		           document.getElementById("cbuhistoryspan").click();
		           cbu_history=$j("#"+id).html();
		           document.getElementById("cbuhistoryspan").click();
		        }else{
			       cbu_history=$j("#"+id).html();
		        }	 
	           break;
	       default:
	    	   if(element.find('.ui-icon').is(".ui-icon-plusthick")){
	    		  var elementId=element.find('.ui-icon-plusthick').attr('id').toString();
		           document.getElementById(elementId).click();
		           temp=$j("#"+id).html();
		           document.getElementById(elementId).click();
		        }else{
			       temp=$j("#"+id).html();
		        }
	       break;
	}
	
}
function getRequestedPage(url){
	var temp="";	
		$j.ajax({
	        type: "GET",
	        url: url,
	        async:false,
	        success: function (result){      	
	        	temp=result;
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});	
	return temp;	 
}
function setWidgetVar(widget_id,url){
	switch(widget_id){
	    case "cbuCharacteristics":
	    	cbu_characteristics=getRequestedPage(url);
	    	break;
	    case "cbuIds":
	    	cbu_id=getRequestedPage(url);
	    	break;
	    case "labsummarydiv":
	    	cbu_labsummary=getRequestedPage(url);
	    	break;
	    case "cbuprocessinginfodiv":
	    	cbu_procinfo=getRequestedPage(url);
	    	break;
	    case "eligibilityInfo":	
	    	cbu_eligibility=getRequestedPage(url);
	    	break;
	    case "cbuHla":
	    	cbu_hla=getRequestedPage(url);
	    	break;
	    case "infectDisease":
	    	cbu_idm=getRequestedPage(url);
	    	break;
	    case "healthHistory":
	    	cbu_healthistory=getRequestedPage(url);
	    	break;
	    case "otherTestResult":
	    	cbu_othertestresult=getRequestedPage(url);
	    	break;
	    case "clinicalNotes":
	    	cbu_clinicalnotes=getRequestedPage(url);
	    	break;
	}	
}
function getWidgetVar(id){
	    switch(id){
	    case "cbuCharacteristics":
	    	return cbu_characteristics;
	    	break;
	    case "cbuIds":
	    	return cbu_id;
	    	break;
	    case "labsummarydiv":
	    	return cbu_labsummary;
	    	break;
	    case "cbuprocessinginfodiv":
	    	return cbu_procinfo;
	    	break;
	    case "eligibilityInfo":
	    	return cbu_eligibility;
	    	break;
	    case "cbuHla":
	    	return cbu_hla;
	    case "infectDisease":
	    	return cbu_idm;
	    	break;	
	    case "healthHistory":
	    	return cbu_healthistory;
	    	break;
	    case "otherTestResult":
	    	return cbu_othertestresult;
	    	break;
	    case "clinicalNotes":
	    	return cbu_clinicalnotes;
	    	break;
	    case "historydiv":
	    	return cbu_history;
	    	break;
	    default:
	    	return temp;
	        break;
	    }	
}
/* to set corresponding widget variables*/
function setWidgetVarData(divName){
	switch(divName){
	case "loadCbuCharacteristicDiv":
		cbu_characteristics=htmresult;		
		break;
	case "loadCordIdsDataDiv":
		cbu_id=htmresult;		
		break;
	case "loadLabSummaryDiv":
		cbu_labsummary=htmresult;		
		break;
	case "loadProcessingProcedureDataDiv":
		cbu_procinfo=htmresult;
		break;
	case "loadEligibilityDiv":
		cbu_eligibility=htmresult;		
		break;
	case "loadHlaDataDiv":
		cbu_hla=htmresult;		
		break;
	case "loadIdmDataDiv":
		cbu_idm=htmresult;		
		break;
	case "loadFormsDataDiv":
		cbu_healthistory=htmresult;		
		break;
	case "loadOthersDataDiv":
		cbu_othertestresult=htmresult;		
		break;
	case "loadWholeNotesDiv":
		cbu_clinicalnotes=htmresult;		
		break;	
	}
	htmresult="";
}
/* expanding widget handler*/
function loadWidget(divName){
	     if(cbu_charcteristicflag && divName==="loadCbuCharacteristicDiv"){
	    	 $j("#"+divName).html(cbu_characteristics);
	    	 cbu_charcteristicflag=false;
	     }
	     else if(cbu_idflag && divName==="loadCordIdsDataDiv"){
	    	 $j("#"+divName).html(cbu_id);
	    	 cbu_idflag=false;
	     }
	     else if(cbu_idflag && divName==="loadLabSummaryDiv"){
	    	 $j("#"+divName).html(cbu_labsummary);
	    	 cbu_labsummaryflag=false;
	     }
	     else if(cbu_procinfoflag && divName==="loadProcessingProcedureDataDiv"){
	    	 $j("#"+divName).html(cbu_procinfo);
	    	 cbu_procinfoflag=false;
	     }
	     else if(cbu_eligibilityflag && divName==="loadEligibilityDiv"){
	    	 $j("#"+divName).html(cbu_eligibility);
	    	 cbu_eligibilityflag=false;
	     }
	     else if(cbu_hlaflag && divName==="loadHlaDataDiv"){
	    	 $j("#"+divName).html(cbu_hla);
	    	 cbu_hlaflag=false;
	     }
	     else if(cbu_idmflag && divName==="loadIdmDataDiv"){
	    	 $j("#"+divName).html(cbu_idm);
	    	 cbu_idmflag=false;
	     }
	     else if(cbu_healthistoryflag && divName==="loadFormsDataDiv"){
	    	 $j("#"+divName).html(cbu_healthistory);
	    	 cbu_healthistoryflag=false;
	     }
	     else if(cbu_othertestresultflag && divName==="loadOthersDataDiv"){
	    	 $j("#"+divName).html(cbu_othertestresult);
	    	 cbu_othertestresultflag=false;
	     }
	     else if(cbu_clinicalnotesflag && divName==="loadWholeNotesDiv"){
	    	 $j("#"+divName).html(cbu_clinicalnotes);
	    	 cbu_clinicalnotesflag=false;
	     }	     
}
/*
function loadPage(url){  
	   // alert(" url " + url);
	$('.progress-indicator').css( 'display', 'block' );
	 $.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert("test" + result);
	        	//showPage(pageName);
	        	//$(".ui-dialog-content").html("");
	        	$('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	$('.tabledisplay').remove();
	        	$('#main').html(result);
	        	valid8();
	        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
		$('.progress-indicator').css( 'display', 'none' );
}*/
// save Modal form

function modalFormSubmit(url,formId)
{
	$j('.progress-indicator').css( 'display', 'block' );
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#modelPopup1").html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}

//Load GET Request by Mohiuddin

function loadPageByGetRequset(url,divname){  
	/*$j('#progress-indicator').css( 'display', 'block' );  
	   var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		
		$j("#progress-indicator").html(progressMsg);
			$j("#progress-indicator").dialog(
					   {autoOpen: false,
						resizable: false,
						closeText: '',
						closeOnEscape: false ,
						modal: true, width : 300, height : 100,
						close: function() {
							//$(".ui-dialog-content").html("");
							//jQuery("#subeditpop").attr("id","subeditpop_old");
			      		jQuery("#progress-indicator").dialog("destroy");
					    }
					   }
					  ); 
		$j("#progress-indicator").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
		$j("#progress-indicator").dialog("open");*/
	
	$j('.progress-indicator').css( 'display', 'block' );
	//showprogressMgs();
	$j.ajax({
	        type: "GET",
	        url: url,
	        //async:false,
	        async:false,
	        success: function (result){
	        	//alert("test" + result);
	        	//showPage(pageName);
	        	//$(".ui-dialog-content").html("");
		    	 $j('.ui-datepicker').html("");
		     	//$('.tabledisplay').html("");
		         var $response=$j(result);
		         //query the jq object for the values
		         htmresult = result;
		         //alert("Inside loadPageByGetRequset"+htmresult);
		         var errorpage = $response.find('#errorForm').html();
		       // alert(errorpage);
		        // var subval = $response.find('#sub').text();
		         if(errorpage != null &&  errorpage != "" ){
		         	$j("#main").html(result);
		         }else{
		        	 closeprogressMsg();
		         	$j("#"+divname).html(result);
		         	
		       }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
	//closeprogressMsg();
	$j('.progress-indicator').css( 'display', 'none' );
	return htmresult;
}

//========== function added for getting progress bar while opening widget========

function loadwidgetByGetRequset(url,divname){  
showprogressMgs();
$j.ajax({
        type: "GET",
        url: url,
        async:false,
        //async:true,
        success: function (result){
	    	 $j('.ui-datepicker').html("");
	         var $response=$j(result);
	         //query the jq object for the values
	         htmresult = result;
	         var errorpage = $response.find('#errorForm').html();
	         if(errorpage != null &&  errorpage != "" ){
	         	$j("#main").html(result);
	         }else{
	        	 closeprogressMsg();
	         	$j("#"+divname).html(result);
	         	
	       }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }

	});

//$j('.progress-indicator').css( 'display', 'none' );
return htmresult;
}


function loadPageByGetRequsetFromCordEntry(url,divname){  
	   // alert(" url " + url); 
	$j('.progress-indicator').css( 'display', 'block' );
	$j.ajax({
	        type: "GET",
	        url: url,
	        async:false,
	        success: function (result){
	        	//alert("test" + result);
	        	//showPage(pageName);
	        	//$(".ui-dialog-content").html("");
		    	 $j('.ui-datepicker').html("");
		     	//$('.tabledisplay').html("");
		         var $response=$j(result);
		         //query the jq object for the values
		         var errorpage = $response.find('#errorForm').html();
		       // alert(errorpage);
		        // var subval = $response.find('#sub').text();
		         if(errorpage != null &&  errorpage != "" ){
		         	$j("#main").html(result);
		         }else{
		         	$j("#"+divname).html(result);
		       }
	        	
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
	$j('.progress-indicator').css( 'display', 'none' );
}

//Load Post Form by Mohiuddin
function loadDivWithFormSubmit(url,divname,formId){
/*	$j('#progress-indicator').css( 'display', 'block' );  
   var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#progress-indicator").html(progressMsg);
		$j("#progress-indicator").dialog(
				   {autoOpen: false,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : 300, height : 100,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#progress-indicator").dialog("destroy");
				    }
				   }
				  ); 
	$j("#progress-indicator").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
	$j("#progress-indicator").dialog("open");*/
	showprogressMgs();
	if($j("#"+formId).valid()){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	}
	closeprogressMsg();
}

//Load Particular div
function loaddiv(url,divname){  
	//$j('.progress-indicator').css( 'display', 'block' );
	showprogressMgs();
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//$('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
		//$j('.progress-indicator').css( 'display', 'none' );
	 closeprogressMsg();
}
//load more div
function loadmultiplediv(url,divname){ 
	showprogressMgs();
	var divnamearr = divname.split(",");
	var newdivname = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	//$('.ui-dialog').html("");
	        	//$('.ui-datepicker').html("");
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{ 
		        	for(i=0;i<divnamearr.length;i++) {
		        		newdivname = divnamearr[i];
		        		$j("#"+newdivname).replaceWith($j('#'+newdivname, $j(result)));
		        	}
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
	 closeprogressMsg();
}

function refreshDiv(url,divname,formId){
	//$('.progress-indicator').css( 'display', 'block' );
	showprogressMgs();
	if($j("#"+formId).valid()){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{            	
            	if(divname==="searchTblediv"){
            		$j("#"+divname).html(result);
            	}else{
            		$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
            	}
            	if(typeof procesingInfoElements !="undefined"){
         	       constructTable();
            	}            	
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	closeprogressMsg();
  }
	
}





/*window.onload=function(){
var formref=document.getElementById("switchform")
indicateSelected(formref.choice)
}*/


function showCallQueue(){
	//alert("showCallQueue");
	$j('#tdshowid').html($j('#callQueue').html())
}
function showResQueue(){
	$j('#tdshowid').html($j('#tblresp').html())
	
}

function showusecase(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_usecase.jpg";
			
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showwireframe(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_wireframe.jpg";
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showassdoc(){
	if(typeof(document.getElementById("formname"))!=undefined){
		var frmName = document.getElementById("formname").value;
		if(frmName != "" ){
			url = "images/" + frmName +"_assdoc.jpg";
			window.open(url);	
		}
	}else{
		alert("To Be Developed");	
	}
}

function showhelp(){
	alert("To Be Developed");	
}

function showdetails(){
	document.getElementById("editrespondent").style.visibility="visible";
	}
	
function showcontact() {
	document.getElementById("contactactivity").style.visibility="visible";
	document.getElementById("recruitactivity").style.visibility="hidden";
	}
	
	
function showrecruit() {
	document.getElementById("contactactivity").style.visibility="hidden";
	document.getElementById("recruitactivity").style.visibility="visible";
	
	}

function showsearchValues(){
	if(document.getElementById("searchVal").innerHTML == ""){
		document.getElementById("searchVal").innerHTML = 	'<a href="#" onclick="removeSearch()" >Remove All</a>' ;
	}
	document.getElementById("searchVal").innerHTML = document.getElementById("searchVal").innerHTML  + '<a href="#">' + document.getElementById("searchType").value +":" + 	document.getElementById("name").value + '<br /></a>';
	
}

function removeSearch(){
	document.getElementById("searchVal").innerHTML = "";
}

function showMedications(){
	document.getElementById("listMedications").style.display="block";
}

function readXml1(fileName){
	// alert(fileName);
	var resXml = "";
	var pathname = window.location.pathname;
	//alert("pathname" + pathname);
	 var fileurl = "xml/"+fileName;
	
	//alert("fileurl " + fileurl);
	 $j.ajaxSetup({
         url: fileurl
      });
	 $j.ajax({
	        type: "GET",	      
	        dataType: "xml",
	        async:false,
	        cache:false,
	        success: function(xml){
	        	resXml = xml;
	        },
			complete: function( xhr, status ){
				//alert("status "  +status);
				if( status == 'parsererror' ){
					xmlDoc = null;
					if (document.implementation.createDocument){
						// code for Firefox, Mozilla, Opera, etc.
						var xmlhttp = new window.XMLHttpRequest();
						xmlhttp.open("GET", fileurl, false);
						xmlhttp.send(null);
						xmlDoc = xmlhttp.responseXML.documentElement;
					}else  if (window.ActiveXObject){
						
					  xmlDoc=new ActiveXObject( "Microsoft.XMLDOM" ) ;
					  xmlDoc.async = "false" ;
					  xmlDoc.loadXML( xhr.responseText ) ;
					}  else if( window.DOMParser ) {
						// others
					  parser=new DOMParser();
					  xmlDoc=parser.parseFromString( xhr.responseText,"text/xml" ) ;
					}else{
						// 
						alert('Your browser cannot handle this script');
					} 
						resXml= xmlDoc ;				
				  }
				
			},
			error: function(xmlhttp, status, error){
	 			//alert("Err"+status +"status"+ error); 
			}
	 });
	 //alert("resXml " + resXml);
	 return resXml;
}

function unliceseReason(value,divName,flag){
	//alert("1 : "+value+"===="+document.getElementById("unLicensedPk").value);
	if(value == document.getElementById("unLicensedPk").value){
		//alert("inside if script.js>>>>>>>>>>");
		if(flag=="false"){
		//alert("2 if open");
		//alert(divName);
		var div = document.getElementById(divName);
		//alert("div name:: "+div);
		//div.removeAttribute("style");
		div.setAttribute("style","display:block");
		 //jQuery("#"+divName).show();
		//document.getElementById(divName).style.display='block';
		}
		}
	else{
		if(flag=="false"){
			//alert("inside else in script.js>>>>>>>>>>");
		//alert("2.1 else close");
		//alert(divName);
		var div = document.getElementById(divName);
		//alert(div);
		//div.removeAttribute("style");
		//alert("div else name:: "+div);
		div.setAttribute("style","display:none");
		$j("#fkCordCbuUnlicenseReason").val("");
		//jQuery("#"+divName).hide();
		//document.getElementById(divName).style.display='none';
		}
	}
	resetSessionWarningOnTimer();
}
function eligibilityNACordEntry(val){

	if(val==document.getElementById("notCollectedToPriorReasonId").value){
		$j( "#dialog-NAppconfirm-1" ).dialog({
			resizable: false,
			modal: true,
			closeText: '',
			closeOnEscape: false ,
			buttons: {
				"Yes": function() {
					$j( this ).dialog( "close" );
					$j("#NAEligibleFlag").val(true);
				},
				"No": function() {
					$j( this ).dialog( "close" );
					if($j('#eligibilityVal').val()!=""){
						$j('#fkCordCbuEligible').val($j('#eligibilityVal').val());
						inEligibleReason($j('#eligibilityVal').val(),'inEligibleReason');
					}
					else{
						$j('#fkCordCbuEligible').val('-1');
						$j("#NAEligibleFlag").val(false);
						inEligibleReason('-1','inEligibleReason');
					}
				}
			}
	
		});
	}
}
function inEligibleReason(value,divName){
	
if((value == document.getElementById("inEligiblePk").value) || (value == document.getElementById("incompleteReasonId").value)){
	document.getElementById(divName).style.display='block';
	$j("#ineligiblereason").show();
	if(value == document.getElementById("inEligiblePk").value){
		$j("#inelreason").css('display','block');
		$j("#increason").css('display','none');
		$j("#priorreason").css('display','none');
		$j("#additonalinfo").css('display','none');
		$j("#word_count").val("");
		$j("#NAEligibleFlag").val(false);
		if($j("#newQstns").length>0){
			$j("#newQstns").show();
		}
		$j("#fkCordCbuEligibleReason").val("");
	}else if(value == document.getElementById("incompleteReasonId").value){
		$j("#inelreason").css('display','none');
		$j("#increason").css('display','block');
		$j("#priorreason").css('display','none');
		$j("#additonalinfo").css('display','none');
		$j("#NAEligibleFlag").val(false);
		if($j("#newQstns").length>0){
			$j("#newQstns").show();
		}
		$j("#word_count").val("");
		$j("#fkCordCbuEligibleReason").val("");
	}
}else if(value == document.getElementById("eligiblePkId").value){

	document.getElementById(divName).style.display='block';
	$j("#inelreason").css('display','none');
	$j("#increason").css('display','none');
	$j("#priorreason").css('display','none');
	$j("#NAEligibleFlag").val(false);
	$j("#additonalinfo").css('display','none');
	document.getElementById(divName).style.display='none';
	$j("#word_count").val("");
	if($j("#newQstns").length>0){
		$j("#newQstns").show();
	}
}
else{
	document.getElementById(divName).style.display='none';
	if(value == document.getElementById("notCollectedToPriorReasonId").value){
		document.getElementById(divName).style.display='block';
		document.getElementById("ineligiblereason").style.display='none';
		$j("#additonalinfo").css('display','block');
		if($j("#newQstns").length>0){
			$j("#newQstns").hide();
		}
		if(($j("#shipmentFlag").val()=="true") && formValueChk("newQstns")){
			eligibilityNACordEntry(value);
		}
	}else if(value == "-1"){
		$j("#NAEligibleFlag").val(false);
		$j("#word_count").val("");
		document.getElementById(divName).style.display='none';
		if($j("#newQstns").length>0){
			$j("#newQstns").show();
		}
	}
	$j("#additonalinfo").css('display','block');
	$j("#fkCordCbuEligibleReason").val("");
}
resetSessionWarningOnTimer();
}


function closeModal(){
	 $j("#modelPopup1").dialog("close");
	 $j("#modelPopup").dialog("close");
	 $j("#modelPopup1").dialog("destroy");
	 $j("#modelPopup").dialog("destroy");
}

function modalFormSubmitRefreshDiv(formid,url,divname,updateDivId,statusDivId,criflag,cordID){
	if($j("#"+formid).valid()){
		/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		var processingSaveDialog = document.createElement('div');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");		
		*/
		showprogressMgs();
			$j.ajax({
		        type: "POST",
		        url: url,
		        async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");
		        	//$('.tabledisplay').html("");
		            var $response=$j(result);
		            //query the jq object for the values
		            var errorpage = $response.find('#errorForm').html();
		           // alert(oneval);
		           // var subval = $response.find('#sub').text();
		            if(errorpage != null &&  errorpage != "" ){
		            	$j('#main').html(result);
		            }else{
		            	if(divname!=""){ 
			            	 $j("#"+divname).html(result);
			            }
		            	if(criflag=='1'){
		            		//alert("flag"+cordID)
		            		$j.ajax({
						        type: "POST",
						        url : 'getCRIFlag?pkcordId='+cordID,
						        async:false,
						        dataType: 'json',
						        success: function (result){
													var flag=result.completedFlag;
													
													if(flag==1){
														
														$j('#CRIDiv').hide();
														$j('#CRICompleted').show();
														$j('#CRINotCompleted').hide();
													} 
													if(flag==0){
									                    $j('#CRIDiv').hide();
									                    $j('#CRINotCompleted').show();
									                    if($j('#CRICompleted').is(':visible')){
									                        $j('#CRICompleted').hide();
									                    }
									                }
						        }
							});
		            	}
		            	if(updateDivId!=undefined && updateDivId!=""){
		            		$j("#"+updateDivId).css('display','block');			            	
		            	}else{
		            		$j("#update").css('display','block');			            	
		            	}
		            	if(statusDivId!=undefined && statusDivId!=""){
		            		$j("#"+statusDivId).css('display','none');			            	
		            	}else{
		            		$j("#status").css('display','none');			            	
		            	}		            	
		            }
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
			closeprogressMsg();
    }
	
}

function toggleDiv(toggleid){
	var contentId = toggleid+"content";
	$j("#"+toggleid).toggle();	
	$j("#"+contentId).find(".ui-icon").each(function(){
		if($j(this).is(".ui-icon-triangle-1-s") || $j(this).is(".ui-icon-triangle-1-e"))
		{
		if($j(this).is(".ui-icon-triangle-1-s")){
				$j(this).removeClass("ui-icon-triangle-1-s").addClass("ui-icon-triangle-1-e");
	    	}
	    	else if($j(this).is(".ui-icon-triangle-1-e")){
	    		$j(this).removeClass("ui-icon-triangle-1-e").addClass("ui-icon-triangle-1-s");
	    	}
		}
	});    
   
}


function addMoreFiles(id){

	$j("#"+id).find("tr .add").each(function(){
         $j(this).after("<tr ><td> <s:text name='garuda.unitreport.label.selectyourfile' /></td>"
 				+"<td><s:file name='fileUpload' /><span id='span1' style='color: red;display: none;'><strong>Uploaded file - <s:property value='#request.path' />"
				+"</strong></span></td></tr>");
		});		
}

function limitText(limitField, limitCount, limitNum) {
	//alert(limitField +"--"+ limitCount+"--"+ limitNum);
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

//for show and hide the columns in the grid


var showFlag1 = false;
var tableColCout =0;

function fn_events(tableall,targetall,ulselectcol){
	 $j('#'+targetall).hide();
		hideCol =[];
	$j('#'+tableall).columnManager({listTargetID:targetall, onClass: 'advon', offClass: 'advoff', hideInList: [tableColCout],  
    saveState: true, colsHidden: hideCol}); 
	//create the clickmenu from the target
	$j('#'+ulselectcol).click( function(){
		  if(showFlag1){
			//$j('#'+targetall).fadeOut();
			showFlag1 = false;
		  }else{
			  $j('#'+targetall).fadeIn();
			  showFlag1 = true;
		  }
		}
	);
	$j('#'+targetall).prepend('<div style="display:inline-block;" ><li onclick="selectCheckBox(this);" class="lineDiv">Reset All</li><img class="crossDiv" src="./images/cross.png" onclick="$j(this).parent().parent().fadeOut();"></div><br/>');
}
var showFlag1 = false;
var tableColCout =0;

function fn_events1(tableall,targetall,ulselectcol,dropId,MODULE){
	
	$j('#'+targetall).hide();
	hideCol =[];
    var o2Table;
	$j('#'+tableall).dTcolumnManager({listTargetID:targetall, onClass: 'advon', offClass: 'advoff', hideInList: [tableColCout],  
    saveState: true, colsHidden: hideCol}); 
	//create the clickmenu from the target
	$j('#'+ulselectcol).click( function(){
		  
		  if(showFlag1){
			//$j('#'+targetall).fadeOut();
			showFlag1 = false;		
			//alert("inside false")
		  }else{
			  $j('#'+targetall).fadeIn();
			  showFlag1 = true;
		  }
		  
	});

	$j('#'+ulselectcol).find('li.advon,li.advoff').click(function(){
		
		
		    if($j('#'+dropId+' :selected').html()=='ALL' || MODULE=='PENDING'){
		    	//o2Table = $j('#'+tableall).dataTable();
		    	ColManForAll(tableall,targetall,'true');
				
		    }else{
		    	if(navigator.appName=="Microsoft Internet Explorer"){
			    	var o2Table=$j('#'+tableall).dataTable();
			    	o2Table.fnDraw();
			    	//$j('#'+targetall).fadeIn();
		    	}
		    }
	});


$j('#'+targetall).prepend('<div style="display:inline-block;" ><li onclick="selectCheckBox(this);" class="lineDiv">Reset All</li><img class="crossDiv" src="./images/cross.png" onclick="$j(this).parent().parent().fadeOut();"></div><br/>');
	
}
function ColManForAll(tableId,targetallid,noflag){
	
	//alert("inside calling colManALL");
	var nvisArr=new Array();
    var visArr=new Array();
    var m=0;
    var n=0;
    var o2Table=$j('#'+tableId).dataTable();
    var flag=true;
    var listValue = $j('#'+tableId+"_wrapper").find('.dataTables_scrollHeadInner').find('thead tr').find('th').get(), lSize = listValue.length, l;
	for(l = 0; l < lSize; l++){
		var thisVal = $j(listValue[l]);
		if(!$j(thisVal).is(':visible')){
    	   $j(thisVal).show();	
    	}
    }
    
	listValue = $j('#'+targetallid).find('li').get(), lSize = listValue.length, l;
	for(l = 0; l < lSize; l++){
		var thisVal = $j(listValue[l]);
    	   if(l!=0 && $j(thisVal).hasClass('advoff')){
    		   nvisArr[m]=l-1;
    		   m++;
    	   }
    	   if(l!=0 && $j(thisVal).hasClass('advon')){
    		   visArr[n]=l-1;
    		   n++;
    	   }
    	 
     }
     for(var k=0;k<nvisArr.length;k++){
    	 var tempindex=nvisArr[k];
    	 var bVis = o2Table.fnSettings().aoColumns[tempindex].bVisible;
         
         if(bVis==true){
        	 //alert("disabling column:"+tempindex);
        	 o2Table.fnSetColumnVis( tempindex,false,false);
        	 flag=false;
         }
     }
     for(var k=0;k<visArr.length;k++){
    	 var tempindex=visArr[k];
    	 var bVis = o2Table.fnSettings().aoColumns[tempindex].bVisible;
        
    	 if(bVis==false){
        	 //alert("enabling column:"+tempindex);
        	 o2Table.fnSetColumnVis( tempindex,true,false);
        	 flag=false;
         }
     }
     
     if(flag==false){
    	 o2Table.fnDraw();
    	 /*if(noflag=='true'){
    		 $j('#'+targetallid).fadeIn();
    	 }*/
    	 if($j('#'+targetallid).css('display','block')){
    		 $j('#'+targetallid).css('display','none');
    	 }
         
     }
    
}
function selectCheckBox(el){
	var ulObj = $j(el).closest("ul");
	var isSelected = $j(ulObj).find(".advoff").length;
	if(isSelected > 0){
		$j(ulObj).find(".advoff").each(function(i,el){
			$j(el).triggerHandler("click");
		})
	}
	else{
		//var shownLength = $j(ulObj).find(".advon");
		//$j(ulObj).find(".advon").each(function(i,el){
		//	if(i!=0){
		//		$j(el).triggerHandler("click");
		//	}
		//})
		 
	}
}
function parseDateValue(rawDate) {
	
	var year= rawDate.substring(7,12);
	var month= rawDate.substring(0,3);
	var dt= rawDate.substring(4,6);
	var parsedDate=dt+"-"+month+"-"+$j.trim(year);
	return $j.trim(parsedDate);
}
function fnclear(id1,id2){
	$j('#'+id1).val('');
	$j('#'+id2).val('');
}

//toolip is for displaying the text in the onmouseover action and hide the text while onmouseout event
/*var tooltip=function(){
	var id = 'tt';
	var top = 3;
	var left = 3;
	var maxw = 300;
	var speed = 10;
	var timer = 20;
	var endalpha = 95;
	var alpha = 0;
	var tt,t,c,b,h;
	var ttie = document.all ? true : false;
	return{
		show:function(v,w){
			if(tt == null){
				tt = document.createElement('div');
				tt.setAttribute('id',id);
				t = document.createElement('div');
				t.setAttribute('id',id + 'top');
				c = document.createElement('div');
				c.setAttribute('id',id + 'cont');
				b = document.createElement('div');
				b.setAttribute('id',id + 'bot');
				tt.appendChild(t);
				tt.appendChild(c);
				tt.appendChild(b);
				document.body.appendChild(tt);
				tt.style.opacity = 0;
				tt.style.filter = 'alpha(opacity=0)';
				document.onmousemove = this.pos;
			}
			tt.style.display = 'block';
			c.innerHTML = v;
			tt.style.width = w ? w + 'px' : 'auto';
			if(!w && ttie){
				t.style.display = 'none';
				b.style.display = 'none';
				tt.style.width = tt.offsetWidth;
				t.style.display = 'block';
				b.style.display = 'block';
			}
			if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
			h = parseInt(tt.offsetHeight) + top;
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(1)},timer);
		},
		pos:function(e){
			var u = ttie ? event.clientY + document.documentElement.scrollTop : e.pageY;
			var l = ttie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
			tt.style.top = (u - h) + 'px';
			tt.style.left = (l + left) + 'px';
		},
		fade:function(d){
			var a = alpha;
			if((a != endalpha && d == 1) || (a != 0 && d == -1)){
				var i = speed;
				if(endalpha - a < speed && d == 1){
					i = endalpha - a;
				}else if(alpha < speed && d == -1){
					i = a;
				}
				alpha = a + (i * d);
				tt.style.opacity = alpha * .01;
				tt.style.filter = 'alpha(opacity=' + alpha + ')';
			}else{
				clearInterval(tt.timer);
				if(d == -1){tt.style.display = 'none'}
			}
		},
		hide:function(){
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
		}
	};
}();*/

	function highlight(id){
		   $j('#'+id).addClass('high');
		}
	
	function removeHighlight(id){
			$j('#'+id).removeClass('high');	
	}
	
	function checkMininum(org_id){	
		
		return ((org_id.replace(/-/g, "").length)==9?true:false);
		
	}
	
	function checkFormat(value){
		var flag = true;
		var flag1 = false;
		var flag2 = false;
		var count = 0;
		if(value.indexOf("-")==-1){
			return false;
		}
		while(value.indexOf("-") > 0){
			var index = value.indexOf("-");		
			value = value.substr(0,index)+value.substr(index+1,value.length-(index+1));
			if(index==4 ){
				flag1 = true;				
			}else if(index==8){
				flag2 = true;
			}
			else{
				flag = false;
				break;
			}
		}
		if(!flag)
		  return flag;
		else 
		  return (flag1 && flag2);
	}
	
	function CheckSum(org_id)
	{
		    if(org_id==""){
		    	return false;
		    }
		    var chkarray = new Array(6);
			var chkdex; 
			var chkadd=0;
			var chksum=0; 
			var nxthighest=0;
			var idlength=0;
			
			while (org_id.indexOf("-") > 0) {
			
			index = org_id.indexOf("-");
			 org_id = org_id.substr(0,index)+org_id.substr(index+1,org_id.length-(index+1)); 
			}
			
			var i = 0;
			var flag = true;
			var j = 0;
			var len = org_id.length;
			while(j!=len){
				if((org_id.charCodeAt(i)>64 && org_id.charCodeAt(i)<91) || (org_id.charCodeAt(i)>96 && org_id.charCodeAt(i)<123) ){
						index = org_id.indexOf(org_id.charAt(i));
						org_id = org_id.substr(0,index)+org_id.substr(index+1,org_id.length-(index+1));							
				}else{
					i++;
				}
				j++;
			}
			idlength=org_id.length;
			
			
			for (chkdex = 0; chkdex <= idlength; chkdex++) { 
			chkarray[chkdex] = parseInt(org_id.substr(chkdex, 1));
			if((chkdex>=1)&&(chkdex%2!=0))
			{
			chkarray[chkdex] = 2 * chkarray[chkdex];
			
			}
			
			} 
			
			chkadd = 0; 
			for(chkdex=0;chkdex<idlength-1;chkdex++)
			{
			if(chkarray[chkdex]>=10)
			{
			num=chkarray[chkdex];
			while(num>0)
			{
			chkadd=chkadd+ num%10;
			num=parseInt(num/10);
			
			}
			}
			else
			{
			chkadd=chkadd+chkarray[chkdex];
			}
			}
			
			nxthighest = (Math.floor(chkadd/10)+1)*10; 
			
			chksum = nxthighest - chkadd; 
			if (chksum == 10) {
			chksum = 1;
			} 
			else { 
			if (chksum == 9)
			 {
			chksum = 0;
			}
			 else {
			chksum = chksum + 1;
			}}
			
			 if (chksum == chkarray[idlength-1])
			{
				 return true;
			}else{				   
				   return false;
			 }
	}
	function submitform(formname)	       
	{		     
          if($j("#"+formname).valid()){				            
	         document.getElementById(formname).submit();				
		 }
	}
	
	function enter2Refreshdiv(e)
	{
	    var key = window.event ? e.keyCode : e.which;
	    if (key == 13) //enter
	   {
	   	 	showDiv();
		        return false;
	   }
		    
	return true;
	}
	
	
	function entersRefreshdiv(e)
	{
	    var key = window.event ? e.keyCode : e.which;
	    if (key == 13) //enter
	   {
	  
	    	showCbuDiv();
		        return false;
	   }
		    
	return true;
	}
	
	
	function checkSpecialCharacters(data){	
	 return (/^[a-zA-Z0-9\-]*$/.test(data));
	}
		
	function validateScreen(pass,e){
		if(e.which==13 && $j('#'+pass).is(':visible')){
			eventCode=e.which;	
		}else if(e.keyCode==13 && $j('#'+pass).is(':visible')){
			eventCode=e.keyCode; 
		}
	if ((e.which == 13) || (e.keyCode == 13)) {
		if (e.preventDefault) { 
			e.preventDefault();
		} 
		else{
			e.returnValue = false; 
			}
    }
	}
	
	function validateSign(value,id,invalid,minimum,pass){
		if(id=="" || id==null){
			id="submit";			
		}
		if(invalid=="" || invalid==null){
			invalid="invalid";			
		}
		if(minimum=="" || minimum==null){
			minimum="minimum";			
		}
		if(pass=="" || pass==null){
			pass="pass";			
		}
		if(value.length<4){
			$j("#"+minimum).css('display','block');
			$j("#"+invalid).css('display','none');
			$j("#"+pass).css('display','none');
			$j("#"+id).attr("disabled", true);
			$j("#"+id).addClass('ui-state-disabled');
			return false;
		}else{
		var url = "getEsignDetails?userPojo.signature="+value;
			$j.ajax({
				type : "POST",
				url : url,
				async : false,
				url : 'getEsignDetails?userPojo.signature='+value,
				success : function(result) {
				if(result.message=="true"){	
					$j("#"+minimum).css('display','none');
					$j("#"+invalid).css('display','none');
					$j("#"+pass).css('display','block'); 
				    $j("#"+id).attr("disabled", false);
				    $j("#"+id).removeClass('ui-state-disabled');
				    if (eventCode == 13) {
				    	eventCode="";
				    	$j("#"+id).click();
				    }
	              }else{
	            	  eventCode="";
	            	  $j("#"+minimum).css('display','none');
	            	  $j("#"+invalid).css('display','block');
	            	  $j("#"+pass).css('display','none'); 
					  $j("#"+id).attr("disabled", true);
					  $j("#"+id).addClass('ui-state-disabled');
				 }
			   }
			});	
			
		}
}
	

function validateSignOne(value){
		if(value.length<4){
			$j("#minimum1").css('display','block');
			$j("#invalid1").css('display','none');
			$j("#pass1").css('display','none');
			$j("#submit1").attr("disabled", true);
			return false;
		}else{
		var url = "getEsignDetails?userPojo.signature="+value;
			$j.ajax({
				type : "POST",
				url : url,
				async : false,
				url : 'getEsignDetails?userPojo.signature='+value,
				success : function(result) {
				if(result.message=="true"){	
					$j("#minimum1").css('display','none');
					$j("#invalid1").css('display','none');
					$j("#pass1").css('display','block'); 
				    $j("#submit1").attr("disabled", false);              
	              }else{
	            	    $j("#minimum1").css('display','none');
						$j("#invalid1").css('display','block');
						$j("#pass1").css('display','none'); 
					    $j("#submit1").attr("disabled", true); 
				 }
			   }
			});		
		}
	}

function showDocument(AttachmentId){
	//var signed='true';
	var encrypted='false';
	var url='getAttachmentFromDcms?docId='+AttachmentId+'&encrypted='+encrypted;
	window.open( url, "Attachment", "status = 1, height = 500, width = 800, resizable = 0" );
}

function showDocumentForNonMembers(attachmentDiv,AttachmentId,divid,filetype,filename){	
	$j('#'+attachmentDiv+' td').each(function(index,el){
			$j(el).addClass('tabnormal').removeClass('tabactive');
	});
	$j('#'+divid).removeClass('tabnormal').addClass('tabactive');
	var signed='true';
	var ftype="";
	var size=filename.length;
	var index=filename.indexOf('.');
	ftype=filename.substring(index+1,size);
	var url='getAttachment?docId='+AttachmentId+'&signed='+signed+'&getDoc=true&ftype='+ftype;
	refreshAttachment(url,"downloaddoc");
	var obj="";
	var path="temp"+"/"+$j('#filepath').val();
	$j('#downloaddoc1').html("");
	$j('#errorpage').hide();
	if($j('#filepath').val()!=""){
		if(filetype=='application/pdf'){
			obj='<object data="'+path+'" height="750" width="100%" type="'+filetype+'">It appears you don\'t have Required plug-in support in this web browser.<a href="'+path+'">Click here to download the Attachment</a></object>';
		}else{
			obj='<object data="'+path+'" width="100%" type="'+filetype+'">It appears you don\'t have Required plug-in support in this web browser.<a href="'+path+'">Click here to download the Attachment</a></object>';
		}
		$j('#downloaddoc1').append(obj);
	}else{
		$j('#errorpage').show();
	}
}

function refreshAttachment(url,divname){
	/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	var processingSaveDialog = document.createElement('div');
		$j(processingSaveDialog).attr("id",'progressMsg');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();
		$j(processingSaveDialog).dialog("open");*/
	showprogressMgs();
	$j.ajax({
	    type: "POST",
	    url: url,
	    async:false,
	    success: function (result){
	        var $response=$j(result);
	       	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
            
        }
	});
	closeprogressMsg();
}
function setValueInSession(url){
	$j.ajax({
	    type: "POST",
	    url: url,
	    async:false,
	    success: function (result){
	        var $response=$j(result);
	       	$j("#emptyDiv").replaceWith($j('#emptyDiv', $j(result)));
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	//alert("Error " + error);
            //alert(request.responseText);
            
        }
	});
}

function sopReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("sops").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("sopname").text()+"</span><ul>";
			$j(this).find("subsop").each(function() {
				html += "<li><span class='file' ><a href='"+$j(this).find("soplink").text()+"' target='blank'>"+$j(this).find("sopdesc").text()+"</a></span></li>";
			});
			html += '</ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	$j("#sopTree").treeview();
}

function mopReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("mops").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("mopname").text()+"</span><ul><li>";
		    html += "<table width='100%' cellpadding='0' cellspacing='0' id='"+$j(this).find("mopid").text()+"'>";
		    html+= "<thead><tr><th></th><th></th></tr></thead>";
		    html += "<tbody>";
	    	$j(this).find("submop").each(function() {
		    	html+="<tr><td>";
				html += "<a href='"+$j(this).find("moplink").text()+"' target='blank'>"+$j(this).find("mopchapter").text()+"</a>";
				html+= "</td>";
				html+= "<td>";
				html += $j(this).find("moppurpose").text();
				html+="</td><tr>";
				
			});
	    	html += "</tbody>";
	    	html+= "<tfoot><tr><td colspan='2'></td></tr></tfoot>";
		    html += "</table>";
			html += '</li></ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	 //$j(data).find("mops").each(function() {
	//	 var id = $j(this).find("mopid").text();
	//	 $j("#"+id).dataTable();
	// )};
	$j("#sopTree").treeview();
}

function guideReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("guides").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("guidename").text()+"</span><ul><li>";
		    html += "<table width='100%' border='1' cellpadding='0' class='displaycdr' cellspacing='0' id='"+$j(this).find("guideid").text()+"'>";
		    html+= "<thead><tr><th>Guide Name</th><th>View</th><th>Print</th><th>Download</th></tr></thead>";
		    html += "<tbody>";
	    	$j(this).find("subguide").each(function() {
		    	html+="<tr><td>";
				html += $j(this).find("subguidename").text();
				html+= "</td>";
				html+= "<td>";
				html += "<a href='"+$j(this).find("subguidelink").text()+"' target='blank'>View</a>";
				html+= "</td>";
				html+= "<td>";
				html += "<span class='ui-icon ui-icon-print'></span>";
				html+= "</td>";
				html+= "<td>";
				html += "<span class='ui-icon ui-icon-arrowthickstop-1-s'></span>";
				html+="</td><tr>";				
			});
	    	html += "</tbody>";
	    	//html+= "<tfoot><tr><td colspan='4'></td></tr></tfoot>";
		    html += "</table>";
			html += '</li></ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	 //$j(data).find("guides").each(function() {
	//	 var id = $j(this).find("guideid").text();
	//	 $j("#"+id).dataTable();
	// )};
	$j("#sopTree").treeview();
}

function helpReference(){
	var data = readXml1("link-navigation.xml");
	var html = "<ul id='sopTree' class='filetree' >";	
	    $j(data).find("helplinks").each(function() {
		    html += "<li><span class='folder' >"+$j(this).find("helplinkname").text()+"</span><ul>";
			$j(this).find("subhelplink").each(function() {
				html += "<li><span class='file' ><a href='"+$j(this).find("subhelplinkref").text()+"' target='blank'>"+$j(this).find("subhelplinkdesc").text()+"</a></span></li>";
			});
			html += '</ul></li>';
		});	
	html += "</ul>";	
	//alert(html);
	$j("#ref").html(html);
	$j("#sopTree").treeview();
}

function networkReference(){
	var data = readXml1("link-navigation.xml");
	var html = "";
	    	html += "<table width='100%' cellpadding='0' cellspacing='0' id='"+$j(this).find("mopid").text()+"'>";
		    html+= "<thead><tr><th></th></tr></thead>";
		    html += "<tbody>";
		    $j(data).find("networks").each(function(){
		    	$j(this).find("subnetwork").each(function() {
			    	html+="<tr><td>";
					html += "<a href='"+$j(this).find("networklink").text()+"' target='blank'>"+$j(this).find("networkdesc").text()+"</a>";
					html+="</td><tr>";
					
				});
		    });
	    	html += "</tbody>";
	    	html+= "<tfoot><tr><td></td></tr></tfoot>";
		    html += "</table>";
	//alert(html);
	$j("#ref").html(html);
	// $j(data).find("mops").each(function() {
	//	 var id = $j(this).find("mopid").text();
	//	 $j("#"+id).dataTable();
	// )};
	$j("#sopTree").treeview();
}

function clickheretoprint(idOfPrintDiv,regId,flag){
	 var sourceFile ="";
	 var regId = $j.trim(regId);
	 if(regId!=""){ 
		 sourceFile = "<table><thead><tr><th><center><h3><u>CBU Registry ID:"+regId+"</u></h3></center></th></tr></thead><tbody><tr><td><br/>"+$j('#'+idOfPrintDiv).html()+"</td></tr></tbody></table>";
		 }
	 else
		 sourceFile = $j('#'+idOfPrintDiv).html();
	 
		 
	 $j('#widgetPrintDiv').html(sourceFile);
	 if(idOfPrintDiv=='cbuHla'){
		 hideshowDivs(false); 
	 }else{
		 hideshowDivs(true);
	 }
	 if(idOfPrintDiv=="shipmentDiv"){
		 $j("#widgetPrintDiv table > thead > tr > th").each(function (i, el) {
	  			if($j(el).hasClass('fc-last')){
	  				$j(el).css("width","33px");
	  			}
	  		});
	 }
	 var tempval=$j('#switchData').val();
	 
	 if(tempval!=undefined && (tempval==null || tempval=="" || tempval=='null')){
			$j('#widgetPrintDiv').find('.switchWrkflowCls').hide();
	 }
	 if(flag=='1' && navigator.appName!="Microsoft Internet Explorer"){
		 $j('#widgetPrintDiv').find('.printClass').show();
		 $j('#widgetPrintDiv').find('.ui-progressbar').each(function(i,el){
			 var varr=$j(this).find('img').hasClass('printClass');
			 if(varr==false){
				 $j(this).find('div').after('<img src="images/tsk_nt_strted.png"  style="width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
			 }
		 });
	 }
	 
	 PrintDiv('widgetPrintDiv');
	 $j('#widgetPrintDiv').html("");
	// $j('#widgetPrintDiv').printArea({
	//	    mode: "popup",
	//         popWd:800,
    //        popClose: false
	//	});
	// $j('#widgetPrintDiv').html("");
}

function clickheretoprintaudit(idOfPrintDiv,auditHeader){
	 var sourceFile ="";
	 var auditHeader = $j.trim(auditHeader);
	 if(auditHeader!=""){
		 //sourceFile = "<center><h3><u>CBU Registry ID:"+regId+"</u></h3></center><br/>"+$j('#'+idOfPrintDiv).html();
		 sourceFile = $j('#'+auditHeader).html()+$j('#'+idOfPrintDiv).html();
		 
		 }
	 else
		 sourceFile = $j('#'+idOfPrintDiv).html();
		 $j('#widgetPrintDiv').html(sourceFile)
	 hideshowDivs(true);
	 $j('#widgetPrintDiv .dataTables_scrollBody > table').css('position', '');
		 $j('#widgetPrintDiv .dataTables_scrollBody > table').css('top', '');
		 $j('#widgetPrintDiv .dataTables_scrollBody > table').css('left', '');
		 $j('#widgetPrintDiv .dataTables_info').next().html("");
	PrintDiv('widgetPrintDiv');
	$j('#widgetPrintDiv').html("");
	// $j('#widgetPrintDiv').printArea({
	//	    mode: "popup",
	//         popWd:800,
   //        popClose: false
	//	});
	// $j('#widgetPrintDiv').html("");
}

function PrintDiv(divName) {    
           var divToPrint = document.getElementById(divName);
           var popupWin = window.open('', '_blank', 'width=600,height=500,scrollbars=yes');
           popupWin.document.open();
           popupWin.document.write('<html><head>'+
'<link rel="stylesheet" type="text/css" href="./styles/velosCDR.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./styles/bethematch/Garuda_skin.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./styles/general.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./styles/portlet.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./styles/jquery.alerts.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./styles/pending-order.css" media="screen"/>'+
'<link rel="stylesheet" type="text/css" href="./styles/jquery.time.slider.css" media="screen"/>'+
'<link rel="stylesheet" type="text/css" href="./js/jquery/themes/nmdp/common/forms.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./js/jquery/themes/nmdp/common/clickmenu.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./js/jquery/themes/nmdp/common/styles.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./js/jquery/themes/nmdp/common/demo_table.css" media="screen" />'+
'<link rel="stylesheet" type="text/css" href="./styles/jquery.treeview.css" type="text/css" />'+
'<style>@media print {  thead {display: table-header-group;} }</style>'+
'<script type="text/javascript">function MyHead() {var find2 = "Reset Filters";var find3 = "Execute Filter";var page = document.body.innerHTML;while (page.indexOf(find2) >= 0) {var m = page.indexOf(find2);var n = find2.length;page = page.substr(0,m) + page.substr(m+n);document.body.innerHTML = page;}while (page.indexOf(find3) >= 0) {var l = page.indexOf(find3);var k = find3.length;page = page.substr(0,l) + page.substr(l+k);document.body.innerHTML = page;}}</script><style>.portlet-header{border: 1px solid #0073AE;text-align: center;}</style></head><body onload="MyHead();window.print();window.close();">' + divToPrint.innerHTML + '</html>');
           //window.print();window.close();           
          popupWin.document.close();
}
function printDUMN(idOfPrintDiv,regId,idOnBag,cbbName,patientId){
	 var sourceFile ="";
	 var regId = $j.trim(regId);
	 if(regId!=""){
		 //sourceFile = "<center><h3><u>CBU Registry ID:"+regId+"</u></h3></center><br/>"+$j('#'+idOfPrintDiv).html();
		 sourceFile +='<table width="100%" style="border:1px solid black;" ><tr><td>Patient Identification Number :'+patientId+'</td>'			 
				   +'</tr><tr><td>Cord Blood Bank/Registry Name :'+cbbName+'</td></tr><tr><td>CBU Registry ID :'+regId+'</td></tr><tr><td>Unique Product Identity on Bag :'+idOnBag+'</td></tr></table>';
		 sourceFile += $j('#'+idOfPrintDiv).html();
		 sourceFile += '<table width="100%"><tr><td width="33%">________________________________</td><td width="33%">________________________________</td><td width="33%">_____/&nbsp;_____/&nbsp;_____/&nbsp;</td></tr>'
		               +'<tr><td width="33%">Transplant Physician Name(Print)</td><td width="33%">Transplant Physician Signature</td><td width="33%"> MM&nbsp;DD&nbsp;YYYY</td></tr>'
	                   +'</table><table width="100%"><tr><td >CBU Declaration of Urgent Medical Need, version x.x,March 31, 2012</td></tr><tr><td> <Font size ="2"><b>Document Number :</b></td></tr>'
		               +'<tr><td><Font size ="2"><b>Replaces :</b></td></tr></table><table width="100%"><tr>'
			           +'<td align="center"><Font size ="1">Copyright &copy; National Marrow Donor Program, All Rights Reserved.</td></tr>'
		               +'<tr><td align="center"><Font size ="1" >National Marrow Donor Program &reg; Priority and Confidential Information</td></tr><p><time pubdate datetime="2012-03-01"></time></p>'
	                   +'</table>';
		 
		 }
	 else
		 sourceFile = $j('#'+idOfPrintDiv).html();
		 $j('#widgetPrintDiv').html(sourceFile)
	 hideshowDivs(true);
	 $j('#widgetPrintDiv').printArea({mode: "popup", popClose: false});
}
function f_countRows(TblEl,tblfoot,searchtag,selectEl,NoofDaysEl,dateEl1,dateEl2,PercentEl){
	
	
	
	var rowCountval=$j('#'+TblEl).find('tbody tr').length;
	var cellCountval = $j("#"+TblEl).find('tbody tr')[0].cells.length;
	var OrdTblCol = $j("#"+TblEl).find('tr')[0].cells.length;
	var ordFlag=0;
	if(cellCountval==1){
		rowCountval=0;
	}
	
	var i=1;
	while(i<OrdTblCol){
		   
			if($j('#'+selectEl+i).val()!=undefined && $j('#'+selectEl+i).val()!=null && $j('#'+selectEl+i).val()!=""){
						ordFlag=1;
			}
			i++;
	}
				
	if($j('#'+NoofDaysEl).val()!=undefined  && $j('#'+NoofDaysEl).val()!=null && $j('#'+NoofDaysEl).val()!='' ){
		ordFlag=1;
	}
	
	if(($j('#'+dateEl1).val()!=undefined && $j('#'+dateEl1).val()!=null  &&  $j('#'+dateEl1).val()!='' ) || ($j('#'+dateEl2).val()!=undefined && $j('#'+dateEl2).val()!=null && $j('#'+dateEl2).val()!='' )){
		ordFlag=1;
	}
	
	if($j('#'+PercentEl).val()!=undefined && $j('#'+PercentEl).val()!=null && $j('#'+PercentEl).val()!='' ){
		ordFlag=1;
	}
	
	if(searchtag=='fromsearchall'){
		var tbl_filter=TblEl+"_filter";
		var value1=$j('#'+tbl_filter).find('input:text').val();
		
		if(value1!=''){
			ordFlag=1;
		}else{
			ordFlag=0;
		}
		
	}

	if(ordFlag==1){
		var maxRec=Number($j('#maxRec'+tblfoot).val())+1;
	 	var minRec=Number($j('#minRec'+tblfoot).val());
	 	var tcount=maxRec-minRec;
		
		var tmp="<b> Showing 1 to "+rowCountval+" of "+rowCountval+" Entries ( filtered from "+tcount+" total entries )</b>";
		if(cellCountval==1){
			var tmp="<b> Showing 0 to "+rowCountval+" of "+rowCountval+" Entries ( filtered from "+tcount+" total entries )</b>";
		}
		
		$j('#filterShow'+tblfoot).html(tmp);
		$j('#paginationDiv'+tblfoot).hide();
		$j('#filterResult'+tblfoot).show();
	}else{
		$j('#paginationDiv'+tblfoot).show();
		$j('#filterResult'+tblfoot).hide();
	}
	
	
}
function hideshowDivs(flag){
		/*if(flag){
		  $j("#widgetPrintDiv div > table > tbody").each(function(i, el){
			 var tr = $j(el).find("tr"); 
			 var trlenth = tr.length;
			 if(trlenth==0){
				  $j(el).closest("div").remove(); 
			  }
			});
		}*/
		$j("#widgetPrintDiv #finaltablelookup > tbody > tr").each(function(i, el){
				  var comment=$j(el).find("#commentTd").find("textarea").val(); 
				  $j(el).find("#commentTd").html(comment);
			});
		var fcEligibleFlag="false";
		var fcLicenseFlag="false";
		var fcFlagitemsFlag="false";
		var fcAttachmentsFlag="false";
		if($j("#widgetPrintDiv #eligibleDiv").css('display') == 'none' ){
			fcEligibleFlag="true";
		}
		if($j("#widgetPrintDiv #licensureDiv").css('display') == 'none' ){
			fcLicenseFlag="true";
		}
		if($j("#widgetPrintDiv #flaggedItemsDiv").css('display') == 'none' ){
			fcFlagitemsFlag="true";
		}
		if($j("#widgetPrintDiv #nonmemberPdfDiv").css('display') == 'none' ){
			fcAttachmentsFlag="true";
		}
		
	  $j('#widgetPrintDiv div').show();
	  $j('#widgetPrintDiv .dataTables_scrollBody').removeAttr("style");
	  $j('#widgetPrintDiv div.printHideDiv').hide();
	  $j('#img_srcId').hide();
	  $j('#widgetPrintDiv #mainpending-assignbar').hide();
	  $j('#widgetPrintDiv .portlet-content').removeAttr("style");
	  $j('#widgetPrintDiv button').remove();
	  $j('#widgetPrintDiv input[type="button"]').remove();
	  $j('#widgetPrintDiv input[type="text"]').each(function(i,el){
			 $j(el).replaceWith($j(el).val());
		  });
	  $j("#widgetPrintDiv textarea").each(function (i, el) {
		  var  text = $j(this).text();
	      var parent = $j(el).closest("textarea");
	      $j(parent).replaceWith(text);
	  });	  		
	  $j("#widgetPrintDiv select").each(function (i, el) {
         var isMultiSelect=$j(el).attr("multiple");
         if(isMultiSelect){
            var id =el.id;
            var text ="";
            $j('#'+id+" option:selected").each(function (i, el) {
                if(text!=""){
               	 text+=",";
                   }
           	 text+=$j(this).text();
              });
            $j(el).replaceWith(text);
         }
       });
		 
	  $j("#widgetPrintDiv select option:selected").each(function (i, el) {
         var  text = $j(this).text();
         if(text == "Select"){
       	  text = "";
            }
         var parent = $j(el).closest("select");
         $j(parent).replaceWith(text);
       });
     
	  $j('#widgetPrintDiv input[type="radio"]').each(function(i,el){
		  	var isChecked = $j(el).attr("checked");
		  	if(!isChecked){
		  		var label = $j(el).next('label');
				$j(label).remove();
			 }
			 $j(el).remove();
		  });

	  $j('#widgetPrintDiv .cmDiv').remove();
	  $j("#widgetPrintDiv .auditCss").removeAttr("style");
	  $j("#widgetPrintDiv #helpmsg").html("");
	  $j("#widgetPrintDiv #trEsign").html("");
	  if(fcEligibleFlag=="true" && fcLicenseFlag=="true" && fcFlagitemsFlag=="true" && fcAttachmentsFlag=="true"){
		  $j("#widgetPrintDiv #finalresult").html("");
		 
	  }
	  if(fcEligibleFlag=="true"){
		 $j("#widgetPrintDiv #eligibleDiv").html("");
	  }
	  if(fcLicenseFlag=="true"){
		  $j("#widgetPrintDiv #licensureDiv").html("");
		  $j("#widgetPrintDiv #licenseDiv").html("");
	  }
	  if(fcFlagitemsFlag=="true"){
		  $j("#widgetPrintDiv #flaggedItemsDiv").html("");
	  }
	  if(fcAttachmentsFlag=="true"){
		  $j("#widgetPrintDiv #nonmemberPdfDiv").html("");
	  }
	  $j("#widgetPrintDiv #dialog-resetLicDiv-1").html("");
	  $j("#widgetPrintDiv #dialog-resetLicDiv").html("");
	  $j("#widgetPrintDiv #update").html("");
	  $j("#widgetPrintDiv #notessearchTable").html("");
	  $j('#widgetPrintDiv #errorpage').html("");
	   $j('#widgetPrintDiv .dataTables_info,#widgetPrintDiv .dataTables_length,#widgetPrintDiv .dataTables_filter').html("");
	   $j('#widgetPrintDiv .dataTables_paginate,#widgetPrintDiv .paging_two_button').html("");
	   var dtable = $j('#widgetPrintDiv .displaycdr');
	  		dtable.css({'border-bottom':'0'});
	  		for(var i=0;i<dtable.length;i++){
		   		dtable[i].border="1";
		   		$j(dtable[i]).css({'border-collapse':'collapse'});
				$j(dtable[i]).find('td').css({'empty-cells:':'show'});
		   		dtable[i].align="center";
		   }
	}
	function tableAlignmentInPrint(printD,tableDivs)
	{
		var printDivArr = tableDivs.split(",");
		for(var i=0;i<printDivArr.length;i++) {
			$j('#'+printD+' #'+printDivArr[i]+'_wrapper .dataTables_scrollBody table > thead').replaceWith($j('#printTemp #'+printDivArr[i]+'_wrapper .dataTables_scrollHeadInner table > thead'));
		}
		
	}
    function paginationFooter(pageNo, allParams,totalCount,module,siteIdentifier){
		//String totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn;
    	defaultPageNojs = pageNo;
		var refershMethod = 0;
		var totalparams = allParams.split(","); 
		var url = totalparams[0]+"?iPageNo="+pageNo+"&iShowRows="+$j('#'+totalparams[1]).val();
		var temp='#'+totalparams[1] +' :selected';
	   	var inputText = $j(temp).text();
	   	var showRowval=$j('#'+totalparams[1]).val();
		//alert("totalparams.length::::"+totalparams.length+"values in array::"+totalparams+"totalCount"+totalCount)
		if(totalCount!=undefined && totalCount!=""){
			if(totalparams[4]=='searchTble' && $j('#sipfilteparams').val()!=""){
				url += "";
			}else{
				url += "&cbuCount="+totalCount;
			}
			
		}else{
			url += "&cbuCount=0";
		}
		
		if(siteIdentifier!=undefined && siteIdentifier!=""){
			if(totalparams[4]=='searchTble' && $j('#sipfilteparams').val()!=""){
				url += "";
			}else{
				url += "&selSiteIdentifier="+siteIdentifier;
			}
			
		}
		
		if(module!=undefined && module!=""){
			url += "&paginateModule="+module;
			if(totalparams[4]=='searchTble1'){
				url += $j('#nacbufilteparams').val();
			}
			if(totalparams[4]=='searchTble'){
				url += $j('#sipfilteparams').val();
			}
			refershMethod = 1;
		}else{
			url += "&paginateModule=";
		}
		if(totalparams[2]!="" && totalparams[2]!="temp"){
			url=url+"&cbuid="+$j('#'+totalparams[2]).val();
		}
		
		if(totalparams[3] == "notrequired"){
			loadPageByGetRequset(url,totalparams[4]);
			fundingrptsPaginateTable();
		}
		if(totalparams.length == 6){
			if(refershMethod==0){
				//alert("condition1...........")
				refreshDiv(url,totalparams[4],totalparams[3]);
				
			}
			else{
				//alert("condition2...........")
				if(totalparams[0]=="getMainCBUDetailsPaginationData"){
					url=url+'&filterTxt='+$j('#lookup_search_txt').val();
					loadDivWithFormSubmit(url,totalparams[4],totalparams[3]);
				}else if(totalparams[0]=="getCordFileUploadPaginationData"){
					url=url+'&filterTxt='+$j('#cimport_search_txt').val();
					loadDivWithFormSubmit(url,totalparams[4],totalparams[3]);
				}else if(totalparams[0]=="openOrderData"){
					url=url+'&filterTxt='+$j('#udoc_search_txt').val();
					loadDivWithFormSubmit(url,totalparams[4],totalparams[3]);
				}else if(totalparams[0]=="getSimpleQueryPaginationData"){
					url=url+'&filterTxt='+$j('#lookup_search_txt').val()+'&currentpage='+$j('#query_builder_tbl').val();
					loadDivWithFormSubmitNew(url,totalparams[4],totalparams[3]);
				}else{
					loadDivWithFormSubmit(url,totalparams[4],totalparams[3]);
				}				
			}
			
		}
		
		if(totalparams.length == 7 && (totalparams[6]== "ord_sortParam" || totalparams[6]== "proc_tbl_param") ){
			
			if(refershMethod==0 && totalparams[6]!= "proc_tbl_param") {
				url = url + $j('#'+totalparams[6]).val();
			    refreshDiv(url,totalparams[4],totalparams[3]);
			}
			else if(refershMethod==0 && totalparams[6]== "proc_tbl_param"){
				url = url + $j('#'+totalparams[6]).val();
				url=url+'&filterTxt='+$j('#proc_filter_value').val();
			    refreshDiv(url,totalparams[4],totalparams[3]);
				
			}else{
				url = url + $j('#'+totalparams[6]).val();
			    loadDivWithFormSubmit(url,totalparams[4],totalparams[3]);
			}
		}
		
		
		if(totalparams.length == 7 || totalparams.length == 8 || totalparams.length == 9 || totalparams.length == 10 || totalparams.length == 11 || totalparams.length == 12){
			if(totalparams.length == 7 && totalparams[6]!="ordParams" && totalparams[6]!="workflowParam" && totalparams[6]!="alertParam" && totalparams[6]!="ord_sortParam" && totalparams[6]!= "proc_tbl_param"){
				url=url+"&pkcordId="+$j('#'+totalparams[6]).val();
				if(totalparams[0]=="getCbuHistory"){
					url=url+'&filterTxt='+$j('#history_search_txt').val();
				}
				if(totalparams[0]=="getCbbDetails"){
					url=url+'&filterTxt='+$j('#'+totalparams[6]).val();
				}
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			if(totalparams.length == 9 && (totalparams[6]=="ordParams" || totalparams[6]=="workflowParam" || totalparams[6]=="alertParam")){
				//alert("inside correct location")
				url=url+$j('#'+totalparams[6]).val()+'&allEntries='+$j('#'+totalparams[8]).val();
				
				if(totalparams[6]=="ordParams"){
					url=url+'&filterTxt='+$j('#ord_filter_value').val();
				}
				if(totalparams[6]=="workflowParam"){
					url=url+'&filterTxt='+$j('#workflow_filter_value').val();
				}
				if(totalparams[6]=="alertParam"){
					url=url+'&filterTxt='+$j('#alrt_filter_value').val();
				}
				
				
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			
			
			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="cbuinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="idinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="procinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="licinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="eliginfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="revinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="labinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="hlainfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="cninfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="orinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="mcinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="idminfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="hhsinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="docinfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}

			if(totalparams.length == 10 && totalparams[7]=="entityId" && totalparams[9]=="pathlainfo_tbl_param"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8]+$j('#'+totalparams[9]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			
			if(totalparams.length == 9 && totalparams[7]=="entityId"){
				url=url+"&entityId="+$j('#'+totalparams[7]).val()+"&cordID="+$j('#'+totalparams[6]).val()+"&widget="+totalparams[8];
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			
			
			if(totalparams.length == 8  && totalparams[6]!="workflowParam" && totalparams[6]!="alertParam" && totalparams[6]!="ordParams"){
				url=url+"&cordId="+$j('#'+totalparams[6]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
			}
			
			if(totalparams.length == 10 && (totalparams[6]=="sipflag" || totalparams[6]=="naCBUflag")){
				url=url+$j('#'+totalparams[7]).val()+'&allEntries='+$j('#'+totalparams[9]).val();
				
				if(totalparams[6]=="sipflag"){
					url=url+'&filterTxt='+$j('#sip_filter_value').val();
				}
				if(totalparams[6]=="naCBUflag"){
					url=url+'&filterTxt='+$j('#nacbu_filter_value').val();
				}
				
				loaddiv(url,totalparams[4]);
			}
			
		
			if(totalparams.length == 9 && totalparams[6]!="filterflag" && totalparams[6] != "naCBUflag" && totalparams[6]!="sipflag" && totalparams[6]!="ordParams" && totalparams[6]!="workflowParam" && totalparams[6]!="alertParam"){
				url=url+"&cbbId="+$j('#'+totalparams[6]).val()+"&cbbName="+$j('#'+totalparams[7]).val();
				refreshDiv(url,totalparams[4],totalparams[3]);
				$j('#'+totalparams[4]).show();
			}
			
			if(totalparams.length == 12 && totalparams[6]=="pendingFilter"){
				url=url+"&pksiteid="+$j('#'+totalparams[7]).val()+"&fkordertype="+$j('#'+totalparams[8]).val()+"&filterPending="+$j('#'+totalparams[6]).val()+$j('#'+totalparams[9]).val()+'&allEntries='+$j('#'+totalparams[11]).val();
				
				if(totalparams[6]=="pendingFilter"){
					url=url+'&filterTxt='+$j('#ord_filter_value').val();
				}
				loaddiv(url,totalparams[4]);
			}			
		}
		/*if(totalparams.length == 14 && module=="advanceLookUp"){
			var cbusearchId = "";
			var collectionDateFromStr = "";
			var collectionDateToStr = "";
			var submitionDateFromStr = "";
			var submitionDateToStr = "";
			if(totalparams[6].indexOf("_")!=-1){
				cbusearchId = totalparams[6];
				cbusearchId = cbusearchId.replace("_",",");
			}else{
				cbusearchId = totalparams[6];
			}
			if(totalparams[8].indexOf("_")!=-1){
				collectionDateFromStr = totalparams[8];
				collectionDateFromStr = collectionDateFromStr.replace("_",",");
			}else{
				collectionDateFromStr = totalparams[8];
			}
			if(totalparams[9].indexOf("_")!=-1){
				collectionDateToStr = totalparams[9];
				collectionDateToStr = collectionDateToStr.replace("_",",");
			}else{
				collectionDateToStr = totalparams[9];
			}
			if(totalparams[10].indexOf("_")!=-1){
				submitionDateFromStr = totalparams[10];
				submitionDateFromStr = submitionDateFromStr.replace("_",",");
			}else{
				submitionDateFromStr = totalparams[10];
			}
			if(totalparams[11].indexOf("_")!=-1){
				submitionDateToStr = totalparams[11];
				submitionDateToStr = submitionDateToStr.replace("_",",");
			}else{
				submitionDateToStr = totalparams[11];
			}
			url=url+"&cbusearchId="+cbusearchId+"&searchId="+totalparams[7]+"&collectionDateFromStr="+collectionDateFromStr+"&collectionDateToStr="+collectionDateToStr+"&submitionDateFromStr="+submitionDateFromStr+"&submitionDateToStr="+submitionDateToStr+"&fkCordCbuEligible="+totalparams[12]+"&cbuLicStatus="+totalparams[13];
			loadDivWithFormSubmit(url,totalparams[4],totalparams[3]);
		}*/
		//alert("totalparams[5]"+totalparams[5])
		if(totalparams[5]!=""){
			window[totalparams[5]]();
			if(totalparams.length == 10 && (totalparams[6]=="sipflag" || totalparams[6]=="naCBUflag")){
				removingCSS();
			}
		}
		if(inputText=='ALL'){
    		 $j("#"+totalparams[1]+" option:contains(" + inputText + ")").attr('selected', 'selected'); 
        }else{
			 $j("#"+totalparams[1]).val(showRowval);
		}	
	 }
	
	function setValueFromId(val,destinationId,valueSetFromId,rights){
		if($j('#'+valueSetFromId).val()!=null && $j('#'+valueSetFromId).val()!="" && $j('#'+valueSetFromId).val()!=undefined
				&& $j('#'+rights).val()!=null && $j('#'+rights).val()!="" && $j('#'+rights).val()!=undefined){
			if($j('#'+rights).val()=='false'){			
			        $j('#'+destinationId).val($j('#'+valueSetFromId).val());								
		   }
	    }
	}
	
	$j(function(){
		$j(".copyPaste").keypress(function(e) {
			var key;
			var keychar;
			var isCtrl;
			var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j','t');

		//controls a=select all, n=new window, c=copy, x=cut, v=paste, j=focus will go to google search , t=new tab.
		 
		if (window.event){
			key = window.event.keyCode;
			if(window.event.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}else if (e){
			key = e.which;
			if(e.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}
		else
		return true;
		if(isCtrl){
			for(i=0; i<forbiddenKeys .length; i++){
				if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase()){
					//alert('Key combination CTRL + '+String.fromCharCode(key)+' has been disabled.');
					return false;
				}
			}
		}
		if((key == 8) || (key == 0))
			return true;
	     });
		
		$j(".copyPaste").keydown(function(e) {
			var key;
			var keychar;
			var isCtrl;
			var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j','t');

		//controls a=select all, n=new window, c=copy, x=cut, v=paste, j=focus will go to google search , t=new tab.
		 
		if (window.event){
			key = window.event.keyCode;
			if(window.event.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}else if (e){
			key = e.which;
			if(e.ctrlKey)
				isCtrl = true;
			else
				isCtrl = false;
		}
		else
		return true;
		if(isCtrl){
			for(i=0; i<forbiddenKeys .length; i++){
				if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase()){
					//alert('Key combination CTRL + '+String.fromCharCode(key)+' has been disabled.');
					return false;
				}
			}
		}
		if((key == 8) || (key == 0))
			return true;
	     });
		$j(document).keydown(function(e) { 
			var target = $j(e.target);
			//alert("$target::"+target);
			 var element = e.target.nodeName.toLowerCase();
			//alert("element::"+element);
			 var charCode = (e.which) ? e.which : event.keyCode;
			//alert("charCode:::"+charCode);
		         if (element != 'input' && element != 'textarea') {
			//	alert("check if");
			    if (charCode === 8) {
			        return false;
			    }
			 }else{
			//	alert("check else");
				var readonly = $j(target).attr("readonly");
			//	alert("readonly::"+readonly);
				var disabled_or_readonly = $j(target).is(':disabled');
//				alert("disabled_or_readonly::"+disabled_or_readonly);
				if ((disabled_or_readonly || readonly) && charCode === 8)
					return false;
			}
		        return true;
		});
	});
	
	function modifyDocument(cdrCbuId,attachmentId,cbuRegisteryId,dcmsAttachId){
			var url='modifyUnitrpt?cbuCordId='+cdrCbuId+'&attachmentId='+attachmentId+'&modifyUnitrpt=True'+'&modifyDoc=True'+'&cbuRegisteryId='+cbuRegisteryId;
			showModal('Modify Document',url,'500','500');
			
	}
	
	function revokeDocument(cdrCbuId,attachmentId,dcmsAttachId,replaceDivId){
		
		var answer = confirm('Please confirm if you want to revoke the document.');
		if (answer){	
					var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&attachmentId='+attachmentId;
					/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
					var processingSaveDialog = document.createElement('div');
					processingSaveDialog.innerHTML=progressMsg;		
					$j(processingSaveDialog).dialog({autoOpen: false,
						resizable: false, width:400, height:90,closeOnEscape: false,
						modal: true}).siblings('.ui-dialog-titlebar').remove();		
					$j(processingSaveDialog).dialog("open");*/
					showprogressMgs();
								$j.ajax({
									type : "POST",
									async : false,
									url : url,
									//data : $j("#searchOpenOrders2").serialize(),
									success : function(result) {
										$j("#"+replaceDivId).html(result);
										//refreshMain(cdrCbuId);
										refreshMultipleDivsOnViewClinical('639',cdrCbuId);
										alert("Document revoked successfully");
									},
									error: function (request, status, error) {
										closeprogressMsg();
										alert("Error " + error);
										alert(request.responseText);
									}
							});
								closeprogressMsg();
		}
}
	function refreshMain(cbuid){
		loadPageByGetRequset('refreshMain?cordId='+cbuid,'main');
	}
	
function revokeDocumentForModal(cdrCbuId,attachmentId,dcmsAttachId,title,url1,lenght,breadth){
		
		var answer = confirm('Please confirm if you want to revoke the document.');
		if (answer){	
	
					var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&attachmentId='+attachmentId;
					/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
					var processingSaveDialog = document.createElement('div');
					processingSaveDialog.innerHTML=progressMsg;		
					$j(processingSaveDialog).dialog({autoOpen: false,
						resizable: false, width:400, height:90,closeOnEscape: false,
						modal: true}).siblings('.ui-dialog-titlebar').remove();		
					$j(processingSaveDialog).dialog("open");*/
					showprogressMgs();
								$j.ajax({
									type : "POST",
									async : false,
									url : url,
									//data : $j("#searchOpenOrders2").serialize(),
									success : function(result) {
										//$j("#main").html(result);
										if(url1!=null && url1!=""){
											subFunForRevokeDocForModal(title,url1,lenght,breadth);
											}
										
										alert("Document revoked successfully");
									},
									error: function (request, status, error) {
										closeprogressMsg();
										alert("Error " + error);
										alert(request.responseText);
									}
							});
								closeprogressMsg();
		}
}
	
	function subFunForRevokeDocForModal(title,url,lenght,breadth){
		showModal(title,url,lenght,breadth);
	}
	
	
	function checkDates(id1,id2){
		var flag = false;
		var val1 = $j("#"+id1).val();
		var val2 = $j("#"+id2).val();
		if(val1!=null && val2!=null && val1!="" && val2!="" && val1!=undefined && val2!=undefined){
			date1 = new Date(val1);
			//date2 = $.datepicker.parseDate('yy-mm-dd', val2);
			date2 = new Date(val2);
			//datearr1 = date1.split("-");
			//datearr2 = date2.split("-");   
			year1 = date1.getFullYear();
			month1 = date1.getMonth();
			day1 = date1.getDate();
			year2 = date2.getFullYear();
			month2 = date2.getMonth();
			day2 = date2.getDate();
			if(year1<year2){
				return true;
			}else if(year1>year2){
				return false;
			}else if(year1 == year2){
					if(month1<month2){
						return true;
					}else if(month1>month2){
						return false;
					}else if(month1==month2){
							if(day1<day2){
								return true;
							}else if(day1>day2){
								return false;
							}else if(day1==day2){
								return true;
							}
					}
			}
			
		}else if(val2==null && val2==""){
			return true;
		}else{
			return false;
		}
	}
	
	function checkEligDates(id1,id2){
		var flag = false;
		var val1 = $j("#"+id1).val();
		var val2 = $j("#"+id2).val();
		if(val1!=null && val2!=null && val1!="" && val2!="" && val1!=undefined && val2!=undefined){
			//date1 = $j.datepicker.parseDate('yy-mm-dd', val1);
			date1 = val1;
			//date2 = $j.datepicker.parseDate('yy-mm-dd', val2);
			date2 = val2;
			datearr1 = date1.split("-");
			datearr2 = date2.split("-");			
			year1 = datearr1[0];
			month1 = datearr1[1];
			day1 = datearr1[2];
			year2 = datearr2[0];
			month2 = datearr2[1];
			day2 = datearr2[2];
			if(year1>year2){
				return true;
			}else if(year1<year2){
				return false;
			}else if(year1 == year2){
					if(month1>month2){
						return true;
					}else if(month1<month2){
						return false;
					}else if(month1==month2){
							if(day1>day2){
								return true;
							}else if(day1<day2){
								return false;
							}else if(day1==day2){
								return true;
							}
					}
			}
			
		}else if(val2==null && val2==""){
			return true;
		}else{
			return false;
		}
	}
	
	function checkHour(id1,id2){
		var flag = false;
		var val1 = $j("#"+id1).val();
		var val2 = $j("#"+id2).val();
		if(val1!=null && val2!=null && val1!="" && val2!="" && val1!=undefined && val2!=undefined){
			date1 = new Date(val1);
			//date2 = $.datepicker.parseDate('yy-mm-dd', val2);
			date2 = new Date(val2);
			//datearr1 = date1.split("-");
			//datearr2 = date2.split("-");   
			year1 = date1.getFullYear();
			month1 = date1.getMonth();
			day1 = date1.getDate();
			year2 = date2.getFullYear();
			month2 = date2.getMonth();
			day2 = date2.getDate();
			var diff = Math.abs(new Date(year1, month1, day1) - new Date(year2, month2, day2));
		    if (Math.floor(diff/86400000)) {
		    	if( Math.floor(diff/86400000) >2){
		        	return false;
		        }else{
		        	return true;
		        }
		    } else if (Math.floor(diff/3600000)) {
		    	if(Math.floor(diff/3600000) >48 ){
		        	return false;
		        }else{
		        	return true;
		        }
		    } else if (Math.floor(diff/60000)) {
		    	return true;
		    } else{
		    	return true;
		    }
			
		}else if(val2==null && val2==""){
			return true;
		}else{
			return false;
		}
	}
	
	function dateTimeFormat(val){
		var flag = false;
		var mflag = false;
		var amflag = false;
		var totflag = false;
		//var totflag2 = false;
		var totval = val;
		    while(val.indexOf(":")!=-1){
		    	index = val.indexOf(":");
		    	val = val.substr(0,index)+val.substr(index+1,val.length-(index+1));
		    	if(index==2){
		    		totflag = true;
		    	}
		    }
		    if(totflag){
		       if(totval!=null && totval!="" && totval!=undefined){
					var valarr = totval.split(":");
					if(valarr[0]!=null && valarr[0]!="" && valarr[0]!=undefined){
						valar0 = valarr[0];
						checkhour = isNaN(valar0);
						if(!checkhour){
							if(valar0<=23){
								flag = true;
							}
						}
				    }
					if(valarr[1]!=null && valarr[1]!="" && valarr[1]!=undefined){
						valar1 = valarr[1];
						checkmm = isNaN(valar1);
						if(!checkmm){
							if(valar1<=59){
								mflag = true;								
							}
						}
						if(valar1.length<2){
							mflag = false;
						}
					}
					/*if(valarr[2]!=null && valarr[2]!="" && valarr[2]!=undefined){
						valar2 = valarr[2];
						checkam = isNaN(valar2);
						if(checkam){
							if(valar2.toUpperCase()=="AM" || valar2.toUpperCase()=="PM"){
								amflag = true;								
							}							
						}
					}*/			
		         }
		       return (flag && mflag);
		    }else{
		    	return false;
		    }
	}

function getDatePic(){
	getClearButton();
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;

	if($j( ".datePicWMaxDate" ) && $j( ".datePicWMaxDate" ).length>0){
		$j( ".datePicWMaxDate" ).removeAttr("readonly").addClass("dpDate").each(function(){
			if($j(this).is(":disabled")){
				$j(this).datepicker("disable");
			}
			var s = this;
	        var selectHolder = $j('<span>').css({ position: 'relative' });
	        
	        if($j("#placeHolderOfid_"+$j(this).attr("id")).length==0){
		    $j(this).wrap(selectHolder);
	        var titleSpan = $j('<label>').attr({'class': 'tooltipspan placeHolderOfid_'+$j(this).attr("id"),'id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
		                				.css({'width':0,'height':0,'top':-5,'left':5}).click(function(){
		                					var txtFldId = $j(this).attr("id"); 
		                					txtFldId = txtFldId.replace("placeHolderOfid_","");
		                					if(!$j("#"+txtFldId).is(":disabled")){
			                					$j("#"+txtFldId).focus();
				                			}
		                				});
			if($j(this).val()!=""){
				$j(titleSpan).css({'display':'none'});
			}else{
				$j(titleSpan).css({'display':'block'});
			}
	        	$j(this).parent().append(titleSpan);
	        }
		}).datepicker({
			dateFormat: 'M dd, yy',
			maxDate: new Date(y, m, d),
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true, 
			prevText: '',
			nextText: '',
			showOn: "button",
			buttonImage: "./images/calendar-icon.png",
			buttonImageOnly: true,

	        /* blur needed to correctly handle placeholder text */
	        onSelect: function(dateText, inst) {
	              $j(this).blur().change().focus();
	        },
		    onChangeMonthYear:function(y, m, i){
		        var d = i.selectedDay;
		        $j(this).datepicker('setDate', new Date(y, m - 1, d));
		        $j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
		    }
		}).focus(function(){
			$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
		}).keydown(function(e) {
			  var val = $j(this).val();
			  if(e.keyCode==191 || e.keyCode==111){
				  return false;
			  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
				  if(val.length==2 || val.length==5){
					  $j(this).val(val+"/");
				  }
			  }else if(e.keyCode==8){
				if(val==""){
					return false;
				}
			  }
		}).focusout(function(){
			if($j(this).val()!=""){
				$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
			}else{
				$j(".placeHolderOfid_"+$j(this).attr("id")).show();
			}
		}).removeAttr("placeholder");
	}
	if($j( ".datePicWMinDate" ) && $j( ".datePicWMinDate" ).length>0){
		$j( ".datePicWMinDate" ).removeAttr("readonly").addClass("dpDate").each(function(){
			if($j(this).is(":disabled")){
				$j(this).datepicker("disable");
			}
			var s = this;
	        var selectHolder = $j('<span>').css({ position: 'relative' });
	        
	        if($j("#placeHolderOfid_"+$j(this).attr("id")).length==0){
		    $j(this).wrap(selectHolder);
	        var titleSpan = $j('<label>').attr({'class': 'tooltipspan placeHolderOfid_'+$j(this).attr("id"),'id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
		                				.css({'width':0,'height':0,'top':-5,'left':5}).click(function(){
		                					var txtFldId = $j(this).attr("id"); 
		                					txtFldId = txtFldId.replace("placeHolderOfid_","");
		                					if(!$j("#"+txtFldId).is(":disabled")){
			                					$j("#"+txtFldId).focus();
				                			}
		                				});
			if($j(this).val()!=""){
				$j(titleSpan).css({'display':'none'});
			}else{
				$j(titleSpan).css({'display':'block'});
			}
	        	$j(this).parent().append(titleSpan);
	        }
		}).datepicker({
			dateFormat: 'M dd, yy',
			minDate: new Date(y, m, d),
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true, 
			prevText: '',
			nextText: '',
			showOn: "button",
			buttonImage: "./images/calendar-icon.png",
			buttonImageOnly: true,

	        /* blur needed to correctly handle placeholder text */
	        onSelect: function(dateText, inst) {
	              $j(this).blur().change().focus();
	        },
		    onChangeMonthYear:function(y, m, i){                                
		        var d = i.selectedDay;
		        $j(this).datepicker('setDate', new Date(y, m - 1, d));
		        $j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
		    }
		}).focus(function(){
			$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
		}).keydown(function(e) {
			  var val = $j(this).val();
			  if(e.keyCode==191 || e.keyCode==111){
				  return false;
			  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
				  if(val.length==2 || val.length==5){
					  $j(this).val(val+"/");
				  }
			  }else if(e.keyCode==8){
				if(val==""){
					return false;
				}
			  }
		}).focusout(function(){
			if($j(this).val()!=""){
				$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
			}else{
				$j(".placeHolderOfid_"+$j(this).attr("id")).show();
			}
		}).removeAttr("placeholder");
	}
	if($j( ".datePicWOMinDate" ) && $j( ".datePicWOMinDate" ).length>0){
		$j( ".datePicWOMinDate" ).removeAttr("readonly").addClass("dpDate").each(function(){
			if($j(this).is(":disabled")){
				$j(this).datepicker("disable");
			}
			var s = this;
	        var selectHolder = $j('<span>').css({ position: 'relative' });
	        
	        if($j("#placeHolderOfid_"+$j(this).attr("id")).length==0){
		    $j(this).wrap(selectHolder);
	        var titleSpan = $j('<label>').attr({'class': 'tooltipspan placeHolderOfid_'+$j(this).attr("id"),'id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
		                				.css({'width':0,'height':0,'top':-5,'left':5}).click(function(){
		                					var txtFldId = $j(this).attr("id"); 
		                					txtFldId = txtFldId.replace("placeHolderOfid_","");
		                					if(!$j("#"+txtFldId).is(":disabled")){
			                					$j("#"+txtFldId).focus();
				                			}
		                				});
			if($j(this).val()!=""){
				$j(titleSpan).css({'display':'none'});
			}else{
				$j(titleSpan).css({'display':'block'});
			}
				$j(this).parent().append(titleSpan);
			}
		}).datepicker({
			dateFormat: 'M dd, yy',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true, 
			prevText: '',
			nextText: '',
			showOn: "button",
			buttonImage: "./images/calendar-icon.png",
			buttonImageOnly: true,

	        /* blur needed to correctly handle placeholder text */
	        onSelect: function(dateText, inst) {
	              $j(this).blur().change().focus();
	        },
		    onChangeMonthYear:function(y, m, i){                                
		        var d = i.selectedDay;
		        $j(this).datepicker('setDate', new Date(y, m - 1, d));
		        $j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
		    }
		}).focus(function(){
			$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
		}).keydown(function(e) {
			  var val = $j(this).val();
			  if(e.keyCode==191 || e.keyCode==111){
				  return false;
			  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
				  if(val.length==2 || val.length==5){
					  $j(this).val(val+"/");
				  }
			  }else if(e.keyCode==8){
				if(val==""){
					return false;
				}
			  }
		}).focusout(function(){
			if($j(this).val()!=""){
				$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
			}else{
				$j(".placeHolderOfid_"+$j(this).attr("id")).show();
			}
		}).removeAttr("placeholder");
	}
}
function onFocusCall(element){
	if($j(element).val()!=""){
		var thisdate = new Date($j(element).val());
		if(!isNaN(thisdate)){
			var formattedDate = dateFormat($j(element).val(),'mm/dd/yyyy');
			$j(element).val(formattedDate);
		}
	}
}
function readNMDPXML(linkId){
	var nmdpLink = readXml1("NMDP-Links.xml");
	$j(nmdpLink).find('links[id="'+linkId+'"]').find('url').each(function() {
		var url=$j(this).text();
		window.open(url);
    });
}

function getJQButton(){
    $j('button').button();
    $j('input type[button]').button();
    $j('input type[submit]').button();
}
function showSelectedValue(obj){
	var fieldId=$j(obj).attr('id');
	var selectedText=$j('#'+fieldId+' option:selected').text();
	overlib(selectedText);
}
function getHiddedValues(){
    /*$j('select').each(function () {
    	if($j(this).is(':visible')){
	        var i = 0;
	        var s = this;
	        for (i = 0; i < s.length; i++){
	            s.options[i].title = s.options[i].text;
	        }
	        if (s.selectedIndex > -1) {
	            s.onmousemove = function () { s.title = s.options[s.selectedIndex].text; };
	
	            if (this.disabled) {
	                var selectHolder = $j('<span>').css({ position: 'relative' });
	                $j(this).wrap(selectHolder);
	
	                var titleSpan = $j('<span>').attr({'class': 'tooltipspan'}).html(s.options[s.selectedIndex].text)
	                							.css({'width':$j(s).width(),'height':$j(s).height(),'top':-5,'left':5,'background-color':'#D49DAB','opacity':0})
	                							.hover(function(){overlib($j(this).html());},function(){nd();});
	                $j(this).parent().append(titleSpan);
	            }
	        }
    	}
    });
    $j('input:text').each(function () {
    	if($j(this).is(':visible')){
	        var i = 0;
	        var s = this;
	        s.title = s.value;
	        s.onmousemove = function () { s.title = s.value; };
	        if (this.disabled) {
	        	var selectHolder = $j('<span>').css({ position: 'relative' });
	            $j(this).wrap(selectHolder);
	            var titleSpan = $j('<span>').attr({'class': 'tooltipspan' }).html(s.value)
	            							.css({'width':$j(s).width(),'height':$j(s).height(),'top':-5,'left':5,'background-color':'#D49DAB','opacity':0})
	            							.hover(function(){if($j.trim($j(this).html()).length>0){overlib($j(this).html());}},function(){nd();});
	            $j(this).parent().append(titleSpan);
	        }
	    	}
    });*/
}


function viewUploadedDocuments(title,url,height,width,divname)
{      
	$j('.progress-indicator').css( 'display', 'block');
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#"+divname).html(progressMsg);
		$j("#"+divname).dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#"+divname).dialog("destroy");
				    }
				   }
				  ); 
		$j("#"+divname).dialog("open");
		
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#"+divname).html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
	//closeModal(); 
	$j("#modelPopup1").dialog("close");
	// $j("#modelPopup").dialog("close");
	// $j("#modelPopup1").dialog("destroy");
	 //$j("#modelPopup").dialog("destroy");
}

function closeDialog(divname)
{
	jQuery("#"+divname).dialog("destroy");
}
var refreshFlag = false;
function refreshMultipleDivsOnViewClinical(divsLoaded,cordID){
	setTimeout(function(){
		if(divsLoaded&1){
			refreshFlag = true;
			refreshClinicalSubModuleData(cordID,'1','loadCbuCharacteristicDiv','cbuCharacteristicsparent','50','');
		}
	},0);
	setTimeout(function(){
		if(divsLoaded&2){
			refreshFlag = true;
			refreshClinicalSubModuleData(cordID,'2','loadEligibilityDiv','eligibilityInfoparent','130','');
		}
	},0);
	setTimeout(function(){
	    if(divsLoaded&4){
	    	refreshFlag = true;
	    	refreshClinicalSubModuleData(cordID,'3','loadIdmDataDiv','infectDiseaseparent','290','8');
		}
	},0);
	setTimeout(function(){
	    if(divsLoaded&8){
	    	refreshFlag = true;
	    	refreshClinicalSubModuleData(cordID,'4','loadOthersDataDiv','otherTestResultparent','2','');
		}
	},0);
	setTimeout(function(){
	    if(divsLoaded&16){
	    	refreshFlag = true;
	    	refreshClinicalSubModuleData(cordID,'5','loadCordIdsDataDiv','cbuIdsparent','2','');
		}
	},0);
	setTimeout(function(){
	    if(divsLoaded&32){
	    	refreshFlag = true;
	    	refreshClinicalSubModuleData(cordID,'6','loadProcessingProcedureDataDiv','cbuprocessinginfodivparant','554','')
		}
	},0);
	setTimeout(function(){
		if(divsLoaded&64){
			refreshFlag = true;
			refreshClinicalSubModuleData(cordID,'7','loadHlaDataDiv','cbuHlaparent','38','');
		}
	},0);
	setTimeout(function(){
		if(divsLoaded&128){
			refreshFlag = true;
			refreshClinicalSubModuleData(cordID,'8','loadFormsDataDiv','healthHistoryparent','290','6')
		}
	},0);
	setTimeout(function(){
	if(divsLoaded&256){
		refreshFlag = true;
		refreshClinicalSubModuleData(cordID,'9','loadWholeNotesDiv','clinicalNotesparent','2','')
	}
	},0);
	setTimeout(function(){
		if(divsLoaded&512){
			refreshFlag = true;
			refreshClinicalSubModuleData(cordID,'10','loadLabSummaryDiv','labsummarydivparent','98','');
		}
	},0);
	setTimeout(function(){
		if(divsLoaded&1024){
			refreshClinicalSubModuleData(cordID,'11','loadStaticPanelDiv','','','');
		}
	},0);
}


function refreshClinicalSubModuleData(cordID,module,divName,parentDivName,loadData,formData){
	switch(module){
	case '1':
		flagCbuInfo = true;
		break;
	case '2':
		flagFinalElig = true;
		break;
	case '3':
		flagmIdm = true;
		break;
	case '4':
		flagOtherData = true;
		break;
	case '5':
		flagCordId = true;
		break;
	case '6':
		flagCordProc = true;
		break;
	case '7':
		flagHla = true;
		break;
	case '8':
		flagHealthHistory = true;
		break;
	case '9':
		flagNoteList = true;
		break;
	case '10':
		flagLabSummary = true;
		break;
	case '11':
		flagmStaticPanel = true;
		break;
	}
	loadClinicalSubModuleData(cordID,module,divName,parentDivName,loadData,formData);
}

function loadClinicalSubModuleData(cordID,module,divName,parentDivName,loadData,formData,max_widget_obj){
   // var url = "load_cb_load_cbu_characteristic?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
	var flag = false;
   var url = "load_";
	switch(module){
	case '1':
		url = url + "cb_load_cbu_characteristic";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		flag = flagCbuInfo;
		break;
	case '2':
		url = url + "cb_load_eligibility_final_cbu_review";
		if(mode=="maximize"){
			url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData+"&modalWindow=1";
		}
		else{
			url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		}
		flag = flagFinalElig;
	    break;
	case '3':
		url = url + "cb_load_idm_data";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData+"&formDataToBeLoaded="+formData;
		flag = flagmIdm;
	    break;
	case '4':
		url = url + "cb_load_other_data";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagOtherData;
	    break;
	case '5':
		url = url + "cb_load_cord_ids_info";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		flag = flagCordId;	
	    break;
	case '6':
		url = url + "cb_load_cord_proc_info";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagCordProc;
	    break;
	case '7':
		url = url + "cb_load_hla_data";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagHla;
	    break;
	case '8':
		url = url + "cb_load_health_history_screen";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData+"&formDataToBeLoaded="+formData;
		flag = flagHealthHistory;
	    break;
	case '9':
		url = url + "cb_load_note_list";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagNoteList;
	    break;
	case '10':
		url = url + "cb_labSummeryForCordInfo";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		flag = flagLabSummary;	
	    break;
	case '11':
		url = url + "cb_load_static_panel";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded=0";
		flag = flagmStaticPanel;	
		break;
	}
	if(module=='11'){
		loadPageByGetRequset(url,divName);
	}else if(flag){
		$j("#"+parentDivName).find(".ui-icon").each(function(){
			if(($j(this).is(".ui-icon-plusthick") || mode=="maximize") && max_widget_obj !=null && max_widget_obj !=="undefined" && max_widget_obj !="" && max_widget_obj.maxwidget==="maxwidget"){
				setWidgetVar(max_widget_obj.widgetid,url);
				mode="";
				return false;
			}
			else if($j(this).is(".ui-icon-plusthick") || refreshFlag == true)
			{
				  loadPageByGetRequset(url,divName);
				  refreshFlag = false;
				  setWidgetVarData(divName);
				  return false;			
			}
		});
	} else if(!flag){
		loadWidget(divName);
	}
	switch(module){
	case '1':
		flagCbuInfo = false;		
		break;
	case '2':
		flagFinalElig = false;
		break;
	case '3':
		flagmIdm = false;
		break;
	case '4':
		flagOtherData = false;
		break;
	case '5':
		flagCordId = false;
		break;
	case '6':
		flagCordProc = false;
		break;
	case '7':
		flagHla = false;
		break;
	case '8':
		flagHealthHistory = false;
		break;
	case '9':
		flagNoteList = false;
		break;
	case '10':
		flagLabSummary = false;
		break;
	case '11':
		flagmStaticPanel = false;
		break;
	}	
	resetSessionWarningOnTimer();
}


//========= Function added for clinical screen widgets to get progress bar. ==========================


function loadClinicalWidgetData(cordID,module,divName,parentDivName,loadData,formData,max_widget_obj){
   // var url = "load_cb_load_cbu_characteristic?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
	var flag = false;
   var url = "load_";
	switch(module){
	case '1':
		url = url + "cb_load_cbu_characteristic";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		flag = flagCbuInfo;
		break;
	case '2':
		url = url + "cb_load_eligibility_final_cbu_review";
		if(mode=="maximize"){
			url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData+"&modalWindow=1";
		}
		else{
			url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		}
		flag = flagFinalElig;
	    break;
	case '3':
		url = url + "cb_load_idm_data";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData+"&formDataToBeLoaded="+formData;
		flag = flagmIdm;
	    break;
	case '4':
		url = url + "cb_load_other_data";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagOtherData;
	    break;
	case '5':
		url = url + "cb_load_cord_ids_info";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		flag = flagCordId;	
	    break;
	case '6':
		url = url + "cb_load_cord_proc_info";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagCordProc;
	    break;
	case '7':
		url = url + "cb_load_hla_data";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagHla;
	    break;
	case '8':
		url = url + "cb_load_health_history_screen";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData+"&formDataToBeLoaded="+formData;
		flag = flagHealthHistory;
	    break;
	case '9':
		url = url + "cb_load_note_list";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;	
		flag = flagNoteList;
	    break;
	case '10':
		url = url + "cb_labSummeryForCordInfo";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded="+loadData;
		flag = flagLabSummary;	
	    break;
	case '11':
		url = url + "cb_load_static_panel";
		url+= "?cdrCbuPojo.cordID="+cordID+"&dataToBeLoaded=0";
		flag = flagmStaticPanel;	
		break;
	}
	if(module=='11'){
		loadPageByGetRequset(url,divName);
	}else if(flag){
		$j("#"+parentDivName).find(".ui-icon").each(function(){
			if(($j(this).is(".ui-icon-plusthick") || mode=="maximize") && max_widget_obj !=null && max_widget_obj !=="undefined" && max_widget_obj !="" && max_widget_obj.maxwidget==="maxwidget"){
				setWidgetVar(max_widget_obj.widgetid,url);
				mode="";
				return false;
			}
			else if($j(this).is(".ui-icon-plusthick") || refreshFlag == true)
			{
				  loadwidgetByGetRequset(url,divName);
				  refreshFlag = false;
				  setWidgetVarData(divName);
				  return false;			
			}
		});
	} else if(!flag){
		loadWidget(divName);
	}
	switch(module){
	case '1':
		flagCbuInfo = false;		
		break;
	case '2':
		flagFinalElig = false;
		break;
	case '3':
		flagmIdm = false;
		break;
	case '4':
		flagOtherData = false;
		break;
	case '5':
		flagCordId = false;
		break;
	case '6':
		flagCordProc = false;
		break;
	case '7':
		flagHla = false;
		break;
	case '8':
		flagHealthHistory = false;
		break;
	case '9':
		flagNoteList = false;
		break;
	case '10':
		flagLabSummary = false;
		break;
	case '11':
		flagmStaticPanel = false;
		break;
	}	
	resetSessionWarningOnTimer();
}


function loadParticularDiv(url,divname,formId,updateDivId,statusDivId){
	var result = "";
	var loadedDivName = "";
	showprogressMgs();
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
		  /*  while(loadedDivName==""){
		    	
		    }
		    completeReq = true;		    
		    alert("1"+loadedDivName);*/
		loadedDivName = getDivNameFromServer();
		/*loadedDivName = '<s:property value="#session.loadDivName" />';*/
		if(loadedDivName!=""){
	    	$j("#"+loadedDivName).html(result);
	    }
	    if(updateDivId!=undefined && updateDivId!=""){
			$j("#"+updateDivId).css('display','block');			            	
		}else{
			$j("#update").css('display','block');			            	
		}
		if(statusDivId!=undefined && statusDivId!=""){
			$j("#"+statusDivId).css('display','none');			            	
		}else{
			$j("#status").css('display','none');			            	
		}	
		    	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	closeprogressMsg();
	loadedDivName = "";
}

function loadParticularDivWithoutForm(url,divname,updateDivId,statusDivId){
	var result = "";
	var loadedDivName = "";
	$j('#progress-indicator').css( 'display', 'block' ); 
  var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#progress-indicator").html(progressMsg);
		$j("#progress-indicator").dialog(
				   {autoOpen: false,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : 300, height : 100,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#progress-indicator").dialog("destroy");
				    }
				   }
				  ); 
	$j("#progress-indicator").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
	$j("#progress-indicator").dialog("open");
	
	$j.ajax({
        type: "GET",
        url: url,
        async:false,
        success: function (result){
		  /*  while(loadedDivName==""){
		    	
		    }
		    completeReq = true;		    
		    alert("1"+loadedDivName);*/
		loadedDivName = getDivNameFromServer();
		/*loadedDivName = '<s:property value="#session.loadDivName" />';*/
		if(loadedDivName!=""){
	    	$j("#"+loadedDivName).html(result);
	    }
	    if(updateDivId!=undefined && updateDivId!=""){
			$j("#"+updateDivId).css('display','block');			            	
		}else{
			$j("#update").css('display','block');			            	
		}
		if(statusDivId!=undefined && statusDivId!=""){
			$j("#"+statusDivId).css('display','none');			            	
		}else{
			$j("#status").css('display','none');			            	
		}	
		    	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	
	jQuery("#progress-indicator").dialog("destroy");
    $j('#progress-indicator').css( 'display', 'none' );
	loadedDivName = "";
}

function getDivNameFromServer(){
	var divName = "";
		$j.ajax({
	        type: "POST",
	        url: "getDivNameFromServer",
	        async:false,
	        success: function (result){
			  divName = result.divName;
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
	
		});		
	return divName;
}

function closeModalsTemp(divname){
	$j("#"+divname).dialog("close");
	 $j("#"+divname).dialog("destroy");
}

function loadMoredivs(url,divname,formId,updateDiv,statusDiv){ 
	showprogressMgs();	
	var divnamearr = divname.split(",");
	var newdivname = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data:$j('#'+formId).serialize(),
	        success: function (result){
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{ 
		        	for(i=0;i<divnamearr.length;i++) {
		        		newdivname = divnamearr[i];
		        	var newDivContent=$j(result).find("#"+newdivname).html();
					//alert($j(result).find(".cbuDetailsDiv").html());
					//alert(newDivContent);
	        		$j("#"+newdivname).html(newDivContent);
		        		
		        	}
		        	if(statusDiv!='' && statusDiv!=null && statusDiv!='undefined' && updateDiv!='' && updateDiv!=null && updateDiv!='undefined'){
		        		$j('#'+statusDiv).hide();
	        			$j('#'+updateDiv).show();
		        	}
		        	//getHiddedValues();
		        	getDatePic();
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
	 closeprogressMsg();
	 resetSessionWarningOnTimer();
}
function loadMoredivsPF(url,divname,formId,updateDiv,statusDiv){ 
	showprogressMgs();	
	var divnamearr = divname.split(",");
	var newdivname = "";
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data:$j('#'+formId).serialize(),
	        success: function (result){
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{ 
		        	for(i=0;i<divnamearr.length;i++) {
		        		newdivname = divnamearr[i];
		        	var newDivContent=$j(result).find("#"+newdivname).html();
					//alert($j(result).find(".cbuDetailsDiv").html());
					//alert(newDivContent);
	        		$j("#"+newdivname).html(newDivContent);
		        		
		        	}
		        	if(statusDiv!='' && statusDiv!=null && statusDiv!='undefined' && updateDiv!='' && updateDiv!=null && updateDiv!='undefined'){
		        		$j('#'+statusDiv).hide();
	        			$j('#'+updateDiv).show();
		        	}
		        	//getHiddedValues();
		        	getDatePic();
		        	updateLastViewBy();
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
	 closeprogressMsg();
	 resetSessionWarningOnTimer();
}
function getSearchableSelect(Id,tag){
	
	var listData=100;
	
	var assignLstsize=0;
	if($j("#assignLstSize").val()!=undefined && $j("#assignLstSize").val()!=null && $j("#assignLstSize").val()!=0 && IsNumeric($j("#assignLstSize").val())){
		listData=Number($j("#assignLstSize").val())+4;
	}
	
	    
	if(tag=="id"){
		
		$j("#"+Id).searchable({
	        maxListSize: 100,                       // if list size are less than maxListSize, show them all
	        maxMultiMatch: listData,                      // how many matching entries should be displayed
	        exactMatch: false,                      // Exact matching on search
	        wildcards: true,                        // Support for wildcard characters (*, ?)
	        ignoreCase: true,                       // Ignore case sensitivity
	        latency: 200,                           // how many millis to wait until starting search
	        warnMultiMatch: 'top {0} matches ...',  // string to append to a list of entries cut short by maxMultiMatch
	        warnNoMatch: 'no matches ...',          // string to show in the list when no entries match
	        zIndex: 'auto'                          // zIndex for elements generated by this plugin
		});
		
	}
	    
	if(tag=="class"){
		$j("."+Id).searchable({
	        maxListSize: 100,                       // if list size are less than maxListSize, show them all
	        maxMultiMatch: listData,                      // how many matching entries should be displayed
	        exactMatch: false,                      // Exact matching on search
	        wildcards: true,                        // Support for wildcard characters (*, ?)
	        ignoreCase: true,                       // Ignore case sensitivity
	        latency: 200,                           // how many millis to wait until starting search
	        warnMultiMatch: 'top {0} matches ...',  // string to append to a list of entries cut short by maxMultiMatch
	        warnNoMatch: 'no matches ...',          // string to show in the list when no entries match
	        zIndex: 'auto'                          // zIndex for elements generated by this plugin
		});
	}
		
		
}
function IsNumeric(sText){
	   var ValidChars = "0123456789";
	   var IsNumber=true;
	   var Char;
	   if(sText.length==0){
		   IsNumber=false;
	   }	 
	   for (i = 0; i < sText.length && IsNumber == true; i++){ 
	      Char = sText.charAt(i); 
	      if (ValidChars.indexOf(Char) == -1){
	         IsNumber = false;
	      }
	   }
	   return IsNumber;
}
function getClearButton(){
	//wrap up the redraw function with our new shiz
	
	var dpFunc = $j.datepicker._generateHTML; //record the original
	$j.datepicker._generateHTML = function(inst){
		var thishtml = $j( dpFunc.call($j.datepicker, inst) ); //call the original
		
		thishtml = $j('<div />').append(thishtml); //add a wrapper div for jQuery context
		
		//locate the button panel and add our button - with a custom css class.
		
		//var len = $j(".ui-datepicker-buttonpane",thishtml).find('button').length;

		//if(len === 2){
			$j(".ui-datepicker-buttonpane",thishtml).html("");
			$j('.ui-datepicker-buttonpane', thishtml).append(
				$j('<button class="\
					ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all\
					"\>Clear</button>'
				).click(function(){
					inst.input.attr('value', '');
					inst.input.trigger('change');
					inst.input.focus();
					setTimeout(function(){
						inst.input.datepicker('hide');
						$j("#placeHolderOfid_"+inst.input.attr("id")).hide();
					},0);
				})
			);
		//}
		thishtml = thishtml.children(); //remove the wrapper div
		
		return thishtml; //assume okay to return a jQuery
	};
}


function cancelBack(){
	/*if ( $j.browser.msie ) {
	    if(event.keyCode == 8 || (event.keyCode == 37 && event.altKey) || (event.keyCode == 39 && event.altKey)){   
			event.cancelBubble = true;   
			event.returnValue = false;   
	    }
	}*/
}
function checkCurrentDateCordDate(datePickTxtFldId,msg){
	 var today = new Date().toLocaleString();
	 var babyBrthDate=new Date($j("#"+datePickTxtFldId).val()).toLocaleString();
	 var todayCompStr=today.split(" ");
	 var babyBrthDateCompStr=babyBrthDate.split(" ");
	 todayCompStr=todayCompStr[1]+todayCompStr[2]+todayCompStr[3];
	 babyBrthDateCompStr=babyBrthDateCompStr[1]+babyBrthDateCompStr[2]+babyBrthDateCompStr[3];
	 if(todayCompStr==babyBrthDateCompStr){
		 alert(msg); 
		 return false;
		 }
	 else{
	 return true;
	 }
	}
function showformsFromStatic(){
	if($j('#staticlicenseid').val()==$j("#staticunLicensedPk").val()){
		$j('.formsClass').show();
	}else{
		$j('.formsClass').hide();
	}
}
/*   Generate dialog of e-sign       */
function commonMethodForSaveAutoDefer(id){		 
		$j("#modalEsign1").dialog(
				   {autoOpen: false,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : 700, height : 100,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
					jQuery("#modalEsign1").dialog("destroy");
					     		
				    }
				   }
				  ); 
	  $j("#modalEsign1").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
	  $j("#modalEsign1").dialog("open");
	  $j("#esignAutoDefer").val("");
	  $j("#esignAutoDefer").focus();
	}

function submitpost(action,params){
    var hiddenForm        =        document.createElement("form");
    hiddenForm.method        =        "post";
    hiddenForm.action        =        action;
    var hiddenInput;
    for (var k in params) {
            hiddenInput = document.createElement("input");
            hiddenInput.setAttribute("type", "hidden");
            hiddenInput.setAttribute("name", k);
            hiddenInput.setAttribute("value", params[k]);
            hiddenForm.appendChild(hiddenInput);
      }
      document.body.appendChild(hiddenForm);
      hiddenForm.submit();
      document.body.removeChild(hiddenForm);
}
function printHistoryWidget(printDiv,regidStr){
	$j('#'+printDiv).removeClass('hide-data');
		var idOfPrint = document.getElementById("printingdiv").value;
		var sourceFile = "";
		sourceFile +="<table><thead><tr><th><center><h3>";
		 if(regidStr!="" && regidStr!="undefined"){
			 sourceFile +="<u>CBU Registry ID:"+regidStr+"</u>";
			}
			 sourceFile +='</h3></center><br/></th></tr></thead><tbody><tr><td><div class="portlet-header">'+$j("#printingdivTitle").val()+'</div><br/>'+$j("#"+idOfPrint).html()+"</td></tr></tbody></table>";
		$j('#noNameDiv').html(sourceFile);
		clickheretoprint('noNameDiv','');
		$j('#noNameDiv').html("");
}
function printQuicklinksWidget(printDiv,regidStr,title){
	
	var sourceFile = "";
	sourceFile +="<table><thead><tr><th><center><h3>";
	 if(regidStr!="" && regidStr!="undefined"){
		 sourceFile +="<u>CBU Registry ID:"+regidStr+"</u>";
		}
		 sourceFile +='</h3></center><br/></th></tr></thead><tbody><tr><td><div class="portlet-header"><b>'+title+'</b></div><br/>'+$j("#"+printDiv).html()+"</td></tr></tbody></table>";
	$j('#noNameDiv').html(sourceFile);
	clickheretoprint('noNameDiv','');
	$j('#noNameDiv').html("");
}
function assignDatePicker(className){
	getClearButton();
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	$j("#"+className).removeAttr("readonly").addClass("dpDate").each(function(){
		if($j(this).is(":disabled")){
			$j(this).datepicker("disable");
		}
		var s = this;
        var selectHolder = $j('<span>').css({ position: 'relative' });
        
        if($j("#placeHolderOfid_"+$j(this).attr("id")).length==0){
	    $j(this).wrap(selectHolder);
        var titleSpan = $j('<label>').attr({'class': 'tooltipspan placeHolderOfid_'+$j(this).attr("id"),'id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
	                				.css({'width':0,'height':0,'top':-5,'left':5}).click(function(){
	                					var txtFldId = $j(this).attr("id"); 
	                					txtFldId = txtFldId.replace("placeHolderOfid_","");
	                					if(!$j("#"+txtFldId).is(":disabled")){
		                					$j("#"+txtFldId).focus();
			                			}
	                				});
		if($j(this).val()!=""){
			$j(titleSpan).css({'display':'none'});
		}else{
			$j(titleSpan).css({'display':'block'});
		}
        	$j(this).parent().append(titleSpan);
        }
	}).datepicker({
		dateFormat: 'M dd, yy',
		maxDate: new Date(y, m, d),
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true, 
		prevText: '',
		nextText: '',
		showOn: "button",
		buttonImage: "./images/calendar-icon.png",
		buttonImageOnly: true,
		
		/* blur needed to correctly handle placeholder text */
        onSelect: function(dateText, inst) {
              $j(this).blur().change().focus();
        },
        beforeShow: function(input, inst) {
        	customCordDateRanges(input);
        },
	    onChangeMonthYear:function(y, m, i){                                
	        var d = i.selectedDay;
	        $j(this).datepicker('setDate', new Date(y, m - 1, d));
	        $j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
	    }
	}).focus(function(){
		$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
		if(!$j("#ui-datepicker-div").is(":visible"))
			customCordDateRanges(this);
		if($j(this).val()!=""){
			var thisdate = new Date($j(this).val());
			if(!isNaN(thisdate)){
				var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
				$j(this).val(formattedDate);
			}
		}
	}).keydown(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode==191 || e.keyCode==111){
			  return false;
		  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
			  if(val.length==2 || val.length==5){
				  $j(this).val(val+"/");
			  }
		  }else if(e.keyCode==8){
			if(val==""){
				return false;
			}
		  }
	}).focusout(function(){
		if($j(this).val()!=""){
			$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
		}else{
			$j(".placeHolderOfid_"+$j(this).attr("id")).show();
		}
	});
}  
/** This function is use to set 
 * Birth Date, Collection Date,
 * Processing Start and Freeze Date Custom Range 
 * @param input
 * @return result
 **/
function customCordDateRanges(input) {
	var field = "";
//	var flag = true;
		field = $j(input).hasClass("birthDate")?"birthDate":$j(input).hasClass("collectDate")?"collectDate":$j(input).hasClass("procStartDate")?"procStartDate":$j(input).hasClass("freezDate")?"freezDate":$j(input).hasClass("bactFungDate")?"bactFungDate":"";
	switch(field){
	       case "birthDate":
	    	   if($j(".collectDate").val() != ""){
	    		   var minTestDate = new Date($j(".collectDate").val());
	    		   var d1 = minTestDate.getDate();
	    		   var m1 = minTestDate.getMonth();
	    		   var y1 = minTestDate.getFullYear();
	    		   $j('.birthDate').datepicker( "option", "maxDate", new Date(y1,m1,d1));
	    	   }else{
	    	    	$j('.birthDate').datepicker( "option", "maxDate", new Date());
	    	   }
	    	   break;
	       case "collectDate":
	    	   if($j(".birthDate").val() != ""){
	    	    	var minTestDate = new Date($j(".birthDate").val());
	    	    	var d1 = minTestDate.getDate();
	    	    	var m1 = minTestDate.getMonth();
	    	    	var y1 = minTestDate.getFullYear();
	    	    	var today = new Date();
	    	    	var d = today.getDate();
	    	    	var m = today.getMonth();
	    	    	var y = today.getFullYear();
	    	    	if(!isNaN(minTestDate)){
		    	    	$j(".collectDate").datepicker( "option", "minDate", new Date(y1,m1,d1));
		    	    	if (new Date(y1,m1,d1+2) <= new Date(y,m,d)) {
		    	    		$j(".collectDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+2));
		    	    	}else if (new Date(y1,m1,d1+1) <= new Date(y,m,d)) {
		    	    		$j(".collectDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+1));
		    	    	}else{
		    	    		$j(".collectDate").datepicker( "option", "maxDate", new Date(y1,m1,d1));
		    	    	}
	    	    	}
	    	   }else{
	    		    $j(".collectDate").datepicker( "option", "minDate", null);
				    $j(".collectDate").datepicker( "option", "maxDate", new Date());
	    	   }
	    	   break;
	       case "procStartDate":
	    	 //  console.log("INside Proc Start Date");
	    	   if($j(".collectDate").val()!=""){
	    		   var today = new Date();
	    	       var d = today.getDate();
	    	       var m = today.getMonth();
	    	       var y = today.getFullYear();
	    		   var minTestDate = new Date($j(".collectDate").val());
	    		   var d1 = minTestDate.getDate();
	    		   var m1 = minTestDate.getMonth();
	    		   var y1 = minTestDate.getFullYear();
	    		   if(!isNaN(minTestDate)){
	    			   $j(".procStartDate").datepicker( "option", "minDate", new Date(y1,m1,d1));
	    			   if (new Date(y1,m1,d1+2) <= new Date(y,m,d)) {
		    	    		$j(".procStartDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+2));
		    	    	}else if (new Date(y1,m1,d1+1) <= new Date(y,m,d)) {
		    	    		$j(".procStartDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+1));
		    	    	}else{
		    	    		$j(".procStartDate").datepicker( "option", "maxDate", new Date(y1,m1,d1));
		    	    	}	    			  
	    		   }
	    	   }else if($j(".collectDate").val()==="" && $j(".birthDate").val() != ""){// collection date is null but baby birth date in not null
	    		   var minTestDate = new Date($j(".birthDate").val());
	    	    	var bd = minTestDate.getDate();
	    	    	var bm = minTestDate.getMonth();
	    	    	var by = minTestDate.getFullYear();
	    	    	 $j(".procStartDate").datepicker( "option", "minDate", new Date(by,bm,bd));
	    	    	 $j(".procStartDate").datepicker( "option", "maxDate", new Date());
	    		   
	    	   }else{
	    		  // console.log("inside else");
	    		   $j(".procStartDate").datepicker( "option", "maxDate", new Date());
	    		   $j(".procStartDate").datepicker( "option", "minDate", null);
	    	   }
	    	   break;
	       case "freezDate":
	    	     var today = new Date();
   	    	     var td = today.getDate();
   	    	     var tm = today.getMonth();
   	    	     var ty = today.getFullYear();
	    	    if($j(".collectDate").val()!="" && $j(".procStartDate").val()!=""){	    	    	
	    	    	var minTestDate = new Date($j(".procStartDate").val());
	    	    	var d = minTestDate.getDate();
	    			var m = minTestDate.getMonth();
	    			var y = minTestDate.getFullYear();
	    			var maxTestDate = new Date($j(".collectDate").val());
	    	    	var d1 = maxTestDate.getDate();
	    			var m1 = maxTestDate.getMonth();
	    			var y1 = maxTestDate.getFullYear();
	    			if(!isNaN(minTestDate)){
		    			//if(new Date(y,m,d)<new Date(y1,m1,d1+2)){
		    				$j(".freezDate").datepicker( "option", "minDate", new Date(y,m,d));
		    			//}
	    			}
	    			if(!isNaN(maxTestDate)){
	    				if (new Date(y1,m1,d1+2) <= new Date(ty,tm,td)) {
		    	    		$j(".freezDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+2));
		    	    	}else if (new Date(y1,m1,d1+1) <= new Date(ty,tm,td)) {
		    	    		$j(".freezDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+1));
		    	    	}else{
		    	    		$j(".freezDate").datepicker( "option", "maxDate", new Date(y1,m1,d1));
		    	    	}	 
	    				//$j(".freezDate").datepicker( "option", "maxDate", new Date(y,m,d));
	    			}
	    	   }else if($j(".procStartDate").val()==="" && $j(".collectDate").val()!=""){// if collection Date is not null and processing start date is null
	    		    var maxTestDate = new Date($j(".collectDate").val());
	    	    	var d1 = maxTestDate.getDate();
	    			var m1 = maxTestDate.getMonth();
	    			var y1 = maxTestDate.getFullYear();
	    		   $j(".freezDate").datepicker( "option", "minDate", new Date(y1,m1,d1));
	    		   if(!isNaN(maxTestDate)){
	    				if (new Date(y1,m1,d1+2) <= new Date(ty,tm,td)) {
		    	    		$j(".freezDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+2));
		    	    	}else if (new Date(y1,m1,d1+1) <= new Date(ty,tm,td)) {
		    	    		$j(".freezDate").datepicker( "option", "maxDate", new Date(y1,m1,d1+1));
		    	    	}else{
		    	    		$j(".freezDate").datepicker( "option", "maxDate", new Date(y1,m1,d1));
		    	    	}	    				
	    			}
	    		   
	    	   }else if($j(".procStartDate").val()==="" && $j(".collectDate").val()==="" && $j(".birthDate").val() != "" ){ // proc start date is null and collection date is null and baby birt date is not null
	    		    var babyBithDate = new Date($j(".birthDate").val());
	    	    	var bd = babyBithDate.getDate();
	    	    	var bm = babyBithDate.getMonth();
	    	    	var by = babyBithDate.getFullYear(); 
	    	    	$j(".freezDate").datepicker( "option", "minDate", new Date(by,bm,bd));
	    	    	$j(".freezDate").datepicker( "option", "maxDate",  new Date());
	    	   }else if($j(".collectDate").val()==="" && $j(".birthDate").val() != ""){// collection date is null but processing date in not null
	    		   var minTestDate = new Date($j(".birthDate").val());
	    	    	var bd = minTestDate.getDate();
	    	    	var bm = minTestDate.getMonth();
	    	    	var by = minTestDate.getFullYear();
	    	    	 $j(".freezDate").datepicker( "option", "minDate", new Date(by,bm,bd));
	    	    	 $j(".freezDate").datepicker( "option", "maxDate", new Date());
	    		   
	    	   }else{
	    	    	$j(".freezDate").datepicker( "option", "maxDate",  new Date());
	    			$j(".freezDate").datepicker( "option", "minDate", null);
	    	   }
	    	   //var result = $j.browser.msie ? !this.fixFocusIE : true;
	    	   //this.fixFocusIE = false;
	    	   //return result;
	    	   break;
	       case "bactFungDate":
	    	   if($j(".collectDate").val() != ""){
	    		   var minTestDate = new Date($j(".collectDate").val());
	    		   var d1 = minTestDate.getDate();
	    		   var m1 = minTestDate.getMonth();
	    		   var y1 = minTestDate.getFullYear();
	    		   if(!isNaN(minTestDate)){
	    			   $j('.bactFungDate').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    		   }
	    	   }else{
	    	    	$j('.bactFungDate').datepicker( "option", "minDate", "");
	    	   }
	    	   break;
         }
}
function validateCordDates(date1,date2,msgId){
	//console.log("inside validateCordDates:::");
	var flag = true;
	if(($j('#'+date1).val() == "" || $j('#'+date1).val() ==null) && $j('#'+date2).val()!="" ){
		//console.log("inside if:::");
		//$j('#'+date2).val("");
		flag = false;
		setTimeout(function(){
			//$j('#'+date1).focus();
			if(date1 == 'datepicker2' && date2 == 'datepicker4'){
				focusDiv('cbuinfoparent');
				$j('#'+date1).focus();
			}else{
				$j('#'+date1).focus();
			}
		},100);
		$j('#'+msgId).show();
	}else{
		//console.log("inside else:::");
		flag = true;
		$j('#'+msgId).hide();
	}
	return flag;
}
function validateCordDateRange(date1,date2,msgId,cordEntryFlag){
	//console.log("inside validateCordDateRange:::");
	var flag = true;
	if(($j('#'+date1).val()!="" || $j('#'+date1).val()!=null) && ($j('#'+date2).val()!="" || $j('#'+date2).val()!=null)){
		//console.log("inside first If:::");
		var sourceDate = new Date($j('#'+date1).val());
		var d = sourceDate.getDate();
		var m = sourceDate.getMonth();
		var y = sourceDate.getFullYear();
		var distinationDate =  new Date($j('#'+date2).val());
		var d1 = distinationDate.getDate();
		var m1 = distinationDate.getMonth();
		var y1 = distinationDate.getFullYear();
		
		if (new Date(y1,m1,d1) > new Date(y,m,d+2)) {
			//console.log("inside if:::");
			flag = false;
			$j('#'+msgId).show();
			//$j('#'+date2).val("");
			setTimeout(function(){
				if(date1 == 'datepicker2' && date2 == 'datepicker4'){
					focusDiv('labsummaryparent');
					$j('#'+date2).focus();
				}else{
					$j('#'+date2).focus();
				}
			},100);
		}else{
			flag = true;
			if(cordEntryFlag){
				flag = setMinDateWidRespCollDat();
			}
			//console.log("inside else:::");
			$j('#'+msgId).hide();
		}
	}
	return flag;
}
function validateHours(date1,date1TimeId,date2,date2TimeId,errMsg){
	var flag = true;
	var sourceDate = new Date($j('#'+date1).val());
	var d = sourceDate.getDate();
	var m = sourceDate.getMonth();
	var y = sourceDate.getFullYear();
	var date1Time = $j('#'+date1TimeId).val();
	var time1Arr = date1Time.split(':');
	//console.log("time1Arr::"+time1Arr);
	var newSource = new Date(y,m,d,time1Arr[0],time1Arr[1],0);
	//console.log("newSource:::"+newSource);
	var h = newSource.getHours();
	var mn = newSource.getMinutes();
	var newSource1 = new Date(y,m,d,h+48,mn,0);
	//console.log("newSource1:::"+newSource1);
	var distinationDate =  new Date($j('#'+date2).val());
	var d1 = distinationDate.getDate();
	var m1 = distinationDate.getMonth();
	var y1 = distinationDate.getFullYear();
	var date2Time = $j('#'+date2TimeId).val();
	var time2Arr = date2Time.split(':');
	//console.log("time1Arr::"+time2Arr);
	var newDistination = new Date(y1,m1,d1,time2Arr[0],time2Arr[1],0);
	//console.log("newDistination:::"+newDistination);
	if(newDistination > newSource1 || newDistination < newSource){
		//console.log("Inside Iff:::");
		//$j('#'+date2TimeId).val("");
		$j('#'+errMsg).show();
		flag = false;
	}else{
		//console.log("INside Else:::");
		$j('#'+errMsg).hide();
	}
	return flag;
}
jQuery.fn.filterByText = function(textbox, selectSingleMatch,id) {
	  return this.each(function() {
	    var select = this;
	    var options = [];
	    $j(select).find('option').each(function() {
	      options.push({value: $j(this).val(), text: $j(this).text()});
	    });
	    $j(select).data('options', options);
	    $j(textbox).bind('change keyup', function() {
	      var options = $j(select).empty().scrollTop(0).data('options');
	      var search = $j.trim($j(this).val());
	      var regex = new RegExp(search,'gi');
	      $j.each(options, function(i) {
	        var option = options[i];
	        if(option.text.match(regex) !== null) {
	          $j(select).append(
	        		  //getCBBdetails();
	             $j('<option>').text(option.text).val(option.value)
	          );
	        }
	      });
	      if (selectSingleMatch === true && $j(select).children().length === 1) {
	         $j(select).children().get(0).selected = true;
	         $j("#"+id).triggerHandler('change');
	      }
	      if(search==""){
	    	   $j(this).val('Enter Any CBB')    	  
	    	  var primaryorg=$j('#primaryOrg').val();
	    	  $j(select).find('option').each(function(i,v){
	    		  if (primaryorg!="" && primaryorg!="null" && $j(this).val()==primaryorg) {
	    			  $j(this).attr('selected','selected');
	    			  $j("#"+id).triggerHandler('change');
	    	      }  
	    	  });    	  
	    	  
		      
	      }
	    });
	  });
	};
	
function trimNumber(s) {
		  while (s.substr(0,1) == '0' && s.length>1) { s = s.substr(1,9999); }
		  return s;
}
function newformatDate(date1){
	
	var returnDate="";
	var tdtArry;
	var dtArry;
	var tempdt;
	var d;
	var m;
	var y;
	var monthNames = [
	                  "",
	                  "Jan", "Feb", "Mar",
	                  "Apr", "May", "Jun",
	                  "Jul", "Aug", "Sep",
	                  "Oct", "Nov", "Dec"
	                  ];
	
	//alert("value::::"+date1)
	
	if(date1!=null && date1!=undefined && date1!=""){

		d = date1.substring(8,10);
		m = date1.substring(5,7);
		y = date1.substring(0,4);
		
		//alert("d:"+d+":m:"+m+":y:"+y)
		
		returnDate= monthNames[trimNumber(m)]+" "+d+", "+y;
		//alert(returnDate);
	}
	return returnDate;
}
function validateDateData(date1,time1,date2,time2,err1,err2){
	var flag = true;
	if(($j('#'+date2).val()!="" && $j('#'+date2).val()!=null) || ($j('#'+time2).val()!="" && $j('#'+time2).val()!=null)){
		var sourceDate = new Date($j('#'+date1).val());
		var d = sourceDate.getDate();
		var m = sourceDate.getMonth();
		var y = sourceDate.getFullYear();
		var date1Time = '00:00';
		if($j('#'+time1).val()!=""){
			date1Time = $j('#'+time1).val();
		}
		var time1Arr = date1Time.split(':');
		var newSource = new Date(y,m,d,time1Arr[0],time1Arr[1],0);
		var h = newSource.getHours();
		var mn = newSource.getMinutes();
		var newSource1 = new Date(y,m,d,h,mn,0);
		
		var distinationDate =  new Date($j('#'+date2).val());
		
		var d1 = distinationDate.getDate();
		var m1 = distinationDate.getMonth();
		var y1 = distinationDate.getFullYear();
		var date2Time = $j('#'+time2).val();
		var time2Arr = date2Time.split(':');
		var newDistination = new Date(y1,m1,d1,time2Arr[0],time2Arr[1],0);
		
		if(distinationDate < sourceDate){
			$j('#'+err1).show();
			flag = false;
		}else{
			$j('#'+err1).hide();
		}
		if(newDistination < newSource1){
			$j('#'+err2).show();
			flag = false;
		}else{
			$j('#'+err2).hide();
			$j('.prcessError').hide();
		}
	}
	return flag;
}

/*** To make widget toggle ***/
$j(function(){
	   $j('.toggleWidgetClass').find(".portlet-header .ui-icon").bind("click",function() {
		   if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
			{
				$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
				
				$j(this).parents(".portlet:first").find(".portlet-content").toggle();						
			}
		   
		});	
	   
	   /*** to toggle sub-widget of main widget ***/
	   $j('.togglesubWidgetClass').find(".ui-icon").bind("click",function() {
		   if($j(this).is(".ui-icon-triangle-1-s") || $j(this).is(".ui-icon-triangle-1-e"))
			{
				$j(this).toggleClass("ui-icon-triangle-1-s").toggleClass("ui-icon-triangle-1-e");
				
				$j(this).parents(".subportlet:first").find(".subportlet-content").toggle();			
			}
		   
		});	
	   
});
function validateIDMBloodCollDate(colDate,BldColDate,msg){
	var colDate = new Date(colDate);
	var d1 = colDate.getDate();
	var m1 = colDate.getMonth();
	var y1 = colDate.getFullYear();
	var BldColDate = new Date(BldColDate);
	if (BldColDate < new Date(y1,m1,d1-7) || BldColDate > new Date(y1,m1,d1+7)) {
		$j("#"+msg).show();
	}else{
		$j("#"+msg).hide();
	}
}
function checkdateformat(input,dateRange){
	var returnval=false;
	if(input.value!=""){
		var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
		if (!validformat.test(input.value)){
			jAlert('Invalid Date Format.Format should be mm/dd/yyyy (eg. 02/05/2013).Please correct it.', 'Warning Message',function(r) {
				if(r==true){
					input.value = "";
					input.focus();
				}});
		}else{ //Detailed check for valid date ranges
			var monthfield=input.value.split("/")[0];
			var dayfield=input.value.split("/")[1];
			var yearfield=input.value.split("/")[2];
			var dayobj = new Date(yearfield, monthfield-1, dayfield);
			if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield)){
			jAlert('Invalid Day, Month, or Year range detected. Please correct it.', 'Warning Message',function(r) {
				if(r==true){
					input.value = "";
					input.focus();
				}});
			}else{
				if(dateRange=="maxDate"){
					if(new Date(input.value) > new Date()){
					jAlert('Entered date should not be greater than today. Please correct it.', 'Warning Message',function(r){
						if(r==true){
							input.value = "";
							input.focus();
						}});
					}else{
						returnval=true;
						var formattedDate = dateFormat($j(input).val(),'mmm dd, yyyy');
						$j(input).val(formattedDate);
					}
				}else if(dateRange=="minDate"){
					if(new Date(input.value) < new Date()){
					jAlert('Entered date should be equal too or greater than today. Please correct it.', 'Warning Message',function(r){
							if(r==true){
								input.value = "";
								input.focus();
							}});
					}else{
						returnval=true;
						var formattedDate = dateFormat($j(input).val(),'mmm dd, yyyy');
						$j(input).val(formattedDate);
					}
				}else{
					returnval=true;
					var formattedDate = dateFormat($j(input).val(),'mmm dd, yyyy');
					$j(input).val(formattedDate);
				}
			}
		}
		//if (returnval==false) input.select();
	}
	return returnval;
}
function setSortData(theadId,sortColHId,sortTypHId){
	   
	$j('#'+theadId).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#'+sortColHId).val(i);
			$j('#'+sortTypHId).val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#'+sortColHId).val(i);
			$j('#'+sortTypHId).val('desc');
		}
   });
}
function showStatusFilterText(textId,statusDivId){
		if($j("#"+statusDivId).val()=='1'){
		$j("#"+textId).css('display','block');
		$j("#"+textId).css('color','red');
		$j("#"+textId).text('Filter Executed').fadeOut(8000);
		$j("#"+statusDivId).val('');
	}
	else if($j("#"+statusDivId).val()=='0'){
		$j("#"+textId).css('display','block');
		$j("#"+textId).css('color','red');
		$j("#"+textId).text('Filter Resetted').fadeOut(8000);
		$j("#"+statusDivId).val('');
	}
}
function isChangeDone(thisData, actualData,cssName){
	if($j(thisData).val()!=null){
		changedData = $j.trim($j(thisData).val());
	}
	if($j(thisData).val()!="" && $j(thisData).hasClass("hasDatepicker")){
		var formattedDate = dateFormat($j(thisData).val(),'mmm dd, yyyy');
		$j(thisData).val(formattedDate);
		if(changedData!=actualData){
			$j(thisData).addClass(cssName);
		}
		else{
			$j(thisData).removeClass(cssName);
		}
	}else if($j(thisData).is('input[type="radio"]')){
		var thisName = $j(thisData).attr("name");
		if(changedData!=actualData){
			$j('input[name='+thisName+']').addClass(cssName);
		}
		else{
			$j('input[name='+thisName+']').removeClass(cssName);
		}
	}else{
		if(changedData!=actualData){
			$j(thisData).addClass(cssName);
		}
		else{
			$j(thisData).removeClass(cssName);
		}
	}
}
function divChanges(classname,divData){
    var changesStatus = false;
    $j('#printTemp').html('<div id="findChanges">'+divData+'</div>');
    $j('#findChanges .'+classname).each(function(){
            changesStatus = true;
            return false
    });
    $j('#printTemp').html("");
    return changesStatus;
}
function showSuccessdiv(successdiv,contentdiv){
    $j("#"+successdiv).css('display','block');
    $j("#"+contentdiv).css('display','none');
}

var xhr ;

function asyncLoadDivWithFormSubmit(url,divname,formId){	
	showProgressOverlayMsg();
	if($j("#"+formId).valid()){
	xhr = $j.ajax({
        type: "POST",
        url: url,
        async:true,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");	        
            var $response=$j(result);	        
            var errorpage = $response.find('#errorForm').html();	         
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }           
            closeProgressOverlayMsg();
        },
        error: function (request, status, error) {
        	closeProgressOverlayMsg();        	
        }
	  });
    }		
}

function showProgressOverlayMsg(){
	$j('#progressindicator').dialog({
		autoOpen: false,
		resizable: false, width:400, height:90,closeOnEscape: false,
		modal: false}).siblings('.ui-dialog-titlebar').remove();	
	$j('#progressindicator').dialog("open");
}

function closeProgressOverlayMsg(){
	setTimeout(function(){
		$j('#progressindicator').dialog("close");
		$j('#progressindicator').dialog("destroy");
	},0);
}

function cancelQueryRequest(){
	if(xhr && xhr.readystate != 4){
		xhr.abort();
		closeProgressOverlayMsg()
	}
			
}

function asyncLoadPageByGetRequset(url,divname,value){  
	showProgressOverlayMsg();
	xhr = $j.ajax({
	        type: "GET",
	        url: url,
	        async:true,
	        success: function (result){
		    	 $j('.ui-datepicker').html("");		     
		         var $response=$j(result);		      
		         htmresult = result;
		         var errorpage = $response.find('#errorForm').html();		    
		         if(errorpage != null &&  errorpage != "" ){
		         	$j("#main").html(result);
		         }else{
		         	$j("#"+divname).html(result);
		       }
		         	loadCallback(value);
		         if(value==='antigenrpt' || value===''){ closeProgressOverlayMsg();}		        
	        },
	        error: function (request, status, error) {
	        	closeProgressOverlayMsg();
	        	//alert("Error " + error);
	            //alert(request.responseText);
	        }

		});
	return htmresult;
	}

function loadDivWithFormSubmitNew(url,divname,formId){
	
	    showProgressOverlayMsg();
		if($j("#"+formId).valid()){
		xhr =	$j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data : $j("#"+formId).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	            var $response=$j(result);
	            var errorpage = $response.find('#errorForm').html();	          
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	$j("#"+divname).html(result);
	            }
	            //closeProgressOverlayMsg();
	        },
	        error: function (request, status, error) {
	        	//closeProgressOverlayMsg();
	        	
	        }

		});
		}		
	}
	function disableFormFlds(formId){
	$j("#"+formId+" :input:not(:button)").each(function(){
	  	if($j(this).attr('type')!="radio"){
	 		$j(this).attr('disabled', 'disabled');
	  	}else{
		 	 $j(this).removeAttr("onclick");
			 $j(this).bind("click",function(){
				 return false;
			});
			 if(!$j(this).is(":checked")){
				 $j(this).attr('disabled', 'disabled');
			}
	  	}	
  });
	$j("#"+formId).find('input[type="hidden"]').each(function(){
		$j(this).removeAttr("disabled");
	});
}
function formValueChk(Id){
	var flag=false;
	if(($j('#'+Id+' :radio').length)!=($j('#'+Id+' :radio:not(:checked)').length)){
		flag=true;
	}
	return flag;
} 