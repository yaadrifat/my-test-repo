var dataGridChangeList = null;
var visitLabelArray = null;
var visitDisplacementArray = null;
var eventLabelArray = null;
var incomingUrlParams = null;
var calStatus = null;
var lastTimeSubmitted = 0;
var horizontalPage = 0;

var ErrGetResp_CnctVelos=M_ErrGetResp_CnctVelos;
var CommErr_CnctVelos=M_CommErr_CnctVelos;
var PlsEnterEsign=M_PlsEnterEsign;
var CnfrmVst_EvtChg=M_CnfrmVst_EvtChg;
var ThereNoChg_Save=M_ThereNoChg_Save;
var Invalid_Esign=L_Invalid_Esign;
var Valid_Esign=L_Valid_Esign;
var LoadingPlsWait=L_LoadingPlsWait;
var PleaseWait_Dots=L_PleaseWait_Dots;
var LEdit=L_Edit;
var Selc=L_Selc;
var Unselc=L_Unselc;
var Total_Chg=L_Total_Chg;
var Esignature=L_Esignature;
var Event_Name=L_Event_Name;

var l_Event_Name = L_Event_Name;
var l_Event = L_Event;
var l_Evt_Lib = L_Evt_Lib;
var Evt_CptCode = L_Evt_CptCode;
//YK : Removed for Bug #7513 
var eventNameArray = null;

var visitHideFlag = null;//YK: Fix for Bug#7264

// Define VELOS.dataGrid object. "VELOS" is a namespace so that the object can be called globally
VELOS.dataGrid = function (url, args) {
	var isIe = jQuery.browser.msie;
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; // this will be sent to updateEvtVisits.jsp
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}
	
	showPanel(); // Show the wait panel; this will be hidden later
	
	// Define handleSuccess() of VELOS.dataGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		/* Response Object Fields:
		 * o.tId, o.status, o.statusText, o.getResponseHeader[],
		 * o.getAllResponseHeaders, o.responseText, o.responseXML, o.argument
		 */
        this.dataTable = args.dataTable ? args.dataTable : 'serverpagination'; // name of the div to hold dataGrid
		var respJ = null; // response in JSON format
		try {
			respJ= $J.parse(o.responseText);
		
		} catch(e) {
			alert(ErrGetResp_CnctVelos);/*alert('Error getting response from server. Please contact Velos Support.');*****/
			return;
		}
		if (respJ.error < 0) {
			var paramArray = [respJ.errorMsg];
		    alert(getLocalizedMessageString("L_Error_Msg", paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
			return;
		}
		var myFieldArray = [];
		var myColumnDefs = [];
		var visitNos=0;
		if (respJ.colArray) {
			myColumnDefs = respJ.colArray;
			for (var iX=0; iX<myColumnDefs.length; iX++) {
				myColumnDefs[iX].width = myColumnDefs[iX].key == 'event' ? 200: 100;
				myColumnDefs[iX].resizeable = true;
				myColumnDefs[iX].sortable = false;
				var fieldElem = [];
				if (myColumnDefs[iX].key && myColumnDefs[iX].key.match(/^v[0-9]+$/)) {
					myColumnDefs[iX].formatter = 'checkbox';
					fieldElem['parser'] = 'checkbox';
				} else if (myColumnDefs[iX].key == 'eventId') {
					myColumnDefs[iX].hidden = true;
				}
				fieldElem['key'] = myColumnDefs[iX].key; 
				myFieldArray.push(fieldElem);
			}
		}
		visitLabelArray = [];
		for (var iX=0; iX<myColumnDefs.length; iX++) {
			if (!myColumnDefs[iX].label) { continue; }
			if (myColumnDefs[iX].key == 'event' || myColumnDefs[iX].key == 'eventId') { continue; }
			var inputPos = myColumnDefs[iX].label.indexOf(" <input type");
			visitLabelArray[myColumnDefs[iX].key] = myColumnDefs[iX].label.slice(0, inputPos);
			visitNos++;
		}
		visitDisplacementArray = [];
		if (respJ.displacements) {
			var myDisplacements = [];
			myDisplacements = respJ.displacements;
			for (var iX=0; iX<myDisplacements.length; iX++) {
				visitDisplacementArray[myDisplacements[iX].key] = myDisplacements[iX].value;
			}
		}
		
		var myDataSource = new YAHOO.util.DataSource(respJ.dataArray);
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
			fields: myFieldArray
		};
		
		var maxWidth = 800;
		var maxHeight = 800;

		if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 900; }
		else if (screen.availWidth >= 1100 && screen.availWidth < 1200) { maxWidth = 1000; }
		else if (screen.availWidth >= 1200 && screen.availWidth < 1290) { maxWidth = 1180; }
				
		var calcWidth;
		calcWidth = visitNos*((isIe==true)?102:120)+((isIe==true)?200:234)+((isIe==true)?20:15);
		
		if(calcWidth>=maxWidth){
			calcWidth=maxWidth;
		}
		
		if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
		else if (screen.availHeight >= 900 && screen.availHeight < 1000) {maxHeight =((isIe==true)?500:620); }
		
		var calcHeight = respJ.dataArray.length*((isIe==true)?33:34)+((isIe==true)?12:8);
		if (calcHeight > maxHeight) { calcHeight = maxHeight; }
				
		var myDataTable = new YAHOO.widget.DataTable(
			this.dataTable,
			myColumnDefs, myDataSource,
			{
				width:calcWidth+"px",
				height:calcHeight+"px",
				//caption:"DataTable Caption", 
				scrollable:true 
			}
		);
		//YK: Fix for Bug#7264
		visitHideFlag = [];
		if (respJ.visitsHideFlag) {
			var visitHides = [];
			visitHides = respJ.visitsHideFlag;
			for (var iX=0; iX<visitHides.length; iX++) {
				visitHideFlag[visitHides[iX].key] = visitHides[iX].value;
			}
		}
		
		//Yk: Added for Enhencement :PCAL-19743, Event names
		eventNameArray = [];
		if (respJ.eventNames) {
			var myEventNames = [];
			myEventNames = respJ.eventNames;
			for (var iX=0; iX<myEventNames.length; iX++) {
				eventNameArray[myEventNames[iX].key] = myEventNames[iX].value;
			}
		}
		var sortOrder= document.getElementById("sortOrder").value;
		eventIdArray = [];
		eventLabelArray = [];
		//YPS: Added for Bug#:10220 Date:26 June 2012
		var eventHeader = jQuery('th.yui-dt-col-event div').html();
		var mouseOverData="";
		if(sortOrder!=undefined && sortOrder!=""){
			mouseOverData=sortOrder=="asc"?M_Click_SortDescend:M_Click_SortAscend;
		}else{
			mouseOverData="";
		}
		var newEventHeader="<a title='"+mouseOverData+"' alt='"+mouseOverData+"'>"+eventHeader+"</a>";
		jQuery('th.yui-dt-col-event div').html(newEventHeader);
		
		var eventIdCells = $D.getElementsByClassName('yui-dt-col-eventId', 'td', myDataTable.getTableEl());
		for (var iX=0; iX<eventIdCells.length; iX++) {
			var el = new YAHOO.util.Element(eventIdCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var eventIdTd = parent.getElementsByClassName('yui-dt-col-eventId', 'td')[0];
			var eventIdEl = new YAHOO.util.Element(eventIdTd);
			var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventIdArray.push(eventId);
			var eventTd = parent.getElementsByClassName('yui-dt-col-event', 'td')[0];
			var eventEl = new YAHOO.util.Element(eventTd);
			var eventName = eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			eventLabelArray['e'+eventId] = eventName;
			var parentId = parent.get('id');
			eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
				= '<input type="checkbox" name="all_e'+eventId+'" id="all_e'+eventId+'"'+
				' onclick="VELOS.dataGrid.eventAll(\''+parentId+'\','+eventId+');" /> '+
				eventEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			//Yk: Added for Enhencement :PCAL-19743
			var mouseOverData = eventNameArray["e"+eventId].split("&");
			/* Bug#9854 04-Jun-2012 -Sudhir*/
			var mouseOver= "<b>"+l_Event_Name+"</b>:&nbsp;"+(eventName.length>25 ? eventName.substr(0,25)+"...":eventName);
			mouseOver +="<br><b>"+l_Evt_Lib+"</b>:&nbsp;"+(mouseOverData[1].length>25 ? mouseOverData[1].substr(0,25)+"...":mouseOverData[1]);
			mouseOver +="<br><b>"+Evt_CptCode+"</b>:&nbsp;"+mouseOverData[2];
			eventTd.innerHTML='<span onmouseover="return overlib(\''+mouseOver+'\',CAPTION,\''+Event_Name/*Event Name*****/+'\')" >'+eventTd.innerHTML+'</span>'
			// YK : Added for Bug#7470
			if(sortOrder!=undefined && sortOrder!="")
			{
				jQuery(eventTd).addClass(sortOrder=="asc"?"yui-dt-asc":"yui-dt-desc");
			}else{
				jQuery(eventTd).removeClass("yui-dt-asc");
				jQuery(eventTd).removeClass("yui-dt-desc");
			}
		}
		
		dataGridChangeList = [];
		myDataTable.subscribe("checkboxClickEvent", function(oArgs){
			var elCheckbox = oArgs.target;
			var oRecord = this.getRecord(elCheckbox);
			var column = this.getColumn(elCheckbox);
			var oKey = 'e'+oRecord.getData().eventId+column.key
			oRecord.setData(oKey, elCheckbox.checked);
			// eventLabelArray['e'+oRecord.getData().eventId] = oRecord.getData().event;
			//alert(YAHOO.lang.dump(oRecord.getData()));
			if (dataGridChangeList[oKey] == undefined) {
				dataGridChangeList[oKey] = elCheckbox.checked;
			} else {
				dataGridChangeList[oKey] = undefined;
			}
		});
		
		myDataTable.subscribe("cellMouseoverEvent", function(oArgs) {
			var elCell = oArgs.target;
			myDataTable.onEventHighlightRow(oArgs);
			var oRecord = this.getRecord(oArgs.target);
			var eventImage = elCell.getElementsByTagName('img')[0];
			if (eventImage){
				elCell.onmouseover = eventImage.onmouseover;
			}else{
				if (!elCell.onmouseover){
					var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
					var parentId = parent.get('id');
					var eventTd = parent.getElementsByClassName('yui-dt-col-event', 'td')[0];
					var eventEl = new YAHOO.util.Element(eventTd);
					var eventSpan = eventEl.getElementsByTagName('span')[0];
					elCell.onmouseover = eventSpan.onmouseover;
				}
			}
			return elCell.onmouseover();
		});
		myDataTable.subscribe("cellMouseoutEvent", function(oArgs) {
			myDataTable.onEventUnhighlightRow(oArgs);
			return nd();
		});
        // Subscribe to events for row selection
        myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);
		myDataTable.focus();   


		if (!$('save_changes')) {
			var dt = $(this.dataTable);
			var saveDiv = document.createElement('div');
			saveDiv.innerHTML = "<br/><table border=0 width='100%'><tbody><tr align='left'>" +
				"<td width='350'></td>"+
				"<td align='left'><button onclick='VELOS.dataGrid.saveDialog();' id='save_changes' name='save_changes'>"+getLocalizedMessageString('L_Preview_AndSave')+"</button></td>"+
				"</td></tr></tbody></table>";
			$D.insertAfter(saveDiv, dt);
			
		}
		
		calStatus = respJ.calStatus;
		var cells = null;
		if (calStatus == 'O') {
			var offlineArray = [];
			var offlineKeys = respJ.offlineKeys;
			var offlineValues = respJ.offlineValues;
			if (offlineKeys && offlineValues) {
				for (var iX = 0; iX < offlineKeys.length; iX++) {
					offlineArray[offlineKeys[iX].key] = offlineValues[iX].value
				}
			}
			cells = $D.getElementsByClassName('yui-dt-checkbox', 'input', myDataTable.getTableEl());
			for (var iX = 0; iX < cells.length; iX++) {
				if (!cells[iX].checked) { continue; }
				var el = new YAHOO.util.Element(cells[iX]);
				var myTd = new YAHOO.util.Element($D.getAncestorByTagName(el, 'td'));
				var vId = myTd.get('headers').slice(myTd.get('headers').indexOf('v'));
				vId = YAHOO.lang.trim(vId);
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
				var eventIdTd = parent.getElementsByClassName('yui-dt-col-eventId', 'td')[0];
				var eventIdEl = new YAHOO.util.Element(eventIdTd);
				var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				var myKey = 'e'+eventId+vId;
				if (!offlineArray[myKey]) { continue; }
				if (offlineArray[myKey]=='1'||offlineArray[myKey]=='2') { cells[iX].disabled = true; }
			}
		}
		//Yk: Added for Enhencement :PCAL-19743, give the sorting functionality.
		eventTh = $D.getElementsByClassName('yui-dt-col-event', 'th');
		for (var iX = 0; iX < eventTh.length; iX++) {
			var el = new YAHOO.util.Element(eventTh[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var eventIdTd = parent.getElementsByClassName('yui-dt-col-event', 'th')[0];
			jQuery(eventIdTd).addClass('yui-dt-sortable');
			// YK : Added for Bug#7470
			jQuery(eventIdTd).click(function() {
				if(sortOrder!=undefined && sortOrder!=""){
				sortOrder=sortOrder=="asc"?"desc":"asc";
				}else{sortOrder="asc";}
				document.getElementById("sortOrder").value=sortOrder;
				reloadDataGrid(sortOrder);
			});
			var eventIdEl = new YAHOO.util.Element(eventIdTd);
			var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0];
			// YK : Added for Bug#7470
			if(sortOrder!=undefined && sortOrder!=""){
			jQuery(eventIdTd).addClass(sortOrder=="asc"?"yui-dt-asc":"yui-dt-desc");
			}else{
				jQuery(eventTd).removeClass("yui-dt-asc");
				jQuery(eventTd).removeClass("yui-dt-desc");
			}
			
		}
		//YK : Removed for Bug #7513 
		var tabLength= myDataTable.getRecordSet().getLength();
		var cellCount = 0;
		for (var iX=0; iX<tabLength; iX++) {
			var eventCells= $D.getElementsByClassName('yui-dt-col-event', 'td');
			var el = new YAHOO.util.Element(eventCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var parentId = parent.get('id');
			//YK : Modified for Bug #7513 
			var eventIdTd = parent.getElementsByClassName('yui-dt-col-eventId', 'td')[0];
			var eventIdEl = new YAHOO.util.Element(eventIdTd);
			var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
			var pattern = /\byui-dt-col-v[0-9]+\b/;
			var checkBoxTD = parent.getElementsByClassName(pattern, 'td');
			var checkBoxLength=checkBoxTD.length;
			for(var iY = 0; iY < checkBoxLength; iY++)
			{
				if (++cellCount>tabLength*checkBoxLength/2) { hidePanel(); }
				checkBoxTD = parent.getElementsByClassName(pattern, 'td')[iY];
				if(checkBoxTD == undefined) { continue;}
				var checkBoxEl = new YAHOO.util.Element(checkBoxTD);
				var getCheckbox = checkBoxEl.getElementsByClassName('yui-dt-checkbox', 'input')[0];
				if(!getCheckbox.checked){continue};
				var myVisitTd = checkBoxTD.headers;
				var myVId = YAHOO.lang.trim(myVisitTd.slice(myVisitTd.indexOf('v')+1));
				//YK : Modified for Bug #7513 
				if (respJ.eventVisitIds) {
					var myEventIds = [];
					myEventIds = respJ.eventVisitIds;
				var eventNewIds = myEventIds.length;
				for(var iZ=0; iZ< eventNewIds; iZ++){
				var evvVisit = eventId+"V"+myVId;
				var evVisit= myEventIds[iZ].eventId;
				if(evvVisit == evVisit ){
					var visitIds= myEventIds[iZ].value;
					var vIds = YAHOO.lang.trim(visitIds.slice(visitIds.indexOf('v')+1,visitIds.indexOf('f')));
					var eIds = YAHOO.lang.trim(visitIds.slice(1,visitIds.indexOf('v')));
					var eFlg = YAHOO.lang.trim(visitIds.slice(visitIds.indexOf('f')+1));
					var mouseOverData = eventNameArray["e"+eIds].split("&");
					/* Bug#9854 04-Jun-2012 -Sudhir*/
					var mouseOver= "<b>"+l_Event_Name+"</b>:&nbsp;"+(mouseOverData[0].length>25 ? mouseOverData[0].substr(0,25)+"...":mouseOverData[0]);
					mouseOver +="<br><b>"+l_Evt_Lib+"</b>:&nbsp;"+(mouseOverData[1].length>25 ? mouseOverData[1].substr(0,25)+"...":mouseOverData[1]);
					mouseOver +="<br><b>"+Evt_CptCode+"</b>:&nbsp;"+mouseOverData[2];
						var param = "eventdetails.jsp?srcmenu=" + document.getElementById("srcmenu").value + "&eventmode=M&eventId=" + eIds + "&duration=" + document.getElementById("duration").value + "&protocolId=" + document.getElementById("protocolId").value + "&calledFrom=" + document.getElementById("calledFrom").value + "&mode=" + document.getElementById("mode").value + "&calStatus=" + document.getElementById("calStatus").value + "&fromPage=fetchProt&selectedTab=1&calassoc="+document.getElementById("calassoc").value+"&offLineFlag="+eFlg;
						checkBoxEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML +=" <img src=\"./images/edit_icon.png\" alt=\"\" onClick=\"openEvtDetailsPopUp('"+param+"')\" onmouseover=\"return overlib('"+mouseOver+"',CAPTION,'"+Event_Name/*Event Name*****/+"');\" onmouseout=\"return nd();\"> ";
						break;
					  }
				    }
			    }
			 }
		}	
		hidePanel();
		//Yk: Added for Enhencement :PCAL-19743 -Ends
		//JM: 23MAR2011: 5938: As 'R' is a common status like 'W', we needed to free the 'R'
		//if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F' || calStatus == 'R') {
		if (calStatus == 'A' || calStatus == 'D' || calStatus == 'F') {			
			cells = $D.getElementsByClassName('yui-dt-checkbox', 'input', myDataTable.getTableEl());
			for (var iX = 0; iX < cells.length; iX++) {
				cells[iX].disabled = true;
			}
			if ($('save_changes')) { $('save_changes').disabled = true;  $('save_changes').visibility = "hidden";}
		}
		
	};
	this.handleFailure = function(o) {
		alert(CommErr_CnctVelos); /*alert(svrCommErr);Error Message displayed from local var*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}

VELOS.dataGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
        		alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {/*if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {*****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}
VELOS.dataGrid.checkAll = function(vId) {
	var selColEls = $D.getElementsByClassName('yui-dt-col-'+vId, 'td');
	for (var iX=0; iX<selColEls.length; iX++) {
		var el = new YAHOO.util.Element(selColEls[iX]);
		var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (input.disabled) { continue; }
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var eventIdTd = parent.getElementsByClassName('yui-dt-col-eventId', 'td')[0];
		var eventIdEl = new YAHOO.util.Element(eventIdTd);
		var eventId = eventIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		// var eventNameTd = parent.getElementsByClassName('yui-dt-col-event', 'td')[0];
		// var eventNameEl = new YAHOO.util.Element(eventNameTd);
		// var eventName = eventNameEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		var oKey = 'e'+eventId+vId;
		if (input.checked != document.getElementById('all_'+vId).checked) {
			// eventLabelArray['e'+eventId] = eventName; 
			if (dataGridChangeList[oKey] == undefined) {
				dataGridChangeList[oKey] = document.getElementById('all_'+vId).checked;
			} else {
				dataGridChangeList[oKey] = undefined;
			}
			input.checked = document.getElementById('all_'+vId).checked;
		}
	}
}
VELOS.dataGrid.eventAll = function(rId, eId) {
	var cells = $D.getChildren($(rId));
	for (var iX=0; iX<cells.length; iX++) {
		if (!cells[iX].className) { continue; }
		var pattern = /\byui-dt-col-v[0-9]+\b/;
		var cName = cells[iX].className.match(pattern);
		if (!cName) { continue; }
		var vId = (''+cName).slice(11); // To skip 'yui-dt-col-'
		var el = new YAHOO.util.Element(cells[iX]);
		var input = el.getElementsByClassName('yui-dt-checkbox', 'input')[0];
		if (input.disabled) { continue; }
		if (input.checked != $('all_e'+eId).checked) {
			var oKey = 'e'+eId+vId;
			if (dataGridChangeList[oKey] == undefined) {
				dataGridChangeList[oKey] = $('all_e'+eId).checked;
			} else {
				dataGridChangeList[oKey] = undefined;
			}
			input.checked = $('all_e'+eId).checked;
		}
	}
}
VELOS.dataGrid.saveDialog = function() {
	if (!$('saveDialog')) {
		var saveDialog = document.createElement('div');
		saveDialog.setAttribute('id', 'saveDialog');
		saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CnfrmVst_EvtChg/*Confirm Visit/Event Changes*****/+'</div>';
		saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
		$D.insertAfter(saveDialog, $('save_changes'));
	}
	
	var noChange = true;
	var numChanges = 0;
	var numDeletes = 0;
	var maxChangesDisplayedBeforeScroll = 25;
	$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="updateEvtVisits.jsp?'+incomingUrlParams+'"><div id="insertFormContent"></div></form>';
	var insertFormContents="";
	for (oKey in dataGridChangeList) {
		if (oKey.match(/^e[0-9]+v[0-9]+$/) && dataGridChangeList[oKey] != undefined) {
			if (!dataGridChangeList[oKey]) { numDeletes++; }
			noChange = false;
			var visitName = visitLabelArray[oKey.slice(oKey.indexOf('v'))];
			var eventName = eventLabelArray[oKey.slice(0, oKey.indexOf('v'))];
			insertFormContents += visitName+' / '+eventName+' => '
				+(dataGridChangeList[oKey]?Selc:Unselc)/*(dataGridChangeList[oKey]?"<b>S</b>elected":"<b>U</b>nselected")*****/+'<br/>';
			numChanges++;
			var opVal = (dataGridChangeList[oKey]?'A':'D')+'||'+oKey+'||'+
				visitDisplacementArray[oKey.slice(oKey.indexOf('v'))]+'||'+eventName+'||'+visitName;
			//YK: Fix for Bug#7264
			var visitHideFlags=oKey.slice(oKey.indexOf('v')+1)+'||'+visitHideFlag[oKey.slice(oKey.indexOf('v'))];
			insertFormContents += 
				'<input type="hidden" name="ops" value="'+opVal+'" /><input type="hidden" name="visitHide" id="visitHide" value="'+visitHideFlags+'" />';
		}
	}	
	if (numChanges < maxChangesDisplayedBeforeScroll) {
		$('insertForm').style.width = null;
		$('insertForm').style.height = null;
		$('insertForm').style.overflow = 'visible';
	} else {
		$('insertForm').style.width = '51em';
		$('insertForm').style.height = '35em';
		$('insertForm').style.overflow = 'scroll';
	}
	if (noChange) {
		insertFormContents += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
		noChange = true;
	} else if (numChanges > 1000) {
		insertFormContents += '<table width="470px"><tr><td>&nbsp;</td></tr><tr><td style="font-size:8pt;"><b>'+
		M_1000OrLessChanges+'</b><br/>'+L_Current_Count+'&nbsp;'+
		  '<input onkeypress="return false;" onkeyup="return false;" readonly="readonly" name="eSign" id="eSign" value="'+numChanges+'" /></td></tr></table>';
		noChange = true;
	} else {
		insertFormContents += Total_Chg/*Total changes*****/+': '+numChanges+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
			'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
			'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
			' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
			' onkeypress="return VELOS.dataGrid.fnOnceEnterKeyPress(event)" '+
			' />&nbsp;</td></tr></table>';
	}
	$('insertFormContent').innerHTML = insertFormContents;
	var myDialog = new YAHOO.widget.Dialog('saveDialog', 
			{
				visible:false, fixedcenter:true, modal:true, resizeable:true,
				draggable:"true", autofillheight:"body", constraintoviewport:false
			});
	var handleCancel = function(e) {
		myDialog.cancel();
	};
	var handleSubmit = function(e) {
		try {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
				alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
			//Changes By Sudhir On 3/30/2012 for Bug # 5796
        	if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) { /* if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) { *****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
			//Changes By Sudhir On 3/30/2012 for Bug # 5796
	        var thisTimeSubmitted = new Date();
	        if (lastTimeSubmitted && thisTimeSubmitted) {
	        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
		        lastTimeSubmitted = thisTimeSubmitted;
	        }
		} catch(e) {}
		myDialog.submit();
	};
	var onButtonsReady = function() {
	    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
	    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
	}
	YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
	var myButtons = noChange ? 
		[   { text: getLocalizedMessageString("L_Close"), handler: handleCancel } ] :
		[
			{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
			{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
		];
	var onSuccess = function(o) {
		hideTransitPanel();
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
		} catch(e) {
			// Log error here
			return;
		}
		if (respJ.result == 0) {
			// alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);
			showFlashPanel(respJ.resultMsg);
			// YK : Added for Bug#7470
			var sortOrder= document.getElementById("sortOrder").value;
			if(!(sortOrder!=undefined && sortOrder!="")){
				sortOrder="";
			}
			if (window.parent.reloadDataGrid) { setTimeout('window.parent.reloadDataGrid(\''+sortOrder+'\');', 1000); }
		} else {
			var paramArray = [respJ.resultMsg];
		    alert(getLocalizedMessageString("L_Error_Cl", paramArray));/*alert("Error: " + respJ.resultMsg);*****/
		}
	};
	var onFailure = function(o) {
		hideTransitPanel();
		var paramArray = [o.status];
	    alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp", paramArray));/*alert(svrCommErr+" : " + o.status);*****/ /*Error Message displayed from local var*/
	};
	var onStart = function(o) {
		showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
	}
	myDialog.callback.customevents = { onStart:onStart };
	myDialog.callback.success = onSuccess;
	myDialog.callback.failure = onFailure;
	myDialog.cfg.queueProperty("buttons", myButtons);
	myDialog.render(document.body);
	myDialog.show();
	if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
	
	var showFlashPanel = function(msg) {
		// Initialize the temporary Panel to display while waiting for external content to load
		if (!(VELOS.flashPanel)) {
			VELOS.flashPanel = 
				new YAHOO.widget.Panel("flashPanel",  
					{ width:"350px", 
					  fixedcenter:true, 
					  close:false, 
					  draggable:false, 
					  zindex:4,
					  modal:true,
					  visible:false
					} 
				);
		}
		VELOS.flashPanel.setHeader("");
		VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.flashPanel.render(document.body);
		VELOS.flashPanel.show();
		setTimeout('VELOS.flashPanel.hide();', 1000);
	}
	
	var showTransitPanel = function(msg) {
		if (!VELOS.transitPanel) {
			VELOS.transitPanel = 
				new YAHOO.widget.Panel("transitPanel",  
					{ width:"240px", 
					  fixedcenter:true, 
					  close:false, 
					  draggable:false, 
					  zindex:4,
					  modal:true,
					  visible:false
					} 
				);
		}
		VELOS.transitPanel.setHeader("");
		VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.transitPanel.render(document.body);
		VELOS.transitPanel.show();
	}
	
	var hideTransitPanel = function() {
		if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
	}
	
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
}

VELOS.dataGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'serverpagination'; }
	if ((args.success) && (typeof args.success == 'function')) { 
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.dataGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.dataGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.dataGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait = 
			new YAHOO.widget.Panel("wait",  
				{ width:"240px", 
				  fixedcenter:true, 
				  close:false, 
				  draggable:false, 
				  zindex:4,
				  modal:true,
				  visible:false
				} 
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.dataGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.dataGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}
//PCAL-20461: To check any change in the Grid before going further.
VELOS.dataGrid.checkDataChange = function() {
	for (oKey in dataGridChangeList) {
		if (oKey.match(/^e[0-9]+v[0-9]+$/) && dataGridChangeList[oKey] != undefined) {
			return true;
		}
		return false;
	}
}