<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Copy_FldToFrm%><%--Copy Field to Form*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT Language="javascript">

 function  validate(formobj){
	  if (!(validate_col('Section Name',formobj.section))) return false
		if (!(validate_col('Field Name',formobj.field))) return false
		   if (!(validate_col('e-Signature',formobj.eSign))) return false
	 if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	  }
	  
   }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formLibJB" scope="request"  class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>



<body>
<br>
<DIV class="popDefault" id="div1"> 
  <%

	String pullDownSection="";
	String pullDownField="";
	String sectionId ="";
	String sectionName="";
	String fieldId ="";
	String fieldName ="";
	
	ArrayList sectionIds = new ArrayList();
	ArrayList sectionNames = new ArrayList();
	ArrayList fieldIds = new ArrayList(); 
	ArrayList fieldNames = new ArrayList(); 


	String from = request.getParameter("from");
	if (from==null) {
		from="initial";
	}

	int  formId= EJBUtil.stringToNum(request.getParameter("formId"));
	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
		
		 String txtSection=request.getParameter("section");
		 int iSection= EJBUtil.stringToNum(txtSection);
		 String txtField=request.getParameter("field");
		 int iField= EJBUtil.stringToNum(txtField);
		
		String uName = (String) tSession.getValue("userName");
		String usr = (String) tSession.getValue("userId");
    	String ipAdd = (String) tSession.getValue("ipAdd");
		
		FormSecDao formsecDao = new FormSecDao();
		FieldLibDao fieldLibDao = new FieldLibDao();
		
		
	if(from.equals("initial")) {

      
		formsecDao.getAllSectionsOfAForm(formId);
		sectionNames = formsecDao.getFormSecName();
		sectionIds = formsecDao.getFormSecId();
		pullDownSection=EJBUtil.createPullDown("section", 0, sectionIds, sectionNames);
		
		fieldLibDao = fieldLibJB.getFltrdFieldNames(formId);
		
		fieldIds = fieldLibDao.getFieldLibId();
		fieldNames = fieldLibDao.getFldName();
		
		pullDownField=EJBUtil.createPullDown("field", 0, fieldIds, fieldNames);
%>

<P class="sectionHeadings"> <%=LC.L_Field_Copy%><%--Field >> Copy*****--%> </P>

<Form name="copyfield" id="cpyfieldfrm" method="post" action="copyfieldtoform.jsp?formId=<%=formId%>&from=final" onsubmit="if (validate(document.copyfield)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >

<P class = "defComments"><table><tr><td> <%=MC.M_SelSec_WantToCopyFld%><%--Please select the Section to which you want to copy the field*****--%> <FONT class="Mandatory">* </FONT></td>  </P>
	<td ><%=pullDownSection%> </td></tr>

<tr> <td><%=MC.M_SelFld_ToCopy%><%--Please select the Field to be copied*****--%> <FONT class="Mandatory">* </FONT></td>  
<td ><%=pullDownField%> </td>
</tr>
</table>
<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="cpyfieldfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
	
</form>

<%
} //end of if for checking whether from.equals("initial")


if(from.equals("final")) {
	 String eSign = request.getParameter("eSign");  
	 String oldESign = (String) tSession.getValue("eSign");
	   
	 if(!oldESign.equals(eSign)) 
	{
%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	} 
	else
	{
		 	    
		formsecDao.getAllSectionsOfAForm(formId);
		sectionNames = formsecDao.getFormSecName();
		sectionIds = formsecDao.getFormSecId();
		pullDownSection=EJBUtil.createPullDown("section", iSection, sectionIds, sectionNames);
		fieldLibDao = fieldLibJB.getFltrdFieldNames(formId);
		fieldIds = fieldLibDao.getFieldLibId();
		fieldNames = fieldLibDao.getFldName();
		pullDownField=EJBUtil.createPullDown("field", iField, fieldIds, fieldNames);

	   int ret=formLibJB.insertCopyFieldToForm(formId,iSection,iField,usr,ipAdd);
	
	 
	  if(ret==-1){%>
	     <P class = "defComments"> <%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully.*****--%> </P>
	  <%}
	  else{%>
	    <P class = "defComments"> <%=MC.M_Fld_BeenCopiedSucc%><%--The field has been copied successfully.*****--%> </P>
	 <% }%>
	  <script>
	    window.opener.location.reload();
		setTimeout("self.close()",1500);
		</script>
	<%
	  }//esign
	}// end of if for checking whether from.equals("final")
} // end if if session valid

else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>


