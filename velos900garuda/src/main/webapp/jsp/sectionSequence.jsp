<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=LC.L_Pcol_Secs%><%--Protocol Sections*****--%></title>
<% } %>


<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>





<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT Language="javascript">

 function  validate(formobj){



	if (!(validate_col('e-Signature',formobj.eSign))) return false

     if(isNaN(formobj.eSign.value) == true) {
    alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

}

</SCRIPT>

<% String src="";
String from = "sectionver";
src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<body>
<jsp:useBean id="sectionB" scope="session" class="com.velos.eres.web.section.SectionJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB"%>
<DIV class="browserDefault" id="div1">
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>
  </jsp:include>
  <%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))

	{

	String uName =(String) tSession.getValue("userName");

	String study = (String) tSession.getValue("studyId");
	int studyVerId = EJBUtil.stringToNum(request.getParameter("studyVerId"));


	   int pageRight= 0;

	   if(study == "" || study == null) {

%>
  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%

	   } else {

		   int studyId = EJBUtil.stringToNum(study);

		   String flag = "a";

		   String tab = request.getParameter("selectedTab");

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	   	   if ((stdRights.getFtrRights().size()) == 0){

		 	pageRight= 0;

		   }else{

			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));

	   	   }

	   	if (pageRight > 0) {

		   SectionDao sectionDao = sectionB.getStudy(studyVerId,flag,"");
		   ArrayList ids = sectionDao.getIds();
		   ArrayList secNames = sectionDao.getSecNames();
	   	   ArrayList secSequences = sectionDao.getSecSequences();

		   String studySecName = "";
		   String sectionId="";
		   String secSequence = "";
	   	   int len= ids.size();
		   int counter = 0;

%>

  <Form name="sectionsequence" id ="secseqfrm" method="post" action="updateSectionSequence.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>" onsubmit = "if (validate(document.sectionsequence)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr >
           <P class = "defComments"> <%=MC.M_EtrSeqNum_CaseArgAlphabet%><%--You can enter the sequence of the sections by changing the number in front of each section.In case of equal sequence numbers the sections are arranged alphabetically.*****--%>
	  </P>
      </tr>
    </table>
	<input type='hidden' name='length' value=<%=len%>></input>
	<input type='hidden' name='studyVerId' value=<%=studyVerId%>></input>
    <table width="100%" >
      <tr>
	<th width="10%"> <%=LC.L_Sr_No%><%--Sr. No.*****--%> </th>
        <th width="75%"> <%=MC.M_SecOf_TheStd%><%--Section(s) of the study*****--%> </th>
	<th width="10%"> <%=LC.L_Seq_Number%><%--Sequence Number*****--%> </th>
      </tr>

      <%



    for(counter = 0;counter<len;counter++)

	{



		studySecName=((secNames.get(counter)) == null)?"-":(secNames.get(counter)).toString();
		sectionId=((ids.get(counter)) == null)?"-":(ids.get(counter)).toString();
		secSequence=((secSequences.get(counter)) == null)?"0":(secSequences.get(counter)).toString();

		if ((counter%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%

		}

		else{

  %>
      <tr class="browserOddRow">
        <%

		}

  %>
        <td> <%=counter+1%>
        </td>
	<td> <%=studySecName%>
	</td>
	<td align='center'>
	   <input type='hidden' name="sectionId<%=counter%>" value=<%=sectionId%>>  </input>
	   <input type=text name="secSequence<%=counter%>" value=<%=secSequence%> size='3' > </input>
	</td>
      </tr>
      <%

		}

%>
	</table>



<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="secseqfrm"/>
	<jsp:param name="showDiscard" value="N"/>
</jsp:include>



  </Form>
  <%}else{%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%}

	} //end of if body for check on studyId

	} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu">
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>

</html>
