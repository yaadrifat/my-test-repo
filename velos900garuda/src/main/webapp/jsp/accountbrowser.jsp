<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%>
<html>
<head>
<%String viewList=request.getParameter("cmbViewList") ;
if(viewList==null){
	viewList= "AA";   
	}
	
if(viewList.equals("AA")){%>
	<%--<title>Manage Account >> Active Account Users</title>*****--%>
	<title><%=MC.M_MngAcc_ActAccUsr%></title>
<%}else if(viewList.equals("NS")){%>
	<%--<title>Manage Account >> Non System Users</title>*****--%>
	<title><%=MC.M_MngAcc_NonSysUsr%></title>
<%}else if(viewList.equals("BD")){%>
	
	<%--<title>Manage Account >> Blocked/Deactivated Users</title>*****--%>
	<title><%=MC.M_MngAcc_BlkOrDeactUsr%></title>
<%}%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.service.util.EJBUtil" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="Javascript1.2">
	function openReport(){
// INF-20084 Datepicker-- AGodara
		window.open("rep_activityreport.jsp",'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=550,height=350');
	}
</SCRIPT>

<SCRIPT language="javascript">

function confirmBox(name,pgRight,viewList) {
	if (f_check_perm(pgRight,'E') == true) {
		name = decodeString(name);
		if(viewList=="AA")
		{
		  var paramArray = [name];
		  if (confirm(getLocalizedMessageString("L_Del_FrmGrp",paramArray))) {/*if (confirm("Delete " + name + " from Group?")) {*****/
			
				return true;
			}
			else{
				return false;
				}
		}
		else if(viewList=="NS")
			{
			 var paramArray = [name];
			 if (confirm(getLocalizedMessageString("L_Del",paramArray))) {/*if (confirm("Delete " + name)) {*****/
			return true;
			}
			else	
				{	
				 return false;
				}
			}
	}else {
		return false;
	}
}
//JM: 20September2006
function fAddMultUserToStudies(pgRight){

	  windowName =window.open('addUsersToMultipleStudies.jsp?pageRight='+pgRight, 'Information', 'toolbar=no,scrollbar=yes,resizable=yes,menubar=no,status=yes,width=820,height=450, top=100, left=100');
	  windowName.focus();

}

function fopen(link,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		windowName = window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=280')
		windowName.focus();
	} else {
		return false;
	}
}

function fEditRight(pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		return true;
	} else {
		return false;
	}
}

//Added by Gopu for July-August'06 Enhancement (#U2) - Admin Settings(Default account login user is Admin then open the admin settings popup page)

function openadminsettings(pageRight) {	
	  
	  windowName=window.open("adminSettings.jsp?pageRight="+pageRight,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=780,height=429, top = 100 , left = 100");
	  windowName.focus();
}
</SCRIPT>
 
</head>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=StringUtil.htmlEncodeXss(src)%>"/>
</jsp:include>   

<body>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.VelosResourceBundle"%>
<%@ page import="com.velos.eres.service.util.FilterUtil"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache"%>


  <%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{

	String uName = (String) tSession.getValue("userName");



	int pageRight = 0;

	String accId = (String) tSession.getValue("accountId");

	//get the default group of the login user. If the default group is Admin then show reset password and reset user session for users
	String logUsr = (String) tSession.getValue("userId");
    if (request.getParameter("refreshCache") != null 
	        && logUsr.equals(request.getParameter("usrId"))) {
        ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
	    objCache.populateObjectSettings();
	    FilterUtil.fillParamHashFromLkpFilterDao();
	    VelosResourceBundle.reloadProperties();
	    LC.reload();
	    MC.reload();
	}
	int loginUser = EJBUtil.stringToNum(logUsr);
	userB.setUserId(loginUser);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = ((groupB.getGroupName())==null)?"-":(groupB.getGroupName());
	
	
	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));	

%>

<DIV class="tabDefTopN" id = "div1"> 

	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="3"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 

<%

   if (pageRight > 0 )

	 {
       ArrayList usrLastNames = null;

       ArrayList usrFirstNames = null;

	   ArrayList usrLoggedInFlags = null;	   

       ArrayList usrMidNames;

       ArrayList siteNames;

       ArrayList jobTypes;
	   ArrayList types = null;

       ArrayList usrIds;	

       ArrayList grpNames = null;
       ArrayList usrGrpIds = null;

	   
	   String usrLastName = null;

       String grpName = null;

	   String usrFirstName = null;

	   String usrMidName = null;
	   String usrLoggedInFlag = null;
	   String siteName = null;

	   String jobType = null;
	   String type = null;

	   String usrId = null;	
	   String usrGrpId = null;

		ArrayList orgNames;
		ArrayList orgIds;
		int inSite = 0;

	   String oldGrp = null;	
	   //String viewList=request.getParameter("cmbViewList") ;

	   String firstName = request.getParameter("firstName");
	   String lastName = request.getParameter("lastName");
	   String siteId = request.getParameter("siteId");
	   if(firstName==null)
	   {
		 firstName="";
	   }

	   if(lastName==null)
	   {
		 lastName="";
	   }
		
	   if(siteId==null)
	   {
		   siteId="";
	   }

		if(viewList==null)
		 {
		 viewList="";
		 }

		 UserDao userDao= new UserDao();
		 //Modified by Manimaran to include first name and last name in the search.

		
		 String frstName = "";
		 String lstName= "";
		
		 frstName = StringUtil.replace(firstName, "'", "''");//KM-031208
		 lstName = StringUtil.replace(lastName, "'", "''");
		 
		 StringBuffer sbSite = new StringBuffer();
		 UserDao usrDao= new UserDao();
		 String ddSite = "";
		 if(viewList.equals("") || viewList.equals("AA"))
		 {
			 userDao = userB.getAccountUsers(Integer.parseInt(accId),"A",frstName.trim(),lstName.trim(),siteId.trim());
			 usrDao = userB.getAccountUsersSites(Integer.parseInt(accId),"A");
	
		 }
		else if(viewList.equals("NS"))
		 {
			userDao = userB.getNonSystemUsers(Integer.parseInt(accId),"N",frstName.trim(),lstName.trim(),siteId.trim());
			usrDao = userB.getNonSystemUsersSites(Integer.parseInt(accId),"N");
		 }

		else if(viewList.equals("BD"))
		 {
				userDao = userB.getUsersSelectively(Integer.parseInt(accId),"'D','B'",frstName.trim(),lstName.trim(),siteId.trim());
				usrDao = userB.getUsersSelectivelySites(Integer.parseInt(accId),"'D','B'");
		 }
		 
		 orgNames = usrDao.getUsrSiteNames();
		 orgIds = usrDao.getSiteIds();
		 sbSite.append("<SELECT NAME='siteId' id='siteId'>") ;	
		 sbSite.append("<option selected value='' >Select</option>") ;
		 if(orgIds.size() > 0) {
			 for (int i=0;i<orgIds.size();i++) {
				 inSite = Integer.parseInt((String)orgIds.get(i));
				 
				 if(inSite == EJBUtil.stringToNum(siteId)){		
					 sbSite.append("<OPTION value = "+ inSite+" selected>" + orgNames.get(i)+ "</OPTION>");
				 }
				 else
				 {
				 sbSite.append("<OPTION value = "+ inSite+">" + orgNames.get(i)+ "</OPTION>");
				 }
			}
		 }
		 sbSite.append("</SELECT>");
		 ddSite  = sbSite.toString();
 	 			
    int counter = 0;
	ArrayList uIds = userDao.getUsrIds();

	int lenUsers=uIds.size();
	
	int countUsers = 0;
	int outLoop=0;
	int inLoop=0;
	if(viewList.equals("") || viewList.equals("AA")){
		for(outLoop=0;outLoop<lenUsers;outLoop++)
			 {
				int count=0;
				if(outLoop==0)
				 {
					//countUsers ++;
				 }
				
				for(inLoop=0;inLoop<outLoop;inLoop++)
				 {
				   if(uIds.get(inLoop).equals(uIds.get(outLoop)))
					 {
						count ++;				 
					 }				 
				 }			 
				 if(count==0)
				 {	
					countUsers ++;
				 }
			 }
		
	}	

%>
  <Form name="accountBrowser" method="post"  action="accountbrowser.jsp"  onsubmit = "">
	  <input type="hidden" name=srcmenu value=<%=StringUtil.htmlEncodeXss(src)%>>
	  <div class="tmpHeight"></div>
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign" >
      <tr height="35">
		<%--<td width="12%"> <b>Search Within</b></td>*****--%>
		<td width="10%"> <b><%=LC.L_Search_Within%></b></td>
		<td width="10%"><select name="cmbViewList">	
	  <% if(viewList.equals("") || viewList.equals("AA")){%>	 
                   		
						<%--<option value="AA" selected>Active Account Users</option>*****--%>
						<option value="AA" selected><%=LC.L_ActAcc_Users%></option>
						<%--<option value="NS">Non System Users</option>*****--%>
						<option value="NS"><%=MC.M_Non_SystemUsers%></option>						
	  <%}
	 else if(viewList.equals("NS")){%>
                   		<%--<option value="AA">Active Account Users</option>*****--%>
						<option value="AA"><%=LC.L_ActAcc_Users%></option>
						<%--<option value="NS" selected>Non System Users</option>*****--%>
						<option value="NS" selected><%=MC.M_Non_SystemUsers%></option>
						
	   <%}
	   if(viewList.equals("BD") &&((groupName.compareToIgnoreCase("Admin")) == 0)){%>
	 
                   		<%--<option value="AA">Active Account Users</option>*****--%>
						<option value="AA"><%=LC.L_ActAcc_Users%></option>
						
						<%--<option value="NS">Non System Users</option>*****--%>
						<option value="NS"><%=MC.M_Non_SystemUsers%>
</option>
						<%--<option value="BD" selected>Blocked/Deactivated Users</option>*****--%>
						<option value="BD" selected><%=LC.L_Blk_DeactUser%></option>	
					
	   <%}else if(!viewList.equals("BD") &&((groupName.compareToIgnoreCase("Admin")) == 0)){%>
						
						<%--<option value="BD" >Blocked/Deactivated Users</option>*****--%>
						<option value="BD" ><%=LC.L_Blk_DeactUser%></option>		
							<%}%>
           </select></td>

		 <%--<td width="10%" align="right">By First Name:&nbsp;<td>*****--%>
		<td width="7%" align="right"><%=LC.L_By_FirstName%>:<td> 
		<td width="6%"> <input type="text" name="firstName" size="10" value="<%=StringUtil.htmlEncodeXss(firstName)%>"></td>
		
		<%--<td width="10%"  align="right">Last Name:&nbsp; <td>*****--%>
		<td width="7%"  align="right"><%=LC.L_Last_Name%>: <td> 
		<td width="6%"> <input type="text" name="lastName" size="10" value="<%=StringUtil.htmlEncodeXss(lastName)%>"></td>

		<td width="7%" align="right">Organization:<td> 
		<td width="6%"> <%=ddSite%> </td>
		
	   <!--Added by Gopu for July-August'06 Enhancement (U2) - Default account level settings for User / Patient Timezone, Seesion Timeout time, Number of days password will expire, Number of days e-Sign will expire  -->
		  <td width="6%"><button type="submit"><%=LC.L_Search%></button></td>
			<td width="25%" align="center"> 
			  
		  
		<%
		if (groupName.equalsIgnoreCase("admin")) {
		%>
 
		  <A href=# onClick="openadminsettings(<%=pageRight%>);">
		  <%--Admin Settings*****--%>
		  <%=LC.L_Admin_Settings%></A> 
		<%}%>	  
		  
		  
		  &nbsp;&nbsp;&nbsp; <A href =# onClick = "fAddMultUserToStudies(<%=pageRight%>);">
		  <%--Add User(s) to Multiple <%=LC.Std_Studies%>*****--%>
		  <%=MC.M_AddUser_SMultiStd%></A> </td>
		<%
		if (groupName.equalsIgnoreCase("admin")) {
		%>
          <td align="center" width="25%"><a  href="accountbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=3&refreshCache=1&usrId=<%=StringUtil.htmlEncodeXss(logUsr)%>">
		  <%--Refresh Menus/Tabs*****--%>
		  <%=LC.L_RefMenusOrTabs%></a></td>
		<%}%>	  
	  </tr>
  	</table>
  </Form>
  <div class="tmpHeight"></div>
			<table border="0" width="100%" cellspacing="0" cellpadding="0" >
				<tr>
					<td width="10%"><b><p class="blackComments">
				<%if(viewList.equals("") || viewList.equals("AA")){
				Object[] arguments = {countUsers};%>
				<%=VelosResourceBundle.getMessageString("M_CntOf_ActvUsers",arguments)%><%--Total Count of Active Users : <%=countUsers%>*****--%></p></b></td>
				<%}else{
				Object[] arguments = {lenUsers};%>
				<%=VelosResourceBundle.getLabelString("L_Total_Cnt",arguments)%><%--Total Count : <%=lenUsers%>*****--%></p></b></td>
				<%}%>
				<td width="10%" align="left"><a href=# onclick="openReport()"><%--ACTIVITY REPORT*****--%><%=LC.L_Activity_Rpt_Upper%></a></td>
				<td width="10%" align="left"><a href="emailuser.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&fromPage=accountbrowser"><%--EMAIL USERS*****--%><%=LC.L_Email_Users_Upper%></a></td>
        <td  width="10%" align="left"> 
			<%if(viewList.equals("") || viewList.equals("AA")){%>
         <A href="userdetails.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&userType=<%=StringUtil.htmlEncodeXss(viewList)%>"><%--ADD A NEW USER*****--%><%=MC.M_AddA_NewUser_Upper%></A>
		 </td>
         <td  width="10%" align="left">
            <A href="group.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&fromPage=accountbrowser">
			<%--ADD A NEW GROUP*****--%><%=MC.M_AddNew_Grp_Upper%></A>
			<%}if(viewList.equals("NS")){%>
			<A href="nonSystemUserDetails.jsp?mode=N&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&fromPage=accountbrowser">
			<%--ADD A NEW NON-SYSTEM USER*****--%><%=MC.M_AddNew_NonSysUsr_Upper%></A>
			<%}%>
			</tr>
			</td> 
			</table>

    <table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0">
      <tr> 
	  <%--<th width=25%> Organization </th>*****--%>
        <th width=25%> <%=LC.L_Organization%> </th>
        <%--<th width=15%> User Name </th>*****--%>
		<th width=15%> <%=LC.L_User_Name%> </th>
	<%if(viewList.equals("BD")){%>
		<%--<th width=15%> Type </th>*****--%>
		<th width=15%> <%=LC.L_Type%>
 </th>
	<%}%>
		<%--<th width=20%> Job Type </th>*****--%>
		<th width=20%> <%=LC.L_Job_Type%> </th>

		<%
		if ((groupName.compareToIgnoreCase("Admin")) == 0 && (viewList.equals("") || viewList.equals("AA"))) {
		%>
		<%--<th width=15%>Reset Password</th>*****--%>
		<th width=15%><%=LC.L_Reset_Pwd%></th>
		<%--<th width=20%>Reset User Session</th>*****--%>
		<th width=20%><%=LC.L_Reset_UserSession%></th>
		<th width="7%"><%=LC.L_Delete%><%--Delete*****--%></th>
		<%}%>
		<!-- th width=15%>&nbsp;</th--><!--commented by gopu fix the bug #2373 -->
	<%if(viewList.equals("NS")){%>
		<%--<th width="15%">User Status</th>*****--%>
		<th width="15%"><%=LC.L_User_Status%></th>
		<th width="7%"></th>
	<%}%>
      </tr>
      <%

		usrLastNames = userDao.getUsrLastNames();
		usrFirstNames = userDao.getUsrFirstNames();
		usrMidNames = userDao.getUsrMidNames();
		siteNames = userDao.getUsrSiteNames();
		jobTypes = userDao.getUsrJobTypes();
		usrIds = userDao.getUsrIds();

//JM: 18/08/05	to  get the user status of 'BD' users
		types = userDao.getUsrStats();

		if(viewList.equals("AA") || viewList.equals("")){

		usrLoggedInFlags = userDao.getUsrLoggedInFlags();

		grpNames = userDao.getUsrDefGrps();

		usrGrpIds = userDao.getUsrGrpIds();
		}

		int i;
		
		int val = 0;

		//int lenUsers = usrLastNames.size();
		int perGrpCnt[] = new int [lenUsers]; 

  if(viewList.equals("AA") || viewList.equals("")){
	  String oldName = "";	
	  if (grpNames.size() > 0 )  //KM
	  	    oldName = (String) grpNames.get(0);
	  String prevGroupName = "";
			for(int j=0,k=0; j<grpNames.size(); j++)
				{				
					prevGroupName = (String) grpNames.get(j);
					if(oldName.equals(prevGroupName))
					{
						    val = val+1;
														
					}
					else
					{
						k++;
						val = 1;
					
					}
					perGrpCnt[k] = val;	
				   oldName = prevGroupName ;						
				}
	}
		
		int cnt=0;
		for(i = 0; i < lenUsers ; i++)

		  {

		usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();

		usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();

		usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();

		siteName=((siteNames.get(i))==null)?"-":(siteNames.get(i)).toString();
		

		jobType=((jobTypes.get(i))==null)?"-":(jobTypes.get(i)).toString();
  
//JM: 18/08/05
	
		if(viewList.equals("BD") || viewList.equals("NS")){ //KM:03/11/08
		type=((types.get(i))==null)?"-":(types.get(i)).toString();
		
	if (type.equals("B")) 
			type="Blocked";
		else if (type.equals("D")) 
			type="Deactivated";
	
		else if (type.equals("A"))
			type ="Activated";
		}
		usrId = ((Integer)usrIds.get(i)).toString();
		
		if(viewList.equals("AA") || viewList.equals("")){

			usrLoggedInFlag=((usrLoggedInFlags.get(i))==null)?"-":(usrLoggedInFlags.get(i)).toString();	

			usrGrpId = (usrGrpIds.get(i)).toString();

			grpName = (String) grpNames.get(i);
		}
	
     if(viewList.equals("AA") || viewList.equals(""))
			  {
			if ( ! grpName.equals(oldGrp)){ 
				%>
      <tr id ="browserBoldRow"> 
        <td>
		<%Object[] arguments = {grpName};%>
		<%=VelosResourceBundle.getLabelString("L_Grp",arguments)%><%--Group : <%= grpName%>*****--%> </td>

					
					<td colspan="5" align="right"><b><p class="blackComments">
					<%Object[] arguments1 = {perGrpCnt[cnt]};%>
					<%=VelosResourceBundle.getLabelString("L_Cnt",arguments1)%><%--Count : <%=perGrpCnt[cnt]%>*****--%></p></b></td> 
					<%cnt++;%>
			
      </tr>
      <%}
		 
	 }
	 

		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td width = '25%'> <%=StringUtil.htmlEncodeXss(siteName)%> </td>
	  <%if(viewList.equals("NS"))
			  {%>
			<td width ='25%'> <A href = "nonSystemUserDetails.jsp?mode=M&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&userId=<%=StringUtil.htmlEncodeXss(usrId)%>">
			<%=StringUtil.htmlEncodeXss(usrFirstName)%>&nbsp;<%=StringUtil.htmlEncodeXss(usrLastName)%> 
           </A> </td>
			  <%}else{%>
        <td width ='25%'> <A href = "userdetails.jsp?mode=M&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&userId=<%=StringUtil.htmlEncodeXss(usrId)%>&userType=<%=StringUtil.htmlEncodeXss(viewList)%>">
        	<%=StringUtil.htmlEncodeXss(usrFirstName)%>&nbsp;<%=StringUtil.htmlEncodeXss(usrLastName)%> </A> </td>
	  <%}
	if(viewList.equals("BD")){%>
		<td width ='20%'> <%=StringUtil.htmlEncodeXss(type)%> </td>
	<%}%>
		<td width ='20%'> <%=StringUtil.htmlEncodeXss(jobType)%> </td>

		<%

		if ((groupName.compareToIgnoreCase("Admin")) == 0 && ((viewList.equals("AA") || viewList.equals("")))) {

			String link ="resetpass.jsp?userId=" + usrId + "&fromPage=User"; 
		%>
	    <td width ='10%'> 
		<A href=#  onClick="return fopen('<%=StringUtil.htmlEncodeXss(link)%>',<%=pageRight%>)">
		<%--Reset*****--%>
		<%=LC.L_Reset%>
		</A>
		</td>
		
	    <td width ='15%'>
		<%

		if (usrLoggedInFlag.equals("1") && !(logUsr.equals(usrId ))) {%>  
			<% link ="resetusersession.jsp?userId=" + usrId ;%>  
			<A href=#  onClick="return fopen('<%=StringUtil.htmlEncodeXss(link)%>',<%=pageRight%>)">
			<%--Reset*****--%>
			<%=LC.L_Reset%>

			</A>
		<%} else {%>
			&nbsp;
		<%}%>
		</td>
		<%
		}
		%>
	
<!--JM: 06/09/05 modified-->
		
			<%if(viewList.equals("AA") || viewList.equals("")){%>
		<td width ='15%'  align="center"> <A href = "delete_usr_from_grp.jsp?srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&usrGrpId=<%=StringUtil.htmlEncodeXss(usrGrpId)%>" 
		onClick="return confirmBox('<%= StringUtil.encodeString(usrFirstName)%>',<%=pageRight%>,'<%=StringUtil.htmlEncodeXss(viewList)%>')">
		<img src="./images/delete.gif" border="0" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>"/>
			
		</A></td>
			<%}if (viewList.equals("BD")){%>
		<td width ='10%'><A onclick ="return fEditRight(<%=pageRight%>) " href = "activateuser.jsp?srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&userId=<%=StringUtil.htmlEncodeXss(usrId)%>&userType=<%=StringUtil.htmlEncodeXss(viewList)%>&delMode=null">
		<%--Reactivate User*****--%>
		<%=LC.L_Reactivate_User%>
 </A></td>
			<%}if (viewList.equals("NS")){%>  <!-- KM:User Status is added for Non system user -->
		<td width="15%"> <%=StringUtil.htmlEncodeXss(type)%> </td>	
		  <!-- Mukul:Bug3362: StringUtil.encodeString() called to remove the Java script error  -->
		<td width ='7%' align="center"><A href = "deleteUser.jsp?srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&userId=<%=StringUtil.htmlEncodeXss(usrId)%>" 
		onClick="return confirmBox('<%= StringUtil.encodeString(usrFirstName)%>',<%=pageRight%>,'<%=StringUtil.htmlEncodeXss(viewList)%>')">
		<img src="./images/delete.gif" border="0" title="<%=LC.L_Delete%>" alt="<%=LC.L_Delete%>"/> </A></td>
		
		<td width ='20%'><A href="userdetails.jsp?mode=M&srcmenu=<%=StringUtil.htmlEncodeXss(src)%>&userType=<%=StringUtil.htmlEncodeXss(viewList)%>&userId=<%=StringUtil.htmlEncodeXss(usrId)%>">
		<%--Convert to Active User*****--%>
		<%=MC.M_Convert_ToActUser%>
 </A></td>
			<%}%>
		</td>
<!--JM-->			
		
		
      </tr>
      <%

		oldGrp = grpName;
		
		}

	

%>
    </table>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<BR>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>
