<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*, com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>

<title><%=LC.L_PrsnlzAcc_Set%><%--Personalize Account >> Settings*****--%></title>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT Language="javascript">

 function  validate(formobj)

   {

	 if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

     if (!(validate_col('session',formobj.sessionTime))) return false


	if(isNaN(formobj.sessionTime.value) == true) {
	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number");*****/
	formobj.sessionTime .focus();
	return false;
   }

	//Modified by Manimaran to fix the Bug2717  Ankit: Bug-14319, Date-16/Jan/2013
    	if ((parseInt(formobj.sessionTime.value) < 10) || (parseInt(formobj.sessionTime.value) >30)) {
	    alert("<%=MC.M_NotLess10_NotMore30%>");
	    return false
	}
     }

</SCRIPT>


<% String src;

src= request.getParameter("srcmenu");

%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%

int pageRight = 0;

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

 {

   String uName = (String) tSession.getValue("userName");

   String accId=(String)tSession.getValue("accountId");

    GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

    //pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSETTINGS"));

	pageRight = 7;
    String tab = request.getParameter("selectedTab");
   //Added for July-August'06 Enhancement (U2) - Admin Settings for Session timeout.
		String setvalueover="1";
		SettingsDao settingsDao=commonB.getSettingsInstance();
		String keyword="ACC_SESSION_TIMEOUT_OVERRIDE";
		String usr;
		int modname=1;
		settingsDao.retrieveSettings(EJBUtil.stringToNum(accId),modname,keyword);
		ArrayList setvalueovers=settingsDao.getSettingValue();
		if(setvalueovers!=null && setvalueovers.size()>0)
		setvalueover=(setvalueovers.get(0)==null)?"":(setvalueovers.get(0)).toString();

	  	CodeDao cd = new CodeDao();
	  	CodeDao cd1 = new CodeDao();
	  	CodeDao cd2 = new CodeDao();
	  	
  	 	cd.getCodeValues("skin");
  	 	cd1.getCodeValues("theme");
  	 	cd2.getCodeValues("tabledisp");
	  	String dSkin = "" ;
	  	String dHomePage = "" ;
	  	String dTableDisplay = "";
		String skin = "" ;
		String themeHomePage = ""  ;
		String skinSelectedId="";
		String themeSelectedId="";
		String skinSelectedDesc="";
		String themeSelectedDesc="";
		String tableSelectedId="";

		int userId = EJBUtil.stringToNum((String) tSession.getValue("userId"));

		userB.setUserId(userId);
		userB.getUserDetails();
		themeSelectedId  = userB.getUserTheme();
		skinSelectedId  = userB.getUserSkin();
		tableSelectedId = userB.getTableRows();

		if (themeSelectedId == null){

			themeSelectedId = "";

		}

		if (skinSelectedId == null) {

			skinSelectedId = "";
		}
		
		if (tableSelectedId == null) {

			tableSelectedId = "";
		}


	//	skinSelectedDesc = codeLst.getCodeDescription(EJBUtil.stringToNum(themeSelectedId) );
	//	themeSelectedDesc = codeLst.getCodeDescription(EJBUtil.stringToNum(themeSelectedId) );


	%>

<DIV class="tabDefTopN" id="div2">
    <jsp:include page="personalizetabs.jsp" flush="true"/>

</DIV>

<DIV class="tabDefBotN" id="div3">
	<%
    if (pageRight > 0 )
	{
	    String uSession  = (String) tSession.getValue("userSession");

	if ((pageRight > 4) ) {
		%>
  <Form name = "settings" id="settingsForm" method="post" action="updatesettings.jsp" onsubmit="if (validate(document.settings)==false){setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
  <%}else{%>
  <Form name = "settings" method="POST">

  <%}

  //Added by IA 12.13.2006 Enhancement UI-1
    dSkin = cd.toPullDown("skin", EJBUtil.stringToNum(skinSelectedId));
    dHomePage = cd1.toPullDown("theme", EJBUtil.stringToNum(themeSelectedId));
    dTableDisplay = cd2.toPullDown("tabledisp", EJBUtil.stringToNum(tableSelectedId));
    
  %>


    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <input type="hidden" name="src" size = 15 value = "<%=src%>">

    <table width="99%" cellspacing="2" cellpadding="5" border="0" >
	<!--  <tr><td>&nbsp;</td></tr>  -->
        <tr> <td>
          <P class = "defComments_txt"> <%=MC.M_AutoLout_ProtectsAcc%><%--The automatic logout feature protects the privacy and security of
            your account. In case you are not working on your application or walk
            away from your computer, your account will automatically logout after
            an elapse of the duration specified below.*****--%></P>
      	</td></tr>
    </table>

    <table width="99%" cellspacing="2" cellpadding="5" border="0" class="basetbl midalign">
     <tr>
        <td width="20%"> <%=LC.L_Auto_LogoutTime%><%--Automatic Logout Time*****--%> <FONT class="Mandatory">* </FONT> </td>

			<%
		if((EJBUtil.stringToNum(setvalueover))==0){
		%>
			<td width="80%" align="left">
				<input type="text" name="sessionTime" size = 15 MAXLENGTH = 2 readonly value = "<%=uSession%>">
			</td>
		<%}
		else {
		%>
			<td width="80%" align="left">
			  <input type="text" name="sessionTime" size = 15 MAXLENGTH = 2 value = "<%=uSession%>">
			</td>
		<%}%>
	  </tr>
	  <tr> <td width="20%"> <%=LC.L_Color_Schema%><%--Color Schema*****--%> </td> <td  width="80%" align="left"><%=dSkin%></td> </tr>
	  <tr><td width="20%"><%=LC.L_Default_Homepage%><%--Default Homepage*****--%>  </td> <td  width="80%" align="left"><%=dHomePage %></td> </tr>
	   <tr><td width="20%">Table Display<%--Table Display*****--%>  </td> <td  width="80%" align="left"><%=dTableDisplay %></td> </tr>

	</table>
<br>
	<% if (pageRight > 4) {%>
<table width="99%" cellspacing="0" cellpadding="0" class="esignStyle midAlign"><tr><td>	
    <!--JM: 19June2009: implementing common Submit Bar in all pages -->
	<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="settingsForm"/>
	<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
</td></tr></table>

	<%}%>
    <table >
      <tr>
        <td align="right">
          <%

    if (pageRight > 4 )

	{

%>
          <!--<input type="Submit" name="submit" value="Submit">-->
	  <!--<input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle" border="0">	-->

          <%

	}

else

	{

%>
          <!--<input type="Submit" name="submit" value="Submit" DISABLED>-->
          <%

	}

%>
        </td>
      </tr>
    </table>

</DIV>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>

