<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Link_Dets%><%--Link Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT Language="javascript">

 function  validate(formobj){

//     formobj=document.link

     if (!(validate_col('Link URL',formobj.lnkURI))) return false

	if(formobj.lnkType.value=='account')
	{
     if (!(validate_col('Link Type',formobj.lnk_type))) return false
	}

     if (!(validate_col('Description',formobj.lnkDesc))) return false
     if (!(checkquote(formobj.lnkDesc.value))) return false


	  if (!(validate_col('e-Signature',formobj.eSign))) return false

 if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

     }

</SCRIPT>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<jsp:useBean id="linkB" scope="session" class="com.velos.eres.web.ulink.ULinkJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



<body>
  <%

	int lnkId =0;

	String userId ="";

	String accId ="";

	String URI ="";

	String desc ="";

	String grpName ="";

	String mode="";
	String dCur = "";
	String lnk_type="";


	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{






	String uName = (String) tSession.getValue("userName");

        mode = request.getParameter("mode");
	String tab = request.getParameter("selectedTab");

	String lnkType = request.getParameter("lnkType");

	CodeDao cdType = new CodeDao();

	if(lnkType.equals("account")) {

	cdType.getCodeValues("lnk_type");
	}




//	SchCodeDao cd = new SchCodeDao();

//	cd.getCodeValues("lnk_type");
//	dCur =  cd.toPullDown("lnk_type");







	int pageRight = 0;

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

		if(lnkType.equals("user")) {
        	   //pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("HPERSHOME"));
			   pageRight = 7;
		} else {
		   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCLINKS"));
		}

%>

<DIV class="tabDefTopN" id="divtab">
<%
   if(lnkType.equals("user")) {
%>

  <jsp:include page="personalizetabs.jsp" flush="true"/>
<%
   } else {
%>
<jsp:include page="accounttabs.jsp" flush="true">
	<jsp:param name="selectedTab" value="4"/>
	</jsp:include>
<%
   }
%>
</DIV>
<DIV class="tabDefBotN" id="div1">

<%

	if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )

	{



	      lnkId = EJBUtil.stringToNum(request.getParameter("lnkId"));



		if (mode.equals("M")) {

			linkB.setLnkId(lnkId);

			linkB.getULinkDetails();

			desc  = linkB.getLnkDesc();

			userId  = linkB.getLnkUserId ();

			URI  = linkB.getLnkURI();

			grpName = linkB.getLnkGrpName();
//JM: 03/07/2006:
			if(grpName==null) grpName="";

			accId = linkB.getLnkAccId();
			lnk_type=linkB.getLnkType();


			if(lnkType.equals("account")) {

			 dCur =  cdType.toPullDown("lnk_type",EJBUtil.stringToNum(lnk_type));
			}
	   	}else{

			UserJB user = (UserJB) (tSession.getValue("currentUser"));

			userId  = String.valueOf(user.getUserId());

			accId = user.getUserAccountId();

			 dCur =  cdType.toPullDown("lnk_type");


		}

%>
<%%>



  <Form name="link" id="linkForm" method="post" action="updatelink.jsp" onsubmit="if (validate(document.link)==false){setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">

    <P class = "sectionHeadings"><%=LC.L_Enter_LinkDets%><%--Enter Link Details*****--%> </P>
    <table width="98%" cellspacing="2" cellpadding="2">
    		 <tr>
      <td colspan="2">
        <P class="defComments_txt"> - <%=MC.M_LnkSecHead_EtrName%><%--To include this link under an existing section heading,
          enter it's name here. If you provide a new name, the link will be placed
          under the new Section Heading.*****--%> </P>
       </td>
      </tr>
      <tr>
        <td colspan="2">
          <P class="defComments_txt"> - <%=MC.M_EtrCpltUrl_Mylink255Max%><%-- Enter complete url eg. 'http://www.myLink.com'
            or &nbsp 'ftp://ftp.myLink.com'(255 char max.)*****--%> </P>
        </td>
	</tr>
      <tr><td>&nbsp </td></tr>
      <tr>
        <td width="15%"> <%=LC.L_Sec_Heading%><%--Section Heading*****--%> </td>
        <td>
          <input type="text" name="lnkGrpName" size = 45 MAXLENGTH = 50 value="<%=grpName%>">
        </td>
      </tr>

      <tr>
        <td > <%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="text" name="lnkURI" size = 45 MAXLENGTH = 300 value="<%=URI%>">
        </td>

	<% if(!(lnkType.equals("user"))) {%>
      <tr>
        <td > <%=LC.L_Link_Type%><%--Link Type*****--%> <FONT class="Mandatory">* </FONT> </td>
	  <td>	<%=dCur%> </td>
	</tr>
	<tr><td>&nbsp</td></tr>

	<%}%>

      <tr>
        <td > <%=LC.L_Description%><%--Description*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="text" name="lnkDesc" size = 45 MAXLENGTH = 300 value="<%=desc%>">
        </td>
      </tr>
    </table>
    <br>
    <input type="hidden" name="lnkAccId" value="<%=accId%>">
    <input type="hidden" name="lnkUserId" value="<%=userId%>">
    <input type="hidden" name="lnkType" value="<%=lnkType%>">
    <input type="hidden" name="mode" value="<%=mode%>">
    <input type="hidden" name="lnkId" value="<%=lnkId%>">
    <input type="hidden" name="src" value="<%=src%>">
    <Input type="hidden" name="selectedTab" value="<%=tab%>">

	<!--JM: 18June2009: implementing common Submit Bar in all pages -->
<table width="100%" cellspacing="0" cellpadding="0" class="esignStyle"><tr><td>	
	<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="linkForm"/>
	<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
</td></tr></table>




    <table width="500" >
      <tr>
        <td align="right">
          <!--<input type="Submit" name="submit" value="Submit">-->
	   <!--<input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle" border="0">	-->
        </td>
      </tr>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
 <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>

