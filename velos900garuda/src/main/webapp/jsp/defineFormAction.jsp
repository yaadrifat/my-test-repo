<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Define Inter-Form Action</title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>


<SCRIPT Language="javascript">

function openFormsLookup(formobj,mode) 
	{
				
		accId=formobj.accId.value;
		userId=formobj.userId.value;
		studyId=formobj.studyId.value;
		var maxSelect = "&maxselect=1";
		
		if (studyId=="")
		{
			studyId="0";
		}
		
		type=formobj.type.value;
		type=type.substr(0,1);
		var filter="";
		
		var fieldName="";		
		
		 
		filter =  getWhereClauseForFormsLookup(type, accId,userId,"",parseInt(studyId),0);
		formobj.dfilter.value=filter;
		formobj.target='formLkp';		
		formobj.action="multilookup.jsp?viewId=&viewName=Form Browser&form=formAction&seperator=,&frameMode=LR&keyword=linkFormName|VELFORMNAME~targetFormId|VELFORMPK|[VELHIDE]&filter=[VELGET:dfilter]" + maxSelect;
			
		windowname=window.open('donotdelete.html' ,'formLkp',"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=600 top=100,left=100 0, ");
		formobj.submit();
		formobj.action="updateFormAction.jsp";
		formobj.target="";
		windowname.focus();	
				
		
	}
	function openFieldLookup(formobj)
	{
				
		
		var formId=formobj.targetFormId.value;
		var maxSelect = "&maxselect=1";
//061708ML	verifying target from is selected before selecting a field
		if ((fnTrimSpaces(formId)=='') || formId==0)
        {
            alert("Please select a Target Form first!");
            return false;
        }
// end of 061708ML		
		formobj.dfilter.value="pk_formlib =" +formId+" and nvl(fld_repeatflag,0) = 0  and formsec_repno = 0  and fld_systemid <> 'er_def_date_01'  and fld_datatype in ('ED','EN') ";
		formobj.target='formLkp';		
		formobj.action="multilookup.jsp?viewId=6052&viewName=&form=formAction&seperator=,&frameMode=LR&keyword=disptargetvalue|field_name~veltargetid|fld_systemid|[VELHIDE]&filter=[VELGET:dfilter]" + maxSelect;
		windowname=window.open('donotdelete.html' ,'formLkp',"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=600 top=100,left=100 0, ");
		formobj.submit();
		formobj.action="updateFormAction.jsp";
		formobj.target="";
		windowname.focus();	
				
		
	}
	
 function  validate(formobj)
 {
 
 //061708 ML Below are validating the forms fields are not NULL
   
   if (  !(validate_col('Select Source Field',formobj.sourceFldDD     )     )   ) return false;
   if (  !(validate_col('Select Action Condition',formobj.actionCondition     )     )   ) return false;
   if (  !(validate_col('\'Select Target Fields\'',formobj.disptargetvalue)     )   ) return false;
  
 // end of 061708 ML       
    if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false;
    
    if(isNaN(formobj.eSign.value) == true) 
    {
        alert("Incorrect e-Signature. Please enter again");
        formobj.eSign.focus();
         return false;
       }

}



function openConditionValue(formobj)
{
        sourceFld = formobj.velsourcefldid.value;
        if ((fnTrimSpaces(sourceFld)=='') || sourceFld==0)
        {
            alert("Please select a Source Field");
            return false;
        }
        
        sourceFormName = formobj.name;
        conditionDispFld = "dispconditionvalue";
        conditionIdFld = "velconditionid";
        conditionKeywordFld = "velconditionvalue";
        
        paramStr="selectActionConditionValues.jsp?fieldLibId="+sourceFld+"&sourceFormName="+sourceFormName+"&conditionDispFld="+conditionDispFld+"&conditionIdFld="+conditionIdFld + "&conditionKeywordFld=" + conditionKeywordFld;
        
        openPopWindow(paramStr);
}

function openTarget(formobj)
{
        sourceFormName = formobj.name;
        targetDispFld = "disptargetvalue";
        targetIdFld = "veltargetid";
        targetKeywordFld = "veltarget";
        formId = formobj.targetFormId.value;
        paramStr="selectActionTargetFields.jsp?formId="+formId +"&sourceFormName="+sourceFormName+"&targetDispFld="+targetDispFld+"&targetIdFld="+targetIdFld + "&targetKeywordFld=" + targetKeywordFld;
        
        openPopWindow(paramStr);
}


function openPopWindow(paramStr)
{
    windowName=window.open(paramStr,"FieldAction","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=450 ,top=200 ,left=250");
    windowName.focus();
    
}


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="lnkformsB"scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>

<jsp:useBean id="formActionJB" scope="request"  class="com.velos.eres.web.formAction.FormActionJB"/>

<%@ page language = "java" import = "com.velos.eres.business.fieldAction.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.tags.*"%>
<%@ page language = "java" import = "com.velos.eres.web.fieldLib.FieldActionJB"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

    HttpSession tSession = request.getSession(true);
    int pageRight = 0;    
    
    FieldActionJB fldActionJB = new FieldActionJB();
//ML
	FormActionDao fa = new FormActionDao();
    
	FieldActionDao fldActionDaoInfo = new FieldActionDao();
    
    FieldLibDao fldlibDao = new FieldLibDao();
    
    FieldActionStateKeeper fsk = new FieldActionStateKeeper();     

    VFieldAction vfldAction = new VFieldAction ();
    
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
     
		// get page right
			
	 String pageRightParam = request.getParameter("pgrt");		
	 pageRight = EJBUtil.stringToNum(pageRightParam);
	 //added 05/05 
	 int  formLibId =  0 ;
	 String name = "";
     
    if (pageRight >= 4)
        
    {
        String mode="";
        mode=request.getParameter("mode");
        
        String stdId=request.getParameter("studyId");
   	 	int studyId= EJBUtil.stringToNum(stdId);			

   		String userIdFromSession = (String) tSession.getValue("userId");
   		String accId = (String) tSession.getValue("accountId");

   		
        String formId = request.getParameter("formId");
        if (!(formId.equals(""))) {
      	  formLibId = EJBUtil.stringToNum(formId);
        	  formlibB.setFormLibId(formLibId);
      	  formlibB.getFormLibDetails();
      	  name=formlibB.getFormLibName();
          }
        
        lnkformsB.findByFormId(EJBUtil.stringToNum(formId));
		String formDisplayType = lnkformsB.getLFDisplayType();
        
		formDisplayType=(formDisplayType==null)?"":formDisplayType;
		
        String fieldAction = request.getParameter("fldActionId");
        
        
        //060508ML
        String operator = "";
        String formActionId = request.getParameter("formActionId");
        if (formActionId == null) formActionId = "";
        else{
        	formActionJB.setFormActionId(EJBUtil.stringToNum(formActionId));
        	formActionJB.getFormActionDetails();
        }
        operator = formActionJB.getFormActionOperator();    
        StringBuffer acSource = new StringBuffer();
        acSource.append("<SELECT NAME= \"actionCondition\" onChange=\"document.formAction.actionCondition.value=document.formAction.actionCondition.options[selectedIndex].text;\">") ;
		if (formActionId.equals("")){
			acSource.append("<OPTION value = '' SELECTED>Select An Option</OPTION>");
			acSource.append("<OPTION value = '&lt;'>Less Than</OPTION>");
        	acSource.append("<OPTION value = '&gt;'>Greater Than</OPTION>");
        	acSource.append("<OPTION value = '&lt;=' >Less Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '&gt;=' >Greater Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '=' >Equal to</OPTION>");
        	acSource.append("<OPTION value = '!=' >Not Equal to</OPTION>");
		}
        else if (operator.equals("<")){
        	acSource.append("<OPTION SELECTED value = '&lt;' SELECTED>Less Than</OPTION>");
        	acSource.append("<OPTION value = '&gt;'>Greater Than</OPTION>");
        	acSource.append("<OPTION value = '&lt;=' >Less Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '&gt;=' >Greater Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '=' >Equal to</OPTION>");
        	acSource.append("<OPTION value = '!=' >Not Equal to</OPTION>");
        }
        else if (operator.equals (">")) {
        	acSource.append("<OPTION SELECTED value = '&gt;' SELECTED>Greater Than</OPTION>");
        	acSource.append("<OPTION value = '&lt;' >Less Than</OPTION>");
        	acSource.append("<OPTION value = '&lt;=' >Less Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '&gt;=' >Greater Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '=' >Equal to</OPTION>");
        	acSource.append("<OPTION value = '!=' >Not Equal to</OPTION>");
        }
        else if (operator.equals("=")){ 
        	acSource.append("<OPTION SELECTED value = '=' SELECTED>Equal To</OPTION>");
        	acSource.append("<OPTION value = '&gt;'>Greater Than</OPTION>");
          	acSource.append("<OPTION value = '&lt;=' >Less Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '&lt;=' >Less Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '&gt;=' >Greater Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '!=' >Not Equal to</OPTION>");
        }

        else if (operator.equals("<=")){ 
        	acSource.append("<OPTION SELECTED value = '&lt;=' SELECTED>Less Than or Equal To</OPTION>"); 
        	acSource.append("<OPTION value = '&lt;'>Less Than</OPTION>");
        	acSource.append("<OPTION value = '&gt;'>Greater Than</OPTION>");
            acSource.append("<OPTION value = '&gt;=' >Greater Than or Equal to</OPTION>");
        	acSource.append("<OPTION value = '=' >Equal to</OPTION>");
        	acSource.append("<OPTION value = '!=' >Not Equal to</OPTION>");
        }
        else if (operator.equals(">=")){ 
        	acSource.append("<OPTION SELECTED value = '&gt;=' SELECTED>Greater Than or Equal To</OPTION>"); 
        acSource.append("<OPTION value = '&lt;='>Less Than or Equal To</OPTION>"); 
    	acSource.append("<OPTION value = '&lt;'>Less Than</OPTION>");
    	acSource.append("<OPTION value = '&gt;'>Greater Than</OPTION>");
 		acSource.append("<OPTION value = '=' >Equal to</OPTION>");
    	acSource.append("<OPTION value = '!=' >Not Equal to</OPTION>");
    	}
        else if (operator.equals("!=")){
        acSource.append("<OPTION SELECTED value = '!=' SELECTED>Not Equal To</OPTION>"); 
        acSource.append("<OPTION value = '&lt;='>Less Than or Equal To</OPTION>"); 
    	acSource.append("<OPTION value = '&lt;'>Less Than</OPTION>");
    	acSource.append("<OPTION value = '&gt;'>Greater Than</OPTION>");
        acSource.append("<OPTION value = '&gt;=' >Greater Than or Equal to</OPTION>");
    	acSource.append("<OPTION value = '=' >Equal to</OPTION>");
    	}
    	acSource.append("</SELECT>");
     
        String pullDownSourceField = "";
        ArrayList sourcefieldIds = new ArrayList();
        ArrayList sourcefieldNames = new ArrayList();
        ArrayList sourcefieldSystemIds = new ArrayList();
        ArrayList sourcefieldUids = new ArrayList();
        ArrayList sourceFldDataTypes=new ArrayList();
        
        Hashtable htActionInfo = new Hashtable();
        String fldActionCondition = "";
        String fldActionType = "";
        String fldAction  = "";
        String selectedSourceFld  = "";
        int selectedSourceFldInt = 0;                
        
        String selectedVelconditionvalue = "";
        String selectedDispconditionvalue = "";
        String selectedVelconditionid = "";
        
        String selectedVeltarget = "";
        String selectedDisptargetvalue = "";
        String selectedVeltargetid = ""; 
        int fieldActionInt = 0;
        String selectedVelReadonlyOrDisable = "";
        String selectedVelSource = "";
        String selectedVelCondition = ""; 
        
        //added 05/05 remove hardcoding
		String linkedFormIdStr="";
		String linkedFormName="";
        //get source form fields
            
        fldlibDao = fldActionJB.getFieldsForFieldAction(EJBUtil.stringToNum(formId));
        
        
        /*
    	linkedFormIdStr = fieldLibJB.getFldLinkedForm();
		
		if (! StringUtil.isEmpty(linkedFormIdStr))	
		{
			formlibB.setFormLibId(EJBUtil.stringToNum(linkedFormIdStr));
			formlibB.getFormLibDetails();
			linkedFormName = formlibB.getFormLibName();
		}	
        */       
        if (EJBUtil.isEmpty(mode) )
        {    mode="N";
        }

        if (EJBUtil.isEmpty(fieldAction))
        {
            fieldAction="";
        }    
        
        fieldActionInt = EJBUtil.stringToNum(fieldAction);
            
        sourcefieldIds = fldlibDao.getFieldLibId();
        sourcefieldNames = fldlibDao.getFldName();
        sourcefieldSystemIds = fldlibDao.getFldSysID();
        sourcefieldUids =  fldlibDao.getFldUniqId();
        sourceFldDataTypes=fldlibDao.getFldDataType();
        
        String sourceFldName = "";
        String sourceFldSystemId = "";
        Integer sourceFldId = null;
        String sourcefieldUid = "";
        String sourceFldType="";
        
        int intFldId  = 0;    

        StringBuffer sbSource = new StringBuffer();
        
        ///////////////////////////////////////////
        sbSource.append("<SELECT NAME=\"sourceFldDD\" onChange=\"document.formAction.sourceFldName.value=document.formAction.sourceFldDD.options[selectedIndex].text;\">") ;

        for (int counter = 0; counter <= sourcefieldIds.size() -1 ; counter++)
        {
            sourceFldId = (Integer) sourcefieldIds.get(counter);
            intFldId =  sourceFldId.intValue() ;
            sourceFldSystemId = ( String) sourcefieldSystemIds.get(counter); 
            sourceFldName = (String) sourcefieldNames.get(counter); 
            sourcefieldUid = (String) sourcefieldUids.get(counter);
            sourceFldType=(String) sourceFldDataTypes.get(counter);
            if (!(sourceFldType.equals("EN") || sourceFldType.equals("ED"))) continue;
            	
            if (StringUtil.isEmpty(sourcefieldUid))
            {
            	sourcefieldUid = "-";
            }
            
            if (selectedSourceFld == sourceFldSystemId )
            {    

                sbSource.append("<OPTION SELECTED value = "+ sourceFldSystemId+ ">" + sourceFldName + " ["+ sourcefieldUid+"]</OPTION>");            
            }    
            else
            {    
                sbSource.append("<OPTION value = "+ sourceFldSystemId+ ">" + sourceFldName + " ["+ sourcefieldUid+"]</OPTION>");            
             }                
            
        }
            if (mode.equals("N"))
            {    
                sbSource.append("<OPTION value ='' SELECTED>Select an Option</OPTION>");            
            }    
            sbSource.append("</SELECT>");

            
                    
        
    
%>

    <Form name="formAction" method="post" action="updateFormAction.jsp" onsubmit="return validate(document.formAction)">
    
    <table width="60%" cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td ><P class="sectionHeadings"> Define Form Action </P> </td>        
     </tr>        
     <tr>
     <td><P class="defComments"><b> Form Name: </b><%=name%></P></td>
     </tr>
     </table>
     <br>
 
    
    <Input type="hidden" name="mode" value=<%=mode%>>
    <Input type="hidden" name="formId" value=<%=formId%>>
    <Input type="hidden" name="type" value=<%=formDisplayType%>>
    <Input type="hidden" name="fieldActionId" value=<%=fieldAction%>>  
    <Input type="hidden" name="actionType" value="tempGrey">  
    
    <Input type="hidden" name="dfilter" value="">
    
    <Input type="hidden" name="studyId" value=<%=studyId%>>  
	<input type=hidden name="accId" value="<%=accId%>">
	<input type=hidden name="userId" value="<%=userIdFromSession%>">
	
 	<Input type="hidden" name="formActionId" value=<%=formActionId%>>  
    
      
       
    <Input type="hidden" name="velcondition" value="<%=selectedVelCondition%>" > 
    <input type="hidden" name="linkFormId" value="1">
          
	
	
    <table width="80%" cellspacing="0" cellpadding="0" border="0"  >
     <tr> 
      <td width="20%">Select Source Field<FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=sbSource.toString()%> </td>
        <Input type="hidden" name="sourceFldName" value="">
      </tr>
       <tr> 
      <td width="20%">Select Action Condition<FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=acSource.toString()%>
  
         </td>
      </tr>  
       <tr>
       
       <tr><td width="15%">Select Target Form <FONT class="Mandatory">*</FONT></td><td width="50%"><input type="text" name="linkFormName" readonly value="<%=linkedFormName%>" size="35" >
        <A href="#" onClick="openFormsLookup(document.formAction)">Select</A>
    	 </td>
    	 <Input type="hidden" name="targetFormId" value="">
       </tr>
      
       
      </tr>  
       <tr> 
      <td width="20%">Select Target Fields <FONT class="Mandatory">* </FONT></td>
        <td width="60%"><Input type="text" name="disptargetvalue" READONLY value="<%=selectedDisptargetvalue%>">
        <Input type="hidden" name="veltargetid"  value="<%=selectedVeltargetid%>">
        <Input type="hidden" name="veltarget"  value="">
        <A href= "#" onClick="openFieldLookup(document.formAction)">Select</A>
        </td>
      </tr>  
       <tr> 
      <td width="20%">Prompt Message</td>
        <td><input type="text" name="actMessage" maxlength="200"></td>
      </tr>
           
  </table>
        <br>

	<% if (pageRight >= 6) { %>
    <table width="75%">
    <tr>
    <td width="25%">e-Signature <FONT class="Mandatory">* </FONT></td>
        <td width="25%"><input type="password" name="eSign" maxlength="8"></td> 
    <td width="25%" ><input type="image" src="../images/jpg/Submit.gif" align="right" border="0" ></td>
    </tr>
    </table>
	<% } %>
		
  </Form>
  
<%
    } //end of if body for page right
    else
    {
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

    } //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 
</body>

</html>



