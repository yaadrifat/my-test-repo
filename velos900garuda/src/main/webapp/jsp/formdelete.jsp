<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Form_Delete%><%-- Form Delete*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>
function  validate(formobj){
	 
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB ,com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="linkedFormsB" scope="session" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>

<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  
<BODY> 
<br>
<%
String from = "";
from=request.getParameter("from");

%>

<DIV class="formDefault" id="div1">
  

<%  HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{	
	
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	//int	study_not_associated_right = 7;//Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSNOSTUDY"));
	//int study_associated_right = 7;//Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMSWSTUDY"));
	int account_form_rights = 0;
	String lnkFormId= "";
	String selectedTab="";
	int studyId=0;
	String formId="";

	lnkFormId= request.getParameter("formId");
	linkedFormsB.setLinkedFormId(EJBUtil.stringToNum(lnkFormId));
	linkedFormsB.getLinkedFormDetails();	 	

	formId = linkedFormsB.getFormLibId();
	formLibB.setFormLibId(EJBUtil.stringToNum(formId));
	formLibB.getFormLibDetails();
	String formName = formLibB.getFormLibName();
	   	
		selectedTab=request.getParameter("selectedTab");
		studyId=EJBUtil.stringToNum(request.getParameter("studyId"));
		int pageRight=0;
		String userIdFromSession = (String) tSession.getValue("userId");
		
		
		int flag = 0;
		int ret=0;
		String delMode=request.getParameter("delMode");
		String lnkfrom="";
		lnkfrom = request.getParameter("lnkfrom");
		if(lnkfrom==null)
		{lnkfrom="-";}
		
		if( from.equals("A")){
	 	    
			if (lnkfrom.equals("S")) 
	 	    {     
				ArrayList tId ;
				tId = new ArrayList();
				if(studyId >0)
				{
					TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();
					if (tId.size() <=0)
					{
						pageRight  = 0;
					} else {
						StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
						 
							 	ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();
							 
						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();
								
						if ((stdRights.getFtrRights().size()) == 0){					
							 pageRight= 0;		
						} else {								
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					}
				}
				if(pageRight >= 6)
	  				{
		 				flag=1;
		 			}
		 			else
		 			{
		  				flag=0;
					 }

				  }
	 		else
	        {
					account_form_rights = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
	  				if(account_form_rights >= 6)
	  				{
		 				flag=1;
		 			}
		 			else
		 			{
		  				flag=0;
					 }

			 }
	  }
	 else
	 {
	 flag=1;
	 }
				
		
		if (delMode.equals("null")) {
		  if(flag==1){
			delMode="final";

       if (from.equals("S")) {
      %>
      <p class="successfulmsg"> <%=MC.M_MngPcolAssoc_FrmDel%><%-- Manage Protocols >> Associated Forms >> Form Delete*****--%> </p>
      <% }else {%>	
      <p class="successfulmsg"> <%=MC.M_MngAccMgmt_FrmDel%><%--Manage Account >> Form Management >> Form Delete*****--%> </p>
      <%}%>
	
	 


	<FORM name="formdelete" id="frmDelete" method="post" action="formdelete.jsp" onSubmit="if (validate(document.formdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	
 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="formId" value="<%=lnkFormId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
  	 <input type="hidden" name="from" value="<%=from%>">

<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
 	<P class="defComments"><b><%=LC.L_Frm_Name%><%-- Form Name*****--%> : <%=formName%></b></P> 
		   	   	
	<b><%=MC.M_PlsEtrEsign_Del %><%-- Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
				
				
	 <jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="frmDelete"/>
				<jsp:param name="showDiscard" value="N"/>
	 </jsp:include>	
				
	

	</FORM>
<%  }
	else
	{%>
	<jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	}
	
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
			<%
			} else {
			 String del="D";
			 String ipAdd=(String)tSession.getValue("ipAdd");
			 String userId=(String)tSession.getValue("userId");
			
			 linkedFormsB.setRecordType(del);
			 linkedFormsB.setIpAdd(ipAdd);
 			 linkedFormsB.setLastModifiedBy(userId);
			 int i=0;
    		 i= linkedFormsB.updateLinkedForm();
			 //Added by Manimaran to set recordtype 'D' in er_formlib table while deleting the form.(Bug 2563)
			 int j=0;
			 formLibB.setRecordType(del);
			 // Modified for INF-18183 ::: AGodara 
			 j=formLibB.updateFormLib(AuditUtils.createArgs(tSession,"",LC.L_Form_Mgmt));
			
			 %>
			 <% if(i==0 && j==0){%>
							
			 <p class = "successfulmsg" align = center> <%=LC.L_FrmDel_Succ %><%-- Form  Deleted successfully*****--%> </p>
			
			  <% }else{%>
  				
			   <p class = "successfulmsg" align = center> <%=LC.L_FrmNot_Del %><%-- Form  Not Deleted*****--%> </p>
			   
			  <%}%>
			  <%if(from.equals("S")){	  %>
						  <META HTTP-EQUIV=Refresh CONTENT="1; URL=studyprotocols.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">	 		  
			  			<%}
						if(from.equals("A")){%>
						  <META HTTP-EQUIV=Refresh CONTENT="1; URL=accountforms.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">
				<%		} %>  				  
			<%} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>

</body>
</HTML>


