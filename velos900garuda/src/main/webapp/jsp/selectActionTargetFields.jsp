<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Select_Flds%><%--Select Fields*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

function toRefresh(formobj)
{
		if(formobj.toRefresh.value=="yes")
		{
			window.opener.location.reload();
		}
}


 function  populateOpener(formobj)
 {
	
	totRows = formobj.totRows.value;

	var valueStr, dispStr, keywordTargetValue;
	var targetDispFld,targetIdFld,targetKeywordFld,targetFormName;
				
	dispStr = "";
	valueStr = "";
	keywordTargetValue = "";
		
	var value = "";

		if (totRows > 1) {
        	for(cnt = 0; cnt<totRows;cnt++)
        	{
    			  if (formobj.selectTarget[cnt].checked)
    			  {
    			  	  value = formobj.targetFldName[cnt].value;
   			  		  value = "[" + value + "]";  
   			  		  
    			  	  dispStr = dispStr + "," + value;
    			  	  valueStr = valueStr + "[VELSEP]" + formobj.targetfldId[cnt].value;
    			  	  keywordTargetValue = keywordTargetValue  + "[VELSEP]" + formobj.targetFldSystemId[cnt].value;
    			  	  
	    		  }
    		
			}//end for
			keywordTargetValue  = keywordTargetValue  + "[VELSEP]" ;
			valueStr = valueStr + "[VELSEP]" ;
		}	 //endif for totRows
		else if (totRows == 1)
		{
		 	 	  value = formobj.targetFldName.value;
  			  	  value = "[" + value + "]";  
   			  	
   			  	  dispStr =  "," + value;
    			  //valueStr = "[VELSEP]" + formobj.targetfldId.value;
   			  	  //keywordTargetValue = "[VELSEP]" + formobj.targetFldSystemId.value;
   			  	  	  
    			  //dispStr =   value;
    			  valueStr =  formobj.targetfldId.value;
   			  	  keywordTargetValue = formobj.targetFldSystemId.value;

   			  	  keywordTargetValue  = "[VELSEP]" + keywordTargetValue  + "[VELSEP]" ;
  	  			  valueStr = "[VELSEP]" + valueStr + "[VELSEP]" ;
   			  	  
		}	
			 if (dispStr.length > 1)
			{
				dispStr = dispStr.substring(1,dispStr.length);
			}
			
			/*if (valueStr.length > 8)
			{	
				valueStr = valueStr.substring(8,valueStr.length);
				keywordTargetValue = keywordTargetValue.substring(8,keywordTargetValue.length);
			} */
			

	targetDispFld = formobj.targetDispFld.value;
	targetIdFld = formobj.targetIdFld.value;
	targetKeywordFld = formobj.targetKeywordFld.value;
	sourceFormName = formobj.sourceFormName.value;
	
	//set values;

	 window.opener.document.forms[sourceFormName].elements[targetDispFld].value=dispStr;	
 	 window.opener.document.forms[sourceFormName].elements[targetIdFld].value=valueStr;	
 	 window.opener.document.forms[sourceFormName].elements[targetKeywordFld].value=keywordTargetValue;	
			
	self.close();		
	return false;

 }

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page language = "java" import = "com.velos.eres.web.fieldLib.FieldActionJB"%>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>
	
<DIV class="popDefault" id="div1"> 
	
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	
	
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>

<%  
		
		//the number of defualt responses
		int fldCount = 0 ; 
		String fieldLibId="";
		
		String mode=request.getParameter("mode");

		
		String targetDispFld = "";
		String targetIdFld  = "";
		String targetKeywordFld  = "";
		String sourceFormName = "";
		
		targetDispFld = request.getParameter("targetDispFld");
		targetIdFld  = request.getParameter("targetIdFld");
		targetKeywordFld  = request.getParameter("targetKeywordFld");
		sourceFormName = request.getParameter("sourceFormName");

		
		String formId = request.getParameter("formId");
		
		
		FieldLibDao fldlibDao = new FieldLibDao();
		FieldActionJB fldActionJB = new FieldActionJB();
			
		ArrayList targetfieldIds = new ArrayList();
		ArrayList targetfieldNames = new ArrayList();
		ArrayList targetfieldSystemIds = new ArrayList();
		ArrayList targetFieldUids = new ArrayList();
		
		//get target form fields
			
		fldlibDao = fldActionJB.getFieldsForFieldAction(EJBUtil.stringToNum(formId));
			
		targetfieldIds = fldlibDao.getFieldLibId();
		targetfieldNames = fldlibDao.getFldName();
		targetfieldSystemIds = fldlibDao.getFldSysID();
		targetFieldUids = fldlibDao.getFldUniqId();
		
		
		String targetFldName = "";
		String targetFldSystemId = "";
		Integer targetFldId = null;
		String targetFieldUid = "";
		
		fldCount = targetfieldIds.size();
		
		int intFldId  = 0;	
		
		%>
	    <Form name="targetfldform" id="targetfldfrm" method="post" action="" onsubmit="if (populateOpener(document.targetfldform)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		
		<Input type="hidden" name="mode" value=<%=mode%> >
		<Input type="hidden" name="formId" value=<%=formId%> >

		<Input type="hidden" name="targetDispFld" value=<%=targetDispFld%> >
		<Input type="hidden" name="targetIdFld" value=<%=targetIdFld%> >
		<Input type="hidden" name="targetKeywordFld" value=<%=targetKeywordFld%> >
		<Input type="hidden" name="sourceFormName" value=<%=sourceFormName%> >
		
		<script>
		//get the selected condition values values 
		var targetfld, targetfldVal, parentForm;

		//declare an associative array
		var arSelectedtargetVal = new Object();
				
		//get the value of this field from parent popup
		parentForm = window.document.targetfldform.sourceFormName.value;
		
		targetfld = window.document.targetfldform.targetIdFld.value;
		targetfldVal = window.opener.document.forms[parentForm].elements[targetfld].value ;
				
		//replace the [velcomma keyword with a comma]
		targetfldVal = replaceSubstring(targetfldVal,"[VELSEP]",",")
		
		if (targetfldVal.length > 0)
		{
			targetfldVal = targetfldVal.substring(1,targetfldVal.length);
			targetfldVal = targetfldVal.substring(0,targetfldVal.length - 1);
		}
		// split
				
		var idarray = targetfldVal.split(",");
		var part_num=0;
		
		while (part_num < idarray.length)
		 {
		   arSelectedtargetVal[idarray[part_num]] = idarray[part_num];
		   part_num+=1;
		 }
		 				
	</script>
				
	    <Input type="hidden" name="totRows" value=<%=fldCount%> >			
  	
		<table width="99%" cellspacing="1" cellpadding="0" border="0">
		  	<tr>
				<P class="sectionHeadings"><%=LC.L_Select_Flds%><%--Select Fields*****--%></P>
			 </tr>   
		 </table>
		 
	  <table width="100%" cellspacing="1" cellpadding="0" border="0" class="basetbl midAlign">
	  <tr>
		<th width="10%"><%=LC.L_Select%><%--Select*****--%></FONT></th>
		<th width="50%"><%=LC.L_Fields%><%--Fields*****--%></th>
	  </tr>   
		<%
		
		for (int counter = 0; counter < fldCount ; counter++)
		{
			targetFldId = (Integer) targetfieldIds.get(counter);
			intFldId =  targetFldId.intValue() ;
			targetFldSystemId = ( String) targetfieldSystemIds.get(counter); 
			targetFldName = (String) targetfieldNames.get(counter); 
			targetFieldUid = (String) targetFieldUids.get(counter);
			
			if (StringUtil.isEmpty(targetFieldUid))
			{
				targetFieldUid = "-";
			}
		%>	
		  <tr> 
	     	<td>
	     		<Input type="checkbox" name = "selectTarget" value = <%=intFldId%>>
	     	</td>
	        <td> 
			 <input name="targetfldId" type="hidden" value="<%=intFldId%>" >
			 <input name="targetFldName" type="hidden" value="<%=targetFldName%>" >
			 <input name="targetFldSystemId" type="hidden" value="<%=targetFldSystemId%>" >
	       	<%=targetFldName%>&nbsp;[<%=targetFieldUid%>] 
	        </td>
		  </tr>
			
		<%
		
		}
	%>
</table>

   <br>
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="targetfldfrm"/>
		<jsp:param name="showDiscard" value="N"/>
 </jsp:include>
 
 <script>
	//  loop through the responses check boxes and select the ones in the associative array
	formobj = window.document.targetfldform;
	totRows = formobj.totRows.value;
	
	var targetVal;
		if (totRows > 1) 
		{
			for(cnt = 0; cnt<totRows;cnt++)
			{
				 targetVal = formobj.selectTarget[cnt].value;
				 
				 // check for this conditionVal in the associative array
				 if (typeof arSelectedtargetVal[targetVal] != "undefined")
				 {
					formobj.selectTarget[cnt].checked = true;
				 }
						 
			}	 //
								
		}	 //endif for totRows
		else if (totRows == 1)
		{
		 	 targetVal = formobj.selectTarget.value;
			 // check for this conditionVal in the associative array
			 if (typeof arSelectedtargetVal[targetVal] != "undefined")
			 {
			 	formobj.selectTarget.checked = true;
			 }
	 
			 
  		}	
				
	</script>
	
				 
  </Form>
  

<%				
		
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>
</html>
