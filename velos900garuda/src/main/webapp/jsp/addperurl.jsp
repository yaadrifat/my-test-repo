<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Pat_PatUrl%><%--Patient>> Patient URL*****--%></title>

<%@ page import="java.util.*,com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:useBean id ="sessionmaint" scope="page" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="perApndxB" scope="page" class="com.velos.eres.web.perApndx.PerApndxJB"/>

<SCRIPT Language="javascript">
 function  validate(formobj){
	 //KM-#4073   // KV:Fixed BugNo:4955 
     if (!(validate_date(formobj.perApndxDate))) return false
	 if (!(validate_col('URL',formobj.perApndxUri))) return false
	 if (!(validate_col('Date',formobj.perApndxDate))) return false
     if (!(validate_col('Description',formobj.perApndxDesc))) return false
     if (!(validate_col('eSign',formobj.eSign))) return false

	
	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>
 

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<body>
<br>
<DIV class="formDefault" id="div1"> 
<%
	String mode = request.getParameter("mode");
	String perApndxId=request.getParameter("perApndxId");	   
	String patId = request.getParameter("patId");
	String selectedTab = request.getParameter("selectedTab");
	
	String studyId = null;
 	String statDesc = null;
	String statid = null;
 	String studyVer = null;
 	studyId = request.getParameter("studyId"); 		
	statDesc = request.getParameter("statDesc");
 	statid = request.getParameter("statid");
	studyVer = request.getParameter("studyVer");
	studyId = request.getParameter("studyId");
			
	String perApndxDate= "";
	String perApndxDesc= "";
	String perApndxUri = "";

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
%>
<BR>

<%
	if (mode.equals("M")) {
		perApndxB.setPerApndxId(EJBUtil.stringToNum(perApndxId));
		perApndxB.getPerApndxDetails();
		perApndxDate = perApndxB.getPerApndxDate();
		perApndxDesc = perApndxB.getPerApndxDesc();
		perApndxUri = perApndxB.getPerApndxUri();
	}
%>
<form name="addurl" id="addperurlfrm" METHOD=POST action="perurlsave.jsp" onsubmit="if (validate(document.addurl)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="perApndxId" value=<%=perApndxId%>>
<input type=hidden name="patId" value=<%=patId%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="statDesc" value=<%=statDesc%>>
<input type=hidden name="statid" value=<%=statid%>>
<input type=hidden name="studyVer" value=<%=studyVer%>>
<input type=hidden name="studyId" value=<%=studyId%>>

<TABLE width="98%" class="basetbl">
      <tr> 
        <td colspan="2" align="center"> 
          <P class = "defComments"> <b><%=MC.M_LnkYour_PatApdx%><%--Add Links to your Patient Appendix*****--%> </b></P>
        </td>
      </tr>
      <tr> 
        <td width="20%" align="right">  <%=LC.L_Date%><%--Date*****--%> <FONT class="Mandatory">* </FONT> </td>
<%-- INF-20084 Datepicker-- AGodara --%>
        <td width="65%"><input type=text name="perApndxDate" class="datefield" value='<%=perApndxDate%>' MAXLENGTH=20 size=10></td>	
      </tr>	
      <tr> 
        <td width="20%" align="right"> <%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=text name="perApndxUri" value='<%=perApndxUri%>' MAXLENGTH=255 size=50>
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_EtrCpltUrl_255Max%><%--Enter complete url eg. 'http://www.centerwatch.com' 
            or 'ftp://ftp.centerwatch.com' (255 char max.)*****--%> </P>
        </td>
      </tr>
      <tr> 
        <td width="20%" align="right"> <%=LC.L_Short_Desc%><%--Short Description*****--%><FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%"> 
          <input type=text name="perApndxDesc" MAXLENGTH=100 value='<%=perApndxDesc%>' size=40>
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_NameToLnk_100CharMax%><%--Give a friendly name to your link. (100 char max.)*****--%> </P>
        </td>
      </tr>
  </table>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="addperurlfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>



