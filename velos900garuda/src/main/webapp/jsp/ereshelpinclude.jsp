<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC"%>
<div class=Section1>

	<p class=MsoNormal style='mso-layout-grid-align:none;text-autospace:none'>
		<b><span style='font-family:Arial'>
				MENU
				<o:p></o:p>
			</span></b>
	</p>

	<p class=MsoNormal style='mso-layout-grid-align:none;text-autospace:none'>
		<b><span style='font-family:Arial'>
			<![if !supportEmptyParas]>&nbsp;
			<![endif]>
			<o:p></o:p>
		</span></b>
	</p>

<p class=MsoToc1 style='tab-stops:right 431.5pt'>

	<!--[if supportFields]><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;mso-bidi-font-family:Arial;
	font-weight:normal'><span style='mso-element:field-begin'></span><span
	style="mso-spacerun: yes"> </span>TOC \o &quot;1-3&quot; \n \h \z <span
	style='mso-element:field-separator'></span></span><![endif]-->
		
	<p class=MsoToc1 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'>
				<a href="#_Toc56934977">
					<span style='mso-bidi-font-size:22.0pt;mso-bidi-font-family:Arial'>Account Management</span>
				</a>
			</span>
		</span>
		<span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
				<a href="#_Toc56934978">
					<span style='mso-bidi-font-family:Arial;mso-bidi-font-style:italic'>Managing Organizations</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934979">
					<span style='mso-bidi-font-weight:bold'>Adding A New Organization To Your Account</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>
	
	<p class=MsoToc2 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
				<a href="#_Toc56934980">
					<span style='mso-bidi-font-family:Arial;mso-bidi-font-style:italic'>Managing Groups</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>
	
	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934981">
					<span style='mso-bidi-font-weight:bold'>Adding A New Group To Your Account</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>
	
	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934982">
					<span style='mso-bidi-font-weight:bold'>Assigning Rights To The Group</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934983">
					<span style='mso-bidi-font-weight:bold'>Assigning A User To A Group</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>
  
	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934984">
					<span style='mso-bidi-font-weight:bold'>Assigning User To Another Group</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>
	
	<p class=MsoToc2 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
				<a href="#_Toc56934985">
					<span style='mso-bidi-font-family:Arial;mso-bidi-font-style:italic'>Managing Users</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>
	
	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934986">
					<span style='mso-bidi-font-weight:bold'>Adding A New User To Your Account</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>
	
	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<a href="#_Toc56934987">
				<span style='mso-bidi-font-weight:bold;mso-bidi-font-style:italic'>Reset Password</span>
			</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934988">
					<span style='mso-bidi-font-weight:bold;mso-bidi-font-style:italic'>Reset User Session</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934989">
					<span style='mso-bidi-font-weight:bold;mso-bidi-font-style:italic'>Blocked/Deactivated Users</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934990">
					<span style='mso-bidi-font-weight:bold;mso-bidi-font-style:italic'>Multiple Organization Access</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
				<a href="#_Toc56934991">
					<span style='mso-bidi-font-family:Arial;mso-bidi-font-style:italic'>Managing Account Links</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"; font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
				<a href="#_Toc56934992">
					<span style='mso-bidi-font-weight:bold;mso-bidi-font-style:italic'>Adding A New Link To Account Links</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'>
				<a href="#_Toc56934993">
					<span style='mso-bidi-font-size:22.0pt;mso-bidi-font-family:Arial'>Account Personalization</span>
				</a>
			</span>
		</span>
		<span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
				<a href="#_Toc56934994">
					<span style='mso-bidi-font-family:Arial;mso-bidi-font-style:italic'>Editing Your Profile</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'>
		<span class=MsoHyperlink>
			<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
				<a href="#_Toc56934995">
					<span style='mso-bidi-font-family:Arial;mso-bidi-font-style:italic'>Changing Your Password</span>
				</a>
			</span>
		</span>
		<span style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'>
			<o:p></o:p>
		</span>
	</p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56934996"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Changing Your e-Signature</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56934997"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Managing My Links</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56934998"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Signing Up For The Notification Service</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56934999"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Customizing Your Automatic Logout Time</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935000"><span style='mso-bidi-font-size:22.0pt'>Message Center</span></a></span></span><span
	style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935001"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Searching a Broadcast Study</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935002"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Sending A Request For More Information For A
	Broadcast Study</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935003"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Viewing A Request For More Information For A
	Broadcast Study</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935004"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Viewing Profile Of Message Sender</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935005"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Granting Access To A Study Protocol</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935006"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Taking And Updating A Snapshot</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935007"><span style='mso-bidi-font-size:22.0pt'>Dynamic Forms</span></a></span></span><span
	style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935008"><span style='mso-bidi-font-style:
	italic'>Creating/Editing a Field in the Field Library</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935009"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Field Library</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935010"><span style='mso-bidi-font-style:
	italic'>Designing a New Form</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:
	normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935011"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Form Library</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935012"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Define the Form</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935013"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Add Fields</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935014"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Form Settings</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935015"><span style='mso-bidi-font-style:
	italic'>Linking a Form to a Study</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935016"><span style='mso-bidi-font-style:
	italic'>Linking a Form at the Account Level</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935017"><span style='mso-bidi-font-size:22.0pt'>Protocol
	Management</span></a></span></span><span style='font-size:10.0pt;mso-bidi-font-size:
	12.0pt;text-transform:none;font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935018"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Documenting A New Study Protocol</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935019"><span style='mso-bidi-font-weight:bold'>Study Summary</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935020"><span style='mso-bidi-font-weight:bold'>Broadcast Protocol
	Summary for Public Viewing</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935021"><span style='mso-bidi-font-weight:bold'>Sections</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935022"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Appendix</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935023"><span style='mso-bidi-font-weight:bold'>Calendar</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935024"><span style='mso-bidi-font-weight:bold'>Study Status</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935025"><span style='mso-bidi-font-weight:bold'>Study Team</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935026"><span style='mso-bidi-font-weight:bold'>New Trial
	Notifications</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935027"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Sharing A Study Protocol</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935028"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Accessing An Existing Protocol</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935029"><span style='mso-bidi-font-size:22.0pt'>Protocol Calendar</span></a></span></span><span
	style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935030"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Associating A Calendar To A Study</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935031"><span style='mso-bidi-font-weight:bold'>Selecting A
	Calendar</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935032"><span style='mso-bidi-font-weight:bold'>Customizing The
	Calendar For A Study</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935033"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Copying An Existing Calendar</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935034"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Creating A New Calendar</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935035"><span style='mso-bidi-font-weight:bold'>Step 1: Define the
	Calendar</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935036"><span style='mso-bidi-font-weight:bold'>Step 2: Select
	Events</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935037"><span style='mso-bidi-font-weight:bold'>Step 3: Schedule
	Events</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935038"><span style='mso-bidi-font-weight:bold'>Step 4: Customize
	Event Details</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935039"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Protocol Calendar Library</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935040"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Event Library</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935041"><span style='mso-bidi-font-weight:bold'>Creating A New
	Category</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935042"><span style='mso-bidi-font-weight:bold'>Creating A New
	Event</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935043"><span style='mso-bidi-font-weight:bold'>Customizing Event
	Details</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935044"><span style='mso-bidi-font-size:22.0pt'><%=LC.Pat_Patient%> Management</span></a></span></span><span
	style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935045"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Registering A <%=LC.Pat_Patient%></span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935046"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Enrolling A <%=LC.Pat_Patient%> To A Study</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935047"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'><%=LC.Pat_Patient%> Schedule</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935048"><span style='mso-bidi-font-weight:bold'>Generating A
	<%=LC.Pat_Patient%>s Schedule</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935049"><span style='mso-bidi-font-weight:bold'>Viewing A
	<%=LC.Pat_Patient%>s Schedule</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935050"><span style='mso-bidi-font-weight:bold'>Managing A <%=LC.Pat_Patient%>s
	Schedule</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935051"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Appendix</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935052"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'><%=LC.Pat_Patient%> Reports</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935053"><span style='mso-bidi-font-size:22.0pt'>Reports</span></a></span></span><span
	style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935054"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Viewing A Report</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935055"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Formatting A Report</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935056"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Printing A Report</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935057"><span style='mso-bidi-font-size:22.0pt'><%=LC.Pat_Patient%> Tracking
	Module</span></a></span></span><span style='font-size:10.0pt;mso-bidi-font-size:
	12.0pt;text-transform:none;font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935058"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Tracking Events</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935059"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Tracking Adverse Events (AE)</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935060"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Tracking CRFs</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935061"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'><%=LC.Pat_Patient%> Schedule Related Alerts And
	Notifications</span></a></span></span><span style='mso-ansi-font-size:10.0pt;
	font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935062">Notification Options</a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935063"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Global Study Alerts / Notifications</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935064"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Receiving Notifications For &quot;Past
	Scheduled Date&quot; Events</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";font-weight:
	normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935065"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Receiving Adverse Events Notifications</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935066"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Receiving Event Status Notifications</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935067"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Receiving CRF Status Notifications</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935068"><span style='mso-bidi-font-size:22.0pt'>Budget</span></a></span></span><span
	style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935069"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Creating A New Budget (<%=LC.Pat_Patient%> Or Study)</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935070"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Copying a Budget</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935071"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Appendix</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935072"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Access Rights</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935073"><span style='mso-bidi-font-size:22.0pt'>Milestones</span></a></span></span><span
	style='font-size:10.0pt;mso-bidi-font-size:12.0pt;text-transform:none;
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935074"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Creating New Milestones</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935075"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'><%=LC.Pat_Patient%> Status Milestones</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935076"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Visit Milestones</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc3 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><a
	href="#_Toc56935077"><span style='mso-bidi-font-weight:bold;mso-bidi-font-style:
	italic'>Event Milestones</span></a></span></span><span style='mso-ansi-font-size:
	10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935078"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Milestone View (Summary Report)</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935079"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Notifications</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935080"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Appendix</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935081"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Payments Received</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935082"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Reports</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc2 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><a href="#_Toc56935083"><span style='mso-bidi-font-family:
	Arial;mso-bidi-font-style:italic'>Saved Reports</span></a></span></span><span
	style='mso-ansi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
	font-weight:normal'><o:p></o:p></span></p>

	<p class=MsoToc1 style='tab-stops:right 431.5pt'><span class=MsoHyperlink><span
	style='font-size:10.0pt;mso-bidi-font-size:14.0pt;font-weight:normal'><a
	href="#_Toc56935084"><span style='mso-bidi-font-size:22.0pt'>Data Safety
	Monitoring</span></a></span></span><span style='font-size:10.0pt;mso-bidi-font-size:
	12.0pt;text-transform:none;font-weight:normal'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-layout-grid-align:none;text-autospace:none'><!--[if supportFields]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-weight:
bold'><span style='mso-element:field-end'></span></span><![endif]--><b><span
style='font-family:Arial'><br clear=all style='mso-special-character:line-break;
page-break-before:always'>
</span></b><a name="_Toc429280684"></a><a name="_Toc429281444"></a><a
name="_Toc429281510"></a><a name="_Toc429288048"></a><a name="_Toc429289286"></a><a
name="_Toc429289340"></a><a name="_Toc429303209"></a><a name="_Toc429372287"></a><a
name="_Toc429388168"></a><a name="_Toc429389091"></a><a name="_Toc429469939"></a><a
name="_Toc429475950"></a><a name="_Toc429476243"></a><a name="_Toc429553624"></a><a
name="_Toc429553800"></a><a name="_Toc429553996"></a><a name="_Toc429557014"></a><a
name="_Toc429803854"></a><a name="_Toc429908873"></a><a name="_Toc430066212"></a><a
name="_Toc430066414"></a><a name="_Toc430152738"></a><a name="_Toc430168766"></a><a
name="_Toc430594180"></a><a name="_Toc430594389"></a><a name="_Toc430685867"></a><a
name="_Toc430687804"></a><a name="_Toc431024502"></a><a name="_Toc431032062"></a><a
name="_Toc432227037"></a><a name="_Toc461189481"></a><a name="_Toc520185010"><span
style='mso-bookmark:_Toc461189481'><span style='mso-bookmark:_Toc432227037'><span
style='mso-bookmark:_Toc431032062'><span style='mso-bookmark:_Toc431024502'><span
style='mso-bookmark:_Toc430687804'><span style='mso-bookmark:_Toc430685867'><span
style='mso-bookmark:_Toc430594389'><span style='mso-bookmark:_Toc430594180'><span
style='mso-bookmark:_Toc430168766'><span style='mso-bookmark:_Toc430152738'><span
style='mso-bookmark:_Toc430066414'><span style='mso-bookmark:_Toc430066212'><span
style='mso-bookmark:_Toc429908873'><span style='mso-bookmark:_Toc429803854'><span
style='mso-bookmark:_Toc429557014'><span style='mso-bookmark:_Toc429553996'><span
style='mso-bookmark:_Toc429553800'><span style='mso-bookmark:_Toc429553624'><span
style='mso-bookmark:_Toc429476243'><span style='mso-bookmark:_Toc429475950'><span
style='mso-bookmark:_Toc429469939'><span style='mso-bookmark:_Toc429389091'><span
style='mso-bookmark:_Toc429388168'><span style='mso-bookmark:_Toc429372287'><span
style='mso-bookmark:_Toc429303209'><span style='mso-bookmark:_Toc429289340'><span
style='mso-bookmark:_Toc429289286'><span style='mso-bookmark:_Toc429288048'><span
style='mso-bookmark:_Toc429281510'><span style='mso-bookmark:_Toc429281444'><span
style='mso-bookmark:_Toc429280684'><b><span style='font-size:10.0pt;font-family:
Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></b></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></a></p>

<span style='mso-bookmark:_Toc429280684'></span><span style='mso-bookmark:_Toc429281444'></span><span
style='mso-bookmark:_Toc429281510'></span><span style='mso-bookmark:_Toc429288048'></span><span
style='mso-bookmark:_Toc429289286'></span><span style='mso-bookmark:_Toc429289340'></span><span
style='mso-bookmark:_Toc429303209'></span><span style='mso-bookmark:_Toc429372287'></span><span
style='mso-bookmark:_Toc429388168'></span><span style='mso-bookmark:_Toc429389091'></span><span
style='mso-bookmark:_Toc429469939'></span><span style='mso-bookmark:_Toc429475950'></span><span
style='mso-bookmark:_Toc429476243'></span>

<p class=MsoNormal style='text-indent:.5in'><span style='mso-bookmark:_Toc520185010'><span
style='mso-bookmark:_Toc461189481'><span style='mso-bookmark:_Toc432227037'><span
style='mso-bookmark:_Toc431032062'><span style='mso-bookmark:_Toc431024502'><span
style='mso-bookmark:_Toc430687804'><span style='mso-bookmark:_Toc430685867'><span
style='mso-bookmark:_Toc430594389'><span style='mso-bookmark:_Toc430594180'><span
style='mso-bookmark:_Toc430168766'><span style='mso-bookmark:_Toc430152738'><span
style='mso-bookmark:_Toc430066414'><span style='mso-bookmark:_Toc430066212'><span
style='mso-bookmark:_Toc429908873'><span style='mso-bookmark:_Toc429803854'><span
style='mso-bookmark:_Toc429557014'><span style='mso-bookmark:_Toc429553996'><span
style='mso-bookmark:_Toc429553800'><span style='mso-bookmark:_Toc429553624'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>

<h1><span style='mso-bookmark:_Toc520185010'><span style='mso-bookmark:_Toc461189481'><span
style='mso-bookmark:_Toc432227037'><span style='mso-bookmark:_Toc431032062'><span
style='mso-bookmark:_Toc431024502'><span style='mso-bookmark:_Toc430687804'><span
style='mso-bookmark:_Toc430685867'><span style='mso-bookmark:_Toc430594389'><span
style='mso-bookmark:_Toc430594180'><span style='mso-bookmark:_Toc430168766'><span
style='mso-bookmark:_Toc430152738'><span style='mso-bookmark:_Toc430066414'><span
style='mso-bookmark:_Toc430066212'><span style='mso-bookmark:_Toc429908873'><span
style='mso-bookmark:_Toc429803854'><span style='mso-bookmark:_Toc429557014'><span
style='mso-bookmark:_Toc429553996'><span style='mso-bookmark:_Toc429553800'><span
style='mso-bookmark:_Toc429553624'><a name="_Toc55184745"></a><a
name="_Toc56933232"></a><a name="_Toc56934869"></a><a name="_Toc56934977"><span
style='mso-bookmark:_Toc56934869'><span style='mso-bookmark:_Toc56933232'><span
style='mso-bookmark:_Toc55184745'><span style='font-size:18.0pt;mso-bidi-font-size:
10.0pt'>Account Management</span></span></span></span></a></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span><span
style='mso-bookmark:_Toc461189481'><span style='font-size:18.0pt;mso-bidi-font-size:
10.0pt'> </span></span><span style='font-size:18.0pt;mso-bidi-font-size:10.0pt'><o:p></o:p></span></h1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section provides step-by-step instructions on the Account Management
capabilities available in Velos eResearch.<span style="mso-spacerun: yes"> 
</span>Several important functionalities explained in this section include
maintaining a library of organizations, groups and Users for your account.<o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184746"></a><a name="_Toc56933233"></a><a name="_Toc56934870"></a><a
name="_Toc56934978"><span style='mso-bookmark:_Toc56934870'><span
style='mso-bookmark:_Toc56933233'><span style='mso-bookmark:_Toc55184746'><b><i><span
style='font-family:Arial;color:#993366'>Managing Organizations</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<h3><a name="_Toc524755578"></a><a name="_Toc55184747"></a><a
name="_Toc56933234"></a><a name="_Toc56934871"></a><a name="_Toc56934979"><span
style='mso-bookmark:_Toc56934871'><span style='mso-bookmark:_Toc56933234'><span
style='mso-bookmark:_Toc55184747'><span style='mso-bookmark:_Toc524755578'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Adding A New
Organization To Your Account</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Each
account can be customized to suit its specific needs. To create a list of
organizations in your specific account:<o:p></o:p></span></p>

<p class=MsoFooter style='text-align:justify;line-height:150%;tab-stops:.5in'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l55 level1 lfo40;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from your
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Organizations</span></b><b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> </span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>in the<b> </b>sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l55 level1 lfo40;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of organizations that have already been added to your
account will be displayed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l55 level1 lfo40;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Add a New Organization</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l55 level1 lfo40;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter the organization details and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l97 level1 lfo195;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The new organization will be added to your list and will be
available for selection in dropdowns and checklists.</span></p>

<p class=MsoNormal><i><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184748"></a><a name="_Toc56933235"></a><a name="_Toc56934872"></a><a
name="_Toc56934980"><span style='mso-bookmark:_Toc56934872'><span
style='mso-bookmark:_Toc56933235'><span style='mso-bookmark:_Toc55184748'><b><i><span
style='font-family:Arial;color:#993366'>Managing Groups</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<h3><a name="_Toc524755580"></a><a name="_Toc55184749"></a><a
name="_Toc56933236"></a><a name="_Toc56934873"></a><a name="_Toc56934981"><span
style='mso-bookmark:_Toc56934873'><span style='mso-bookmark:_Toc56933236'><span
style='mso-bookmark:_Toc55184749'><span style='mso-bookmark:_Toc524755580'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Adding A New
Group To Your Account</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Creation of
Groups helps in organizing your Account Users and assigning rights to them. To
add a new Group to your account:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l49 level1 lfo41;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from your
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Groups</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in the<b>
</b>sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l49 level1 lfo41;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of Groups that have already been added to your
account will be displayed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l49 level1 lfo41;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Add a New Group</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l49 level1 lfo41;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter a Group name and a brief description and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l49 level1 lfo41;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The new Group will be added to your list and will be
available for selection in dropdowns and checklists.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc524755581"></a><a name="_Toc55184750"></a><a
name="_Toc56933237"></a><a name="_Toc56934874"></a><a name="_Toc56934982"><span
style='mso-bookmark:_Toc56934874'><span style='mso-bookmark:_Toc56933237'><span
style='mso-bookmark:_Toc55184750'><span style='mso-bookmark:_Toc524755581'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Assigning
Rights To The Group</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Specific
rights can be assigned to each Group.<span style="mso-spacerun: yes"> 
</span>This helps to categorize Users by Groups and simplifies the process of
assigning rights to individual Users.<span style="mso-spacerun: yes"> 
</span>Any User assigned to a Group will automatically assume the rights that
have been assigned to the Group.<span style="mso-spacerun: yes">  </span>To
assign rights to a Group:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l117 level1 lfo42;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from your
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Groups</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in the<b>
</b>sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l117 level1 lfo42;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of Groups that have already been added to your
account will be displayed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l117 level1 lfo42;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Assign Rights</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
corresponding to the appropriate Group name.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l117 level1 lfo42;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Group rights are categorized into three categories 
Administrative Rights, Application Rights and Forms Rights.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l117 level1 lfo42;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Assign Rights to Group page, New, Edit and View
rights checkboxes are displayed on the screen under the Administrative Rights,
Application Rights and Forms Rights.<span style="mso-spacerun: yes"> 
</span>Select the rights that you wish to assign to the Group for the various
features listed. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l120 level1 lfo194;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> and in
just 3 steps you have assigned rights to a Group.</span></p>

<p class=MsoNormal style='margin-left:.05in;text-align:justify;text-indent:
.5in;line-height:150%'><span style="mso-spacerun: yes">  </span><span
style='text-transform:uppercase'><o:p></o:p></span></p>

<h3><a name="_Toc524755582"></a><a name="_Toc55184751"></a><a
name="_Toc56933238"></a><a name="_Toc56934875"></a><a name="_Toc56934983"><span
style='mso-bookmark:_Toc56934875'><span style='mso-bookmark:_Toc56933238'><span
style='mso-bookmark:_Toc55184751'><span style='mso-bookmark:_Toc524755582'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Assigning A
User To A Group</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='margin-right:-13.5pt;text-align:justify;
line-height:150%'>Once specific rights have been given to a Group, the process
of assigning rights to individual Users is highly simplified.<span
style="mso-spacerun: yes">  </span>Linking Users to a Group automatically gives
them all the rights specified for that Group.<span style="mso-spacerun: yes"> 
</span>To assign a User to a Group, follow these steps:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l17 level1 lfo43;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Clicking on the </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Groups</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> sub-menu from
the left-hand navigation bar will display a list of groups that have already
been added to your account.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l17 level1 lfo43;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Find the Group that you wish to assign a User to.<span
style="mso-spacerun: yes">  </span>Click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Group Users</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the Group name.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l17 level1 lfo43;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>This will bring up a list of Users who are currently
assigned to this Group.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l17 level1 lfo43;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new User to this Group, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Assign
Users to this Group</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l91 level1 lfo193;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the User and click on </span><span style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:
Arial'>Submit</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><span style="mso-spacerun: yes">   </span>This User has now
been added to the Group and will automatically receive all rights that have
been assigned to the Group.</span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc524755583"></a><a name="_Toc55184752"></a><a
name="_Toc56933239"></a><a name="_Toc56934876"></a><a name="_Toc56934984"><span
style='mso-bookmark:_Toc56934876'><span style='mso-bookmark:_Toc56933239'><span
style='mso-bookmark:_Toc55184752'><span style='mso-bookmark:_Toc524755583'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Assigning
User To Another Group</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Users may be
assigned to multiple groups. To assign a User to a new Group, follow these
steps:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l34 level1 lfo44;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Clicking on the </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Groups</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> sub-menu
from the left-hand navigation bar will display a list of Groups that have
already been added to your account.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l34 level1 lfo44;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Find the Group to which you wish to assign a User.<span
style="mso-spacerun: yes">  </span>Click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Group Users</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the Group name.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l34 level1 lfo44;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>This will bring up a list of Users who are currently
assigned to this Group. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l34 level1 lfo44;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Assign Users to this Group</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link to
add a new User to this Group.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l34 level1 lfo44;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the User and click on </span><span style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:
Arial'>Submit</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><span style="mso-spacerun: yes">   </span><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:40.5pt;text-align:justify;line-height:
150%'><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
User has now been added to the Group and automatically receives all the rights
that have been assigned to the Group.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l108 level1 lfo192;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To bring up the User Profile page, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>User Name</span></u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> </span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>link.<br>
The Profile page provides details about the Users account information. The
Default Group field will show a selection of all Groups that the User is
assigned. Whichever group is selected as the Default Group will be the Group
that the User may log in and the respective rights will be applied.</span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h2>

<h2><a name="_Toc55184753"></a><a name="_Toc56933240"></a><a name="_Toc56934877"></a><a
name="_Toc56934985"><span style='mso-bookmark:_Toc56934877'><span
style='mso-bookmark:_Toc56933240'><span style='mso-bookmark:_Toc55184753'><b><i><span
style='font-family:Arial;color:#993366'>Managing Users</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<h3><a name="_Toc524755585"></a><a name="_Toc55184754"></a><a
name="_Toc56933241"></a><a name="_Toc56934878"></a><a name="_Toc56934986"><span
style='mso-bookmark:_Toc56934878'><span style='mso-bookmark:_Toc56933241'><span
style='mso-bookmark:_Toc55184754'><span style='mso-bookmark:_Toc524755585'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Adding A New
User To Your Account</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'>To add Users to your account, you must have
a Group Account. Individual accounts cannot add other Users to their account.
To add a new User to your account:<o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l124 level1 lfo45;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from your
left-hand menu bar.<span style="mso-spacerun: yes">  </span>Click on </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Users
</span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:
Arial'>in the<b> </b>sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l124 level1 lfo45;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of Users belonging to your account will be displayed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l124 level1 lfo45;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new User to this list, click on the<span
style="mso-spacerun: yes">  </span></span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Add a New User</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
style="mso-spacerun: yes">  </span>link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l124 level1 lfo45;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter User details and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:40.5pt;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l185 level1 lfo191;tab-stops:list 40.5pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The new User is now a member of your account and will be
available for selection in dropdowns and checklists.</span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc55184755"></a><a name="_Toc56933242"></a><a name="_Toc56934879"></a><a
name="_Toc56934987"><span style='mso-bookmark:_Toc56934879'><span
style='mso-bookmark:_Toc56933242'><span style='mso-bookmark:_Toc55184755'><b><i><span
style='font-size:10.0pt'>Reset Password</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l191 level1 lfo120;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If a User forgets his/her password and/or
     e-Signature or if, for security reasons, the password and/or e-Signature
     need to be changed, go to the Users browser and click on the </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Reset</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     next to the User whose password/e-Signature needs to be reset.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l191 level1 lfo120;
     tab-stops:list .5in'><u><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Reset</span></u><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> link only appears in the
     Users browser if the logged in User belongs to the Admin group.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l166 level1 lfo190;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on the </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Reset</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     and in the page that comes up select the appropriate check box for
     resetting Password or e-Signature or both and click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
     mso-bidi-font-family:Arial'>Submit</span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l166 level1 lfo190;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>On saving, an email will be sent to the User
     containing information about the new Password and e-Signature.</span></li>
</ul>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc55184756"></a><a name="_Toc56933243"></a><a name="_Toc56934880"></a><a
name="_Toc56934988"><span style='mso-bookmark:_Toc56934880'><span
style='mso-bookmark:_Toc56933243'><span style='mso-bookmark:_Toc55184756'><b><i><span
style='font-size:10.0pt'>Reset User Session</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l191 level1 lfo120;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If a User does not click on the </span><b><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     color:#CC99FF'>Logout</span></b><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'> menu bar link and just closes the browser or
     shuts down the machine, the Users session remains active until his/her
     automatic log-off time has elapsed. If this User tries to login again
     during this time, successful entry will not be allowed into the system.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l191 level1 lfo120;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If the User needs to login immediately, Admin
     group Users have the ability to reset the Users session in order to allow
     access.<o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='margin-left:.75in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l30 level1 lfo151;tab-stops:list .75in'><![if !supportLists]><span
style='mso-bidi-font-family:Arial'>1. <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The Admin group User should login and select </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Manage
Account</span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> from the left-hand menu bar.<span style="mso-spacerun:
yes">  </span>Click on </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Users </span></b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>in the<b><span
style='color:#CC99FF'> </span></b></span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.75in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l30 level1 lfo151;tab-stops:list .75in'><![if !supportLists]>2.
<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>To reset
the Users session: Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Reset</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
under the column 'Reset User Session' and in the page that comes up, enter
e-Signature and click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc55184757"></a><a name="_Toc56933244"></a><a name="_Toc56934881"></a><a
name="_Toc56934989"><span style='mso-bookmark:_Toc56934881'><span
style='mso-bookmark:_Toc56933244'><span style='mso-bookmark:_Toc55184757'><b><i><span
style='font-size:10.0pt'>Blocked/Deactivated Users</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l113 level1 lfo121;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To access a list of Users who have been blocked
     (blocked Users are those who have had 3 consecutive failed login
     attempts) or have been deactivated by the system Admin, click on the </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Blocked/Deactivated
     Users</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'> link from the Users browser.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l113 level1 lfo121;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>This brings up a browser with a list of all
     Users whose account has been deactivated or blocked.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l113 level1 lfo121;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on the link </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Reactivate
     User</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'> next to the User whose account needs to be activated.<o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l130 level1 lfo189;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>On reactivating the User, a system generated e-Signature and
Password will be created and the User will be informed automatically by email.</span></p>

<h3><b><i><span style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h3>

<h3><a name="_Toc55184758"></a><a name="_Toc56933245"></a><a name="_Toc56934882"></a><a
name="_Toc56934990"><span style='mso-bookmark:_Toc56934882'><span
style='mso-bookmark:_Toc56933245'><span style='mso-bookmark:_Toc55184758'><b><i><span
style='font-size:10.0pt'>Multiple Organization Access</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'> <o:p></o:p></span></i></b></h3>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Unless
specified otherwise, a User in the system belongs to an organization and can
view/edit <%=LC.Pat_Patient_Lower%>-related information for <%=LC.Pat_Patients_Lower%> belonging to that
organization only. However, Users may need access to <%=LC.Pat_Patient_Lower%> details from
multiple organizations, such as if one organization is a parent organization or
if an investigator has <%=LC.Pat_Patients_Lower%> in multiple facilities, etc. To allow such
scenarios, the Velos system has the ability to provide Multiple Organization
Access.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.3in;line-height:150%;mso-list:l124 level1 lfo45;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from your
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Users </span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>in the<b> </b>sub-menu.<o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l99 level1 lfo131;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on the </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>User Name</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     of the User who needs access to multiple organizations.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l99 level1 lfo131;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>In the User Details page, click on the link </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Multiple
     Organization Access</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'><o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Clicking
on this link brings up a pop-up window with all organizations in the account
and the ability to select New, Edit and View rights for one or more. The
organizations are displayed with clear definition of parent and child
organizational relationships.<o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l99 level1 lfo131;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Depending on the specific situation, select
     User has access to all child organizations or User has access to only
     specified organizations. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l99 level1 lfo131;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>As a default, Users automatically inherit the
     Group rights for Manage <%=LC.Pat_Patients%> for the Users primary organization. If
     those rights are changed from this page, the Users specific rights
     override the Group rights that were inherited.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l99 level1 lfo131;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If the option User has access to only specified
     organizations is selected, the Admin can assign New, Edit and View rights
     for any of the other organizations in the list in addition to a Users
     primary organization.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l99 level1 lfo131;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If the option User has access to all child
     organizations is selected, automatically all child organizations (of the
     Users primary organization) are switched on for New, Edit and View
     rights. Admin can make edits to that list.<span style="mso-spacerun:
     yes">  </span><i>Note: Parent-child organizational relationships are
     defined in the New/Edit Organizations pages.</i><o:p></o:p></span></li>
</ul>

<p class=MsoToc1><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184759"></a><a name="_Toc56933246"></a><a name="_Toc56934883"></a><a
name="_Toc56934991"><span style='mso-bookmark:_Toc56934883'><span
style='mso-bookmark:_Toc56933246'><span style='mso-bookmark:_Toc55184759'><b><i><span
style='font-family:Arial;color:#993366'>Managing Account Links</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc55184760"></a><a name="_Toc56933247"></a><a name="_Toc56934884"></a><a
name="_Toc56934992"><span style='mso-bookmark:_Toc56934884'><span
style='mso-bookmark:_Toc56933247'><span style='mso-bookmark:_Toc55184760'><b><i><span
style='font-size:10.0pt'>Adding A New Link To Account Links</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Accounts can
be customized to display a list of links to appear on the My eResearch
Homepage and Adverse Event Details pages of all account Users. This can
include announcements, messages or articles of interest that the network
administrator would like to share with all account Users. </p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>To add a new
link to this list:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l13 level1 lfo46;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Account Links </span></b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>in the<b><span
style='color:#CC99FF'> </span></b></span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l13 level1 lfo46;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The browser displays a list of the links that have already
been added under the two categories: Links to be included in the Homepage and
Links to be included in the Adverse Events Details page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l13 level1 lfo46;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on any Link Description to view or edit link details.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l13 level1 lfo46;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Add a New Link</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> to add a new General or Adverse
Event link to this list.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l13 level1 lfo46;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter link details, such as Section heading, URL and a brief
description.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l13 level1 lfo46;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select General from the link type dropdown in case this
link needs to be displayed in the Quick Links box in My eResearch Homepage. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l13 level1 lfo46;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select Adverse Event from the link type dropdown in case
this link needs to be displayed in the Important Links box in the Adverse Event
Details page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l22 level1 lfo188;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
style="mso-spacerun: yes">   </span>The new link is now added to the list of
Account Links.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h1><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></h1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184761"></a><a name="_Toc56933248"></a><a name="_Toc56934885"></a><a
name="_Toc56934993"><span style='mso-bookmark:_Toc56934885'><span
style='mso-bookmark:_Toc56933248'><span style='mso-bookmark:_Toc55184761'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt;font-family:Arial'>Account
Personalization</span></span></span></span></a><span style='font-size:18.0pt;
mso-bidi-font-size:10.0pt;font-family:Arial'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><span style='font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoHeader style='tab-stops:.5in'><span style='font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section describes options available to Users to personalize and manage their
account with respect to password, automatic logout time, favorite links,
etc.<span style="mso-spacerun: yes">  </span><o:p></o:p></span></p>

<p class=MsoHeading7 style='text-align:justify'><a name="_Toc524755587"><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></a></p>

<p class=MsoNormal><span style='mso-bookmark:_Toc524755587'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><span style='mso-bookmark:_Toc524755587'><a name="_Toc55184762"></a><a
name="_Toc56933249"></a><a name="_Toc56934886"></a><a name="_Toc56934994"><span
style='mso-bookmark:_Toc56934886'><span style='mso-bookmark:_Toc56933249'><span
style='mso-bookmark:_Toc55184762'><b><i><span style='font-family:Arial;
color:#993366'>Editing Your Profile</span></i></b></span></span></span></a></span><span
style='mso-bookmark:_Toc524755587'><b><i><span style='font-family:Arial;
color:#993366'><o:p></o:p></span></i></b></span></h2>

<p class=MsoNormal><span style='mso-bookmark:_Toc524755587'><span
style='font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></span></p>

<span style='mso-bookmark:_Toc524755587'></span>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Users may
check or update their profile at any time.<span style="mso-spacerun: yes"> 
</span>To access your profile information:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l169 level1 lfo47;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Personalize Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l169 level1 lfo47;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The screen will display a selection of tabs across the top
showing the Users options for personal account customization.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l169 level1 lfo47;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the <b>My Profile</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l169 level1 lfo47;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Check your profile information or make any changes as
required and </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:.05in;
line-height:150%'><!--[if gte vml 1]><v:rect id="_x0000_s1384" style='position:absolute;
 left:0;text-align:left;margin-left:108pt;margin-top:233.3pt;width:45pt;
 height:9pt;z-index:1' stroked="f"/><![endif]--><![if !vml]><span
style='mso-ignore:vglayout;position:absolute;z-index:0;left:0px;margin-left:
144px;margin-top:311px;width:60px;height:12px'><img width=60 height=12
src="./Velos%20eResearch%20User%20Manual(v-4.0-2.3).online%20help_files/image001.gif"
v:shapes="_x0000_s1384"></span><![endif]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><span style="mso-spacerun: yes">  
</span><o:p></o:p></span></p>

<h2><a name="_Toc55184763"></a><a name="_Toc56933250"></a><a name="_Toc56934887"></a><a
name="_Toc56934995"><span style='mso-bookmark:_Toc56934887'><span
style='mso-bookmark:_Toc56933250'><span style='mso-bookmark:_Toc55184763'><b><i><span
style='font-family:Arial;color:#993366'>Changing Your Password</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>You may
change your password at any time should you have concerns regarding the privacy
of your information. You may also<ins cite="mailto:psahai"
datetime="2001-09-04T14:13"> specify a time duration </ins>(number of days) <ins
cite="mailto:psahai" datetime="2001-09-04T14:15">in which your password will
automatically expire</ins>. To change your password:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l125 level1 lfo48;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Personalize Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l125 level1 lfo48;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The screen will display a selection of tabs across the top
showing the Users options for personal account customization.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l125 level1 lfo48;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the <b><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T14:16">Change </del></span>Password<ins
cite="mailto:psahai" datetime="2001-09-04T14:16">/e-Sign</ins></b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l125 level1 lfo48;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>You will be required to enter your old and new password and </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184764"></a><a name="_Toc56933251"></a><a name="_Toc56934888"></a><a
name="_Toc56934996"><span style='mso-bookmark:_Toc56934888'><span
style='mso-bookmark:_Toc56933251'><span style='mso-bookmark:_Toc55184764'><b><i><span
style='font-family:Arial;color:#993366'>Changing Your e-Signature</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText2 style='text-align:justify;text-indent:0in;mso-text-indent-alt:
0in;line-height:150%;mso-list:none;mso-list-ins:psahai 20010904T1412'><ins
cite="mailto:psahai" datetime="2001-09-04T14:12">You may change your e-</ins>S<ins
cite="mailto:psahai" datetime="2001-09-04T14:12">ignature at any time </ins><ins
cite="mailto:psahai" datetime="2001-09-04T14:13">or specify a time duration </ins>(number
of days) <ins cite="mailto:psahai" datetime="2001-09-04T14:15">in which your e-</ins>S<ins
cite="mailto:psahai" datetime="2001-09-04T14:15">ignature will automatically
expire</ins><ins cite="mailto:psahai" datetime="2001-09-04T14:12">. To change
your </ins><ins cite="mailto:psahai" datetime="2001-09-04T14:16">e-</ins>S<ins
cite="mailto:psahai" datetime="2001-09-04T14:16">ignature</ins><ins
cite="mailto:psahai" datetime="2001-09-04T14:12">:</ins></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l89 level1 lfo49;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Personalize Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar.<ins cite="mailto:psahai" datetime="2001-09-04T14:12"><o:p></o:p></ins></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l89 level1 lfo49;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T14:12">The
screen will display a selection of tabs across the top </ins>showing<ins
cite="mailto:psahai" datetime="2001-09-04T14:12"> the </ins>U<ins
cite="mailto:psahai" datetime="2001-09-04T14:12">sers options for personal
account customization</ins>.<ins cite="mailto:psahai"
datetime="2001-09-04T14:12"><o:p></o:p></ins></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l89 level1 lfo49;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T14:12">Select
the <b>Password</b></ins><b><ins cite="mailto:psahai"
datetime="2001-09-04T14:16">/e-Sign</ins></b> <ins cite="mailto:psahai"
datetime="2001-09-04T14:12">tab</ins>.<ins cite="mailto:psahai"
datetime="2001-09-04T14:12"><o:p></o:p></ins></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l89 level1 lfo49;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T14:12">You
will be required to enter your old and new </ins><ins cite="mailto:psahai"
datetime="2001-09-04T14:17">e-</ins>S<ins cite="mailto:psahai"
datetime="2001-09-04T14:17">ignature</ins><ins cite="mailto:psahai"
datetime="2001-09-04T14:12"> and </ins></span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T14:12">Submit</ins></span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T14:12"><o:p></o:p></ins></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184765"></a><a name="_Toc56933252"></a><a name="_Toc56934889"></a><a
name="_Toc56934997"><span style='mso-bookmark:_Toc56934889'><span
style='mso-bookmark:_Toc56933252'><span style='mso-bookmark:_Toc55184765'><b><i><span
style='font-family:Arial;color:#993366'>Managing My Links</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:11.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Each Users
My eResearch Homepage can be customized to display a list of favorite
links.<span style="mso-spacerun: yes">  </span>To add a new link to your list:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l21 level1 lfo50;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Personalize Account </span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>from the
left-hand menu bar.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l21 level1 lfo50;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The screen will display a selection of tabs across the top
showing the Users options for personal account customization.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l21 level1 lfo50;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the <b>My Links</b> tab (by default this tab page
will be displayed).<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l21 level1 lfo50;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on any </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Link Description</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to view
and edit its details.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l21 level1 lfo50;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new link to this list,<b><i><span style='color:
#993366'> </span></i></b>click on </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Add a New Link</span></u><u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> </span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
style="mso-spacerun: yes"> </span><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:-.25in;margin-bottom:
0in;margin-left:.55in;margin-bottom:.0001pt;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l21 level1 lfo50;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter details regarding the link, such as its URL and a
brief description, and </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l21 level1 lfo50;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The new link is now added to your list of customized
favorite links.<ins cite="mailto:psahai" datetime="2001-09-04T14:19"><o:p></o:p></ins></span></p>

<p class=MsoNormal><span style='font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184766"></a><a name="_Toc56933253"></a><a name="_Toc56934890"></a><a
name="_Toc56934998"><span style='mso-bookmark:_Toc56934890'><span
style='mso-bookmark:_Toc56933253'><span style='mso-bookmark:_Toc55184766'><b><i><span
style='font-family:Arial;color:#993366'>Signing Up For The Notification Service</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Users can
sign up to receive automatic email notifications whenever a new study protocol
is broadcast to the public in their area of interest.<span style="mso-spacerun:
yes">  </span>To subscribe to this service:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l180 level1 lfo51;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Personalize Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l180 level1 lfo51;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The screen will display a selection of tabs across the top
displaying the Users options for personal account customization.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l180 level1 lfo51;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the <b>New Trials</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l180 level1 lfo51;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of therapeutic areas that the User has already
subscribed will be shown.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l180 level1 lfo51;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Subscribe to Notifications for
other Therapeutic Areas</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> link to add more therapeutic areas to the list.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l180 level1 lfo51;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select all the therapeutic areas of interest and </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l180 level1 lfo51;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Whenever a new study protocol is broadcast in any of these
therapeutic areas, you will be sent an email notifying you of the new study.<o:p></o:p></span></p>

<p class=MsoToc3 style='text-align:justify;line-height:150%'><span
style='font-size:12.0pt;font-family:Arial;text-transform:uppercase'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184767"></a><a name="_Toc56933254"></a><a name="_Toc56934891"></a><a
name="_Toc56934999"><span style='mso-bookmark:_Toc56934891'><span
style='mso-bookmark:_Toc56933254'><span style='mso-bookmark:_Toc55184767'><b><i><span
style='font-family:Arial;color:#993366'>Customizing Your Automatic Logout Time</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:11.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>The Automatic
Logout Feature protects the privacy and security of your account.<span
style="mso-spacerun: yes">  </span>If you are not working on your application
or walk away from your computer, your account will automatically logout at the
end of the duration specified. </p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l23 level1 lfo52;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Personalize Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l23 level1 lfo52;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The screen will display a selection of tabs across the top
displaying the Users options for personal account customization.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l23 level1 lfo52;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the <b>Settings</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l23 level1 lfo52;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Specify the duration after which you would like the
application to logout automatically by entering the value (in minutes) in the
specified field and </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h1><span style='font-size:10.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></h1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184768"></a><a name="_Toc56933255"></a><a name="_Toc56934892"></a><a
name="_Toc56935000"><span style='mso-bookmark:_Toc56934892'><span
style='mso-bookmark:_Toc56933255'><span style='mso-bookmark:_Toc55184768'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Message Center</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section describes the process involved in searching broadcast studies, viewing
requests for more information and granting permission to access the study
details.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184769"></a><a name="_Toc56933256"></a><a name="_Toc56934893"></a><a
name="_Toc56935001"><span style='mso-bookmark:_Toc56934893'><span
style='mso-bookmark:_Toc56933256'><span style='mso-bookmark:_Toc55184769'><b><i><span
style='font-family:Arial;color:#993366'>Searching a Broadcast Study</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'> <o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Users can
search Study Summaries from our entire database of studies that are being
broadcast to the public.<span style="mso-spacerun: yes">  </span>All study
protocols documented in the Velos eResearch system are restricted in access by
the protocol author.<span style="mso-spacerun: yes">  </span>The author must
broadcast a summary of the study for it to appear in the Search result.<span
style="mso-spacerun: yes">  </span>To search a study summary:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l16 level1 lfo53;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Personalize Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l16 level1 lfo53;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The screen will display a selection of tabs across the top
displaying the Users options for personal account customization.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l16 level1 lfo53;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the <b>New Trials</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l16 level1 lfo53;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter a keyword in the Search field and click on the </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Go</span><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> button.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l158 level1 lfo186;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A window will open and display the search results in the
browser with Study Number, Study Title. It also displays View and Modify links
to communicate with the protocol author to request more information for the
study.</span></p>

<p class=MsoNormal style='margin-left:.3in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184770"></a><a name="_Toc56933257"></a><a name="_Toc56934894"></a><a
name="_Toc56935002"><span style='mso-bookmark:_Toc56934894'><span
style='mso-bookmark:_Toc56933257'><span style='mso-bookmark:_Toc55184770'><b><i><span
style='font-family:Arial;color:#993366'>Sending A Request For More Information
For A Broadcast Study</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'>If your search yields a study that is of
interest, you may communicate with the protocol author to request more
information.<o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l193 level1 lfo54;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select <b>Personalize Account &gt;&gt; New Trials</b> tab
page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l193 level1 lfo54;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter keywords in the Search field and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Go</span><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> button.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l193 level1 lfo54;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A window displaying the search results is displayed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l193 level1 lfo54;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Clicking on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Study Number</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link will
bring up the study details.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l193 level1 lfo54;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Next to each study title displayed in the search results,
there is a Send a request for followed by </span><u><span style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>View</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> and </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Modify</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> links.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l193 level1 lfo54;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>View</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> if you wish to get a copy of the
online study protocol in a Read Only format.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l193 level1 lfo54;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Modify</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> to send a request for an
Editable format of the protocol.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l81 level1 lfo187;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Your request for more information along with your name and
profile information will be sent to the protocol author.<span
style="mso-spacerun: yes">  </span>It is up to the protocol author to grant or
deny your request.</span> </p>

<p class=MsoNormal><a name="_Toc524755597"><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></a></p>

<h2><span style='mso-bookmark:_Toc524755597'><a name="_Toc55184771"></a><a
name="_Toc56933258"></a><a name="_Toc56934895"></a><a name="_Toc56935003"><span
style='mso-bookmark:_Toc56934895'><span style='mso-bookmark:_Toc56933258'><span
style='mso-bookmark:_Toc55184771'><b><i><span style='font-family:Arial;
color:#993366'>Viewing A Request For More Information For A Broadcast Study</span></i></b></span></span></span></a></span><span
style='mso-bookmark:_Toc524755597'><b><i><span style='font-family:Arial;
color:#993366'> <o:p></o:p></span></i></b></span></h2>

<p class=MsoNormal><span style='mso-bookmark:_Toc524755597'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<span style='mso-bookmark:_Toc524755597'></span>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Every User
has a secure Message Center through which they can view requests (from other
Users) for a copy of a protocol that they have authored, grant or deny access
to the requester, and keep track of their own requests that have been
answered.<span style="mso-spacerun: yes">  </span>Your study summary needs to
have been broadcast to the public before you can receive requests for a
Protocol Copy.<span style="mso-spacerun: yes">  </span>When you broadcast your
Study Summary and make it available for Public Search, then other members of
the Velos eResearch community can view your Study Summary and send you a
request for more information.<span style="mso-spacerun: yes">  </span><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l44 level1 lfo55;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Login to your account and enter your My eResearch
Homepage.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l44 level1 lfo55;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Your Message Center will display all </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Unread</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> Messages
that are requests from other Users who would like to have a copy of your
protocol. You will see the name of the requester with a link to their profile
information, the study that they have sent a request for and the type of
permission (Modify or View) that they would like to receive for their Protocol
Copy. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l44 level1 lfo55;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Clicking on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Read</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link will
display a list of all messages that you have previously received and answered,
along with a record of the permissions that you granted.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l33 level1 lfo185;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Clicking on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Acknowledgements</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
displays the list of studies that you have requested and which have been
answered.</span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:1.5in'><span style="mso-spacerun:
yes">     </span></p>

<p class=MsoNormal style='margin-left:1.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184772"></a><a name="_Toc56933259"></a><a name="_Toc56934896"></a><a
name="_Toc56935004"><span style='mso-bookmark:_Toc56934896'><span
style='mso-bookmark:_Toc56933259'><span style='mso-bookmark:_Toc55184772'><b><i><span
style='font-family:Arial;color:#993366'>Viewing Profile Of Message Sender</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'> <o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Once you
broadcast your study summary and make it available for Public Search, you may
receive a Request for More Information from other members of the Velos
eResearch community.<span style="mso-spacerun: yes">  </span>To view these
requests: <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l145 level1 lfo56;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Login to your account and enter your My eResearch
Homepage.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l145 level1 lfo56;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Your Message Center will display all Unread Messages that
are requests from other Users who would like to have a copy of your protocol. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l145 level1 lfo56;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>You will see the name of the requester with a link to their
profile information, the study that they have requested and the type of
permission (Modify or View) that they would like to receive for their Protocol
Copy.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l145 level1 lfo56;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Clicking on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Name</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> of the
message sender will display their profile information, including contact
information.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l58 level1 lfo184;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once you decide that you would like to share a Protocol Copy
with this User, you may grant them View or Modify rights to your Protocol
Copy.<span style="mso-spacerun: yes">  </span>It is your option to grant or
deny the request.</span> </p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.05in;text-indent:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.05in;text-indent:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184773"></a><a name="_Toc56933260"></a><a name="_Toc56934897"></a><a
name="_Toc56935005"><span style='mso-bookmark:_Toc56934897'><span
style='mso-bookmark:_Toc56933260'><span style='mso-bookmark:_Toc55184773'><b><i><span
style='font-family:Arial;color:#993366'>Granting Access To A Study Protocol</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Once you
broadcast your Study Summary and make it available for Public Search, you may
receive a Request for More Information from other members of the Velos
eResearch community.<span style="mso-spacerun: yes">  </span>You may grant or
deny the request. View allows the User to view details and Modify grants
editing capabilities. Modifications made by them will not be reflected in your
protocol but will be limited to their copy.<span style="mso-spacerun: yes">  
</span><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l40 level1 lfo57;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Login to your account.<span style="mso-spacerun: yes"> 
</span>Your Message Center will display Unread Messages that are requests from
other Users who would like to have a copy of your protocol. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l40 level1 lfo57;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>You will see the name of the requester with a link to their
profile information, the study that they have requested and the type of
permission (Modify or View) that they would like to receive for their Protocol
Copy. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l40 level1 lfo57;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the senders </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Name</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to
display their profile and contact information.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l40 level1 lfo57;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once you decide that you would like to share a Protocol Copy
with this User, you may grant them View or Modify rights to your Protocol Copy by
selecting the appropriate radio button and clicking on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit </span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><span style='mso-prop-change:psahai 20010910T1115'><span
style='mso-prop-change:psahai 20010910T1115'><o:p></o:p></span></span></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l40 level1 lfo57;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial;text-transform:uppercase'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-10T11:16">By
giving your study 'View' permission, you will provide the requester with access
to the current </ins>S<ins cite="mailto:psahai" datetime="2001-09-10T11:16">napshot
of your study. </ins><span style="mso-spacerun: yes"> </span><ins
cite="mailto:psahai" datetime="2001-09-10T11:16">A </ins>S<ins
cite="mailto:psahai" datetime="2001-09-10T11:16">napshot of a study is an image
of study data such as Study Summary, </ins>P<ins cite="mailto:psahai"
datetime="2001-09-10T11:16">ublic Sections and Appendix.</ins><span
style='text-transform:uppercase'><ins cite="mailto:psahai"
datetime="2001-09-10T11:15"><o:p></o:p></ins></span></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l40 level1 lfo57;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial;text-transform:uppercase'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>If you decide to Deny the request for a Protocol Copy,
select the Deny radio button and click on </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;text-transform:
uppercase'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l107 level1 lfo183;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
text-transform:uppercase'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The requester will receive an acknowledgement of your
message, whether it was to grant access or deny it.<span style="mso-spacerun:
yes">  </span>If you have granted access, they will also receive a link to
access a copy of your protocol.</span> <span style='text-transform:uppercase'><span
style='mso-prop-change:psahai 20010910T1118'><span style='mso-prop-change:psahai 20010910T1118'><o:p></o:p></span></span></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184774"></a><a name="_Toc56933261"></a><a name="_Toc56934898"></a><a
name="_Toc56935006"><span style='mso-bookmark:_Toc56934898'><span
style='mso-bookmark:_Toc56933261'><span style='mso-bookmark:_Toc55184774'><b><i><span
style='font-family:Arial;color:#993366'>Taking And Updating A Snapshot</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'> <o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%;mso-list:none;
mso-list-ins:psahai 20010910T1118'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-10T11:18">A
Snapshot will be created automatically by the system in following cases: <o:p></o:p></ins></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='margin-top:5.0pt;margin-bottom:5.0pt;text-align:
     justify;line-height:150%;mso-list:l131 level1 lfo3;tab-stops:list .5in'><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
     cite="mailto:psahai" datetime="2001-09-10T11:18">When you release the
     study to </ins>P<ins cite="mailto:psahai" datetime="2001-09-10T11:18">ublic
     from </ins><b>Broadcast/<ins cite="mailto:psahai"
     datetime="2001-09-10T11:18">Notify</ins></b><ins cite="mailto:psahai"
     datetime="2001-09-10T11:18"> tab for the first time, a Snapshot is
     created. <o:p></o:p></ins></span></li>
</ul>

<p class=MsoNormal style='margin-top:5.0pt;margin-right:0in;margin-bottom:5.0pt;
margin-left:.5in;text-align:justify;text-indent:-.5in;mso-text-indent-alt:-.25in;
line-height:150%;mso-list:l131 level1 lfo3;mso-list-ins:psahai 20010910T1118;
tab-stops:list .5in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-10T11:18">·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></ins></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-10T11:18">If
you have <u>not</u> broadcast the study and you want to grant 'View' access to
an External </ins>User<ins cite="mailto:psahai" datetime="2001-09-10T11:18">
from <b>Study Team</b></ins> <ins cite="mailto:psahai"
datetime="2001-09-10T11:18">tab, a Snapshot is created. </ins><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:5.0pt;margin-right:0in;margin-bottom:5.0pt;
margin-left:.5in;text-align:justify;text-indent:-.5in;mso-text-indent-alt:-.25in;
line-height:150%;mso-list:l131 level1 lfo3;mso-list-ins:psahai 20010910T1118;
tab-stops:list .5in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial;
text-transform:uppercase'><ins cite="mailto:psahai" datetime="2001-09-10T11:18">·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></ins></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-10T11:18">Study
Snapshot will be updated automatically every time you release the Study to </ins>P<ins
cite="mailto:psahai" datetime="2001-09-10T11:18">ublic from </ins><b>Broadcast/<ins
cite="mailto:psahai" datetime="2001-09-10T11:18">Notify</ins></b><ins
cite="mailto:psahai" datetime="2001-09-10T11:18"> tab.</ins><span
style='text-transform:uppercase'><o:p></o:p></span></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184775"></a><a name="_Toc56933262"></a><a name="_Toc56934899"></a><a
name="_Toc56935007"><span style='mso-bookmark:_Toc56934899'><span
style='mso-bookmark:_Toc56933262'><span style='mso-bookmark:_Toc55184775'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Dynamic Forms</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section describes the steps involved in creating dynamic fields and forms and
the process of associating the form to a study or <%=LC.Pat_Patient_Lower%>, or providing access
to it, at the account level. With this capability, Users can design their own
questionnaires, case report forms, regulatory forms and <%=LC.Pat_Patient%> databases, to
name a few.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184776"></a><a name="_Toc56933263"></a><a name="_Toc56934900"></a><a
name="_Toc56935008"><span style='mso-bookmark:_Toc56934900'><span
style='mso-bookmark:_Toc56933263'><span style='mso-bookmark:_Toc55184776'><b><i><span
style='font-family:Arial;mso-bidi-font-family:"Times New Roman";color:#993366'>Creating/Editing
a Field in the Field Library</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;mso-bidi-font-family:"Times New Roman";color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Velos eResearch provides Users with the ability to define or
import fields, store the fields in a Field Library and subsequently use them to
design forms in a matter of minutes.<o:p></o:p></span></p>

<h3><b><i><span style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h3>

<h3><a name="_Toc55184777"></a><a name="_Toc56933264"></a><a name="_Toc56934901"></a><a
name="_Toc56935009"><span style='mso-bookmark:_Toc56934901'><span
style='mso-bookmark:_Toc56933264'><span style='mso-bookmark:_Toc55184777'><b><i><span
style='font-size:10.0pt'>Field Library</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Depending on the accounts needs, the Field Library can be a
compilation of standard data elements, and/or fields created by the end-Users,
to be used in designing forms. This dictionary/library allows reuse of elements
and standardization across the account for querying and reporting.<o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To access the Field Library, s<ins cite="mailto:psahai"
datetime="2001-09-04T15:45">e</ins>lect<ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> </ins></span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45">Library</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> from your </ins>left-hand<ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>menu<ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> bar and </ins>click on<ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins></span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Field
Library</span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>in
the sub-menu. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The Field Library browser will be displayed, listing all the
fields currently available in the library, </span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>search
options allow searching fields based on category, field name and keyword.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To add a new Category
for grouping Fields within the Library, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Add a New Category</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
link and enter the Category name and description in the pop-up window that
comes up. Click on </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:"Arial Black"'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
to save your data. Examples of a Category would be Diagnosis, Demographics,
etc.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l82 level1 lfo133;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To edit Categories,
click on the appropriate </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Category</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'> link and make required changes.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To add a new Edit
Field to the Library, click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Add/Edit
Field</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'> link. Enter the
following information:<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Category: Select the
appropriate option from the list of available Categories.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Field Display Name:
Enter the field name, as you would like to see it appear in the form.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Field ID:
Specify a Unique ID for the field within the Field Library.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Search Keyword: Enter
a keyword for the field (helps in performing a search for the field).<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Field Help: Enter the
help text that you would like Users to see in the finished form (seen when
cursor is placed over field).<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Edit Box Type:
Specify whether the Edit box is Text, Number or Date field.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Enter field length,
Maximum characters, Default response and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black"'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To copy an existing
Field, click on the link </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Copy an
Existing Field</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>, select the field
from the Library, copy and make required changes.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l82 level1 lfo133;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To add a Multiple
Choice Field to the Field Library, click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Add Multiple Choice Field</span></u><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>,
select the field type e.g. Dropdown, Checkbox or Radio Button and enter the
required data as specified above.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l82 level1 lfo133;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To modify an existing
field, click on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Field Name</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'> link and to delete a field, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Delete</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'> link next to
the respective field name in the Field Library browser.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184778"></a><a name="_Toc56933265"></a><a name="_Toc56934902"></a><a
name="_Toc56935010"><span style='mso-bookmark:_Toc56934902'><span
style='mso-bookmark:_Toc56933265'><span style='mso-bookmark:_Toc55184778'><b><i><span
style='font-family:Arial;mso-bidi-font-family:"Times New Roman";color:#993366'>Designing
a New Form</span></i></b></span></span></span></a><span style='mso-bookmark:
_Toc55184778'></span><span style='mso-bookmark:_Toc56933265'></span><span
style='mso-bookmark:_Toc56934902'></span><span style='mso-bookmark:_Toc56935010'></span><b><i><span
style='font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman";
color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal style='margin-left:.3in;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='margin-left:0in'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>The Dynamic Forms module allows
Users to design forms using fields that have been imported or added to the
Field Library. If required fields are not available in the Library, they can be
added directly in the Form without affecting the Field Library.<span
style="mso-spacerun: yes">  </span>Forms can be stored in a Form Library for
subsequent reuse.<o:p></o:p></span></p>

<p class=MsoBodyTextIndent><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc55184779"></a><a name="_Toc56933266"></a><a name="_Toc56934903"></a><a
name="_Toc56935011"><span style='mso-bookmark:_Toc56934903'><span
style='mso-bookmark:_Toc56933266'><span style='mso-bookmark:_Toc55184779'><b><i><span
style='font-size:10.0pt'>Form Library</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l25 level2 lfo134;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To access the Form Library, s<ins cite="mailto:psahai"
datetime="2001-09-04T15:45">e</ins>lect<ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> </ins></span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45">Library</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> from your </ins>left-hand<ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>menu<ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> bar and </ins>click on<ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins></span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Form
Library</span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>in
the sub-menu.</span> </p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l25 level1 lfo134;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>The
     displayed Form Library browser lists previously created forms stored in
     the Library along with details such as form name, type, description,
     status and form sharing information. A specific form may be searched by
     the options provided: By Form Name and By Form Type.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l25 level1 lfo134;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form Types
     are meant for organizing forms into groups for easy searching. To add a
     new form type, click on the link </span><u><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Add
     New Form Type</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>, enter
     Form Type and Type Description, and click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black"'>Submit</span><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     mso-bidi-font-family:"Times New Roman"'> to save. To edit this
     information, click on the appropriate </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
     "Times New Roman"'>Form Type</span></u><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
     link in your Form Library browser.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l25 level1 lfo134;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To open a
     Form for editing, click on the Form </span><u><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Name</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     mso-bidi-font-family:"Times New Roman"'> link.<span style="mso-spacerun:
     yes">  </span>To design a new form, click on the link </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     mso-bidi-font-family:"Times New Roman"'>Create a New Form</span></u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     mso-bidi-font-family:"Times New Roman"'>.</span><span style='font-size:
     10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
     "Times New Roman"'><span style="mso-spacerun: yes">  </span>This opens up
     the Form Designer Wizard with the following three steps: Define the Form,
     Add Fields and Form Settings.<o:p></o:p></span></li>
</ul>

<h3><b><i><span style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h3>

<h3><a name="_Toc55184780"></a><a name="_Toc56933267"></a><a name="_Toc56934904"></a><a
name="_Toc56935012"><span style='mso-bookmark:_Toc56934904'><span
style='mso-bookmark:_Toc56933267'><span style='mso-bookmark:_Toc55184780'><b><i><span
style='font-size:10.0pt'>Define the Form</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>This
is the first step in the form designing process and captures the following
information.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l128 level1 lfo138;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form
     Name: Name of the finished form to be displayed to Users performing data
     entry.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l128 level1 lfo138;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form
     Description: A brief description of the form.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l128 level1 lfo138;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form
     Type: Form type to be selected from the list of available options.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l128 level1 lfo138;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form
     Status: Defaults to Work in Progress and allows changes to be made to
     the form as long as the form has this status. Changing the status to
     Freeze does not allow further changes to be made to the form.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l128 level1 lfo138;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Shared
     With: Defines Users who have access to this form in the Library. This can
     be Private, shared with all organization Users, All Study Team
     Members, All Group Users and All Account Users. The default value is
     all account Users.<span style="mso-spacerun: yes">  </span>Access can be
     given to multiple groups, organizations and study teams, if required.<o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>To make a copy of an existing form, click on </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Copy an Existing Form</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
link and select the form to be copied from your Form Library. Enter the
information in the fields mentioned above and click on the<span
style="mso-spacerun: yes">  </span></span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black"'>Next</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><span style="mso-spacerun: yes">  </span>button to save the
information and proceed to the next step  Add Fields.<o:p></o:p></span></p>

<h3><b><i><span style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h3>

<h3><a name="_Toc55184781"></a><a name="_Toc56933268"></a><a name="_Toc56934905"></a><a
name="_Toc56935013"><span style='mso-bookmark:_Toc56934905'><span
style='mso-bookmark:_Toc56933268'><span style='mso-bookmark:_Toc55184781'><b><i><span
style='font-size:10.0pt'>Add Fields</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>The Add Fields page is the second step in the form
designing process. This step defines the fields to be included in the form.
This can be done in two ways:<o:p></o:p></span></p>

<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l173 level1 lfo141;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>User may
     have elected to copy one or more previously created forms, as described
     above, and in that scenario all fields from the copied form will
     automatically be available in this page and can be edited, if required.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l173 level1 lfo141;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>User may
     add fields to a blank new form. These fields can be included in the form
     by selecting existing fields from the Field Library or by adding new
     fields directly to the form.<o:p></o:p></span></li>
</ol>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>The Add Fields page displays a browser with all the fields
that have been included in the form. Button to perform the following actions
are displayed across the top: <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l134 level1 lfo140;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Select from Library:
Allows User to select fields that exist in the Field Library.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l134 level1 lfo140;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Edit Box:<span
style="mso-spacerun: yes">  </span>Adds a new Edit field to the form (can be
Text, Date or Number).<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l134 level1 lfo140;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Multiple Choice: Adds
a new multiple-choice field to the form (can be Dropdown, Check box or Radio
button).<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l134 level1 lfo140;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Comments: Adds
comments to the form.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l134 level1 lfo140;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Line Break: Adds a
horizontal line at the specified place in the form.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l134 level1 lfo140;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>New Sections: Starts
a new section.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc55184782"></a><a name="_Toc56933269"></a><a name="_Toc56934906"></a><a
name="_Toc56935014"><span style='mso-bookmark:_Toc56934906'><span
style='mso-bookmark:_Toc56933269'><span style='mso-bookmark:_Toc55184782'><b><i><span
style='font-size:10.0pt'>Form Settings</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>The third step in the form design wizard is the Form
Settings page. In this page, User can specify the following:<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l98 level1 lfo142;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Fields that should
appear in the browser for answered forms. For example, data entry date is a
default option automatically included by the system. To include other fields,
click on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Add/Edit</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'> link. A pop-up window is displayed from which additional
fields can be selected. The maximum number of fields that can be displayed in
any browser is 10.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l98 level1 lfo142;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Pop-up messages or email
notifications linked to form submission. Clicking on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Specify New</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
link allows you to specify the message text to be displayed as a pop-up message
or to be sent as an email notification at the time of form submission.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184783"></a><a name="_Toc56933270"></a><a name="_Toc56934907"></a><a
name="_Toc56935015"><span style='mso-bookmark:_Toc56934907'><span
style='mso-bookmark:_Toc56933270'><span style='mso-bookmark:_Toc55184783'><b><i><span
style='font-family:Arial;mso-bidi-font-family:"Times New Roman";color:#993366'>Linking
a Form to a Study</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;mso-bidi-font-family:"Times New Roman";color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoToc1><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>To link a form to a specific study, select </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman";color:#CC99FF'>Manage Protocol</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'> from the left-hand menu bar and click on </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman";color:#CC99FF'>Open</span></b><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>
in the sub menu. Select the study to which you wish to link the form and click
on the <b>Study Setup</b> tab (available in the set of tabs across the top of
the screen). This page displays a list of all forms currently associated to the
selected study.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l75 level1 lfo144;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>To
     associate a new form to the study, click on the </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     mso-bidi-font-family:"Times New Roman"'>Select a Form from your Library</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     mso-bidi-font-family:"Times New Roman"'> link. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l75 level1 lfo144;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>The
     screen that comes up allows you to search forms on the basis of Form Name,
     Type or Study. Select the Form(s) that needs to be linked to the study.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l75 level1 lfo144;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>At this
     point, the User is allowed to edit the following information:<o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='margin-left:.8in;text-align:justify;text-indent:-.3in;
line-height:150%;mso-list:l57 level1 lfo145;tab-stops:list .8in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form Name (brings in
selected name but can be changed)<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.8in;text-align:justify;text-indent:-.3in;
line-height:150%;mso-list:l57 level1 lfo145;tab-stops:list .8in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form Description
(brings in selected description but can be changed)<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.8in;text-align:justify;text-indent:-.3in;
line-height:150%;mso-list:l57 level1 lfo145;tab-stops:list .8in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Display Form Link
(with options for displaying the form at the Study level or at the
Study-<%=LC.Pat_Patient%> level)<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.8in;text-align:justify;text-indent:-.3in;
line-height:150%;mso-list:l57 level1 lfo145;tab-stops:list .8in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Select Organization
link for restricting the study team members or <%=LC.Pat_Patients_Lower%> of a specific
organization that should have the form linked to them.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.8in;text-align:justify;text-indent:-.3in;
line-height:150%;mso-list:l57 level1 lfo145;tab-stops:list .8in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Form Characteristic
(with options for whether the form is to be answered only once and the answered
form should reopen every time for editing, or if the form should allow
answering multiple times)<o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l95 level1 lfo147;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Select
     information, make changes and click on </span><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:"Arial Black"'>Submit</span><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     mso-bidi-font-family:"Times New Roman"'> to link the form to the study.<o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Once the Form has been associated to a study, it can be
modified at the Study level and these changes are not reflected back to the
Library. To make changes in a form, click on the appropriate form </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>Name</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'> link in the
Associated Forms browser. Once the form is ready to be made available for
data entry, change the Form Status to Active. Forms that have a status of
Work in Progress are not available for data entry.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184784"></a><a name="_Toc56933271"></a><a name="_Toc56934908"></a><a
name="_Toc56935016"><span style='mso-bookmark:_Toc56934908'><span
style='mso-bookmark:_Toc56933271'><span style='mso-bookmark:_Toc55184784'><b><i><span
style='font-family:Arial;mso-bidi-font-family:"Times New Roman";color:#993366'>Linking
a Form at the Account Level</span></i></b></span></span></span></a><span
style='mso-bookmark:_Toc55184784'></span><span style='mso-bookmark:_Toc56933271'></span><span
style='mso-bookmark:_Toc56934908'></span><span style='mso-bookmark:_Toc56935016'></span><span
style='font-size:10.0pt'> <o:p></o:p></span></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'>To link form(s) at the Account level, select </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman";color:#CC99FF'>Manage Account</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'> from the left-hand menu bar and click on </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman";color:#CC99FF'>Form Management</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'> in the sub menu.<span style="mso-spacerun: yes"> 
</span>Follow the steps as mentioned in the section Linking a Form to a
Study.<span style="mso-spacerun: yes">  </span>In addition to linking forms to
any study, the account administrator can also link forms to other parts of the
application such as:<o:p></o:p></span></p>

<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l167 level1 lfo150;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Account
     level (link to answer the form appears on Users homepage)<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l167 level1 lfo150;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><%=LC.Pat_Patient%>
     level (link to answer the form appears in the <%=LC.Pat_Patient%> Profile section
     for all <%=LC.Pat_Patients_Lower%>, irrespective of the study they are enrolled to)<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l167 level1 lfo150;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Study
     Level (link to answer the form appears for all studies)<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l167 level1 lfo150;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Specific
     Study Level (link to answer the form appears for a specific study)<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l167 level1 lfo150;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'>Specific
     Study - <%=LC.Pat_Patient%> level (link to answer the form appears for <%=LC.Pat_Patients_Lower%>
     enrolled to a specific study).<o:p></o:p></span></li>
</ol>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h1><a name="_Toc524755601"></a><a name="_Toc55184785"></a><a
name="_Toc56933272"></a><a name="_Toc56934909"></a><a name="_Toc56935017"><span
style='mso-bookmark:_Toc56934909'><span style='mso-bookmark:_Toc56933272'><span
style='mso-bookmark:_Toc55184785'><span style='mso-bookmark:_Toc524755601'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Protocol Management</span></span></span></span></span></a><span
style='mso-bookmark:_Toc524755601'><span style='font-size:18.0pt;mso-bidi-font-size:
10.0pt'> <o:p></o:p></span></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><span style='mso-bookmark:_Toc524755601'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoHeader style='tab-stops:.5in'><span style='mso-bookmark:_Toc524755601'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='mso-bookmark:_Toc524755601'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>This section describes the steps involved in protocol
design, documentation, broadcasting and sharing. <o:p></o:p></span></span></p>

<p class=MsoNormal><span style='mso-bookmark:_Toc524755601'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></span></p>

<p class=MsoNormal><span style='mso-bookmark:_Toc524755601'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></span></p>

<span style='mso-bookmark:_Toc524755601'></span>

<h2><a name="_Toc55184786"></a><a name="_Toc56933273"></a><a name="_Toc56934910"></a><a
name="_Toc56935018"><span style='mso-bookmark:_Toc56934910'><span
style='mso-bookmark:_Toc56933273'><span style='mso-bookmark:_Toc55184786'><b><i><span
style='font-family:Arial;color:#993366'>Documenting A New Study Protocol</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'> <o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>To
document a new study protocol, select </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><span
style='mso-prop-change:psahai 20010910T1126'><span style='mso-prop-change:psahai 20010910T1126'>Manage
Protocols</span></span></span></b><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> from your left-hand menu bar and click on </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><span
style='mso-prop-change:psahai 20010910T1126'><span style='mso-prop-change:psahai 20010910T1126'>New
</span></span></span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>in the</span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'> </span></b><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>sub-menu</span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>
</span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:
Arial'>followed by clicking on the </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Proceed
</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:
Arial'>button.</span><span style='font-size:10.0pt;font-family:Arial;
mso-bidi-font-family:"Times New Roman";mso-bidi-font-style:italic'><o:p></o:p></span></p>

<h3><a name="_Toc524755603"><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></h3>

<h3><span style='mso-bookmark:_Toc524755603'><a name="_Toc55184787"></a><a
name="_Toc56933274"></a><a name="_Toc56934911"></a><a name="_Toc56935019"><span
style='mso-bookmark:_Toc56934911'><span style='mso-bookmark:_Toc56933274'><span
style='mso-bookmark:_Toc55184787'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Study Summary</span></i></b></span></span></span></a></span><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Entering
Study Number, Title of the study, Therapeutic Area, Phase and Study Data
Manager is mandatory.<span style="mso-spacerun: yes">  </span>Enter information
in the other fields according to instructions and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>The Study
Data Manager is considered to be the Administrator for this protocol and will
have the following rights:<span style='color:black'> </span><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:126.7pt;text-align:justify;text-indent:
-9.35pt;line-height:150%;mso-list:l56 level1 lfo22;tab-stops:list 1.75in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:black'>Authority to Edit Protocol</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:126.7pt;text-align:justify;text-indent:
-9.35pt;line-height:150%;mso-list:l56 level1 lfo22;tab-stops:list 1.75in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:black'>Authority to Assign Rights to Other Users</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:126.7pt;text-align:justify;text-indent:
-9.35pt;line-height:150%;mso-list:l56 level1 lfo22;tab-stops:list 1.75in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:black'>Authority to View Reports Specific to this
Protocol</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

<h3><a name="_Toc524755604"><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></h3>

<h3><span style='mso-bookmark:_Toc524755604'><a name="_Toc55184788"></a><a
name="_Toc56933275"></a><a name="_Toc56934912"></a><a name="_Toc56935020"><span
style='mso-bookmark:_Toc56934912'><span style='mso-bookmark:_Toc56933275'><span
style='mso-bookmark:_Toc55184788'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Broadcast Protocol Summary for Public Viewing</span></i></b></span></span></span></a></span><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Each
section of the study protocol is followed by the query: Do you want
information in this section to be available to the public?<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
style="mso-spacerun: yes"> </span><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l118 level1 lfo92;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select Yes for all the sections that you would like to be
included in the Public Summary and click on </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l118 level1 lfo92;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Specify one or more keywords that will be used to search out
your study.<span style="mso-spacerun: yes">  </span>Examples of keywords that
are helpful to include are Therapeutic Area, Organization Name, Drug or
Disease Name, etc.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>When you
are ready to broadcast your Study Summary, go to the <ins cite="mailto:psahai"
datetime="2001-09-04T14:32">Broadcast/</ins>Notify tab across the top and click
on the </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:"Arial Black";mso-bidi-font-family:Arial'>Release Protocol to
Public</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> button. All the study sections selected for public
broadcast will be available for display when a search is performed. All Users
performing a search for protocol summaries will be able to view the sections
included in your protocol summary. They can contact you for more information or
request to be included in your trial. If you do not want to publish the trial
at this time, select the 'No' option.<span style="mso-spacerun: yes"> 
</span>You can open this protocol at anytime and broadcast a summary for public
viewing.</span></p>

<h3><a name="_Toc524755605"></a><a name="_Toc55184789"></a><a
name="_Toc56933276"></a><a name="_Toc56934913"></a><a name="_Toc56935021"><span
style='mso-bookmark:_Toc56934913'><span style='mso-bookmark:_Toc56933276'><span
style='mso-bookmark:_Toc55184789'><span style='mso-bookmark:_Toc524755605'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Sections</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new section, click on the <b>Versions</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Versions browser, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Sections</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the version for which sections needs to be added.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new Section, click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Click Here</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>. This
opens up a new Section page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Entering a Section Name is mandatory. This is the title of
the section, which is User-defined.<span style="mso-spacerun: yes">  </span>It
can be Administrative Issues, Evaluation Criteria, Study Monitoring, etc.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once the Section Name has been defined, enter the contents
for that section and click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To modify existing Section information, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Section
Name</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> link displayed in the Sections page, make any necessary
changes and click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.3in'><a name="_Toc513645851"></a><a
name="_Toc524755606"><span style='mso-bookmark:_Toc513645851'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></a></p>

<p class=MsoBodyText style='margin-left:.3in'><span style='mso-bookmark:_Toc524755606'><span
style='mso-bookmark:_Toc513645851'><span style="mso-spacerun: yes">     
</span></span></span></p>

<span style='mso-bookmark:_Toc524755606'></span>

<h3><span style='mso-bookmark:_Toc513645851'><a name="_Toc524755607"></a><a
name="_Toc55184790"></a><a name="_Toc56933277"></a><a name="_Toc56934914"></a><a
name="_Toc56935022"><span style='mso-bookmark:_Toc56934914'><span
style='mso-bookmark:_Toc56933277'><span style='mso-bookmark:_Toc55184790'><span
style='mso-bookmark:_Toc524755607'><b><i><span style='font-size:10.0pt'>Appendix</span></i></b></span></span></span></span></a></span><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>The Appendix
is the part of your protocol that links to other web pages such as pre-existing
forms on your facilitys intranet or other web sites.<span style="mso-spacerun:
yes">  </span>You can also attach files from your own system to this study for
viewing by other authorized Users.<span style="mso-spacerun: yes">  </span><i>Please
note: Examples of files that can be attached are Text Files, Excel Files, PDF
and Word documents.<o:p></o:p></i></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new Appendix, click on the <b>Versions</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Versions browser, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Attachments</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the version for which Appendix needs to be added.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level1 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Appendix page, click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Click Here</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> under My
Links to add a new link and click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Click Here</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> under My
Files to upload a new File.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l123 level2 lfo58;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To view or to edit the details of the Link or File already
added, click on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Edit</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> link next to the record (Link or
Files) needed to be modified or simply to be viewed.</span></p>

<h3><a name="_Toc513645850"></a><a name="_Toc524755608"><span style='mso-bookmark:
_Toc513645850'><b><i style='mso-bidi-font-style:normal'><span style='font-size:
10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></span></a></h3>

<h3><span style='mso-bookmark:_Toc524755608'><span style='mso-bookmark:_Toc513645850'><a
name="_Toc55184791"></a><a name="_Toc56933278"></a><a name="_Toc56934915"></a><a
name="_Toc56935023"><span style='mso-bookmark:_Toc56934915'><span
style='mso-bookmark:_Toc56933278'><span style='mso-bookmark:_Toc55184791'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Calendar</span></i></b></span></span></span></a></span></span><span
style='mso-bookmark:_Toc524755608'><span style='mso-bookmark:_Toc513645850'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></span></span></h3>

<p class=MsoFooter style='tab-stops:.5in'><span style='mso-bookmark:_Toc524755608'><span
style='mso-bookmark:_Toc513645850'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></span></p>

<p class=MsoNormal><span style='mso-bookmark:_Toc524755608'><span
style='mso-bookmark:_Toc513645850'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Please refer to the <b>Protocol Calendar</b> section
for details.<o:p></o:p></span></span></span></p>

<p class=MsoNormal><span style='mso-bookmark:_Toc524755608'><span
style='mso-bookmark:_Toc513645850'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></span></p>

<h3><span style='mso-bookmark:_Toc524755608'><span style='mso-bookmark:_Toc513645850'><a
name="_Toc55184792"></a><a name="_Toc56933279"><span style='mso-bookmark:_Toc55184792'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></span></a></span></span></h3>

<h3><span style='mso-bookmark:_Toc524755608'><span style='mso-bookmark:_Toc513645850'><span
style='mso-bookmark:_Toc56933279'><span style='mso-bookmark:_Toc55184792'><a
name="_Toc56934916"></a><a name="_Toc56935024"><span style='mso-bookmark:_Toc56934916'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Study </span></i></b></span></a></span></span></span></span><span
style='mso-bookmark:_Toc513645850'><span style='mso-bookmark:_Toc56933279'><span
style='mso-bookmark:_Toc55184792'><span style='mso-bookmark:_Toc56935024'><span
style='mso-bookmark:_Toc56934916'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Status</span></i></b></span></span></span></span></span><span
style='mso-bookmark:_Toc513645850'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'> </span></i></b></span><b><i style='mso-bidi-font-style:
normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l68 level1 lfo59;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new Status, click on the <b>Study Status</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l68 level1 lfo59;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Study Status tab page, a default status of Not
Active is displayed. Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Add New Status</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l68 level1 lfo59;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter information as indicated.<span style="mso-spacerun:
yes">  </span>Mandatory fields are Study Status, Status Valid From (Date) and
Documented By.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l68 level1 lfo59;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Selecting a Study<ins cite="mailto:psahai"
datetime="2001-09-04T14:39"> Status </ins>option of <ins cite="mailto:psahai"
datetime="2001-09-04T14:39">Active</ins>/Enrolling <ins cite="mailto:psahai"
datetime="2001-09-04T14:39">allows </ins><ins cite="mailto:psahai"
datetime="2001-09-04T15:03">the </ins>User<ins cite="mailto:psahai"
datetime="2001-09-04T15:03"> to start enrolling </ins><ins cite="mailto:psahai"
datetime="2001-09-04T14:39"><%=LC.Pat_Patients_Lower%> to this study</ins><ins
cite="mailto:psahai" datetime="2001-09-10T11:27">.</ins><ins
cite="mailto:psahai" datetime="2001-09-04T14:39"><o:p></o:p></ins></span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;text-indent:-.3in;
mso-text-indent-alt:0in;line-height:150%;mso-list:l68 level1 lfo59;mso-list-change:
\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>You may view or modify a record at anytime by clicking on
the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Study Status</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;text-indent:0in;
line-height:150%;mso-list:l68 level1 lfo59;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To change the Study Start Date and/or Study End Date, click
on the link </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Change Dates</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l68 level2 lfo59;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To enter/edit the Study Start Date, a status of
Active/Enrolling should have been entered. </span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l68 level2 lfo59;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To enter/edit the Study End Date, a status of Permanent
Closure should have been entered.</span></p>

<h3><a name="_Toc524755609"><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></h3>

<h3><span style='mso-bookmark:_Toc524755609'><a name="_Toc55184793"></a><a
name="_Toc56933280"></a><a name="_Toc56934917"></a><a name="_Toc56935025"><span
style='mso-bookmark:_Toc56934917'><span style='mso-bookmark:_Toc56933280'><span
style='mso-bookmark:_Toc55184793'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Study Team</span></i></b></span></span></span></a></span><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Velos
eResearch provides the Study Data Manager with the option to create a team of
Users who have access to the protocol.<span style="mso-spacerun: yes"> 
</span>Access rights to specific functions regarding the protocol can be
controlled for each of the team members.<span style="mso-spacerun: yes"> 
</span>All members of the Study Team will have access to the same online
protocol.<span style="mso-spacerun: yes">  </span>This means that modifications
made by any member will be viewable by all other team members.</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Also
displayed on this screen will be a list of all Velos eResearch members who have
been given a copy of this study protocol.</p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l20 level1 lfo60;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To assign Study Team Users, specify criteria to view your
account User list. The User must be a member of your account to be available
for selection into the Study Team.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l20 level1 lfo60;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>From the list of available Users, select the Users that you
wish to include in your study team.<span style="mso-spacerun: yes"> 
</span>Define their role and click on </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l20 level1 lfo60;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The User has been added to your Study Team and will be
listed in the Study Team list. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l20 level2 lfo60;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Access Rights</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link to
assign specific rights to this User.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc524755610"></a><a name="_Toc55184794"></a><a
name="_Toc56933281"></a><a name="_Toc56934918"></a><a name="_Toc56935026"><span
style='mso-bookmark:_Toc56934918'><span style='mso-bookmark:_Toc56933281'><span
style='mso-bookmark:_Toc55184794'><span style='mso-bookmark:_Toc524755610'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>New Trial
Notifications</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Registered
Users of the Velos eResearch system can sign up to receive email notifications
for new trials that are added to our database and broadcast to the public. Once
notified about the new trial, they can then login to their account to view the
Study Summary that has been broadcast. This allows them to communicate with the
Study Data Manager to express their interest in participation and request more
information about the trial.<span style="mso-spacerun: yes">  </span>To send
notifications to members who have subscribed to your protocols therapeutic area:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l39 level1 lfo61;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Login to your account and the list of the five last accessed
protocols you have accessed will be shown on your My eResearch Homepage.<span
style="mso-spacerun: yes">   </span>Alternatively, you can access this list by
selecting </span><b><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#CC99FF'>Open</span></b><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> in the sub-menu under </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Manage
Protocols</span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> in your left-hand menu bar OR by clicking on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>View All Study
Protocols</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> link from the My eResearch Homepage.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l39 level1 lfo61;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the study that you wish to notify Users about.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l39 level1 lfo61;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once the study protocol is open, you will see a row of
navigation tabs across the top. Select the <b><ins cite="mailto:psahai"
datetime="2001-09-04T15:10">Broadcast</ins> <ins cite="mailto:psahai"
datetime="2001-09-04T15:10">/ </ins>Notify</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l39 level2 lfo61;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Notify Subscribers
Now!</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:
Arial'> button only once.<span style="mso-spacerun: yes">  </span>Automatic
email notifications will be sent to all subscribed members updating them about
the addition of a new trial and inviting them to login to the system and view
the Study Summary.</span></p>

<p class=MsoNormal style='margin-left:40.5pt;text-align:justify;line-height:
150%'><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
style="mso-spacerun: yes"> </span><br>
<b><i>Please note:</i></b><span style='mso-tab-count:1'>    </span>If the
Notify tab page is displaying the </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Release
Protocol to Public</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> <br>
button, then your study has not yet been broadcast and you cannot notify
subscribers unless the Study Summary has been made public.</span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184795"></a><a name="_Toc56933282"></a><a name="_Toc56934919"></a><a
name="_Toc56935027"><span style='mso-bookmark:_Toc56934919'><span
style='mso-bookmark:_Toc56933282'><span style='mso-bookmark:_Toc55184795'><b><i><span
style='font-family:Arial;color:#993366'>Sharing A Study Protocol</span></i></b></span></span></span></a><span
style='mso-bookmark:_Toc55184795'></span><span style='mso-bookmark:_Toc56933282'></span><span
style='mso-bookmark:_Toc56934919'></span><span style='mso-bookmark:_Toc56935027'></span><b><i><span
style='font-size:10.0pt;font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>You may
share your study protocols with other Users either by including them in your
Study Team so that you are working of<ins cite="mailto:psahai"
datetime="2001-09-04T15:11">f</ins> the same protocol or by giving them a
Protocol Copy.<span style="mso-spacerun: yes">  </span>Giving a Protocol Copy
to another User will allow them to access your protocol details.<span
style="mso-spacerun: yes">  </span>If you have granted them Modify rights to the
Protocol Copy, they will be able to edit these details.<span
style="mso-spacerun: yes">  </span>Any modifications made by them will not be
reflected in your protocol but will be limited to the copy they have. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>You can
share a protocol in two ways: <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l36 level1 lfo30;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>1)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>By
selecting Users in another account (External User), with whom you wish to share
your protocol.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.25in;text-align:justify;line-height:
150%'><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l181 level1 lfo62;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Open the protocol that you would like to share and go to the
Study Team tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l181 level1 lfo62;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>If the User to whom the Protocol copy to be given is a
member of another account, select the link </span><u><span style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Click here to give a Protocol
Copy to an External Account User</span></u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>. </span><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span style="mso-spacerun:
yes"> </span><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l181 level1 lfo62;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Identify the User by their unique User ID, which the
External User must have provided to you. Specify whether the Protocol Copy
should be given in a viewable or modifiable mode, and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.25in;text-align:justify;line-height:
150%'><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l36 level1 lfo30;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>2)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>By
broadcasting a Study Summary and receiving a request for more information, from
any interested registered Velos eResearch member, through your secure Message
Center.<span style="mso-spacerun: yes">  </span>It is your prerogative to grant
or deny the request. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.25in;text-align:justify;line-height:
150%'><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l36 level2 lfo30;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Login to your account and enter your My eResearch
Homepage.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l36 level2 lfo30;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Your Message Center will display all </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Unread</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> Messages
that are requests from other Users who would like to have a copy of your
protocol.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l36 level2 lfo30;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>You will see the name of the requester with a link to their
profile information, the study that they have requested and the type of
permission (View or Modify) that they would like to receive for their Protocol
Copy.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l36 level2 lfo30;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Name</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> of the
message sender to display their profile information, including contact
information.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l36 level2 lfo30;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial;text-transform:uppercase'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once you decide that you would like to share a Protocol Copy
with this User, you may grant them View or Modify rights to your Protocol Copy
by selecting the appropriate radio button and clicking on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> <span style='text-transform:uppercase'><o:p></o:p></span></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l36 level2 lfo30;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial;text-transform:uppercase'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>If you decide to deny the request for a Protocol Copy,
select the Deny radio button and </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;text-transform:
uppercase'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l36 level2 lfo30;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
text-transform:uppercase'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The requester will receive an acknowledgement of your
message, whether it was to grant access or deny it.<span style="mso-spacerun:
yes">  </span>If you have granted access, they will also receive a link to
access a copy of your protocol.</span><span style='text-transform:uppercase'><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><a name="_Toc513645853"><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></a></p>

<p class=MsoNormal><span style='mso-bookmark:_Toc513645853'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><span style='mso-bookmark:_Toc513645853'><a name="_Toc524755612"><b><i><span
style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></span></h2>

<h2><span style='mso-bookmark:_Toc513645853'><span style='mso-bookmark:_Toc524755612'><a
name="_Toc55184796"></a><a name="_Toc56933283"></a><a name="_Toc56934920"></a><a
name="_Toc56935028"><span style='mso-bookmark:_Toc56934920'><span
style='mso-bookmark:_Toc56933283'><span style='mso-bookmark:_Toc55184796'><b><i><span
style='font-family:Arial;color:#993366'>Accessing An Existing Protocol</span></i></b></span></span></span></a></span></span><span
style='mso-bookmark:_Toc513645853'><span style='mso-bookmark:_Toc524755612'><b><i><span
style='font-family:Arial;color:#993366'> <o:p></o:p></span></i></b></span></span></h2>

<p class=MsoNormal><span style='mso-bookmark:_Toc513645853'><span
style='mso-bookmark:_Toc524755612'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></span></p>

<span style='mso-bookmark:_Toc524755612'></span><span style='mso-bookmark:_Toc513645853'></span>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'>To open an existing study protocol:<o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l154 level1 lfo63;tab-stops:list .55in'><![if !supportLists]><span
style='mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Login to your
account and the list of the Five last accessed study protocols that you have
access to will be shown on your </span><span style='mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'>My eResearch Homepage.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l154 level1 lfo63;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Alternatively,
you can access this list of all the available protocols by selecting </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial;
color:#CC99FF'>Open</span></b><span style='mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'> in the </span><span style='mso-bidi-font-family:
Arial'>sub-menu under </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;mso-bidi-font-family:Arial;color:#CC99FF'>Manage Protocols</span></b><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'> </span><span
style='mso-bidi-font-family:Arial'>in your left-hand menu bar OR by clicking on
the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:10.0pt;
mso-bidi-font-family:Arial'>View All Study Protocols</span></u><span
style='mso-bidi-font-family:Arial'> link from the My eResearch Homepage.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l154 level1 lfo63;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Study
Number</span></u><span style='mso-bidi-font-family:Arial'> OR </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:10.0pt;mso-bidi-font-family:Arial'>Study
Status</span></u><span style='mso-bidi-font-family:Arial'> link respective to
the study protocol that you wish to open.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l154 level1 lfo63;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>If User
clicked on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:10.0pt;
mso-bidi-font-family:Arial'>Study Number</span></u><span style='mso-bidi-font-family:
Arial'> link this will open the Manage Protocols &gt;&gt; Summary page.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l154 level1 lfo63;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>If User
clicked on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:10.0pt;
mso-bidi-font-family:Arial'>Study Status</span></u><span style='mso-bidi-font-family:
Arial'> link this will open the Manage Protocols &gt;&gt; Study Status page.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h1><span style='font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></h1>

<h1><a name="_Toc55184797"></a><a name="_Toc56933284"></a><a name="_Toc56934921"></a><a
name="_Toc56935029"><span style='mso-bookmark:_Toc56934921'><span
style='mso-bookmark:_Toc56933284'><span style='mso-bookmark:_Toc55184797'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Protocol Calendar</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section describes the creation of a Protocol Calendar Template and its
association to a specific study.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc524755614"></a><a name="_Toc55184798"></a><a
name="_Toc56933285"></a><a name="_Toc56934922"></a><a name="_Toc56935030"><span
style='mso-bookmark:_Toc56934922'><span style='mso-bookmark:_Toc56933285'><span
style='mso-bookmark:_Toc55184798'><span style='mso-bookmark:_Toc524755614'><b><i><span
style='font-family:Arial;color:#993366'>Associating A Calendar To A Study</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'>The Protocol Calendar is the framework on
which the generation of <%=LC.Pat_Patient_Lower%> schedules and subsequent workflow management is
based.<span style="mso-spacerun: yes">  </span>These functionalities can be
executed only when a Protocol Calendar is associated to a study.<span
style="mso-spacerun: yes">  </span>Once a calendar is associated to a study, it
will appear in the list of options for selecting a Protocol Calendar on which a
<%=LC.Pat_Patient_Lower%>s schedule will be generated.<span style="mso-spacerun: yes">  </span><o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc524755615"></a><a name="_Toc55184799"></a><a
name="_Toc56933286"></a><a name="_Toc56934923"></a><a name="_Toc56935031"><span
style='mso-bookmark:_Toc56934923'><span style='mso-bookmark:_Toc56933286'><span
style='mso-bookmark:_Toc55184799'><span style='mso-bookmark:_Toc524755615'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Selecting A
Calendar</span></i></b></span></span></span></span></a><b><i style='mso-bidi-font-style:
normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'>To select a calendar and associate it to a
study:<o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l154 level1 lfo63;tab-stops:list .55in'><![if !supportLists]><span
style='mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Login to your
account and the list of the Five last accessed study protocols that you have
access to will be shown on your </span><span style='mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'>My eResearch Homepage. <o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l154 level1 lfo63;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Alternatively,
you can access this list of all the available protocols by selecting </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial;
color:#CC99FF'>Open</span></b><span style='mso-bidi-font-family:Arial'><span
style="mso-spacerun: yes">  </span>in the sub-menu under </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial;
color:#CC99FF'>Manage Protocols</span></b><span style='mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'> </span><span style='mso-bidi-font-family:Arial'>in
your left-hand menu bar OR by clicking on the </span><u><span style='font-size:
9.0pt;mso-bidi-font-size:10.0pt;mso-bidi-font-family:Arial'>View All Study
Protocols</span></u><span style='mso-bidi-font-family:Arial'> link from the My
eResearch Homepage.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l15 level1 lfo64;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Study
Number</span></u><span style='mso-bidi-font-family:Arial'> OR </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:10.0pt;mso-bidi-font-family:Arial'>Study
Status</span></u><span style='mso-bidi-font-family:Arial'> link respective to
the study protocol that you wish to open and select the </span><b><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Study Setup</span></b><span
style='mso-bidi-font-family:Arial'> tab from across the top.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l15 level1 lfo64;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>The screen
will display the list of calendars already associated to the study.<span
style="mso-spacerun: yes">  </span>You may associate a new calendar by clicking
on the link </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'>Select a Calendar from your Library</span></u><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>.</span><span
style='mso-bidi-font-family:Arial'><o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l15 level1 lfo64;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>This link will
open the </span><span style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:
Arial'>Protocol Calendar Library</span><span style='mso-bidi-font-family:Arial'>.<span
style="mso-spacerun: yes">  </span>You may select one of the existing Calendars
to associate to your study by clicking on the </span><u><span style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Select</span></u><span
style='mso-bidi-font-family:Arial'> link respective to the Calendar that you
wish to select, or create a new calendar. <o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l15 level1 lfo64;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>To create a
New Calendar, see the section on </span><span style='mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'>Create a New Calendar.</span><span
style='mso-bidi-font-family:Arial'><o:p></o:p></span></p>

<h3><a name="_Toc524755616"><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></a></h3>

<h3><span style='mso-bookmark:_Toc524755616'><i style='mso-bidi-font-style:
normal'><span style='font-size:10.0pt'><br>
<a name="_Toc55184800"></a><a name="_Toc56933287"></a><a name="_Toc56934924"></a><a
name="_Toc56935032"><span style='mso-bookmark:_Toc56934924'><span
style='mso-bookmark:_Toc56933287'><span style='mso-bookmark:_Toc55184800'><b>Customizing
The Calendar For A Study</b></span></span></span></a></span></i></span><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'>Once a Calendar has been selected from the </span><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Protocol Calendar
Library</span><span style='mso-bidi-font-family:Arial'> and associated to a
study, it can be customized to reflect the studys specific requirements.<span
style="mso-spacerun: yes">  </span>The </span><span style='mso-bidi-font-size:
12.0pt;mso-bidi-font-family:Arial'>Events </span><span style='mso-bidi-font-family:
Arial'>(<%=LC.Pat_Patient_Lower%> visits) can be associated to details such as forms, cost and
resources, CRF details relevant to the study.<span style="mso-spacerun: yes"> 
</span>To customize the Calendar:<o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level1 lfo65;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Open the
desired Protocol and select the </span><b><span style='mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'>Study Setup</span></b><span style='mso-bidi-font-family:
Arial'> tab from across the top.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level1 lfo65;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>The screen
will display the list of Calendars already associated to the study.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level1 lfo65;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>All fields can
be modified while the status of your Calendar is selected to the </span><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Work in Progress</span><span
style='mso-bidi-font-family:Arial'> option.<span style="mso-spacerun: yes"> 
</span><b>Note</b>:<span style="mso-spacerun: yes">  </span>Once the status is
changed to </span><span style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:
Arial'>Freeze,</span><span style='mso-bidi-font-family:Arial'> no further
modifications will be allowed.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level1 lfo65;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Select the
Calendar that you wish to customize by clicking on the Calendar Name link.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level1 lfo65;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>The Calendar
will open to display information in the following sequence:<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:1.0in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level2 lfo65;tab-stops:list 1.0in'><![if !supportLists]><span
style='font-family:"Courier New";mso-bidi-font-family:Arial'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><b><span style='mso-bidi-font-family:Arial'>Define the
Calendar</span></b><span style='mso-bidi-font-family:Arial'>: Review/modify
description, duration and status.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:1.0in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level2 lfo65;tab-stops:list 1.0in'><![if !supportLists]><span
style='font-family:"Courier New";mso-bidi-font-family:Arial'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><b><span style='mso-bidi-font-family:Arial'>Select
Events</span></b><span style='mso-bidi-font-family:Arial'>: Review/modify
events to be included in the calendar.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:1.0in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level2 lfo65;tab-stops:list 1.0in'><![if !supportLists]><span
style='font-family:"Courier New";mso-bidi-font-family:Arial'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><b><span style='mso-bidi-font-family:Arial'>Schedule
Events:</span></b><span style='mso-bidi-font-family:Arial'> Review/modify the
days that an event is scheduled to occur.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:1.0in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l161 level2 lfo65;tab-stops:list 1.0in'><![if !supportLists]><span
style='font-family:"Courier New";mso-bidi-font-family:Arial'>o<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><b><span style='mso-bidi-font-family:Arial'>Customize
Event Details</span></b><span style='mso-bidi-font-family:Arial'>:
Review/modify the details of each event such as associated forms, cost,
resources, CRF details or notifications.<o:p></o:p></span></p>

<p class=MsoBodyText2><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc524755617"></a><a name="_Toc55184801"></a><a
name="_Toc56933288"></a><a name="_Toc56934925"></a><a name="_Toc56935033"><span
style='mso-bookmark:_Toc56934925'><span style='mso-bookmark:_Toc56933288'><span
style='mso-bookmark:_Toc55184801'><span style='mso-bookmark:_Toc524755617'><b><i><span
style='font-family:Arial;color:#993366'>Copying An Existing Calendar</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'>All Protocol Calendars created by a User are
stored in the Users </span><span style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:
Arial'>Protocol Calendar Library</span><span style='mso-bidi-font-family:Arial'>.<span
style="mso-spacerun: yes">  </span>The existing Calendars can be reused and
associated to multiple studies with minor modifications.<span
style="mso-spacerun: yes">  </span>To create a new Calendar in the </span><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Library</span><span
style='mso-bidi-font-family:Arial'>, a User has the option to copy an existing
Calendar or create a New Calendar.<span style="mso-spacerun: yes">  </span>To
copy an existing Calendar, follow the steps below:<o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l143 level1 lfo123;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>S<ins cite="mailto:psahai"
     datetime="2001-09-04T15:45">e</ins>lect<ins cite="mailto:psahai"
     datetime="2001-09-04T15:45"> </ins></span><b><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
     cite="mailto:psahai" datetime="2001-09-04T15:45">Library</ins></span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
     cite="mailto:psahai" datetime="2001-09-04T15:45"> from your </ins>left-hand<ins
     cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>menu<ins
     cite="mailto:psahai" datetime="2001-09-04T15:45"> bar and </ins>click on<ins
     cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins></span><b><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     color:#CC99FF'><ins cite="mailto:psahai" datetime="2001-09-04T15:45">Protocol</ins>
     <ins cite="mailto:psahai" datetime="2001-09-04T15:45">Calendar</ins></span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
     cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>in the sub-menu.<o:p></o:p></span></li>
</ul>

<p class=MsoBodyText style='margin-left:.5in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l143 level1 lfo123;tab-stops:list .5in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Click on the
link </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'>Create a New Calendar</span></u><span
style='mso-bidi-font-family:Arial'>. This opens up the first step of the </span><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Wizard<span
style="mso-spacerun: yes">   </span></span><span style='mso-bidi-font-family:
Arial'>designed to help you create a Calendar<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.5in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l143 level1 lfo123;tab-stops:list .5in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Copy
an Existing Protocol Calendar</span></u><span style='mso-bidi-font-family:Arial'>
at the top of the screen, select the Calendar that you wish to copy and save it
under a new name.<o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.5in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l143 level1 lfo123;tab-stops:list .5in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>Once the
Calendar has been successfully copied, it will be available in your </span><span
style='mso-bidi-font-size:12.0pt;mso-bidi-font-family:Arial'>Library.</span><span
style='mso-bidi-font-family:Arial'><o:p></o:p></span></p>

<p class=MsoBodyText style='margin-left:.55in;text-align:justify;text-indent:
-.3in;line-height:150%;mso-list:l101 level1 lfo182;tab-stops:list .5in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='mso-bidi-font-family:Arial'>You can now
open the copied Calendar and modify or customize it according to your
needs.<span style="mso-spacerun: yes">  </span>Once calendar status is set to
Freeze it becomes available for association to a study.</span><span
style='font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:"Times New Roman"'><o:p></o:p></span></p>

<h2><span style='font-size:10.0pt;font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></h2>

<p class=MsoToc3><span style='font-size:12.0pt;text-transform:uppercase'><br
style='mso-special-character:line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc524755618"></a><a name="_Toc55184802"></a><a
name="_Toc56933289"></a><a name="_Toc56934926"></a><a name="_Toc56935034"><span
style='mso-bookmark:_Toc56934926'><span style='mso-bookmark:_Toc56933289'><span
style='mso-bookmark:_Toc55184802'><span style='mso-bookmark:_Toc524755618'><b><i><span
style='font-family:Arial;color:#993366'>Creating A New Calendar</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>All Protocol
Calendars created by a User are stored in the Users Protocol Calendar
Library.<span style="mso-spacerun: yes">   </span>The existing Calendars can be
reused and associated to multiple studies with minor modifications.<span
style="mso-spacerun: yes">  </span>To create a new Calendar: </p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l77 level1 lfo67;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select<ins cite="mailto:psahai" datetime="2001-09-04T15:45">
</ins></span><b><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#CC99FF'><ins cite="mailto:psahai"
datetime="2001-09-04T15:45">Library</ins></span></b><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> from your </ins>left-hand<ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> </ins>menu<ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> bar and </ins>click on </span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45">Protocol Calendar</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>in the sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l77 level1 lfo67;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the link </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Create a New Calendar</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>.<span
style="mso-spacerun: yes">  </span>This opens up the first step of the Wizard
designed to help you create a Calendar.<o:p></o:p></span></p>

<p class=MsoToc3><span style='font-size:12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoToc3><span style='font-size:12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc524755619"></a><a name="_Toc55184803"></a><a
name="_Toc56933290"></a><a name="_Toc56934927"></a><a name="_Toc56935035"><span
style='mso-bookmark:_Toc56934927'><span style='mso-bookmark:_Toc56933290'><span
style='mso-bookmark:_Toc55184803'><span style='mso-bookmark:_Toc524755619'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Step 1:
Define the Calendar</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'> <o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyTextIndent2 style='margin-left:.55in;text-align:justify;
text-indent:-.25in;line-height:150%;mso-list:l65 level1 lfo68;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>Enter a name for your Protocol Calendar, a brief
description and the duration of the Protocol. </p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l65 level1 lfo68;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select an option for your Calendar status: <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:1.0in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l65 level2 lfo68;tab-stops:list 1.0in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:"Courier New";
mso-bidi-font-family:Arial'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Work in Progress: Protocol Calendar is being created or can
undergo modifications.<span style="mso-spacerun: yes">  </span>The Calendar
cannot be associated to a study and <%=LC.Pat_Patient_Lower%> schedules cannot be generated while
Calendar is in this mode.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:1.0in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l65 level2 lfo68;tab-stops:list 1.0in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:"Courier New";
mso-bidi-font-family:Arial'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Freeze: Once the Freeze status is selected, the Calendar
can be associated to a study and <%=LC.Pat_Patient_Lower%> schedules can be generated.<span
style="mso-spacerun: yes">  </span>However, no further modifications can be
made to the Calendar.<span style="mso-spacerun: yes">  </span><b>Note:</b> To
make modifications: The Protocol Calendar will need to be copied and saved
under another name.<o:p></o:p></span></p>

<p class=MsoBodyTextIndent2 style='margin-left:.55in;text-align:justify;
text-indent:-.25in;line-height:150%;mso-list:l151 level1 lfo181;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol;mso-bidi-font-family:Tahoma'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>Click on <span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black"'>Next</span>.<span style="mso-spacerun: yes"> 
</span>This saves your data and takes you to the next step in the Calendar
Creation Wizard.<span style='font-size:12.0pt;font-family:Impact;mso-bidi-font-family:
Tahoma'><o:p></o:p></span></p>

<p class=MsoBodyTextIndent2 style='margin-left:.3in'><span style='font-size:
12.0pt;font-family:Impact;mso-bidi-font-family:Tahoma'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc524755620"></a><a name="_Toc55184804"></a><a
name="_Toc56933291"></a><a name="_Toc56934928"></a><a name="_Toc56935036"><span
style='mso-bookmark:_Toc56934928'><span style='mso-bookmark:_Toc56933291'><span
style='mso-bookmark:_Toc55184804'><span style='mso-bookmark:_Toc524755620'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Step 2:
Select Events</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'> <o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyTextIndent2 style='margin-left:.55in;text-align:justify;
text-indent:-.25in;line-height:150%;mso-list:l133 level1 lfo69;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>Step 2 displays <ins cite="mailto:psahai"
datetime="2001-09-04T15:18">a link to </ins>your Event Library and the Events
(<%=LC.Pat_Patient_Lower%> visits) listed in it.<span style="mso-spacerun: yes">  </span>Select
the events to be included in your Protocol.<span style="mso-spacerun: yes">  
</span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l133 level1 lfo69;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In case you do not find an event listed in the Library, you
can add a New Event. Add a new category or go to the category that you wish to
list the Event under.<span style="mso-spacerun: yes">  </span>Click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Add a New
Event</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> link.<span style="mso-spacerun: yes">  </span>Enter Event
details as described under the section Event Library.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l133 level1 lfo69;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>It is advisable to check the Event details before selecting
it for the Protocol by clicking on the Event name.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l41 level1 lfo180;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Tahoma'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once you have selected the Events to be included in your
Protocol, click on the </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Next</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> button.
This saves your data and takes you to the next step in the Calendar Creation
Wizard.</span><span style='font-family:Impact;mso-bidi-font-family:Tahoma'><o:p></o:p></span></p>

<h3><a name="_Toc524755621"><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></h3>

<h3><span style='mso-bookmark:_Toc524755621'><a name="_Toc55184805"></a><a
name="_Toc56933292"></a><a name="_Toc56934929"></a><a name="_Toc56935037"><span
style='mso-bookmark:_Toc56934929'><span style='mso-bookmark:_Toc56933292'><span
style='mso-bookmark:_Toc55184805'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Step 3: Schedule Events</span></i></b></span></span></span></a></span><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'> <o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l103 level1 lfo70;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Step 3 displays your selected Events (<%=LC.Pat_Patient_Lower%> visits) and
the duration of your Protocol in terms of days and weeks.<ins
cite="mailto:psahai" datetime="2001-09-04T15:19"><o:p></o:p></ins></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l103 level1 lfo70;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T15:19">You
can specify criteria to create a view of your choi</ins><ins
cite="mailto:psahai" datetime="2001-09-04T15:20">ce. Select </ins><b>D<ins
cite="mailto:psahai" datetime="2001-09-04T15:20">ays</ins></b><ins
cite="mailto:psahai" datetime="2001-09-04T15:20">, </ins><b>W<ins
cite="mailto:psahai" datetime="2001-09-04T15:20">eeks</ins></b><ins
cite="mailto:psahai" datetime="2001-09-04T15:20"> <b>or </b></ins><b>M<ins
cite="mailto:psahai" datetime="2001-09-04T15:20">onths</ins></b><ins
cite="mailto:psahai" datetime="2001-09-04T15:20"> depending on the time
intervals that you want to be displayed and define the number of the intervals
to appear on a page</ins>.<ins cite="mailto:psahai" datetime="2001-09-04T15:19">
<o:p></o:p></ins></span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l103 level1 lfo70;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Mark the checkbox indicating the day/week that a certain
event is to be performed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l177 level1 lfo179;
mso-list-change:- psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once all the events have been placed in the appropriate
interval slots, click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Next</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to save
your data and take you to the next step.</span> </p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc524755622"></a><a name="_Toc55184806"></a><a
name="_Toc56933293"></a><a name="_Toc56934930"></a><a name="_Toc56935038"><span
style='mso-bookmark:_Toc56934930'><span style='mso-bookmark:_Toc56933293'><span
style='mso-bookmark:_Toc55184806'><span style='mso-bookmark:_Toc524755622'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Step 4:
Customize Event Details</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l121 level1 lfo71;
mso-list-change:- psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Step 4 displays the same data from the previous step,
however the Events (<%=LC.Pat_Patient_Lower%> visits) are editable in this step.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l121 level1 lfo71;
mso-list-change:- psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Therefore, each Event can be customized with respect to such
details as associated cost, resources, forms, CRF Details or messages.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l112 level1 lfo178;
mso-list-change:- psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The Events may be customized at this point in the Library or
may be customized after it has been associated with a study.<span
style="mso-spacerun: yes">  </span>If customized after association with a
study, then the specific details will be available only to members with access
to the Study Protocol. </span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc524755623"><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></h2>

<h2><span style='mso-bookmark:_Toc524755623'><a name="_Toc55184807"></a><a
name="_Toc56933294"></a><a name="_Toc56934931"></a><a name="_Toc56935039"><span
style='mso-bookmark:_Toc56934931'><span style='mso-bookmark:_Toc56933294'><span
style='mso-bookmark:_Toc55184807'><b><i><span style='font-family:Arial;
color:#993366'>Protocol Calendar Library</span></i></b></span></span></span></a></span><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>The Protocol
Calendar Library is a powerful feature that enables you to reuse your basic
components as often as needed.<span style="mso-spacerun: yes">  </span>Once a
Protocol Calendar has been created in your Library, you can copy it to create a
new Calendar with minor modifications or you can select it, as is, to be
associated to a study.<span style="mso-spacerun: yes">  </span>This saves time
that otherwise would be spent recreating the same components repeatedly.<span
style="mso-spacerun: yes">  </span>To access the Protocol Calendar Library:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l162 level1 lfo72;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select<ins cite="mailto:psahai" datetime="2001-09-04T15:45">
</ins></span><b><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#CC99FF'><ins cite="mailto:psahai"
datetime="2001-09-04T15:45">Library</ins></span></b><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> from your </ins>left-hand<ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> </ins>menu<ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> bar and </ins>click on<ins cite="mailto:psahai"
datetime="2001-09-04T15:45"> </ins></span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45">Protocol Calendar</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T15:45"> </ins>in the sub-menu. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l43 level1 lfo177;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T15:47">You
will be able to view a list of all the calendars saved in your Account Library.
You can create new calendars or make changes to existing calendars that are in
a </ins><ins cite="mailto:psahai" datetime="2001-09-04T15:49">Work in Progress
mode</ins><ins cite="mailto:psahai" datetime="2001-09-04T15:50">. </ins><span
style="mso-spacerun: yes"> </span><ins cite="mailto:psahai"
datetime="2001-09-04T15:50">To associate a calendar to a specific study, open
the </ins>preferred <ins cite="mailto:psahai" datetime="2001-09-04T15:50">study
protocol and go to the Calendar tab.</ins></span><ins cite="mailto:psahai"
datetime="2001-09-04T15:44"><o:p></o:p></ins></p>

<p class=MsoNormal style='text-indent:0in;mso-text-indent-alt:0in;mso-list:
none;mso-list-ins:psahai 20010904T1544'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc524755624"></a><a name="_Toc55184808"></a><a
name="_Toc56933295"></a><a name="_Toc56934932"></a><a name="_Toc56935040"><span
style='mso-bookmark:_Toc56934932'><span style='mso-bookmark:_Toc56933295'><span
style='mso-bookmark:_Toc55184808'><span style='mso-bookmark:_Toc524755624'><b><i><span
style='font-family:Arial;color:#993366'>Event Library</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Similar to
the Protocol Calendar Library, the Event Library is a powerful feature that
enables you to reuse your basic components as often as needed. Events (<%=LC.Pat_Patient_Lower%>
visits) that have been saved in the Library can be reused as the basic building
blocks for all your calendars.<span style="mso-spacerun: yes">  </span>New
Events can be added as needed.<span style="mso-spacerun: yes">  </span>To
access your Event Library:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l88 level1 lfo73;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select<ins cite="mailto:psahai" datetime="2001-09-04T15:56">
</ins></span><b><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#CC99FF'><ins cite="mailto:psahai"
datetime="2001-09-04T15:56">Library</ins></span></b><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins cite="mailto:psahai"
datetime="2001-09-04T15:56"> from your </ins>left-hand<ins cite="mailto:psahai"
datetime="2001-09-04T15:56"> </ins>menu<ins cite="mailto:psahai"
datetime="2001-09-04T15:56"> bar and </ins>click on<ins cite="mailto:psahai"
datetime="2001-09-04T15:56"> </ins></span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T15:56">Event </ins><ins
cite="mailto:psahai" datetime="2001-09-04T15:57">Library</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T15:57"> </ins>in the sub-menu. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l94 level1 lfo176;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T15:56">You
will be able to view a list of all the </ins>E<ins cite="mailto:psahai"
datetime="2001-09-04T15:57">vents</ins><ins cite="mailto:psahai"
datetime="2001-09-04T15:56"> saved in your Account Library. You can</ins> <ins
cite="mailto:psahai" datetime="2001-09-04T15:56">create </ins>N<ins
cite="mailto:psahai" datetime="2001-09-04T15:56">ew </ins>E<ins
cite="mailto:psahai" datetime="2001-09-04T15:57">vents</ins><ins
cite="mailto:psahai" datetime="2001-09-04T15:56"> or make changes to existing </ins>E<ins
cite="mailto:psahai" datetime="2001-09-04T15:57">vents</ins><ins
cite="mailto:psahai" datetime="2001-09-04T15:56">.</ins></span><ins
cite="mailto:psahai" datetime="2001-09-04T15:56"><o:p></o:p></ins></p>

<h3><a name="_Toc524755625"><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></a></h3>

<h3><span style='mso-bookmark:_Toc524755625'><a name="_Toc55184809"></a><a
name="_Toc56933296"></a><a name="_Toc56934933"></a><a name="_Toc56935041"><span
style='mso-bookmark:_Toc56934933'><span style='mso-bookmark:_Toc56933296'><span
style='mso-bookmark:_Toc55184809'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Creating A New Category</span></i></b></span></span></span></a></span><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l71 level1 lfo74;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>At the top right-hand corner of the Event Library screen,
there is a link for </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Add New Category</span></u><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>. This link will take you
to the New Category screen.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l71 level1 lfo74;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter the category name and description and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;line-height:
150%'><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Your
new category will be added to the Library.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc524755626"><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></h3>

<h3><span style='mso-bookmark:_Toc524755626'><a name="_Toc55184810"></a><a
name="_Toc56933297"></a><a name="_Toc56934934"></a><a name="_Toc56935042"><span
style='mso-bookmark:_Toc56934934'><span style='mso-bookmark:_Toc56933297'><span
style='mso-bookmark:_Toc55184810'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Creating A New Event</span></i></b></span></span></span></a></span><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoToc3><span style='font-size:12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l122 level1 lfo75;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add a new Event to your library, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Add New
Event</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'> link. This will take you to the New Event Details page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l122 level1 lfo75;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Event Details page, select the category that you wish
to place the Event under.<span style="mso-spacerun: yes">  </span>If there is
no relevant category, you can create a new one.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l165 level1 lfo175;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter as much information as you would like to associate to
the Event. You can describe the Event in terms of duration, fuzzy period,
associated cost, resources, forms, messages and CRF details</span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l122 level1 lfo75;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> and your
new event will be added to the Library.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h3><a name="_Toc524755627"><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><br>
</span></i></a><a name="_Toc55184811"></a><a name="_Toc56933298"></a><a
name="_Toc56934935"></a><a name="_Toc56935043"><span style='mso-bookmark:_Toc56934935'><span
style='mso-bookmark:_Toc56933298'><span style='mso-bookmark:_Toc55184811'><span
style='mso-bookmark:_Toc524755627'><b><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Customizing Event Details</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l190 level1 lfo76;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To customize an Event to suit your studys specific needs,
open the required Protocol Calendar and proceed to Step 4 (Customize Event
Details) or go to the Event Library.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l190 level1 lfo76;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the Event that you wish to customize and click on it
to open the Event Details page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l190 level1 lfo76;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter as much information as you would like to associate to
the Event.<span style="mso-spacerun: yes">  </span>You can describe the event
in terms of duration, fuzzy period, associated cost, resources, forms, messages
and CRF details.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l7 level1 lfo174;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> button to
save your information.<span style="mso-spacerun: yes">  </span>This information
will be submitted to the <%=LC.Pat_Patient_Lower%> schedules whenever a <%=LC.Pat_Patient_Lower%> is enrolled to
this protocol.</span></p>

<p class=MsoToc3><span style='font-size:12.0pt;text-transform:uppercase'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h1><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></h1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184812"></a><a name="_Toc56933299"></a><a name="_Toc56934936"></a><a
name="_Toc56935044"><span style='mso-bookmark:_Toc56934936'><span
style='mso-bookmark:_Toc56933299'><span style='mso-bookmark:_Toc55184812'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'><%=LC.Pat_Patient%> Management</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section provides details on creating <%=LC.Pat_Patient_Lower%> records, enrolling <%=LC.Pat_Patients_Lower%> to a
study and managing their study-related information.<o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc524755629"></a><a name="_Toc55184813"></a><a
name="_Toc56933300"></a><a name="_Toc56934937"></a><a name="_Toc56935045"><span
style='mso-bookmark:_Toc56934937'><span style='mso-bookmark:_Toc56933300'><span
style='mso-bookmark:_Toc55184813'><span style='mso-bookmark:_Toc524755629'><b><i><span
style='font-family:Arial;color:#993366'>Registering A <%=LC.Pat_Patient%></span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>You can
register <%=LC.Pat_Patients_Lower%> online and track their study enrollment and schedules easily
and efficiently. The network feature provides you with the ability to
coordinate across multiple sites. You will have access to these pages only if
you have been assigned rights to Manage <%=LC.Pat_Patient%> while assigning rights to the
study team members. To register a <%=LC.Pat_Patient_Lower%>:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l61 level1 lfo77;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage <span class=msoDel><del
cite="mailto:psahai" datetime="2001-09-04T16:00">Protocols </del></span><ins
cite="mailto:psahai" datetime="2001-09-04T16:00"><%=LC.Pat_Patients%></ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T16:00"> </ins>from your left-hand
menu bar and click on </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'><span class=msoDel><del
cite="mailto:psahai" datetime="2001-09-04T16:00"><span style="mso-spacerun:
yes"> </span>Open</del></span><ins cite="mailto:psahai"
datetime="2001-09-04T16:00">New</ins> </span></b><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>in the</span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>
</span></b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:
Arial'>sub-menu<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.2in;
line-height:150%;mso-list:l85 level1 lfo25;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:01">·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></del></span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:01">A list of the protocols that you have access to
will be displayed along with links to Protocol Details and Manage <%=LC.Pat_Patients%>.
Select the Manage <%=LC.Pat_Patients%> option.</del></span><span class=msoDel><del
cite="mailto:psahai" datetime="2001-09-04T16:01"><o:p></o:p></del></span></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.5in;
mso-text-indent-alt:-.2in;line-height:150%;mso-list:l85 level1 lfo25;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:01">·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></del></span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:01">A list of the <%=LC.Pat_Patients_Lower%> currently enrolled in the
study will be displayed on the screen. There is also a link to Register a
<%=LC.Pat_Patient%> at the top. To register a new <%=LC.Pat_Patient_Lower%> into the system, click on this
link.</del></span><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:01"><o:p></o:p></del></span></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l61 level1 lfo77;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter relevant information in the registration page provided
and click on </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l61 level1 lfo77;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>When selecting the Organization for registration details,
please note that only those organizations for which login User has New right
will be populated in the Organization dropdown.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l61 level1 lfo77;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Once a <%=LC.Pat_Patient_Lower%> is registered into the system, the <%=LC.Pat_Patient_Lower%>
will now be available for enrollment in a study.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l61 level1 lfo77;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><%=LC.Pat_Patient%> will only be visible/available for enrollment to
Users who have the access rights for the organization selected in the <%=LC.Pat_Patient%>
Demographics page. Either the selected organization should be the Users
primary organization or child to the Users primary organization or User has
been granted access to the organization using the link Multiple Organization
Access in the User Details page. <o:p></o:p></span></p>

<p class=MsoNormal align=center style='text-align:center'><a
name="_Toc524755630"><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:05">Search a <%=LC.Pat_Patient%></del></span></a><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05"><o:p></o:p></del></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><span class=msoDel><del
cite="mailto:psahai" datetime="2001-09-04T16:05">To search <%=LC.Pat_Patients_Lower%> currently
registered to your organization, follow these steps:</del></span><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05"><o:p></o:p></del></span></span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.2in;mso-list:l85 level1 lfo25;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05">·</del></span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:05">Open the list of protocols that you have access to
and select the Manage <%=LC.Pat_Patients%> link next to the specific protocol. </del></span><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05"><o:p></o:p></del></span></span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.5in;mso-text-indent-alt:
-.2in;mso-list:l85 level1 lfo25;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-family:Symbol'><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05">·</del></span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:05">A list of the <%=LC.Pat_Patients_Lower%> currently enrolled in the
study will be displayed on the screen. There is also a link to Search a <%=LC.Pat_Patient%>
at the top. Clicking on this link will take you to the <%=LC.Pat_Patient%> Search screen.</del></span></span><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05"><o:p></o:p></del></span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.5in;mso-text-indent-alt:
-.2in;mso-list:l85 level1 lfo25;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-family:Symbol'><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05">·</del></span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:05">Enter a <%=LC.Pat_Patient%> ID or select a search option such
as Active or Inactive <%=LC.Pat_Patients_Lower%> and click on Go.</del></span></span><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05"><o:p></o:p></del></span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:-.5in;mso-text-indent-alt:
-.2in;mso-list:l85 level1 lfo25;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-family:Symbol'><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05">·</del></span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;mso-bidi-font-family:
"Times New Roman"'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:05">A list of <%=LC.Pat_Patients_Lower%> matching your search criteria
will be displayed. Select any <%=LC.Pat_Patient_Lower%> ID to see further details. How much
information you are able to see will depend on the rights assigned to you.</del></span></span><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:05"><o:p></o:p></del></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc524755631"><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></a></h2>

<h2><span style='mso-bookmark:_Toc524755631'><b><i><span style='font-family:
Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></span></h2>

<h2><span style='mso-bookmark:_Toc524755631'><b><i><span style='font-family:
Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></span></h2>

<h2><span style='mso-bookmark:_Toc524755631'><a name="_Toc55184814"></a><a
name="_Toc56933301"></a><a name="_Toc56934938"></a><a name="_Toc56935046"><span
style='mso-bookmark:_Toc56934938'><span style='mso-bookmark:_Toc56933301'><span
style='mso-bookmark:_Toc55184814'><b><i><span style='font-family:Arial;
color:#993366'>Enrolling A <%=LC.Pat_Patient%> To A Study</span></i></b></span></span></span></a></span><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Once a
<%=LC.Pat_Patient_Lower%> has been registered into the Velos eResearch system, the <%=LC.Pat_Patient_Lower%> is now
available for enrolling in a study.<span style="mso-spacerun: yes">  </span>To enroll
a <%=LC.Pat_Patient_Lower%> to a specific study, you must have access rights to Manage
<%=LC.Pat_Patients%>.<span style="mso-spacerun: yes">  </span>These access rights are
assigned through the Study Team tab of a protocol.<span style="mso-spacerun:
yes">  </span>To enroll a <%=LC.Pat_Patient_Lower%>, follow these steps:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l144 level1 lfo78;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:06">Open the list of protocols that you have access to
and select </del></span>Select<ins cite="mailto:psahai"
datetime="2001-09-04T16:06"> </ins></span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Manage <%=LC.Pat_Patients%></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> <span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:06">link</del></span>from
your<ins cite="mailto:psahai" datetime="2001-09-04T16:06"> </ins>left-hand<ins
cite="mailto:psahai" datetime="2001-09-04T16:06"> </ins>menu<ins
cite="mailto:psahai" datetime="2001-09-04T16:06"> bar and </ins>click on<ins
cite="mailto:psahai" datetime="2001-09-04T16:06"> </ins></span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T16:06">Open</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T16:06"> </ins>in the sub-menu. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l144 level1 lfo78;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of all studies that are active/enrolling <%=LC.Pat_Patients_Lower%>
will be displayed. Along with the study number, a summary of information such
as date of enrolling first <%=LC.Pat_Patient_Lower%>, total <%=LC.Pat_Patients_Lower%> enrolled and a breakdown of
<%=LC.Pat_Patients_Lower%> that are active, completed, dropped out, suspended or terminated is
also displayed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l144 level1 lfo78;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the appropriate </span><u><span style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>View</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the study in which <%=LC.Pat_Patient_Lower%> needs to be enrolled.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l144 level1 lfo78;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of all <%=LC.Pat_Patients_Lower%> currently enrolled in <span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:07">the study</del></span>the
<ins cite="mailto:psahai" datetime="2001-09-04T16:07">stud</ins>y will be
displayed on the screen.<span style="mso-spacerun: yes">  </span><ins
cite="mailto:psahai" datetime="2001-09-04T16:08">Different filters are provided
for you to select the information that you would like to view.</ins><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l144 level1 lfo78;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To enroll a new <%=LC.Pat_Patient_Lower%>, click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Enroll a <ins
cite="mailto:psahai" datetime="2001-09-04T16:07"><u>New </u></ins><%=LC.Pat_Patient%> to <span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:07">a</del></span><ins
cite="mailto:psahai" datetime="2001-09-04T16:07"><u>this</u></ins> Study</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> at the
top<ins cite="mailto:psahai" datetime="2001-09-04T16:12"> of </ins>the<ins
cite="mailto:psahai" datetime="2001-09-04T16:12"> list</ins>.<span
style="mso-spacerun: yes">  </span>This brings up the <%=LC.Pat_Patient%> Search page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l144 level1 lfo78;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter search criteria to select the appropriate <%=LC.Pat_Patient_Lower%> that
you wish to enroll in the study and click on </span><span style='font-size:
9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:
Arial'>Go</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l144 level1 lfo78;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of <%=LC.Pat_Patients_Lower%> matching your search criteria will be
displayed.<span style="mso-spacerun: yes">  </span>Select any </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><%=LC.Pat_Patient%> ID</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link to
see further details.<span style="mso-spacerun: yes">  </span><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l144 level1 lfo78;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Clicking on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Select</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to a specific <%=LC.Pat_Patient_Lower%>s ID will bring up the <b>Study Enrollment</b> screen for
entering enrollment details. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l114 level1 lfo79;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Under the Treatment Details section, select the appropriate
protocol calendar that the <%=LC.Pat_Patient_Lower%> is to be put on and specify a start date
(the date considered as the start date for the protocol).<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l195 level1 lfo173;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter all other relevant information and click on </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>. The <%=LC.Pat_Patient_Lower%> is now enrolled in
the study.</span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc524755632"></a><a name="_Toc55184815"></a><a
name="_Toc56933302"></a><a name="_Toc56934939"></a><a name="_Toc56935047"><span
style='mso-bookmark:_Toc56934939'><span style='mso-bookmark:_Toc56933302'><span
style='mso-bookmark:_Toc55184815'><span style='mso-bookmark:_Toc524755632'><b><i><span
style='font-family:Arial;color:#993366'><%=LC.Pat_Patient%> Schedule</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc524755633"></a><a name="_Toc55184816"></a><a
name="_Toc56933303"></a><a name="_Toc56934940"></a><a name="_Toc56935048"><span
style='mso-bookmark:_Toc56934940'><span style='mso-bookmark:_Toc56933303'><span
style='mso-bookmark:_Toc55184816'><span style='mso-bookmark:_Toc524755633'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Generating A
<%=LC.Pat_Patient%>s Schedule</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Once a
<%=LC.Pat_Patient_Lower%> has been enrolled to a specific study, their specific schedule can be
generated based on the Protocol that they are assigned and their start date. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b><i><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Note</span></i></b><i><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>: To
generate a schedule, you must have created and associated at least one Protocol
Calendar to the study under the Manage Protocol &gt; Calendar section. <o:p></o:p></span></i></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l114 level1 lfo79;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:09">Open the list of protocols that you have access to
and select the </del></span>Select<ins cite="mailto:psahai"
datetime="2001-09-04T16:09"> </ins></span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Manage <%=LC.Pat_Patients%></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the <span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:09"><span
style="mso-spacerun: yes"> </span>link next to the specific protocol</del></span>left-hand<ins
cite="mailto:psahai" datetime="2001-09-04T16:09"> </ins>menu<ins
cite="mailto:psahai" datetime="2001-09-04T16:09"> bar and </ins>click on<ins
cite="mailto:psahai" datetime="2001-09-04T16:09"> </ins></span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T16:09">Open</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in the
sub-menu. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l114 level1 lfo79;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>View</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the study for which a <%=LC.Pat_Patient_Lower%> schedule needs to be generated.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l114 level1 lfo79;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of the <%=LC.Pat_Patients_Lower%> currently enrolled in the selected<ins
cite="mailto:psahai" datetime="2001-09-04T16:09"> stud</ins>y<ins
cite="mailto:psahai" datetime="2001-09-04T16:10"> </ins><span class=msoDel><del
cite="mailto:psahai" datetime="2001-09-04T16:10">study</del></span>will be
displayed on the screen.<span style="mso-spacerun: yes">  </span>Select the
required <%=LC.Pat_Patient_Lower%> by clicking on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><%=LC.Pat_Patient%> ID</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l114 level1 lfo79;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><b><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Schedule</span></b><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> page will be displayed. This page
displays information previously specified in the Study Enrollment page such as
Protocol Calendar and Start Date.<span style="mso-spacerun: yes"> 
</span>Verify the information and make any required changes from the Study
Enrollment page.<span style="mso-spacerun: yes">  </span><br>
Click on the </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:"Arial Black";mso-bidi-font-family:Arial'>Generate Schedule</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> button to
display a schedule based on the Protocol Calendar Template.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l46 level1 lfo172;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To change a <%=LC.Pat_Patient_Lower%>s protocol schedule, go to the Study
Enrollment tab and change the Protocol Calendar to the desired calendar and
select the start date and </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>. </span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l14 level1 lfo171;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>You will be required to </span><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>enter Protocol Discontinuation
details and </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>. You can
now once again </span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>go to the Schedule tab and click on the </span><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial'>Generate Schedule</span><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> button.<span
style="mso-spacerun: yes">  </span>A new schedule will be generated based on
the new calendar and start date selected.</span> </p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc524755634"></a><a name="_Toc55184817"></a><a
name="_Toc56933304"></a><a name="_Toc56934941"></a><a name="_Toc56935049"><span
style='mso-bookmark:_Toc56934941'><span style='mso-bookmark:_Toc56933304'><span
style='mso-bookmark:_Toc55184817'><span style='mso-bookmark:_Toc524755634'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Viewing A
<%=LC.Pat_Patient%>s Schedule</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l126 level1 lfo80;mso-list-change:\F0B7 psahai 20010904T1411;
tab-stops:list .55in'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-bidi-font-family:Arial'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select<ins cite="mailto:psahai" datetime="2001-09-04T16:11">
</ins></span><b><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#CC99FF'><ins cite="mailto:psahai"
datetime="2001-09-04T16:11">Manage <%=LC.Pat_Patients%></ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> </ins>left-hand<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> </ins>menu<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> bar and </ins>click on<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> </ins></span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T16:11">Open</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in the
sub-menu<ins cite="mailto:psahai" datetime="2001-09-04T16:11">.</ins><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l114 level1 lfo79;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>View</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the study for which <%=LC.Pat_Patient_Lower%> schedule needs to be viewed.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l114 level1 lfo79;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A list of all the <%=LC.Pat_Patients_Lower%> currently enrolled in the
selected<ins cite="mailto:psahai" datetime="2001-09-04T16:09"> stud</ins>y<ins
cite="mailto:psahai" datetime="2001-09-04T16:10"> </ins><span class=msoDel><del
cite="mailto:psahai" datetime="2001-09-04T16:10">study</del></span>will be
displayed on the screen.<span style="mso-spacerun: yes">  </span>Select the
required <%=LC.Pat_Patient_Lower%> by clicking on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><%=LC.Pat_Patient%> ID</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l50 level1 lfo170;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><b><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Schedule</span></b><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> page will be displayed. This page
will display the schedule for the <%=LC.Pat_Patient_Lower%> calculated based on the Protocol Calendar
selected for the <%=LC.Pat_Patient_Lower%> and the start date.</span></p>

<p class=MsoNormal style='mso-list:none;mso-list-ins:psahai 20010904T1611'><ins
cite="mailto:psahai" datetime="2001-09-04T16:11"><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></ins></p>

<p class=MsoToc3><span style='font-size:12.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc524755635"></a><a name="_Toc55184818"></a><a
name="_Toc56933305"></a><a name="_Toc56934942"></a><a name="_Toc56935050"><span
style='mso-bookmark:_Toc56934942'><span style='mso-bookmark:_Toc56933305'><span
style='mso-bookmark:_Toc55184818'><span style='mso-bookmark:_Toc524755635'><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'>Managing A
<%=LC.Pat_Patient%>s Schedule</span></i></b></span></span></span></span></a><b><i
style='mso-bidi-font-style:normal'><span style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Once a
<%=LC.Pat_Patient_Lower%>s schedule has been generated, Events (<%=LC.Pat_Patient_Lower%> visits) can be tracked
with respect to scheduled dates for events, actual dates of completion and
specific actions to occur for each Event.<span style="mso-spacerun: yes"> 
</span>To manage a <%=LC.Pat_Patient_Lower%>s schedule:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select<ins cite="mailto:psahai" datetime="2001-09-04T16:11">
</ins></span><b><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:#CC99FF'><ins cite="mailto:psahai"
datetime="2001-09-04T16:11">Manage <%=LC.Pat_Patients%></ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> the </ins>left-hand<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> </ins>menu<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> bar and </ins>click on<ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> </ins></span><b><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'><ins
cite="mailto:psahai" datetime="2001-09-04T16:11">Open</ins></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> </ins>in the sub-menu<ins
cite="mailto:psahai" datetime="2001-09-04T16:11">.</ins><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>View</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the study for which <%=LC.Pat_Patient_Lower%> schedule needs to be viewed/modified.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.55in;mso-text-indent-alt:-.25in;line-height:150%;mso-list:l197 level1 lfo81;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><ins cite="mailto:psahai" datetime="2001-09-04T16:11">A list
of the <%=LC.Pat_Patients_Lower%> currently enrolled in the various studies will be displayed on
the screen. Select the required <%=LC.Pat_Patient_Lower%> by clicking on the </ins></span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T16:11"><u><%=LC.Pat_Patient%> ID</u></ins></span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><ins
cite="mailto:psahai" datetime="2001-09-04T16:11"> link</ins>. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Suggested scheduled dates with their fuzzy periods are
displayed.<span style="mso-spacerun: yes">  </span>Users have the ability to
mark Events as Done along with the details such as actual date of Done
Event and any associated notes. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>User also has the option to mark the Event to another status
also apart from being done. Another status can be Not Required, Suspended
Or To Be Rescheduled.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Based on the <%=LC.Pat_Patient_Lower%>s schedules and the event details
specified for Events, reports can be generated easily to track <%=LC.Pat_Patient_Lower%> visits.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>By default the Actual scheduled date is the same as the
Suggested scheduled date of the event, but it can also be changed by clicking
on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>C</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> link next to the Actual scheduled date.<span
style="mso-spacerun: yes">  </span>In the Change Actual Date page, enter the
new date and select one of three (3) options available on the page.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To view the Event Status History page, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>H</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the Event Status.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l197 level1 lfo81;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Events</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link to
view information related to the event details, link, associated costs,
resources, messages and CRF details.</span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h2>

<h2><a name="_Toc55184819"></a><a name="_Toc56933306"></a><a name="_Toc56934943"></a><a
name="_Toc56935051"><span style='mso-bookmark:_Toc56934943'><span
style='mso-bookmark:_Toc56933306'><span style='mso-bookmark:_Toc55184819'><b><i><span
style='font-family:Arial;color:#993366'>Appendix</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>The
Appendix is that part of your <%=LC.Pat_Patient%> Management that links to other web pages
such as pre- existing forms on your facilitys intranet or other web
sites.<span style="mso-spacerun: yes">  </span>You can also attach files from
your own system to the <%=LC.Pat_Patient%> Appendix. For details on add links or uploading
documents to the <%=LC.Pat_Patient_Lower%> appendix, refer to the Appendix section under budget
or study. <b><i>Note</i></b><i>:<span style="mso-spacerun: yes"> 
</span>Examples of files that can be attached are Text files, Excel files, PDF
and Word documents.<b style='mso-bidi-font-weight:normal'><o:p></o:p></b></i></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184820"></a><a name="_Toc56933307"></a><a name="_Toc56934944"></a><a
name="_Toc56935052"><span style='mso-bookmark:_Toc56934944'><span
style='mso-bookmark:_Toc56933307'><span style='mso-bookmark:_Toc55184820'><b><i><span
style='font-family:Arial;color:#993366'><%=LC.Pat_Patient%> Reports</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><%=LC.Pat_Patient%>-related reports could be run
directly from within the </span><span style='mso-bidi-font-size:12.0pt;
mso-bidi-font-family:Arial'><%=LC.Pat_Patient%> Management Module:<o:p></o:p></span></p>

<p class=MsoBodyText style='text-align:justify;line-height:150%'><span
style='mso-bidi-font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.2in;
line-height:150%;mso-list:l85 level1 lfo25;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the <b>Reports</b> tab across the top.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.2in;
line-height:150%;mso-list:l85 level1 lfo25;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Choose from the powerful list of report types.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.2in;
line-height:150%;mso-list:l85 level1 lfo25;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select appropriate filters.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l92 level1 lfo169;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Display Report</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to run
your report for the selected <%=LC.Pat_Patient_Lower%> and study. </span></p>

<h2><b style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><br style='mso-special-character:
line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]><o:p></o:p></span></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></h1>

<h1><a name="_Toc55184821"></a><a name="_Toc56933308"></a><a name="_Toc56934945"></a><a
name="_Toc56935053"><span style='mso-bookmark:_Toc56934945'><span
style='mso-bookmark:_Toc56933308'><span style='mso-bookmark:_Toc55184821'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Reports</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section provides instructions on displaying, formatting and printing reports.<o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc524755637"></a><a name="_Toc55184822"></a><a
name="_Toc56933309"></a><a name="_Toc56934946"></a><a name="_Toc56935054"><span
style='mso-bookmark:_Toc56934946'><span style='mso-bookmark:_Toc56933309'><span
style='mso-bookmark:_Toc55184822'><span style='mso-bookmark:_Toc524755637'><b><i><span
style='font-family:Arial;color:#993366'>Viewing A Report</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Velos
eResearch has the powerful ability to aggregate and analyze the data that you
have entered into the system and present it to you in the form of User-friendly
reports.<span style="mso-spacerun: yes">  </span>These reports can be <b>account-related</b>
(aggregating and analyzing data across all account Users) or <b>study-related</b>
(access controlled analysis of study specific data) or <b><%=LC.Pat_Patient_Lower%>-related</b>
(analyzing associated cost and study-related data for <%=LC.Pat_Patients_Lower%> entered into the
system).</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>To view
reports:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l72 level1 lfo90;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>Select <b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;color:#CC99FF'>Reports</span></b> from the left-hand menu bar.</p>

<p class=MsoBodyText2 style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l72 level1 lfo90;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>This will take you to the Reports Home page.</p>

<p class=MsoBodyText2 style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l72 level1 lfo90;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>The Reports Home page will display dropdowns for
Account-Related, Study-Related and <%=LC.Pat_Patient%>-Related reports. It also displays
links to the various reports categorized by sections: Account, Study and
<%=LC.Pat_Patient%> related reports.</p>

<p class=MsoBodyText2 style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l72 level1 lfo90;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>User can access any of these reports by either
selecting the required report from the relevant dropdown or by clicking on the
relevant report name.</p>

<p class=MsoBodyText2 style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l72 level1 lfo90;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>The subsequent screen allows Users to select
appropriate filters such as Date, Study, and Organization, etc.</p>

<p class=MsoBodyText2 style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l72 level1 lfo90;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]>Click on the <span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black"'>Display Report</span> button to view your
report.</p>

<p class=MsoBodyText2><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoToc3><span style='font-size:12.0pt;text-transform:uppercase'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc524755638"></a><a name="_Toc55184823"></a><a
name="_Toc56933310"></a><a name="_Toc56934947"></a><a name="_Toc56935055"><span
style='mso-bookmark:_Toc56934947'><span style='mso-bookmark:_Toc56933310'><span
style='mso-bookmark:_Toc55184823'><span style='mso-bookmark:_Toc524755638'><b><i><span
style='font-family:Arial;color:#993366'>Formatting A Report</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Once you have
pulled up a Report for viewing, you can download the data in Word or Excel
format. Click on the link at the top of the Report Output page to download the
report in the required format. Customize the report to suit your needs by
inserting titles, formatting the text, applying filters, creating charts and
graphs or any other feature of the application to which you download the data.</p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><span
style="mso-spacerun: yes"> </span><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc524755639"></a><a name="_Toc55184824"></a><a
name="_Toc56933311"></a><a name="_Toc56934948"></a><a name="_Toc56935056"><span
style='mso-bookmark:_Toc56934948'><span style='mso-bookmark:_Toc56933311'><span
style='mso-bookmark:_Toc55184824'><span style='mso-bookmark:_Toc524755639'><b><i><span
style='font-family:Arial;color:#993366'>Printing A Report</span></i></b></span></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>To Print a
Report once you have pulled up a Report for viewing, first download the Report
in the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Printer Friendly Format</span></u><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> by clicking on the
specific Link on top of the Report Output page.<span style="mso-spacerun:
yes">  </span>As the Report is opened in the printer-friendly format, in order
to print the Report, select Print under the File option.<span
style="mso-spacerun: yes">  </span>Alternatively, you can download the Report
in Word or Excel, and format the Report to include any specific titles or apply
filters.<span style="mso-spacerun: yes">  </span>Under the File option, select
Print and your customized Report will be printed.</span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184825"></a><a name="_Toc56933312"></a><a name="_Toc56934949"></a><a
name="_Toc56935057"><span style='mso-bookmark:_Toc56934949'><span
style='mso-bookmark:_Toc56933312'><span style='mso-bookmark:_Toc55184825'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'><%=LC.Pat_Patient%> Tracking Module</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section described the <%=LC.Pat_Patient%> Tracking Module, an add-on module that enhances
the Consoles capabilities with CRF and Adverse Event tracking and enhanced
scheduling features.<o:p></o:p></span></p>

<p class=MsoNormal><br style='mso-special-character:line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]></p>

<h2><a name="_Toc55184826"></a><a name="_Toc56933313"></a><a name="_Toc56934950"></a><a
name="_Toc56935058"><span style='mso-bookmark:_Toc56934950'><span
style='mso-bookmark:_Toc56933313'><span style='mso-bookmark:_Toc55184826'><b><i><span
style='font-family:Arial;color:#993366'>Tracking Events</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>While
managing the <%=LC.Pat_Patient_Lower%>'s schedule, the status on any Event can be documented and
tracked. To change the status of an Event:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l53 level1 lfo82;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage <%=LC.Pat_Patients%></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Open</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in the
sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l53 level1 lfo82;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the required <%=LC.Pat_Patient_Lower%> by clicking on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><%=LC.Pat_Patient%> ID</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l53 level1 lfo82;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Go to the appropriate protocol and in the schedule page,
click on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Not Done</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> link under the Event Status column for the
respective event.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l187 level1 lfo168;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Event Status pop-up window, enter the relevant
information. The Event Browser in the Schedule page displays the new
status.<span style="mso-spacerun: yes">  </span>To review the history of the
status changes of this Event, click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>H</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> next to
the status.<span style='mso-tab-count:1'>            </span></span></p>

<h2><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></h2>

<p class=MsoNormal align=center style='margin-left:1.5in;text-align:center'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184827"></a><a name="_Toc56933314"></a><a name="_Toc56934951"></a><a
name="_Toc56935059"><span style='mso-bookmark:_Toc56934951'><span
style='mso-bookmark:_Toc56933314'><span style='mso-bookmark:_Toc55184827'><b><i><span
style='font-family:Arial;color:#993366'>Tracking Adverse Events (AE)</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Adverse
Events can be documented for <%=LC.Pat_Patient%> enrolled to a study along with their
type, start and stop date, outcome, and descriptive information. To add an
Adverse Event to a <%=LC.Pat_Patient_Lower%>:</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l156 level1 lfo85;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage <%=LC.Pat_Patients%></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Open</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in the
sub-menu.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l53 level1 lfo82;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the required <%=LC.Pat_Patient_Lower%> by clicking on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><%=LC.Pat_Patient%> ID</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link and
proceed to the appropriate protocol.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l156 level1 lfo85;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Adverse Event</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
across the top, this page will display all the adverse events already added. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l156 level1 lfo85;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the link </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Add a new Adverse Event</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to add a
new adverse event record.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-13.5pt;
line-height:150%;mso-list:l198 level1 lfo167;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>In the
&quot;Adverse Event Details&quot; page, enter all relevant information and
click on </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:"Arial Black"'>Submit</span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal align=center style='margin-left:.3in;text-align:center;
text-indent:.2in'><span style="mso-spacerun: yes"> </span></p>

<p class=MsoNormal style='margin-left:.5in;text-indent:.05in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184828"></a><a name="_Toc56933315"></a><a name="_Toc56934952"></a><a
name="_Toc56935060"><span style='mso-bookmark:_Toc56934952'><span
style='mso-bookmark:_Toc56933315'><span style='mso-bookmark:_Toc55184828'><b><i><span
style='font-family:Arial;color:#993366'>Tracking CRFs</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'> <o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Case Report
Forms (CRFs) can be tracked with their number, name, status, etc.<span
style="mso-spacerun: yes">  </span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l0 level1 lfo87;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:#CC99FF'>Manage <%=LC.Pat_Patients%></span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> from the
left-hand menu bar and click on </span><b><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Open</span></b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in the
sub-menu<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l53 level1 lfo82;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select the required <%=LC.Pat_Patient_Lower%> by clicking on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><%=LC.Pat_Patient%> ID</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l0 level1 lfo87;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>In the Schedule, click on the </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>No
Associated Forms</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'> link under the CRF Status column for the respective
visit for which you want to document a CRF status.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l0 level1 lfo87;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>On the &quot;CRF New Status&quot; page, enter all relevant
information and click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l32 level1 lfo152;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>The &quot;CRF Status&quot; page displays the new CRF
tracking information. <br>
Click on the link </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Add New Status</span></u><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'> to change the status of the CRF,
or click on the link </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Associate a new CRF</span></u><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to add another CRF to the
same visit.</span></p>

<p class=MsoNormal style='margin-left:.3in;text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h2>

<h2><a name="_Toc55184829"></a><a name="_Toc56933316"></a><a name="_Toc56934953"></a><a
name="_Toc56935061"><span style='mso-bookmark:_Toc56934953'><span
style='mso-bookmark:_Toc56933316'><span style='mso-bookmark:_Toc55184829'><b><i><span
style='font-family:Arial;color:#993366'><%=LC.Pat_Patient%> Schedule Related Alerts And
Notifications</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<h3><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h3>

<h3><a name="_Toc55184830"></a><a name="_Toc56933317"></a><a name="_Toc56934954"></a><a
name="_Toc56935062"><span style='mso-bookmark:_Toc56934954'><span
style='mso-bookmark:_Toc56933317'><span style='mso-bookmark:_Toc55184830'><b
style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'>Notification Options</span></i></b></span></span></span></a><b
style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l2 level1 lfo89;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Check the relevant options to prevent Event Notifications to
the <%=LC.Pat_Patient_Lower%>, or to Users in the Study Team for this <%=LC.Pat_Patient_Lower%>.<o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l31 level1 lfo166;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on </span><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:
     Arial'>Submit</span><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'> to save your settings.</span></li>
</ul>

<p class=MsoNormal style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184831"></a><a name="_Toc56933318"></a><a name="_Toc56934955"></a><a
name="_Toc56935063"><span style='mso-bookmark:_Toc56934955'><span
style='mso-bookmark:_Toc56933318'><span style='mso-bookmark:_Toc55184831'><b><i><span
style='font-family:Arial;color:#993366'>Global Study Alerts / Notifications</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
feature allows the capability of defining alerts and notifications that apply
to all <%=LC.Pat_Patients_Lower%> enrolled to a specific study. <o:p></o:p></span></p>

<p class=MsoFooter style='text-align:justify;line-height:150%;tab-stops:.5in'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoFooter style='text-align:justify;text-indent:.25in;line-height:
150%;tab-stops:.5in'><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To access and save global alerts/notifications settings:<o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l18 level1 lfo124;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>This functionality can be accessed by opening
     any study protocol through </span><b><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Manage
     Protocols &gt;&gt; Open</span></b><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> and going to the <b>Alerts/Notifications</b>
     tab.<span style="mso-spacerun: yes">  </span>Alternatively, select </span><b><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     color:#CC99FF'>Manage <%=LC.Pat_Patients%></span></b><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> from the left-hand menu bar
     and click on </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;color:#CC99FF'>Open</span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> in
     the sub-menu.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l18 level1 lfo124;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Go to the Enrolled <%=LC.Pat_Patients%> tab to view a list
     of all studies that are active/enrolling.<o:p></o:p></span></li>
</ul>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l18 level1 lfo124;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>View</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link next
to the study for which <%=LC.Pat_Patient_Lower%> global Alerts / Notifications need to be added.<o:p></o:p></span></p>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l18 level1 lfo124;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on the link </span><u><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Alerts/Notifications Settings</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> and
specify settings for Event Status Notifications, CRFs Notification and Alert
and Notifications. You may need to select specific calendars to define those
settings that are calendar based such as Event related notifications.<o:p></o:p></span></p>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l182 level1 lfo165;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>These settings are automatically applied to all <%=LC.Pat_Patients_Lower%>
enrolled to the study.</span></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184832"></a><a name="_Toc56933319"></a><a name="_Toc56934956"></a><a
name="_Toc56935064"><span style='mso-bookmark:_Toc56934956'><span
style='mso-bookmark:_Toc56933319'><span style='mso-bookmark:_Toc55184832'><b><i><span
style='font-family:Arial;color:#993366'>Receiving Notifications For &quot;Past
Scheduled Date&quot; Events</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><span style="mso-spacerun: yes"> 
</span><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><i><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></i></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Users can
elect to be notified whenever an Events scheduled date becomes past due
without the Event status being changed to a status other than Not Done. To
set up &quot;Past Schedule Date&quot; notifications:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l60 level1 lfo84;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Go to the <b>Alerts/Notifications</b> page as described
above.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l60 level1 lfo84;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Check the option &quot;Send alert when an event status
changes to Past Scheduled Date&quot; and select the Users whom you want a
notification email to be sent.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l60 level1 lfo84;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Enter the email-IDs, cell phone or pager number.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l60 level1 lfo84;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Click on </span><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to save
your settings.<o:p></o:p></span></p>

<h2><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h2>

<h2><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h2>

<h2><a name="_Toc55184833"></a><a name="_Toc56933320"></a><a name="_Toc56934957"></a><a
name="_Toc56935065"><span style='mso-bookmark:_Toc56934957'><span
style='mso-bookmark:_Toc56933320'><span style='mso-bookmark:_Toc55184833'><b><i><span
style='font-family:Arial;color:#993366'>Receiving Adverse Events Notifications</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'> <o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Users can
elect to be notified whenever an Adverse Event is reported. To set up AE
notifications:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l105 level1 lfo86;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Go to the <b>Alerts/Notifications</b> page as described
above.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l105 level1 lfo86;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Check the option &quot;Send alert if Serious Adverse Event
is reported.&quot;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l105 level1 lfo86;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Select Users who should receive email notifications, or
enter cell phone/ pager number.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l105 level1 lfo86;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A similar option is available for alerts if Death is
reported as Adverse Event outcome&quot;<o:p></o:p></span></p>

<p class=MsoFooter style='tab-stops:.5in'><br style='mso-special-character:
line-break'>
<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>
<![endif]></p>

<h2><a name="_Toc55184834"></a><a name="_Toc56933321"></a><a name="_Toc56934958"></a><a
name="_Toc56935066"><span style='mso-bookmark:_Toc56934958'><span
style='mso-bookmark:_Toc56933321'><span style='mso-bookmark:_Toc55184834'><b><i><span
style='font-family:Arial;color:#993366'>Receiving Event Status Notifications</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Users can
elect to be notified whenever the status of an event is changed.<span
style="mso-spacerun: yes">  </span><br>
To set up Event Status notifications:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l127 level1 lfo83;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Go to the <b>Alerts/Notifications</b> page as described
above.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l127 level1 lfo83;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Under the Event Notification heading, click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Add Another
Event Notification</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-13.5pt;
line-height:150%;mso-list:l146 level1 lfo164;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Enter the relevant
information.<span style="mso-spacerun: yes">  </span>You can choose to setup
this notification for all events, or for a specific event.<span
style="mso-spacerun: yes">  </span>Click on </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to save.<span
style='mso-tab-count:1'>            </span></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184835"></a><a name="_Toc56933322"></a><a name="_Toc56934959"></a><a
name="_Toc56935067"><span style='mso-bookmark:_Toc56934959'><span
style='mso-bookmark:_Toc56933322'><span style='mso-bookmark:_Toc55184835'><b><i><span
style='font-family:Arial;color:#993366'>Receiving CRF Status Notifications</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Users can
elect to be notified whenever the status of a CRF is changed or a new specified
status is being added. To set up CRF notifications:<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l90 level1 lfo88;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Go to the <b>Alerts/Notifications</b> page as described
above.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l90 level1 lfo88;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Under the CRF Notifications heading, click on the link </span><u><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Add Another
CRF Notification</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l90 level1 lfo88;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>CRF Notifications page is displayed. Enter all the relevant
information.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l90 level1 lfo88;tab-stops:list .55in'><![if !supportLists]><span
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>You can choose to set up this notification for all CRFs, or
for a specific CRF only. Click on </span><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to save.</span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184836"></a><a name="_Toc56933323"></a><a name="_Toc56934960"></a><a
name="_Toc56935068"></a><a name="_Section_10:_Budget"></a><span
style='mso-bookmark:_Toc55184836'><span style='mso-bookmark:_Toc56933323'><span
style='mso-bookmark:_Toc56934960'><span style='mso-bookmark:_Toc56935068'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Budget</span></span></span></span></span><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section describes features available in the Budgeting section of the
application. This module is available as an add-on module and enhances the
Consoles capabilities by allowing Users to create and share budgets for
different studies. This process is facilitated by the provision of pre-defined
budget templates, which are customizable, at both the study level and at the
<%=LC.Pat_Patient_Lower%> level. <b style='mso-bidi-font-weight:normal'><o:p></o:p></b></span></p>

<h1><span style='font-size:12.0pt;font-family:"Times New Roman";color:windowtext;
font-weight:normal'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></h1>

<h2><b><i><span style='font-family:Arial;color:#993366'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184837"></a><a name="_Toc56933324"></a><a name="_Toc56934961"></a><a
name="_Toc56935069"><span style='mso-bookmark:_Toc56934961'><span
style='mso-bookmark:_Toc56933324'><span style='mso-bookmark:_Toc55184837'><b><i><span
style='font-family:Arial;color:#993366'>Creating A New Budget (<%=LC.Pat_Patient%> Or
Study)</span></i></b></span></span></span></a><b><i><span style='font-family:
Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l111 level1 lfo129;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To document a new Budget, select </span><b><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     color:#CC99FF'>Budget</span></b><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'> menu from your left-hand menu bar and click on </span><b><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     color:#CC99FF'><span style='mso-prop-change:psahai 20010910T1126'><span
     style='mso-prop-change:psahai 20010910T1126'>New </span></span></span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     color:black'>in the</span><b><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;color:#CC99FF'> </span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>sub-menu.<o:p></o:p></span></li>
</ul>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:01">A list of
the protocols that you have access to will be displayed along with links to
Protocol Details and Manage <%=LC.Pat_Patients%>. Select the Manage <%=LC.Pat_Patients%> option.</del></span><span
class=msoDel><del cite="mailto:psahai" datetime="2001-09-04T16:01"><o:p></o:p></del></span></span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.5in;
mso-text-indent-alt:-.2in;line-height:150%;mso-list:l85 level1 lfo25;
mso-list-change:\F0B7 psahai 20010904T1411;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:01">·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></del></span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:01">A list of the <%=LC.Pat_Patients_Lower%> currently enrolled in the
study will be displayed on the screen. There is also a link to Register a
<%=LC.Pat_Patient_Lower%> at the top. To register a new <%=LC.Pat_Patient_Lower%> into the system, click on this
link.</del></span><span class=msoDel><del cite="mailto:psahai"
datetime="2001-09-04T16:01"><o:p></o:p></del></span></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l111 level1 lfo129;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Enter relevant information in the Budget Open
     page provided and select an option from the Budget Type dropdown,
     available options are <%=LC.Pat_Patient%> and Study. The Study Budget option
     provides you with a template for creating an overall Study budget whereas
     selecting the <%=LC.Pat_Patient%> Budget option provides you with a template that
     allows entering line items for visits/events associated at a <%=LC.Pat_Patient%>
     level.<span style="mso-spacerun: yes">  </span><o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l111 level1 lfo129;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If the Budget is specific to an organization,
     select the appropriate option from the list of available Organizations. If
     not, select the Option Budget is Organization non-specific. Associating
     a budget to an Organization allows you to make copies for different sites
     as well as facilitates sharing with all members of that Organization in a
     single click (refer to the Access Rights part in this section for
     details on sharing your budget with other Users).<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l4 level1 lfo96;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If required, you can associate the Budget to a
     specific Study by selecting one from the Select Study dropdown. This
     dropdown has all the studies created within the account irrespective of
     their study status.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l4 level1 lfo96;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To save this information, enter your e-Signature
     and click on </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
     font-family:"Arial Black";mso-bidi-font-family:Arial'>Proceed to Budget</span><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>.
     This action takes you to the Budget &gt;&gt; Open page, the template
     displayed is Study or <%=LC.Pat_Patient%> depending on the selection you had made
     in the earlier screen. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l4 level1 lfo96;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Entries can be made in the appropriate fields
     and calculations can be performed based on those entries. Sections and
     Line Items can be edited to customize the template.<span
     style="mso-spacerun: yes">  </span><o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l115 level1 lfo163;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To aid the process of creating a <%=LC.Pat_Patient%> Budget,
     Users have the option of using a protocol calendar from the study selected
     at the time of budget creation or a calendar from the </span></li>
</ul>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Library.
The protocol calendar mirrors the schedule of visits for this study and can be
used as the baseline for the budget template.</span></p>

<p class=MsoNormal style='margin-left:.25in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184838"></a><a name="_Toc56933325"></a><a name="_Toc56934962"></a><a
name="_Toc56935070"><span style='mso-bookmark:_Toc56934962'><span
style='mso-bookmark:_Toc56933325'><span style='mso-bookmark:_Toc55184838'><b><i><span
style='font-family:Arial;color:#993366'>Copying a Budget</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l3 level1 lfo97;
     tab-stops:list .5in'><u><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Copy this Budget</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     allows Users to copy an existing budget (both freeze and work in
     progress budgets).<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l3 level1 lfo97;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Select </span><b><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Open</span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>
     sub-menu under the </span><b><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial;color:#CC99FF'>Budget</span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> menu
     from your left-hand menu bar. This will display a list of all the Budgets
     that you have created or have access to.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l3 level1 lfo97;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on the </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Budget Name</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> of
     the budget that you wish to copy.<span style="mso-spacerun: yes"> 
     </span>Open the Budget and then click on the link </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Copy
     this Budget</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'><o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l3 level1 lfo97;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>In the Budget Copy page, enter a name and
     version number for your new budget.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l168 level1 lfo162;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Remember that the Budget Name/Version Number
     combination must be unique within the account.</span> </li>
</ul>

<p class=MsoNormal style='margin-left:.25in;text-align:justify;line-height:
150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><span style="mso-spacerun: yes">   
</span><span style='font-size:6.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
color:#993366'><o:p></o:p></span></p>

<h2><a name="_Toc55184839"></a><a name="_Toc56933326"></a><a name="_Toc56934963"></a><a
name="_Toc56935071"><span style='mso-bookmark:_Toc56934963'><span
style='mso-bookmark:_Toc56933326'><span style='mso-bookmark:_Toc55184839'><b><i><span
style='font-family:Arial;color:#993366'>Appendix</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>From the
Appendix tab, you can link to other web pages such as pre-existing forms on
your facilitys Intranet or other web sites and you can also upload and attach
files from your own system to the Budget for viewing by other authorized Users.</p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b><i><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Note</span></i></b><i><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>:</span></i><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
style="mso-spacerun: yes">  </span>The files that can be attached are Text
files, Excel files, PDF and Word documents.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l27 level1 lfo98;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To add a new/edit Appendix record (Link or
     File), click on the Budget <b>Appendix</b> tab.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l27 level1 lfo98;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To add a new link, click on the </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Add
     New URL</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'> link and in the Budget URL page, enter data in both
     the mandatory fields URL and its Short Description, and click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
     mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> to save the record.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l76 level1 lfo161;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To upload a new document or file to the budget,
     click on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Upload Document</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     and in the Budget File page, browse for the file to be uploaded or enter
     the file path in the Edit Box, enter a short identifying description and
     click on </span><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
     font-family:"Arial Black";mso-bidi-font-family:Arial'>Submit</span><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to
     save the record.</span></li>
</ul>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184840"></a><a name="_Toc56933327"></a><a name="_Toc56934964"></a><a
name="_Toc56935072"><span style='mso-bookmark:_Toc56934964'><span
style='mso-bookmark:_Toc56933327'><span style='mso-bookmark:_Toc55184840'><b><i><span
style='font-family:Arial;color:#993366'>Access Rights</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>Your Budget
can be shared with other account Users and specific rights assigned based on
access permissions. For ease of use, access to the Budget can be provided to a
group of Users simultaneously: all Users within an account, all Users in the
organization (organization associated to the budget) and all Users within the
Study Team (study associated to the budget).</p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l117 level1 lfo42;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To add/edit Budget Access Rights, click on the Budget <b>Access
Rights</b> tab.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:.55in;text-align:justify;text-indent:
-.25in;line-height:150%;mso-list:l117 level1 lfo42;tab-stops:list .55in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>To assign Access Rights to a group of Users: select any of
the following options<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:58.5pt;text-indent:-.25in;line-height:
150%;mso-list:l117 level2 lfo42;tab-stops:list 58.5pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Select the
option </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>All Users of this Account</span></u><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to assign budget access
rights to all Users in the account.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:58.5pt;text-indent:-.25in;line-height:
150%;mso-list:l117 level2 lfo42;tab-stops:list 58.5pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Select the
option </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>All Users of this Organization</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to assign
budget access rights to all Users belonging to the specific Organization that
has been associated to the Budget. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:58.5pt;text-indent:-.25in;line-height:
150%;mso-list:l117 level2 lfo42;tab-stops:list 58.5pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Select the
option </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>All Users within this Study Team</span></u><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to assign
budget access rights to all Users in the study team for the study that has been
associated to the Budget.<o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l48 level1 lfo102;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Once the appropriate option has been selected
     New, Edit and View access rights are defined for the various screens
     within the Budget module for this specific Budget.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l84 level1 lfo153;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The budget creator is automatically added to the
     list of Specific Users with Access but additional Users can be added to
     the list by clicking on the </span><u><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'>Select Users</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>
     link. </span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l84 level1 lfo153;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Access rights for these Users are assigned by
     clicking on the respective </span><u><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'>Access Rights</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     for specific Users and then assigning rights as required.</span></li>
</ul>

<p class=MsoNormal style='margin-left:.25in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-indent:.5in'><span style="mso-spacerun: yes">  
</span></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184841"></a><a name="_Toc56933328"></a><a name="_Toc56934965"></a><a
name="_Toc56935073"><span style='mso-bookmark:_Toc56934965'><span
style='mso-bookmark:_Toc56933328'><span style='mso-bookmark:_Toc55184841'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Milestones</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'> <o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section describes the features available in the Milestones module, available as
an add-on module to enhance the Consoles capabilities for participants
(investigators, coordinators and sponsors) wanting to track the cost involved,
achievement of major milestones, payment due and payment received.<span
style="mso-spacerun: yes">  </span>These milestones and parameters need to be
defined based on a studys needs.<span style="mso-spacerun: yes">  </span>The
achievement can be tracked via reports and notifications and the User is kept
aware of the most current status.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<h2><a name="_Toc55184842"></a><a name="_Toc56933329"></a><a name="_Toc56934966"></a><a
name="_Toc56935074"><span style='mso-bookmark:_Toc56934966'><span
style='mso-bookmark:_Toc56933329'><span style='mso-bookmark:_Toc55184842'><b><i><span
style='font-family:Arial;color:#993366'>Creating New Milestones</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;mso-bidi-font-family:"Times New Roman"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l4 level1 lfo96;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To define new Milestones for a specific study,
     select </span><b><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial;color:#CC99FF'>Milestones</span></b><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> menu
     from your left-hand menu bar and click on the link </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Create
     New</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'> next to the appropriate Study Number.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l102 level1 lfo154;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Clicking on </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Create New</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     takes you to the Milestones &gt;&gt; Open page. On this page, you have
     options to add and/or edit parameters around <%=LC.Pat_Patient%> Status, Visit and
     Event Milestones that need to be defined.</span></li>
</ul>

<p class=MsoNormal style='margin-left:.25in;text-indent:.25in'><span
style="mso-spacerun: yes">   </span></p>

<h3><a name="_Toc55184843"></a><a name="_Toc56933330"></a><a name="_Toc56934967"></a><a
name="_Toc56935075"><span style='mso-bookmark:_Toc56934967'><span
style='mso-bookmark:_Toc56933330'><span style='mso-bookmark:_Toc55184843'><b><i><span
style='font-size:10.0pt'><%=LC.Pat_Patient%> Status Milestones</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To define a Milestone related to <%=LC.Pat_Patient_Lower%>
     enrollment and <%=LC.Pat_Patient_Lower%> status, click on the </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Add
     New</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'> link next to <%=LC.Pat_Patient%> Status Milestones.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The <%=LC.Pat_Patient%> Status Milestones page displayed has
     multiple rows to facilitate data entry. Each record/row requires data to
     be entered in the following fields: <o:p></o:p></span></li>
 <ol style='margin-top:0in' start=1 type=1>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>When <%=LC.Pat_Patient%> Count Equals: This is a mandatory
      number field and the milestone is considered achieved when number of enrolled
      <%=LC.Pat_Patients_Lower%> reaches this count taking into account the Rule entered in the
      next field <i style='mso-bidi-font-style:normal'>e.g. if this field has a
      value of 10 and the Milestone Rule is Enrolled <%=LC.Pat_Patients_Lower%>, only Active
      then this milestone is reported as Achieved when the number of enrolled
      active <%=LC.Pat_Patients_Lower%> reaches 10.</i><o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Milestone Rule: This is a mandatory dropdown
      field with an available set of pre-defined rules. The Milestone Rule is
      the logic based on which this milestone is considered Achieved for
      reporting purpose.<o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Amount: This is a non-mandatory number field
      and allows you to associate a payment amount to this milestone.<o:p></o:p></span></li>
 </ol>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l152 level1 lfo160;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Once data has been entered, click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
     mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> and the saved data will be
     displayed in the Milestone &gt;&gt; Open browser.</span></li>
</ul>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc55184844"></a><a name="_Toc56933331"></a><a name="_Toc56934968"></a><a
name="_Toc56935076"><span style='mso-bookmark:_Toc56934968'><span
style='mso-bookmark:_Toc56933331'><span style='mso-bookmark:_Toc55184844'><b><i><span
style='font-size:10.0pt'>Visit Milestones</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To define a milestone related to the occurrence
     of one or more specific visits from a studys pre-defined schedule of
     visits, click on the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Add New</span></u><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> link next to the Visit
     Milestones. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The page displayed provides you with options of
     the various protocol calendars associated with this study. Select the
     calendar that you would like to work with and click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
     mso-bidi-font-family:Arial'>Go</span><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'>. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Depending on the calendar selected, the
     corresponding Visit Schedule is displayed with the option to define the
     following parameters for each Milestone:<o:p></o:p></span></li>
 <ol style='margin-top:0in' start=1 type=1>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Select Visit: Visits from the selected protocol
      calendar are displayed with checkboxes to select the visits to be
      included as a Milestone.<o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Milestone Rule: This field is mandatory for
      visits that have been selected. <br>
      The Milestone Rule is the logic based on which a Milestone is considered
      Achieved for reporting purpose.<o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Amount: This is a non-mandatory number field
      and allows you to associate a payment amount to this Milestone.<o:p></o:p></span></li>
 </ol>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l86 level1 lfo155;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Once data has been entered, click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
     mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> and the saved data will be
     displayed in the Milestone &gt;&gt; Open browser.</span></li>
</ul>

<p class=MsoNormal style='margin-left:.25in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h3><a name="_Toc55184845"></a><a name="_Toc56933332"></a><a name="_Toc56934969"></a><a
name="_Toc56935077"><span style='mso-bookmark:_Toc56934969'><span
style='mso-bookmark:_Toc56933332'><span style='mso-bookmark:_Toc55184845'><b><i><span
style='font-size:10.0pt'>Event Milestones</span></i></b></span></span></span></a><b><i><span
style='font-size:10.0pt'><o:p></o:p></span></i></b></h3>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To define a Milestone related to the occurrence
     of one or more specific events from a studys pre-defined schedule of
     visits/events, click on the </span><u><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'>Add New</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     next to the Event Milestones.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The page displayed provides you with options of
     the various protocol calendars associated with this study. Select the
     calendar that you would like to work with and click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
     mso-bidi-font-family:Arial'>Go</span><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'>. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level1 lfo103;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Depending on the calendar selected, the
     corresponding visit/event schedule is displayed with the option to define
     the following parameters for each milestone:<o:p></o:p></span></li>
 <ol style='margin-top:0in' start=1 type=1>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Select Event: Events are displayed with
      checkboxes to select the events to be included as a milestone.<o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Milestone Rule: This field is mandatory for
      events that have been selected. The Milestone Rule is the logic based on
      which a milestone is considered Achieved for reporting purpose.<o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l35 level2 lfo103;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Amount: This is a non-mandatory number field
      and allows you to associate a payment amount to this milestone.<o:p></o:p></span></li>
 </ol>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l106 level1 lfo157;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Once data has been entered, click on </span><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
     mso-bidi-font-family:Arial'>Submit</span><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> and the saved data will be
     displayed in the Milestone &gt;&gt; Open browser.</span></li>
</ul>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184846"></a><a name="_Toc56933333"></a><a name="_Toc56934970"></a><a
name="_Toc56935078"><span style='mso-bookmark:_Toc56934970'><span
style='mso-bookmark:_Toc56933333'><span style='mso-bookmark:_Toc55184846'><b><i><span
style='font-family:Arial;color:#993366'>Milestone View (Summary Report)</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l63 level1 lfo104;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>If Milestones have been defined for a specific
     study, the Milestones Summary column in the Milestone Home browser will
     display a </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'>View</span></u><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> link instead of </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Data
     Not Available</span></u><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'> seen for studies for which no milestones have
     been defined.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l63 level1 lfo104;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on the </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>View</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     next to the appropriate Study to display the Milestones Summary (Study)
     report with an aggregate of milestones achieved (<%=LC.Pat_Patient%> Status, Visit and
     Event Milestones) for the study.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l69 level1 lfo156;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The report can be printed and/or downloaded into
     Word or Excel files.</span></li>
</ul>

<p class=MsoNormal style='margin-left:.25in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal style='margin-left:.25in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184847"></a><a name="_Toc56933334"></a><a name="_Toc56934971"></a><a
name="_Toc56935079"><span style='mso-bookmark:_Toc56934971'><span
style='mso-bookmark:_Toc56933334'><span style='mso-bookmark:_Toc55184847'><b><i><span
style='font-family:Arial;color:#993366'>Notifications</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l163 level1 lfo106;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The Notifications tab allows you to select Users
     who should receive automatic notifications whenever the corresponding
     milestone is achieved. These notifications are sent as email messages to
     the selected Users.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l163 level1 lfo106;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To access the Notification page, click on the <b>Notifications</b>
     tab. Notification page displays the following: -<o:p></o:p></span></li>
 <ol style='margin-top:0in' start=1 type=1>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l163 level2 lfo106;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>A list of the milestones as specified in the
      Milestones page.<o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l163 level2 lfo106;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Selected User(s) receiving notification for the
      respective Milestone.<o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l163 level2 lfo106;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Link for selecting/deselecting Users.<o:p></o:p></span></li>
 </ol>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l79 level1 lfo158;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Clicking on the link </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Select/Deselect
     User(s)</span></u><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'> next to the appropriate Milestone brings up the User
     Search page for selecting and deselecting Users.</span> </li>
</ul>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184848"></a><a name="_Toc56933335"></a><a name="_Toc56934972"></a><a
name="_Toc56935080"><span style='mso-bookmark:_Toc56934972'><span
style='mso-bookmark:_Toc56933335'><span style='mso-bookmark:_Toc55184848'><b><i><span
style='font-family:Arial;color:#993366'>Appendix</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoBodyText2 style='text-align:justify;line-height:150%'>From the
Appendix tab, you can link to other web pages such as pre-existing forms on
your facilitys Intranet or other web sites and you can also upload and attach
files from your own system to the Budget for viewing by other authorized Users.</p>

<p class=MsoNormal style='text-align:justify'><b><span style='font-size:5.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><b><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Note</span></b><i><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>:</span></i><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'><span
style="mso-spacerun: yes">  </span>The files that can be attached are Text
files, Excel files, PDF and Word documents.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:5.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>For
detailed steps on Add New URL Or to Upload Document, refer to the Appendix
paragraph in the Budget or Study section.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;line-height:150%'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184849"></a><a name="_Toc56933336"></a><a name="_Toc56934973"></a><a
name="_Toc56935081"><span style='mso-bookmark:_Toc56934973'><span
style='mso-bookmark:_Toc56933336'><span style='mso-bookmark:_Toc55184849'><b><i><span
style='font-family:Arial;color:#993366'>Payments Received</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l28 level1 lfo107;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To record payments received, click on </span><b><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial;
     color:#CC99FF'>Milestones</span></b><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> from your left-hand menu bar
     and open the appropriate Milestones record for the Study by clicking on
     the </span><u><span style='font-size:9.0pt;mso-bidi-font-size:12.0pt;
     font-family:Arial'>Edit Details</span></u><span style='font-size:10.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'> link.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l28 level1 lfo107;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on the <b>Payments Received</b> tab. This
     page captures payment data such as Date, Amount Received and Description,
     and displays it in the browser.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l1 level1 lfo112;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>In the Milestone Payment browser there is a </span><u><span
     style='font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Delete</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     next to the Payment record to remove it from the browser. Clicking on this
     link prompts the User to enter his/her e-Signature for deletion of the
     record.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l174 level1 lfo159;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The </span><u><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial'>Date</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>
     field in the browser links to the details for that entry and should be
     used for viewing or editing Milestone Payment details.</span> </li>
</ul>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h2><a name="_Toc55184850"></a><a name="_Toc56933337"></a><a name="_Toc56934974"></a><a
name="_Toc56935082"><span style='mso-bookmark:_Toc56934974'><span
style='mso-bookmark:_Toc56933337'><span style='mso-bookmark:_Toc55184850'><b><i><span
style='font-family:Arial;color:#993366'>Reports</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l149 level1 lfo114;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To access the Milestones Reports, click on the <b>Reports</b>
     tab.<span style="mso-spacerun: yes">  </span>Milestone Reports are classified
     into two broad categories: <o:p></o:p></span></li>
 <ul style='margin-top:0in' type=circle>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l149 level2 lfo114;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Milestone Status Reports, and <o:p></o:p></span></li>
  <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l149 level2 lfo114;
      tab-stops:list 1.0in'><span style='font-size:10.0pt;mso-bidi-font-size:
      12.0pt;font-family:Arial'>Milestones/Payments Received Reports.<o:p></o:p></span></li>
 </ul>
</ul>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l64 level1 lfo115;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>A </span><u><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>Select a <%=LC.Pat_Patient%></span></u><span style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> filter is available for
displaying a <%=LC.Pat_Patient_Lower%> specific report such as Milestones Summary (<%=LC.Pat_Patient%>).<o:p></o:p></span></p>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l64 level1 lfo115;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;
mso-bidi-font-family:Arial'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Date filters are available and if specified, the date filter
is applied on the Date Milestone Achieved column of the reports.<o:p></o:p></span></p>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l83 level1 lfo197;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><![endif]><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>Give a name for the Generated Report edit box is for
entering a name to identify the report to be generated. Often these reports are
data intensive and may require several minutes to generate. In such cases, you
will receive an email notification a few minutes later informing you that your
report is ready. These reports can be viewed under the <b>Saved Reports</b>
tab. The report is identified by the name that you have provided.</span></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><span style="mso-spacerun: yes">  
</span></p>

<h2><a name="_Toc55184851"></a><a name="_Toc56933338"></a><a name="_Toc56934975"></a><a
name="_Toc56935083"><span style='mso-bookmark:_Toc56934975'><span
style='mso-bookmark:_Toc56933338'><span style='mso-bookmark:_Toc55184851'><b><i><span
style='font-family:Arial;color:#993366'>Saved Reports</span></i></b></span></span></span></a><b><i><span
style='font-family:Arial;color:#993366'><o:p></o:p></span></i></b></h2>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l175 level1 lfo117;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>To view your reports that were generated
     previously or just a few minutes ago, access the appropriate Milestones
     record and click on the <b>Saved Reports</b> tab. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l175 level1 lfo117;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>The browser displays information like Report
     Name, Report Run By, Date/Time report was executed, <%=LC.Pat_Patient%> selected (if
     any) and the Date Range selected. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l175 level1 lfo117;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Click on the </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Report Name</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> to
     View the selected milestone saved report.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l45 level1 lfo198;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>There is a </span><u><span style='font-size:
     9.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Delete</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'> link
     next to the report name to delete a saved report.</span></li>
</ul>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<span style='font-size:12.0pt;font-family:"Times New Roman";mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:AR-SA'><br clear=all style='mso-special-character:line-break;
page-break-before:always'>
</span>

<p class=MsoNormal style='margin-left:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<h1><a name="_Toc55184852"></a><a name="_Toc56933339"></a><a name="_Toc56934976"></a><a
name="_Toc56935084"><span style='mso-bookmark:_Toc56934976'><span
style='mso-bookmark:_Toc56933339'><span style='mso-bookmark:_Toc55184852'><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'>Data Safety Monitoring</span></span></span></span></a><span
style='font-size:18.0pt;mso-bidi-font-size:10.0pt'><o:p></o:p></span></h1>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoHeader style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='text-align:justify;line-height:150%;tab-stops:.5in'><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>This
section describes the Data and Safety Monitoring reports module that is
available as an add-on module. It enhances the Consoles capabilities by
allowing the generation of important reports used for the purpose of monitoring
clinical trials and focuses on areas such as:<o:p></o:p></span></p>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l139 level1 lfo118;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Performance
- to assess the performance of one or more sites with respect to subject
recruitment, retention and follow-up, protocol adherence, case report form
status and quality of data;<o:p></o:p></span></p>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l139 level1 lfo118;tab-stops:list .5in'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Safety -
to assess the magnitude of Adverse Events; and<o:p></o:p></span></p>

<p class=MsoFooter style='margin-left:.5in;text-align:justify;text-indent:-.25in;
line-height:150%;mso-list:l139 level1 lfo118;tab-stops:list .5in'><![if !supportLists]>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><![endif]><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Treatment
- to monitor and assess treatment effects.</span></p>

<p class=MsoFooter style='margin-left:.25in;tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='margin-left:.25in;tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><b><i><span style='font-family:Arial;
color:#993366'>Accessing Data Safety Monitoring Reports<o:p></o:p></span></i></b></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l175 level1 lfo117;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Select </span><b><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:Arial;color:#CC99FF'>Data Safety
     Monitoring</span></b><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'> from your left-hand menu bar to access these
     reports. <o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l175 level1 lfo117;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>A Select Study drop-down is available to
     select a study. Data Safety Monitoring reports generated will display data
     for the selected study.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l175 level1 lfo117;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Date filters are available to select the
     required date range such as All, Year, Month or any other User
     defined Date Range.<o:p></o:p></span></li>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l175 level1 lfo117;
     tab-stops:list .5in'><u><span style='font-size:9.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>Select Organization</span></u><span
     style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>
     filter is available for displaying an Organization specific report such as
     Protocols By Organization report, etc.<o:p></o:p></span></li>
</ul>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='text-align:justify;line-height:150%;mso-list:l6 level1 lfo199;
     tab-stops:list .5in'><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'>After Selecting the desired report and any
     required filters, click on the </span><span style='font-size:9.0pt;
     mso-bidi-font-size:12.0pt;font-family:"Arial Black";mso-bidi-font-family:
     Arial'>Display Report</span><span style='font-size:10.0pt;mso-bidi-font-size:
     12.0pt;font-family:Arial'> button to view the generated report.</span></li>
</ul>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoFooter style='tab-stops:.5in'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

<p class=MsoNormal align=center style='margin-right:-22.5pt;text-align:center'><span
style='font-family:Arial;color:black'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>

</div>
