<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page import="java.util.HashMap"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="ctrpDraftJB"  scope="request" class="com.velos.eres.ctrp.web.CtrpDraftJB"/>

<%!
private static String MESSAGE_KEY = "messageKey";
private static String ID = "id";
private static String[] URL_LIST  = {"ctrpDraftBrowser","ctrpDraftNonIndustrial","ctrpDraftNonIndustrial",
	"ctrpDraftIndustrial","ctrpDraftIndustrial"};
%>
<%
// The only scriptlet in this JSP
String displayText = null;
String nextUrl = null;
Integer dPk = null;
try {
	if (!sessionmaint.isValidSession(request.getSession(false))) { return; }
	displayText = (String)MC.class.getField(request.getParameter(MESSAGE_KEY)).get(null);
	Integer id = StringUtil.stringToInteger(request.getParameter(ID));
	nextUrl = URL_LIST[id];
	dPk = ctrpDraftJB.getId();

	switch(id){
	case 1:
		//Mark Non-Indust Draft RFS
	case 4:
		//Mark Indust Draft RFS
		nextUrl += "?draftId="+dPk;
		break;
	case 2:
		//Save Non-Indust Draft
	case 3:
		//Save Indust Draft
		nextUrl += "?draftId="+dPk;
		nextUrl += "&saved=1";
		break;
	}
} catch(Exception e) {
	e.printStackTrace();
	return;
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Refresh" content="1; URL='<%=nextUrl%>' ">
<title></title>
<jsp:include page="/jsp/localization.jsp"></jsp:include>
<jsp:include page="/jsp/panel.jsp"></jsp:include>
<jsp:include page="/jsp/velos_includes.jsp"></jsp:include>
</head>
<body>
<div class="BrowserBotN" >
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<p class="sectionHeadings" align="center"><%=displayText%></p>
</div>
</body>
</html>
