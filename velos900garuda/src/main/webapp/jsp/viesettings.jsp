<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<SCRIPT LANGUAGE="JavaScript" SRC="resolution.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<title><%=LC.L_My_Settings%><%--My Settings*****--%></title>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT Language="javascript">

 function  validate(formobj)

   {
	if (!(validate_col('IP Address',formobj.IPA))) return false;
	if (!(validate_col('IP Address',formobj.IPB))) return false	;
	if (!(validate_col('IP Address',formobj.IPC))) return false	;	
	if (!(validate_col('IP Address',formobj.IPD))) return false;
		
    if(isNaN(formobj.IPA.value) == true) {
	alert("<%=MC.M_Etr_ValidIpAddr%>");/*alert("Please enter Valid IP address.");*****/
	formobj.IPA.focus();
	return false;
	} 
 if(isNaN(formobj.IPB.value) == true) {
	 alert("<%=MC.M_Etr_ValidIpAddr%>");/*alert("Please enter Valid IP address.");*****/
	formobj.IPB.focus();
	return false;
	} 
 if(isNaN(formobj.IPC.value) == true) {
	 alert("<%=MC.M_Etr_ValidIpAddr%>");/*alert("Please enter Valid IP address.");*****/
	formobj.IPC.focus();
	return false;
	} 
 if(isNaN(formobj.IPD.value) == true) {
	 alert("<%=MC.M_Etr_ValidIpAddr%>");/*alert("Please enter Valid IP address.");*****/
	formobj.IPD.focus();
	return false;
	} 
	formobj.server.value=formobj.IPA.value+"."+formobj.IPB.value+"."+formobj.IPC.value+"."+formobj.IPD.value;
		if (!(validate_col('Port',formobj.port))) return false;
	if(isNaN(formobj.port.value) == true) {
	alert("<%=MC.M_Err_EtrValid_PortNum%>");/*alert("Error", "Please enter Valid port number.");*****/
	formobj.port.focus();
	return false;
	} 
	
		
		
	if (formobj.notification[0].checked==true){
			
			if ((formobj.notifyemail.value==null) || (formobj.notifyemail.value=="")){
			alert("<%=MC.M_Specify_ValidEmail%>");/*alert("Please specify valid email address.");*****/
			formobj.notifyemail.focus();
			return false;
		}
	}
 }

</SCRIPT>



<% String src="",server="",port="",notifyemail="",notification="",IPA="",IPB="",IPC="",IPD="",tempStr="";
   int fromIndex=1;
  HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{


src= request.getParameter("srcmenu");
VieDao viedao=new VieDao();
viedao.getsettings("setting");
server=viedao.getServer();
port=viedao.getPort();
notification=viedao.getNotification();
notifyemail=viedao.getNotifyemail();
if (notifyemail==null) notifyemail="";

StringTokenizer st = new StringTokenizer(server,".");
     IPA=st.nextToken();
     IPB=st.nextToken();
     IPC=st.nextToken();
     IPD=st.nextToken();

%>

<body>
<br>
<DIV > 

  <%

int pageRight = 0;

    String tab = request.getParameter("selectedTab");

	%>
	

    <P class="sectionHeadings"><%=LC.L_VieSet_Server%><%--VIE Settings>>Server*****--%></P>
    <jsp:include page="settingtabs.jsp" flush="true"/>


  
 
  <Form name = "settings" method="post" action="updateviesettings.jsp?scr=setting" >
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
    <input type="hidden" name="src" size = 15 value = "<%=src%>">
    <table width="400" cellspacing="0" cellpadding="0">
      <tr> 
        <td> 
          <P class = "defComments"> <br>
            <br>
            <%=MC.M_YouMaySpecify_ServerSett%><%--You may specify the HL7 Server settings to check the server status. Server settings*****--%>: </P>
        </td>
      </tr></table>
     <br>
      <table>
      <tr><td class="tdDefault" width="150" align="right"><%=LC.L_Host_IpAdd%><%--Hostname(IP Address)*****--%></td><td width="250"><input type="text" name="IPA" size="2" maxlength="3" value="<%=IPA%>"><b>.</b><input type="text" name="IPB" size="2" maxlength="3" value="<%=IPB%>"><b>.</b><input type="text" name="IPC" size="2" maxlength="3" value="<%=IPC%>"><b>.</b><input type="text" name="IPD" size="2" maxlength="3" value="<%=IPD%>"></td></tr>
      <tr><td  class="tdDefault" width="150" align="right"><%=LC.L_Port%><%--Port*****--%></td><td width="250"><input type="text" name="port" size="4" maxlength="6" value="<%=port%>"></td></tr>
    </table>
    <table width="400" cellspacing="0" cellpadding="0">
      <tr> 
      	   <td> 
          <P class = "defComments"> <br>
            <br>
             <%=MC.M_PlsSelYes_SpecifyInfo%><%--Please select ''Yes'' and specify notification email address,if you would like to receive daily message notification/event notification*****--%> </P>
        </td>
      </tr></table><br><table>
      	  <tr><td class="tdDefault" width="150" align="right"><%=LC.L_Enable_Notification%><%--Enable notification*****--%></td><td width="300" class="tdDefault" >
      <%if (notification.equals("Y")){%>
      <input type="radio" name="notification" value="Y" checked><%=LC.L_Yes%><%--Yes*****--%>&nbsp;&nbsp;&nbsp;
      <input type="radio" name="notification" value="N"> 
     <%}else{%> 	  
		<input type="radio" name="notification" value="Y"><%=LC.L_Yes%><%--Yes*****--%>&nbsp;&nbsp;&nbsp; 
		<input type="radio" name="notification" value="N" checked>      	 
     	 <%}%>
<%=LC.L_No%><%--No*****--%></td></tr>  </table>
       <table width="400" cellspacing="0" cellpadding="0">
      <tr> 
        <td height="10"> </td>
      </tr>
      <tr> 
        <td width="150" class="tdDefault" align="right"> <%=LC.L_Notification_Email%><%--Notification Email*****--%>:   </td>
        <td> 
          <input type="text" name="notifyemail" size = 20 value = "<%=notifyemail%>">
        </td>
      </tr>
	  </table>
	  <table width="400" cellspacing="0" cellpadding="0">
      <tr> 
        <input type="hidden" name="server" value="<%=server%>">
          <P class = "defComments"> <br>
            <br>
            </P>
      </tr>
    </table>	
	
    <table width="400" cellspacing="0" cellpadding="0">
	  <tr > 
        <td width="1000" class="tdDefault" align="right"> 
		 
	   </td>
	   <td>
   <input type="image" src="../images/jpg/Submit.gif" onClick = "return validate(document.settings)" align="absmiddle" border="0">	

	   </td>
	   	    <td>
   				<button onClick="javascript:self.close()"><%=LC.L_Close%></button>
		   </td>
	</tr>
	</table>
	


    <br>
    <table width="400" >
      <tr> 
        <td align="right"> 
          <%

    if (pageRight > 4 )

	{	

%>
          <!--<input type="Submit" name="submit" value="Submit">-->
	  <!--<input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle" border="0">	-->

          <%

	}

else

	{

%>
          <!--<input type="Submit" name="submit" value="Submit" DISABLED>-->
          <%

	}

%>
        </td>
      </tr>
    </table>
  </Form>
 <%}else{%>		 	
<br>
<br>
<br>
<br>
<br>

<p class = "sectionHeadings" align = center> <%=MC.M_AppTimeout_Relogin%><%--Application Timed out. Please relogin.*****--%> </p>
<script>window.self.close();</script>
	<!--<META HTTP-EQUIV=Refresh CONTENT="3; URL=viehome.jsp">-->

<%}%>



</body>

</html>

