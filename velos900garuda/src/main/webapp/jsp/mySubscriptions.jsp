<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_PrsnlzAcc_TrialsSubsc%><%--Personalize Account >> New Trials Subscription*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
	<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
    	String tab = request.getParameter("selectedTab");
	    int pageRight = 0;
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

	    //pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MNOTIFY"));
		pageRight = 7;
		
		if (pageRight > 0 )
		{
			String userId = (String) tSession.getValue("userId");
			StudyNotifyDao sDao = new StudyNotifyDao();
			sDao.getUserSubs(Integer.parseInt(userId),"tarea");		
			ArrayList studyNotifyIds = sDao.getStudyNotifyIds(); 
			ArrayList studyNotifyTAreas = sDao.getStudyNotifyTArea(); 
			ArrayList studyNotifyKeywords = sDao.getStudyNotifyKeywords(); 
			String studyNotifyId = null;
			String studyNotifyTArea= null;
			String studyNotifyKeyword = null;
			int len = studyNotifyIds.size();
			int counter = 0;
			%>
<DIV class="tabDefTopN" id="divtab"> 
		
			<jsp:include page="personalizetabs.jsp" flush="true"/>
</DIV>			
<DIV class="tabDefBotN" id="divbot"> 
	  <div class="tmpHeight"></div>
			<Form name="form1" method="post" action="studySearchAfterLogin.jsp">
				<table width="100%"  cellspacing="0" cellpadding="0" border="0">		 
					<tr>
						<td width="30%"><%=MC.M_EtrKwrd_SrchClinicalTrail%><%--Enter keywords to search a clinical trial that has been broadcast within your network*****--%>
						</td>
						<td width="70%"><Input type="text" name="search"> <button type="submit"><%=LC.L_Search%></button>
						</td>
					</tr>
					<tr>
						<td><p class="defcomemnts"><i>(<%=MC.M_EtrKeywords_ByComma%><%--Enter one or more keywords separated by a comma*****--%>)</i></p>
						</td>
				<td>&nbsp;</td>
					</tr>
				</table>
		        <input type="hidden" name="searchOn" value="keyword">				
				 <input type="hidden" name="selectedTab" value=<%=tab%>>
			 	<input type="hidden" name="srcmenu" value=<%=src%>>
			</Form>
			<Form name="subs" method="post" action="" onsubmit="">
			    <table width="100%" cellspacing="0" cellpadding="0" border="0" >
				    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr > 
				        <td width = "50%"> 
					        <P class = "defComments"> <%=MC.M_NotficsRecieved_ThrpAreas%><%--You will receive notifications for <%=LC.Std_Studies_Lower%> added in the following Therapeutic Areas*****--%>: </P>
				        </td>
				    
				        <td width = "50%" class="rhsFont"> 
					        <p align="right"> <A href="subscribe.jsp?mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>"><%=MC.M_ThrpNotfic_Subscribe%><%--Subscribe to Notifications for other Therapeutic Areas*****--%></A> 
							</p>
				        </td>
				    </tr>
				 <!--    <tr> 
				        <td height="5"></td>
				        <td height="5"></td>
				    </tr>  -->
				</table>
			    <table class="outline midAlign" width="99%" cellspacing="0" cellpadding="0" border="0">
				    <tr> 
				        <th width="30%"> <%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%> </th>
				    </tr>
				    <%
				    for(counter = 0;counter<len;counter++)
					{	
						studyNotifyId = studyNotifyIds.get(counter).toString();
						studyNotifyTArea =((studyNotifyTAreas.get(counter)) == null)?"-":(studyNotifyTAreas.get(counter)).toString();
						studyNotifyKeyword =((studyNotifyKeywords.get(counter)) == null)?"-":(studyNotifyKeywords.get(counter)).toString();
						if ((counter%2)==0) {
						%>
						    <tr class="browserEvenRow"> 
				        <%
						}
						else{
						%>
						    <tr class="browserOddRow"> 
				        <%
						}
						%>
				        <td><%= studyNotifyTArea %> </td>
					</tr>
				    <%
					}
					%>
				</table>
			</Form>
			<%
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<DIV class="mainMenu" id = "emenu"> 
	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</DIV>
</body>
</html>

