<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=LC.L_Labs%><%--Labs*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="studyProt" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="labJB" scope="page" class="com.velos.eres.web.lab.LabJB" />
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
</head>


<script>
function openwindow(val)
{

windowname=window.open("Pcategory.jsp?from=lookup"+val,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
windowname.focus();

}
function openlookup(){
windowname=window.open("getlookup.jsp?viewId=1&form=patlab&dfilter=&keyword=labname|category~testId|grade" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, ");
windowname.focus();
}
function openwindowcalc(nciVersion,pkey)
{
	windowname=window.open("getAdvToxicity.jsp?nciVersion=" + nciVersion + "&pkey=" + pkey + "&parentform=patlab&gradefld=grade&advnamefld=advName&dispfield=toxicity" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
	//windowname=window.open("getAdvToxicity.jsp?nciVersion=" + nciVersion ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
	windowname.focus();

}
function validate(formobj){
if (!(validate_col('Lab Results Date',formobj.labdate))) return false

var fdaVal = document.getElementById("fdaStudy").value;
if (fdaVal == "1"){
	if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
}

if (!(validate_col('e-Signature',formobj.eSign))) return false

if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
   if(isNaN(formobj.tresult.value) == true) {
	alert("<%=MC.M_InvalidTestRes_UseLngANum%>");/*alert("Invalid Test Results.Please use Long Test results for alpha-numeric data.");*****/
	formobj.tresult.focus();
	return false;
   }
 if (formobj.tresult.value.length>0){
 	if (formobj.longresult.value.length>0){
	alert("<%=MC.M_ResultsEntry_NotAlw%> ");/*alert("Test Results entry not allowed in 'Test Results' and 'Long Test Results' simultaneously. Please use one of them for results. ");*****/
	formobj.tresult.focus();
	return false;
	}
}
if (formobj.tresult.value.length>0)
  {
          formobj.testtype.value="NM";
	  }
      else {
      formobj.testtype.value="TX";
      }

 }



</script>
<% String src;

src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%



   HttpSession tSession = request.getSession(true);



   if (sessionmaint.isValidSession(tSession))



	{

  			int personPK = 0;
			String pk_lab="";
  			String patientId = "";
			String dob = "";
			String gender = "";
			String genderId = "";
			String yob = "";
			String age = "";

			int labPageRight = 0;

			GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
    		labPageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATLABS"));

			Calendar cal1 = new GregorianCalendar();
			String abnresult="",tstatus="";
			personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
			String pkey = request.getParameter("pkey");
			person.setPersonPKId(personPK);
		 	person.getPersonDetails();
			patientId = person.getPersonPId();
			genderId = person.getPersonGender();
			dob = person.getPersonDob();
			gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
			//Set the Value of Id to the bean
			pk_lab=request.getParameter("pk_lab");
			labJB.setId(EJBUtil.stringToNum(pk_lab));
			if (gender==null){ gender=""; }

			//modified on 040405 for fixing the Bug # 2063

			if(!(dob == null) && !(dob.equals(""))){
				yob = dob.substring(6,10);
				age = (new Integer(cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob))).toString();
			} else {
				age = "-";
			}

	String mode = null;

	String uName = (String) tSession.getValue("userName");

	String userIdFromSession = (String) tSession.getValue("userId");

	String acc = (String) tSession.getValue("accountId");

	String patientCode = request.getParameter("patientCode");
	patientCode=(patientCode==null)?"":patientCode;
	String fromLab=request.getParameter("fromlab");
	fromLab=(fromLab==null)?"":fromLab;
	String patProtId=request.getParameter("patProtId");
	//String patientName = request.getParameter("patientName");
     patProtId=(patProtId==null)?"":patProtId;
     //in case string 'null'
     patProtId=(patProtId.equals("null"))?"":patProtId;




	String testId="";
	String perId="";
	String testDate="";
	String testName="";
	String testResult="";
	String testType="";
	String testUnit="";
	String testRange="";
	String testLLN="";
	String testULN="";
	String siteId="";
	String testStatus="";
	String accnNum="";
	String testGroupId="";
	String abnrFlag="";
	String patprotId="";
	String eventId="";
	String creator="";
	String modifiedBy="";
	String ipAdd="";
	String erName="";
	String erGrade="";
	String notes="";
	String longResult="",unitStr="";
	String tempToxic="",stdPhase="",stdPhaseDD="",studyDD="";
	ArrayList labUnits=null;
	labJB.getLabDetails();
	testId=labJB.getTestId();
	testDate=labJB.getTestDate();
	testResult=labJB.getTestResult();
	if (testResult==null) testResult="";
	testName=request.getParameter("testName");
	testName=StringUtil.decodeString(testName);
	testType=labJB.getTestType();
	testUnit=labJB.getTestUnit();
	if (testUnit==null) testUnit="";
	testLLN=labJB.getTestLLN();
	if (testLLN==null) testLLN="";
	testULN=labJB.getTestULN();
	if (testULN==null) testULN="";
	testStatus=labJB.getTestStatus();
	accnNum=labJB.getAccnNum();
	if (accnNum==null) accnNum="";
	testGroupId=labJB.getTestgroupId();
	abnrFlag=labJB.getAbnrFlag();
	patprotId=labJB.getPatprotId();
	patprotId=(patprotId==null)?"":patprotId;
	erName=labJB.getErName();
	if (erName==null) erName="";
	erGrade=labJB.getErGrade();
	if (erGrade==null) erGrade="";
	stdPhase=labJB.getStdPhase();
	stdPhase=(stdPhase==null)?"":stdPhase;
	String studyId=request.getParameter("studyId");
	studyId=(studyId==null)?"":studyId;

	CodeDao cdLabs = new CodeDao();
	CodeDao cdLabs1=new CodeDao();
	CodeDao cdLabs2=new CodeDao();

	cdLabs.getCodeValues("abflag");
	abnresult=cdLabs.toPullDown("abnresult",EJBUtil.stringToNum(abnrFlag));
	cdLabs1.getCodeValues("tststat");
	tstatus=cdLabs1.toPullDown("tstatus",EJBUtil.stringToNum(testStatus));
	cdLabs2.getCodeValues("stdphase");
	stdPhaseDD=cdLabs2.toPullDown("stdPhase",EJBUtil.stringToNum(stdPhase));
	PatStudyStatDao stDao=studyB.getPatientStudies(personPK,EJBUtil.stringToNum(userIdFromSession));
	ArrayList arStudyIds=stDao.getStudyIds();
	ArrayList arStudyNums=stDao.getStudyNums();

	//String patientName = request.getParameter("patientName");
	String selStudyId = labJB.getStudyId();
	studyB.setId(StringUtil.stringToNum(selStudyId));
	studyB.getStudyDetails();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;
	
	studyId=(studyId==null)?"":studyId;
	studyDD = EJBUtil.createPullDown("selstudyId", EJBUtil.stringToNum(selStudyId), arStudyIds , arStudyNums );
	page=request.getParameter("page");
	LabDao labDao=new LabDao();
	labDao=labJB.getClobData(pk_lab);
	notes=labDao.getNotes();
	longResult=labDao.getLongResult();
	labDao=labJB.retLabUnit(testId);
	labUnits=labDao.getLabUnits();
	unitStr=EJBUtil.createPullDownWithStr("unit", testUnit,labUnits,labUnits );
	if (erGrade.length()==0)
	tempToxic="";
	else
	 tempToxic="Grade :"+ erGrade + " " + erName;


	//added by JM on 04Mar05

	String filEnrDt=request.getParameter("filterEnrDate");
	filEnrDt=(filEnrDt==null)?"":filEnrDt;
	String groupId=request.getParameter("groupName");
	groupId=(groupId==null)?"":groupId;
	String labText=request.getParameter("labname");
	labText=(labText==null)?"":labText;
	String selStudyId222=request.getParameter("selStudyId");
	selStudyId222=(selStudyId222==null)?"":selStudyId222;
	String abnText=request.getParameter("abnresult1");
	abnText=(abnText==null)?"":abnText;
	String pagenum=request.getParameter("pagenum");


	//JM: 07Jul2009: #INVP2.11
	String specimenPk=request.getParameter("specimenPk");
	specimenPk=(specimenPk==null)?"":specimenPk;

	String calldFrom=request.getParameter("calledFromPg");
	calldFrom=(calldFrom==null)?"":calldFrom;





%>


<body>
<DIV class="formDefault"  id="div1">

<form name="patlab" id="patlabmodifyfrm" method="post"  action="updatelab.jsp" onSubmit="if (validate(document.patlab)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<P class = "userName"> <%= uName %> </P>

<% if (page.equals("patient")){%>
<P class="sectionHeadings"> <%=MC.M_MngPat_PatLabs%><%--Manage <%=LC.Pat_Patients%> >> <%=LC.Pat_Patient%> Labs*****--%> </P>
<%}else if (page.equals("specimen")){%>
<P class="sectionHeadings"> <%=MC.M_MngInv_SpmenLabs%><%--Manage Inventory >> Specimen >> Labs*****--%><BR> </P>
<%}%>

<!--added by JM on 04Mar05-->

<input type="hidden" name="filterEnrDate" value="<%=filEnrDt%>">
<input type="hidden" name="groupName" value="<%=groupId%>">
<input type="hidden" name="labname" value="<%=labText%>">
<input type="hidden" name="selStudyId" value="<%=selStudyId222%>">
<input type="hidden" name="abnresult1" value="<%=abnText%>">
<input type="hidden" name="pagenum" value="<%=pagenum%>">
<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">




<input type="hidden" name="pkey" value="<%=pkey%>">
<input type="hidden" name="patientCode" value="<%=patientCode%>">
<input type="hidden" name="patProtId" value=<%=patProtId%>>
<input type="hidden" name="srcmenu" value=<%=src%>>
<input type="hidden" name="labpk" value="<%=pk_lab%>">
<input type="hidden" name="studyId" value="<%=studyId%>">
<input type="hidden" name="fromlab" value="<%=fromLab%>">
<input type="hidden" name="mode" value="M">
<input type="hidden" name="testcatId" size="15" value="<%=testGroupId%>">
<input type = "hidden" maxLength="30" size="15" name="grade" value= "<%=erGrade%>" >
<input type = "hidden" maxLength="200" size="15" name="advName" value= "<%=erName%>" >
<input type = "hidden" maxLength="200" size="15" name="testtype" value= "<%=testType%>" >

<input type="hidden" name="page" value=<%=page%>>


<input type="hidden" name="specimenPk" value="<%=specimenPk%>">
<input type="hidden" name="calledFromPg" value="<%=calldFrom%>">

<% if (page.equals("patient")){%>
<!-- SV, 8/2/04, fix for bug #1343, moved the jsp include below the section name above, to be consistent with the other screens -->
<jsp:include page="patienttabs.jsp" flush="true"/>
<%}else if (page.equals("specimen")){%>
	<jsp:include page="inventorytabs.jsp" flush="true">
	<jsp:param name="selectedTab" value="1"/>
	<jsp:param name="specimenPk" value="<%=specimenPk%>" />
	</jsp:include>
<%}%>
<br>

<table width="100%" class="basetbl">
<tr>
<td width="15%"><%=LC.L_Lab_ResultsDate%><%--Lab Results Date*****--%><FONT class="Mandatory">* </FONT></td>
<%-- INF-20084 Datepicker-- AGodara --%>
<td width="35%"> <input type="text" name="labdate" size="10" value="<%=testDate%>" readonly class="datefield" ></td>
<td width="50%"><%=LC.L_Lab_Name%><%--Lab Name*****--%><input type="text" name="labname" size="15" value="<%=testName%>" readonly>
</td>
</tr>
<tr>
<td width="15%"><%=LC.L_Test_Result%><%--Test Result*****--%> </td>
<td width="35%"><input type="text" name="tresult" size="3" value="<%=testResult%>">&nbsp;&nbsp;<%=LC.L_Unit%><%--Unit*****--%>&nbsp;&nbsp;<%=unitStr%>
<!--<input type="text" name="unit" size="3" value=<%=testUnit%>>--></td>
<td width="50%"><%=LC.L_Range_Lln%><%--Range:LLN*****--%>
<input type="text" name="LLN" size="3" value="<%=testLLN%>">&nbsp;&nbsp;<%=LC.L_Uln%><%--ULN*****--%>&nbsp;&nbsp;<input type="text" name="ULN" size="3" value="<%=testULN%>"></td>
</tr><tr>
<td width="15%"><%=LC.L_Long_TestResult%><%--Long Test Result*****--%> </td>
<td colspan="8" ><TEXTAREA name="longresult" cols="60" rows="4"><%=longResult%></TEXTAREA>
</td></tr>
<tr>
<tr>
<td width="15%"><%=LC.L_Accession_Number%><%--Accession Number*****--%> </td>
<td width="35%"><input type="text" name="accnumber" size="15" value="<%=accnNum%>"></td>
<td width="50%"><%=LC.L_Toxicity%><%--Toxicity*****--%>
<input type="text" name="toxicity" size="15" value="<%=tempToxic%>"><A href=# onClick="openwindowcalc('2.0',<%=pkey%>);"><%=LC.L_Calculate%><%--Calculate*****--%></A></td>
</tr>

<td width="15%"><%=LC.L_Lab_Status%><%--Lab Status*****--%></td>
<td width="60%" colspan="5"><%=tstatus%><!--<input type="text" name="tstatus" size="15">--></td>
</tr>
<tr>
<td width="15%"><%=LC.L_Result_Type%><%--Result Type*****--%></td>
<td width="35%"><%=abnresult%><!--<input type="text" name="abnresult" size="15">--></td>
</tr>
<tr><td><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%></td><td><%=studyDD%></td><td><%=LC.L_Std_Phase%><%--<%=LC.Std_Study%> Phase*****--%>&nbsp;<%=stdPhaseDD%></td></tr>
<tr>
<td width="15%"><%=LC.L_Notes%><%--Notes*****--%> </td>
<td colspan="8"><TEXTAREA name="notes" cols="60" rows="4"><%=notes%></TEXTAREA>
</td></tr>
<tr>
    <td width="20%"><%=LC.L_ReasonForChangeFDA%>
      	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
    </td>
	<td width="60%"><textarea id="remarks" name="remarks" rows="3" cols="60" MAXLENGTH="4000"></textarea></td>
</tr>
</table>

 <% String showSubmit ="";
	 if (labPageRight >= 6) { showSubmit="Y";} else {showSubmit="N";} %>


<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="patlabmodifyfrm"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
</jsp:include>
<br>
</form>

	</DIV>
<DIV class="mainMenu" id = "emenu">

  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</DIV>

</body>
		<%}
	else { %>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</html>
