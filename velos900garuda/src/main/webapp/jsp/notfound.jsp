<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Velos_Eres%><%--Velos eResearch*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<jsp:useBean id="homepage" class="com.velos.eres.service.util.SearchHomepageXml" scope="session" />

<% 
	Configuration.readSettings();
	homepage.setRoot(Configuration.ERES_HOME+"HomePage.xml"); 
%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<script>
function openwin1() {
      window.open("loggingin.htm","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=500")
;}
</script>


<% String srcMenu;
String scHeight = "";
String scWidth = "";
srcMenu= request.getParameter("src");

scHeight = request.getParameter("scHeight");
scWidth = request.getParameter("scWidth");

if (scHeight == null)
	{
		scHeight = "600";
		scWidth = "800";	
		}

%>
<jsp:include page="homepanel.jsp" flush="true"> 
</jsp:include>
<jsp:param name="scHeight" value="<%=scHeight%>"/>
<jsp:param name="scWidth" value="<%=scWidth%>"/>
   

<body id = "home">
<SCRIPT LANGUAGE="JavaScript">
<!--
function OpenCertDetails()
	{
	thewindow = window.open('https://www.thawte.com/cgi/server/certdetails.exe?code=USVELO5', 'anew', config='height=400,width=450,toolbar=no,menubar=no,scrollbars=yes,resizable=no,location=no,directories=no,status=yes');
	}
// -->
</SCRIPT>

<DIV class="eresLogin" id=div1 style="border-style:none;"> 
<form method="post" action="login.jsp?&scWidth=<%=scWidth%>&scHeight=<%=scHeight%>">
  <!-- <div class=login id="emenu"> -->
	  <P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <B><%=LC.L_Login_Here%><%--Login Here*****--%>...</B></P>
	  <table width="700" cellpadding="0" cellspacing="0"  border="0">
		  <tr>
	  	  <td width=200 id=tddefinition><%=MC.M_ProLog_BrwsWebsite%><%--Problem logging in? <BR><BR>You may need to upgrade your Internet browser to support the security requirements of our website*****--%>
		  <BR><BR><A href="#" onClick=openwin1()><%=LC.L_ClkHere_LrnMore%><%--Click here</A> to learn more*****--%>
		</td>
	    <td width=20>&nbsp;</td>
		<td>  
		  <table width="250" cellpadding="0" cellspacing="0"  border="0">
		  <tr>
		   <td id=trcolor width=21 align = left> <img src= "../images/jpg/topleft_login.jpg" ></td>
        	<td id=trcolor colspan=2 width="206">&nbsp;</td>
	        <td id=trcolor width="23" align = right><img src="../images/jpg/top_login.jpg"></td>
			</tr>
			<tr>
	        <td id=trcolor width="50">&nbsp;</td>
    	    <td id=trcolor width="90"><%=LC.L_User_Name%><%--User Name*****--%></td>
        	<td id=trcolor width="90">
          	<input type="text" name="username" size = 13 MAXLENGTH = 20>
			</td>
			<td id=trcolor width="23">&nbsp;</td>
        </td>
      </tr>
      <tr height = 5> 
        <td id=trcolor width="50">&nbsp;</td>
        <td id=trcolor width="90"  align="center">&nbsp; </td>
        <td id=trcolor width="90"  align="center">&nbsp; </td>
		<td id=trcolor width="23">&nbsp;</td>
      </tr>
      <tr > 
        <td width="50" id=trcolor>&nbsp;</td>
        <td width="90" id=trcolor><%=LC.L_Password%><%--Password*****--%></td>
        <td width="90" id=trcolor>
		  <input type="password" name="password" size = 13 MAXLENGTH = 20>
        </td>
		<td id=trcolor width="23">&nbsp;</td>
      </tr>
      <tr height = 5 > 
        <td width="50" id=trcolor>&nbsp;</td>
        <td width="90" id=trcolor> &nbsp;</td>
        <td width="90" id=trcolor> &nbsp;</td>
		<td id=trcolor width="23">&nbsp;</td>
      </tr>
      <tr>  
        <td align="right" width=200 colspan = 3 id=trcolor> 
        <!--  <input type="submit" name="submit" value = "Login"> -->
	  <input type="image" src="../images/jpg/Login.gif" onClick = "form.submit()" align="absmiddle"  border="0">	
        </td>
        <td id=trcolor>&nbsp;</td>
      </tr>
      <tr height = 5> 
        <td width="50" id=trcolor>&nbsp;</td>
        <td width="90" id=trcolor align="center">&nbsp; </td>
        <td width="90"  id=trcolor align="center">&nbsp; </td>
		<td id=trcolor width="23">&nbsp;</td>
      </tr>
      <tr > 
        <td id=trcolor  width="50">&nbsp;</td>
        <td id=trcolor  colspan=3> <font size=1><%=LC.L_New_User%><%--New User?*****--%></font> <A href="register.jsp"><%=LC.L_Sign_Up%><%--Sign Up!*****--%> </a> </td>
      </tr>
      <tr>
		 <td id=trcolor width=21 valign="absbottom"> <img src= "../images/jpg/bottomleft_login.jpg" align="absbottom"></td>
        <td id=trcolor colspan=2 width="358">&nbsp;</td>
        <td id=trcolor width="23" valign = absbottom  align="center"><img src="../images/jpg/bottom_login.jpg"></td>
      </tr>
	  </table>
	  </td>
	    <td width=20></td>
	  	  <td width=200 id=tddefinition><%=MC.M_LoginInfo_CaseSensitive%><%--Please remember that your login information is case sensitive*****--%> <br><br>
		  <A href="forgotpwd.jsp?mode=N&scHeight=<%=scHeight%>&scWidth=<%=scWidth%>"> <%=LC.L_Forgot_Pwd%><%--Forgot Password?*****--%> </a>
		</td>
	</tr>	 
	<TR>
	<tr height=20><TD COLSPAN=5></td></tr>
	<TD COLSPAN=5 align=center id=remarks><%=MC.M_WebSiteUses_128BitEncr%><%--This web site uses 128-bit encryption to secure all data transmitted to and from this site.*****--%></td>
	</TR>
	<tr height=20><TD COLSPAN=5></td></tr>	
	<TR>
	<TD COLSPAN=5 align=center id=tddefinition>
		<P>
			<A href="javascript:OpenCertDetails()">
			<IMG SRC="../images/jpg/stamp.gif" BORDER=0 ALT='<%=MC.M_ClkHere_MoreDets%><%--Click here for more details*****--%>'></A>
		</P>
	</td></tr>
    </table>
  </div>
    <div> 
    <jsp:include page="homebottompanel.jsp" flush="true"/>
  </div>

  <div id="emenu"></div>
  <div id="secdiv"></div>
 
</form>
	

</body>
</html>

