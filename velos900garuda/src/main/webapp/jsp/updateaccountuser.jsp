<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.Configuration" %>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 


<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="accountB" scope="page" class="com.velos.eres.web.account.AccountJB" />
<jsp:useBean id="msgcntrB" scope="page" class="com.velos.eres.web.msgcntr.MsgcntrJB" />
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%
   int ret1=-1;
   int ret2=-1;
   String msg="";
   int userId=0;
   int msgId = 0;
   int rows = 0;
   String userLogin="";
   String userPassword = "";
   String userStatus = "";
   String userEmail = "";
   int accountId = 0;
   String accountType = "";
   String messageFrom = "";
   String messageText = "";
   String messageSubject = "";
   String messageHeader = "";
   String messageFooter = "";		
   String completeMessage = "";

   String messageTo = "";
   String accStartDt = "";
   String userCode = "";
   String prevStatus= "";	
   String timeZone="";
   String eSign="";	
   ArrayList rights = new ArrayList();
   int totrows = 0;
   String messageSubtext = "";
   
   
	
   
   GrpRightsJB grpRightsB = new GrpRightsJB();

   String userResetPass="";
   String usereResetSign="";
   String siteUrl = "";
   //int rows = 0;
   
    String userMailStatus = "";
	int metaTime = 2;
   
// Added by Rajeev K - 02/18/04
// Begin
// get site URL
	rows = 0;
	CtrlDao urlCtrl = new CtrlDao();	
	urlCtrl.getControlValues("site_url");
	rows = urlCtrl.getCRows();
   	if (rows > 0)
	   {
	   	siteUrl = (String) urlCtrl.getCValue().get(0);
	   }
// End

   userId = EJBUtil.stringToNum(request.getParameter("userId"));
   userStatus = request.getParameter("userStatus");
   userEmail = request.getParameter("userEmail");
   userLogin = request.getParameter("userLogin");
   
//   userPassword = request.getParameter("userPassword");
   
   msgId = EJBUtil.stringToNum(request.getParameter("msgId"));
   accStartDt = request.getParameter("accStartDt");
   String storage = request.getParameter("storage");
   String totalUsers = request.getParameter("totalusers");
// JM:
   String totalPortals = request.getParameter("totalportals");
   
   String accNote = request.getParameter("accNote");
   String ldapEnabled=request.getParameter("ldapenabled");
   ldapEnabled=(ldapEnabled==null)?"":ldapEnabled;
   String wsEnabled=request.getParameter("wsenabled");
   wsEnabled=(wsEnabled==null)?"":wsEnabled;
   timeZone=request.getParameter("timeZone");
   totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows
   String modules[]; String moduleStr = ""; 
   
   double randomNumber=Math.random();
   String randomNum=String.valueOf(randomNumber);
   
   
   String autoGenStudy = request.getParameter("autoGenStudy");
   String autoGenPatient = request.getParameter("autoGenPatient");
   
   if (autoGenStudy != null)
	   autoGenStudy ="1";
   else
	   autoGenStudy ="0";
   
   if (autoGenPatient != null)
	   autoGenPatient ="1";
   else
	   autoGenPatient ="0";
   
   
   
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {	
   	VMailer vm = new VMailer();
	
	String supportEmail = Configuration.SUPPORTEMAIL;   
   ////////////////
   if (totrows > 1){

	modules = request.getParameterValues("modrights");

	for (int count=0;count<totrows;count++){

		moduleStr = moduleStr + modules[count];

		//out.print("Count" +count +" Rights" +rght);

	}

}else{

	moduleStr = request.getParameter("modrights");

}

  System.out.println(moduleStr); 
   ///////////////////
   
   
   

   //String ipAdd = (String) tSession.getValue("ipAdd");
//   String usr = (String) tSession.getValue("userId");

   userB.setUserId(userId);
   userB.getUserDetails();
   
   userPassword = userB.getUserPwd(); 

   String defaultGroup = userB.getUserGrpDefault();
   eSign=userB.getUserESign();   
   
   
   userCode = userB.getUserCode((new Integer(userId)).toString());

   //userB.setModifiedBy(usr);
   //userB.setIpAdd(ipAdd);
   prevStatus=userB.getUserStatus();
   
 	//userB.setTimeZoneId(timeZone);
	
   userB.setUserStatus(userStatus);
   
   // set the attempt count
   if( prevStatus.equals("D") && (!userStatus.equals("J")) )
   {
	   userB.setUserAttemptCount("0");
	   userResetPass=randomNum.substring(2,10);
	   usereResetSign=randomNum.substring(11,15);
	   userB.setUserPwd(Security.encryptSHA(userResetPass));
	   userB.setUserESign(usereResetSign);
	   messageSubtext=" \n"+LC.L_New_Pwd+"    : " + userResetPass +  "\n"+LC.L_New_Esign+" : " + usereResetSign + "\n"+LC.L_App_Url+" : " + siteUrl;/*messageSubtext=" \nNew Password    : " + userResetPass +  "\nNew e-Signature : " + usereResetSign + "\nApplication URL : " + siteUrl;*****/
   
   }
      
   ret1 = userB.updateUser();

   msgcntrB.setMsgcntrId(msgId);
   msgcntrB.getMsgcntrDetails();
   
   msgcntrB.setMsgcntrStatus("R");

   //msgcntrB.setModifiedBy(usr);
   //msgcntrB.setIpAdd(ipAdd);

   msgcntrB.updateMsgcntr();

   accountId = EJBUtil.stringToNum(request.getParameter("accountId"));
   accountType =  request.getParameter("userAccType");

   accountB.setAccId(accountId);
   accountB.getAccountDetails();

   

   accountB.setAccType(accountType);
   accountB.setAccStartDate(accStartDt);
   accountB.setAccMaxUsr(totalUsers);
// JM:
   if (StringUtil.isEmpty(totalPortals))
   {
   	totalPortals = "-1";
   }
   accountB.setAccMaxPortal(totalPortals);

   accountB.setAccMaxStorage(storage);
   accountB.setAccNote(accNote);
   accountB.setLdapEnabled(ldapEnabled);
   
   accountB.setWSEnabled(wsEnabled);
   
   accountB.setAccModRight(moduleStr);

   //accountB.setModifiedBy(usr);
   //accountB.setIpAdd(ipAdd);
   
   accountB.setAutoGenStudy(autoGenStudy);//KM
   accountB.setAutoGenPatient(autoGenPatient);

   ret2 = accountB.updateAccount();

   CtrlDao ctrlAppRights = new CtrlDao();

   ctrlAppRights.getControlValues("app_rights");
   ArrayList feature =  ctrlAppRights.getCValue();
   ArrayList rtSeq = ctrlAppRights.getCSeq();

   String ftr = "";
   String rtStr = "";
   int seqVal = 0;
   
   
   
   int rowsCount = ctrlAppRights.getCRows();
   for(int count=0;count<rowsCount;count++){
   
	ftr = (String) feature.get(count); 
    seqVal = ((Integer) rtSeq.get(count)).intValue();

	//check for special rights

	       	if (ftr.compareTo("MGRPRIGHTS") == 0)  
			{
			 rtStr = "6";
			}
			else if (ftr.compareTo("REPORTS") == 0)
			{
             rtStr = "6";
			}
			else if (ftr.compareTo("DSM") == 0)
			{
             rtStr = "4";
			}
			else
			{
			 rtStr = "7";
			}
			
	//end of check		

	
	//changed by sonia 29 may.
	//changed by sonia 21 aug 2002.
	
		
	if(accountType.equals("G"))
	{
	   rights.add(rtStr);
	} else if (accountType.equals("I") && (ctrl.getAccTypeRight(seqVal).indexOf("I") >= 0))
	{
	   rights.add(rtStr);
	}
	 else if (accountType.equals("E") && (ctrl.getAccTypeRight(seqVal).indexOf("E") >= 0))
	{
	   rights.add(rtStr);
	}else {
		rights.add("0");
	}
		
   }

   grpRightsB.setId(EJBUtil.stringToNum(defaultGroup));
   grpRightsB.getGrpRightsDetails();
   grpRightsB.setFtrRights(rights);

   //grpRightsB.setModifiedBy(usr);
   //grpRightsB.setIpAdd(ipAdd);

   grpRightsB.updateGrpRights();

   if ((ret1 == 0)&(ret2==0))
    {
	if(userStatus.equals("A"))
	{
	msg =  MC.M_Data_SvdSucc;/*msg =  "Data was saved successfully";*****/
	
	 if(prevStatus.equals("P"))	
	 {
		accountB.copyAccount(accountId);

        	messageTo = userEmail;
        	CtrlDao userCtrl = new CtrlDao();	
        	userCtrl.getControlValues("eresuser");
        	rows = userCtrl.getCRows();
        	if (rows > 0)
        	   {
        	   	messageFrom = (String) userCtrl.getCDesc().get(0);
        	   }
        	
        	messageSubject = MC.M_YourNew_VeEresAcc;/*messageSubject = "Your New Velos eResearch Account";*****/	
        	messageHeader ="\n"+MC.M_NewVelos_EresMem+"," ;/*messageHeader ="\nDear New Velos eResearch Member," ;*****/ 
        	messageText = "\n\n"+MC.M_AccApproved_InfoDisp+":\n\n"+LC.L_Login_Id+" : " + userLogin + "\n"+LC.L_Password+"        : <"+MC.M_Supp_EarlyEmail+">  \n"+LC.L_Esignature+"     : <"+MC.M_Supp_EarlyEmail+"> \n\n"+LC.L_App_Url+" : " + siteUrl + "\n\n"+MC.M_UsrPwd_PersnlzAcc+"\n\n"+MC.M_BeginVelos_EresSys+" " + siteUrl + MC.M_EtrLogin_ClkLogin+"\n**** "+LC.L_Important+" ****\n"+MC.M_PlsRemSecure_InfoAcc+"\n\n"+MC.M_YouQueFeed_FreeContact+" " + supportEmail + ".\n\n"+MC.M_ThankEres_CustVelosCa ;/*messageText = "\n\nWelcome to Velos eResearch. Your account has been approved. Your account information is displayed below:\n\nLogin ID : " + userLogin + "\nPassword        : <Supplied in earlier email>  \ne-Signature     : <Supplied in earlier email> \n\nApplication URL : " + siteUrl + "\n\nYou may of course change your password at any time should you have any concerns regarding the privacy of your information. To change your password, you will need to login with the above ID and password, go to the Personalize Account section and then change your password.\n\nTo begin using the Velos eResearch system, go to " + siteUrl + ", enter your login ID and password and click on the Login button.\n**** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nIf you have any questions or feedback, please feel free to contact us at " + supportEmail + ".\n\nThank you for your interest in Velos eResearch.\n\nVelos eResearch Customer Support.\n\nVelos, Inc.\n2201 Walnut Avenue, Suite 208\nFremont, CA. 94538" ;*****/
        	messageFooter = "\n\n"+MC.M_EmailContain_SecureInfo ;/*messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;*****/
        	completeMessage = messageHeader  + messageText + messageFooter ;

        	try{
			   	VMailMessage msgObj = new VMailMessage();
    				
        		msgObj.setMessageFrom(messageFrom);
        		msgObj.setMessageFromDescription(MC.M_VeResCustSrv);/*msgObj.setMessageFromDescription("Velos eResearch Customer Services");*****/
        		msgObj.setMessageTo(messageTo);
        		msgObj.setMessageSubject(messageSubject);
        		msgObj.setMessageSentDate(new Date());
        		msgObj.setMessageText(completeMessage);
        
        		vm.setVMailMessage(msgObj);
        		userMailStatus = vm.sendMail();
        		msg =  MC.M_DataSvdSucc_NotficSent;/*msg =  "Data was saved successfully and notification sent.";*****/
				metaTime = 2;
          	  }
        	 catch(Exception ex)
		      {
     			  userMailStatus = ex.toString();Object[] arguments = {userMailStatus}; 
      			 msg =VelosResourceBundle.getMessageString("M_DataSuccErr_CustSup",arguments)+ "</p>";/*msg =  "Data was saved successfully but there was an error in sending notification to the user.<br> Please contact Customer Support with the following message:<BR>" + userMailStatus + "</p>";*****/
  				 metaTime = 10;
		       }

			
    	 } //end of if - for previous status as pending
	 else if( prevStatus.equals("D")) //for previous status as deactivated
	 {
	 	////////////////////////////////
        	
        	messageTo = userEmail;
        
        	CtrlDao userCtrl = new CtrlDao();	
        	userCtrl.getControlValues("eresuser");
        	rows = userCtrl.getCRows();
        	if (rows > 0)
        	   {
        	   	messageFrom = (String) userCtrl.getCDesc().get(0);
        	   }
        	
        	messageSubject = MC.M_ReqChg_Pwd;/*messageSubject = "Request for change of password/esign";*****/	
        	messageHeader =MC.M_Dear_VeResMem+"," ;/*messageHeader ="Dear Velos eResearch Member," ;*****/ 
        
        	messageText = "\n\n"+MC.M_ReqPwdReset_NewPwd+"        : " + userLogin + messageSubtext + "\n\n"+MC.M_ChgPwdInfo_VeResImpInfo+" www.veloseresearch.com"+MC.M_EtrLogin_ClkLogin+"\n **** "+LC.L_Important+" ****\n"+MC.M_PlsRemSecure_InfoAcc+"\n\n"+MC.M_YouQueFeed_FreeContact+" " + supportEmail + ".\n\n "+MC.M_VelosEresCust_Supp208Ca ;/*messageText = "\n\nAs per your request your Password/e-Signature has been reset. Your new Password/e-Signature is:\n\nLogin ID        : " + userLogin + messageSubtext + "\n\nYou may of course change your Password/e-Signature at any time should you have any concerns regarding the privacy of your information. To change your Password/e-Signature, you will need to login with the above ID , go to the Personalize Account section and then change your Password/e-Signature.\n\nTo begin using the Velos eResearch system, go to www.veloseresearch.com, enter your login ID and password and click on the Login button.\n **** Important ****\nPlease remember that the above information needs to be kept in a secure place. Any one with access to the above information can access your account information.\n\nIf you have any questions or feedback, please feel free to contact us at " + supportEmail + ".\n\n Velos eResearch Customer Support.\n\nVelos, Inc.\n2201 Walnut Avenue, Suite 208\nFremont, CA. 94538" ;*****/
        
        	messageFooter = "\n\n"+MC.M_EmailContain_SecureInfo ;/*messageFooter = "\n\nThis email transmission and any documents, files or previous email messages attached to it may contain information that is confidential or legally privileged. If you are not the intended recipient or a person responsible for delivering this transmission to the intended recipient you are hereby notified that you must not read this transmission and that any disclosure, copying, printing, distribution or use of this transmission is strictly prohibited. If you have received this transmission in error, please immediately notify the sender by telephone or return email and delete the original transmission and its attachments without reading or saving in any manner." ;*****/
        	completeMessage = messageHeader  + messageText + messageFooter ;
        
        
        	try{
        
				VMailMessage msgObj = new VMailMessage();
    				
        		msgObj.setMessageFrom(messageFrom);
        		msgObj.setMessageFromDescription(MC.M_VeResCustSrv);/*msgObj.setMessageFromDescription("Velos eResearch Customer Services");*****/
        		msgObj.setMessageTo(messageTo);
        		msgObj.setMessageSubject(messageSubject);
        		msgObj.setMessageSentDate(new Date());
        		msgObj.setMessageText(completeMessage);
        
        		vm.setVMailMessage(msgObj);
        		userMailStatus = vm.sendMail(); 		
        
        		msg =  MC.M_DataSvdSucc_NotifySent;/*msg =  "Data was saved successfully and notification was sent.";*****/
 				metaTime = 2;
        	  }
        	 catch(Exception ex)
		      {
     			  userMailStatus = ex.toString();Object[] arguments1 = {userMailStatus}; 
     			 msg =VelosResourceBundle.getMessageString("M_DataSuccErr_CustSup",arguments1)+ "</p>";/*msg =  "Data was saved successfully but there was an error in sending notification to the user.<br> Please contact Customer Support with the following message:<BR>" + userMailStatus + "</p>";*****/
  				  metaTime = 10;
		       }

	    ///////////////////////////////	
	 }
	 
	} else {
		msg =  MC.M_Data_SvdSucc;/*msg =  "Data was saved successfully";*****/
	}
   }
   else {
	   msg =  MC.M_AccDets_SvdRet1+"=" + ret1 + " "+LC.L_Ret2+"= " + ret2;/*msg =  "and Account details not saved ret1=" + ret1 + " ret2= " + ret2;*****/
   }  
%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<P class="successfulmsg" align=center> <%=msg%></P>

<META HTTP-EQUIV=Refresh CONTENT="<%=metaTime%>; URL=velosmsgs.jsp?userStatus=<%=userStatus%>">
<%
}//end of if body for session


else

{

%>
 <jsp:include page="timeout_admin.html" flush="true"/>
<%

}

%>
</BODY>
</HTML>


