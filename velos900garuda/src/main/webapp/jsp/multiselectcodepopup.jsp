<!--this is a generic window which can be used for any type of codelist data
The calling window has to pass the form object name, html element names for settings the selected ids 
and text, code_type etc 
-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%String ptitle = request.getParameter("ptitle");%>
<title><%=VelosResourceBundle.getLabelString("L_Selct",ptitle)%><%--Select <%=ptitle%>*****--%></title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" >

	function getDescs(formobj)
	{

	selIds = new Array();
	selDescs = new Array(); //array of selected descs
	totrows = formobj.totalrows.value;
	checked = false;
	var k=0;

	if (totrows==1) {
		if (formobj.chkDesc.checked) {
			selValue = formobj.chkDesc.value;
			pos = selValue.indexOf("*");
			selIds[0] = selValue.substring(0,pos);
			selDescs[0] = selValue.substring(pos+1);
			
		}
	} else {
		j=0;
		for (i=0;i<totrows;i++) {
			if (formobj.chkDesc[i].checked) {				
				selValue = formobj.chkDesc[i].value;
				pos = selValue.indexOf("*");
				selIds[j] = selValue.substring(0,pos);
				selDescs[j] = selValue.substring(pos+1);
				j++;
				var len=j;
				
			} 	
		}		
	}

	openerForm = formobj.openerForm.value;
	hidEleName = formobj.hidEleName.value;
	eleName = formobj.eleName.value;	

if (document.layers) { 
	eval("window.opener.document.div1.document."+openerForm+"."+hidEleName+".value='"+selIds+"'");
	eval("window.opener.document.div1.document."+openerForm+"."+eleName+".value='"+selDescs+"'");	 
} else {
    eval("window.opener.document."+openerForm+"."+hidEleName+".value='"+selIds+"'" ) ;
	eval("window.opener.document."+openerForm+"."+eleName+".value='"+selDescs+"'");	
}

self.close();
	}
	
	
	
	</SCRIPT>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao"%><%@page import="com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<div class="popDefault" >

<%String heading = request.getParameter("heading");%>
 
<P class="sectionHeadings"> <%=VelosResourceBundle.getLabelString("L_Slct",heading,ptitle)%><%--<%=heading%> >> Select <%=ptitle%>*****--%> </P>
<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	 <jsp:include page="include.jsp" flush="true"/>
<% 	
  		String openerForm = request.getParameter("openerForm");
 		String hidEleName = request.getParameter("hidEleName");		
 		String eleName = request.getParameter("eleName");				
		
		int numIds = 0;
		String selectedIds="";
		String selectedNames="";

		selectedIds = request.getParameter("selectedIds");
		if (selectedIds == null) selectedIds = "";
	
		String codeType = request.getParameter("codeType");
			
		String desc="";
		String id="";
		int codeInd=0;
		ArrayList codeLstDesc=null;
		ArrayList codeLstId= null;
		
		CodeDao cd = new CodeDao();
		cd.getCodeValues(codeType);
		
		codeLstDesc = cd.getCDesc();
		codeLstId = cd.getCId();
		
		int codeLstLen=codeLstId.size();
		String[] j = new String[codeLstLen];

		StringTokenizer idsDesc = new StringTokenizer(selectedIds,",");
		numIds=idsDesc.countTokens();
		
		for(int h=0;h<codeLstLen;h++)
		{
			j[h]="";
		}
		
		int index=0;
		String strId= "";
		int  ind=0;
		int[] selIds=null;
		ArrayList arr =null;
		int cnt=0;
		for(cnt=0;cnt<numIds;cnt++)
			{
					strId=idsDesc.nextToken();
					codeInd = codeLstId.indexOf(Integer.valueOf(strId));					
					j[codeInd]=strId;
					ind++;

			}

		%>
<Form  name="formSelect" id="formSelectMulti" method="post" action="" onsubmit="if (getDescs(document.formSelect)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
   <input type="hidden" name="openerForm" value="<%=openerForm%>">
   <input type="hidden" name="hidEleName" value="<%=hidEleName%>">
   <input type="hidden" name="eleName" value="<%=eleName%>">
   
    <table width="40%" cellspacing="2" cellpadding="0" border=0 >
      <tr> 
		
			<th width="50%"><%=ptitle%></th>
        <th width="10%"> &nbsp;&nbsp;</th>
      </tr>
		<%if(codeLstLen==0)
		{
		}
		else
		{
		for(int i=0;i<codeLstLen;i++)
			{
			String checked= "0";
			desc = codeLstDesc.get(i).toString();
			id=codeLstId.get(i).toString();
			
			if(i%2==0)
			{%>

          <tr class=browserEvenRow>
			<%}
			   else
			{%>
				<tr class=browserOddRow>
			<%}

			

			   %>
			<td><%=desc%></td>
				<%
		
			  if( !j[i].equals(""))
				{ %>
			<td><input type="checkbox" name="chkDesc" value="<%=id%>*<%=desc%>" checked></td>
					  
				<%}  
			else{%>
			<td><input type="checkbox" name="chkDesc" value="<%=id%>*<%=desc%>" ></td>
				<%}%>
				

		   </tr>
	<%}	
		}%>
			
    </table>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="formSelectMulti"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	<Input type="hidden" name="totalrows" value=<%=codeLstLen%> >

</Form>

<%
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>

</html>

