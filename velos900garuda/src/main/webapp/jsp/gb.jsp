<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Account Groups</title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

</head>
<Script>

function openGroup(id,name,grprights)
{
	var src = document.group.src.value;
	var frompage =document.group.frompage.value;
	var mode =document.group.mode.value;
	group.action= "group.jsp?mode="+mode +"&grpId=" +id +"&srcmenu="+src +"&fromPage="+frompage;
	//alert(group.action);
	//this.location.replace("group.jsp?mode="+mode +"&grpId=" +id +"&srcmenu="+src +"&fromPage="+frompage)
	group.submit();
}

function assignRights(id,name,grprights)
{
	var src = document.group.src.value;
	var frompage =document.group.frompage.value;
	var mode =document.group.mode.value;
	group.action= "groupRights.jsp?mode="+mode +"&groupId=" +id +"&srcmenu="+src +"&fromPage="+frompage+"&groupName=" +name;
	group.submit();
}

function groupUsers(id,name,grprights)
{
	var src = document.group.src.value;
	var frompage =document.group.frompage.value;
	var mode =document.group.mode.value;
	group.action= "groupUsers.jsp?mode="+mode +"&srcmenu=" +src +"&groupId=" +id +"&grpName=" +name;
	group.submit();
}

</Script>
<% String src;
src= request.getParameter("src");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<body>
<br>
<jsp:useBean id="groupB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*"%>
<% int accountId=0;  
   int pageRight = 0;
   HttpSession tSession = request.getSession(true); 
%>
<DIV class="browserDefault" id="div1">
	<%
	if (sessionmaint.isValidSession(tSession))
	{

%>
	
	<form name="group" method=post action="gb.jsp">
<input type=hidden name=src value=<%=src%>>
<input type=hidden name=frompage value="groupbrowser">
<input type=hidden name=mode value="M">
	<% 

	   	String acc = (String) tSession.getValue("accountId");
	   	String uName = (String) tSession.getValue("userName");
	   	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	   	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
	   	accountId = EJBUtil.stringToNum(acc);
	  
	  
	   //	GroupDao groupDao = groupB.getByAccountId(accountId);
	//	   	ArrayList grpIds = groupDao.getGrpIds(); 
	//   	ArrayList grpNames = groupDao.getGrpNames();
	  // 	ArrayList grpDescs = groupDao.getGrpDescs();
	  
	  
	  
	   	String grpName = null;
	   	String grpDesc = null;
	   	//int len = grpIds.size();
	   	int counter = 0;
	   	if (pageRight > 0 )
		{
			Vector vec = new Vector();
			
			Column pkGrp = new Column("Group Id");
			pkGrp.hidden = true;
			
			vec.add(pkGrp);
			
			
			Column pkGrpName = new Column("Group Name");
			pkGrpName.funcName = "openGroup";
			pkGrpName.passAsParameter = true;
			pkGrpName.width = "25%"	;
			pkGrpName.align = "left"	;		
			vec.add(pkGrpName);
			
			Column grpDesc1 = new Column("Group Desc");
			grpDesc1.width = "45%" ;
			grpDesc1.align = "left"	;
			vec.add(grpDesc1);
			
			Column grprights = new Column("Grp Rights");
			grprights.hidden = true;
			vec.add(grprights);
			
			Column assgn  = new Column("Assign");
			assgn.value = "Assign Rights";
			assgn.funcName = "assignRights";
			assgn.width = "15%";
			assgn.align = "left";
			vec.add(assgn);
			
			Column users  = new Column("");
			users.value = "Group Users";
			users.funcName = "groupUsers";
			users.width = "15%";
			users.align = "left";
			vec.add(users);
			
			String sSQL = " select  pk_grp, GRP_NAME,GRP_DESC,grp_rights from er_grps where  FK_ACCOUNT = " + accountId ;				 
			String title ="	The list below displays the Groups that have already been created for your account:";
			
			String srchType=request.getParameter("srchType");
			String sortType=request.getParameter("sortType");
			String sortOrder=request.getParameter("sortOrder");

			request.setAttribute("sSQL",sSQL);

			request.setAttribute("srchType",srchType);
			request.setAttribute("vec",vec);
			request.setAttribute("sTitle",title);

			String s_scroll=request.getParameter("scroll");
			request.setAttribute("scroll",s_scroll);
			request.setAttribute("sortType",sortType);
			request.setAttribute("sortOrder",sortOrder);
		%>
			<P class = "userName"> <%= uName %> </P>
			<P class="sectionHeadings"> Manage Account >> Groups</P>
			
			<br>
 		<table width="100%" cellspacing="0" cellpadding="0" border="0" >
			<tr> 
			    <td width = "50%"> 
			        <P class="defComments">The list below displays the Groups that have already been created for your account:</P>
			    </td>
			    <td width="50%" align="right"> 
				<p>
				  <A href=grouplist.jsp?mode=N&srcmenu=tdmenubaritem2&fromPage=groupbrowser> Add multiple Groups</A>
				  <A href=group.jsp?mode=N&srcmenu=tdmenubaritem2&fromPage=groupbrowser> Add a New Group</A>
				  
			  </p>
			    </td>
		    </tr>
		</table>

			<jsp:include page="result.jsp?jsFile=searchJS" flush="true"></jsp:include> 
	</form>
				<%	
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	</DIV>
<div class ="mainMenu" id="emenu"> 
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>
