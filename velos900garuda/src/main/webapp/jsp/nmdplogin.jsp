<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.business.domain.AppMessages"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<title><%=MC.M_VelosEres_UsrLogin %><%-- Velos eResearch : User login*****--%></title>
<!-- Gopu : import the objects for customized filed -->
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.web.user.ConfigFacade" %>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<% 
	Configuration.readSettings();

	//modifed by Gopu for MAHI Enahncement on 3rd Mar 2005
//	ConfigFacade.getConfigFacade();

%>
<meta name="Keywords" content="" />
<link href="styles/nmdplogin.css" rel="stylesheet" type="text/css" />

</head>
<script>
function openwin1() {
      window.open("loggingin.htm","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=500")
;}
</script>


<%
String scHeight = "";
String scWidth = "";
List loginAppmsgs = new ArrayList();
//KN
String appver = acmod.getControlValue("app_version");
String siteAdmin = acmod.getControlValue("eresuser");



scHeight = request.getParameter("scHeight");
scWidth = request.getParameter("scWidth");

if (scHeight == null)
	{
		scHeight = "600";
		scWidth = "800";	
		}

%>

<body>
<%String mode=request.getParameter("mode");
mode=(mode==null)?"":mode;
String logName="";
String logUrl="";
if (mode.equals("popup"))
{
 logName=request.getParameter("logname");
 logName=(logName==null)?"":logName;
 logUrl=request.getParameter("logurl");
 logUrl=(logUrl==null)?"":logUrl;
 
 logUrl="user="+logName+",url="+logUrl;
}

loginAppmsgs = (List)application.getAttribute("loginAppMsgsList");
%>

<div class="wrapper">

<% if (!mode.equals("popup")) { %><div class="helix"></div><% } %>
    <div class="header">
    <div class="logo">
    <img src="images/logo-nmdp.gif" width="65" height="65" alt="<s:text name='garuda.label.login.logotitle'/> <%--EmTrax*****--%>" title="<s:text name='garuda.label.login.logotitle'/> <%--EmTrax*****--%>" />
    <img src="images/logo-bethematch.gif" width="190" height="33" alt="<s:text name='garuda.label.login.logotitle'/> <%--EmTrax*****--%>" title="<s:text name='garuda.label.login.logotitle'/> <%--EmTrax*****--%>" />
    </div>
    <div class="welcome">
	    <s:text name="garuda.label.login.welcometext"></s:text>
    </div>
    </div>
</div>
<form  name="login" method="post" action="login.jsp?&scWidth=<%=StringUtil.htmlEncodeXss(scWidth)%>&scHeight=<%=StringUtil.htmlEncodeXss(scHeight)%>">
<div class="clr"></div>
	<div class="lt-border"></div>
<%	 if (mode.length()==0){%>
    <div class="clr"></div>
<div class="container">
	<div class="left-container">
    	<div class="single-prod">
     		<ul>
            	<li><%=MC.M_UnamePwd_CaseSensitive%><%--Username and Password are case sensitive.*****--%></li>
                <li><%=MC.M_PopUpMustDisabled_ForApp %><%-- Pop-up blockers must be disabled for this application.*****--%></li>
            </ul>
      </div>
      </div>
    </div>
<div class="clr"></div>
<div class="copyright"><%=MC.M_CpyrgtVelos_RgtRerv%><%-- Copyright &copy; 2012 Velos Inc. All Rights Reserved.*****--%></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td class="lt-btm-border">&nbsp;</td>
    <td  class="wrapper"><div class="bottom-panel"> 
	<div class="container">
  
    	<div class="lt-btm-panel">
        	<table width="331" border="0" cellspacing="0" cellpadding="0" class="tbbg">
  <tr>
    <td width="5" height="10" valign="top" class="curv1"></td>
    <td width="321" height="10" valign="top" class="bg-white"></td>
    <td width="5" height="10" valign="top" class="curv2"></td>
  </tr>
  <tr>
    <td colspan="3" valign="top" class="bg-white">
    <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td height="40"><%=LC.L_Username%><%--Username*****--%>:</td>
        <td width="75%" height="40"><input autocomplete="off"  type="text" name="username" id="textfield" class="input1" /></td>
      </tr>
      <tr>
        <td height="40"><%=LC.L_Password%><%--Password*****--%>:</td>
        <td height="40"><input autocomplete="off"  type="password" name="password" id="textfield2" class="input1" /></td>
      </tr>
      <tr>
        <td height="30" colspan="2" valign="top"><input type="submit" class="ui-button" value="<%=LC.L_Login%><%--Login*****--%>" role="button" aria-disabled="false"></td>
        </tr>
      </table>
    </td>
    </tr>
  <tr>
    <td height="10" valign="top" class="curv3"></td>
    <td height="10" valign="top" class="bg-white"></td>
    <td height="10" valign="top" class="curv4"></td>
  </tr>
          </table>
<!--<div class="copyright">Copyright &copy; 2012 Velos Inc. All Rights Reserved.</div>-->
<div class="version"><%=appver%><%-- <%="Ver "+appver%>*****--%></div>
</div>
       
    </div>
</div></td>
    <td style="width:50%">&nbsp;</td>
  </tr>
</table>
<%}else if (mode.equals("popup")){%>
<table width="60%" align="center">
<tr>
<td width="5%"></td>
<td width="90%"><p><%=MC.M_AppLogPwd_BlkToolIePop %><%-- Application was unable to open a new window. This <FONT color=#025293><B>possibly</b></font>,can happen because 
of a <FONT color=red><B>Popup Blocker</B></FONT>.
A good way to verify if this is the issue is to do the following:<br>
<br>
1. Enter your login and password.<br>
2. Hold the *Ctrl* key down while clicking with the mouse on the &quot;Login&quot; <br>
button.<br>
<br>
This will allow the pop-up window to open. Please keep in mind that our program 
utilizes pop-up windows frequently, so you will need to locate the pop-up 
blocker being used on your computer and either disable it or allow pop-ups from 
the program.<br>
<br>
If you have a third party tool bar, such as Yahoo!, Google, etc., these do have 
the capability to block pop-ups. You should be able to see the pop-up manager 
icon within the tool bar. You'll need to allow the URL to have pop-up windows.<br>
<br>
The latest version of Netscape (7.2) has a pop-up blocker, which can be managed 
either in the Netscape Tool bar, or in the &quot;Tools&quot; menu option.<br>
<br>
Here is some information regarding pop-up windows in Internet Explorer. <br>
In the Windows XP Service Pack 2, there is a pop-up manager. If your computer 
has had this service pack, your Internet Explorer will have &quot;Pop-up Manager&quot; 
under the &quot;Tools&quot; menu in the top bar. Select this option and it should walk you 
through the steps for allowing pop-up windows for specific sites. Previous 
versions of Internet Explorer do not have the capability to block pop-up 
windows.*****--%></p>
<P> <%=MC.M_OnceVerifyInfo_Click %><%-- Once you verify this information, please click here to*****--%> <A href="nmdplogin.jsp"><B><%=LC.L_Login %><%-- Login*****--%></b></A>.<br>
<%=MC.M_IfStillLogin_PlsCont %><%-- If you are still unable to login, please contact*****--%> <A href="mailto:<%=siteAdmin%>?subject=[Login Issue,<%=StringUtil.htmlEncodeXss(logUrl)%>]">
<B><%=LC.L_Support %><%-- Support*****--%></B></A>.
</P>
</td><td width="5%"></td>
</tr>
</table>
 <!-- LIBRARY OBJECT START-->
	  	      <td width="1" bgcolor="9999ff"><img src="./images/sspacer.gif" width="1">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
<%}%>
<div class="clr"></div>
<div class="ques"><%=LC.L_Ques_OrIssues %><%-- Questions or Issues?*****--%> <a href="mailto:servicedesk@nmdp.org" title="<%=LC.L_Ques_OrIssues %><%-- Questions or Issues?*****--%>"><img src="images/icon-mail.jpg" alt="<%=LC.L_Ques_OrIssues %><%-- Questions or Issues?*****--%>" border="0" /></a></div>
<div class="clr"></div>
<div class="footer-nav-border">
<div class="bg-footer">
	<div class="suggestion"><%=MC.M_FfoxIe_ScrnReso%><%--Firefox 4.0, Firefox 7.0, Internet Explorer 8.0. | Screen Resolution 1024x768 and 1280x1024*****--%></div>
</div>
<div class="container" id="loginAppMsgs">
	<div class="left-container">
	<div style=" margin-left: 20%;padding-top: 5%;">
		<a href="#" onclick="toggleMsgDiv('loginAppMsgDiv');" style="font-weight: bold;text-decoration: none;font-size: 13pt;padding-right: 5px;color: #025293;" id="linkSign">-</a>
		<s:text name='garuda.applMessge.label.applicationMsg'/>
	</div>
    	<div id="loginAppMsgDiv" class="single-prod" style="width:100%;display: block;">
     		<ul>
                <% AppMessages  appMsgs = null;
                if(loginAppmsgs!=null){
                for(int i=0;i<loginAppmsgs.size();i++){
                	appMsgs = new AppMessages();
                	appMsgs = (AppMessages)loginAppmsgs.get(i);
                	if((appMsgs.getMsgStartDt()==null && appMsgs.getMsgEndDt()==null) || ((!application.getAttribute("pkInActiveMsg").toString().equals(appMsgs.getMsgStatus().toString())) && (appMsgs.getMsgStartDt().before(new Date()) || appMsgs.getMsgStartDt().equals(new Date())) )){
                	%>
                	<s:hidden name="loginInner" id="loginInner" />
                <li><%=appMsgs.getMsgDesc() %></li>	
                	<%} }}%>
            </ul>
      </div>
      </div>
    </div>
</div>
</form>
<script language="JavaScript">
    function toggleMsgDiv(id) {
        
        if($j('#linkSign').text()=="+"){
        	$j('#linkSign').text("-");
        }else if($j('#linkSign').text()=="-"){
        	$j('#linkSign').text("+");
        }
    	$j('#'+id).slideToggle("slow");
        }

    function checkAppMsg(){
    	var innerHidden = document.getElementById('loginInner');
    	//var innerHidden_Val = null;
    	if(innerHidden!=null){
    		$j('#loginAppMsgs').show();
    	}else{
    		$j('#loginAppMsgs').hide();
    	}
    }
    $j(function(){
    checkAppMsg();
    });
</script>
</body>
</html>
