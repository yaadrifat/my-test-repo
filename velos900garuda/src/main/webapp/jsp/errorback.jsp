<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.service.util.*"%>
<%
    String errorMsg = request.getParameter("errorMsg");
    if (errorMsg == null) { errorMsg = ""; }
    errorMsg = StringUtil.htmlEncode(errorMsg)
    .replaceAll("\n", "").replaceAll("\t","").replaceAll("\r","")
    .replaceAll("\f","").replaceAll("\u0007","").replaceAll("\u001B","")
    .replaceAll("\u000e","").replaceAll("\\x0e","").replaceAll("\"","&quot;");
%>

<br><br><br><br>
<p class = "sectionHeadings" align = center><%=errorMsg%></p>
<table width="80%"><tr><td width="20%"></td><td width="80%" align="center">&nbsp;&nbsp;
<%  String agent1 = request.getHeader("USER-AGENT").toLowerCase(); 
    if ((agent1.indexOf("mozilla") != -1) && (agent1.indexOf("gecko") <0)) { %>
     <button onclick="history.go(-1);"><%=LC.L_Back%></button>
<%  } else { %>
     <button onClick="history.go(-1);"><%=LC.L_Back%></button>	
<%  }  %>
</td></tr></table>
