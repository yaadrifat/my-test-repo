<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Pcol_Secs%><%--Protocol Sections*****--%></title>
<%@ page import="com.velos.eres.service.util.*"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% String src="";
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<br>
<jsp:useBean id="sectionB" scope="session" class="com.velos.eres.web.section.SectionJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB"%>
<DIV class="browserDefault" id="div1">

<%
String from = "customHeader";
%>
	<jsp:include page="studytabs.jsp" flush="true">
	<jsp:param name="from" value="<%=from%>"/>
	</jsp:include>
  	<%
   	HttpSession tSession = request.getSession(true);
   	if (sessionmaint.isValidSession(tSession))
	{
		String uName =(String) tSession.getValue("userName");
		String study = (String) tSession.getValue("studyId");
		String studyVersion = "";

		boolean isVersion = false;
		studyVersion = (String) tSession.getAttribute("studyVersion");
		if ((studyVersion == null) || (studyVersion.trim().equals("")) || (studyVersion.trim().equals("NULL")))
		{
			isVersion = false;
	    }
		else
		{
	   		isVersion = true;
	  	}
		%>
		<%
	   	int pageRight= 0;
	   	if(study == "" || study == null) {
		%>
			<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
		<%
		} else {
			int studyId = EJBUtil.stringToNum(study);
			String flag = "a";
			String tab = request.getParameter("selectedTab");
   	   	   	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
			if ((stdRights.getFtrRights().size()) == 0){
			 	pageRight= 0;
			}else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
			}
		   	if (pageRight > 0) {
			SectionDao sectionDao = sectionB.getStudy(studyId,flag);
		   	ArrayList ids		=sectionDao.getIds();
		   	ArrayList secStudys		=sectionDao.getSecStudys();
		   	ArrayList secNames		=sectionDao.getSecNames();
		   	ArrayList secContents	=sectionDao.getContents();
		   	ArrayList secPubFlags	=sectionDao.getSecPubFlags();
	   	   	ArrayList secSequences	=sectionDao.getSecSequences();
		   	String studySecName = "s";
		   	String studySecContents = "h";
		   	String sectionId="21";
		   	String secPubFlag="Y";
	   	   	int len= ids.size();
  	        int counter = 0;
			%>
			<Form name="sectionbrowser" method="post" action="" onsubmit="">
    			<input type="hidden" name="srcmenu" value='<%=src%>'>
				<input type="hidden" name="selectedTab" value='<%=tab%>'>
			    <table width="100%" cellspacing="0" cellpadding="0" border="0" >
					<tr>
						<td colspan="2">
					        <%Object[] arguments1 = {len}; %><%=VelosResourceBundle.getMessageString("M_SecAttachStd_EdtCont",arguments1)%><P class = "defComments"> <%--There are <%= len %> Sections attached to this <%=LC.Std_Study%>. [Click on Section's name to edit its contents]***** --%>
							</P>
						</td>
				    </tr>
					<% if(!isVersion)
					{%>
				    <tr>
					   	<td width="50%">
							<P class = "defComments">
					            <A href="studysection.jsp?sectionId=<%=sectionId%>&mode=N&srcmenu=<%=src%>&selectedTab=<%=tab%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_ClickHere_AddNewSec%><%--Click Here</A> to add a new Section*****--%>
							</P>
				        </td>
				        <td width = "50%">
							<%
							if(len > 0) {
							%>
								<P class = "defComments">
							        <A href="sectionSequence.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><%=MC.M_ClickHere_ChgSeqSec%><%--Click Here</A> to change the sequencing of the sections*****--%>
								</P>
								<%
							}
							%>
				        </td>
					</tr>
					<%
					}
					else
					{
					 %>
						<tr>
							<td colspan="2"><P class = "defComments"><FONT class="Mandatory"><%=MC.M_AssignedVer_NoChg%><%--This <%=LC.Std_Study%> has been assigned a version number. You can not make any changes in <%=LC.Std_Study%> Sections.*****--%></font></p>
							</td>
						</tr>
						<%
					}
					%>
					<tr>
						<td height="10"></td>
				        <td height="10"></td>
					</tr>
				</table>
			    <table width="98%" >
									<tr><th width="98%"> <%=MC.M_SecOf_TheStd%><%--Section(s) of the <%=LC.Std_Study%>*****--%> </th></tr>
<%--
				    <tr>
						<th width="100%"> Section(s) of the study </th>
				   	</tr>
--%>
					<%
				    for(counter = 0;counter<len;counter++)
					{
						studySecName=((secNames.get(counter)) == null)?"-":(secNames.get(counter)).toString();
						studySecContents=((secContents.get(counter)) == null)?"-":(secContents.get(counter)).toString();
						sectionId=((ids.get(counter)) == null)?"-":(ids.get(counter)).toString();
						secPubFlag=((secPubFlags.get(counter)) == null)?"-":(secPubFlags.get(counter)).toString();
						if ((counter%2)==0) {
						%>
						    <tr class="browserEvenRow">
					        <%
						}
						else{
						%>
						    <tr class="browserOddRow">
				        <%
						}
						%>
						<td><A href="studysection.jsp?sectionId=<%=sectionId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>" ><%= studySecName %></A>
							<br>
							<%
							if(studySecContents.length() < 101){
							%>
						        <pre><%= studySecContents %></pre>&nbsp;&nbsp;&nbsp;<A href="viewsection.jsp?sectionId=<%=sectionId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>"><%=LC.L_View_Section%><%--View Section*****--%></A>
					        <%
							} else {
								studySecContents = studySecContents.substring(0,100) + "...";
							%>
								<pre><%= studySecContents %></pre> <A href="studysection.jsp?sectionId=<%=sectionId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><%=LC.L_More%><%--More*****--%></A>&nbsp;&nbsp;&nbsp;<A href="viewsection.jsp?sectionId=<%=sectionId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=tab%>"><%=LC.L_View_Section%><%--View Section*****--%></A>
					        <%
							}
							%>
						</td>
					</tr>
					<%
					}
					%>
				</table>
			</Form>
			<%}else{%>
				<jsp:include page="accessdenied.jsp" flush="true"/>
			<%}
		} //end of if body for check on studyId
	} //end of if session times out
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
  	<%
	} //end of else body for page right
	%>
	<br>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<div class ="mainMenu" id="emenu">
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>

</html>
