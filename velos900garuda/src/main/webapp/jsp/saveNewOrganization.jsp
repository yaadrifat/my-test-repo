<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Add_NewOrg%><%--Add New Organization*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="addInfo" scope="request" class="com.velos.eres.web.stdSiteAddInfo.StdSiteAddInfoJB"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>

<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.stdSiteAddInfo.impl.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil" %>

<%
 int iret = 0;
 int iupret = 0;

 //String mode =request.getParameter("mode");



 String eSign = request.getParameter("eSign");
 String mode = request.getParameter("mode");

 int rows = 0;



HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{

%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
else
	{

	 String ipAdd = (String) tSession.getValue("ipAdd");

 	 String usr = (String) tSession.getValue("userId");

 	 String studyId = request.getParameter("studyId");

	 String siteId =request.getParameter("accsites");

	 String siteTypeId =request.getParameter("siteType");

	 String sampleSize =request.getParameter("sampleSize");

	 String pubFlag =request.getParameter("pubFlag");

	 String selEnrNotId = request.getParameter("selEnrNotId");
 	 String selEnrNotStatusId = request.getParameter("selEnrNotStatusId");
 	 String isReviewBased= request.getParameter("isReviewBased");

 	 if (StringUtil.isEmpty(isReviewBased))
			{
				isReviewBased = "0";
			}
			else
			{
				isReviewBased = "1";
			}

	if (StringUtil.isEmpty(selEnrNotStatusId))
			{
				selEnrNotStatusId = "";
			}
			if (StringUtil.isEmpty(selEnrNotId))
			{
				selEnrNotId = "";
			}


	  String preSiteId = request.getParameter("siteId");
if(preSiteId == null)
	preSiteId = "";

	 if(pubFlag == null)
	 	 pubFlag = "0";
	 String repFlag =request.getParameter("repFlag");

	 if(repFlag == null)
	 	 repFlag = "0";
	 String strStudySiteId=  request.getParameter("studySiteId");
	 int studySiteId = EJBUtil.stringToNum(strStudySiteId);

	 if(mode.equals("N") || (mode.equals("M") && !preSiteId.equals(siteId))){
	 // Check whether this organization already has this type in this particular study
	 	 StudySiteDao studySiteDao = new StudySiteDao();
	 studySiteDao = studySiteB.getCntForSiteInStudy(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(siteId));
	 rows = studySiteDao.getCRows();
	 }

	 if(rows >= 1 ){
			%>

		<br><br><br><br><br><p class = "successfulmsg" align = center>
		<%=MC.M_OrgEntrAldy_NewOrg%><%--The Organization that you have entered already exists in this <%=LC.Std_Study_Lower%>. Please enter a new Organization.*****--%>
		<Br><Br>


		<button onclick="window.history.back();"><%=LC.L_Back%></button>
		</p>


		<% return;  }
		//////////////////////////////

   	if(mode.equals("M"))
    	{
        	studySiteB.setStudySiteId(studySiteId);
        	studySiteB.getStudySiteDetails();
        	studySiteB.setSiteId(siteId);
        	studySiteB.setStudySiteType(siteTypeId);
         	studySiteB.setSampleSize(sampleSize);
         	studySiteB.setStudySitePublic(pubFlag);
         	studySiteB.setRepInclude(repFlag);
         	studySiteB.setIpAdd(ipAdd);
			studySiteB.setModifiedBy(usr);

	  	    studySiteB.setIsReviewBasedEnrollment(isReviewBased);
    		studySiteB.setSendEnrNotificationTo(selEnrNotId);
	    	studySiteB.setSendEnrStatNotificationTo(selEnrNotStatusId);

		   	iupret=studySiteB.updateStudySite();
    	} else
    	{

        	studySiteB.setStudyId(studyId);
         	studySiteB.setSiteId(siteId);
         	studySiteB.setStudySiteType(siteTypeId);
         	studySiteB.setSampleSize(sampleSize);
         	studySiteB.setStudySitePublic(pubFlag);
         	studySiteB.setRepInclude(repFlag);
         	studySiteB.setCreator(usr);
         	studySiteB.setIpAdd(ipAdd);

         	studySiteB.setIsReviewBasedEnrollment(isReviewBased);
    		studySiteB.setSendEnrNotificationTo(selEnrNotId);
	    	studySiteB.setSendEnrStatNotificationTo(selEnrNotStatusId);

		   	studySiteB.setStudySiteDetails();
         	iret = studySiteB.getStudySiteId();

         	strStudySiteId  = String.valueOf(iret);

    	}
  /////////////
  	  		String[] arRecordType = request.getParameterValues("recordType");

			String[] arData = request.getParameterValues("data");

			String[] arId = request.getParameterValues("id");

			String[] arAddInfoType = request.getParameterValues("addInfoType");

			int errorCode = -1;

			StdSiteAddInfoBean sMain = new StdSiteAddInfoBean();
			ArrayList alData = new ArrayList();
			if(arData!=null)
				alData= EJBUtil.strArrToArrayList(arData) ;

				int len = 0;

				len = alData.size();

				for ( int j = 0 ; j< len ; j++)
				{
					StdSiteAddInfoBean sskTemp = new StdSiteAddInfoBean();

					if   ( ( !(EJBUtil.isEmpty (arData[j])) &&  arRecordType[j].equals("N")) || arRecordType[j].equals("M"))
					{
						sskTemp.setId(EJBUtil.stringToNum(arId[j]));
						sskTemp.setStudySiteId(strStudySiteId) ;
						sskTemp.setAddCodelstInfo(arAddInfoType[j]);
						sskTemp.setAddInfoData(arData[j]);
						sskTemp.setRecordType(arRecordType[j]);
						if (arRecordType[j].equals("N"))
						{
							sskTemp.setCreator(usr);
						}else
						{
							sskTemp.setModifiedBy(usr);
						}
						sskTemp.setIpAdd(ipAdd);
						sMain.setStdSiteAddInfoStateKeepers(sskTemp);
					}
				}

			errorCode = addInfo.createMultipleStdSiteAddInfo(sMain);

  	  /////////


 	if ((iupret< 0) || (iret<0)) {%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>
			<%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%></p> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;
		} else {%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>
			<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>
		<%if(mode.equals("N")){%>
<!--Modified by Manimaran to fix the Bug2791-->
<META HTTP-EQUIV=Refresh CONTENT="1; URL=newOrganization.jsp?mode=M&studyId=<%=studyId%>&studySiteId=<%=iret%>&flag=0">
		<%}else{%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=newOrganization.jsp?mode=M&studyId=<%=studyId%>&studySiteId=<%=studySiteId%>&flag=0">
		<%}

	}
}//esign

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





