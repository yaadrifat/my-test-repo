<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>

<HTML>

<HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<TITLE><%=MC.M_PrsnlzAccNotific_Subscr%><%--Personalize Account >> Notifications >> Subscribe*****--%></TITLE>



<SCRIPT Language="javascript">

	function changeRows(obj,row, frmname)

	{

	   selrow = row ;

	   totrows = frmname.selectedrows.value;

	   if(frmname.totalrows.value > 1) {
	     	if( frmname.subscribeme[selrow].checked == true) {
		   totrows ++
		   frmname.selectedrows.value = totrows;
	   	} else {
		   totrows --
		   frmname.selectedrows.value = totrows;
		}
	   } else {
		if( frmname.subscribeme.checked == true) {
		   totrows ++
		   frmname.selectedrows.value = totrows;
		} else {
		   totrows --
		   frmname.selectedrows.value = totrows;
		}
	   }
	}

	function totalRows(formobj)
	{
    	if (!(validate_col('esign',formobj.eSign))) return false;
    	if(isNaN(formobj.eSign.value) == true) {
    		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
    		formobj.eSign.focus();
			return false;
       	}
  	   if( document.subs.selectedrows.value == 0)
		{
  		 alert("<%=MC.M_SelThrpArea_BeforeSbmt%>");/*alert("Please select some Theraputic Area before submitting");*****/
			return false;
		} else {
			return true;
		}

	}

</SCRIPT>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<BODY>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="code" scope="request" class="com.velos.eres.business.common.CodeDao"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%

	HttpSession tSession = request.getSession(true);

	int pageRight = 0;



	if (sessionmaint.isValidSession(tSession))

	{

	String uName = (String) tSession.getValue("userName");

  	GrpRightsJB gRights = (GrpRightsJB) tSession.getValue("GRights");

      //pageRight = Integer.parseInt(gRights.getFtrRightsByValue("MNOTIFY"));

	  pageRight = 7;

	String userId = (String) tSession.getValue("userId");

 	 String tab = request.getParameter("selectedTab");




	if (pageRight > 4)

	{



%>

<DIV class="tabDefTopN" id="divtab">
    <jsp:include page="personalizetabs.jsp" flush="true"/>
</DIV>
<DIV class="tabDefBotN" id="divbot">

<Form Name="subs" id="subsForm" method="post" action="saveSubscribe.jsp" onsubmit="if (totalRows(document.subs)==false){setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
<!--KM-4260 -->
<%



	code.getCodeValuesNotify("tarea",Integer.parseInt(userId));

	int rows = code.getCRows();

	ArrayList tAreadesc =  code.getCDesc();

	ArrayList tAreaId = code.getCId();

	ArrayList tMail = code.getCSubType();

	String cVal = null;

%>
    <Input type="hidden" name="totalrows" value= <%=rows%> >
    <Input type="hidden" name="selectedrows" value=0>
    <Input type="hidden" name="src" value= <%=src%> >
    <Input type="hidden" name="selectedTab" value="<%=tab%>">
<%
    if(rows > 0) {
%>
	<table><tr><td><P class="defComments lhsfont"><%=MC.M_Selc_ThrapArea%><%--Select Therapeutic Area(s):*****--%></P></td></tr></table>
    
    <TABLE width="99%" cellspacing="0" cellpadding="0" border="0" class="outline midAlign">
      <TH><%=LC.L_Therapeutic_Areas%><%--Therapeutic Areas*****--%></TH>
      <TH><%=LC.L_Subscribe%><%--Subscribe*****--%></TH>
      <% for(int count=0;count<rows;count++){

	cVal = tAreaId.get(count) + "~" + tMail.get(count);

%>
      <TR>
        <TD> <%=tAreadesc.get(count) %> </TD>
        <td>
      	   <Input type="checkbox" name="subscribeme" value = <%= cVal%> onClick="changeRows(this,<%=count%>, document.subs)">
        </TD>
      </TR>
      <%}%>
</table>
<%
if (pageRight > 4) {

%>
<br>
<table width="99%" cellspacing="0" cellpadding="0" class="esignStyle midalign"><tr><td>
 <!--JM: 19June2009: implementing common Submit Bar in all pages -->
	<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="subsForm"/>
	<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
</td></tr></table>
<%
}
%>
<!--<table>
      <TR></tr>
      <TR>
        <TD COLSPAN =4 align="right">
          <% if (pageRight > 4)

		  { %>
         	  <br><input type="image" src="../images/jpg/Submit.gif"  align="absmiddle" border="0">

          <% }

		else { %>

          <% } %>
        </TD>
      </TR>
    </TABLE> -->
<%
    } else {
%>
    <P class="defComments"><%=MC.M_AldySbcrb_AllThrpArea%><%--You are already subscribed to all the available Therapeutic Areas.*****--%></P>
<%
    }
%>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</BODY>

</HTML>







