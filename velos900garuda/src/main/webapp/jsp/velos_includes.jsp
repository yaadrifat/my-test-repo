<% String contextpath=request.getContextPath();%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.util.List,java.util.ArrayList,java.util.Map,java.util.HashMap" %>
<%@ page import="com.velos.eres.web.user.UserJB"%>
<%@ page import="java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<script>
var contextpath = "<%=contextpath%>";
</script>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>

<%
String skin = "";
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int patientRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	int studyRights = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	UserJB userB1 = (UserJB) tSession.getValue("currentUser");
	String accName = (String) tSession.getValue("accName");
	String accId = (String) tSession.getValue("accountId");
	accName=(accName==null)?"default":accName;
	String accSkinId = "";
	String usersSkinId = userB1.getUserSkin();
	String userSkin = "";
	String accSkin = (String) tSession.getValue("accSkin");
	skin = "default";
	userSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	} else {
		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}
}
%>

<!-- Garuda Common CSS -->
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/portlet.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.alerts.css" media="screen" />
 
<!-- Common CSS -->
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/clickmenu.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/styles.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/demo_table.css" media="screen" />

<!-- Garuda Common JS -->
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/dateFormat.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jsapi.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/scripts.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/general_message.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/modernizr-1.1.min.js"></script>

<!-- Overlib JS -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

