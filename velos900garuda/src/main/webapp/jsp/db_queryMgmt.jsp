<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
function fOpenWindow(type,section,frmobj){
	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=500;
		height=120;
	}
	if (type == "M"){
		width=270;
		height=70;
	}
	frmobj.year.value ="";
	frmobj.month.value ="";
	frmobj.dateFrom.value ="";
	frmobj.dateTo.value ="";

	frmobj.range.value = "";
	if (type == 'A'){
		frmobj.range.value = "[<%=LC.L_All%><%--All*****--%>]"
	}
	if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section+"&from=db","Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}
}

/*function filterFade(repId,flag)
{

var arrayOfFilters= new Array
if (flag=='O')
{
dojo.lfx.html.wipeOut('orgDIV', 300).play();
dojo.lfx.html.wipeOut('orgDIV1', 300).play();
dojo.lfx.html.wipeOut('protCalDIV', 300).play();
dojo.lfx.html.wipeOut('protCalDIV1', 300).play();
dojo.lfx.html.wipeOut('userDIV', 300).play();
dojo.lfx.html.wipeOut('userDIV1', 300).play();

}
else {


dojo.lfx.html.wipeIn('orgDIV', 300).play();
dojo.lfx.html.wipeIn('orgDIV1', 300).play();
dojo.lfx.html.wipeIn('protCalDIV', 300).play();
dojo.lfx.html.wipeIn('protCalDIV1', 300).play();
dojo.lfx.html.wipeIn('userDIV', 300).play();
dojo.lfx.html.wipeIn('userDIV1', 300).play();
}

}*/
function filterFade(repId,repFilter)
{
var currentFilter,dispDiv;
filterApplicable=repFilter;

arrayOfAvailFilters=(document.getElementById("keywordStr").value).split(",");

//if no filters are set for the report, we will show all the options
//this is to override the bahaviour where no filter would be shown
if (filterApplicable.length==0)
{
for (var i=0;i<arrayOfAvailFilters.length;i++)
{
if (filterApplicable.length==0)
 filterApplicable=arrayOfAvailFilters[i];
 else
 filterApplicable=filterApplicable+","+arrayOfAvailFilters[i];

}

}


arrayOfApbFilter=filterApplicable.split(":");

for (var i=0;i<arrayOfAvailFilters.length;i++)
{
currentFilter=arrayOfAvailFilters[i];

if ((typeof(document.getElementById("DIV"+currentFilter))!="undefined") && (document.getElementById("DIV"+currentFilter)!=null))
dispDiv=document.getElementById("DIV"+currentFilter).value.split(",");

if ((filterApplicable.indexOf(currentFilter)>=0))
{
	if (dispDiv!=null)
	{
  for (var j=0;j<dispDiv.length;j++)
  {
	if (dispDiv[j].length>0)
	{
	dojo.lfx.html.wipeIn(dispDiv[j], 300).play();
	}

}
}

} else
{
  if (dispDiv!=null) {
 for (var k=0;k<dispDiv.length;k++)
  {


	  if (dispDiv[k].length>0)
		dojo.lfx.html.wipeOut(dispDiv[k], 300).play();

}
}
}



}

}

function changeReportType(formobj)
{
 formobj.action="reportcentral.jsp";
 formobj.submit();
}
function checkDepend(keyword)
{
     formobj=document.dashboardpg;
	var dependStr=eval("formobj.depend" +keyword+".value");
   if (dependStr.length>0)
   {
		arrayOfStrings=dependStr.split(":");
		for (var i=0;i<arrayOfStrings.length;i++)
		{
		 if (eval("formobj.param" +arrayOfStrings[i]+".value.length==0"))
		 {
		 	tempName=eval("formobj.key"+arrayOfStrings[i]+".value");
		 	var paramArray = [tempName,eval("formobj.key"+keyword+".value")];
		 	alert(getLocalizedMessageString("M_PlsEtr_ValsForSel",paramArray));/*alert("Please enter value(s) for "+tempName+ " before selecting "+eval("formobj.key"+keyword+".value"));*****/
			return false;
		 }

		}

   } // end if for (mandStr.length>0)
   return true;


}
function openLookup(formobj,url,filter) {

 // process the depend String to see if current lookup depend on some other values
 //if yes, prompt a message


var tempValue;
var urlL="";
formobj.target="Lookup";
formobj.method="post";
urlL="multilookup.jsp?"+url;
if (filter.length>0){
	if (filter.indexOf(':sessUserId')>=0)
	{
	  tempValue="";
	  tempValue=document.getElementById('sessUserId').value;
	  filter=replaceSubstring(filter,":sessUserId",tempValue);
	}
	if (filter.indexOf(':sessAccId')>=0)
	{
	  tempValue="";
	  tempValue=document.getElementById('sessAccId').value;
	  filter=replaceSubstring(filter,":sessAccId",tempValue);
	}
	if (filter.indexOf(':studyId')>=0)
	{
	  tempValue="";
	  tempValue=formobj.paramstudyId.value;
	  filter=replaceSubstring(filter,":studyId",tempValue);
	
	}
	urlL=urlL+"&"+filter;
}
formobj.action=urlL;


formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
formobj.submit();
formobj.target="";
}
function openwin12() {

    windowName=window.open("multipleusersearchdetails.jsp?mode=initial&fname=&lname=&from=dashboardpg&dispFld=seluserId&dataFld=paramuserId&names&ids=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	windowName.focus();
	}

function fSetId(frmobj){
	var rep = frmobj.qmReport.value;
	frmobj.repId.value = rep;
	frmobj.repName.value = frmobj.qmReport.options[frmobj.qmReport.selectedIndex].id;

	var repFilter = eval("frmobj.keyFilters" +rep+".value");
	filterFade(rep,repFilter);
}

var dd = 'fqCreatorIdDD';
var param ='paramfqCreatorId';
var sel = 'selfqCreatorId';

function fSetParams(){
	document.getElementById(param).value=document.getElementById(dd).value;
	document.getElementById(sel).value=document.getElementById(dd).options[document.getElementById(dd).selectedIndex].text;;
}

function validate(formobj)
{
  var tempName;
  var mandStr="";
  var repId;
  var arrayOfStrings;

  repId=formobj.repId.value;
 if (repId.length==0)
 {
  alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
  return false;
 }

 if (formobj.range.value.length==0)
 {
 alert("<%=MC.M_Etr_DtFilter%>");/*alert("Please enter a Date Filter");*****/
 return false;
 }

  mandStr=eval("formobj.keyword" +repId+".value");

  if (mandStr.length>0)
   {
		arrayOfStrings=mandStr.split(":");
		for (var i=0;i<arrayOfStrings.length;i++)
		{
		if  (arrayOfStrings[i].indexOf("[DATEFILTER]")>=0)
		{
		 tempName=replaceSubstring(arrayOfStrings[i],"[DATEFILTER]","");

		 if ((tempName.indexOf("A")<0) && (formobj.filterType[0].checked))
		 {
		 alert("<%=MC.M_OptNotAvalFilter_SelOpt%>");/*alert("'All' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		 if ((tempName.indexOf("Y")<0) && (formobj.filterType[1].checked))
		 {
		 alert("<%=MC.M_YearOptNotAval_SelOther%>");/* alert("'Year' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		 if ((tempName.indexOf("M")<0) && (formobj.filterType[2].checked))
		 {
		 alert("<%=MC.M_MonthOptNotAval_DtFilter%>");/*alert("'Month' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		 if ((tempName.indexOf("D")<0) && (formobj.filterType[3].checked))
		 {
		 alert("<%=MC.M_DataRangeNotAval_SelOther%>");/*alert("'Date Range' option is not available for the Date Filter. Please select any of the other available options.");*****/
		 return false;
		 }
		}
		else
		{
		if (eval(typeof(eval("formobj.param" +arrayOfStrings[i]))!="undefined")) {
		  if (eval("formobj.param" +arrayOfStrings[i]+".value.length==0"))
		 {
		 	tempName=eval("formobj.key"+arrayOfStrings[i]+".value");
		 	var paramArray = [tempName];
		 	alert(getLocalizedMessageString("M_PlsEtr_ValsFor",paramArray));/*alert("Please enter value(s) for "+tempName);*****/
			return false;
		 }
		 }
		}
		} //end for

   } // end if for (mandStr.length>0)

    else {

 //Run the validation check based on Mandatory filter columns - Category wide
 mandStr =formobj.mandatory.value;
 arrayOfStrings=new Array(1);
 if (mandStr.length>0){

 arrayOfStrings=mandStr.split(":");
 for (var i=0;i<arrayOfStrings.length;i++)
 {
   if (eval("formobj.param" +arrayOfStrings[i]+".value.length==0"))
  {
  	tempName=eval("formobj.key"+arrayOfStrings[i]+".value");
  	var paramArray = [tempName];
 	alert(getLocalizedMessageString("M_PlsEtr_ValsFor",paramArray));/*alert("Please enter value(s) for "+tempName);*****/
 	return false;
  }
 }
 }
 }// end for else (mandStr.length>0)
 //end validation
 // formobj.action="dashboardcentral.jsp?selRep="+repId+"&selRepName="+formobj.repName.value;
 // formobj.submit();
 
 formobj.target = "repDisplay";
 formobj.action = "reportDisplay.jsp?repId="+repId+"&repName="+formobj.repName.value+"&includedIn=db";
 formobj.submit();
 formobj.target="";
}

//JM: 12Mar2008: Enh #REP6:
function fnCheck(formobj){

	if (formobj.fltrChk.checked==true)
		formobj.fltrChk.value="1";
	else
		formobj.fltrChk.value="0";


	if (formobj.dnldChk.checked==true)
		formobj.dnldChk.value="1";
	else
		formobj.dnldChk.value="0";


}

function getFormQryStatus(formobj){
	if (!formobj.repId) { return; }

	if (!formobj.amILoaded) { return; }

	var valLoad = document.getElementById("amILoaded").value;
	
	if (valLoad=="Yes"){
		var repId = formobj.repId.value;
		if ((repId == '156' && document.getElementById("QStatDDLabel").style.display =='none')||
			(repId != '156' && document.getElementById("FStatDDLabel").style.display =='none')){
			var studyId = formobj.paramstudyId.value;
			var codeType="";
			var ddName = "statIdDD";
			
			var onchange=' id="'+ ddName + '" onChange="document.getElementById(\'paramstatId\').value=document.getElementById(\'statIdDD\').value; ';
			onchange = onchange + 'document.getElementById(\'selstatId\').value=document.getElementById(\'statIdDD\').options[document.getElementById(\'statIdDD\').selectedIndex].text;"';
		
			document.getElementById('paramstatId').value="";
			document.getElementById('selstatId').value="";
			
			document.getElementById("QStatDDLabel").style.display="none";
			document.getElementById("FStatDDLabel").style.display="none";
			
			if (repId == '156') {ddType ="QStat";	codeType="query_status";}
			if (repId == '157') {ddType ="FStat";	codeType="fillformstat";}
			if (repId == '158') {ddType ="FStat";	codeType="fillformstat";}
		
			document.getElementById(ddType+"DDLabel").style.display="block";
		
			new VELOS.ajaxObject("getCodeDropDown.jsp", {
		   		urlData:"ddName="+ddName+"&codeType="+codeType+"&ddType="+ddType+"&prop="+onchange ,
				   reqType:"POST",
				   outputElement: "statDD" } 
		   
		   ).startRequest();
			  
			/*new VELOS.ajaxObject("getCodeDropDownByRole.jsp", {
		   		urlData:"studyId="+studyId + "&ddName="+ddName+"&codeType="+codeType+"&ddType="+ddType+"&prop="+onchange ,
				   reqType:"POST",
				   outputElement: "statDD" } 
		   
		   ).startRequest();*/
		}
	}
}

</script>


<%
	String src = "";

	src = request.getParameter("srcmenu");
	String calledfrom = request.getParameter("calledfrom");

	String pkey = "";
	String patientCode = "";
	String studyPk = "";
	String studyNumber  = "";
	String reportsHeading = "";

		if (StringUtil.isEmpty(calledfrom))
		{
			calledfrom = "";
			reportsHeading = "";
		}
		else if (calledfrom.equals("patient"))
		{
			pkey  = request.getParameter("pkey");
			patientCode  = request.getParameter("patientCode");
			patientCode=(patientCode==null)?"":patientCode;
			patientCode=StringUtil.decodeString(patientCode);
			reportsHeading = "";
		}
		else if (calledfrom.equals("study"))
		{
			studyPk  = request.getParameter("studyPk");
			studyNumber  = request.getParameter("studyNumber");
			studyNumber=(studyNumber==null)?"":studyNumber;
			studyNumber=StringUtil.decodeString(studyNumber);
			reportsHeading = "";
		}


%>


<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew" />
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew" />
<jsp:useBean id="ctrlD" scope="request" class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB" />
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew" />
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<%@ page import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.user.*,com.velos.eres.business.common.*,com.velos.eres.web.grpRights.GrpRightsJB"%><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>

<script>
	function onLoad() {
		return getFormQryStatus(document.dashboardpg);
	}
	onLoad();
</script>
<!-- div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div-->
<script language="JavaScript" src="overlib.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<%

	StringBuffer rep_first = new StringBuffer();

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{			
		String userId = (String) tSession.getValue("userId");
		UserJB user = (UserJB) tSession.getValue("currentUser");
		String siteId = user.getUserSiteId();
		String siteName = "";
		siteB.setSiteId(EJBUtil.stringToNum(siteId));
		siteB.getSiteDetails();
		siteName = siteB.getSiteName();
		String acc = (String) tSession.getValue("accountId");
		int accId = EJBUtil.stringToNum(acc);
		
		//Process selected report
		String selReport = "";
		String selRepName = "";		
		String includedIn="db";	
		
		//Process all the report categories available
		String reportCat = request.getParameter("repcat");
	     
		//Added by Manimaran to give access right for default admin group to the delete link
	    int usrId = EJBUtil.stringToNum(userId);
	    
	    userB = (UserJB) tSession.getValue("currentUser");
		 
		String defGroup = userB.getUserGrpDefault();
		
		groupB.setGroupId(EJBUtil.stringToNum(defGroup));
		groupB.getGroupDetails();
		String groupName = groupB.getGroupName();
		
		String superUserRights = "";
		superUserRights = groupB.getDefaultStudySuperUserRights(userId);
		
		if (StringUtil.isEmpty(superUserRights))
		{
			superUserRights = "";
		} 		

		int grpRight = 0;
		
		GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

		grpRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

		String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");

		int protocolManagementRightInt = 0;
		protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);

		String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		reportCat = (reportCat == null) ? "" : reportCat;

		StringBuffer reportCatDD = new StringBuffer();
		CodeDao codeDao = codeB.getCodeDaoInstance();
		codeDao.getCodeCustomCol("report");
		
		codeDao.setCType("report");
	    codeDao.setForGroup(defUserGroup);
	    codeDao.getHiddenCodelstDataForUserCustom();
	    String hideStr = "";
	    
		
		ArrayList reportCatList = codeDao.getCodeCustom();
		//Assumption is that reportCatList will never be null
		if (reportCat.length() == 0)
			reportCat = ((String) reportCatList.get(0)).toLowerCase();
		//Retrieve all the filter columns available for this category
		ArrayList repFilterColumns = new ArrayList();
		ArrayList repMapFilterColumns = new ArrayList();
		ArrayList repFilterDispNames = new ArrayList();
		ArrayList repFilterMandatoryList = new ArrayList();
		ArrayList repFilterDependOnList = new ArrayList();
		ArrayList repFilterKeywords = new ArrayList();
		ArrayList repFilterDispDIV = new ArrayList();

		ReportDaoNew repfilterDao = repB.getFilterColumns("form query management");
		repFilterColumns = repfilterDao.getRepFilterColumns();
		repMapFilterColumns = repfilterDao.getRepMapFilterColumns();
		repFilterDispNames = repfilterDao.getRepFilterDisplayNames();
		repFilterMandatoryList = repfilterDao.getRepFilterMandatoryList();
		repFilterDependOnList = repfilterDao.getRepFilterDependOnList();
		repFilterKeywords = repfilterDao.getRepFilterKeywords();
		repFilterDispDIV=repfilterDao.getRepFilterDispDiv();
		String mandStr = "", dependStr = "", tempMandStr = "", tempDependStr = "", keywordStr = "";

		//process the Mandatory and Depend on list to create delimited Strings
		for (int count = 0; count < repFilterKeywords.size(); count++) {
			if (keywordStr.length() == 0)
				keywordStr = (String) repFilterKeywords.get(count);
			else
				keywordStr = keywordStr+","+ (String) repFilterKeywords.get(count);

			tempMandStr = (String) repFilterMandatoryList.get(count);
			tempMandStr = (tempMandStr == null) ? "" : tempMandStr;
			if (tempMandStr.equals("Y")) {
				if (mandStr.length() > 0)
					mandStr = mandStr + ":"+ (String) repFilterKeywords.get(count);
				else
					mandStr = (String) repFilterKeywords.get(count);
			}
		}
		//end create Delimited String

		//end retrieving

		//Check rights

		// end checking rights

		//for dashboardpg dropdown
		ReportDaoNew repDao = new ReportDaoNew();
		repDao.getReports("rep_qmgmt",accId);
		
		ArrayList reportList = repDao.getRepName();
		ArrayList repIdList = repDao.getPkReport();
		
		//Assumption is that reportList will never be null
		selReport = (""+repIdList.get(0));
		selRepName = (""+reportList.get(0));
		
		StringBuffer reportDD = new StringBuffer();
		reportDD.append("<SELECT NAME='qmReport' id='qmReport' onchange='javascript:fSetId(document.dashboardpg); return getFormQryStatus(document.dashboardpg)'>");

		String tempReport = "";
		String tempRepId = "";
		
		for (int counter = 0; counter < reportList.size(); counter++) {
			tempReport = (String) reportList.get(counter);
			tempRepId = "" +repIdList.get(counter);
			if (selReport.equals(tempRepId))
				reportDD.append("<OPTION id ='"+tempReport+"' value = '"+ tempRepId + "' selected>"	+ tempReport + "</OPTION>");
			else
				reportDD.append("<OPTION id ='"+tempReport+"' value = '"	+ tempRepId + "'>" + tempReport	+ "</OPTION>");
		}
		reportDD.append("</SELECT>");
		//end processing
		
		codeDao.resetDao();
		codeDao.getCodeValues("report", "form query management");
		ArrayList codeSubTypeList = codeDao.getCSubType();
		ArrayList codeDescList = codeDao.getCDesc();
		
		//Process selected Query Creator
		String tempfqCreator = "";
		int tempfqCreatorId = 0;

		StringBuffer fqCreatorDDStr = new StringBuffer();
		fqCreatorDDStr.append("<SELECT NAME='fqCreatorIdDD' id='fqCreatorIdDD' onChange='javascript:fSetParams();'");
		fqCreatorDDStr.append("><OPTION id ='0' value = 'role_All'>"+LC.L_All_Upper/*ALL*****/+"</OPTION>");
		fqCreatorDDStr.append("<OPTION id ='1' value = 'role_system'>"+LC.L_Sys_Generated/*System Generated*****/+"</OPTION>");
		
		codeDao.resetDao();
		codeDao.getCodeValues("role");
		
		tempfqCreatorId = codeDao.getCodeId("role","role_monit");
		tempfqCreator = codeDao.getCodeDesc(tempfqCreatorId)+" "+LC.L_Only/*"Only"*****/;
		if (tempfqCreatorId != 0)
			fqCreatorDDStr.append("<OPTION id ='"+tempfqCreatorId+"' value = '"+ codeDao.getCodeSubtype(tempfqCreatorId) + "'>"	+ tempfqCreator + "</OPTION>");
		
		tempfqCreatorId = codeDao.getCodeId("role","role_dm");
		tempfqCreator = codeDao.getCodeDesc(tempfqCreatorId)+" "+LC.L_Only/*"Only"*****/;
		if (tempfqCreatorId != 0)
			fqCreatorDDStr.append("<OPTION id ='"+tempfqCreatorId+"' value = '"+ codeDao.getCodeSubtype(tempfqCreatorId) + "'>"	+ tempfqCreator + "</OPTION>");

		fqCreatorDDStr.append("</SELECT>");
		
		ArrayList repIds = new ArrayList();
		ArrayList repNames = new ArrayList();
		ArrayList repColumns = new ArrayList();
		ArrayList repFilters = new ArrayList();
		ArrayList reportFilterKeywords = new ArrayList();
		ArrayList reportFilterApplicable = new ArrayList();
		String helpStr = "", repColumn = "", repFilter = "", reportFilterKeyword = "",repFilterApplicable="";
		int repId = 0;
		
		int idStudy = 0 ;
		//String ddstudy ="";
		//StudyDao studyDao = studyB.getUserStudies(userId, "studyId id='selstudyId' STYLE='WIDTH:177px' onChange='javascript:return getFormQryStatus(document.dashboardpg);'",idStudy,"activeForEnrl");
		//ddstudy = studyDao.getStudyDropDown();
%>
<DIV Name="reports" method="post" action="">
	<input type="hidden" name="srcmenu" value='<%=src%>'> 
	<input type="hidden" id="repId" name="repId" value="<%=selReport%>"/>
	<input type="hidden" name="repName" value="<%=selRepName%>"/>
	<input type="hidden" name="year"> <!-- <input type="hidden" name="paramaccId" value="<%=accId%>">-->
	<input type="hidden" name="month"> <input type="hidden" name="year1"> <input type="hidden" name="dateFrom"> 
	<input type="hidden" name="dateTo"> <input type="hidden" name="mandatory" value='<%=mandStr%>'> 
	<input type="hidden" name="depend" value='<%=dependStr%>'>
	<input type="hidden" id="keywordStr" name="keywordStr" value='<%=keywordStr%>'>
	 <%
 		//print all the keyword and their display values
 		for (int i = 0; i < repFilterKeywords.size(); i++) {
 %> <input type="hidden" name="key<%=repFilterKeywords.get(i)%>" value="<%=repFilterDispNames.get(i)%>"> <%
 }
 %>
</DIV>

<!-- Table width="98%">
	<tr height="2"><td></td></tr>
</Table-->

<%
String amILoaded ="";

if (grpRight >= 4) {
	amILoaded ="Yes";
}%>
<input type="hidden" id="amILoaded" name="amILoaded" value="<%=amILoaded%>">

<%if (grpRight >= 4) {%>
<div class="tmpHeight"></div>
<Table width="99%" class="lhsFont">
	<tr>
		<td>
		<table width="100%">
			<%
					for (int i = 0; i < codeSubTypeList.size(); i++) {
					repfilterDao.getReports((String) codeSubTypeList.get(i), accId);
					repIds = repfilterDao.getPkReport();
					repNames = repfilterDao.getRepName();
					repColumns = repfilterDao.getRepColumns();
					repFilters = repfilterDao.getRepFilters();
					reportFilterKeywords = repfilterDao.getReportFilterKeywords();
					reportFilterApplicable = repfilterDao.getRepFilterApplicable();
					//System.out.println("reportFilterApplicable"+reportFilterApplicable);

					for (int j = 0; j < repIds.size(); j++) {
						if ((j % 2) == 0) {
						%>
						<tr>
						<%
						} else {
						%>
						</tr>
						<tr>
						<%
						}
						repId = ((Integer) repIds.get(j)).intValue();
						repColumn = (String) repColumns.get(j);
						repColumn = (repColumn == null) ? "" : repColumn;
						repFilter = (String) repFilters.get(j);
						repFilter = (repFilter == null) ? "" : repFilter;
						reportFilterKeyword = (String) reportFilterKeywords.get(j);
						reportFilterKeyword = (reportFilterKeyword == null) ? "": reportFilterKeyword;
						repFilterApplicable=(String) reportFilterApplicable.get(j);
						repFilterApplicable=(repFilterApplicable==null)?"":repFilterApplicable;
						//out.println("repId "+repId);
				%>
				<input type="hidden" name="keyword<%=repId%>" value='<%=reportFilterKeyword%>'>
				<input type="hidden" name="keyFilters<%=repId%>" value='<%=repFilterApplicable%>'>
				<%
						repfilterDao.resetDao();
					}//end inner for loop with j
				}//end for loop with i
				%>
			</tr>
		</table>
		</td>
		<td>
		<DIV id="repFilter" class="lhsFont">
		<table width="100%" style="border: solid #9d9d9d 1px;">
			<tr bgcolor="#dcdcdc" height="40">
				<td><Font class="reportText lhsFont"><%=LC.L_Dboard_Views%><%--Dashboard Views*****--%>:  </Font><%=reportDD.toString()%></td>
			</tr>
			<tr>
				<td colspan=5><Font class="reportText lhsFont"><%=LC.L_Available_Filters%><%--Available filters*****--%>:</Font></td>
			</tr>
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td width="30%"><Font class="reportText lhsFont"><%=LC.L_Date_Filter%><%--Date Filter*****--%>:</Font></td>
							<td width="60%"><Font class="reportText lhsFont"><%=LC.L_Addl_Filters%><%--Additional Filters*****--%>:</Font></td>
						</tr>
						<tr>
							<td class="lhsFont">
								<Input type="radio" name="filterType" checked value="1" onClick="fOpenWindow('A','1', document.dashboardpg)"><%=LC.L_All%><%--All*****--%> 
								<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1', document.dashboardpg)"> <%=LC.L_Year%><%--Year*****--%> 
								<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1', document.dashboardpg)"> <%=LC.L_Month%><%--Month*****--%> 
								<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1', document.dashboardpg)"> <%=LC.L_Date_Range%><%--Date Range*****--%> <br>
								<input type="text" name="range" size="33" READONLY value="[<%=LC.L_All%><%--All*****--%>]">
							</td>
							<td>
								<table width="100%">
						<%
						String tempRepFilterColumn = "";
						if (repFilterColumns != null) {
							if (repFilterColumns.size() > 0) {
								for (int i = 0; i < repFilterColumns.size(); i++) {
									tempRepFilterColumn = (String) repMapFilterColumns.get(i);
									tempRepFilterColumn = (tempRepFilterColumn == null) ? "": tempRepFilterColumn;
									if (tempRepFilterColumn.length() == 0)
										tempRepFilterColumn = (String) repFilterColumns.get(i);
		
									if (tempRepFilterColumn.indexOf("[SESSSITENAME]") >= 0)
										tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SESSSITENAME]", siteName);
		
									if (tempRepFilterColumn.indexOf("[SESSSITEID]") >= 0)
										tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SESSSITEID]", siteId);
		
									if (tempRepFilterColumn.indexOf("[FQCREATORID]") >= 0)
										tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[FQCREATORID]", fqCreatorDDStr.toString());
									
									if (calledfrom.equals("patient")){
										if (tempRepFilterColumn.indexOf("[SELPATPK]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATPK]", pkey);
			
										if (tempRepFilterColumn.indexOf("[SELPATCODE]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATCODE]", patientCode);
									}else{
										if (tempRepFilterColumn.indexOf("[SELPATPK]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATPK]", "[ALL]");
			
										if (tempRepFilterColumn.indexOf("[SELPATCODE]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELPATCODE]", "[ALL]");		
									}
			
									//for study
									if (calledfrom.equals("study")){
										if (tempRepFilterColumn.indexOf("[SELSTUDYPK]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYPK]", studyPk);
			
										if (tempRepFilterColumn.indexOf("[SELSTUDYNUMBER]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYNUMBER]", studyNumber);		
									}else{
										if (tempRepFilterColumn.indexOf("[SELSTUDYPK]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYPK]", "[ALL]");
			
										if (tempRepFilterColumn.indexOf("[SELSTUDYNUMBER]") >= 0)
											tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SELSTUDYNUMBER]", "[ALL]");		
									}
			
									//account ID
									if (tempRepFilterColumn.indexOf("[SESSACC]") >= 0)
										tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn, "[SESSACC]",acc);
			
									//logged in user id
									if (tempRepFilterColumn.indexOf("[SESSLOGUSER]") >= 0)
										tempRepFilterColumn = StringUtil.replace(tempRepFilterColumn,"[SESSLOGUSER]", userId);
					%>
					<tr>
						<%=tempRepFilterColumn%>
						<input type="hidden" name="depend<%=repFilterKeywords.get(i)%>" value='<%=((String)repFilterDependOnList.get(i))==null?"":repFilterDependOnList.get(i)%>'>
						<input type="hidden" id="DIV<%=repFilterKeywords.get(i)%>"	value='<%=((String)repFilterDispDIV.get(i))==null?"":repFilterDispDIV.get(i)%>'>
					</tr>
					<%
								}
							} else {
					%>
					<td><%=MC.M_NoFiltersAvailable%><%--No Filters available*****--%></td>
					<%
							}
						}
					%>

						</tr>
					</table>
				</td>
				<td>
					<A type="submit" href="javascript:void(0);" onClick="return validate(document.dashboardpg);"><%=LC.L_Display%></A>
				</td>
			</tr>
		</table>
		<DIV id="repFilterHidden"></DIV>
		</td>
	    </tr>
        </table>
        </DIV>
        </td>
        </tr>
    </Table>

<div class="tmpHeight"></div>
	<TABLE width="99%" height="75%" cellpadding="0" cellspacing="0" border="0" class="midAlign lhsFont" >
		<TR ><TD>
			<iframe name="repDisplay" id="repDisplay"  style="border: solid #9d9d9d 1px;"
			 src="" width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency="true">
			</iframe>
		</TD></TR>
	</TABLE>
	<%} else{
			%>
			  <jsp:include page="accessdenied.jsp" flush="true"/>
			<%
		} //end of else body for page right	

	} //end of if session times out

	else {
		%> 
			<jsp:include page="timeout.html" flush="true" /> 
		<%
 	}%>


<%if (StringUtil.isEmpty(calledfrom) )
		{ %>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
<% } %>


<%if (StringUtil.isEmpty(calledfrom) )
		{ %>
<DIV class="mainMenu" id="emenu"><jsp:include page=getmenu.jsp flush="true" /></DIV>
<% } %>

<script>
	fSetId(document.dashboardpg);
</script>