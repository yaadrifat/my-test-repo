<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=MC.M_MngAcc_OrgDets%><%--Manage Account >> Organization Details*****--%></title>
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>
	<jsp:useBean id="addressB" scope="request" class="com.velos.eres.web.address.AddressJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>

<% 
	String src;
	src= request.getParameter("srcmenu");  
%>
<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<SCRIPT Language="javascript">
// Added by Gopu for September Enhancement #U3
	function  validate(formobj) {				
		var parentOrg = formobj.siteParent.options[formobj.siteParent.selectedIndex].value;		
		var siteIdOrg = formobj.siteId.value;
		if (siteIdOrg == parentOrg) {
			alert("<%=MC.M_CntSameOrg_AsParentOrg%>");/*alert("Cannot assign the same Organization as a Parent Organization");*****/
			formobj.siteParent.focus();
			return false;
		}
	//     formobj=document.site
//		if (!(validate_col('e-Signature',formobj.eSign))) return false
//		if(isNaN(formobj.eSign.value) == true) {
//			alert("Incorrect e-Signature. Please enter again");
//			formobj.eSign.focus();
//			return false;
//		}
		if (!(validate_col('Organization Name',formobj.siteName))) return false   
		if (!(validate_col('Organization Name',formobj.siteType))) return false
		//document.site.submit();
	}
	// Added by Amar for Bugzilla issue # 3026	
		function checkinput(obj,max){
		var result = true;
		if (obj.value.length >= max){
		result = false;
		}
		if (window.event)
		window.event.returnValue = result;
		return result;	
	} 

</SCRIPT>
<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="1"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 
<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession)) {
		String uName = (String) tSession.getValue("userName");	
        String mode = "";
		String accId = (String) tSession.getValue("accountId");	
		String ddSiteParent = null;
		CodeDao cdSite = new CodeDao();
		cdSite.getAccountSites(Integer.parseInt(accId)); 
		mode = request.getParameter("mode");
		int pageRight = 0;
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));
// change by salil 
	if ((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) {
		int siteId=0;  
		int siteAddId=0;
		String siteName = "";
		String siteType = "";
		String siteInfo = "";
		String siteParent = "";
		String siteStatus = "";
		String siteAddress = "";
		String siteCity = "";
		String siteState = "";
		String siteZip = "";
		String siteCountry = "";
		String sitePhone = "";
		String siteEmail = "";
		String siteNotes = "";   // Amarnadh  
		String siteAccount = "";
		String siteIdentifier = "";	
		String siteHidden = "";//KM
		siteId = EJBUtil.stringToNum(request.getParameter("siteId"));		
		String site = request.getParameter("siteParentOrg");	//gopu
		String dSiteType = "" ;
		CodeDao cdType = new CodeDao();
		cdType.getCodeValues("site_type"); 
		if (mode.equals("M")) {
			siteB.setSiteId(siteId);
			siteB.getSiteDetails();
			siteId = siteB.getSiteId();
			siteName = siteB.getSiteName();
			siteStatus = siteB.getSiteStatus();
			
			if (StringUtil.isEmpty(siteStatus))
			{
				siteStatus="";
			}

		/*Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 */

			siteNotes = siteB.getSiteNotes();

			siteIdentifier = siteB.getSiteIdentifier();
			siteHidden = siteB.getSiteHidden(); //KM
			
				if (StringUtil.isEmpty(siteIdentifier)) {
					siteIdentifier = "";
				}		
				siteType = siteB.getSiteCodelstType();
				siteInfo = siteB.getSiteInfo();
				siteInfo = (   siteInfo  == null      )?"":(  siteInfo ) ;	 
				siteParent = siteB.getSiteParent();
				dSiteType = cdType.toPullDown("siteType",EJBUtil.stringToNum(siteType));
				ddSiteParent = cdSite.toPullDown("siteParent",EJBUtil.stringToNum(siteParent));	
				siteAddId= EJBUtil.stringToNum(siteB.getSitePerAdd()); 
				addressB.setAddId(siteAddId);
				addressB.getAddressDetails();
				siteAddress = addressB.getAddPri();
				siteAddress =   (   siteAddress   == null      )?"":(  siteAddress  ) ;	
				siteCity = addressB.getAddCity();
				siteCity =   (   siteCity   == null      )?"":(  siteCity  ) ;	
				siteState = addressB.getAddState();
				siteState =   (   siteState   == null      )?"":(  siteState  ) ;	
				siteZip = addressB.getAddZip();
				siteZip =   (   siteZip   == null      )?"":(  siteZip  ) ;	
				siteCountry = addressB.getAddCountry();
				siteCountry =   (   siteCountry   == null      )?"":(  siteCountry  ) ;	
				sitePhone = addressB.getAddPhone();
				sitePhone =   (   sitePhone   == null      )?"":(  sitePhone  ) ;	
				siteEmail = addressB.getAddEmail();
				siteEmail =   (   siteEmail   == null      )?"":(  siteEmail  ) ;	

				
				siteNotes =  ( siteNotes == null  )?" " : (siteNotes ) ;

			
				siteAccount = siteB.getSiteAccountId();
				
	   } else {

			ddSiteParent = cdSite.toPullDown("siteParent");	
			dSiteType = cdType.toPullDown("siteType");

		  } 
%>
	<Form name="site" id="sitefrm" method="post" action="updatesite.jsp" onsubmit ="if (validate(document.site)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<!-- Added by Gopu for September Enhancement'06 #U3 -->
		<input type="hidden" name="siteParentOrg" value ="<%=site%>">
		
		<!--Commented by Gopu for September Enhancement #U3 -->
	<!--input type="text" name="siteId" Value="<%=siteId%>"-->
	<input type="hidden" name="siteAddId" Value="<%=siteAddId%>">
	<input type="hidden" name="mode" Value="<%=mode%>">
	<br>
		<table width="98%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"> <%=LC.L_Organization_Name%><%--Organization Name*****--%> <FONT class="Mandatory">* </FONT> </td>
				<td> 
					<input type="text" name="siteName" size = 50 MAXLENGTH = 50 Value="<%=siteName%>">
				</td>
			</tr>
			<tr> 
		        <td > <%=LC.L_Type%><%--Type*****--%> <FONT class="Mandatory">* </FONT> </td>
				<td> <%=dSiteType%> </td>
			</tr>
			<tr> 
				<td> &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Description%><%--Description*****--%> </td>
				<td> 
					<input type="text" name="siteInfo" size = 50 MAXLENGTH = 2000 Value="<%=siteInfo%>">
		        </td>
			</tr>
			<tr> 
				<td >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Parent_Org%><%--Parent Organization*****--%> </td>
		        <td> <%=ddSiteParent%></td>					
			</tr>
			<tr> 
				<td > &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Site_Id%><%--Site ID*****--%></td>
	    	    <td><%--Yogendra Pratap : Modified for ACC-22253: Increase the front-end limit for SITE ID field to 50 :14 March 2012 --%>
					<input type="text" name="siteIdentifier" size = 50 MAXLENGTH = 50 Value="<%=siteIdentifier%>">
		        </td>
			</tr>
			<tr> 
				<td > <%=LC.L_Address%><%--Address*****--%> </td>
			    <td> 
					<input type="text" name="siteAddress" size = 50 MAXLENGTH = 50 Value="<%=siteAddress%>">
				</td>				
			</tr>
	<tr> 
			<tr> 
				<td > &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_City%><%--City*****--%> </td>
				<td> 
				  <input type="text" name="siteCity" size = 35  MAXLENGTH = 30 Value="<%=siteCity%>">
				</td>
			</tr>
			<tr> 
				<td > &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_State%><%--State*****--%> </td>
		        <td> 
					<input type="text" name="siteState" size = 35 MAXLENGTH = 30 Value="<%=siteState%>">
				</td>
			</tr>
			<tr> 
				<td > &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_ZipOrPostal_Code%><%--Zip/ Postal Code*****--%> </td>
				<td> 
					<input type="text" name="siteZip" size = 35 MAXLENGTH = 15 Value="<%=siteZip%>">
				</td>
			</tr>
			<tr> 
			<tr> 
				<td > &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Country%><%--Country*****--%> </td>
				<td> 
					<input type="text" name="siteCountry" size = 35 MAXLENGTH = 30 Value="<%=siteCountry%>">
				</td>
			</tr>
			<tr>
				<td >  <%=LC.L_Contact_Phone%><%--Contact Phone*****--%> </td>
				<td> 
					<input type="text" name="sitePhone" size = 35 MAXLENGTH = 100 Value="<%=sitePhone%>">
				</td>
			</tr>
			<tr> 
				<td >  <%=LC.L_Contact_Email%><%--Contact E-Mail*****--%> </td>
		        <td> 
					<input type="text" name="siteEmail" size = 35 MAXLENGTH = 100 Value="<%=siteEmail%>">
				</td>
			</tr>

			<!--Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 -->
			<tr>

			<td >  <%=LC.L_Notes%><%--Notes*****--%> </td>


  <td width="60%"> <TEXTAREA name="siteNotes"  rows=3 cols=50 MAXLENGTH=256 onkeyup="if(this.value.length > 256) this.value=this.value.substr(0,256)" onkeypress="checkinput(this,255)" onclick="checkinput(this,256)"><%=siteNotes%></TEXTAREA> </td>

			</tr>  

	   <% 
	   	//Added by Manimaran for Enh.#U11
		  String checkStr ="";
	    if (siteHidden.equals("1"))
		   checkStr = "checked";
	
   	    if (mode.equals("M")){ %>
	       <tr><td colspan="2"> <input type="checkbox" name="siteHidden" <%=checkStr%>> <%=MC.M_HideOrg_InOrgLkUp%><%--Hide this Organization in Organization and User selection Lookups*****--%><td></td></tr>
	   <%}%>
			<tr>   
				<td> 
					<input type="hidden" name="siteAccount" MAXLENGTH = 15 Value="<%=siteAccount%>">
				</td>
			</tr>
			<tr> 
				<td> 
					<input type="hidden" name="siteId" MAXLENGTH = 15 Value="<%=siteId%>">
					<input type="hidden" name="siteStatus" MAXLENGTH = 15 Value="<%=siteStatus%>">
				
					<input type="hidden" name="src" Value="<%=src%>">
				</td>
			</tr>
		</table>
	<%	if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) {%>
	<br>
	<table width="98%" cellspacing="0" cellpadding="0">
	 <!--  	<td width="50%" align="right" bgcolor="#cccccc"> <span id="eSignMessage"></span>
				e-Signature <FONT class="Mandatory">* </FONT>
	   </td>

	   <td width="20%" bgcolor="#cccccc">
		<input type="password" name="eSign" id="eSign" maxlength="8" onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','Valid e-Sign','Invalid e-Sign','sessUserId')">
	    </td>	-->
		<tr valign="center">
			<td width=35%>&nbsp;</td>
			<td>
				<jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="N"/>
					<jsp:param name="formID" value="sitefrm"/>
					<jsp:param name="showDiscard" value="N"/>
					<jsp:param name="noBR" value="Y"/>
				</jsp:include>  
			</td>
		</tr>
	</table>
	<% } %>
  </Form>

<%} //end of if body for page right

else { %>

  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
		} //end of else body for page right
	}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>

	<div class = "myHomebottomPanel"> 
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>

<div class ="mainMenu" id="emenu"> 
	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>

</html>



