<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Acc_Orgs%><%--Account Organizations*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.BrowserRows,java.sql.*" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<% String src;
String pagenum = "";
int curPage = 0;
long startPage = 1;
String stPage;

src= request.getParameter("srcmenu");
pagenum = request.getParameter("page");

if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);
String orderBy = "";
orderBy = request.getParameter("orderBy");

String order = "";
order = request.getParameter("order");

if (order == null)
{
	order = "asc";
}


%>

<Script>

function setOrder(frm,orderBy) {
	var lorder;
	if (frm.order.value=="asc") {
		frm.order.value= "desc";		
		lorder="desc";
	} else 	if (frm.order.value=="desc") {
		frm.order.value= "asc";
		lorder="asc";	
	}

	lsrc = frm.srcmenu.value;
	lcurPage = frm.page.value;

	frm.action="sitebrowserpages.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&order="+lorder;

	frm.submit(); 
}

</Script>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   
<body>
<br>
<jsp:useBean id="siteB" scope="session" class="com.velos.eres.web.site.SiteJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<DIV class="browserDefault" id="div1">

  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{

	 String uName = (String) tSession.getValue("userName");

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		

     pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));

     if (pageRight > 0 )

	 { 
		
	   int accountId=0;   
	   long cntr = 0;
	   
	   String acc = (String) tSession.getValue("accountId");
	   accountId = EJBUtil.stringToNum(acc);

	   String siteSql = "select b.PK_SITE as pk_site, CODELST_DESC as SITE_TYPE, b.SITE_NAME as site_name, a.SITE_NAME as PARENT_SITE from "  
	   					+ "ER_SITE b,er_codelst c , er_site a  where b.FK_ACCOUNT = " + acc+ " and "
						+ "c.PK_CODELST = b.FK_CODELST_TYPE and "
						+ "a.PK_SITE (+) = b.SITE_PARENT";
		
	   String countSql = "select count(*) from "  
	   					+ "ER_SITE b,er_codelst c , er_site a  where b.FK_ACCOUNT = " + acc+ " and "
						+ "c.PK_CODELST = b.FK_CODELST_TYPE and "
						+ "a.PK_SITE (+) = b.SITE_PARENT";
						
						
						
//ORDER BY b.site_name					

	   long rowsReturned = 0;
	   long showPages = 0;


	   String siteType = null;
	   String siteName = null;
	   String siteParent = null;
	   int siteId=0;
	   boolean hasMore = false;
	   boolean hasPrevious = false;

	   
       BrowserRows br = new BrowserRows();
	     
	   br.getPageRows(curPage,5,siteSql,6,countSql,orderBy,order);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();

%>

  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=MC.M_MngAcc_Org%><%--Manage Account >> Organizations*****--%></P>
  <Form name="sitebrowser" method="post" action="" onsubmit="">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr > 
        <td width = "70%"> 
          <P class = "defComments"><%=MC.M_ListOrg_AldyCreated%><%--The list below displays the Organizations that have already been created for your account*****--%>:</P>
        </td>
        <td width = "30%"> 
          <p align="right"> <A href="sitedetails.jsp?mode=N&srcmenu=<%=src%>"><%=MC.M_AddNew_Org%><%--Add a New Organization*****--%></A> </p>
        </td>
      </tr>
      <tr> 
        <td height="10"></td>
        <td height="10"></td>
      </tr>
    </table>
	
	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="order" value="<%=order%>">
	
    <table width="100%" >
      <tr> 
        <th width="40%" onClick="setOrder(document.sitebrowser,3)"><%=LC.L_Organization_Name%><%--Organization Name*****--%></th>
        <th width="20%" onClick="setOrder(document.sitebrowser,2)"> <%=LC.L_Type%><%--Type*****--%> </th>
        <th width="40%" onClick="setOrder(document.sitebrowser,4)"> <%=LC.L_Parent_Org%><%--Parent Organization*****--%> </th>
      </tr>
      <%

    for(int counter = 1;counter<=rowsReturned;counter++)

	{	
		siteId=EJBUtil.stringToNum(br.getBValues(counter,"pk_site")) ;
		siteType=br.getBValues(counter,"site_type");
		siteName=br.getBValues(counter,"site_name");
		siteParent=br.getBValues(counter,"parent_site");
	
		if ((counter%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td> <A href="../jsp/sitedetails.jsp?siteId=<%=siteId%>&mode=M&srcmenu=<%=src%>"> 
          <%= siteName%> </A> </td>
        <td> <%= siteType%> </td>
        <td> <%= siteParent%> </td>
      </tr>
      <%

		}
%>
    </table>
	<br>
	<table align=center>
	<tr>
<%
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;
	 
	if ((count == 1) && (hasPrevious))
	{   
    %>
	<td colspan = 2>
  	<A href="sitebrowserpages.jsp?srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&order=<%=order%>"><%=LC.L_Back%><%--Back*****--%></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>	
	<%
  	}	
	%>
	<td>
	<%
 
	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "Mandatory"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		
		
	   <A href="sitebrowserpages.jsp?srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&order=<%=order%>"><%= cntr%></A>
       <%
    	}	
	 %>
	</td>
	<%
	  }
	if (hasMore)
	{   
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="sitebrowserpages.jsp?srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&order=<%=order%>"><%=LC.L_Next%><%--Next*****--%></A>
	</td>	
	<%
  	}	
	%>
   </tr>
  </table>
  
  </Form>

  <%
  
  	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>

