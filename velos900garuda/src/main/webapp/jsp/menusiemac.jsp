<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<LINK REL="stylesheet" TYPE="text/css" HREF="menusiemac.css" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language=javascript>
var eOpenMenu=null	;
	function openwin() {
      window.open("#","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400");
	}
function show(elementID){

eOpenMenu=elementID ;

document.all.item(elementID).style.display = "";


}

function hide(elementID){

document.all.item(elementID).style.display = "none";
}

function toogle(elementID){
if (document.all.item(elementID).style.display == "none"){
if ((elementID != eOpenMenu) && (eOpenMenu!=null) ) {
hide(eOpenMenu);

}
show(elementID);

}
else
{
hide(elementID);}
}
function toogle_refresh() {
 if (document.panel.src.value !=null)
 	 toogle(document.panel.src.value);
 }


</script>
</head>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@ page import="com.velos.eres.service.util.LC"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<%
	HttpSession tSession = request.getSession(true);
    String src= request.getParameter("srcmenu");

    int siteRight = 0, groupRight = 0 , userRight = 0, userPers = 7;
	int userProtocol = 0, calLib = 0, userPat = 0, userReports = 0, manageAct = 1, modbudget = 0, budgetrights = 0, dataSafMonGrpRight = 0;
	int adhocqSeq = 0;
	int adhocqModSeq = 0;
	int evLib=0;
	int adHocGrpRight = 0;

	char portalRight = '0';
	char protocolManagementRight = '0';
	int patientPortalSeq = 0,patientPortalModSeq = 0 ;
	int protocolMgmtSeq = 0,protocolMgmtModSeq = 0;
	int portalGroupRight = 0;



	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getAttribute("GRights");

	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();

	acmod.getControlValues("module"); //get extra modules

	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();

	int modseq = 0, budseq = 0,mileSeq = 0, milestoneSeq = 0 ;
	char budRight = '0';
	char mileRight = '0';
	char dataSafMonAppRight = '0';
	char adhocqAppRight = '0';
	int dsmSeq=0 ;

	modseq = acmodfeature.indexOf("MODBUD");
	mileSeq = acmodfeature.indexOf("MODMIL");
	dsmSeq = acmodfeature.indexOf("MODDSM");
	adhocqModSeq =  acmodfeature.indexOf("ADHOCQ");

	budseq	= ((Integer) acmodftrSeq.get(modseq)).intValue();
	milestoneSeq = ((Integer) acmodftrSeq.get(mileSeq)).intValue();
	dsmSeq = ((Integer) acmodftrSeq.get(dsmSeq)).intValue();
	adhocqSeq  = ((Integer) acmodftrSeq.get(adhocqModSeq)).intValue();

    budRight = 	modRight.charAt(budseq - 1);
    mileRight = modRight.charAt(milestoneSeq - 1);
	dataSafMonAppRight = modRight.charAt(dsmSeq - 1);
	adhocqAppRight = modRight.charAt(adhocqSeq - 1);


	//check for organizations
    siteRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSITES"));

	//check for Groups
    groupRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));

	//check for Users
   userRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));

	//check for Personalize Account -- this right has been removed, do not check this
    //userPers = Integer.parseInt(grpRights.getFtrRightsByValue("HPERSHOME"));

	//check for Manage Protocols
    userProtocol = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

	//check for Calendar Library
    calLib = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

  	//check for Event Library
    evLib = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

	//check for Manage Patients
   userPat = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

	//check for Manage Patients
  userReports = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));

      // check for data safety montitoring
  dataSafMonGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("DSM"));
  adHocGrpRight = Integer.parseInt(grpRights.getFtrRightsByValue("ADHOC"));

 if (siteRight == 0 && groupRight == 0 && userRight == 0)
 {
	 manageAct = 0;
 }

  budgetrights = Integer.parseInt(grpRights.getFtrRightsByValue("BUDGT"));

  //for portal and protocol management

	patientPortalModSeq = modCtlDaofeature.indexOf("MODPPORTAL");
	protocolMgmtModSeq = modCtlDaofeature.indexOf("MODPROTOCOL");


	patientPortalSeq = ((Integer) modCtlDaoftrSeq.get(patientPortalModSeq)).intValue();
	protocolMgmtSeq = ((Integer) modCtlDaoftrSeq.get(protocolMgmtModSeq)).intValue();

	portalRight = modRight.charAt(patientPortalSeq - 1);
	protocolManagementRight = modRight.charAt(protocolMgmtSeq - 1);

	portalGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));



   %>

<body onload=toogle_refresh()>
<DIV style=" background-color:#CFC8FF; width:160px;margin-left:25; margin-top:25;border-style:none">
<!-- tree structure -->


<ul style="list-style:none;font-size:11;font-weight:bold;height:8px;">
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict1" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;padding:0px 0px 0px 0px;margin-top:1;margin-left:-37;">
<!--<DIV Class="clsMenuBarItemPictA" id="tdMenuBarItemPict1" >-->
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
<DIV CLASS="clsMenuBarItem" Id="DivMenu1" MAX = 9  style="margin-top:8;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!-- <DIV CLASS="clsMenuBarItemA" Id="DivMenu1" MAX = 9>-->
    <a id=240 href="myHome.jsp?srcmenu=tdMenuBarItem1"><label for="a240"><%=LC.L_My_Homepage%><%--My Homepage*****--%></label></a>
</DIV>
   </ul>
<ul style="list-style:none;font-size:11;font-weight:bold;height:8px;">
<!--<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8px;padding:0px 0px 0px 0px;width:130;">-->
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict7" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top: -10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict8" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
 <DIV CLASS="clsMenuBarItem" Id="DivMenu8" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!-- <DIV CLASS="clsMenuBarItemA" Id="DivMenu1" MAX = 9>-->
    <a id=240 href="ulinkBrowser.jsp?srcmenu=tdMenuBarItem11&selectedTab=2"><label for="a240"><%=LC.L_Personalize_Acc%><%--Personalize Account*****--%></label></a>
</DIV>
   </ul>

<!--Menu for Manage Account-->
	<% if (manageAct > 0)
		{
		%>

<ul style="list-style:none;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;">
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict2" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;padding:0px 0px 0px 0px;margin-top:-10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict2">
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25" >   </DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="DivMenu2" MAX = 9>-->
<DIV CLASS="clsMenuBarItem" Id="DivMenu3" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">

 <a id=250  onclick=toogle("tdMenuBarItem2")><label for="a250"><%=LC.L_Manage_Acc%><%--Manage Account*****--%></label></a> </DIV>
<!--submenu-->
<ul id="tdMenuBarItem2" style="list-style:none;display: none;font-size:11;background-color:'#9A99FF';font-weight:bold;px;padding:0px 0px 0px 0px; width:157px;margin-left:-25; margin-top:5;border-style:none">
<%
	if (siteRight > 0)
	{
	%>
    <DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--   <DIV CLASS="subMenu" Id="tdMenuBarItem21" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" > -->
<DIV CLASS="subMenu" Id="tdMenuBarItem21" MAX = 9>
  	<a id=260 href="sitebrowser.jsp?srcmenu=tdMenuBarItem2"><label for="a260"><%=LC.L_Organizations%><%--Organizations*****--%></label></a>	</DIV>
	<%
	}
	%>
	<%
		if (groupRight > 0)
		{
	%>

	<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"> 	</DIV>
	<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem21" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >  -->
	<DIV CLASS="subMenu" Id="tdMenuBarItem21" MAX = 9>
	<a id=261 href="groupbrowserpg.jsp?srcmenu=tdMenuBarItem2"><label for="a261"><%=LC.L_Groups%><%--Groups*****--%></label></a></DIV>
	<% }%>
	<%
		if (userRight > 0)
		{
	%>
	<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--	<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem21" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20" >  -->
	<DIV CLASS="subMenu" Id="tdMenuBarItem21" MAX = 9 >
	<a id=262 HREF="accountbrowser.jsp?srcmenu=tdMenuBarItem2"><label for="a262"><%=LC.L_Users%><%--Users*****--%></label></a>	</DIV>
	<% }%>
		<%
			if (userPers > 0)
		{
	%>
	<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
	<DIV CLASS="subMenu" Id="tdMenuBarItem21" MAX = 9 >
	<a id=262 HREF="accountlinkbrowser.jsp?srcmenu=tdMenuBarItem2&selectedTab=2"><label for="a263"><%=LC.L_Acc_Links%><%--Account Links*****--%></label></a></DIV>
	<%}%>

	<% if ( (String.valueOf(portalRight).compareTo("1") == 0)  && (portalGroupRight > 0 ))
		{
		%>
	<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
	<DIV CLASS="subMenu" Id="tdMenuBarItem21" MAX = 9 >
	<a id=263 HREF="portal.jsp?srcmenu=tdMenuBarItem2&selectedTab=6"><label for="a263"><%=LC.L_Portal_Admin%><%--Portal Admin*****--%></label></a></DIV>

		<%}%>



	</ul></ul> 	 <%}%>
<!-- End manage Account -->

<!-- Manage Protocol Menu-->
<%
if (userProtocol > 0 (String.valueOf(protocolManagementRight).compareTo("1") == 0) )
 {
%>

<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8 px;padding:0px 0px 0px 0px;width:130;">

<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict3" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top: -10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict3">
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25"> </DIV>
<DIV CLASS="clsMenuBarItem" Id="DivMenu3" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV CLASS="clsMenuBarItem" Id="DivMenu3" MAX = 9 >-->
 <a id=250  onclick=toogle("tdMenuBarItem3")><label for="a250"><%=LC.L_Manage_Pcols%><%--Manage Protocols*****--%></label></a></DIV>
<!-- submenu-->
<ul id ="tdMenuBarItem3" style="list-style:none;display: none;font-size:11;height:50;background-color:'#9A99FF';font-weight:bold;px;padding:0px 0px 0px 0px; width:157px;margin-left:-25; margin-top:5;border-style:none">
<%
if (userProtocol == 5 || userProtocol == 7 )
	 {
	%>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem31" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" > -->
<DIV CLASS="subMenu" Id="tdMenuBarItem31" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >
<a id=270 HREF="study.jsp?srcmenu=tdMenuBarItem3&selectedTab=1&mode=N"><label for="a270"><%=LC.L_New%><%--New*****--%></label></a></DIV>
<%}%>
<%
		if (userProtocol > 0)
	 {
%>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem31" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" > 	-->
<DIV CLASS="subMenu" Id="tdMenuBarItem31" MAX = 9 >
<a id=271 HREF="studybrowserpg.jsp?srcmenu=tdMenuBarItem3"><label for="a271"><%=LC.L_Open%><%--Open*****--%></label></a></DIV>
<%}%>
</ul></ul> <%}%>

<!-- End Manage Protocol-->
<!--Start library menu-->

<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict4" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top:-10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict4" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
 <DIV CLASS="clsMenuBarItem" Id="DivMenu4" MAX = 9   style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV CLASS="clsMenuBarItem" Id="DivMenu4" MAX = 9>-->
<a id=250  onclick=toogle("tdmenubaritem4")><label for="a250"><%=LC.L_Library%><%--Library*****--%></label></a></DIV>
<!--Submenu-->

<ul id ="tdmenubaritem4" style="list-style:none;display: none;font-size:11;height:50;background-color:'#9A99FF';font-weight:bold;px;padding:0px 0px 0px 0px; width:157px;margin-left:-25; margin-top:5;border-style:none">
 <%if (calLib > 0)
 {
%>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem41" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20 ;" > -->
<DIV CLASS="subMenu" Id="tdMenuBarItem41" MAX = 9 >
<a id=280 HREF="protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&selectedTab=1&calledFrom=P"><label for="a280"><%=LC.L_Cal_Lib%><%--Calendar Library*****--%></label></a></DIV>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<%}%>

<%if (evLib > 0)
{
%>

<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem41" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" > -->
<DIV CLASS="subMenu" Id="tdMenuBarItem41" MAX = 9 >
<a id=281 HREF="eventlibrary.jsp?srcmenu=tdmenubaritem4&selectedTab=2&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W">
<label for="a271"><%=LC.L_Evt_Lib%><%--Event Library*****--%></label></a></DIV>
<%}%>
</ul></ul>
<!--end menu Library-->
<!--menu Manager Patients-->
<%	if (userPat > 0)
	 {
	%>

<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict4" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top:-10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict4">
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
<DIV CLASS="clsMenuBarItem" Id="DivMenu5" MAX = 9   style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV CLASS="clsMenuBarItem" Id="DivMenu5" MAX = 9 >-->
<a id=250  onclick=toogle("tdmenubaritem5")><label for="a250"><%=LC.L_Mng_Pats%><%--Manage <%=LC.Pat_Patients%>*****--%></label></a></DIV>
<!--submenu-->
<ul id ="tdmenubaritem5" style="list-style:none;display: none;font-size:11;height:50;background-color:'#9A99FF';font-weight:bold;px;padding:0px 0px 0px 0px; width:157px;margin-left:-25; margin-top:5;border-style:none">
<%
 	    if (userPat == 5 || userPat == 7)
		  {
		%>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem51" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem51" MAX = 9>
<a id=290 HREF="patientdetailsquick.jsp?srcmenu=tdmenubaritem5&mode=N&selectedTab=1&pkey=&page=patient">
	<label for="a290"><%=LC.L_New%><%--New*****--%></label></a></DIV>
<%}%>
<%
 if (userPat > 0)
  {
%>
<DIV style="margin-left:-8"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem51" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem51" MAX = 9>
<!--<a id=291 HREF="allPatient.jsp?srcmenu=tdmenubaritem5&selectedTab=1&searchFrom=initial" >
<label for="a291"><%=LC.L_Open%><%--Open*****--%></label></a>--></DIV>
<%}%>
</ul></ul><%}%>
<!--end menu Manage Patients-->

<!-- Budget Menu-->
<% if (String.valueOf(budRight).compareTo("1") == 0 && budgetrights > 0)
{
%>
<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict4" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top:-10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict4" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
<DIV CLASS="clsMenuBarItem" Id="DivMenu6" MAX = 9   style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV CLASS="clsMenuBarItem" Id="DivMenu6" MAX = 9  >-->
 <a id=250  onclick=toogle("tdmenubaritem6")><label for="a250"><%=LC.L_Budget%><%--Budget*****--%></label></a> </DIV>
<!-- Submenu-->

<ul id ="tdmenubaritem6" style="list-style:none;display: none;font-size:11;height:50;background-color:'#9A99FF';font-weight:bold;px;padding:0px 0px 0px 0px; width:157px;margin-left:-25; margin-top:5;border-style:none">
	<%
    if (budgetrights == 5 || budgetrights == 7)
	  {
	%>

<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem61" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem61" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >
<a id=300 HREF="budget.jsp?srcmenu=tdmenubaritem6&mode=N"><label for="a300"><%=LC.L_New%><%--New*****--%></label></a></DIV>
<%}%>
<%
   if (budgetrights >  0)
	  {
	%>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem61" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem61" MAX = 9>
<a id=301 href="budgetbrowserpg.jsp?srcmenu=tdmenubaritem6" >
<label for="a301"><%=LC.L_Open%><%--Open*****--%></label></a></DIV>
<%}%>
</ul></ul> 	   <%}%>
<!--end menu Budget-->
<!-- start menu Milstone-->
<% if (String.valueOf(mileRight).compareTo("1") == 0)
{
%>
<ul style="list-style:none;font-size:11;font-weight:bold;height:8px;">
<!--<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8px;padding:0px 0px 0px 0px;width:130;">-->
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict7" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top: -10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict7" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
 <DIV CLASS="clsMenuBarItem" Id="DivMenu7" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!-- <DIV CLASS="clsMenuBarItem" Id="DivMenu7" MAX = 9>-->
    <a id=310 href="milestonehome.jsp?srcmenu=tdmenubaritem7"><label for="a310"><%=LC.L_Milestones%><%--Milestones*****--%></label></a>
</DIV>
</ul>
		<%}%>


	<!--end Menu Milestone-->
<!--start menu Reports-->
<% if (userReports > 0)
{
%>
<ul style="list-style:none;font-size:11;font-weight:bold;height:8px;">
<!--<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8px;padding:0px 0px 0px 0px;width:130;">-->
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict7" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top: -10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict8" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
 <DIV CLASS="clsMenuBarItem" Id="DivMenu8" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!-- <DIV CLASS="clsMenuBarItem" Id="DivMenu7" MAX = 9>-->
    <!-- <a id=310 href="repmain.jsp?selectedTab=1&srcmenu=tdmenubaritem8"><label for="a310">Reports</label></a>-->
    <a id=310 href="reportcentral.jsp?selectedTab=1&srcmenu=tdmenubaritem8"><label for="a310"><%=LC.L_Rpt_Central%><%--Report Central*****--%></label></a>
</DIV>
</ul>
		<%}%>

<% if (String.valueOf(dataSafMonAppRight).compareTo("1") == 0 && dataSafMonGrpRight > 0)
{
%>
<!--  <ul style="list-style:none;font-size:11;font-weight:bold;height:8px;">
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict11" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
 <DIV CLASS="clsMenuBarItem" Id="DivMenu11" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
   <a id=310 href="datamonitoring.jsp?srcmenu=tdmenubaritem11"><label for="a310">Data Safety Monitoring</label></a>
</DIV>
</ul>-->
		<%}%>

<%
	if (String.valueOf(adhocqAppRight).compareTo("1") == 0 && adHocGrpRight >= 4)
	{
	%>
<ul style="list-style:none;font-size:11;font-weight:bold;height:8px;">
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict12" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
 <DIV CLASS="clsMenuBarItem" Id="DivMenu12" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
    <a id=310 href="dynrepbrowse.jsp?srcmenu=tdmenubaritem12&selectedTab=1"><label for="a310"><%=LC.L_Adhoc_Queries%><%--Ad-Hoc Queries*****--%></label></a>
</DIV>
</ul>
		<%}%>



<!--end menu Reports-->
<!--start menu Help -->
<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict8" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top:-10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict9">
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
<DIV CLASS="clsMenuBarItem" Id="DivMenu9" MAX = 9   style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!--<DIV CLASS="clsMenuBarItem" Id="DivMenu8" MAX = 9>  -->
<a id=250  onclick=toogle("tdMenuBarItem9")>
<label for="a250"><%=LC.L_Help%><%--Help*****--%></label></a></DIV>
<!--submenu-->
<ul id ="tdMenuBarItem9" style="list-style:none;display: none;font-size:11;height:100;background-color:'#9A99FF';font-weight:bold;px;padding:0px 0px 0px 0px; width:157px;margin-left:-25; margin-top:5;border-style:none">
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem81" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem81" MAX = 9>
<a id=320 href="./help/Velos_eResearch_User_Manual-8-14SEPT2008.pdf" target="Information" onclick="openwin()"><label for="a320"><%=LC.L_Online_Help%><%--Online Help*****--%></label></a></DIV>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem81" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem81" MAX = 9>
<a id=321 href="contactus.html" target="Information" onclick="openwin()"><label for="a321"><%=LC.L_Contact_Us%><%--Contact Us*****--%></label></a></DIV>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem81" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem81" MAX = 9>
<a id=322 href="termsofservice.html" target="Information" onclick="openwin()" ><label for="a322"><%=LC.L_Terms_OfService%><%--Terms of Service*****--%></label></a></DIV>
<DIV style="margin-left:-8;"><img src = "../images/jpg/submenu_pict.jpg" width="17" height="25"></DIV>
<!--<DIV CLASS="clsMenuBarItem" Id="tdMenuBarItem81" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >-->
<DIV CLASS="subMenu" Id="tdMenuBarItem81" MAX = 9 style="margin-top:-20;margin-left:15;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;padding:0px 0px 0px 0px;width:130;height:20;" >
<a id=323 href="privacypolicy.html" target="Information" onclick="openwin()"><label for="a322"><%=LC.L_Privacy_Policy%><%--Privacy Policy*****--%></label></a></DIV>
</ul></ul>
<!--end Menu Help-->



<!--start menu Logout-->
<ul style="list-style:none;font-size:11;font-weight:bold;height:8px;">
<!--<ul style="list-style:none;visibility:visible;position:relative;cursor:hand;font-size:11;font-weight:bold;height:8px;padding:0px 0px 0px 0px;width:130;">-->
<!--<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict9" style="background-color:'#CFC8FF';visibility:visible;position:absolute;cursor:hand;font-size:12;font-weight:bold;height:20px;padding:0px 0px 0px 0px;margin-top:-10;margin-left:-37;">-->
<DIV Class="clsMenuBarItemPict" id="tdMenuBarItemPict9" >
<img src = "../images/jpg/left_menu_pict.jpg" width="17" height="25">   </DIV>
<DIV CLASS="clsMenuBarItem" Id="DivMenu9" MAX = 9  style="margin-top:-4;margin-left:-18;visibility:visible;position:relative;cursor:hand;font-size:12;font-weight:bold;height:20 px;padding:0px 0px 0px 0px;width:130;">
<!-- <DIV CLASS="clsMenuBarItem" Id="DivMenu9" MAX = 9 >-->
<a id=330 href="logout.jsp"><label for="a330"><%=LC.L_Logout%><%--Logout*****--%></label></a> </DIV>
</ul>

</DIV>

</body>
