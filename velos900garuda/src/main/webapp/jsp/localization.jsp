<%-- 
This file is created by BT Team on 31-Aug-2011. 
This file is included in almost all the JSPs where localization changes are required in the entire application
since LC.jsp and MC.jsp are needed for java script and external JS files  
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
// Set character encoding to UTF-8 in every JSP
request.setCharacterEncoding("UTF-8");
%>
<jsp:include page="LC.jsp"></jsp:include>
<jsp:include page="MC.jsp"></jsp:include>