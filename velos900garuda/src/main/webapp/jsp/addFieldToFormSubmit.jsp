<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_FldEdt_BoxSubmit%><%--Field -Edit Box Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>
	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
	
	
	String[] selected = request.getParameterValues("selected");
	String[] deselected = request.getParameterValues("deselected");
	String[] deselectedAll = request.getParameterValues("deselectedAll");
	String formId= request.getParameter("formId");
	String refreshFlag=request.getParameter("refreshFlag");
	String category =request.getParameter("category");
	String keyword =request.getParameter("keyword");
	String name=request.getParameter("name");
	String mode=request.getParameter("mode");
	String section=request.getParameter("section");
	int len=0;
%>
<form name="captureIds" action="addFieldToForm.jsp" onsubmit="" method="post">
<%		
	//out.println("MDOOEOOO"+refreshFlag);	
	//WHEN THE SELECTED SIDE IS SELECTED WE ONLY TAKE INTO CONSIDERATION THE EXISTING FIELDS 
	// ON RIGHT SIDE AND MORE THAT ARE ADDED THE THIS FLAG HANDLES THAT 
	if (refreshFlag.equals("captureIdsSel"))
	{
		
		if (!(selected==null)) 
		{
		 	len = selected.length;
		}
//	out.println("lenlen"+len);	
		for (int i=0;i< len;i++)
 		{ //out.println("lenlen111"+i);	
%>
			<input type="hidden" name="deselected" value="<%=selected[i]%>" >
			
<%
		} //len
		len=0;
		if (!(deselectedAll==null))
		{
		     len = deselectedAll.length;
		} //len deselectedAll
		for(int j= 0 ; j<len ; j++)
		{ %>
			<input type="hidden" name="deselected" value="<%=deselectedAll[j]%>" >
	<%  }   %>
	  
	<input type="hidden" name="formId" value=<%=formId%>>
		<input type="hidden" name="category" value=<%=category%>>
		<input type="hidden" name="keyword" value=<%=keyword%>>
		<input type="hidden" name="name" value=<%=name%>>
		<Input type="hidden" name="mode" value=<%=mode%> >
		<Input type="hidden" name="section" value=<%=section%> >
		
		<script>
		document.captureIds.submit();
		</script>
	<% } //captureIdsSel 
	//WHEN THE DESELECTED SIDE IS CLICKED (OR LEFT ARROW ) WE ONLY TAKE INTO CONSIDERATION THE EXISTING FIELDS 
	// ON LEFT SIDE AND REMOVE THAT HAVE BEEN CHECKED 
 	if (refreshFlag.equals( "captureIdsDSel" ) ) 
	{
		
   		//out.println("MDOOEOOO"+refreshFlag);	
		 len = deselectedAll.length;
		 //out.println("lenlen"+len);	
		len=0;
		int lenDeselect = 0 ;
		if (!(deselectedAll==null))
		{
		    //out.println("lenlen"+len);	
			 len = deselectedAll.length;
			 if (!(deselected==null))
			 {
			 	lenDeselect =  deselected.length; 
			 }
  	
		}
		 boolean flg = false ;
		 if (!(deselected==null))
		 {
			for(int j= 0 ; j<len ; j++)
			{  
				flg= true  ;
				for (int i=0;i<lenDeselect;i++) 
				{
					flg = true ;
					
					 if ( ( deselectedAll[j].equals (  deselected[i])    )  )
					 { 
						//out.println( " deselectedAll[j] " + deselectedAll[j] );
						//out.println();
						//out.println( " deselected[j] " + deselected[i] );
						flg = false ;
						break ;
		  										  	
						} //end of if
						else
						{ 
							flg=true ;
						} 
				} //end of for lenDeselect
				if (  flg )
				{ %>
				<input type="hidden" name="deselected" value="<%=deselectedAll[j]%>" >
				<% }  
			}  //end of for len
				
		 }  // // end of  if deselected = null 
		 else 
		 {
			for(int j= 0 ; j<len ; j++)
			{ %>
			<input type="hidden" name="deselected" value="<%=deselectedAll[j]%>" >
		<%} //end of for 	
	 } //end of else part		  %>				
		<input type="hidden" name="formId" value=<%=formId%>>
		<input type="hidden" name="category" value=<%=category%>>
		<input type="hidden" name="keyword" value=<%=keyword%>>
		<input type="hidden" name="name" value=<%=name%>>
		<Input type="hidden" name="mode" value=<%=mode%> >
		<Input type="hidden" name="section" value=<%=section%> >
		
		<script>
		// TO SUBMIT ON ITS OWN
		document.captureIds.submit();
		</script>
			
<%		} //end of refreshFlag=captureIdsDSel
//THE SUBMIT TAKES ALL THE FIELDS IRRESPECTIVE OF THE CHECKED CRITERIA AND COPIES THEM TO THE FORM
		else if(refreshFlag.equals("submit"))
		{   //out.println("MDOOEOOO"+refreshFlag);	
			String eSign= request.getParameter("eSign");
		   	String oldESign = (String) tSession.getValue("eSign");
			String[] fieldLibIds= null ;
			if (oldESign.equals(eSign)) 
			{
			
				int cnt =  0 ;
				if (!(deselectedAll==null))
				{
				   cnt = deselectedAll.length;
				   	fieldLibIds= new String[cnt];
				} //len deselectedAll
				int errorCode=0;
				
				String creator="";
				String ipAdd="";
				
			
				String accountId="";
		
				creator=(String) tSession.getAttribute("userId");
				ipAdd=(String)tSession.getAttribute("ipAdd");
	    	 	accountId=(String)tSession.getAttribute("accountId");
				refreshFlag=request.getParameter("refreshFlag");
				section=request.getParameter("section");
				
				deselectedAll = request.getParameterValues("deselectedAll");
				formId=request.getParameter("formId");
				int counter = 0 ;
				
				for(counter = 0;counter<cnt;counter++)
					{
						
						int strlen = (deselectedAll[counter]).length();	
						int firstpos = (deselectedAll[counter]).indexOf("VELSCOLON",0); 
						String deselId = (deselectedAll[counter]).substring(0,firstpos - 1);
						
						//System.out.println("deselectedAll[counter]" + deselectedAll[counter]);
						//System.out.println("firstpos" + firstpos);
						//System.out.println("deselId" + deselId);
						fieldLibIds[counter]=deselId ;  
						
				}
				
				errorCode=fieldLibJB.copyMultipleFields(  section,fieldLibIds, creator,ipAdd);
				//out.println("SONIA SONIA +++++++++"+errorCode);
				//out.println("SECTION SECTION --------------"+section);
		
		
				if ( errorCode == -2 || errorCode == -3 )
				{
%>
  	

				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<p class = "successfulmsg" ><%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%> </p>

<%
		
				} //end of if for Error Code
			
				else

				{
%>	
		
				<br><br><br><br><br>
				<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data Saved Successfully*****--%> </p>
				<script>
					window.opener.location.reload();
					setTimeout("self.close()",1000);
			</script>		

<%
				}//end of else for Error Code Msg Display
			}//end of if old esign
	else
	{
%> <!--This is done because incorrectsign has <input> tag for back button that refreshes the previous page and
reset all the values to null.all the selected values get reset and section DD doesnt get any values-->
		<br><br><br><br><br><br><br><br>
<table align="center">
<tr>
<td align=center>

<p class = "successfulmsg">

<%=MC.M_EtrWrongEsign_ClkBack%><%--You have entered a wrong e-Signature. Please click on Back button to enter again.*****--%>

</p>


</td>
</tr>

<tr height=20></tr>
<tr>
	
<td>



		
</td>		
</tr>		
</table>		
<% String agent1 = request.getHeader("USER-AGENT");
    agent1=agent1.toLowerCase();    
    System.out.println("incrorrect" + agent1);
  if ((agent1.indexOf("mozilla") != -1) && (agent1.indexOf("gecko") <0)) {

  	   %>
  	 <table>	<tr><td width="85%"></td><td>&nbsp;&nbsp;

  <button onclick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table> 
	 <%} else{

	 %>
   <table>
   	<!-- Modified by Gopu to fix the Bugzilla Issue #2573 -->
	<tr><td width="85%"></td><td>&nbsp;&nbsp;
		<button onclick="history.go(-2);"><%=LC.L_Back%></button>	
</td></tr>	</table>
<%}%>
<%	  
	}//end of else of incorrect of esign
		
    }//	end of mode =submit
 }//end of if body for session 
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
	} //end of else of the Session 
 

%>

</body>

</html>
