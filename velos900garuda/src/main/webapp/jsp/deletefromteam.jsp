<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page language = "java" import="com.velos.eres.service.util.StringUtil,com.aithent.audittrail.reports.AuditUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_DelFrmStudyTeam%><%--Delete from Study Team*****--%></title>
<jsp:useBean id="teamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="stdSiteRightsB" scope="request" class="com.velos.eres.web.stdSiteRights.StdSiteRightsJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<%
    String src = request.getParameter("srcmenu");
    boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false;
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
	String teamId =request.getParameter("teamId");
	int studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
	int userId = EJBUtil.stringToNum(request.getParameter("userId"));
	String selectedTab = StringUtil.htmlEncode(request.getParameter("selectedTab"));
	

	int teamPK =EJBUtil.stringToNum(teamId);
	
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>
	<FORM name="deleteTeam" id="delTeamId" method="post" action="deletefromteam.jsp" onSubmit="if (validate(document.deleteTeam)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
		<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="copyverfrm"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
 	 <input type="hidden" name="teamId" value="<%=teamId%>">
 	 <input type="hidden" name="studyId" value="<%=studyId%>">
 	 <input type="hidden" name="userId" value="<%=userId%>">
 	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	
	teamB.setId(teamPK);
	int i=0;
	// first delete the user record from 
	
	// Modified for INF-18183 ::: AGodara 
	i=teamB.deleteFromTeam(AuditUtils.createArgs(tSession,"",LC.L_Study));   // Fixed Bug#7604 : Raviesh
	
	
	int r = stdSiteRightsB.deleteStdSiteRights(studyId, userId);
	
	String nextSelectedTab = isIrb ? "irb_personnel_tab" : "5";
	out.println("<META HTTP-EQUIV=Refresh CONTENT=\"0; URL=teamBrowser.jsp?selectedTab="+nextSelectedTab
	        +"&srcmenu=tdmenubaritem3\">");
	} //end esign
	} //end of delMode
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>

</BODY>
</HTML>

