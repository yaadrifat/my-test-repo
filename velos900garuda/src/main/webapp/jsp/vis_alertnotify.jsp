<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="JavaScript1.1">



function  validate(formobj){

     if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
     }

}




function openwin1(formobj,counter) {
	  var names = formobj.enteredByName[counter].value;
	  var ids = formobj.enteredByIds[counter].value;
             patprot=formobj.pat_ProtId.value ;
  alert(patprot)   ;
	  var windowToOpen = "vis_multipleusersearchdetails.jsp?fname=&lname=&from=alertnotify&mode=initial&names=" + names + "&ids=" + ids + "&rownum=" + counter + "&patprot=" + patprot;
//alert(windowToOpen);
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
;}




</SCRIPT>

</head>



<% String src="";
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="alertNotifyB" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*"%>
<%@ page import="com.velos.eres.service.util.*"%>
<body>
<br>

<DIV class="browserDefault" id="div1">


  <%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))
   {
    String mode="N";//request.getParameter("mode");
	String uName = (String) tSession.getValue("userName");

	int pageRight=7;
	Integer rt = null;
	rt = (Integer)tSession.getValue("studyManagePat");
    pageRight = rt.intValue();


	int personPK = 0;
	String patientId = "";
	String dob = "";
	String gender = "";
	String genderId = "";
	String yob = "";
	String protocolId="";
	int age = 0;
	Calendar cal1 = new GregorianCalendar();

	String pkey = request.getParameter("pkey");
	String patProtId=request.getParameter("patProtId");
	String patientCode= request.getParameter("patientCode");
	String page1=request.getParameter("page");
	String studyId = (String) tSession.getValue("studyId");
	String statDesc=request.getParameter("statDesc");
	String statid=request.getParameter("statid");
	String studyVer = request.getParameter("studyVer");
	String enrollId =(String) tSession.getValue("enrollId");

	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(studyId));

    studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();

	patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
	patEnrollB.getPatProtDetails();
	protocolId =patEnrollB.getPatProtProtocolId();

	//if global setting=G or GR, user cannot enter data
	String globalSetting = alertNotifyB.getANGlobalFlag(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId));

	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	} else {
		protocolId = "";
	}

	person.setPersonPKId(EJBUtil.stringToNum(pkey));
	person.getPersonDetails();
	patientId = person.getPersonPId();
	genderId = person.getPersonGender();
	dob = person.getPersonDob();
	gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
	if (gender==null){ gender=""; }
	yob = dob.substring(6,10);
	age = cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob);
%>

<P class = "userName"> <%= uName %> </P>

<P class="sectionHeadings"> <%=MC.M_MngPat_AlrtNotify%><%--Manage <%=LC.Pat_Patients%> >> Alerts and Notifications*****--%> </P>
<jsp:include page="patienttabs.jsp" flush="true">
</jsp:include>
<br>
	<%
if(protocolId != null) {

%>
<table width=100%>
 <tr>
 	<td class=tdDefault width=30%>	<B> <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> Id*****--%>: <%=patientId%> </B>	</td>
	<td class=tdDefault width=20%>	<B> <%=LC.L_Age%><%--Age*****--%>: <%Object[] arguments1 = {age}; %><%=VelosResourceBundle.getLabelString("L_Yrs",arguments1)%><%--<%=age%> years*****--%></B>	</td>
	<td class=tdDefault width=20%>	<B><%=LC.L_Gender%><%--Gender*****--%>: <%=gender%> </B>	</td>
 </tr>
</table>
<table width=100%>
<tr>
	<td class=tdDefault width = 20% ><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:</td><td class=tdDefault><%=studyNumber%></td>
</tr>
<tr>
	<td class=tdDefault><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:</td><td class=tdDefault><%=studyTitle%></td>
</tr>
 <tr>
 	<td class=tdDefault ><%=LC.L_Protocol_Calender%><%--Protocol Calender*****--%>:</td><td class=tdDefault><%=protName%></td>
 </tr>
</table>

<jsp:param name="pkey" value="<%=pkey%>"/>
<jsp:param name="patientCode" value="<%=patientId%>"/>
<jsp:param name="patProtId" value="<%=patProtId%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
<jsp:param name="studyVer" value="<%=studyVer%>"/>
</jsp:include>

<Form name="alertnotify" method=post action="updatealertnotify.jsp" onsubmit="return validate(document.alertnotify)">

<table width="100%" cellspacing="0" cellpadding="0" >
	<tr>
	<td width=60%>
	<p class = "sectionHeadings" ><%=LC.L_Alert%><%--Alert*****--%></p>
	</td>
	<td width=10%>
	<%=LC.L_Send_AsEmail%><%--Send as Email*****--%>
	</td>

	<td width=20% align="center">
	<%=LC.L_Send_ToCellphone%><%--Send To  CellPhone/Pager*****--%>
	</td>
	</tr>

<tr>
<td width=60%>&nbsp;</td>
<td width=20%>&nbsp;</td>
<td width=20%>(<%=MC.M_Specify_CellEmailOrPager%><%--Specify cellphone email ID or pager number*****--%>)</td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" >


	<% EventInfoDao alertNotify = new EventInfoDao();

	alertNotify.getAlertNotifyValues(EJBUtil.stringToNum(patProtId));
	ArrayList anCodelstDescs=alertNotify.getAnCodelstDescs();
	ArrayList anIds=alertNotify.getAnIds();
	ArrayList anCodelstSubTypes=alertNotify.getAnCodelstSubTypes();
	ArrayList anTypes=alertNotify.getAnTypes();
	ArrayList anuserLists=alertNotify.getAnuserLists();
	ArrayList anuserIds=alertNotify.getAnuserIds();
	ArrayList anMobIds=alertNotify.getAnMobIds();
	ArrayList anFlags=alertNotify.getAnFlags();


	String anCodelstDesc="";
	String anId="";
	String anCodelstSubType="";
	String anType="";
	String anuserList="";
	String anuserId="";
	String anMobId="";
	String anFlag="";
	String status="";
	int checkcount=0;

	int length=anIds.size();
//	out.println("length is " +length);
	%>
          	 <input type="hidden" name=pat_ProtId value=<%=patProtId%>>

	<% for (int i=0;i<length; i++) {

	anCodelstDesc=((anCodelstDescs.get(i)) == null)?"-":(anCodelstDescs.get(i)).toString();
	anId=((anIds.get(i)) == null)?"-":(anIds.get(i)).toString();
	anCodelstSubType=((anCodelstSubTypes.get(i)) == null)?"":(anCodelstSubTypes.get(i)).toString();
	anType=((anTypes.get(i)) == null)?"-":(anTypes.get(i)).toString();
	anuserList=((anuserLists.get(i)) == null)?"":(anuserLists.get(i)).toString();
	anuserId=((anuserIds.get(i)) == null)?"":(anuserIds.get(i)).toString();
	anMobId=((anMobIds.get(i)) == null)?"":(anMobIds.get(i)).toString();
	anFlag=((anFlags.get(i)) == null)?"-":(anFlags.get(i)).toString();

	if((anType.equals("A"))&& (anCodelstSubType.substring(0,2).equals("al")))
	{
	checkcount++;
	if(anFlag.equals("1")){
	status="Checked=true";
	}else{ status="";}

	 %>
	<tr>
	<td width=60%>

	<input type="checkbox" name=alertTypes<%=checkcount%> <%=status%> ><%=anCodelstDesc%>
	</td>

	<td width=15%>
	 <input type=text size=15 READONLY name=enteredByName value='<%=anuserList%>'>

	 <input type="hidden" name=enteredByIds value='<%=anuserId%>'>
	 <input type="hidden" name=anIds value='<%=anId%>'>

	 <input type="hidden" name=patProtId value=<%=patProtId%>>
	 <input type="hidden" name=patientCode value=<%=patientCode%>>
	 <input type="hidden" name=studyId value=<%=studyId%>>
	 <input type="hidden" name=statDesc value=<%=statDesc%>>
	 <input type="hidden" name=statid value=<%=statid%>>
	 <input type="hidden" name=pkey value=<%=pkey%>>
	 <input type="hidden" name=page value=<%=page1%>>
	<input type=hidden name=studyVer value=<%=studyVer%>>
	<input type="hidden" name=protocolId value=<%=protocolId%>>

	</td>
	<td width=10%>
	<A HREF=# onClick='openwin1(document.alertnotify,<%=i%>)'><%=LC.L_User_Search%><%--User Search*****--%></A>
	</td>
	<td width=15%>
	<INPUT type=text name=userMobs value='<%=anMobId%>'size=15 >
	</td>
	</tr>


	<% }} %>
</table>





<table width="700" cellspacing="0" cellpadding="0" >
	<tr>
	<td COLSPAN=3><br>
	<p class = "sectionHeadings" ><%=LC.L_Notification_Opts%><%--Notification Options*****--%></p><br>
	</td>
	</tr>


	<% for (int i=0;i<length; i++) {

	anCodelstDesc=((anCodelstDescs.get(i)) == null)?"-":(anCodelstDescs.get(i)).toString();
	anId=((anIds.get(i)) == null)?"-":(anIds.get(i)).toString();
	anCodelstSubType=((anCodelstSubTypes.get(i)) == null)?"-":(anCodelstSubTypes.get(i)).toString();
	anMobId=((anMobIds.get(i)) == null)?"":(anMobIds.get(i)).toString();
	anFlag=((anFlags.get(i)) == null)?"-":(anFlags.get(i)).toString();
	anType=((anTypes.get(i)) == null)?"-":(anTypes.get(i)).toString();


	if(((anType.equals("A"))&&(!(anCodelstSubType.substring(0,2).equals("al")))))
	{
	checkcount++;
	if(anFlag.equals("1")){
	status="Checked=true";
	}else{ status="";}

	%>
	<tr>
	<td>

	<input type="checkbox" name=alertTypes<%=checkcount%> <%=status%> >&nbsp;&nbsp;&nbsp;<%=anCodelstDesc%>
	 <input type="hidden" name=anIds value='<%=anId%>'>
	<INPUT type="hidden" name=userMobs value='' >
	 <input type="hidden" name=enteredByIds value=''>
	</td>

	</tr>

	<% }} %>

	<tr><td>&nbsp;</td></tr>


</table>
	<%
	if ((mode.equals("M") && (pageRight >= 6)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))
	{ if ((globalSetting.equals("N")) || (globalSetting.equals("NR"))) {

	%>

<table>
<tr>
	   <td>
		<%=LC.L_EHypenSign%><%--E-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	  <td>&nbsp;</td>

	   <td>
		<input type="password" name="eSign" maxlength="8">
	   </td>
	  <td>&nbsp;</td>
	   <td align=right>
		<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0">

	</td>
	</tr>
</table>
	<%
		}
 }
%>
</FORM>

<HR class = "thickline" align="left" width="95%">

<table class=tableDefault  width="95%" border="0">
<tr>
	<td width="50%" align="left">
		<p class = "sectionHeadings" ><%=LC.L_Evt_Notifications%><%--Event Notifications*****--%> </p>
	</td>
	<td width="50%" align="right">
	  <% if ((globalSetting.equals("N")) || (globalSetting.equals("NR"))) {%>
		<a onClick="return f_check_perm(<%=pageRight%>,'N')" href="notification.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&pkey=<%=pkey%>&mode=N&patProtId=<%=patProtId%>&page=<%=page1%>&generate=N&studyId=<%=studyId %>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>&protocolId=<%=protocolId%>"> <%=MC.M_Add_AnotherEvtNotfic%><%--Add Another Event Notification*****--%></a>
	  <%}%>
	</td>
</tr>
</table>
<table class=tableDefault  width="95%" border="0">
<tr>
	<th width=25% ><%=LC.L_For_EvtStatus%><%--For Event Status*****--%> </th>
	<th width=40% ><%=LC.L_Notify_User%><%--Notify User*****--%> </th>
	<th width=30% ><%=LC.L_Event%><%--Event*****--%></th>
</tr>
<%checkcount=0; %>
	<% for (int i=0;i<length; i++) {


	anCodelstDesc=((anCodelstDescs.get(i)) == null)?"-":(anCodelstDescs.get(i)).toString();
	anId=((anIds.get(i)) == null)?"-":(anIds.get(i)).toString();
	anCodelstSubType=((anCodelstSubTypes.get(i)) == null)?"-":(anCodelstSubTypes.get(i)).toString();
	anType=((anTypes.get(i)) == null)?"-":(anTypes.get(i)).toString();
	anuserList=((anuserLists.get(i)) == null)?"":(anuserLists.get(i)).toString();
	anMobId=((anMobIds.get(i)) == null)?"":(anMobIds.get(i)).toString();
	%>

	<% if(anType.equals("N")) { 	 %>

	<% if ((checkcount%2)==0) { %>
      <tr class="browserEvenRow">
        <%}
	else{
	 %>
      <tr class="browserOddRow">
      <%
	}
	checkcount++;
	%>
	<td align="center"><a onClick="return f_check_perm(<%=pageRight%>,'E')" href="notification.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&pkey=<%=pkey%>&mode=M&patProtId=<%=patProtId%>&page=	<%=page1%>&generate=N&studyId=<%=studyId %>&statDesc=<%=statDesc%>&statid=<%=statid%>&alertNotifyId=<%=anId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>&protocolId=<%=protocolId%>"> <%=anCodelstDesc%></a></td>
	<td align="center"><%=anuserList%></td>
	<td align="center"><%=anMobId%></td>
	</tr>
	<% } %>
<% } %>

</table>
<HR class = "thickline" align="left" width="95%">
<table class=tableDefault  width="95%" border="0">
<tr>
	<td width="50%" align="left">
		<p class = "sectionHeadings" ><%=LC.L_Crf_Notifications%><%--CRF Notifications*****--%> </p>
	</td>
	<td width="50%" align="right">
	 	<% if ((globalSetting.equals("N")) || (globalSetting.equals("NR"))) {%>
		<a onClick="return f_check_perm(<%=pageRight%>,'N')" href="crfnotify.jsp?srcmenu=<%=src%>&selectedTab=5&pkey=<%=pkey%>&mode=N&patProtId=<%=patProtId%>&page=<%=page1%>&generate=N&studyId=<%=studyId %>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>&protocolId=<%=protocolId%>&fromPage=alertnotify"> <%=MC.M_Add_AnotherCrfNotfic%><%--Add Another CRF Notification*****--%></a>
		<%}%>
	</td>
</tr>
</table>
<TABLE class=tableDefault width="95%" border="0">
  <TR>
  	<th width=25% ><%=LC.L_For_CrfStatus%><%--For CRF Status*****--%> </th>
	<th width=40% ><%=LC.L_Notify_Users%><%--Notify Users*****--%> </th>
	<th width=30% ><%=LC.L_Crf%><%--CRF*****--%></th>
  </TR>
<%
  CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
  crfNotifyDao = crfNotifyB.getCrfNotifyValues(EJBUtil.stringToNum(patProtId)) ;

  ArrayList crfNotIds = null;
  ArrayList crfNotStats = null;
  ArrayList crfNotUsers = null;
  ArrayList crfNumbers = null;
  ArrayList crfIds = null;

  crfNotIds = crfNotifyDao.getCrfNotIds();
  crfNotStats = crfNotifyDao.getCrfNotStats();
  crfNotUsers = crfNotifyDao.getCrfNotUsers();
  crfNumbers = crfNotifyDao.getCrfNumbers();
  crfIds = crfNotifyDao.getCrfNotCrfIds();

  int lenCrf = crfNotifyDao.getCRows();
//  out.println("****" + lenCrf + "****");
	for(int i=0; i<lenCrf ; i++)
	{
		String crfNotId = ((Integer) crfNotIds.get(i)).toString();
		String crfNotStat = (String)crfNotStats.get(i);
		String crfNotUser = (String) crfNotUsers.get(i);
		String crfNumber = (String) crfNumbers.get(i);
		String crfId = (String) crfIds.get(i);

		crfNotId = (crfNotId == null)?"-":crfNotId;
		crfNotStat = (crfNotStat == null)?"-":crfNotStat;
		crfNotUser = (crfNotUser == null)?"-":crfNotUser;
		crfNumber = (crfNumber == null)?"All":crfNumber;
		crfId = (crfId == null)?"":crfId;


	if(i%2==0){
%>
	<TR class="browserEvenRow">
<%
	}else{
%>
	<TR class="browserOddRow">
<%
	}
%>

	<td align="center"><A onClick="return f_check_perm(<%=pageRight%>,'E')" HREF=crfnotify.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=5&pkey=<%=pkey%>&crfNotifyId=<%=crfNotId%>&mode=M&fromPage=alertnotify&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>&protocolId=<%=protocolId%>> <%=crfNotStat%> </A></td>
	<td align="center"><%=crfNotUser%></td>
	<td align="center"><%=crfNumber%></td>
</tr>

<%
	}
%>

</TABLE>

<%



 } else { //else for if of protocolid != null
%>

<P class = "defComments">
<%=MC.M_PatNotAssigned_PcolCal%><%--This <%=LC.Pat_Patient_Lower%> has not been assigned any protocol calendar yet. Please assign a protocol calendar before accessing the Alerts/Notification.*****--%>
</P>

<%
  } //end for if of protocolid != null



















} //end of if session times out
else
{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
   <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>
</body>
</html>
