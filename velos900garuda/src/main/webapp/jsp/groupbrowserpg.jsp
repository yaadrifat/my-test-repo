<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_MngAcc_Grp%><%--Manage Account >> Groups*****--%></title>

<script>

function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	formObj.action="groupbrowserpg.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	formObj.submit(); 
}
</Script>

<%
String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*"%><%@page import="com.velos.eres.service.util.*"%>

<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="2"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 
<%
String pagenum = "";
int curPage = 0;
long startPage = 1;
String stPage;
long cntr = 0;

pagenum = request.getParameter("page");

if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");
// Added by Gopu on 24th March 2005 for fixing the bug No. 2072 Ascending order fo Group Name 
if (orderBy == null)
{
	orderBy = "lower(GRP_NAME)";
}

String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
	orderType = "asc";
}

String searchCriteria = request.getParameter("searchCriteria");
if (searchCriteria==null) {searchCriteria="";}


String searchCriteriaForSql = StringUtil.escapeSpecialCharSQL(searchCriteria);

searchCriteria = StringUtil.htmlEncodeXss(searchCriteria);
 
	
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
	String userId = (String) tSession.getValue("userId");
   	String acc = (String) tSession.getValue("accountId");
   	int accountId = EJBUtil.stringToNum(acc);	
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
			
	if (pageRight > 0 )	{

	   String grpSql = "select  pk_grp, GRP_NAME,GRP_DESC,grp_rights from er_grps where  FK_ACCOUNT = " + accountId +" and (lower(GRP_NAME) like lower('%"+searchCriteriaForSql.trim()+"%'))" ;
		
	   String countSql = "select count(*) from er_grps where  FK_ACCOUNT = " + accountId +" and (lower(GRP_NAME) like lower('%"+searchCriteriaForSql.trim()+"%'))" ;
			
	   long rowsPerPage=0;
   	   long totalPages=0;	
	   long rowsReturned = 0;
	   long showPages = 0;
	   long firstRec = 0;
	   long lastRec = 0;	   
	   long totalRows = 0;	   
	   
	   String grpName = null;
	   String grpDesc = null;
	   int grpId=0;
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   
		rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
		totalPages =Configuration.PAGEPERBROWSER ; 
  
       BrowserRows br = new BrowserRows();
	     
	   br.getPageRows(curPage,rowsPerPage,grpSql,totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();	   
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();	   	   
	   
%>
  
<Form name="groupbrowserpg" method=post onsubmit="" action="groupbrowserpg.jsp?srcmenu=<%=src%>&page=1">
<div class="tmpHeight"></div>
	<table width="99%" border="0" cellpadding="0" cellspacing="0" class="basetbl midalign">
      <tr height="20">
		<td colspan="3"><b><%=LC.L_Search_By%><%--Search By*****--%></b></td>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr height="35">	
 	 	<td width="10%"> <%=LC.L_Group_Name%><%--Group Name*****--%>:</td>
		<td width="10%"> <Input type=text name="searchCriteria"> </td>
		<td width="50%" align="left"><button type="submit"><%=LC.L_Search%></button></td>
		<td width="10%"  align="right"><A class="rhsFont" href=grouplist.jsp?mode=N&srcmenu=tdmenubaritem2&fromPage=groupbrowser> <%=LC.L_Add_MultiGrps_Upper%><%--ADD MULTIPLE GROUPS*****--%></A></td>
		<td width="10%"  align="right" ><A class="rhsfont" href=group.jsp?mode=N&srcmenu=tdmenubaritem2&fromPage=groupbrowser> <%=MC.M_AddNew_Grp_Upper%><%--ADD A NEW GROUP*****--%></A></td>
	</tr>
	<tr>
		 <% if (!(searchCriteria.equals(""))) {%> 
          <P class = "defComments"><%=MC.M_FlwGrpCreated_SrchCrit%><%--The following Groups created for your account match your search criteria*****--%>: "<%=searchCriteria%>"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

</P>
		 <%}%>
		</td>
<td  colspan="5">&nbsp;</td>
				  
	</tr>		
	</table>  	
	<div class="tmpHeight"></div>
	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
		
    <table width="99%" class="outline midalign">
      <tr>
	  <!-- modified by Gopu on 24th March 2005 for fixing the bug No. 2072 Ascending order fo Group Name -->
        <th width="25%" onClick="setOrder(document.groupbrowserpg,'lower(GRP_NAME)')"><%=LC.L_Group_Name%><%--Group Name*****--%> &loz;</th>
        <th width="45%" onClick="setOrder(document.groupbrowserpg,'lower(GRP_DESC)')"><%=LC.L_Description%><%--Description*****--%> &loz;</th>
        <th width="15%" > <%=LC.L_Assign%><%--Assign*****--%></th>
        <th width="15%" ><%=LC.L_Grp_Users%><%--Group Users*****--%></th>				
      </tr>
	  
      <%
    for(int counter = 1;counter<=rowsReturned;counter++)
	{	
		grpId=EJBUtil.stringToNum(br.getBValues(counter,"pk_grp")) ;
		
		grpName=br.getBValues(counter,"GRP_NAME");
		grpDesc=((br.getBValues(counter,"GRP_DESC")==null)?"-":br.getBValues(counter,"GRP_DESC"));		
	
		if ((counter%2)==0) {
  %>
      <tr class="browserEvenRow"> 
        <%
		}
		else{
  %>
      <tr class="browserOddRow"> 
        <%
		}
  %>
        <td> <A href="group.jsp?mode=M&grpId=<%=grpId%>&srcmenu=<%=src%>&fromPage=groupbrowser"> <%=grpName%> </A> </td>
        <td> <%=grpDesc%> </td>
        <td align="center"> <A href="groupRights.jsp?mode=M&groupId=<%=grpId%>&srcmenu=<%=src%>&fromPage=groupbrowser"> <img title="<%=LC.L_Assign_Rights%>" src="./images/AccessRights.gif" border ="0"><%//=LC.L_Assign_Rights%><%--Assign Rights*****--%> </A></td>
        <td align="center"> <A href="groupUsers.jsp?mode=M&groupId=<%=grpId%>&srcmenu=<%=src%>"><img title="<%=LC.L_Grp_Users%>" src="./images/User.gif" border ="0"> <%//=LC.L_Grp_Users%><%--Group Users*****--%> </A></td>	
      </tr>
      <%

		}
%>
    </table>
	
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
	<%}%>	
	</td>
	</tr>
	</table>
	<!-- Bug#9751 16-May-2012 Ankit -->
	<div align="center" class="midalign">
	<%
		if (curPage==1) startPage=1;
	
	    for (int count = 1; count <= showPages;count++)
		{
	     cntr = (startPage - 1) + count;
	
		if ((count == 1) && (hasPrevious))
		{
	    %>
	  	<A href="groupbrowserpg.jsp?searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
		<%
	  	}	
		%>
		<%
	 
		 if (curPage  == cntr)
		 {
	     %>	   
			<FONT class="pageNumber"><%= cntr %></Font>
	       <%
	       }
	      else
	        {
	       %>		
			
		   <A href="groupbrowserpg.jsp?searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
	       <%
	    	}	
		 %>
		<%
		  }
	
		if (hasMore)
		{   
	   %>
	  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="groupbrowserpg.jsp?searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
		<%
	  	}	
		%>
	</div>
  
  </Form>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</DIV>

</body>
</html>

