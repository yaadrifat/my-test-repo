<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<script>


function confirmBox(fldCatName,pageRight, flag ) 
{
          fldCatName=decodeString(fldCatName); 
	if ( flag == 'F' )
	{
		if (f_check_perm(pageRight,'E') == true) 
		{
			var paramArray = [fldCatName];
			msg=getLocalizedMessageString("L_Del_FrmLib",paramArray);/*msg="Delete " + fldCatName + " from Library?";*****/
 	
			if (confirm(msg)) 
			{
    			return true;
			}
			else
			{
				return false;
			}
		} 
		else 
		{
			return false;
		}
	}
	else if ( flag == 'C' )
	{
		if (f_check_perm(pageRight,'E') == true) 
		{
			var paramArray = [fldCatName];
			msg=getLocalizedMessageString("M_DelCat_Lib",paramArray);/*msg="Delete Category " + fldCatName + " from Library?";*****/
 	
			if (confirm(msg)) 
			{
    			return true;
			}
			else
			{
				return false;
			}
		} 
		else 
		{
			return false;
		}
	}
}



function openWin(Id,flag,formobject,pageRight)
{
	if (flag =="E")
	{
		
		if (f_check_perm(pageRight,'N') == true) 
		{  
		param1="editBox.jsp?fieldLibId="+Id+"&mode=N "  ; 
		//&srcmenu="+formobject.srcmenu.value ;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=600 ,top=90 ,left=100");
		windowName.focus();
		return ;
		}
	}
	
	else if (flag =="M")
	{
		
		if (f_check_perm(pageRight,'N') == true) 
		{  	
		param1="multipleChoiceBox.jsp?fieldLibId="+Id+"&mode=N" ;
		//&srcmenu="+formobject.srcmenu.value ;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=600,top=25 ,left=150");
		windowName.focus();
		return ;
		}
	}
	else if (flag =="C")
	{		
		
		if (f_check_perm(pageRight,'N') == true) 
		{  
		param1="fieldCategory.jsp?&mode=N&type=C"  ; 
		//&srcmenu="+formobject.srcmenu.value ;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300 , top=200 ,left=250");
		windowName.focus();
		return ;
		}
	}
	
	
	else if ( (flag =="ET") || (flag=="EN") || (flag=="ED" ) || (flag=="EI") )
	{
		

		//if (f_check_perm(pageRight,'E') == true) {  
		param1="editBox.jsp?fieldLibId="+Id+"&mode=M" ; 
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=600,top=90 ,left=100");
		windowName.focus();
		return ;
		//}
	}
	
	else if ( (flag =="ML")  || (flag=="MD") || (flag=="MC") || (flag=="MR") )
	{
					
		
		//if (f_check_perm(pageRight,'E') == true) {  			
		param1="multipleChoiceBox.jsp?fieldLibId="+Id+"&mode=M" ;
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=600,top=25 ,left=150");
		windowName.focus();
		return ;
		//}
	}
	else if  (  flag =="CA" )
	{										
			
			//if (f_check_perm(pageRight,'E') == true) {  
			param1="fieldCategory.jsp?catLibId="+Id+"&type=C&mode=M" ; 
			windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300, top=200 ,left=250");
			windowName.focus();
			return ;
			//}
	}
	
}
////////////// FOR PAGINATION


//modified by gopu on 21st March for fixing the bug 2066
//function setOrder(formObj,orderBy,catFld) //orderBy column number
function setOrder(formObj,orderBy) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	//Added by Gopu on 15th March 2005 for Mahi Enhancement Phase II in Field Library Sorting
	setSearchFilters(document.fieldLibrary);	

//if(catFld == 1){
	//	tmporder = orderBy + lorderType + ",fld_name asc";
	//formObj.action="fieldLibraryBrowser.jsp?mode=final&srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+ orderBy +"&orderType="+lorderType + ",lower(fld_name) asc";
//		orderBy+"&orderType="+lorderType ;	
	//}else{
	//formObj.action="fieldLibraryBrowser.jsp?mode=final&srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	//}




	formObj.action="fieldLibraryBrowser.jsp?mode=final&srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+ orderBy +"&orderType="+lorderType;
	formObj.submit(); 
}



 function setSearchFilters(form)
 {
  	  form.nameSearch.value=form.name.value;
 	  form.keywordSearch.value=form.keyword.value;
	  form.categorySearch.value=form.category.value;
 }

function openWinCopy(formobject,pageRight)
   {
	if (f_check_perm(pageRight,'N') == true) 
	{  	
		windowName= window.open("copyFromFieldLibrary.jsp?mode=initial","information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=870,height=600,top=50 ,left=100");
		windowName.focus();
		return;
	}
   }
</script>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>


<title><%=LC.L_Fld_LibBrowser%><%--Field Library Browser*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% String src;
src= request.getParameter("srcmenu");
%>


<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>    


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="catLibJB" scope="request" class="com.velos.eres.web.catLib.CatLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<% 
   HttpSession tSession = request.getSession(true); 

%>

<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%
	if (sessionmaint.isValidSession(tSession))
	{%>
<DIV class="tabDefTopN" id="div1"> 
	<jsp:include page="librarytabs.jsp" flush="true"></jsp:include>
</div>
<DIV class="tabDefBotN" id="div1"> 
	<%
		int pageRight = 0;	
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB"));
		String modRight = (String) tSession.getValue("modRight");
		int patProfileSeq = 0, formLibSeq = 0;

		// To check for the account level rights
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProfileAppRight = '0';
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);
	
		
	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
		{
		
		String fldLibId="";
		String fkCategory="";
		String fldName="";
		String fldUniqueId="";
		String fldDescription="";	 
		String fldType="";
		String fldKeyword="";
		String catLibName="";	
		String catLibId="";
		String fldDataType="";		
		String tmpFldDataType= "";
		int counter = 0;
		int len=0;
		int tempAccountId= 0 ;
		int fkCatlib = 0 ;
		
		////////SEARCH FILTERS

	  String  nameSearch = "";
	  String  keywordSearch = ""; 
	  String  categorySearch = ""; 
	   
	if (( request.getParameter("nameSearch"))!= null &&  (request.getParameter("nameSearch").length() > 0)  )		    nameSearch= request.getParameter("nameSearch");
   	if (( request.getParameter("keywordSearch"))!= null &&  (request.getParameter("keywordSearch").length() > 0)  )		    keywordSearch= request.getParameter("keywordSearch");	
   	if (( request.getParameter("categorySearch"))!= null &&  (request.getParameter("categorySearch").length() > 0)  )		    categorySearch= request.getParameter("categorySearch");	

	////////////////////////////		
		
		
		String uName = (String) tSession.getValue("userName");
		String accountId=(String)tSession.getAttribute("accountId");
		String selectedTab=request.getParameter("selectedTab");
		String mode= request.getParameter("mode");
		fkCategory= request.getParameter("category");
		if ( ( categorySearch != null )   || (   !categorySearch.equals("null")  )  ) fkCategory = categorySearch ;
		 
		fldName= request.getParameter("name");
		if (  nameSearch != null || (!nameSearch.equals("null")  )   ) fldName = nameSearch ;
		
		
		
		fldKeyword= request.getParameter("keyword");
		if ( keywordSearch != null || (!keywordSearch.equals("null") )   )   fldKeyword = keywordSearch ;
		
		tempAccountId=EJBUtil.stringToNum(accountId);
		 fkCatlib = EJBUtil.stringToNum(fkCategory);
		
		if (fldName == null ) 
		{
			fldName="";
		}
		
		if (fldKeyword == null ) 
		{
			fldKeyword="" ;
		}


///FOR HANDLING PAGINATION
	String pagenum = "";
	int curPage = 0;
	long startPage = 1;
	String stPage;
	long cntr = 0;

	pagenum = request.getParameter("page");

	if (pagenum == null)
	{
		pagenum = "1";
	}
	curPage = EJBUtil.stringToNum(pagenum);

	String orderBy = "";
	orderBy = request.getParameter("orderBy");

	String orderType = "";
	orderType = request.getParameter("orderType");
	    
	String str2 ="";
	String str3 ="";
	String str4 ="";
	String str5 ="";
	String str6 ="";
	String str7 ="";
	String str1 ="";
	String count1="";
	String count2="";
	String sortOrderType="";
	
	if (orderType == null)
	{
		orderType = "asc";
	} 
 //Added on 6th April for fixing the bug 2066 

	if (EJBUtil.isEmpty(orderBy) || (orderBy==null)){
				orderBy="CATLIB_NAME";
				sortOrderType="lower(CATLIB_NAME) asc ,lower(FLD_NAME) asc " ;
	} else {
	 //Added by gopu on 21st March for fixing the bug 2066 instring & to cut the order
//	 if(orderType.indexOf(",") >0){
//		orderType = orderType.substring(0,orderType.indexOf(","));
//	}

		if (orderBy.equals("CATLIB_NAME")){
			sortOrderType = "lower(CATLIB_NAME) "+orderType+", lower(FLD_NAME) asc " ;
		}
		else {
			sortOrderType = "lower(CATLIB_NAME) asc , lower("+orderBy+") "+orderType ;
		}
	}

			//Query modified by Gopu on 15th March 2005 for Mahi Enhancement Phase II in Field Library Sorting			
			 str1="SELECT NVL(FL.PK_FIELD, -999)   PK_FIELD,  CAT.PK_CATLIB FK_CATLIB , NVL(FL.FLD_LIBFLAG,'-') FLD_LIBFLAG ,"
						+"  NVL(FL.FLD_NAME,'-' ) FLD_NAME , case when (nvl(length(FLD_DESC),0)<100) then NVL(FLD_DESC,'-')"
						+"  when (length(FLD_DESC)>=100) then substr(FLD_DESC,1,100)||'...' end	 as  FLD_DESC, NVL(FL.FLD_UNIQUEID,'-') FLD_UNIQUEID , " 
						+"  NVL(FL.FLD_SYSTEMID,'-') FLD_SYSTEMID, NVL(FL.FLD_KEYWORD, '-') FLD_KEYWORD,NVL(FL.FLD_TYPE, '-') FLD_TYPE,"
						+"  NVL(FL.FLD_DATATYPE,'-' ) FLD_DATATYPE,NVL(FL.FLD_INSTRUCTIONS,'-') FLD_INSTRUCTIONS,NVL(FL.FLD_FORMAT,'-') FLD_FORMAT ,"
						+"  NVL(FL.FLD_REPEATFLAG,0)FLD_REPEATFLAG , CAT.CATLIB_NAME CATLIB_NAME" ;

			//Query modified by Gopu on 15th March 2005 for Mahi Enhancement Phase II in Field Library Sorting
			 str2="  FROM   ER_CATLIB CAT  left outer join ER_FLDLIB FL on ( FL.FK_CATLIB =  CAT.PK_CATLIB  and " 
						+ " fl.fk_account="+tempAccountId +" AND  FL.RECORD_TYPE <>'D' and FL.FLD_LIBFLAG = 'L' ) "
						+ " where cat.fk_account="+tempAccountId +" AND CAT.RECORD_TYPE <>'D' AND CATLIB_TYPE='C' ";
			 
		/*				str2="  FROM ER_FLDLIB FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT=  "+tempAccountId
						+" AND  FL.RECORD_TYPE <> 'D'   AND CAT.RECORD_TYPE <> 'D'  "
						+" AND   FL.FK_CATLIB = CAT.PK_CATLIB  " 
						+" AND FL.FLD_LIBFLAG = 'L' ";
		*/
		 

			
			if ( !fldName.equals(""))
			{
				 str3=" AND lower(FLD_NAME) LIKE lower('%"+fldName.trim()+"%')";
			}
			else
			{
				 str3= " ";
			}
			
			if ( !fldKeyword.equals("")) 
			{
			   str4=" AND lower(FLD_KEYWORD)  LIKE lower('%"+fldKeyword.trim()+"%') " ;	
			}
			else
			{
				str4=" ";
			}
			
			if ( fkCatlib == 0 || fkCatlib == -1 )
			{
			  	str5= "  " ;
				
			}
			else
			{
				str5 = " AND FK_CATLIB = "+fkCatlib;
			}
	// Commented by Gopu on 12th March 2005 for Mahi Enhacement Phase II. 
	// str6 can be used for retrieving the category recordsets, which does not have any fields. 
			
/*			if ( fkCatlib == 0 || fkCatlib == -1 )
			{
			  	str6= "  UNION  (   SELECT  NULL   , PK_CATLIB  ,NULL, "
					+ "  NULL 	, NULL , NULL ,"
					+ "  NULL , NULL , NULL , "
					+ "  NULL , NULL , NULL , "
					+ "  NULL , CATLIB_NAME  "
					+ "  FROM ER_CATLIB  WHERE FK_ACCOUNT =  " +tempAccountId
					+ "  AND CATLIB_TYPE='C' AND RECORD_TYPE <> 'D'  "
					+ "  AND PK_CATLIB  NOT IN ( SELECT FK_CATLIB "
					+ "	 FROM ER_FLDLIB F , ER_CATLIB C    " 
					+ "  WHERE   F.FK_ACCOUNT  = "+tempAccountId +"   AND "
					+ "  F.RECORD_TYPE <> 'D' AND FK_CATLIB = PK_CATLIB  )  ) " ; 
				
			}
			else
			{
				str6 = "  ";
			}*/
			
//				str7="order by  ( FK_CATLIB )    " ;
		
			
				
//			String finalLibSql = str1+str2+str3+str4+str5+str6+str7 ;
			String finalLibSql = str1+str2+str3+str4+str5;
			String countSql = "";
			
			if ( fkCatlib == 0 || fkCatlib == -1 )
			{
					if (     ! fldName.equals("")  )
					{
						count1=" AND lower(FLD_NAME) LIKE lower('%"+fldName.trim()+"%')";
					}
					else
					{
					 count1= " ";
					}
					if ( !fldKeyword.equals("")) 
					{
				 	  count2=" AND lower(FLD_KEYWORD)  LIKE lower('%"+fldKeyword.trim()+"%') " ;	
					}
					else count2="";
			
					/*countSql = "select count(*) from  (   " 
								+"  SELECT		 NVL(FL.PK_FIELD, -999)   PK_FIELD,  FL.FK_CATLIB   "   
								+"	FROM ER_FLDLIB  FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT=  " +tempAccountId 
								+"	AND  FL.RECORD_TYPE <> 'D'     AND CAT.RECORD_TYPE <> 'D'    "   
								+ " AND   FL.FK_CATLIB = CAT.PK_CATLIB  AND FL.FLD_LIBFLAG = 'L'   "   + count1 + count2  
								+"  UNION  (   SELECT  NULL   , PK_CATLIB   FROM ER_CATLIB  WHERE FK_ACCOUNT = "+ tempAccountId 
								+"  AND CATLIB_TYPE='C' AND RECORD_TYPE <> 'D'   " 
								+"  AND PK_CATLIB  NOT IN  " 
								+" ( SELECT FK_CATLIB     FROM ER_FLDLIB F , ER_CATLIB C    WHERE   F.FK_ACCOUNT  = " + tempAccountId    
								+" AND   F.RECORD_TYPE <> 'D'   " 
								+ "AND FK_CATLIB = PK_CATLIB  )   )    )  "   ;*/
			}
			else
			{
					if (     ! fldName.equals("")  )
					{
						count1=" AND lower(FLD_NAME) LIKE lower('%"+fldName.trim()+"%')";
					}
					else
					{
					 count1= " ";
					}
					if ( !fldKeyword.equals("")) 
					{
				 	  count2=" AND lower(FLD_KEYWORD)  LIKE lower('%"+fldKeyword.trim()+"%') " ;	
					}
					else 
					{ count2= ""; }
					
				
				/*countSql = "SELECT  count( PK_FIELD)  "
								+"	FROM ER_FLDLIB   FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT = " +   tempAccountId
								+" AND  FL.RECORD_TYPE <> 'D'   " 
								+" AND CAT.RECORD_TYPE <> 'D'  "   
								+" AND   FL.FK_CATLIB = CAT.PK_CATLIB  "   
								+" AND FL.FLD_LIBFLAG = 'L'     AND FL.FK_CATLIB = " + fkCatlib+count1+count2 ; */
			}
			
			
			
	   long rowsPerPage=0;
   	   long totalPages=0;	
	   long rowsReturned = 0;
	   long showPages = 0;
			
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;	   
	   long totalRows = 0;	   	   
	   
		rowsPerPage =  Configuration.MOREBROWSERROWS ;
		totalPages =Configuration.PAGEPERBROWSER ;

		countSql = "Select count(9) from ("+finalLibSql + ")";
		
		//System.out.println("countSql" +countSql);
		//System.out.println("finalLibSql " + finalLibSql);
		
       BrowserRows br = new BrowserRows();
//     br.getPageRows(curPage,rowsPerPage,finalLibSql,totalPages,countSql,orderBy,orderType);
       br.getPageRows(curPage,rowsPerPage,finalLibSql,totalPages,countSql,sortOrderType,"");
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();	 
	   totalRows = br.getTotalRows();	   
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();	   	 
		

////////////////////////////////////////////////////////////////	
	
		
		ArrayList fldLibIds= null; 
		ArrayList catLibIds= null; 
		ArrayList fldNames= null;
		ArrayList fldUniqueIds= null;
		ArrayList fldDescriptions= null;
		ArrayList fldTypes= null;
		ArrayList catLibNames= null;
		ArrayList fldDataTypes= null;

		//here we will get all the categories
		
		String catLibType="C";
		CatLibDao catLibDao= new CatLibDao();
		ArrayList categoryIds= new ArrayList();
		ArrayList categoryDesc= new ArrayList();
		String categoryPullDown;
		int forLastAll = 0 ;  
		FieldLibDao fieldLibDao = new FieldLibDao();

		if ( mode.equals("initial"))
		{
			mode= "final";		
		
			
			catLibDao= catLibJB.getCategoriesWithAllOption(tempAccountId,catLibType,forLastAll);
		
			categoryIds = catLibDao.getCatLibIds();
			categoryDesc= catLibDao.getCatLibNames();
			categoryPullDown=EJBUtil.createPullDown("category",-1, categoryIds, categoryDesc);
			
			
					
		}
		else
		{
			int tempfkCategory= 0;
			
			
			if (fkCategory != null)
			tempfkCategory=EJBUtil.stringToNum(fkCategory);
					
		
			//to create the Category Pull Down
			catLibDao.getCategoriesWithAllOption(tempAccountId,catLibType,forLastAll);
			categoryIds = catLibDao.getCatLibIds();
			categoryDesc= catLibDao.getCatLibNames();
			categoryPullDown=EJBUtil.createPullDown("category", tempfkCategory, categoryIds, categoryDesc);
			
			
	
			
		}
		
		

		//if (pageRight >= 4 )

   		//{

%>
			 <Form name="fieldLibrary" action="fieldLibraryBrowser.jsp?srcmenu=<%=src%>&page=1&mode=final"  method="post" onSubmit= "setSearchFilters(document.fieldLibrary)">
			<Input type="hidden" name="mode" value=<%=mode%> >
			<Input type="hidden" name="selectedTab" value=<%=selectedTab%> >
			<Input type="hidden" name="page" value="<%=curPage%>">
			<Input type="hidden" name="orderBy" value="<%=orderBy%>">
			<Input type="hidden" name="orderType" value="<%=orderType%>">
			<Input type="hidden" name="nameSearch" >
			<Input type="hidden" name="keywordSearch">
			<Input type="hidden" name="categorySearch">
			<Input type="hidden" name="srcmenu" value="<%=src%>">
			
		
	     	  
			    				
				<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign" >
		<div class="tmpHeight"></div>
      <tr height="20" >
		<td colspan="7"><b><%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr >
				      <td width="9%"><%=LC.L_Category%><%--Category*****--%>: </td>
			          <td width="23%"><%=categoryPullDown%> </td>
					  <td width="10%" align="right"><%=LC.L_Fld_Name%><%--Field Name*****--%>: &nbsp; </td>
			          <td width="20%"><input type="text" name="name" size = 15 MAXLENGTH = 50 value="<%=fldName%>"> </td>
					  <td width="10%" align="right"><%=LC.L_Keyword%><%--Keyword*****--%>: &nbsp;</td>
			          <td width="18%"><input type="text" name="keyword" size = 15 MAXLENGTH = 50 value="<%=fldKeyword%>"> </td>
					  <td width ="12%"><button type="submit"><%=LC.L_Search%></button></td>				
</tr>				
</table>
<div class="tmpHeight"></div>
<table width="98%">
	<tr>
	<td width="26%"><P class="defComments"> <%=LC.L_Lib_Fields%><%--Library Fields*****--%></P></td>
	<td width="18%" align="right"><A href="#" onClick="openWinCopy(document.fieldLibrary,<%=pageRight%>)"><%=MC.M_Copy_ExistingFld_Upper%><%--COPY AN EXISTING FIELD*****--%> </A></td>
	<td width="18%" align="right">
		<A href="#" onClick="openWin(0,'C',document.fieldLibrary,<%=pageRight%>) ">
			<img src="../images/jpg/AddNewCategory.gif" border="0" title="<%=LC.L_NewFldCat%>">
		</A>
	</td>
	<td width="18%" align="right"> <A href="#" onClick=" openWin(0,'E',document.fieldLibrary,<%=pageRight%>)"><%=LC.L_Add_EditFld_Upper%><%--ADD EDIT FIELD*****--%> </A></td>
	<td width="18%" align="right"><A href="#" onClick="openWin(0,'M',document.fieldLibrary,<%=pageRight%>) "><%=MC.M_AddMulti_Fld_Upper%><%--ADD MULTIPLE CHOICE FIELD*****--%> </A></td>
	</tr>
</table>
					
				<!--</FORM>
				
			<Form name="fieldLibraryBrowser2" action=" "  method="post" >    !-->
				
				<table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0" >
					<tr>
				        <th width="15%" onClick="setOrder(document.fieldLibrary,'CATLIB_NAME')"> <%=LC.L_Category%><%--Category*****--%> &loz; </th>

				        <th width="20%" onClick="setOrder(document.fieldLibrary,'FLD_NAME')"> <%=LC.L_Fld_Name%><%--Field  Name*****--%> &loz; </th>
				        <th width="15%" onClick="setOrder(document.fieldLibrary,'FLD_UNIQUEID')"> <%=MC.M_FldId_Lib%><%--Field ID(in Library)*****--%> &loz;</th>
						<th width="25%" onClick="setOrder(document.fieldLibrary,'FLD_DESC')"><%=LC.L_Description%><%--Description*****--%> &loz;</th>
						<th width="15%" onClick="setOrder(document.fieldLibrary,'FLD_DATATYPE')"><%=LC.L_Fld_Type%><%--Field Type*****--%> &loz;</th>
						<!-- commented by gopu to fix #2373 -->
						<th width="15%"> <%=LC.L_Delete%><%--Delete*****--%></th>
					</tr>

		<%     
	
					String tempStr2 = "";
					String catLibNameHref="";				
					String tempStr1 = "";
					String deleteHref = "";
					String fldNameHref="";
				
																				
					for(counter = 1;counter<=rowsReturned;counter++)
					{  

						tempStr1= br.getBValues(counter,"FK_CATLIB");	

						fldLibId= br.getBValues(counter,"PK_FIELD");	
						fldLibId = (  fldLibId==null )?"0":(  fldLibId) ;						
			
						if ( counter !=1 )
						{ 
						
						  tempStr2 = br.getBValues(counter-1,"FK_CATLIB");	
						 }

										 
						if (   ( counter == 1 ) || (tempStr1!=null && ! tempStr1.equals(tempStr2 ) )    )
						{
						
							catLibId= br.getBValues(counter,"FK_CATLIB");	
							catLibName=  br.getBValues(counter,"CATLIB_NAME");	
							catLibNameHref="<A href=\"# \" onClick=\"openWin( '"+catLibId+"' , 'CA' ,document.fieldLibraryBrowser2,'"+pageRight+"') \" >" + catLibName+"</A>" ;
							// modified by gopu to fix #2373
							//deleteHref = "<A href= 'catLibDelete.jsp?catLibId=" + catLibId + "&mode=initial&srcmenu=" + src + "&selectedTab="+selectedTab+"' onClick=\"return confirmBox('"+catLibName+"','"+pageRight+"','C')\">Delete  </A> "  ;
							deleteHref = "<A href= 'catLibDelete.jsp?catLibId=" + catLibId + "&mode=initial&srcmenu=" + src + "&selectedTab="+selectedTab+"' onClick=\"return confirmBox('"+catLibName+"','"+pageRight+"','C')\"><img src=\"./images/delete.gif\" title=\""+LC.L_Delete+"\" border=\"0\"/>  </A> "  ;
						%>
						<tr>
							<td><%=catLibNameHref%></td>
							<td>&nbsp;&nbsp; </td>
							<td>&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>	
							<td>&nbsp;&nbsp;</td>	
							<td align="center"><%=deleteHref%></td>
						
							
						</tr>
						<%
						}
															
								
								catLibName="";	
								catLibNameHref="&nbsp;&nbsp;";
						
									
								fldName = (     ( br.getBValues(counter,"FLD_NAME")  ) == null)?"-":(  br.getBValues(counter,"FLD_NAME")   );		
								
								fldUniqueId = (  (br.getBValues(counter,"FLD_UNIQUEID")   ) == null)?"-":br.getBValues(counter,"FLD_UNIQUEID") ;
																
								fldDescription = (  (br.getBValues(counter,"FLD_DESC")      ) == null)?"-":br.getBValues(counter,"FLD_DESC") ;
									
								tmpFldDataType=(   ( br.getBValues(counter,"FLD_DATATYPE")    ) == null)?"-":br.getBValues(counter,"FLD_DATATYPE") ;

										
								if (tmpFldDataType.equals("ET") ) 
								{ fldDataType="Text"; }
								if (tmpFldDataType.equals("EN") ) 
								{ fldDataType="Number"; }
								if (tmpFldDataType.equals("ED") ) 
								{ fldDataType="Date"; }
								if (tmpFldDataType.equals("EI") ) 
								{ fldDataType="Time"; }
								if (tmpFldDataType.equals("ML") ) 
								{ fldDataType="Lookup"; }
								if (tmpFldDataType.equals("MD") ) 
								{ fldDataType="Dropdown"; }
								if (tmpFldDataType.equals("MC") ) 
								{ fldDataType="Checkbox"; }
								if (tmpFldDataType.equals("MR") ) 
								{ fldDataType="Radio Button"; }  
								if (tmpFldDataType.equals("-") )
								{ fldDataType="-"; }  				
								
								 if ( fldLibId.equals("0") ) 
								{
									deleteHref = "-" ;
									fldNameHref="-";
								}  
								else 
								{  
									//Added by Gopu on 15th March 2005 for Mahi Enhancement Phase II in Field Library Sorting 
									deleteHref="-";
									fldNameHref="-";
									if (!fldName.equals("-")) {
									// modified by gopu to fix #2373
								//	deleteHref = " <A href= 'fieldLibDelete.jsp?fieldLibId=" + fldLibId + "&mode=initial&srcmenu=" + src + "&selectedTab="+selectedTab+"' onClick=\"return confirmBox('"+StringUtil.encodeString(fldName)+"','"+pageRight+"','F')\">Delete  </A> "  ;
									deleteHref = " <A href= 'fieldLibDelete.jsp?fieldLibId=" + fldLibId + "&mode=initial&srcmenu=" + src + "&selectedTab="+selectedTab+"' onClick=\"return confirmBox('"+StringUtil.encodeString(fldName)+"','"+pageRight+"','F')\"><img src=\"./images/delete.gif\" title=\""+LC.L_Delete+"\" border=\"0\"/>  </A> "  ;
									
									fldNameHref="<A href=\"# \" onClick=\"openWin( '"+fldLibId+"' , '"+tmpFldDataType+"' ,document.fieldLibraryBrowser2, '"+pageRight+"') \" >" + fldName+"</A>" ;		
									}
								
						} //to show the GUI with a blank row for the category 
									
								
						if ((counter%2)==0) 
						{

						%>

					      	<tr class="browserEvenRow"> 

						<%}else
						 { %>

				      		<tr class="browserOddRow"> 

						<%}  %>	
					
						
						<td><%=catLibNameHref%></td>
						<td> <%=fldNameHref%> </A></td>
						<td><%=fldUniqueId%></td>
						<td><%=fldDescription%></td>	
						<td><%=fldDataType%></td>	
						<td align="center"><%=deleteHref%></td>
												
			    	 	</tr>

					<%
			}//for loop  		
					%>
	</table>	
	<!-- Bug#9751 16-May-2012 Ankit -->		
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
		<tr valign="top"><td>
		<% if (totalRows > 0) 
		{	Object[] arguments = {firstRec,lastRec,totalRows};
		%>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
		<%}%>	
	   </td></tr>
	</table>
			
	<div align="center" class="midalign">			
	<%
		if (curPage==1) startPage=1;
	    for (int count = 1; count <= showPages;count++)
		{
		
		 
   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{  
			  %>
			
			  	<A href="fieldLibraryBrowser.jsp?selectedTab=<%=selectedTab%>&mode=final&categorySearch=<%=categorySearch%>&nameSearch=<%=nameSearch%>&keywordSearch=<%=keywordSearch%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
				
				<%
  			}	%>
		
		<%
 		 if (curPage  == cntr)
		 {	 
	    	 %>	   
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>		
		<A href="fieldLibraryBrowser.jsp?selectedTab=<%=selectedTab%>&mode=final&categorySearch=<%=categorySearch%>&nameSearch=<%=nameSearch%>&keywordSearch=<%=keywordSearch%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%}%>
		
		<%	}
		if (hasMore)
		{   %>
	   &nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="fieldLibraryBrowser.jsp?selectedTab=<%=selectedTab%>&mode=final&categorySearch=<%=categorySearch%>&nameSearch=<%=nameSearch%>&keywordSearch=<%=keywordSearch%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
       <%}%>
	</div>
</Form>

<jsp:include page="paramHide.jsp" flush="true"/>			

		<%



		} //end of if body for page right

		else

		{

		%>

			<jsp:include page="accessdenied.jsp" flush="true"/>

		<%

		} //end of else body for page right

	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>

	<div> 

    	<jsp:include page="bottompanel.jsp" flush="true"/>

  	</div>

</div>

<div class ="mainMenu" id = "emenu"> 

  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>

</body>

</html>


