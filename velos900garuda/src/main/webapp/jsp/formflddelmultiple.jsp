<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<title><%=LC.L_Multi_Del%><%--Multiple Delete*****--%></title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="include.jsp" flush="true"/>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>


<SCRIPT LANGUAGE="JavaScript">

	if ((document.layers) || (navigator.appName == 'Netscape') || (navigator.appName == 'Opera')) {  
			if (screen.width=='800'){
				document.write("<Link Rel=STYLESHEET HREF='commonNS86.css' type=text/css>");
			}
			else if (screen.width=='1024'){
			
				document.write("<Link Rel=STYLESHEET HREF='commonNS.css' type=text/css>");
			}
			else {
				document.write("<Link Rel=STYLESHEET HREF='commonNSgt1024.css' type=text/css>");
			}
		}
			if (document.all) { 
				var agt=navigator.userAgent.toLowerCase()
				if (screen.width=='800'){
				if (agt.indexOf("mac")!=-1) {
					document.write("<Link Rel=STYLESHEET HREF='commonmac86.css' type=text/css>");	
				}else {
					document.write("<Link Rel=STYLESHEET HREF='common86.css' type=text/css>");
					  }
				}
			else if (screen.width=='1024'){
			
				if (agt.indexOf("mac")!=-1) {
					document.write("<Link Rel=STYLESHEET HREF='commonmac.css' type=text/css>");	
				}else {
					document.write("<Link Rel=STYLESHEET HREF='common.css' type=text/css>");
					}
				}else {
					document.write("<Link Rel=STYLESHEET HREF='commongt1024.css' type=text/css>");
					}
			}

	function fnclose(formobj){
		window.close();
		return false;
	}


	



	function setVals(formobj){
		var chkvalue="0"
		if(formobj.eSign.value== "")
		{
			alert("<%=MC.M_PlsEnterEsign%>");/*alert("Please enter e-Signature");*****/
			formobj.eSign.focus();
			return false;
		}
		else{ 
			var flag1=0;
			if(parseInt(formobj.hidValue.value)>0){

			if(formobj.chkformFldId.length > 1){
			   for(i=0;i<formobj.chkformFldId.length;i++){
				   
					if(formobj.chkformFldId[i].checked==true){
						flag1=1;	
					}	
               }
			}
			else{
				if(formobj.chkformFldId.checked==true){
						flag1=1;	
				}	
			}
		
			if(flag1==0){
				alert("<%=MC.M_SelFlds_ToDel%>");/*alert("Please select Field(s) to be deleted");*****/
				return false;
            }  
			}
			else{
				chkvalue="1"
			}

			

        }
		
		
		
		 if(chkvalue=="1"){
			window.close();
			
		 } 	 
		 else if(formobj.chkformFldId.length==1 && flag1==0){
			window.close();
		 }
		  			 
		
		 if (!(validate_col('e-Signature',formobj.eSign))) return false
		
		 if(isNaN(formobj.eSign.value) == true) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		   }

		 formobj.action = "savedelmultiple.jsp"; 
		 return true;
	
	}

</SCRIPT>


<%@ page language = "java"  import = "com.velos.eres.business.common.*,com.velos.eres.business.fieldAction.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.UserSiteDao,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.FormFieldDao,com.velos.eres.service.util.*"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formSecJB" scope="request" class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formfieldB" scope="request" class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>

	<%
	int ienet = 2;
	String agent1 = request.getHeader("USER-AGENT");
	if (agent1 != null && agent1.indexOf("MSIE") != -1) 
		ienet = 0; //IE
	else
		ienet = 1;

	if (ienet == 0) {
	%>
	<body style="overflow:scroll" >
	<%} else {%>
	<body>
	<%}%>

	<DIV class="popDefault" > 

	<%

		String formFldId= "";
		String secId= "";
		int pageRight = 0;	


		HttpSession tSession = request.getSession(true);
		if (sessionmaint.isValidSession(tSession))	
		{
	 %>



	<%


		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
		int formId;
		formId= EJBUtil.stringToNum(request.getParameter("formId"));


	%>

	<% 	
			
		
		
		if (pageRight >= 4)
			{
		    String headingBasedOnFrom = null;
		    if ("A".equals(request.getParameter("calledFrom"))) {
		        headingBasedOnFrom = MC.M_MngAcc_FormMgmt;/*headingBasedOnFrom = "Manage Account >> Form Management >> ";*****/
		    } else if ("St".equals(request.getParameter("calledFrom"))) {
		        headingBasedOnFrom = LC.L_StdSetup_Frm;/*headingBasedOnFrom = LC.Std_Study+" Setup >> Form >> ";*****/
		    } else {
		        headingBasedOnFrom = LC.L_Lib_FrmLib;/*headingBasedOnFrom = "Library >> Form Library >> ";*****/
		    } Object[] arguments = {headingBasedOnFrom};
			%>
				<br>
			<P class="sectionHeadings"> <%=VelosResourceBundle.getMessageString("M_AddFlds_DelMultiple",arguments) %><%-- <%=headingBasedOnFrom%>Add Fields >> Delete Multiple*****--%> </P>
			<%
			 

			 ArrayList secNames = null;
			 ArrayList secIds = null;
			 ArrayList fieldNames = null;
			 ArrayList fieldUniIds = null;
			 ArrayList repeatFields = null;
			 ArrayList formFldIds = null;
			 ArrayList formFldNums = null;
			 ArrayList formFldSeqs = null;
				

			 ArrayList secOfflines	= null;
			 ArrayList fldOfflines	= null;
			 
			
				
			//ArrayList arFldActionIds = null;
			//Integer fldActionId ;
			int fldActionId ;
								
			 String secName = "";
			 String fieldName = "";
			 String fieldUniId = "";
			 String fieldType = "";
			 String repeatField = "";
			 String tempFldName = ""; 
			 String fieldDataType = "";
			 int fldOffline = 0;
			 int formFldNum = 0;
		
			
	%>

<Form  name="formflddelmultiple" id="frmflddelmult" action="" onsubmit="if (setVals(document.formflddelmultiple)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	
	<input type="hidden" name="formFldId" value="<%=formFldId%>">

	<input type="hidden" name="formId" value="<%=formId%>">
	<input type="hidden" name="calledFrom" value="<%=request.getParameter("calledFrom")%>">
	<table class=tableDefault width="90%" cellspacing="2" cellpadding="0" border="0" >
		<% 
		
		FormLibDao formLibDao = new FormLibDao();
		formLibDao = formlibB.getSectionFieldInfo(formId);
		
		
		secNames = formLibDao.getSecNames(); 	
		secIds = formLibDao.getSecIds();	
		fieldNames = formLibDao.getFieldNames();
		fieldUniIds = formLibDao.getFieldUniIds();	
		repeatFields = formLibDao.getRepeatFields();		 
		formFldIds = formLibDao.getFormFldIds();		
		formFldNums = formLibDao.getFormFldNums();
		formFldSeqs = formLibDao.getFormFldSeqs();	

		secOfflines = formLibDao.getArrFormSecOfflines();
		fldOfflines = formLibDao.getArrFormFldOfflines();

		//arFldActionIds =formLibDao.getFieldActions();
		

		
		
		int len= secIds.size(); 

		int fmtcount=1;
		int repcount=1;
		String fldDisplayHref="";
		int hidvalue=0;
		
		
		for(int count=0; count<len ;count++)
		{							
			
			
			String tempSecName="";
			String tempRepeatField="";
			String tempSecId = "";


//				 fldActionId = ((Integer)arFldActionIds.get(count)).intValue(); 
					

				 tempSecName=(secNames.get(count)).toString();			
				 tempRepeatField=(repeatFields.get(count)).toString();
				 tempSecId = secIds.get(count).toString();
				 formFldNum = (   (Integer)formFldNums.get(count)  ).intValue(); 
				 fieldUniId = ((fieldUniIds.get(count)) == null)?"-":(fieldUniIds.get(count)).toString();	
				 
				 fldOffline = ((Integer)fldOfflines.get(count)).intValue();

					if ( count ==0){ 
						  secName=tempSecName;
						  repeatField=tempRepeatField;
						}else if ( ! tempSecId.equals((secIds.get(count-1)).toString())){
							 secName=tempSecName;

							 repeatField=tempRepeatField;
								fmtcount=1;
								repcount=1;						
								}else{
								secName="";

								repeatField=" ";
								fmtcount=0;
								repcount=0;

							}
						if (fieldUniId=="-"){
							fieldName="";
							}
						if((fieldNames.get(count)) != null){
							fieldName = fieldNames.get(count).toString();
							
							}
							formFldId = formFldIds.get(count).toString();
							
				/*		if (fieldUniId.trim().equals( "er_def_date_01" )){
								
								fieldName="";	
								
							}
				*/

						if(fmtcount==1){
				%>
						<tr height="20">
							<td><P class="defComments"><%=secName%></P></td>					
							<td>&nbsp;</td>
							<%if(pageRight > 4 ) {%>
							<td>&nbsp;</td>
				<%
						}
					}
				%>
						</tr>					
				<% 
					if ((count%2)==0) 
				{    if(!fieldName.equals("") || formFldNum==0 || fldOffline==1 ||fieldName==""){  %>
						<tr class="browserEvenRow"> 
					<% }   
						
					}else{ 
						if(!fieldName.equals("")|| formFldNum==0 || fldOffline==1 || fieldName==""){   %>
						<tr class="browserOddRow"> 
						<%}  
						
					}%>

						
						
						<%	if (formFldNum==0) {%>
						<input type="hidden" name="formFldNum" value="<%=formFldNum%>">
						<td ><B><%=MC.M_NoFld_InSec%><%--No fields in the section*****--%></B></td>

						<%}else if  (fldOffline==1 ){  %>
						<td ><%=fieldName%>&nbsp;&nbsp;&nbsp;[<%=MC.M_CntDelFrm_WithOfflineStat%><%--Cannot delete this field. Form is in 'Offline for Editing' status.*****--%>]</td>

						<%}else {%>
						<td ><%=fieldName%></td>

						<td >
						<% if (formFldNum==0  || fieldName=="" || fldOffline==1 || fieldUniId.trim().equals( "er_def_date_01" )){%>
						<input type="hidden" name="chkformFldId1" value="<%=formFldId%>" >
						
						<%}else{%>
						<input type="checkbox" name="chkformFldId" value="<%=formFldId%>" onclick="">
					<%	hidvalue=hidvalue+1;
							}%>
					</td>
				</tr>
				<% } 
				}//end of for loop with count	
				%>
				<input type="hidden" name="hidValue" value="<%=hidvalue%>">
			</table>	
			
		<!-- Commented by Manimaran to fix the Bug:2637 -->
		<!--Added by Gopu to fix the bugzilla issue #2593 llows to delete Field(s) linked with 'Inter field action' from Delete Multiple page. -->
		<!--% if(fieldUniId.trim().equals( "er_def_date_01" )) { % -->
			
		<!--table align="left">
			<tr height=20>				
				<td>
   					<P class="sectionHeadings" >This section can't be deleted as it has the mandatory field 'Data entry Date', delete the fields individually if neccessary </P>
				</td>
			</tr>
			<tr height=20>
				<td align=center>
				<A href=# onclick="window.close();"><img src="../images/jpg/Back.gif" align="absmiddle"  border=0> </A>
				</td>
			</tr>
		</table-->
		<!--%} else {%-->
		<table align="left" width="90%" cellspacing="2" cellpadding="0" border="0" >
			<tr align=baseline>
				<td>
					<jsp:include page="submitBar.jsp" flush="true"> 
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="frmflddelmult"/>
						<jsp:param name="showDiscard" value="Y"/>
						<jsp:param name="noBR" value="Y"/>
					</jsp:include>
				</td>
			</tr>
		</table>		
		<!--%}%-->	
		</Form>
	<%}// end of page right
		else{
	%>
		 <jsp:include page="accessdenied.jsp" flush="true"/>
	<%}
 }//end of if body for session
	  else{
	%>
		 <jsp:include page="timeout.html" flush="true"/> 
	<%
	}

	%>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
