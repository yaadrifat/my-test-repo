<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Blk_DeactUser%><%--Blocked/Deactivated Users*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>




<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

 
</head>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<body>
<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<BR>
<DIV class="browserDefault" id = "div1"> 
  <%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{

	String uName = (String) tSession.getValue("userName");



	int pageRight = 0;

	String accId = (String) tSession.getValue("accountId");




	//out.println("accId"+accId);
	UserDao userDao = userB.getActivatedDeactivatedUsers(Integer.parseInt(accId),"D");



	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));	

	

   if (pageRight > 0 )

	 {

       ArrayList usrLastNames;

       ArrayList usrFirstNames;

       ArrayList usrMidNames;

       ArrayList siteNames;

       ArrayList jobTypes;

       ArrayList usrIds;	

       ArrayList grpNames;
       ArrayList usrGrpIds;



	   String usrLastName = null;

           String grpName = null;

	   String usrFirstName = null;

	   String usrMidName = null;

	   String siteName = null;

	   String jobType = null;

	   String usrId = null;	
	   String usrGrpId = null;



	   String oldGrp = null;	





   int counter = 0;



%>

  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=MC.M_MngAcc_BlkOrDeactUsr%><%--Manage Account >> Blocked/Deactivated Users*****--%></P>
  <Form name="accountBrowser" method="post" action="" onsubmit="">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr > 
        <td width = "65%"> <P class = "defComments"><%=MC.M_ListUsr_AccBlocked%><%--The list below displays the users whose accounts have been blocked/deactivated*****--%>:</P> </td>
      </tr>
    </table>
    <table width="100%" border=0>
      <tr> 
        <th width=25%> <%=LC.L_Organization%><%--Organization*****--%> </th>
        <th width=25%> <%=LC.L_User_Name%><%--User Name*****--%> </th>
        <th width=25%> <%=LC.L_Job_Type%><%--Job Type*****--%> </th>
		<th width=25%></th>
      </tr>
      <%

		



		usrLastNames = userDao.getUsrLastNames();

		usrFirstNames = userDao.getUsrFirstNames();

		usrMidNames = userDao.getUsrMidNames();

		siteNames = userDao.getUsrSiteNames();

		jobTypes = userDao.getUsrJobTypes();

		usrIds = userDao.getUsrIds();

		grpNames = userDao.getUsrDefGrps();

		usrGrpIds = userDao.getUsrGrpIds();

		int i;

		int lenUsers = usrLastNames.size();

	%>
      <%

		for(i = 0 ; i < lenUsers ; i++)

		  {



		usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();

		usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();

		usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();

		siteName=((siteNames.get(i))==null)?"-":(siteNames.get(i)).toString();

		jobType=((jobTypes.get(i))==null)?"-":(jobTypes.get(i)).toString();

		
		//usrGrpId = (usrGrpIds.get(i)).toString();
		usrId = ((Integer)usrIds.get(i)).toString();

		//grpName = (String) grpNames.get(i);



		%>
      <%// if ( ! grpName.equals(oldGrp)){ %>
      <!-- <tr id ="browserBoldRow"> 
        <td width = 150>Group: <%= grpName%> </td>
      </tr> -->
      <%//}%>
      <%		



		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td width = '30%'> <%= siteName%> </td>
        <td width ='30%'> <A href = "userdetails.jsp?mode=M&srcmenu=<%=src%>&userId=<%=usrId%>"><%= usrLastName%>, 
          &nbsp; <%= usrFirstName%></A> </td>
        <td width ='25%'> <%= jobType%> </td>

        <td width ='15%'> 
		<A href = "activateuser.jsp?srcmenu=<%=src%>&userId=<%=usrId%>&delMode=null">
		<%=LC.L_Reactivate_User%><%--Reactivate User*****--%>
		</A>
	</td>

      </tr>
      <%

		//oldGrp = grpName;

		}

	

%>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<BR>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>
