<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_Fld_DelFromLib%><%-- Field Delete From Library*****--%> </title>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id="fieldLibJB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	
	String selectedTab="";
	String mode= "";
	String fieldLibId="";
	String fldRecordType="";
	int ret=0;
	HttpSession tSession = request.getSession(true); 
 	if (sessionmaint.isValidSession(tSession))
	{ 	
		
		mode= request.getParameter("mode");
		selectedTab= request.getParameter("selectedTab");
		fieldLibId=  request.getParameter("fieldLibId");	
		//out.println("fieldLibIdfieldLibIdfieldLibId"+fieldLibId);
			
%>
		
		<FORM name="deleteField" id="delFieldFrm" method="post" action="fieldLibDelete.jsp" onSubmit="if (validate(document.deleteField)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<br><br>
		<%  if ( mode.equals("initial") )
			{ %>     
				<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%-- Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
		
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="copyverfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
		
				<Input type="hidden" name="mode" value="final" >
				<Input type="hidden" name="fieldLibId" value="<%=fieldLibId%>" >
				<Input type="hidden" name="srcmenu" value="<%=src%>" >
				<Input type="hidden" name="selectedTab" value="<%=selectedTab%>" >
		</FORM>
		<%  
			}
		else
		{
				String eSign = request.getParameter("eSign");	
				String oldESign = (String) tSession.getValue("eSign");
				if(!oldESign.equals(eSign)) 
				{ 
		%>
	 			  <jsp:include page="incorrectesign.jsp" flush="true"/>	
		<%
				} 
				else 
				{
					
					fieldLibId=  request.getParameter("fieldLibId");	
					fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
					fieldLibJB.getFieldLibDetails();				
					fieldLibJB.setRecordType("D");
					ret =fieldLibJB.updateFieldLib();
		
					if (ret==-2) 
					{%>	

						<br><br><br><br><br><br>
	
						<table width=100%>
						<tr>
						<td align=center><p class = "successfulmsg"><%=MC.M_ErrorCnt_Del%><%--Error cannot be deleted.*****--%></p>
					<%} else 
					{ %>
						<br><br><br><br><br><br>
						<P class="successfulmsg" align="center"><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </P>
												
						<META HTTP-EQUIV=Refresh CONTENT="1; URL=fieldLibraryBrowser.jsp?srcmenu=<%=src%>&mode=initial&selectedTab=<%=selectedTab%>" >
					<%}%>
					</p>
			 			</td>
						</tr>
						<tr height=20></tr>
						<tr>
						<td align=center>
						</td>		
						</tr>		
						</table>				
						<br><br><br><br><br> <p class = "successfulmsg" align = center>  </p>
				<% 
				} %>
		<%
		}  %>
 
			
		 				
   <% 	 
	}//end esign
		
	else
	{ %> 
	 	<jsp:include page="timeout.html" flush="true"/> 
 <% } %>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</DIV>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>
</body>
</HTML>


