<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<!-- Creation date: 3/14/2002 -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Manage_Pcols %><%-- Manage Protocols*****--%></title>

<% String src;
   src= "tdMenuBarItem3";
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<script language="Javascript" src="multisearchJS.js">
</script>

<script>
	function setAction(formObj){
		formObj.action="studysearchbrowser.jsp";
		formObj.submit();
	}
</script>

<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.*"%>
<br>
<div class="browserDefault" id="div1">
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
  	ienet = 0; //IE
else
  	ienet = 1;


HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

	if (pageRight > 0 )	{
%>


<Form name="studysearchbrowser" method=post onsubmit="" action="">
<table width=100%>
<tr>
	<td width=50%>
	<P class="sectionHeadings"> <%=LC.L_MngPcol_Open %><%-- Manage Protocols >> Open*****--%> </P>
	</td>

	<td width=25% align="right"> <P class="defComments"><%=LC.L_Search_AStd %><%--Search a <%=LC.Std_Study%>*****--%></P></td>
	<td class=tdDefault width=30%> <Input type=text name="searchCriteria" size="20%"> </td>
    <td width=5% align="left">
    	<button type="submit" onClick="setAction(document.studysearchbrowser)"><%=LC.L_Search%></button>
	</td>
</tr>
<tr>
		<td></td>
		<td></td>
		<td>
		<%
		if (ienet==0) {%>
			<A href="studysearchbrowser.jsp"><P class="defComments"><%=LC.L_View_AllStudies %><%-- View All <%=LC.Std_Studies%>*****--%></P></A>
		<%} else {%>
			<A href="userStudies.jsp?srcmenu=tdMenuBarItem3"><P class="defComments"><%=LC.L_View_AllStudies %><%-- View All <%=LC.Std_Studies%>*****--%></P></A>
		<%}%>
		</td>
</tr>
</table>

</Form>


<jsp:include page="studysearchresults.jsp" flush="true">
<jsp:param name="resultSet" value="1"/>
</jsp:include>

<jsp:include page="studysearchviewresults.jsp" flush="true">
<jsp:param name="resultSet" value="2"/>
</jsp:include>


<%	//wait for xml to load
//	Thread t = new Thread();
//	t.sleep(100);
%>
<script>
<!-- call the function of multisearchJS with the parameter of resultSet. Depending on the
<!-- resultSet all the variables will be stored in the appropriate position in the array-->
	for (q=1;q<3;q++){
		javascript:mainFunc(q);
	}
</script>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</DIV>

</body>
</html>
