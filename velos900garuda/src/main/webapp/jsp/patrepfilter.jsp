<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<head>
<title><%=LC.L_Pat_Rpts%><%--<%=LC.Pat_Patient%> Reports*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page import="com.velos.eres.service.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="Javascript">

function fOpenWindow(type,section,frmobj){
	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=500;
		height=120;
	}
	if (type == "M"){
		width=270;
		height=70;
	}
	frmobj.year.value ="";
	frmobj.month.value ="";
	frmobj.dateFrom.value ="";
	frmobj.dateTo.value ="";

	frmobj.range.value ="";
	if (type == 'A'){
			frmobj.range.value = "All"
	}

	 if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}
}

function fValidate(frmobj){
	reportChecked=false;
	for(i=0;i<frmobj.reportName.length;i++){
		sel = frmobj.reportName[i].checked;
		if (frmobj.reportName[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
		return false;
	}
	frmobj.action = "repRetrieve.jsp";
	reportNo = frmobj.repId.value;
	switch (reportNo) {

		case "10": //Patient Schedule

		case "93": //Patient Timeline

		case "11": //Patient Events (Done)

		case "12": //Patient Events To Be Done

		case "65": //Patient Event/CRF Status

		case "66": //Patient CRF Tracking


		case "88": //Adverse Events by Patient
				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}

				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
	   			if (fnTrimSpaces(frmobj.studyPk.value) == "") {

	   				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
	   				return false;

				}

	   			if (fnTrimSpaces(frmobj.id.value) == "") {
	   				alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/
	   				return false;
				}

	   			break;

		case "13": //All Patients Event List

		case "16": //All Patient Enrolled to a study
			dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}

			if (fnTrimSpaces(frmobj.selSiteIds.value) == "") {
				alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization");*****/
				return false;
			}

			if (fnTrimSpaces(frmobj.studyPk.value) == "") {
				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
				return false;
			}
			break;

		case "94": //Study Visit Calendar
		dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}
			if(frmobj.filterType[0].checked == true)
			{
				alert("<%=MC.M_DtFilter_NotAval%>");/*alert("The Date Filter 'All' is not available for this report. Please select an appropriate Date Range or select a Month/Year");*****/
				return false;
			}
			if (fnTrimSpaces(frmobj.studyPk.value) == "") {
				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
				return false;
			}
			if (fnTrimSpaces(frmobj.selSiteIds.value) == "") {
				alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization");*****/
				return false;
			}
			break;
		case "95": //Patient Visit Calendar
		dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}
			if(frmobj.filterType[0].checked == true)
			{
				alert("<%=MC.M_DtFilter_NotAval%>");/*alert("The Date Filter 'All' is not available for this report. Please select an appropriate Date Range or select a Month/Year");*****/
				return false;
			}
			if (fnTrimSpaces(frmobj.studyPk.value) == "") {
				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
				return false;
			}
	   			if (fnTrimSpaces(frmobj.id.value) == "") {
	   				alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/
	   				return false;
				}
			break;
		case "96": //PSA / Treatment

		dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}
			if (fnTrimSpaces(frmobj.studyPk.value) == "") {
				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
				return false;
			}
	   			if (fnTrimSpaces(frmobj.id.value) == "") {
	   				alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/
	   				return false;
				}
			break;
		case "89": //All Patients Associated Cost(By Cost Type)
		case "90": //All Patients Associated Cost(Bill)

		case "45": //All Patients Associated Cost
			dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}

			if (fnTrimSpaces(frmobj.selSiteIds.value) == "") {
				alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization");*****/
				return false;
			}

   			if (fnTrimSpaces(frmobj.studyPk.value) == "") {
   				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
	   				return false;
			}

			if (fnTrimSpaces(frmobj.protId.value) == "") {
				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/
				return false;
			}

			break;
		case "47": //Patient Associated Cost
			dateChecked =false;

			if (frmobj.range.value != ""){
   				dateChecked=true;
			}
			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}

			if (fnTrimSpaces(frmobj.studyPk.value) == "") {
				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
				return false;
			}
			if (fnTrimSpaces(frmobj.protId.value) == "") {
				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/
				return false;
			}

   			if (fnTrimSpaces(frmobj.id.value) == "") {
   				alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/
   				return false;
			}
			break;
	case "105": //Study CRF Tracking
			dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}
				if (fnTrimSpaces(frmobj.studyPk.value) == "") {
					alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
				return false;
			}
			if (fnTrimSpaces(frmobj.protId.value) == "") {
				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/
				return false;
			}
			if (fnTrimSpaces(frmobj.selSiteIds.value) == "") {
				alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization");*****/
				return false;
			}
			break;
	}

	//SV, 8/25/04, already submitted frmobj.submit();

}



function fSetIdNew(ddtype,frmobj){
	if (ddtype == "patient") {
		frmobj.id.value = "";
		frmobj.selPatient.value = "None";
		frmobj.val.value = "";
		openwindow();
	}

	if (ddtype == "study1") {
		frmobj.selProtocol.value = "None";
		frmobj.protId.value = "";
		i=frmobj.sId1.options.selectedIndex;
		frmobj.studyPk.value = frmobj.sId1.options[i].value;
	}

	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		for(i=0;i<frmobj.reportName.length;i++)	{
			if (frmobj.reportName[i].checked){
				lsReport = frmobj.reportName[i].value;
				ind = lsReport.indexOf("%");
				frmobj.repId.value = lsReport.substring(0,ind);
				frmobj.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}
}

</SCRIPT>



<SCRIPT language="JavaScript1.1">

function openwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	windowName.focus();
}


function openProtWindow(param,frmobj) {
	lstudyPk = frmobj.studyPk.value;

	if (fnTrimSpaces(lstudyPk) == "") {
			alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
			return false;
	}

	windowName=window.open("protocolPopup.jsp?studyPk="+lstudyPk+"&reportType="+param,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=200");
	windowName.focus();
}

function openOrgWindow() {
    windowName=window.open("selectsites.jsp","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=300");
	windowName.focus();
}


</SCRIPT>



</head>



<% String src="";

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>





<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrlD" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>


<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.user.UserJB"%>



<body>

<br>



<DIV class="browserDefault" id="div1">



<%


     String report_id="",report_name="",report_type="",repid_name="" ;
     StringBuffer rep_first = new StringBuffer();

   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))

   {
    	String userId = (String) tSession.getValue("userId");
    	UserJB user = (UserJB) tSession.getValue("currentUser");
     	String siteId = user.getUserSiteId();
     	String siteName = "";
     	siteB.setSiteId(EJBUtil.stringToNum(siteId));
     	siteB.getSiteDetails();
     	siteName = siteB.getSiteName();

	    if ((request.getParameter("pat_rep").length())>0)  {
	        	repid_name=request.getParameter("pat_rep");
	      }

	     report_id =repid_name.substring(0,(repid_name.indexOf("%")));
	     report_name=repid_name.substring((repid_name.indexOf("%")+1),repid_name.lastIndexOf("%"));
	     report_name=report_name.replace('~',' ');
	     report_type=repid_name.substring((repid_name.lastIndexOf("%") + 1));


	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));

	String tab = request.getParameter("selectedTab");

	String modRight = (String) tSession.getValue("modRight");



	int modlen = modRight.length();



	ctrlD.getControlValues("module");

	int ctrlrows = ctrlD.getCRows();



	ArrayList feature =  ctrlD.getCValue();

	ArrayList ftrDesc = ctrlD.getCDesc();

	ArrayList ftrSeq = ctrlD.getCSeq();

	ArrayList ftrRight = new ArrayList();

	String strR;



	ArrayList ctrlValue = new ArrayList();



	for (int counter = 0; counter <= (modlen - 1);counter ++)

	{

		strR = String.valueOf(modRight.charAt(counter));

		ftrRight.add(strR);

	}



	grpRights.setGrSeq(ftrSeq);

    grpRights.setFtrRights(ftrRight);

	grpRights.setGrValue(feature);

	grpRights.setGrDesc(ftrDesc);





	int eptRight = 0;

	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));



	Calendar cal = Calendar.getInstance();

	int currYear = cal.get(cal.YEAR);



	String filterText="";



	int counter=0;

	int studyId=0;

	StringBuffer study1=new StringBuffer();

	StudyDao studyDao = new StudyDao();

	studyDao.getReportStudyValuesForUsers(userId);

	study1.append("<SELECT NAME=sId1 onChange=fSetIdNew('study1',document.reports)>") ;

	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++)

		{

		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();

		study1.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+"</OPTION>");

		}



	study1.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

	study1.append("</SELECT>");
	CtrlDao ctrl = new CtrlDao();

	ctrl.getControlValues("allpat");

	String catId = (String) ctrl.getCValue().get(0);



	repdao.getAllRep(EJBUtil.stringToNum(catId),acc); //All Patient Reports

	ArrayList repIdsAllPatients= repdao.getPkReport();

	ArrayList namesAllPatients= repdao.getRepName();

   	ArrayList descAllPatients= repdao.getRepDesc();

	ArrayList repFilterAllPatients = repdao.getRepFilters();


	CtrlDao ctrl1 = new CtrlDao();

	ctrl1.getControlValues("patspecific");

	catId = (String) ctrl1.getCValue().get(0);

	repdao1.getAllRep(EJBUtil.stringToNum(catId),acc); //Patient Specific Reports

	ArrayList repIdsOnePatients= repdao1.getPkReport();

	ArrayList namesOnePatients= repdao1.getRepName();

   	ArrayList descOnePatients= repdao1.getRepDesc();

	ArrayList repFilteronePatients = repdao1.getRepFilters();


	int repIdAllPatient=0;

	String nameAllPatient="";

	int repIdOnePatient=0;

	String nameOnePatient="";



	int lenAllPatient = repIdsAllPatients.size() ;

	int lenOnePatient = repIdsOnePatients.size() ;

%>

	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>


<Form Name="reports" method="post" action="repRetrieve.jsp" target="_blank" onSubmit="return fValidate(document.reports)" >

	<input type="hidden" name="selSiteIds" value="<%=siteId%>">

	<input type="hidden" name="srcmenu" value='<%=src%>'>

	<input type="hidden" name="selectedTab" value='<%=tab%>'>



<input type=hidden name=year>

<input type=hidden name=month>

<input type=hidden name=year1>

<input type=hidden name=dateFrom>

<input type=hidden name=dateTo>



         <Table width=100%>
         <tr>
         	 <td width="10%">
         	<A href="repmain.jsp?selectedTab=<%=tab%>&srcmenu=<%=src%>" type="submit"><%=LC.L_Back%></A>
         	</td>
         	 <td>
         	<p class = "sectionHeadings"><%=MC.M_Rpt_PatRpt%><%--Report >> <%=LC.Pat_Patient%> Reports*****--%></p>
         	</td>
         	</tr>
		</table>

 <table width="100%">
<tr>
	<th width="35%"><Font class="reportText"><%=LC.L_Report%><%--Report*****--%></font></th>
	<th width="25%"><Font class="reportText"><%=LC.L_Required_Filters%><%--Required Filters*****--%></font></th>
	<th width="40%"><Font class="reportText"><%=LC.L_Available_Filters%><%--Available Filters*****--%></font></th>
 </tr>
</table>
<table width="100%">
	<tr>
	<td width="60%">

		<table>

         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=LC.L_All_PatsRpts%><%--All <%=LC.Pat_Patients%> Reports*****--%></p>
         	</td>
         	</tr>
<%
 		for(counter = 0;counter<lenAllPatient;counter++)
		{
			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow">
        	<%
			}else{ %>
      			<tr class="browserOddRow">
        	<%}%>
         	<td width="35%">
         	<%
			repIdAllPatient = ((Integer)repIdsAllPatients.get(counter)).intValue();
			nameAllPatient=((namesAllPatients.get(counter)) == null)?"-":(namesAllPatients.get(counter)).toString();

				if (report_id.equals((repIdsAllPatients.get(counter)).toString())) {
         	%>
				<Input Type="radio" name="reportName" value="<%=repIdAllPatient%>%<%=nameAllPatient%>" onClick=fSetIdNew('report',document.reports) checked> <%=nameAllPatient%>
			<%} else {%>
				<Input Type="radio" name="reportName" value="<%=repIdAllPatient%>%<%=nameAllPatient%>" onClick=fSetIdNew('report',document.reports)> <%=nameAllPatient%>
         	<%}%>
			</td>
			<td width="25%"><%=repFilterAllPatients.get(counter)%></td>
		</tr>
        <%
          }%>

         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=LC.L_Pat_SpecificRpts%><%--<%=LC.Pat_Patient%> Specific Reports*****--%></p>
         	</td>
         	</tr>

         	<% for(counter = 0;counter<lenOnePatient;counter++)
         	{
			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow">
        	<%
			}else{ %>
      			<tr class="browserOddRow">
        	<%
		}
         	%><td width="35%">
         	<%
			repIdOnePatient = ((Integer)repIdsOnePatients.get(counter)).intValue();
			nameOnePatient=((namesOnePatients.get(counter)) == null)?"-":(namesOnePatients.get(counter)).toString();
//		if(eptRight==0){
//			if ((repIdOnePatient!=65) && (repIdOnePatient!=66)) {
			if (report_id.equals((repIdsOnePatients.get(counter)).toString())) {%>
			<Input Type="radio" name="reportName"  value="<%=repIdOnePatient%>%<%=nameOnePatient%>" onClick=fSetIdNew('report',document.reports) checked> <%=nameOnePatient%>
			<%} else { %>
			<Input Type="radio" name="reportName"  value="<%=repIdOnePatient%>%<%=nameOnePatient%>" onClick=fSetIdNew('report',document.reports)> <%=nameOnePatient%>
			<%}%>
			</td>
			<td width="25%"><%=repFilteronePatients.get(counter)%></td>
		</tr>
         	<%}%>


         </table>
</td>

 <td width="40%" valign="top">
	<table><tr>
         	<td colspan="2"><br><br><Font class=reportText><%=LC.L_Date_Filter%><%--Date Filter*****--%>:</Font></td></tr>
			<tr>
			<td colspan="2">
         	<Input type="radio" name="filterType" checked value="1" onClick="fOpenWindow('A','1', document.reports)"> <%=LC.L_All%><%--All*****--%>
         	<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1', document.reports)"> <%=LC.L_Year%><%--Year*****--%>
         	<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1', document.reports)"> <%=LC.L_Month%><%--Month*****--%>
         	<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1', document.reports)"> <%=LC.L_Date_Range%><%--Date Range*****--%> <br>
         	<input type=text name=range size=35 READONLY value="All">
         	</td>
			</tr>
			 <tr>
	<td colspan="2"><br><br><Font class=reportText><%=LC.L_Addl_Filters%><%--Additional Filters*****--%>:</Font><BR></td>
</tr>

		<tr>
	       	<td colspan="2"><FONT class = "comments"><A HREF=# onClick="openOrgWindow()"><%=LC.L_Select_Org%><%--Select Organization*****--%>:</A> </FONT> </td>
		</tr>
		<tr>
	       	<td colspan="2"><input type="text"  name="selSiteNames" size=35 value="<%=siteName%>" READONLY ></td>
		</tr>

		<tr>
	       	<td><Font class=comments><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%>:</Font></td>
	       	<td> <%=study1%></td>
		</tr>

		<tr>
	       	<td><FONT class = "comments"><A HREF=# onClick="openProtWindow('all',document.reports)"><%=LC.L_Select_PcolCal%><%--Select Protocol Calendar*****--%>:</A> </FONT> </td>
	       	<td> <Input type=text name=selProtocol size=20 READONLY value=None></td>
		</tr>

		<tr>
	       	<td><FONT class = "comments"><A href="#" onclick="fSetIdNew('patient',document.reports)"><%=LC.L_Select_APat%><%--Select a <%=LC.Pat_Patient%>*****--%>:</A> </FONT> </td>
	       	<td> <Input type=text name=selPatient size=20 READONLY value=None></td>
		</tr>


		<tr>
	       	<td><Font class=comments><%=LC.L_Select_DispStyle%><%--Select Display Style*****--%>:</Font></td>
	       	<td>
					<SELECT name=dispStyle>
					<OPTION value = 'P'><%=LC.L_Disp_ByPat%><%--Display By <%=LC.Pat_Patient%>*****--%> </OPTION>
					<OPTION value = 'D'><%=MC.M_Disp_DateRange%><%--Display By Date Range*****--%> </OPTION>
					</SELECT>
	       	</td>
		</tr>

		<tr>
			<td colspan="2" align="center">
			<br><br>
			<!-- <A onClick = "return fValidate(document.reports)" href="#"><input type="image" src="../images/jpg/displayreport.jpg" align="absmiddle" border=0></A> -->
			<input type="image" src="../images/jpg/displayreport.jpg" align="absmiddle" border="0">
			</td>
		</tr>


         </table>
</td></tr>
</table>

<Input type = hidden  name=id value="">
<Input type = hidden  name="val" >
<Input type = hidden  name=protId value="">
<Input type=hidden name="studyPk">
<Input type=hidden name="repId" value="<%=report_id%>">
<Input type=hidden name="repName" value="<%=report_name%>">
<Input type=hidden name="rangeType">
<Input Type=hidden name="reportNumber">
<Input type=hidden name="filterText" value = "<%=filterText%>">
</form>

<%} //end of if session times out

else{

%>

	<jsp:include page="timeout.html" flush="true"/>

<%

}

%>



<div>

<jsp:include page="bottompanel.jsp" flush="true"/>

</div>



</div>



<DIV class="mainMenu" id = "emenu">

  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</DIV>

</body>

</html>





