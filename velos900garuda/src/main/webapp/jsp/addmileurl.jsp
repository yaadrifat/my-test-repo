<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Mstone_MstoneUrl%><%--Milestone >> Milestone URL*****--%></title>

<%@ page import="java.util.*,com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:useBean id ="sessionmaint" scope="page" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="mileApndxB" scope="page" class="com.velos.eres.web.mileApndx.MileApndxJB"/>

<SCRIPT Language="javascript">
 function  validate(formobj){
    
     if (!(validate_col('URL',formobj.mileApndxUri))) return false
     if (!(validate_col('Description',formobj.mileApndxDesc))) return false
 //    if (!(validate_col('eSign',formobj.eSign))) return false

	
//	if(isNaN(formobj.eSign.value) == true) {
//	alert("Incorrect E-Signature. Please enter again");
//	formobj.eSign.focus();
//	return false;
//   }
}
</SCRIPT>


<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<body>
<br>
<DIV class="tabDefTopN" id="div1"> 
<%
	String mode = request.getParameter("mode");
	String mileApndxId=request.getParameter("mileApndxId");	   
	String studyId = request.getParameter("studyId");
	String selectedTab = request.getParameter("selectedTab");
	
	String mileApndxDesc= "";
	String mileApndxUri = "";

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
%>

<%
	if (mode.equals("M")) {
		mileApndxB.setMileApndxId(EJBUtil.stringToNum(mileApndxId));
		mileApndxB.getMileApndxDetails();
		mileApndxDesc = mileApndxB.getDesc();
		mileApndxUri = mileApndxB.getUri();
	}
%>
<form name="addurl" id="addmileurl" METHOD=POST action="mileurlsave.jsp" onsubmit="if (validate(document.addurl)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="mileApndxId" value=<%=mileApndxId%>>
<input type=hidden name="studyId" value=<%=studyId%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>


</DIV>
	<DIV class="tabDefBotN" id="div2">
<TABLE width="98%">
      <tr> 
        <td colspan="2" align="center"> 
          <P class = "defComments"> <%=MC.M_AddLnk_ToMstoneAppx%><%--Add Links to your Milestone Appendix*****--%> </P>
        </td>
      </tr>
    <tr><td>&nbsp;</td></tr>
      <tr> 
        <td width="20%" align="right"> <%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> &nbsp; &nbsp;</td>
        <td width="65%"> 
          <input type=text name="mileApndxUri" value='<%=mileApndxUri%>' MAXLENGTH=255 size=50>
        </td>
      </tr>
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_EtrCpltUrl_255Max%><%--Enter complete url eg. 'http://www.centerwatch.com' 
            or 'ftp://ftp.centerwatch.com' (255 char max.)*****--%> </P>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      <tr> 
        <td align="right"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT> &nbsp; &nbsp;
        </td>
        <td > 
          <input type=text name="mileApndxDesc" MAXLENGTH=100 value='<%=mileApndxDesc%>' size=40>
        </td>
      </tr>
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_NameToLnk_100CharMax%><%--Give a friendly name to your link. (100 char max.)*****--%> </P>
        </td>
      </tr>
	
	  <tr><td>&nbsp;</td></tr>
      		<tr><td>&nbsp;</td></tr>
  </table>
	
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="addmileurl"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>



