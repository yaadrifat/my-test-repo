<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Patient_Details%><%--<%=LC.Pat_Patient%>
 Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript">
<!-- begin script
var dd1, delay;
function startDate1(delay1) {
  var adate, date, amonth;
  delay = delay1;
  adate = new Date();
  date = adate.getDate();
  amonth = adate.getMonth()+1;

  if (amonth == 1) date += "<%=LC.L_January%><%--January*****--%>";
  else if (amonth == 2) date += "<%=LC.L_February%><%--February*****--%>";
  else if (amonth == 3) date += " <%=LC.L_March%><%--March*****--%>";
  else if (amonth == 4) date += " <%=LC.L_April%><%--April*****--%>";
  else if (amonth == 5) date += " <%=LC.L_May%><%--May*****--%>";
  else if (amonth == 6) date += " <%=LC.L_June%><%--June*****--%>";
  else if (amonth == 7) date += " <%=LC.L_July%><%--July*****--%>";
  else if (amonth == 8) date += " <%=LC.L_August%><%--August*****--%>";
  else if (amonth == 9) date += " <%=LC.L_September%><%--September*****--%>";
  else if (amonth == 10) date += " <%=LC.L_October%><%--October*****--%>";
  else if (amonth == 11) date += " <%=LC.L_November%><%--November*****--%>";
  else if (amonth == 12) date += " <%=LC.L_December%><%--December*****--%>";
  date += " " + adate.getFullYear();
  document.atime21.date.value = date;
  dd1 = setTimeout("startDate1(delay)",delay1);
}
//  end script -->
</SCRIPT>
</head>

<SCRIPT language="JavaScript1.1">
function openwin1() {
      windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=patient","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
;}
</SCRIPT>


<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>


<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<% String src;

src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String page1 = request.getParameter("page");
%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<SCRIPT Language="javascript">

 function  validate(formobj){

    
     if(!(validate_col('Pat ID',formobj.patID))) return false
     if (!(validate_col('dob',formobj.patdob))) return false
     if (!(validate_col('organization',formobj.patorganization))) return false
     if (!(validate_col('regdate',formobj.patregdate))) return false
     if (!(validate_col('RegBy',formobj.patregbyname))) return false     
     if (!(validate_col('pat status',formobj.patstatus))) return false
	 if (!(validate_col('Time Zone',formobj.timeZone))) return false
	
	//
	//
	 if (CompareDates(formobj.patdob.value,formobj.patregdate.value,'>'))
	 {
	  	alert ("<%=MC.M_RegDate_LessPatDob%>");/*alert ("Registration Date can not be less than <%=LC.Pat_Patient%>'s Date of Birth");*****/
		//formobj.patregdate.focus();
		return false;
	 }
	 
	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
     }

</SCRIPT>

<body>
<br>
<DIV class="formDefault"  id="div1">



<%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{
	String uName = (String) tSession.getValue("userName");
	String userIdFromSession = (String) tSession.getValue("userId");
	
//	String studyNo = (String) tSession.getValue("studyNo");
	String acc = (String) tSession.getValue("accountId");
	String studyId = null;
	String statDesc = null;
	String statid = null;
	String studyVer = null;
	
	
	
	studyId = request.getParameter("studyId");
	
	
	
	statDesc = request.getParameter("statDesc");
	statid = request.getParameter("statid");
	studyVer = request.getParameter("studyVer");

	int pageRight = 7;
	String mode = null;
	String calledFrom = "";
	String parPage = "";
	String patCode="";

    	if(page1.equals("enrollPatientsearch")){ 
	patCode=request.getParameter("patCode");
	}

 	mode = request.getParameter("mode");	
	calledFrom="";
//	out.println(page1);	
	if (mode.equals("M"))
	{
		calledFrom = request.getParameter("calledFrom");
	}	
	if (calledFrom == null || calledFrom.equals(""))
	{
		calledFrom = "M"; //i.e to edit patient details as maintenance and not from a study
	}
	
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
	Integer rt = null;	
	if (calledFrom.equals("S"))
	{
	  rt = (Integer)tSession.getValue("studyManagePat");
	  pageRight = rt.intValue();

	}
	else
	{
	    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	}

 	if ((mode.equals("M") && (pageRight >= 4)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))

	{

	int personPK = 0;
//out.println(page1);
  
  String patientID = "";
  String fname = "";
  String mname = "";
  String lname = "";
  String dob = "";
  String gender = "";
  String maritalStatus = "";
  String bloodGroup = "";
  String race = "";
  String ssn = "";
  String organization = "";
  String regDate = "";
  String regBy = "";
  String regByName = "";
  String add1 = "";
  String add2 = "";
  
  String city = "";
  String state = "";
  String zip = "";
  String country = "";
  String bphone = "";
  String hphone= "";
  String email = "";
  
  String employment = "";
  String education  = "";
  String status = "";
  String notes = "";
	
  String dGender = "";
  String dMarital = "";
  String dBloodGroup = "";
  String dRace = "";
  String dSites = "";
  String dEmp = "";
  String dEdu = "";
  String dStatus = "";
  String dTimeZone="";
  boolean completeDet = false;
  String inputType = "text";	
  String displayStar = "";	
String timeZone="";
 
 



  

  CodeDao cd1 = new CodeDao();
  CodeDao cd2 = new CodeDao();  	
  CodeDao cd3 = new CodeDao();	
  CodeDao cd4 = new CodeDao();	  
  CodeDao cd5 = new CodeDao();	
  CodeDao cd6 = new CodeDao();	
  CodeDao cd7 = new CodeDao();		   
  CodeDao cd8 = new CodeDao();	
  CodeDao cd9 = new CodeDao();	
  
  cd1.getCodeValues("gender");
  cd2.getCodeValues("marital_st");
  cd3.getCodeValues("bloodgr");
  cd4.getCodeValues("race");	
  cd5.getAccountSites(EJBUtil.stringToNum(acc)); 
  cd6.getCodeValues("employment");	
  cd7.getCodeValues("education");	
  cd8.getCodeValues("patient_status");	
  cd9.getTimeZones();
%>
<P class = "userName"> <%= uName %> </P>
<P class="sectionHeadings"> <%=LC.L_MngPats_Dgraphics%><%--Manage <%=LC.Pat_Patients%> >> Demographics*****--%></P>

<%
  if (mode.equals("M")) {

	
	   personPK = EJBUtil.stringToNum(request.getParameter("pkey"));		
	   person.setPersonPKId(personPK);
	   person.getPersonDetails();
	
	   patientID = person.getPersonPId();
	   patientID = (patientID==null)?"":patientID;
	   
	   organization =   person.getPersonLocation() ;
	   organization = (organization==null)?"":organization;
	   
	   bloodGroup =  person.getPersonBloodGr();
	   bloodGroup = (bloodGroup==null)?"":bloodGroup;
	   
	   dob =  person.getPersonDob();
	   dob = (dob==null)?"":dob;
	  
	  
	   education =   person.getPersonEducation();
	   education = (education==null)?"":education;
	 
	 
 	   employment  =  person.getPersonEmployment();
	   employment = (employment==null)?"":employment;
	
	
	   fname  =  person.getPersonFname();
	   fname = (fname==null)?"":fname;
	
	
 	gender  =  person.getPersonGender();
	 gender = (gender==null)?"":gender;
	
	
	
	 lname =  person.getPersonLname();
	 lname = (lname==null)?"":lname;
	 
	 
  	 maritalStatus =   person.getPersonMarital();
	 maritalStatus = (maritalStatus==null)?"":maritalStatus;
	 
	 
	 
	mname =   person.getPersonMname();
	mname = (mname==null)?"":mname;
	
	
	notes =   person.getPersonNotes();
	notes = (notes==null)?"":notes;
	
	
	 race =  person.getPersonRace();
	 race = (race==null)?"":race;
	 
	ssn  =  person.getPersonSSN();
	ssn = (ssn==null)?"":ssn;
	
	
	
	status =   person.getPersonStatus();
	status = (status==null)?"":status;
	
	add1  =  person.getPersonAddress1();
	add1 = (add1==null)?"":add1;
	
	
	 add2 =  person.getPersonAddress2();
	 add2 = (add2==null)?"":add2;
	 
	city =	person.getPersonCity();
	city = (city==null)?"":city;
	
	
	country = person.getPersonCountry();
	country = (country==null)?"":country;
	
	
	email	= person.getPersonEmail();
	email = (email==null)?"":email;	
	
	hphone = person.getPersonHphone();
	hphone = (hphone==null)?"":hphone;
	
	bphone = person.getPersonBphone();
	bphone = (bphone==null)?"":bphone;	
	
	state	= person.getPersonState();
	state = (state==null)?"":state;	
	
	zip =	person.getPersonZip();
	zip = (zip==null)?"":zip;
	
	regDate = person.getPersonRegDate();
	regDate = (regDate==null)?"":regDate;	
	
	regBy = person.getPersonRegBy();
	regBy = (regBy==null)?"":regBy;
	
	
	
	timeZone=person.getTimeZoneId();
	timeZone = (timeZone==null)?"":timeZone;
	
	
	//get reg by name 
	userB.setUserId(EJBUtil.stringToNum(regBy));
    userB.getUserDetails();
      regByName = userB.getUserLastName() + ", " + userB.getUserFirstName();
	


	dGender = cd1.toPullDown("patgender", EJBUtil.stringToNum(gender));	

      dMarital = cd2.toPullDown("patmarital",EJBUtil.stringToNum(maritalStatus));	
		
      dBloodGroup = cd3.toPullDown("patblood", EJBUtil.stringToNum(bloodGroup));	

	dRace = cd4.toPullDown("patrace", EJBUtil.stringToNum(race));	
		
	dSites = cd5.toPullDown("patorganization", EJBUtil.stringToNum(organization));
	   
	dEmp = cd6.toPullDown("patemp", EJBUtil.stringToNum(employment));
	dEdu = cd7.toPullDown("patedu", EJBUtil.stringToNum(education));
	dStatus = cd8.toPullDown("patstatus", EJBUtil.stringToNum(status));
	
	dTimeZone = cd9.toPullDown("timeZone", EJBUtil.stringToNum(timeZone));	
	String enrollId = (String) tSession.getValue("enrollId");
%>
<jsp:include page="patienttabs.jsp" flush="true"> 
<jsp:param name="pkey" value="<%=personPK%>"/>
<jsp:param name="patientCode" value="<%=patientID%>"/>
<jsp:param name="patProtId" value="<%=enrollId%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>


<%


   }

	else

	{
		completeDet = true; //all data is visible
		displayStar = ""; // no star will be visible

		regBy = userIdFromSession;
		regByName = uName;
		
		dGender = cd1.toPullDown("patgender");	

	    dMarital = cd2.toPullDown("patmarital");	
		
		dBloodGroup = cd3.toPullDown("patblood");	

		dRace = cd4.toPullDown("patrace");	
		
	   dSites = cd5.toPullDown("patorganization");
	   
		dEmp = cd6.toPullDown("patemp");
		dEdu = cd7.toPullDown("patedu");
		dStatus = cd8.toPullDown("patstatus");
		dTimeZone = cd9.toPullDown("timeZone");	
				
	}

%>


<Form name="patient" method="post" action="updateallpatientdetails.jsp" onsubmit="return validate(document.patient)">




<input type="hidden" name="srcmenu" Value="<%=src%>">
<input type="hidden" name="pataccount" Value="<%=acc%>">
<input type="hidden" name="pkey" Value="<%=personPK%>">
<input type="hidden" name="page" Value="<%=page1%>">
<Input type="hidden" name="mode" value=<%=mode%>>
<Input type="hidden" name="patCode" value=<%=patCode%>>
<Input type="hidden" name="studyId" value=<%=studyId%>>




<Input type="hidden" name="statDesc" value=<%=statDesc%>>
<Input type="hidden" name="statid" value=<%=statid%>>
<Input type="hidden" name="studyVer" value=<%=studyVer%>>
<Input type="hidden" name="calledFrom" value=<%=calledFrom%>>




<table width="600" cellspacing="0" cellpadding="0" >

  <tr> 

    <td width="200">

       <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%> <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="patID" size = 20 MAXLENGTH = 20 value='<%=patientID%>'>

    </td>

  </tr>
	<tr colspan = 2>
	<td><br>
	<p class = "sectionHeadings" ><%=LC.L_Personal_Dets%><%--Personal Details*****--%></p><br>
	</td>
	</tr>

  <tr> 

    <td width="200">

       <%=LC.L_First_Name%><%--First Name*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patfname" size = 20 MAXLENGTH = 30 value='<%=fname%>'><%=displayStar%>

    </td>

  </tr>

 <tr> 

    <td width="200">

       <%=LC.L_Middle_Name%><%--Middle Name*****--%>

    </td>

    <td>

	<input type=<%=inputType%>  name="patmname" size = 20 MAXLENGTH = 20 value='<%=mname%>'><%=displayStar%>

    </td>

  </tr>

<tr> 

    <td width="200">

       <%=LC.L_Last_Name%><%--Last Name*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patlname" size = 20 MAXLENGTH = 20 value='<%=lname%>'><%=displayStar%>

    </td>

  </tr>

	<tr> 

    <td width="200">

       <%=LC.L_Date_OfBirth%><%--Date of Birth*****--%> <FONT class="Mandatory">* </FONT>

    </td>

    <td>
<%-- INF-20084 Datepicker-- AGodara --%>
	<%if(completeDet){ %>
		<input type=<%=inputType%> name="patdob" class="datefield" size = 10 MAXLENGTH = 10 value='<%=dob%>' readonly><%=displayStar%>
	<%}else{ %>
		<input type=<%=inputType%> name="patdob" size = 10 MAXLENGTH = 10 value='<%=dob%>' readonly><%=displayStar%>
	<% } %>
	</td>
	
  </tr>
   <tr> 

    <td width="200">

       <%=LC.L_Gender%><%--Gender*****--%>

    </td>

    <td>

	<%=dGender%>

    </td>

  </tr>
   <tr> 

    <td width="200">

       <%=LC.L_Marital_Status%><%--Marital Status*****--%>

    </td>

    <td>

	<%=dMarital%>

    </td>

  </tr>
	<tr> 

    <td width="200">

       <%=LC.L_Blood_Grp%><%--Blood Group*****--%>

    </td>

    <td>

	<%=dBloodGroup%>

    </td>

  </tr>

	<tr> 

    <td width="200">

       <%=LC.L_Race%><%--Race*****--%>

    </td>

    <td>

	<%=dRace%>

    </td>

  </tr>


	<tr> 

    <td width="200">

       <%=LC.L_Ssn_Upper%><%--SSN*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patssn" size = 20 MAXLENGTH = 20 value='<%=ssn%>'><%=displayStar%>

    </td>

  </tr>
  
  
  
	<tr>
	<td width="300"> <%=LC.L_Time_Zone%><%--Time Zone*****--%> <FONT class="Mandatory">* </FONT> </td>
	<td > <%=dTimeZone%> </td>
	</tr>
  
  
  
  
  
  
  
  
  
  
  
  

	<tr colspan = 2>
	<td><br>
	<p class = "sectionHeadings" ><%=LC.L_Reg_Dets%><%--Registration Details*****--%></p><br>
	</td>
	</tr>

	<tr> 

    <td width="200">

       <%=LC.L_Organization%><%--Organization*****--%> <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<%=dSites%>

    </td>

  </tr>
	<tr> 

    <td width="200">

       <%=LC.L_Reg_Date%><%--Registration Date*****--%> <FONT class="Mandatory">* </FONT>

    </td>

    <td>
<%-- INF-20084 Datepicker-- AGodara --%>	
	<% if(completeDet){ %>
		<input type=<%=inputType%> name="patregdate" class="datefield" size = 10 MAXLENGTH = 10 value="<%=regDate%>" readonly><%=displayStar%>
	<% } %>
	<%else { %>
		 	<input type=<%=inputType%> name="patregdate" size = 10 MAXLENGTH = 10 value="<%=regDate%>" readonly><%=displayStar%>
	<% } %>
    </td>
  </tr>


	<tr> 

    <td width="200">

       <%=LC.L_Reg_By%><%--Registered By*****--%> <FONT class="Mandatory">* </FONT>

    </td>

    <td>
	<input type=hidden name=patregby value='<%=regBy%>'>
          <input type=text name=patregbyname value='<%=regByName%>' readonly>
          <A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A> 
    </td>
		
  </tr>
  <tr> 

    <td width="200">

       <%=LC.L_Status%><%--Status*****--%> <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<%=dStatus%>

    </td>

  </tr>
  
  
  	<tr colspan = 2>
	<td><br>
	<p class = "sectionHeadings" ><%=LC.L_Contact_Info%><%--Contact Information*****--%></p><br>
	</td>
	</tr>

	
	<tr> 

    <td width="200">

       <%=LC.L_Address_1%><%--Address 1*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patadd1" size = 30 MAXLENGTH = 100 value='<%=add1%>'><%=displayStar%>

    </td>

  </tr>

	<tr> 

    <td width="200">

       <%=LC.L_Address_2%><%--Address 2*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patadd2" size = 30 MAXLENGTH = 100 value='<%=add2%>'><%=displayStar%>

    </td>

  </tr>
  
  	<tr> 

    <td width="200">

       <%=LC.L_City%><%--City*****--%>

    </td>

    <td>

	<input type=<%=inputType%>  name="patcity" size = 20 MAXLENGTH = 100 value='<%=city%>'><%=displayStar%>

    </td>

  </tr>
  
  	<tr> 

    <td width="200">

	<%=LC.L_State%><%--State*****--%>
    </td>

    <td>

	<input type=<%=inputType%> name="patstate" size = 20 MAXLENGTH = 100 value='<%=state%>'><%=displayStar%>

    </td>

  </tr>
  	<tr> 

    <td width="200">

	<%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patzip" size = 20 MAXLENGTH = 20 value='<%=zip%>'><%=displayStar%>

    </td>

  </tr>
	<tr> 

    <td width="200">

	<%=LC.L_Country%><%--Country*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patcountry" size = 30 MAXLENGTH = 100 value='<%=country%>'><%=displayStar%>

    </td>

  </tr>

<tr> 

    <td width="200">

	<%=LC.L_Home_Phone_S%><%--Home Phone(s)*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="pathphone" size = 30 MAXLENGTH = 100 value='<%=hphone%>'><%=displayStar%>

    </td>

  </tr>

	<tr> 

    <td width="200">

	<%=LC.L_Work_Phone_S%><%--Work Phone(s)*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patbphone" size = 30 MAXLENGTH = 100 value='<%=bphone%>'><%=displayStar%>

    </td>

  </tr>

	<tr> 

    <td width="200">

	<%=LC.L_E_Mail%><%--E-Mail*****--%>

    </td>

    <td>

	<input type=<%=inputType%> name="patemail" size = 30 MAXLENGTH = 100 value='<%=email%>'><%=displayStar%>

    </td>

  </tr>


  <tr colspan = 2>
	<td><br>
	<p class = "sectionHeadings" ><%=LC.L_Other%><%--Other*****--%></p><br>
	</td>
	</tr>

<tr> 

    <td width="200">

       <%=LC.L_Employment%><%--Employment*****--%>

    </td>

    <td>

	<%=dEmp%>

    </td>

  </tr>
  
  <tr> 

    <td width="200">

       <%=LC.L_Education%><%--Education*****--%>

    </td>

    <td>

	<%=dEdu%>

    </td>

  </tr>


	<tr> 

    <td width="200">

	<%=LC.L_Notes%><%--Notes*****--%>

    </td>
	</tr>
	
	<tr>
	<td>
	</td>
    <td  colspan = 2>

	<textarea name="patnotes" rows=5 cols=50 MAXLENGTH = 4000><%=notes%></textarea>

    </td>

  </tr>
  
</table>

<table width="100%">
<tr>
	   <td>
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8">
		
		</td><td width="50%">
		
		<%
if ((mode.equals("M") && (pageRight >= 6)) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 ) ))
{
%>

<button type="submit"><%=LC.L_Submit%></button>
<%
}
%>
		
		
	   </td>
</tr>
</table>

<br>


</Form>




<%

	} //end of if body for page right

else

{

%>



<jsp:include page="accessdenied.jsp" flush="true"> 

</jsp:include>   





<%

} //end of else body for page right

}//end of if body for session

else

{

%>



<jsp:include page="timeout.html" flush="true"/> 

<%

}

%>



<div>

<jsp:include page="bottompanel.jsp" flush="true"> 

</jsp:include>   

</div>



</div>



<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->   

</div>


</body>

</html>

