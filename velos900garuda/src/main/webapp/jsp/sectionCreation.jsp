<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%=MC.M_SecCreation_InEres%><%--Section Creation in eResearch*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT Language="javascript">
 function  validate(){
     formobj=document.sectionCreation
     if (!(validate_col('Section Name',formobj.sectionName))) return false
     }
</SCRIPT>

<body>

<jsp:include page="panel.htm" flush="true"/>   

<DIV class="formDefault">

<Form name="sectionCreation" method="post" action="sineup.jsp" onsubmit="return validate()">

<P class = "sectionHeadings">
 <%=LC.L_Sec_Dets%><%--Section Details*****--%>
</P>

<table width="500" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="300">
       <%=LC.L_Sec_Name%><%--Section Name*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="sectionName" size=20 MAXLENGTH = 100>
    </td>
  </tr>
  <tr> 
    <td width="300">
       <%=LC.L_Sec_Contents%><%--Section Contents*****--%>
    </td>
    <td>
	<TextArea name="sectionContents" rows=3 cols=40 MAXLENGTH = 2000>
	</TextArea>
    </td>
  </tr>
   <tr> 
    <td width="500" colspan=2>
	<%=MC.M_DoYouWant_InfoToPublic%><%--Do you want Information in this section to be available to the public?*****--%>
    </td>
    <tr>
	<td colspan=2>
	<input type="Radio" name="studyPubFlag" value='Y'><%=LC.L_Yes%><%--Yes*****--%>
	<input type="Radio" name="studyPubFlag" value='N'><%=LC.L_No%><%--No*****--%>
	&nbsp;
	<A href=""><%=MC.M_PublicVsNotPublic_Info%><%--What is Public vs Not Public Information?*****--%></A>
        </td>
    </tr>
 </tr>
</table>

<br>
<table width="200" cellspacing="0" cellpadding="0">
<td align=right>
<input type="Submit" name="submit" value="<%=LC.L_Submit%><%--Submit*****--%>">
</td>
</tr>
</table>
</Form>
</div>
</body>
</html>
