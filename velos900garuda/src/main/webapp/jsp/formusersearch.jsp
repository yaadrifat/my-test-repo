<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_User_Search%><%-- User Search*****--%></title>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="Javascript1.2">
	
function back(formobj,colName,uName) {
alert(formobj);
alert(colName);
alert(window.opener.document.form1.ld144_8.value); 
	/*if (document.layers) { 
		window.opener.document.div1.document.reports.id.value = id;
		window.opener.document.div1.document.reports.selUser.value = name;
		window.opener.document.div1.document.reports.val.value = name;
	}
	else
	{*/
		window.opener.document.formobj.colName.value = uName;
/*	}*/
	self.close();
}

</SCRIPT>

<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

</head>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>	
<%HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
//get the form object and the column name of user field 
//using these the user name would be set in the form
 
    String formobj = request.getParameter("formobj");
    String colName = request.getParameter("colName");	

	String accountId = (String) tSession.getValue("accountId");

	String userJobType = "";
	String dJobType = "";
	String dAccSites ="";
	CodeDao cd = new CodeDao();
	CodeDao cd2 = new CodeDao();
	cd2.getAccountSites(EJBUtil.stringToNum(accountId)); 
   	cd.getCodeValues("job_type");
	userJobType =userB.getUserCodelstJobtype();
	dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));		
	
%>
<BR>
<P class="defComments"><%=LC.L_Search_Criteria%><%--Search Criteria*****--%></P>
<br>
<Form  name="search1" method="post" action="formusersearch.jsp">
<input type="hidden" name="formobj" value=<%=formobj%>>
<input type="hidden" name="colName" value=<%=colName%>>

  <table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr valign="bottom"> 	
      <td class=tdDefault width=15%> <%=LC.L_User_Search%><%--User Search*****--%></td>
      <td class=tdDefault width=15%> <%=LC.L_User_FirstName%><%--User First Name*****--%>: 
        <Input type=text name="fname" size=20>
      </td>
      <td class=tdDefault width=15%> <%=LC.L_User_Last_Name%><%--User Last Name*****--%>: 
        <Input type=text name="lname" size=20>
      </td>
	  
	  <td class=tdDefault width=20%> <%=LC.L_Job_Type%><%--Job Type*****--%>:
		 <%=dJobType%> 
	  </td>
	  
	  <td class=tdDefault width=20%> <%=LC.L_Organization_Name%><%--Organization Name*****--%>:
         <%dAccSites = cd2.toPullDown("accsites");%>
		 <%=dAccSites%> 
	  </td>
	  
      <td class=tdDefault> 
	 	<button type="submit"><%=LC.L_Search%></button>
      </td>
    </tr>
  </table>
</Form>
 
  <%
	String uName = (String) tSession.getValue("userName");
	int pageRight = 0;
	String jobTyp = "";
	String orgName ="";
	String ufname= request.getParameter("fname") ;
	String ulname=request.getParameter("lname") ;

	jobTyp=request.getParameter("jobType") ;
	orgName=request.getParameter("accsites") ;
	
	if ((ufname==null) && (ulname==null) && (jobTyp == null) && (orgName == null)){%>
	
  <P class="defComments"> <%=MC.M_PlsSrchCrit_ClkGo%><%-- Please specify the Search criteria and then click on 
    'Search' to view the User List*****--%></P>
  <%}else{
		UserDao userDao = new UserDao();
		userDao.searchAccountUsers(EJBUtil.stringToNum(accountId),ulname,jobTyp,orgName,ufname);

	    ArrayList usrLastNames;
      	ArrayList usrFirstNames=null;
	    ArrayList usrMidNames=null;
      	ArrayList siteNames=null;
		ArrayList userSiteNames = null;
	    ArrayList jobTypes=null;
      	ArrayList usrIds=null;	
	    ArrayList grpNames=null;	
      	String usrLastName = null;
	    String grpName = null;
		String usrFirstName = null;
	    String usrMidName = null;
      	String siteName = null;
	    String jobType = null;
      	String usrId = null;	
		int counter = 0;
		String oldGrp = null;
		String usrName= "";
		String usr_id = "";
		String orgNam = "";
%>
  <br>
  <Form name="accountBrowser" method="post">
    <table class=tableDefault width="100%" border=0>
      <tr> 
      <%
		usrLastNames = userDao.getUsrLastNames();
		usrFirstNames = userDao.getUsrFirstNames();
		userSiteNames = userDao.getUsrSiteNames();
		usrIds = userDao.getUsrIds();
		int i;
		int lenUsers = usrLastNames.size();
	%>
	  <% if (lenUsers > 0){ %>
        <th width=50% align =left> <%=LC.L_User_Name%><%--User Name*****--%> </th>
		
		<th width="50%"> <%=LC.L_Organization%><%--Organization*****--%> </th>
		
	<%}else{%>
	        <th width=35% align =center><P class=defcomments> <%=MC.M_NoSrchResMatch_Criteria%><%-- No search results matching given criteria found*****--%></P></th>
	<%}%>
      </tr>

      <%

		for(i = 0 ; i < lenUsers ; i++)
	  	{
			usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();
			usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();
			usrId = ((Integer)usrIds.get(i)).toString();
			
			
			orgNam = (String)userSiteNames.get(i);
	
			if (orgNam == null){
				orgNam ="-";
			}			
					
			
			if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
        <%
			}else{
		  %>
      <tr class="browserOddRow"> 
        <% } usrName = usrFirstName + " " +usrLastName; 
		   %>
        <td width =200> <A href = "#" onClick="back('<%=formobj%>','<%=colName%>','<%=usrName%>');"><%= usrFirstName%>&nbsp; 
          <%= usrLastName%></A> </td>
		  
		  <td> <%= orgNam%> </td>
		  
		  
      </tr>
      <%}%>
    </table>
  </Form>
  <%}//end of Parameter check
}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
</div>
<div class ="mainMenu" id = "emenu"> </div>
</body>
</html>

