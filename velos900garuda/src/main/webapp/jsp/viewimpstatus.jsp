<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Import_Logs%><%--Import Logs*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<script>

function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;
	formObj.action="allPatientNew.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.submit(); 
}

function openLog( expId)
{
		param = "viewlog.jsp?expId=" + expId ;
		windowName= window.open(param,"log_self","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,dependant=false,width=800,height=500");
		windowName.focus();
}




</script>

<% String src="";
src= request.getParameter("srcmenu");
if (EJBUtil.isEmpty(src))
	src="";
%>	

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="bRows" scope="request" class="com.velos.eres.web.browserRows.BrowserRowsJB"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<BR>
<DIV class="browserDefault" id="div1"> 

 <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))
	{

	int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));	
	String uName = (String) tSession.getValue("userName");
	
	String strSearchImpDate = null;
	String strSearchImpId = null;
	String strSearchExpId = null;
	String strSearchImpStatus = null;
	
	String selStatus0 = "";
	String selStatus1 = "";
	String selStatus2 = "";
	  
	strSearchImpDate = request.getParameter("searchImpDate");
	strSearchImpId = request.getParameter("searchImpId");
	strSearchExpId = request.getParameter("searchExpId");
	strSearchImpStatus = request.getParameter("searchImpStatus");
	
  %>
  <%
  	        int counter = 0;
			String whereClause="";
			
			boolean refreshFlag = false;
			
			//////////FOR PAGINATION 
			StringBuffer sbImpSQL = new StringBuffer();
			StringBuffer sbCountSQL = new StringBuffer();
			StringBuffer sbWhere = new StringBuffer();
			
			sbImpSQL.append(" SELECT PK_IMPREQLOG, to_char(IMP_DATETIME,pkg_dateutil.f_get_datetimeformat ) IMP_DATETIME,");
			sbImpSQL.append(" decode(IMP_STATUS,0,'Not Started',1,'Importing',2,'Imported') IMP_STATUS_DESC,   usr_lst(IMP_REQUESTEDBY) IMP_REQUESTEDBY_NAME,IMP_REQUESTEDBY ,IMP_STATUS, EXP_ID");
			sbImpSQL.append(" from  er_impreqlog, er_user where pk_user = IMP_REQUESTEDBY and fk_account = " + accId );
			
			if (! EJBUtil.isEmpty(strSearchImpId))
					sbWhere.append(" and PK_IMPREQLOG = " + strSearchImpId);
			else	
					strSearchImpId = "";
					
			
			if (! EJBUtil.isEmpty(strSearchExpId))
					sbWhere.append(" and EXP_ID = " + strSearchExpId);
			else	
					strSearchExpId = "";
							
			
			if (! EJBUtil.isEmpty(strSearchImpStatus))
			{
				sbWhere.append(" and IMP_STATUS = " + strSearchImpStatus);
				
				if (strSearchImpStatus.equals("1"))
					selStatus1 = "SELECTED";
				else if (strSearchImpStatus.equals("2"))
					selStatus2 = "SELECTED";
				else
					selStatus0 = "SELECTED";	
			}	
			else
			{
				strSearchImpStatus = "";
			}	

			if (! EJBUtil.isEmpty(strSearchImpDate))
				sbWhere.append(" and to_char(trunc(IMP_DATETIME),pkg_dateutil.f_get_dateformat ) = '" + strSearchImpDate + "'");
			else
				strSearchImpDate = "";	
				
			sbImpSQL.append(sbWhere.toString());
			sbImpSQL.append(" ORDER BY PK_IMPREQLOG DESC");
					
					
			sbCountSQL.append(" SELECT count(*) ");
			sbCountSQL.append(" from  er_impreqlog , er_user where pk_user = IMP_REQUESTEDBY and fk_account = " + accId);
			sbCountSQL.append(sbWhere.toString()); 							
					
	
			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;
			
			pagenum = request.getParameter("page");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);
			
			String orderBy = "";
			orderBy = request.getParameter("orderBy");

			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "asc";
			}
			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;	
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;	   
			long firstRec = 0;
			long lastRec = 0;	   
			boolean hasMore = false;
			boolean hasPrevious = false;

			
			rowsPerPage =  Configuration.MOREBROWSERROWS ;
 			totalPages =Configuration.PAGEPERBROWSER ; 
		
			
			BrowserRows br = new BrowserRows();
			br = bRows.getPageRows(curPage,rowsPerPage,sbImpSQL.toString(),totalPages,sbCountSQL.toString(),orderBy,"null");

			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();
         	startPage = br.getStartPage();
		     
			hasMore = br.getHasMore();
			 
			hasPrevious = br.getHasPrevious();
			totalRows = br.getTotalRows();	   	   
			firstRec = br.getFirstRec();
			lastRec = br.getLastRec();	  	   

	
			///////////FOR PAGINATION END 
			
			
%>
  <Form name="impbrowser" method="post" action="viewimpstatus.jsp" onsubmit="">
	<BR>
  	<P class="sectionHeadings"><%=LC.L_View_ImportLogs%><%--View Import Logs*****--%></P>
	
	<P class="defComments"><%=MC.M_SrchFor_ImportReq%><%--Search for an Import Request*****--%></P>
	  
    <input type="hidden" name="srcmenu" value='<%=src%>'>
	<table width="100%" cellspacing="0" cellpadding="0" >
      <tr>
  		<td><%=LC.L_Import_ReqId%><%--Import Request Id*****--%>: <input type="text" name="searchImpId" size="10" value=<%=strSearchImpId%>></td>
		<td><%=LC.L_Export_ReqId%><%--Export Request Id*****--%>: <input type="text" name="searchExpId" size="10" value=<%=strSearchExpId%>></td>
<%-- INF-20084 Datepicker-- AGodara --%>
		 <td><%=LC.L_Date%><%--Date*****--%>: <input type="text" name="searchImpDate" class="datefield" size = 20 MAXLENGTH = 20 READONLY value=<%=strSearchImpDate%>></td>
	 	 <td><%=LC.L_Status%><%--Status*****--%>: 
		 <Select name="searchImpStatus">
		 <option value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
		 <option value="2" <%=selStatus2%> ><%=LC.L_Imported%><%--Imported*****--%></option>
		 <option value="1"  <%=selStatus1%> ><%=LC.L_Importing%><%--Importing*****--%></option>
		 <option value="0"  <%=selStatus0%> ><%=LC.L_Not_Started%><%--Not Started*****--%></option>
		  </Select>
		 </td>
		<td width ="5%" align="center"><button type="submit"><%=LC.L_Search%></button></td>
	</tr>				
	</table>
	<br>
	<P class="defComments"><%=MC.M_PgRefrAuto_ImpShowPending%><%--This page will be automatically refreshed after every 15 seconds to show the status of pending or current import requests (if any displayed in the current view)***** --%></font>. 
	</P>
    <table width="100%" >
       <tr> 
        	<th width="15%"> <%=LC.L_Import_ReqId%><%--Import Request ID*****--%> </th>
			<th width="15%"> <%=LC.L_Export_ReqId%><%--Export Request ID*****--%> </th>
	        <th width="20%"> <%=LC.L_Date%><%--Date*****--%> </th>
	        <th width="15%"> <%=LC.L_Status%><%--Status*****--%> </th>
			<th width="20%"> <%=LC.L_Req_By%><%--Requested By*****--%> </th>
			<th width="15%"> <%=LC.L_Import_Log%><%--Import Log*****--%> </th>
      </tr>
       <%

	int i ; 

	String impId = null;
	String expId = null;
	String impDate = null;
	String impStatusDesc = null;
	String impBy = null;
	String impStatus = null;
	int  impStatNum = 0;
	
	   	
    for(i = 1;i<=rowsReturned;i++)
	{	
		impId = br.getBValues(i,"PK_IMPREQLOG")  ;
		impDate =  br.getBValues(i,"IMP_DATETIME")  ; 
		impStatusDesc =  br.getBValues(i,"IMP_STATUS_DESC")  ; 
		impBy =  br.getBValues(i,"IMP_REQUESTEDBY_NAME")  ;
		impStatus =  br.getBValues(i,"IMP_STATUS")  ;
		expId =  br.getBValues(i,"EXP_ID")  ;

		if (EJBUtil.isEmpty(expId))
		{
			expId = MC.M_NotAval_AtMoment/*"Not available at the moment"*****/;
		}	
		impStatNum = EJBUtil.stringToNum(impStatus);
		
		if (impStatNum < 2)
		{
			refreshFlag	= true;
			
			if (impStatNum == 1)
			{
				impStatusDesc = "<font class=\"Green\">" + impStatusDesc + "</font>";  
			}
			else
			{
				impStatusDesc = "<font class=\"Red\">" + impStatusDesc + "</font>";
			}
		}	

		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td><%=impId%></td>
        <td><%=expId%></td>
		<td> <%=impDate%> </td>
        <td> <%=impStatusDesc%></td>
		<td> <%=impBy%></td>
		<td>
		<% if (impStatNum == 2)
		 {
		%>
		<A href="#" onClick="openLog(<%=expId%>)" ><%=LC.L_View_Log%><%--View Log*****--%></A>
		<%
		  }
		  else
		  {
		  %>
		  	<%=MC.M_NotAval_AtMoment%><%--Not available at the moment*****--%>
		  <%
		  	
		  }	
		%>	
		</td>
	 </tr>
      <%

	}//for

%>
<table>
		<tr><td>
		<% if (totalRows > 0) 
		{ Object[] arguments = {firstRec,lastRec,totalRows};		
		%>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
		<%}%>	
		</td></tr>
		</table>


    <table align=center>
	<tr>
<% 
	
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
	   cntr = (startPage - 1) + count;
	 
	if ((count == 1) && (hasPrevious))
	{   
    %>
	<td colspan = 2>
	<A href="viewimpstatus.jsp?srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchImpDate=<%=strSearchImpDate%>&searchImpId=<%=strSearchImpId%>&searchImpStatus=<%=strSearchImpStatus%>&searchExpId=<%=strSearchExpId%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>	
	<%
  	}	
	%>
	<td>
	<%
 
	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		
	    <A href="viewimpstatus.jsp?srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchImpDate=<%=strSearchImpDate%>&searchImpId=<%=strSearchImpId%>&searchImpStatus=<%=strSearchImpStatus%>&searchExpId=<%=strSearchExpId%>"><%= cntr%></A>
       <%
    	}	
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{   
   %>
   <td colspan = 3 align = center>
	&nbsp;&nbsp;&nbsp;&nbsp;<A href="viewimpstatus.jsp?srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchImpDate=<%=strSearchImpDate%>&searchImpId=<%=strSearchImpId%>&searchImpStatus=<%=strSearchImpStatus%>&searchExpId=<%=strSearchExpId%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> ></A>
  </td>	
	<%
  	}	
	%>
   </tr>
  </table>
</table>
</Form>
<%
	if (refreshFlag)
	{
%>
	 <META HTTP-EQUIV=Refresh CONTENT="15; URL=viewimpstatus.jsp?srcmenu=<%=src%>&page=<%=curPage%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchImpDate=<%=strSearchImpDate%>&searchImpId=<%=strSearchImpId%>&searchImpStatus=<%=strSearchImpStatus%>&searchExpId=<%=strSearchExpId%>">
 
<%
	} //end of refresh flag
	} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu"> 
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>

</html>












