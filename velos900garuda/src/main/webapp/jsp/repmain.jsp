<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_My_Rpts%><%--My Reports*****--%></title>

<%@ page import="com.velos.eres.service.util.*"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT>
function fMakeVisible(type){
	document.all("year_t").style.visibility = "hidden";
	document.all("month_t").style.visibility = "hidden";
	document.all("date_t").style.visibility = "hidden";
	typ = document.all(type);
	typ.style.visibility = "visible";
}
function setReport_acc(form,str){
	form.acc_rep[form.acc_rep.selectedIndex].value=str ;
	form.action="accrepfilter.jsp" ;
	form.submit();
}
function setReport_study(form,str){

	form.study_rep[form.study_rep.selectedIndex].value=str ;
	form.action="studyrepfilter.jsp" ;
	form.submit();
}
function setReport_pat(form,str){
	form.pat_rep[form.pat_rep.selectedIndex].value=str ;

	form.action="patrepfilter.jsp" ;
	form.submit();
}


function changeAction(formobj,str) {


	if (str =="Acc") {
	  formobj.action="accrepfilter.jsp" ;
	  formobj.study_rep.selectedIndex =0 ;
	  formobj.pat_rep.selectedIndex =0 ;


	    }
	else if (str=="Study") { formobj.action="studyrepfilter.jsp" ;
	     formobj.acc_rep.selectedIndex =0 ;
	  formobj.pat_rep.selectedIndex =0 ;
	 }
	  else {formobj.action="patrepfilter.jsp" ;
	   formobj.study_rep.selectedIndex =0 ;
	  formobj.acc_rep.selectedIndex =0 ;
	}
 }
function reportcheck(formobj,str){

	if (str=='Acc'){
		if (formobj.acc_rep[formobj.acc_rep.selectedIndex].value ==0)
		{alert("<%=MC.M_Selc_AccRpt%>");/*alert("Please select an account report");*****/
		return ;	}
	}
	if (str=='Study'){
		if (formobj.study_rep[formobj.study_rep.selectedIndex].value ==0)
		{ alert("<%=MC.M_Selc_StdRpt%>");/*alert("Please select a <%=LC.Std_Study_Lower%> report");*****/
		return ;	 }
	}
	if (str=='Pat'){

		if (formobj.pat_rep[formobj.pat_rep.selectedIndex].value ==0) {
		alert("<%=MC.M_Selc_PatRpt%>");/*alert("Please select a <%=LC.Pat_Patient_Lower%> report");*****/
		return  ;	}
	}
	formobj.submit();
}

function fOpenWindow(type,section,form){
	if (section==2) { //uncheck the upper filter range if lower range is selected
		for(i=0;i<form.filterType.length;i++)	{
			form.filterType[i].checked=false;
		}
	}

	if (section==1) { //uncheck the lower filter range if upper range is selected
		for(i=0;i<form.filterType2.length;i++)	{
			form.filterType2[i].checked=false;
		}
	}

	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=500;
		height=120;
	}
	if (type == "M"){
		width=270;
		height=70;
	}
	form.year.value ="";
	form.year1.value ="";
	form.month.value ="";
	form.dateFrom.value ="";
	form.dateTo.value ="";
	for (cnt=0;cnt <2;cnt++){
		form.range[cnt].value ="";
	}
	form.range[section - 1].value ="";
	if (type == 'A'){
			form.range[section - 1].value = "All"
	}
	 if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}

}

function fValidate(form){
	reportChecked=false;
	for(i=0;i<form.reportName.length;i++){
		sel = form.reportName[i].checked;
		if (form.reportName[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
		return false;
	}

	reportNo = form.repId.value;

	switch (reportNo) {
		case "1": //Active Protocol
			case "6":
			dateChecked=false;
			/*for(i=0;i<document.reports.filterType.length;i++){
				sel = document.reports.filterType[i].checked;
				if (document.reports.filterType[i].checked){
	   				dateChecked=true;
				}
			}*/

//			alert("*" +document.reports.range[0].value +"*")
			if (form.range[0].value != ""){
	   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}
			break;

		case "2": //Protocol by user
			if (form.selUser.value == "None") {
				alert("<%=MC.M_Selc_User%>");/*alert("Please select a User");*****/
				return false;
			}
			break;
		case "5": //Protocol by Therapeutic Area
			dateChecked=false;
			/*for(i=0;i<document.reports.filterType3.length;i++){
				sel = document.reports.filterType3[i].checked;
				if (document.reports.filterType3[i].checked){
	   				dateChecked=true;
				}
			}*/

			if (form.range[1].value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}

			if (form.tArea.value == "") {
				alert("<%=MC.M_Selc_ThrpArea%>");/*alert("Please select a Therapeutic Area");*****/
				return false;
			}

			break;

		case "7": //Protocol by Organization
			if (form.orgn.value == "") {
				alert("<%=MC.M_Selc_AnOrg%>");/*alert("Please select an Organization");*****/
				return false;
			}

			break;

		case "43": //User Profile
			if (form.selUser.value == "None") {
				alert("<%=MC.M_Selc_User%>");/*alert("Please select a User");*****/
				return false;
			}

			break;

			for(i=0;i<form.filterType.length;i++)	{
				if (form.filterType[i].checked){

				if (form.filterType[i].value == "4") {
					if (form.dateFrom.value == "") {
						alert("<%=MC.M_Selc_FromDate%>");/*alert("Please select From Date");*****/
						return false;
					}
					if (form.dateTo.value == "") {
						alert("<%=MC.M_Selc_ToDate%>");/*alert("Please select To Date");*****/
						return false;
					}

				}
				}
			}
		}
		form.submit();
}

function fSetId(ddtype, frmname){
	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		//alert(frmname.name);
		for(i=0;i<frmname.reportName.length;i++)	{
			if (frmname.reportName[i].checked){
				lsReport = frmname.reportName[i].value;
				ind = lsReport.indexOf("%");
				frmname.repId.value = lsReport.substring(0,ind);
				frmname.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}
}
</SCRIPT>


<SCRIPT language="JavaScript1.1">
function openwin12() {
    windowName=window.open("usersearchdetails.jsp?fname=&lname=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
}
</SCRIPT>


</head>



<% String src="";

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao2" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao3" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao4" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao5" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao6" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao7" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>


<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<br>
<DIV class="browserDefault" id="div1">

<%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))

   {

   	  int pageRight = 0;

      GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

      pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("REPORTS"));

	  if (pageRight > 0 )
	  {
       	String uName =(String) tSession.getValue("userName");
      	String acc = (String) tSession.getValue("accountId");
     	String tab = request.getParameter("selectedTab");
     	StringBuffer orgn = new StringBuffer();
     	StringBuffer user = new StringBuffer();
     	StringBuffer user1 = new StringBuffer();

      	ArrayList rep_first_list =new ArrayList(20);
     	ArrayList rep_second_list=new ArrayList(20);
     	ArrayList rep_third_list=new ArrayList(20);
     	ArrayList rep_first_help = new ArrayList(20);
     	ArrayList rep_second_help = new ArrayList(20);
     	ArrayList rep_third_help = new ArrayList(20);

     	Calendar cal = Calendar.getInstance();
     	int currYear = cal.get(cal.YEAR);
        StringBuffer rep_first = new StringBuffer();
        StringBuffer rep_second = new StringBuffer();
        StringBuffer rep_third = new StringBuffer();
     	int userId=0;
     	int counter=0;
     	int tAreaId =0;
     	StringBuffer tArea=new StringBuffer();
     	int siteId=0;
     	CodeDao cdTArea = new CodeDao();
     	SiteDao siteDao = new SiteDao();
     	cdTArea.getCodeValues("tarea");
        	UserDao userDao = new UserDao();

     	tArea.append("<SELECT NAME=tArea>") ;
     	for (counter = 0; counter <= (cdTArea.getCDesc()).size() -1 ; counter++){
     		tAreaId = ((Integer)((cdTArea.getCId()).get(counter))).intValue();
     		tArea.append("<OPTION value = "+ tAreaId +">" + (cdTArea.getCDesc()).get(counter) + "</OPTION>");
     	}

     	tArea.append("</SELECT>");

     	siteDao.getSiteValues(EJBUtil.stringToNum(acc));
     	orgn.append("<SELECT NAME=orgn >") ;
     	for (counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){
     		siteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue();
     		orgn.append("<OPTION value = "+ siteId +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
     	}
     	orgn.append("</SELECT>");
     %>
		<Form Name="reports" method="post">

	      <P class = "userName"><%= uName %></P>

         <!--<jsp:include page="reporttabs.jsp" flush="true"/>   -->

         <input type="hidden" name="srcmenu" value='<%=src%>'>

         <input type="hidden" name="selectedTab" value='<%=tab%>'>

         <br>
        <table>
         <tr>
          <td colspan = 3>

		 <P class="sectionHeadings"><%=LC.L_Rpt_Home%><%--Reports >> Home*****--%></P>
         <p class = "defComments"><%=MC.M_SelRpt_FromDropDown%><%--Select a report from the drop down to go directly to the filters selection page, or*****--%>...</p>
         </td>
         </tr>
<!--         <tr>
          <td width="40%" >
         	<Font class=numberFonts > 1.</font><Font class=reportText ><I>Select your report type </I></font>
         </td>
          <td width="40%" >
         	<Font class=numberFonts >2.</font> <Font class=reportText ><I>Select your report filters</I></font>
         </td>
          <td width="40%">
         	<Font class=numberFonts >3.</font> <Font class=reportText ><I>View your report</I></font>
         </td>
         </tr>
-->
         </Table>
<!--         <HR class=blue> -->
         <input type=hidden name=year>
         <input type=hidden name=month>
         <input type=hidden name=year1>
         <input type=hidden name=dateFrom>
         <input type=hidden name=dateTo>




         <%
         	CtrlDao ctrl = new CtrlDao();
         	ctrl.getControlValues("accspecific");
         	String catId = (String) ctrl.getCValue().get(0);

         	int accId = EJBUtil.stringToNum(acc);
         	repdao.getAllRep(EJBUtil.stringToNum(catId),accId); //Account Reports
         	ArrayList repIds= repdao.getPkReport();
         	ArrayList names= repdao.getRepName();
           	ArrayList desc= repdao.getRepDesc();

			//new methods to get the report columns and filters
			ArrayList repCols = repdao.getRepColumns();
			ArrayList repFilters = repdao.getRepFilters();
			String repFilter="";

			//

         	int repId=0;
         	String name="";

         	int len= repIds.size() ;

         	/*************************************************************/
         	CtrlDao ctrl1 = new CtrlDao();
         	ctrl1.getControlValues("usrspecific");
         	catId = (String) ctrl1.getCValue().get(0);
         	repdao1.getAllRep(EJBUtil.stringToNum(catId),accId); //User Related Reports
         	ArrayList repIdsUserRelated = repdao1.getPkReport();
         	ArrayList namesUserRelated = repdao1.getRepName();
           	ArrayList descUserRelated= repdao1.getRepDesc();
           	ArrayList repColsUserRelated = repdao1.getRepColumns();
			ArrayList repFiltersUserRelated = repdao1.getRepFilters();

         	int repIdUserRelated=0;
         	String nameUserRelated="";

         	int lenUserRelated = repIdsUserRelated.size() ;

         	CtrlDao ctrl2 = new CtrlDao();
         	ctrl2.getControlValues("orgspecific");
         	catId = (String) ctrl2.getCValue().get(0);
         	repdao2.getAllRep(EJBUtil.stringToNum(catId),accId); //Orgn Related Reports
         	ArrayList repIdsOrgRelated = repdao2.getPkReport();
         	ArrayList namesOrgRelated = repdao2.getRepName();
           	ArrayList descOrgRelated= repdao2.getRepDesc();
           	ArrayList repColsOrgRelated = repdao2.getRepColumns();
			ArrayList repFiltersOrgRelated = repdao2.getRepFilters();

         	int repIdOrgRelated=0;
         	String nameOrgRelated="";

         	int lenOrgRelated = repIdsOrgRelated.size() ;

         	CtrlDao ctrl3 = new CtrlDao();
         	ctrl3.getControlValues("tareaspecific");
         	catId = (String) ctrl3.getCValue().get(0);
         	repdao3.getAllRep(EJBUtil.stringToNum(catId),accId); //TArea Related Reports
         	ArrayList repIdsTareaRelated = repdao3.getPkReport();
         	ArrayList namesTareaRelated = repdao3.getRepName();
           	ArrayList descTareaRelated= repdao3.getRepDesc();
           	ArrayList repColsTareaRelated = repdao3.getRepColumns();
			ArrayList repFiltersTareaRelated = repdao3.getRepFilters();

         	int repIdTareaRelated=0;
         	String nameTareaRelated="";

         	int lenTareaRelated = repIdsTareaRelated.size() ;


         	/*************************************************************/

         	//***********report for Study specific and Protocol Specific**
               ctrl3 = new CtrlDao();
              ctrl3.getControlValues("studyspecific");

              catId = (String) ctrl3.getCValue().get(0);

			repdao4.getAllRep(EJBUtil.stringToNum(catId),accId); //Study Related Reports

			ArrayList repIdsSRelated= repdao4.getPkReport();

			ArrayList namesSRelated= repdao4.getRepName();
			ArrayList descSRelated= repdao4.getRepDesc();
			ArrayList repColsSRelated = repdao4.getRepColumns();
			ArrayList repFiltersSRelated = repdao4.getRepFilters();



			int lenSRelated = repIdsSRelated.size() ;

			//Protocl Specific
				ctrl3 = new CtrlDao();
			ctrl3.getControlValues("protcalspecific");

			catId = (String) ctrl3.getCValue().get(0);

			repdao5.getAllRep(EJBUtil.stringToNum(catId),accId); //Study Related Reports

			ArrayList repIdsProtCal= repdao5.getPkReport();

			ArrayList namesProtCal= repdao5.getRepName();

		   	ArrayList descProtCal= repdao5.getRepDesc();
		   	ArrayList repColsProtCal = repdao5.getRepColumns();
			ArrayList repFiltersProtCal = repdao5.getRepFilters();

			int lenProtCal = repIdsProtCal.size() ;


         	//End Study Specific and Protocol specific
         	//Patient Related Reports
          ctrl3 = new CtrlDao();
	      ctrl3.getControlValues("allpat");
		  catId = (String) ctrl3.getCValue().get(0);
          repdao6.getAllRep(EJBUtil.stringToNum(catId),accId); //All Patient Reports
          ArrayList repIdsAllPatients= repdao6.getPkReport();
          ArrayList namesAllPatients= repdao6.getRepName();
          ArrayList descAllPatients= repdao6.getRepDesc();
         ArrayList repColsAllPatients = repdao6.getRepColumns();
		 ArrayList repFiltersAllPatients = repdao6.getRepFilters();
   	       int lenAllPatient = repIdsAllPatients.size() ;

   	      	 ctrl3 = new CtrlDao();
        	ctrl3.getControlValues("patspecific");
        	catId = (String) ctrl3.getCValue().get(0);
        	repdao7.getAllRep(EJBUtil.stringToNum(catId),accId); //Patient Specific Reports
         	ArrayList repIdsOnePatients= repdao7.getPkReport();
        	ArrayList namesOnePatients= repdao7.getRepName();
         	ArrayList descOnePatients= repdao7.getRepDesc();
         	ArrayList repColsOnePatients = repdao7.getRepColumns();
	    	 ArrayList repFiltersOnePatients = repdao7.getRepFilters();

  			int lenOnePatient = repIdsOnePatients.size() ;

         	//End patient related reports

         	%>
         	<!--<tr>-->


           	<%
           	   rep_first.append("<SELECT  Size='1' NAME='acc_rep' onChange=changeAction(document.reports,'Acc') >") ;
  	   		    rep_first.append("<OPTION value=''  selected >"+LC.L_Select_AnOption/*Select an Option*****/+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</OPTION>");
           		 for(counter = 0;counter<len;counter++)
         	{
         		repId = ((Integer)repIds.get(counter)).intValue();
         		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
         		rep_first.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%accspecific"+">"+name+"</OPTION>");
         		rep_first_list.add(repId+"%"+name.replace(' ','~')+"%accspecific");
				repFilter=((repFilters.get(counter)) == null)?"-":(repFilters.get(counter)).toString();
         		rep_first_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repCols.get(counter) + "</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFilter+"</li></ul>");
         	%>


         	<%}%>

<!--         </td>
         	<td width=50%>
         	<Input type="radio" name="filterType" value="1" onClick="fOpenWindow('A','1', document.reports)"> All
         	<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1', document.reports)"> Year
         	<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1', document.reports)"> Month
         	<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1', document.reports)"> Date Range
         	<BR><Font class=comments>Selected Date Filter:</Font>
         	<input type=text name=range size=30 READONLY>
         	</td>

         <td width=10%>
         </td>
         </table>

         <HR class=blue></hr>
         <Br>
         <table border=0 width=100%>
         	<tr>
         	 <td colspan=3>
         	<p class = "reportHeadings">User Specific Reports</p>
         	</td>
         	</tr>
         	<tr>
         	<td width=40%> -->

         	<%
         		for(counter = 0;counter < lenUserRelated;counter++)
         	{
         		repId = ((Integer)repIdsUserRelated.get(counter)).intValue();
         		name=((namesUserRelated.get(counter)) == null)?"-":(namesUserRelated.get(counter)).toString();
         		rep_first.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%usrspecific"+">"+name+"</OPTION>");
         		rep_first_list.add(repId+"%"+name.replace(' ','~')+"%usrspecific");
         		rep_first_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repColsUserRelated.get(counter)+"</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFiltersUserRelated.get(counter)+"</li></ul>");
         	%>

         	<%}       %>
<!--         </td>
         	<td width=50%>
         		<p class = "defComments"> For user specific reports, clicking on the link will bring up the <A href="#" onclick="openwin12()">User Search</A> window. Currently the selected user is:
         		<Input type=text name=selUser size=20 READONLY value=None></p>
         	 </td>

         <td width=10%></td>
         </table> -->
<!--         <HR class=blue></hr>
         <Br>
         <table border=0 width=100%>
         	<tr>
         	 <td colspan=3>
         	<p class = "reportHeadings">Organization Specific Reports</p>
         	</td>
         	</tr>
         	<tr>
         	<td width=40%>-->

         	<% for(counter = 0;counter<lenOrgRelated;counter++)
         	{
         		repId = ((Integer)repIdsOrgRelated.get(counter)).intValue();
         		name=((namesOrgRelated.get(counter)) == null)?"-":(namesOrgRelated.get(counter)).toString();
         		rep_first.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%orgspecific"+">"+name+"</OPTION>");
         		rep_first_list.add(repId+"%"+name.replace(' ','~')+"%orgspecific");
                rep_first_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repColsOrgRelated.get(counter)+"</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFiltersOrgRelated.get(counter)+"</li></ul>");
         	%>

         	<%}%>
<!--         </td>
	  	<td width=50%><Font class=comments>
	      	Select Organization <%=orgn%></Font>
      	</td>
         <td width=10%></td>
         </table>

         <HR class=blue></hr>
         <Br>
         <table border=0 width=100%>
         	<tr>
         	 <td colspan=3>
         	<p class = "reportHeadings">Therapeutic Area Specific Reports</p>
         	</td>
         	</tr>
         	<tr>

         	 </td>
         	</td>
         	<td width=40%> -->

         	<% for(counter = 0;counter<lenTareaRelated;counter++)
         	{
         		repId = ((Integer)repIdsTareaRelated.get(counter)).intValue();
         		name=((namesTareaRelated.get(counter)) == null)?"-":(namesTareaRelated.get(counter)).toString();
         		rep_first.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%tareaspecific"+">"+name+"</OPTION>");
         		rep_first_list.add(repId+"%"+name.replace(' ','~')+"%tareaspecific");
         		rep_first_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repColsTareaRelated.get(counter)+"</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFiltersTareaRelated.get(counter)+"</li></ul>");
         	%>

         	<%}	rep_first.append("</SELECT>");
         		%>
             <Table border=0 >
	            <tr>
         	 <td>
         	<p class = "reportHeadings"><%=LC.L_Acc_Rpts%><%--Account Reports*****--%></p>
         	</td>	<td >
         	<%=rep_first%></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A onClick = "return reportcheck(document.reports,'Acc')"
      href="#"><IMG   src="./images/select.gif" align=absMiddle border=0></A></td></tr>

         	<% rep_second.append("<SELECT  Size='1' NAME=study_rep onChange=changeAction(document.reports,'Study')>") ;
  	   			rep_second.append("<OPTION value=''  selected >"+LC.L_Select_AnOption/*Select an Option*****/+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</OPTION>");
         		//Add Study and Protocol reports to dropdown

         		for(counter = 0;counter<lenSRelated;counter++)

			{

				repId = ((Integer)repIdsSRelated.get(counter)).intValue();

				name=((namesSRelated.get(counter)) == null)?"-":(namesSRelated.get(counter)).toString();
				rep_second.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%studyspecific"+">"+name+"</OPTION>");
				rep_second_list.add(repId+"%"+name.replace(' ','~')+"%studyspecific");
                rep_second_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repColsSRelated.get(counter)+"</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFiltersSRelated.get(counter)+"</li></ul>");
			}
			for(counter = 0;counter<lenProtCal;counter++)

	{

	       repId = ((Integer)repIdsProtCal.get(counter)).intValue();
           name=((namesProtCal.get(counter)) == null)?"-":(namesProtCal.get(counter)).toString();
           rep_second.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%protcalspecific"+">"+name+"</OPTION>");
			rep_second_list.add(repId+"%"+name.replace(' ','~')+"%protcalspecific");
			rep_second_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repColsProtCal.get(counter)+"</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFiltersProtCal.get(counter)+"</li></ul>");
        	}   	rep_second.append("</SELECT>");%>
         	  <tr><td ><p class = "reportHeadings"><%=LC.L_Std_Rpts%><%--<%=LC.Std_Study%> Reports*****--%></p></td><td ><%=rep_second%></td>
         	  	  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A onClick = "return reportcheck(document.reports,'Study')"
      href="#"><IMG   src="./images/select.gif" align=absMiddle border=0></A></td></tr>
             <%  rep_third.append("<SELECT  Size='1' NAME=pat_rep onChange=changeAction(document.reports,'Pat') >") ;
  	   			rep_third.append("<OPTION value=''  selected >"+LC.L_Select_AnOption/*Select an Option*****/+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</OPTION>");
                for(counter = 0;counter<lenAllPatient;counter++)
        		{
      			repId = ((Integer)repIdsAllPatients.get(counter)).intValue();
     			name=((namesAllPatients.get(counter)) == null)?"-":(namesAllPatients.get(counter)).toString();
     			rep_third.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%allpat"+">"+name+"</OPTION>");
     			rep_third_list.add(repId+"%"+name.replace(' ','~')+"%allpat");
     			rep_third_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repColsAllPatients.get(counter)+"</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFiltersAllPatients.get(counter)+"</li></ul>");
   	    	    }
   	    	    for(counter = 0;counter<lenOnePatient;counter++)
         		{
           		 repId = ((Integer)repIdsOnePatients.get(counter)).intValue();
       			name=((namesOnePatients.get(counter)) == null)?"-":(namesOnePatients.get(counter)).toString();
       			rep_third.append("<OPTION value = " +repId+"%"+name.replace(' ','~')+"%patspecific"+">"+name+"</OPTION>");
    			rep_third_list.add(repId+"%"+name.replace(' ','~')+"%patspecific");
                rep_third_help.add("<ul><li>"+LC.L_Rpt_Col/*Report Columns*****/+": " + repColsOnePatients.get(counter)+"</li><li>"+LC.L_Filter_By/*Filter By******/+": " + repFiltersOnePatients.get(counter)+"</li></ul>");

 	          }	rep_third.append("</SELECT>");%>
 	          	  <tr>     </tr>
         	 <tr><td ><p class = "reportHeadings"><%=LC.L_Pat_Rpts%><%--<%=LC.Pat_Patient%> Reports*****--%></p></td><td ><%=rep_third%></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A onClick = "return reportcheck(document.reports,'Pat')"
      href="#"><IMG   src="./images/select.gif" align=absMiddle border=0></A></td></tr>
<!--      	  <tr><td ><p class = "reportHeadings"><A href="dynreport.jsp?mode=M&srcmenu=tdmenubarItem8">Forms Reports</A></p></td><td></td></tr> -->

<tr><td><p><br></p></td></tr>
              </table>
           <HR class=blue>
 <TABLE width="100%" cellspacing="0" cellpadding="0">
<tr><td><p><br></p></td></tr>

		<tr>
			<td align="right">
				<p class = "defComments">...<%=MC.M_CursorOverLnk_ToBrowseRpt%><%--move cursor over the links below to browse report content and available filters*****--%>	</p>
			</td>
		</tr>


	    <TR>
  		  <th><Font class=reportText ><%=LC.L_Acc_Rpts%><%--Account Reports*****--%></font> </th></tr></table>
  		<table width="100%">



  		<tr class="browserOddRow">
  	<%  String report_name ;
         String repid_name  = "";
         int item_counter = 0;
  	   for (int i=0;i<rep_first_list.size();i++){
  	   	      item_counter ++ ;
            repid_name  = ((String) rep_first_list.get(i));
        report_name=repid_name.substring((repid_name.indexOf("%")+1),repid_name.lastIndexOf("%"));
	     report_name=report_name.replace('~',' ');
  	        if (item_counter==3) {
  	          %>


	   	     <td width="33%"><A href="#" onClick= "setReport_acc(document.reports,'<%=repid_name%>')" onmouseover="return overlib('<%=rep_first_help.get(i)%>',CAPTION,'<%=report_name%>');" onmouseout="return nd();"><b><%=report_name%></b></A></td></tr>

<%
		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}

//	   	     	 <tr> <%
  	     		 item_counter=0;
  	     		  }else { %>
  	     		 <td width="33%"><A href="#" onClick="setReport_acc(document.reports,'<%=repid_name%>')" onmouseover="return overlib('<%=rep_first_help.get(i)%>',CAPTION,'<%=report_name%>');" onmouseout="return nd();"><b><%=report_name%></b></A></td>
  	     		 <% }
  	     		  	  %>

  	     <%}%>
  	     </tr></table>
  	     	 <table width="100%">
  	     	 <tr><td><p><br></p></td></tr>

  	     	 <tr class="browserOddRow">
  	     	 <th><Font class=reportText ><%=LC.L_Std_Rpts%><%--<%=LC.Std_Study%> Reports*****--%></font></th></tr></table><table width="100%"><tr class="browserOddRow">
  	     <%	 item_counter = 0;
  	     for ( int i=0;i<rep_second_list.size();i++){
  	     	 item_counter++ ;
         repid_name  = ((String) rep_second_list.get(i));
        report_name=repid_name.substring((repid_name.indexOf("%")+1),repid_name.lastIndexOf("%"));
	     report_name=report_name.replace('~',' ');
  	            if (item_counter==3) {
  	          %>
  	     <td width="33%"><A href="#" onClick="setReport_study(document.reports,'<%=repid_name%>')" onmouseover="return overlib('<%=rep_second_help.get(i)%>',CAPTION,'<%=report_name%>');" onmouseout="return nd();"><b><%=report_name%></b></A></td></tr>

<%
		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}
//  	     		  <tr> <%
  	     		 item_counter=0;
  	     		  }else { %>
  	     		 <td width="33%"><A href="#" onClick="setReport_study(document.reports,'<%=repid_name%>')" onmouseover="return overlib('<%=rep_second_help.get(i)%>',CAPTION,'<%=report_name%>');" onmouseout="return nd();"><b><%=report_name%></b></A></td>
  	     		 <% }
  	     		  	  %>

  	    <% }%>
  	    </tr></table>
  	    <table width="100%">
		<tr><td><p><br></p></td></tr>
  	     	 <tr>
  	     	 <th><Font class=reportText><%=LC.L_Pat_Rpts%><%--<%=LC.Pat_Patient%> Reports*****--%></font></th></tr></table><table width="100%"><tr class="browserOddRow">

  	    <%		  item_counter=0 ;
  	    for (int i=0;i<rep_third_list.size();i++){
  	    	item_counter++;
            repid_name  = ((String) rep_third_list.get(i));
        report_name=repid_name.substring((repid_name.indexOf("%")+1),repid_name.lastIndexOf("%"));
	     report_name=report_name.replace('~',' ');
  	         if (item_counter==3) {
  	          %>
        <td width="33%"><A href="#" onClick="setReport_pat(document.reports,'<%=repid_name%>')" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS((String)rep_third_help.get(i))%>',CAPTION,'<%=report_name%>',LEFT, ABOVE);" onmouseout="return nd();"><b><%=report_name%></b></A></td></tr>
<%
		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}
//        		  <tr><%
  	     		 item_counter=0;
  	     		  }else { %>
  	    <td width="33%"> <A href="#" onClick="setReport_pat(document.reports,'<%=repid_name%>')" onmouseover="return overlib('<%=rep_third_help.get(i)%>',CAPTION,'<%=report_name%>',LEFT, ABOVE);" onmouseout="return nd();"><b><%=report_name%></b></A> </td>
  	     		 <% }
  	     		  	  %>
  	    	<%}	   %>

  	     	 </tr>




  </TABLE>
         <Input type = hidden  name=id >
         <Input type = hidden  name=accId value="<%=acc%>">
         <Input type = hidden  name="val" >
         <Input type=hidden name="repId">
         <Input type=hidden name="repName">

</form>


<%

} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

} //end of if session times out

else

{

%>

<jsp:include page="timeout.html" flush="true"/>


<%

}

%>


<div>

<jsp:include page="bottompanel.jsp" flush="true"/>

</div>


</div>



<DIV class="mainMenu" id = "emenu">

  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</DIV>


</body>

</html>
