<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT  Language="JavaScript1.2">
</SCRIPT>

</HEAD>


<BODY>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
  <jsp:useBean id="InvDetB" scope="request" class="com.velos.eres.web.invoice.InvoiceDetailJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

	String src = null;

	
	String studyId = request.getParameter("studyId");
	
	
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
	

	
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	
	String invPk = request.getParameter("invPk");
	
	String arIsSelected[] = null;
	String arDisplayDetails[] = null;
	String arMileStoneIds[] = null;
	String  arMilestoneAchievedCounts[] = null;
	String arMileAmountDue[]  = null;
	String arMileAmountInvoiced[] = null;
	String arMileDetailCount[] = null;
	 
	
	int totalMilestones = 0;
	String isSelected = "";
	String displayDetails = "";
	String mileStoneId = "";
	String milestoneAchievedCount = "";
	String mileAmountDue = "";

	String mileAmountInvoiced = "";
	String mileDetailCount = "";
	int intMileDetailCount = 0;
	
	InvoiceDetailDao inv = new InvoiceDetailDao();
	
	totalMilestones = EJBUtil.stringToNum(request.getParameter("totalmilecount"));
	
	
	
	if (totalMilestones > 1)
	{
		arIsSelected = request.getParameterValues("isChecked");
		arDisplayDetails = request.getParameterValues("displayDetails")	;
		arMileStoneIds = request.getParameterValues("mileStoneId")	;
		arMilestoneAchievedCounts = request.getParameterValues("mileAchCount")	;
		arMileAmountDue = request.getParameterValues("mileAmountDue")	;
		arMileAmountInvoiced = request.getParameterValues("mileAmountInvoiced")	;
		arMileDetailCount = request.getParameterValues("mileDetailCount")	;
	}
	else
	{
		isSelected = request.getParameter("isChecked");
		
		arIsSelected = new String[1];
		arIsSelected[0] = isSelected;
		
		displayDetails = request.getParameter("displayDetails")	;
		
		arDisplayDetails = new String[1];
		arDisplayDetails[0] = displayDetails;
		
	    mileStoneId = request.getParameter("mileStoneId")	;
	    
	    arMileStoneIds = new String[1];
	    arMileStoneIds[0] = mileStoneId ;
	    
	    
   		milestoneAchievedCount = request.getParameter("mileAchCount")	;
   		
   		arMilestoneAchievedCounts = new String[1];
   		arMilestoneAchievedCounts[0] = milestoneAchievedCount;
   		
		mileAmountDue = request.getParameter("mileAmountDue")	;

		arMileAmountDue = new String[1];
		arMileAmountDue [0] = mileAmountDue;
		
		
		mileAmountInvoiced = request.getParameter("mileAmountInvoiced")	;
		arMileAmountInvoiced = new String[1];
		arMileAmountInvoiced[0] = mileAmountInvoiced ;
		
		mileDetailCount = request.getParameter("mileDetailCount")	;
		arMileDetailCount = new String[1];
		arMileDetailCount [0] = mileDetailCount;
		
	}
	
	for (int i = 0; i< totalMilestones ; i++)
	{
			isSelected = arIsSelected[i];
			
			if (isSelected.equals("1")) //milestone was selected for the invoice
			{
				//set the default record for the invoice
				
				inv.setAmountDue(arMileAmountDue[i]); 
				inv.setAmountInvoiced(arMileAmountInvoiced[i]); 
				inv.setInvPk(invPk) ;
				inv.setMilestone(arMileStoneIds[i]); 
				inv.setPatient("0") ;
				inv.setPatProt("0") ;
				inv.setPaymentDueDate(""); 
				inv.setStudy(studyId) ;
				inv.setCreator(usr); 
				inv.setIPAdd(ipAdd) ;
				inv.setDisplayDetail(arDisplayDetails[i]);
				inv.setDetailType("H");
				inv.setMilestonesAchievedCount(arMilestoneAchievedCounts[i]);
				inv.setLastModifiedBy("0");
				inv.setAchDate("") ;
					 	inv.setLinkMileAchieved("0") ;
				
				// get the detailed records
				
				String arMileDetailAmountInvoiced[] = null;
				String arMileDetailPat[] = null;
				String arMileDetailAmount[] = null;
				String arMileDetAchievedOn[] = null;
				String arMileDetFKAchieved[] = null;
				String arMileDetChecked[] = null;
				
			
				intMileDetailCount	 = EJBUtil.stringToNum(arMileDetailCount[i]);
				
				 if (intMileDetailCount > 1)
				 {
				 	arMileDetailAmountInvoiced =  request.getParameterValues("mileDetailAmountInvoiced"+arMileStoneIds[i])	;
				 	arMileDetailPat =  request.getParameterValues("mileDetailPat"+arMileStoneIds[i])	;
				 	arMileDetailAmount =  request.getParameterValues("mileDetailAmount"+arMileStoneIds[i])	;
				 	
				 	arMileDetAchievedOn = request.getParameterValues("mileDetAchievedOn"+arMileStoneIds[i])	;
				 	arMileDetFKAchieved = request.getParameterValues("mileDetFKAchieved"+arMileStoneIds[i])	;
				 	arMileDetChecked = request.getParameterValues("mileDetailCheckHidden"+arMileStoneIds[i])	;
				 	
				 }
				 else
				 {
				 	arMileDetailAmountInvoiced = new String[1];
				 	arMileDetailPat = new String[1];
				 	arMileDetailAmount = new String[1];
				 	arMileDetAchievedOn = new String[1];
				 	arMileDetFKAchieved= new String[1];
				 	arMileDetChecked= new String[1];
				 	
				 	arMileDetailAmountInvoiced [0] = request.getParameter("mileDetailAmountInvoiced"+arMileStoneIds[i])	;
				 	arMileDetailPat[0] = request.getParameter("mileDetailPat"+arMileStoneIds[i])	;
				 	arMileDetailAmount[0] =  request.getParameter("mileDetailAmount"+arMileStoneIds[i])	;
				 	arMileDetAchievedOn[0] = request.getParameter("mileDetAchievedOn"+arMileStoneIds[i])	;
				 	arMileDetFKAchieved[0] = request.getParameter("mileDetFKAchieved"+arMileStoneIds[i])	;
				 	arMileDetChecked[0] = request.getParameter("mileDetailCheckHidden"+arMileStoneIds[i])	;
				 }
				 
					 
				 
				 // if mileDetailCount > 0, add invoice detail records
				 
				 if (intMileDetailCount > 0)
				 {
				 	for (int det=0; det < intMileDetailCount ; det++)
				 	{
				 		if ( EJBUtil.stringToNum(arMileDetChecked[det]) == 1)
				 		{
							inv.setAmountDue(arMileDetailAmount[det]); 
							inv.setAmountInvoiced(arMileDetailAmountInvoiced[det]); 
							inv.setInvPk(invPk) ;
							inv.setMilestone(arMileStoneIds[i]); 
							inv.setPatient(arMileDetailPat[det]) ;
							inv.setPatProt("0") ;
							inv.setPaymentDueDate(""); 
							inv.setStudy(studyId) ;
							inv.setCreator(usr); 
							inv.setIPAdd(ipAdd) ;
							inv.setDisplayDetail(arDisplayDetails[i]);
							inv.setDetailType("D");
							inv.setMilestonesAchievedCount("0");
							inv.setLastModifiedBy("0");
							inv.setAchDate( arMileDetAchievedOn[det]) ;
							inv.setLinkMileAchieved( arMileDetFKAchieved[det]) ;
						}	
								 
				 	}
				 
				 }
				 
				
				
			
			}
	
	} // for loop for total milestones
	
	int ret = 0;
	ret = InvDetB.setInvoiceDetails(inv);
	
	if (ret > 0)
	{
	
	
%>
<BR><BR><BR><BR>
	<p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
	
	<script>
		window.opener.location.reload();
	</script>
		
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=viewInvoice.jsp?invPk=<%=invPk%>">
	<%
	} %>
</div>
<%

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

<script>

</script>

</HTML>





