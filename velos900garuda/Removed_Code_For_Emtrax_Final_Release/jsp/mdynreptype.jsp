<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title> <%=MC.M_AdhocQry_RptType%><%--Ad-Hoc Query >> Report Type*****--%> </title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.dynrep.holder.ReportHolder,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
	<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
	<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>
window.name = "dynrep";
var formWin = null;
var formLkp="lookup";

 function  validate(formobj,act){

	formId=formobj.formIdStr.value;
	prevId=formobj.prevIdStr.value;

	if (act!="mdynreptype.jsp"){

	/*if (formobj.dynType.value=="A")
	{
		if (formId.length==0){
		alert("Please select a form to proceed.");
		return false;
		}
	}
	*/

	if (formobj.dynType.value=="P"){
	 if (formobj.patselect[0].checked){
	 formobj.patId.value="";
	 formobj.patPk.value="";
	 formobj.study1.value="";
	 } else if (formobj.patselect[1].checked){
	 formobj.patId.value="";
	 formobj.patPk.value="";

	 	if (formobj.study1.value=='')
	 	{
	 		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
	 		formobj.study1.focus();
	 		return false;
	 	}


	 } else if (formobj.patselect[2].checked){
	 		formobj.study1.value="";

	 	  if (formobj.patId.value=='')
		  {
		  	alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/
		  	return false;
		  }
	 }
	 }
	 else if (formobj.dynType.value=="S"){
	  if (formobj.studyselect[0].checked){
	  formobj.study1.value="";
	 }else  if (formobj.studyselect[1].checked){
	 	   	if (formobj.study1.value=='')
	 	{
	 		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
	 		formobj.study1.focus();
	 		return false;
	 	}
	 }

	}


	} else {

	if (formobj.dynType.value=="S")
	{
		formobj.study1.value="";
		formobj.patId.value="";
		formobj.patPk.value="";
		formobj.formIdStr.value="";
		formobj.formIdLookupStr.value="";
		formobj.prevIdStr.value="";
		formobj.formName.value="";

	}else if (formobj.dynType.value=="P"){
		formobj.study1.value="";
		formobj.formIdStr.value="";
		formobj.formIdLookupStr.value="";
		formobj.prevIdStr.value="";
		formobj.formName.value="";


	 }else if (formobj.dynType.value=="A"){
		formobj.formIdStr.value="";
		formobj.formIdLookupStr.value="";
		formobj.prevIdStr.value="";
		formobj.formName.value="";
	 }

	}

formobj.action=act;

// added by sonia abrol. 12/14/05, to call submit in case the page needs to be refreshed
if (act=="mdynreptype.jsp")
{
	formobj.submit();
}
//formobj.target="";

}
function openFormWin(formobj,act) {
formId=formobj.formIdStr.value;
prevId=formobj.prevId.value;
//act="dynpreview.jsp";

if (formId==prevId)
{}
else
{
alert("<%=MC.M_SelFlds_ForFrm%>");/*alert("Please Select the Fields for Selected Form.");*****/
return false;
}
formobj.target="formWin";
formobj.action=act;
formWin = open('','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynreport.submit();
void(0);
}
function resetVals(formobj,mode){
	if (formobj.repMode.value=="N"){
prevSelect=formobj.select.value;
if (formobj.dynType.value=="P"){
if (formobj.patselect[0].checked) selected="0";
if (formobj.patselect[1].checked) selected="1";
if (formobj.patselect[2].checked) selected="2";
if (mode=="All"){
if ((formobj.patId.value.length>0) || (formobj.study1.value.length>0)){
if (confirm("<%=MC.M_OptWillReset_WantCont%>"))/*if (confirm("Other options will be reset. Do you want to continue?"))*****/{
formobj.patId.value="";
formobj.patPk.value="";
formobj.study1.value="";
formobj.formIdStr.value="";
formobj.formIdLookupStr.value="";
formobj.formName.value="";
formobj.prevId.value="";
} else {
if (prevSelect=="0") formobj.patselect[0].checked=true;
if (prevSelect=="1") formobj.patselect[1].checked=true;
if (prevSelect=="2") formobj.patselect[2].checked=true;

return false;
}
}
}else if (mode=="pat"){
if ((formobj.study1.value.length>0) || (formobj.formIdStr.value.length>0) ){
if (confirm("<%=MC.M_OptWillReset_WantCont%>"))/*if (confirm("Other options will be reset. Do you want to continue?"))*****/{
formobj.study1.value="";
formobj.formIdStr.value="";
formobj.formIdLookupStr.value="";
formobj.formName.value="";
formobj.prevIdStr.value="";
} else {
if (prevSelect=="0") formobj.patselect[0].checked=true;
if (prevSelect=="1") formobj.patselect[1].checked=true;
if (prevSelect=="2") formobj.patselect[2].checked=true;
return false;
}
}
} else if (mode=="study"){
if ((formobj.patId.value.length>0) ||  (formobj.formIdStr.value.length>0)){
if (confirm("<%=MC.M_OptWillReset_WantCont%>"))/*if (confirm("Other options will be reset. Do you want to continue?"))*****/{
formobj.patId.value="";
formobj.patPk.value="";
formobj.formIdStr.value="";
formobj.formIdLookupStr.value="";
formobj.formName.value="";
formobj.prevIdStr.value="";

} else {
if (prevSelect=="0") formobj.patselect[0].checked=true;
if (prevSelect=="1") formobj.patselect[1].checked=true;
if (prevSelect=="2") formobj.patselect[2].checked=true;

return false;
}
}

}
} else if (formobj.dynType.value=="S"){

if (formobj.studyselect[0].checked) selected="0";
if (formobj.studyselect[1].checked) selected="1";

if (mode=="study"){
if (formobj.formIdStr.value.length>0){
if (confirm("<%=MC.M_OptWillReset_WantCont%>"))/*if (confirm("Other options will be reset. Do you want to continue?"))*****/{
formobj.formIdStr.value="";
formobj.formIdLookupStr.value="";
formobj.formName.value="";
formobj.prevId.value="";
}else {
if (prevSelect=="0") formobj.studyselect[0].checked=true;
if (prevSelect=="1") formobj.studyselect[1].checked=true;
return false;
}
}
} if (mode=="All"){
if (formobj.study1.value.length>0){
if (confirm("<%=MC.M_OptWillReset_WantCont%>"))/*if (confirm("Other options will be reset. Do you want to continue?"))*****/{
formobj.formIdStr.value="";
formobj.formIdLookupStr.value="";
formobj.study1.value="";
formobj.formName.value="";
formobj.prevId.value="";
}else{
if (prevSelect=="0") formobj.studyselect[0].checked=true;
if (prevSelect=="1") formobj.studyselect[1].checked=true;
return false;
}
}
}


}
formobj.select.value=selected;
}
}

function getWhereClauseForFormsLookup1(formtype, uniqueSearchType, searchUniqueId, study, pat) {
	return "mdynreptype|"+formtype+"|"+uniqueSearchType+"|"+searchUniqueId+"|"+study+"|"+pat;
}

function openLookup(formobj)
{
accId=formobj.accId.value;
userId=formobj.userId.value;
studyId=formobj.study1.value;
type=formobj.dynType.value;
var filter="";
//studyflg=formobj.studyselect[0].checked;

var searchUniqueId = formobj.searchUniqueId.value;
var uniqueSearchType = formobj.uniqueSearchType.value;


var uniqueFilterString;

if (searchUniqueId.length > 0)
{
	uniqueFilterString = " and exists (select * from erv_formflds v where v.pk_formlib = b.fk_formlib and lower(trim(v.fld_uniqueid))  " ;

	searchUniqueId = replaceSubstring(searchUniqueId,"~","[VELTILDE]");
	searchUniqueId = replaceSubstring(searchUniqueId,"'","''");
	searchUniqueId = replaceSubstring(searchUniqueId,"[VELTILDE]","~");


	if (!checkquote(searchUniqueId))
	{
	  return false;
	}

	if (uniqueSearchType == "E")  //equals
	{
		 uniqueFilterString  =  uniqueFilterString + " = lower(trim('" + searchUniqueId + "') ))" ;
	}
	else //like
	{
		 uniqueFilterString  =  uniqueFilterString + " like lower(trim('%" + searchUniqueId + "%') ))" ;
	}

}
else
{
	uniqueFilterString = "";
}



if (type=="A")
{
	filter = getWhereClauseForFormsLookup1(type, uniqueSearchType, searchUniqueId, 0, 0);
}
else if (type=="S"){
	if (formobj.studyselect[0].checked)
	{
	studyList=formobj.studyList.value;
 	//********************************start here*********************************************//
	filter = getWhereClauseForFormsLookup1(type, uniqueSearchType, searchUniqueId, 0, 0);
	//******************************endhere**************************************************//

	}
	else{
		if (studyId.length==0){
		alert("<%=MC.M_PlsSel_StdFst%>");/*alert("Please Select a <%=LC.Std_Study%> First");*****/
		formobj.study1.focus();
		return;
		 }

		filter = getWhereClauseForFormsLookup1(type, uniqueSearchType, searchUniqueId, parseInt(studyId, 10), 0);

//******************************endhere**************************************************//
	}
} else if (type=="P")
{
	if (formobj.patselect[0].checked){
		studyList=formobj.studyList.value;
	  /* filter=" ((b.fk_account="+accId+" and b.lf_displaytype='PA'))  and  b.FK_FORMLIB=a.pk_formlib and c.FK_FORMLIB=a.pk_formlib and " +
	 	 "  b.record_type <> 'D'  and c.formstat_enddate IS NULL and c.fk_codelst_stat in " +
		 "  (select pk_codelst from er_codelst where lower(codelst_subtyp)='a' and lower(codelst_type)='frmstat') ";*/
	//********************************start here*********************************************//
	filter = getWhereClauseForFormsLookup1(type, uniqueSearchType, searchUniqueId, 0, 0);

	}
	if (formobj.patselect[1].checked)
	{
		if (studyId.length==0){
		alert("<%=MC.M_PlsSel_StdFst%>");/*alert("Please Select a <%=LC.Std_Study%> First");*****/
		formobj.study1.focus();
		return;
		 }
		filter = getWhereClauseForFormsLookup1(type, uniqueSearchType, searchUniqueId, parseInt(studyId, 10), 0);

	}
if (formobj.patselect[2].checked)
{
	if (formobj.patId.value.length==0){
	alert("<%=MC.M_PlsSel_PatProc%>");/*alert("Please Select a <%=LC.Pat_Patient%> to proceed");*****/
	formobj.patId.focus();
	return;
	 }
 patId=formobj.patPk.value;
 //********************************start here*********************************************//
filter = getWhereClauseForFormsLookup1(type, uniqueSearchType, searchUniqueId, 0, parseInt(patId, 10));
}

}
//filter1=" F2.FK_ACCOUNT ="+ accId + " AND F2.FK_FORMLIB = F1.PK_FORMLIB and " +
//" F2.record_type <> 'D'   AND F4.FK_FORMLIB = F1.PK_FORMLIB   and F4.formstat_enddate IS NULL " +
// "  and lf_displaytype IN ( 'S','SP','A','SA','PA' )  and F4.fk_codelst_stat in (select pk_codelst from er_codelst where lower(codelst_desc)='active') " ;
    //alert(filter);
formobj.dfilter.value=filter;



formobj.target='formLkp';
formobj.action="multilookup.jsp?viewId=&viewName=Form Browser&form=dynreport&seperator={VELSEP}&frameMode=TB&keyword=formName|VELFORMNAME~formIdLookupStr|VELFORMPK|[VELHIDE]&filter=[VELGET:dfilter]";
//formobj.action="getlookup.jsp?viewId=&viewName=Form Browser&form=dynreport&keyword=formName|VELFORMNAME~formId|VELFORMPK&filter=[VELGET:dfilter]";
windowname=window.open('donotdelete.html' ,'formLkp',"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=600 top=100,left=100 0, ");
formobj.submit();
formobj.action="mdynreptype.jsp";
formobj.target="";
windowname.focus();

}
function selectFlds(formobj){
formId=formobj.formIdStr.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
from=formobj.from.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter+"&from="+from ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, ");
windowname.focus();
}
function setSeqFlag(formobj){
formobj.seqChg.value="Y";

}
function openpatwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=&form=dynreport&dispFld=patId&dataFld=patPk","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=500");
   windowName.focus();
}

function replaceSeparator()
{

	var formVal = "";
	formVal = document.dynreport.formName.value;
	var formIdVal = "";
	formIdVal  = document.dynreport.formIdLookupStr.value;
	document.dynreport.formNameReplaced.value = replaceSubstring(formVal, "{VELSEP}", ";") ;
	document.dynreport.formIdStr.value = replaceSubstring(formIdVal, "{VELSEP}", ";") ;


}
</SCRIPT>
<% String src,tempStr="",fldName="",fldCol="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");


%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll;" onFocus="replaceSeparator();">
<%} else {%>
<body onFocus="replaceSeparator();">
<%}%>
<DIV  id="div1" >
<%
        String formIdStr="",formName="",formType="",fldType="",repIdStr="",fldDisp="",fldWidth="",fldId="",select="0";
	String repName="",repHdr="",repFtr="",repType="",repFilter="",selStudy="",selPat="",dynType="",repMode="",patCode="";
	String dynTypeSess="",dynTypeReq="";
	String formIdLookupStr = "";

	HashMap TypeAttr=new HashMap();
	int seq=0,repId=0,pkey=0;
	ArrayList repColId=new ArrayList();
	ArrayList repCol=new ArrayList();
	ArrayList repColSeq=new ArrayList();
	ArrayList repColWidth=new ArrayList();
	ArrayList repColFormat=new ArrayList();
	ArrayList repColDispName=new ArrayList();
	ArrayList repColName=new ArrayList();
	ArrayList repColType=new ArrayList();
	ArrayList formIdList=new ArrayList();
	ArrayList formNameList=new ArrayList();
	DynRepDao dyndao=new DynRepDao();
	HashMap attributes = new HashMap();
	HashMap RepAttr = new HashMap();
	String containerRepType = "";
	String formNameReplacedStr = "";

	String availableCoreLookupTables = "";

	ReportHolder container=new ReportHolder();
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{
	String mode = request.getParameter("mode");
	mode=(mode==null)?"":mode;
	 if (mode.equals("M"))
	 {
	     System.out.println("Modify Mode" + mode);
	 	DynRepDao mDynDao=new DynRepDao();
		repIdStr=request.getParameter("repId");
		if (repIdStr==null) repIdStr="";
		System.out.println("repIdStr"+repIdStr);
		if (repIdStr.length() > 0)
		repId=(new Integer(repIdStr)).intValue();
		//dyndao=dynrepB.fillReportContainer(repIdStr);
		mDynDao.fillReportContainer(repIdStr);
		container=mDynDao.getReportContainer();
		container.DisplayDetail();
		tSession.setAttribute("reportHolder",container);
	}
	else if (mode.equals("N"))
	{
		if (tSession.getAttribute("reportHolder")!=null) tSession.removeAttribute("reportHolder");
		container.setReportMode("N");
	}
	if ((tSession.getAttribute("reportHolder"))==null)
	{
	tSession.setAttribute("reportHolder",container);

	}
	container=(ReportHolder)tSession.getAttribute("reportHolder");
	System.out.println("Container****" + container);
	container.DisplayDetail();
	//dynType
	dynType=request.getParameter("dynType");
	dynTypeReq=dynType;
	dynTypeReq=(dynTypeReq==null)?"":dynTypeReq.trim();
	//if (TypeAttr.containsKey("dynType")) dynTypeSess=(String)TypeAttr.get("dynType");
	//dynTypeSess=(dynTypeSess==null)?"":dynTypeSess.trim();
	if (dynType==null) dynType="";

	containerRepType = container.getReportType();

	if (StringUtil.isEmpty(containerRepType))
	{
		containerRepType = "";
	}
	if (dynType.length()==0)
	{
	dynType=container.getReportType();
	if (dynType==null) dynType="";
	}
	if (dynType.length()==0) dynType="P";

	//form
	formIdStr=request.getParameter("formIdStr");
	if (formIdStr==null) formIdStr="";
	if (formIdStr.length()==0)
	{
		if (containerRepType.equals(dynType))
		{
			formIdStr= container.getFormIdStr();
		}
	}
	if (formIdStr==null) formIdStr="";

	formIdLookupStr = StringUtil.replace(formIdStr,";","{VELSEP}");

	if (containerRepType.equals(dynType))
		{
			formName=container.getFormNameStr();
			formNameReplacedStr = StringUtil.replace(formName,"{VELSEP}",";");
		}
	//retrieve the formName


	container.setReportType(dynType);
	System.out.println("setting report typeMM" + dynType)	;


	selStudy=request.getParameter("study1");
	if (selStudy==null) selStudy="";
		if (selStudy.length()==0)
	selStudy=container.getStudyId();
	if (selStudy==null) selStudy="";


	//Patient
	selPat=request.getParameter("patId");
	if (selPat==null) selPat="";
	if (selPat.length()==0) {
	selPat=container.getPatientId();
	if (selPat==null) selPat="";
	if (selPat.length()>0){
	pkey=EJBUtil.stringToNum(selPat);
	person.setPersonPKId(pkey);
	person.getPersonDetails();
	patCode = person.getPersonPId();
	}
	}




	availableCoreLookupTables = container.getCoreLookupViewNamesString();

	//Rep
	repMode=container.getReportMode();
	String accId = (String) tSession.getValue("accountId");
	String userId=	(String) tSession.getValue("userId");
	String userIdFromSession = (String) tSession.getValue("userId");

	 int counter=0;
	int studyId=0;
	String studyList="";
	StringBuffer study=new StringBuffer();

	StudyDao studyDao = studyB.getUserStudies(userId, "dummy",0,"");
	study.append("<SELECT NAME=study1>") ;
	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();
		if (studyList.length()==0)
		   studyList=studyList+studyId;
		   else
		   studyList=studyList+","+studyId;
		 if (selStudy.length()>0){
		   if (studyId==((new Integer(selStudy)).intValue()))
		study.append("<OPTION value = "+ studyId +" selected>" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
		else
		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
	} else{
	study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
	}
	}
	study.append("</SELECT>");



%>


<%
%>

<form name="dynreport"  METHOD="POST" onsubmit="return validate(document.dynreport,'mdynselectflds.jsp?mode=next');">

<div class="tabDefTopN" id="divTab">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">

<tr bgcolor="#dcdcdc" align="center">

<%if (container.getFormIdStr()!=null){%>
<td><A href="mdynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><font color="red"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></font></A></td>
<%}else{%>
<td><font color="red"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></font></td>
<%}%>
<td>>> </td>
<%if ((container.getFldObjects()).size()>0){%>
<td><A href="mdynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>

<%}else{%>
<td><%=LC.L_Select_Flds%><%--Select Fields*****--%></td>
<%}%>
<td> >> </td>
<%if ((container.getFilterObjects()).size()>0){%>
<td><A href="mdynfilter.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>

<%}else{%>
<td><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></td>
<%}%>
<td> >> </td>
<%if ((container.getSortObjects()).size()>0){%>
<td><A href="mdynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>

<%}else{%>
<td><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></td>
<%}%>
<td> >> </td>
<%if (container.getReportName().length()>0){%>
<td><A href="mdynrep.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></A></td>
<%}else{%>
<td><%=LC.L_Preview_Save%><%--Preview & Save*****--%></td>
<%}%>

<td width="25%" align="right"> <button type="submit"><%=LC.L_Next%></button>  </td>
</tr>

</table>
</div>

<DIV class="tabDefBotN" id="div1">

<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="repMode" value=<%=repMode%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formIdStr" readonly value="<%=formIdStr%>">
<input type="hidden" name="formIdLookupStr" readonly value="<%=formIdLookupStr%>">

<input type="hidden" name="studyList" readonly value="<%=studyList%>">
<input type="hidden" name="dfilter" readonly value="">
<%if (mode.equals("M")){%>
<input type="hidden" name="formType" readonly value="<%=repType%>">
<%}else{%>
<input type="hidden" name="formType" readonly value="<%=formType%>">
<%}%>
<input type="hidden" name="prevIdStr" readonly value="<%=formIdStr%>">
<input type="hidden" name="prevId" readonly value="<%=formIdStr%>">
<input type="hidden" name="seqChg" readonly value="">
<input type="hidden" name="from" readonly value="pat">
<input type="hidden" name="repId" readonly value="<%=repId%>">

<P class="sectionHeadings"> <%=LC.L_Select_RptType%><%--Select Report Type*****--%> </P>
<table width="100%"><tr><td width="15%">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Rpt_Type%><%--Report Type*****--%></td>
<td width="50%">
<%if (repMode.equals("M")){%>
<select size="1" name="dynType" disabled onChange="return validate(document.dynreport,'mdynreptype.jsp');">
<%}else{%>
<select size="1" name="dynType" onChange="return validate(document.dynreport,'mdynreptype.jsp');">
<%}%>
<option value="S" <%if (dynType.equals("S")){%>selected<%}%>><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%></option>
<option value="P" <%if (dynType.equals("P")){%>selected<%}%>><%=LC.L_Patient%><%--<%=LC.Pat_Patient%>*****--%></option>
<option value="A" <%if (dynType.equals("A")){%>selected<%}%>><%=LC.L_Account%><%--Account*****--%></option>
</select></td>
<%if (repMode.equals("N")){%>
<td></td>
<!--<td><A href="#" onClick="return validate(document.dynreport,'dynreptype.jsp');"><img src="../images/jpg/Go.gif" align="absmiddle" border="0"></A></td>-->
<%}else{%>
<td></td>
<%}%>
</tr></table>
 <%
    if (dynType.equals("A"))
	 { %>
			<input name="studyselect" type="hidden">
			<input name="study1" type="hidden">
			<input name="patId" type="hidden">
			<input name="patPk" type="hidden">


	<%}

    if (dynType.equals("P")){%>
<P class="sectionHeadings"> <%=LC.L_SelPat_Popu%><%--Select <%=LC.Pat_Patient%> Population*****--%> </P>
<table width="100%">
  <tr><td>&nbsp;&nbsp;&nbsp;<input name="patselect" type="radio"  value="0" checked onClick="return resetVals(document.dynreport,'All')" <%if (repMode.equals("M")){%>disabled<%}%>><%=LC.L_All_Patients%><%--All <%=LC.Pat_Patients%>*****--%></td></tr>
   <tr><td width="15%">&nbsp;&nbsp;
   <%if (selStudy.length()>0){
   select="1";

   %>
     <input name="patselect" type="radio"  value="1" checked onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
   <%}else{%>
   <input name="patselect" type="radio"  value="1" onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
   <%}%>
   <%=MC.M_PatOn_Std%><%--<%=LC.Pat_Patients%> on a <%=LC.Std_Study%>*****--%></td><td width="50%"><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%>&nbsp;<%=study%></td></tr>
  <tr><td width="15%">&nbsp;&nbsp;
  <%if (selPat.length()>0){
  select="2";

  %>
  <input name="patselect" type="radio"  value="2" checked onClick="return resetVals(document.dynreport,'pat')" <%if (repMode.equals("M")){%>disabled<%}%>>
  <%}else {%>
  <input name="patselect" type="radio"  value="2" onClick="return resetVals(document.dynreport,'pat')" <%if (repMode.equals("M")){%>disabled<%}%>>
  <%}%>
  <%=LC.L_Specific_Patient%><%--Specific <%=LC.Pat_Patient%>*****--%></td><td width="50%">
  <%if (repMode.equals("N")){%>
  <A href="#" onClick="openpatwindow()"><%=LC.L_Select_APat%><%--Select a <%=LC.Pat_Patient%>*****--%></A>&nbsp;
  <input type="text" name="patId" readonly value="<%=patCode%>"></td></tr>
  <%}else{%>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="text" name="patId" readonly value="<%=patCode%>"></td></tr>
  <%}%>

  <input type="hidden" name="patPk" readonly value=<%=selPat%>>
    </table>
    <%}else if (dynType.equals("S")){ %>
    <P class="sectionHeadings"> <%=LC.L_Select_StdGrp%><%--Select <%=LC.Std_Study%> Group*****--%> </P>
<table width="100%" >
  <tr><td>&nbsp;&nbsp;&nbsp;<input name="studyselect" type="radio"  value="1" checked onClick="return resetVals(document.dynreport,'All')" <%if (repMode.equals("M")){%>disabled<%}%>><%=LC.L_All_Studies%><%--All <%=LC.Std_Studies%>*****--%></td></tr>
    <tr><td width="15%">&nbsp;&nbsp;
    <%if (selStudy.length()>0){
    select="1";

    %>
    <input name="studyselect" type="radio"  value="2" checked onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
    <%}else{%>
    <input name="studyselect" type="radio" value="2" onClick="return resetVals(document.dynreport,'study')" <%if (repMode.equals("M")){%>disabled<%}%>>
    <%}%>
    <%=LC.L_Specific_Study%><%--Specific Study*****--%></td><td width="50%"><%=study%></td></tr>
    </table>
    <%}%>
    <br>
    <P class="sectionHeadings"> <%=LC.L_Select_Form_S%><%--Select Form(s)*****--%> </P>
    <table width="100%">
    <tr><td width="15%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Frm_Name%><%--Form Name*****--%>
    </td><td width="85%"><input type="hidden" name="formName" readonly value="<%=formName%>" size="35" >
    	<input type="text" name="formNameReplaced" readonly value="<%=formNameReplacedStr%>" size="50" >

    <%if (repMode.equals("N")){%>
    <A href="#" onClick="return openLookup(document.dynreport)"><%=LC.L_SelOrSrch_Frm%><%--Select/Search Form*****--%></A>
   <BR>	<%=MC.M_SrchFrm_WhereFldID%><%--Search Forms with fields Where Field ID*****--%> &nbsp;&nbsp; <select size="1" name="uniqueSearchType" >
		<option value="L" selected ><%=LC.L_Contains%><%--Contains*****--%></option>
		<option value="E" ><%=LC.L_Is_EqualTo%><%--Is Equal To*****--%></option>
	</select> &nbsp;&nbsp;
   		<input type="text" name="searchUniqueId" value="" size="20" >

    <%}%>
    </td></tr>
    </table>
	<br>
	<P class="sectionHeadings"> <%=LC.L_Available_Tables%><%--Available Tables*****--%> </P>

    <table width="100%">
	<tr>
	<td>
    	<%=availableCoreLookupTables	%>
    	</td>
	</tr>
		<tr><td>&nbsp;</td></tr>
	</table>

<%if (dynType.equals("S") || dynType.equals("P") ){%>
    	<table width="100%"><tr><td>
<P class="defComments"><%=MC.M_IfMoreFrmSel_DataDispPat%><%--If one or more forms are selected, the data from Available Tables will be displayed only for those <%=LC.Pat_Patients_Lower%>/<%=LC.Std_Studies_Lower%> that have data entered in the selected Forms(s). <br> (Subject to logged in user's access rights)*****--%> </P>

	</td></tr></table>
    <% } %>
     <input type="hidden" name="select" value="<%=select%>">
    <br>

<!-- <table width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
      <td >

		<A href=""> <img src="../images/jpg/Back.gif" align="absmiddle" border="0"> </img> </A>

      </td>

      <td>
		<input type="image" src="../images/jpg/Next.gif" align="absmiddle"  border="0">
		<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>
      </td>
      </tr>

  </table> -->

</DIV>

    <input type="hidden" name="sess" value="keep">


</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
  </div>


</body>
</html>
