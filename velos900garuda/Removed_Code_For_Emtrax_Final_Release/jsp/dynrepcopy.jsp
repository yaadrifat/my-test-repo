<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_AdhocQry_CpyRptTmpt%><%--Ad-Hoc Query >> Copy Report Template*****--%> </title>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){

	

//	if (!(validate_col('e-Signature',formobj.eSign))) return false
//	if(isNaN(formobj.eSign.value) == true) {
//	alert("Incorrect e-Signature. Please enter again");
//	formobj.eSign.focus();
//	return false;
//  }
   
 
     
	//Added by IA 10.12.2006
	//Check if the template name is null


  if (!(validate_col('New Template Name',formobj.newTmplt))) {
	   formobj.newTmplt.focus();;
	   return false;
    }

// Added by Gopu to fix the bugzilla Issue #2719 (Trim leading and trailing spaces from Report Template Name while saving)

formobj.newTmplt.value = fnTrimSpaces(formobj.newTmplt.value);

}

</SCRIPT>
</head>

<BODY> 
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>

<jsp:useBean id="dynrep" scope="request" class="com.velos.eres.web.dynrep.DynRepJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 



<% String src;

	src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>  





<br>

<DIV class="formDefault" id="div1">

<% 

	String mode= "";
	String repId="";	
	DynRepDao dynDao=new DynRepDao();

	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	

		repId=request.getParameter("repId");
		repId=(repId==null)?"":repId;		
		String copyMode=request.getParameter("copyMode");
		String repName=request.getParameter("repName");
		String newTmplt=request.getParameter("newTmplt");
		String usrId = (String) tSession.getValue("userId");
		
		if (copyMode==null) {
			copyMode="final";
%>

	<FORM name="dynrepcopy" method="POST" id="dynrepcopyfrm" action="dynrepcopy.jsp" onsubmit="if (validate(document.dynrepcopy)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
 	
	<BR><BR>

	<TABLE width="98%" cellspacing="0" cellpadding="0" >
	
	<tr><td>&nbsp;</td></tr>
	
	<tr>
	   <td width="30%">
		<%=MC.M_AHocQryTemplate_Copied%><%--Ad-Hoc Query Template Being Copied*****--%>:   
	   </td>
	   <td ><%=repName%></td>
	</tr>
	
	<tr><td>&nbsp;</td><tr>
	
	

	<tr>
	   <td width="30%"><%=MC.M_New_AdhQryName%><%--New Ad-Hoc Query Name*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td >
		<input name="newTmplt" size="35" maxlength=100>
		 </td>
		</tr>
		<tr><td>&nbsp;</td><tr>
			<tr><td>&nbsp;<BR><BR></td><tr>
	
			</table>
		
			<table width="98%" bgcolor="#cccccc">

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="dynrepcopyfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
	
	



	 <input type="hidden" name="repId" value="<%=repId%>">
 	 <input type="hidden" name="copyMode" value="<%=copyMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">

	</FORM>
<%
	}  else if(copyMode.equals("final")) {
 //		String eSign = request.getParameter("eSign");	
//		String oldESign = (String) tSession.getValue("eSign");
//		if(!oldESign.equals(eSign)) {
%>
	  
<%
//		} else {   
    dynDao=dynrep.getReportDetails(repId);      
    dynrep.setId(EJBUtil.stringToNum(repId));
    dynrep.setRepName(newTmplt);
    dynrep.setUserId(usrId);
    int ret = 0;
    ret = dynrep.copyDynRep();    
    if (ret==1){
%>	
	<br><br><br><br><br> <p class = "sectionHeadings" align = center> <%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%> </p>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=dynrepbrowse.jsp?srcmenu=<%=src%>&selectedTab=1">
	<%	}else{%>
    <table>
       <tr>
       <td>
	<br><br><br><br><br> <p class = "sectionHeadings" align = center> <%=MC.M_AdHocQryName_AldyExst%><%--The Ad-Hoc Query Name entered already exists. Please click on the Back button and enter a new name.*****--%> 
 </p></td>
       </tr>
		 <tr height=20></tr>	
		 <tr>
			<td align=center>
			<button onclick="window.history.back();"><%=LC.L_Back%></button>
			</td>		
	     </tr>
       </table>	
		<%
		}	
    
		}
		
 //}
		
	 }//end of if body for session 
	
 	
else { %>

 <jsp:include page="timeout.html" flush="true"/> 

 <% } %>

<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

  

</DIV>



<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 

</div>



</body>

</HTML>
