<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/> 

<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title> <%=LC.L_Adhoc_FldSec%><%--Ad-Hoc Query >> Field Selection*****--%> </title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>

 function  checkValue(fldObj)
{

	if ( isWhitespace(fldObj.value) )
	{
		alert("<%=MC.M_SeqCntBlank_EtrValid%>");/*alert("Sequence cannot be blank or whitespace. Please enter a valid Number");*****/
		fldObj.focus()
		return false;		
	}
	
	if ( !isInteger(fldObj.value) )
	{
	  alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
	  fldObj.focus()
	return false;		
	}
	
	
}

window.name = "dynrep";
var formWin = null;
function  validate(formobj,act){
totcount=formobj.rows.value;

if (totcount==1){
/*if (!formobj.fldSelect.checked){
alert("Please select a field to proceed")
return false;
}*/
//if ((formobj.fieldCount.value<=0) && (!formobj.fldSelect.checked)) {
//KM-#5916
if (!formobj.fldSelect.checked) {
alert("<%=MC.M_PlsSelFld_ToProc%>");/*alert("Please select a field to proceed");*****/
return false;
}
makeStr(formobj,1)
} else if (totcount>1){
checkFlg=false; //flag to track if any field is selected
for(j=0;j<totcount;j++){
if (!formobj.fldSelect[j].checked) {continue;}
else {
checkFlg=true;
makeStr(formobj,j);
}
}//end for
//if ((!checkFlg) && (formobj.fieldCount.value<=0)){
//KM-#5916
if ((!checkFlg)){
	alert("<%=MC.M_PlsSelFld_ToProc%>");/*alert("Please select a field to proceed");*****/
return false;
}
} else {
alert("<%=MC.M_NoFldSel_SelcAnotherFrm%>")/*alert("No fields in selected form. Please select another form or add fields to form to proceed.")*****/
return false;
}

formobj.action="mdynfilter.jsp";

}

function beforeSelectForm(formobj,act)
 {
	totcount=formobj.rows.value;
	if (totcount==1){
		makeStr(formobj,1)
	}
	else if (totcount>1)
	{
		checkFlg=false; //flag to track if any field is selected
		for(j=0;j<totcount;j++){
			if (!formobj.fldSelect[j].checked) {
				continue;
			}
			else {
				checkFlg=true;
				makeStr(formobj,j);
			}
		}//end for
	}
		formobj.mode.value="formchange";
		formobj.action="mdynselectflds.jsp";
		formobj.submit();
		formobj.action="mdynfilter.jsp";
	
 }

function openFormWin(formobj,act) {
formId=formobj.formId.value;
prevId=formobj.prevId.value;
//act="dynpreview.jsp";

if (formId==prevId)
{}
else
{
alert("<%=MC.M_SelFlds_ForFrm%>");/*alert("Please Select the Fields for Selected Form.");*****/
return false;
}
formobj.target="formWin";
formobj.action=act;
formWin = open('','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynreport.submit();
void(0);
}


function selectFlds(formobj){
formId=formobj.formId.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
from=formobj.from.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter+"&from="+from ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, "); 
windowname.focus();
}
function setSeqFlag(formobj){
formobj.seqChg.value="Y";

}
function openpatwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=&form=dynreport&dispFld=patId&dataFld=patPk","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
   windowName.focus();
}

function makeStr(formobj,count)
{
	var totcount=formobj.rows.value;
	var uniqeRadio;
	var fldDispNameVal;
	var tempObj;
	var useUnique;
	var useUniqueVal;
	var fldUseDataValue;
 	 var calcSum;
 	var calcPer;
 	var fldRepeatCount;
      			
if (totcount>1){
	width=formobj.fldWidth[count].value;
	
	uniqeRadio= "formobj.fldUseUnique"+count;
    tempObj = eval(uniqeRadio); 
	
	useUnique = tempObj[1].checked;
	fldDispNameVal = formobj.fldDispName[count].value;
	
	if (useUnique)
	{
		useUniqueVal = "1";
	}
	else
	{
		useUniqueVal = "0";
	}
	
	 if (formobj.fldUseDataValue[count].checked)
	 {
	 	fldUseDataValue = "1";
	 }
	 else
	 {
	 	fldUseDataValue = "0";
	 }
	 
	 
 	  if (formobj.fldCalcPer[count].checked)
	 {
	 	calcPer = "1";
	 }
	 else
	 {
	 	calcPer = "0";
	 }
	  	 
 	  if (formobj.fldCalcSum[count].checked)
	 {
	 	calcSum = "1";
	 }
	 else
	 {
	 	calcSum = "0";
	 }
	 
	 
	 fldRepeatCount = formobj.fldRepeatCount[count].value;

	var fldKeyword;
	
	fldKeyword = formobj.fldKey[count].value;
	
	if (width.length==0) width="0";
		//formobj.fldSelect[count].value=encodeString(formobj.fldSeq[count].value+"[$]"+formobj.fldName[count].value+"[$]"+fldDispNameVal+"[$]" +width+"[$]"+formobj.fldCol[count].value + "[$]"+formobj.fldType[count].value+ "[$]"+formobj.fldFormat[count].value+ "[$]"+formobj.fldId[count].value +"[$]" + useUniqueVal + "[$]" +fldUseDataValue);
		formobj.fldSelect[count].value=(formobj.fldSeq[count].value+"[$]"+formobj.fldName[count].value+"[$]"+fldDispNameVal+"[$]" +width+"[$]"+formobj.fldCol[count].value + "[$]"+formobj.fldType[count].value+ "[$]"+formobj.fldFormat[count].value+ "[$]"+formobj.fldId[count].value +"[$]" + useUniqueVal + "[$]" +fldUseDataValue + "[$]" + calcSum + "[$]" + calcPer + "[$]" + fldRepeatCount + "[$]" + fldKeyword);
		
	
		
} else
{
	width=formobj.fldWidth.value;
	
	useUnique = formobj.fldUseUnique0[1].checked;
	
	if (useUnique)
	{
		fldDispNameVal = formobj.fldUniqueId.value;
		useUniqueVal = "1";
		
	}
	else
	{
		fldDispNameVal = formobj.fldDispName.value;
		useUniqueVal = "0";
	}
	
	if (formobj.fldUseDataValue.checked)
	 {
	 	fldUseDataValue = "1";
	 }
	 else
	 {
	 	fldUseDataValue = "0";
	 }
	 
	 
	  if (formobj.fldCalcPer.checked)
	 {
	 	calcPer = "1";
	 }
	 else
	 {
	 	calcPer = "0";
	 }
	  	 
 	  if (formobj.fldCalcSum.checked)
	 {
	 	calcSum = "1";
	 }
	 else
	 {
	 	calcSum = "0";
	 }
	 fldRepeatCount = formobj.fldRepeatCount.value;
	 fldKeyword = formobj.fldKey.value;
	
	if (width.length==0) width="0";
	
	//formobj.fldSelect.value=encodeString(formobj.fldSeq.value+"[$]"+formobj.fldName.value+"[$]"+fldDispNameVal +"[$]" +width+"[$]"+formobj.fldCol.value + "[$]"+formobj.fldType.value + "[$]"+formobj.fldFormat.value +"[$]"+formobj.fldId.value +"[$]" + useUniqueVal + "[$]" +fldUseDataValue);
	formobj.fldSelect.value=(formobj.fldSeq.value+"[$]"+formobj.fldName.value+"[$]"+fldDispNameVal +"[$]" +width+"[$]"+formobj.fldCol.value + "[$]"+formobj.fldType.value + "[$]"+formobj.fldFormat.value +"[$]"+formobj.fldId.value +"[$]" + useUniqueVal + "[$]" +fldUseDataValue + "[$]" + calcSum + "[$]" + calcPer + "[$]" + fldRepeatCount + "[$]" + fldKeyword);
}

}

function checkAll(formobj){
 	act="check";
 	totcount=formobj.rows.value;
  if (formobj.All.value=="checked") act="uncheck" ;
   if (totcount==1){
   if (act=="uncheck") formobj.fldSelect.checked =false ;
   else formobj.fldSelect.checked =true ;}
   else if (totcount>1) {
     for (i=0;i<totcount;i++){
     if (act=="uncheck") formobj.fldSelect[i].checked=false;
     else formobj.fldSelect[i].checked=true;
     }
    }else{}
       
    if (act=="uncheck") formobj.All.value="unchecked"; 
    else formobj.All.value="checked"; 
    
    
 }
 
 function checkUseUnique(formobj){
 	var act="check";
 	 var tempObj;
 	 var fldName = "";
 	totcount=formobj.rows.value;
 	
 	if (! formobj.repUseUnique.checked ) act="uncheck" ;
   	
    
   if (totcount>0) {
   	    for (i=0;i<totcount;i++)
     {
     	 	fldName = "formobj.fldUseUnique"+i;
      		tempObj = eval(fldName); 
    
       if (act=="uncheck") 
     	{     		
     	    tempObj[0].checked = true;
     	    tempObj[1].checked = false;
     	    formobj.fldDispName[i].value = formobj.fldName[i].value;
     	}
        else 
        {
        	 tempObj[0].checked = false;
        	 tempObj[1].checked = true;
        	 formobj.fldDispName[i].value = formobj.fldUniqueId[i].value;
        }	
     }
    }
             
 }

 function checkSingleUseUnique(formobj,row,action){
 	
 	 var tempObj;
 	 var fldName = "";
 	 
 	 fldName = "formobj.fldUseUnique"+row;
     tempObj = eval(fldName); 
      		
       if (action == "uncheck" ) 
     	{	
     	
     	    tempObj[0].checked = true;
     	    tempObj[1].checked = false;
     	    formobj.fldDispName[row].value = formobj.fldName[row].value;
     	}
        else 
        {
        	 tempObj[0].checked = false;
        	 tempObj[1].checked = true;
        	 formobj.fldDispName[row].value = formobj.fldUniqueId[row].value;
        }	
     
 }



 function checkUseDataVal(formobj){
 	var act="check";
 	var fldType;
 	
 	var totcount=formobj.rows.value;
 	var multipleChoiceCount = formobj.multipleChoiceCount.value;
 	 	
 	if (! formobj.repUseDataVal.checked ) act="uncheck" ;
  	  	
    if (totcount==1){
    	fldType = formobj.fldType.value;
    	
    	if (fldType == 'MD' || fldType == 'MC' || fldType == 'MR')
    	{
    	    	if (act=="uncheck") 
    	    		formobj.fldUseDataValue.checked = false ;
   	 			else 
   	 	 			formobj.fldUseDataValue.checked = true ;
   	 	}
   	 	else
   	 	{
   	 	    	if (act=="uncheck") 
   	 	    	formobj.fldUseDataValue.value = "1";
   	 				else 
   	 	 		formobj.fldUseDataValue.value = "0";
   	 		
   	 	} 			
   	}
   else if (totcount>1) {
   	    for (i=0;i<totcount;i++)
     {
     	 	    	 	      		     		
    	fldType = formobj.fldType[i].value;
    	
    	
       if (act=="uncheck") 
     	{     		
     		if (fldType == 'MD' || fldType == 'MC' || fldType == 'MR')
    		{
    			 formobj.fldUseDataValue[i].checked = false;
    	    }
    	    else
    	    {
    	    	formobj.fldUseDataValue[i].value = "0";
    	    }	    
     	}
        else 
        {
        	 if (fldType == 'MD' || fldType == 'MC' || fldType == 'MR')
    		{
    			formobj.fldUseDataValue[i].checked = true;
    	    }
    	    else
    	    {
    	    	formobj.fldUseDataValue[i].value = "1";
    	    }
        	 
        }	
     }
    }    
             
 }


</SCRIPT>
<% String src,tempStr="",fldName="",fldCol="",prevIdStr="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");


%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>
<DIV id="div1"> 
<%
        String formIdStr="",formNames="",formType="",fldType="",repIdStr="",fldDisp="",fldWidth="",fldId="0",repMode="",fldSec="",fldSecId="",secId="",prevSecId="",secName="";
	String repName="",repHdr="",repFtr="",repType="",repFilter="",selStudy="",fldSelect="",studyId="",patId="",dynType="",fldCheck="",patselect="",studyselect="";
	String prevSelForm="",isFieldSelected="N";
	String[] fldSelectArray=null;
	int seq=0,repId=0,len=0,index=-1;
	ArrayList mapCols=new ArrayList();
	ArrayList mapOrigIds= new ArrayList(); 
	ArrayList mapDispNames=new ArrayList();
	ArrayList mapColRepeatCounts=new ArrayList();
	ArrayList mapFormTypes=new ArrayList();
	ArrayList mapColTypes=new ArrayList();
	ArrayList mapSecNames=new ArrayList();
	ArrayList mapUids = new ArrayList();
	ArrayList mapColTypesNoRepeat = new ArrayList();
	ArrayList mapKeywords = new ArrayList();
		
	String mapPKTableName = "";
	String mapTablePK = "";
	String mapTableFilterCol = "";
	String mapStudyColumn = "";
	ArrayList mapSecIds=new ArrayList();
	ArrayList sessCol=new ArrayList();
	ArrayList sessColId=new ArrayList();
	ArrayList sessColName=new ArrayList();
	ArrayList sessColDisp=new ArrayList();
	ArrayList sessColSeq=new ArrayList();
	ArrayList sessColType=new ArrayList();
	ArrayList sessColStr=new ArrayList();
	ArrayList sessColWidth=new ArrayList();
	ArrayList sessColSec=new ArrayList();
	ArrayList sessColSecIds=new ArrayList();
	ArrayList sessColUseUniqueId = new ArrayList();
	ArrayList sessColUseDataVal = new ArrayList();
	
	ArrayList sessCalcSum = new ArrayList();
	ArrayList sessCalcPer = new ArrayList();
	
	ArrayList sessColRepeatCount = new ArrayList();
	ArrayList sessColKeyWord =new ArrayList();
	
	
	ReportHolder container = new ReportHolder();
	String mode = request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	String fldUniqueId = "";
	ArrayList formIdList,formNameList;
	int fldSelectArrayCount = 0;
	String useUniqueFldName = "";
	int multipleChoiceCount = 0;
	
	String repUseDataVal = "0";
	String repUseUnique = "0";
	String checkRepUseDataVal = "";
	String checkRepUseUnique = "";
	
	String fldColUseUniqueId = "";
	String fldColUseDataVal = "";
	String fldCheckDisplay = "";
	String fldCheckUnique = "";
	String fldCheckUseDataVal = "";
	String prevIgnoreFilters = "";
	
		String fldCalcSumVal = "";
	String fldCalcPersVal = "";
	String fldCalcSumCheck = "";
	String fldCalcPersCheck = "";
	String fldRepeatCount = "";
	String fldKey = "";
	
	int prevSeq = 0;
	
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	//HashMap attributes=new HashMap();
	//HashMap TypeAttr=new HashMap();
	//HashMap FldAttr=new HashMap();
	//attributes=(HashMap)tSession.getAttribute("attributes");
	
	container=(ReportHolder)tSession.getAttribute("reportHolder");
	
	if (container==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">
	<%}else {
	//if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	
	/*repUseDataVal = request.getParameter("repUseDataVal");
	repUseUnique = request.getParameter("repUseUnique");
	
	
	
	if (StringUtil.isEmpty(repUseUnique))
	{
		repUseUnique = "0";
	}
	if (StringUtil.isEmpty(repUseDataVal))
	{
		repUseDataVal = "0";
	}*/
	

	if (mode.equals("next"))
	{
	studyId=request.getParameter("study1");
	 if (studyId==null) studyId="";
	 
	selStudy=request.getParameter("study1");
	if (selStudy==null) selStudy="";
	
	patId=request.getParameter("patPk");
	 if (patId==null) patId="";
	
	 dynType=request.getParameter("dynType");
	 dynType=(dynType==null)?"":dynType;
	if (dynType.length()==0){
	 dynType= container.getReportType();
	if (dynType==null) dynType="";
	}
	
			 
	 //form
	 formIdStr=request.getParameter("formIdStr");
	 prevIdStr=request.getParameter("prevIdStr");
	 
	 
	 
	 if (prevIdStr==null) prevIdStr="";
	 if (formIdStr==null) formIdStr="";
	 
	 container.setFormIdStr(formIdStr);
	 
	 //prevId stores the previous form selcted if user plans to change the form again dring creatio,then we will remove sesion variable
	 //changed by sonia, to review the change,previously it was compared with 0
	 	 
	 if (prevIdStr.length()> 0)  container.setPrevFormSelection(prevIdStr);
	 
	 
	  formNames=request.getParameter("formName");
	 if( formNames==null) formNames="";
	  container.setFormNameStr(formNames);
	
	repId=EJBUtil.stringToNum(container.getReportId()); 
	  if (dynType.equals("S")){
	studyselect=request.getParameter("studyselect");
	studyselect=(studyselect==null)?"":studyselect;
	if (repId==0 )
	{
		if (studyselect.equals("2"))
		{
		 if (studyId.length()>0) container.setStudyId(studyId);
		}
		else
		{
			container.setStudyId("");
		}
	}	
	}
	
	container.setReportType(dynType);
	 if (dynType.equals("P")){
	 patselect=request.getParameter("patselect");
	 patselect=(patselect==null)?"":patselect;
	 if (repId ==0)
	 {
		 if (patselect.equals("1"))
		 {
			 if (studyId.length()>0) 
			 {
			 	container.setStudyId(studyId);
			 }	
		 }
		 else
		 {
		 	 	container.setStudyId("");
		 }
		 
		 if (patselect.equals("2"))
		 {
		 if (patId.length()>0) container.setPatientId(patId);
		 }
		 else
		 {
		 	container.setPatientId("");
		 }
	  }	 
	}
	 
	  
	  
	  if (repId == 0) //new mode
	  {
	  	
	  	 if (! prevIdStr.equals(formIdStr))
	  	 {
	  		container.removeObsoleteDependentObjects();
	  	 }	
	  }
	 
	 }//mode.equals("next") 
	  
	 String selForm=request.getParameter("selForm");
	 selForm=(selForm==null)?"":selForm;
	 formIdList= container.getFormIds();
	  formNameList=container.getFormNames();
	  container.DisplayDetail();
	  if (selForm.length()==0)
	     if (formIdList.size()>0) selForm=(String)formIdList.get(0);
	    
	 StringBuffer formDD=new StringBuffer("<select name='selForm' onchange='beforeSelectForm(document.dynreport)'>");
	 for (int i=0;i<formIdList.size();i++)
	 {
	   String selId=(String)formIdList.get(i);
	   if (selId.equals(selForm))
	   {
	    formDD.append("<option value="+selId+" selected>"+formNameList.get(i)+"</option>");
	   }
	   else
	   {
	    formDD.append("<option value="+selId+">"+formNameList.get(i)+"</option>");
	   }
	 }
	 
	 //String formDD=EJBUtil.createPullDown("selForm",EJBUtil.stringToNum(selForm), formIdList, formNameList);
	 
	
	/*else 
	{
	if (prevId.length()==0){
	 if (TypeAttr.containsKey("prevId")) prevId= (String)TypeAttr.get("formId");
	 }
	 if (prevId==null) prevId="";
	 if (!formId.equals(prevId)) {//means used changed the form
	 if (attributes.containsKey("FldAttr")) attributes.remove("FldAttr");
	 }
	}*/	
	 //study
	 
	if (studyId.length()==0) {
	  studyId= container.getStudyId();
	if (studyId==null) studyId="";
	}
	 //Patient
	 
	 if (patId.length()==0){
	patId=container.getPatientId();
	if (patId==null) patId="";
	}
	 
	
	// Set the selected fields session object
	fldSelectArray=request.getParameterValues("fldSelect");
	if (fldSelectArray==null)  
	{	
		fldSelectArray=new String[0];
		fldSelectArrayCount = 0;
	}
	else
	{
		fldSelectArrayCount = fldSelectArray.length ;
	}	
	
	
	container.Display();
	
	FieldContainer fldContainer=container.getFldObject(request.getParameter("prevSelForm"));
	
	prevSelForm = request.getParameter("prevSelForm");
	prevIgnoreFilters = request.getParameter("ignoreFilters");
	
	if (! StringUtil.isEmpty(prevSelForm))
	{
		// if fldSelectArray.length > 0, and the prevSelForm is of corelookup type
		if (prevSelForm.startsWith("C") && (fldSelectArrayCount > 0))	
		{
			container.setSelectedCoreLookupformIds(prevSelForm);
		}
		if (prevSelForm.startsWith("C") && (fldSelectArrayCount <= 0))	
		{
			container.removeSelectedCoreLookupformId(prevSelForm);
		}	
			
	}	
		
		
		
	if ((fldContainer==null) && fldSelectArray.length>0)
	    fldContainer=container.newFldObject(request.getParameter("prevSelForm"));
	  if (fldContainer!=null)
	  {
	  fldContainer.reset();
		 for(int z=0;z<fldSelectArray.length;z++)
		 {
			 String[] tempArray=StringUtil.strSplit(fldSelectArray[z],"[$]",false);
			 fldContainer.setFieldStr(fldSelectArray[z]);
			 fldContainer.setFieldSequence(tempArray[0]);
			 fldContainer.setFieldName(tempArray[1]);
			 fldContainer.setFieldDisplayName(tempArray[2]);
			 fldContainer.setFieldWidth(tempArray[3]);
			 fldContainer.setFieldColId(tempArray[4]);
			 fldContainer.setFieldType(tempArray[5]);
			 fldContainer.setFieldFormat(tempArray[6]);
			 fldContainer.setFieldDbId(tempArray[7]);
			 fldContainer.setUseUniqueIds(tempArray[8]);
			 fldContainer.setUseDataValues(tempArray[9]);
			 fldContainer.setCalcSums(tempArray[10]);
			 fldContainer.setCalcPers(tempArray[11]);
			 fldContainer.setFieldRepeatNumber(tempArray[12]);
			 
			 
			 			  	
			 		 
		  }//end for Z loop
		 //remove the unselected columns
	int colLen=0;
	ArrayList prevSessColId=fldContainer.getFieldPrevColId();
	sessColId=fldContainer.getFieldDbId();
	if (prevSessColId!=null) colLen=prevSessColId.size();
	
	
	if (colLen>0){
	for (int k=0;k<colLen;k++)
		{
		
		if (sessColId.contains(prevSessColId.get(k))){
		}else{
		
		if (!(sessColId.contains("-"+(String)prevSessColId.get(k))))
		fldContainer.setFieldDbId("-"+(String)prevSessColId.get(k));
		}   	
		}//end for
	}
	} //end (fldContainer!=null)
	
	
	
	if (fldContainer!=null)
	{
	 if (request.getParameterValues("fldCol")!=null)
	 fldContainer.setFieldList(EJBUtil.strArrToArrayList(request.getParameterValues("fldCol")));
	 if (request.getParameterValues("fldId")!=null)
	 fldContainer.setFieldPkList(EJBUtil.strArrToArrayList(request.getParameterValues("fldId")));
	 if ((request.getParameterValues("fldName"))!=null)
	 fldContainer.setFieldNameList(EJBUtil.strArrToArrayList(request.getParameterValues("fldName")));
	 if ((request.getParameterValues("fldType"))!=null)
	 fldContainer.setFieldTypeList(EJBUtil.strArrToArrayList(request.getParameterValues("fldType")));
	 
	 
	  if ((request.getParameterValues("fldKey"))!=null)
	  fldContainer.setFieldKeyword(EJBUtil.strArrToArrayList(request.getParameterValues("fldKey")));
	 
	 
	 	
	 
	 if ((request.getParameterValues("fldType"))!=null)
	 fldContainer.setFieldTypesNoRepeatFields(EJBUtil.strArrToArrayList(request.getParameterValues("fldType")));
	 
	 
	 
	 fldContainer.setPkTableName(request.getParameter("mapPKTableName"));
	 fldContainer.setMainFilterColName(request.getParameter("mapTableFilterCol"));
	 fldContainer.setPkColName(request.getParameter("mapTablePK"));	 	 
	 fldContainer.setMapStudyColumn(request.getParameter("mapStudyColumn"));	 	 
	  // set fld sequence
		 fldContainer.setFieldSequence(request.getParameter("fldSeq"));	 	
		 
	 
	 if (! StringUtil.isEmpty(request.getParameter("mapStudyColumn")) )
	 {
	 	container.setCheckForStudyRights(true);
	 }
	 
	 }
	 
	 
	 container.Display();
	 
	if (! StringUtil.isEmpty(prevIgnoreFilters))
	{
		container.setIgnoreFilters(prevSelForm,prevIgnoreFilters);
	}	

	 	
	 tSession.setAttribute("reportHolder",container);
	
	 formType=request.getParameter("formType");
	 formType=(formType==null)?"":formType;
	 
	 String accId = (String) tSession.getValue("accountId");
	 String userId=	(String) tSession.getValue("userId");
	 String userIdFromSession = (String) tSession.getValue("userId");
	 
	 //formId=(String)TypeAttr.get("formId");
	// if( formId==null) formId="";
	
	DynRepDao dyndao=dynrepB.getMapData(selForm);
	String ignoreFilters = "";
	//dyndao.getMapData(formId);
	
	if (! selForm.startsWith("C")) // pre process map only if the form is not a core table lookup view
	{
		dyndao.preProcessMap();
	}	
	ignoreFilters = dyndao.getIgnoreFilters();
	if (StringUtil.isEmpty(ignoreFilters))
		ignoreFilters = "";

	mapCols= dyndao.getMapCols();
	mapOrigIds= dyndao.getMapOrigIds();
	mapDispNames= dyndao.getMapDispNames();
	mapColRepeatCounts = dyndao.getMapColSecRepNo();
	mapFormTypes=dyndao.getMapFormTypes();
	mapColTypes=dyndao.getMapColTypes();
	
	mapColTypesNoRepeat = dyndao.getMapColTypesNoRepeatFields();
	mapSecNames=dyndao.getMapColSecNames();
	mapSecIds=dyndao.getMapColSecIds();
	mapUids = dyndao.getMapUids();
	
	mapPKTableName = dyndao.getMapTableName();
	mapTablePK = dyndao.getMapPrimaryKeyCol();
	mapTableFilterCol = dyndao.getMapFilterCol();
	mapStudyColumn = dyndao.getMapStudyColumn();
	mapKeywords  = dyndao.getFieldKeyword();
	
	
	
	if (mapFormTypes.size()>0)
	formType= (String)mapFormTypes.get(0);
	len=mapOrigIds.size();
	 int counter=0;
	  //attributes=(HashMap)tSession.getAttribute("attributes");
	   FieldContainer fieldContainer=container.getFldObject(selForm);
	   if (fieldContainer!=null)
	   {
		  sessCol = fieldContainer.getFieldColId();
		  sessColName = fieldContainer.getFieldName();
		  sessColDisp = fieldContainer.getFieldDisplayName();
		  sessColSeq = fieldContainer.getFieldSequence();
		  sessColType = fieldContainer.getFieldType();
		  sessColStr = fieldContainer.getFieldStr();
		  sessColWidth = fieldContainer.getFieldWidth();
		  sessColId = fieldContainer.getFieldDbId();
		  sessColUseUniqueId = fieldContainer.getUseUniqueIds();
		  sessColUseDataVal = fieldContainer.getUseDataValues();
		  
		  sessCalcSum =  fieldContainer.getCalcSums();
		  sessCalcPer =  fieldContainer.getCalcPers();
		  sessColRepeatCount =  fieldContainer.getFieldRepeatNumber();
		  sessColKeyWord =  fieldContainer.getFieldKeyword();
		  
	  }

  	
  	/*repUseDataVal = container.getRepUseDataValue();
	repUseUnique = container.getRepUseUniqueId();
	
	if (StringUtil.isEmpty(repUseDataVal))
	{
		repUseDataVal = "0";
	}
	if (StringUtil.isEmpty(repUseUnique))
	{
		repUseUnique = "0";
	}*/
	
	if (repUseDataVal.equals("1"))
	{
		checkRepUseDataVal = "CHECKED";
	}
	if (repUseUnique.equals("1"))
	{
		checkRepUseUnique = "CHECKED";
	}
	

	  
	  
	 %>
	 

<form name="dynreport"  METHOD=POST>

<div class="tabDefTopN" id="divTab">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">

<tr bgcolor="#dcdcdc" align="center">
<%if (container.getFormIdStr()!=null){%>
<td><A href="mdynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><%=LC.L_Select_RptType%><%--Select Report Type*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getFldObjects()).size()>0){%>
<td><A href="mdynselectflds.jsp?mode=link&srcmenu=<%=src%>"><font color="red"><%=LC.L_Select_Flds%><%--Select Fields*****--%></font></A></td>
<%}else{%>
<td><font color="red"><%=LC.L_Select_Flds%><%--Select Fields*****--%></font></td>
<%}%>
<td>>></td>
<%if ((container.getFilterObjects()).size()>0){%>
<td><A href="mdynfilter.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>
<%}else{%>
<td><b><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></b></td>
<%}%>
<td>>></td>
<%if ((container.getSortObjects()).size()>0){%>
<td><A href="mdynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>
<%}else{%>
<td><b><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></b></td>
<%}%>
<td>>></td>
<%if (container.getReportName().length()>0){%>
<td><A href="mdynrep.jsp?mode=link&srcmenu=<%=src%>"><b><%=LC.L_Preview_Save%><%--Preview & Save*****--%></b></A></td>
<%}else{%>
<td><%=LC.L_Preview_Save%><%--Preview & Save*****--%></td>
<%}%>

<td width="20%" align="right"> 
<button type="submit" onClick ="return validate(document.dynreport,'mdynfilter.jsp');"><%=LC.L_Next%></button>
</td> 
</tr>

</table>

</div>

<DIV class="tabDefBotN" id="div1">

<input type=hidden name="mode" value="next">
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formIdStr" readonly value="<%=formIdStr%>">
<input type="hidden" name="formName" readonly value="<%=formNames%>">
<input type="hidden" name="prevSelForm" value="<%=selForm%>">
<input type="hidden" name="mapPKTableName" value="<%=mapPKTableName%>">
<input type="hidden" name="mapTablePK" value="<%=mapTablePK%>">
<input type="hidden" name="mapStudyColumn" value="<%=mapStudyColumn%>">
<input type="hidden" name="mapTableFilterCol" value="<%=mapTableFilterCol%>">
<input type="hidden" name="ignoreFilters" value="<%=ignoreFilters%>">
	
<%if (mode.equals("M")){%>
<input type="hidden" name="formType" readonly value="<%=repType%>">
<%}else{%>
<input type="hidden" name="formType" readonly value="<%=formType%>">
<%}%>
<input type="hidden" name="prevId" readonly value="<%=formIdStr%>">
<input type="hidden" name="seqChg" readonly value="">
<input type="hidden" name="from" readonly value="pat">
<input type="hidden" name="repId" readonly value="<%=repId%>">
<input name="rows" type="hidden" value="<%=len%>">
<input name="fieldCount" type="hidden" value=<%=container.getSelectedFieldCount()%>>

<table width="98%">
<tr>
<td width="15%"><b><%=LC.L_Sel_FrmOrTable%><%--Select Form/ Table*****--%></b></td>
<td width="35%"><%=formDD%></td>
<td width="50%">
<input type="checkbox" name="repUseUnique" value="1" onClick="return checkUseUnique(document.dynreport)" <%=checkRepUseUnique%> > <%=MC.M_SellD_AsDispName%><%--Select Field ID as 'Display Name' for all Fields*****--%>
<br><input type="checkbox" name="repUseDataVal" value="1" onClick="return checkUseDataVal(document.dynreport)" <%=checkRepUseDataVal%> > <%=MC.M_DispResp_RptOutput%><%--Display Response Data Value in Report Output*****--%>
</td>
</tr>
</table>

<table width="100%" Border="1">
  <tr>
  <TH width="10%"><input type="checkbox" name="All" value="" onClick="checkAll(document.dynreport)"><%=LC.L_Select%><%--Select*****--%></TH>
  <!--<TH width="10%">Sequence</TH>-->
  <TH width="25%"><%=LC.L_Fld_Name%><%--Field Name*****--%></TH>
  <TH width="25%"><%=LC.L_Display_Name%><%--Display Name*****--%></TH>
  <TH width="20%"><%=LC.L_Fld_Id%><%--Field ID*****--%></TH>
    
  <TH width="10%"><%=LC.L_Fld_Seq%><%--Field Seq*****--%>.</TH>
  
  </tr>
  <%for(int i=0;i<len;i++){
  	  		
  	  		useUniqueFldName = "fldUseUnique" + i;
    		fldCheck="N";
			fldWidth="";
			fldId="0";
    		fldCol = (String)mapCols.get(i);
    		fldCol = fldCol.trim();
			index=sessCol.indexOf(fldCol);
			
			secId=(String)mapSecIds.get(i);
			secName=(String)mapSecNames.get(i);
			fldUniqueId = (String)mapUids.get(i);
			
			if ( selForm.startsWith("C")) // keywords for core data only
			{
				fldKey = (String) mapKeywords.get(i);
			}	
			
						
		if (index>=0){
			fldType=(String)sessColType.get(index);
			fldName=(String)sessColName.get(index);
			fldDisp=(String)sessColDisp.get(index);
			
			seq=EJBUtil.stringToNum((String)sessColSeq.get(index));
			//commented to regenerate sequence each time, this is to handle form offline feature 
			//Sonia Abrol, 09/19/06 - above code was commented and sequence was regenerated each time. uncommented to allow sequencing
			//seq=seq+10;
			
			fldWidth=(String)sessColWidth.get(index);
			fldCheck="Y";
			fldId=(String)sessColId.get(index);
			fldColUseUniqueId = (String) sessColUseUniqueId.get(index);
		    fldColUseDataVal = (String) sessColUseDataVal.get(index);
		    fldCalcSumVal = (String) sessCalcSum.get(index);
			fldCalcPersVal = (String) sessCalcPer.get(index);
			//fldRepeatCount = (String) sessColRepeatCount.get(index);
		  
		    fldRepeatCount = (String) sessColRepeatCount.get(index);
		
		}else{
			fldType=(String)mapColTypesNoRepeat.get(i); 
			//out.println("fldType" + fldType + "*" + i);
			fldName=(String)mapDispNames.get(i);
			fldRepeatCount = (String) mapColRepeatCounts.get(i);
			fldDisp=fldName;
			seq=seq+10;
					
			fldColUseUniqueId = repUseUnique;
			fldColUseDataVal = repUseDataVal;
			
			fldCalcSumVal = "0";
			fldCalcPersVal= "0";
			
		}
		
		  /* if 	(prevSeq == seq)
		    {
		    	 seq = seq + 10;
		    }
		    prevSeq = seq; */
		
		if (fldColUseUniqueId.equals("1"))
		{
			fldCheckDisplay = "";
			fldCheckUnique = "CHECKED";	
		}else
		{
			fldCheckDisplay = "CHECKED";
			fldCheckUnique = "";		
		}
		
		if (fldColUseDataVal.equals("1"))
		{
			fldCheckUseDataVal = "CHECKED";
		}
		else
		{
			fldCheckUseDataVal = "";
		}
		
			if (fldCalcSumVal.equals("1"))
		{
			fldCalcSumCheck = "CHECKED"; 
		}
		else
		{
			fldCalcSumCheck = "";
		}


		if (fldCalcPersVal.equals("1"))
		{
			fldCalcPersCheck = "CHECKED";
		}
		else
		{
			fldCalcPersCheck = "";
		}
			
		if (!secId.equals(prevSecId))
		{
		%>
		<tr>
		<td colspan=4><font size=2><b><%=secName%></b></font></td>
		</tr>
		<%} else { // changed this part to have alternate rows 09-02-2012
			if ((i%2)==0) {
						%>
							<tr class="browserOddRow">
				        <%
						}
						else{
						%>
						    <tr class="browserEvenRow">
				        <%
						}
		}%>
<td width="5"><p align="center">
<input type="checkbox" name="fldSelect" <%if (fldCheck.equals("Y")){%>checked<%}%>></p>
</td>

<td width="25%"><input type="radio" name="<%=useUniqueFldName%>" value="0" onClick="checkSingleUseUnique(document.dynreport,<%=i%>,'uncheck')" <%=fldCheckDisplay%>> <%=StringUtil.decodeString(fldName)%>
	<% if (fldType.equals("MD") || fldType.equals("MC") || fldType.equals("MR")) {
			multipleChoiceCount = multipleChoiceCount+1;
	%>
		<P class="defComments"><input type="checkbox" name="fldUseDataValue" value="1" <%=fldCheckUseDataVal%>> <%=MC.M_Disp_RespDataValue%><%--Display Response Data Valuet*****--%></P>
		
		
	<% 	} else { %>
			<input type="hidden" name="fldUseDataValue" value="0">
			<% }%>	
			
			
	<% if ( (  fldType.equals("MD") || fldType.equals("MC") || fldType.equals("MR") || fldType.equals("EN") )  && EJBUtil.stringToNum(secId) > 0  ) 
		{
		 %>
			<P class="defComments"><input type="checkbox" name="fldCalcPer" value="1" <%= fldCalcPersCheck%> > <%=MC.M_Calcu_CountAndPerc%><%--Calculate Count and Percentage*****--%>
			
			<% if (fldType.equals("EN") ) 
			{
		 	%>
				 <br><input type="checkbox" name="fldCalcSum" value="1" <%= fldCalcSumCheck%>> <%=LC.L_Calcu_BasicStats%><%--Calculate Basic Stats*****--%>
		 <%
			} else
			{
			%>
			 <input type="hidden" name="fldCalcSum" value="0">
			<%
				}
			%>
			
			</P>
		 <%
		 }
		else
		{
		 %>
			<input type="hidden" name="fldCalcPer" value="0">
			 <input type="hidden" name="fldCalcSum" value="0">	
				
		 <%
		
		}
		 %>
		 		
	</td>
    <td width="35%"><input type="text" name="fldDispName" value="<%=StringUtil.decodeString(fldDisp)%>" size="35"></td>
    <td width="20%"><input type="radio" name="<%=useUniqueFldName%>" value="1" onClick="checkSingleUseUnique(document.dynreport,<%=i%>,'check')" <%=fldCheckUnique%>><%=fldUniqueId%></td>
      <input type="hidden" name="fldWidth" size="2" value="<%=fldWidth%>">
     <td><input type="text" name="fldSeq" value="<%=seq%>" size=1 maxlength=5 onChange="setSeqFlag(document.dynreport)" onBlur="checkValue(document.dynreport.fldSeq[<%=i%>])">	</td>
     <td width="10%"><input type="hidden" name="fldCol" value="<%=fldCol%>"></td>
     <input type="hidden" name="fldName" value="<%=StringUtil.decodeString(fldName)%>">
     <input type="hidden" name="fldType" value="<%=fldType%>">
     <input type="hidden" name="fldFormat" value="N">
     <input type="hidden" name="fldId" value="<%=fldId%>">
     <input type="hidden" name="fldUniqueId" value="<%=fldUniqueId%>">
     <input type="hidden" name="sectionId" value="<%=secId%>">
      <input type="hidden" name="fldRepeatCount" value="<%=fldRepeatCount%>">
      <input type="hidden" name="fldKey" value="<%=fldKey%>">
        
    </tr>
    

    
   <%prevSecId=secId;
   }//end for loop
    %>    
    <input name="multipleChoiceCount" type="hidden" value="<%=multipleChoiceCount%>">	
    <input name="repFilter" type="hidden" size="100" value="<%=repFilter%>">
    </table>
   <br>
    <!--<table width="100%" cellspacing="0" cellpadding="0">
       <td width="70%"></td><td> 
		<input type="image" src="../images/jpg/Next.gif" align="absmiddle"  border="0" onClick ="return validate(document.dynreport,'mdynfilter.jsp');">
		<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>
      </td> 
      </tr>
  
</table> -->

</DIV>
	 
<input type="hidden" name="sess" value="keep">
</form>
<%}// end for if attributes==null
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
  </div>


</body>
</html>
