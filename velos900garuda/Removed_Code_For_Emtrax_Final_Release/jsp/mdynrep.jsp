<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title> <%=MC.M_AdhocQry_PreviewSave%><%--Ad-Hoc Query >> Preview and Save*****--%> </title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.web.dynrep.holder.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>
window.name = "dynrep";
var formWin = null;
 function  validate(formobj){
 	  //sonika

		 if((formobj.rbSharedWith[2].checked == true) && (formobj.selGrpNames.value==""))
		 {
		 alert("<%=MC.M_PlsSelectGrp%>");/*alert("Please Select Group");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[3].checked == true) && (formobj.selStudy.value==""))
		 {
		 alert("<%=MC.M_PlsSelectStd%>");/*alert("Please Select <%=LC.Std_Study%>");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[4].checked == true) && (formobj.selOrg.value==""))
		 {
		 alert("<%=MC.M_PlsSelectOrg%>");/*alert("Please Select Organization");*****/
		 return false;
		 }
 
 //end sonika
 
// Added by Gopu to fix the bugzilla Issue #2719 (Trim leading and trailing spaces from Report Template Name while saving)

formobj.repName.value = fnTrimSpaces(formobj.repName.value);

 repName=fnTrimSpaces(formobj.repName.value);
 if (repName.length==0){
 alert("<%=MC.M_Etr_RptTemplateName%>");/*alert("Please enter report template name");*****/
 formobj.repName.focus();
 return false;
 }
 
 if (!(validateDataSize(formobj.repDesc,500,'Report Description'))) return false ;
	
     if (!(validate_col('eSign',formobj.eSign))) return false
     if (!(validateDataSize(formobj.repHeader,4000,'Report Header'))) 
     { 
     	
     	return false;
     }
     
      
	if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/

	formobj.eSign.focus();
	
	return false;
   }
formobj.action="updatemdynrep.jsp";
formobj.target="";
}
function openFormWin(formobj,act) {

formobj.target="formWin";
formobj.action=act;
formWin = open('donotdelete.html','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynrep.submit();
void(0);
}

function setAlignment(formobj,param)
{
if (formobj.repFooter.value.length>0)
{
  if (param=="CF") formobj.footerAlign.value="[VELCENTER]";
  if (param=="LF") formobj.footerAlign.value="[VELLEFT]";
  if (param=="RF") formobj.footerAlign.value="[VELRIGHT]";
  }
  if (formobj.repHeader.value.length>0) 
  {
  if (param=="CH") formobj.headerAlign.value="[VELCENTER]";
  if (param=="LH") formobj.headerAlign.value="[VELLEFT]";
  if (param=="RH") formobj.headerAlign.value="[VELRIGHT]";
  }
}

function openLookup(formobj){
accId=formobj.accId.value;
userId=formobj.userId.value;
studyId=formobj.study1.value;
studyflg=formobj.studyselect[0].checked;
if (studyflg){
	studyList=formobj.studyList.value;	
   filter="mdynrep|"+studyList;
}
else{
if (studyId.length==0){
alert("<%=MC.M_PlsSel_StdFirst%>");/*alert("Please Select a <%=LC.Std_Study%> First");*****/
return;
 }
 filter="mdynrep|"+studyId;
}
 
 
   
windowname=window.open("getlookup.jsp?viewId=&viewName=Form Browser&form=dynreport&dfilter=" + filter + "&keyword=formName|VELFORMNAME~formId|VELFORMPK" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, "); 
windowname.focus();	
}
function selectFlds(formobj){
formId=formobj.formId.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
from=formobj.from.value;
studyId=formobj.study1.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter+"&from="+from+"&study1="+studyId ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, "); 
windowname.focus();
}
function setSeqFlag(formobj){
formobj.seqChg.value="Y";

}





</SCRIPT>
<% String src,tempStr="",fldCol="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
System.out.println(src);

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   

<body>
<DIV id="div1"> 
<%
        String formId="",formName="",formType="",fldType="",repIdStr="",fldDisp="",fldWidth="",fldId="";
	String repName="",repHdr="",repFtr="",repType="",repFilter="",selStudy="",repDesc="",dynType="",shareWith="",headAlign="",footAlign="";
	String[] fldName,fldOrder;
	int seq=0,repId=0;
	String mode = request.getParameter("mode");
	mode=(mode==null)?"":mode;
	mode=(mode.equals("link"))?"M":mode;
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	ArrayList fldNameList=new ArrayList(),
	          fldOrderList=new ArrayList();
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	  int pageRight = 0;
  	  GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
      pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ADHOC"));  

		
	 HashMap attributes=new HashMap();
	 HashMap SortAttr=new HashMap();
	 HashMap TypeAttr=new HashMap();
	 HashMap FltrAttr=new HashMap();
	 HashMap RepAttr=new HashMap();
	 
	// attributes=(HashMap)tSession.getAttribute("attributes");
	ReportHolder container=(ReportHolder) tSession.getAttribute("reportHolder");
	 	if (container==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">
	
	<%}else {
	 
	 String selForm=request.getParameter("selForm");
	 selForm=(selForm==null)?"":selForm;
	 
	  
	 fldName=request.getParameterValues("fldNameOrder");
	 if (fldName!=null)
	    fldNameList=EJBUtil.strArrToArrayList(fldName); 
	 
	  fldOrder=request.getParameterValues("fldOrder");
	  if (fldOrder!=null)
	  fldOrderList = EJBUtil.strArrToArrayList(fldOrder);
	  
	 String dataOrder=request.getParameter("dataOrder");
	 dataOrder=(dataOrder==null)?"":dataOrder;
	 if (dataOrder.length()==0) if (SortAttr.containsKey("dataOrder")) dataOrder=(String)SortAttr.get("dataOrder");
	 dataOrder=(dataOrder==null)?"":dataOrder;
	 
	 String order=request.getParameter("order");
	 order=(order==null)?"":order;
	 if ((order.length()==0) && (dataOrder.length()>0)) {
	 order =" Order By " + StringUtil.replaceAll(dataOrder,"[$]"," ");
	 order=StringUtil.replaceAll(order,"[$$]",",");
	 System.out.println("order***"+order+"***dataOrder***"+dataOrder);
	 }
	 
	  
	 SortContainer sortContainer=container.getSortObject(selForm);
	 if ((fldNameList.size()>0) && (sortContainer==null))
	    {
	     sortContainer=container.newSortObject(selForm);
	    }
	    
	  if  (sortContainer!=null)
	  {
	    sortContainer.setSortFields(fldNameList);
	    sortContainer.setSortOrderBy(fldOrderList);
	    sortContainer.setSortStr(dataOrder);
	  }
	  
	  tSession.setAttribute("reportHolder",container);
	  
	  container.Display();
	 
	 shareWith=container.getShareWith();
	 shareWith=(shareWith==null)?"":shareWith;
	 
	 	 
	 repHdr=container.getReportHeader();
	 repHdr=(repHdr==null)?"":repHdr;
	 
	 if ((repHdr.indexOf("[VELCENTER]")>=0)||(repHdr.indexOf("[VELLEFT]")>=0)||(repHdr.indexOf("[VELRIGHT]")>=0))
	 {
	   headAlign=repHdr.substring(0,(repHdr.indexOf(":",0)));
	   repHdr=repHdr.substring(headAlign.length()+1);
	   }
	   else headAlign="[VELCENTER]"; 
	   
	 repFtr=container.getReportFooter();
	 repFtr=(repFtr==null)?"":repFtr;
	 if ((repFtr.indexOf("[VELCENTER]")>=0)||(repFtr.indexOf("[VELLEFT]")>=0)||(repFtr.indexOf("[VELRIGHT]")>=0))
	  {
	  footAlign=repFtr.substring(0,(repFtr.indexOf(":",0)));
	  repFtr=repFtr.substring(footAlign.length()+1);
	 }
	 else footAlign="[VELCENTER]";
	 
	 repName=container.getReportName();
	 repName=(repName==null)?"":repName;
	 
	 repDesc=container.getReportDesc();
	 repDesc=(repDesc==null)?"":repDesc;
	 
	 repId=EJBUtil.stringToNum(container.getReportId());
	 
	 formId= request.getParameter("formId");
	 if (formId==null) formId="";
	 
	 if (formId.length()==0){
	  formId=container.getFormIdStr();
	 }
		
	 formName=container.getFormNameStr();
	  formName = StringUtil.replace(formName,"{VELSEP}",";");
	dynType=container.getReportType();
	
	if (dynType==null) dynType="";
	
	 String accId = (String) tSession.getValue("accountId");
	 String userId=	(String) tSession.getValue("userId");
	 String userIdFromSession = (String) tSession.getValue("userId");
	
	  int counter=0;
	 
	
%>

<%

%>

<form name="dynrep"  METHOD=POST id="mdynrepfrm"  action="updatemdynrep.jsp" onsubmit="if (validate(document.dynrep)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	
<div class="tabDefTopN" id="divTab">


<table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">

<tr bgcolor="#dcdcdc" align="center">

<%if (container.getFormIdStr()!=null){%>
<td><A href="mdynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><%=LC.L_Select_RptType%><%--Select Report Type*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getFldObjects()).size()>0){%>
<td><A href="mdynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields******--%></A></td>

<%}else{%>
<td><%=LC.L_Select_Flds%><%--Select Fields******--%></td>
<%}%>
<td>>></td>
<%if ((container.getFilterObjects()).size()>0){%>
<td><A href="mdynfilter.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>

<%}else{%>
<td><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></td>
<%}%>
<td>>></td>
<%if ((container.getSortObjects()).size()>0){%>
<td><A href="mdynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>

<%}else{%>
<td><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></td>
<%}%>
<td>>></td>
<%if (container.getReportName().length()>0){%>
<td><A href="mdynrep.jsp?mode=link&srcmenu=<%=src%>"><font color="red"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></font></A></td>

<%}else{%>
<td><font color="red"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></font></td>
<%}%>
	
	<td width="25%" align="right">
	<A type="submit" href="#" onClick="openFormWin(document.dynrep,'mdynpreview.jsp');"><%=LC.L_Display%></A>
	</td>
		
</tr>

</table>
</div>


<DIV class="tabDefBotN" id="div1">
<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formId" readonly value="<%=formId%>">
<input type="hidden" name="formType" readonly value="<%=repType%>">
<input type="hidden" name="from" readonly value="study">
<input type="hidden" name="repId" readonly value="<%=repId%>">
<input name="order" type="hidden" size="100" value="<%=order%>">
<input name="dataOrder" type="hidden" size="100" value="<%=dataOrder%>">

<table width="98%">
<!-- <P class="sectionHeadings">Selected Form: <%=formName%> </P> -->
   <td ><input type="hidden" name="headerAlign" value="<%=headAlign%>" size= "50" maxlength="100"></td>
   <td ><input type="hidden" name="footerAlign" value="<%=footAlign%>"  size="50" maxlength="100"></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
   
     <tr>
	<td width="20%"><%=LC.L_Rpt_Header%><%--Report Header*****--%></td>
     <td width="25%"><textarea type="text" name="repHeader" value="<%=repHdr%>" rows="3" cols="50"><%=repHdr%></textArea></td>
     <td width="10%">
     <table>
     <tr><td><input name="alignHeader" type="radio" <% if (headAlign.equals("[VELCENTER]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'CH')"><%=LC.L_Center%><%--Center*****--%></td></tr>
     <tr><td><input name="alignHeader" type="radio" <% if (headAlign.equals("[VELLEFT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'LH')"><%=LC.L_Left%><%--Left*****--%></td></tr>
     <tr><td><input name="alignHeader" type="radio" <% if (headAlign.equals("[VELRIGHT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'RH')"><%=LC.L_Right%><%--Right*****--%></td></tr>
     </table>
     </td>
	<td width="45%">
<!--	<A href="#" onClick="openFormWin(document.dynrep,'mdynpreview.jsp');"><img src="../images/jpg/displayreport.jpg" align="absmiddle" border="0"></A> -->
	</td>
	
     
     </tr>
    
	<tr><td ><%=LC.L_Rpt_Footer%><%--Report Footer*****--%></td>
     
     <td width="25%"><textarea type="text" name="repFooter" value="<%=repFtr%>" rows="3" cols="50"><%=repFtr%></textArea></td>
     <td width="10%">
     <table>
     <tr><td><input name="alignFooter" type="radio" checked <% if (footAlign.equals("[VELCENTER]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'CF')"><%=LC.L_Center%><%--Center*****--%></td></tr>
     <tr><td><input name="alignFooter" type="radio" <% if (footAlign.equals("[VELLEFT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'LF')"><%=LC.L_Left%><%--Left*****--%></td></tr>
     <tr><td><input name="alignFooter" type="radio"  <% if (footAlign.equals("[VELRIGHT]")){ %>checked <%}%> onClick="setAlignment(document.dynrep,'RF')"><%=LC.L_Right%><%--Right*****--%></td></tr>
     </table>
     </td>
	<td>&nbsp;
	<p class= "defComments"> <%=MC.M_FldSeqApp_SvgRepeatSec%><%--Note: Field sequence changes are applicable only after saving the report and is not applied to fields in 'Repeating Sections'*****--%>
      </p>
	</td>
     </tr>

     <!--<td><A href="#" onClick="openFormWin(document.dynrep,'mdynpreview.jsp');"><img src="../images/jpg/displayreport.jpg" align="absmiddle" border="0"></A>
</td> -->
	 
<tr>
<td width="15%"><%=LC.L_Save_RptAs%><%--Save Report As*****--%> <FONT class="Mandatory">* </FONT> </td>
<td><input type="text" maxlength=100 size="67" name="repName" value="<%=repName%>"></td>
</tr>
<tr>
<td width="15%"> <%=LC.L_Rpt_Desc%><%--Report Description*****--%> </td>
<td><input type="text" size="67" name="repDesc" value="<%=repDesc%>" ></textArea></td>
</tr>
     	<!--sonika-->
<%	
	String repMode = "";	
	//if (TypeAttr.containsKey("repMode")) repMode = (String)TypeAttr.get("repMode");
	
	if ( repId > 0)
	{
		repMode="M";
	}
	else
	{
		repMode = "N";
	}	
	
%>

   	 <jsp:include page="objectsharewith.jsp" flush="true">
   	 <jsp:param name="objNumber" value="2"/>
   	 <jsp:param name="mode" value="<%=repMode%>"/>
   	 <jsp:param name="formobject" value="document.dynrep"/>
   	 <jsp:param name="formnamevalue" value="dynrep"/>
   	 <jsp:param name="fkObj" value="<%=repId%>"/>	 		  	
   	 <jsp:param name="sharedWith" value="<%=shareWith%>"/>	 		  	 		 	 	 	 
	 </jsp:include>
	<!--end sonika-->    
    
    <tr><td>&nbsp;</td></tr>
     </table>
    

	<% String showSubmit="";
	if ((repMode.equals("N") && (pageRight == 5 || pageRight == 7) ) ||  ( repMode.equals("M") && pageRight >=6 ) ) 
		{ showSubmit ="Y";} 
		else {showSubmit="N";}
	%>

	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="mdynrepfrm"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
	</jsp:include>
</DIV>
<input type="hidden" name="sess" value="keep">
</form>

<%
}// end for (attributes==null)
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
  </div>


</body>
</html>
