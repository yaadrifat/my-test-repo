<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Dynamic_RptCreation%><%--Dynamic Report Creation*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>
window.name = "dynrep";
var formWin = null;
function  validate(formobj,act){
totcount=formobj.rows.value;
if (totcount==1){
if (!formobj.fldSelect.checked){
alert("<%=MC.M_PlsSelFld_ToProc%>")/*alert("Please select a field to proceed")*****/
return false;
}
makeStr(formobj,1)
} else if (totcount>1){
checkFlg=false; //flag to track if any field is selected
for(j=0;j<totcount;j++){
if (!formobj.fldSelect[j].checked) {continue;}
else {
checkFlg=true;
makeStr(formobj,j);
}
}//end for
if (!checkFlg){
	alert("<%=MC.M_PlsSelFld_ToProc%>")/*alert("Please select a field to proceed")*****/
return false;
}
} else {
alert("<%=MC.M_NoFldSel_SelcAnotherFrm%>")/*alert("No fields in selected form. Please select another form or add fields to form to proceed.")*****/
return false;
}

formobj.action="dynfilter.jsp";

}
function openFormWin(formobj,act) {
formId=formobj.formId.value;
prevId=formobj.prevId.value;
//act="dynpreview.jsp";

if (formId==prevId)
{}
else
{
alert("<%=MC.M_SelFlds_ForFrm%>");/*alert("Please Select the Fields for Selected Form.");*****/
return false;
}
formobj.target="formWin";
formobj.action=act;
formWin = open('','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynreport.submit();
void(0);
}


function selectFlds(formobj){
formId=formobj.formId.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
from=formobj.from.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter+"&from="+from ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, "); 
windowname.focus();
}
function setSeqFlag(formobj){
formobj.seqChg.value="Y";

}
function openpatwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=&form=dynreport&dispFld=patId&dataFld=patPk","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
   windowName.focus();
}

function makeStr(formobj,count){
var totcount=formobj.rows.value;
if (totcount>1){
width=formobj.fldWidth[count].value;
if (width.length==0) width="0";
formobj.fldSelect[count].value=encodeString(formobj.fldSeq[count].value+"[$]"+formobj.fldName[count].value+"[$]"+formobj.fldDispName[count].value+"[$]" +width+"[$]"+formobj.fldCol[count].value + "[$]"+formobj.fldType[count].value+ "[$]"+formobj.fldFormat[count].value+ "[$]"+formobj.fldId[count].value);
} else{
width=formobj.fldWidth.value;
if (width.length==0) width="0";
dispName=encodeString(formobj.fldDispName.value)
formobj.fldSelect.value=encodeString(formobj.fldSeq.value+"[$]"+formobj.fldName.value+"[$]"+dispName+"[$]" +width+"[$]"+formobj.fldCol.value + "[$]"+formobj.fldType.value + "[$]"+formobj.fldFormat.value +"[$]"+formobj.fldId.value);
}
}
function checkAll(formobj){
 	act="check";
 	totcount=formobj.rows.value;
  if (formobj.All.value=="checked") act="uncheck" ;
   if (totcount==1){
   if (act=="uncheck") formobj.fldSelect.checked =false ;
   else formobj.fldSelect.checked =true ;}
   else if (totcount>1) {
     for (i=0;i<totcount;i++){
     if (act=="uncheck") formobj.fldSelect[i].checked=false;
     else formobj.fldSelect[i].checked=true;
     }
    }else{}
       
    if (act=="uncheck") formobj.All.value="unchecked"; 
    else formobj.All.value="checked"; 
    
    
 }
</SCRIPT>
<% String src,tempStr="",fldName="",fldCol="",prevId="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");


%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   

<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>
<DIV class="formDefault" id="div1"> 
<%
        String formId="",formName="",formType="",fldType="",repIdStr="",fldDisp="",fldWidth="",fldId="0",repMode="",fldSec="",fldSecId="";
	String repName="",repHdr="",repFtr="",repType="",repFilter="",selStudy="",fldSelect="",studyId="",patId="",dynType="",fldCheck="",patselect="",studyselect="";
	int seq=0,repId=0,len=0,index=-1;
	ArrayList mapCols=new ArrayList();
	ArrayList mapOrigIds= new ArrayList(); 
	ArrayList mapDispNames=new ArrayList();
	ArrayList mapFormTypes=new ArrayList();
	ArrayList mapColTypes=new ArrayList();
	ArrayList mapSecNames=new ArrayList();
	ArrayList mapSecIds=new ArrayList();
	ArrayList repColId=new ArrayList();
	ArrayList repCol=new ArrayList();
	ArrayList repColSeq=new ArrayList();
	ArrayList repColWidth=new ArrayList();
	ArrayList repColFormat=new ArrayList();
	ArrayList repColDispName=new ArrayList();
	ArrayList repColName=new ArrayList();
	ArrayList repColType=new ArrayList();
	ArrayList sessCol=new ArrayList();
	ArrayList sessColId=new ArrayList();
	ArrayList sessColName=new ArrayList();
	ArrayList sessColDisp=new ArrayList();
	ArrayList sessColSeq=new ArrayList();
	ArrayList sessColType=new ArrayList();
	ArrayList sessColStr=new ArrayList();
	ArrayList sessColWidth=new ArrayList();
	ArrayList sessColSec=new ArrayList();
	ArrayList sessColSecIds=new ArrayList();
	
	String mode = request.getParameter("mode");
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	HashMap attributes=new HashMap();
	HashMap TypeAttr=new HashMap();
	HashMap FldAttr=new HashMap();
	attributes=(HashMap)tSession.getAttribute("attributes");
	if (attributes==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">
	
	<%}else {
	
	if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	selStudy=request.getParameter("study1");
	if (selStudy==null) selStudy="";
	 dynType=request.getParameter("dynType");
	 dynType=(dynType==null)?"":dynType;
	if (dynType.length()==0){
	 if (TypeAttr.containsKey("dynType")) dynType= (String)TypeAttr.get("dynType");
	if (dynType==null) dynType="";
	}
	
	 //form
	 formId=request.getParameter("formId");
	 prevId=request.getParameter("prevId");
	 if (prevId==null) prevId="";
	 if (formId==null) formId="";
	if (formId.length()==0){
	 if (TypeAttr.containsKey("formId")) formId= (String)TypeAttr.get("formId");
	if (formId==null) formId="";
	} else {
	if (prevId.length()==0){
	 if (TypeAttr.containsKey("prevId")) prevId= (String)TypeAttr.get("formId");
	 }
	 if (prevId==null) prevId="";
	 if (!formId.equals(prevId)) {//means used changed the form
	 if (attributes.containsKey("FldAttr")) attributes.remove("FldAttr");
	 }
	}	
	 //study
	 studyId=request.getParameter("study1");
	 if (studyId==null) studyId="";
	if (studyId.length()==0) {
	 if (TypeAttr.containsKey("studyId")) studyId= (String)TypeAttr.get("studyId");
	if (studyId==null) studyId="";
	}
	 //Patient
	 patId=request.getParameter("patPk");
	 if (patId==null) patId="";
	 if (patId.length()==0){
	if (TypeAttr.containsKey("patId")) patId=(String)TypeAttr.get("patId");
	if (patId==null) patId="";
	}
	 TypeAttr.put("dynType",dynType);
	 if (dynType.equals("P")){
	 patselect=request.getParameter("patselect");
	 patselect=(patselect==null)?"":patselect;
	 if (patselect.equals("1"))
	 {
	 if (studyId.length()>0) TypeAttr.put("studyId",studyId);
	 }
	 else
	 {
	 if (TypeAttr.containsKey("studyId")) TypeAttr.remove("studyId");
	 }
	 if (patselect.equals("2")){
	 if (patId.length()>0) TypeAttr.put("perId",patId);
	 }
	 else 
	 {
	 if (TypeAttr.containsKey("perId")) TypeAttr.remove("perId");
	 }
	}
	if (dynType.equals("S")){
	studyselect=request.getParameter("studyselect");
	studyselect=(studyselect==null)?"":studyselect;
	if (studyselect.equals("2")){
	 if (studyId.length()>0) TypeAttr.put("studyId",studyId);
	}
	}
	 
	 
	 TypeAttr.put("formId",formId);
	 //prevId stores the previous form selcted if user plans to change the form again dring creatio,then we will remove sesion variable
	 if (prevId.length()==0)  TypeAttr.put("prevId",prevId);
	 
	 formName=request.getParameter("formName");
	 if( formName==null) formName="";
	 if (mode.equals("next")) TypeAttr.put("formName",formName);
	 
	  attributes.put("TypeAttr",TypeAttr);
	 tSession.setAttribute("attributes",attributes);
	 
	 formType=request.getParameter("formType");
	 formType=(formType==null)?"":formType;
	 formName=(String)TypeAttr.get("formName");
	 if( formName==null) formName="";
	 String accId = (String) tSession.getValue("accountId");
	 String userId=	(String) tSession.getValue("userId");
	 String userIdFromSession = (String) tSession.getValue("userId");
	 
	 //formId=(String)TypeAttr.get("formId");
	// if( formId==null) formId="";
	
	DynRepDao dyndao=dynrepB.getMapData(formId);
	//dyndao.getMapData(formId);
	dyndao.preProcessMap();
	mapCols= dyndao.getMapCols();
	mapOrigIds= dyndao.getMapOrigIds();
	mapDispNames= dyndao.getMapDispNames();
	mapFormTypes=dyndao.getMapFormTypes();
	mapColTypes=dyndao.getMapColTypes();
	mapSecNames=dyndao.getMapColSecNames();
	mapSecIds=dyndao.getMapColSecIds();
	System.out.println("mapSecNames " + mapSecNames + " mapSecIds " +mapSecIds );
	if (mapFormTypes.size()>0)
	formType= (String)mapFormTypes.get(0);
	len=mapOrigIds.size();
	 int counter=0;
	 //attributes=(HashMap)tSession.getAttribute("attributes");
	 if (attributes.containsKey("FldAttr")) FldAttr=(HashMap)attributes.get("FldAttr");
	if (attributes.containsKey("FldAttr")){
	  if (FldAttr.containsKey("sessCol")) {sessCol=(ArrayList)FldAttr.get("sessCol");
	  
	  }
	 if (FldAttr.containsKey("sessColName")) sessColName=(ArrayList)FldAttr.get("sessColName");
	  if (FldAttr.containsKey("sessColDisp")) sessColDisp=(ArrayList)FldAttr.get("sessColDisp");
	 if (FldAttr.containsKey("sessColSeq")) sessColSeq=(ArrayList)FldAttr.get("sessColSeq");
	 if (FldAttr.containsKey("sessColType")) sessColType=(ArrayList)FldAttr.get("sessColType");
	if (FldAttr.containsKey("sessColStr")) sessColStr=(ArrayList)FldAttr.get("sessColStr");
	if (FldAttr.containsKey("sessColWidth")) sessColWidth=(ArrayList)FldAttr.get("sessColWidth");
	if (FldAttr.containsKey("sessColId")) sessColId=(ArrayList)FldAttr.get("sessColId");
	
	 }
	
	
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FldAttr")) System.out.println("I am here-FldAttr"+attributes.get("FldAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("TypeAttr")) System.out.println("I am here-TypeAttr"+attributes.get("TypeAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FltrAttr")) System.out.println("I am here-FltrAttr"+attributes.get("FltrAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("SortAttr")) System.out.println("I am here-SortAttr"+attributes.get("SortAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("RepAttr")) System.out.println("I am here-RepAttr"+attributes.get("RepAttr"));
	System.out.println("*****************************************************************************************");


	
%>

<P class="sectionHeadings"> <%=LC.L_Adhoc_FldSec%><%--Ad-Hoc Query >> Field Selection*****--%> </P>

<form name="dynreport"  METHOD=POST>
<table><tr>
<%if (attributes.get("TypeAttr")!=null){%>
<td><A href="dynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FldAttr")!=null){%>

<td><A href="dynselectflds.jsp?mode=link&srcmenu=<%=src%>"><font color="red"><b><%=LC.L_Select_Flds%><%--Select Fields*****--%></b></font></A></td>

<%}else{%>
<td><p class="sectionHeadings"><font color="red"><b><%=LC.L_Select_Flds%><%--Select Fields*****--%></b></font></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FltrAttr")!=null){%>
<td><A href="dynfilter.jsp?srcmenu=<%=src%>"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("SortAttr")!=null){%>
<td><A href="dynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("RepAttr")!=null){%>
<td><A href="dynrep.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></p></td>
<%}%>
</tr></table>
<input type=hidden name="mode" value="next">
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formId" readonly value="<%=formId%>">

<%if (mode.equals("M")){%>
<input type="hidden" name="formType" readonly value="<%=repType%>">
<%}else{%>
<input type="hidden" name="formType" readonly value="<%=formType%>">
<%}%>
<input type="hidden" name="prevId" readonly value="<%=formId%>">
<input type="hidden" name="seqChg" readonly value="">
<input type="hidden" name="from" readonly value="pat">
<input type="hidden" name="repId" readonly value="<%=repId%>">
<input name="rows" type="hidden" value="<%=len%>">

   <br>
   <!--<table width="100%">
    <tr><td width="15%">Selected Form</td><td width="50%"><input type="text" name="formName" readonly value="<%=formName%>"></td></tr>
    </table>-->
    <P class="sectionHeadings"><%=LC.L_Selected_Form%><%--Selected Form*****--%>: <%=formName%> </P>
    <P class="sectionHeadings"> <%=MC.M_SelFld_InRptOutput%><%--Please select fields to include in report output*****--%>:</P>

<table width="100%" Border="1">
  <tr>
  <TH width="10%"><input type="checkbox" name="All" value="" onClick="checkAll(document.dynreport)"><%=LC.L_Select%><%--Select*****--%></TH>
<!--<TH width="10%">Sequence</TH>-->
  <TH width="25%"><%=LC.L_Fields%><%--Fields*****--%></TH>
  <TH width="35%"><%=LC.L_Display_Name%><%--Display Name*****--%></TH>
    <TH width="5%"><%=LC.L_Width%><%--Width*****--%></TH>
     </tr>
    <%for(int i=0;i<len;i++){
    		fldCheck="N";
		fldWidth="";
		fldId="0";
    		fldCol = (String)mapCols.get(i);
		index=sessCol.indexOf(fldCol);
		System.out.println("Index@" + index+"SessCol" + sessColName + "SessName" + sessCol);
		if (index>=0){
		fldType=(String)sessColType.get(index);
		fldName=(String)sessColName.get(index);
		fldDisp=(String)sessColDisp.get(index);
		
		seq=EJBUtil.stringToNum((String)sessColSeq.get(index));
		fldWidth=(String)sessColWidth.get(index);
		fldCheck="Y";
		fldId=(String)sessColId.get(index);
		}else{
		fldType=(String)mapColTypes.get(i);
		fldName=(String)mapDispNames.get(i);
		fldDisp=fldName;
		seq=seq+10;
		}%>
		<tr>
<td width="10%"><p align="center">
<input type="checkbox" name="fldSelect" <%if (fldCheck.equals("Y")){%>checked<%}%>></p>
</td>
<!--<td width="10%"><p align="Center"><input type="text" name="fldSeq" value="<%=seq%>" size=1 maxlength=5 onChange="setSeqFlag(document.dynreport)"></p></td>-->
<td width="25%"><%=StringUtil.decodeString(fldName)%></td>
    <td width="35%"><input type="text" name="fldDispName" value="<%=StringUtil.decodeString(fldDisp)%>" size="35"></td>
      <td width="5%"><input type="text" name="fldWidth" size="2" value="<%=fldWidth%>">%</td>
     <td width="60%"><input type="hidden" name="fldCol" value=<%=fldCol%>></td>
     <input type="hidden" name="fldName" value="<%=StringUtil.decodeString(fldName)%>">
     <input type="hidden" name="fldType" value="<%=fldType%>">
     <input type="hidden" name="fldFormat" value="N">
     <input type="hidden" name="fldId" value="<%=fldId%>">
      </tr>
    <input type="hidden" name="fldSeq" value="<%=seq%>" size=1 maxlength=5 onChange="setSeqFlag(document.dynreport)">	
   <%}//end for loop
    %>    
    <input name="repFilter" type="hidden" size="100" value="<%=repFilter%>">
    </table>
   <br>
    <table width="100%" cellspacing="0" cellpadding="0">
       <td width="70%"></td><td> 
		<button type="submit" onClick ="return validate(document.dynreport,'dynfilter.jsp');"><%=LC.L_Next %></button>
		<!--<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>-->
      </td> 
      </tr>
  </table>
   <input type="hidden" name="sess" value="keep">
</form>

<%
}// end for if attributes==null
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
  </div>


</body>
</html>
