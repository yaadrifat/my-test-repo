<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Fld_MultiChoiceSubmit%><%--Field Multiple Choice Submit*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<jsp:useBean id="fieldRespB" scope="request" class="com.velos.eres.web.fldResp.FldRespJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		String eSign= request.getParameter("eSign");
    	String oldESign = (String) tSession.getValue("eSign");
	   if(oldESign.equals(eSign)) 
	   {
			int errorCode=0;
			String mode=request.getParameter("mode");
			String refresh=request.getParameter("refresh");
			//new responses made through refresh link
			String respFromRefresh=request.getParameter("respFromRefresh");
			if (respFromRefresh==null) respFromRefresh="false";
									
			String moreRespNo=request.getParameter("moreRespNo");
			String fldLibId = request.getParameter("fieldLibId");
			int fieldLibId=EJBUtil.stringToNum(fldLibId);
			String fldCategory="";
			String fldName="";
			String fldNameFormat="";
			String fldExpLabel="";
			String fldDesc="";
			String fldUniqId="";
			String fldKeyword="";
			String fldInstructions="";
			String fldType="";
			String fldDataType="";
			String fldColCount="";
			String sortOrder="";
			int countResp = 0;
		
		
			String accountId="";
			String fldLibFlag="L"; // received from session in future 
			Style aStyle = new Style();
			
			String creator="";
			String ipAdd="";
		
			creator=(String) tSession.getAttribute("userId");
			ipAdd=(String)tSession.getAttribute("ipAdd");
	     	accountId=(String)tSession.getAttribute("accountId");

			String recordType=mode;  
	
			fldCategory=request.getParameter("category");
			fldName=request.getParameter("name");
			
			fldNameFormat=request.getParameter("nameta");
			fldNameFormat=(fldNameFormat==null)?"":fldNameFormat;
			
			fldDesc=request.getParameter("desc");
			fldUniqId=request.getParameter("uniqueId");
			fldKeyword=request.getParameter("keyword");
			fldInstructions=request.getParameter("instructions");
			fldType="M"; //mulitple choice box type to be entered by us 
			fldDataType=request.getParameter("editBoxType");
			fldColCount=request.getParameter("colcount");
			fldExpLabel = request.getParameter("expLabel");	
			if(fldExpLabel==null){
				fldExpLabel="0";
			}
			else{
				fldExpLabel="1";
		   }
		   	sortOrder = request.getParameter("sortOrder");
			if (sortOrder==null) sortOrder="";
			

	
			if (mode.equals("M"))
			{
				fieldLibJB.setFieldLibId(fieldLibId);
				fieldLibJB.getFieldLibDetails();
			}
			
			
			fieldLibJB.setCatLibId(fldCategory);
			fieldLibJB.setFldName(fldName);
			fieldLibJB.setFldNameFormat(fldNameFormat);
			fieldLibJB.setFldDesc(fldDesc);
			fieldLibJB.setFldUniqId(fldUniqId);
			fieldLibJB.setFldKeyword(fldKeyword);
			fieldLibJB.setFldInstructions(fldInstructions);
			fieldLibJB.setFldType(fldType);
			fieldLibJB.setFldDataType(fldDataType);
			fieldLibJB.setIpAdd(ipAdd);
			fieldLibJB.setRecordType(recordType);
			fieldLibJB.setAccountId(accountId);
			fieldLibJB.setLibFlag(fldLibFlag);
			fieldLibJB.setExpLabel(fldExpLabel);
			fieldLibJB.setFldIsVisible("0");
			fieldLibJB.setAStyle(aStyle);
			fieldLibJB.setFldColCount(fldColCount);
			fieldLibJB.setFldSortOrder(sortOrder);


			//this indicates new responses have been entered 
			//either through new mode or Refresh link
			
           	FldRespDao fldRespDao=new FldRespDao();			
			if ((mode.equals("N")) || respFromRefresh.equals("true")) 
			{
     			String[] saSeq = request.getParameterValues("newTxtSeq");            			
             	String[] saDispVal=request.getParameterValues("newTxtDispVal");
             	String[] saDataVal=request.getParameterValues("newTxtDataVal");
             	String[] saScore=request.getParameterValues("newTxtScore");
             	String[] saDefault=request.getParameterValues("newHdDefault");
             	String[] saRecordType=request.getParameterValues("newHdRecordType");            				
				for(int cnt=0 ;	cnt<saDispVal.length ; cnt++)
				{					
					if(!saDispVal[cnt].equals("")){
						countResp++;
					}
				}
				
				String[] seq = new String[countResp];
				String[] dispVal = new String[countResp];
				String[] dataVal = new String[countResp];
				String[] score = new String[countResp];
				String[] defaultVal = new String[countResp];
				String[] rcrdType = new String[countResp];	
				int i=0;
				for(int count=0 ;	count<saDispVal.length ; count++)
				{
					if(!saDispVal[count].equals("")){
						seq[i] = saSeq[count];
						dispVal[i] = saDispVal[count].trim();
						dataVal[i] = saDataVal[count].trim();
						score[i] = saScore[count];
						defaultVal[i] = saDefault[count];
						rcrdType[i] = saRecordType[count];
						i++;
					}
				}
				fldRespDao.setSequences(seq);
             	fldRespDao.setScores(score);
             	fldRespDao.setDispVals(dispVal);
             	fldRespDao.setDataVals(dataVal);
             	fldRespDao.setDefaultValues(defaultVal);
             	fldRespDao.setLastModifiedBy(creator);
             	fldRespDao.setCreator(creator);
             	fldRespDao.setRecordTypes(rcrdType);
             	fldRespDao.setIPAdd(ipAdd);
				
			}
			
			
			// Set the field Lib State Keeper
			if (mode.equals("N"))
			{				
				fieldLibJB.setCreator(creator);
				//pk of new field is returned if successful
				errorCode = fieldLibJB.setFieldLibDetails(fldRespDao);
				fieldLibId = errorCode;
			}					

			if (mode.equals("M"))
			{
				aStyle= new Style();	
				fieldLibJB.setCatLibId(fldCategory);
				fieldLibJB.setFldUniqId(fldUniqId);
				fieldLibJB.setFldKeyword(fldKeyword);
				fieldLibJB.setFldInstructions(fldInstructions);
				fieldLibJB.setFldType(fldType);
				fieldLibJB.setFldDataType(fldDataType);
				fieldLibJB.setModifiedBy(creator);
				fieldLibJB.setIpAdd(ipAdd);
				fieldLibJB.setRecordType(recordType);
				fieldLibJB.setLibFlag(fldLibFlag);
				fieldLibJB.setAccountId(accountId);
				fieldLibJB.setExpLabel(fldExpLabel);
				fieldLibJB.setFldIsVisible("0");
				fieldLibJB.setAStyle(aStyle);

				//anu
				String[] saRespId = request.getParameterValues("fldRespId"); 
     			String[] saSeq = request.getParameterValues("txtSeq");            			
             	String[] saDispVal=request.getParameterValues("txtDispVal");
             	String[] saDataVal=request.getParameterValues("txtDataVal");
             	String[] saScore=request.getParameterValues("txtScore");
             	String[] saDefault=request.getParameterValues("hdDefault");
             	String[] saRecordType=request.getParameterValues("hdRecordType");

				for(int cnt=0 ;	cnt<saDispVal.length ; cnt++)
				{					
					if(!saDispVal[cnt].equals("")){
						countResp++;
					}
				}

				String[] dispVal = new String[countResp];
				String[] dataVal = new String[countResp];


				int i=0;
				for(int count=0 ;	count<saDispVal.length ; count++)
				{
					if(!saDispVal[count].equals("")){             	
						dispVal[i] = saDispVal[count].trim();
						dataVal[i] = saDataVal[count].trim();
						
						i++;
					}
				}
				
             	
				
             	FldRespDao fldRespDaoModify=new FldRespDao();
				fldRespDaoModify.setFldRespIds(saRespId);          		
             	fldRespDaoModify.setSequences(saSeq);

             	fldRespDaoModify.setScores(saScore);
             	//fldRespDaoModify.setDispVals(saDispVal);
				fldRespDaoModify.setDispVals(dispVal);

             	//fldRespDaoModify.setDataVals(saDataVal);
				fldRespDaoModify.setDataVals(dataVal);

             	fldRespDaoModify.setDefaultValues(saDefault);
             	fldRespDaoModify.setLastModifiedBy(creator);
             	fldRespDaoModify.setRecordTypes(saRecordType);
             	fldRespDaoModify.setIPAdd(ipAdd);
				//anu
				errorCode = fieldLibJB.updateFieldLib(fldRespDaoModify);

				//this implies that new responses have also been added
				if (respFromRefresh.equals("true")) 
				{
				 fldRespDao.setField(fldLibId);

				errorCode = fieldLibJB.insertFieldLibResponses(fldRespDao);
				}				
			
		}
		
		
		if ( errorCode == -2 || errorCode == -3 )
		{
%>

<%             if (errorCode == -3 )
				{
%>
				<br><br><br><br><br><p class = "successfulmsg" align = center>
				 <%=MC.M_UnqFldName_EtrFldId%><%--The unique field name already exists. Please enter a different Field ID.*****--%></P>
				<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
				
				
<%
				} else 
%>				
				<br><br><br><br><br><p class = "successfulmsg" align = center>		
	 			<%=MC.M_Not_Succ%><%--Not Successful*****--%></p>
<%	
		} //end of if for Error Code

		else

		{
%>		<br><br><br><br><br><p class = "successfulmsg" align = center>	
		<%=MC.M_Data_SavedSucc%><%--Data saved Successfully*****--%></p>
		<%if (refresh.equals("true")) 
		{ %>
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=multipleChoiceBox.jsp?mode=M&fieldLibId=<%=fieldLibId%>&moreRespNo=<%=moreRespNo%>&refresh=true">
		<%} else {%>
			<script>
					//Added to resolve Bug#7594 : Raviesh					
					window.opener.location.href = 'fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4#';
					//window.opener.location.reload();			
					setTimeout("self.close()",1000);
			</script>		

		 <%}%> 
		
	<%
		}//end of else for Error Code Msg Display
	}//end of if old esign
	else{  
%>
		<jsp:include page="incorrectesign.jsp" flush="true"/>
<%	  
			}//end of else of incorrect of esign
     }//end of if body for session 	
	
	else
	{

%>
		  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>

</body>

</html>
