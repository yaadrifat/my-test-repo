<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Form_RptPreview%><%--Form Report Preview*****--%></title>
<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<script>

function changeMode(formobj,modeStr){

formobj.mode.value=modeStr;
if(isNaN(formobj.addrow.value) == true) {

	alert("<%=MC.M_InorrectRowsNum_ReEtr%>");/*alert("Incorrect number of rows specified. Please enter again");*****/

	formobj.addrow.focus();
	
	return false;
   }	
}
function setType(formobj,count){
totcount=formobj.totcount.value;
if (totcount==1){
index=formobj.fldName.selectedIndex;
if (index<totcount){
type=formobj.fldTypeOrig.value;
formobj.fldType.value=type;

}
}else if (totcount>1){

index=formobj.fldName[count].selectedIndex;

if (index<totcount){
type=formobj.fldTypeOrig[index].value
formobj.fldType[count].value=type;

}}
}
function setFilter(formobj){
var fltrName="";
totcount=formobj.totcount.value;
formId=formobj.formId.value;
if (formobj.selFltr.type=="hidden") {
fltrName=fnTrimSpaces(formobj.fltrName.value);
}
var startcount=0;
var endcount=0;
var filterTemp="";
var filter="";
var criteriaStr="";
var extend="";
var prevCount=-1;
//do the validation first for other fields

if (totcount==1){
if (formobj.fldName.value.length>0 && fltrName.length==0 && formobj.selFltr.type=="hidden"){
 alert("<%=MC.M_FilterName_ToProc%>");/*alert("Please specify a filter name to proceed.");*****/
 formobj.fltrName.focus();
 return false;
 }
 
 } else {
 for (j=0;j<totcount;j++){
  if ((formobj.fldName[j].value.length>0) && (fltrName.length==0) && (formobj.selFltr.type=="hidden")){
 alert("<%=MC.M_FilterName_ToProc%>");/*alert("Please specify a filter name to proceed.");*****/
 formobj.fltrName.focus();
 return false;
 } else 
 {
 continue;
 }
 }
 
 }
//end validation


if (totcount==1){
for (i=0;i<totcount;i++){
if (formobj.exclude.checked)
continue;
startBrac=formobj.startBracket.value;
 
 if (formobj.startBracket.checked)
    startcount++; 
 fldName=formobj.fldName.value;
 criteria=formobj.scriteria.value;
 fltdata=formobj.fltData.value;
 endBrac=formobj.endBracket.value;
 fldType=formobj.fldType.value;
 if (formobj.startBracket.checked)
    endcount++;
 //extend=formobj.extend.value;
 
if (fldName.length==0 && fltdata.length==0 && criteria.length==0)
continue;
if (((fldName.length>0) && fltdata.length==0) && criteria.length==0){
	alert("<%=MC.M_SelValue_ForQualifier%>");/*alert("Please select a value for the Qualifier.");*****/
formobj.scriteria.focus();
return false;
}
if (((fldName.length>0) && fltdata.length>0) && criteria.length==0){
	alert("<%=MC.M_SelValue_ForQualifier%>");/*alert("Please select a value for the Qualifier.");*****/
formobj.scriteria.focus();
return false;
}

if ((fldName.length==0) && (fltdata.length>0 && criteria.length>0)){
 alert("<%=MC.M_Selc_FldFld%>");/*alert("Please select a filter Field.");*****/
 formobj.fldName.focus();
 return false;
}

if (((fldName.length>0) && criteria.length>0) &&  fltdata.length==0 && (criteria!="first" && criteria!="latest" && criteria!="highest" && criteria!="lowest" && criteria!="isnull")){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsEtrVal",paramArray));/*alert("Row#"+i+"-Please enter a value.");*****/
formobj.fltData.focus();
return false;
}
if ((fldType=="ED") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){ 
if (!validate_date(formobj.fltData)) return false;
}
if ((fldType=="EN") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
if(isNaN(formobj.fltData.value) == true) {
	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number.");*****/
	formobj.fltData.focus();
	return false;
   }
}

if ((fldName.length==0 && fltdata.length==0) &&  (criteria.length>0 )) 
continue;
if ((fldName.length==0 && criteria.length==0) && (fltdata.length>0))  
continue;
if (formobj.startBracket.checked){
	 filterTemp="(";
  }
   if (criteria=="isnull"){
 if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    /*for(count=0;count<tempArray.length;count++){
    
    if (criteriaStr.length==0)
    criteriaStr="("+tempArray[count]+") is null ";
    else 
    criteriaStr=criteriaStr+" or ("+fldName+") is null ";
    }*/
    } else{
   criteriaStr="("+fldName+")  is null"
   }
  }
  
 if (criteria=="contains"){
 if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    
    if (criteriaStr.length==0)
    criteriaStr="lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
    else 
    criteriaStr=criteriaStr+" or lower("+fldName+") like lower('%"+ fltdata +"%')";
    }
    } else{
   criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="isequalto"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") = TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") = TO_NUMBER('"+ fltdata +"')"
  else criteriaStr="lower("+fldName+") = lower('"+ fltdata +"')"
  }
  if (criteria=="isnotequal"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") != TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") != TO_NUMBER('"+ fltdata +"')"
  else criteriaStr="lower("+fldName+") != lower('"+ fltdata +"')"
  }
  if (criteria=="start"){
   criteriaStr="lower("+fldName+") like lower('"+ fltdata +"%')"
  }
  if (criteria=="ends"){
   criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"')"
  }
  if (criteria=="isgt"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") > TO_NUMBER('"+ fltdata +"')"
   else   criteriaStr="lower("+fldName+") > lower('%"+ fltdata +"%')"
   }
  if (criteria=="islt"){
  if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
  else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") < TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") < lower('%"+ fltdata +"%')"
  
  }
  if (criteria=="isgte"){
    if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") >= TO_NUMBER('"+ fltdata +"')"
   else    criteriaStr="lower("+fldName+") >= lower('%"+ fltdata +"%')"
   
  }
  if (criteria=="islte"){
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") <= TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") <= lower('%"+ fltdata +"%')"
  }
  if (criteria=="first"){
   criteriaStr="filldate=(select min(filldate) from er_formslinear where fk_form="+formId+")";
  }
  if (criteria=="latest"){
   criteriaStr="filldate=(select max(filldate) from er_formslinear where fk_form="+formId+")";
  }
  
  if (criteria=="lowest"){
   criteriaStr="lower("+fldName+") =(select min(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")" 
  }
  if (criteria=="highest"){
   criteriaStr="lower("+fldName+") =(select max(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")" 
  }
  if (criteriaStr.length>0){
  filter=filter+filterTemp+criteriaStr;
  if (formobj.endBracket.checked){
    filter=filter+" )";
  }
  }
 

}//end Fpr loop
}
else{ //else for totcount
criteriaStr="";
for(i=0;i<totcount;i++){
filterTemp="";
criteriaStr="";
if (formobj.exclude[i].checked)
 continue;
 startBrac=formobj.startBracket[i].value;
  if (formobj.startBracket[i].checked)
    startcount++; 
 fldName=formobj.fldName[i].value;
 criteria=formobj.scriteria[i].value;
 fltdata=formobj.fltData[i].value;
 endBrac=formobj.endBracket[i].value;
 fldType=formobj.fldType[i].value;
 
 if (formobj.endBracket[i].checked)
    endcount++;
   if ((fldName.length==0 && fltdata.length==0) &&  (criteria.length>0)) 
continue;
if ((fldName.length==0 && criteria.length==0) && (fltdata.length>0))  
continue;
if (((fldName.length>0) && fltdata.length==0) && criteria.length==0){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsSelQualifier",paramArray));/*alert("Row#"+i+"-Please select a value for the Qualifier.");*****/
formobj.scriteria[i].focus();
return false;
}
if ((fldName.length==0) && (fltdata.length==0 &&  criteria.length==0)) 
continue;
    
    
if ((i>0) && (filter.length>0))  {
 extend=formobj.extend[prevCount].value;
 
 }
 
 else {
 extend="";
 }
 prevCount=i;
 
if (((fldName.length>0) && fltdata.length>0) && criteria.length==0){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsSelQualifier",paramArray));/*alert("Row#"+i+"-Please select a value for the Qualifier.");*****/
formobj.scriteria[i].focus();
return false;
}

if ((fldName.length==0) && (fltdata.length>0 && criteria.length>0)){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsSelFld",paramArray));/*alert("Row#"+i+"-Please select a filter Field.");*****/
formobj.fldName[i].focus();
 return false;
}
if (((fldName.length>0) && criteria.length>0) &&  fltdata.length==0 && (criteria!="first" && criteria!="latest" && criteria!="highest" && criteria!="lowest" && criteria!="isnull")){
var paramArray = [i];
alert(getLocalizedMessageString("M_Row_PlsEtrVal",paramArray));/*alert("Row#"+i+"-Please enter a value.");*****/
formobj.fltData[i].focus();
return false;
}

if ((fldType=="ED") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){ 
if (!validate_date(formobj.fltData[i])) return false;
}
if ((fldType=="EN") && (criteria=="isequalto" || criteria=="isgt" || criteria=="islt" || criteria=="isgte" || criteria=="islte" )){
if(isNaN(formobj.fltData[i].value) == true) {
	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number.");*****/
	formobj.fltData[i].focus();
	return false;
   }
}
if (formobj.startBracket[i].checked){
	 filterTemp=" "+extend+" (";
  }
  else {
  	 filterTemp=" "+extend+" ";
  }   
  if (criteria=="isnull"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    /*for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(("+tempArray[count]+") is null ";
    else 
    criteriaStr=criteriaStr+" or ("+tempArray[count]+") is null";
    }
    criteriaStr=criteriaStr+")";*/
    } else{
   criteriaStr="("+fldName+") is null"
   }
  }
  
 if (criteria=="contains"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
    else 
    criteriaStr=criteriaStr+" or lower("+tempArray[count]+") like lower('%"+ fltdata +"%')";
    }
    criteriaStr=criteriaStr+")";
    } else{
   criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="isequalto"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
       
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") =TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") =TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") = lower('"+ fltdata +"')"
    }
    else{ 
        
	if (fldType=="ED") criteriaStr=criteriaStr +" or F_TO_DATE("+tempArray[count]+") = TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr=criteriaStr +" or TO_NUMBER("+tempArray[count]+") = TO_NUMBER('"+ fltdata +"')"
	else criteriaStr=criteriaStr +" or lower("+tempArray[count]+") = lower('"+ fltdata +"')"
	
	}
    }
    criteriaStr=criteriaStr+")";
    }else{
    if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") = TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") = TO_NUMBER('"+ fltdata +"')"
    else  criteriaStr="lower("+fldName+") = lower('"+ fltdata +"')"
   
  }
  }
   if (criteria=="isnotequal"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
       
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") !=TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") !=TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") != lower('"+ fltdata +"')"
    }
    else{ 
        
	if (fldType=="ED") criteriaStr=criteriaStr +" or TO_DATE("+tempArray[count]+") != TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr=criteriaStr +" or TO_NUMBER("+tempArray[count]+") != TO_NUMBER('"+ fltdata +"')"
	else criteriaStr=criteriaStr +" or lower("+tempArray[count]+") != lower('"+ fltdata +"')"
	
	}
    }
    criteriaStr=criteriaStr+")";
    }else{
    if (fldType=="ED") criteriaStr="TO_DATE("+fldName+") != TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") != TO_NUMBER('"+ fltdata +"')"
    else  criteriaStr="lower("+fldName+") != lower('"+ fltdata +"')"
   
  }
  }
  if (criteria=="start"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") like lower('"+ fltdata +"%')"
    else
    criteriaStr=criteriaStr+" or lower("+tempArray[count]+") like lower('"+ fltdata +"%')"
    }
    criteriaStr=criteriaStr+")";
    }else{
   criteriaStr="lower("+fldName+") like lower('"+ fltdata +"%')"
   }
  }
  if (criteria=="ends"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") like lower('%"+ fltdata +"')"
    else 
    criteriaStr=criteriaStr + " or lower("+tempArray[count]+") like lower('%"+ fltdata +"')"
    }
    criteriaStr=criteriaStr+")";
  }else{
    criteriaStr="lower("+fldName+") like lower('%"+ fltdata +"')"
    }
  }
  if (criteria=="isgt"){
   if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_To_DATE("+tempArray[count]+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr="(To_NUMBER("+tempArray[count]+") > TO_NUMBER('"+ fltdata +"')"
    else    criteriaStr="(lower("+tempArray[count]+") > lower('%"+ fltdata +"%')"
    }else{
    if (fldType=="ED") criteriaStr=criteriaStr + " or F_TO_DATE("+tempArray[count]+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr=criteriaStr + " or TO_NUMBER("+tempArray[count]+") > TO_NUMBER('"+ fltdata +"')"
    else criteriaStr=criteriaStr + " or lower("+tempArray[count]+") > lower('%"+ fltdata +"%')"
    }
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") > TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") > TO_NUMBER('"+ fltdata +"')"
   else   criteriaStr="lower("+fldName+") > lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="islt"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy)"
    else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") < TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") < lower('%"+ fltdata +"%')"
    }else{
    if (fldType=="ED") criteriaStr=criteriaStr+" or TO_DATE("+tempArray[count]+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr=criteriaStr+" or TO_NUMBER("+tempArray[count]+") < TO_NUMBER('"+ fltdata +"')"
    else  criteriaStr=criteriaStr+" or lower("+tempArray[count]+") < lower('%"+ fltdata +"%')"
    }
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") < TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") < TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") < lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="isgte"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+",'mm/dd/yyyy') >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
     else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") >= TO_NUMBER('"+ fltdata +"')"
    else criteriaStr="(lower("+tempArray[count]+") >= lower('%"+ fltdata +"%')"
    }
    else{
    if (fldType=="ED") criteriaStr=criteriaStr+ " or F_TO_DATE("+tempArray[count]+") >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
    else if (fldType=="EN") criteriaStr=criteriaStr+ " or TO_NUMBER("+tempArray[count]+") >= TO_NUMBER('"+ fltdata +"')"
    else criteriaStr=criteriaStr+ " or lower("+tempArray[count]+") >= lower('%"+ fltdata +"%')"
    }
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") >= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") >= TO_NUMBER('"+ fltdata +"')"
   else    criteriaStr="lower("+fldName+") >= lower('%"+ fltdata +"%')"
   }
  }
  if (criteria=="islte"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0){
    	if (fldType=="ED") criteriaStr="(F_TO_DATE("+tempArray[count]+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr="(TO_NUMBER("+tempArray[count]+") <= TO_NUMBER('"+ fltdata +"')"
    	else criteriaStr="(lower("+tempArray[count]+") <= lower('%"+ fltdata +"%')"
	}else{
	if (fldType=="ED") criteriaStr=criteriaStr+ "or F_TO_DATE("+tempArray[count]+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
	else if (fldType=="EN") criteriaStr=criteriaStr+ "or TO_NUMBER("+tempArray[count]+") <= TO_NUMBER('"+ fltdata +"')"
	else criteriaStr=criteriaStr+ "or lower("+tempArray[count]+") <= lower('%"+ fltdata +"%')"
	}
    }
    criteriaStr=criteriaStr+")";
   }else{
   if (fldType=="ED") criteriaStr="F_TO_DATE("+fldName+") <= TO_DATE('"+ fltdata +"','mm/dd/yyyy')"
   else if (fldType=="EN") criteriaStr="TO_NUMBER("+fldName+") <= TO_NUMBER('"+ fltdata +"')"
   else criteriaStr="lower("+fldName+") <= lower('%"+ fltdata +"%')"
   }
  }
  
  if (criteria=="first"){
   criteriaStr="filldate=(select min(filldate) from er_formslinear where fk_form="+formId+")";
  }
  if (criteria=="latest"){
   criteriaStr="filldate=(select max(filldate) from er_formslinear where fk_form="+formId+")";
  }
  
  if (criteria=="lowest"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") =(select min(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    else 
    criteriaStr=criteriaStr + " or lower("+tempArray[count]+") =(select min(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    }
    criteriaStr=criteriaStr+")";
   }else{
   criteriaStr="lower("+fldName+") =(select min(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")"
   }
  }
  if (criteria=="highest"){
  if (fldName.indexOf("|")>-1){ 
    tempArray=fldName.split("|");
    for(count=0;count<tempArray.length;count++){
    if (criteriaStr.length==0)
    criteriaStr="(lower("+tempArray[count]+") =(select max(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    else
    criteriaStr=criteriaStr + " or lower("+tempArray[count]+") =(select max(lower("+ tempArray[count]+")) from er_formslinear where fk_form="+ formId+")"
    }
    criteriaStr=criteriaStr+")";
    }else{
   criteriaStr="lower("+fldName+") =(select max(lower("+ fldName+")) from er_formslinear where fk_form="+ formId+")"
   }
  }
  if (criteriaStr.length>0){
  filter=filter+filterTemp+criteriaStr;
  if (formobj.endBracket[i].checked){
    filter=filter+" )";
  }
  }
    
  
}//end for
}//end else totcount
if (startcount!=endcount){
alert("<%=MC.M_BeginBrac_EndMismatch%>");/*alert("Begin Bracket/End Bracker Mismatch. Please Verify");*****/
return false;
}

//window.opener.document.forms["dynreport"].elements["repFilter"].value=filter;
if (filter.length>0) filter="(" + filter+")" ;

formobj.filter.value=filter;

formobj.action="dynorder.jsp";
//window.close();
}
function setInclude(formobj,count){
totcount=formobj.totcount.value;
if (totcount==1){

formobj.include.checked=true;
} else
{
formobj.include[count].checked=true;
}


}


</script>

</head>

<% String src,sql="",tempStr="",sqlStr="",repMode="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="sess" value="keep"/>

</jsp:include>   



<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<br>
 <DIV class="formDefault" id="div1">  
<%

	
	
	String selectedTab = request.getParameter("selectedTab");
	String value="",patientID="",studyNumber="",orderStr="",order="",flgMatch="",fltrName="",fltrDD="",prevId="",fldType="",dynType="";
	String[] scriteria=null,fldData=null,fldSelect= null;
	int personPk=0,studyPk=0,index=-1,arraySize=0,row=0,colLen=0;
	DynRepDao dynDao=new DynRepDao();
	ArrayList selectedfld=null;
	ArrayList flltDtIds=new ArrayList();
	ArrayList dataList=new ArrayList() ;
	ArrayList prevSessColId=new ArrayList();
	ArrayList tempList=new ArrayList();
	ArrayList startBracList=new ArrayList();
	ArrayList endBracList=new ArrayList();
	ArrayList scriteriaList=new ArrayList();
	ArrayList fldDataList=new ArrayList();
	ArrayList sessCol=new ArrayList();
	ArrayList sessColName=new ArrayList();
	ArrayList sessColDisp=new ArrayList();
	ArrayList sessColSeq=new ArrayList();
	ArrayList sessColType=new ArrayList();
	ArrayList sessColStr=new ArrayList();
	ArrayList sessColWidth=new ArrayList();
	ArrayList sessColFormat=new ArrayList();
	ArrayList sessColId=new ArrayList();
	HashMap attributes = new HashMap();
	HashMap FldAttr = new HashMap();
	HashMap FltrAttr = new HashMap();
	HashMap TypeAttr = new HashMap();
	
	String[] fldName=null;
	String[] fldCol=null;
	String[] fldNameSelect=null;
	String[] extend=null;
	ArrayList fldTypeList=new ArrayList();
	ArrayList fldNameSelList=new ArrayList();
	ArrayList extendList=new ArrayList();
	ArrayList excludeList=new ArrayList();
	ArrayList multiFltrIds=new ArrayList();
	ArrayList multiFltrNames=new ArrayList();
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{ 
	attributes=(HashMap)tSession.getAttribute("attributes");
		if (attributes==null){%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=dynrepbrowse.jsp?srcmenu=<%=src%>">
	
	<%}else {
	String mode = request.getParameter("mode");
	mode=(mode==null)?"":mode;
	
	if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	String formId=request.getParameter("formId");
	if (formId==null) formId="";
	if (formId.length()==0){
	
	if (TypeAttr.containsKey("formId")) formId=(String)TypeAttr.get("formId");
	}
	if (formId==null) formId="";
	
	if (TypeAttr.containsKey("prevId")) prevId=(String)TypeAttr.get("prevId");
	if (prevId==null) prevId="";
	
	if (prevId.length()>0){
	if (!formId.equals(prevId)){
	 if (attributes.containsKey("FltrAttr")) attributes.remove("FltrAttr");
	}
	}
	if (attributes.containsKey("FltrAttr")) FltrAttr=(HashMap)attributes.get("FltrAttr");
	if (attributes.containsKey("FldAttr")) FldAttr=(HashMap)attributes.get("FldAttr");
	if (attributes.containsKey("TypeAttr")) TypeAttr=(HashMap)attributes.get("TypeAttr");
	if (TypeAttr.containsKey("repMode")) repMode=(String) TypeAttr.get("repMode");
	if (repMode==null) repMode="";
	if (TypeAttr.containsKey("dynType")) dynType=(String) TypeAttr.get("dynType");
	if (dynType==null) dynType="";
	
	
	fltrName=request.getParameter("fltrName");
	fltrName=(fltrName==null)?"":fltrName;
	if (fltrName.length()==0){
	if (FltrAttr.containsKey("fltrName")){
	fltrName=(String)FltrAttr.get("fltrName");
	fltrName=(fltrName==null)?"":fltrName;
	}
	}
	
	if (FltrAttr.containsKey("MultiFltrIds")) multiFltrIds=(ArrayList) FltrAttr.get("MultiFltrIds");
	if (FltrAttr.containsKey("MultiFltrNames")) multiFltrNames=(ArrayList) FltrAttr.get("MultiFltrNames");
	int selValue=EJBUtil.stringToNum(request.getParameter("selFltr"));
	if (selValue==0){ 
	if (FltrAttr.containsKey("fltrId")) selValue= EJBUtil.stringToNum((String)FltrAttr.get("fltrId"));
	if (FltrAttr.containsKey("fltrName")) fltrName= (String)FltrAttr.get("fltrName");
	}
	
	if (multiFltrIds.size()>1){
	if (selValue==0) selValue=EJBUtil.stringToNum((String)multiFltrIds.get(0));
	fltrDD=EJBUtil.createPullDown("selFltr",selValue,multiFltrIds,multiFltrNames);
	} else if (multiFltrIds.size()==1) {
	if (selValue==0) selValue=EJBUtil.stringToNum((String)multiFltrIds.get(0));
	}
	if (fltrName.length()==0){
	if (multiFltrNames.size()==1) fltrName=(String)multiFltrNames.get(0); 
	}
	fltrName=(fltrName==null)?"":fltrName;
	if (mode.equals("addrow") || (mode.equals("chgfltr")) ){
	fldName=request.getParameterValues("fldLocal");
	fldCol=request.getParameterValues("fldLocalCol");
	for(int len=0;len<fldName.length;len++){
	System.out.println(" count " + len + " with fldname " + fldName[len] + " and  fldCol " + fldCol[len] );
	}
	}
	else{ 
	fldName=request.getParameterValues("fldName");
	if (fldName==null){
	if (FltrAttr.containsKey("fldName")) {
	fldName=(String[])FltrAttr.get("fldName");
	}
	}
	fldCol=request.getParameterValues("fldCol");
	if (fldCol==null){ 
	 if (FltrAttr.containsKey("fldCol")) {
	 fldCol=(String[])FltrAttr.get("fldCol");
	  }
	  }
	fldSelect=request.getParameterValues("fldSelect");
	}//else end
	
	//let's fix fldSleect to make session variables
	//start here
	StringTokenizer st=null;
	if (fldSelect!=null){
	System.out.println("fldSelect Length"+fldSelect.length);
	for(int z=0;z<fldSelect.length;z++){
	System.out.println("Element of fldSelectr"+fldSelect[z]);
	st=new StringTokenizer(StringUtil.decodeString(fldSelect[z]),"[$]");
	sessColStr.add(fldSelect[z]);
	sessColSeq.add(st.nextToken());
	sessColName.add(st.nextToken());
	sessColDisp.add(st.nextToken());
	sessColWidth.add(st.nextToken());
	sessCol.add(st.nextToken());
	sessColType.add(st.nextToken());
	sessColFormat.add(st.nextToken());
	sessColId.add(st.nextToken());
	}
	if (FldAttr.containsKey("prevSessColId")) prevSessColId=(ArrayList)FldAttr.get("prevSessColId");
	if (prevSessColId!=null) colLen=prevSessColId.size();
	if (colLen>0){
	for (int z=0;z<colLen;z++){
	if (sessColId.contains(prevSessColId.get(z))){
	}else{
	sessColId.add("-"+(String)prevSessColId.get(z));
	}   	
	}//end for
	}//end colLen
	FldAttr.put("sessColStr",sessColStr);
	FldAttr.put("sessColSeq",sessColSeq);
	FldAttr.put("sessColName",sessColName);
	FldAttr.put("sessColDisp",sessColDisp);
	FldAttr.put("sessColWidth",sessColWidth);
	FldAttr.put("sessCol",sessCol);
	FldAttr.put("sessColType",sessColType);
	FldAttr.put("sessColFormat",sessColFormat);
	FldAttr.put("sessColId",sessColId);
	}
	attributes.put("FldAttr",FldAttr);
	tSession.setAttribute("attributes",attributes);
	
	//end here 
	String addrow=request.getParameter("addrow");
	
	
	if (addrow==null) addrow="";
	else row=EJBUtil.stringToNum(addrow);
	if (mode.equals("addrow")){
	String[] startBracket=request.getParameterValues("startBracket");
	String[] endBracket=request.getParameterValues("endBracket");
	String[] exclude = request.getParameterValues("exclude");
	String[] fltDt=request.getParameterValues("fltrDtId");
	if (fltDt!=null) 
	flltDtIds=EJBUtil.strArrToArrayList(fltDt);
	if (startBracket!=null)
	startBracList=EJBUtil.strArrToArrayList(startBracket);
	if (endBracket!=null)
	endBracList=EJBUtil.strArrToArrayList(endBracket);
	fldNameSelect=request.getParameterValues("fldName");
	if (fldNameSelect!=null)
	fldNameSelList=EJBUtil.strArrToArrayList(fldNameSelect);
	scriteria=request.getParameterValues("scriteria");
	if (scriteria!=null)
	scriteriaList=EJBUtil.strArrToArrayList(scriteria);
	fldData=request.getParameterValues("fltData");
	if (fldData!=null) 
	fldDataList=EJBUtil.strArrToArrayList(fldData);
	extend=request.getParameterValues("extend");
	if (extend!=null)
	extendList=EJBUtil.strArrToArrayList(extend);
	System.out.println(fldNameSelect);
	if (exclude!=null)
	excludeList=EJBUtil.strArrToArrayList(exclude);
	}
	if (fldNameSelList!=null){
	if (fldNameSelList.size()==0){
	if (FltrAttr.containsKey("startBracList")) startBracList=(ArrayList) FltrAttr.get("startBracList");
	if (FltrAttr.containsKey("endBracList")) endBracList=(ArrayList) FltrAttr.get("endBracList");
	if (FltrAttr.containsKey("fldNameSelList")) fldNameSelList=(ArrayList) FltrAttr.get("fldNameSelList");
	if (FltrAttr.containsKey("scriteriaList")) scriteriaList=(ArrayList) FltrAttr.get("scriteriaList");
	if (FltrAttr.containsKey("fldDataList")) fldDataList=(ArrayList) FltrAttr.get("fldDataList");
	if (FltrAttr.containsKey("extendList")) extendList=(ArrayList) FltrAttr.get("extendList");
	if (FltrAttr.containsKey("excludeList")) excludeList=(ArrayList) FltrAttr.get("excludeList");
	if (FltrAttr.containsKey("fltrDtIdList")) flltDtIds=(ArrayList) FltrAttr.get("fltrDtIdList");
	
	
	}}
	if ((mode.equals("chgfltr")) || ((selValue>0) && (fldNameSelList.size()==0))){
	
	dynDao=dynrepB.getFilterDetails(EJBUtil.integerToString(new Integer(selValue)));
	startBracList=dynDao.getFiltBbrac();
	endBracList=dynDao.getFiltEbrac();
	fldNameSelList=dynDao.getFiltCol();
	flltDtIds= dynDao.getFltrColIds();
	scriteriaList=dynDao.getFiltQf();
	fldDataList=dynDao.getFiltData();
	extendList=dynDao.getFiltOper();
	excludeList=dynDao.getFiltExclude();
	if (fldNameSelList.size() > fldName.length)	row=(fldNameSelList.size() - fldName.length);
//	setFiltSeq(EJBUtil.integerToString(new Integer(rs.getInt("dynrepfilterdt_seq"))));
	}
	System.out.println("Selected filter" + fldNameSelList + row );
	//request.setAttribute("fldCol",selectList);
	//create a dropdown for fieldColumns
	int counter=0;
	String name="";
	StringBuffer fldDD=new StringBuffer();	
	System.out.println("**************************MODe"+mode+"**************************");
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FldAttr")) System.out.println("I am here-FldAttr"+attributes.get("FldAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("TypeAttr")) System.out.println("I am here-TypeAttr"+attributes.get("TypeAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("FltrAttr")) System.out.println("I am here-FltrAttr"+attributes.get("FltrAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("SortAttr")) System.out.println("I am here-SortAttr"+attributes.get("SortAttr"));
	System.out.println("*****************************************************************************************");
	if (attributes.containsKey("RepAttr")) System.out.println("I am here-RepAttr"+attributes.get("RepAttr"));
	System.out.println("*****************************************************************************************");
	String formName=(String)TypeAttr.get("formName");
	 if( formName==null) formName="";
	 //Retrieve the field types
	  if (FldAttr.containsKey("sessColType")) fldTypeList=(ArrayList)FldAttr.get("sessColType");
	 
	 //end retrieve field types
	 //Retrieve columns names from database,problem appears in modify mode,if there was not filter selected and user comes straight
	 //by clicking on top link instead from previosu page.
	 if (repMode.equals("M")){
	 DynRepDao dyndao=dynrepB.getMapData(formId);
	 dyndao.preProcessMap();
	if ((fldName==null) || (fldName.length==0)){
	 fldCol= ((String[])(dyndao.getMapCols()).toArray(new String [ (dyndao.getMapCols()).size() ]));
	fldName=((String[])(dyndao.getMapDispNames()).toArray(new String [ (dyndao.getMapDispNames()).size() ]));
	fldTypeList=dyndao.getMapColTypes();
	}
	}
	//end for retrieve column name from database.
	System.out.println("FLDName" + fldName.length + "fldCol" + fldCol.length);
	
%>

<P class="sectionHeadings"> <%=LC.L_FrmRpt_Filter%><%--Form Report >> Filter*****--%> </P>
<form name="dynadvanced" method="post">
<table><tr>
<%if (attributes.get("TypeAttr")!=null){%>
<td><A href="dynreptype.jsp?mode=link&srcmenu=<%=src%>&dynType=<%=dynType%>"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></A></td>
<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_RptType%><%--Select Report Type*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FldAttr")!=null){%>
<td><A href="dynselectflds.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Select_Flds%><%--Select Fields*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("FltrAttr")!=null){%>
<td><A href="dynfilter.jsp?srcmenu=<%=src%>"><font color="red"><b><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></b></font></A></td>

<%}else{%>
<td><p class="sectionHeadings"><font color="red"><b><%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%></b></font></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("SortAttr")!=null){%>
<td><A href="dynorder.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Sort_Criteria%><%--Sort Criteria*****--%></p></td>
<%}%>
<td><p class="sectionHeadings">>></p></td>
<%if (attributes.get("RepAttr")!=null){%>
<td><A href="dynrep.jsp?mode=link&srcmenu=<%=src%>"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></A></td>

<%}else{%>
<td><p class="sectionHeadings"><%=LC.L_Preview_Save%><%--Preview & Save*****--%></p></td>
<%}%>
</tr></table>
<input type="hidden" name="formId" value="<%=formId%>")>
<input type="hidden" name="filter" value="")>
<input type="hidden" name="totcount" value="<%=fldName.length+row%>">
<P class="sectionHeadings"><%=LC.L_Selected_Form%><%--Selected Form*****--%>: <%=formName%> </P>
<table width="100%"><tr>
<%if (multiFltrIds.size()>1){%>
<td><%=LC.L_Select_Filter%><%--Select Filter*****--%>:<%=fltrDD%>&nbsp;&nbsp;<button type="submit"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button></td>

<%}else{%>
<td width="60%"><%=LC.L_Specify_FilterName%><%--Specify Filter Name*****--%>&nbsp;&nbsp;
<%if (fltrName.length()>0){%>
<Input Type="text" name="fltrName" value="<%=fltrName%>" readonly>
<%}else{%>
<Input Type="text" name="fltrName" value="<%=fltrName%>">
<%}%>
</td>
<Input Type="hidden" name="selFltr" value="<%=selValue%>">
<%}%>
<td width="20%"><%=LC.L_Add%><%--Add*****--%>&nbsp; <input type="text" name="addrow" value="0" size="2" maxlength="2">&nbsp;<%=LC.L_More_Rows%><%--more rows*****--%></td>
<td><button type="submit" onClick="return changeMode(document.dynadvanced,'addrow');"><%=LC.L_Refresh%></button></td>

</tr></table>

<input type="hidden" name="mode" value="chgfltr">
<input type="hidden" name="srcmenu" value="<%=src%>">
<table class="tableDefault" width="100%" border="1">
<tr>
<th width="10%"><%=LC.L_Exclude%><%--Exclude*****--%></th>
<th width="15%"><%=LC.L_Start_Bracket%><%--Start Bracket*****--%></th>
<th width="20%"><%=LC.L_Select_Fld%><%--Select Field*****--%></th>
<th width="10%"><%=LC.L_Criteria%><%--Criteria*****--%></th>
<th width="20%"><%=LC.L_Value%><%--Value*****--%></th>
<th width="15%"><%=LC.L_End_Bracket%><%--End Bracket*****--%></th>
<th width="10%"><%=LC.L_AndOrOr%><%--And/Or*****--%></th>
</tr>
<%
System.out.println("fldName.length+row"+fldName.length);
for (int i=0;i<(fldName.length+row);i++){
	
  if ((i%2)==0)
  {%>
  <tr class="browserEvenRow">
   <%}else{ %>
   <tr class="browserOddRow">
   <%}
   if (i<(fldName.length)){%>
     <input type="hidden" name="fldLocal" value="<%=fldName[i]%>">
     <input type="hidden" name="fldLocalCol" value="<%=fldCol[i]%>">
     
     <%}else{%>
     <input type="hidden" name="fldLocal" value="">
     <input type="hidden" name="fldLocalCol" value="">
     <%}%>
      <td width="10%">
   <%if (excludeList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>"   checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="exclude" value="<%=i%>" ></p>
   <%}%>
     </td>
   <td width="15%">
   <%if (startBracList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="startBracket" value="<%=i%>"   checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="startBracket" value="<%=i%>" ></p>
   <%}%>
   </td>
   <%if (i<(flltDtIds.size())){%>
        <input type="hidden" name="fltrDtId" value=<%=flltDtIds.get(i)%>>
	<%}else{%>
	<input type="hidden" name="fltrDtId" value="0">
	<%}%>
	
   <td width="20%"><%
   if (fldName!=null){
   flgMatch="N";
   %>
	<SELECT NAME='fldName' onChange="setType(document.dynadvanced,<%=i%>)"> 
	<%for (counter = 0; counter < (fldName.length) ; counter++){
		name =fldCol[counter];
		if (name==null) name="";
		/*if (name.length()>=32) name=name.substring(0,30);
		if (name.indexOf("?")>=0) 
		name=name.replace('?','q');
		if (name.indexOf("\\")>=0)
		name=name.replace('\\','S');*/
		if (name.length()>0){
		
		
		if ((fldNameSelList.size()>0) && (i<fldNameSelList.size())){
		
		if (name.equals(fldNameSelList.get(i))){
		flgMatch="Y";
		
		if (counter<fldTypeList.size())
		fldType=(String)fldTypeList.get(counter);
		if (fldType==null) fldType="";
		%>
		<OPTION value ='<%=name%>' selected><%=fldName[counter]%></OPTION>;
		<%}else{
		%>
		
		<OPTION value='<%=name%>'><%=fldName[counter]%></OPTION>
		<%}
		}
		else{
		
		%>
		<OPTION value='<%=name%>'><%=fldName[counter]%></OPTION>
		<%}
		}

	} 
	if (flgMatch.equals("N")){%>
		<OPTION value=''  selected><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
		<%}else{%>
	<OPTION value='' ><%=LC.L_Select_AnOption%><%--Select an Option*****--%></OPTION>
	<%}%>
	</SELECT>
	<%}%>
	</td>
   <%if (i<(fldTypeList.size())){%>
        <input type="hidden" name="fldType" value="<%=(fldType.length()==0)?"":fldType%>">
	<input type="hidden" name="fldTypeOrig" value="<%=fldTypeList.get(i)%>">
	<%}else{%>
	<input type="hidden" name="fldType" value="">
	<input type="hidden" name="fldTypeOrig" value="">
	<%}%>

      <td width="10%"> <select size="1" name="scriteria">
      		  <option value="contains" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("contains")){%> Selected <%}}%>><%=LC.L_Contains%><%--Contains*****--%></option>
		  <option value="isnull" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isnull")){%> Selected <%}}%>><%=LC.L_Is_Null%><%--Is Null*****--%></option>
		  <option value="isnotequal" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isnotequal")){%> Selected <%}}%>><%=MC.M_Is_NotEqualTo%><%--Is Not Equal To*****--%></option>
  			<option value="isequalto" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isequalto")){%> Selected <%}}%>><%=LC.L_Is_EqualTo%><%--Is equal to*****--%></option>
			  <option value="start" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("start")){%> Selected <%}}%>><%=LC.L_Begins_With%><%--Begins with*****--%></option>
			  <option value="ends" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("ends")){%> Selected <%}}%>><%=LC.L_Ends_With%><%--Ends with*****--%></option>
			  <option value="isgt" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isgt")){%> Selected <%}}%>><%=LC.L_Is_GreaterThan%><%--Is Greater Than*****--%></option>
			  <option value="islt" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("islt")){%> Selected <%}}%>><%=LC.L_Is_LessThan%><%--Is Less Than*****--%></option>
			  <option value="isgte" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("isgte")){%> Selected <%}}%>><%=MC.M_Is_GtrThanEqual%><%--Is Greater than Equal*****--%></option>
			  <option value="islte" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("islte")){%> Selected <%}}%>><%=MC.M_Is_LessThanEqual%><%--Is Less than Equal*****--%></option>
			  <option value="first" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("first")){%> Selected <%}}%>><%=LC.L_First_Value%><%--First Value*****--%></option>
			  <option value="latest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("latest")){%> Selected <%}}%>><%=LC.L_Latest_Value%><%--Latest Value*****--%></option>
			  <option value="highest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("highest")){%> Selected <%}}%>><%=LC.L_Highest_Val%><%--Highest Value*****--%></option>
			  <option value="lowest" <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if ((scriteriaList.get(i)).equals("lowest")){%> Selected <%}}%>><%=LC.L_Lowest_Value%><%--Lowest Value*****--%></option>
			  <option value='' <%if ((scriteriaList.size()>0) && (i<scriteriaList.size())) {if (((String)scriteriaList.get(i)).length()==0){%> Selected <%}}else{%>Selected<%}%>><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
			  </select></td>
	<td width="20%"><input type="text" name="fltData" maxLength="50"  value=<%=(fldDataList.size()>0 && i<fldDataList.size())?((fldDataList.get(i)==null)?"":fldDataList.get(i)):""%>></td>
	<td width="15%">
	<%if (endBracList.indexOf((new Integer(i)).toString())>-1){%>
   <p align="center"><input type="checkbox" name="endBracket" value="<%=i%>" checked></p>
   <%}else{%>
   <p align="center"><input type="checkbox" name="endBracket" value="<%=i%>"></p>
   <%}%>
	</td>
<%if (i<((fldName.length+row)-1)){ %>
	<td width="10%"><select size="1" name="extend">
	
<option value="and" <%if ((extendList.size()>0) && (i<extendList.size())) {if ((extendList.get(i)).equals("and")){%> Selected <%}}%>><%=LC.L_And%><%--And*****--%></option>
<option value="or" <%if ((extendList.size()>0) && (i<extendList.size())) {if ((extendList.get(i)).equals("or")){%> Selected <%}}%>><%=LC.L_Or%><%--Or*****--%></option>
</select></td>
<%} else {%>
<input name="extend" value="and" type="hidden">
<%}%>
	
   <!--<td width="10%"><select size="1" name="fldOrder">
   <option value="">Select an Option</option>   
<option value="Asc">Ascending</option>
<option value="Desc">Descending</option>
</select></td><td width="65%" ></td>-->
      			
   
  
 </tr>
<%}%>
</table>
<br>
<table width="100%" cellspacing="0" cellpadding="0">
    <td width="70%"></td> 
      <td>     <input name="direction" type="hidden" value="forward">
		<button onClick ="return setFilter(document.dynadvanced);"><%=LC.L_Next%></button>
		<!--<A href=""><img src="../images/jpg/Next.gif" align="absmiddle" border="0"></img></A>-->
      </td> 
      </tr>
  </table>
  <input type="hidden" name="sess" value="keep">
</form>
<%
} // end for (attributes==null)
} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
  </div>


</body>
</html>
