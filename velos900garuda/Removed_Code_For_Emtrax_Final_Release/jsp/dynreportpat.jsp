<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title><%=LC.L_Dynamic_RptCreation%><%--Dynamic Report Creation*****--%></title>
	<%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<SCRIPT>
window.name = "dynrep";
var formWin = null;
 function  validate(formobj){
	formId=formobj.formId.value;
	prevId=formobj.prevId.value;
	if (formId==prevId)
	{}
	else
	{
	alert("<%=MC.M_SelFlds_ForFrm%>");/*alert("Please Select the Fields for Selected Form.");*****/
	return false;
	}
     if (!(validate_col('eSign',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/

	formobj.eSign.focus();
	
	return false;
   }
formobj.action="updatedynrep.jsp";
formobj.target="";
}
function openFormWin(formobj,act) {
formId=formobj.formId.value;
prevId=formobj.prevId.value;
//act="dynpreview.jsp";

if (formId==prevId)
{}
else
{
alert("<%=MC.M_SelFlds_ForFrm%>");/*alert("Please Select the Fields for Selected Form.");*****/
return false;
}
formobj.target="formWin";
formobj.action=act;
formWin = open('','formWin','resizable=1,status=0, width=850,height=550 top=100,left=100,scrollbars=1');
if (formWin && !formWin.closed) formWin.focus();
//formobj.submit();
document.dynreport.submit();
void(0);
}


function openLookup(formobj){
accId=formobj.accId.value;
userId=formobj.userId.value;
studyId=formobj.study1.value;
studyflg=formobj.studyselect[0].checked;
if (studyflg){
	studyList=formobj.studyList.value;	
   filter=" ab.Fk_study in ( "+ studyList + ")  and  ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and " +  
 	 "  ab.record_type <> 'D'  and e.formstat_enddate IS NULL and e.fk_codelst_stat in " + 
	 "  (select pk_codelst from er_codelst where lower(codelst_desc)='active') ";
}
else{
if (studyId.length==0){
alert("<%=MC.M_PlsSel_StdFst%>");/*alert("Please Select a Study First");*****/
return;
 }
 filter=" ab.Fk_study= "+ studyId + "  and  ab.FK_FORMLIB=b.pk_formlib and e.FK_FORMLIB=b.pk_formlib and " +  
 	 "  ab.record_type <> 'D'  and e.formstat_enddate IS NULL and e.fk_codelst_stat in " + 
	 "  (select pk_codelst from er_codelst where lower(codelst_desc)='active') ";
}
//filter1=" F2.FK_ACCOUNT ="+ accId + " AND F2.FK_FORMLIB = F1.PK_FORMLIB and " +   
//" F2.record_type <> 'D'   AND F4.FK_FORMLIB = F1.PK_FORMLIB   and F4.formstat_enddate IS NULL " +  
// "  and lf_displaytype IN ( 'S','SP','A','SA','PA' )  and F4.fk_codelst_stat in (select pk_codelst from er_codelst where lower(codelst_desc)='active') " ;
    //alert(filter);
 
 
   
windowname=window.open("getlookup.jsp?viewId=&viewName=Form Browser&form=dynreport&dfilter=" + filter + "&keyword=formName|VELFORMNAME~formId|VELFORMPK" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=550 top=100,left=100 0, "); 
windowname.focus();	
}
function selectFlds(formobj){
formId=formobj.formId.value;
if (formId.length==0){
alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a form");*****/
return false ;
}

formName=formobj.formName.value;
src=formobj.srcmenu.value;
selectedTab=formobj.selectedTab.value;
repHeader=formobj.repHeader.value;
repFooter=formobj.repFooter.value;
from=formobj.from.value;
windowname=window.open("dynrepselect.jsp?formId=" +formId + "&srcmenu=" + src+"&selectedTab="+selectedTab+"&formName="+formName+"&repHeader="+repHeader+"&repFooter="+repFooter+"&from="+from ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=850,height=550 top=100,left=100 0, "); 
windowname.focus();
}
function setSeqFlag(formobj){
formobj.seqChg.value="Y";

}
function openpatwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=&form=dynreport&dispFld=patId&dataFld=patPk","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
   windowName.focus();
}

</SCRIPT>
<% String src,tempStr="",fldName="",fldCol="";
   int strlen=0,firstpos=-1,secondpos=-1;
src= request.getParameter("srcmenu");
System.out.println(src);

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<body>
<DIV class="formDefault" id="div1"> 
<%
        String formId="",formName="",formType="",fldType="",repIdStr="",fldDisp="",fldWidth="",fldId="";
	String repName="",repHdr="",repFtr="",repType="",repFilter="",selStudy="";
	int seq=0,repId=0;
	ArrayList repColId=new ArrayList();
	ArrayList repCol=new ArrayList();
	ArrayList repColSeq=new ArrayList();
	ArrayList repColWidth=new ArrayList();
	ArrayList repColFormat=new ArrayList();
	ArrayList repColDispName=new ArrayList();
	ArrayList repColName=new ArrayList();
	ArrayList repColType=new ArrayList();
	DynRepDao dyndao=new DynRepDao();
	String mode = request.getParameter("mode");
	
	String selectedTab = request.getParameter("selectedTab");
	ArrayList selectedfld=null;
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
	 selStudy=request.getParameter("study1");
	 if (selStudy==null) selStudy="";
	 if (mode.equals("M")){
	repIdStr=request.getParameter("repId");
	if (repIdStr==null) repIdStr="";
	if (repIdStr.length() > 0)
	repId=(new Integer(repIdStr)).intValue();
	dynrepB.setId(repId);
	dynrepB.getDynRepDetails();
	repName=dynrepB.getRepName();
	repName=(repName==null)?"":repName;
	repHdr=dynrepB.getRepHdr();
	repHdr=(repHdr==null)?"":repHdr;
	repFtr=dynrepB.getRepFtr();
	repFtr=(repFtr==null)?"":repFtr;
	repType=dynrepB.getRepType();
	repType=(repType==null)?"":repType;
	repFilter=dynrepB.getRepFilter();
	repFilter=(repFilter==null)?"":repFilter;
	selStudy=dynrepB.getStudyId();
	selStudy=(selStudy==null)?"":selStudy;
	dyndao=dynrepB.getReportDetails(repIdStr);
	repColId=dyndao.getRepColId();
	repCol=dyndao.getRepCol();
	repColSeq=dyndao.getRepColSeq();
	repColWidth=dyndao.getRepColWidth();
	repColFormat=dyndao.getRepColFormat();
	repColDispName=dyndao.getRepColDispName();
	repColName=dyndao.getRepColName();
	repColType=dyndao.getRepColType();

	}
	 formId= request.getParameter("formId");
	 formName=request.getParameter("formName");
	 formType=request.getParameter("formType");
	 formType=(formType==null)?"":formType;
	 String accId = (String) tSession.getValue("accountId");
	 String userId=	(String) tSession.getValue("userId");
	 String userIdFromSession = (String) tSession.getValue("userId");
	 if( formName==null) formName="";
	 if( formId==null) formId="";
	 int counter=0;

	int studyId=0;
	String studyList="";		
	StringBuffer study=new StringBuffer();	

	StudyDao studyDao = new StudyDao();	

	studyDao.getReportStudyValuesForUsers(userId);



	study.append("<SELECT NAME=study1>") ;
	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
		
		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();
		if (studyList.length()==0) 
		   studyList=studyList+studyId;
		   else 
		   studyList=studyList+","+studyId;
		 if (selStudy.length()>0){
		   if (studyId==((new Integer(selStudy)).intValue()))
		study.append("<OPTION value = "+ studyId +" selected>" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
		else 	
		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
	} else{
	study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
	}

	}

	

	study.append("</SELECT>");

 

	 
%>

<P class="sectionHeadings"> <%=LC.L_DynaRpt_Creat%><%--Dynamic Report >> Create*****--%> </P>

<%


%>

<form name="dynreport"  METHOD=POST  action="updatedynrep.jsp" onsubmit="return validate(document.dynreport);">
<jsp:include page="dynreptabs.jsp" flush="true"/> 
<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="accId" value="<%=accId%>">
<input type=hidden name="userId" value="<%=userIdFromSession%>">
<input type="hidden" name="formId" readonly value="<%=formId%>">
<input type="hidden" name="studyList" readonly value="<%=studyList%>">

<%if (mode.equals("M")){%>
<input type="hidden" name="formType" readonly value="<%=repType%>">

<%}else{%> 
<input type="hidden" name="formType" readonly value="<%=formType%>">
<%}%>
<input type="hidden" name="prevId" readonly value="<%=formId%>">
<input type="hidden" name="seqChg" readonly value="">
<input type="hidden" name="from" readonly value="pat">
<input type="hidden" name="repId" readonly value="<%=repId%>">
<P class="sectionHeadings"> <%=LC.L_SelPat_Popu%><%--Select <%=LC.Pat_Patient%> Population*****--%> </P>
<table width="100%" >
  <tr><td><input name="patselect" type="radio"  value="all" checked><%=LC.L_All_Patients%><%--All <%=LC.Pat_Patients%>*****--%></td></tr>
  <tr><td><input name="patselect" type="radio"  value="all" checked><%=MC.M_PatOn_Std%><%--<%=LC.Pat_Patients%> on a Study*****--%></td><td width="50%"><%=LC.L_Select_Study%><%--Select Study*****--%>&nbsp;<%=study%></td></tr>
  <tr><td width="15%"><input name="patselect" type="radio"> <%=LC.L_Specific_Patient%><%--Specific <%=LC.Pat_Patient%>*****--%></td><td width="50%"><A href="#" onClick="openpatwindow()"><%=LC.L_Select_APat%><%--Select a <%=LC.Pat_Patient%>*****--%></A>&nbsp;&nbsp;<input type="text" name="patId" readonly></td></tr>
  <input type="hidden" name="patPk" readonly>
    </table>
    <br>
    
    <P class="sectionHeadings"> <%=MC.M_Selc_FrmAndFld%><%--Select Form and Fields*****--%>  </P>
    <table width="100%">
    <tr><td width="15%"><%=LC.L_Frm_Name%><%--Form Name*****--%></td><td width="50%"><input type="text" name="formName" readonly value="<%=formName%>"><A href="#" onClick="openLookup(document.dynreport)"><%=LC.L_Select%><%--Select*****--%></A></td></tr>
    </table>
       
    <%if (mode.equals("fldselect")){
    	String select[]=request.getParameterValues("select");
	selectedfld=EJBUtil.strArrToArrayList(select);
	%>
<table width="100%"><tr><td width="80%"></td><td><A href="#" onClick="openFormWin(document.dynreport,'dynadvanced.jsp');"><%=LC.L_Advanced%><%--Advanced*****--%></A></td></tr></table>	
<table width="100%" Border="1">
  <tr>
  <TH width="10%"><%=LC.L_Sequence%><%--Sequence*****--%></TH>
  <TH width="25%"><%=LC.L_Selected_Flds%><%--Selected Fields*****--%></TH>
  <TH width="35%"><%=LC.L_Display_Name%><%--Display Name*****--%></TH>
  <TH width="10%"><%=LC.L_Sort_By%><%--Sort By*****--%></TH>
   <TH width="5%"><%=LC.L_Width%><%--Width*****--%></TH>
     </tr>
    <%for(int i=0;i<selectedfld.size();i++){
    		tempStr=(String)selectedfld.get(i);
		strlen = tempStr.length();		
        	firstpos = (tempStr).indexOf("[VEL]",1);	
		fldName = (tempStr).substring(0,firstpos);
		secondpos=(tempStr).indexOf("[VEL]",firstpos+5);
		fldCol = (tempStr).substring(firstpos+5,secondpos);
		fldType=(tempStr).substring(secondpos+5,strlen);;
		seq=seq+10;
		%>
		
<tr><td width="10%"><p align="Center"><input type="text" name="fldSeq" value="<%=seq%>" size=1 maxlength=5 onChange="setSeqFlag(document.dynreport)"></p></td><td width="25%"><%=StringUtil.decodeString(fldName)%></td>
    <td width="35%"><input type="text" name="fldDispName" value="<%=StringUtil.decodeString(fldName)%>" size="35"></td>
    <td width="10%"><select size="1" name="fldOrder">
 <option value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>   
<option value="Asc"><%=LC.L_Ascending%><%--Ascending*****--%></option>
<option value="Desc"><%=LC.L_Descending%><%--Descending*****--%></option>
</select></td>
<td width="5%"><input type="text" name="fldWidth" size="2">%</td>
     <td width="60%"><input type="hidden" name="fldCol" value=<%=fldCol%>></td>
     <input type="hidden" name="fldName" value="<%=StringUtil.decodeString(fldName)%>">
     <input type="hidden" name="fldType" value="<%=fldType%>">
     <input type="hidden" name="fldFormat" value="">
     
         
    </tr>
    	
    
    <%}//end for loop
    
    %>    
    
    <input name="repFilter" type="hidden" size="100" value="<%=repFilter%>">
    </table>
    <table width="100%">
    <tr><td width="25%"></td><td width="25%"><A href="#" onClick="selectFlds(document.dynreport)"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>
    <td><A href="#" onClick="openFormWin(document.dynreport,'dynpreview.jsp');"><%=LC.L_Preview%><%--Preview*****--%></td></tr>
      <!--  <td><A href="#" onClick="return openFormWin(document.dynreport);document.dynreport.submit();void(0)">Preview</td></tr>-->
     </table>
     
    <% } else if (mode.equals("M")){%>
<table width="100%"><tr><td width="80%"></td><td><A href="#" onClick="openFormWin(document.dynreport,'dynadvanced.jsp');"><%=LC.L_Advanced%><%--Advanced*****--%></A></td></tr></table>	
<table width="100%" Border="1">
  <tr>
  <TH width="10%"><%=LC.L_Sequence%><%--Sequence*****--%></TH>
  <TH width="25%"><%=LC.L_Selected_Flds%><%--Selected Fields*****--%></TH>
  <TH width="35%"><%=LC.L_Display_Name%><%--Display Name*****--%></TH>
  <TH width="10%"><%=LC.L_Sort_By%><%--Sort By*****--%></TH>
   <TH width="5%"><%=LC.L_Width%><%--Width*****--%></TH>
     </tr>
    <%for(int i=0;i<repColId.size();i++){
    				
        	fldName = (String)repColName.get(i);
		fldName=(fldName==null)?"":fldName;
		fldCol = (String) repCol.get(i);
		fldCol=(fldCol==null)?"":fldCol;
		fldType=(String) repColType.get(i);
		fldType=(fldType==null)?"":fldType;
		seq= EJBUtil.stringToNum((String) repColSeq.get(i));
		fldDisp=(String) repColDispName.get(i);
		fldDisp=(fldDisp==null)?"":fldDisp;
		fldWidth=(String) repColWidth.get(i);
		fldWidth=(fldWidth==null)?"":fldWidth;
		System.out.println("fldWidth" +  fldWidth.length());
		fldId=(String)repColId.get(i);
		%>
	<input type="hidden" name="colId" value="<%=fldId%>">	
<tr><td width="10%"><p align="Center"><input type="text" name="fldSeq" value="<%=seq%>" size=1 maxlength=5 onChange="setSeqFlag(document.dynreport)"></p></td><td width="25%"><%=(fldName)%></td>
    <td width="35%"><input type="text" name="fldDispName" value="<%=fldDisp%>" size="35"></td>
    <td width="10%"><select size="1" name="fldOrder">
 <option value=""><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>   
<option value="Asc"><%=LC.L_Ascending%><%--Ascending*****--%></option>
<option value="Desc"><%=LC.L_Descending%><%--Descending*****--%></option>
</select></td>
<td width="5%"><input type="text" name="fldWidth" size="2" value="<%=fldWidth%>">%</td>
     <td width="60%"><input type="hidden" name="fldCol" value=<%=fldCol%>></td>
     <input type="hidden" name="fldName" value="<%=fldName%>">
     <input type="hidden" name="fldType" value="<%=fldType%>">
     <input type="hidden" name="fldFormat" value="">
     
         
    </tr>
    	
    
    <%}//end for loop
    
    %>    
    
    <input name="repFilter" type="hidden" size="100" value="<%=repFilter%>">
    </table>
    <table width="100%">
    <tr><td width="25%"></td><td width="25%"><A href="#" onClick="selectFlds(document.dynreport)"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td>
    <td><A href="#" onClick="openFormWin(document.dynreport,'dynpreview.jsp');"><%=LC.L_Preview%><%--Preview*****--%></td></tr>
      <!--  <td><A href="#" onClick="return openFormWin(document.dynreport);document.dynreport.submit();void(0)">Preview</td></tr>-->
     </table>
     
    <%}else {%>
    <table width="100%">
    <tr><td width="15%"><%=LC.L_Fields%><%--Fields*****--%></td><td width="50%"><%=MC.M_NoFldsSelected%><%--No Fields Selected*****--%>&nbsp;&nbsp;<A href="#" onClick="selectFlds(document.dynreport)"><%=LC.L_Select_Flds%><%--Select Fields*****--%></A></td></tr>
    </table>
    
    <%}%>
    <P class="sectionHeadings"> <%=LC.L_Format_YourRpt%><%--Format Your Report*****--%>  </P>
    <table>
    <tr><td><%=LC.L_Rpt_Name%><%--Report Name*****--%></td><td><input type="text" name="repName" value="<%=repName%>"></td></tr>
    <tr><td width="15%"><%=LC.L_Rpt_Header%><%--Report Header*****--%></td><td width="50%"><input type="text" name="repHeader" value="<%=repHdr%>">
    &nbsp;&nbsp;<%=LC.L_Rpt_Footer%><%--Report Footer*****--%>&nbsp;&nbsp;<input type="text" name="repFooter" value="<%=repFtr%>"></td>
    <%if (selectedfld!=null){%>
    <input name="rows" type="hidden" value="<%=selectedfld.size()%>">
    <%}else{%>
    <input name="rows" type="hidden" value="<%=repColId.size()%>">
    <%}%>
    </tr>
    </table>
    <table width="100%" >
   <tr><td width="15%">

		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>

	   </td>

	   <td width="50%">

		<input type="password" name="eSign" maxlength="8">

	   </td>

	</tr>
<tr><td></td>
<td>
		<button type="submit"><%=LC.L_Submit%></button>
</td>

</tr>

	</table>

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>

  <%
}


%>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>
  <div class ="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
  </div>


</body>
</html>
