/*

 * Classname : 				PortalDesignJB

 * 

 * Version information : 	1.0

 *

 * Date 					04/19/07

 * 

 * Copyright notice: 		Velos Inc

 * 

 * Author 					Manimaran

 */


package com.velos.eres.web.portalDesign;


/* IMPORT STATEMENTS */

import com.velos.eres.business.common.PortalDesignDao;
import com.velos.eres.business.portalDesign.impl.PortalDesignBean;
import com.velos.eres.service.portalDesignAgent.PortalDesignAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import java.sql.*;
import java.util.Hashtable;
/* END OF IMPORT STATEMENTS */


/**
 * 
 * Client side bean for Portal Design module. This class is used for any kind of
 * manipulation for the PortalDesign.
 * 
 * @author Manimaran
 * 
 * @version 1.0, 04/19/2007
 * 
 */
public class PortalDesignJB {
	
	private int portalModId;
	
	private String fkPortalId;
	
	private String fkId;
	
	private String portalModType;
	
	private String portalModAlign;
	
	private String portalModFrom;
	
	private String portalModTo;
	
	private String portalModFromUnit;
	
	private String portalModToUnit;
	
	private String portalFormAfterResp;
	 /**
     * 
     * 
     * The creator of the field type : CREATOR
     * 
     */

    private String creator;

    /**
     * 
     * The id of the user who last modified the field : LAST_MODIFIED_BY
     * 
     */

    private String modifiedBy;

    /**
     * 
     * The IP Address of the site which made the change : IP_ADD
     * 
     */

    private String ipAdd;
	
    
//  GETTERS AND SETTERS

    /**
     * 
     * 
     * @return portalModId
     */
    public int getPortalModId() {
        return this.portalModId;
    }

    /**
     * 
     * 
     * @param portalModId
     */
    public void setPortalModId(int portalModId) {
        this.portalModId = portalModId;
    }
    
    
    /**
     * 
     * 
     * @return fkPortalId
     */
    public String getFkPortalId() {
        return this.fkPortalId;
    }

    /**
     * 
     * 
     * @param fkId
     */
    public void setFkPortalId(String fkPortalId) {
        this.fkPortalId = fkPortalId;
    }
    /**
     * 
     * 
     * @return fkId
     */
    public String getFkId() {
        return this.fkId;
    }

    /**
     * 
     * 
     * @param fkId
     */
    public void setFkId(String fkId) {
        this.fkId = fkId;
    }
    
    /**
     * 
     * 
     * @return portalModType
     */
    public String getPortalModType() {
        return this.portalModType;
    }

    /**
     * 
     * 
     * @param portalModType
     */
    public void setPortalModType(String portalModType) {
    	
        this.portalModType = portalModType;
    }
    
    /**
     * 
     * 
     * @return portalModAlign
     */
    public String getPortalModAlign() {
        return this.portalModAlign;
    }

    /**
     * 
     * 
     * @param portalModAlign
     */
    public void setPortalModAlign(String portalModAlign) {
        this.portalModAlign = portalModAlign;
    }
    
    /**
     * 
     * 
     * @return portalModFrom
     */
    public String getPortalModFrom() {
        return this.portalModFrom;
    }

    /**
     * 
     * 
     * @param portalModFrom
     */
    public void setPortalModFrom(String portalModFrom) {
        this.portalModFrom = portalModFrom;
    }
    
    /**
     * 
     * 
     * @return portalModTo
     */
    public String getPortalModTo() {
        return this.portalModTo;
    }

    /**
     * 
     * 
     * @param portalModTo
     */
    public void setPortalModTo(String portalModTo) {
        this.portalModTo = portalModTo;
    }      
    
    /**
     * 
     * 
     * @return portalModFromUnit
     */
    public String getPortalModFromUnit() {
        return this.portalModFromUnit;
    }

    /**
     * 
     * 
     * @param portalModFromUnit
     */
    public void setPortalModFromUnit(String portalModFromUnit) {
        this.portalModFromUnit = portalModFromUnit;
    }
    
    /**
     * 
     * 
     * @return portalModToUnit
     */
    public String getPortalModToUnit() {
        return this.portalModToUnit;
    }

    /**
     * 
     * 
     * @param portalModToUnit
     */
    public void setPortalModToUnit(String portalModToUnit) {
        this.portalModToUnit = portalModToUnit;
    }
    
    /**
     * 
     * 
     * @return portalFormAfterResp
     */
    public String getPortalFormAfterResp() {
        return this.portalFormAfterResp;
    }

    /**
     * 
     * 
     * @param portalModTo
     */
    public void setPortalFormAfterResp(String portalFormAfterResp) {
        this.portalFormAfterResp = portalFormAfterResp;
    }
    //////////
    /**
     * 
     * 
     * @return creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * 
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 
     * 
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * 
     * 
     * @param modifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * 
     * 
     * @return ipAdd
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * 
     * 
     * @param ipAdd
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

////////////////    
    
    // END OF THE GETTER AND SETTER METHODS
    
    /**
     * Constructor
     * 
     * @param portalId:
     *            Unique Id constructor
     * 
     */

    public PortalDesignJB(int portalModId) {
    	setPortalModId(portalModId);
    }

    /**
     * Default Constructor
     */

    public PortalDesignJB() {
        Rlog.debug("PortalDesign", "PortalDesignJB.PortalDesignJB() ");
    }
    
    
    public PortalDesignJB(int portalModId, String fkPortalId, String fkId,
            String portalModType, String portalModAlign, String portalModFrom,
            String portalModTo, String portalModFromUnit, String portalModToUnit,
            String portalFormAfterResp, String creator, String modifiedBy, String ipAdd) {
    
        setPortalModId(portalModId);
    	setFkPortalId(fkPortalId);
    	setFkId(fkId);
    	setPortalModType(portalModType);
    	setPortalModAlign(portalModAlign);
    	setPortalModFrom(portalModFrom);
    	setPortalModTo(portalModTo);
    	setPortalModFromUnit(portalModFromUnit);
    	setPortalModToUnit(portalModToUnit);
    	setPortalFormAfterResp(portalFormAfterResp);
    	setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        
        Rlog.debug("PortalDesign", "PortalDesignJB.PortalDesignJB(all parameters)");
    }
    
    /**
     * Retrieves the details of an portal in PortalDesignStateKeeper Object
     * 
     * @return PortalDesignStateKeeper consisting of the portal design details
     * @see PortalDesignStateKeeper
     */
    public PortalDesignBean getPortalDesignDetails() {
    	PortalDesignBean pdsk = null;
        try {
            PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
            pdsk = portalDesignAgentRObj.getPortalDesignDetails(this.portalModId);
            Rlog.debug("portalDesign", "In PortalDesignJB.getPortalDesignDetails() " + pdsk);
        } catch (Exception e) {
            Rlog
                    .fatal("portalDesign", "Error in getPortalDesignDetails() in PortalDesignJB "
                            + e);
        }
        if (pdsk != null) {
            this.portalModId = pdsk.getPortalModId();
            this.fkPortalId = pdsk.getFkPortalId();
            this.fkId = pdsk.getFkId();
            this.portalModType = pdsk.getPortalModType();
            this.portalModAlign = pdsk.getPortalModAlign();
            this.portalModFrom = pdsk.getPortalModFrom();
            this.portalModTo = pdsk.getPortalModTo();
            this.portalModFromUnit = pdsk.getPortalModFromUnit();
            this.portalModToUnit = pdsk.getPortalModToUnit();
            this.portalFormAfterResp = pdsk.getPortalFormAfterResp();
            this.creator = pdsk.getCreator();
            this.modifiedBy = pdsk.getModifiedBy();
            this.ipAdd = pdsk.getIpAdd();
            
        }
        return pdsk;
    }
    
    public int setPortalDesignDetails() {
        int output = 0;
        try {
        	PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
            output = portalDesignAgentRObj.setPortalDesignDetails(this.createPortalDesignStateKeeper());
        	           
            this.setPortalModId(output);
            Rlog.debug("portalDesign", "PortalJB.setPortalDesignDetails()");

        }catch (Exception e) {
            Rlog
                    .fatal("portalDesign", "Error in setPortalDesignDetails() in PortalDesignJB "
                            + e);
          return -2;
        }
        return output;
    }
    
    public int updatePortalDesign() {
        int output;
        try {
        	PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
            output = portalDesignAgentRObj.updatePortalDesign(this
                    .createPortalDesignStateKeeper());
        } catch (Exception e) {
            Rlog.debug("portalDesign",
                    "EXCEPTION IN SETTING PORTAL DESIGN DETAILS TO  SESSION BEAN"
                            + e.getMessage());
            e.printStackTrace();

            return -2;
        }
        return output;
    }
   
     
    public PortalDesignBean createPortalDesignStateKeeper() {
        Rlog.debug("portalDesign", "PortalDesignJB.createPortalDesignStateKeeper ");
        
         
        
        return new PortalDesignBean(this.portalModId, this.fkPortalId, this.fkId,
                this.portalModType, this.portalModAlign, this.portalModFrom,
                this.portalModTo, this.portalModFromUnit, this.portalModToUnit,
                this.portalFormAfterResp, this.creator,  this.modifiedBy,  this.ipAdd);
    }
    
    
    
    public PortalDesignDao getPortalModValues(int portalId) {
    	PortalDesignDao pdDao = new PortalDesignDao();
        try {
            Rlog.debug("portalDesign", "PortalDesignJB.getPortalModValues starting");
            PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
            pdDao = portalDesignAgentRObj.getPortalModValues(portalId);

            return pdDao;
        } catch (Exception e) {
            Rlog.fatal("portalDesign",
                    "getPortalModValues(int portalId) in PortalDesignJB "
                            + e);
        }
        return pdDao;
    }
//  JM: 17Aug2007:  issue #3061 
    public int deletePortalModules(int modId){
    	
    	  int output = 0;
    	  try {
              Rlog.debug("portalDesign", "PortalDesignJB.deletePortalModules starting");
              PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
              output = portalDesignAgentRObj.deletePortalModules(modId);
              
          } catch (Exception e) {
              Rlog.fatal("portalDesign","deletePortalModules(int modId) in PortalDesignJB " + e);
          }
          return output; 
    	
    	
    }
    
 // Overloaded for INF-18183 ::: AGodara
    public int deletePortalModules(int modId,Hashtable<String, String> auditInfo){
    	
  	  int output = 0;
  	  try {
            Rlog.debug("portalDesign", "PortalDesignJB.deletePortalModules starting");
            PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
            output = portalDesignAgentRObj.deletePortalModules(modId,auditInfo);
            
        } catch (Exception e) {
            Rlog.fatal("portalDesign","deletePortalModules(int modId) in PortalDesignJB " + e);
        }
        return output; 
  	
  	
  }
    
//  JM: 20Aug2007: issue #3072
    public int deletePortalDesignData(int portalID){
    	
    	  int output = 0;
    	  try {
              Rlog.debug("portalDesign", "PortalJB.deletePortalDesignData starting...");
              PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
              output = portalDesignAgentRObj.deletePortalDesignData(portalID);
              
          } catch (Exception e) {
              Rlog.fatal("portalDesign","deletePortalDesignData(int portalID) in PortalJB " + e);
          }
          return output; 
    	
    	
    } 
    
 // Overloaded for INF-18183 ::: Akshi
    public int deletePortalDesignData(int portalID,Hashtable<String, String> args){
    	
  	  int output = 0;
  	  try {
            Rlog.debug("portalDesign", "PortalJB.deletePortalDesignData starting...");
            PortalDesignAgentRObj portalDesignAgentRObj = EJBUtil.getPortalDesignAgentHome();
            output = portalDesignAgentRObj.deletePortalDesignData(portalID,args);
            
        } catch (Exception e) {
            Rlog.fatal("portalDesign","deletePortalDesignData(int portalID) in PortalJB " + e);
        }
        return output;   	  	
  } 
       
}
