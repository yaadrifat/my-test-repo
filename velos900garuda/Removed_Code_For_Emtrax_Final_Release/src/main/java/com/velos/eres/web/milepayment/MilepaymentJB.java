/*
 * Classname : MilepaymentJB
 * 
 * Version information: 1.0
 *
 * Date: 05/30/2002
 * 
 * Copyright notice: Velos, Inc
 */

package com.velos.eres.web.milepayment;

/* Import Statements */
import java.util.Hashtable;
import com.velos.eres.business.common.MilepaymentDao;
import com.velos.eres.business.milepayment.impl.MilepaymentBean;
import com.velos.eres.service.milepaymentAgent.MilepaymentAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * Client side bean for milepayment
 * 
 * @author Sonika Talwar
 * @version 1.0 05/30/2002
 */

public class MilepaymentJB {
    /**
     * the Milepayment Id
     */
    private int id;

    /**
     * the Study Id to which Milepayment is related
     */
    private String milepaymentStudyId;

    /**
     * the Milepayment Milestone Id
     */
    private String milepaymentMileId;

    /**
     * the Milepayment Date
     */
    private String milepaymentDate;

    /**
     * the Milepayment Desc
     */
    private String milepaymentDesc;

    /**
     * the Milepayment Amount
     */
    private String milepaymentAmount;

    /**
     * the Milepayment Delflag
     */
    private String milepaymentDelFlag;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * Comments
     */
    private String milepaymentComments ;
    
    /*
     * Payment type
     */
    private String milepaymentType ;
    
    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMilepaymentStudyId() {
        return this.milepaymentStudyId;
    }

    public void setMilepaymentStudyId(String milepaymentStudyId) {
        this.milepaymentStudyId = milepaymentStudyId;
        Rlog.debug("milepayment", "study Id " + milepaymentStudyId);
    }

    public String getMilepaymentMileId() {
        return this.milepaymentMileId;
    }

    public void setMilepaymentMileId(String milepaymentMileId) {
        this.milepaymentMileId = milepaymentMileId;
    }

    public String getMilepaymentDate() {
        return this.milepaymentDate;
    }

    public void setMilepaymentDate(String milepaymentDate) {
        this.milepaymentDate = milepaymentDate;
    }

    public String getMilepaymentAmount() {
        return this.milepaymentAmount;
    }

    public void setMilepaymentAmount(String milepaymentAmount) {
        this.milepaymentAmount = milepaymentAmount;
    }

    public String getMilepaymentDesc() {
        return this.milepaymentDesc;
    }

    public void setMilepaymentDesc(String milepaymentDesc) {
        this.milepaymentDesc = milepaymentDesc;
    }

    public String getMilepaymentDelFlag() {
        return this.milepaymentDelFlag;
    }

    public void setMilepaymentDelFlag(String milepaymentDelFlag) {
        this.milepaymentDelFlag = milepaymentDelFlag;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // end of getter and setter methods

    /**
     * Constructor
     * 
     * @param milepaymentId
     */
    public MilepaymentJB(int milepaymentId) {
        setId(milepaymentId);
    }

    /**
     * Default Constructor
     * 
     */
    public MilepaymentJB() {
        Rlog.debug("milepayment", "milepaymentJB.milepaymentJB() ");
    }

    /**
     * Full Argument Constructor
     */
    public MilepaymentJB(int id, String milepaymentStudyId,
            String milepaymentMileId, String milepaymentDate,
            String milepaymentAmount, String milepaymentDesc,
            String milepaymentDelFlag, String creator, String modifiedBy,
            String ipAdd, String milepaymentComments,String milepaymentType) {

        setId(id);
        setMilepaymentStudyId(milepaymentStudyId);
        setMilepaymentMileId(milepaymentMileId);
        setMilepaymentAmount(milepaymentAmount);
        setMilepaymentDesc(milepaymentDesc);
        setMilepaymentDate(milepaymentDate);
        setMilepaymentDelFlag(milepaymentDelFlag);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setMilepaymentComments(milepaymentComments);
        setMilepaymentType(milepaymentType);
        Rlog
                .debug("milepayment",
                        "milepaymentJB.milepaymentJB(all parameters)");
    }

    /**
     * Calls getMilepaymentDetails of milepayment Session Bean:
     * milepaymentAgentBean
     * 
     * @returns milepaymentStateKeeper object
     * @see milepaymentAgentBean
     */
    public MilepaymentBean getMilepaymentDetails() {
        MilepaymentBean msk = null;

        try {

            MilepaymentAgentRObj milepaymentAgent = EJBUtil
                    .getMilepaymentAgentHome();
            msk = milepaymentAgent.getMilepaymentDetails(this.id);
            Rlog.debug("milepayment",
                    "milepaymentJB.getMilepaymentDetails() milepaymentStateKeeper "
                            + msk);
        } catch (Exception e) {
            Rlog.debug("milepayment",
                    "Error in getMilepaymentDetails() in UserJB " + e);
        }
        if (msk != null) {
            this.id = msk.getId();
            this.milepaymentStudyId = msk.getMilepaymentStudyId();
            this.milepaymentMileId = msk.getMilepaymentMileId();
            this.milepaymentAmount = msk.getMilepaymentAmount();
            this.milepaymentDesc = msk.getMilepaymentDesc();
            this.milepaymentDate = msk.getMilepaymentDate();
            this.milepaymentDelFlag = msk.getMilepaymentDelFlag();
            this.creator = msk.getCreator();
            this.modifiedBy = msk.getModifiedBy();
            this.ipAdd = msk.getIpAdd();
            setMilepaymentComments(msk.getMilepaymentComments());
            setMilepaymentType(msk.getMilepaymentType());
        }
        return msk;
    }

    /**
     * Calls setMilepaymentDetails of milepayment Session Bean:
     * milepaymentAgentBean
     * 
     * @see milepaymentAgentBean
     */
    public void setMilepaymentDetails() {
        try {
            Rlog.debug("milepayment",
                    "In milepaymentJB.setMilepaymentDetails()");
            Rlog.debug("milepayment", "Study Id in JB "
                    + getMilepaymentStudyId());

            MilepaymentAgentRObj milepaymentAgent = EJBUtil
                    .getMilepaymentAgentHome();
            this.setId(milepaymentAgent.setMilepaymentDetails(this
                    .createMilepaymentStateKeeper()));
            Rlog.debug("milepayment",
                    "Out milepaymentJB.setMilepaymentDetails()");
        } catch (Exception e) {
            Rlog.debug("milepayment",
                    "Error in setMilepaymentDetails() in milepaymentJB " + e);
        }
    }

    /**
     * Calls updateDetails of milepayment Session Bean: milepaymentAgentBean
     * 
     * @return int 0 if successful, -2 for exception
     * @see milepaymentAgentBean
     */
    public int updateMilepayment() {
        int output;
        try {

            MilepaymentAgentRObj milepaymentAgentRObj = EJBUtil
                    .getMilepaymentAgentHome();
            output = milepaymentAgentRObj.updateMilepayment(this
                    .createMilepaymentStateKeeper());
        } catch (Exception e) {
            Rlog.debug("milepayment",
                    "EXCEPTION IN SETTING MILEPAYMENT DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int updateMilepayment(Hashtable<String, String> auditInfo) {
        int output;
        try {
            MilepaymentAgentRObj milepaymentAgentRObj = EJBUtil
                    .getMilepaymentAgentHome();
            output = milepaymentAgentRObj.updateMilepayment(this
                    .createMilepaymentStateKeeper(),auditInfo);
        } catch (Exception e) {
            Rlog.debug("milepayment",
                    "EXCEPTION IN SETTING MILEPAYMENT DETAILS TO  SESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    /**
     * Creates a milepaymentStateKeeper object with the attribute vales of the
     * bean
     * 
     * @return milepaymentStateKeeper
     */
    public MilepaymentBean createMilepaymentStateKeeper() {
        Rlog
                .debug("milepayment",
                        "milepaymentJB.createMilepaymentStateKeeper ");

        return new MilepaymentBean(id, milepaymentStudyId, milepaymentMileId,
                milepaymentDate, milepaymentAmount, milepaymentDesc,
                milepaymentDelFlag, creator, modifiedBy, ipAdd,milepaymentComments, milepaymentType);
    }

    public MilepaymentDao getMilepaymentReceived(int studyId) {
        MilepaymentDao milepaymentDao = new MilepaymentDao();
        try {
            Rlog.debug("milepayment",
                    "MilepaymentJB.getMilepaymentReceived starting");

            MilepaymentAgentRObj milepaymentAgentRObj = EJBUtil
                    .getMilepaymentAgentHome();
            milepaymentDao = milepaymentAgentRObj
                    .getMilepaymentReceived(studyId);
            Rlog.debug("milepayment",
                    "MilepaymentJB.getMilepaymentReceived after Dao");
            return milepaymentDao;
        } catch (Exception e) {
            Rlog.fatal("milepayment",
                    "Error in getMilepaymentReceived(int studyId) in MilepaymentJB "
                            + e);
            return null;
        }
    }

	public String getMilepaymentComments() {
		return milepaymentComments;
	}

	public void setMilepaymentComments(String milepaymentComments) {
		this.milepaymentComments = milepaymentComments;
	}

	public String getMilepaymentType() {
		return milepaymentType;
	}

	public void setMilepaymentType(String milepaymentType) {
		this.milepaymentType = milepaymentType;
	}

} // end of class
