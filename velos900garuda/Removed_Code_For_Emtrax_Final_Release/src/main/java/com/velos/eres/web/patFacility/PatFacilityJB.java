/*
 * Classname : 				PatFacilityJB
 * 
 * Version information : 	1.0
 *
 * Date 					09/22/2005
 * 
 * Copyright notice: 		Velos Inc
 * 
 * Author 					Sonia Abrol
 */

package com.velos.eres.web.patFacility;

/* IMPORT STATEMENTS */

import javax.persistence.Column;

import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.patFacility.impl.PatFacilityBean;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.person.PersonJB;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for patient Facility module.
 * 
 * @author Sonia Abrol
 * @version 1.0, 09/22/2005
 */

public class PatFacilityJB {

       /**
     * the Patient Facility Primary Key
     */
    private int id;

    /**
     * the Patient Facility Id
     */
    private String patientFacilityId;

    /**
     * the patient Primary Key
     */
    private String patientPK;

    /**
     * the patient Site
     */
    private String patientSite;

    /**
     * the registration date
     */
    private String regDate	;

    /**
     * the id of the user who registered the patient
     */
    private String registeredBy	;
    
    /**
     * the person who registered the patient (and is not a user in the system)
     */
    private String registeredByOther;
    

    /**
     * a comma separated string of specialty ids that have access to the patient. 
     <br> If no specialty is selected, that means all the users of the registering facility can access the patient 
     */
    private String patSpecialtylAccess;
    
    /**
     * Flag to indicate the access level of the registering site's users
     * <br> Currently we support only two levels:
     * <br> 0 : No Access
     * <br> 7 : Full Access
     */
    private String patAccessFlag;
    
    /**
     * Flag to indicate if this site is the default site for the patient
     * <br> Possible Values:
     * <br> 0 : Not Default
     * <br> 1 : Default
     */
    private String isDefault;
       
    /**
     * the id of the user who created the record
     */
    private String creator;

    /**
     * Id of the user who last modified the record
     */
    private String lastModifiedBy;

    /**
     * the IP address of the user who created or modified the record
     */
    private String iPAdd;

	/**
	 * Flag to indicate if this is a read only record <br>
	 * Possible Values: <br>
	 * 0 : Not Readonly<br>
	 * 1 : ReadOnly
	 */
	public String isReadOnly;

	
    // GETTER SETTER METHODS

	public String getIsReadOnly() {
		return isReadOnly;
	}

	public void setIsReadOnly(String isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

    
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public String getPatientPK() {
        return this.patientPK;

    }

    public void setPatientPK(String patPK) {
            this.patientPK = patPK;

    }
    
    	/**
	 * Returns the value of isDefault.
	 */
	
	public String getIsDefault()
	{
		return isDefault;
	}

	/**
	 * Sets the value of isDefault.
	 * @param isDefault The value to assign isDefault.
	 */
	public void setIsDefault(String isDefault)
	{
		this.isDefault = isDefault;
	}

	/**
	 * Returns the value of patAccessFlag.
	 */
	
	public String getPatAccessFlag()
	{
		return patAccessFlag;
	}

	/**
	 * Sets the value of patAccessFlag.
	 * @param patAccessFlag The value to assign patAccessFlag.
	 */
	public void setPatAccessFlag(String patAccessFlag)
	{
		this.patAccessFlag = patAccessFlag;
	}

	/**
	 * Returns the value of patSpecialtylAccess.
	 */
	
	public String getPatSpecialtylAccess()
	{
		return patSpecialtylAccess;
	}

	/**
	 * Sets the value of patSpecialtylAccess.
	 * @param patSpecialtylAccess The value to assign patSpecialtylAccess.
	 */
	public void setPatSpecialtylAccess(String patSpecialtylAccess)
	{
		this.patSpecialtylAccess = patSpecialtylAccess;
	}

	/**
	 * Returns the value of registeredByOther.
	 */
	
	public String getRegisteredByOther()
	{
		return registeredByOther;
	}

	/**
	 * Sets the value of registeredByOther.
	 * @param registeredByOther The value to assign registeredByOther.
	 */
	public void setRegisteredByOther(String registeredByOther)
	{
		this.registeredByOther = registeredByOther;
	}

	/**
	 * Returns the value of registeredBy.
	 */
	
	public String getRegisteredBy()
	{
		return registeredBy;
	}

	/**
	 * Sets the value of registeredBy.
	 * @param registeredBy The value to assign registeredBy.
	 */
	public void setRegisteredBy(String registeredBy)
	{
		this.registeredBy = registeredBy;
	}

	/**
	 * Returns the value of regDate.
	 */
	 
	public String getRegDate()
	{
		return regDate;
	}

	/**
	 * Sets the value of regDate.
	 * @param regDate The value to assign regDate.
	 */
	public void setRegDate(String regDate)
	{
		this.regDate = regDate;
	}

		
	/**
	 * Returns the value of patientSite.
	 */
	
	public String getPatientSite()
	{
		return patientSite;
	}

	/**
	 * Sets the value of patientSite.
	 * @param patientSite The value to assign patientSite.
	 */
	public void setPatientSite(String patientSite)
	{
		this.patientSite = patientSite;
	}


    
    public String  getCreator() {
        return this.creator;

    }

    public void setCreator(String creator) {
                this.creator = creator;

    }

    
    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIPAdd() {
        return this.iPAdd;
    }

    public void setIPAdd(String iPAdd) {
        this.iPAdd = iPAdd;
    }

    	/**
	 * Returns the value of patientFacilityId.
	 */
	public String getPatientFacilityId()
	{
		return patientFacilityId;
	}

	/**
	 * Sets the value of patientFacilityId.
	 * @param patientFacilityId The value to assign patientFacilityId.
	 */
	public void setPatientFacilityId(String patientFacilityId)
	{
		this.patientFacilityId = patientFacilityId;
	}


    // END OF GETTER SETTER METHODS

    /**
     * Defines an PatFacilityJB object with the specified category Id
     */
    public PatFacilityJB(int id) {
        setId(id);
    }

    /**
     * Defines an PatFacilityJB object with default values for fields
     */
    public PatFacilityJB() {
        Rlog.debug("PatFacility", "PatFacilityJB.PatFacilityJB() ");
    };

    /**
     * Defines an PatFacilityJB object with the specified values for the fields
     * 
     */
     /** Full Argument constructor*/
    
     public PatFacilityJB(int id,String patientFacilityId, String patientPK, String patientSite,
    String regDate,String registeredBy, String registeredByOther, String patSpecialtylAccess,String patAccessFlag ,
    String isDefault ,String creator, String lastModifiedBy, String iPAdd, String isReadOnly) {
        super();
        setId(id);
        setPatientFacilityId(patientFacilityId); 
		setPatientPK(patientPK);
		setPatientSite(patientSite);
		setRegDate(regDate);
		setRegisteredBy(registeredBy);
		setRegisteredByOther(registeredByOther);
		setPatSpecialtylAccess(patSpecialtylAccess);
		setPatAccessFlag(patAccessFlag);
        setIsDefault(isDefault);
        setCreator(creator);
		setLastModifiedBy(lastModifiedBy);
		setIPAdd(iPAdd);
		setIsReadOnly(isReadOnly);
    }
    

    /**
     * Retrieves the details of an category in PatFacilityStateKeeper Object
     * 
     * @return PatFacilityStateKeeper consisting of the category details
     * @see PatFacilityStateKeeper
     */
    public PatFacilityBean getPatFacilityDetails() {
        PatFacilityBean pBean = null;
        try {
            PatFacilityAgentRObj patFacilityAgentRObj = EJBUtil.getPatFacilityAgentHome();
            pBean = patFacilityAgentRObj.getPatFacilityDetails(this.id);
            Rlog.debug("PatFacility", "In PatFacilityJB.getPatFacilityDetails() " + pBean);
        } catch (Exception e) {
            Rlog.fatal("PatFacility", "Error in getPatFacilityDetails() in PatFacilityJB "
                            + e);
        }
        if (pBean != null) {
            setPatientPK(pBean.getPatientPK());
	    setIsDefault(pBean.getIsDefault());
	    setPatAccessFlag(pBean.getPatAccessFlag());
	    setPatSpecialtylAccess(pBean.getPatSpecialtylAccess());
	    setRegisteredByOther(pBean.getRegisteredByOther());
	    setRegisteredBy(pBean.getRegisteredBy());
	    setRegDate(pBean.returnRegDate());
	    setPatientSite(pBean.getPatientSite());
	    setPatientFacilityId(pBean.getPatientFacilityId());
	    setCreator(pBean.getCreator());
         setLastModifiedBy(pBean.getLastModifiedBy());
        setIPAdd(pBean.getIPAdd());
        
        setIsReadOnly(pBean.getIsReadOnly());
        }
        return pBean;
    }

    public void setPatFacilityDetails() {
        int output = 0;
        try {
            PatFacilityAgentRObj patFacilityAgentRObj = EJBUtil.getPatFacilityAgentHome();
            output = patFacilityAgentRObj.setPatFacilityDetails(this.createPatFacilityBean());
            this.setId(output);
            Rlog.debug("PatFacility", "PatFacilityJB.setPatFacilityDetails()");
            
            if (this.isDefault.equals("1"))
            {
            	updatePatientFacilityDefaults();
             }
            
        } catch (Exception e) {
            Rlog
                    .fatal("PatFacility", "Error in setPatFacilityDetails() in PatFacilityJB "
                            + e);
            e.printStackTrace();
        }
    }

    /**
     * Saves the modified details of the category to the database
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int updatePatFacility() {
        int output;
        try {
            PatFacilityAgentRObj patFacilityAgentRObj = EJBUtil.getPatFacilityAgentHome();
            output = patFacilityAgentRObj.updatePatFacility(this.createPatFacilityBean());
            
            
            Rlog.debug("PatFacility",  "isDefault " + isDefault + "*");
            if (this.isDefault.equals("1"))
            {
            	Rlog.debug("PatFacility",  "updatePatFacility  : before calling updatePatientFacilityDefaults : starting ");	
            	updatePatientFacilityDefaults();
             }
        } catch (Exception e) {
            Rlog.fatal("PatFacility",
                    "EXCEPTION IN SETTING PATIENT FACILITYSESSION BEAN"
                            + e.getMessage());

            return -2;
        }
        return output;
    }

    /**
     * Uses the values of the current PatFacilityJB object to create an
     * PatFacilityBean Object
     * 
     * @return PatFacilityBean object 
      */
    public PatFacilityBean createPatFacilityBean() {
        Rlog.debug("PatFacility", "PatFacilityJB.createPatFacilityBean ");
        return new PatFacilityBean(this.id,this.patientFacilityId, this.patientPK, this.patientSite,
    this.regDate,this.registeredBy, this.registeredByOther, this.patSpecialtylAccess,this.patAccessFlag ,
    this.isDefault ,this.creator, this.lastModifiedBy, this.iPAdd, this.isReadOnly);
    }

     /**
     * Deletes Patient Facility Record
     * 
     * @return 0 - If update succeeds <BR>
     *         -2 - If update fails
     */
    public int deletePatFacility() {
        int output;
        try {
            PatFacilityAgentRObj patFacilityAgentRObj = EJBUtil.getPatFacilityAgentHome();
            output = patFacilityAgentRObj.deletePatFacility(this.id);
        } catch (Exception e) {
            Rlog.fatal("PatFacility",
                    "EXCEPTION IN deletePatFacility"      + e.getMessage());

            return -2;
        }
        return output;
    }
    
    
    /**
     * Gets Patient facilities
     * 
     * @param perPK
     */

    public PatFacilityDao getPatientFacilities(int perPk) {
    	PatFacilityDao pd = new PatFacilityDao ();
    	
    	try
    	{
    		PatFacilityAgentRObj patFacilityAgentRObj = EJBUtil.getPatFacilityAgentHome();
    		pd = patFacilityAgentRObj.getPatientFacilities(perPk);
    		return pd;
    	}
    	catch (Exception e) {
            Rlog.fatal("PatFacility",  "EXCEPTION IN getPatientFacilities"      + e.getMessage());
            e.printStackTrace();

            return pd;
        }
    	
    } 
    
    public PatFacilityDao getPatientFacilitiesSiteIds(int perPk){
    	PatFacilityDao pd = new PatFacilityDao ();
    	
    	try
    	{
    		PatFacilityAgentRObj patFacilityAgentRObj = EJBUtil.getPatFacilityAgentHome();
    		pd = patFacilityAgentRObj.getPatientFacilitiesSiteIds(perPk);
    		return pd;
    	}
    	catch (Exception e) {
            Rlog.fatal("PatFacility",  "EXCEPTION IN getPatientFacilities"      + e.getMessage());
            e.printStackTrace();

            return pd;
        }
    	
    } 
   
    public void updatePatientFacilityDefaults()
    {
    	Rlog.debug("PatFacility",  "updatePatientFacilityDefaults : starting ");
    	if (! StringUtil.isEmpty(patientPK) )
    	{
    		Rlog.debug("PatFacility",  "updatePatientFacilityDefaults : starting patient pk:" + patientPK      );
    		
    		PersonJB person = new PersonJB(); 
    		person.setPersonPKId(EJBUtil.stringToNum(patientPK));
            person.getPersonDetails();
            
            //update the details:
            
            person.setPersonRegDate(this.regDate);
    		person.setPersonRegBy(this.registeredBy);
    		person.setPhyOther(this.registeredByOther);
    		person.setPersonSplAccess(this.patSpecialtylAccess);
    		person.setPersonLocation(this.patientSite);
    		person.setPatientFacilityId(this.patientFacilityId);
    		person.setModifiedBy(this.lastModifiedBy);
    		person.updatePerson();
    		Rlog.debug("PatFacility",  "updatePatientFacilityDefaults : finished");
    	}
    	
    	
    }
    
 /** Returns the access right flag for user's specialty. Specialty filters for all patient facilities are checked.
    
     * @param splId Speciality Id
     * */
    
    public int getUserFacilitySpecialtyAccessRight(int splId, int perPk) {
    	int right = 0;
    	
    	try
    	{
    		PatFacilityAgentRObj patFacilityAgentRObj = EJBUtil.getPatFacilityAgentHome();
    		right = patFacilityAgentRObj.getUserFacilitySpecialtyAccessRight(splId, perPk);
    		return right;
    	}
    	catch (Exception e) {
            Rlog.fatal("PatFacility",  "EXCEPTION IN getUserFacilitySpecialtyAccessRight"      + e.getMessage());
            e.printStackTrace();

            return right;
        }

    }
 //end of class   
}
