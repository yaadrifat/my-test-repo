/*
 * Classname			InvoiceDetailAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					10/18/2005
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.invoiceAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.InvoiceDetailDao;
import com.velos.eres.business.invoice.InvoiceDetailBean;



/**
 * Remote interface for PatFacilityAgentBean session EJB
 * 
 * @author Sonia Abrol
 * @version 1.0, 09/22/2005
 */
@Remote
public interface InvoiceDetailAgentRObj {
    
	public InvoiceDetailBean getInvoiceDetail(int pb);

    public int setInvoiceDetail(InvoiceDetailBean pb);
    
    public int setInvoiceDetails(InvoiceDetailDao inv);
    public int updateInvoiceDetails(InvoiceDetailDao inv);

    public int updateInvoiceDetail(InvoiceDetailBean pb);

    public int  deleteInvoiceDetail(int Id);
    
    /** returns InvoiceDetailDao with details of a generated invoice (for a milestone type)*/
    public InvoiceDetailDao getInvoiceDetailsForMilestoneType(int invId, String mileStoneType, Hashtable htAdditionalParam);
    
    
        
}
