/*
 * Classname			PatLoginAgentBean
 * 
 * Version information : 
 *
 * Date					04/05/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.patLoginAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.PortalDao;
import com.velos.eres.business.patLogin.impl.PatLoginBean;
import com.velos.eres.service.patLoginAgent.PatLoginAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Security;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.portal.PortalJB;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the POJO.
 * 
 * @Jnanamay Majumdar
 * @version 1.0, 04/05/2007
  */

@Stateless
public class PatLoginAgentBean implements PatLoginAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the id parameter passed in and constructs and
     * returns a PatLoginBean containing all the pat login in 
     * @param id   The Id
     */
     
    public PatLoginBean getPatLoginDetails(int id) {
    	PatLoginBean patb = null; 
        try {

        	patb = (PatLoginBean) em.find(PatLoginBean.class, new Integer(id));

        } catch (Exception e) {
            Rlog.fatal("PatLogin",
                    "Error in getPatLoginDetails() in PatLoginAgentBean" + e);
        }

        return patb;
    }

    /**
     * Creates a new Invoice record
     * 
     * @param patLogin
     *            A PatLoginBean containing patient login details.
     * @return The patLogin Id for the new record
     *         0 - in case the method fails
     */

    public int setPatLoginDetails(PatLoginBean patLogin) {

        try {
        	                
        	PatLoginBean patb = new PatLoginBean();
               
            patb.updatePatLogin(patLogin);
            em.persist(patb);
            
        	notifyPatient(patLogin.getPlId(),patLogin.getPlLogin(),(patLogin.getPlPassword()),patLogin.getModifiedBy(),patLogin.getFkPortal());
        	
            return (patb.getId());
            
        } catch (Exception e) {
            Rlog.fatal("PatLogin",  "Error in setPatLoginDetails() in PatLoginAgentBean " + e);
            e.printStackTrace();
        }
        return 0;
    }

    

    /**
     * Updates the patLogin record with data passed in PatLoginBean object 
     * 
     *
     */

    public int updatePatLogin(PatLoginBean patb) {

    	PatLoginBean retrieved = null; 
        int output;
        
        try {

            retrieved = (PatLoginBean) em.find(PatLoginBean.class, new Integer(patb.getId()));

            if (retrieved == null) {
                return -2;
            }
            
            //check is user login or passwordis changed, if yes, send notification
            
            if (! ((retrieved.getPlLogin()).equals(patb.getPlLogin()))  || ( ! retrieved.getPlPassword().equals(patb.getPlPassword())) )
            {
            	notifyPatient(patb.getPlId(),patb.getPlLogin(),patb.getPlPassword(),patb.getModifiedBy(),patb.getFkPortal());
            }	
            
            output = retrieved.updatePatLogin(patb);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.fatal("PatLogin", "Error in updatePatLogin in PatLoginAgentBean"
                            + e);
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public void notifyPatient(String patientPK,String loginName,String password,String user,String portalPK)
    {
    	int notFlag = 0;
        try {
        	
        	//find out if notify patient is checked in the portal
        	
        	PortalJB pjb = new PortalJB();
        	
        	pjb.setPortalId(EJBUtil.stringToNum(portalPK));
        	pjb.getPortalDetails();
        	notFlag = EJBUtil.stringToNum(pjb.getPortalNotificationFlag());
        	
        	if (notFlag == 1)
        	{
        		PortalDao pd = new PortalDao();
        		pd.notifyPatient(EJBUtil.stringToNum(patientPK), loginName,  Security.decrypt(password),EJBUtil.stringToNum(user));
        	}	

        } catch (Exception e) {
            Rlog.fatal("PatLogin", "Exeception in notifyPatient in PatLoginAgentBean"
                            + e);
            e.printStackTrace();
            
        }

    	
    }
    /**
     * Deletes record 
     * @param id  The patLogin Id
     */
     
    public int  deletePatLogin(int id) {
    	PatLoginBean patb = null; 
        try {

        	patb = (PatLoginBean) em.find(PatLoginBean.class, new Integer(id));
	   em.remove(patb);
	   return 0;
        } catch (Exception e) {
            Rlog.fatal("Invoice",
                    "Error in deletePatLogin() in PatLoginAgentBean" + e);
	 return -2;
        }
     
    }
    
 
    /**
     * Checks for duplicate User for the given Login name 
     *     
     */

    public PatLoginBean validateLogin(String LoginName) {    
    	PatLoginBean pBean = null;      
        try {

            Query queryPatLoginIdentifier = em.createNamedQuery("findValidLogin");
            queryPatLoginIdentifier.setParameter("pl_login", LoginName);
            
            queryPatLoginIdentifier.setMaxResults(1);
            
            pBean = (PatLoginBean )queryPatLoginIdentifier.getSingleResult();            
            //return pBean;

        } catch (Exception e) {
            Rlog.fatal("patlogin", "Exception in validateLogin() in PatLoginAgentBean" + e);
            //e.printStackTrace();
        }

        return pBean;
    }
    
    /**
     * Checks if there is a patient login for the given Login name and Password
     * 
     * @param patlogin
     *             patient login name
     * @password 
     * 			    patient password            
     * @return PatLoginBean object with login details in case its a valid
     *         login. Returns null object in case of invalid login/pwd combination
     */

    public PatLoginBean validateLoginPassword(String loginname, String password,String ipAdd) {

    	PatLoginBean pBean = null;
    	PortalDao pd = new PortalDao();
    	Hashtable htData = new Hashtable();
    	String loginFlag = "";
    	String newPass = "";
    	
        try {

            Query query = em.createNamedQuery("validateLogin");
            query.setParameter("pl_login", loginname);
            query.setParameter("pl_password", (password));
            try{
            	pBean = (PatLoginBean) query.getSingleResult();
            }
            catch (EntityNotFoundException e )
            {
            	
            	Rlog.fatal("PatLogin", "Exception in validateLoginPassword() in PatLoginAgentBean : Login/pwd not found");
            }
            
            if (pBean == null)
            {
            	
            	htData = pd.createAutoPatientLoginIfEnabled(loginname,(password),ipAdd);
            	
            	if (htData != null)
            	{
            		loginFlag = (String) htData.get("loginFlag");
            		
            		if (StringUtil.isEmpty(loginFlag))
            		{
            			loginFlag = "0";
            		}
            		
            		if (loginFlag.equals("1"))
            		{
            			newPass = (String) htData.get("password");
            			
            			//validate the login again
            			 
            			
            			if (! StringUtil.isEmpty(newPass))
            			{
            				query.setParameter("pl_login", loginname);
            	            query.setParameter("pl_password", newPass);//no need to encrypt as we got the encrypted pwd from db
            	            pBean = (PatLoginBean) query.getSingleResult();
            	            pBean.setIsAutoLogin(true);
            	         	
            			}
            			
            		}
            		
            		
            	}
            }
            
            return pBean;

        } catch (Exception e) {
            Rlog.fatal("PatLogin", "Exception in validateLoginPassword() in PatLoginAgentBean" + e);
            e.printStackTrace();
        }

        return pBean;
    }
    
}// end of class

