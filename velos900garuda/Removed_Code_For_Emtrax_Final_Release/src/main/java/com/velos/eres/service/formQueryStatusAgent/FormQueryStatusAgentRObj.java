/**
 * Classname : FormQueryStatusAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 01/03/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.formQueryStatusAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.eres.business.formQueryStatus.impl.FormQueryStatusBean;

/* End of Import Statements */

/**
 * Remote interface for FormQueryStatusAgent session EJB
 * 
 * @author Anu Khanna
 */
@Remote
public interface FormQueryStatusAgentRObj {
    /**
     * gets the FormQuery status record
     */
    FormQueryStatusBean getFormQueryStatusDetails(int formQueryStatusId);

    /**
     * creates a new FormQueryStatus Record
     */
    public int setFormQueryStatusDetails(FormQueryStatusBean fqsk);

    /**
     * updates the FormQuery status Record
     */
    public int updateFormQueryStatus(FormQueryStatusBean fqsk);

    /**
     * remove FormQuery status Record
     */
    public int removeFormQueryStatus(int formQueryStatusId);

}
