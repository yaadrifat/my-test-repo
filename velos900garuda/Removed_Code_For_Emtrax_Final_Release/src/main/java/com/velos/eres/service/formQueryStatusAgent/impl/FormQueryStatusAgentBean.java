/**
 * Classname : FormQueryStatusAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 01/03/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.formQueryStatusAgent.impl;

/* Import Statements */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.formQueryStatus.impl.FormQueryStatusBean;
import com.velos.eres.service.formQueryStatusAgent.FormQueryStatusAgentRObj;
import com.velos.eres.service.util.Rlog;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * FormQueryStatusAgentBean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * @see FormQueryStatusAgentBean
 * @ejbHome formQueryStatusAgentHome
 * @ejbRemote formQueryStatusAgentRObj
 */

@Stateless
public class FormQueryStatusAgentBean implements FormQueryStatusAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * 
     * Looks up on the id parameter passed in and constructs and returns a
     * FormQueryStatus state keeper. containing all the details of the
     * FormQueryStatus <br>
     * 
     * @param Form
     *            Query Status Id
     */

    public FormQueryStatusBean getFormQueryStatusDetails(int formQueryStatusId) {
        FormQueryStatusBean retrieved = null;

        try {
            retrieved = (FormQueryStatusBean) em.find(
                    FormQueryStatusBean.class, new Integer(formQueryStatusId));

        } catch (Exception e) {
            Rlog.fatal("formQueryStatus",
                    "Exception in FormQueryStatusStateKeeper() in FormQueryStatusAgentBean"
                            + e);
        }

        return retrieved;

    }

    /**
     * Creates FormQueryStatus record.
     * 
     * @param a
     *            State Keeper containing the formQueryId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setFormQueryStatusDetails(FormQueryStatusBean fqsk) {
        try {
            FormQueryStatusBean fb = new FormQueryStatusBean();

            fb.updateFormQueryStatus(fqsk);
            em.persist(fb);
            return fb.getFormQueryStatusId();
        } catch (Exception e) {
            Rlog.fatal("formQueryStatus",
                    "Error in setFormQueryStatusDetails() in FormQueryStatusAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates a FormQueryStatus record.
     * 
     * @param a
     *            FormQueryStatus state keeper containing studySite attributes
     *            to be set.
     * @return int for successful:0 ; e\Exception : -2
     */

    public int updateFormQueryStatus(FormQueryStatusBean fqsk) {

        FormQueryStatusBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = (FormQueryStatusBean) em.find(
                    FormQueryStatusBean.class, new Integer(fqsk
                            .getFormQueryStatusId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateFormQueryStatus(fqsk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.fatal("formQueryStatus",
                    "Error in updateFormQueryStatus in FormQueryStatusAgentBean"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a FormQueryStatus record.
     * 
     * @param a
     *            FormQueryStatus Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeFormQueryStatus(int formQueryStatusId) {
        int output;
        FormQueryStatusBean retrieved = null;

        try {

            retrieved = (FormQueryStatusBean) em.find(
                    FormQueryStatusBean.class, new Integer(formQueryStatusId));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("formQueryStatus", "Exception in removeFormQueryStatus"
                    + e);
            return -1;

        }

    }

}
