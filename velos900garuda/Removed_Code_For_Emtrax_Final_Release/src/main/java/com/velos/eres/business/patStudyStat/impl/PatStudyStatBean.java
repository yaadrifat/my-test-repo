/*
 * Classname			PatStudyStatBean.class
 * 
 * Version information  2
 *
 * Date					06/15/2001
 * 
 * Copyright notice
 */

package com.velos.eres.business.patStudyStat.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * CMP entity bean for Patient Study Status
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Business layer.
 * </p>
 * 
 * @author Sonia Sahni
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:08/16/2004 Comment: 1. Added new attribute screenNumber,
 * screenedBy, screeningOutcome and respective getter/setters 2. Modified
 * methods for the new attribute 3. Modified JavaDoc comments
 * *************************END****************************************
 */
@Entity
@Table(name = "er_patstudystat")
@NamedQueries( {
        @NamedQuery(name = "findByPreviousCurrentStatusPat", query = " SELECT OBJECT(stat) FROM PatStudyStatBean stat where to_number(stat.studyId) = :fk_study and  to_number(stat.patientId) = :fk_per and       		stat.endDate is null and stat.startDate <= to_date(:begindate,:date_format) and       		stat.id <> :pk_patstudystat "),
        @NamedQuery(name = "findByNextStatusPat", query = "	SELECT OBJECT(stat) FROM PatStudyStatBean stat where		stat.id = ( select max(i.id) from PatStudyStatBean  i where				to_number(i.studyId) = :fk_study and  (i.patientId) = :fk_per and		i.startDate = ( select min(ii.startDate) from PatStudyStatBean  ii Where		to_number(ii.studyId) = :fk_study and  to_number(ii.patientId) = :fk_per and		ii.startDate >= to_date(:begindate,:date_format) and ii.id <> :pk_patstudystat) and		i.id <> :pk_patstudystat)"),
        @NamedQuery(name = "findByPreviousStatusPat", query = " SELECT OBJECT(stat) FROM PatStudyStatBean stat where		stat.id = ( select max(i.id) from PatStudyStatBean  i where				to_number(i.studyId) = :fk_study and  to_number(i.patientId) = :fk_per and	i.startDate = ( select max(ii.startDate) from PatStudyStatBean  ii Where		to_number(ii.studyId) = :fk_study and  to_number(ii.patientId) = :fk_per and		ii.startDate <= to_date(:begindate,:date_format) and ii.id <> :pk_patstudystat) and		i.id <> :pk_patstudystat)	"),
        @NamedQuery(name = "findByCurrentPatientStudyStatus", query = " SELECT OBJECT(stat) FROM PatStudyStatBean stat where   	    	to_number(stat.studyId) = :fk_study and  to_number(stat.patientId) = :fk_per and	    	stat.endDate is null and rownum < 2 order by stat.id desc"),
        @NamedQuery(name = "findByPatientStudyStatusCodeSubType", query = " SELECT OBJECT(stat) FROM PatStudyStatBean stat where 			to_number(stat.studyId) = :fk_study and  to_number(stat.patientId) = :fk_per and	to_number(stat.patStudyStat) = :fk_codelst_status order by stat.id desc "),
        @NamedQuery(name = "findByAtLeastOneStatusPat", query = "	SELECT OBJECT(stat) FROM PatStudyStatBean stat where to_number(stat.studyId) = :fk_study and  to_number(stat.patientId) = :fk_per and stat.id <> :pk_patstudystat and rownum < 2 order by stat.id desc"),
        @NamedQuery(name = "findByLatestStatusPat", query = "SELECT OBJECT(stat) FROM PatStudyStatBean stat where	stat.id = ( select max(i.id) from PatStudyStatBean  i where			to_number(i.studyId) = :fk_study and  to_number(i.patientId) = :fk_per and	trunc(i.startDate) = ( select max(trunc(ii.startDate)) from PatStudyStatBean  ii Where	to_number(ii.studyId) = :fk_study and  to_number(ii.patientId) = :fk_per ))")

})
public class PatStudyStatBean implements Serializable {
 
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * the Patient Study Status Id
     */
    public int id;

    /**
     * Patient study status
     */
    public Integer patStudyStat;

    /**
     * the Study id
     */

    public Integer studyId;

    /**
     * the Patient id
     */
    public Integer patientId;

    /**
     * the status start date
     */

    public java.util.Date startDate;

    /**
     * the associated Notes
     */
    public String notes;

    /**
     * the status end date
     */

    public java.util.Date endDate;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    /**
     * PatStudyStat Reason
     */
    public Integer patStudyStatReason;

    /**
     * Screen Number for patient
     */
    public String screenNumber;

    /**
     * User who screened the patient
     */
    public Integer screenedBy;

    /**
     * Screening Outcome Code
     */

    public Integer screeningOutcome;

    /**
     * Patient Enrollment Statekeeper
     */

    private PatProtBean patProtObj;
    
    
    //JM: 01.09.2006       
       /**
        * Patient next followup date
        */
       public Date ptstNextFlwup = null;
       
           
       /**
        * Patient consent verison
        */
       public String ptstConsentVer;
      
      
    /**
     * the Current Status
     */
    public String currentStat;  //KM

    // GETTER SETTER METHODS

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PATSTUDYSTAT", allocationSize=1)
    @Column(name = "PK_PATSTUDYSTAT")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "PATSTUDYSTAT_DATE")
    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {

        this.startDate = startDate;
    }

    public String returnStartDate() {
        return DateUtil.dateToString(getStartDate());
    }

    public void updateStartDate(String startDate) {
        setStartDate(DateUtil.stringToDate(startDate, null));
    }

    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(this.studyId);
    }

    public void setStudyId(String studyId) {
        if (studyId != null) {
            this.studyId = Integer.valueOf(studyId);
        }
    }

    @Column(name = "FK_PER")
    public String getPatientId() {
        return StringUtil.integerToString(this.patientId);
    }

    public void setPatientId(String patientId) {
        if (patientId != null) {
            this.patientId = Integer.valueOf(patientId);
        }
    }

    @Column(name = "FK_CODELST_STAT")
    public String getPatStudyStat() {
        return StringUtil.integerToString(this.patStudyStat);
    }

    public void setPatStudyStat(String patStudyStat) {
        if (patStudyStat != null) {
            this.patStudyStat = Integer.valueOf(patStudyStat);
        }
    }

    @Column(name = "PATSTUDYSTAT_REASON")
    public String getPatStudyStatReason() {
        return StringUtil.integerToString(this.patStudyStatReason);
    }

    public void setPatStudyStatReason(String patStudyStatReason) {
        if (!StringUtil.isEmpty(patStudyStatReason)) {
            this.patStudyStatReason = Integer.valueOf(patStudyStatReason);
        } else {
            this.patStudyStatReason = null;
        }

    }

    @Column(name = "PATSTUDYSTAT_NOTE")
    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "PATSTUDYSTAT_ENDT")
    public Date getEndDate() {
        Rlog.debug("studystatus", "statDocUser *" + this.endDate + "*");
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String returnEndDate() {
        return DateUtil.dateToString(getEndDate());
    }

    public void updateEndDate(String endDate) {

        setEndDate(DateUtil.stringToDate(endDate, null));

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @return Returns the value of Screen Number.
     */
    @Column(name = "SCREEN_NUMBER")
    public String getScreenNumber() {
        return screenNumber;
    }

    /**
     * Sets the value of Screen Number.
     * 
     * @param screenNumber
     *            The patient screen Number.
     */
    public void setScreenNumber(String screenNumber) {
        this.screenNumber = screenNumber;
    }

    /**
     * @return Returns the value of screenedBy - The user who screened the
     *         patient.
     */
    @Column(name = "SCREENED_BY")
    public String getScreenedBy() {
        return StringUtil.integerToString(screenedBy);
    }

    /**
     * Sets the value of screenedBy.
     * 
     * @param screenedBy
     *            The id of the user who screened the patient.
     */
    public void setScreenedBy(String screenedBy) {
        if (!StringUtil.isEmpty(screenedBy)) {
            Rlog.debug("patstudystat", " screenedBy NOT NULL*" + screenedBy
                    + "*");
            this.screenedBy = Integer.valueOf(screenedBy);
        } else {
            Rlog.debug("patstudystat", " screenedBy  NULL*");
            this.screenedBy = null;
        }

    }

    /**
     * @return Returns the value of Screening Outcome code for the patient.
     */
    @Column(name = "SCREENING_OUTCOME")
    public String getScreeningOutcome() {
        return StringUtil.integerToString(screeningOutcome);
    }

    /**
     * Sets the value of Screening Outcome Code.
     * 
     * @param screeningOutcome
     *            Screening Outcome Code for the patient.
     */
    public void setScreeningOutcome(String screeningOutcome) {
        Rlog.debug("patstudystat", " ScreeningOutcome *");
        if (!StringUtil.isEmpty(screeningOutcome)) {
            Rlog.debug("patstudystat", " ScreeningOutcome NOT NULL*"
                    + screeningOutcome + "*");
            this.screeningOutcome = Integer.valueOf(screeningOutcome);
        } else {
            Rlog.debug("patstudystat", " ScreeningOutcome NULL*");
            this.screeningOutcome = null;
        }

    }

    /**
     * Returns the Patient Enrollment StateKeeper.
     * 
     * @return the Patient Enrollment StateKeeper.
     */
    @Transient
    public PatProtBean getPatProtObj() {
        return patProtObj;
    }

    /**
     * Sets the Patient Enrollment StateKeeper.
     * 
     * @param patProtObj
     *            the Patient Enrollment StateKeeper.
     */
    public void setPatProtObj(PatProtBean patProtObj) {
        this.patProtObj = patProtObj;
    }    
    
    
//JM: 01.09.2006////////////////
    
    @Transient
    public String getPtstNextFlwup() {
        return DateUtil.dateToString(getPtstNextFlwupPersistent());
    }

    @Column(name = "NEXT_FOLLOWUP_ON")
    public Date getPtstNextFlwupPersistent() {
        return ptstNextFlwup;
    }

    public void setPtstNextFlwup(String ptstNextFlwup) {
    	
        setPtstNextFlwupPersistent(DateUtil.stringToDate(ptstNextFlwup,
                null));
        
    }

    public void setPtstNextFlwupPersistent(Date ptstNextFlwup) {
    	
    	this.ptstNextFlwup = ptstNextFlwup;
    	
    }
    
    
    /**
     * @return Returns the value of informed consent version number for the patient.
     */
       
    
    @Column(name = "INFORM_CONSENT_VER")
    public String getPtstConsentVer() {
        return ptstConsentVer;
    }

    
    /**
     * Sets the value of informed consent version number for the patient
     * 
     * @param ptstConsentVer
     *            Informed Consent Version Number
     */
    public void setPtstConsentVer(String ptstConsentVer) {
        this.ptstConsentVer = ptstConsentVer;
    }    

        
    /////////////////////////    
    
    
    //  Added by Manimaran for the September Enhancement(S8)
    @Column(name="current_stat")
    public String getCurrentStat() {
    	return this.currentStat;
    }
    
    public void setCurrentStat(String currentStat) {
    	this.currentStat = currentStat;
    }
    // END OF GETTER SETTER METHODS

    /*
     * public PatStudyStatStateKeeper getPatStudyStatStateKeeper() {
     * PatStudyStatPK pk = (PatStudyStatPK) context.getPrimaryKey();
     * 
     * int id = pk.id;
     * 
     * Rlog.debug("patstudystat", "CHECKING GET STATUS KEEPER " + id);
     * 
     * return (new PatStudyStatStateKeeper(id, getStudyId(), getPatientId(),
     * getPatStudyStat(), getNotes(), getStartDate(), getEndDate(),
     * getCreator(), getModifiedBy(), getIpAdd(), getPatStudyStatReason(),
     * getScreenNumber(), getScreeningOutcome(), getScreenedBy(), null)); }
     */

    /*
     * public int setPatStudyStatStateKeeper(PatStudyStatStateKeeper psk) {
     * Rlog.debug("patstudystat", "IN ENTITY BEAN - SET PATIENT STATUS STATE
     * HOLDER");
     * 
     * GenerateId genId = null;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("patstudystat", "Connection :" + conn);
     * 
     * id = genId.getId("SEQ_ER_PATSTUDYSTAT", conn); conn.close();
     * 
     * Rlog.debug("patstudystat", "GOT ID" + id); Rlog.debug("patstudystat",
     * "GOT END DATE" + psk.getEndDate() + "*"); Rlog.debug("patstudystat",
     * "Study Status " + this.patStudyStat); Rlog.debug("patstudystat", "Study
     * Status " + psk.getPatStudyStat());
     * 
     * setNotes(psk.getNotes()); setStartDate(psk.getStartDate());
     * setEndDate(psk.getEndDate()); setStudyId(psk.getStudyId());
     * Rlog.debug("patstudystat", "pat study status setStudyId" +
     * psk.getStudyId()); setPatientId(psk.getPatientId());
     * Rlog.debug("patstudystat", "pat study status setPatientId" +
     * psk.getPatientId()); setPatStudyStat(psk.getPatStudyStat());
     * Rlog.debug("patstudystat", "pat study status setPatStudyStat" +
     * psk.getPatStudyStat()); setCreator(psk.getCreator());
     * Rlog.debug("patstudystat", "pat study status setCreator" +
     * psk.getCreator()); setIpAdd(psk.getIpAdd());
     * setPatStudyStatReason(psk.getPatStudyStatReason());
     * Rlog.debug("patstudystat", "pat study status setPatStudyStatReason" +
     * psk.getPatStudyStatReason());
     * setScreeningOutcome(psk.getScreeningOutcome());
     * Rlog.debug("patstudystat", "pat study status setScreeningOutcome" +
     * psk.getScreeningOutcome()); setScreenedBy(psk.getScreenedBy());
     * Rlog.debug("patstudystat", "pat study status setScreenedBy" +
     * psk.getScreenedBy()); setScreenNumber(psk.getScreenNumber()); return id; }
     * catch (Exception e) { Rlog.fatal("patstudystat", "EXCEPTION IN CREATING
     * NEW PATIENT STUDY STATUS " + e); return -2; } }
     */

    /**
     * Updates the entity bean with data in the PatStudyStatStateKeeper object
     * 
     * @param psk
     *            A PatStudyStatStateKeeper with patient study status data
     * @return 0 if successful; -2 in case of any exception
     */
    public int updatePatStudyStat(PatStudyStatBean psk) {
        Rlog.debug("patstudystat",
                "IN ENTITY BEAN - UPDATE PATIENT STATUS STATE HOLDER");
        try {
            Rlog.debug("patstudystat", "GOT END DATE" + psk.getEndDate() + "*");

            setNotes(psk.getNotes());
            updateStartDate(psk.returnStartDate());
            setEndDate(psk.getEndDate());
            setStudyId(psk.getStudyId());
            Rlog.debug("patstudystat", "GOT STUDY" + psk.getStudyId() + "*");
            setPatientId(psk.getPatientId());
            Rlog
                    .debug("patstudystat", "GOT PATIENT" + psk.getPatientId()
                            + "*");
            setPatStudyStat(psk.getPatStudyStat());
            Rlog.debug("patstudystat", "GOT getPatStudyStat"
                    + psk.getPatStudyStat() + "*");
            setModifiedBy(psk.getModifiedBy());
            Rlog.debug("patstudystat", "GOT getModifiedBy"
                    + psk.getModifiedBy() + "*");
            setIpAdd(psk.getIpAdd());
            setPatStudyStatReason(psk.getPatStudyStatReason());
            Rlog.debug("patstudystat", "GOT reason"
                    + psk.getPatStudyStatReason() + "*");
            setScreeningOutcome(psk.getScreeningOutcome());
            Rlog.debug("patstudystat", "GOT ScreeningOutcome"
                    + psk.getScreeningOutcome() + "*");
            setScreenedBy(psk.getScreenedBy());
            Rlog.debug("patstudystat", "GOT getScreenedBy"
                    + psk.getScreenedBy() + "*");
            setScreenNumber(psk.getScreenNumber());

            Rlog.debug("patstudystat", "saved");
            
            setCreator(psk.getCreator());
            
//JM:01.09.2006 ////
            
        
            setPtstNextFlwup(psk.getPtstNextFlwup());
            setPtstConsentVer(psk.getPtstConsentVer());
            setCurrentStat(psk.getCurrentStat());//KM
          
            
            
            return 0;
        } catch (Exception e) {
            Rlog.fatal("patstudystat",
                    "EXCEPTION IN UPDATING PATIENT STUDY STATUS " + e);
            return -2;
        }
    }

    public PatStudyStatBean() {
        patProtObj = new PatProtBean();

    }

    public PatStudyStatBean(int id, String patStudyStat, String studyId,
            String patientId, String startDate, String notes, String endDate,
            String creator, String modifiedBy, String ipAdd,
            String patStudyStatReason, String screenNumber, String screenedBy,String screeningOutcome,            
            String ptstNextFlwup,
            String ptstConsentVer,PatProtBean patProtObj, String currentStat){          
            
            
        // TODO Auto-generated constructor stub
        setId(id);
        setPatStudyStat(patStudyStat);
        setStudyId(studyId);
        setPatientId(patientId);
        updateStartDate(startDate);
        setNotes(notes);
        updateEndDate(endDate);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPatStudyStatReason(patStudyStatReason);
        setScreenNumber(screenNumber);
        setScreenedBy(screenedBy);
        setScreeningOutcome(screeningOutcome);
        
//JM: 01.09.2006
        
        setPtstNextFlwup(ptstNextFlwup);              
        setPtstConsentVer(ptstConsentVer);
        
/////////////
        setPatProtObj(patProtObj);
        setCurrentStat(currentStat);
    }

}// end of class

