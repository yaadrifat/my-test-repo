/*
 * Classname : DynRepFltrAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/05/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Vishal Abrol
 */

package com.velos.eres.service.dynrepfltrAgent.impl;

/* Import Statements */

import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.dynrepfltr.impl.DynRepFltrBean;
import com.velos.eres.service.dynrepfltrAgent.DynRepFltrAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.business.audit.impl.AuditBean;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP - AgentBean.<br>
 * <br>
 * 
 * @author Vishal Abrol
 * @see Dyn
 * @version 1.0 11/05/2003
 * @ejbHome DynRepDFltrAgentHome
 * @ejbRemote DynRepFltrAgentRObj
 */
@Stateless
public class DynRepFltrAgentBean implements DynRepFltrAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    public DynRepFltrBean getDynRepFltrDetails(int id) {
        DynRepFltrBean retrieved = null;

        try {
            retrieved = (DynRepFltrBean) em.find(DynRepFltrBean.class,
                    new Integer(id));
            return retrieved;

        } catch (Exception e) {
            Rlog.fatal("dynrep",
                    "Exception in getDynRepFltrDetails() in DynRepFltrAgentBean"
                            + e);
        }

        return retrieved;

    }

    /**
     * Creates dynrep record.
     * 
     * @param a
     *            State Keeper containing the dynrep attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setDynRepFltrDetails(DynRepFltrBean dsk) {
        try {
            DynRepFltrBean df = new DynRepFltrBean();
            df.updateDynRepFltr(dsk);
            em.persist(df);

            return df.getId();
        } catch (Exception e) {
            Rlog.fatal("dynrep",
                    "Error in setDynRepFltrDetails() in DynRepFltrAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates a dynrep record.
     * 
     * @param a
     *            dynrep state keeper containing studyId attributes to be set.
     * @return int for successful:0 ; e\Exception : -2
     */
    public int updateDynRepFltr(DynRepFltrBean dsk) {

        DynRepFltrBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {

            retrieved = (DynRepFltrBean) em.find(DynRepFltrBean.class,
                    new Integer(dsk.getId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateDynRepFltr(dsk);
            em.merge(retrieved);

        } catch (Exception e) {
            Rlog.fatal("dynrep",
                    "Error in updateDynRepFltr in DynRepFltrAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a dynrep record.
     * 
     * @param a
     *            dynrep Primary Key
     * @return int for successful:0; e\Exception : -2
     */
     
    public int removeDynRepFltr(int id) {
        int output;
        DynRepFltrBean retrieved = null;

        try {
            retrieved = (DynRepFltrBean) em.find(DynRepFltrBean.class,
                    new Integer(id));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Exception in removeDynrepFltr" + e);
            return -1;

        }

    }
    
    // removeDynRepFltr() Overloaded for INF-18183 ::: Raviesh
    public int removeDynRepFltr(int id,Hashtable<String, String> args) {
        DynRepFltrBean retrieved = null;
        try {
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_DYNREPFILTER","eres","PK_DYNREPFILTER="+id);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("ER_DYNREPFILTER",String.valueOf(id),getRidValue,
        			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/ 
            
            retrieved = (DynRepFltrBean) em.find(DynRepFltrBean.class,
                    new Integer(id));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("dynrep", "Exception in removeDynrepFltr" + e);
            return -1;

        }

    }
    
    public int removeDynFilters(ArrayList Ids) {
        Rlog.debug("dynrep", "removeDynFilters in DynRepFltrAgentBean line 1"
                + Ids);
        int i = 0;
        int rows = Ids.size();

        try {
            for (i = 0; i < rows; i++) {
                if (this.removeDynRepFltr(StringUtil.stringToNum((String) Ids
                        .get(i))) < 0) {
                    return -2;
                }
            }
        } catch (Exception e) {
            Rlog.fatal("dynrep",
                    "Error in removeDynFilters in DynRepDtAgentBean " + e);
            return -2;
        }
        return 0;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int removeDynFilters(ArrayList Ids, Hashtable<String, String> auditInfo) {
        Rlog.debug("dynrep", "removeDynFilters in DynRepFltrAgentBean line 1"
                + Ids);
        int i = 0;
        int rows = Ids.size();
        String condition = "";
        try {
        	
        	for(i=0; i<rows; i++)
        		condition = condition + ", " + StringUtil.stringToNum((String) Ids.get(i));  
        	
        	condition = "PK_DYNREPFILTER IN ("+condition.substring(1)+")";
        	
        	AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("ER_DYNREPFILTER",condition,"eres");/*Fetches the RID/PK_VALUE*/
            ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
    		
    		for(i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_DYNREPFILTER",pkVal,ridVal,
        			userID,currdate,moduleName,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
        	
            for (i = 0; i < rows; i++) {
                if (this.removeDynRepFltr(StringUtil.stringToNum((String) Ids
                        .get(i))) < 0) {
                    return -2;
                }
            }
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("dynrep",
                    "Error in removeDynFilters in DynRepDtAgentBean " + e);
            return -2;
        }
        return 0;
    }

}
