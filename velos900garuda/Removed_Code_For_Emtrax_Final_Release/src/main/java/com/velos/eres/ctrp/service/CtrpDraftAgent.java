package com.velos.eres.ctrp.service;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.business.CtrpValidationDao;

@Remote
public interface CtrpDraftAgent {
	public CtrpDraftBean getCtrpDraftBean(Integer id);
	public Integer createCtrpDraft(CtrpDraftBean ctrpDraftBean);
	public Integer updateCtrpDraft(CtrpDraftBean ctrpDraftBean);
	public CtrpDraftBean findByDraftId(Integer findByDraftId);
	public ArrayList findDraftsByStudyIds(Integer accId, String studyIds);
	public CtrpValidationDao runCtrpDraftValidation(Integer ctrpDraftId);
	public int unmarkStudyCtrpReportable(String studyIds);
	public int deleteStudyDrafts(String studyIds);
	//Modified for Bug#8029:Akshi
	public int deleteStudyDrafts(String studyIds,Hashtable<String, String> args);
}
