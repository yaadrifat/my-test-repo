/*
 * Classname : StudyNIHGrantAgentRObj
 *
 * Version information: 1.0
 *
 * Date: 11/21/2010
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.service.studyNIHGrantAgent;
/* Import Statements */
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.StudyNIHGrantDao;
import com.velos.eres.business.studyNIHGrant.impl.StudyNIHGrantBean;
import com.velos.eres.business.studyNotify.impl.StudyNotifyBean;

@Remote
public interface StudyNIHGrantAgentRObj {
	 /**
     * gets the studyNIHGrantInfo 
     */
	StudyNIHGrantBean getStudyNIHGrantInfo(int studyId);

    /**
     * sets the studyNotify details
     */
    public int setStudyNIHGrantInfo(StudyNIHGrantBean snk);

    /**
     *     
     * @param fkStudyId
     * @return StudyNIHGrantDao
     */
	public StudyNIHGrantDao getStudyNIHGrantRecords(int fkStudyId);
	/**
	 * 
	 * @param snk
	 * @return
	 */

	public int updateStudyNIHGrant(StudyNIHGrantBean snk);
	/**
	 * 
	 * @param studyNIHGrantId
	 * @param argsvalues
	 * @return
	 */
	
	public int deleteStudyNIHGrantRecord(int studyNIHGrantId, Hashtable<String, String> argsvalues) ;


}
