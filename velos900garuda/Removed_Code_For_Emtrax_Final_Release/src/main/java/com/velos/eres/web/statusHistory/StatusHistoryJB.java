/*
 * Classname : StatusHistoryJB
 * 
 * Version information: 1.0
 *
 * Date: 08/09/2004
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.web.statusHistory;

/* IMPORT STATEMENTS */
import java.util.Hashtable;
import com.velos.eres.business.common.StatusHistoryDao;
import com.velos.eres.business.statusHistory.impl.StatusHistoryBean;
import com.velos.eres.service.statusHistoryAgent.StatusHistoryAgentRObj;
import com.velos.eres.service.statusHistoryAgent.impl.StatusHistoryAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for StatusHistory
 * 
 * @author Anu
 * @version 1.0 08/09/2004
 * 
 */

public class StatusHistoryJB {

    /**
     * the Status Id
     */
    private int statusId;

    /**
     * the status Module Id
     */
    private String statusModuleId;

    /**
     * the status Module Table
     */

    private String statusModuleTable;

    /**
     * the status Codelst Id
     */

    private String statusCodelstId;

    /**
     * the status Start Date
     */
    private String statusStartDate;

    /**
     * the status End Date
     */
    private String statusEndDate;

    /**
     * the status history Notes
     */

    private String statusNotes;

    /*
     * For future use
     */
    private String statusCustom1;

    /*
     * Status Entered by
     */

    private String statusEnteredBy;

    /*
     * creator
     */

    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;
    
    /*
     * Record Type
     */
    private String recordType;
    /*
     * Current Status
     */
    private String isCurrentStat;
    


    // GETTER SETTER METHODS

    public int getStatusId() {
        return this.statusId;
    }

    /**
     * sets Status History ID
     * 
     * @param statusId
     */
    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    /**
     * Get Status Module ID
     * 
     * @return String Module Id
     */

    public String getStatusModuleId() {
        return this.statusModuleId;
    }

    /**
     * sets Status Module Id
     * 
     * @param statusModuleId
     */
    public void setStatusModuleId(String statusModuleId) {
        this.statusModuleId = statusModuleId;
    }

    /**
     * Get Status Module Table
     * 
     * @return String Module Table
     */

    public String getStatusModuleTable() {
        return this.statusModuleTable;
    }

    /**
     * sets Status Module Table
     * 
     * @param statusModuleTable
     */
    public void setStatusModuleTable(String statusModuleTable) {
        this.statusModuleTable = statusModuleTable;
    }

    /**
     * Get Status Codelst ID
     * 
     * @return String codelst id
     */

    public String getStatusCodelstId() {
        return this.statusCodelstId;
    }

    /**
     * sets Status Codelst Id
     * 
     * @param statusCodelstId
     */

    public void setStatusCodelstId(String statusCodelstId) {
        this.statusCodelstId = statusCodelstId;

    }

    /**
     * Get Status Start Date
     * 
     * @return String status start date
     */

    public String getStatusStartDate() {
        return this.statusStartDate;
    }

    /**
     * sets Status Start Date
     * 
     * @param statusStartDate
     */

    public void setStatusStartDate(String statusStartDate) {
        this.statusStartDate = statusStartDate;
    }

    /**
     * Get Status End Date
     * 
     * @return String Status end date
     */

    public String getStatusEndDate() {
        return this.statusEndDate;
    }

    /**
     * sets Status End Date
     * 
     * @param statusEndDate
     */

    public void setStatusEndDate(String statusEndDate) {
        this.statusEndDate = statusEndDate;
    }

    /**
     * Get Status Notes
     * 
     * @return String status notes
     */

    public String getStatusNotes() {
        return this.statusNotes;

    }

    /**
     * sets Status Notes
     * 
     * @param statusNotes
     */
    public void setStatusNotes(String statusNotes) {
        this.statusNotes = statusNotes;
    }

    /*
     * For Future use
     */
    public String getStatusCustom1() {
        return this.statusCustom1;
    }

    /*
     * For Future use
     */

    public void setStatusCustom1(String statusCustom1) {
        this.statusCustom1 = statusCustom1;
    }

    /**
     * Get Status Entered by
     * 
     * @return String Status entered by
     */
    public String getStatusEnteredBy() {
        return this.statusEnteredBy;
    }

    /**
     * sets Status Entered by
     * 
     * @param statusEnteredBy
     */
    public void setStatusEnteredBy(String statusEnteredBy) {
        this.statusEnteredBy = statusEnteredBy;
    }

    /**
     * Get Status Creator
     * 
     * @return String status creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * sets Status Creator
     * 
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Get Status Modified by
     * 
     * @return String Status modified by
     */

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * sets Status Modified by
     * 
     * @param modifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Get Ip Address
     * 
     * @return String ip address
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * sets Status Ip Address
     * 
     * @param ipAdd
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    public String getRecordType() {
        return this.recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }
    
    //Added by Manimaran for the July-August Enhancement S4.
    public String getIsCurrentStat() {
    	return this.isCurrentStat;
    }
    
    public void setIsCurrentStat(String isCurrentStat) {
    	this.isCurrentStat = isCurrentStat;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts status Id
     * 
     * @param statusId
     *            Id
     */
    public StatusHistoryJB(int statusId) {
        setStatusId(statusId);
    }

    /**
     * Default Constructor
     * 
     */
    public StatusHistoryJB() {

    }

    /**
     * Full Arguments Constructor
     * 
     * @param statusId
     * @param statusModuleId
     * @param statusModuleTable
     * @param statusCodelstId
     * @param statusStartDate
     * @param statusEndDate
     * @param statusNotes
     * @param statusCustom1
     * @param statusEnteredBy
     * @param creator
     * @param modifiedBy
     * @param ipAdd
     */
    public StatusHistoryJB(int statusId, String statusModuleId,
            String statusModuleTable, String statusCodelstId,
            String statusStartDate, String statusEndDate, String statusNotes,
            String statusCustom1, String statusEnteredBy, String creator,
            String modifiedBy, String ipAdd,String recordType, String isCurrentStat) {

        setStatusId(statusId);
        setStatusModuleId(statusModuleId);
        setStatusModuleTable(statusModuleTable);
        setStatusCodelstId(statusCodelstId);
        setStatusStartDate(statusStartDate);
        setStatusEndDate(statusEndDate);
        setStatusNotes(statusNotes);
        setStatusCustom1(statusCustom1);
        setStatusEnteredBy(statusEnteredBy);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setRecordType(recordType);
        setIsCurrentStat(isCurrentStat);
    }

    /**
     * Calls getStatusHistoryDetails() of StatusHistory Session Bean:
     * StatusHistoryAgentBean
     * 
     * 
     * @return The Details of the status in the StatusHistory StateKeeper Object
     * @see StatusHistoryAgentBean
     */
    public StatusHistoryBean getStatusHistoryDetails() {
        StatusHistoryBean hsk = null;

        try {

            StatusHistoryAgentRObj statusHistoryAgent = EJBUtil
                    .getStatusHistoryAgentHome();
            hsk = statusHistoryAgent.getStatusHistoryDetails(this.statusId);

        } catch (Exception e) {
            Rlog.debug("statusHistory",
                    "Error in getStatusHistoryDetails() in StatusHistoryJB "
                            + e);
        }
        if (hsk != null) {

            this.statusId = hsk.getStatusId();
            this.statusModuleId = hsk.getStatusModuleId();
            this.statusModuleTable = hsk.getStatusModuleTable();
            this.statusCodelstId = hsk.getStatusCodelstId();
            this.statusStartDate = hsk.returnStatusStartDate();
            this.statusEndDate = hsk.returnStatusEndDate();
            this.statusNotes = hsk.getStatusNotes();
            this.statusCustom1 = hsk.getStatusCustom1();
            this.statusEnteredBy = hsk.getStatusEnteredBy();
            this.creator = hsk.getCreator();
            this.modifiedBy = hsk.getModifiedBy();
            this.ipAdd = hsk.getIpAdd();
            this.recordType = hsk.getRecordType();
            this.isCurrentStat= hsk.getIsCurrentStat();

        }
        return hsk;
    }

    /**
     * Calls setStatusHistoryDetails() of StatusHistory Session Bean:
     * StatusHistoryAgentBean
     * 
     * 
     */
    public void setStatusHistoryDetails() {
        try {
            StatusHistoryAgentRObj statusHistoryAgent = EJBUtil.getStatusHistoryAgentHome();

            this.setStatusId(statusHistoryAgent.setStatusHistoryDetails(this
                    .createStatusHistoryStateKeeper()));

        } catch (Exception e) {
            Rlog.debug("statusHistory",
                    "Error in setStatusHistoryDetails() in StatusHistoryJB "
                            + e);
        }
    }

    
    //  Added by Manimaran for the July-August Enhancement S4.
    /**
     * Calls setStatusHistoryDetailsWithEndDate() of StatusHistory Session Bean:
     * StatusHistoryAgentBean
     * 


     * 
     */
    public void setStatusHistoryDetailsWithEndDate() {
        try {
            StatusHistoryAgentRObj statusHistoryAgent = EJBUtil
                    .getStatusHistoryAgentHome();
           
            this.setStatusId(statusHistoryAgent.setStatusHistoryDetailsWithEndDate(this
                    .createStatusHistoryStateKeeper()));

        } catch (Exception e) {
            Rlog.debug("statusHistory",
                    "Error in setStatusHistoryDetailsWithEndDate() in StatusHistoryJB "
                            + e);
        }
    }

    /**
     * Calls updateStatusHistory() of StatusHistory Session Bean:
     * StatusHistoryAgentBean
     * 
     * @return int 0 if successful else returns -2
     */
    public int updateStatusHistory() {
        int output;
        try {
            StatusHistoryAgentRObj statusHistoryAgent = EJBUtil
                    .getStatusHistoryAgentHome();

            output = statusHistoryAgent.updateStatusHistory(this
                    .createStatusHistoryStateKeeper());
        } catch (Exception e) {
            Rlog
                    .fatal("statusHistory",
                            "EXCEPTION IN SETTING StatusHistory DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    
    //Added by Manimaran for the July-August Enhancement S4.
    /**
     * Calls updateStatusHistoryWithEndDate() of StatusHistory Session Bean:
     * StatusHistoryAgentBean
     * 
     * @return int 0 if successful else returns -2
     */
    public int updateStatusHistoryWithEndDate() {
        int output;
        try {
            StatusHistoryAgentRObj statusHistoryAgent = EJBUtil
                    .getStatusHistoryAgentHome();
            output = statusHistoryAgent.updateStatusHistoryWithEndDate(this
                    .createStatusHistoryStateKeeper());
        } catch (Exception e) {
            Rlog
                    .fatal("statusHistory",
                            "EXCEPTION IN SETTING StatusHistory DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
        
    
    /**
     * 
     * 
     * @return the StatusHistory StateKeeper Object with the current values of
     *         the Bean
     */
    public StatusHistoryBean createStatusHistoryStateKeeper() {

        return new StatusHistoryBean(statusId, statusModuleId,
                statusModuleTable, statusCodelstId, statusStartDate,
                statusEndDate, statusNotes, statusCustom1, statusEnteredBy,
                creator, modifiedBy, ipAdd, recordType, isCurrentStat);

    }

    /**
     * To delete status
     * 
     * @return int 0 if successful, else -2
     */

    public int deleteStatusHistory() {
        int ret = 0;
        try {
            StatusHistoryAgentRObj statusHistoryAgentRObj = EJBUtil
                    .getStatusHistoryAgentHome();

            ret = statusHistoryAgentRObj.deleteStatusHistory(this
                    .createStatusHistoryStateKeeper());

            return ret;

        }

        catch (Exception e)

        {

            Rlog.fatal("statusHistory",
                    "StatusHistoryJB.deleteStatusHistory exception - " + e);

            return -2;

        }

    }

    // deleteStatusHistory() Overloaded for INF-18183 ::: Raviesh
    public int deleteStatusHistory(Hashtable<String, String> args) {
        int ret = 0;
        try {
            StatusHistoryAgentRObj statusHistoryAgentRObj = EJBUtil
                    .getStatusHistoryAgentHome();

            ret = statusHistoryAgentRObj.deleteStatusHistory(this
                    .createStatusHistoryStateKeeper(),args);

            return ret;

        }

        catch (Exception e)

        {

            Rlog.fatal("statusHistory",
                    "StatusHistoryJB.deleteStatusHistory exception - " + e);

            return -2;

        }

    }

    /**
     * 
     * This method get the records in er_status_history for a particular module
     * id and module table Calls getStatusHistoryInfo of StatusHistory Session
     * Bean
     * 
     * @param module
     *            pk : pk of module for which status history record are to be
     *            fetched
     * @param moduleTable:
     *            module table for which status history records are to b fetched
     * @return StatusHistoryDao: object of status history dao
     */

    public StatusHistoryDao getStatusHistoryInfo(int modulePK,
            String moduleTable) {
        int ret;
        StatusHistoryDao statusHistoryDao = new StatusHistoryDao();
        try {
            StatusHistoryAgentRObj statusHistoryAgentRObj = EJBUtil
                    .getStatusHistoryAgentHome();

            statusHistoryDao = statusHistoryAgentRObj.getStatusHistoryInfo(
                    modulePK, moduleTable);
            return statusHistoryDao;
        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Error in getStatusHistoryInfo in StatusHistoryJB " + e);

        }
        return statusHistoryDao;
    }

    /**
     * Method to get the latest status for a particular moduleId and
     * moduleTable. Calls getLatestStatus of StatusHistory Session Bean
     * 
     * @param modulePK
     *            PK of module for which latest status is to be retrieved
     * @param moduleTable :
     *            module table name for which latest status is to be retrieved
     * @returns String: codelst subtype value if successful else return null
     */

    public String getLatestStatus(int modulePK, String moduleTable) {
        String success = null;
        StatusHistoryDao statusHistoryDao = new StatusHistoryDao();
        try {
            StatusHistoryAgentRObj statusHistoryAgentRObj = EJBUtil
                    .getStatusHistoryAgentHome();

            success = statusHistoryAgentRObj.getLatestStatus(modulePK,
                    moduleTable);

        } catch (Exception e) {
            Rlog.fatal("statusHistory",
                    "Error in getStatusHistoryInfo in StatusHistoryJB " + e);

        }
        return success;
    }
    
    /**
     * Method to get the latest status history ID for a particular modulePK and moduleTable.
     * 
     * @param modulePK
     *            PK of module for which the latest status is to be retrieved
     * @param moduleTable :
     *            Table name for which the latest status is to be retrieved
     * @returns int: PK of the ER_STATUS_HISTORY table
     */
    public int getLatestStatusId(int modulePK, String moduleTable) {
    	StatusHistoryAgentRObj statusHistoryAgentRObj = EJBUtil.getStatusHistoryAgentHome();
    	return statusHistoryAgentRObj.getLatestStatusId(modulePK, moduleTable);
    }

}// end of class
