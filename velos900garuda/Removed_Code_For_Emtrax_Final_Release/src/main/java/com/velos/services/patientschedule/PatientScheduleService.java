package com.velos.services.patientschedule;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
/**
 * Remote Interface declaring methods from service implementation 
 * @author Virendra
 *
 */
@Remote
public interface PatientScheduleService{
	/**
	 * 
	 * @param patientId
	 * @param studyIdentifier
	 * @return PatSchedule
	 * @throws OperationException
	 */
	public ArrayList<PatientSchedule> getPatientSchedules(PatientIdentifier patientId, StudyIdentifier studyIdentifier) throws OperationException;
	}