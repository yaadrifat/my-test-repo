package com.velos.services.studypatient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.OperationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.util.ServicesUtil;

public class StudyPatientDAO extends CommonDAO{
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(StudyPatientDAO.class);
	
	public static String getSQLString(){
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append(" Select distinct p.rowid as rowcount,erv.fk_per, p.person_lname as mask_person_lname, p.person_fname as mask_person_fname, "); 
		sbSQL.append(" erv.per_code,");
		sbSQL.append(" erv.PATSTUDYSTAT_SUBTYPE, ");
		sbSQL.append(" (select site_name from ER_SITE where pk_site = p.fk_site) site_name,");  
		sbSQL.append(" erv.patprot_enroldt as patprot_enroldt_datesort,"); 
		sbSQL.append(" erv.last_visit_name,erv.cur_visit, erv.cur_visit_date, erv.next_visit as next_visit_datesort, ");
		sbSQL.append(" lower(p.person_lname || p.person_fname) mask_patnamelower,lower(erv.PATPROT_PATSTDID) lowerpatstdid,erv.PATPROT_PATSTDID,");
		sbSQL.append(" assignedto_name,physician_name,enrolledby_name,treatingorg_name");
//		sbSQL.append(" (select tx_name from er_studytxarm where pk_studytxarm=(select fk_studytxarm from er_pattxarm z where Z.FK_PATPROT=erv.PK_PATPROT and pk_pattxarm = (  ") ;
//		sbSQL.append(" select max(pk_pattxarm) from er_pattxarm z where tx_start_date=(select max(tx_start_date) from er_pattxarm y where y.FK_PATPROT=erv.PK_PATPROT ) and Z.FK_PATPROT=erv.PK_PATPROT ) )  )  current_tx_arm,") ;
		sbSQL.append("  from erv_studypat_by_visit erv ,  erv_person p where P.pk_person = erv.fk_per ");
		sbSQL.append(" and erv.fk_study = ? ");
        sbSQL.append(" and exists (select * from ER_PATFACILITY fac where fac.fk_per = erv.fk_per  ");
        sbSQL.append(" and fac.fk_site in ( ? ) and fac.patfacility_accessright > 0 ) ");
		String masterSql = sbSQL.toString();
		return masterSql;
	}
	

	
	public static List<StudyPatient> getStudyPatientByStudyPK(Integer studyPK, Integer sitePK) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		ArrayList<StudyPatient> lstStudyPatient = new ArrayList<StudyPatient>();
		
		try{
			
			conn = getConnection();

			//if (logger.isDebugEnabled()) logger.debug(" sql:" + sql);
			StudyPatient studyPatient = null;
			pstmt = conn.prepareStatement(getSQLString());
			pstmt.setInt(1, studyPK);
			pstmt.setInt(2, sitePK);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				studyPatient =  new StudyPatient();
				PatientIdentifier patientIdentifier = new PatientIdentifier();
				//create or get person object map for studyPatient
				ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
				ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, rs.getInt("fk_per"));
				String strOID = obj.getOID();
				
				patientIdentifier.setPatientId(rs.getString("per_code"));
				patientIdentifier.setOID(strOID);
				studyPatient.setPatientIdentifier(patientIdentifier);
				
				studyPatient.setStudyPatId(rs.getString("PATPROT_PATSTDID"));
				studyPatient.setStudyPatFirstName(rs.getString("mask_person_lname"));
				studyPatient.setStudyPatLastName(rs.getString("mask_person_fname"));
				lstStudyPatient.add(studyPatient);
			}
			
		}
		catch(Throwable t){
			t.printStackTrace();
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return lstStudyPatient;
		
	}
	
}