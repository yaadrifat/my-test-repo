package com.velos.services.patientschedule;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.common.SchCodeDao;
import com.velos.services.Issue;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.Duration;
import com.velos.services.model.Event;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.EventStatus;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.model.Visit;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.patientdemographics.PatientDemographicsDAO;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;
import com.velos.services.util.ServicesUtil;

/**
 * Data access object dealing with 
 * @author Virendra
 *
 */
public class PatientScheduleDAO extends CommonDAO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(PatientDemographicsDAO.class);
	private static String getPatScheduleSql="Select distinct(PATPROT_START), FK_PATPROT,PATPROT_STAT," +
			"PERSON_CODE,PAT_STUDYID,PERSON_NAME," +
			"PROTOCOL_NAME,SITE_NAME,STUDY_NUMBER" +
			" from ERV_PATSCH where fk_per = ? and fk_study = ?";
	protected static ResponseHolder response = new ResponseHolder();
	Issue issue = new Issue();
	SchCodeDao schCodeDao = new SchCodeDao(); 
	//EventStatus evenStatus = new EventStatus();
	static String siteName = "";
	/**
	 * 
	 * @param personPk
	 * @param studyPk
	 * @return
	 * @throws OperationException
	 */
	public static HashMap getPatientSchedule(int personPk, int studyPk) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		PatientSchedule patSchedule = null;
		HashMap map = new HashMap();
		ArrayList<Integer> lstPatProtPK = new ArrayList<Integer>();
		ArrayList<PatientSchedule> lstPatientSchedule = new ArrayList<PatientSchedule>();
		
		try{
			//patientPK=28629;
			conn = getConnection();

			if (logger.isDebugEnabled()) logger.debug(" sql:" + getPatScheduleSql);
			
			pstmt = conn.prepareStatement(getPatScheduleSql);
			pstmt.setInt(1, personPk);
			pstmt.setInt(2, studyPk);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				//Fixed:# 6059, reopened issue. initial assign null
				//and instantiate if schedule exists.
				patSchedule = new PatientSchedule();
				
				StudyPatient studyPatient = new StudyPatient();
				PatientIdentifier patientIdentifier = new PatientIdentifier();
				//create or get person object map for studyPatient
				ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
				ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personPk);
				String strPerOID = obj.getOID();
				
				patientIdentifier.setPatientId(rs.getString("PERSON_CODE"));
				patientIdentifier.setOID(strPerOID);
				studyPatient.setPatientIdentifier(patientIdentifier);
				
				studyPatient.setStudyPatId(rs.getString("PAT_STUDYID"));
				//Virendra:#6259
				String name= rs.getString("PERSON_NAME").trim();
				if(name != null){
					String[] arrName = name.split(",");
					if(arrName.length > 0){
						String fName = arrName[1];
						String lName = arrName[0];
						
						studyPatient.setStudyPatFirstName(fName);
						studyPatient.setStudyPatLastName(lName);
					}
				}
				
				StudyIdentifier studyIdentifier = new StudyIdentifier();
				
				ObjectMap objMapStudy = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPk);
				String strStudyOID = objMapStudy.getOID();
				studyIdentifier.setStudyNumber(rs.getString("STUDY_NUMBER"));
				studyIdentifier.setOID(strStudyOID);
				studyPatient.setPatStudyIdentifier(studyIdentifier);
				
				OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
				siteName = rs.getString("SITE_NAME");
				orgIdentifier.setSiteName(siteName);
				studyPatient.setStudyPatEnrollingSite(orgIdentifier);
				
				studyPatient.setStudyPatEnrollDate(rs.getDate("PATPROT_START"));
				patSchedule.setStudyPatient(studyPatient);
				//Viendra:#6061, for more than one schedule names
				patSchedule.setScheduleName(rs.getString("PROTOCOL_NAME")+","+(rs.getDate("PATPROT_START").toString()));
				
				patSchedule.setCurrent(rs.getString("PATPROT_STAT").equals("1") ? true : false);
				lstPatientSchedule.add(patSchedule);
				
				Integer patProtPK = rs.getInt("FK_PATPROT");
				lstPatProtPK.add(patProtPK);
				map.put("lstPatientSchedule", lstPatientSchedule);
				map.put("lstPatProtPK", lstPatProtPK);
			}
			
			return map;
		}
		catch(Throwable t){
	
			t.printStackTrace();
			
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		
	}
	/**
	 * 
	 * @param personPk
	 * @param StudyPk
	 * @return
	 * @throws OperationException
	 */
	public static ArrayList<Integer> getVisitPkPatProtPk(int personPK, int StudyPK, int patProtPK) throws OperationException{
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String sql = "";
	   
	    ArrayList<Integer> lstVisitPk = new ArrayList<Integer>();
	    try {
	    	conn = getConnection();
	
	        sql= "select distinct(ev1.fk_visit),p.pk_patprot,p.fk_protocol,ev.name as protocol_name, " +
	        		"p.patprot_start patprot_start, p.fk_study " +
	        		"from er_patprot p, event_assoc ev,SCH_EVENTS1 ev1 " +
	        		"where p.fk_study = ?  and  p.fk_per = ? and p.pk_patprot= ? and ev.event_id = p.fk_protocol " +
	        		"and ev1.fk_patprot = p.pk_patprot and ev.event_type = 'P'" +
	        		" order by pk_patprot desc";	
		          pstmt = conn.prepareStatement(sql);
		          pstmt.setInt(1, StudyPK);                      
		          pstmt.setInt(2, personPK);  
		          pstmt.setInt(3, patProtPK); 
	        ResultSet rs = pstmt.executeQuery();
	        
	          while (rs.next()) {
	          	
	          	Integer VisitPk = rs.getInt("fk_visit");
	          	lstVisitPk.add(VisitPk);
	          	
	          }
	          return lstVisitPk;
	          
	    } 
	    catch(Throwable t){
	    	t.printStackTrace();
	    	throw new OperationException();
	}
	finally {
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (Exception e) {
		}
		try {
			if (conn != null)
				conn.close();
			} catch (Exception e) {
		}
	    } 
	
	}
	/**
	 * 
	 * @param VisitPk
	 * @param patProtPk
	 * @param userAccountPk
	 * @return
	 * @throws OperationException
	 */
	public Visit getVisitsEvents(int visitPk, int patProtPk, int userAccountPk) throws OperationException{
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    Visit visit = new Visit();
	    VisitIdentifier visitIdentifier = new VisitIdentifier();
	    ArrayList<Event> lstEvent = new ArrayList<Event>();
	    CodeCache codeCache = CodeCache.getInstance();
	    
	    ObjectMapService objectMapService = ServicesUtil.getObjectMapService(); 
	  	ObjectMap obj2 = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, visitPk);
	  	String OID = obj2.getOID();
	  	visitIdentifier.setOID(OID);
	  	visit.setVisitIdentifier(visitIdentifier);
	    
	    try {
	    	conn = getConnection();
	
	    	String scheduleSql=   "select ev.DESCRIPTION, to_char(START_DATE_TIME, 'yyyy-mm-dd') START_DATE_TIME, "
				+ " nvl(evass.FUZZY_PERIOD,0) FUZZY_PERIOD ,"
				+ " evass.EVENT_DURATIONBEFORE,evass.EVENT_FUZZYAFTER,evass.EVENT_DURATIONAFTER,"
				+ " ev.EVENT_ID,estat.PK_EVENTSTAT, estat.EVENTSTAT_DT, estat.EVENTSTAT_NOTES,"//EVENTSTAT_DT,ev.SERVICE_SITE_ID,ev.REASON_FOR_COVERAGECHANGE,EVENTSTAT_NOTES,,
				+ " ev.ISCONFIRMED, "
				+ " ev.FK_ASSOC, "
				+ " ev.NOTES, "
				+ "	 to_char(ev.EVENT_EXEON, PKG_DATEUTIL.F_GET_DATEFORMAT) as EVENT_EXEON, "
				+ "	to_char(ev.ACTUAL_SCHDATE, 'yyyy-mm-dd')  ACTUAL_SCHDATE, "
				+ "	ev.EVENT_EXEBY, "
				+ "	ev.ADVERSE_COUNT, "
				+ " ev.visit,vis.pk_protocol_visit, "
				+ " vis.visit_name," 
				+ " vis.win_after_unit, vis.win_after_number, vis.win_before_unit,vis.win_before_number," 
				+ "ev.fk_visit, "
				+ " ev.status,ev.event_notes, (select count(*) from sch_event_kit where fk_event = ev.fk_assoc) kitCount ,pkg_gensch.f_get_event_roles(ev.fk_assoc) event_roles, vis.no_interval_flag interval_flag,  "
				+ " ev.SERVICE_SITE_ID,ev.FACILITY_ID,ev.FK_CODELST_COVERTYPE, ev.REASON_FOR_COVERAGECHANGE "
				+ " FROM SCH_EVENTS1 ev"
				+ " , SCH_PROTOCOL_VISIT vis"
				+ " , EVENT_ASSOC evass, SCH_EVENTSTAT estat"
				+ " WHERE  ev.fk_patprot = ?" 
				+ " and ev.fk_visit = ?" 
				+ " and ev.fk_visit = vis.pk_protocol_visit "
				+ " and evass.EVENT_ID = ev.FK_ASSOC and estat.FK_EVENT = ev.EVENT_ID "
				//Virendra:Fixed #6117, modified query for event with latest event status
				+ " and estat.pk_eventstat = (select max(estat1.pk_eventstat) "
				+ "from sch_eventstat estat1 where estat1.fk_event = ev.event_id ) ";
	    	
		          pstmt = conn.prepareStatement(scheduleSql);
		          pstmt.setInt(1, patProtPk);                      
		          pstmt.setInt(2, visitPk);             
	        ResultSet rs = pstmt.executeQuery();
	        
	          while (rs.next()) {
	        	  
	        	 
	        	  
	        	  visit.setVisitName(rs.getString("VISIT_NAME"));
	        	  Date suggestedStartDate = rs.getDate("START_DATE_TIME");
	        	  Date actualScheduleDate =rs.getDate("ACTUAL_SCHDATE");
	        	  visit.setVisitSuggestedDate(suggestedStartDate);
	        	  visit.setVisitScheduledDate(actualScheduleDate);
	        	  //VISIT WINDOW
	        	  Integer visitWinBeforeNumber = rs.getInt("WIN_BEFORE_NUMBER");
	        	  String visitWinBeforeUnit = rs.getString("WIN_BEFORE_UNIT");
	        	  Integer visitWinAfterNumber = rs.getInt("WIN_AFTER_NUMBER");
	        	  String visitWinAfterUnit = rs.getString("WIN_AFTER_UNIT");
	        	  
	        	  StringBuffer visitWindowSB = new StringBuffer();
	        	  String visitWindow = null;
	        	 
	        	  if (visitWinBeforeNumber != null && visitWinBeforeNumber != 0 &&
	        			  visitWinBeforeUnit != null && visitWinBeforeUnit.length() > 0){
		        	  Date beginDate = calculateDate(actualScheduleDate, -1*visitWinBeforeNumber,
		        			  visitWinBeforeUnit);
		        	  visitWindowSB.append(DateUtil.dateToString(beginDate)).append("~");
	        	  }
	        	  else{
	        		  visitWindowSB = null;
	        	  }
	        	  if (visitWinAfterNumber != null && visitWinAfterNumber != 0 &&
	        			  visitWinAfterUnit != null && visitWinAfterUnit.length() > 0){
		        	  Date endDate = calculateDate(actualScheduleDate, visitWinAfterNumber,
		        			  visitWinAfterUnit);
		        	  visitWindowSB.append(DateUtil.dateToString(endDate));
	        	  }
	        	  else{
	        		  visitWindowSB = null;
	        	  }
	        	  if(visitWindowSB != null){
		        	  visitWindow = visitWindowSB.toString();
		        	  visit.setVisitWindow(visitWindow);
	        	  }
	        	  //VISIT WINDOW END
	        	  ObjectMap obj = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT, rs.getInt("EVENT_ID"));
//	        	  String strOID = obj.getOID();
	        	  Event patEvent = new Event();
	        	  EventIdentifier eventId = new EventIdentifier();
	        	  eventId.setOID(obj.getOID());
	        	  patEvent.setEventIdentifier(eventId);
	        	  
	        	  patEvent.setEventSuggestedDate(suggestedStartDate);
	        	  patEvent.setEventScheduledDate(actualScheduleDate);
	        	  
	        	  //setting eventstatus
	        	
	        	  /*
	        	   * Code studyStatusCodeType = 
						codeCache.getCodeSubTypeByPK(
								CodeCache.CODE_TYPE_STATUS_TYPE, 
								rs.getInt("STATUS_TYPE"),
								userAccountPK1);
	        	   * 
	        	   * 
	        	   * 
	        	   * */
	        	  EventStatus eventStatus = new EventStatus();
	        	  Integer eventStatusPk = rs.getInt("ISCONFIRMED");
	        	  Code eventStatusCode = 
						codeCache.getSchCodeSubTypeByPK(
								CodeCache.CODE_TYPE_EVENT_STATUS_TYPE, 
								eventStatusPk,
								userAccountPk);
	        	  
	        	  Code coverageTypeCode = 
						codeCache.getSchCodeSubTypeByPK(
								CodeCache.CODE_TYPE_COVERAGE_TYPE, 
								rs.getInt("FK_CODELST_COVERTYPE"),
								userAccountPk);
	        	  
	        	  //Virendra: Fixed#6118
	        	  String reasonForChangeCoverType = rs.getString("REASON_FOR_COVERAGECHANGE");
	        	  
	        	  Date eventStatusDateValidFrom = rs.getDate("EVENTSTAT_DT");
	        	  
	        	  String eventStatusNotes = rs.getString("EVENTSTAT_NOTES");
	        	  
//	        	  OrganizationIdentifier organizationIdService = new OrganizationIdentifier();
//	        	  Integer serviceSitePK = rs.getInt("SERVICE_SITE_ID");
	        	  
	        	  OrganizationIdentifier orgIdentifier = new OrganizationIdentifier();
	        	  orgIdentifier.setSiteName(siteName);
	        	  
	        	  
	        	  
	        	  if(eventStatusCode != null){
	        		  eventStatus.setEventStatusCode(eventStatusCode);
	        	  }
	        	  if(coverageTypeCode != null){
	        		  eventStatus.setCoverageType(coverageTypeCode);
	        	  }
	        	 
	        	  eventStatus.setStatusValidFrom(eventStatusDateValidFrom);
	        	  eventStatus.setSiteOfService(orgIdentifier);
	        	  eventStatus.setReasonForChangeCoverType(reasonForChangeCoverType);
	        	  eventStatus.setNotes(eventStatusNotes);
	        	  
	        	  patEvent.setEventStatus(eventStatus);
	        	
	        	  
	        	  patEvent.setDescription(rs.getString("DESCRIPTION"));
	        	  Integer winBeforeNumber = rs.getInt("FUZZY_PERIOD");
	        	  String winBeforeUnit = rs.getString("EVENT_DURATIONBEFORE");
	        	  Integer winAfterNumber = rs.getInt("EVENT_FUZZYAFTER");
	        	  String winAfterUnit = rs.getString("EVENT_DURATIONAFTER");
	        	  //Duration dur = new Duration();
	        	  //dur.setUnit(TimeUnits.);
	        	  //EVENT WINDOW
	        	  //String strActualDate = actualDate.toString();
	        	  StringBuffer windowSB = new StringBuffer();
	        	  String eventWindow = null;
	        	 
	        	  if (winBeforeNumber != null && winBeforeNumber != 0 &&
	        			  winBeforeUnit != null && winBeforeUnit.length() > 0){
		        	  Date beginDate = calculateDate(actualScheduleDate, -1*winBeforeNumber,
		                        winBeforeUnit);
		        	  windowSB.append(DateUtil.dateToString(beginDate)).append("~");
	        	  }
	        	  else{
	        	  windowSB = null;
	        	  }
	        	  if (winAfterNumber != null && winAfterNumber != 0 &&
	        			  winAfterUnit != null && winAfterUnit.length() > 0){
		        	  Date endDate = calculateDate(actualScheduleDate, winAfterNumber,
		                        winAfterUnit);
		        	  windowSB.append(DateUtil.dateToString(endDate));
	        	  }
	        	  else{
		        	  windowSB = null;
	        	  }
	        	  if(windowSB != null){
		        	  eventWindow = windowSB.toString();
		        	  patEvent.setEventWindow(eventWindow);
	        	  }
	        	  //EVENT WINDOW END
	        	 
	        	  //setting status coverage type
	        	 
	        	  if(coverageTypeCode != null){
	        		  patEvent.setCoverageType(coverageTypeCode);
	        	  }
	        	  
	        	  lstEvent.add(patEvent);
	        	  visit.setEvents(lstEvent);
	          }
		} 
		catch(Throwable t){
			
			t.printStackTrace();
			throw new OperationException();
	}
	finally {
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (Exception e) {
		}
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
		}
		
    	} 
		return visit;
	}
	/**
	 * 
	 * @param startDate
	 * @param offsetNumber
	 * @param offsetUnit
	 * @return
	 */
	 private Date calculateDate(Date startDate, Integer offsetNumber, String offsetUnit) {
	        Calendar cal1 = Calendar.getInstance();
	        cal1.setTime(startDate);
	        cal1.add(convertUnitToField(offsetUnit), offsetNumber);
	        return cal1.getTime();
    }
    /**
     * 
     * @param offsetUnit
     * @return
     */
    private int convertUnitToField(String offsetUnit) {
        if (offsetUnit == null) { return 0; }
        if ("D".equals(offsetUnit.toUpperCase())) { return Calendar.DAY_OF_YEAR; }
        if ("W".equals(offsetUnit.toUpperCase())) { return Calendar.WEEK_OF_YEAR; }
        if ("M".equals(offsetUnit.toUpperCase())) { return Calendar.MONTH; }
        if ("Y".equals(offsetUnit.toUpperCase())) { return Calendar.YEAR; }
        return 0;
    }
    /**
     * get Eventstatus with StatusPK 
     * @param statusPk
     * @return
     */
//	private EventStatus getEventStatus(int statusPk){
//		
//		  EventStatAgentRObj eventStatAgent = EJBUtil.getEventStatAgentHome();
//		  EventStatus eventStatus = new EventStatus();
//		  
//    	  EventStatBean eventStatBean =  eventStatAgent.getEventStatDetails(statusPk);
//    	 
//    	  eventStatus.setNotes(eventStatBean.getEvtStatNotes());
//    	  
//    	  String statusCodeSubType = schCodeDao.getCodeSubtype(statusPk);
//    	  String statusCodeDesc = schCodeDao.getCodeDescription(statusPk);
//    	  Code statusCode = new Code();
//    	  statusCode.setType(statusCodeSubType);
//    	  statusCode.setDescription(statusCodeDesc);
//    	  Date eventStatDate = eventStatBean.getEventStatDt();
//    	  eventStatus.setStatusValidFrom(eventStatDate);
//    	  
//    	  return eventStatus;
//	}
    public ArrayList<String> getScheduleNames(){
    	ArrayList<String> lstScheduleNames = new ArrayList<String>();
    	return lstScheduleNames;
    }
    public ArrayList<Integer> getDeactivatedEvents(int patientId, int protId, java.sql.Date bookedOn){	
		PreparedStatement pstmt = null;
	    Connection conn = null;
	    String sql = "";
	   
	    ArrayList<Integer> lstEventPk = new ArrayList<Integer>();
	    try {
	    	conn = getConnection();
	
	        sql= "select EVENT_ID FROM SCH_EVENTS1 " 
	        	+ "WHERE PATIENT_ID= LPAD(TO_CHAR(?),10,'0') AND " 
	        	+ "SESSION_ID= LPAD(TO_CHAR(?),10,'0') AND bookedon=?";	
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, patientId);                      
			pstmt.setInt(2, protId);  
			pstmt.setDate(3, bookedOn); 
	        ResultSet rs = pstmt.executeQuery();
	        
			while (rs.next()) {
			  	Integer eventPk = rs.getInt("EVENT_ID");
			  	lstEventPk.add(eventPk);
			}
	    } 
	    catch(Exception e){
	    	e.printStackTrace();
	    	return null;
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
				} catch (Exception e) {
			}
		}
		return lstEventPk;
	}
	
}
