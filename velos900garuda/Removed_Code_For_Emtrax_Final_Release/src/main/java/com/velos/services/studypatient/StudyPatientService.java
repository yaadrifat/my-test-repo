package com.velos.services.studypatient;

import java.util.List;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
/**
 * Remote Interface with declaration of methods in Service Implementation.
 * @author Virendra
 *
 */
@Remote
public interface StudyPatientService{
	/**
	 * 
	 * @param studyId
	 * @return
	 * @throws OperationException
	 */
	public List<StudyPatient> getStudyPatients(StudyIdentifier studyId) throws OperationException;

}