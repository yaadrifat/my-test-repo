package com.velos.services.patientdemographics;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;

/**
 * A remote service supporting the PatientDemographics service messages.
 * 
 * @author virendra
 *
 */
@Remote
public interface PatientDemographicsService{ 
	/**
	 * PatientDemographics remote service
	 * @param patientId
	 * @return
	 * @throws OperationException
	 */
	
public PatientDemographics getPatientDemographics(PatientIdentifier patientId) throws OperationException;

}