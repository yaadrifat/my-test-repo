package com.velos.services.patientdemographics;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.business.patProt.impl.PatProtBean;
import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.business.site.impl.SiteBean;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.Code;
import com.velos.services.model.NVPair;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

@Stateless
@Remote(PatientDemographicsService.class)
public class PatientDemographicsServiceImpl 
extends AbstractService 
implements PatientDemographicsService{
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private PersonAgentRObj personAgent;
@Resource 
private SessionContext sessionContext;
@EJB
private UserAgentRObj userAgent;
@EJB
private SiteAgentRObj siteAgent;
@EJB
private ObjectMapService objectMapService;
@EJB
private PatProtAgentRObj patProtAgent;
@EJB
private UserSiteAgentRObj userSiteAgent;
@EJB
private PatFacilityAgentRObj patFacilityAgent;



Integer personPK=0;
Boolean hasViewManageCompletePatient= true;

private static Logger logger = Logger.getLogger(PatientDemographicsServiceImpl.class.getName());
/**
 * @author Virendra
 * Implementation class for Monitoring Services operations.
 *
 */
public PatientDemographics getPatientDemographics(PatientIdentifier patientId)
			throws OperationException {
	
		try{
		    callingUser = 
		        getLoggedInUser(
		                sessionContext,
		                userAgent);
		    //fetching personPK from objectlocator with patientId
		    //		PersonDao personDao = new PersonDao();
		    Integer personPK = ObjectLocator.personPKFromPatientIdentifier(
		            callingUser, 
		            patientId, 
		            objectMapService);


		    //if person not found, add an issue
		    if(personPK == null | personPK ==0){
		        addIssue(new Issue(
		                IssueTypes.PATIENT_NOT_FOUND, 
		                "Patient not found for code: "+patientId.getPatientId()));
		        throw new OperationException();
		    }
            PersonBean personBean = personAgent.getPersonDetails(personPK);
            if (personBean == null){
                addIssue(new Issue(
                        IssueTypes.PATIENT_NOT_FOUND, 
                        "Patient not found for code: "+patientId.getPatientId()+" OID: "+patientId.getOID()));
                throw new OperationException();
            }
            if (callingUser.getUserAccountId() == null
                    || !callingUser.getUserAccountId().equals(personBean.getPersonAccount())){
                addIssue(new Issue(
                        IssueTypes.PATIENT_NOT_FOUND, 
                        "Patient not found for OID: "+patientId.getOID()));
                throw new OperationException();
            }
            
			//checking for group rights
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			
			Integer managePatientPriv =groupAuth.getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();//getAppManagePatientsPrivileges();
			boolean hasViewManagePatient = 
				GroupAuthModule.hasViewPermission(managePatientPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + managePatientPriv);
			if (!hasViewManagePatient){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User not authorized to view patient data"));
				throw new AuthorizationException("User not authorized to view patient data");
			}
			
			// #6228: Check patient facility rights; this is the max rights among all facilities of this patient
			Integer patFacilityRights = userSiteAgent.getUserPatientFacilityRight(callingUser.getUserId(), personPK);
			if (!GroupAuthModule.hasViewPermission(patFacilityRights)) {
                if (logger.isDebugEnabled()) logger.debug("User not authorized to access data of this patient");
                addIssue(
                        new Issue(IssueTypes.PATIENT_DATA_AUTHORIZATION, 
                                "User not authorized to access data of this patient"));
                throw new AuthorizationException("User not authorized to access data of this patient");
			}
			
			ArrayList<Integer> lstPatProtPK = ObjectLocator.getPatProtPkFromPersonPk(personPK, objectMapService);
			for(Integer patProtPK: lstPatProtPK){
				
				PatProtBean patProtBean =   patProtAgent.getPatProtDetails(patProtPK);
				Integer studyPK =  EJBUtil.stringToInteger(patProtBean.getPatProtStudyId());
				
				//Virendra:#6073
				TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
				int viewPatientPrivileges = teamAuthModule.getPatientViewDataPrivileges();
				if(!TeamAuthModule.hasViewPermission(viewPatientPrivileges) ){
					hasViewManageCompletePatient=false;
					break;
				}
				
			}
			//Checking if user has view patient complete data rights
			//if then show complete patient data otherwise show only
			//PHI data.
			Integer managePatientPrivComplete = 
				groupAuth.getAppViewCompletePatientDataPrivileges();
			Integer CompleteDetailsAccessRight = 
				personAgent.getPatientCompleteDetailsAccessRight(
						callingUser.getUserId(),Integer.parseInt(callingUser.getUserGrpDefault()) ,
						personPK);
			//setting boolean hasViewManageCompletePatient to false if user does not have
			//privilege to view complete data.
			if(managePatientPrivComplete == 3 || CompleteDetailsAccessRight== 0 ){
				hasViewManageCompletePatient=false;
			}
				
			PatientDemographics patientDemographics =
				marshalPatientDemographics(personBean);
			
			return patientDemographics;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

	}
	/**
	 * marshals patientDemographics with input of personBean
	 * @param personBean
	 * @return
	 * @throws OperationException
	 * @throws AuthorizationException
	 * @throws ParseException
	 */

	private PatientDemographics marshalPatientDemographics(PersonBean personBean) 
	throws 
	OperationException, 
	AuthorizationException, ParseException{
		
		PatientDemographics returnPatientDemographics = new PatientDemographics();
		Integer callingUserAccountId = Integer.valueOf(callingUser.getUserAccountId());
		CodeCache codeCache = CodeCache.getInstance();
		//Integer userAccountPK = EJBUtil.stringToInteger(callingUser.getUserAccountId());
		PatientIdentifier patiIdentifier = marshallPatientIdentifier(personBean);
		//patiIdentifier.setPatientId(personBean.getPersonPId());
		//patiIdentifier.setOID(personBean.get)
		returnPatientDemographics.setPatientId(patiIdentifier);
				
		Collection<NVPair> morePatientDetailsCollection =
			MorePatientDetailsDAO.getMorePatientDetails(
					personBean.getPersonPKId(),
					EJBUtil.stringToInteger(callingUser.getUserGrpDefault())).values();
		
		if (morePatientDetailsCollection != null){
			List<NVPair> morePatientDetailsList = new ArrayList<NVPair>();
			
			morePatientDetailsList.addAll(morePatientDetailsCollection);

			returnPatientDemographics.setMorePatientDetails(morePatientDetailsList);
		}
		
		Code cdSurvivalStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, personBean.getPersonStatus(), callingUserAccountId);
		returnPatientDemographics.setSurvivalStatus(cdSurvivalStatus);
		
		Code cdEthnicity = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_ETHNICITY, personBean.getPersonEthnicity(), callingUserAccountId);
		returnPatientDemographics.setEthnicity(cdEthnicity);
		
		Code cdRace = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_RACE, personBean.getPersonRace(), callingUserAccountId);
		returnPatientDemographics.setRace(cdRace);
		
		Integer personLocationPK = EJBUtil.stringToInteger(personBean.getPersonLocation());
		SiteBean siteBean =siteAgent.getSiteDetails(personLocationPK);
		OrganizationIdentifier patOrg= marshallOrganizationIdentifier(siteBean);
		returnPatientDemographics.setOrganization(patOrg);
		
		returnPatientDemographics.setRegistrationDate(personBean.getPersonRegDt());
		
		if(hasViewManageCompletePatient){
			
		returnPatientDemographics.setFirstName(personBean.getPersonFname());
		returnPatientDemographics.setLastName(personBean.getPersonLname());
		returnPatientDemographics.setMiddleName(personBean.getPersonMname());
		
		Code cdGender = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_GENDER, personBean.getPersonGender(), callingUserAccountId);
		returnPatientDemographics.setGender(cdGender);
		returnPatientDemographics.setDateOfBirth(personBean.getPersonDb());
		returnPatientDemographics.setDeathDate(personBean.getPersonDeathDt());
		returnPatientDemographics.seteMail(personBean.getPersonEmail());
		returnPatientDemographics.setAddress1(personBean.getPersonAddress1());
		returnPatientDemographics.setAddress2(personBean.getPersonAddress2());
		returnPatientDemographics.setCity(personBean.getPersonCity());
		returnPatientDemographics.setState(personBean.getPersonState());
		returnPatientDemographics.setCounty(personBean.getPersonCounty());
		returnPatientDemographics.setHomePhone(personBean.getPersonHphone());
		returnPatientDemographics.setWorkPhone(personBean.getPersonBphone());
		returnPatientDemographics.setPatFacilityId(personBean.getPatientFacilityId());
		
		
		}else{
			returnPatientDemographics.setFirstName("*");
			returnPatientDemographics.setLastName("*");
			returnPatientDemographics.setMiddleName("*");
			
	//		Code cdGender = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_GENDER, personBean.getPersonGender(), callingUserAccountId);
	//		returnPatientDemographics.setGender();
			
	//		Code cdEthnicity = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_ETHNICITY, personBean.getPersonEthnicity(), callingUserAccountId);
	//		returnPatientDemographics.setEthnicity(cdEthnicity);
	//		
	//		Code cdRace = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_RACE, personBean.getPersonRace(), callingUserAccountId);
	//		returnPatientDemographics.setRace(cdRace);
			
			//returnPatientDemographics.setDateOfBirth(Date.valueOf("*"));
			
	//		Code cdSurvivalStatus = codeCache.getCodeSubTypeByPK(CodeCache.CODE_TYPE_PATIENT_SURVIVAl_STATUS, personBean.getPersonStatus(), callingUserAccountId);
	//		returnPatientDemographics.setSurvivalStatus(cdSurvivalStatus);
			
			//returnPatientDemographics.setDeathDate(Date.valueOf("*"));
			returnPatientDemographics.seteMail("*");
			returnPatientDemographics.setAddress1("*");
			returnPatientDemographics.setAddress2("*");
			returnPatientDemographics.setCity("*");
			returnPatientDemographics.setState("*");
			returnPatientDemographics.setCounty("*");
			returnPatientDemographics.setHomePhone("*");
			returnPatientDemographics.setWorkPhone("*");
			
	//		OrganizationIdentifier patOrg= new OrganizationIdentifier();
	//		Integer personLocationPK = EJBUtil.stringToInteger(personBean.getPersonLocation());
	//		SiteBean siteBean =siteAgent.getSiteDetails(personLocationPK);
	//		patOrg.setSiteName(siteBean.getSiteName());
	//		returnPatientDemographics.setOrganization(patOrg);
				
			returnPatientDemographics.setPatFacilityId("*");
	//		returnPatientDemographics.setRegistrationDate(personBean.getPersonRegDt());
			
		}
		
		return returnPatientDemographics;
	}
	/**
	 * marshalls patientIdentifier
	 * @param personBean
	 * @return
	 */
	private PatientIdentifier marshallPatientIdentifier(PersonBean personBean) {
		PatientIdentifier patientId = new PatientIdentifier(); 
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_PERSON, personBean.getPersonPKId()); 
		patientId.setOID(map.getOID());
		patientId.setPatientId(personBean.getPersonPId()); 
		
		return patientId; 
	}
	/**
	 * marshalls OrganizationIdentifier
	 * @param siteBean
	 * @return
	 */
	private OrganizationIdentifier marshallOrganizationIdentifier(SiteBean siteBean) {
		OrganizationIdentifier orgId = new OrganizationIdentifier(); 
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_ORGANIZATION, siteBean.getSiteId()); 
		orgId.setOID(map.getOID());
		orgId.setSiteName(siteBean.getSiteName()); 
		
		return orgId; 
	}

} 


	
