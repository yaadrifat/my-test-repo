package com.velos.esch.audit.service;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import com.velos.esch.audit.business.AuditRowEschBean;
import com.velos.esch.business.common.AuditRowEschDao;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { AuditRowEschAgent.class } )
public class AuditRowEschAgentBean implements AuditRowEschAgent {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

	public AuditRowEschBean getAuditRowEschBean(Integer id) {
		if (id == null || id == 0) { return null; }
		AuditRowEschBean AuditRowBean = null;
		try {
			AuditRowBean = (AuditRowEschBean) em.find(AuditRowEschBean.class, new Integer(id));
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowEschAgentBean.getAuditRowEschBean: "+e);
		}
		return AuditRowBean;
	}

	public int getRID(int pkey, String tableName, String pkColumnName) {
		int output = 0;
		AuditRowEschDao auditDao = new AuditRowEschDao();
		output = auditDao.getRID(pkey, tableName, pkColumnName);
		return output;
	}
	
	public int findLatestRaid(int rid, int userId) {
		int output = 0;
		try{
			Query query = em.createNamedQuery("findLatestAuditRowEschByUserIdRID");
			query.setParameter("rid", rid);
			query.setParameter("userId", userId+"");
			query.setParameter("userIdLike", userId+",%");
			
			ArrayList list = (ArrayList) query.getResultList();
			if (list == null || list.size() == 0) { return -1; }

			AuditRowEschBean auditRow = (AuditRowEschBean) list.get(0);
			output = ((Integer)auditRow.getId()).intValue();
		} catch (Exception e){
			Rlog.fatal("audit", "Exception in AuditRowEschAgentBean.findLatestRaid: "+e);
		}
		return output;
	}
	
	public int setReasonForChange(int raid, String remarks) {
		int output = 0;
		AuditRowEschDao auditDao = new AuditRowEschDao();
		output = auditDao.setReasonForChange(raid, remarks);
		return output;
	}
}
