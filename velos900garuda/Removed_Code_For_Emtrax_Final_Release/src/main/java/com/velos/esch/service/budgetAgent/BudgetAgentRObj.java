/*
 * Classname : BudgetAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 06/12/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.budgetAgent;

/* Import Statements */
import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.budget.impl.BudgetBean;
import com.velos.esch.business.common.BudgetDao;

@Remote
public interface BudgetAgentRObj {

    BudgetBean getBudgetDetails(int budgetId);

    public int setBudgetDetails(BudgetBean bdsk);

    public int updateBudget(BudgetBean bdsk);

    // Overloaded for INF-18183 ::: Akshi
    public int updateBudget(BudgetBean bdsk,Hashtable<String, String> args);

    public BudgetDao getBudgetInfo(int budgetId);

    public int copyBudget(int oldBudget, String newBudName, String newBudVer,
            int creator, String ipAdd);

    public BudgetDao getBudgetsForUser(int userId);
    
    public BudgetDao getCombBudgetForStudy(int userId, int studyId);

    public int copyBudgetForTemplate(int oldBudget, int budgetId, int creator,
            String ipAdd);

    public BudgetDao getBgtTemplateSubType(int bgtTemplate, int budgetId);

    /**
     * Get Default Budget PK For a Calendar
     * 
     * @param PKProtocol Protocol ID
     */

    public int getDefaultBudgetForCalendar(int PKProtocol) ;
}