/*
 * Classname : BgtSectionAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 03/21/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni */

package com.velos.esch.service.bgtSectionAgent.impl;

/* IMPORT STATEMENTS */

import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.bgtSection.impl.BgtSectionBean;
import com.velos.esch.business.common.BgtSectionDao;
import com.velos.esch.service.bgtSectionAgent.BgtSectionAgentRObj;
import com.velos.esch.service.util.Rlog;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.eres.service.util.DateUtil;
import javax.annotation.Resource;
import javax.ejb.SessionContext;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * BgtSectionBean.<br>
 * <br>
 * 
 */
@Stateless
public class BgtSectionAgentBean implements BgtSectionAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * 
     */
    private static final long serialVersionUID = 3257003242008228145L;

    /**
     * Calls getBgtSectionDetails() on BgtSection Entity Bean BgtSectionBean.
     * Looks up on the BgtSection id parameter passed in and constructs and
     * returns a BgtSection state holder. containing all the summary details of
     * the BgtSection<br>
     * 
     * @param bgtSectionId
     *            the BgtSection id
     * @ee BgtSectionBean
     */
    public BgtSectionBean getBgtSectionDetails(int bgtSectionId) {

        try {
            return (BgtSectionBean) em.find(BgtSectionBean.class, new Integer(
                    bgtSectionId));
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Exception In getBgtSectionDetails in BgtSectionAgentBean "
                            + e);
            return null;
            // System.out.print("Error in getBgtSectionDetails() in
            // BgtSectionAgentBean" + e);
        }

    }

    /**
     * Calls setBgtSectionDetails() on BgtSection Entity Bean
     * bgtSectionBean.Creates a new BgtSection with the values in the
     * StateKeeper object.
     * 
     * @param bgtSection
     *            a bgtSection state keeper containing bgtSection attributes for
     *            the new BgtSection.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setBgtSectionDetails(BgtSectionBean edsk) {

        try {
            BgtSectionBean bgtSection = new BgtSectionBean();
            bgtSection.updateBgtSection(edsk);
            em.persist(bgtSection);
            return (bgtSection.getBgtSectionId());
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Exception In setBgtSectionDetails in BgtSectionAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateBgtSection(BgtSectionBean edsk) {

        BgtSectionBean bgtSectionBean = null;
        int output;

        try {

            bgtSectionBean = (BgtSectionBean) em.find(BgtSectionBean.class,
                    edsk.getBgtSectionId());

            if (bgtSectionBean == null) {
                return -2;
            }
            output = bgtSectionBean.updateBgtSection(edsk);
            em.merge(bgtSectionBean);

        } catch (Exception e) {
            Rlog.debug("bgtSection",
                    "Error in updateBgtSection in BgtSectionAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit    
    public int updateBgtSection(BgtSectionBean edsk, Hashtable<String, String> auditInfo) {

        BgtSectionBean bgtSectionBean = null;
        int output;

        try {

            bgtSectionBean = (BgtSectionBean) em.find(BgtSectionBean.class,
                    edsk.getBgtSectionId());

            if (bgtSectionBean == null) {
                return -2;
            }
            
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            String getRidValue= AuditUtils.getRidValue("SCH_BGTSECTION","esch","PK_BUDGETSEC ="+edsk.getBgtSectionId());/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("SCH_BGTSECTION",String.valueOf(edsk.getBgtSectionId()),getRidValue,
        			userID,currdate,moduleName,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
         
            
            output = bgtSectionBean.updateBgtSection(edsk);
            em.merge(bgtSectionBean);

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("bgtSection",
                    "Error in updateBgtSection in BgtSectionAgentBean" + e);
            return -2;
        }
        return output;
    }

    public BgtSectionDao getBgtSections(int calId) {
        Rlog.debug("bgtSection", "In BgtSectionAgentBean getBudgetSections");
        try {
            BgtSectionDao bgtSectionDao = new BgtSectionDao();
            bgtSectionDao.getBgtSections(calId);
            return bgtSectionDao;
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Error in BgtSectionAgentBean getBudgetSections " + e);
        }
        return null;
    }

    /**
     * Delete section from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */

    public int bgtSectionDelete(int bgtSectionId) {
        BgtSectionBean bgtSectionBean = null;
        int ret = 0;
        try {

            bgtSectionBean = em.find(BgtSectionBean.class, new Integer(
                    bgtSectionId));
            em.remove(bgtSectionBean);
            Rlog.debug("BgtSection", "remove budget section");
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            Rlog
                    .fatal("BgtSection", "EXCEPTION IN REMOVING BUDGET SECTION"
                            + e);
            return -2;
        }
    }

    public int insertDefaultSecLineitems(int bgtSecId, int bgtCalId,
            int creator, String ipAdd) {

        BgtSectionDao bgtSecDao = new BgtSectionDao();
        try {

            bgtSecDao.insertDefaultSecLineitems(bgtSecId, bgtCalId, creator,
                    ipAdd);

        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "bgtsection agentbean.insertDefaultSecLineitems " + e);
            return -1;
        }
        return 0;
    }

    /**
     * Get all the sections (with calendar names) for a budget
     * 
     * @param budgetId
     */

    public BgtSectionDao  getAllBgtSections(int budgetId)
    {
    	Rlog.debug("bgtSection", "In BgtSectionAgentBean getAllBgtSections");
        try {
            BgtSectionDao bgtSectionDao = new BgtSectionDao();
            bgtSectionDao.getAllBgtSections(budgetId);
            return bgtSectionDao;
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Error in BgtSectionAgentBean getAllBgtSections " + e);
        }
        return null;
    	
    	
    }
    
    /** 
     * Update patient number for all per patient sections in a budget
     *  */
    public int applyNumberOfPatientsToAllSections(String sourceSectionPK,String bgtCalPK,String patNo,String last_modified_by,String ipAdd)
    {
    	int ret = -1;
        try {
            BgtSectionDao bgtSectionDao = new BgtSectionDao();
            ret = bgtSectionDao.applyNumberOfPatientsToAllSections(sourceSectionPK,bgtCalPK,patNo, last_modified_by,ipAdd);
            
            return ret ;
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Exception in BgtSectionAgentBean applyNumberOfPatientsToAllSections " + e);
        }
        return ret;
    	
    	
    }
    
    
}// end of class
