/*
 * Classname : EventcostAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 06/20/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.service.eventcostAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.esch.business.eventcost.impl.EventcostBean;

/* End of Import Statements */
@Remote
public interface EventcostAgentRObj {

    EventcostBean getEventcostDetails(int eventcostId);

    public int setEventcostDetails(EventcostBean account);

    public int updateEventcost(EventcostBean usk);
}