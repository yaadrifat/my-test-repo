/*
 * Classname : CrfAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/23/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.crfAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.common.CrfDao;
import com.velos.esch.business.crf.impl.CrfBean;
import com.velos.esch.service.crfAgent.CrfAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP CrfBean.<br>
 * <br>
 * 
 */
@Stateless
public class CrfAgentBean implements CrfAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /**
     * Calls getCrfDetails() on Crf Entity Bean CrfBean. Looks up on the Crf id
     * parameter passed in and constructs and returns a Crf state holder.
     * containing all the summary details of the Crf<br>
     * 
     * @param crfId
     *            the Crf id
     * @ee CrfBean
     */
    public CrfBean getCrfDetails(int crfId) {

        try {
            return (CrfBean) em.find(CrfBean.class, new Integer(crfId));
        } catch (Exception e) {
            System.out.print("Error in getCrfDetails() in CrfAgentBean" + e);
            return null;
        }

    }

    /**
     * Calls setCrfDetails() on Crf Entity Bean crfBean.Creates a new Crf with
     * the values in the StateKeeper object.
     * 
     * @param crf
     *            a crf state keeper containing crf attributes for the new Crf.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setCrfDetails(CrfBean edsk) {
        try {
            em.merge(edsk);
            return (edsk.getCrfId());
        } catch (Exception e) {
            System.out.print("Error in setCrfDetails() in CrfAgentBean " + e);
        }
        return 0;
    }

    public int updateCrf(CrfBean edsk) {

        CrfBean retrieved = null; // Crf Entity Bean Remote Object
        int output;

        try {

            retrieved = (CrfBean) em.find(CrfBean.class, new Integer(edsk
                    .getCrfId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateCrf(edsk);

        } catch (Exception e) {
            Rlog.debug("crf", "Error in updateCrf in CrfAgentBean" + e);
            return -2;
        }
        return output;
    }

    public CrfDao getCrfValues(int patProtId) {
        try {
            CrfDao crfDao = new CrfDao();
            crfDao.getCrfValues(patProtId);
            return crfDao;
        } catch (Exception e) {
            Rlog.fatal("crf", "Exception In getCrfValues in CrfAgentBean " + e);
        }
        return null;
    }

}// end of class
