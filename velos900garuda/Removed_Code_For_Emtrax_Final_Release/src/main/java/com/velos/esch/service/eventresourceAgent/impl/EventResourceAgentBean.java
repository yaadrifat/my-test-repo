/*
 * Classname : EventuserAgentBean
 *
 * Version information: 1.0
 *
 * Date: 02/15/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.service.eventresourceAgent.impl;

/* IMPORT STATEMENTS */
import com.velos.esch.business.audit.impl.AuditBean;
import java.util.ArrayList;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.esch.service.util.EJBUtil;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ejb.SessionContext;
import javax.annotation.Resource;

import com.velos.esch.business.eventresource.impl.EventResourceBean;
import com.velos.esch.service.eventresourceAgent.EventResourceAgentRObj;
import com.velos.esch.service.util.Rlog;

import com.velos.esch.business.common.EventResourceDao;
import java.util.Enumeration;
import java.util.Hashtable;
/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * EventResourceBean.<br>
 * <br>
 *
 */
@Stateless

public class EventResourceAgentBean implements EventResourceAgentRObj {
	@PersistenceContext(unitName = "esch")
	protected EntityManager em;
	@Resource private SessionContext context;

	/**
	 *
	 */
	private static final long serialVersionUID = 3257565122447683633L;

	public EventResourceBean getEventResourceDetails(int evtResTrackId) {

		try {

			return (EventResourceBean) em.find(EventResourceBean.class,
					new Integer(evtResTrackId));

		} catch (Exception e) {
			System.out
					.print("Error in getEventResourceDetails() in EventResourceAgentBean"
							+ e);
			return null;
		}

	}

	/**
	 * Calls setEventResourceDetails() on EventResource Entity Bean
	 * EventResourceBean. Creates a new EventResource with the values in the
	 * StateKeeper object.
	 *
	 * @param eventResource
	 *            a eventResource state keeper containing eventresopurce
	 *            attributes for the new EventResource.
	 * @return int - 0 if successful; -2 for Exception;
	 */

	public int setEventResourceDetails(EventResourceBean ersk) {
		try {
			ArrayList roleIds = ersk.getEvtRoles();
			ArrayList resDurations = ersk.getEvtTrackDurations();

			for (int i = 0; i < roleIds.size(); i++) {
				String roleId = (String) roleIds.get(i);
				ersk.setEvtRole(roleId);
				String duration = (String) resDurations.get(i);
				ersk.setEvtTrackDuration(duration);
				em.merge(ersk);
			}

			return (ersk.getEvtResTrackId());
		} catch (Exception e) {
			System.out
					.print("Error in setEventResourceDetails() in EventResourceAgentBean "
							+ e);
		}
		return 0;
	}


	/**
     * Deletes record
     * @param eventId  the fk_event for SCH_EVRES_TRACK
     */
	
	// Overloaded for INF-18183 ::: Akshi
	  public int removeEventResources(int eventId,Hashtable<String, String> args) {
		  EventResourceBean evresb = null;
	        try {
	        	
	        	 AuditBean audit=null;
	             String currdate =DateUtil.getCurrentDate();
	             String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
	             String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
	             String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
	             String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
	             String getRidValue= AuditUtils.getRidValue("SCH_EVRES_TRACK","esch","PK_EVRES_TRACK="+eventId);/*Fetches the RID/PK_VALUE*/ 
	             audit = new AuditBean("SCH_EVRES_TRACK",String.valueOf(eventId),getRidValue,
	         			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
	         	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
	        	
	        	
	        	 evresb = (EventResourceBean) em.find(EventResourceBean.class, new Integer(eventId));
	             em.remove(evresb);
	             return 0;
	        } catch (Exception e) {
	        	context.setRollbackOnly();
	        	Rlog.debug("eventresource","Error in removeEventResources in EventResourcesAgentBean" + e);
	   		  	return -2;
	        }

	  }
	  public int removeEventResources(int eventId) {


			  EventResourceBean evresb = null;
			  //int output;
		        try {
		        			        	 	        	
		            evresb = (EventResourceBean) em.find(EventResourceBean.class, new Integer(eventId));
		             em.remove(evresb);
		             return 0;
		        } catch (Exception e) {
		        	Rlog.debug("eventresource","Error in removeEventResources in EventResourcesAgentBean" + e);
		   		  	return -2;
		        }

		  }



	public int getStatusIdOfTheEvent(int eventId) {

		EventResourceDao evResDao = new EventResourceDao();
        int eventStatusId = 0;
        try {
            Rlog.debug("eventresource", "EventResourceAgentBean:getStatusIdOfTheEvent ...");
            eventStatusId = evResDao.getStatusIdOfTheEvent(eventId);

        } catch (Exception e) {
            Rlog.fatal("eventresource", "EventResourceAgentBean:getStatusIdOfTheEvent-Exception In eventStatusId "
                    + e);
            return eventStatusId;
        }
        return eventStatusId;

	}



	public EventResourceDao getAllRolesForEvent(int eventId) {

		EventResourceDao evResDao = new EventResourceDao();

        try {
            Rlog.debug("eventresource", "EventResourceAgentBean:getStatusIdOfTheEvent ...");
            evResDao.getAllRolesForEvent(eventId);
            Rlog.debug("eventresource", "In EventResourceAgentBean getting exception...");

        } catch (Exception e) {
            Rlog.fatal("eventresource", "EventResourceAgentBean:getStatusIdOfTheEvent-Exception In eventStatusId "
                    + e);
        }
        return evResDao;

	}


}// end of class
