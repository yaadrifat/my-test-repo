/*
 * Classname : CrfStatBean
 *
 * Version information: 1.0
 *
 * Date: 11/26/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.business.crfStat.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

//import com.velos.esch.service.util.DateUtil;
import com.velos.eres.service.util.DateUtil;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

// import java.sql.*;

/* End of Import Statements */
@Entity
@Table(name = "sch_crfstat")
public class CrfStatBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3257571719467578673L;

    /**
     * crfStat id
     */
    public int crfStatId;

    /**
     * crf id
     */
    public Integer crfStatCrfId;

    /**
     * codelist Crf stat
     */
    public Integer crfStatCodelstCrfStatId;

    /**
     * Enter By
     */

    public Integer crfStatEnterBy;

    /**
     * Review By
     */

    public Integer crfStatReviewBy;

    /**
     * Review On
     */

    public Date crfStatReviewOn;

    /**
     * Sent To
     */

    public String crfStatSentTo;

    /**
     * Sent Flag
     */

    public Integer crfStatSentFlag;

    /**
     * Sent By
     */

    public Integer crfStatSentBy;

    /**
     * Sent On
     */

    public Date crfStatSentOn;

    /*
     * creator
     */
    public Integer creator;

    /*
     * Modified By
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_CRFSTAT", allocationSize=1)
    @Column(name = "PK_CRFSTAT")
    public int getCrfStatId() {
        return this.crfStatId;
    }

    public void setCrfStatId(int crfStatId) {
        this.crfStatId = crfStatId;
    }

    @Column(name = "FK_CRF")
    public String getCrfStatCrfId() {
        return StringUtil.integerToString(this.crfStatCrfId);
    }

    public void setCrfStatCrfId(String crfStatCrfId) {
        if (crfStatCrfId != null && crfStatCrfId.trim().length() > 0) {
            this.crfStatCrfId = Integer.valueOf(crfStatCrfId);
        }
    }

    @Column(name = "FK_CODELST_CRFSTAT")
    public String getCrfStatCodelstCrfStatId() {
        return StringUtil.integerToString(this.crfStatCodelstCrfStatId);
    }

    public void setCrfStatCodelstCrfStatId(String crfStatCodelstCrfStatId) {
        if (crfStatCodelstCrfStatId != null
                && crfStatCodelstCrfStatId.trim().length() > 0) {
            this.crfStatCodelstCrfStatId = Integer
                    .valueOf(crfStatCodelstCrfStatId);
        }
    }

    /**
     * @return enter by
     */
    @Column(name = "CRFSTAT_ENTERBY")
    public String getCrfStatEnterBy() {
        return StringUtil.integerToString(this.crfStatEnterBy);
    }

    /**
     * @param enter
     *            by
     */
    public void setCrfStatEnterBy(String crfStatEnterBy) {
        if (crfStatEnterBy != null && crfStatEnterBy.trim().length() > 0) {
            this.crfStatEnterBy = Integer.valueOf(crfStatEnterBy);
        }
    }

    /**
     * @return review by
     */
    @Column(name = "CRFSTAT_REVIEWBY")
    public String getCrfStatReviewBy() {
        return StringUtil.integerToString(this.crfStatReviewBy);
    }

    /**
     * @param review
     *            by
     */
    public void setCrfStatReviewBy(String crfStatReviewBy) {
        if (crfStatReviewBy != null && crfStatReviewBy.trim().length() > 0) {
            this.crfStatReviewBy = Integer.valueOf(crfStatReviewBy);
        }
    }

    @Column(name = "CRFSTAT_REVIEWON")
    public Date getCrfStatReviewOnDate() {
        return (this.crfStatReviewOn);
    }

    public void setCrfStatReviewOnDate(Date crfStatReviewOn) {

        this.crfStatReviewOn = crfStatReviewOn;

    }

    @Transient
    public String getCrfStatReviewOn() {
        return DateUtil.dateToString(getCrfStatReviewOnDate());
    }

    public void setCrfStatReviewOn(String crfStatReviewOn) {
        setCrfStatReviewOnDate(DateUtil.stringToDate(crfStatReviewOn,null));

    }

    @Column(name = "CRFSTAT_SENTTO")
    public String getCrfStatSentTo() {
        return this.crfStatSentTo;
    }

    public void setCrfStatSentTo(String crfStatSentTo) {
        this.crfStatSentTo = crfStatSentTo;
    }

    @Column(name = "CRFSTAT_SENTFLAG")
    public String getCrfStatSentFlag() {
        return StringUtil.integerToString(this.crfStatSentFlag);
    }

    /**
     * @param sent
     *            flag
     */
    public void setCrfStatSentFlag(String crfStatSentFlag) {
        if (crfStatSentFlag != null && crfStatSentFlag.trim().length() > 0) {
            this.crfStatSentFlag = Integer.valueOf(crfStatSentFlag);
        }
    }

    /**
     * @return sent by
     */
    @Column(name = "CRFSTAT_SENTBY")
    public String getCrfStatSentBy() {
        return StringUtil.integerToString(this.crfStatSentBy);
    }

    /**
     * @param sent
     *            by
     */
    public void setCrfStatSentBy(String crfStatSentBy) {
        if (crfStatSentBy != null && crfStatSentBy.trim().length() > 0) {
            this.crfStatSentBy = Integer.valueOf(crfStatSentBy);
        }
    }

    @Column(name = "CRFSTAT_SENTON")
    public Date getCrfStatSentOnDate() {
        return (this.crfStatSentOn);
    }

    public void setCrfStatSentOnDate(Date crfStatSentOn) {
        this.crfStatSentOn = crfStatSentOn;

    }

    @Transient
    public String getCrfStatSentOn() {
        return DateUtil.dateToString(getCrfStatSentOnDate());
    }

    public void setCrfStatSentOn(String crfStatSentOn) {

        setCrfStatSentOnDate(DateUtil.stringToDate(crfStatSentOn, null));

    }

    /**
     * @return Creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param Creator
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return Modified By
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modified
     *            by
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this crf
     */

    /*
     * public CrfStatStateKeeper getCrfStatStateKeeper() { Rlog.debug("crfStat",
     * "CrfStatBean.getCrfStatStateKeeper"); return new
     * CrfStatStateKeeper(getCrfStatId(), getCrfStatCrfId(),
     * getCrfStatCodelstCrfStatId(), getCrfStatEnterBy(), getCrfStatReviewBy(),
     * getCrfStatReviewOn(), getCrfStatSentTo(), getCrfStatSentFlag(),
     * getCrfStatSentBy(), getCrfStatSentOn(), getCreator(), getModifiedBy(),
     * getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the crf
     *
     * @param edsk
     */

    /*
     * public void setCrfStatStateKeeper(CrfStatStateKeeper usk) { GenerateId
     * cfId = null;
     *
     * try { Connection conn = null; conn = getConnection();
     * Rlog.debug("crfStat", "CrfBean.setCrfStateKeeper() Connection :" + conn);
     * crfStatId = cfId.getId("SEQ_SCH_CRFSTAT", conn); conn.close();
     * setCrfStatCrfId(usk.getCrfStatCrfId());
     * setCrfStatCodelstCrfStatId(usk.getCrfStatCodelstCrfStatId());
     * setCrfStatEnterBy(usk.getCrfStatEnterBy());
     * setCrfStatReviewBy(usk.getCrfStatReviewBy());
     * setCrfStatReviewOn(usk.getCrfStatReviewOn());
     * setCrfStatSentTo(usk.getCrfStatSentTo());
     * setCrfStatSentFlag(usk.getCrfStatSentFlag());
     * setCrfStatSentBy(usk.getCrfStatSentBy());
     * setCrfStatSentOn(usk.getCrfStatSentOn()); setCreator(usk.getCreator());
     * setModifiedBy(usk.getModifiedBy()); setIpAdd(usk.getIpAdd());
     *
     * Rlog.debug("crfStat", "UserBean.setCrfStatStateKeeper() crfStatId :" +
     * crfStatId); } catch (Exception e) { Rlog.fatal("crfStat", "Error in
     * setCrfStatStateKeeper() in CrfStatBean " + e); } }
     */

    /**
     * updates the crfStat details
     *
     * @param edsk
     * @return 0 if successful; -2 for exception
     */
    public int updateCrfStat(CrfStatBean edsk) {
        try {

            setCrfStatCrfId(edsk.getCrfStatCrfId());
            setCrfStatCodelstCrfStatId(edsk.getCrfStatCodelstCrfStatId());
            setCrfStatEnterBy(edsk.getCrfStatEnterBy());
            setCrfStatReviewBy(edsk.getCrfStatReviewBy());
            setCrfStatReviewOn(edsk.getCrfStatReviewOn());
            setCrfStatSentTo(edsk.getCrfStatSentTo());
            setCrfStatSentFlag(edsk.getCrfStatSentFlag());
            setCrfStatSentBy(edsk.getCrfStatSentBy());
            setCrfStatSentOn(edsk.getCrfStatSentOn());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("crfStat", " error in CrfStatBean.updateCrfStat : " + e);
            return -2;
        }
    }

    public CrfStatBean(int crfStatId, String crfStatCrfId,
            String crfStatCodelstCrfStatId, String crfStatEnterBy,
            String crfStatReviewBy, String crfStatReviewOn,
            String crfStatSentTo, String crfStatSentFlag, String crfStatSentBy,
            String crfStatSentOn, String creator, String modifiedBy,
            String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setCrfStatId(crfStatId);
        setCrfStatCrfId(crfStatCrfId);
        setCrfStatCodelstCrfStatId(crfStatCodelstCrfStatId);
        setCrfStatEnterBy(crfStatEnterBy);
        setCrfStatReviewBy(crfStatReviewBy);
        setCrfStatReviewOn(crfStatReviewOn);
        setCrfStatSentTo(crfStatSentTo);
        setCrfStatSentFlag(crfStatSentFlag);
        setCrfStatSentBy(crfStatSentBy);
        setCrfStatSentOn(crfStatSentOn);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class
