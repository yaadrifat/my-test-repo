/*
 * Classname : EventuserBean
 * 
 * Version information: 1.0
 *
 * Date: 06/21/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.business.eventuser.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "SCH_EVENTUSR")
@NamedQuery(name = "findEventuserIds", query = "SELECT OBJECT(eventuser) FROM EventuserBean eventuser where trim(FK_EVENT)=:eventId and  trim(EVENTUSR_TYPE)=:eventusr_type")
public class EventuserBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3618700816186357304L;

    /**
     * eventuserId
     */
    public int eventuserId;

    /**
     * userId
     */
    public Integer userId;

    /**
     * eventuserType
     */
    public String userType;

    /**
     * eventId
     */
    public Integer eventId;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /*
     * event user duration
     */
    public String duration;

    /*
     * event user notes
     */

    public String notes;

    private ArrayList evNotes;

    private ArrayList evDuration;

    private ArrayList userIds;

    public EventuserBean() {

    }

    public EventuserBean(int eventuserId, String userId, String userType,
            String eventId, String creator, String modifiedBy, String ipAdd,
            String duration, String notes, ArrayList evNotes,
            ArrayList evDuration, ArrayList userIds) {
        super();
        // TODO Auto-generated constructor stub
        setEventuserId(eventuserId);
        setUserId(userId);
        setUserType(userType);
        setEventId(eventId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setDuration(duration);
        setNotes(notes);
        setEvNotes(evNotes);
        setEvDuration(evDuration);
        setUserIds(userIds);
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SCH_EVENTUSR_SEQ", allocationSize=1)
    @Column(name = "PK_EVENTUSR")
    public int getEventuserId() {
        return this.eventuserId;
    }

    public void setEventuserId(int eventuserId) {

        this.eventuserId = eventuserId;
    }

    @Column(name = "EVENTUSR")
    public String getUserId() {
        return StringUtil.integerToString(this.userId);

    }

    public void setUserId(String userId) {
        if (userId != null) {
            this.userId = Integer.valueOf(userId);
        }
    }

    @Column(name = "EVENTUSR_TYPE")
    public String getUserType() {
        return this.userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Column(name = "FK_EVENT")
    public String getEventId() {
        return StringUtil.integerToString(this.eventId);
    }

    public void setEventId(String eventId) {
        if (eventId != null) {
            this.eventId = Integer.valueOf(eventId);
        }
    }

    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Transient
    public ArrayList getUserIds() {
        return this.userIds;
    }

    public void setUserIds(ArrayList userIds) {
        this.userIds = userIds;
    }

    @Column(name = "eventusr_notes")
    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "EVENTUSR_DURATION")
    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
    
    @Transient
    public ArrayList getEvNotes() {
        return this.evNotes;
    }

    public void setEvNotes(ArrayList evNotes) {
        this.evNotes = evNotes;
    }

    @Transient
    public ArrayList getEvDuration() {
        return this.evDuration;
    }

    public void setEvDuration(ArrayList evDuration) {
        this.evDuration = evDuration;
    }

    // / END OF GETTER SETTER METHODS

    /**
     * returns the state holder object associated with this eventuser
     */

    /*
     * public EventuserStateKeeper getEventuserStateKeeper() {
     * Rlog.debug("eventuser", "EventuserBean.getEventuserStateKeeper");
     * 
     * EventuserStateKeeper eusk = new EventuserStateKeeper();
     * 
     * eusk.setEventuserId(getEventuserId()); eusk.setUserId(getUserId());
     * eusk.setUserType(getUserType()); eusk.setEventId(getEventId());
     * eusk.setCreator(getCreator()); eusk.setModifiedBy(getModifiedBy());
     * eusk.setIpAdd(getIpAdd()); eusk.setDuration(getDuration());
     * eusk.setNotes(getNotes()); // eusk.setEvDuration(getEvDuration()); //
     * eusk.setEvNotes(getEvNotes());
     * 
     * return eusk; // return new //
     * EventuserStateKeeper(getEventuserId(),getUserId(),getUserType(),getEventId(),getUserIds(),getCreator(),getModifiedBy(), //
     * getIpAdd(),getDuration(), getNotes() ); }
     */

    /**
     * sets up a state keeper containing details of the eventuser
     * 
     * @param eusk
     */

    /*
     * public void setEventuserStateKeeper(EventuserStateKeeper eusk) {
     * GenerateId Id = null;
     * 
     * try {
     * 
     * Connection conn = null; conn = getConnection(); Rlog.debug("eventuser",
     * "EventuserBean.setEventuserStateKeeper() Connection :" + conn);
     * eventuserId = Id.getId("SCH_EVENTUSR_SEQ", conn); conn.close();
     * 
     * setUserId(eusk.getUserId()); setUserType(eusk.getUserType());
     * setEventId(eusk.getEventId()); setCreator(eusk.getCreator());
     * setModifiedBy(eusk.getModifiedBy()); setIpAdd(eusk.getIpAdd());
     * setDuration(eusk.getDuration()); setNotes(eusk.getNotes());
     * 
     * Rlog.debug("eventuser", "EventuserBean.setEventuserStateKeeper()
     * eventuserId :" + eventuserId); } catch (Exception e) { System.out
     * .println("Error in setEventuserStateKeeper() in EventuserBean " + e); } }
     */

    /**
     * updates the eventuser details
     * 
     * @param eusk
     * @return 0 if successful; -2 for exception
     */
    public int updateEventuser(EventuserBean eusk) {
        try {

            setUserId(eusk.getUserId());
            setUserType(eusk.getUserType());
            setEventId(eusk.getEventId());
            setCreator(eusk.getCreator());
            setModifiedBy(eusk.getModifiedBy());
            setIpAdd(eusk.getIpAdd());
            setDuration(eusk.getDuration());
            setNotes(eusk.getNotes());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("eventuser",
                    " error in EventuserBean.updateEventuser : " + e);
            return -2;
        }
    }

}// end of class
