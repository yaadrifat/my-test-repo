/*
 * Classname : EventStatJB
 *
 * Version information: 1.0
 *
 * Date: 08/27/2008
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majumdar
 */

package com.velos.esch.web.eventstat;

/* IMPORT STATEMENTS */

import java.util.ArrayList;

//import com.velos.esch.business.common.EventStatDao;
import com.velos.esch.business.eventstat.impl.EventStatBean;
import com.velos.esch.service.eventstatAgent.EventStatAgentRObj;
import com.velos.esch.service.util.EJBUtil;

import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

public class EventStatJB {


	private int evtStatId; //PK_EVENTSTAT
	private String evtStatDate; //EVENTSTAT_DT
	private String  evtStatNotes; //EVENTSTAT_NOTES
	private String evt_fk; //FK_EVENT
	private String evtStat; //EVENTSTAT
	private String evtStatEndDate; //EVENTSTAT_ENDDT
    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;





    // GETTER SETTER METHODS

    // Start of Getter setter methods

    public int getEvtStatId() {
        return this.evtStatId;
    }

    public void setEvtStatId(int evtStatId) {
        this.evtStatId = evtStatId;
    }


    public String getEventStatDate() {
    	return this.evtStatDate;
    }


    public void setEventStatDate(String evtStatDate) {
    	this.evtStatDate = evtStatDate;

    }

    public String getEvtStatNotes() {
        return this.evtStatNotes;
    }


    public void setEvtStatNotes(String evtStatNotes) {
        this.evtStatNotes = evtStatNotes;
    }

    public String getFkEvtStat() {
    	return this.evt_fk;
    }

    public void setFkEvtStat(String evt_fk) {

    	this.evt_fk = evt_fk;
    }

    public String getEvtStatus() {
    	return this.evtStat;
    }

    public void setEvtStatus(String evtStat) {

    	this.evtStat = evtStat;
    }


    public String getEventStatEndDate() {
    	return this.evtStatEndDate;
    }

    /**
     * Sets Event Status End date
     *
     * @param evtStatEndDate
     *            Event Status End date
     */
    public void setEventStatEndDate(String evtStatEndDate) {
    	this.evtStatEndDate = evtStatEndDate;

    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }


    /**
     * Default Constructor
     *
     */
    public EventStatJB() {
        Rlog.debug("eventstat", "EventStatJB.EventStatJB() ");
    }

    public EventStatJB(int evtStatId, String evtStatDate, String evtStatNotes, String evt_fk, String evtStat,
    		String evtStatEndDate, String creator, String modifiedBy, String ipAdd)

    {
    	setEvtStatId(evtStatId);
        setEventStatDate(evtStatDate);
        setEvtStatNotes(evtStatNotes);
        setFkEvtStat(evt_fk);
        setEvtStatus(evtStat);
        setEventStatEndDate(evtStatEndDate);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getEventStatDetails() of EventStat Session Bean: EventStatAgentBean
     *
     *
     * @return The Details of the eventStat in the EventStat StateKeeper Object
     * @See EventStatBean
     */

    public EventStatBean getEventStatDetails() {
    	EventStatBean essk = null;

        try {

        	EventStatAgentRObj eventStatAgent = EJBUtil.getEventStatAgentHome();
        	essk = eventStatAgent.getEventStatDetails(this.evtStatId);
            Rlog.debug("eventstat","EventStatJB.getEventStatDetails()  " + essk);
        } catch (Exception e) {
            Rlog.debug("eventstat",
                    "Error in getEventStatDetails() in EventStatJB " + e);
        }
        if (essk != null) {


            this.evtStatId = essk.getEvtStatId();
            this.evtStatDate =	essk.getEventStatDate();
            this.evtStatNotes =	essk.getEvtStatNotes();
            this.evt_fk =	essk.getFkEvtStat();
            this.evtStat =	essk.getEvtStatus();
            this.evtStatEndDate =	essk.getEventStatEndDate();
            this.creator =	essk.getCreator();
            this.modifiedBy =	essk.getModifiedBy();
            this.ipAdd =	essk.getIpAdd();
        }
        return essk;
    }

    /**
     * Calls setEventStatDetails() of EventStat Session Bean: EventStatAgentBean
     *
  	 */
    public  void setEventStatDetails() {

            int output = 0;
            try {

            	EventStatAgentRObj eventStatAgent = EJBUtil.getEventStatAgentHome();
                output = eventStatAgent.setEventStatDetails(this.createEventStatStateKeeper());
                this.setEvtStatId(output);
                Rlog.debug("eventstat", "EventStatJB.setEventStatDetails()");

            } catch (Exception e) {
            	Rlog.debug("eventstat",
                    "Error in setEventStatDetails() in EventStatJB " + e);
            }

        }


    /**
     * Calls updateEventResource() of EventResource Session Bean: EventResourceAgentBean
     *
     * @return
     */
    public int updateEventStat() {
        int output;
        try {

        	EventStatAgentRObj eventStatAgent = EJBUtil.getEventStatAgentHome();
            output = eventStatAgent.updateEventStat(this.createEventStatStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventstat",
                    "EXCEPTION IN SETTING EVENT STATUS DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }


     /**
     *
     *
     * @return the EventStat StateKeeper Object with the current values of the Entity Bean EventStatBean
     *
     */

    public EventStatBean createEventStatStateKeeper() {
        Rlog.debug("eventstat", "EventStatJB.createEventStatStateKeeper() ");

        return new EventStatBean(evtStatId, evtStatDate, evtStatNotes, evt_fk, evtStat,
        		evtStatEndDate, creator, modifiedBy, ipAdd);
    }



}// end of class
