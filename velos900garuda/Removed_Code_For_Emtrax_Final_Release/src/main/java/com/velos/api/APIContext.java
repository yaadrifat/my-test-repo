package com.velos.api;

import com.velos.api.execption.*;

/*
 * 
 */
public class APIContext {
	

private boolean authenticate()
{
	//FIXME: Authentication Routine should go here
	//If authenticated successfully, return true
	return true;
}
	
private boolean checkRights()
{
	//FIXME: Check each method access rights here
	//If successful, return true
	return true;
}

private boolean validateAccess()
{
	if (authenticate() && checkRights()) return true;
return false;	
}

/** Validate and returns the requested context
 * @param contextName e.g. form
 * @return Appropriate conext based on parameter name <br>
 *  e.g. FormAPIConext Object for form
 */
public Object  getContext(String contextName) throws InvalidContextException 
{
	 if ( (contextName.toLowerCase()).equals("form") )
	 	return new FormAPIContext();
	 if ((contextName==null) || (contextName.length()==0)) 
	 throw new InvalidContextException("Invalid Context. Please Contact Velos customer support.");
	 
	 return null;
}

}
