package com.velos.api.execption;


import org.apache.log4j.Logger;
/**
 * Access exception 
 * 
 * @author Vishal Abrol
 */
public class NoAccessException extends Exception {
    private static Logger logger = Logger.getLogger(NoAccessException.class
            .getName());

    /**
     * Constructs an instance of <code>ApplicationException</code> with the
     * specified detail message.
     */
    public NoAccessException(String msg) {
        super(msg);
        logger.fatal(msg);
    }
}
