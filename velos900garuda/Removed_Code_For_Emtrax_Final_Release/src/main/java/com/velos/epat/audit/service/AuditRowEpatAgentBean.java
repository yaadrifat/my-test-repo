package com.velos.epat.audit.service;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;

import com.velos.epat.audit.business.AuditRowEpatBean;
import com.velos.epat.business.common.AuditRowEpatDao;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { AuditRowEpatAgent.class } )
public class AuditRowEpatAgentBean implements AuditRowEpatAgent {
    @PersistenceContext(unitName = "epat")
    protected EntityManager em;

	public AuditRowEpatBean getAuditRowEpatBean(Integer id) {
		if (id == null || id == 0) { return null; }
		AuditRowEpatBean AuditRowBean = null;
		try {
			AuditRowBean = (AuditRowEpatBean) em.find(AuditRowEpatBean.class, new Integer(id));
		} catch (Exception e) {
			Rlog.fatal("audit", "Exception in AuditRowEpatAgentBean.getAuditRowEpatBean: "+e);
		}
		return AuditRowBean;
	}

	public int getRID(int pkey, String tableName, String pkColumnName) {
		int output = 0;
		AuditRowEpatDao auditDao = new AuditRowEpatDao();
		output = auditDao.getRID(pkey, tableName, pkColumnName);
		return output;
	}
	
	public int findLatestRaid(int rid) {
		int output = 0;
		try{
			Query query = em.createNamedQuery("findLatestAuditRowEpatByRID");
			query.setParameter("rid", rid);
			
			ArrayList list = (ArrayList) query.getResultList();
			if (list == null || list.size() == 0) { return -1; }

			AuditRowEpatBean auditRow = (AuditRowEpatBean) list.get(0);
			output = (null == (Integer)auditRow.getId())? 0 : (Integer)auditRow.getId().intValue();
		} catch (Exception e){
			Rlog.fatal("audit", "Exception in AuditRowAgentEpatBean.findLatestRaid: "+e);
		}
		return output;
	}
	
	public int findLatestRaid(int rid, int userId) {
		int output = 0;
		try{
			Query query = em.createNamedQuery("findLatestAuditRowEpatByUserIdRID");
			query.setParameter("rid", rid);
			query.setParameter("userId", userId+"");
			query.setParameter("userIdLike", userId+",%");
			
			ArrayList list = (ArrayList) query.getResultList();
			if (list == null || list.size() == 0) { return -1; }

			AuditRowEpatBean auditRow = (AuditRowEpatBean) list.get(0);
			output = ((Integer)auditRow.getId()).intValue();
		} catch (Exception e){
			Rlog.fatal("audit", "Exception in AuditRowAgentEpatBean.findLatestRaid: "+e);
		}
		return output;
	}
	
	public int setReasonForChange(int raid, String remarks) {
		int output = 0;
		AuditRowEpatDao auditDao = new AuditRowEpatDao();
		output = auditDao.setReasonForChange(raid, remarks);
		return output;
	}
}
