/**
 * 
 */
package com.velos.webservices;

import java.util.List;

import javax.jws.WebService;
import javax.jws.WebResult;
import javax.jws.WebParam;

import com.velos.services.OperationException;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.model.StudyCalendar;

/**
 * @author dylan 
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
public interface StudyCalendarSEI {

	@WebResult(name = "Response")
	public abstract ResponseHolder createStudyCalendar(
			@WebParam(name = "StudyCalendar") 
			StudyCalendar study)
			throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarSummary(
			@WebParam(name="CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name="StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName, 
			@WebParam(name = "CalendarSummary") 
			CalendarSummary study)
			throws OperationException;

	
	@WebResult(name = "StudyCalendar")
	public abstract StudyCalendar getStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier, 
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier studyIdentifier, 
			@WebParam(name = "CalendarName")
			String calendarName) 
		throws OperationException;
	
	
	//Visit Methods

	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "Visit") 
			CalendarVisit studyCalendarVisit) 
	throws OperationException;
	

	@WebResult(name="Visit")
	public abstract CalendarVisit getStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name="VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name = "VisitName")
			String visitName) 
	throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder addVisitsToStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "Visits") 
			CalendarVisits newVisit) 
	throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder removeVisitFromStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name= "VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name = "VisitName")
			String visitName) 
	throws OperationException;
	
	
	//Event Methods

	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyCalendarEvent(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier studyCalendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name="EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName,
			@WebParam(name = "Event") 
			CalendarEvent studyCalendarEvent) 
	throws OperationException;
	
	@WebResult(name="Event")
	public abstract CalendarEvent getStudyCalendarEvent(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "VisitIdentifier")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "EventIdentifier")
			EventIdentifier eventIdentifier, 
			@WebParam(name = "EventName")
			String eventName) 
	throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder addEventsToStudyCalendarVisit(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "VisitIdentifer")
			VisitIdentifier visitIdentifier, 
			@WebParam(name = "Events") 
			CalendarEvents studyCalendarEvent) 
	throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder removeEventFromStudyCalendar(
			@WebParam(name = "CalendarIdentifier")
			CalendarIdentifier calendarIdentifier,
			@WebParam(name= "VisitIdentifier")
			VisitIdentifier visitIdentifier,
			@WebParam(name = "VisitName")
			String visitName,
			@WebParam(name = "EventIdentifier")
			EventIdentifier eventIdentifier,
			@WebParam(name = "EventName")
			String eventName) 
	throws OperationException;
}