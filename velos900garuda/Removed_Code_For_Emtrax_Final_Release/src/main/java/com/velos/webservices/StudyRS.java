/**
 * 
 */
package com.velos.webservices;

import java.security.Principal;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.StudyClient;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;

/**
 * @author dylan
 *
 */

public class StudyRS{
	private static Logger logger = Logger.getLogger(StudyRS.class.getName());
	
	@Resource MessageContext jaxrsContext;


	public StudyRS(){

	}
	
	public ResponseHolder create(Study study, boolean createNonSystemUsers)
			throws OperationException {
		
		
		return null;
//		ResponseHolder response = new ResponseHolder();
//		try{
//			login();
//			response = 
//				StudyClient.create(study, createNonSystemUsers);
//		
//		}
//		catch(OperationException e){
//			response.getIssues().addAll(e.getIssues());
//		}
//		catch(Throwable t){
//			response.addIssue(new Issue(t));
//		}
//		return response;
	}


	@Produces("application/json")
	@GET
	@Path("/study/{studyNumber}")
	/*public Study getStudyByStudyNumber(
			@Context HttpServletRequest request,
			@PathParam("studyNumber") String studyNumber)
			throws OperationException {*/

		/*try{
			Principal user = 
				jaxrsContext.getSecurityContext().getUserPrincipal();
			login(user);
			StudyIdentifier studyId = new StudyIdentifier(studyNumber);
			Study fetchedStudy = 
				StudyClient.getStudy(studyId);
			return fetchedStudy;	
		}
		catch(OperationException e){
			throw new WebApplicationException(e);
		}
		catch(Throwable t){
			logger.error("getStudyByStudyNumber", t);
			
		}
		return null;*/
	//} 


	public Study getStudy(String systemId) throws OperationException {

//		try{
//			login();
//			Study fetchedStudy = StudyClient.getStudy(systemId);
//			return fetchedStudy;	
//		}
//		catch(OperationException e){
//			throw new OperationException(e);
//		}
//		catch(Throwable t){
//			logger.error("getStudyByStudyNumber", t);
//		}
		return null;

	}


	/* (non-Javadoc)
	 * @see com.velos.webservices.StudySEI#getStudyByStudyNumber(java.lang.String)
	 */
	public Study getStudyByStudyNumber(String studyNumber)
			throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}
	private void login(Principal user)
	throws OperationException{
		LoginContext lctx;
		
		try{
			//invokes the standard jboss client login module...required
			//for all EJB client communication in JBOss
			lctx = new LoginContext("client-login", 
					new SimpleAuthCallbackHandler(
							user.getName()));
			lctx.login();
		}
		catch(LoginException e){
			throw new OperationException(e);
		}
	}

}
