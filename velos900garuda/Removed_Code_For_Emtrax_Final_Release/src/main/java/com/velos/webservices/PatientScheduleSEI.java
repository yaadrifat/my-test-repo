package com.velos.webservices;

import java.util.ArrayList;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Visit;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of Patient Schedule Web Services
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
	/**
	 * 
	 */
	public interface PatientScheduleSEI {
		/**
		 * public method of Patient Schedule Service Endpoint Interface
		 * which calls getPatientSchedule method of PatientSchedule webservice
		 * and returns Patient Schedule
		 * @param patientId
		 * @param studyId
		 * @return PatSchedule
		 * @throws OperationException
		 */
		 //Virendra:#6061
		@WebResult(name = "PatientSchedule" )
		public ArrayList<PatientSchedule> getPatientSchedules(
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId,
		@WebParam(name = "StudyIdentifier")		
		StudyIdentifier studyId
		)
		throws OperationException;   

}