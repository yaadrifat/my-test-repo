package com.velos.webservices;

import java.lang.management.ManagementFactory;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.StudyCalendarClient;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.StudyCalendar;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
 
@WebService (
		serviceName="StudyCalendarService", 
		targetNamespace = "http://velos.com/services/",
		endpointInterface = "com.velos.webservices.StudyCalendarSEI")
public class StudyCalendarWS implements StudyCalendarSEI
{

	private static Logger logger = Logger.getLogger(StudyCalendarWS.class.getName());
	@Resource
	private WebServiceContext context;
	
	public StudyCalendarWS()
	{
		  MBeanServer mbs = ManagementFactory.getPlatformMBeanServer(); 
	      ObjectName name;
		try {
			name = new ObjectName("com.velos:type=ServiceMonitor,name=StudyCalendar");
			ServiceMonitor mbean = new ServiceMonitor(); 
			if(mbs.isRegistered(name)) mbs.unregisterMBean(name);
			mbs.registerMBean(mbean, name); 
		} catch (Throwable t) {
			logger.error("Error creating MBean for ServiceMonitor", t);
		}
	}
	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#createStudyCalendar(com.velos.services.model.StudyCalendar)
	 */
	public ResponseHolder createStudyCalendar(StudyCalendar study)
			throws OperationException {
		
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.createStudyCalendar(study); 
		} catch (OperationException e) {
			logger.error("createStudyCalendar", e);
			throw e;
		} catch (Throwable t) {
			logger.error("createStudyCalendar", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#updateCalendarSummary(com.velos.services.model.CalendarIdentifier, com.velos.services.model.CalendarSummary)
	 */
	public ResponseHolder updateStudyCalendarSummary(
			CalendarIdentifier studyCalendarIdentifier, StudyIdentifier studyIdentifier, String calendarName, CalendarSummary calendarSummary)
			throws OperationException {
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.updateStudyCalendarSummary(studyCalendarIdentifier, studyIdentifier,calendarName, calendarSummary); 
		} catch (OperationException e) {
			logger.error("updateStudyCalendarSummary", e);
			throw e;
		} catch (Throwable t) {
			logger.error("updateStudyCalendarSummary", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#getStudyCalendar(com.velos.services.model.CalendarIdentifier)
	 */
	public StudyCalendar getStudyCalendar(
			CalendarIdentifier studyCalendarIdentifier, StudyIdentifier studyIdentifier, String calendarName)
			throws OperationException {
		try
		{
			StudyCalendar calendar = StudyCalendarClient.getStudyCalendar(studyCalendarIdentifier, studyIdentifier,calendarName); 
			return calendar; 
		} catch (OperationException e) {
				logger.error("getStudyCalendar", e);				
				throw e;
		} catch (Throwable t) {
				logger.error("getStudyCalendar", t); 
				throw new OperationException(t);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#updateStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, java.lang.String, com.velos.services.model.Visit)
	 */
	public ResponseHolder updateStudyCalendarVisit(
			CalendarIdentifier studyCalendarIdentifier, String visitName,
			CalendarVisit studyCalendarVisit) throws OperationException {
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.updateStudyCalendarVisit(studyCalendarIdentifier, visitName, studyCalendarVisit); 
		} catch (OperationException e) {
			logger.error("updateStudyCalendarVisit", e);
			throw e;
		} catch (Throwable t) {
			logger.error("updateStudyCalendarVisit", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#getStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String)
	 */
	public CalendarVisit getStudyCalendarVisit(CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName)
			throws OperationException {
		try
		{
			CalendarVisit visit = StudyCalendarClient.getStudyCalendarVisit(calendarIdentifier, visitIdentifier, visitName); 
			return visit; 
		} catch (OperationException e) {
			logger.error("getStudyCalendarVisit", e);				
			throw e;
		} catch (Throwable t) {
			logger.error("getStudyCalendarVisit", t); 
			throw new OperationException(t);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#addVisitsToStudyCalendar(com.velos.services.model.CalendarIdentifier, java.util.List)
	 */
	public ResponseHolder addVisitsToStudyCalendar(
			CalendarIdentifier calendarIdentifier, CalendarVisits newVisit)
			throws OperationException {
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.addVisitsToStudyCalendar(calendarIdentifier, newVisit); 
		} catch (OperationException e) {
			logger.error("addVisitsToStudyCalendar", e);
			throw e;
		} catch (Throwable t) {
			logger.error("addVisitsToStudyCalendar", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#removeVisitFromStudyCalendar(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String)
	 */
	public ResponseHolder removeVisitFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName)
			throws OperationException {
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.removeVisitFromStudyCalendar(calendarIdentifier, visitIdentifier, visitName); 
		} catch (OperationException e) {
			logger.error("removeVisitFromStudyCalendar", e);
			throw e;
		} catch (Throwable t) {
			logger.error("removeVisitFromStudyCalendar", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#updateStudyCalendarEvent(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String, com.velos.services.model.Event)
	 */
	public ResponseHolder updateStudyCalendarEvent(
			CalendarIdentifier studyCalendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName,
			CalendarEvent studyCalendarEvent) throws OperationException {
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.updateStudyCalendarEvent(studyCalendarIdentifier, visitIdentifier, visitName, eventIdentifier, eventName, studyCalendarEvent); 
		} catch (OperationException e) {
			logger.error("updateStudyCalendarEvent", e);
			throw e;
		} catch (Throwable t) {
			logger.error("updateStudyCalendarEvent", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#getStudyCalendarEvent(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String)
	 */
	public CalendarEvent getStudyCalendarEvent(CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName)
			throws OperationException {
		try
		{
			CalendarEvent event = StudyCalendarClient.getStudyCalendarEvent(calendarIdentifier, visitIdentifier, visitName, eventIdentifier, eventName); 
			return event;
		} catch (OperationException e) {
			logger.error("getStudyCalendarEvent", e);				
			throw e;
		} catch (Throwable t) {
			logger.error("getStudyCalendarEvent", t); 
			throw new OperationException(t);
	}
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#addEventsToStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, java.lang.String, com.velos.services.model.VisitIdentifier, java.util.List)
	 */
	public ResponseHolder addEventsToStudyCalendarVisit(
			CalendarIdentifier calendarIdentifier, String visitName,
			VisitIdentifier visitIdentifier, CalendarEvents studyCalendarEvent)
			throws OperationException {
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.addEventsToStudyCalendarVisit(calendarIdentifier, visitName, visitIdentifier, studyCalendarEvent); 
		} catch (OperationException e) {
			logger.error("addEventsToStudyCalendarVisit", e);
			throw e;
		} catch (Throwable t) {
			logger.error("addEventsToStudyCalendarVisit", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.CalendarSEI#removeEventFromStudyCalendar(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String)
	 */
	public ResponseHolder removeEventFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName)
			throws OperationException {
		ResponseHolder responseHolder = new ResponseHolder();
		try
		{
			responseHolder = StudyCalendarClient.removeEventFromStudyCalendar(calendarIdentifier, visitIdentifier, visitName, eventIdentifier, eventName); 
		} catch (OperationException e) {
			logger.error("removeEventFromStudyCalendar", e);
			throw e;
		} catch (Throwable t) {
			logger.error("removeEventFromStudyCalendar", t);
			responseHolder.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return responseHolder;
	}

}
