/**
 * 
 */
package com.velos.webservices;

import java.io.IOException;
import java.util.List;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.velos.eres.service.util.DateUtil;
import com.velos.services.OperationException;
import com.velos.services.monitoring.MessageLog;
import com.velos.services.monitoring.MonitorDAO;
import com.velos.services.monitoring.SessionService;
//import com.velos.services.study.StudyService;


/**
 * @author dylan
 *
 */
public class ServiceMonitor implements ServiceMonitorMBean{
	private static Logger logger = Logger.getLogger(ServiceMonitor.class.getName());
	public ServiceMonitor(){
		
	}
	
	public TabularDataSupport currentSuccessfulMessages(String fromDate, String toDate) throws IOException{
		try {
			//SessionService service = getMonitorService();
		
//			List<MessageLog> messages =
//				service.getMessages(
//						DateUtil.stringToDate(fromDate), 
//						DateUtil.stringToDate(toDate));
			
			List<MessageLog> messages =
				MonitorDAO.fetchMessageLogsByDateRange(
					DateUtil.stringToDate(fromDate), 
					DateUtil.stringToDate(toDate),
					StudyWS.class.getName());
			
			
			CompositeData[] compositeArray = new CompositeData[messages.size()];
			TabularDataSupport tabularSupport = null;
		
			try {
				TabularType tabularType = new TabularType(
						"com.velos.message.tabular", 
						"table of message", 
						MessageLog.getCompositeType(), 
						new String[]{MessageLog.FIELD_MESSAGE_ID});
				tabularSupport= new TabularDataSupport(tabularType);
				for (int x = 0; x < messages.size(); x++){
					try {
						tabularSupport.put(messages.get(x).toCompositeType());
					} catch (OpenDataException e) {
						logger.error(e);
					}
				}
			} catch (OpenDataException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
		
			return tabularSupport;
		} catch (OperationException e) {
			logger.fatal(e);
		}
		
		return null;
	}

	public Integer getErroredMessageCount(String fromDate, String toDate)  throws IOException{

		return 3;
	}
	
	private static SessionService getMonitorService() throws OperationException{
    
		SessionService monitorRemote = null;
		InitialContext ic;
		try{
			ic = new InitialContext();
			monitorRemote =
				(SessionService) ic.lookup(SessionService.class.getName());
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return monitorRemote;

     
	}


	/* (non-Javadoc)
	 * @see com.velos.webservices.ServiceMonitorMBean#getServiceState()
	 */
	public ServiceState getServiceState() throws IOException {
		return ServiceState.ACTIVE;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.ServiceMonitorMBean#getServiceType()
	 */
	public String getServiceType() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.velos.webservices.ServiceMonitorMBean#getVersion()
	 */
	public String getVersion() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
