package com.velos.webservices;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.PatientScheduleClient;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Visit;
/**
 * WebService class for PatientSchedule
 * @author Virendra
 *
 */
@WebService(
		serviceName="PatientScheduleService",
		endpointInterface="com.velos.webservices.PatientScheduleSEI",
		targetNamespace="http://velos.com/services/")
		
public class PatientScheduleWS implements PatientScheduleSEI{
	
	private static Logger logger = Logger.getLogger(PatientScheduleWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public PatientScheduleWS(){
	}
	/**
	 * Calls getPatientSchedule of Patient Schedule Client with PatientIdentifier
	 * and StudyIdentifier objects and returns List of PatientSchedule for a patient in a study
	 */
	 //Virendra:#6061
	/*public ArrayList<PatientSchedule> getPatientSchedules(PatientIdentifier patientId, StudyIdentifier studyIdentifier)
			throws OperationException {
		ArrayList<PatientSchedule> lstPatSchedule = PatientScheduleClient.getPatientSchedules(patientId, studyIdentifier);
		return lstPatSchedule;
	}*/

	@Override
	public ArrayList<PatientSchedule> getPatientSchedules(
			PatientIdentifier patientId, StudyIdentifier studyId)
			throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}