<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Bgt_Browser%><%--Budget Browser*****--%></title>

<script>

function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}
	if (orderBy==2) orderBy = "lower(budget_name)";
	
	 
	
	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	searchCriteria = formObj.searchCriteria1.value;
	
	// for milestone module
	studyId = formObj.studyId.value;
	paymentPk = formObj.paymentPk.value;
	calledFrom =formObj.calledFrom.value;
	pR = formObj.pR.value;	
	var includeDefaultCalendarBudgets= formObj.includeDefaultCalendarBudgets.value;
	//BK #5127 Fix 
	formObj.action="budgetbrowserpg.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&searchCriteria="+searchCriteria + "&calledFrom=" + calledFrom + "&studyId=" + studyId + "&paymentPk=" + paymentPk + "&pR=" + pR;
	formObj.submit(); 
}

function confirmBox(budgetName,pgRight) {
	 
 if (f_check_perm(pgRight,'E') == true) {
	 var paramArray = [decodeString(budgetName)];
	msg=getLocalizedMessageString("L_Delete_Bgt",paramArray);/*msg="Delete Budget '" + decodeString(budgetName) + "'?";*****/
	if (confirm(msg)) 
	{
    	return true;
	}
	else
	{
		return false;
	}

 } else {
	return false;
 }

}



</script>

<%
String src;
src= request.getParameter("srcmenu");
String includeDefaultCalendarBudgets= "";
String includeDCBudgetsCheckedText = "";


String calledFrom = "";
String browserHeading  = "";

calledFrom = request.getParameter("calledFrom");

String paymentPk = request.getParameter("paymentPk");
String pR = request.getParameter("pR");
String studyId = request.getParameter("studyId");
	

if (StringUtil.isEmpty(calledFrom))
{
	calledFrom = "budget";
	paymentPk = "";
	pR = "";
	studyId = "";
}

if (calledFrom.equals("study"))
{
	calledFrom = "budget";
	paymentPk = "";
	pR = "";
	studyId = request.getParameter("studyId");
}


if (calledFrom.equals("budget"))
{
	browserHeading  = LC.L_BgtHome_Open;/*browserHeading  = " Budget Home >> Open";*****/
	
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<% } else {

	browserHeading  = LC.L_SelA_Bgt;/*browserHeading  = "Select a Budget";*****/
}%>

<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id="grpB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="grpRight" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id ="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="bgtcalB" scope="page" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,com.velos.esch.business.common.BudgetcalDao"%>

<%
int budgetId=0;
String pagenum = "";
int curPage = 0;
long startPage = 1;
String stPage;
long cntr = 0;



pagenum = request.getParameter("page");

if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");
if (EJBUtil.isEmpty(orderBy))
   orderBy = "lower(budget_name)";


String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
	orderType = "asc";
}


includeDefaultCalendarBudgets = request.getParameter("includeDefaultCalendarBudgets");

if (includeDefaultCalendarBudgets== null)
{
	includeDefaultCalendarBudgets = "0";
}
 
if (includeDefaultCalendarBudgets.equals("1"))
{
	includeDCBudgetsCheckedText=" CHECKED ";
}


String searchCriteria = request.getParameter("searchCriteria");
if (searchCriteria==null) {searchCriteria="";}

String searchCriteriaForSql = StringUtil.escapeSpecialCharSQL(searchCriteria);

searchCriteria = StringUtil.htmlEncodeXss(searchCriteria);



HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
	//get budget module account right 
	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();
	CtrlDao acmod = new CtrlDao();
	acmod.getControlValues("module");
	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();
	int budSeq = acmodfeature.indexOf("MODBUD");
	budSeq = ((Integer) acmodftrSeq.get(budSeq)).intValue();
	char budAppRight = modRight.charAt(budSeq - 1);
	
	int accountId=0;  
	//get budget module group right 
	int pageRight = 0;
   	String acc = (String) tSession.getValue("accountId");
   	String uName = (String) tSession.getValue("userName");
	String userId = (String) tSession.getValue("userId");
   	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("BUDGT"));
   	accountId = EJBUtil.stringToNum(acc);
	userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
	String defaultGrp=userB.getUserGrpDefault();
	
	if (pageRight == 5)
	{
		pageRight=7;
	}
	if (pageRight == 6)
	{
		pageRight=4; //as budget can never have edit in group rights, 6 would only be for default admin groups if NEW right is revoked
	}
	
	//there should be only 7,5,4,0 for budget
	

	if (String.valueOf(budAppRight).compareTo("1") == 0 && pageRight >= 4 )	{
	

// RK - 07/14/04 < -- Added Budget type to the SQL

	   String budgetSql = "select a.PK_BUDGET, a.budget_name, a.budget_version , nvl((select codelst_desc from sch_codelst where codelst_type = 'budget_template' and pk_codelst = a.budget_template ),(select budget_name from sch_budget where pk_budget = a.budget_template )) as budget_template , b.STUDY_NUMBER ," 
	   	   		+ " b.STUDY_TITLE, c.SITE_NAME ,  budget_status,(select codelst_desc from sch_codelst where pk_codelst = fk_codelst_status) budget_status_desc, budget_type,budget_calendar, a.BUDGET_COMBFLAG, fk_study from sch_budget a, er_study b, er_site c " 
				+ " where ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
				+ " Where d.fk_user = " + userId+ " ))  OR " 
				+" (SELECT COUNT(*) FROM ER_GRPS WHERE pk_grp="+ defaultGrp  
				+" AND (grp_supbud_flag=1  AND (grp_supbud_rights IS NOT NULL OR to_number(grp_supbud_rights)>0)))>0 " 
				+"  )  AND a.fk_account="+ accountId +"	and " 
				+ "a.FK_SITE=c.PK_SITE(+) ";
				
				if (includeDefaultCalendarBudgets.equals("0"))
				{
					budgetSql  = budgetSql  +	" and  budget_calendar is null ";
				}	
				
				if (StringUtil.isEmpty(studyId))
				{
				budgetSql  = budgetSql  +	" and  a.FK_STUDY=b.PK_STUDY(+) ";
				}	
				else
				{
				budgetSql  = budgetSql  + 	" and  a.FK_STUDY  = " + studyId +  " and a.FK_STUDY  = b.PK_STUDY  ";
				}
				
				budgetSql  = budgetSql  + " and  nvl(a.budget_delflag,'Z') <> 'Y'  "
				+ " and (lower(b.study_number) like lower('%"+searchCriteriaForSql.trim()+"%') or lower(study_title) like lower('%"+searchCriteriaForSql.trim()+"%') or lower(b.study_keywrds) like lower('%"+searchCriteriaForSql.trim()+"%') or  lower(a.budget_name) like lower('%"+searchCriteriaForSql.trim()+"%')) ";
			
	String finalSql=	" select * from ("+budgetSql+" AND "
				+ " ( (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp  
				+ " AND settings_keyword = 'BUD_TAREA') LIKE '%'||TO_CHAR(b.fk_codelst_tarea)||'%') AND (fk_codelst_tarea IS NOT NULL)) "
				+ " OR	 (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=" + defaultGrp 
				+ " AND settings_keyword = 'BUD_DIVISION') LIKE '%'||TO_CHAR(b.study_division)||'%') AND (b.study_division IS NOT NULL)) "
				+ " OR "  
				+ " (1=(SELECT  Pkg_Util.f_compare('SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp 
				+ " AND settings_keyword = ''BUD_DISSITE''', b.study_disease_site,',',',','SQL','') FROM dual) AND (study_disease_site IS NOT NULL) ) "
				+ " OR (0=(SELECT COUNT(*) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp
				+" AND settings_keyword IN ( 'BUD_TAREA','BUD_DISSITE','BUD_DIVISION')) ) " 
				+ " OR ((a.pk_budget in (select d.fk_budget from sch_bgtusers d " 
				+ " Where d.fk_user = " + userId+ " ))))  )";
				
				
				/*+" UNION  " +  budgetSql  
				+ " AND (0=(SELECT COUNT(*) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum="+defaultGrp
				+" AND settings_keyword IN ( 'BUD_TAREA','BUD_DISSITE','BUD_DIVISION')) ))"; */
				
				
				//+ " order by lower(a.budget_name) ";
// RK - 07/14/04 >
	//System.out.println("FiablSQL"+finalSql);
	
	   String countSql =  "select count(*) from (" + finalSql+")"; 
			

	   long rowsPerPage=0;
   	   long totalPages=0;	
	   long rowsReturned = 0;
	   long totalRows = 0;	   
	   long showPages = 0;
	   long firstRec = 0;
	   long lastRec = 0;
	   
	   String budgetName = null;
	   String studyNumber = null;
	   String budgetVersion = null;
	   String studyTitle = null;
	   String budgetTemplate = "";	   
	   String budgetType = "";	   
	   String budgetStatus = null;
	   String budgetSite = null;
	  String defualtBudgetCalendar="";
	  
	  String budgetCombFlag = null;
	  String budgetStudyId =null;
	  
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   
	   
	   
		 rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
		totalPages =Configuration.PAGEPERBROWSER ;
  
       BrowserRows br = new BrowserRows();
	     
	   br.getPageRows(curPage,rowsPerPage,finalSql,totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   totalRows = br.getTotalRows();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();	  
	   
	  
	   
%>
  
<Form name="budgetbrowserpg" method=post onsubmit="" action="budgetbrowserpg.jsp?srcmenu=<%=src%>&page=1">

<%
if (calledFrom.equals("budget"))
{ %>
	<div class="BrowserTopN" id="div1">

<!--	<P class="sectionHeadings"> <%=browserHeading%></P>  -->

	
	<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr height="20">
		<td colspan="5" > <b> <%=LC.L_Search_By%><%--Search By*****--%></b> 
			<!-- <img src="../images/jpg/help.jpg" onmouseover="return overlib('Enter a study number or keywords in the study or budget name.',CAPTION,'Search Help');" onmouseout="return nd();"> -->
		</td>
	</tr>
	<tr>
		<!--SM: Modified Virendra's fix to occupy the links. Also reduced the checkbox text-->
		<td width="30%" > <%=MC.M_BgtName_StdNum%><%--Budget Name/<%=LC.Std_Study%> Number--%>: 
			<Input type=text name="searchCriteria"/> &nbsp;
		</td>
		<td width="15%" >
			<input name="includeDefaultCalendarBudgets" type="checkbox"  value="1" <%=includeDCBudgetsCheckedText%> title="<%=MC.M_InclDef_CalBgt%><%--Include Default Calendar Budgets--%>"><label title="<%=MC.M_InclDef_CalBgt%><%--Include Default Calendar Budgets--%>">&nbsp;<%=LC.L_Default_CalBgts%><%--Default Calendar Budgets*****--%></label></input>&nbsp;
		</td>
	    <td width="7%" ><button type="submit"><%=LC.L_Search%></button></td>
		<td width="8%" align="center" >
			<b><A href="budgetbrowserpg.jsp?&srcmenu=<%=src%>"><%=LC.L_View_AllBgts_Upper%><%--VIEW ALL BUDGETS*****--%></A></b>
		</td>
		<td width="8%" align="center" > 
			<b><A href="budget.jsp?mode=N&srcmenu=<%=src%>&fromPage=budgetbrowser" onClick="return f_check_perm(<%=pageRight%>,'N')"> <%=MC.M_Create_ANewBgt_Upper%><%--CREATE A NEW BUDGET*****--%></A></b>
		</td>
	</tr>

	</table>
</div>	   	
<% } else { 
%>
<!--	<P class="sectionHeadings"> <%=browserHeading%></P>  -->
<%

}%>

<%
if (calledFrom.equals("budget"))
{ %>
<div class="BrowserBotN BrowserBotN_BGT_1" id="div1">
<% } else { %> <div> <%}%>

	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	<input type=hidden name=frompage value="budgetbrowser">
   <input type=hidden name="searchCriteria1" value="<%=searchCriteria%>">
   
   <!--for milestones module -->
   
   <Input type="hidden" name="calledFrom" value="<%=calledFrom%>">
   <Input type="hidden" name="studyId" value="<%=studyId%>">
   <Input type="hidden" name="paymentPk" value="<%=paymentPk%>">
   <Input type="hidden" name="pR" value="<%=pR%>">
   
	<input type=hidden name=mode value="M">	
    <table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<% if (!(searchCriteria.equals(""))) {Object[] arguments = {searchCriteria};%> 
    	<tr> 
		    <td colspan="6"> 
		        <P class="defComments"><%=VelosResourceBundle.getMessageString("M_BgtMatch_SrchCrit",arguments)%><%--The following budgets match your search criteria*****: "<%=searchCriteria%>"--%></P>
		    </td>
	    </tr>
	<%}%>
      <tr> 
        <th width="15%" onClick="setOrder(document.budgetbrowserpg,2)"><%=LC.L_Budget_Name%><%--Budget Name*****--%> &loz;</th>
        <th width="9%" onClick="setOrder(document.budgetbrowserpg,3)"> <%=LC.L_Version%><%--Version*****--%> &loz;</th>
        <th width="12%" onClick="setOrder(document.budgetbrowserpg,4)"> <%=LC.L_Template%><%--Template*****--%> &loz;</th>
        <th width="15%" onClick="setOrder(document.budgetbrowserpg,5)"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%> &loz;</th>
        <th width="20%" onClick="setOrder(document.budgetbrowserpg,6)"> <%=LC.L_Title%><%--Title*****--%> &loz;</th>
        <th width="15%" onClick="setOrder(document.budgetbrowserpg,7)"> <%=LC.L_Organization%><%--Organization*****--%> &loz; </th>
        <th width="10%" onClick="setOrder(document.budgetbrowserpg,8)"> <%=LC.L_Status%><%--Status*****--%> &loz;</th>
        <%if(calledFrom.equals("budget")){%>
        	<th width="7%" onClick="setOrder(document.budgetbrowserpg,8)"> <%=LC.L_Delete%><%--Delete*****--%></th>		
        <%}else{%>
        	<th width="7%" onClick="setOrder(document.budgetbrowserpg,8)"> <%=LC.L_Select%><%--Select*****--%></th>
       <%}%>
      </tr>
	  
  
	<!--KM-28Aug08 -->
	<script language=javascript>
	   var varViewTitle =  new Array (<%=rowsReturned%>);
	</script>
	  
	  
	  <%
    for(int counter = 1;counter<=rowsReturned;counter++)
  	{		
		  
		budgetId=EJBUtil.stringToNum(br.getBValues(counter,"PK_BUDGET")) ;
		
		//KM-#2005
		grpB.setGroupId(EJBUtil.stringToNum(defaultGrp));
		grpB.getGroupDetails();


		String rightStr = budgetUsersB.getBudgetUserRight(budgetId,EJBUtil.stringToNum(userId));
		int rightLen = rightStr.length();

		if (rightLen==0)
		{
		rightStr=grpB.getGroupSupBudRights();
		rightLen=rightStr.length();
		}

		ctrl.getControlValues("bgt_rights");

		ArrayList feature =  ctrl.getCValue();
		ArrayList ftrDesc = ctrl.getCDesc();
		ArrayList ftrSeq = ctrl.getCSeq();
		ArrayList ftrRight = new ArrayList();
		String strR;

		for (int count = 0; count <= (rightLen - 1);count ++)
		{
			strR = String.valueOf(rightStr.charAt(count));
			ftrRight.add(strR);
		}

		grpRight.setGrSeq(ftrSeq);
    	grpRight.setFtrRights(ftrRight);
		grpRight.setGrValue(feature);
		grpRight.setGrDesc(ftrDesc);


		pageRight  = Integer.parseInt(grpRight.getFtrRightsByValue("BGTDET"));


		budgetName=br.getBValues(counter,"budget_name");
		
		defualtBudgetCalendar = br.getBValues(counter,"budget_calendar");
		
		if (StringUtil.isEmpty(defualtBudgetCalendar))
		{
			defualtBudgetCalendar="";
		}
		
		budgetVersion=((br.getBValues(counter,"budget_version")==null)?"-":br.getBValues(counter,"budget_version"));

		budgetTemplate=br.getBValues(counter,"budget_template");
		
		budgetTemplate=(budgetTemplate==null)?"":budgetTemplate;
		
		
// RK - 07/14/04 <
		budgetType=br.getBValues(counter,"budget_type");
		
// RK - 07/14/04 >
		
		studyNumber=((br.getBValues(counter,"study_number")==null)?"-":br.getBValues(counter,"study_number"));
		
		studyTitle=((br.getBValues(counter,"study_title")==null)?"-":br.getBValues(counter,"study_title"));
		
	  		/* KM-
			if (studyTitle.length() > 80)
			{
				studyTitle = studyTitle.substring(0,80) + " ...";
			}*/
		
		budgetSite=((br.getBValues(counter,"site_name")==null)?"-":br.getBValues(counter,"site_name"));
		
		budgetStatus=br.getBValues(counter,"budget_status_desc");

		budgetCombFlag=br.getBValues(counter,"BUDGET_COMBFLAG");
		
		budgetStudyId=br.getBValues(counter,"fk_study");
	
		if ((counter%2)!=0) {
  %>
      <tr class="browserEvenRow"> 
        <%
		}
		else{ 
  %>
      <tr class="browserOddRow"> 
        <%
		}
  %>

        <td>
		<%
// RK - 07/14/04 < Changed check based on budgetType instead of budgetTemplate


	// budgetType null check --anu 
	if(budgetType==null)
	budgetType ="";

	if (calledFrom.equals("budget"))
	{
			
			if (budgetType.equals("P") || budgetType.equals("C")) {
				if (StringUtil.isEmpty(defualtBudgetCalendar))
				{
					if (StringUtil.isEmpty(budgetCombFlag))
					{%>
						<A href="patientbudget.jsp?mode=M&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetType%>&srcmenu=<%=src%>&selectedTab=2"> <%=budgetName%> </A> 
					<%}else{%>
						<%if (budgetCombFlag.equals("Y")){%>
							<A href="combinedBudget.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=11&studyId=<%=budgetStudyId%>"> <%=budgetName%> </A>
						<%}else{%>
							<%=budgetName%> 
						<%} %> 
					<%}%>
				<%} else {%>
					<!--Default Calendar Budget-->
					<%
						BudgetcalDao bgtcaldao = new BudgetcalDao();
						bgtcaldao.getAllBgtCals(budgetId, 0);
						ArrayList bdgProtIds=bgtcaldao.getBgtcalProtIds();	
						ArrayList bdgProtTypes=bgtcaldao.getBgtcalProtTypes();
						
						String bgtProtType = "";
						if (bdgProtTypes.size() > 0){
							bgtProtType = String.valueOf(bdgProtTypes.get(1));
							if (bgtProtType.equals("L")) 
								bgtProtType ="P";
						}
						
						bgtcaldao.resetObject();
						
						if (1 <= bdgProtIds.size()){%>
							<A href="protLinkedBudget.jsp?selectedTab=4&mode=M&calledFrom=<%=bgtProtType %>&studyId=<%=budgetStudyId%>&protocolId=<%=bdgProtIds.get(1)%>&srcmenu=tdmenubaritem4&pageNo=1&displayType=V&calassoc=P"> <%=budgetName%> </A>
						<%}else{%>
							<%=budgetName%> 
						<%} %>
				<%}
			} else {%>
				<A href="studybudget.jsp?mode=M&budgetId=<%=budgetId%>&srcmenu=<%=src%>&selectedTab=2"> <%=budgetName%> </A>		
			<%}
		
	} // if called from Budget
	else
	{
		%>
			<%=budgetName%>
		<%
	
	}	
// RK - 07/14/04 >
		%>
		</td>
				
        <td> <%=budgetVersion%> </td>
        <td> <%=budgetTemplate%> </td>
        <td> <%=studyNumber%> </td>

		<%
		//Modified by Manimaran to display full title in mouseover.
		String viewStudyTitle = StringUtil.escapeSpecialCharJS(studyTitle); 
		%>
		
		<script language=javascript>
		   varViewTitle[<%=counter%>] = htmlEncode('<%=viewStudyTitle%>');
		</script>
        <td>
		<%if(viewStudyTitle.length()>50){%>
		<%=viewStudyTitle.substring(0,50)%>&nbsp;<IMG class="asIsImage" src ='./images/More.png' border=0 onMouseOver="return overlib('<%=viewStudyTitle%>',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.L_Study_Title%> Study Title*****--%>');" onMouseOut="return nd();"></a>
		<!--<a href="#" onmouseover="return overlib(varViewTitle[<%//=counter%>],CAPTION,'<%//=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><%//=LC.L_View_Title%><%--View Title*****--%></a>   </td>-->
        <%} else {%>
		&nbsp;&nbsp;<%=viewStudyTitle%>
		<%}%></td>

		
		<td> <%=budgetSite%> </td>
        <td> <%=budgetStatus%> </td>
        <td  align="center"> 
			<%
			if (calledFrom.equals("budget"))
				{
			%>
        		<A href="budgetdelete.jsp?budgetId=<%=budgetId%>&srcmenu=<%=src%>" onClick="return confirmBox('<%=StringUtil.encodeString(budgetName)%>',<%=pageRight%>);"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A>
        	<% 
        		} 
        		else
        		{ // for milestones module
        		%>
        		
        			<A href="budPaymentDetails.jsp?budgetId=<%=budgetId%>&studyId=<%=budgetStudyId%>&paymentPk=<%=paymentPk%>&pR=<%=pR%>" ><%=LC.L_Select%><%--Select*****--%></A>
        		<%
        		
        		}
        	%>	
        	
        </td>				
      </tr>
      <%

		}
		
%>
    </table>
    
	<!-- Bug#9751 16-May-2012 Ankit -->
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
	
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>	
	<%}%>	
	</td>
	</tr>
	</table>
	
	<div align="center" class="midalign">	
	<%  if (curPage==1) startPage=1;
	    for (int count = 1; count <= showPages;count++)
		{
	   	cntr = (startPage - 1) + count;
		 
		if ((count == 1) && (hasPrevious))
		{   
	    %>
		
	  	<A href="budgetbrowserpg.jsp?includeDefaultCalendarBudgets=<%=includeDefaultCalendarBudgets%>&searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&calledFrom=<%=calledFrom%>&studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pR%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
		
		<%
	  	}	
		%>
	
		<%
	 
		 if (curPage  == cntr)
		 {
	     %>	   
			<FONT class = "pageNumber"><%= cntr %></Font>
	       <%
	       }
	      else
	        {
	       %>		
			
		   <A href="budgetbrowserpg.jsp?includeDefaultCalendarBudgets=<%=includeDefaultCalendarBudgets%>&searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&calledFrom=<%=calledFrom%>&studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pR%>"><%= cntr%></A>
	       <%
	    	}	
		 %>
		<%
		  }
	
		if (hasMore)
		{   
	   %>
	  
	  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="budgetbrowserpg.jsp?includeDefaultCalendarBudgets=<%=includeDefaultCalendarBudgets%>&searchCriteria=<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&calledFrom=<%=calledFrom%>&studyId=<%=studyId%>&paymentPk=<%=paymentPk%>&pR=<%=pR%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
		<%
	  	}	
	%>
  </div>
  
  </Form>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {

	if (calledFrom.equals("budget"))
	{
%>
	<jsp:include page="timeout.html" flush="true"/>
<%
	}
	else
	{
		%>
		<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
		<%
			
	}
}
%>


<%
if (calledFrom.equals("budget"))
{ %>

	<div> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
<% } %>

</div>

<%
if (calledFrom.equals("budget"))
{ %>
<DIV class="mainMenu" id = "emenu"> 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</DIV>
<% } %>

</body>
</html>

