<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>
<title><%=LC.L_MngPat_Pcol%><%--Manage <%=LC.Pat_Patient%> >> Protocols*****--%></title>

<SCRIPT Language="javascript">

 function back(pk,patId) {

	window.opener.document.reports.id.value = pk;
	window.opener.document.reports.selPatient.value = patId;
	window.opener.document.reports.val.value = patId;
	self.close();
}

	function  validate(formobj){
		if (!(isInteger(formobj.filterLastVisit.value))){
	 		alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid number");*****/
			formobj.filterLastVisit.focus();
            return false;
		}
	    }

function patstudy(formobj){

studyId=formobj.selstudyId.value;

/*for(i=0;i<studIds.length;i++)
{
if(studIds[i] == studyId){
  if(confirm("This patient has been associated to this <%=LC.Std_Study_Lower%> before. Do you want to create a new link to the same <%=LC.Std_Study_Lower%>?")==false)
    return false;
 else
   break;
}
}*/


if (studyId.length==0){
alert("<%=MC.M_PlsSel_StdFst%>");/*alert("Please Select a <%=LC.Std_Study_Lower%> First.");*****/
return false;
}
pkey=formobj.pkey.value;
patientCode=formobj.patientCode.value;
selectedTab=formobj.selectedTab.value;

enrollingOrg = formobj.enrollingSite.value ;

//formobj.action="enrollpatient.jsp?mode=N&srcmenu=tdmenubaritem5&page=patientEnroll&studyId="+studyId+"&pkey="+pkey+"&patientCode="+patientCode+"&patProtId="+patProtId+"&selectedTab=2";
window.location="enrollpatient.jsp?mode=N&srcmenu=tdmenubaritem5&page=patientEnroll&studyId="+studyId+"&pkey="+pkey+"&patientCode="+patientCode+"&patProtId=&selectedTab=2&patStudiesEnrollingOrg=" +enrollingOrg;
return false;

}

function openWinStatus(patId,patStatPk,changeStatusMode)
{
	windowName = window.open("patstudystatus.jsp?changeStatusMode="+changeStatusMode+"&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=500");
	windowName.focus();

}

</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="studyConB" scope="request" class="com.velos.eres.web.study.StudyJB"/>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB,com.velos.eres.web.patFacility.PatFacilityJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>


<% String src;

src= request.getParameter("srcmenu");

//openMode=request.getParameter("openMode"); //open this window as popup or full screen

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<body>



<%

HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))

{

	int pageRight = 0;
	int orgRight = 0;
	String pkey = "";
 	pkey = request.getParameter("pkey") ;
	String userId = (String) tSession.getValue("userId");


	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

	orgRight = usrSite.getUserPatientFacilityRight(EJBUtil.stringToNum(userId), EJBUtil.stringToNum(pkey));

	if ( pageRight >= 4 && orgRight >= 4)

	{

%>



<%

	CodeDao cdDisplay = new CodeDao();


	String dStatus = "";

	StringBuffer sb = new StringBuffer();

	String patorg = "";



	String pstat =  "";

	String ddStudy = "";

	String filEnrDt = "ALL";
	String filVisit = null ;
 	String filDoneOn = "ALL";
	String filNext = "ALL";

	int p;

	Integer iStat;

	String pOrganization = "";
    String selectedTab="" ;

    selectedTab=request.getParameter("selectedTab") ;

	pstat = request.getParameter("patstatus") ;

 	userB = (UserJB)tSession.getValue("currentUser");
	String userIdFromSession = (String) tSession.getAttribute("userId");
	//uOrganization = userB.getUserSiteId();

	//if (uOrganization  == null) uOrganization  = "";



	String patId="";
	String uName = (String) tSession.getValue("userName");

	String accId = (String) tSession.getValue("accountId");
	String page1 = request.getParameter("page");
	String pstudy = "0";
	String patientCode = request.getParameter("patientCode") ;
	String  encodedStudyNum	=  null;

	String linkTo=request.getParameter("linkto");
	linkTo=(linkTo==null)?"":linkTo;
	String studyId = request.getParameter("studyId") ;
		if (studyId == null)
			studyId = "0";
		int personPK = 0;
		 personPK = EJBUtil.stringToNum(request.getParameter("pkey"));


	     person.setPersonPKId(personPK);
		 person.getPersonDetails();
		 patId = person.getPersonPId();

		pOrganization = person.getPersonLocation();

		/////////
			String ddEnrollingSite = "";
			ArrayList  patSitePks = new ArrayList ();
			ArrayList  patSitenNames = new ArrayList ();

			PatFacilityDao patFacilityDao = new PatFacilityDao();

			PatFacilityJB patFacility = new PatFacilityJB();
			patFacilityDao = patFacility.getPatientFacilitiesSiteIds(personPK);

			if (patFacilityDao == null)
			{
				patFacilityDao = new PatFacilityDao();
			}

			patSitePks = patFacilityDao.getPatientSite();
			patSitenNames = patFacilityDao.getPatientSiteName();

			if (patSitePks == null)
			{
				patSitePks = new ArrayList();
				patSitenNames = new ArrayList();
			}


			 ddEnrollingSite = EJBUtil.createPullDownWithStrNoSelect("enrollingSite",  pOrganization, patSitePks, patSitenNames);


		////////

		ArrayList arStudyId = new ArrayList();
		ArrayList arStudyNum = new ArrayList();
		ArrayList tempStudyId=new ArrayList();
		ArrayList tempStudyNum=new ArrayList();
		//StudyDao sd = studyB.getUserStudies(userIdFromSession, "dummy",0,"active");
		// Fix for bug # 1936

		//StudyDao sd = studyB.getUserStudiesWithRights(userIdFromSession, "dummy",0,pOrganization);

		//Modified by Sonia Abrol, to fix 2385, now the pull down will get the same studies as manage patients>enrolled
		//(minus studies to which patient is already enrolled (handled below)
		StudyDao sd = studyB.getUserStudies(userIdFromSession, "dStudy",0,"active");

		arStudyId = sd.getStudyIds();
		arStudyNum = sd.getStudyNumbers();

		String strStudyId = "";

		//strStudyId = EJBUtil.createPullDown("selstudyId", 0, arStudyId , arStudyNum )	;
		PatStudyStatDao pdao = new PatStudyStatDao();

	   	pdao = studyB.getPatientStudiesWithVisits(0, EJBUtil.stringToNum(pkey), EJBUtil.stringToNum(pstat) ,EJBUtil.stringToNum(userId), EJBUtil.stringToNum(pOrganization),filEnrDt,filDoneOn,filNext,filVisit );


		ArrayList patientIds = pdao.getPatientIds();
		ArrayList patientEnrollDates = pdao.getPatientEnrollDates();
		ArrayList patientCodes = pdao.getPatientCodes();
		ArrayList patientProtIds = pdao.getPatientProtIds();
		ArrayList studyNums = pdao.getStudyNums();
		ArrayList   studIds = pdao.getStudyIds();
		ArrayList   statDescs = pdao.getPatientStatusDescs();
		ArrayList   patientStatusPks = pdao.getPatientStatusPk();
		ArrayList curVisits = pdao.getCurVisits();
		ArrayList curVisitsName=pdao.getCurVisitsName();
		ArrayList curVisitDates = pdao.getCurVisitDates();
		ArrayList nextVisits  = pdao.getNextVisits();

		ArrayList studyTitles  = pdao.getStudyTitles();

		System.out.println("arStudyId.size()" + arStudyId.size());

		//loop to remove the studies patient is already enrolled in
	    for (int count=0;count<arStudyId.size();count++){
		 if (studIds.contains(arStudyId.get(count))) continue;
		 tempStudyId.add(arStudyId.get(count));
		 tempStudyNum.add(arStudyNum.get(count));
		}
		//create the study dropdown

		strStudyId = EJBUtil.createPullDown("selstudyId", 0, tempStudyId , tempStudyNum );
	//	strStudyId = EJBUtil.createPullDown("selstudyId", 0, arStudyId , arStudyNum );
if ((linkTo!=null) && (studIds.size()>0)){
int pageRightTemp = 0;


	//******************GET STUDY TEAM RIGHTS and put the same in session, this will be a separate obj as no study is selected //***************************************************************
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(((Integer)studIds.get(0)).intValue(),EJBUtil.stringToNum(userId));
    ArrayList tId = teamDao.getTeamIds();
    int patdetright = 0;
    if (tId.size() == 0)
	{
    	pageRightTemp=0 ;
    }
	else
	{
    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

    		ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();

    	tSession.setAttribute("studyRights",stdRights);
    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRightTemp= 0;
    		patdetright = 0;
    	}
		else
		{
    		pageRightTemp = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    		patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));
    		tSession.setAttribute("studyManagePat",new Integer(pageRightTemp));
		tSession.setAttribute("studyViewPatDet",new Integer(patdetright));
    	}
    }

		}
if ((linkTo.equals("adverse")) && (studIds.size()==1))
		{



%>

<META HTTP-EQUIV=refresh   CONTENT="0;URL=adveventbrowser.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&pkey=<%=pkey%>&patProtId=<%=patientProtIds.get(0)%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&studyId=<%=studIds.get(0)%>&statDesc=<%=statDescs.get(0)%>&statid=<%=patientStatusPks.get(0)%>&studyVer=null">

<%} else if ((linkTo.equals("schedule")) && (studIds.size()==1)){
%>
<META HTTP-EQUIV=refresh   CONTENT="0;URL=patientschedule.jsp?srcmenu=<%=src%>&selectedTab=3&mode=M&pkey=<%=pkey%>&patProtId=<%=patientProtIds.get(0)%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&studyId=<%=studIds.get(0)%>&statDesc=<%=statDescs.get(0)%>&statid=<%=patientStatusPks.get(0)%>&studyVer=null&generate=N">
<%} else if ((linkTo.equals("forms")) && (studIds.size()==1)){
%>
<META HTTP-EQUIV=refresh   CONTENT="0;URL=formfilledstdpatbrowser.jsp?srcmenu=<%=src%>&selectedTab=8&mode=M&pkey=<%=pkey%>&patProtId=<%=patientProtIds.get(0)%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&studyId=<%=studIds.get(0)%>&statDesc=<%=statDescs.get(0)%>&statid=<%=patientStatusPks.get(0)%>&studyVer=null&calledFrom=S">
<%}

else {%>




<DIV>

 <Form name="results" id="patStdFrm" method="post" onsubmit="if (patstudy(document.results)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<%
	 %>

<DIV class="BrowserTopn" id="div1">

	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
	<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patId)%>"/>
	<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	<jsp:param name="page" value="<%=page1%>"/>
	</jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_top1" id="div2">


     <%
	   int i = 0;

	   String patientId = null;
	   String patientEnrollDate = null;
	   String patientLname = null;
	   String patientFName = null;
	   String patientProtId = null;
	   String studyNum = null;
	   String studyTitle = null;

	   String studyContact = "";
	   String userLname = "";
	   String userFName = "";

	   int studId = 0;

	   String statDesc = null;

	   int tempStudy = 0;
	   int patStatPk = 0;

	   String curVisit = null;
	   String curVisitName="";
   	   String curVisitDate = null;
	   String nextVisit  = null;

  	   int length = pdao.getCRows();
	   if (arStudyId.size()>0){
	   %>
	     <br>
   <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign outline">
   <tr height="30">
   <td width="50%" ><%=MC.M_EnrlThisPat_SelStdPatOrg%><%--To screen/enroll this <%=LC.Pat_Patient_Lower%> in a new <%=LC.Std_Study_Lower%>, select <%=LC.Std_Study%> and <%=LC.Pat_Patient%> Organization*****--%>:</td>
   <td width="40%" ><%=strStudyId%>&nbsp;&nbsp;<%=ddEnrollingSite%></td>
   <td width="5%">
	<jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="patStdFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	</td>
	</tr>

  </table>
  <input type="hidden" name="pkey" value="<%=pkey%>">
	<input type="hidden" name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>">
	<input type="hidden" name="patProtId" value="">
	<input type="hidden" name="selectedTab" value="<%=selectedTab%>">

<%}
		if (length == 0) {

		if (EJBUtil.stringToNum(pstudy) <= 0)

		{

%>
<table width="99%" cellspacing="0" cellpadding="0" border="0" >
<tr><td><P class="defComments">
		<%=MC.M_NoRecPat_ActiveEnrl%><%--No records found. This <%=LC.Pat_Patient_Lower%> is either not 'Active' or not enrolled in any <%=LC.Std_Study%>.*****--%>
	</P></td></tr></table>
	

	<%

	}


	}
	else
	{
		%>
	<P class="defComments"><%=MC.M_PatAssocTo_Std%><%--This <%=LC.Pat_Patient_Lower%> has been associated to the following <%=LC.Std_Study_ies%>*****--%>:</P>
    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign outline">

    	<tr>
		    	<th width="10%" align =center><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%></th>
				<th width="18%" align =center><%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%></th>
				<th width="10%" align =center><%=LC.L_Study_Contact%><%--<%=LC.Std_Study%> Contact*****--%></th>
    			<th width="8%" align =center><%=LC.L_Enrolled_On%><%--Enrolled On*****--%></th>
				<th width="8%" align =center><%=LC.L_Last_Visit%><%--Last Visit*****--%></th>
				<th width="8%" align =center><%=LC.L_Done_On%><%--Done On*****--%></th>
				<th width="8%" align =center><%=LC.L_Next_Visit%><%--Next Visit*****--%></th>
		  	    <th width="10%" align =center><%=LC.L_Patient_Status%><%--<%=LC.Pat_Patient%> Status*****--%></th>
			    <th width="13%"></th>

		 </tr>

   <%

		for(i = 0 ; i < length; i++)

	  	{

	 	   patientId = patientIds.get(i).toString();
	 	   patientEnrollDate = (String) patientEnrollDates.get(i);
	 	   patientProtId = (String) patientProtIds.get(i);
		   nextVisit = (String) nextVisits.get(i);
		   curVisitDate = (String) curVisitDates.get(i);
		   studyTitle = (String) studyTitles.get(i);

//JM: 10MAR2008: get the individual study access..#PS15, March2008, Enhacement

		    int stdTeamRight = 0;

		    TeamDao stdTeamDao = new TeamDao();
		    stdTeamDao.getTeamRights(((Integer)studIds.get(i)).intValue(),EJBUtil.stringToNum(userId));
		    ArrayList teamId = stdTeamDao.getTeamIds();

		    if (teamId.size() == 0){
				stdTeamRight=0 ;
		    }
			else
			{
		    		stdRights.setId(EJBUtil.stringToNum(teamId.get(0).toString()));

		    		ArrayList stdTeamRights ;
					stdTeamRights  = new ArrayList();
					stdTeamRights = stdTeamDao.getTeamRights();

					stdRights.setSuperRightsStringForStudy((String)stdTeamRights.get(0));
					stdRights.loadStudyRights();

		    	if ((stdRights.getFtrRights().size()) == 0)
				{
		    	 	stdTeamRight= 0;

		    	}
				else
				{
		    		stdTeamRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));// STUDYTEAM

		    	}
		    }
//JM: JM: 10MAR2008: added above study acess permission





	   		if (studyTitle.length() > 80)
			{
				studyTitle = studyTitle.substring(0,80) + " ...";
			}

		   curVisit =  curVisits.get(i).toString();
		   curVisitName=(String)curVisitsName.get(i);
		   curVisitName=(curVisitName==null)?"-":curVisitName;


		   if (curVisit.equals("0"))
		   	curVisit = "-";

		   if ( ! EJBUtil.isEmpty(patientEnrollDate) )
		   {

		 	  patientEnrollDate =	DateUtil.dateToString(java.sql.Date.valueOf(patientEnrollDate.substring(0,10)));

		  }
		  else
		  {
		  	  	patientEnrollDate = "-";
		  }

		  if ((nextVisit!= null) && (!nextVisit.equals("")))

		   	  nextVisit =	DateUtil.dateToString(java.sql.Date.valueOf(nextVisit.substring(0,10)));

			  else

  			  nextVisit = "-";


		  	 if ((curVisitDate!= null) && (!curVisitDate.equals("")))
	    	  curVisitDate =	DateUtil.dateToString(java.sql.Date.valueOf(curVisitDate.substring(0,10)));
			  else
			  curVisitDate =	"-";



		   studyNum = (String) studyNums.get(i);

		   encodedStudyNum	=  StringUtil.encodeString(studyNum);

		   studId = ((Integer) studIds.get(i)).intValue();

//JM: 10Mar2008

			studyConB.setId(studId);
			studyConB.getStudyDetails();
			studyContact ="";
			userLname= "";
			userFName="";

			studyContact=studyConB.getStudyCoordinator();
			studyContact=(studyContact==null)?"":studyContact;

		   	if (!studyContact.equals("")){
		   	UserJB uJB = new UserJB();
		   	uJB.setUserId(EJBUtil.stringToNum(studyContact));
			uJB.getUserDetails();
			userLname = uJB.getUserLastName();
			userFName = uJB.getUserFirstName();
			studyContact = userFName + "  " + userLname;
			}


		   statDesc = (String) statDescs.get(i);

		   patStatPk = ((Integer) patientStatusPks.get(i)).intValue();



			 //if ( tempStudy!= studId)

			// {
			%>





		      <%

			  //}



	 	   if ((i%2)==0) {

%>

      <tr class="browserEvenRow">

<%

	 		   }else{

%>

      <tr class="browserOddRow">

<%

	 		   }
	 	if (stdTeamRight >= 4){
%>


	<td width =15%><A href ="patientschedule.jsp?srcmenu=<%=src%>&selectedTab=3&mode=M&pkey=<%=patientId%>&patProtId=<%=patientProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&studyId=<%=studId%>&generate=N&statDesc=<%=statDesc%>&statid=<%=patStatPk%>&studyVer=null"><%=studyNum%></A></td>
        <td width =15%><%=studyTitle%></td>
        <%}else{%>
        <td width =15%>****</td>
        <td width =15%>****</td>
        <%}%>
        <td width =10%><%=studyContact%></td>
        <td width =8%> <%=patientEnrollDate%> </td>
     <%   if (stdTeamRight >= 4){
     %>
        <td width =8%> <%=curVisitName%> </td>
        <td width =8%> <%=curVisitDate%> </td>
        <td width =8%> <%=nextVisit%> </td>
	<%} else{%>
		<td width =8%> **** </td>
        <td width =8%> ****</td>
        <td width =8%> **** </td>
	<%}%>
		<td width =20% align="center">

		 <%--<a onclick="openWinStatus('<%=patientId%>','<%=patStatPk%>','no')"  href="#"><%=statDesc%></a> --%>		 	
		  <A Title="<%=LC.L_Change_Status%><%--Change Status*****--%>" href ="enrollpatient.jsp?srcmenu=<%=src%>&selectedTab=2&pkey=<%=patientId%>&patProtId=<%=patientProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&studyId=<%=studId%>"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"><%//=LC.L_Change%><%--Change*****--%></A>&nbsp;<%=statDesc%>
		<%-- // changeStatusMode is set to "no" when the user clicks on the status link and is set to "yes" when user clicks  on the change status link
		 &nbsp;<A onclick="openWinStatus('<%=patientId%>','<%=patStatPk%>','yes')"  href="#">Change Status</A>
  		 &nbsp;<A href ="patstudyhistory.jsp?selectedTab=<%=selectedTab%>&from=patientstudies&studyNum=<%=encodedStudyNum%>&pcode=<%=StringUtil.encodeString(patientCode)%>&srcmenu=<%=src%>&studyId=<%=studId%>&patientId=<%=patientId%>">H</A>
  		--%>

		</td>
		<td width="10%"><A href="adveventbrowser.jsp?srcmenu=<%=src%>&selectedTab=6&pkey=<%=patientId%>&patProtId=<%=patientProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&page=patientEnroll&studyId=<%=studId%>&statDesc=<%=statDesc%>&statid=<%=patStatPk%>&studyVer=null&visit=1"><img border="0" title="<%=LC.L_Adverse_Event%>" alt="<%=LC.L_Adverse_Event%>" src="./images/UnexpectedEvent.gif" ><%//=LC.L_Ae%><%--AE*****--%></A>
		<A href="formfilledstdpatbrowser.jsp?srcmenu=<%=src%>&selectedTab=8&mode=M&pkey=<%=patientId%>&patProtId=<%=patientProtId%>&patientCode=<%=StringUtil.encodeString(patientCode)%>&studyId=<%=studId%>&generate=N&statDesc=<%=statDesc%>&statid=<%=patStatPk%>&studyVer=null&calledFrom=S"><%=LC.L_Forms%><%--Forms*****--%></A>

		</td>

      </tr>

	  <%

		tempStudy = studId; //set temp var for control break format

	  }// end for for loop

	  } // end of if for length == 0

	  %>



    </table>

  </Form>

<%

  /////////////////
}//end for else if studids.size>0
  	} //end of if body for page right

else
{

%>



<jsp:include page="accessdenied.jsp" flush="true"/>





<%



} //end of else body for page right





  ///////////////

}//end of if body for session

else{

%>



  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>

</div>





<div class ="mainMenu" id = "emenu">

 <!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>









</body>

</html>

