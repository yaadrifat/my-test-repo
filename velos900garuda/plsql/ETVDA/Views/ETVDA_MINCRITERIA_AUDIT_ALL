 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD") AS 
  SELECT ACM.COLUMN_NAME,
    ACM.COLUMN_DISPLAY_NAME,
    (SELECT USR_LOGNAME FROM eres.ER_USER WHERE PK_USER = ARM.USER_ID
    ) USER_ID,
    TO_CHAR(ARM.TIMESTAMP,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(ARM.ACTION,'I','Add','U','Update','-') ACTION,
    ACM.OLD_VALUE,
    ACM.NEW_VALUE,
    ARM.TABLE_NAME,
    ARM.TIMESTAMP,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA = ARM.MODULE_ID
    ) pk_cord
  FROM eres.AUDIT_ROW_MODULE ARM ,
    eres.AUDIT_COLUMN_MODULE ACM
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
    )
  AND ARM.TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA')
  UNION
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA IN
      (SELECT FK_MINIMUM_CRITERIA
      FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
      WHERE PK_MINIMUM_CRITERIA_FIELD =ARM.MODULE_ID
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_MINIMUM_CRITERIA_FIELD
    FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
    WHERE FK_MINIMUM_CRITERIA in
      (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
      )
    )
  AND ARM.TABLE_NAME = 'CB_CORD_TEMP_MINIMUM_CRITERIA')
  ORDER BY 9 DESC;
 /
 
 COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
 
  COMMENT ON TABLE "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"
IS
  'This view contains the information about the Minimum Criteria and shows all fields in addition to the fields being displayed over Audit Trail widget. ';
  / 
  