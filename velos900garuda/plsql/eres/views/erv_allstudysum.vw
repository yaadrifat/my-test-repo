/* Formatted on 2/9/2010 1:39:13 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_ALLSTUDYSUM
(
   PK_STUDY,
   STUDY_PUBCOLLST,
   STUDY_NUMBER,
   STUDY_TITLE,
   STUDY_VER_NO,
   PK_STUDYVER,
   STUDY_OBJ,
   STUDY_SUM,
   STUDY_PRODNAME,
   STUDY_TAREA,
   STUDY_SAMPLSIZE,
   STUDY_DURATION,
   STUDY_ESTBEGINDT,
   STUDY_PHASE,
   STUDY_RESTYPE,
   STUDY_TYPE,
   STUDY_BLIND,
   STUDY_RANDOM,
   STUDY_SPONSOR,
   STUDY_CONTACT,
   STUDY_INFO,
   STUDY_PARTCNTR,
   STUDY_KEYWRDS,
   PK_STUDYAPNDX,
   FK_STUDY,
   STUDYAPNDX_URI,
   STUDYAPNDX_DESC,
   STUDYAPNDX_FILE,
   STUDYAPNDX_TYPE,
   RECTYPE,
   BIT1,
   BIT2,
   BIT3,
   FK_AUTHOR,
   STUDYSEC_CONTENTS,
   STUDYSEC_CONTENTS2,
   STUDYSEC_CONTENTS3,
   STUDYSEC_CONTENTS4,
   STUDYSEC_CONTENTS5,
   STUDYSEC_SEQ,
   STUDYSEC_NUM,
   STUDY_END_DATE,
   STUDY_ACTUALDT,
   STUDYSEC_TEXT,
   FK_CODELST_SPONSOR
)
AS
   SELECT   a.pk_study,                                                    --1
            a.study_pubcollst,                                             --2
            a.study_number study_number,                                   --3
            a.study_title study_title,                                     --4
            c.studyver_number,                                            --4a
            c.pk_studyver,                                                 --5
            a.STUDY_OBJ_CLOB study_obj,                                   -- 6
            a.STUDY_SUM_CLOB study_sum,                                    --7
            a.STUDY_PRODNAME study_prodname,                               --8
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_tarea)
               study_tarea,                                                --9
            a.STUDY_SAMPLSIZE study_samplsize,                            --10
            a.STUDY_DUR || ' ' || a.STUDY_DURUNIT study_duration,         --11
            a.STUDY_ESTBEGINDT study_estbegindt,                          --12
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_phase)
               study_phase,                                               --13
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_restype)
               study_restype,                                            -- 14
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_TYPE)
               study_type,                                                --15
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_BLIND)
               study_blind,                                               --16
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_RANDOM)
               study_random,                                              --17
            a.STUDY_SPONSOR,                                              --18
            a.STUDY_CONTACT,                                             -- 19
            a.STUDY_INFO,                                                -- 20
            a.STUDY_PARTCNTR,                                            -- 21
            a.STUDY_KEYWRDS,                                             -- 22
            b.pk_studyapndx,                                              --23
            b.fk_study,                                                  -- 24
            b.studyapndx_uri,                                            -- 25
            b.studyapndx_desc,                                           -- 26
            b.studyapndx_file x,                                         -- 27
            b.studyapndx_type,                                           -- 28
            'A' rectype,                                                  --29
            SUBSTR (STUDY_PUBCOLLST, 1, 1) bit1,                          --30
            SUBSTR (STUDY_PUBCOLLST, 2, 1) bit2,                          --31
            SUBSTR (STUDY_PUBCOLLST, 3, 1) bit3,                          --32
            (SELECT   USR_LASTNAME || ', ' || USR_FIRSTNAME
               FROM   ER_USER
              WHERE   pk_user = a.fk_author)
               fk_author,                                                -- 33
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            -1,
            NULL,
            a.study_end_date,
            a.STUDY_ACTUALDT,
            NULL AS STUDYSEC_TEXT,
            a.fk_codelst_sponsor
     FROM   ER_STUDY a, ER_STUDYAPNDX b, ER_STUDYVER c
    WHERE   a.pk_study = c.fk_study(+) AND c.pk_studyver = b.fk_studyver(+)
   UNION ALL
   SELECT   a.pk_study,                                                    --1
            a.study_pubcollst,                                             --2
            a.study_number study_number,                                   --3
            a.study_title study_title,                                     --4
            c.studyver_number,                                            --4a
            c.pk_studyver,                                                 --5
            a.STUDY_OBJ_CLOB study_obj,                                   -- 6
            a.STUDY_SUM_CLOB study_sum,                                    --7
            a.STUDY_PRODNAME study_prodname,                               --8
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_tarea)
               study_tarea,                                                --9
            a.STUDY_SAMPLSIZE study_samplsize,                            --10
            a.STUDY_DUR || ' ' || a.STUDY_DURUNIT study_duration,         --11
            a.STUDY_ESTBEGINDT study_estbegindt,                          --12
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = a.fk_codelst_phase)
               study_phase,                                               --13
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_restype)
               study_restype,                                            -- 14
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_TYPE)
               study_type,                                                --15
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_BLIND)
               study_blind,                                               --16
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = FK_CODELST_RANDOM)
               study_random,                                              --17
            a.STUDY_SPONSOR,                                              --18
            a.STUDY_CONTACT,                                             -- 19
            a.STUDY_INFO,                                                -- 20
            a.STUDY_PARTCNTR,                                            -- 21
            a.STUDY_KEYWRDS,                                             -- 22
            b.pk_studysec,                                               -- 23
            b.fk_study,                                                  -- 24
            b.studysec_name,                                             -- 25
            b.studysec_pubflag,                                          -- 26
            TO_CHAR (b.studysec_seq) x,                                  -- 27
            NULL AS sc,                                                  -- 28
            'S' rectype,                                                  --29
            SUBSTR (STUDY_PUBCOLLST, 1, 1) bit1,                         -- 30
            SUBSTR (STUDY_PUBCOLLST, 2, 1) bit2,                          --31
            SUBSTR (STUDY_PUBCOLLST, 3, 1) bit3,                          --32
            (SELECT   USR_LASTNAME || ', ' || USR_FIRSTNAME
               FROM   ER_USER
              WHERE   pk_user = a.fk_author)
               fk_author,                                                -- 33
            TRIM (b.studysec_contents),
            TRIM (b.studysec_contents2),
            TRIM (b.studysec_contents3),
            TRIM (b.studysec_contents4),
            TRIM (b.studysec_contents5),
            b.STUDYSEC_SEQ,
            TRIM (b.studysec_num),
            a.study_end_date,
            a.STUDY_ACTUALDT,
            STUDYSEC_TEXT,
            a.fk_codelst_sponsor
     FROM   ER_STUDY a, ER_STUDYSEC b, ER_STUDYVER c
    WHERE   a.pk_study = c.fk_study AND c.pk_studyver = b.fk_studyver(+)
   ORDER BY   x ASC;


CREATE SYNONYM ESCH.ERV_ALLSTUDYSUM FOR ERV_ALLSTUDYSUM;


CREATE SYNONYM EPAT.ERV_ALLSTUDYSUM FOR ERV_ALLSTUDYSUM;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_ALLSTUDYSUM TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_ALLSTUDYSUM TO ESCH;

