/* Formatted on 2/9/2010 1:39:33 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_SPECIMEN_STORAGE
(
   FK_ACCOUNT,
   PK_STORAGE,
   STORAGE_ID,
   STORAGE_NAME,
   STORAGE_ALTERNATEID,
   STORAGE_TYPE,
   CHILD_STORAGE_TYPE,
   PARENT_STORAGE_ID,
   PARENT_STORAGE,
   STORAGE_FK_STUDY,
   STORAGE_STUDY_NUMBER,
   STORAGE_STATUS,
   STORAGE_STATUS_DATE,
   STORAGE_TRACKING_USER,
   STORAGE_TRACKING_NO,
   STORAGE_RID,
   STORAGE_CREATOR,
   STORAGE_CREATED_ON,
   STORAGE_LAST_MODIFIED_BY,
   STORAGE_LAST_MODIFIED_DATE,
   PK_SPECIMEN,
   SPEC_ID,
   SPEC_ALTERNATE_ID,
   SPEC_DESCRIPTION,
   SPEC_TYPE,
   SPEC_COLLECTION_DATE,
   EXP_SPEC_QTY,
   EXP_QTY_UNITS,
   COLL_SPEC_QTY,
   COLL_QTY_UNITS,
   SPEC_QTY,
   SPEC_QTY_UNITS,
   OWNER,
   SPEC_FK_STUDY,
   SPEC_STUDY_NUMBER,
   SPEC_ORGN,
   SPEC_ANATOMIC_SITE,
   SPEC_TISSUE_SIDE,
   SPEC_PATHOLOGY_STAT,
   PARENT_SPEC_ID,
   SPEC_STATUS,
   SPEC_STATUS_DATE,
   SPEC_STATUS_QTY,
   SPEC_STATUS_QTY_UNITS,
   SPEC_STATUS_ACTION,
   SPEC_PROCESSING_TYPE,
   SPEC_REMOVAL_TIME,
   SPEC_FREEZE_TIME,
   RECEPIENT,
   PATHOLOGIST,
   SURGEON,
   COLLECTING_TECHNICIAN,
   PROCESSING_TECHNICIAN,
   SPEC_TRACKING_NO,
   SPEC_CREATOR,
   SPEC_CREATED_ON,
   SPEC_LAST_MODIFIED_BY,
   SPEC_LAST_MODIFIED_DATE,
   CREATED_ON
)
AS
   SELECT   a.fk_account,
            pk_storage,
            a.storage_id,
            storage_name,
            STORAGE_ALTERNALEID AS STORAGE_ALTERNATEID,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_type)
               storage_type,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_child_stype)
               child_storage_type,
            (SELECT   aa.storage_id
               FROM   ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage_id,
            (SELECT   aa.storage_name
               FROM   ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage,
            b.fk_study storage_fk_study,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = b.fk_study)
               storage_study_number,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_status)
               storage_status,
            ss_start_date storage_status_date,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = b.fk_user)
               AS Storage_tracking_user,
            b.ss_tracking_number Storage_tracking_no,
            a.rid storage_rid,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.creator)
               storage_creator,
            a.created_on storage_created_on,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.last_modified_by)
               storage_last_modified_by,
            a.last_modified_date storage_last_modified_date,
            s.pk_specimen pk_specimen,
            SPEC_ID,
            SPEC_ALTERNATE_ID,
            SPEC_DESCRIPTION,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_TYPE)
               SPEC_TYPE,
            SPEC_COLLECTION_DATE,
            SPEC_EXPECTED_QUANTITY exp_spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_EXPECTEDQ_UNITS)
               exp_qty_units,
            SPEC_BASE_ORIG_QUANTITY coll_spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_BASE_ORIGQ_UNITS)
               coll_qty_units,
            SPEC_ORIGINAL_QUANTITY spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_QUANTITY_UNITS)
               spec_qty_units,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = SPEC_OWNER_FK_USER)
               AS Owner,
            s.FK_STUDY spec_fk_study,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = s.FK_STUDY)
               spec_study_number,
            (SELECT   site_name
               FROM   ER_SITE
              WHERE   pk_site = s.FK_SITE)
               AS spec_orgn,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_ANATOMIC_SITE)
               SPEC_ANATOMIC_SITE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_TISSUE_SIDE)
               SPEC_TISSUE_SIDE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_PATHOLOGY_STAT)
               SPEC_PATHOLOGY_STAT,
            (SELECT   aa.SPEC_ID
               FROM   ER_SPECIMEN aa
              WHERE   aa.pk_specimen = s.FK_SPECIMEN)
               parent_spec_id,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = ss.FK_CODELST_STATUS)
               spec_status,
            SS_DATE spec_status_date,
            SS_QUANTITY spec_status_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SS_QUANTITY_UNITS)
               spec_status_qty_units,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SS_ACTION)
               spec_status_action,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = ss.SS_PROC_TYPE)
               spec_processing_type,
            SPEC_REMOVAL_TIME,
            SPEC_FREEZE_TIME,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_RECEPIENT)
               AS RECEPIENT,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_PATH)
               AS Pathologist,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_SURG)
               AS Surgeon,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_COLLTECH)
               AS Collecting_Technician,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_PROCTECH)
               AS Processing_Technician,
            ss.SS_TRACKING_NUMBER spec_tracking_no,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = s.CREATOR)
               spec_creator,
            s.CREATED_ON spec_created_on,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = s.LAST_MODIFIED_BY)
               spec_LAST_MODIFIED_BY,
            s.LAST_MODIFIED_DATE spec_last_modified_date,
			a.created_on
     FROM   ER_STORAGE a,
            ER_STORAGE_STATUS b,
            ER_SPECIMEN s,
            ER_SPECIMEN_STATUS ss
    WHERE       a.FK_ACCOUNT = s.FK_ACCOUNT
            AND a.pk_storage = b.fk_storage
            AND a.pk_storage = s.fk_storage
            AND b.pk_storage_status =
                  (SELECT   MAX (c.pk_storage_status)
                     FROM   ER_STORAGE_STATUS c
                    WHERE   a.pk_storage = c.fk_storage
                            AND c.ss_start_date =
                                  (SELECT   MAX (d.ss_start_date)
                                     FROM   ER_STORAGE_STATUS d
                                    WHERE   a.pk_storage = d.fk_storage))
            AND ss.pk_specimen_status =
                  (SELECT   MAX (c.pk_specimen_status)
                     FROM   ER_SPECIMEN_STATUS c
                    WHERE   s.pk_specimen = c.fk_specimen
                            AND c.ss_date =
                                  (SELECT   MAX (d.ss_date)
                                     FROM   ER_SPECIMEN_STATUS d
                                    WHERE   s.pk_specimen = d.fk_specimen));


