/* Formatted on 2/9/2010 1:39:27 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_PATSCH
(
   PERSON_CODE,
   PAT_STUDYID,
   FK_SITE,
   SITE_NAME,
   PERSON_NAME,
   STUDY_NUMBER,
   STUDY_TITLE,
   STUDY_ACTUALDT,
   STUDY_VER_NO,
   PATPROT_ENROLDT,
   PROTOCOL_NAME,
   PATPROT_START,
   EVENT_ID,
   EVENT_NAME,
   MONTH,
   START_DATE_TIME,
   SORT_DATE,
   EVENT_STATUS,
   FK_ASSOC,
   PROTOCOLID,
   FK_STUDY,
   FK_PER,
   EVENT_EXEON,
   EVENT_SCHDATE,
   FK_PATPROT,
   VISIT,
   SCHEVE_STATUS,
   FUZZY_PERIOD,
   VISIT_NAME,
   FK_CODELST_STAT,
   TX_ARM,
   FK_USERASSTO,
   EVENT_STATUS_SUBTYPE,
   EVENT_STATUS_DESC,
   NOTES,
   PATPROT_STAT,
   EVENT_SEQUENCE
)
AS
   SELECT   pr.person_code,
            pp.patprot_patstdid pat_studyid,
            pr.fk_site,
            pr.site_name,
            pr.person_lname || ', ' || pr.person_fname person_name,
            st.study_number,
            st.study_title,
            st.study_actualdt,
            st.study_ver_no,
            pp.patprot_enroldt,
            (SELECT   NAME
               FROM   erv_eveassoc
              WHERE   event_id = sc.session_id)
               protocol_name,
            pp.patprot_start,
            sc.event_id,
            sc.description event_name,
            TRIM (TO_CHAR (sc.event_schdate, 'Month YYYY')) MONTH,
            sc.start_date_time,
            TO_CHAR (sc.event_schdate, 'YYYYMM') AS sort_date,
            sc.isconfirmed event_status,
            sc.fk_assoc,
            sc.session_id protocolid,
            pp.fk_study,
            pp.fk_per,
            sc.event_exeon,
            sc.event_schdate,
            pp.pk_patprot,
            sc.visit,
            sc.status,
            sc.visit_type_id,
            visit_name,
            fk_codelst_stat,
            (SELECT   fk_studytxarm
               FROM   ER_PATTXARM
              WHERE   pk_pattxarm =
                         (SELECT   MAX (pk_pattxarm)
                            FROM   ER_PATTXARM b
                           WHERE   b.fk_patprot = pp.pk_patprot
                                   AND tx_start_date =
                                         (SELECT   MAX (tx_start_date)
                                            FROM   ER_PATTXARM a
                                           WHERE   a.fk_patprot =
                                                      pp.pk_patprot)))
               AS tx_arm,
            pp.fk_userassto,
            sc.event_status_subtype event_status_subtype,
            sc.codelst_desc status_desc,
            notes,
            patprot_stat,
            sc.event_sequence
     FROM   erv_scheve sc,
            ER_PATPROT pp,
            ER_STUDY st,
            erv_person pr,
            ER_PATSTUDYSTAT ps
    WHERE       sc.fk_patprot = pp.pk_patprot
            AND sc.patient_id = pr.pk_person
            AND pp.fk_study = st.pk_study
            AND pp.fk_study = ps.fk_study
            AND sc.patient_id = ps.fk_per
            AND ps.patstudystat_endt IS NULL;


CREATE SYNONYM ESCH.ERV_PATSCH FOR ERV_PATSCH;


CREATE SYNONYM EPAT.ERV_PATSCH FOR ERV_PATSCH;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_PATSCH TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_PATSCH TO ESCH;

