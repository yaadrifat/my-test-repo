/* Formatted on 2/9/2010 1:39:45 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_VISIT1_EVCOUNT
(
   PK_PATPROT,
   TOT_EVENT_COUNT,
   FK_STUDY
)
AS
     SELECT   s.fk_patprot patprot, COUNT (s.EVENT_ID) cnt, e.fk_study
       FROM   SCH_EVENTS1 s, er_patprot e
      WHERE   e.pk_patprot = s.fk_patprot AND s.visit = 1
   GROUP BY   s.fk_patprot, e.fk_study;


CREATE SYNONYM ESCH.ERV_VISIT1_EVCOUNT FOR ERV_VISIT1_EVCOUNT;


CREATE SYNONYM EPAT.ERV_VISIT1_EVCOUNT FOR ERV_VISIT1_EVCOUNT;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_VISIT1_EVCOUNT TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_VISIT1_EVCOUNT TO ESCH;

