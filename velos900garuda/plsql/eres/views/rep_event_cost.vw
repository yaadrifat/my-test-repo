/* Formatted on 2/9/2010 1:39:51 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_EVENT_COST
(
   EVCST_EVENT,
   EVCST_DESCRIPTION,
   EVCST_UNIT,
   EVCST_COST,
   FK_ACCOUNT,
   RID,
   EVCST_RESPONSE_ID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   EVENT_CATEGORY_NAME,
   EVENT_LIBRARY_TYPE,
   BUDGET_CATEGORY
)
AS
   SELECT   d.NAME AS EVCST_EVENT,
            (SELECT   CODELST_DESC
               FROM   SCH_CODELST
              WHERE   PK_CODELST = FK_COST_DESC)
               EVCST_DESCRIPTION,
            (SELECT   CODELST_DESC
               FROM   SCH_CODELST
              WHERE   PK_CODELST = FK_CURRENCY)
               EVCST_UNIT,
            EVENTCOST_VALUE EVCST_COST,
            (SELECT   USER_ID
               FROM   EVENT_DEF
              WHERE   EVENT_ID = FK_EVENT)
               FK_ACCOUNT,
            c.RID,
            PK_EVENTCOST EVCST_RESPONSE_ID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = c.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = c.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            c.LAST_MODIFIED_DATE,
            c.CREATED_ON,
            (SELECT A.NAME 
              FROM EVENT_DEF A 
              WHERE a.event_type = 'L'  AND A.EVENT_ID = D.CHAIN_ID) EVENT_CATEGORY_NAME,
            (SELECT CODELST_DESC 
	     FROM SCH_CODELST , EVENT_DEF B   
	     WHERE  B.event_type = 'L'  AND B.EVENT_ID = D.CHAIN_ID AND 
	     SCH_CODELST.PK_CODELST = D.EVENT_LIBRARY_TYPE) EVENT_LIBRARY_TYPE,
	    (SELECT CODELST_DESC 
	     FROM SCH_CODELST , EVENT_DEF B   
	     WHERE  B.event_type = 'L'  AND B.EVENT_ID = D.CHAIN_ID AND 
	     SCH_CODELST.PK_CODELST = D.EVENT_LINE_CATEGORY) BUDGET_CATEGORY
     FROM   SCH_EVENTCOST c, EVENT_DEF d
    WHERE   EVENT_ID = FK_EVENT AND event_type = 'E';


