/* Formatted on 2/9/2010 1:39:36 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STUDY_MORE_DETAILS
(
   STUDY_NUMBER,
   STUDY_TITLE,
   FIELD_NAME,
   FIELD_VALUE,
   FK_STUDY,
   CREATED_ON,
   FK_ACCOUNT,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   STMSD_RESPONSE_ID
)
AS
   SELECT   study_number,
            study_title,
            REPLACE (codelst_desc, '&nbsp;', '') AS field_name,
            studyid_id field_value,
            fk_study,
            ER_STUDYID.created_on,
            ER_STUDY.fk_account,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYID.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYID.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDYID.LAST_MODIFIED_DATE,
            ER_STUDYID.RID,
            PK_STUDYID STMSD_RESPONSE_ID
     FROM   ER_STUDYID, ER_CODELST, ER_STUDY
    WHERE   studyid_id IS NOT NULL AND pk_codelst = fk_codelst_idtype
            AND ( (TRIM (studyid_id) = 'Y'
                   AND TRIM (codelst_custom_col) = 'chkbox')
                 OR (codelst_custom_col) = 'dropdown')
            AND FK_STUDY = PK_STUDY
   UNION
   SELECT   study_number,
            study_title,
            REPLACE (codelst_desc, '&nbsp;', '') AS field_name,
            studyid_id field_value,
            fk_study,
            ER_STUDYID.created_on,
            ER_STUDY.fk_account,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYID.CREATOR)
               CREATOR,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_STUDYID.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_STUDYID.LAST_MODIFIED_DATE,
            ER_STUDYID.RID,
            PK_STUDYID STMSD_RESPONSE_ID
     FROM   ER_STUDYID, ER_CODELST, ER_STUDY
    WHERE       studyid_id IS NOT NULL
            AND LENGTH (TRIM (studyid_id)) > 0
            AND pk_codelst = fk_codelst_idtype
            AND (codelst_custom_col IS NULL
                 OR LENGTH (TRIM (codelst_custom_col)) = 0)
            AND FK_STUDY = PK_STUDY;


CREATE SYNONYM ESCH.ERV_STUDY_MORE_DETAILS FOR ERV_STUDY_MORE_DETAILS;


CREATE SYNONYM EPAT.ERV_STUDY_MORE_DETAILS FOR ERV_STUDY_MORE_DETAILS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDY_MORE_DETAILS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDY_MORE_DETAILS TO ESCH;

