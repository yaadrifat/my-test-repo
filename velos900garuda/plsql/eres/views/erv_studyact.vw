/* Formatted on 2/9/2010 1:39:39 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_STUDYACT
(
   STUDY_NUMBER,
   STUDY_TITLE,
   STUDY_VER_NO,
   TA,
   STUDY_PRINV,
   PI,
   FK_CODELST_PHASE,
   PHASE,
   FK_CODELST_RESTYPE,
   RESEARCH_TYPE,
   FK_CODELST_TYPE,
   STUDY_TYPE,
   STUDY_OTHERPRINV,
   STUDY_PARTCNTR,
   DATA_MGR,
   STUDYSTAT_DATE,
   FK_ACCOUNT,
   STUDYACTUAL_DATE,
   STUDYEND_DATE,
   STUDY_DIVISION,
   FK_CODELST_TAREA,
   PK_STUDY
)
AS
   SELECT   ER_STUDY.study_number,
            ER_STUDY.study_title,
            ER_STUDY.study_ver_no,
            ER_CODELST.codelst_desc ta,
            ER_STUDY.study_prinv,
            (SELECT   ER_USER.usr_firstname || '  ' || ER_USER.usr_lastname
               FROM   ER_USER
              WHERE   pk_user = ER_STUDY.study_prinv)
               AS pi,
            ER_STUDY.fk_codelst_phase,
            (SELECT   ER_CODELST.codelst_desc
               FROM   ER_CODELST
              WHERE   ER_CODELST.codelst_type = 'phase'
                      AND ER_STUDY.fk_codelst_phase = ER_CODELST.pk_codelst)
               AS Phase,
            ER_STUDY.fk_codelst_restype,
            (SELECT   ER_CODELST.codelst_desc
               FROM   ER_CODELST
              WHERE   ER_CODELST.codelst_type = 'research_type'
                      AND ER_STUDY.fk_codelst_restype = ER_CODELST.pk_codelst)
               AS Research_Type,
            ER_STUDY.fk_codelst_type,
            (SELECT   ER_CODELST.codelst_desc
               FROM   ER_CODELST
              WHERE   ER_CODELST.codelst_type = 'study_type'
                      AND ER_STUDY.fk_codelst_type = ER_CODELST.pk_codelst)
               AS Study_Type,
            ER_STUDY.study_otherprinv,
            ER_STUDY.study_partcntr,
            ER_USER.usr_firstname || '  ' || ER_USER.usr_lastname data_mgr,
            ER_STUDYSTAT.studystat_date,
            ER_STUDY.fk_account,
            ER_STUDY.study_actualdt,
            ER_STUDY.study_end_date,
            ER_STUDY.study_division,
            ER_STUDY.fk_codelst_tarea,
            ER_STUDY.pk_study
     FROM   ER_STUDY,
            ER_STUDYSTAT,
            ER_CODELST,
            ER_USER
    WHERE   fk_codelst_studystat =
               (SELECT   pk_codelst
                  FROM   ER_CODELST
                 WHERE   codelst_type = 'studystat'
                         AND codelst_subtyp = 'active')
            AND pk_study = fk_study
            AND fk_codelst_tarea = pk_codelst
            AND ER_USER.pk_user = ER_STUDY.fk_author
            AND ER_STUDY.study_verparent IS NULL
            AND pk_study NOT IN
                     (SELECT   fk_study
                        FROM   ER_STUDYSTAT
                       WHERE   fk_codelst_studystat IN
                                     (SELECT   pk_codelst
                                        FROM   ER_CODELST
                                       WHERE   codelst_type = 'studystat'
                                               AND codelst_subtyp =
                                                     'prmnt_cls'));


CREATE SYNONYM ESCH.ERV_STUDYACT FOR ERV_STUDYACT;


CREATE SYNONYM EPAT.ERV_STUDYACT FOR ERV_STUDYACT;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDYACT TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERV_STUDYACT TO ESCH;

