/* Formatted on 2/9/2010 1:39:10 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_ADMINSCH
(
   STUDY_NUMBER,
   STUDY_TITLE,
   STUDY_ACTUALDT,
   STUDY_VER_NO,
   PROTOCOL_NAME,
   EVENT_ID,
   EVENT_NAME,
   MONTH,
   START_DATE_TIME,
   SORT_DATE,
   EVENT_STATUS,
   FK_ASSOC,
   PROTOCOLID,
   EVENT_EXEON,
   ACTUAL_SCHDATE,
   VISIT,
   STATUS,
   VISIT_TYPE_ID,
   VISIT_NAME,
   FK_STUDY,
   EVENT_STATUS_DESC
)
AS
   SELECT   st.study_number,
            st.study_title,
            st.study_actualdt,
            st.study_ver_no,
            (SELECT   NAME
               FROM   erv_eveassoc
              WHERE   event_id = sc.session_id)
               protocol_name,
            sc.event_id,
            sc.description event_name,
            TRIM (TO_CHAR (sc.event_schdate, 'Month YYYY')) MONTH,
            sc.start_date_time,
            TO_CHAR (sc.event_schdate, 'YYYYMM') AS sort_date,
            sc.isconfirmed event_status,
            sc.fk_assoc,
            sc.session_id protocolid,
            sc.event_exeon,
            sc.actual_schdate,
            sc.visit,
            sc.status,
            sc.visit_type_id,
            visit_name,
            sc.fk_study,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   pk_codelst = sc.isconfirmed)
               EVENT_STATUS_DESC
     FROM   erv_scheve sc, ER_STUDY st
    WHERE   sc.fk_study = st.pk_study;


