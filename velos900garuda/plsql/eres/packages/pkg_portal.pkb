CREATE OR REPLACE PACKAGE BODY        "PKG_PORTAL" AS
/******************************************************************************
   NAME:       PKG_PATIENT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8/24/2007 Sonia Abrol             1. Created this package for portal specific Pl/SQL.
******************************************************************************/

/* create default logins for the portal*/
PROCEDURE SP_CREATE_DEFLOGINS(
   p_portal IN Number, p_creator IN NUMBER,p_ip_add IN Varchar2, o_ret OUT NUMBER
   )
   AS
   v_defpass varchar2(150);
   v_portal_level varchar2(2);
   v_login_status  Number;
   v_logout_time Number := 15;

   BEGIN

   	begin
	   	select portal_defaultpass,portal_level
	   	into v_defpass ,v_portal_level
	   	from ER_PORTAL
	   	where pk_portal = p_portal;

	   	select Pkg_Util.f_getcodepk ('A', 'patLoginStat')
	   	into v_login_status
	   	from dual;

	   	--plog.debug(pctx,'v_defpass' || v_defpass || 'v_portal_level' || v_portal_level||'*');

	   	if v_defpass is not null then
	   		--get the patient list according to portal population

	   		if v_portal_level = 'S' then		--Study  level

	   			INSERT INTO ER_PORTAL_LOGINS (PK_PORTAL_LOGIN, PL_ID, PL_ID_TYPE, PL_LOGIN, PL_PASSWORD, PL_STATUS, PL_LOGOUT_TIME, FK_PORTAL, 			  CREATOR,CREATED_ON,IP_ADD)
	   			Select seq_ER_PORTAL_LOGINS.nextval,pk_person,'P',person_code,v_defpass,v_login_status,v_logout_time,p_portal,p_creator,sysdate,p_ip_add
	   			from person, (select pp_object_id from ER_PORTAL_POPLEVEL where fk_portal =p_portal and pp_object_type='S') ps,ER_PATPROT pp
	   			where PERSON_DEATHDT is null
	   			and pp.fk_study =  ps.pp_object_id
	   			and	pk_person = pp.fk_per  and pp.patprot_stat = 1
	   			and not exists (select * from ER_PORTAL_LOGINS where ( PL_ID = pk_person and PL_ID_TYPE = 'P' and fk_portal = p_portal) or trim(lower(PL_LOGIN)) = trim(lower(person_code )) )
	   			and Pkg_User.f_right_forpatsites(pk_person,p_creator)  > 0 ;
			elsif v_portal_level = 'O' then		--organization level


				INSERT INTO ER_PORTAL_LOGINS (PK_PORTAL_LOGIN, PL_ID, PL_ID_TYPE, PL_LOGIN, PL_PASSWORD, PL_STATUS, PL_LOGOUT_TIME, FK_PORTAL, 			  CREATOR,CREATED_ON,IP_ADD)
	   			Select seq_ER_PORTAL_LOGINS.nextval,pk_person,'P',person_code,v_defpass,v_login_status,v_logout_time,p_portal,p_creator,sysdate,p_ip_add
	   			from person, (select fk_per patfac_per, pp_object_id from ER_PORTAL_POPLEVEL,ER_PATFACILITY where fk_portal =p_portal and pp_object_type='O' 	and fk_site = pp_object_id) ps
	   			where PERSON_DEATHDT is null
	   			and	pk_person = ps.patfac_per
	   			and not exists (select * from ER_PORTAL_LOGINS where ( PL_ID = pk_person and PL_ID_TYPE = 'P' and fk_portal = p_portal) or trim(lower(PL_LOGIN)) = trim(lower(person_code )) )
	   			and Pkg_User.f_right_forpatsites(pk_person,p_creator)  > 0 ;
	   		elsif v_portal_level = 'P' then		--patient level

	   			INSERT INTO ER_PORTAL_LOGINS (PK_PORTAL_LOGIN, PL_ID, PL_ID_TYPE, PL_LOGIN, PL_PASSWORD, PL_STATUS, PL_LOGOUT_TIME, FK_PORTAL, 			  CREATOR,CREATED_ON,IP_ADD)
	   			Select seq_ER_PORTAL_LOGINS.nextval,pk_person,'P',person_code,v_defpass,v_login_status,v_logout_time,p_portal,p_creator,sysdate,p_ip_add
	   			from person,(select pp_object_id from ER_PORTAL_POPLEVEL where fk_portal = p_portal and pp_object_type='P' ) pp
	   			where PERSON_DEATHDT is null
	   			and	pk_person = pp.pp_object_id
	   			and not exists (select * from ER_PORTAL_LOGINS where ( PL_ID = pk_person and PL_ID_TYPE = 'P' and fk_portal = p_portal) or trim(lower(PL_LOGIN)) = trim(lower(person_code )) )
	   			and Pkg_User.f_right_forpatsites(pk_person,p_creator)  > 0 ;

	   		elsif v_portal_level = 'A' then --Account level

	   		--plog.debug(pctx,'v_portal_level' || v_portal_level||'*');

	   	INSERT INTO ER_PORTAL_LOGINS (PK_PORTAL_LOGIN, PL_ID, PL_ID_TYPE, PL_LOGIN, PL_PASSWORD, PL_STATUS,  PL_LOGOUT_TIME, FK_PORTAL,CREATOR,CREATED_ON,IP_ADD)
	   			Select seq_ER_PORTAL_LOGINS.nextval,pk_person,'P',person_code,v_defpass,v_login_status,
	   			v_logout_time,p_portal,p_creator,sysdate, p_ip_add
	   			from person,(select pp_object_id from ER_PORTAL_POPLEVEL where fk_portal = p_portal and pp_object_type='A' ) pp
	   			where PERSON_DEATHDT is null
	   			and	fk_account = pp.pp_object_id
	   			and not exists (select * from ER_PORTAL_LOGINS where ( PL_ID = pk_person and PL_ID_TYPE = 'P' and fk_portal = p_portal) or trim(lower(PL_LOGIN)) = trim(lower(person_code )) )
	   			and Pkg_User.f_right_forpatsites(pk_person,p_creator)  > 0 ;

	   		--plog.debug(pctx,'p_portal' || p_portal||'*' || 'p_creator' || p_creator|| 'fqlcode' || sqlcode);


	   		end if; --portal level if;

  		end if;
  		o_ret := 0;
  	exception when OTHERS then
  		 	o_ret := -1;
  		 	plog.fatal(pctx,'Exception in creating portal logins, pkg_portal.SP_CREATE_DEFLOGINS : ' || sqlerrm);
   	end;

  commit;


 END;

 /* creates default login using the patient MRN if the account is set for automatic login facility*/

  PROCEDURE SP_CREATE_AUTOLOGIN(
   p_login IN Varchar, o_password out Varchar,o_loginflag out Number, o_err OUT Varchar ,p_providedpassword In VARCHAR2,
   p_ipadd in varchar2
   )
   AS
   v_portal Number;
   v_count_login Number;
   v_pat_pk Number;
   v_patsite Number;
   v_acc Number;
   v_audituser Number;
   v_stat Number;
   v_pass Varchar2(300);
   v_login_stat Number;
   v_tz varchar2(200);
   v_autologin_flag Number := 0;

   Begin
	begin
	   	begin
	   	   --get the portal set for auto logins

	   	   select ctrl_value
	   	   into v_portal
	   	   from ER_CTRLTAB
	   	   where ctrl_key = 'auto_portal';

	   	exception when NO_DATA_FOUND then
	   		v_portal := 0;
	    end;

    if v_portal = 0 then -- no portal set for auto logins
    	o_loginflag := 0;
    	return;
    else
    	--check for duplicate patient login using the p_login parameter

    	select count(*)
    	into v_count_login
    	from ER_PORTAL_LOGINS
    	where lower(trim(pl_login)) = lower(trim(p_login));


    	if v_count_login = 0 then -- login does not exist, create one

    	 	--check if the patient exists using the p_login parameter

    	 	begin
	    		select pk_person
	    		into v_pat_pk
	    		from person
	    		where lower(trim(person_code)) = lower(trim(p_login));

	    	 exception 	when NO_DATA_FOUND then
				v_pat_pk := 0;

			end;-- for transaction block for patient check

			--get portal details

			select fk_Account,portal_audituser,portal_defaultpass,nvl(portal_createlogins,0)
			into v_acc, v_audituser,v_pass,v_autologin_flag
			from ER_PORTAL where pk_portal = v_portal;

			if nvl(p_providedpassword,'') <> v_pass then
				o_loginflag := 0;
				return;
			end if;

			if v_pat_pk = 0 then --create  a patient
					   select ctrl_value
				   	   into v_patsite
				   	   from ER_CTRLTAB
				   	   where ctrl_key = 'portal_site';

					if v_patsite > 0 then

						select SEQ_ER_PER.nextval
						into v_pat_pk
						from dual;

						--get 'active' status
						select Pkg_Impex.getCodePk('A','patient_status')
						into v_stat
						from dual;

						--check for account level timezone settings
						begin
							select settings_value
							into v_tz
							from ER_SETTINGS where
	                		settings_modname= 1 and settings_modnum=v_acc
	                		and settings_keyword='ACC_PAT_TZ';

	                	exception when NO_DATA_FOUND then

	                		v_tz := null;

	                	end;


						insert into person (pk_person,person_code,fk_account,fk_site,fk_codelst_pstat,creator,pat_facilityid,ip_add,fk_timezone)
						values(v_pat_pk,p_login,v_acc,v_patsite,v_stat,v_audituser,p_login,p_ipadd,v_tz);

					end if;

			end if;

			--create portal login

			if  v_autologin_flag = 0 then  -- that means the auto login flag is not set for the portal and the login was not created by default
				select Pkg_Impex.getCodePk('A','patLoginStat')
				into v_login_stat
				from dual;

				INSERT INTO ER_PORTAL_LOGINS ( PK_PORTAL_LOGIN, PL_ID, PL_ID_TYPE, PL_LOGIN, PL_PASSWORD, PL_STATUS,
				PL_LOGOUT_TIME, FK_PORTAL,  CREATOR,ip_add) VALUES (
				seq_ER_PORTAL_LOGINS.nextval, v_pat_pk, 'P', p_login, v_pass, v_login_stat, 15, v_portal, v_audituser ,p_ipadd);

			end if;


			commit;

				o_loginflag := 1;
				o_password := v_pass;

		else -- login exists
			o_loginflag := 0;

    	end if; -- for v_count_login

    end if; -- for else v_portal

   exception when OTHERS then
   				o_loginflag := 0;
   				o_err := sqlerrm;
   				plog.fatal(pctx,'Exception in creating auto login, pkg_portal.SP_CREATE_AUTOLOGIN : ' || sqlerrm);

   end;
 End;

 --this procedure is called from triggers, so commit is not added
  procedure sp_create_deflogin(p_calledfrom varchar2, p_patient in number,p_study in number,
  p_site in number,p_usr in number,p_ipadd in Varchar2
  )
  As
  v_pass Varchar2(200);
  v_portal Number;
  v_patientcode varchar2(30);
  v_count Number := 0;
  v_login_stat Number := 0;
  begin
  		--check from where the procedure is called

  		if p_calledfrom  = 'S' then -- patient was enrolled
  			--check if there is a portal for this study that is set to create auto logins

  			begin

	  			select pk_portal,portal_defaultpass
	  			into v_portal,v_pass
	  			from ER_PORTAL where fk_study = p_study
	  			and portal_createlogins = 1 and portal_status = 'A' and
	  			 	rownum < 2;

			exception when NO_DATA_FOUND then
				v_portal := 0;
			end;
		else --called from facility, patient was added to a facility

  			 --check if there is a portal for patient's account
  			 begin

  			 	select pk_portal,portal_defaultpass
  			 	into v_portal,v_pass
  			 	from ER_PORTAL P, ER_SITE s, ER_PORTAL_POPLEVEL l
  			 	where pk_site = p_site and  s.fk_account = P.fk_account and
  			 	l.fk_portal = pk_portal and
  			 	l.pp_object_type = 'A' and l.pp_object_id = s.fk_account and
  			 	portal_createlogins = 1 and portal_status = 'A' and
	  			 	rownum < 2;


  			 exception when NO_DATA_FOUND then
  			 	v_portal := 0;
  			 end;


  			 if v_portal = 0 then --check if there is a portal linked with site

	  			 begin
	  			 	select pk_portal,portal_defaultpass
	  			 	into v_portal,v_pass
	  			 	from ER_PORTAL P, ER_SITE s
	  			 	where pk_site = p_site and  s.fk_account = P.fk_account and
	  			 	portal_createlogins = 1 and
	  			 	exists (select * from ER_PORTAL_POPLEVEL
	  			 			where fk_portal = pk_portal and pp_object_type = 'O' and pp_object_id = pk_site) and
	  			 			portal_status = 'A' and
	  			 	rownum < 2		;

	  			 exception when NO_DATA_FOUND then
	  			 	v_portal := 0;
	  			 end;


  			 end if;


  	    end if;


		if v_portal > 0 then

			select per_code
			into v_patientcode
			from ER_PER
			where pk_per = p_patient;


			--check if login exists for v_patientcode


			select count(*)
			into v_count
			from ER_PORTAL_LOGINS
			where ( lower(trim(PL_LOGIN)) = lower(trim(v_patientcode)) )
			 or (pl_id = p_patient and PL_ID_TYPE = 'P' and FK_PORTAL = v_portal);

			if v_count = 0 then


				select Pkg_Impex.getCodePk('A','patLoginStat')
				into v_login_stat
				from dual;


				INSERT INTO ER_PORTAL_LOGINS ( PK_PORTAL_LOGIN, PL_ID, PL_ID_TYPE, PL_LOGIN, PL_PASSWORD, PL_STATUS,
				PL_LOGOUT_TIME, FK_PORTAL,  CREATOR,ip_add) VALUES (
			seq_ER_PORTAL_LOGINS.nextval, p_patient, 'P', v_patientcode, v_pass,
			v_login_stat, 15, v_portal, p_usr ,p_ipadd);



			end if;

		end if;

  end;

END Pkg_Portal;
/


