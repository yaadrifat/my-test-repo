CREATE OR REPLACE PACKAGE        "PKG_PM_RULE" 
AS
   PROCEDURE SP_CHK_PAT_RULE_ENR_ACT (
      P_STUDY            NUMBER,
	 P_ORG_ID           NUMBER,
      P_MILCOUNT         NUMBER,
      P_STDATE           DATE,
      P_ENDDATE          DATE,
      O_RESULT     OUT   NUMBER,
      O_DATE       OUT   DATE
   );   --PM_1 , all patients
   PROCEDURE SP_CHK_PAT_RULE_ENR_ALL (
      P_STUDY            NUMBER,
	 P_ORG_ID           NUMBER,
      P_MILCOUNT         NUMBER,
      P_STDATE           DATE,
      P_ENDDATE          DATE,
      O_RESULT     OUT   NUMBER,
      O_DATE       OUT   DATE
   );   --PM_2 , all patients
   PROCEDURE SP_RULE_CHK_VIS_DONE_COMPLETE (
      P_STUDY            NUMBER,
	 P_ORG_ID           NUMBER,
      P_MILCOUNT         NUMBER,
      P_STDATE           DATE,
      P_ENDDATE          DATE,
      O_RESULT     OUT   NUMBER,
      O_DATE       OUT   DATE
   );   --PM_5 , all patients, in use
   PROCEDURE SP_RULE_CHK_VIS_DONE_ONE (
      P_STUDY            NUMBER,
	 P_ORG_ID           NUMBER,
      P_MILCOUNT         NUMBER,
      P_VISIT            NUMBER,
      P_STDATE           DATE,
      P_ENDDATE          DATE,
      O_RESULT     OUT   NUMBER,
      O_DATE       OUT   DATE
   );   --PM_4 , all patients IN USE, FINAL

   PROCEDURE SP_FORECAST_VIS_EVENT_DONE_ONE (
      P_STUDY            NUMBER,
	 P_ORG_ID           NUMBER,
      P_MILCOUNT         NUMBER,
      P_VISIT            NUMBER,
      P_STDATE           DATE,
      P_ENDDATE          DATE,
      O_RESULT     OUT   NUMBER,
      O_DATE       OUT   DATE
   );   --forecast PM_4, PM_5, all patients


END PKG_PM_RULE;
/


CREATE SYNONYM ESCH.PKG_PM_RULE FOR PKG_PM_RULE;


CREATE SYNONYM EPAT.PKG_PM_RULE FOR PKG_PM_RULE;


GRANT EXECUTE, DEBUG ON PKG_PM_RULE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_PM_RULE TO ESCH;

