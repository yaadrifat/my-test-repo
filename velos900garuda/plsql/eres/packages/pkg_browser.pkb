CREATE OR REPLACE PACKAGE BODY        "PKG_BROWSER" AS
PROCEDURE SP_INSERT_BROWSERSETTINGS(p_modnum NUMBER,p_modname  NUMBER, p_settings_keyword ARRAY_STRING, p_settings_value ARRAY_STRING)
	AS

i NUMBER;
v_cnt NUMBER;
v_modnum NUMBER;
v_smodule NUMBER;
v_skeyword VARCHAR2(500);
v_svalue VARCHAR2(500);
sql1 VARCHAR2(4000);
v_rowcount NUMBER;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'BROWSERSETTINGS', pLEVEL  => Plog.LDEBUG);
BEGIN

  Plog.DEBUG(pCTX,'modnum and modname' || p_modnum  || p_modname  );

v_cnt:=  p_settings_keyword.count();
i:=1;
Plog.DEBUG(pCTX,' setting array size is ' || v_cnt);

WHILE i<= v_cnt LOOP
  Plog.DEBUG(pCTX,'in loop ' || i);
 IF (LENGTH(p_settings_keyword(i)) > 0) THEN
      v_svalue := p_settings_value(i);
      v_skeyword := p_settings_keyword(i);
	  v_skeyword:= upper(v_skeyword);
	  v_smodule := p_modname;
  	  v_modnum := p_modnum;

	  sql1:= 'SELECT COUNT (*)  FROM er_settings WHERE settings_keyword ='''|| v_skeyword ||''' and settings_modname= '''|| v_smodule ||'''  and settings_modnum= '''|| v_modnum ||'''';

	  Plog.DEBUG(pCTX,'sql1=' || sql1 || 'settingsvalue=' || v_svalue || 'settings keyword='||v_skeyword || 'module=' || v_smodule || 'id=' || v_modnum);

	  EXECUTE IMMEDIATE sql1 INTO v_rowcount;

	  IF (v_rowcount=0) THEN
	    Plog.DEBUG(pCTX,'-inserting now-');
		  BEGIN
	        INSERT INTO ER_SETTINGS
	           (SETTINGS_PK, SETTINGS_MODNUM, SETTINGS_MODNAME, SETTINGS_KEYWORD,
			   SETTINGS_VALUE)
	         VALUES(seq_er_settings.NEXTVAL,v_modnum,v_smodule,v_skeyword,v_svalue);


	      END;

	  ELSIF(v_rowcount>0) THEN
	   Plog.DEBUG(pCTX,'-updating now-');
	       BEGIN
		   		UPDATE ER_SETTINGS
					SET
					SETTINGS_VALUE = v_svalue
					WHERE
					SETTINGS_MODNUM = v_modnum AND	SETTINGS_MODNAME = v_smodule AND SETTINGS_KEYWORD = v_skeyword;

		   END;
	   END IF;

  i := i+1;

 END IF;

END LOOP;


END;




END pkg_browser;
/


