create or replace
PACKAGE      "PKG_MSG_QUEUE"
AS
   MODULE_STUDY_STATUS   VARCHAR2 (200) := 'study_status';
   pCTX Plog.LOG_CTX
         := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL => Plog.LFATAL) ;

   PROCEDURE SP_POPULATE_MSG_QUEUE (
      P_TABLE_NAME          ER_MSG_QUEUE.TABLENAME%TYPE,
      P_TABLE_PK            ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT          ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION              ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK             ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_ROOT_TABLENAME      ER_MSG_QUEUE.ROOT_TABLENAME%TYPE,
      O_PK_MESSAGE   OUT  ER_MSG_QUEUE.PK_MESSAGE%TYPE
   );

   PROCEDURE SP_POPULATE_MSG_ADDINFO (
      P_FK_MESSAGE        ER_MSG_QUEUE.PK_MESSAGE%TYPE,
      P_ELEMENT_NAME      ER_MSG_ADDINFO.ELEMENT_NAME%TYPE,
      P_ELEMENT_VALUE     ER_MSG_ADDINFO.ELEMENT_VALUE%TYPE,
      P_COL_NAME          ER_MSG_ADDINFO.COL_NAME%TYPE
   );

   PROCEDURE SP_POPULATE_STUDY_STATUS_MSG (
      P_TABLE_PK              ER_MSG_QUEUE.TABLE_PK%TYPE,
      P_FK_ACCOUNT            ER_MSG_QUEUE.FK_ACCOUNT%TYPE,
      P_ACTION                ER_MSG_QUEUE.ACTION%TYPE,
      P_ROOT_PK               ER_MSG_QUEUE.ROOT_PK%TYPE,
      P_STUDY_STAT_CODEFK     ER_CODELST.PK_CODELST%TYPE
   );
END PKG_MSG_QUEUE;
/
