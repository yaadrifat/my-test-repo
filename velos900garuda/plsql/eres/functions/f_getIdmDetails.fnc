create or replace
function f_getIdmDetails(cordId number) return SYS_REFCURSOR
IS
c_IdmDetails SYS_REFCURSOR;
BEGIN
open C_IDMDETAILS for
SELECT
        PL.TESTDATE TESTDATEVAL,
        LTEST.LABTEST_NAME LABTESTNAME,
        F_CODELST_DESC(PL.FK_TEST_OUTCOME) FK_TEST_OUTCOME,
        PL.CMS_APPROVED_LAB CMS_APPROVED_LAB,
        PL.FDA_LICENSED_LAB FDA_LICENSED_LAB,
        PL.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER FLAG_FOR_LATER,
        PL.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY) ASSESSMENT_POSTEDBY,
        TO_CHAR(PL.ASSESSMENT_POSTEDON,'Mon DD, YYYY') ASSESSMENT_POSTEDON,
        F_GETUSER(PL.ASSESSMENT_CONSULTBY) ASSESSMENT_CONSULTBY,
        TO_CHAR(PL.ASSESSMENT_CONSULTON,'Mon DD, YYYY') ASSESSMENT_CONSULTON,
        PL.SOURCEVAL SOURCEVALS,
        LTEST.PK_LABTEST LAB_TEST,
        DECODE(PL.FK_TEST_OUTCOME,
                '0','YES',
                '1','No',
                null,'') lastval
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                'Mon DD, YYYY') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ASS.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=CORDID)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type='I'
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE='IOG'
            )AND PL.FK_TESTID=LTEST.PK_LABTEST
            ORDER BY LAB_TEST;
RETURN C_IDMDETAILS;
END;