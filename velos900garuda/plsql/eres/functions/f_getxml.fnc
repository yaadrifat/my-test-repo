CREATE OR REPLACE FUNCTION ERES."F_GETXML" (P_SXML CLOB)
RETURN CLOB
IS
  v_ora_version VARCHAR2(256) default null;
  v_xsml CLOB default null;
BEGIN 
  -- Use dbms_ only for newer Oracle
  select lower(banner) into v_ora_version from v$version where rownum <= 1;
  if (v_ora_version like '% 9i %' or v_ora_version like '%10.2.0.0%' 
        or v_ora_version like '%10.2.0.1%' or v_ora_version like '%10.2.0.2%' 
        or v_ora_version like '%10.2.0.3%') then
    v_xsml := Xmlgen.GETXML(P_SXML);
  else
    v_xsml := dbms_Xmlgen.GETXML(P_SXML);
  end if;
  return v_xsml;
END;
/

