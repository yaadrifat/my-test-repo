CREATE OR REPLACE FUNCTION        "F_FIND_STUDY_PI" (p_study_id NUMBER)
RETURN VARCHAR2
IS
  v_retval VARCHAR2(4000);
  v_user VARCHAR2(200);
BEGIN

FOR i IN (SELECT fk_user FROM ER_STUDYTEAM WHERE fk_study=p_study_id AND fk_codelst_tmrole =(SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp='role_prin') )
LOOP
	 SELECT usr_firstname || ' ' || usr_lastname INTO v_user FROM ER_USER WHERE PK_USER = i.fk_user;
	 v_retval := v_retval || v_user || '; ';
END LOOP;

IF INSTR(v_retval,';') >0 THEN
   v_retval := SUBSTR(v_retval,1,INSTR(v_retval,';')-1);
END IF;

RETURN v_retval ;
END ;
/


