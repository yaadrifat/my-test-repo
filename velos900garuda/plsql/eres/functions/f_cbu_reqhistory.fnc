create or replace
FUNCTION f_cbu_reqhistory(cordid NUMBER) RETURN SYS_REFCURSOR  IS
v_cbu_reqhist SYS_REFCURSOR;
BEGIN
    OPEN v_cbu_reqhist FOR   SELECT TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY') AS Requestdate,
    CASE WHEN f_codelst_desc(ord.order_type)='CT' THEN decode(ord.order_sample_at_lab,'Y','CT - Sample at Lab','N','CT - Ship Sample') WHEN f_codelst_desc(ord.order_type)!='CT' THEN f_codelst_desc(ord.order_type) END AS requestype,
    cbustat.CBU_STATUS_DESC RESOLUTION,
    reci.receipant_id    AS patientid,
    site.site_id AS transplantcenter,
    reci.pk_receipant   AS pk_receipant,
    ord.pk_order         AS pkorder,
    crd.pk_cord          AS pkcord,
    ordhdr.fk_site_id siteid
  FROM er_order ord
  LEFT OUTER JOIN er_order_header ordhdr
  ON (ord.fk_order_header=ordhdr.pk_order_header)
  LEFT OUTER JOIN cb_cord crd
  ON(ordhdr.order_entityid=crd.pk_cord)
  LEFT OUTER JOIN cb_cbu_status cbustat
  ON(ord.fk_order_resol_by_tc =cbustat.pk_cbu_status)
  LEFT OUTER JOIN
    (SELECT RECEIPANT_ID,
      TRANS_CENTER_ID,
      PK_RECEIPANT,
      FK_ORDER_ID
    FROM cb_receipant_info reci,
      ER_ORDER_RECEIPANT_INFO PRE
    WHERE PRE.FK_RECEIPANT=RECI.PK_RECEIPANT
    ) reci
  ON(reci.fk_order_id=ord.pk_order)
  LEFT OUTER JOIN er_site site ON (reci.TRANS_CENTER_ID=site.pk_site)
  WHERE crd.pk_cord  = cordid ORDER BY ordhdr.order_open_date DESC;

RETURN v_cbu_reqhist;
END;
/
