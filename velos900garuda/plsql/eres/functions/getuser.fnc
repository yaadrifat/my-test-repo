CREATE OR REPLACE FUNCTION        "GETUSER" (p_userid number ) return varchar2
as
USR varchar2(100) ;
Begin
 select to_char(pk_user)||', '||usr_lastname ||', ' ||usr_firstname
  into usr
  from er_user
 where pk_user = p_userid  ;
 return usr ;
 Exception WHEN NO_DATA_FOUND then
  usr := USER ;
   return usr ;
End ;
/


CREATE SYNONYM ESCH.GETUSER FOR GETUSER;


CREATE SYNONYM EPAT.GETUSER FOR GETUSER;


GRANT EXECUTE, DEBUG ON GETUSER TO EPAT;

GRANT EXECUTE, DEBUG ON GETUSER TO ESCH;

