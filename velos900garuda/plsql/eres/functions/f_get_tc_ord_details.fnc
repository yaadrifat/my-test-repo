create or replace
FUNCTION F_GET_TC_ORD_DETAILS(ORDERID NUMBER) RETURN SYS_REFCURSOR IS
SERVICEDATA SYS_REFCURSOR;
BEGIN
OPEN SERVICEDATA FOR 
SELECT ORDSER.SERVICE_CODE SERVICECODE,
  TO_CHAR(ORDSER.REQUESTED_DATE,'Mon DD, YYYY') REQ_REC_DT,
  TO_CHAR(ORDSER.RESULTS_REC_DATE,'Mon DD, YYYY') RES_REC_DT,
  TO_CHAR(ORDSER.CANCELLED_DATE,'Mon DD, YYYY') CAN_DT,
  CASE
    WHEN ORDSER.RESULTS_REC_FLAG IS NOT NULL
    AND (ORDSER.RESULTS_REC_FLAG  ='Y'
    OR ORDSER.RESULTS_REC_FLAG    ='1')
    THEN '1'
    WHEN ORDSER.CANCELLED_FLAG IS NOT NULL
    AND (ORDSER.CANCELLED_FLAG  ='Y'
    OR ORDSER.CANCELLED_FLAG    ='1')
    THEN '2'
  END FLAGVAL,
  ORDSER.FK_ORDER_ID,
  F_CODELST_DESC(ORDSER.SERVICE_STATUS) STATUS
FROM ER_ORDER_SERVICES ORDSER
WHERE ORDSER.FK_ORDER_ID=ORDERID
ORDER BY ORDSER.RESULTS_REC_DATE;

RETURN SERVICEDATA;
END;
/
