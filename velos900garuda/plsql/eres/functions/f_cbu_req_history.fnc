create or replace
function f_cbu_req_history(cordid number,orderid number) return SYS_REFCURSOR
IS
v_cord_req_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_REQ_HTY FOR
                        SELECT *FROM(SELECT
                        TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY')AS DATEVAL,
                        STATUS.CBU_STATUS_DESC DESCRIPTION1,
                        CASE
                              WHEN STAT.CREATOR=0
                              THEN 'System'
                              WHEN STAT.CREATOR!=0
                              THEN F_GETUSER(STAT.CREATOR)
                        END CREATOR,
                        NVL(F_CODELST_DESC(ORD1.ORDER_TYPE),'-') ORDERTYPE,
                        STAT.STATUS_TYPE_CODE TYPECODE,
                        TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                        F_CODELST_DESC(STAT.FK_STATUS_VALUE) DESCRIPTION2,
                        STAT.STATUS_DATE
                        FROM
                        CB_ENTITY_STATUS STAT
                        LEFT OUTER JOIN
                        CB_CBU_STATUS STATUS ON(STAT.FK_STATUS_VALUE=STATUS.PK_CBU_STATUS)
                        LEFT OUTER JOIN ER_ORDER ORD1 ON(STAT.ENTITY_ID=ORD1.PK_ORDER)
                        WHERE
                        (STAT.ENTITY_ID=CORDID OR STAT.ENTITY_ID =ORDERID) AND (STAT.STATUS_TYPE_CODE='order_status' OR STAT.STATUS_TYPE_CODE='cord_status' OR STAT.STATUS_TYPE_CODE='Cord_Nmdp_Status' )
                        UNION ALL
                        SELECT
                        TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY') DATEVAL,
                        FRM.FORMS_DESC DESC1,
                        F_GETUSER(FRMVR.CREATOR) CREA,
                        '-',
                        'FORMS' FRMNAME,
                        TO_CHAR(sysdate,'Mon DD, YYYY') systemdate,
                        '' desc7,
                        FRMVR.CREATED_ON
                        FROM CB_FORM_VERSION FRMVR
                        LEFT OUTER JOIN CB_FORMS FRM ON (FRMVR.FK_FORM =FRM.PK_FORM)
                        WHERE FRMVR.ENTITY_ID  =CORDID AND FRMVR.CREATED_ON  IS NOT NULL AND FRMVR.ENTITY_TYPE IN (SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='test_source' AND C.CODELST_SUBTYP='maternal')
                        )ORDER BY 8 DESC;
RETURN V_CORD_REQ_HTY;
END;
/