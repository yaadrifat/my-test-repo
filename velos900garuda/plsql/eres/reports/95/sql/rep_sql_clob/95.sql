SELECT (SELECT study_number FROM ER_STUDY WHERE pk_study in (:studyId)) AS study_number, year1,DECODE(month1,'01','January','02','February','03','March','04','April','05','May','06','June','07','July','08','August','09','September','10','October','11','November','December') AS month1,
       F_Get_Patevesch(sunday,':patientId',:studyId) AS sunday,
       F_Get_Patevesch(monday,':patientId',:studyId) AS monday,
       F_Get_Patevesch(tuesday,':patientId',:studyId) AS tuesday,
       F_Get_Patevesch(wednesday,':patientId',:studyId) AS wednesday,
       F_Get_Patevesch(thursday,':patientId',:studyId) AS thursday,
       F_Get_Patevesch(friday,':patientId',:studyId) AS friday,
       F_Get_Patevesch(saturday,':patientId',:studyId) AS saturday FROM (
SELECT year1,month1,week1,MAX(sunday) AS SUNDAY, MAX(monday) AS MONDAY, MAX(tuesday) AS TUESDAY, MAX(wednesday) AS WEDNESDAY, MAX(thursday) AS THURSDAY, MAX(friday) AS FRIDAY, MAX(saturday) AS SATURDAY FROM (
SELECT year1,month1,week1,date1, TO_CHAR(date1,'d'),DECODE(TO_CHAR(date1,'d'),1,date1,'') AS Sunday,
                              DECODE(TO_CHAR(date1,'d'),2,date1,NULL) AS Monday,
                              DECODE(TO_CHAR(date1,'d'),3,date1,NULL) AS Tuesday,
                              DECODE(TO_CHAR(date1,'d'),4,date1,NULL) AS Wednesday,
                              DECODE(TO_CHAR(date1,'d'),5,date1,NULL) AS Thursday,
                              DECODE(TO_CHAR(date1,'d'),6,date1,NULL) AS Friday,
                              DECODE(TO_CHAR(date1,'d'),7,date1,NULL) AS Saturday
                           FROM (
   SELECT TO_CHAR(dt,'yyyy') AS year1, TO_CHAR(dt,'MM') AS month1, TO_CHAR(dt+1,'IW') AS week1 , dt AS date1
   FROM
           (SELECT TO_DATE(TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)) + ROWNUM - 1 AS dt
         FROM ALL_OBJECTS
         WHERE ROWNUM <= TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) - TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) + 1)
         ) GROUP BY year1,week1,month1, date1 order by year1,week1,month1, date1
) GROUP BY year1,month1,week1 ORDER BY year1,month1,week1
)
