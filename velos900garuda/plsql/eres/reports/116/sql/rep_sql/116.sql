SELECT (SELECT study_number FROM er_study WHERE pk_study = fk_study) AS study_number,
(SELECT study_title FROM ER_STUDY WHERE pk_study = fk_study) AS study_title,
TO_CHAR((SELECT study_actualdt FROM ER_STUDY WHERE pk_study = fk_study),PKG_DATEUTIL.F_GET_DATEFORMAT) AS study_startdate,
site_name, race,SUM(race_count) AS race_count,
SUM(DECODE(gender,'female',race_count,0)) AS female_count,
SUM(DECODE(gender,'male',race_count,0)) AS male_count,
SUM(DECODE(gender,'other',race_count,0)) AS other_count,
SUM(DECODE(gender,'Unknown',race_count,0)) AS unknown_count,
SUM(DECODE(gender,'zzzz',race_count,0)) AS gen_ne_count,
SUM(DECODE(ethnicity,'hispanic',race_count,0)) AS hispanic_count,
SUM(DECODE(ethnicity,'nonhispanic',race_count,0)) AS nonhispanic_count,
SUM(DECODE(ethnicity,'notreported',race_count,0)) AS notreported_count,
SUM(DECODE(ethnicity,'Unknown',race_count,0)) AS eunknown_count,
SUM(DECODE(ethnicity,'zzzz',race_count,0)) AS eth_ne_count
FROM (
SELECT fk_study,
(SELECT site_name FROM ER_SITE WHERE pk_site = fk_site) AS site_name,
DECODE(fk_codelst_race,NULL,'zzzz',(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_race)) AS race,
DECODE(fk_codelst_gender,NULL,'zzzz',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_gender)) AS gender,
DECODE(fk_codelst_ethnicity,NULL,'zzzz',(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity)) AS ethnicity,
COUNT(fk_codelst_race || '*' || fk_codelst_gender || '*' || fk_codelst_ethnicity) AS race_count
FROM EPAT.person, ER_PATPROT
WHERE pk_person = fk_per AND
fk_study = ~1 AND
patprot_enroldt BETWEEN '~2' AND '~3' AND
patprot_stat = 1
GROUP BY
GROUPING SETS ( (fk_study,fk_site,fk_codelst_race,fk_codelst_gender,fk_codelst_ethnicity))
ORDER BY fk_site, fk_codelst_race)
GROUP BY fk_study,site_name, race
ORDER BY site_name,race