select
a.STUDY_NUMBER           ,
a.STUDY_TITLE            ,
a.STUDY_ACTUALDT,
a.PROTOCOLID   ,
a.PROTOCOL_NAME          ,
a.PAT_STUDYID            ,
a.PERSON_NAME            ,
a.EVENT_NAME             ,
a.ENROLDT ,
a.MONTH                  ,

a.st    ,
a.EVENT_STATUS,
a.FUZZY_PERIOD,
a.site_name,
a.DONE_DATE,
a.before_date ,
a.after_date ,
case when  a.done_date is not null and a.done_date >= a.before_date and a.done_date  <= a.after_date then 'NO'
when a.done_date is not null and a.done_date < a.before_date or a.done_date  > a.after_date then 'YES' end as out_ofrange
from
(select
STUDY_NUMBER           ,
STUDY_TITLE            ,
to_char(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
PROTOCOLID   ,
PROTOCOL_NAME          ,
PAT_STUDYID            ,
PERSON_NAME            ,
EVENT_NAME             ,
to_char(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) ENROLDT ,
MONTH                  ,

to_char(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)    st     ,
EVENT_STATUS,
FUZZY_PERIOD,
er_site.site_name,
(select to_char(min(nvl(a.eventstat_dt,sysdate)),PKG_DATEUTIL.F_GET_DATEFORMAT)  from ESCH.sch_eventstat a
where a.fk_event=erv_patsch.event_id
and eventstat=(select pk_codelst from ESCH.sch_codelst
where codelst_type='eventstatus'
and codelst_subtyp = 'ev_done'))  DONE_DATE ,
substr(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,1,  (INSTR(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,',',1)-1)) after_date,
substr(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,(INSTR(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD))  ,',',1)+1),length(f_check_outofrange(EVENT_SCHDATE,to_number(FUZZY_PERIOD)))) before_date
from   erv_patsch, er_site
where  fk_study = ~1  and
fk_site = ~2 and
fk_site = pk_site and
EVENT_SCHDATE between '~3' and '~4' ) a