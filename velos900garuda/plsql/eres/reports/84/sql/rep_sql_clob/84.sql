
SELECT PTSCH_STUDY_NUM,PTSCH_PAT_ID,PTSCH_PAT_STUDY_ID,PTSCH_CALENDAR,PTSCH_VISIT,PTSCH_EVENT,
to_char(PTSCH_ACTSCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as PTSCH_ACTSCH_DATE,PTSCH_VISIT_WIN,FUZZY_DURATION_BEF,FUZZY_DURATION_AFT,EVENT_STATUS,
to_char(EVENTSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as EVENTSTAT_DATE ,EVENTSTAT_NOTES,
CASE WHEN eventstat_date > fuzzy_duration_aft OR eventstat_date < fuzzy_duration_bef THEN 'Yes' ELSE 'No' END AS out_of_range
FROM (
SELECT        (SELECT STUDY_NUMBER FROM ERES.ER_STUDY
        WHERE PK_STUDY = c.FK_STUDY) PTSCH_STUDY_NUM,
PERSON_CODE PTSCH_PAT_ID,
       PATPROT_PATSTDID PTSCH_PAT_STUDY_ID,
       (SELECT NAME FROM EVENT_ASSOC
        WHERE EVENT_ASSOC.EVENT_ID = a.SESSION_ID) PTSCH_CALENDAR,
       (SELECT VISIT_NAME FROM SCH_PROTOCOL_VISIT
        WHERE PK_PROTOCOL_VISIT = a.FK_VISIT) PTSCH_VISIT,
       a.DESCRIPTION PTSCH_EVENT,
       ACTUAL_SCHDATE PTSCH_ACTSCH_DATE,
        (SELECT  NVL(FUZZY_PERIOD,0)  || ' ' ||  NVL(F_Get_Durunit(EVENT_DURATIONBEFORE),'days') || ' Before / ' ||
        NVL(EVENT_FUZZYAFTER,0) || ' ' ||  NVL(F_Get_Durunit(EVENT_DURATIONAFTER),'days') || ' After'
       FROM EVENT_ASSOC   WHERE EVENT_ASSOC.EVENT_ID = a.fk_assoc) PTSCH_VISIT_WIN,
ACTUAL_SCHDATE - DECODE(event_durationbefore,'D',( fuzzy_period * 1 ),'H',1,'W',(fuzzy_period * 7 ),'M',(fuzzy_period *30) ) AS fuzzy_duration_bef,
ACTUAL_SCHDATE + DECODE(event_durationafter,'D',(event_fuzzyafter * 1),'H',1,'W',(event_fuzzyafter * 7),'M',(event_fuzzyafter *30 ))  AS fuzzy_duration_aft,
CASE WHEN ACTUAL_SCHDATE < SYSDATE AND (SELECT codelst_subtyp FROM sch_codelst WHERE pk_codelst = isconfirmed) = 'ev_notdone'
             THEN 'Past Scheduled Date' ELSE (SELECT CODELST_DESC FROM SCH_CODELST
                WHERE ISCONFIRMED = PK_CODELST) END AS EVENT_STATUS,
     EVENT_EXEON   EVENTSTAT_DATE,
   a.notes EVENTSTAT_NOTES
FROM SCH_EVENTS1 a,person b,ER_PATPROT c, EVENT_ASSOC d
WHERE
pk_person = c.fk_per AND
pk_patprot = fk_patprot AND
d.EVENT_ID = a.fk_assoc AND
c.fk_study IN (:studyId) AND
fk_site IN (:orgId) AND
ACTUAL_SCHDATE BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
)
