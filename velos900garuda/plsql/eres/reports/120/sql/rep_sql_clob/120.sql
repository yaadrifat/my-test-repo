SELECT * FROM (
SELECT DECODE(table_name,'ER_STUDY','Study Summary','ER_STUDYSTAT','Study Status', 'ER_STUDYVER','Study Version',
'ER_STUDYID','More Study Details', 'ER_PATSTUDYSTAT','Patient Study Status') AS MODULE,
DECODE(table_name,'ER_STUDY',(SELECT study_number FROM ER_STUDY WHERE rid = a.rid),
'ER_STUDYSTAT',(SELECT study_number FROM ER_STUDYSTAT x,ER_STUDY WHERE x.rid = a.rid AND pk_study = fk_study),
'ER_STUDYVER',(SELECT study_number FROM ER_STUDYVER x, ER_STUDY WHERE x.rid = a.rid AND pk_study = fk_study),
'ER_STUDYID',(SELECT study_number FROM ER_STUDYID x,ER_STUDY WHERE x.rid = a.rid AND pk_study = fk_study),
'ER_PATSTUDYSTAT',(SELECT study_number FROM ER_PATSTUDYSTAT x,ER_STUDY WHERE x.rid=a.rid AND pk_study = fk_study)) AS study_number,
DECODE(table_name,'ER_PATSTUDYSTAT',(SELECT per_code FROM ER_PATSTUDYSTAT x,ER_PER WHERE x.rid=a.rid AND pk_per = fk_per)) AS patient_id,
to_chAR(TIMESTAMP,PKG_DATEUTIL.F_GET_DATEtimeFORMAT) TIMESTAMP,user_name, column_name,replace(old_value,chr(11),'') as old_value,replace(new_value,chr(11),'') as new_value,
(Select fk_account FROM ER_STUDY WHERE rid = a.rid ) as account_study
FROM AUDIT_ROW a,AUDIT_COLUMN b
WHERE  a.raid = b.raid AND table_name IN
('ER_STUDY','ER_STUDYSTAT','ER_STUDYTEAM','ER_STUDYVER','STUDYID','ER_PATSTUDYSTAT','ER_STATUS_HISTORY') AND
TIMESTAMP BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
column_name NOT IN ('LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','PATSTUDYSTAT_ENDT','STUDY_TEAM_RIGHTS','STUDYSTAT_ENDT','STUDY_PUBCOLLST','STATUS_END_DATE') AND
user_name <> 'ERES')
WHERE (study_number IS NOT NULL OR patient_id IS NOT NULL) AND ( account_study = :sessAccId)
UNION ALL
SELECT * FROM ( SELECT DECODE(table_name,'PERSON','Patient Demographics') AS MODULE, '' AS study_number,
DECODE(table_name,'PERSON',(SELECT person_code FROM person x WHERE x.rid=a.rid and rownum = 1)) AS patient_id,
to_chAR(TIMESTAMP,PKG_DATEUTIL.F_GET_DATEtimeFORMAT) TIMESTAMP,user_name, column_name,replace(old_value,chr(11),'') as old_value,replace(new_value,chr(11),'') as new_value,
(SELECT fk_account FROM person x WHERE x.rid=a.rid and rownum = 1) as account_patient
FROM EPAT.AUDIT_ROW a,EPAT.AUDIT_COLUMN b
WHERE  a.raid = b.raid AND table_name IN ('PERSON') AND
TIMESTAMP BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
column_name NOT IN ('LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','FK_ACCOUNT') AND user_name <> 'EPAT')
WHERE patient_id IS NOT NULL  AND ( account_patient = :sessAccId)
UNION ALL
Select * FROM (SELECT 'Adverse Events' AS MODULE,
(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study) AS study_number,
(SELECT per_code FROM ER_PER WHERE pk_per = fk_per) AS patient_id,
to_chAR(TIMESTAMP,PKG_DATEUTIL.F_GET_DATEtimeFORMAT) TIMESTAMP,
Decode(pkg_util.f_to_number(user_name), 0, user_name,(SELECT usr_lastname  || ', ' || usr_firstname FROM ER_USER WHERE pk_user = user_name)) AS user_name,
column_name,
CASE
    WHEN (column_name = 'AE_OUTTYPE')
       THEN pkg_util.f_decipher_codelist(old_value,'outcome','esch.sch_codelst')
    WHEN (column_name = 'AE_ADDINFO')
       THEN pkg_util.f_decipher_codelist (old_value,'adve_info','esch.sch_codelst')
    WHEN (column_name = 'AE_REPORTEDBY')
       THEN (SELECT usr_lastname || ', ' || usr_firstname FROM er_user WHERE pk_user = old_value)
    WHEN (column_name = 'AE_ENTERBY')
       THEN (SELECT usr_lastname || ', ' || usr_firstname FROM er_user WHERE pk_user = old_value)
	WHEN (column_name = 'AE_RELATIONSHIP')
       THEN (SELECT codelst_desc FROM esch.sch_codelst WHERE pk_codelst = old_value)
    WHEN (column_name='AE_RECOVERY_DESC')
       THEN (SELECT codelst_desc FROM esch.sch_codelst WHERE pk_codelst = old_value)
    WHEN (column_name='AE_LOGGEDDATE')
       THEN old_value
    WHEN (column_name='AE_DISCVRY')
       THEN old_value
    ELSE old_value
END AS old_value,
CASE
    WHEN (column_name = 'AE_OUTTYPE')
       THEN pkg_util.f_decipher_codelist(new_value,'outcome','esch.sch_codelst')
    WHEN (column_name = 'AE_ADDINFO')
       THEN pkg_util.f_decipher_codelist (new_value,'adve_info','esch.sch_codelst ')
    WHEN (column_name = 'AE_REPORTEDBY')
       THEN (SELECT usr_lastname || ', ' || usr_firstname FROM er_user WHERE pk_user = new_value)
     WHEN (column_name = 'AE_ENTERBY')
       THEN (SELECT usr_lastname || ', ' || usr_firstname FROM er_user WHERE pk_user = new_value)
    WHEN (column_name = 'AE_RELATIONSHIP')
       THEN (SELECT codelst_desc FROM esch.sch_codelst WHERE pk_codelst = new_value)
     WHEN (column_name='AE_RECOVERY_DESC')
       THEN ( SELECT codelst_desc FROM esch.sch_codelst WHERE pk_codelst = new_value)
	WHEN (column_name='AE_LOGGEDDATE')
       THEN new_value
	WHEN (column_name='AE_DISCVRY')
       THEN new_value
    ELSE new_value
END AS new_value,
(SELECT fk_account FROM ER_STUDY WHERE pk_study = fk_study) as account_adverse
FROM ESCH.AUDIT_ROW a,ESCH.AUDIT_COLUMN b, sch_adverseve c
WHERE a.raid = b.raid AND a.rid = c.rid AND table_name = 'SCH_ADVERSEVE' AND
TIMESTAMP BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
column_name NOT IN ('LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','FK_ACCOUNT') AND
user_name <> 'ESCH' ) WHERE account_adverse = :sessAccId
ORDER BY 4 DESC ,5
