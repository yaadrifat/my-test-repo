SELECT study_number,
study_title,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'',study_otherprinv),(SELECT usr_lastname || ',' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv) || DECODE(study_otherprinv,NULL,'','; ' || study_otherprinv)) AS PI,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_tarea) AS tarea,
f_getdis_site(study_disease_site) AS disease_site,
f_getlocal_samplesize(pk_study) AS study_samplsize,
study_nsamplsize,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_phase) AS phase,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_restype) AS restype,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_scope) AS SCOPE,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_type) AS study_type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_blind) AS blind,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.fk_codelst_random) AS RANDOM,
study_sponsor,
f_getmore_studydetails(pk_study) AS study_details,
TO_CHAR((SELECT MIN(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp ='active' AND codelst_type='studystat')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS open_for_enroll_dt,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'active_cls' AND codelst_type='studystat')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS closed_to_accr_dt,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'prmnt_cls' AND codelst_type='studystat')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS retired_dt,
TO_CHAR((SELECT MAX(studystat_date) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'app_CHR' AND codelst_type='studystat')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS chrstat_validfrm_dt,
TO_CHAR((SELECT MAX(studystat_validt) FROM ER_STUDYSTAT WHERE fk_study = a.pk_study AND fk_codelst_studystat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp ='app_CHR' AND codelst_type='studystat')),PKG_DATEUTIL.F_GET_DATEFORMAT) AS chrstat_validuntil_dt,
(SELECT COUNT(*) FROM ER_PATPROT WHERE fk_study = a.pk_study AND patprot_stat = 1 AND PATPROT_ENROLDT IS NOT NULL) AS patients_accrued
FROM ER_STUDY a
WHERE fk_account = ~1 AND
EXISTS (SELECT 9 FROM ER_STUDYTEAM WHERE fk_study = pk_study AND fk_user = ~2)
ORDER BY study_number
