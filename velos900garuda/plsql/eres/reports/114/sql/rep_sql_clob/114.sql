SELECT disease_site, total,
(SELECT COUNT(*) FROM EPAT.person a, EPAT.pat_perid, ER_SITE b  WHERE a.fk_account = :sessAccId AND pk_person = fk_per AND pk_site = fk_site AND site_stat = 'Y' AND
person_regdate BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND  TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND
perid_id = disease_site_subtyp) AS new_reg
FROM (
SELECT disease_site, disease_site_subtyp, SUM(cnt) AS total FROM (
(SELECT disease_site, disease_site_subtyp, COUNT(DISTINCT fk_per || '*' ||fk_study) AS CNT FROM (
SELECT DECODE(INSTR(study_disease_site,','),0,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = f_to_number(a.study_disease_site)),'MULTIPLE') AS disease_site,
DECODE(INSTR(study_disease_site,','),0,(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = f_to_number(a.study_disease_site)),'MULTIPLE') AS disease_site_subtyp,
b.fk_per,b.fk_study
FROM ER_STUDY a,ER_PATPROT b,ER_STUDYID c
WHERE fk_account = :sessAccId AND
pk_study = b.fk_study  AND pk_study = c.fk_study AND
fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_agent') AND studyId_id='Y'    AND
patprot_enroldt BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND  TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND fk_site_enrolling = (SELECT pk_site FROM ER_SITE WHERE fk_account = :sessAccId AND site_stat = 'Y')
) GROUP BY disease_site, disease_site_subtyp)
UNION
(SELECT codelst_desc AS disease_site,codelst_subtyp AS disease_site_subtyp,0 AS total FROM ER_CODELST WHERE  codelst_type ='disease_site')
) GROUP BY disease_site, disease_site_subtyp ORDER BY disease_site, disease_site_subtyp)