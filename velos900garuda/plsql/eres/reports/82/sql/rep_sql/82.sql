SELECT DISTINCT STUDY_NUMBER,
TO_CHAR(STUDYACTUAL_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDYACTUAL_DATE,
studyend_date, STUDY_TITLE, TA, data_mgr,
DECODE(study_prinv,NULL,DECODE(study_otherprinv,NULL,'',study_otherprinv),(SELECT usr_lastname || ', ' || usr_firstname FROM ER_USER WHERE pk_user = study_prinv) || DECODE(study_otherprinv,NULL,'','; ' || study_otherprinv)) AS PI
FROM erv_studyact
  WHERE fk_account = ~1
    AND STUDYSTAT_DATE <= '~3' AND
    NVL(studyend_date,SYSDATE) >= '~2'
  ORDER BY TA
