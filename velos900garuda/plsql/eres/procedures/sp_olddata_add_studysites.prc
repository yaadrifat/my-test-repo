CREATE OR REPLACE PROCEDURE        "SP_OLDDATA_ADD_STUDYSITES" 
 AS
   v_study_id NUMBER;
   v_site_id NUMBER;
    v_last_modified_by NUMBER;
   v_ip_add VARCHAR2(15);
   v_studysite NUMBER;

   BEGIN

      FOR i IN (SELECT  DISTINCT pk_study,fk_siteid,s.Last_MODIFIED_BY last_mod_by,s.ip_add ip  FROM er_user,er_study s WHERE pk_user  IN (SELECT DISTINCT fk_user  FROM er_studyteam WHERE fk_study=pk_study)
		AND  fk_siteid NOT IN(SELECT fk_site FROM er_studysites WHERE fk_study = pk_study)	ORDER BY pk_study)
     LOOP
   v_study_id := i.pk_study;
   v_site_id := i.fk_siteid;
   v_last_modified_by := i.last_mod_by;
   v_ip_add := i.ip;
   SELECT SEQ_ER_STUDYSITES.NEXTVAL
   INTO v_studysite
   FROM dual;

   INSERT INTO ER_STUDYSITES
    (PK_STUDYSITES,
     FK_STUDY ,
     FK_SITE,
    CREATOR,
 CREATED_ON,
 IP_ADD)
     VALUES
   ( v_studysite,
    v_study_id,
    v_site_id,
   v_last_modified_by,
   SYSDATE,
   v_ip_add
   );

   END LOOP;
   COMMIT;
END sp_olddata_add_studysites;
/


CREATE SYNONYM ESCH.SP_OLDDATA_ADD_STUDYSITES FOR SP_OLDDATA_ADD_STUDYSITES;


CREATE SYNONYM EPAT.SP_OLDDATA_ADD_STUDYSITES FOR SP_OLDDATA_ADD_STUDYSITES;


GRANT EXECUTE, DEBUG ON SP_OLDDATA_ADD_STUDYSITES TO EPAT;

GRANT EXECUTE, DEBUG ON SP_OLDDATA_ADD_STUDYSITES TO ESCH;

