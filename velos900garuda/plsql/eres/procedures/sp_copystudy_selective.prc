CREATE OR REPLACE PROCEDURE        "SP_COPYSTUDY_SELECTIVE" (
  p_origstudy IN NUMBER,
  p_statflag IN NUMBER,
  p_teamflag IN NUMBER,
  p_verarr IN ARRAY_STRING,
  p_calarr IN ARRAY_STRING,
  p_newstudynum IN VARCHAR2,
  p_newstudydm IN NUMBER,
  p_newstudytitle IN VARCHAR2,
  p_user IN NUMBER,
  p_ip IN VARCHAR2,
  o_success OUT NUMBER)
  AS
   v_newstudy NUMBER;
   v_rights                      VARCHAR2(25);
   --added by salil on 24 sep 2003
   v_oldrights                      VARCHAR2(25);
   v_role  NUMBER;
   v_oldpkstudyver NUMBER;
   v_newpkstudyver NUMBER;
   v_newstudysite NUMBER;
   v_orig_studysite NUMBER;
   v_dt  CHAR(12);
   v_oldcal NUMBER;
   v_newcal NUMBER;
   v_new_ver NUMBER;
   cnt_datamng NUMBER;
   v_creator_site_cnt NUMBER;
   v_creator_site NUMBER;
   v_newstudyid NUMBER;--JM
    BEGIN
   /****************************************************************************************************
   ** Procedure to create a copy of the study. Will be used for enhancements required for April'03 : multi-org requirements
   ** Parameter Description
   ** p_origstudy : the Study that is tobe copied
   ** p_statflag : flag if the study status will be copied - 0 : No, 1 : Yes
   ** p_teamflag : flag if the study team will be copied - 0 : No, 1 : Yes
   ** p_verarr : Array of version ids that will be copied
   ** p_calarr : Array of calendar ids that will be copied
   ** p_newstudynum : study number for new study
   ** p_newstudydm :data manager for new study
   ** p_newstudytitle : title for new study
   ** p_user : user who created the record
   ** p_ip : ip address of the user
   ** o_success : Out parameter to indicate success, 0 : successful, 1 : unsuccessful
   **
   ** Author: Sonia Sahni , 5th May 2003
   ** Modified By: Salil, 1st Oct 2003 to copy only study team members and not super users
   ** Modified By: Sam, 8/5/04  fix for bug #1551, when a study is copied, the calendar status must be WIP nd creator must be set. Also added last_modified_by and ip_add
   ** Modified By: Sonia, 08/06/04  copy new study columns : STUDY_DIVISION , STUDY_INVIND_FLAG , STUDY_INVIND_NUMBER
   ** Modified by Sonia Sahni 9th Aug 04 to add a default version status history record
   ** Modified by Sonia Sahni 30th Aug 04 : while adding default version status history record, the status date
   ** used to have a 'time' part. Howver from application we do not send any time part. So truncated the time
   ** part
   ** JM: 04-10-06 modified the query to rectify copy study problem, columns added are STUDY_OTHERPRINV, STUDY_MAJ_AUTH,
   ** STUDY_DISEASE_SITE,STUDY_ICDCODE1, STUDY_ICDCODE2, STUDY_NSAMPLSIZE, STUDY_ASSOC
   *****************************************************************************************************
   */
   -- copy study summary
    SELECT seq_er_study.NEXTVAL INTO v_newstudy   FROM dual;
    SELECT TO_CHAR (SYSDATE, 'DDMMYYHHMISS') INTO v_dt FROM dual;
    --check if the new study number is unique for the account, return -2 if not unique
        BEGIN
           INSERT INTO ER_STUDY (pk_study, study_sponsor, fk_account, study_contact, study_pubflag, study_title,
                        study_obj_clob,study_sum_clob, study_prodname, study_samplsize, study_dur,
                        study_durunit, study_estbegindt, study_actualdt, study_prinv,
			STUDY_OTHERPRINV, STUDY_MAJ_AUTH, STUDY_DISEASE_SITE,STUDY_ICDCODE1, STUDY_ICDCODE2, STUDY_NSAMPLSIZE, STUDY_ASSOC,
                        study_partcntr, study_keywrds, study_pubcollst, study_parentid,
                        fk_author, fk_codelst_tarea, fk_codelst_phase, fk_codelst_blind,
                        fk_codelst_random, creator, study_current, last_modified_by, last_modified_date,
                        created_on,study_number,fk_codelst_restype,FK_CODELST_CURRENCY,study_end_date,ip_add,
        study_info,fk_codelst_type,study_advlkp_ver,STUDY_DIVISION ,
                      STUDY_INVIND_FLAG , STUDY_INVIND_NUMBER,STUDY_COORDINATOR
                     )
           SELECT   v_newstudy, study_sponsor,fk_account,study_contact,'N',
                    study_title,study_obj_clob,study_sum_clob,study_prodname,study_samplsize,
                    study_dur,study_durunit,study_estbegindt,study_actualdt,study_prinv,
		    STUDY_OTHERPRINV, STUDY_MAJ_AUTH, STUDY_DISEASE_SITE,STUDY_ICDCODE1, STUDY_ICDCODE2, STUDY_NSAMPLSIZE, STUDY_ASSOC,
		    study_partcntr,study_keywrds,'00000',p_origstudy,
                    p_newstudydm,fk_codelst_tarea,fk_codelst_phase,fk_codelst_blind,
                    fk_codelst_random,p_user,study_current,NULL,NULL,SYSDATE,p_newstudynum ,
                    fk_codelst_restype, FK_CODELST_CURRENCY,study_end_date,p_ip,study_info,
    fk_codelst_type,study_advlkp_ver,STUDY_DIVISION ,
                      STUDY_INVIND_FLAG , STUDY_INVIND_NUMBER,STUDY_COORDINATOR
           FROM ER_STUDY
           WHERE pk_study = p_origstudy;
   IF LENGTH(trim(p_newstudytitle)) > 0 THEN
      UPDATE ER_STUDY
         SET study_title = p_newstudytitle
     WHERE pk_study = v_newstudy;
   END IF;
   EXCEPTION WHEN NO_DATA_FOUND THEN
         o_success := -1;
         END ;
   ---
--JM: added 04-10-06 copy more study details
	FOR i IN(SELECT PK_STUDYID FROM ER_STUDYID WHERE fk_study = p_origstudy)
	LOOP

	SELECT seq_er_studyid.NEXTVAL INTO v_newstudyid FROM dual;
	  -- copy more study details

	INSERT INTO ER_STUDYID ( PK_STUDYID,FK_STUDY,FK_CODELST_IDTYPE,STUDYID_ID,CREATOR,CREATED_ON,IP_ADD )

	SELECT v_newstudyid, v_newstudy,FK_CODELST_IDTYPE,STUDYID_ID,p_user,SYSDATE,p_ip
	FROM  ER_STUDYID WHERE PK_STUDYID=i.PK_STUDYID;
	END LOOP;


  /*INSERT INTO ER_STUDYSITES(PK_STUDYSITES, FK_STUDY, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE, CREATOR, CREATED_ON, IP_ADD)
  SELECT seq_er_studysites.NEXTVAL, v_newstudy, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE,p_user, SYSDATE,p_ip
  FROM ER_STUDYSITES WHERE fk_study = p_origstudy*/
  INSERT INTO ER_STUDYSITES(PK_STUDYSITES, FK_STUDY, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE, CREATOR, CREATED_ON, IP_ADD)
  SELECT  seq_er_studysites.NEXTVAL, v_newstudy,
  a,b,c,d,e,
  p_user, SYSDATE,p_ip
  FROM (
  SELECT  FK_SITE a, FK_CODELST_STUDYSITETYPE b, STUDYSITE_LSAMPLESIZE c, STUDYSITE_PUBLIC d, STUDYSITE_REPINCLUDE e
  FROM ER_STUDYSITES WHERE fk_study = p_origstudy
  UNION
  SELECT  FK_SITEID, NULL, NULL, NULL, NULL
  FROM ER_USER WHERE pk_user  = p_newstudydm
  AND fk_siteid NOT IN (SELECT fk_site FROM ER_STUDYSITES WHERE fk_study = p_origstudy)
  );
  -- copy access rights for user
  /*INSERT INTO ER_STUDY_SITE_RIGHTS(PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,
  USER_STUDY_SITE_RIGHTS,CREATOR, CREATED_ON, IP_ADD)
  SELECT seq_er_studysite_rights.NEXTVAL,fk_site, v_newstudy, FK_USER,
   USER_STUDY_SITE_RIGHTS,p_user, SYSDATE,p_ip
   FROM ER_STUDY_SITE_RIGHTS
   WHERE fk_study = p_origstudy
   */
  INSERT INTO ER_STUDY_SITE_RIGHTS(PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,
  USER_STUDY_SITE_RIGHTS,CREATOR, CREATED_ON, IP_ADD)
  SELECT seq_er_studysite_rights.NEXTVAL,a,v_newstudy,b,c,p_user, SYSDATE,p_ip
  FROM(
  SELECT fk_site a, FK_USER b, USER_STUDY_SITE_RIGHTS c
   FROM ER_STUDY_SITE_RIGHTS
   WHERE fk_study = p_origstudy
   UNION
   SELECT fk_site, FK_USER,
   CASE WHEN USERSITE_RIGHT > 0 THEN 1 ELSE 0 END CASE
   FROM ER_USERSITE
   WHERE fk_user =  p_newstudydm
   AND fk_user NOT IN(SELECT FK_USER
   FROM ER_STUDY_SITE_RIGHTS
   WHERE fk_study = p_origstudy )
   );
   FOR i IN(SELECT pk_studysites FROM ER_STUDYSITES WHERE fk_study = p_origstudy)
  LOOP
  -- copy study organizations
  SELECT seq_er_studysites.NEXTVAL INTO v_newstudysite FROM dual;
  -- copy study info
  INSERT INTO ER_STUDY_SITE_ADD_INFO(PK_STUDYSITES_ADDINFO,FK_STUDYSITES,FK_CODELST_SITEADDINFO,
  STUDYSITE_DATA,CREATOR,CREATED_ON,IP_ADD)
  SELECT seq_er_studysites_addinfo.NEXTVAL,v_newstudysite, FK_CODELST_SITEADDINFO,
  STUDYSITE_DATA,p_user, SYSDATE,p_ip
  FROM ER_STUDY_SITE_ADD_INFO
  WHERE  FK_STUDYSITES = i.pk_studysites;
  -- copy appendix info
  INSERT INTO ER_STUDYSITES_APNDX(PK_STUDYSITES_APNDX,FK_STUDYSITES,APNDX_TYPE,APNDX_NAME,APNDX_DESCRIPTION,
  APNDX_FILE,APNDX_FILESIZE,CREATOR,CREATED_ON,IP_ADD)
  SELECT seq_er_studysites_apndx.NEXTVAL, v_newstudysite, APNDX_TYPE,APNDX_NAME,APNDX_DESCRIPTION,
  APNDX_FILE,APNDX_FILESIZE,p_user, SYSDATE,p_ip
  FROM ER_STUDYSITES_APNDX
  WHERE FK_STUDYSITES = i.pk_studysites;
 END LOOP;
    -- copy study status records if p_statflag = 1
     IF p_statflag = 1 THEN
         DELETE FROM ER_STUDYSTAT WHERE fk_study = v_newstudy;
--JM: 04-10-06 modified: STUDYSTAT_VALIDT column added in the ql
         INSERT INTO ER_STUDYSTAT(PK_STUDYSTAT ,FK_USER_DOCBY  , FK_CODELST_STUDYSTAT ,FK_STUDY,
         STUDYSTAT_DATE ,STUDYSTAT_HSPN  ,STUDYSTAT_NOTE  ,STUDYSTAT_ENDT, STUDYSTAT_VALIDT,
         FK_CODELST_APRSTAT  , CREATOR   ,CREATED_ON  ,FK_CODELST_APRNO ,IP_ADD , fk_site )
     SELECT seq_er_studystat.NEXTVAL, FK_USER_DOCBY  , FK_CODELST_STUDYSTAT ,v_newstudy,
         STUDYSTAT_DATE ,STUDYSTAT_HSPN  ,STUDYSTAT_NOTE  ,STUDYSTAT_ENDT, STUDYSTAT_VALIDT,
         FK_CODELST_APRSTAT  , p_user, SYSDATE,FK_CODELST_APRNO ,p_ip, fk_site
     FROM ER_STUDYSTAT
     WHERE fk_study = p_origstudy ;
  UPDATE ER_STUDY a
     SET a.study_actualdt = (SELECT b.study_actualdt FROM
                             ER_STUDY b WHERE b.pk_study = p_origstudy ) ,
         a.study_end_date = (SELECT b.study_end_date FROM
                             ER_STUDY b WHERE b.pk_study = p_origstudy )
         WHERE a.pk_study = v_newstudy;
    ELSE
         UPDATE ER_STUDY
     SET study_actualdt = NULL, study_end_date = NULL
     WHERE pk_study = v_newstudy;
    END IF;
    --copy study team if  p_teamflag = 1
    -- create rights string - all rights are given to the study data manager
       FOR i IN ( SELECT CTRL_VALUE
        FROM ER_CTRLTAB  WHERE CTRL_KEY ='study_rights'
        ORDER BY CTRL_SEQ )
        LOOP
             v_rights := v_rights || '7';
    END LOOP;
    --insert into t(c3,c4) values ('intial',v_rights);
    IF  p_teamflag = 1 THEN -- copy the existing team
        INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
                                   FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD,
           study_team_usr_type )
        SELECT seq_er_studyteam.NEXTVAL, FK_CODELST_TMROLE, FK_USER , v_newstudy, p_user,SYSDATE,STUDY_TEAM_RIGHTS , p_ip,
  study_team_usr_type
    FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user <> p_newstudydm
       AND fk_user <> p_user;
--JM: 04-10-06 Blocked
--    AND NVL(study_team_usr_type,'D') = 'D' ;--added by salil on 24 sep 2003
    END IF;

    --get role for data manager
    SELECT ctrl_value INTO v_role  FROM ER_CTRLTAB
     WHERE trim(ctrl_key) = 'author';
 -- insert study team record for data manager
    --insert into t(c3,c4) values ('for dm',v_rights);
       INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type )
   VALUES (seq_er_studyteam.NEXTVAL, v_role,p_newstudydm,v_newstudy, p_user,SYSDATE,
          v_rights, p_ip, 'D');
  IF  p_teamflag = 1 THEN
 -- if data manager and creator are different, insert a record for the creator also
IF p_newstudydm <> p_user THEN
SELECT fk_siteid INTO v_creator_site FROM ER_USER WHERE pk_user = p_user;
     SELECT COUNT(*) INTO v_creator_site_cnt
  FROM ER_STUDYSITES WHERE fk_site = (SELECT fk_siteid FROM ER_USER WHERE pk_user = p_user)
  AND fk_study = v_newstudy;
      --- to get the rights of the old data manager done by salil 24 sep 2003
   IF(v_creator_site_cnt = 0) THEN
     INSERT INTO ER_STUDYSITES(PK_STUDYSITES, FK_STUDY, FK_SITE, FK_CODELST_STUDYSITETYPE,
  STUDYSITE_LSAMPLESIZE, STUDYSITE_PUBLIC, STUDYSITE_REPINCLUDE, CREATOR, CREATED_ON, IP_ADD)
  VALUES(seq_er_studysites.NEXTVAL, v_newstudy,v_creator_site,NULL,NULL,NULL,NULL,p_user, SYSDATE, p_ip);
  END IF;

    SELECT COUNT(*) INTO cnt_datamng FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user
       AND NVL(study_team_usr_type,'D') = 'D' ;
   IF(cnt_datamng >0) THEN
    SELECT STUDY_TEAM_RIGHTS INTO v_oldrights FROM ER_STUDYTEAM
    WHERE fk_study = p_origstudy AND fk_user = p_user
       AND NVL(study_team_usr_type,'D') = 'D' ;
        INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , STUDY_TEAM_RIGHTS , IP_ADD, study_team_usr_type )
        VALUES (seq_er_studyteam.NEXTVAL, v_role,p_user,v_newstudy, p_user,SYSDATE,
          v_oldrights, p_ip, 'D');-- v_oldrights added by salil on 24 sep 2003
   ELSE
    -- Rights are inserted based on the role through trigger er_studyteam_BI_role
     INSERT INTO ER_STUDYTEAM ( PK_STUDYTEAM, FK_CODELST_TMROLE,
              FK_USER , FK_STUDY  , CREATOR , CREATED_ON , IP_ADD, study_team_usr_type )
           VALUES (seq_er_studyteam.NEXTVAL, v_role,p_user,v_newstudy, p_user,SYSDATE,
           p_ip, 'D');

     INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON)
      SELECT SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL,FK_SITE,v_newstudy,p_user,1,p_user,SYSDATE
      FROM ER_USERSITE s,ER_USER u
      WHERE s.fk_user = u.pk_user
      AND s.fk_user = p_user
      AND USERSITE_RIGHT >= 4
      AND fk_user NOT IN(SELECT FK_USER
      FROM ER_STUDY_SITE_RIGHTS
      WHERE fk_study = p_origstudy );
      INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON)
      SELECT SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL,FK_SITE,v_newstudy,p_user,0,p_user,SYSDATE
      FROM ER_USERSITE s,ER_USER u
      WHERE s.fk_user = u.pk_user
      AND s.fk_user = p_user
      AND USERSITE_RIGHT < 4
      AND fk_user NOT IN(SELECT FK_USER
       FROM ER_STUDY_SITE_RIGHTS
       WHERE fk_study = p_origstudy );
     END IF;
     END IF;-- study team selected
 END IF;
 -- get study versions from   p_verarr (TYPES.NUMBER_ARRAY)
 FOR i IN 1..p_verarr.COUNT
 LOOP
          v_oldpkstudyver := TO_NUMBER(p_verarr(i));
  --copy version details
  SELECT seq_er_studyver.NEXTVAL INTO v_newpkstudyver  FROM dual;
  INSERT INTO ER_STUDYVER  (PK_STUDYVER,FK_STUDY,STUDYVER_NUMBER,
            STUDYVER_STATUS,STUDYVER_NOTES,STUDYVER_DATE,STUDYVER_CATEGORY,STUDYVER_TYPE, CREATOR, CREATED_ON, IP_ADD)
          SELECT v_newpkstudyver ,v_newstudy,STUDYVER_NUMBER,
            STUDYVER_STATUS,STUDYVER_NOTES,STUDYVER_DATE,STUDYVER_CATEGORY,STUDYVER_TYPE, p_user, SYSDATE, p_ip
     FROM ER_STUDYVER
  WHERE pk_studyver= v_oldpkstudyver;
   -- insert record into er_status_history for version history
     INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
     STATUS_MODPK , STATUS_MODTABLE,
     FK_CODELST_STAT ,
     STATUS_DATE ,
     CREATOR , RECORD_TYPE, CREATED_ON,IP_ADD,STATUS_ENTERED_BY)
        VALUES ( seq_er_status_history.NEXTVAL,
      v_newpkstudyver,'er_studyver',
      Pkg_Util.f_getCodePk('W','versionStatus'),
      TRUNC(SYSDATE),
      p_user,'N',SYSDATE,p_ip,p_user);
  --copy sections
  INSERT INTO ER_STUDYSEC ( pk_studysec, fk_study,studysec_name, studysec_num,
                        studysec_text,studysec_pubflag,
                        studysec_seq,creator, created_on,fk_studyver,ip_add)
             SELECT seq_er_studysec.NEXTVAL,NULL, studysec_name,studysec_num,
                    studysec_text, 'N', studysec_seq,
                    p_user, SYSDATE,v_newpkstudyver,p_ip
               FROM ER_STUDYSEC
              WHERE fk_studyver = v_oldpkstudyver;
      --insert appendix
        INSERT INTO ER_STUDYAPNDX (pk_studyapndx,fk_study,studyapndx_uri,studyapndx_desc,
                        studyapndx_pubflag,creator,created_on,studyapndx_file,studyapndx_type,
        studyapndx_fileobj,studyapndx_filesize,fk_studyver,ip_add )
            SELECT seq_er_studyapndx.NEXTVAL, NULL,studyapndx_uri,studyapndx_desc,
                    'N',p_user,SYSDATE,  DECODE (trim (studyapndx_type), 'file', SUBSTR ( studyapndx_uri,
                                  1, (INSTR (studyapndx_uri, '.', -1) - 1) ) || v_dt ||
                               SUBSTR (studyapndx_uri, INSTR (studyapndx_uri, '.', -1)), studyapndx_uri),
                    studyapndx_type,  studyapndx_fileobj,  studyapndx_filesize,
         v_newpkstudyver,p_ip
               FROM ER_STUDYAPNDX
              WHERE fk_studyver = v_oldpkstudyver ;
 END LOOP;
 IF p_verarr.COUNT < 1 THEN --if no version is selected, insert a default version
   SELECT seq_er_studyver.NEXTVAL
   INTO v_new_ver
   FROM dual;
      INSERT INTO ER_STUDYVER (PK_STUDYVER,FK_STUDY,STUDYVER_NUMBER,STUDYVER_STATUS,
          CREATOR,CREATED_ON,IP_ADD )
      VALUES (v_new_ver,v_newstudy, '1','W', p_user, SYSDATE,p_ip);
   -- insert record into er_status_history for version history
     INSERT INTO ER_STATUS_HISTORY( PK_STATUS ,
     STATUS_MODPK , STATUS_MODTABLE,
     FK_CODELST_STAT ,
     STATUS_DATE ,
     CREATOR , RECORD_TYPE, CREATED_ON,IP_ADD,STATUS_ENTERED_BY)
        VALUES ( seq_er_status_history.NEXTVAL,
      v_new_ver,'er_studyver',
      Pkg_Util.f_getCodePk('W','versionStatus'),
      SYSDATE,
      p_user,'N',SYSDATE,p_ip,p_user);
 END IF;
 -- get study versions from   p_calarr (TYPES.NUMBER_ARRAY)
 FOR i IN 1..p_calarr.COUNT
 LOOP
         v_oldcal := TO_NUMBER(p_calarr(i));
     -- insert calendar record
        SELECT EVENT_DEFINITION_SEQ.NEXTVAL
        INTO v_newcal FROM DUAL;
  --SV, 8/5/04, fix for bug #1551, when a study is copied, the calendar status must be WIP nd creator must be set. Also added last_modified_by and ip_add
  --JM: 04-10-06 added 'event_calassocto' column in the query
    INSERT INTO EVENT_ASSOC
         (
        EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, NOTES,
       COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,event_calassocto,
           FUZZY_PERIOD, MSG_TO, STATUS, DESCRIPTION, DISPLACEMENT,
           ORG_ID, EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
           PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
           PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, STATUS_DT,
           STATUS_CHANGEBY, CREATOR, LAST_MODIFIED_BY, IP_ADD
         )
         SELECT v_newcal,v_newstudy,EVENT_TYPE, NAME, NOTES,
       COST, COST_DESCRIPTION, DURATION, USER_ID, LINKED_URI,event_calassocto,
           FUZZY_PERIOD, MSG_TO, 'W', DESCRIPTION, DISPLACEMENT, ORG_ID,
   EVENT_MSG, EVENT_RES, EVENT_FLAG, PAT_DAYSBEFORE,
           PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE,
           PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER, SYSDATE,
           p_user, p_user, p_user, p_ip
     FROM EVENT_ASSOC
     WHERE event_id =  v_oldcal AND CHAIN_ID = p_origstudy
         AND event_type = 'P';
         -- copy event details of the protocol
         --changed by Sonika on March 09,04, added ip
         sp_cpprotevents(v_oldcal , v_newcal, p_user,p_ip ) ;
     END LOOP;
    -- end of procedure
    COMMIT;
    o_success := v_newstudy; --returning study id
    END;
/


