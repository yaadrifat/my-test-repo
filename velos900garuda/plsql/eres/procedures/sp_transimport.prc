CREATE OR REPLACE PROCEDURE        "SP_TRANSIMPORT" IS
tmpVar NUMBER;
i NUMBER;
v_cycleno NUMBER;
v_patId VARCHAR2(20);
v_studyId VARCHAR2(20) ;
v_reccount NUMBER;
v_form VARCHAR2(20) ;
v_mode VARCHAR2(2);
v_sqlstr VARCHAR2(4000);
v_valuestr VARCHAR2(4000) ;
v_iter NUMBER;
v_patprot NUMBER ;
v_sql LONG;
notes VARCHAR2(4000);
v_ver NUMBER;
v_study VARCHAR2(20);
v_per VARCHAR2(20);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'sp_transimport', pLEVEL  => Plog.LFATAL);

/******************************************************************************
   NAME:       sp_transimport
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8/3/2004          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     sp_transimport
      Sysdate:         8/3/2004
      Date and Time:   8/3/2004, 2:16:33 PM, and 8/3/2004 2:16:33 PM
 ******************************************************************************/
BEGIN

   tmpVar := 0;
   v_iter := 0;
   FOR j IN (SELECT DISTINCT col4,col5,col15 FROM ER_TRANSIMPORT WHERE import_type='MIG')
     LOOP
	  Plog.DEBUG(pCTX,'start sp_transimport');
	  	   	  Plog.DEBUG(pCTX,'j.col15'||j.col15||'j.col14'||j.col4||'j.col5'||j.col5);
	   SELECT COUNT(*) INTO v_reccount FROM ER_IMPPATFORM WHERE fk_form=j.col15 AND fk_per=j.col4 AND Pkg_Util.F_To_Number(col1)=j.col5;
	   	  Plog.DEBUG(pCTX,'v_reccount'||v_reccount);
--	    IF (v_reccount>0) THEN
		--v_mode:='U';
--		v_sqlstr:='update er_imppatform';

		/* Always do the insertion*/
		v_mode:='I';
		v_sqlstr:='Insert into er_imppatform (pk_imppatform,fk_per,fk_patprot,fk_form,filldate,form_type,export_flag,custom_col,fk_formlibver,col1,col2,col3,col4,col5,col6,'||
 					   'col7,col8,col9';
	--	ELSE
		--v_mode:='I';
--		v_sqlstr:='Insert into er_imppatform (pk_imppatform,fk_per,fk_patprot,fk_form,filldate,form_type,export_flag,custom_col,fk_formlibver,col1,col2,col3,col4,col5,col6,'||
 	--				   'col7,col8,col9';
		--END IF;
		FOR i IN (SELECT col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15 FROM ER_TRANSIMPORT
		 WHERE col4=j.col4 AND col5=j.col5 ORDER BY col3,col4,col5,col6 ASC )
				  LOOP
				  	  Plog.DEBUG(pCTX,'inside loop with Iteration'||v_iter||' mode '||v_mode);

					  v_study:=i.col3;
					  v_per:=i.col4;

					  				  	  Plog.DEBUG(pCTX,'for study '||i.col3||'  and  patient   '||i.col4);
				  -- PATPROT ID
				  SELECT pk_patprot INTO v_patprot FROM ER_PATPROT WHERE fk_study=i.col3 AND fk_per=i.col4 AND patprot_stat=1;
				  IF  (v_iter=0) THEN
				    Plog.DEBUG(pCTX,'iteration'||v_iter||'date'||i.col6);
				   IF  (v_mode='I') THEN
								  v_valuestr:='values(seq_er_imppatform.nextval,'||i.col4||','||v_patprot||','||i.col15||','||'TO_DATE('''||i.col6||''',''mm/dd/yyyy'')'||',''P'',''1'',''' ||i.col1||''',459,'''||i.col5||''','''||i.col6||''','''||i.col7||''','''||i.col8||''','''||i.col10||''','''||i.col9||''','''||i.col11||''','''||i.col12||''','''||i.col13;
								  					  		  		 Plog.DEBUG(pCTX,'v_valuestr1'||v_valuestr);
  					  END IF; --end if v_mode
					  END IF; -- end if for (v_iter==0)
					 IF  (v_iter=1) THEN
					 				    Plog.DEBUG(pCTX,'iteration1'||v_iter);
					 			   IF  (v_mode='I') THEN
  				  v_sqlstr:=v_sqlstr||',col10,col11,col12,col13,col14,col15,col16';
				  v_valuestr:=v_valuestr||''','''||i.col7||''','''||i.col8||''','''||i.col10||''','''||i.col9||''','''||i.col11||''','''||i.col12||''','''||i.col13;
				  					  		  		 Plog.DEBUG(pCTX,'v_valuestr2'||v_valuestr);
				  END IF ;
				    END IF ;
						 IF  (v_iter=2) THEN
						 				    Plog.DEBUG(pCTX,'iteration2'||v_iter);
					 		   IF  (v_mode='I') THEN
  				  v_sqlstr:=v_sqlstr||',col17,col18,col19,col20,col21,col22,col23';
				  v_valuestr:=v_valuestr||''','''||i.col7||''','''||i.col8||''','''||i.col10||''','''||i.col9||''','''||i.col11||''','''||i.col12||''','''||i.col13;
				  					  		  		 Plog.DEBUG(pCTX,'v_valuestr3'||v_valuestr);
				  END IF ;
				    END IF ;
			 IF  (v_iter=3) THEN
			 				    Plog.DEBUG(pCTX,'iteration3'||v_iter);
					    IF  (v_mode='I') THEN
  				  v_sqlstr:=v_sqlstr||',col24,col25,col26,col27,col28,col29,col30';
				  v_valuestr:=v_valuestr||''','''||i.col7||''','''||i.col8||''','''||i.col10||''','''||i.col9||''','''||i.col11||''','''||i.col12||''','''||i.col13;
				  					  		  		 Plog.DEBUG(pCTX,'v_valuestr4'||v_valuestr);
				  END IF ;
				    END IF ;

				  v_iter:= v_iter+1;
				  IF LENGTH(i.col14) >0			THEN
				  notes:=i.col14;
				  END IF;
			  	 	  END LOOP; --loop for i
					  -- execute the SQL
					  v_sqlstr:=v_sqlstr||',col31)';
					  v_valuestr:=v_valuestr||''','''||notes||''')';
					  		  		 Plog.DEBUG(pCTX,'v_valuestr'||v_valuestr);
					  v_sql:=v_sqlstr||' '||v_valuestr;
		  		 Plog.DEBUG(pCTX,v_sql);
					  EXECUTE IMMEDIATE v_sql;

					  COMMIT;
					  v_sql:=NULL;
					  v_sqlstr:=NULL;
					  v_valuestr:=NULL;
					  v_iter:=0;
				  END LOOP;  --loop for  j
  DELETE FROM ER_TRANSIMPORT WHERE import_type='MIG';
  COMMIT;

   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END Sp_Transimport;
/


CREATE SYNONYM ESCH.SP_TRANSIMPORT FOR SP_TRANSIMPORT;


CREATE SYNONYM EPAT.SP_TRANSIMPORT FOR SP_TRANSIMPORT;


GRANT EXECUTE, DEBUG ON SP_TRANSIMPORT TO EPAT;

GRANT EXECUTE, DEBUG ON SP_TRANSIMPORT TO ESCH;

