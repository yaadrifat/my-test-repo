create or replace
PROCEDURE SP_CRI_TASKS(PK_CORDID IN NUMBER) IS
IS_SYSTEM_USER VARCHAR2(10);
IDS_COMPL_FLAG NUMBER(2) := 0;
CBUINFO_COMPL_FLAG NUMBER(2) := 0;
PROS_COMPL_FLAG NUMBER(2) :=0;
CORD_UNLIC_FLAG NUMBER(2) :=0;
CORD_LIC_FLAG NUMBER(2) :=0;
masterques_response varchar2(1000);
idm_compl_flag number(1):=0;
pk_grp_additest number(6);
IS_IDM_DATA NUMBER(10):=0;
IS_IDM_DATA1 NUMBER(10):=0;
mrq_masterques_response varchar2(6);
mrq_compl_flag number(1):=0;
IS_MRQ_DATA NUMBER(10):=0;
IS_MRQ_DATA1 NUMBER(10):=0;
IS_LABEL_TYP NUMBER(6):=0;
fmhq_masterques_response varchar2(1000);
fmhq_res_type varchar2(50);
fmhq_compl_flag number(1):=0;
IS_PROCESSING_DATA number(1):=0;
IS_FMHQ_DATA NUMBER(2):=0;
IS_FMHQ_DATA1 NUMBER(2):=0;
IS_TXT_TYP varchar2(5):='0';
IS_MULTISELL_TYP varchar2(5):='0';
IS_DRPDOWN_TYP varchar2(5):='0';
ASSESS_PASSED NUMBER(2):=2;
FMHQ_UNEXPECTED_CODELST_DESC varchar2(70);
v_split_string emb_string.string_array;
temp_string varchar2(70);
LIC_COMPL_FLAG NUMBER(2):=0;
LAB_SUMMAR_COMP1_FLAG NUMBER(2):=0;
NOT_DONE_RES NUMBER(10):=0;
CRI_COMP_FLAG NUMBER(10):=0;
MASTER_QUES_DATA_IS_THER NUMBER(10):=0;
MASTER_QUES_DATA_IS_THER_MRQ NUMBER(10):=0;
MASTER_QUES_DATA_IS_THER_FMHQ NUMBER(10):=0;
is_data_for_cord number;
OLD_IDS_COMPL_FLAG NUMBER(2) := 0;
OLD_CBUINFO_COMPL_FLAG NUMBER(2) := 0;
OLD_PROS_COMPL_FLAG NUMBER(2) :=0;
OLD_LAB_SUMMAR_COMP1_FLAG NUMBER(2):=0;
OLD_LIC_COMPL_FLAG NUMBER(2):=0;
OLD_PK_CRI NUMBER(10):=0;
old_idm_compl_flag number(1):=0;
old_mrq_compl_flag number(1):=0;
old_fmhq_compl_flag number(1):=0;
DONT_ALLOW_INSERT_FLAG NUMBER(1):=1;
old_cri_compl_flag number(1):=0;
pk_shipped_reason NUMBER(10):=0;
PK_CBU_STATUS_VAL NUMBER(10):=0;
FMHQ_UNEXPECTED_RESP_COUNT NUMBER(10):=0;
FMHQ_RESP_NEED_ASSESS NUMBER(10):=0;
FMHQ_RESP_GOT_ASSESS NUMBER(10):=0;
FMHQ_ASSESS_DATA_COUNT NUMBER(10):=0;
FMHQ_ASSESS_DATA VARCHAR(50):=0;
ELIG_COMPL_FLAG NUMBER(1):=0;
ELIG_COMPL_FLAG_P1 NUMBER(1):=0;
ELIG_COMPL_FLAG_P2 NUMBER(1):=0;
OLD_ELIG_COMPL_FLAG NUMBER(1):=0;
ELIG_QUES_FLAG NUMBER(1):=0;
COMPARE_DATE DATE;
V_CORD_SHIP_DATE DATE;
V_CONSIDER_ELIG_FMHQ NUMBER(1):=0;
IS_OTHER_VER_FMHQ_DATA NUMBER(10):=0;
ORDER_ID_VAL NUMBER(10):=0;
IS_ORDER_DATA NUMBER(10):=0;
IS_FCR_DATA NUMBER(10):=0;
SHIP_CONFIRM_FLAG NUMBER(10):=0;
V_ORDER_TYPE NUMBER(10):=0;
V_SHIP_COMP_TASK_FLAG VARCHAR2(1):=NULL;
V_CRI_COMP_TASK_FLAG VARCHAR2(1):=NULL;
V_FCR_COMP_TASK_FLAG VARCHAR2(1):=NULL;
V_PK_OR_ORDER NUMBER(10):=0;
FK_CODELST_SHIPMENT NUMBER(10):=0;
IS_SHIPMENT_DATA NUMBER(10):=0;
V_CONS_UNLIC_REASON NUMBER(10):=1;
V_TASK_FLAG NUMBER(1):=0;
BEGIN

      select count(*) into is_data_for_cord from cb_cord_complete_req_info where fk_cord_id=PK_CORDID;
      SELECT NVL(FK_CORD_CBU_STATUS,0) INTO PK_CBU_STATUS_VAL FROM CB_CORD WHERE PK_CORD=PK_CORDID;
      SELECT PK_CBU_STATUS INTO pk_shipped_reason FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SH' AND CODE_TYPE='Response';
      SELECT COUNT(*) INTO IS_ORDER_DATA FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER ) WHERE HDR.ORDER_ENTITYID=PK_CORDID;
      SELECT PK_CODELST INTO V_PK_OR_ORDER FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='OR';
      SELECT CODELST_DESC INTO COMPARE_DATE FROM ER_CODELST WHERE CODELST_TYPE='VersionDate' AND CODELST_SUBTYP='Version1.3';
      SELECT PK_CODELST INTO FK_CODELST_SHIPMENT FROM ER_CODELST WHERE CODELST_TYPE='shipment_type' AND CODELST_SUBTYP='CBU';



      if is_data_for_cord>0 then

      for compreq_data IN  (select IDS_FLAG,COMPLETE_REQ_INFO_FLAG,CBU_INFO_FLAG,CBU_PROCESSING_FLAG,LAB_SUMMARY_FLAG,IDM_FLAG,MRQ_FLAG,FMHQ_FLAG,LICENSURE_FLAG,ELIGIBILITY_FLAG,PK_CORD_COMPLETE_REQINFO from cb_cord_complete_req_info where fk_cord_id=PK_CORDID and pk_cord_complete_reqinfo=(select max(pk_cord_complete_reqinfo) from cb_cord_complete_req_info where fk_cord_id=PK_CORDID))
      loop
          OLD_IDS_COMPL_FLAG:=compreq_data.IDS_FLAG;
          OLD_CBUINFO_COMPL_FLAG:=compreq_data.CBU_INFO_FLAG;
          OLD_PROS_COMPL_FLAG:=compreq_data.CBU_PROCESSING_FLAG;
          OLD_LAB_SUMMAR_COMP1_FLAG:=compreq_data.LAB_SUMMARY_FLAG;
          OLD_LIC_COMPL_FLAG:=compreq_data.LICENSURE_FLAG;
          old_idm_compl_flag:=compreq_data.IDM_FLAG;
          old_mrq_compl_flag:=compreq_data.MRQ_FLAG;
          old_fmhq_compl_flag:=compreq_data.FMHQ_FLAG;
          OLD_ELIG_COMPL_FLAG:=compreq_data.ELIGIBILITY_FLAG;
          old_cri_compl_flag:=compreq_data.COMPLETE_REQ_INFO_FLAG;
	  OLD_PK_CRI:=compreq_data.PK_CORD_COMPLETE_REQINFO;
      end loop;

      end if;




      IF IS_ORDER_DATA=0 THEN
    
        V_CONSIDER_ELIG_FMHQ:=1;
        V_CONS_UNLIC_REASON:=0;
    
      ELSIF IS_ORDER_DATA>0 THEN 
    
                SELECT MAX(ORD.PK_ORDER) INTO ORDER_ID_VAL FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER ) WHERE HDR.ORDER_ENTITYID=PK_CORDID;
                
                SELECT ORD.ORDER_TYPE,NVL(ORD.CORD_SHIPPED_FLAG,'N'),NVL(ORD.COMPLETE_REQ_INFO_TASK_FLAG,'N'),NVL(ORD.FINAL_REVIEW_TASK_FLAG,'N') INTO V_ORDER_TYPE,V_SHIP_COMP_TASK_FLAG,V_CRI_COMP_TASK_FLAG,V_FCR_COMP_TASK_FLAG FROM ER_ORDER ORD WHERE ORD.PK_ORDER=ORDER_ID_VAL;

                SELECT F_IS_NEW_TASK_NEEDED(PK_CORDID) INTO V_TASK_FLAG FROM DUAL;

		IF V_TASK_FLAG=1 THEN
		    V_CONSIDER_ELIG_FMHQ:=1;
		    V_CONS_UNLIC_REASON:=0;
                END IF;


		--DBMS_OUTPUT.PUT_LINE('ORDER_ID_VAL::::::::::::'||ORDER_ID_VAL);
                --DBMS_OUTPUT.PUT_LINE('V_SHIP_COMP_TASK_FLAG::::::::::::'||V_SHIP_COMP_TASK_FLAG);
                --DBMS_OUTPUT.PUT_LINE('V_CRI_COMP_TASK_FLAG::::::::::::'||V_CRI_COMP_TASK_FLAG);
                --DBMS_OUTPUT.PUT_LINE('V_FCR_COMP_TASK_FLAG::::::::::::'||V_FCR_COMP_TASK_FLAG);
                --DBMS_OUTPUT.PUT_LINE('OLD_PK_CRI::::::::::::'||OLD_PK_CRI);
  
        END IF;

        DBMS_OUTPUT.PUT_LINE('V_CONSIDER_ELIG_FMHQ::::::::::::'||V_CONSIDER_ELIG_FMHQ);


        --DBMS_OUTPUT.PUT_LINE('PK_CORDID VALUE::::::::::::'||PK_CORDID);

        SELECT NVL(CBBTBL.USING_CDR,0) INTO IS_SYSTEM_USER FROM CB_CORD CRD LEFT OUTER JOIN CBB CBBTBL ON (CRD.FK_CBB_ID=CBBTBL.FK_SITE) WHERE CRD.PK_CORD = PK_CORDID;
        

        --DBMS_OUTPUT.PUT_LINE('IS_SYSTEM_USER VALUE::::::::::::'||IS_SYSTEM_USER);

        IF IS_SYSTEM_USER=1 THEN

                            ---IDS TASKS FOR SYS CORD---

                           SELECT CASE WHEN CORD_REGISTRY_ID IS NOT NULL AND (CORD_LOCAL_CBU_ID IS NOT NULL) AND REGISTRY_MATERNAL_ID IS NOT NULL AND (CORD_ID_NUMBER_ON_CBU_BAG!='-1' AND CORD_ID_NUMBER_ON_CBU_BAG IS NOT NULL) THEN 1 ELSE 0 END IDSFLAG INTO IDS_COMPL_FLAG
                           FROM CB_CORD
                           WHERE PK_CORD IN (PK_CORDID);


                           --DBMS_OUTPUT.PUT_LINE('IDS_COMPL_FLAG VALUE FOR SYSCORD::::::::::::'||IDS_COMPL_FLAG);

                           ---CBU INFORMATION TASKS FOR SYS CORD---

                           SELECT
                           CASE WHEN
                           (SPE.SPEC_COLLECTION_DATE IS NOT NULL OR CRD.CORD_BABY_BIRTH_DATE IS NOT NULL)  AND
                           CRD.FK_CORD_BABY_GENDER_ID IS NOT NULL AND
                           (CRD.FK_CODELST_ETHNICITY!=-1 AND  CRD.FK_CODELST_ETHNICITY IS NOT NULL) AND
                           (CRD.MULTIPLE_BIRTH!=-1 AND CRD.MULTIPLE_BIRTH IS NOT NULL) AND
                           (CRD.FK_CBU_COLL_TYPE!=-1 AND CRD.FK_CBU_COLL_TYPE IS NOT NULL) AND
                           (CRD.FK_CBU_DEL_TYPE!=-1 AND CRD.FK_CBU_DEL_TYPE IS NOT NULL) AND
                           (SELECT count(*) FROM CB_MULTIPLE_VALUES RACE_DATA WHERE RACE_DATA.ENTITY_ID IN (PK_CORDID) AND RACE_DATA.ENTITY_TYPE=(SELECT CODELST_DESC FROM ER_CODELST WHERE CODELST_TYPE='entity_type' and CODELST_SUBTYP='CBU') AND RACE_DATA.FK_CODELST_TYPE='race') > 0  THEN 1 ELSE 0 END CBUINFO INTO CBUINFO_COMPL_FLAG
                           FROM CB_CORD CRD
                           LEFT OUTER JOIN ER_SPECIMEN SPE ON (CRD.FK_SPECIMEN_ID = SPE.PK_SPECIMEN)
                           WHERE CRD.PK_CORD IN (PK_CORDID);

                           --DBMS_OUTPUT.PUT_LINE('CBUINFO_COMPL_FLAG VALUE FOR SYSCORD::::::::::::'||CBUINFO_COMPL_FLAG);

                           ---PROCESSING INFORMATION TASKS FOR SYS CORD---

                          SELECT
                          COUNT(*) INTO IS_PROCESSING_DATA
                          FROM CB_CORD CRD
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES PROC ON (CRD.FK_CBB_PROCEDURE=PROC.PK_PROC)
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES_INFO PROC_INFO ON (PROC_INFO.FK_PROCESSING_ID=PROC.PK_PROC)
                          LEFT OUTER JOIN CB_ENTITY_SAMPLES SAMPLES ON (SAMPLES.ENTITY_ID=CRD.PK_CORD)
                          WHERE CRD.PK_CORD IN (PK_CORDID) AND SAMPLES.PK_ENTITY_SAMPLES =(SELECT MAX(PK_ENTITY_SAMPLES) FROM CB_ENTITY_SAMPLES WHERE ENTITY_ID=CRD.PK_CORD);

                          IF IS_PROCESSING_DATA>0 THEN
                          SELECT
                          CASE
                          WHEN
                          PROC.PROC_NAME IS NOT NULL AND
                          PROC.PROC_START_DATE IS NOT NULL AND
                          PROC_INFO.FK_PROC_METH_ID IS NOT NULL AND
                          PROC_INFO.FK_PRODUCT_MODIFICATION IS NOT NULL AND
                          --PROC_INFO.FK_STOR_METHOD IS NOT NULL AND
                          F_PROC_OTHER_MANUFAC(PK_CORDID) = 1 AND
                          PROC_INFO.FK_STOR_TEMP IS NOT NULL AND
                          --PROC_INFO.FK_CONTRL_RATE_FREEZING IS NOT NULL AND
                          F_PROC_VIALS_BAGS_TUBS(PK_CORDID) = 1 AND
                          PROC_INFO.NO_OF_INDI_FRAC IS NOT NULL AND
                          SAMPLES.FILT_PAP_AVAIL IS NOT NULL AND
                          SAMPLES.RBC_PEL_AVAIL IS NOT NULL AND
                          SAMPLES.NO_EXT_DNA_ALI IS NOT NULL AND
                          SAMPLES.NO_SER_ALI IS NOT NULL AND
                          SAMPLES.NO_PLAS_ALI IS NOT NULL AND
                          SAMPLES.NO_NON_VIA_ALI IS NOT NULL AND
                          SAMPLES.NO_VIA_CEL_ALI IS NOT NULL AND
                          SAMPLES.NO_OF_SEG_AVAIL IS NOT NULL AND
                          SAMPLES.CBU_OT_REP_CON_FIN IS NOT NULL AND
                          SAMPLES.CBU_NO_REP_ALT_CON IS NOT NULL AND
                          SAMPLES.NO_SER_MAT_ALI IS NOT NULL AND
                          SAMPLES.NO_PLAS_MAT_ALI IS NOT NULL AND
                          SAMPLES.NO_EXT_DNA_MAT_ALI IS NOT NULL AND
                          SAMPLES.SI_NO_MISC_MAT IS NOT NULL
                          THEN 1
                          ELSE 0
                          END PROC_INF0_FLAG INTO PROS_COMPL_FLAG
                          FROM CB_CORD CRD
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES PROC ON (CRD.FK_CBB_PROCEDURE=PROC.PK_PROC)
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES_INFO PROC_INFO ON (PROC_INFO.FK_PROCESSING_ID=PROC.PK_PROC)
                          LEFT OUTER JOIN CB_ENTITY_SAMPLES SAMPLES ON (SAMPLES.ENTITY_ID=CRD.PK_CORD)
                          WHERE CRD.PK_CORD IN (PK_CORDID) AND SAMPLES.PK_ENTITY_SAMPLES =(SELECT MAX(PK_ENTITY_SAMPLES) FROM CB_ENTITY_SAMPLES WHERE ENTITY_ID=CRD.PK_CORD);

                          END IF;


 			  ---- ELIGIBILITY TASK FOR SYS CORD ---

                          IF V_CONSIDER_ELIG_FMHQ=1 THEN  

		                   SELECT
		                   CASE
		                          WHEN CRD.FK_CORD_CBU_ELIGIBLE_STATUS IS NOT NULL AND CRD.FK_CORD_CBU_ELIGIBLE_STATUS !=-1 THEN
		                                                                 CASE
		                                                                      WHEN CRD.FK_CORD_CBU_ELIGIBLE_STATUS IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='eligibility' AND CODELST_SUBTYP in ('ineligible','incomplete')) THEN
		                                                                        CASE
		                                                                          WHEN (SELECT COUNT(*) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD IN (PK_CORDID))>0 THEN 1
		                                                                          ELSE 0
		                                                                        END
		                                                                        WHEN CRD.FK_CORD_CBU_ELIGIBLE_STATUS IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='eligibility' AND CODELST_SUBTYP in ('not_appli_prior')) THEN
		                                                                        CASE
		                                                                          WHEN CRD.CORD_ELIGIBLE_ADDITIONAL_INFO IS NOT NULL THEN 1
		                                                                          ELSE 0
		                                                                        END
		                                                                        ELSE 1
		                                                                 END
		                         ELSE 0
		                   END INTO ELIG_COMPL_FLAG_P1
		                   FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);


				   SP_NEW_FINAL_ELIG_TASK(PK_CORDID,ELIG_COMPL_FLAG_P2);

		                   IF ELIG_COMPL_FLAG_P1=1 AND ELIG_COMPL_FLAG_P2=1 THEN 
					ELIG_COMPL_FLAG:=1;
		                   END IF;


                         END IF;


                          ---LICENSURE TASK FOR SYS CORD---

			  IF V_CONS_UNLIC_REASON=1 THEN  

		                  SELECT
		                   CASE
		                          WHEN CRD.FK_CORD_CBU_LIC_STATUS IS NOT NULL AND CRD.FK_CORD_CBU_LIC_STATUS !=-1 THEN
		                                                                 CASE
		                                                                      WHEN CRD.FK_CORD_CBU_LIC_STATUS IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='licence' AND CODELST_SUBTYP='unlicensed') THEN
		                                                                        CASE
		                                                                          WHEN (SELECT COUNT(*) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD IN (PK_CORDID))>0 THEN 1
		                                                                          ELSE 0
		                                                                        END
		                                                                      ELSE 1
		                                                                 END
		                         ELSE 0
		                   END INTO LIC_COMPL_FLAG
		                   FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);

                           END IF;

			  IF V_CONS_UNLIC_REASON=0 THEN  

		                   SELECT
		                   CASE
		                          WHEN CRD.FK_CORD_CBU_LIC_STATUS IS NOT NULL AND CRD.FK_CORD_CBU_LIC_STATUS !=-1 THEN  1          
		                          ELSE 0
		                   END INTO LIC_COMPL_FLAG
		                   FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);

                           END IF;


                          ---LAB SUMMARY TASK FOR SYS CORD---


                          SELECT F_LABSUMMARY_COMPL_CHECK(PK_CORDID) into LAB_SUMMAR_COMP1_FLAG FROM DUAL;

                          --DBMS_OUTPUT.PUT_LINE('LAB_SUMMAR_COMP1_FLAG VALUE FOR SYSCORD::::::::::::'||LAB_SUMMAR_COMP1_FLAG);


                          ----IDM TASKS FOR SYS CORD---

                         SELECT COUNT(*) INTO IS_IDM_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1);


                          IF IS_IDM_DATA > 0 THEN

                                 SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_IDM_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1));

                                 IF IS_IDM_DATA1=1 THEN

                                             for idm_data in ( select
                                                ques.pk_questions pk_ques,
                                                forq.ques_seq QUES_SEQ,
                                                NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                                                NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                                                f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                                                f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                                                forq.fk_master_ques MASTER_QUES,
                                                ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                                                nvl(formres.resp_code,'0') RESPONSE_CODE,
                                                case
                                                    when formres.resp_val is null then '0'
                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                                                end RESPONSE_VAL,
                                                NVL(formres.resp_val,'0') RESPONSE_VAL12,
                                                formres.fk_form_responses FK_FORM_RESP,
                                                nvl(formres.dynaformDate,'0') DYNAFORMDATE,
                                                ques.unlicen_req_flag UNLIC_REQ_FLAG,
                                                ques_desc QUES_DESC,
                                                formres.fk_form_version fk_form_vrson,
                                                formres.pk_cordid pk_cordid,
                                                qgrp.FK_QUESTION_GROUP fk_ques_grp,
                                                case when ques.assesment_flag = 1 and formres.resp_code is not null then
                                                           case when f_codelst_desc(formres.resp_code) in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then case when formres.pk_assessment is null then 0 when f_codelst_desc(formres.assess_reason2)=f_codelst_desc(formres.resp_code) then nvl(formres.pk_assessment,0) when f_codelst_desc(formres.assess_reason2)!=f_codelst_desc(formres.resp_code) then 0 end
                                                                when f_codelst_desc(formres.resp_code) not in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then nvl(formres.pk_assessment,1)
                                                           end
                                                     else 2
                                                end  assesment_value,
                                                ques.assesment_flag quest_assess_flag
                                                from
                                                cb_form_questions forq,
                                                cb_question_grp qgrp,
                                                cb_questions ques
                                                left outer join
                                                (select
                                                    frmres.pk_form_responses,
                                                    frmres.fk_question,
                                                    frmres.response_code resp_code,
                                                    frmres.response_value resp_val,
                                                    frmres.comments,
                                                    frmres.fk_form_responses,
                                                    asses.pk_assessment pk_assessment,
                                                    f_codelst_desc(asses.assessment_for_response) assess_response,
                                                    asses.assessment_remarks assess_remarks,
                                                    to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
                                                    frmres.fk_form_version fk_form_version,
                                                    frmres.entity_id pk_cordid,
                                                    asses.assess_reason1 assess_reason2
                                                  from
                                                      cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response,cb_assessment.assessment_reason assess_reason1 from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='IDM')) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                                  where
                                                      frmres.entity_id=PK_CORDID and
                                                      frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='IDM' and cb_forms.is_current_flag=1)) and
                                                      frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                                                on(formres.FK_QUESTION=ques.pk_questions)
                                                where
                                                ques.pk_questions=forq.fk_question and
                                                ques.pk_questions=qgrp.fk_question and
                                                forq.fk_form=(select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1)  and
                                                qgrp.fk_form=(select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1)
                                                order by  qgrp.pk_question_grp,ques.pk_questions )
                                                loop

                                                    select pk_question_group into pk_grp_additest  from cb_question_group where question_grp_desc='Additional / Miscellaneous IDM Tests';
                                                    SELECT PK_CODELST INTO NOT_DONE_RES FROM ER_CODELST WHERE CODELST_TYPE='test_outcome1' AND CODELST_SUBTYP='not_done';

                                                    if idm_data.DEPENT_QUES_PK='0' then
                                                           if(idm_data.RESPONSE_CODE!='0' or idm_data.dynaformDate!='0') then
                                                           --dbms_output.put_line('data is for non dependent question::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                           idm_compl_flag:=1;
                                                           end if;

                                                           if(idm_data.RESPONSE_CODE='0' and idm_data.dynaformDate='0') or ( idm_data.assesment_value=0 ) then
                                                              if(idm_data.fk_ques_grp!=pk_grp_additest) then
                                                                --dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                idm_compl_flag:=0;
                                                                EXIT;
                                                              end if;
                                                              if(idm_data.fk_ques_grp=pk_grp_additest and idm_data.RESPONSE_CODE!='0') then
                                                                dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                idm_compl_flag:=0;
                                                                EXIT;
                                                              end if;
                                                           end if;


                                                    end if;


                                                   if idm_data.DEPENT_QUES_PK!='0' then
                                                         --dbms_output.put_line('master question value::::'||idm_data.DEPENT_QUES_PK);
                                                         --select nvl(formres2.RESPONSE_CODE,'0') into  masterques_response from cb_form_responses formres2 where formres2.FK_QUESTION = idm_data.DEPENT_QUES_PK and formres2.entity_id = idm_data.pk_cordid and formres2.fk_form_version = idm_data.fk_form_vrson;
                                                         --dbms_output.put_line('master question value::::'||masterques_response);

                                                         select COUNT(*) into MASTER_QUES_DATA_IS_THER from cb_form_responses formres2 where formres2.FK_QUESTION = idm_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='IDM' and cb_forms.is_current_flag=1));

                                                         IF MASTER_QUES_DATA_IS_THER>0 THEN
                                                          select nvl(formres2.RESPONSE_CODE,'0') into  masterques_response from cb_form_responses formres2 where formres2.FK_QUESTION = idm_data.DEPENT_QUES_PK and formres2.entity_id =PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='IDM' and cb_forms.is_current_flag=1));
                                                          --dbms_output.put_line('master question value2::::'||masterques_response);
                                                         END IF;

                                                         IF MASTER_QUES_DATA_IS_THER=0 THEN
                                                            masterques_response:='0';
                                                         END IF;

                                                         if masterques_response!='0'  and (instr(idm_data.DEPENT_QUES_VALUE,masterques_response) !=0 ) then
                                                         v_split_string := emb_string.split_string(idm_data.DEPENT_QUES_VALUE);
                                                         if v_split_string.count > 0 then
                                                                  for i in 1..v_split_string.count loop
                                                                      --dbms_output.put_line(v_split_string(i));
                                                                      --select f_codelst_desc(v_split_string(i)) into temp_string from dual;
                                                                      if idm_data.RESPONSE_CODE != '0' or idm_data.dynaformDate != '0'  and instr(masterques_response,v_split_string(i)) !=0 then
                                                                        idm_compl_flag:=1;
                                                                        --dbms_output.put_line('required data in child question::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                      end if;
                                                                   end loop;
                                                         end if;
                                                         end if;



                                                         if masterques_response!='0'  and (instr(idm_data.DEPENT_QUES_VALUE,masterques_response) !=0 ) then

                                                            if idm_data.RESPONSE_CODE != '0' or idm_data.dynaformDate != '0' then
                                                              idm_compl_flag:=1;
                                                              --dbms_output.put_line('required data in child question::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                            end if;
  							if( idm_data.assesment_value=0 ) then
                                                              idm_compl_flag:=0;
                                                              EXIT;
                                                            end if;
                                                            if(idm_data.RESPONSE_CODE = '0' and idm_data.dynaformDate = '0') then
                                                                if(idm_data.QUES_SEQ!='14') then

                                                                  --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1::::::::::::'||idm_data.RESPONSE_CODE ||' '||idm_data.dynaformDate);
                                                                  --dbms_output.put_line('Inside exit of condit 1::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                  idm_compl_flag:=0;
                                                                  EXIT;

                                                               end if;

                                                               if idm_data.QUES_SEQ='14'and instr(masterques_response, NOT_DONE_RES)=0 then
                                                                  --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1 for quest14::::::::::::'||idm_data.RESPONSE_CODE ||' '||idm_data.dynaformDate);
                                                                  --dbms_output.put_line('Inside exit of condit seperate condi 1::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                  idm_compl_flag:=0;
                                                                  exit;
                                                               end if;

                                                            end if;

                                                         end if;
                                          --               if(masterques_response='0' and instr(idm_data.DEPENT_QUES_VALUE, masterques_response)=0) then
                                          --                     dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 2::::::::::::'||idm_data.DEPENT_QUES_VALUE||' '||instr(idm_data.DEPENT_QUES_VALUE, masterques_response));
                                          --                    idm_compl_flag:=0;
                                          --                    EXIT;
                                          --               end if;

                                                   end if;

                                                end loop;
                                  END IF;

                              END IF;

                              --dbms_output.put_line('idm_compl_flag::::'||idm_compl_flag);


                              select case when fk_cord_cbu_lic_status in (select pk_codelst from er_codelst where codelst_type='licence' and codelst_subtyp='unlicensed') then 1 else 0 end unlicensedflag INTO CORD_UNLIC_FLAG from cb_cord where pk_cord in (PK_CORDID);

                              select case when fk_cord_cbu_lic_status in (select pk_codelst from er_codelst where codelst_type='licence' and codelst_subtyp='licensed') then 1 else 0 end unlicensedflag INTO CORD_LIC_FLAG from cb_cord where pk_cord in (PK_CORDID);

                              --DBMS_OUTPUT.PUT_LINE('CORD_UNLIC_FLAG VALUE FOR SYSCORD::::::::::::'||CORD_UNLIC_FLAG);
                              --DBMS_OUTPUT.PUT_LINE('CORD_LIC_FLAG VALUE FOR SYSCORD::::::::::::'||CORD_LIC_FLAG);



                              -------------MRQ,FMHQ FORM FOR UNLICENSED CORD---------------


                              IF CORD_UNLIC_FLAG=1 OR CORD_LIC_FLAG=1 THEN


                                          --- MRQ TASK FOR UNLICENSED SYS CORD -----

                                          SELECT COUNT(*) INTO IS_MRQ_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1);


                                          IF IS_MRQ_DATA > 0 THEN

                                                 SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_MRQ_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1));

                                                 IF IS_MRQ_DATA1=1 THEN

                                                             for mrq_data in ( select
                                                                ques.pk_questions pk_ques,
                                                                forq.ques_seq QUES_SEQ,
                                                                NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                                                                NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                                                                f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                                                                f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                                                                forq.fk_master_ques MASTER_QUES,
                                                                ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                                                                nvl(formres.resp_code,'0') RESPONSE_CODE,
                                                                case
                                                                    when formres.resp_val is null then '0'
                                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                                                                end RESPONSE_VAL,
                                                                NVL(formres.resp_val,'0') RESPONSE_VAL12,
                                                                formres.fk_form_responses FK_FORM_RESP,
                                                                nvl(formres.dynaformDate,'0') DYNAFORMDATE,
                                                                ques.unlicen_req_flag UNLIC_REQ_FLAG,
                                                                ques_desc QUES_DESC,
                                                                formres.fk_form_version fk_form_vrson,
                                                                formres.pk_cordid pk_cordid,
                                                                qgrp.FK_QUESTION_GROUP fk_ques_grp,
                                                                ques.response_type RESPONSE_TYPE_PK,
                                                                case when ques.assesment_flag = 1 and formres.resp_code is not null then
                                                                      case when f_codelst_desc(formres.resp_code) in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then case  when formres.pk_assessment is null then 0 when f_codelst_desc(formres.assess_reason2)=f_codelst_desc(formres.resp_code) then nvl(formres.pk_assessment,0) when f_codelst_desc(formres.assess_reason2)!=f_codelst_desc(formres.resp_code) then 0 end
                                                                           when f_codelst_desc(formres.resp_code) not in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then nvl(formres.pk_assessment,1)
                                                                      end
                                                                    else 2
                                                                end  assesment_value,
                                                                ques.assesment_flag quest_assess_flag
                                                                from
                                                                cb_form_questions forq,
                                                                cb_question_grp qgrp,
                                                                cb_questions ques
                                                                left outer join
                                                                (select
                                                                    frmres.pk_form_responses,
                                                                    frmres.fk_question,
                                                                    frmres.response_code resp_code,
                                                                    frmres.response_value resp_val,
                                                                    frmres.comments,
                                                                    frmres.fk_form_responses,
                                                                    asses.pk_assessment pk_assessment,
                                                                    f_codelst_desc(asses.assessment_for_response) assess_response,
                                                                    asses.assessment_remarks assess_remarks,
                                                                    to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
                                                                    frmres.fk_form_version fk_form_version,
                                                                    asses.assess_reason1 assess_reason2,
                                                                    frmres.entity_id pk_cordid
                                                                  from
                                                                      cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response,cb_assessment.assessment_reason assess_reason1 from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='MRQ')) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                                                  where
                                                                      frmres.entity_id=PK_CORDID and
                                                                      frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='MRQ' and cb_forms.is_current_flag=1)) and
                                                                      frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                                                                on(formres.FK_QUESTION=ques.pk_questions)
                                                                where
                                                                ques.pk_questions=forq.fk_question and
                                                                ques.pk_questions=qgrp.fk_question and
                                                                forq.fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1)  and
                                                                qgrp.fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1)
                                                                order by  qgrp.pk_question_grp,ques.pk_questions )
                                                                loop
                                                                    --dbms_output.put_line('question pk'||mrq_data.pk_ques);


                                                                    select PK_CODELST into IS_LABEL_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='label';

                                                                    if mrq_data.DEPENT_QUES_PK='0' then
                                                                           if(mrq_data.RESPONSE_CODE!='0' or mrq_data.dynaformDate!='0') then
                                                                           --dbms_output.put_line('data is for non dependent question::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                           mrq_compl_flag:=1;
                                                                           end if;

                                                                           if(mrq_data.RESPONSE_CODE='0' and mrq_data.dynaformDate='0') or ( mrq_data.assesment_value=0 ) then
                                                                              if(mrq_data.RESPONSE_TYPE_PK!=IS_LABEL_TYP) then
                                                                                --dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                                mrq_compl_flag:=0;
                                                                                EXIT;
                                                                              end if;
                                                                           end if;


                                                                    end if;

                                                                    --dbms_output.put_line('data is there::::'||mrq_data.pk_ques);

                                                                   if mrq_data.DEPENT_QUES_PK!='0' then
                                                                         --dbms_output.put_line('master question value::::'||mrq_data.DEPENT_QUES_PK);
                                                                         select count(*) into  MASTER_QUES_DATA_IS_THER_MRQ from cb_form_responses formres2 where formres2.FK_QUESTION = mrq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='MRQ' and cb_forms.is_current_flag=1));
                                                                         --dbms_output.put_line('master question value::::'||mrq_masterques_response);
                                                                         IF MASTER_QUES_DATA_IS_THER_MRQ>0 THEN
                                                                          select NVL(FORMRES2.RESPONSE_CODE,'0') into  mrq_masterques_response from cb_form_responses formres2 where formres2.FK_QUESTION = mrq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='MRQ' and cb_forms.is_current_flag=1));
                                                                         END IF;
                                                                         IF MASTER_QUES_DATA_IS_THER_MRQ=0 THEN
                                                                            mrq_masterques_response:='0';
                                                                         END IF;
                                                                         --dbms_output.put_line('master question value::::'||mrq_masterques_response);
                                                                         if mrq_masterques_response!='0'  and instr(mrq_data.DEPENT_QUES_VALUE, mrq_masterques_response) !=0  then

                                                                            if mrq_data.RESPONSE_CODE != '0' or mrq_data.dynaformDate != '0' or mrq_data.RESPONSE_VAL is not null then
                                                                              mrq_compl_flag:=1;
                                                                              --dbms_output.put_line('required data in child question::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                            end if;

                                                                            if(mrq_data.RESPONSE_CODE = '0' and mrq_data.dynaformDate = '0' and mrq_data.RESPONSE_VAL='0') or ( mrq_data.assesment_value=0 ) then
                                                                              --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1::::::::::::'||mrq_data.RESPONSE_CODE ||' '||mrq_data.dynaformDate);
                                                                              --dbms_output.put_line('Inside exit of condit 1::::::::::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                              mrq_compl_flag:=0;
                                                                              EXIT;
                                                                            end if;

                                                                         end if;
                                                          --               if(mrq_masterques_response='0' and instr(mrq_data.DEPENT_QUES_VALUE, mrq_masterques_response)=0) then
                                                          --                     dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 2::::::::::::'||mrq_data.DEPENT_QUES_VALUE||' '||instr(mrq_data.DEPENT_QUES_VALUE, mrq_masterques_response));
                                                          --                    mrq_compl_flag:=0;
                                                          --                    EXIT;
                                                          --               end if;

                                                                   end if;

                                                                end loop;
                                                  END IF;

                                              END IF;

                                              --dbms_output.put_line('mrq_compl_flag FOR UNLICENSED CORD::::'||mrq_compl_flag);


                              END IF;
                                               --- FMHQ TASK FOR UNLICENSED SYS CORD -----


                         SELECT COUNT(*) INTO IS_FMHQ_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1);
			 SELECT COUNT(*) INTO IS_OTHER_VER_FMHQ_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG<>1);

                         select PK_CODELST into IS_TXT_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='txtbox';
                         select PK_CODELST into IS_MULTISELL_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='mulselc';
                         select PK_CODELST into IS_DRPDOWN_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='dropdown';
                        

                         IF CORD_UNLIC_FLAG=1 AND V_CONSIDER_ELIG_FMHQ=1 THEN 

						
					    IF IS_FMHQ_DATA=0 AND IS_OTHER_VER_FMHQ_DATA>0 THEN
                                             fmhq_compl_flag:=1;
					    END IF;                         
						
                                            IF IS_FMHQ_DATA > 0 AND IS_OTHER_VER_FMHQ_DATA=0 THEN

                                                  SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_FMHQ_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1));

                                                   IF IS_FMHQ_DATA1=1 THEN

                                                               for fmhq_data in ( select
                                                                  ques.pk_questions pk_ques,
                                                                  forq.ques_seq QUES_SEQ,
                                                                  NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                                                                  NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                                                                  f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                                                                  f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                                                                  forq.fk_master_ques MASTER_QUES,
                                                                  ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                                                                  nvl(formres.resp_code,'0') RESPONSE_CODE,
                                                                  case
                                                                      when formres.resp_val is null then '0'
                                                                      when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                                                      when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                                                      when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                                                                  end RESPONSE_VAL,
                                                                  NVL(formres.resp_val,'0') RESPONSE_VAL12,
                                                                  formres.fk_form_responses FK_FORM_RESP,
                                                                  nvl(formres.dynaformDate,'0') DYNAFORMDATE,
                                                                  ques.unlicen_req_flag UNLIC_REQ_FLAG,
                                                                  ques_desc QUES_DESC,
                                                                  formres.fk_form_version fk_form_vrson,
                                                                  formres.pk_cordid pk_cordid,
                                                                  qgrp.FK_QUESTION_GROUP fk_ques_grp,
                                                                  nvl(ques.response_type,'') RESPONSE_TYPE_PK,
                                                                  forq.UNEXPECTED_RESPONSE_VALUE unexpected_resp,
                                                                  NVL(formres.assess_reason2,0) response_in_asses,
                                                                  NVL(formres.asses_txt_remark1,'0') response_txt_asses,
                                                                  ques.assesment_flag quest_assess_flag,
                                                                  formres.pk_form_responses pkres
                                                                  from
                                                                  cb_form_questions forq,
                                                                  cb_question_grp qgrp,
                                                                  cb_questions ques
                                                                  left outer join
                                                                  (select
                                                                      frmres.pk_form_responses,
                                                                      frmres.fk_question,
                                                                      frmres.response_code resp_code,
                                                                      frmres.response_value resp_val,
                                                                      frmres.comments,
                                                                      frmres.fk_form_responses,
                                                                      asses.pk_assessment pk_assessment,
                                                                      f_codelst_desc(asses.assessment_for_response) assess_response,
                                                                      asses.assessment_remarks assess_remarks,
                                                                      to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
                                                                      frmres.fk_form_version fk_form_version,
                                                                      asses.assess_reason1 assess_reason2,
                                                                      asses.asses_txt_remark asses_txt_remark1,
                                                                      frmres.entity_id pk_cordid
                                                                    from
                                                                        cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response,cb_assessment.assessment_reason assess_reason1,cb_assessment.assessment_reason_remarks asses_txt_remark from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='FMHQ')) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                                                    where
                                                                        frmres.entity_id=PK_CORDID and
                                                                        frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1)) and
                                                                        frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                                                                  on(formres.FK_QUESTION=ques.pk_questions)
                                                                  where
                                                                  ques.pk_questions=forq.fk_question and
                                                                  ques.pk_questions=qgrp.fk_question and
                                                                  forq.fk_form=(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1)  and
                                                                  qgrp.fk_form=(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1)
                                                                  order by  qgrp.pk_question_grp,ques.pk_questions )
                                                                  loop


                                                                      if( fmhq_data.quest_assess_flag=1 and (fmhq_data.RESPONSE_CODE!='0' or fmhq_data.RESPONSE_VAL12!='0') and (f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='multiselect' or f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='dropdown' or f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='textfield')) then

                                                                             --dbms_output.put_line('INSIDE CHECKING ASSESSMENT 1');

                                                                             if f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='dropdown' then


                                                                                   for unexpected_res in ( select codelst_desc INTO FMHQ_UNEXPECTED_CODELST_DESC from er_codelst where codelst_type =fmhq_data.unexpected_resp)
                                                                                   LOOP
                                                                                            --DBMS_OUTPUT.put_line('unexpected_res: ' || unexpected_res.codelst_desc);
                                                                                            if unexpected_res.codelst_desc=f_codelst_desc(fmhq_data.RESPONSE_CODE) then
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)=f_codelst_desc(fmhq_data.RESPONSE_CODE) then

                                                                                                      ASSESS_PASSED:=1;
                                                                                                   end if;
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)!=f_codelst_desc(fmhq_data.RESPONSE_CODE) then

                                                                                                      ASSESS_PASSED:=0;
                                                                                                      exit;
                                                                                                   end if;
                                                                                                   if fmhq_data.response_in_asses=0 then

                                                                                                      ASSESS_PASSED:=0;
                                                                                                      exit;

                                                                                                   end if;
                                                                                            end if;
                                                                                            if unexpected_res.codelst_desc!=f_codelst_desc(fmhq_data.RESPONSE_CODE) then
                                                                                                ASSESS_PASSED:=1;
                                                                                            end if;
                                                                                   END LOOP;
                                                                                   --dbms_output.put_line('1.....FMHQ_UNEXPECTED_CODELST_DESC:::::::::::::::::'||FMHQ_UNEXPECTED_CODELST_DESC||'ASSESS_PASSED::::'||fmhq_data.QUES_SEQ ||' '||ASSESS_PASSED);

                                                                            end if;


                                                                            if f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='multiselect' and ( LENGTH(fmhq_data.RESPONSE_VAL12) - LENGTH( REPLACE(fmhq_data.RESPONSE_VAL12, ',', '') ) )=0 then

                                                                                   --dbms_output.put_line('INSIDE CHECKING ASSESSMENT 2');
                                                                                   
                                                                                   for unexpected_res1 in ( select codelst_desc INTO FMHQ_UNEXPECTED_CODELST_DESC from er_codelst where codelst_type =fmhq_data.unexpected_resp)
                                                                                   LOOP
                                                                                            --DBMS_OUTPUT.put_line('unexpected_res: ' || unexpected_res1.codelst_desc);
                                                                                            if unexpected_res1.codelst_desc=f_codelst_desc(fmhq_data.RESPONSE_VAL12) then
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)=f_codelst_desc(fmhq_data.RESPONSE_VAL12) then
                                                                                                      ASSESS_PASSED:=1;
                                                                                                   end if;
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)!=f_codelst_desc(fmhq_data.RESPONSE_VAL12) then
                                                                                                      ASSESS_PASSED:=0;
                                                                                                      --exit;
                                                                                                   end if;
                                                                                                   if fmhq_data.response_in_asses=0 then
                                                                                                      ASSESS_PASSED:=0;
                                                                                                      --exit;

                                                                                                   end if;
                                                                                            end if;
                                                                                   END LOOP;
                                                                                   --dbms_output.put_line('2.....FMHQ_UNEXPECTED_CODELST_DESC:::::::::::::::::'||FMHQ_UNEXPECTED_CODELST_DESC||'ASSESS_PASSED::::'||fmhq_data.QUES_SEQ ||' '||ASSESS_PASSED);

                                                                            end if;


                                                                             if f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='multiselect' and ( LENGTH(fmhq_data.RESPONSE_VAL12) - LENGTH( REPLACE(fmhq_data.RESPONSE_VAL12, ',', '') ) )>0 then

                                                                                   --dbms_output.put_line('INSIDE CHECKING ASSESSMENT 3');
                                                                                   --DBMS_OUTPUT.put_line('3 data for multiselect with multiple values' ||fmhq_data.RESPONSE_VAL12);
                                                                                   FMHQ_RESP_NEED_ASSESS:=0;
                                                                                   FMHQ_RESP_GOT_ASSESS:=0;
                                                                                   
                                                                                           select COUNT(*) INTO FMHQ_UNEXPECTED_RESP_COUNT from er_codelst where codelst_type =fmhq_data.unexpected_resp;


                                                                                            v_split_string := emb_string.split_string(fmhq_data.RESPONSE_VAL12);
                                                                                            if v_split_string.count > 0 then
                                                                                            for i in 1..v_split_string.count
                                                                                            loop
                                                                                                       select f_codelst_desc(v_split_string(i)) into temp_string from dual; 
                                                                                                       for unexpected_res2 in ( select codelst_desc INTO FMHQ_UNEXPECTED_CODELST_DESC from er_codelst where codelst_type =fmhq_data.unexpected_resp)
                                                                                                       LOOP
                                                                                                                                                                                                           
                                                                                                        if unexpected_res2.codelst_desc=temp_string then
                                                                                                        
                                                                                                             FMHQ_RESP_NEED_ASSESS:=FMHQ_RESP_NEED_ASSESS+1;
                                                                                                          
                                                                                                              select COUNT(*) INTO FMHQ_ASSESS_DATA_COUNT from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='FMHQ') and sub_entity_id=fmhq_data.pkres;
                                                                                                              
                                                                                                              IF FMHQ_ASSESS_DATA_COUNT>0 THEN 
                                                                                                              
                                                                                                                     FOR FMHQ_ASSESS_DATAVALUES in (  select cb_assessment.assessment_reason assessment_reason from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='FMHQ') and sub_entity_id=fmhq_data.pkres)
                                                                                                                     LOOP
                                                                                                                     
                                                                                                                     --dbms_output.put_line('ASSESS VALUE::::'||f_codelst_desc(FMHQ_ASSESS_DATAVALUES.assessment_reason));
                                                                                                                     
                                                                                                                     if fmhq_data.response_in_asses!=0 AND f_codelst_desc(FMHQ_ASSESS_DATAVALUES.assessment_reason)=temp_string then
                                                                                                                        FMHQ_RESP_GOT_ASSESS:=FMHQ_RESP_GOT_ASSESS+1;
                                                                                                                        --dbms_output.put_line('Inside assessment set 1::: Assessment Passed');
                                                                                                                  --exit;
                                                                                                                      end if;
                                                                                                                     
                                                                                                                     
                                                                                                                     END LOOP;
                                                                                                              END IF;

--                                                                                                        --and instr(f_codelst_desc(fmhq_data.response_in_asses),temp_string) !=0 then
--                                                                                                               dbms_output.put_line(' '||fmhq_data.QUES_SEQ||' inside correct way...............................................RESP IN ASS'||fmhq_data.response_in_asses||'check responval::'||temp_string||'::::unexpected Respons::::'||f_codelst_desc(fmhq_data.response_in_asses)||':::::::::::');
--                                                                                                               if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)=temp_string then
--                                                                                                                  ASSESS_PASSED:=1;
--                                                                                                                  dbms_output.put_line('Inside assessment set 1::: Assessment Passed');
--                                                                                                                  --exit;
--                                                                                                               end if;
--                                                                                                               if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)!=temp_string then
--                                                                                                                  ASSESS_PASSED:=0;
--                                                                                                                  dbms_output.put_line('1. Inside assessment set 0::: Assessment Not Passed'||'f_codelst_desc(fmhq_data.response_in_asses)::::::'||f_codelst_desc(fmhq_data.response_in_asses)||'::::::'||temp_string||'::::::::');
--                                                                                                                  --exit;
--                                                                                                               end if;
--                                                                                                               if fmhq_data.response_in_asses=0 then
--                                                                                                                  ASSESS_PASSED:=0;
--                                                                                                                  dbms_output.put_line('2. Inside assessment set 0::: Assessment Not Passed');
--                                                                                                                  --exit;
--                                                                                                               end if;
                                                                                                        end if;

                                                                                                        
                                                                                                        END LOOP;

                                                                                            end loop;
                                                                                            end if;
                                                                                            
                                                                                            
                                                                                            IF FMHQ_RESP_NEED_ASSESS<>0 AND FMHQ_RESP_GOT_ASSESS<>0 AND FMHQ_RESP_NEED_ASSESS=FMHQ_RESP_GOT_ASSESS THEN
                                                                                                    ASSESS_PASSED:=1;
                                                                                                    --dbms_output.put_line('Inside assessment set 1::: Assessment Passed:::'|| fmhq_data.QUES_SEQ);
                                                                                            END IF;
                                                                                            
                                                                                            IF FMHQ_RESP_NEED_ASSESS<>0 AND FMHQ_RESP_GOT_ASSESS<>0 AND FMHQ_RESP_NEED_ASSESS!=FMHQ_RESP_GOT_ASSESS THEN
                                                                                                    ASSESS_PASSED:=0;
                                                                                                    --dbms_output.put_line('1. Inside assessment set 0::: Assessment Not Passed::::f_codelst_desc(fmhq_data.response_in_asses)::::::'||f_codelst_desc(fmhq_data.response_in_asses)||'::::::'||temp_string||'::::::::');
                                                                                            END IF;
                                                                                            
                                                                                            IF FMHQ_RESP_NEED_ASSESS=0 AND FMHQ_RESP_GOT_ASSESS=0 THEN
                                                                                                   ASSESS_PASSED:=1;
                                                                                                   --dbms_output.put_line('3::: Assessment Passed:::'|| fmhq_data.QUES_SEQ);
                                                                                            END IF;
                                                                                            
                                                                                            IF FMHQ_RESP_NEED_ASSESS>0 AND FMHQ_ASSESS_DATA_COUNT=0 THEN
                                                                                                     ASSESS_PASSED:=0;
                                                                                                     --dbms_output.put_line('4. Inside assessment set 0::: Assessment Not Passed');
                                                                                            END IF;
                                                                    

                                                                                end if;
                                                                                

                                                                                if f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='textfield'  then

                                                                                            if fmhq_data.RESPONSE_VAL12!='0' then

                                                                                                            if fmhq_data.RESPONSE_VAL12=fmhq_data.response_txt_asses then
                                                                                                                      ASSESS_PASSED:=1;
                                                                                                            end if;
                                                                                                            if fmhq_data.RESPONSE_VAL12!=fmhq_data.response_txt_asses then
                                                                                                                ASSESS_PASSED:=0;
                                                                                                            end if;
                                                                                                            --dbms_output.put_line('INSIDE CONDITION 1');

                                                                                             end if;
                                                                                             if fmhq_data.RESPONSE_VAL12='0' then

                                                                                                                ASSESS_PASSED:=0;
                                                                                                                --dbms_output.put_line('INSIDE CONDITION 2');

                                                                                             end if;
                                                                                end if;


                                                                                --dbms_output.put_line('QUES NO:::::::::::::::::'||fmhq_data.QUES_SEQ ||'::ASSES VALUE:: '||ASSESS_PASSED);


                                                                      end if;

                                                                     ---dbms_output.put_line('ASSESS_PASSED::::'||fmhq_data.QUES_SEQ ||':flag: '||ASSESS_PASSED);



                                                                      if fmhq_data.DEPENT_QUES_PK='0' then
                                                                             if(fmhq_data.RESPONSE_CODE!='0' or fmhq_data.dynaformDate!='0') then
                                                                             --dbms_output.put_line('data is for non dependent question::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                             fmhq_compl_flag:=1;
                                                                             end if;


                                                                             if(fmhq_data.RESPONSE_CODE='0' and fmhq_data.dynaformDate='0') or ASSESS_PASSED=0 then
                                                                                  --dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                                  fmhq_compl_flag:=0;
                                                                                  EXIT;

                                                                             end if;


                                                                      end if;

                                                                     if fmhq_data.DEPENT_QUES_PK!='0' then




                                                                         select
                                                                              count(*) fmhq_res_type INTO MASTER_QUES_DATA_IS_THER_FMHQ
                                                                              from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1)) and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;


                                                                          IF MASTER_QUES_DATA_IS_THER_FMHQ>0 THEN

                                                                              select
                                                                              CASE
                                                                              WHEN ques2.RESPONSE_TYPE=IS_TXT_TYP OR ques2.RESPONSE_TYPE=IS_MULTISELL_TYP THEN 0
                                                                              ELSE 1
                                                                              END INTO fmhq_res_type
                                                                              from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1)) and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;

                                                                              --dbms_output.put_line('response from multiselect or textfield'||fmhq_masterques_response);


                                                                             IF fmhq_res_type=0 THEN
                                                                                  select
                                                                                  NVL(FORMRES2.RESPONSE_VALUE,'0') INTO fmhq_masterques_response
                                                                                  from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1))  and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;
                                                                             END IF;


                                                                             IF fmhq_res_type=1 THEN
                                                                                select
                                                                                  NVL(FORMRES2.RESPONSE_CODE,'0') INTO fmhq_masterques_response
                                                                                  from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1))  and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;

                                                                             END IF;

                                                                    END IF;

                                                                    IF MASTER_QUES_DATA_IS_THER_FMHQ=0 THEN
                                                                              fmhq_masterques_response:='0';
                                                                    END IF;


                                                                          --dbms_output.put_line('master question value::::'||fmhq_data.DEPENT_QUES_VALUE||'response from previous codit::::::;'||fmhq_masterques_response||'CONDITION RESULT:::::::'||instr(fmhq_masterques_response,fmhq_data.DEPENT_QUES_VALUE));

                                                                           if fmhq_masterques_response!='0'  and instr(fmhq_masterques_response,fmhq_data.DEPENT_QUES_VALUE) !=0 then

                                                                              if fmhq_data.RESPONSE_CODE != '0' or fmhq_data.dynaformDate != '0' or fmhq_data.RESPONSE_VAL12 !='0' then
                                                                                fmhq_compl_flag:=1;
                                                                                --dbms_output.put_line('required data in child question::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                              end if;

                                                                              if(fmhq_data.RESPONSE_CODE = '0' and fmhq_data.dynaformDate = '0' and fmhq_data.RESPONSE_VAL='0')  or ASSESS_PASSED=0   then
                                                                                --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1::::::::::::'||fmhq_data.RESPONSE_CODE ||' '||fmhq_data.dynaformDate);
                                                                                --dbms_output.put_line('Inside exit of condit 1::::::::::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                                fmhq_compl_flag:=0;
                                                                                EXIT;
                                                                              end if;

                                                                           end if;
                                                            --               if(fmhq_masterques_response='0' and instr(fmhq_data.DEPENT_QUES_VALUE, fmhq_masterques_response)=0) then
                                                            --                     dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 2::::::::::::'||fmhq_data.DEPENT_QUES_VALUE||' '||instr(fmhq_data.DEPENT_QUES_VALUE, fmhq_masterques_response));
                                                            --                    fmhq_compl_flag:=0;
                                                            --                    EXIT;
                                                            --               end if;

                                                                     end if;

                                                                  end loop;
                                                    END IF;

                                                END IF;

                                                --dbms_output.put_line('fmhq_compl_flag FOR UNLICENSED CORD::::'||fmhq_compl_flag);

                        END IF;

        END IF;



        IF IS_SYSTEM_USER=0 THEN

                           ---IDS TASKS FOR NON-SYS CORD---

                           SELECT CASE WHEN CORD_REGISTRY_ID IS NOT NULL AND (CORD_ID_NUMBER_ON_CBU_BAG!='-1' AND CORD_ID_NUMBER_ON_CBU_BAG IS NOT NULL) THEN 1 ELSE 0 END IDSFLAG INTO IDS_COMPL_FLAG
                           FROM CB_CORD
                           WHERE PK_CORD IN (PK_CORDID);

                           --DBMS_OUTPUT.PUT_LINE('IDS INFORMATION FLAG FOR NON-SYS CORD VALUE::::::::::::'||IDS_COMPL_FLAG);

                           --LICENSURE TASK FOR NON-SYS CORD---

                           IF V_CONS_UNLIC_REASON=1 THEN  

		                  SELECT
		                   CASE
		                          WHEN CRD.FK_CORD_CBU_LIC_STATUS IS NOT NULL AND CRD.FK_CORD_CBU_LIC_STATUS !=-1 THEN
		                                                                 CASE
		                                                                      WHEN CRD.FK_CORD_CBU_LIC_STATUS IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='licence' AND CODELST_SUBTYP='unlicensed') THEN
		                                                                        CASE
		                                                                          WHEN (SELECT COUNT(*) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD IN (PK_CORDID))>0 THEN 1
		                                                                          ELSE 0
		                                                                        END
		                                                                      ELSE 1
		                                                                 END
		                         ELSE 0
		                   END INTO LIC_COMPL_FLAG
		                   FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);

                           END IF;

			  IF V_CONS_UNLIC_REASON=0 THEN  

		                   SELECT
		                   CASE
		                          WHEN CRD.FK_CORD_CBU_LIC_STATUS IS NOT NULL AND CRD.FK_CORD_CBU_LIC_STATUS !=-1 THEN 1         
		                          ELSE 0
		                   END INTO LIC_COMPL_FLAG
		                   FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);

                           END IF;

           END IF;





                          --dbms_output.put_line('OLD FLAG VALUES::::IDS_COMPL_FLAG::::::'||OLD_IDS_COMPL_FLAG||'CBUINFO_COMPL_FLAG:::::::'||OLD_CBUINFO_COMPL_FLAG||'LAB_SUMMAR_COMP1_FLAG:::::::::'||OLD_LAB_SUMMAR_COMP1_FLAG||'mrq_compl_flag::::::::::::'||old_mrq_compl_flag||'fmhq_compl_flag::::::::::::'||old_fmhq_compl_flag||'idm_compl_flag::::::::::::'||old_idm_compl_flag||'CBU_PROCESSING_FLAG::::::::::::::'||OLD_PROS_COMPL_FLAG||'LIC_COMPL_FLAG:::::::::::::'||OLD_LIC_COMPL_FLAG||'DONT_ALLOW_INSERT_FLAG:::::::::'||DONT_ALLOW_INSERT_FLAG);
                          --dbms_output.put_line('NEW FLAG VALUES::::IDS_COMPL_FLAG::::::'||IDS_COMPL_FLAG||'CBUINFO_COMPL_FLAG:::::::'||CBUINFO_COMPL_FLAG||'LAB_SUMMAR_COMP1_FLAG:::::::::'||LAB_SUMMAR_COMP1_FLAG||'mrq_compl_flag::::::::::::'||mrq_compl_flag||'fmhq_compl_flag::::::::::::'||fmhq_compl_flag||'idm_compl_flag::::::::::::'||idm_compl_flag||'CBU_PROCESSING_FLAG::::::::::::::'||PROS_COMPL_FLAG||'LIC_COMPL_FLAG:::::::::::::'||LIC_COMPL_FLAG);


                        IF IS_SYSTEM_USER=0  THEN

                              IF(IDS_COMPL_FLAG=1 AND LIC_COMPL_FLAG=1) THEN

                                          CRI_COMP_FLAG:=1;

                              END IF;

                              IF(IDS_COMPL_FLAG=0 OR LIC_COMPL_FLAG=0) THEN

                                          CRI_COMP_FLAG:=0;

                              END IF;

                        END IF;

                        


                        IF V_CONSIDER_ELIG_FMHQ=1 THEN 

		                IF IS_SYSTEM_USER=1  AND CORD_UNLIC_FLAG=1 THEN

		                      IF(IDS_COMPL_FLAG=1 AND CBUINFO_COMPL_FLAG=1 AND PROS_COMPL_FLAG=1 AND LAB_SUMMAR_COMP1_FLAG=1 AND mrq_compl_flag=1 AND idm_compl_flag=1 AND LIC_COMPL_FLAG=1 AND fmhq_compl_flag=1 AND ELIG_COMPL_FLAG=1) THEN

		                              CRI_COMP_FLAG:=1;


		                      END IF;
		                      IF(IDS_COMPL_FLAG=0 OR CBUINFO_COMPL_FLAG=0 OR PROS_COMPL_FLAG=0 OR LAB_SUMMAR_COMP1_FLAG=0 OR mrq_compl_flag=0 OR idm_compl_flag=0 OR LIC_COMPL_FLAG=0 OR fmhq_compl_flag=0 OR ELIG_COMPL_FLAG=0 ) THEN

		                              CRI_COMP_FLAG:=0;


		                      END IF;

		                 END IF;


		                  IF IS_SYSTEM_USER=1 AND CORD_LIC_FLAG=1 THEN

		                      IF(IDS_COMPL_FLAG=1 AND CBUINFO_COMPL_FLAG=1 AND PROS_COMPL_FLAG=1 AND LAB_SUMMAR_COMP1_FLAG=1 AND idm_compl_flag=1  AND LIC_COMPL_FLAG=1 AND ELIG_COMPL_FLAG=1) THEN

		                              CRI_COMP_FLAG:=1;

		                      END IF;
		                      IF(IDS_COMPL_FLAG=0 OR CBUINFO_COMPL_FLAG=0 OR PROS_COMPL_FLAG=0 OR LAB_SUMMAR_COMP1_FLAG=0 OR idm_compl_flag=0 OR LIC_COMPL_FLAG=0 OR ELIG_COMPL_FLAG=0) THEN

		                              CRI_COMP_FLAG:=0;

		                      END IF;

		                 END IF;

                       END IF;

		       IF V_CONSIDER_ELIG_FMHQ=0  THEN 


				IF IS_SYSTEM_USER=1  AND CORD_UNLIC_FLAG=1 THEN

		                      IF(IDS_COMPL_FLAG=1 AND CBUINFO_COMPL_FLAG=1 AND PROS_COMPL_FLAG=1 AND LAB_SUMMAR_COMP1_FLAG=1 AND mrq_compl_flag=1 AND idm_compl_flag=1 AND LIC_COMPL_FLAG=1 ) THEN

		                              CRI_COMP_FLAG:=1;


		                      END IF;
		                      IF(IDS_COMPL_FLAG=0 OR CBUINFO_COMPL_FLAG=0 OR PROS_COMPL_FLAG=0 OR LAB_SUMMAR_COMP1_FLAG=0 OR mrq_compl_flag=0 OR idm_compl_flag=0 OR LIC_COMPL_FLAG=0 ) THEN

		                              CRI_COMP_FLAG:=0;


		                      END IF;

		                 END IF;


		                  IF IS_SYSTEM_USER=1 AND CORD_LIC_FLAG=1 THEN

		                      IF(IDS_COMPL_FLAG=1 AND CBUINFO_COMPL_FLAG=1 AND PROS_COMPL_FLAG=1 AND LAB_SUMMAR_COMP1_FLAG=1 AND idm_compl_flag=1  AND LIC_COMPL_FLAG=1 ) THEN

		                              CRI_COMP_FLAG:=1;

		                      END IF;
		                      IF(IDS_COMPL_FLAG=0 OR CBUINFO_COMPL_FLAG=0 OR PROS_COMPL_FLAG=0 OR LAB_SUMMAR_COMP1_FLAG=0 OR idm_compl_flag=0 OR LIC_COMPL_FLAG=0) THEN

		                              CRI_COMP_FLAG:=0;

		                      END IF;

		                 END IF;

                       END IF;




                      IF is_data_for_cord!=0 THEN

                          IF OLD_IDS_COMPL_FLAG!=IDS_COMPL_FLAG  THEN    DONT_ALLOW_INSERT_FLAG:=0; END IF;

                          IF OLD_CBUINFO_COMPL_FLAG!=CBUINFO_COMPL_FLAG  THEN   DONT_ALLOW_INSERT_FLAG:=0; END IF;

                          IF OLD_PROS_COMPL_FLAG!=PROS_COMPL_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;

                          IF OLD_LAB_SUMMAR_COMP1_FLAG!=LAB_SUMMAR_COMP1_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0; END IF;

                          IF OLD_LIC_COMPL_FLAG!=LIC_COMPL_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;

                          IF OLD_ELIG_COMPL_FLAG!=ELIG_COMPL_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;

                          IF old_idm_compl_flag!=idm_compl_flag  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;

                          IF old_mrq_compl_flag!=mrq_compl_flag  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;

                          IF old_fmhq_compl_flag!=fmhq_compl_flag THEN DONT_ALLOW_INSERT_FLAG:=0; END IF;

                          IF old_cri_compl_flag!=CRI_COMP_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0; END IF;

                     END IF;

                     IF is_data_for_cord=0 THEN
                          DONT_ALLOW_INSERT_FLAG:=0;
                     END IF;

                     DBMS_OUTPUT.PUT_LINE('DONT_ALLOW_INSERT_FLAG::::::::::::'||DONT_ALLOW_INSERT_FLAG);


                     IF (IS_SYSTEM_USER=0  OR IS_SYSTEM_USER=1)  AND DONT_ALLOW_INSERT_FLAG=0 THEN

					
					                                DBMS_OUTPUT.PUT_LINE('INSIDE INSERTION::::::::::::');				  
      
                                          Insert into CB_CORD_COMPLETE_REQ_INFO (PK_CORD_COMPLETE_REQINFO,COMPLETE_REQ_INFO_FLAG,IDS_FLAG,ELIGIBILITY_FLAG,LICENSURE_FLAG,CBU_INFO_FLAG,CBU_PROCESSING_FLAG,LAB_SUMMARY_FLAG,MRQ_FLAG,FMHQ_FLAG,IDM_FLAG,FK_CORD_ID,FK_ORDER_ID,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG) values
                                                                                 (SEQ_CB_CORD_COMPLETE_REQ_INFO.nextval,CRI_COMP_FLAG,IDS_COMPL_FLAG,ELIG_COMPL_FLAG,LIC_COMPL_FLAG,CBUINFO_COMPL_FLAG,PROS_COMPL_FLAG,LAB_SUMMAR_COMP1_FLAG,mrq_compl_flag,fmhq_compl_flag,idm_compl_flag,PK_CORDID,null,null,sysdate,null,null,null,null,null);
                                          commit;
                    END IF;
                    
                    
                    
                    
                  IF V_CONSIDER_ELIG_FMHQ=1 AND V_CRI_COMP_TASK_FLAG='Y' AND old_cri_compl_flag=1 AND CRI_COMP_FLAG=0  THEN
                      
                       UPDATE ER_ORDER SET COMPLETE_REQ_INFO_TASK_FLAG='N' WHERE PK_ORDER=ORDER_ID_VAL;
                       DBMS_OUTPUT.PUT_LINE('INSIDE RESET CRI::::::::::::'||OLD_PK_CRI);
                      COMMIT;
                  
                  END IF;
        
                  IF V_CONSIDER_ELIG_FMHQ=1 AND V_FCR_COMP_TASK_FLAG='Y' AND old_cri_compl_flag=1 AND CRI_COMP_FLAG=0 THEN
                  
                      SELECT COUNT(*) INTO IS_FCR_DATA FROM CB_CORD_FINAL_REVIEW FREVIEW WHERE FREVIEW.FK_CORD_ID=PK_CORDID;
                      
                      IF IS_FCR_DATA>0 THEN
                        UPDATE CB_CORD_FINAL_REVIEW SET LICENSURE_CONFIRM_FLAG=0,ELIGIBLE_CONFIRM_FLAG=0,FINAL_REVIEW_CONFIRM_FLAG=NULL,REVIEW_CORD_ACCEPTABLE_FLAG=NULL WHERE PK_CORD_FINAL_REVIEW=(SELECT MAX(PK_CORD_FINAL_REVIEW) FROM CB_CORD_FINAL_REVIEW WHERE FK_CORD_ID=PK_CORDID);
                        UPDATE ER_ORDER SET FINAL_REVIEW_TASK_FLAG='N' WHERE PK_ORDER=ORDER_ID_VAL;
                        COMMIT;
                          DBMS_OUTPUT.PUT_LINE('INSIDE RESET FCR::::::::::::');
                      END IF;

                  END IF;






                          --DBMS_OUTPUT.PUT_LINE('LICENCE COMPLETE FLAG FOR NON-SYS CORD VALUE::::::::::::'||LIC_COMPL_FLAG);



EXCEPTION

   WHEN NO_DATA_FOUND THEN dbms_output.put_line ('NO DATA FOUND IN SP_CRI_TASKS');

END;
/