CREATE OR REPLACE TRIGGER ER_ORDER_ATTACHMENTS_AU1 AFTER UPDATE ON ER_ORDER_ATTACHMENTS 
referencing OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */
    NEW_LAST_MODIFIED_BY VARCHAR2(200);
    OLD_LAST_MODIFIED_BY VARCHAR2(200);
    NEW_CREATOR VARCHAR2(200);
    OLD_CREATOR VARCHAR2(200);
BEGIN
    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;
    IF NVL(:OLD.CREATOR,0) !=  NVL(:NEW.CREATOR,0) THEN
    BEGIN
           SELECT F_GETUSER(:NEW.CREATOR) INTO NEW_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_CREATOR := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.CREATOR) INTO OLD_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_CREATOR := '';
   END;
   END IF;
   IF NVL(:OLD.LAST_MODIFIED_BY,0) !=  NVL(:NEW.LAST_MODIFIED_BY,0) THEN
    BEGIN
            SELECT F_GETUSER(:NEW.LAST_MODIFIED_BY) INTO NEW_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_LAST_MODIFIED_BY := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.LAST_MODIFIED_BY) INTO OLD_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_LAST_MODIFIED_BY := '';
   END;
   END IF;
    pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER_ATTACHMENTS',:OLD.rid,:OLD.PK_ORDER_ATTACHMENT,'U',:NEW.LAST_MODIFIED_BY);
    
    IF NVL(:OLD.PK_ORDER_ATTACHMENT,0) != NVL(:NEW.PK_ORDER_ATTACHMENT,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'PK_ORDER_ATTACHMENT',:OLD.PK_ORDER_ATTACHMENT, :NEW.PK_ORDER_ATTACHMENT,NULL,NULL);
  END IF;
     IF NVL(:OLD.FK_ORDER,0) !=  NVL(:NEW.FK_ORDER,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'FK_ORDER',:OLD.FK_ORDER, :NEW.FK_ORDER,NULL,NULL);
  END IF;
   IF NVL(:OLD.FK_ATTACHMENT,0) != NVL(:NEW.FK_ATTACHMENT,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'FK_ATTACHMENT',:OLD.FK_ATTACHMENT, :NEW.FK_ATTACHMENT,NULL,NULL);
  END IF;
    IF NVL(:OLD.CREATOR,0) !=   NVL(:NEW.CREATOR,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'CREATOR', OLD_CREATOR, NEW_CREATOR,NULL,NULL);
  END IF;
   IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=  NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
 IF nvl(:OLD.LAST_MODIFIED_BY,0) != nvl(:NEW.LAST_MODIFIED_BY,0) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS','LAST_MODIFIED_BY',OLD_LAST_MODIFIED_BY,NEW_LAST_MODIFIED_BY,NULL,NULL);
      END IF;   
   IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
   IF NVL(:OLD.IP_ADD,' ') !=  NVL(:NEW.IP_ADD,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF;  
  IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
  END IF;
  IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_ATTACHMENTS', 'RID',:OLD.RID, :NEW.RID,NULL,NULL);
  END IF;  
  END;

/