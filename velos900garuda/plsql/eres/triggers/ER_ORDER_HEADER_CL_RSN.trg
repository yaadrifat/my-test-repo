create or replace
TRIGGER ER_ORDER_HEADER_CL_RSN AFTER UPDATE OF ORDER_CLOSE_REASON ON ER_ORDER_HEADER REFERENCING OLD AS OLDVAL NEW AS NEWVAL
FOR EACH ROW
DECLARE
VAR_ORD_TYPE NUMBER(10);
BEGIN

      --DBMS_OUTPUT.PUT_LINE('INSIDE TRIGGER'||:OLDVAL.PK_ORDER_HEADER);

      SELECT PK_CODELST INTO VAR_ORD_TYPE FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='ORDER_HEADER';

      IF ( NVL( :NEWVAL.ORDER_CLOSE_REASON , 0 ) > 0 ) THEN

         INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON)
                               VALUES (SEQ_CB_ENTITY_STATUS.nextval,:OLDVAL.PK_ORDER_HEADER,VAR_ORD_TYPE,'closed_reason',:NEWVAL.ORDER_CLOSE_REASON,SYSDATE,NULL,0,SYSDATE,NULL,NULL,NULL,NULL,NULL,NULL);
      END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN DBMS_OUTPUT.PUT_LINE('NO DATA FOUND IN COLUMN');
END;
/