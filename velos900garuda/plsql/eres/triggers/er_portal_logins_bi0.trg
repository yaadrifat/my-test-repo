CREATE OR REPLACE TRIGGER "ER_PORTAL_LOGINS_BI0" BEFORE INSERT ON ER_PORTAL_LOGINS
       REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
 BEGIN
  BEGIN
  usr := getuser(:NEW.creator);
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;

  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_PORTAL_LOGINS',erid, 'I',usr);
  insert_data:=:NEW.PK_PORTAL_LOGIN||'|'|| :NEW.PL_ID||'|'||
     :NEW.PL_ID_TYPE||'|'|| :NEW.PL_LOGIN||'|'|| :NEW.PL_PASSWORD||'|'||
     :NEW.PL_STATUS||'|'||:NEW.PL_LOGOUT_TIME||'|'|| :NEW.FK_PORTAL||'|'||
     :NEW.RID||'|'|| :NEW.CREATOR||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD;

  INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
  END ;
/


