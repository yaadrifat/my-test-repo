CREATE OR REPLACE TRIGGER "ER_FLDACTION_AD0" AFTER DELETE ON ER_FLDACTION
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Gopu for Audit Report After Delete
DECLARE
   raid number(10);
   deleted_data varchar2(2000);
BEGIN
   select seq_audit.nextval into raid from dual;
   audit_trail.record_transaction (raid, 'ER_FLDACTION', :old.rid, 'D');
   deleted_data :=
      to_char(:old.PK_FLDACTION) || '|' ||
      to_char(:old.FK_FORM) || '|' ||
      :old.FLDACTION_TYPE || '|' ||
      to_char(:old.FK_FIELD) || '|' ||
      :old.FLDACTION_CONDITION || '|' ||
      to_char(:old.rid) || '|' ||
      to_char(:old.creator) || '|' ||
      to_char(:old.last_modified_by) || '|' ||
      to_char(:old.last_modified_date) || '|' ||
      to_char(:old.created_on) || '|' ||
      :old.ip_add;
    insert into audit_delete (raid, row_data) values (raid, deleted_data);
END;
/


