CREATE OR REPLACE TRIGGER ER_PORTAL_BI0 BEFORE INSERT ON ER_PORTAL
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_PORTAL',erid, 'I',usr);

   insert_data:= :NEW.PK_PORTAL||'|'||:NEW.PORTAL_NAME||'|'||:NEW.FK_ACCOUNT||'|'|| :NEW.PORTAL_CREATED_BY||'|'||:NEW.PORTAL_DESC||'|'||:NEW.PORTAL_LEVEL||'|'||
   :NEW.PORTAL_NOTIFICATION_FLAG||'|'||:NEW.PORTAL_STATUS||'|'||
   :NEW.PORTAL_DISP_TYPE||'|'||:NEW.PORTAL_BGCOLOR||'|'||:NEW.PORTAL_TEXTCOLOR||'|'||:NEW.PORTAL_AUDITUSER||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)
   ||'|'||:NEW.IP_ADD||'|'||:NEW.FK_STUDY || '|' || to_char(:new.portal_selflogout) || '|' || to_char(:new.portal_createlogins) || '|' || :new.portal_defaultpass
   || '|' || :new.portal_consenting_form ;

   INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END ;
/


