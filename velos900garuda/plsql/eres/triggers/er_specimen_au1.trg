CREATE OR REPLACE TRIGGER ER_SPECIMEN_AU1 AFTER UPDATE ON ER_SPECIMEN
FOR EACH ROW
DECLARE

v_occupied_stat_code number;
v_available_stat_code number;

BEGIN

  select pk_Codelst into v_occupied_stat_code from er_Codelst where codelst_type='storage_stat' and codelst_subtyp='Occupied';
  select pk_Codelst into v_available_stat_code from er_Codelst where codelst_type='storage_stat' and codelst_subtyp='Available';

IF (NVL(:OLD.fk_storage,0) !=      NVL(:NEW.fk_storage,0) ) THEN
	insert into ER_STORAGE_STATUS (PK_STORAGE_STATUS,
	FK_STORAGE,
	SS_START_DATE,
	FK_CODELST_STORAGE_STATUS,
	FK_USER,
	SS_TRACKING_NUMBER,
	FK_STUDY,
	CREATOR,
	CREATED_ON,
	IP_ADD,
	SS_NOTES) values (
	seq_er_storage_stat.nextval, :new.FK_STORAGE, TO_DATE(TO_CHAR(sysdate,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT), v_occupied_stat_code, :new.CREATOR, '', :new.FK_STUDY, :new.CREATOR,
	sysdate, :new.IP_ADD, '');

	insert into ER_STORAGE_STATUS (PK_STORAGE_STATUS,
	FK_STORAGE,
	SS_START_DATE,
	FK_CODELST_STORAGE_STATUS,
	FK_USER,
	SS_TRACKING_NUMBER,
	FK_STUDY,
	CREATOR,
	CREATED_ON,
	IP_ADD,
	SS_NOTES) values (
	seq_er_storage_stat.nextval, :old.FK_STORAGE, TO_DATE(TO_CHAR(sysdate,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT), v_available_stat_code, :new.CREATOR, '', :old.FK_STUDY, :new.CREATOR,
	sysdate, :new.IP_ADD, '');


END IF;

END;
/


