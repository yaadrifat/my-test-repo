create or replace TRIGGER CB_CORD_TEMP_MIN_CRIT_BI0 BEFORE INSERT ON CB_CORD_TEMP_MINIMUM_CRITERIA     REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) 
DECLARE
raid NUMBER(10);
erid NUMBER(10);
usr VARCHAR(2000);
insert_data VARCHAR2(4000);
 BEGIN
  BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;
  audit_trail.record_transaction(raid, 'CB_CORD_TEMP_MINIMUM_CRITERIA',erid, 'I',usr);
       INSERT_DATA := :NEW.PK_MINIMUM_CRITERIA_FIELD || '|' ||	
to_char(:NEW.FK_MINIMUM_CRITERIA) || '|' ||	
to_char(:NEW.BACTERIAL_RESULT) || '|' ||	
to_char(:NEW.FUNGAL_RESULT) || '|' ||	
to_char(:NEW.HEMOGLOB_RESULT) || '|' ||	
to_char(:NEW.HIVRESULT) || '|' ||	
to_char(:NEW.HIVPRESULT) || '|' ||	
to_char(:NEW.HBSAG_RESULT) || '|' ||	
to_char(:NEW.HBVNAT_RESULT) || '|' ||	
to_char(:NEW.HCV_RESULT) || '|' ||	
to_char(:NEW.T_CRUZI_CHAGAS_RESULT) || '|' ||	
to_char(:NEW.WNV_RESULT) || '|' ||	
to_char(:NEW.TNCKG_PT_WEIGHT_RESULT) || '|' ||	
to_char(:NEW.VIABILITY_RESULT) || '|' ||	
usr || '|' ||	
to_char(:NEW.CREATED_ON) || '|' ||	
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||	
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||	
to_char(:NEW.IP_ADD) || '|' ||
erid || '|' ||	
to_char(:NEW.DELETEDFLAG);
  INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/