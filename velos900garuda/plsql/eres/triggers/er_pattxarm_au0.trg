CREATE OR REPLACE TRIGGER ER_PATTXARM_AU0
AFTER UPDATE
ON ER_PATTXARM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := Getuser(:NEW.last_modified_by);
  Audit_Trail.record_transaction
    (raid, 'ER_PATTXARM', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_pattxarm,0) !=
     NVL(:NEW.pk_pattxarm,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_PATTXARM',
       :OLD.pk_pattxarm, :NEW.pk_pattxarm);
  END IF;

  IF NVL(:OLD.fk_patprot,0) !=
     NVL(:NEW.fk_patprot,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_PATPROT',
       :OLD.FK_PATPROT, :NEW.FK_PATPROT);
  END IF;

  IF NVL(:OLD.fk_studytxarm,0) !=
     NVL(:NEW.fk_studytxarm,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDYTXARM',
       :OLD.fk_studytxarm, :NEW.fk_studytxarm);
  END IF;

  IF NVL(:OLD.tx_drug_info,' ') !=
     NVL(:NEW.tx_drug_info,' ') THEN
     Audit_Trail.column_update
       (raid, 'TX_DRUG_INFO',
       :OLD.tx_drug_info, :NEW.tx_drug_info);
  END IF;
IF NVL(:OLD.tx_drug_info,' ') !=
     NVL(:NEW.tx_drug_info,' ') THEN
     Audit_Trail.column_update
       (raid, 'TX_DRUG_INFO',
       :OLD.tx_drug_info, :NEW.tx_drug_info);
  END IF;
IF NVL(:OLD.tx_start_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.tx_start_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'TX_START_DATE',
       to_char(:OLD.tx_start_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.tx_start_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.tx_end_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.tx_end_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'tx_end_date',
       to_char(:OLD.tx_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.tx_end_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;


  IF NVL(:OLD.notes,' ') !=
     NVL(:NEW.notes,' ') THEN
     Audit_Trail.column_update
       (raid, 'NOTES',
       :OLD.notes, :NEW.notes);
  END IF;


  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
     Audit_Trail.column_update      (raid, 'LAST_MODIFIED_BY',      old_modby, new_modby);

  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
	    Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
END;
/


