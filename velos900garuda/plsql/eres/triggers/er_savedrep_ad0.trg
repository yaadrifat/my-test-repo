CREATE OR REPLACE TRIGGER "ER_SAVEDREP_AD0" 
  after delete
  on er_savedrep
  for each row
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_SAVEDREP', :old.rid, 'D');

  deleted_data :=
  to_char(:old.pk_savedrep) || '|' ||
  to_char(:old.fk_study) || '|' ||
  to_char(:old.fk_report) || '|' ||
  :old.savedrep_name || '|' ||
  to_char(:old.savedrep_asofdate) || '|' ||
  to_char(:old.fk_user) || '|' ||
  to_char(:old.fk_pat) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  :old.savedrep_filter;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


