CREATE OR REPLACE TRIGGER "ER_PATLABS_BI0" BEFORE INSERT ON ER_PATLABS
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
--Created by Manimaran for Audit Insert
BEGIN
   BEGIN
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_PATLABS',erid, 'I',usr);
   insert_data:= :NEW.PK_PATLABS||'|'|| :NEW.FK_PER||'|'||:NEW.FK_TESTID||'|'||TO_CHAR(:NEW.TEST_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.TEST_RESULT||'|'||:NEW.TEST_TYPE||'|'||:NEW.TEST_UNIT||'|'||
   :NEW.TEST_RANGE||'|'||:NEW.TEST_LLN||'|'||:NEW.TEST_ULN||'|'||:NEW.FK_SITE||'|'||
   :NEW.FK_CODELST_TSTSTAT||'|'||:NEW.ACCESSION_NUM||'|'||
   :NEW.FK_TESTGROUP||'|'||:NEW.FK_CODELST_ABFLAG||'|'||:NEW.FK_PATPROT||'|'||:NEW.FK_EVENT_ID||'|'||
   :NEW.CUSTOM001||'|'||:NEW.CUSTOM002||'|'||:NEW.CUSTOM003||'|'||:NEW.CUSTOM004||'|'||:NEW.CUSTOM005||'|'||:NEW.CUSTOM006||'|'||:NEW.CUSTOM007||'|'||:NEW.CUSTOM008||'|'||:NEW.CUSTOM009||'|'||:NEW.CUSTOM010||'|'||
   :NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||:NEW.ER_NAME||'|'||:NEW.ER_GRADE||'|'||
   :NEW.FK_CODELST_STDPHASE||'|'||:NEW.FK_STUDY||'|'||:NEW.FK_SPECIMEN;
   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


