create or replace TRIGGER "ERES".CB_CORD_COMPLETE_REQ_INFO_BI0 BEFORE INSERT ON CB_CORD_COMPLETE_REQ_INFO
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_CORD_COMPLETE_REQ_INFO',erid, 'I',usr);

  INSERT_DATA:=:NEW.PK_CORD_COMPLETE_REQINFO || '|' ||
to_char(:NEW.CBU_INFO_FLAG) || '|' ||
to_char(:NEW.CBU_PROCESSING_FLAG) || '|' ||
to_char(:NEW.LAB_SUMMARY_FLAG) || '|' ||
to_char(:NEW.MRQ_FLAG) || '|' ||
to_char(:NEW.FMHQ_FLAG) || '|' ||
to_char(:NEW.IDM_FLAG) || '|' ||
to_char(:NEW.FK_CORD_ID) || '|' ||
to_char(:NEW.FK_ORDER_ID) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.COMPLETE_REQ_INFO_FLAG);
INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);
END;
/