CREATE OR REPLACE TRIGGER FCR_CBB_CONFIRM_TASK AFTER INSERT ON CB_UPLOAD_INFO REFERENCING OLD AS OLDVAL NEW AS NEWVAL FOR EACH ROW
declare
V_CORDID NUMBER:=0;
V_CODELST_PK NUMBER;
V_PK_FCR NUMBER;
V_CDRUSER NUMBER;
v_category number;
is_rec_in_fcr number;
v_attachmentid number;
v_confirm_cbb NUMBER;
V_ENTITY_CORD NUMBER;
begin
dbms_output.put_line('...........'||:newval.FK_ATTACHMENTID);

v_category:=:newval.fk_category;
V_ATTACHMENTID:=:NEWVAL.FK_ATTACHMENTID;

SELECT CO.PK_CODELST INTO V_CODELST_PK FROM ER_CODELST CO WHERE CO.CODELST_TYPE='doc_categ' AND CO.CODELST_SUBTYP='cbu_asmnt_cat';
SELECT CO.PK_CODELST INTO V_ENTITY_CORD FROM ER_CODELST CO WHERE CO.CODELST_TYPE='entity_type' AND CO.CODELST_SUBTYP='CBU';

IF (V_CATEGORY=V_CODELST_PK) THEN

        SELECT ENTITY_ID INTO V_CORDID FROM ER_ATTACHMENTS WHERE PK_ATTACHMENT=V_ATTACHMENTID and ENTITY_TYPE=V_ENTITY_CORD;
        SELECT NVL(USING_CDR,0) INTO V_CDRUSER FROM CB_CORD CRD,ER_SITE S,CBB CB WHERE CRD.FK_CBB_ID=S.PK_SITE AND CB.FK_SITE=S.PK_SITE AND CRD.PK_CORD=V_CORDID;
        
        IF(V_CDRUSER=0) THEN
        
            SELECT COUNT(*) INTO IS_REC_IN_FCR FROM CB_CORD_FINAL_REVIEW WHERE FK_CORD_ID=V_CORDID;
        
        
            IF IS_REC_IN_FCR >0 THEN
        
                select max(pk_cord_final_review) into v_pk_fcr from cb_cord_final_review where fk_cord_id=v_cordid;
                select confirm_cbb_sign_flag into v_confirm_cbb from cb_cord_final_review where pk_cord_final_review=v_pk_fcr;
                
                if(v_confirm_cbb!=1) then
                UPDATE CB_CORD_FINAL_REVIEW SET CONFIRM_CBB_SIGN_FLAG=1,CBB_SIGNATURE_CREATED_ON=SYSDATE,CBB_SIGNATURE_CREATOR=:NEWVAL.CREATOR WHERE PK_CORD_FINAL_REVIEW=V_PK_FCR;
                end if;
        
            end if; 
        
            if is_rec_in_fcr=0 then
                 INSERT INTO CB_CORD_FINAL_REVIEW (PK_CORD_FINAL_REVIEW,CONFIRM_CBB_SIGN_FLAG,CBB_SIGNATURE_CREATED_ON,CBB_SIGNATURE_CREATOR,FK_CORD_ID)VALUES(SEQ_CB_CORD_FINAL_REVIEW.NEXTVAL,1,SYSDATE,:NEWVAL.CREATOR,V_CORDID);
            END IF;
       
          END if;
END IF;


EXCEPTION
  WHEN NO_DATA_FOUND THEN
        dbms_output.put_line ('INSIDE EXCEPTION...........DATA NOT FIND');
end;
/