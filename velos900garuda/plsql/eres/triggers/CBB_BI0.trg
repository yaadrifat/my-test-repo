create or replace
TRIGGER CBB_BI0 BEFORE INSERT ON CBB
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data VARCHAR2(4000);
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CBB',ERID, 'I',USR);
       insert_data:=:NEW.PK_CBB || '|' ||
to_char(:NEW.CBB_ID) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||
to_char(:NEW.IP_ADD) || '|' ||
to_char(:NEW.DELETEDFLAG) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.FK_PERSON) || '|' ||
to_char(:NEW.FK_ADD) || '|' ||
to_char(:NEW.FK_SITE) || '|' ||
to_char(:NEW.ENABLE_EDIT_ANTIGENS) || '|' ||
to_char(:NEW.DISPLAY_MATERNAL_ANTIGENS) || '|' ||
to_char(:NEW.ENABLE_CBU_INVENT_FEATURE) || '|' ||
to_char(:NEW.DEFAULT_CT_SHIPPER) || '|' ||
to_char(:NEW.DEFAULT_OR_SHIPPER) || '|' ||
to_char(:NEW.MICRO_CONTAM_DATE) || '|' ||
to_char(:NEW.CFU_TEST_DATE) || '|' ||
to_char(:NEW.VIAB_TEST_DATE) || '|' ||
to_char(:NEW.ENABLE_CORD_INFO_LOCK) || '|' ||
to_char(:NEW.DATE_FORMAT) || '|' ||
to_char(:NEW.MEMBER) || '|' ||
to_char(:NEW.USING_CDR) || '|' ||
to_char(:NEW.LOC_CBU_ID_PRE) || '|' ||
to_char(:NEW.LOC_MAT_ID_PRE) || '|' ||
to_char(:NEW.LOC_CBU_ID_STR_NUM) || '|' ||
to_char(:NEW.LOC_MAT_ID_STR_NUM) || '|' ||
to_char(:NEW.FK_LOC_CBU_ASSGN_ID) || '|' ||
to_char(:NEW.FK_LOC_MAT_ASSGN_ID) || '|' ||
to_char(:NEW.COMP_CORD_ENTRY) || '|' ||
to_char(:NEW.COMP_ACK_RES_TASK) || '|' ||
to_char(:NEW.FK_SHP_PWRK_LOC) || '|' ||
to_char(:NEW.FK_CBU_PCK_ADD) || '|' ||
to_char(:NEW.USE_OWN_DUMN) || '|' ||
to_char(:NEW.DOMESTIC_NMDP) || '|' ||
to_char(:NEW.USE_FDOE) || '|' ||
to_char(:NEW.ATTENTION) || '|' ||
to_char(:NEW.FK_TIME_FORMAT) || '|' ||
to_char(:NEW.FK_DATE_FORMAT) || '|' ||
to_char(:NEW.FK_ACCREDITATION) || '|' ||
to_char(:NEW.FK_CBU_STORAGE) || '|' ||
to_char(:NEW.FK_CBU_COLLECTION) || '|' ||
to_char(:NEW.FK_ID_ON_BAG) || '|' ||
to_char(:NEW.FK_DRY_SHIPPER_ADD) || '|' ||
to_char(:NEW.ATTENTION_DRY_SHIPPER) || '|' ||
to_char(:NEW.PICKUP_ADD_SPL_INSTRUCTION) || '|' ||
to_char(:NEW.LINKS_HOVER_FLAG) || '|' ||
to_char(:NEW.NMD_PRESEARCH_FLAG) || '|' ||
to_char(:NEW.PICKUP_ADD_CONTACT_LASTNAME) || '|' ||
to_char(:NEW.PICKUP_ADD_CONTACT_FIRSTNAME) || '|' ||
to_char(:NEW.BMDW) || '|' ||
to_char(:NEW.EMDIS) || '|' ||
to_char(:NEW.CBU_ASSESSMENT) || '|' ||
to_char(:NEW.UNIT_REPORT);
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/