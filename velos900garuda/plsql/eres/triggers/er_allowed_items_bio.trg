create or replace TRIGGER "ER_ALLOWED_ITEMS_BIO" BEFORE INSERT ON ER_ALLOWED_ITEMS
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;

SELECT seq_audit.NEXTVAL INTO raid FROM dual;

 Audit_Trail.record_transaction(raid, 'ER_ALLOWED_ITEMS',erid, 'I',usr);

 insert_data:=:NEW.PK_AI||'|'||:NEW.FK_STORAGE||'|'||:NEW.FK_CODELST_STORAGE_TYPE||'|'||:NEW.FK_CODELST_SPECIMEN_TYPE||'|'||:NEW.AI_ITEM_TYPE||'|'|| :NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat);

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);


END ;
/


