CREATE OR REPLACE TRIGGER "ER_SPECIMEN_BU_LM" BEFORE UPDATE ON ER_SPECIMEN
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
begin
:new.last_modified_date := sysdate;
end;
/


