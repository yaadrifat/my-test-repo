CREATE OR REPLACE TRIGGER "ER_PORTAL_POPLEVEL_AD0" AFTER DELETE ON ER_PORTAL_POPLEVEL
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ER_PORTAL_POPLEVEL', :old.rid, 'D');
--Created by Gopu for Audit delete
deleted_data :=
to_char(:old.PK_PORTAL_POPLEVEL) || '|' ||
to_char(:old.FK_PORTAL) || '|' ||
:old.PP_OBJECT_TYPE || '|' ||
to_char(:old.PP_OBJECT_ID) || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
:old.IP_ADD;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


