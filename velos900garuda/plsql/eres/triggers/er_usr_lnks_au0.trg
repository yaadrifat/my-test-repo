CREATE OR REPLACE TRIGGER ER_USR_LNKS_AU0
AFTER UPDATE
ON ER_USR_LNKS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  old_account varchar2(30) ;
  new_account varchar2(30) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_USR_LNKS', :old.rid, 'U', usr);
  if nvl(:old.pk_usr_lnks,0) !=
     NVL(:new.pk_usr_lnks,0) then
     audit_trail.column_update
       (raid, 'PK_USR_LNKS',
       :old.pk_usr_lnks, :new.pk_usr_lnks);
  end if;
  if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
	begin
      select ac_name into old_account
      from er_account where pk_account = :old.fk_account ;
	  Exception When NO_DATA_FOUND then
	   old_account := null ;
	End ;
     Begin
      select ac_name into new_account
      from er_account where pk_account = :new.fk_account ;
 	  Exception When NO_DATA_FOUND then
	   new_account := null ;
	End ;
      audit_trail.column_update
       (raid, 'FK_ACCOUNT',
	old_account, new_account);
  end if;
  if nvl(:old.fk_user,0) !=
     NVL(:new.fk_user,0) then
     Begin
      select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
        into old_modby
        from er_user
       where pk_user = :old.fk_user  ;
     Exception When NO_DATA_FOUND then
	   old_modby := null ;
	End ;
     Begin
      select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
        into new_modby
        from er_user
       where pk_user = :new.fk_user  ;
     Exception When NO_DATA_FOUND then
	   new_modby := null ;
	End ;
     audit_trail.column_update
       (raid, 'FK_USER',
       old_modby, new_modby);
  end if;
  if nvl(:old.link_uri,' ') !=
     NVL(:new.link_uri,' ') then
     audit_trail.column_update
       (raid, 'LINK_URI',
       :old.link_uri, :new.link_uri);
  end if;
  if nvl(:old.link_desc,' ') !=
     NVL(:new.link_desc,' ') then
     audit_trail.column_update
       (raid, 'LINK_DESC',
       :old.link_desc, :new.link_desc);
  end if;
  if nvl(:old.link_grpname,' ') !=
     NVL(:new.link_grpname,' ') then
     audit_trail.column_update
       (raid, 'LINK_GRPNAME',
       :old.link_grpname, :new.link_grpname);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
	Begin
      select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
        into old_modby
        from er_user
       where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
	   old_modby := null ;
	End ;
     Begin
      select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
        into new_modby
        from er_user
       where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
	   new_modby := null ;
	End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
end;
/


