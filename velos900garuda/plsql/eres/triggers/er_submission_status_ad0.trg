CREATE OR REPLACE TRIGGER "ER_SUBMISSION_STATUS_AD0"
  after delete
  on ER_SUBMISSION_STATUS
  for each row
declare
  raid number(10);
  deleted_data varchar2(4000);
  usr VARCHAR2(500);

begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(NVL(:OLD.LAST_MODIFIED_BY,:OLD.CREATOR));

  audit_trail.record_transaction
    (raid, 'ER_SUBMISSION_STATUS', :old.rid, 'D', usr);

  deleted_data :=
  to_char(:old.PK_SUBMISSION_STATUS) || '|' ||
  to_char(:old.FK_SUBMISSION) || '|' ||
  to_char(:old.FK_SUBMISSION_BOARD) || '|' ||
  to_char(:old.SUBMISSION_STATUS) || '|' ||
  to_char(:old.SUBMISSION_STATUS_DATE) || '|' ||
  to_char(:old.SUBMISSION_ENTERED_BY) || '|' ||
  to_char(:old.SUBMISSION_ASSIGNED_TO) || '|' ||
  to_char(:old.SUBMISSION_COMPLETED_BY) || '|' ||
  :old.SUBMISSION_NOTES || '|' ||
  to_char(:old.IS_CURRENT) || '|' ||
  to_char(:old.CREATED_ON) || '|' ||
  to_char(:old.CREATOR) || '|' ||
  to_char(:old.LAST_MODIFIED_DATE) || '|' ||
  to_char(:old.LAST_MODIFIED_BY) || '|' ||
  to_char(:old.RID) || '|' ||
  :old.IP_ADD;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


