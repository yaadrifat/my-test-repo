create or replace TRIGGER "ERES".ER_FORMLIB_BI0 BEFORE INSERT ON ER_FORMLIB
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0)
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM ER_USER
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

        SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_FORMLIB',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMLIB||'|'|| :NEW.FK_CATLIB||'|'||
    :NEW.FK_ACCOUNT||'|'||:NEW.FORM_NAME||'|'||:NEW.FORM_DESC||'|'||:NEW.FORM_SHAREDWITH||'|'||
    :NEW.FORM_STATUS||'|'||:NEW.FORM_LINKTO||'|'|| :NEW.FORM_NEXT||'|'||
    :NEW.FORM_NEXTMODE||'|'|| :NEW.FORM_BOLD||'|'|| :NEW.FORM_ITALICS||'|'||
    :NEW.FORM_ALIGN||'|'|| :NEW.FORM_UNDERLINE||'|'||:NEW.FORM_COLOR||'|'||:NEW.FORM_BGCOLOR||'|'||
    :NEW.FORM_FONT||'|'|| :NEW.FLD_FONTSIZE||'|'|| :NEW.RID||'|'||
    :NEW.FORM_FILLFLAG||'|'|| :NEW.CREATOR||'|'|| :NEW.FORM_XSLREFRESH||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.RECORD_TYPE||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||:NEW.FORM_KEYWORD || '|' || :new.FORM_ESIGNREQ;

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/

