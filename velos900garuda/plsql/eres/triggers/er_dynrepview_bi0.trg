create or replace TRIGGER "ER_DYNREPVIEW_BI0" BEFORE INSERT ON ER_DYNREPVIEW
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_DYNREPVIEW',erid, 'I',usr);
   insert_data:= :NEW.PK_DYNREPVIEW||'|'|| :NEW.FK_DYNREP||'|'||:NEW.DYNREPVIEW_COL||'|'||:NEW.DYNREPVIEW_SEQ||'|'||:NEW.DYNREPVIEW_WIDTH||'|'||:NEW.DYNREPVIEW_FORMAT||'|'||:NEW.DYNREPVIEW_DISPNAME||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||:NEW.IP_ADD||'|'||:NEW.DYNREPVIEW_COLNAME||'|'||:NEW.DYNREPVIEW_COLDATATYPE||'|'||:NEW.FK_FORM||'|'||:NEW.FORM_TYPE||'|'||:NEW.USEUNIQUEID||'|'||:NEW.USEDATAVAL||'|'||:NEW.CALCPER||'|'||:NEW.CALCSUM ||'|'||:NEW.REPEAT_NUMBER;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


