CREATE OR REPLACE TRIGGER "ER_CRFFORMS_BI0" BEFORE INSERT ON ER_CRFFORMS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
     BEGIN
   BEGIN

 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;


                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_CRFFORMS',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_CRFFORMS||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.FK_SCHEVENT1||'|'|| :NEW.FK_CRF||'|'||
     TO_CHAR(:NEW.CRFFORMS_FILLDATE,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.RECORD_TYPE||'|'||
   :NEW.FORM_COMPLETED||'|'|| :NEW.ISVALID||'|'|| :NEW.NOTIFICATION_SENT||'|'||
   :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
   :NEW.IP_ADD||'|'|| :NEW.PROCESS_DATA||'|'||
    :NEW.FK_FORMLIBVER;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


