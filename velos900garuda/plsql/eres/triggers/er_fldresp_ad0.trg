CREATE OR REPLACE TRIGGER "ER_FLDRESP_AD0" 
  AFTER UPDATE --delete
  ON er_fldresp
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FLDRESP', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_fldresp) || '|' ||
  TO_CHAR(:OLD.fk_field) || '|' ||
  TO_CHAR(:OLD.fldresp_seq) || '|' ||
  :OLD.fldresp_dispval || '|' ||
  :OLD.fldresp_dataval || '|' ||
  TO_CHAR(:OLD.fldresp_score) || '|' ||
  TO_CHAR(:OLD.fldresp_isdefault) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


