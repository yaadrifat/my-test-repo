CREATE OR REPLACE TRIGGER "ER_CATLIB_BI0" BEFORE INSERT ON ER_CATLIB   REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
  insert_data CLOB;
     BEGIN

  BEGIN
    SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                      SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_CATLIB',erid, 'I',usr);
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_CATLIB||'|'|| :NEW.FK_ACCOUNT||'|'||
    :NEW.CATLIB_TYPE||'|'||:NEW.CATLIB_NAME||'|'||:NEW.CATLIB_DESC||'|'||:NEW.RECORD_TYPE||'|'||
     :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||
     :NEW.CATLIB_SUBTYPE;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


