CREATE OR REPLACE TRIGGER "ERES"."ER_STUDY_NIH_GRANT_BI0" BEFORE INSERT ON ER_STUDY_NIH_GRANT 
FOR EACH ROW 
DECLARE
	raid NUMBER(10);
	erid NUMBER(10);
	usr VARCHAR(2000);
	insert_data CLOB;
BEGIN
	BEGIN
		SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
		WHERE pk_user = :NEW.creator ;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		USR := 'New User' ;
	END ;

	SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
	:NEW.rid := erid ;
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;

	audit_trail.record_transaction(raid, 'ER_STUDY_NIH_GRANT',erid, 'I',usr);
	insert_data:= :NEW.PK_STUDY_NIH_GRANT||'|'||
	:NEW.FK_STUDY||'|'||
	:NEW.FK_CODELST_FUNDMECH||'|'||
	:NEW.FK_CODELST_INSTCODE||'|'||
	:NEW.NIH_GRANT_SERIAL||'|'||
	:NEW.FK_CODELST_PROGRAM_CODE||'|'||
	:NEW.RID||'|'||
	:NEW.IP_ADD||'|'||
	:NEW.CREATOR||'|'||
	TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||
	:NEW.LAST_MODIFIED_BY||'|'||
	TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);
	
	INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END; 
/
