set define off;
--When records are inserted in CB_ANTIGEN table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "CB_ANTIGEN_AI0" AFTER INSERT ON CB_ANTIGEN 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */ 
	NEW_MODIFIED_BY VARCHAR2(200);
	NEW_CREATOR VARCHAR2(200);
	NEW_VALID_IND VARCHAR2(200);
	NEW_ACTIVE_IND VARCHAR2(200);
	NEW_DELETEDFLAG VARCHAR2(200);
	BEGIN
  
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
	
	  --Inserting row in AUDIT_ROW_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_ANTIGEN', :NEW.rid,:NEW.PK_ANTIGEN,'I',:NEW.Creator);
	IF :NEW.PK_ANTIGEN IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','PK_ANTIGEN',NULL,:NEW.PK_ANTIGEN,NULL,NULL);
	END IF;
    IF :NEW.ANTIGEN_ID IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','ANTIGEN_ID',NULL,:NEW.ANTIGEN_ID,NULL,NULL);
	END IF;
    IF :NEW.VALID_IND IS NOT NULL THEN
	SELECT DECODE(:NEW.VALID_IND,'1','Yes','0','No')		INTO NEW_VALID_IND  FROM DUAL;
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','VALID_IND',NULL,NEW_VALID_IND,NULL,NULL);
	END IF;
	IF :NEW.CREATOR IS NOT NULL THEN
	SELECT f_getuser(:NEW.CREATOR)        INTO NEW_CREATOR  from dual;
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','CREATOR',NULL,NEW_CREATOR,NULL,NULL);
    END IF;
    IF :NEW.CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
	SELECT f_getuser(:NEW.LAST_MODIFIED_BY)        INTO NEW_MODIFIED_BY  from dual;
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','LAST_MODIFIED_BY',NULL,NEW_MODIFIED_BY,NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.IP_ADD IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF :NEW.DELETEDFLAG IS NOT NULL THEN
	SELECT DECODE(:NEW.DELETEDFLAG,'1','Yes','0','No')		INTO NEW_DELETEDFLAG  FROM DUAL;
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','DELETEDFLAG',NULL,NEW_DELETEDFLAG,NULL,NULL);
    END IF;
    IF :NEW.RID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','RID',NULL,:NEW.RID,NULL,NULL);
    END IF;
	IF :NEW.ACTIVE_IND IS NOT NULL THEN
	SELECT DECODE(:NEW.ACTIVE_IND,'1','Yes','0','No')		INTO NEW_ACTIVE_IND  FROM DUAL;
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','ACTIVE_IND',NULL,NEW_ACTIVE_IND,NULL,NULL);
	END IF;
	IF :NEW.HLA_LOCUS IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','HLA_LOCUS',NULL,:NEW.HLA_LOCUS,NULL,NULL);
	END IF;
	IF :NEW.HLA_TYPING_METHOD IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','HLA_TYPING_METHOD',NULL,:NEW.HLA_TYPING_METHOD,NULL,NULL);
	END IF;
	IF :NEW.ALT_ANTIGEN_ID IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','ALT_ANTIGEN_ID',NULL,:NEW.ALT_ANTIGEN_ID,NULL,NULL);
	END IF;
	IF :NEW.LAST_UPDATED_SOURCE IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','LAST_UPDATED_SOURCE',NULL,:NEW.LAST_UPDATED_SOURCE,NULL,NULL);
	END IF;
	IF :NEW.CREATED_BY_SOURCE IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','CREATED_BY_SOURCE',NULL,:NEW.CREATED_BY_SOURCE,NULL,NULL);
	END IF;
end;
/