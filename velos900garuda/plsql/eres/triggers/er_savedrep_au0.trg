CREATE OR REPLACE TRIGGER ER_SAVEDREP_AU0
  after update of
  pk_savedrep,
  fk_study,
  fk_report,
  savedrep_name,
  savedrep_asofdate,
  fk_user,
  fk_pat,
  creator,
  created_on,
  ip_add,
  savedrep_filter,
  rid
  on er_savedrep
  for each row
declare
  raid number(10);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_SAVEDREP', :old.rid, 'U', :new.LAST_MODIFIED_BY);

  if nvl(:old.pk_savedrep,0) !=
     NVL(:new.pk_savedrep,0) then
     audit_trail.column_update
       (raid, 'PK_SAVEDREP',
       :old.pk_savedrep, :new.pk_savedrep);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
  end if;
  if nvl(:old.fk_report,0) !=
     NVL(:new.fk_report,0) then
     audit_trail.column_update
       (raid, 'FK_REPORT',
       :old.fk_report, :new.fk_report);
  end if;
  if nvl(:old.savedrep_name,' ') !=
     NVL(:new.savedrep_name,' ') then
     audit_trail.column_update
       (raid, 'SAVEDREP_NAME',
       :old.savedrep_name, :new.savedrep_name);
  end if;
  if nvl(:old.savedrep_asofdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.savedrep_asofdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'SAVEDREP_ASOFDATE',
       to_char(:old.savedrep_asofdate,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.savedrep_asofdate,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.fk_user,0) !=
     NVL(:new.fk_user,0) then
     audit_trail.column_update
       (raid, 'FK_USER',
       :old.fk_user, :new.fk_user);
  end if;
  if nvl(:old.fk_pat,0) !=
     NVL(:new.fk_pat,0) then
     audit_trail.column_update
       (raid, 'FK_PAT',
       :old.fk_pat, :new.fk_pat);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
  if nvl(:old.savedrep_filter,' ') !=
     NVL(:new.savedrep_filter,' ') then
     audit_trail.column_update
       (raid, 'SAVEDREP_FILTER',
       :old.savedrep_filter, :new.savedrep_filter);
  end if;

end;
/


