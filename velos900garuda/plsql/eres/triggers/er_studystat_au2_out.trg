create or replace
TRIGGER ER_STUDYSTAT_AU2_OUT
AFTER UPDATE ON ER_STUDYSTAT
FOR EACH ROW
DECLARE 
fkaccount NUMBER;
pkmsg NUMBER; 
codesubtype VARCHAR(15); 
codedes VARCHAR(200); 
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SVC_OUTBOUND', pLEVEL  => Plog.LFATAL);
BEGIN
  SELECT fk_account
  INTO fkaccount
  FROM er_user
  WHERE pk_user = NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR);
  
  PKG_MSG_QUEUE.SP_POPULATE_STUDY_STATUS_MSG (
      :NEW.pk_studystat,
      fkaccount,
      'U', 
      to_char(:NEW.fk_study),
      :NEW.FK_CODELST_STUDYSTAT
   );  
  EXCEPTION WHEN OTHERS THEN
   Plog.FATAL(pCTX,'exception in ER_STUDYSTAT_AU2_OUT ' || SQLERRM);
END;
/
