CREATE OR REPLACE TRIGGER "ER_USR_LNKS_BI0" BEFORE INSERT ON ER_USR_LNKS
        REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
raid NUMBER(10);
    erid NUMBER(10);
      insert_data CLOB;
      usr VARCHAR(2000);
     BEGIN
    usr := getuser(:NEW.creator);
       SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
    audit_trail.record_transaction(raid, 'ER_USR_LNKS',erid, 'I',usr);
   -- Added by Ganapathy on 06/21/05 for Audit inserting
        insert_data:= :NEW.PK_USR_LNKS||'|'|| :NEW.FK_ACCOUNT||'|'||
      :NEW.FK_USER||'|'||:NEW.LINK_URI||'|'||:NEW.LINK_DESC||'|'||:NEW.LINK_GRPNAME||'|'||
    :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.IP_ADD||'|'|| :NEW.FK_CODELST_LNKTYPE;
  INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
    END ;
/


