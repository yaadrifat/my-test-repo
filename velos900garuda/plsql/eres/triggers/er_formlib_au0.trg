create or replace TRIGGER ERES.ER_FORMLIB_AU0
  after update of
  pk_formlib,
  fk_catlib,
  fk_account,
  form_name,
  form_desc,
  form_sharedwith,
  form_status,
  form_linkto,
  form_next,
  form_nextmode,
  form_bold,
  form_italics,
  form_align,
  form_underline,
  form_color,
  form_bgcolor,
  form_font,
  fld_fontsize,
  form_fillflag,
  form_xslrefresh,
  last_modified_by,
  last_modified_date,
  record_type,
  ip_add,
  rid,
FORM_ESIGNREQ
  on ER_FORMLIB
  for each row
declare
  raid number(10);

   usr varchar2(100);

   old_modby varchar2(100);

   new_modby varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);
   --KM-#3634
    IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
  audit_trail.record_transaction
    (raid, 'ER_FORMLIB', :old.rid, 'U', usr);
   end if;
   
  if nvl(:old.pk_formlib,0) !=
     NVL(:new.pk_formlib,0) then
     audit_trail.column_update
       (raid, 'PK_FORMLIB',
       :old.pk_formlib, :new.pk_formlib);
  end if;
  if nvl(:old.fk_catlib,0) !=
     NVL(:new.fk_catlib,0) then
     audit_trail.column_update
       (raid, 'FK_CATLIB',
       :old.fk_catlib, :new.fk_catlib);
  end if;
  if nvl(:old.fk_account,0) !=
     NVL(:new.fk_account,0) then
     audit_trail.column_update
       (raid, 'FK_ACCOUNT',
       :old.fk_account, :new.fk_account);
  end if;
  if nvl(:old.form_name,' ') !=
     NVL(:new.form_name,' ') then
     audit_trail.column_update
       (raid, 'FORM_NAME',
       :old.form_name, :new.form_name);
  end if;
  if nvl(:old.form_desc,' ') !=
     NVL(:new.form_desc,' ') then
     audit_trail.column_update
       (raid, 'FORM_DESC',
       :old.form_desc, :new.form_desc);
  end if;
  if nvl(:old.form_sharedwith,' ') !=
     NVL(:new.form_sharedwith,' ') then
     audit_trail.column_update
       (raid, 'FORM_SHAREDWITH',
       :old.form_sharedwith, :new.form_sharedwith);
  end if;
  if nvl(:old.form_status,0) !=
     NVL(:new.form_status,0) then
     audit_trail.column_update
       (raid, 'FORM_STATUS',
       :old.form_status, :new.form_status);
  end if;
  if nvl(:old.form_linkto,' ') !=
     NVL(:new.form_linkto,' ') then
     audit_trail.column_update
       (raid, 'FORM_LINKTO',
       :old.form_linkto, :new.form_linkto);
  end if;
  if nvl(:old.form_next,0) !=
     NVL(:new.form_next,0) then
     audit_trail.column_update
       (raid, 'FORM_NEXT',
       :old.form_next, :new.form_next);
  end if;
  if nvl(:old.form_nextmode,' ') !=
     NVL(:new.form_nextmode,' ') then
     audit_trail.column_update
       (raid, 'FORM_NEXTMODE',
       :old.form_nextmode, :new.form_nextmode);
  end if;
  if nvl(:old.form_bold,0) !=
     NVL(:new.form_bold,0) then
     audit_trail.column_update
       (raid, 'FORM_BOLD',
       :old.form_bold, :new.form_bold);
  end if;
  if nvl(:old.form_italics,0) !=
     NVL(:new.form_italics,0) then
     audit_trail.column_update
       (raid, 'FORM_ITALICS',
       :old.form_italics, :new.form_italics);
  end if;
  if nvl(:old.form_align,' ') !=
     NVL(:new.form_align,' ') then
     audit_trail.column_update
       (raid, 'FORM_ALIGN',
       :old.form_align, :new.form_align);
  end if;
  if nvl(:old.form_underline,0) !=
     NVL(:new.form_underline,0) then
     audit_trail.column_update
       (raid, 'FORM_UNDERLINE',
       :old.form_underline, :new.form_underline);
  end if;
  if nvl(:old.form_color,' ') !=
     NVL(:new.form_color,' ') then
     audit_trail.column_update
       (raid, 'FORM_COLOR',
       :old.form_color, :new.form_color);
  end if;
  if nvl(:old.form_bgcolor,' ') !=
     NVL(:new.form_bgcolor,' ') then
     audit_trail.column_update
       (raid, 'FORM_BGCOLOR',
       :old.form_bgcolor, :new.form_bgcolor);
  end if;
  if nvl(:old.form_font,' ') !=
     NVL(:new.form_font,' ') then
     audit_trail.column_update
       (raid, 'FORM_FONT',
       :old.form_font, :new.form_font);
  end if;
  if nvl(:old.fld_fontsize,0) !=
     NVL(:new.fld_fontsize,0) then
     audit_trail.column_update
       (raid, 'FLD_FONTSIZE',
       :old.fld_fontsize, :new.fld_fontsize);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.form_fillflag,0) !=
     NVL(:new.form_fillflag,0) then
     audit_trail.column_update
       (raid, 'FORM_FILLFLAG',
       :old.form_fillflag, :new.form_fillflag);
  end if;
  if nvl(:old.form_xslrefresh,0) !=
     NVL(:new.form_xslrefresh,0) then
     audit_trail.column_update
       (raid, 'FORM_XSLREFRESH',
       :old.form_xslrefresh, :new.form_xslrefresh);
  end if;

    if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

    if nvl(:old.FORM_ESIGNREQ,0) !=
     NVL(:new.FORM_ESIGNREQ,0) then
     audit_trail.column_update
       (raid, 'FORM_ESIGNREQ',
       :old.FORM_ESIGNREQ, :new.FORM_ESIGNREQ);
  end if;



 if nvl(:old.LAST_MODIFIED_BY,0) !=
 NVL(:new.LAST_MODIFIED_BY,0) then
	Begin
		Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
		into old_modby from ER_USER  where pk_user = :old.last_modified_by ;
		Exception When NO_DATA_FOUND then
			old_modby := null;
	End ;
	Begin
		Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
		into new_modby   from ER_USER   where pk_user = :new.LAST_MODIFIED_BY ;
		Exception When NO_DATA_FOUND then
			new_modby := null;
	 End ;
	audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
 end if;

end;
/