CREATE OR REPLACE TRIGGER "ER_LINKEDFORMS_AI0" 
AFTER INSERT
ON ER_LINKEDFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
v_status NUMBER;
/******************************************************************************
   NAME:       ER_LINKEDFORMS_AI0
   PURPOSE:    To perform work as each row is inserted or updated.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8/27/2003             1. Created this trigger.

   PARAMETERS:
   INPUT:
   OUTPUT:
   RETURNED VALUE:
   CALLED BY:
   CALLS:
   EXAMPLE USE:
   ASSUMPTIONS:
   LIMITATIONS:
   ALGORITHM:
   NOTES:

   Here is the complete list of available Auto Replace Keywords:
      Object Name:     ER_LINKEDFORMS_AI0 or ER_LINKEDFORMS_AI0
      Sysdate:         8/27/2003
      Date/Time:       8/27/2003 5:08:17 PM
      Date:            8/27/2003
      Time:            5:08:17 PM
      Username:         (set in TOAD Options, Procedure Editor)
******************************************************************************/
BEGIN

   v_status := 0;
   select pk_codelst into v_status from er_codelst where codelst_type='frmstat' and codelst_subtyp='W';

   insert into er_formstat(pk_formstat,fk_formlib,fk_codelst_stat,formstat_stdate,formstat_changedby,record_type,creator,created_on,ip_add)
          values(seq_er_formstat.nextval,:new.fk_formlib,v_status,sysdate,:new.creator,'N',:new.creator,sysdate,:new.ip_add);


END ER_LINKEDFORMS_AI0;
/


