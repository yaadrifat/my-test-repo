CREATE OR REPLACE TRIGGER "ER_PORTAL_MODULES_AD0" AFTER DELETE ON ER_PORTAL_MODULES
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ER_PORTAL_MODULES', :old.rid, 'D');
--Created by Gopu for Audit delete
deleted_data :=
to_char(:old.PK_PORTAL_MODULES) || '|' ||
to_char(:old.FK_PORTAL) || '|' ||
:old.FK_ID || '|' ||
:old.PM_TYPE || '|' ||
:old.PM_ALIGN || '|' ||
to_char(:old.PM_FROM) || '|' ||
to_char(:old.PM_TO) || '|' ||
:old.PM_FROM_UNIT || '|' ||
:old.PM_TO_UNIT || '|' ||
:old.PM_FORM_AFTER_RESP || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
:old.IP_ADD;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


