CREATE OR REPLACE TRIGGER "ER_STUDYID_AD0" 
AFTER DELETE
ON ER_STUDYID
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_STUDYID', :old.rid, 'D');

  deleted_data :=
  to_char(:old.PK_STUDYID) || '|' ||
  to_char(:old.FK_STUDY) || '|' ||
  to_char(:old.FK_CODELST_IDTYPE) || '|' ||
  :old.STUDYID_ID  || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on)  || '|' ||
  :old.ip_Add ;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


