create or replace TRIGGER ER_MILEAPNDX_AU0
  after update of
  pk_mileapndx,
  fk_study,
  fk_milestone,
  mileapndx_desc,
  mileapndx_uri,
  mileapndx_type,
  creator,
  created_on,
  ip_add,
  rid
  on er_mileapndx
  for each row
declare
  raid number(10);
  usr varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_MILEAPNDX', :old.rid, 'U', usr);

  if nvl(:old.pk_mileapndx,0) !=
     NVL(:new.pk_mileapndx,0) then
     audit_trail.column_update
       (raid, 'PK_MILEAPNDX',
       :old.pk_mileapndx, :new.pk_mileapndx);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
  end if;
  if nvl(:old.fk_milestone,0) !=
     NVL(:new.fk_milestone,0) then
     audit_trail.column_update
       (raid, 'FK_MILESTONE',
       :old.fk_milestone, :new.fk_milestone);
  end if;
  if nvl(:old.mileapndx_desc,' ') !=
     NVL(:new.mileapndx_desc,' ') then
     audit_trail.column_update
       (raid, 'MILEAPNDX_DESC',
       :old.mileapndx_desc, :new.mileapndx_desc);
  end if;
  if nvl(:old.mileapndx_uri,' ') !=
     NVL(:new.mileapndx_uri,' ') then
     audit_trail.column_update
       (raid, 'MILEAPNDX_URI',
       :old.mileapndx_uri, :new.mileapndx_uri);
  end if;
  if nvl(:old.mileapndx_type,' ') !=
     NVL(:new.mileapndx_type,' ') then
     audit_trail.column_update
       (raid, 'MILEAPNDX_TYPE',
       :old.mileapndx_type, :new.mileapndx_type);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

end;
/


