CREATE OR REPLACE TRIGGER ER_REVIEW_MEETING_TOPIC_AU0
  after update of
  PK_REVIEW_MEETING_TOPIC,
  FK_REVIEW_MEETING,
  TOPIC_NUMBER,
  MEETING_TOPIC,
  CREATED_ON,
  CREATOR,
  LAST_MODIFIED_DATE,
  LAST_MODIFIED_BY,
  IP_ADD,
  RID
  ON ER_REVIEW_MEETING_TOPIC for each row
WHEN (
OLD.rid IS NOT NULL
      )
declare
  raid number(10);
  usr VARCHAR2(500);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(NVL(:NEW.LAST_MODIFIED_BY,:OLD.CREATOR));

  audit_trail.record_transaction(raid, 'ER_REVIEW_MEETING_TOPIC', :old.rid, 'U', usr);

  if nvl(:old.PK_REVIEW_MEETING_TOPIC,0) !=
     NVL(:new.PK_REVIEW_MEETING_TOPIC,0) then
     audit_trail.column_update
       (raid, 'PK_REVIEW_MEETING_TOPIC',
       :old.PK_REVIEW_MEETING_TOPIC, :new.PK_REVIEW_MEETING_TOPIC);
  end if;
  if nvl(:old.FK_REVIEW_MEETING,0) !=
     NVL(:new.FK_REVIEW_MEETING,0) then
     audit_trail.column_update
       (raid, 'FK_REVIEW_MEETING',
       :old.FK_REVIEW_MEETING, :new.FK_REVIEW_MEETING);
  end if;
  if nvl(:old.TOPIC_NUMBER,0) !=
     NVL(:new.TOPIC_NUMBER,0) then
     audit_trail.column_update
       (raid, 'TOPIC_NUMBER',
       :old.TOPIC_NUMBER, :new.TOPIC_NUMBER);
  end if;
  if nvl(:old.MEETING_TOPIC,0) !=
     NVL(:new.MEETING_TOPIC,0) then
     audit_trail.column_update
       (raid, 'MEETING_TOPIC',
       :old.MEETING_TOPIC, :new.MEETING_TOPIC);
  end if;
  if nvl(:old.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),
       to_char(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.CREATOR,0) !=
     NVL(:new.CREATOR,0) then
     audit_trail.column_update
       (raid, 'CREATOR',
       :old.CREATOR, :new.CREATOR);
  end if;
  if nvl(:old.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),
       to_char(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.LAST_MODIFIED_BY,0) !=
     NVL(:new.LAST_MODIFIED_BY,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.LAST_MODIFIED_BY, :new.LAST_MODIFIED_BY);
  end if;
  if nvl(:old.RID,0) !=
     NVL(:new.RID,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.RID, :new.RID);
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

end;
/


