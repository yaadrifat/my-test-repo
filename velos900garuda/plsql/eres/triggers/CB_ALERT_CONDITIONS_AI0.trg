set define off;
--When records are inserted in CB_ALERT_CONDITIONS table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "CB_ALERT_CONDITIONS_AI0" AFTER INSERT ON CB_ALERT_CONDITIONS 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */ 
	NEW_MODIFIED_BY VARCHAR2(200);
	NEW_CREATOR VARCHAR2(200);
	NEW_CODLST_ALERT VARCHAR2(200);
BEGIN
  
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
	
	  --Inserting row in AUDIT_ROW_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_ALERT_CONDITIONS', :NEW.rid,:NEW.PK_ALERT_CONDITIONS,'I',:NEW.Creator);
	IF :NEW.PK_ALERT_CONDITIONS IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','PK_ALERT_CONDITIONS',NULL,:NEW.PK_ALERT_CONDITIONS,NULL,NULL);
	END IF;
    IF :NEW.FK_CODELST_ALERT IS NOT NULL THEN
	SELECT F_Codelst_Desc(:NEW.FK_CODELST_ALERT)  INTO NEW_CODLST_ALERT   FROM dual;
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','FK_CODELST_ALERT',NULL,NEW_CODLST_ALERT,NULL,NULL);
	END IF;
    IF :NEW.CONDITION_TYPE IS NOT NULL THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','CONDITION_TYPE',NULL,:NEW.CONDITION_TYPE,NULL,NULL);
	END IF;
	IF :NEW.START_BRACE IS NOT NULL THEN	
	    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','START_BRACE',NULL,:NEW.START_BRACE,NULL,NULL);  
    END IF;
    IF :NEW.CREATOR IS NOT NULL THEN
	SELECT f_getuser(:NEW.CREATOR)        INTO NEW_CREATOR  from dual;
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','CREATOR',NULL,NEW_CREATOR,NULL,NULL);
    END IF;
    IF :NEW.CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
	SELECT f_getuser(:NEW.LAST_MODIFIED_BY)        INTO NEW_MODIFIED_BY  from dual;
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','LAST_MODIFIED_BY',NULL,NEW_MODIFIED_BY,NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.IP_ADD IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF :NEW.DELETEDFLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','DELETEDFLAG',NULL,:NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF :NEW.RID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','RID',NULL,:NEW.RID,NULL,NULL);
    END IF;
    IF :NEW.TABLE_NAME IS NOT NULL THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','TABLE_NAME',NULL,:NEW.TABLE_NAME,NULL,NULL);
	end if;
	IF :NEW.COLUMN_NAME IS NOT NULL THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','COLUMN_NAME',NULL,:NEW.COLUMN_NAME,NULL,NULL);
	END IF;
	IF :NEW.CONDITION_VALUE IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','CONDITION_VALUE',NULL,:NEW.CONDITION_VALUE,NULL,NULL);
	END IF;
		IF :NEW.ARITHMETIC_OPERATOR IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','ARITHMETIC_OPERATOR',NULL,:NEW.ARITHMETIC_OPERATOR,NULL,NULL);
	END IF;
		IF :NEW.LOGICAL_OPERATOR IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','LOGICAL_OPERATOR',NULL,:NEW.LOGICAL_OPERATOR,NULL,NULL);
	END IF;
		IF :NEW.END_BRACE IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','END_BRACE',NULL,:NEW.END_BRACE,NULL,NULL);
	END IF;
		IF :NEW.KEY_CONDITION IS NOT NULL THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ALERT_CONDITIONS','KEY_CONDITION',NULL,:NEW.KEY_CONDITION,NULL,NULL);
	END IF;
end;
/