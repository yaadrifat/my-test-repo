CREATE OR REPLACE TRIGGER "ER_FORMFLD_AD0" 
  AFTER UPDATE--delete
  ON er_formfld
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FORMFLD', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_formfld) || '|' ||
  TO_CHAR(:OLD.fk_formsec) || '|' ||
  TO_CHAR(:OLD.fk_field) || '|' ||
  TO_CHAR(:OLD.formfld_seq) || '|' ||
  TO_CHAR(:OLD.formfld_mandatory) || '|' ||
  TO_CHAR(:OLD.formfld_browserflg) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  :OLD.formfld_xsl || '|' ||
  :OLD.formfld_javascr || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


