CREATE OR REPLACE TRIGGER ER_STUDYSITES_AU
AFTER UPDATE
ON ER_STUDYSITES REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_study varchar2(4000) ;
  new_study varchar2(4000) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
  orid number(10);
BEGIN
   select seq_audit.nextval into raid from dual;
   usr := getuser(:new.last_modified_by);
   orid:=nvl(:old.rid,0);
   audit_trail.record_transaction
    (raid, 'ER_STUDYSITES', orid, 'U', usr);

   if nvl(:old.pk_studysites,0) !=
     NVL(:new.pk_studysites,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYSITES',
       :old.pk_studysites, :new.pk_studysites);
   end if;

  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
   Begin
     select study_title into old_study
     from er_study where pk_study = :old.fk_study ;
     Exception When NO_DATA_FOUND then
      old_study := null ;
    End ;
    Begin
     select study_title into new_study
     from er_study where pk_study = :new.fk_study ;
     Exception When NO_DATA_FOUND then
      new_study := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_STUDY',
       old_study, new_study);
  end if;

  if nvl(:old.fk_site,0) !=
     NVL(:new.fk_site,0) then
     audit_trail.column_update
       (raid, 'FK_SITE',
       :old.fk_site, :new.fk_site);
   end if;

 if nvl(:old.fk_codelst_studysitetype,0) !=
     NVL(:new.fk_codelst_studysitetype,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_STUDYSITETYPE',
       :old.fk_codelst_studysitetype, :new.fk_codelst_studysitetype);
   end if;

  if nvl(:old.studysite_lsamplesize,0) !=
     NVL(:new.studysite_lsamplesize,0) then
     audit_trail.column_update
       (raid, 'STUDYSITE_LSAMPLESIZE',
       :old.studysite_lsamplesize, :new.studysite_lsamplesize);
   end if;


if nvl(:old.STUDYSITE_PUBLIC,0) !=
     NVL(:new.STUDYSITE_PUBLIC,0) then
     audit_trail.column_update
       (raid, 'STUDYSITE_PUBLIC',
       :old.STUDYSITE_PUBLIC, :new.STUDYSITE_PUBLIC);
   end if;

if nvl(:old.STUDYSITE_REPINCLUDE,0) !=
     NVL(:new.STUDYSITE_REPINCLUDE,0) then
     audit_trail.column_update
       (raid, 'STUDYSITE_REPINCLUDE',
       :old.STUDYSITE_REPINCLUDE, :new.STUDYSITE_REPINCLUDE);
   end if;

if nvl(:old.RID,0) !=
     NVL(:new.RID,0) then
     audit_trail.column_update
       (raid, 'RID',
       orid, :new.RID);
   end if;

if nvl(:old.CREATOR,0) !=
     NVL(:new.CREATOR,0) then
     audit_trail.column_update
       (raid, 'CREATOR',
       :old.CREATOR, :new.CREATOR);
   end if;

if nvl(:old.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;

if nvl(:old.last_modified_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_ON',
       to_char(:OLD.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;


if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;

if nvl(:old.STUDYSITE_ENRCOUNT,0) !=
     NVL(:new.STUDYSITE_ENRCOUNT,0) then
     audit_trail.column_update
       (raid, 'STUDYSITE_ENRCOUNT',
       :old.STUDYSITE_ENRCOUNT, :new.STUDYSITE_ENRCOUNT);
  end if;
END;
/


