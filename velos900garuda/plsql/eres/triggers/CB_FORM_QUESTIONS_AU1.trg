CREATE OR REPLACE TRIGGER "CB_FORM_QUESTIONS_AU1"  AFTER UPDATE ON CB_FORM_QUESTIONS
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	OLD_BACT_CUL_RESULT VARCHAR2(200);
	OLD_FUNGAL_CUL_RESULT VARCHAR2(200);
	OLD_ABO_BLOOD_TYPE VARCHAR2(200);
	NEW_ABO_BLOOD_TYPE VARCHAR2(200);
	NEW_BACT_CUL_RESULT VARCHAR2(200);
	NEW_FUNGAL_CUL_RESULT VARCHAR2(200);
	NEW_MODIFIED_BY VARCHAR2(200);
	NEW_CREATOR VARCHAR2(200);
	OLD_CREATOR VARCHAR2(200);
	OLD_MODIFIED_BY VARCHAR2(200);
	
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
	
	  --Inserting row in AUDIT_ROW_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_FORM_QUESTIONS', :NEW.RID,:NEW.PK_FORM_QUESTION,'I',:NEW.CREATOR);
	IF NVL(:old.PK_FORM_QUESTION,0) != NVL(:new.PK_FORM_QUESTION,0) THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','PK_FORM_QUESTION',:OLD.PK_FORM_QUESTION,:NEW.PK_FORM_QUESTION,NULL,NULL);
	END IF;	
	IF NVL(:old.FK_FORM,0) != NVL(:new.FK_FORM,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','FK_FORM',:OLD.FK_FORM,:NEW.FK_FORM,NULL,NULL);
	END IF;
    IF NVL(:old.QUES_SEQ,' ') != NVL(:new.QUES_SEQ,' ') THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','QUES_SEQ',:OLD.QUES_SEQ,:NEW.QUES_SEQ,NULL,NULL);
	END IF;
	IF NVL(:old.FK_MASTER_QUES,0) != NVL(:new.FK_MASTER_QUES,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','FK_MASTER_QUES',:OLD.FK_MASTER_QUES,:NEW.FK_MASTER_QUES,NULL,NULL);  
	END IF;
	IF NVL(:old.FK_DEPENDENT_QUES,0) != NVL(:new.FK_DEPENDENT_QUES,0) THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','FK_DEPENDENT_QUES',:OLD.FK_DEPENDENT_QUES,:NEW.FK_DEPENDENT_QUES,NULL,NULL);
	END IF;
	IF NVL(:old.FK_DEPENDENT_QUES_VALUE,' ') != NVL(:new.FK_DEPENDENT_QUES_VALUE,' ') THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','FK_DEPENDENT_QUES_VALUE',:OLD.FK_DEPENDENT_QUES_VALUE,:NEW.FK_DEPENDENT_QUES_VALUE,NULL,NULL);
	END IF;
	IF NVL(:old.RESPONSE_VALUE,' ') != NVL(:new.RESPONSE_VALUE,' ') THEN
		PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','RESPONSE_VALUE',:OLD.RESPONSE_VALUE,:NEW.RESPONSE_VALUE,NULL,NULL);
    END IF;
	IF NVL(:old.UNEXPECTED_RESPONSE_VALUE,' ') != NVL(:new.UNEXPECTED_RESPONSE_VALUE,' ') THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','UNEXPECTED_RESPONSE_VALUE',:OLD.UNEXPECTED_RESPONSE_VALUE,:NEW.UNEXPECTED_RESPONSE_VALUE,NULL,NULL);
     END IF;
	IF NVL(:old.DEFER_VALUE,' ') != NVL(:new.DEFER_VALUE,' ') THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','DEFER_VALUE',:OLD.DEFER_VALUE,:NEW.DEFER_VALUE,NULL,NULL);
     END IF;
	IF NVL(:old.FDA_ELIGIBILITY,' ') != NVL(:new.FDA_ELIGIBILITY,' ') THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','FDA_ELIGIBILITY',:OLD.FDA_ELIGIBILITY,:NEW.FDA_ELIGIBILITY,NULL,NULL);
     END IF;
	IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
		SELECT f_getuser(:OLD.CREATOR)        INTO OLD_CREATOR  from dual;
		SELECT f_getuser(:NEW.CREATOR)        INTO NEW_CREATOR  from dual;
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','CREATOR',OLD_CREATOR,NEW_CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','CREATED_ON',TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
		SELECT f_getuser(:OLD.LAST_MODIFIED_BY)        INTO OLD_MODIFIED_BY  from dual;
		SELECT f_getuser(:NEW.LAST_MODIFIED_BY)        INTO NEW_MODIFIED_BY  from dual;
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','LAST_MODIFIED_BY',OLD_MODIFIED_BY,NEW_MODIFIED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN	
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','LAST_MODIFIED_DATE',TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
   IF NVL(:old.IP_ADD,' ') != NVL(:new.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF NVL(:old.DELETEDFLAG,0) != NVL(:new.DELETEDFLAG,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','DELETEDFLAG',:OLD.DELETEDFLAG,:NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF NVL(:old.RID,0) != NVL(:new.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','RID',:OLD.RID,:NEW.RID,NULL,NULL);
    END IF;
	IF NVL(:old.FK_QUESTION,0) != NVL(:new.FK_QUESTION,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_FORM_QUESTIONS','FK_QUESTION',:OLD.FK_QUESTION,:NEW.FK_QUESTION,NULL,NULL);
	END IF;	
end;
/