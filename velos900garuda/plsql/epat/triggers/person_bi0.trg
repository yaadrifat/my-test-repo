CREATE OR REPLACE TRIGGER "PERSON_BI0" BEFORE INSERT ON PERSON
FOR EACH ROW
DECLARE
  raid NUMBER(10);
      insert_data CLOB;
   erid NUMBER(10);
   usr VARCHAR(100);
    v_new_creator NUMBER;
     BEGIN
 BEGIN
   v_new_creator := :NEW.creator;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;

  SELECT TRUNC(seq_rid.NEXTVAL)

  INTO erid FROM dual;

  :NEW.rid := erid ;

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;



  audit_trail.record_transaction

    (raid, 'PERSON', erid, 'I', USR );



-- Added by Ganapathy on 23/06/05 for Audit Insert
       insert_data:= :NEW.FK_CODELST_PAT_DTH_CAUSE||'|'||
    :NEW.CAUSE_OF_DEATH_OTHER||'|'|| :NEW.PK_PERSON||'|'||:NEW.PERSON_CODE||'|'||
    :NEW.PERSON_ALTID||'|'||
    :NEW.PERSON_LNAME||'|'||:NEW.PERSON_FNAME||'|'||:NEW.PERSON_MNAME||'|'||
    :NEW.PERSON_SUFFIX||'|'||:NEW.PERSON_PREFIX||'|'||:NEW.PERSON_DEGREE||'|'||
    TO_CHAR(:NEW.PERSON_DOB,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.FK_CODELST_GENDER||'|'||
    :NEW.PERSON_AKA||'|'|| :NEW.FK_CODELST_RACE||'|'||
     :NEW.FK_CODELST_PRILANG||'|'|| :NEW.FK_CODELST_MARITAL||'|'|| :NEW.FK_ACCOUNT||'|'||:NEW.PERSON_SSN||'|'||
  :NEW.PERSON_DRIV_LIC||'|'|| :NEW.PERSON_ETHGRP||'|'||:NEW.PERSON_BIRTH_PLACE||'|'||
   :NEW.PERSON_MULTI_BIRTH||'|'|| :NEW.FK_CODELST_CITIZEN||'|'||:NEW.PERSON_MILVET||'|'||
    :NEW.FK_CODELST_NATIONALITY||'|'|| TO_CHAR(:NEW.PERSON_DEATHDT,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.FK_SITE||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.FK_CODELST_EMPLOYMENT||'|'|| :NEW.FK_CODELST_EDU||'|'||
     :NEW.FK_CODELST_PSTAT||'|'|| :NEW.FK_CODELST_BLOODGRP||'|'||:NEW.PERSON_ADDRESS1||'|'||
     :NEW.PERSON_ADDRESS2||'|'||:NEW.PERSON_CITY||'|'||:NEW.PERSON_STATE||'|'||
     :NEW.PERSON_ZIP||'|'||:NEW.PERSON_COUNTRY||'|'||:NEW.PERSON_HPHONE||'|'||
     :NEW.PERSON_BPHONE||'|'||:NEW.PERSON_EMAIL||'|'||:NEW.PERSON_COUNTY||'|'||
     TO_CHAR(:NEW.PERSON_REGDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.PERSON_REGBY||'|'|| :NEW.FK_CODLST_NTYPE||'|'||
     :NEW.PERSON_MOTHER_NAME||'|'|| :NEW.FK_PERSON_MOTHER||'|'|| :NEW.FK_CODELST_RELIGION||'|'||
     -- Added by Gopu to fix the bugzilla Issue #2956
     :NEW.PAT_FACILITYID||'|'||
      :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||  :NEW.IP_ADD||'|'|| :NEW.FK_TIMEZONE||'|'||
   :NEW.PERSON_PHYOTHER||'|'||:NEW.PERSON_ORGOTHER||'|'||:NEW.PERSON_SPLACCESS||'|'||
   :NEW.FK_CODELST_ETHNICITY||'|'||:NEW.PERSON_ADD_RACE||'|'||:NEW.PERSON_ADD_ETHNICITY;--||'|'||dbms_lob.substr(:NEW.PERSON_NOTES_CLOB,4000);--KM

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

   END;
/


