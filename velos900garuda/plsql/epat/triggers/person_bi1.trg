CREATE OR REPLACE TRIGGER "PERSON_BI1" 
BEFORE INSERT
ON PERSON 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
NEW.fk_timezone IS NULL OR NEW.fk_timezone = 0
      )
DECLARE

tzpk NUMBER(10);

BEGIN

 BEGIN

  SELECT nvl(trim(codelst_subtyp),0)
  INTO tzpk
  FROM sch_codelst  
  WHERE trim(codelst_type) = 'DBTimezone' ;
  
  if tzpk = 0 then
    tzpk:= 136;
end if;  
  
  EXCEPTION WHEN NO_DATA_FOUND THEN
     tzpk := 136 ;
 END ;

 :NEW.fk_timezone := tzpk;

END;
/


