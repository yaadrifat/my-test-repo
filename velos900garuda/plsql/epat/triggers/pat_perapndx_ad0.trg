CREATE OR REPLACE TRIGGER "PAT_PERAPNDX_AD0" 
  after delete
  on pat_perapndx
  for each row
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'PAT_PERAPNDX', :old.rid, 'D');

  deleted_data :=
  to_char(:old.pk_perapndx) || '|' ||
  to_char(:old.fk_per) || '|' ||
  to_char(:old.perapndx_date) || '|' ||
  :old.perapndx_desc || '|' ||
  :old.perapndx_uri || '|' ||
  :old.perapndx_type || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


