CREATE OR REPLACE PACKAGE BODY        "PKG_CAL_ENH_DATAMIG" AS

PROCEDURE SP_dm_visits_for_allevents
AS
/****************************************************************************************************
   ** Procedure to create generated visits for all events

   ** SamV	   No 02, 2004, created this proc

   *****************************************************************************************************
   */
   v_EVENT_ID           NUMBER;
   v_CHAIN_ID           NUMBER;
   v_EVENT_TYPE         CHAR (1);
   v_NAME               VARCHAR2 (50);
   v_DURATION           NUMBER;
   v_USER_ID            NUMBER;
   v_DISPLACEMENT       NUMBER;
   v_CURRVAL          NUMBER;
   v_COUNT            NUMBER;
   V_EVENT_VISIT_ID	  NUMBER; --SV, 9/30
   V_RET			  NUMBER;
   v_protocol_id NUMBER;
   v_ipadd VARCHAR2(15);
   -- pick up all the protocol records and the scheduled event records
   CURSOR C1
   IS
      SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, DURATION, USER_ID, DISPLACEMENT,NVL(FK_VISIT, 0), IP_ADD
      FROM EVENT_DEF
	  WHERE (EVENT_TYPE = 'A' AND DISPLACEMENT <> 0)
	  ORDER BY CHAIN_ID, EVENT_ID;


BEGIN


   OPEN C1;


   LOOP
      FETCH C1 INTO
	  		v_EVENT_ID,
			v_CHAIN_ID,
			v_EVENT_TYPE,
			v_NAME,
			v_DURATION,
			v_USER_ID,
			v_DISPLACEMENT,
			V_EVENT_VISIT_ID,
			v_ipadd;

      EXIT WHEN C1%NOTFOUND;


IF (v_event_visit_id = 0) THEN
		         Sp_Get_Prot_Visit(V_CHAIN_ID,v_DISPLACEMENT, v_USER_ID, v_IPADD, v_ret);
				 IF (v_ret > 0) THEN
				 	UPDATE EVENT_DEF
	 					 SET FK_VISIT = v_ret
	 					 WHERE EVENT_ID = V_EVENT_ID;
				 END IF;


        DBMS_OUTPUT.PUT_LINE('ev='||TO_CHAR(v_event_id));
 END IF;


   END LOOP;

   CLOSE C1;
/**********************************
TEST
set serveroutput on
declare
myevent_id number ;

begin
sp_copyprotocol(5128,'checkup1',myevent_id) ;
dbms_output.put_line(to_char(myevent_id) ) ;
end ;
**************************************/

   COMMIT;
END SP_dm_visits_for_allevents;


PROCEDURE SP_dm_visits_for_assoc
AS
/****************************************************************************************************
   ** Procedure to create generated visits for all events

   ** SamV	   No 02, 2004, created this proc

   *****************************************************************************************************
   */
   v_EVENT_ID           NUMBER;
   v_CHAIN_ID           NUMBER;
   v_EVENT_TYPE         CHAR (1);
   v_NAME               VARCHAR2 (50);
   v_DURATION           NUMBER;
   v_USER_ID            NUMBER;
   v_DISPLACEMENT       NUMBER;
   v_CURRVAL          NUMBER;
   v_COUNT            NUMBER;
   V_EVENT_VISIT_ID	  NUMBER; --SV, 9/30
   V_RET			  NUMBER;
   v_protocol_id NUMBER;
   v_ipadd VARCHAR2(15);
   -- pick up all the protocol records and the scheduled event records
   CURSOR C1
   IS
      SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, DURATION, USER_ID, DISPLACEMENT,NVL(FK_VISIT, 0), IP_ADD
      FROM EVENT_ASSOC
	  WHERE (EVENT_TYPE = 'A' AND DISPLACEMENT <> 0  AND EVENT_CALASSOCTO='P')
	  ORDER BY CHAIN_ID, EVENT_ID;


BEGIN


   OPEN C1;


   LOOP
      FETCH C1 INTO
	  		v_EVENT_ID,
			v_CHAIN_ID,
			v_EVENT_TYPE,
			v_NAME,
			v_DURATION,
			v_USER_ID,
			v_DISPLACEMENT,
			V_EVENT_VISIT_ID,
			v_ipadd;

      EXIT WHEN C1%NOTFOUND;


IF (v_event_visit_id = 0) THEN
		         Sp_Get_Prot_Visit(V_CHAIN_ID,v_DISPLACEMENT, v_USER_ID, v_IPADD, v_ret);
				 IF (v_ret > 0) THEN
				 	UPDATE EVENT_ASSOC
	 					 SET FK_VISIT = v_ret
	 					 WHERE EVENT_ID = V_EVENT_ID
						 AND EVENT_CALASSOCTO='P';
				 END IF;


        DBMS_OUTPUT.PUT_LINE('ev='||TO_CHAR(v_event_id));
 END IF;


   END LOOP;

   CLOSE C1;
/**********************************
TEST
set serveroutput on
declare
myevent_id number ;

begin
sp_copyprotocol(5128,'checkup1',myevent_id) ;
dbms_output.put_line(to_char(myevent_id) ) ;
end ;
**************************************/

--   COMMIT;
END SP_dm_visits_for_assoc;

PROCEDURE SP_dm_patient_sched
AS
/****************************************************************************************************
   ** Procedure to create generated visits for all events

   ** SamV	   No 02, 2004, created this proc

   *****************************************************************************************************
   */
   v_EVENT_ID           NUMBER;
   v_CHAIN_ID           NUMBER;
   v_EVENT_TYPE         CHAR (1);
   v_NAME               VARCHAR2 (50);
   v_DURATION           NUMBER;
   v_USER_ID            NUMBER;
   v_DISPLACEMENT       NUMBER;
   v_CURRVAL          NUMBER;
   v_COUNT            NUMBER;
   V_EVENT_VISIT_ID	  NUMBER; --SV, 9/30
   V_RET			  NUMBER;
   v_protocol_id NUMBER;
   v_ipadd VARCHAR2(15);
   -- pick up all the protocol records and the scheduled event records
   CURSOR C1
   IS
      SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, DURATION, USER_ID, DISPLACEMENT,NVL(FK_VISIT, 0), IP_ADD
      FROM EVENT_ASSOC
	  WHERE (EVENT_TYPE = 'A' AND DISPLACEMENT <> 0 AND EVENT_CALASSOCTO='P')
	  ORDER BY CHAIN_ID, EVENT_ID;


BEGIN


   OPEN C1;


   LOOP
      FETCH C1 INTO
	  		v_EVENT_ID,
			v_CHAIN_ID,
			v_EVENT_TYPE,
			v_NAME,
			v_DURATION,
			v_USER_ID,
			v_DISPLACEMENT,
			V_EVENT_VISIT_ID,
			v_ipadd;

      EXIT WHEN C1%NOTFOUND;


IF (v_event_visit_id > 0) THEN

				 	UPDATE SCH_EVENTS1
	 					 SET FK_VISIT = V_EVENT_VISIT_ID
	 					 WHERE FK_ASSOC = V_EVENT_ID;



        DBMS_OUTPUT.PUT_LINE('ps='||TO_CHAR(v_event_id));
 END IF;


   END LOOP;

   CLOSE C1;
/**********************************
TEST
set serveroutput on
declare
myevent_id number ;

begin
sp_copyprotocol(5128,'checkup1',myevent_id) ;
dbms_output.put_line(to_char(myevent_id) ) ;
end ;
**************************************/

   COMMIT;
END;



PROCEDURE SP_dm_event_milestone
AS
/****************************************************************************************************
   ** Procedure to create generated visits for all events

   ** SamV	   No 02, 2004, created this proc

   *****************************************************************************************************
   */
   v_EVENT_ID           NUMBER;
   v_CHAIN_ID           NUMBER;
   v_EVENT_TYPE         CHAR (1);
   v_NAME               VARCHAR2 (50);
   v_DURATION           NUMBER;
   v_USER_ID            NUMBER;
   v_DISPLACEMENT       NUMBER;
   v_CURRVAL          NUMBER;
   v_COUNT            NUMBER;
   V_EVENT_VISIT_ID	  NUMBER; --SV, 9/30
   V_RET			  NUMBER;
   v_protocol_id NUMBER;
   v_ipadd VARCHAR2(15);
   -- pick up all the protocol records and the scheduled event records
   CURSOR C1
   IS
      SELECT EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME, DURATION, USER_ID, DISPLACEMENT,NVL(FK_VISIT, 0), IP_ADD
      FROM EVENT_ASSOC
	  WHERE (EVENT_TYPE = 'A' AND DISPLACEMENT <> 0 AND EVENT_CALASSOCTO='P')
	  ORDER BY CHAIN_ID, EVENT_ID;


BEGIN


   OPEN C1;


   LOOP
      FETCH C1 INTO
	  		v_EVENT_ID,
			v_CHAIN_ID,
			v_EVENT_TYPE,
			v_NAME,
			v_DURATION,
			v_USER_ID,
			v_DISPLACEMENT,
			V_EVENT_VISIT_ID,
			v_ipadd;

      EXIT WHEN C1%NOTFOUND;


IF (v_event_visit_id > 0) THEN

				 	UPDATE ER_MILESTONE
	 					 SET FK_VISIT = V_EVENT_VISIT_ID
	 					 WHERE FK_EVENTASSOC = V_EVENT_ID AND milestone_type = 'EM';



        DBMS_OUTPUT.PUT_LINE('ps='||TO_CHAR(v_event_id));
 END IF;


   END LOOP;

   CLOSE C1;
/**********************************
TEST
set serveroutput on
declare
myevent_id number ;

begin
sp_copyprotocol(5128,'checkup1',myevent_id) ;
dbms_output.put_line(to_char(myevent_id) ) ;
end ;
**************************************/

   COMMIT;
END SP_dm_event_milestone;






PROCEDURE SP_dm_update_flags
AS

BEGIN

	UPDATE EVENT_ASSOC
	SET duration_Unit = 'D'
	 WHERE duration_Unit IS NULL
	 AND event_type = 'P' AND EVENT_CALASSOCTO='P' ;

	 UPDATE  EVENT_DEF
	 SET calendar_sharedwith = 'A'
	 WHERE calendar_sharedwith IS NULL
	 AND event_type = 'P' ;

	UPDATE EVENT_DEF
	SET duration_Unit = 'D'
	 WHERE duration_Unit IS NULL
	 AND event_type = 'P'  ;

COMMIT;
END sp_dm_update_flags;


END Pkg_Cal_Enh_Datamig;
/


CREATE SYNONYM ERES.PKG_CAL_ENH_DATAMIG FOR PKG_CAL_ENH_DATAMIG;


CREATE SYNONYM EPAT.PKG_CAL_ENH_DATAMIG FOR PKG_CAL_ENH_DATAMIG;


GRANT EXECUTE, DEBUG ON PKG_CAL_ENH_DATAMIG TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_CAL_ENH_DATAMIG TO ERES;

