CREATE OR REPLACE PACKAGE BODY      "PKG_GENSCH"  IS
   PROCEDURE deprecated_SP_GENCH (
      p_protocol    IN   NUMBER,
      p_patientid   IN   NUMBER,
      p_date        IN   DATE,
      p_patprotid   IN   NUMBER,
      p_days        IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2
   )
   AS
      v_pro_id           CHAR (10);
      v_currval          CHAR (10);
      my_date            DATE;
      v_study            NUMBER;
      v_notdone          NUMBER;
      vcursch            NUMBER          := 0;
      v_sql              VARCHAR2 (2000);
--SV, 9/27/04, added to use defined visit names if they are available.
      v_defined_visits   NUMBER          := 0;
      v_calassoc         CHAR (1);
      v_protocol_study   NUMBER;
      v_patprot          NUMBER;
	  v_err VARCHAR(4000);
	  v_mindisp NUMBER;
	  v_days NUMBER;
   BEGIN
       --Check if the schedule is generated from Study admin. If passed from Study admin calendar parameter will contains different values
      -- fk_patprot will contain study primary key if called from study admin calendar. If called from patient schedule, it will contain patprot Id.
      --PatientId is not needed for studyadmin schedule,so will contain 0
      BEGIN
         v_patprot := p_patprotid;

         SELECT event_calassocto, chain_id
           INTO v_calassoc, v_protocol_study
           FROM EVENT_ASSOC
          WHERE event_id = p_protocol AND UPPER (event_type) = 'P';

		  IF (p_days=1)  THEN
		  	 		  SELECT MIN(displacement) INTO v_mindisp FROM EVENT_ASSOC WHERE chain_id=p_protocol AND UPPER(event_type)='A';

					  IF (v_mindisp<0) THEN
					  	  v_days:=v_mindisp;
					  ELSE
					  	  v_days:=p_days;
					  END IF;

		   ELSE
		 		  v_days:=p_days;

		  END IF;

		 IF (v_calassoc = 'S')    THEN

            SELECT COUNT (*)
              INTO vcursch
              FROM SCH_EVENTS1
             WHERE fk_study =
                      v_patprot
					  AND session_id=LPAD(TO_CHAR(p_protocol),10,'0')
                    --p_patprotid will contain study primary key in this case.
               AND status = 0;

			v_study := v_patprot;
            v_patprot := 0;
         ELSE
            v_protocol_study := 0;

            SELECT COUNT (*)
              INTO vcursch
              FROM SCH_EVENTS1
             WHERE fk_patprot = v_patprot AND status = 0;
       END IF;

         SELECT COUNT (*)
           INTO v_defined_visits
           FROM SCH_PROTOCOL_VISIT
          WHERE fk_protocol = p_protocol;

         IF vcursch = 0
         THEN
            SELECT chain_id
              INTO v_study
              FROM EVENT_ASSOC
             WHERE event_id = p_protocol AND event_type = 'P';

            SELECT pk_codelst
              INTO v_notdone
              FROM SCH_CODELST
             WHERE codelst_subtyp = 'ev_notdone';


            v_sql :=
                  'INSERT INTO SCH_EVENTS1  ( EVENT_ID, LOCATION_ID, NOTES, DESCRIPTION, BOOKED_BY,'
               || 'START_DATE_TIME, END_DATE_TIME, PATIENT_ID, OBJECT_ID, STATUS, TYPE_ID, '
               || 'SESSION_ID, VISIT_TYPE_ID, FK_ASSOC,  ISCONFIRMED, BOOKEDON, CREATOR , LAST_MODIFIED_BY ,'
               || ' LAST_MODIFIED_DATE ,CREATED_ON, IP_ADD, FK_PATPROT,ACTUAL_SCHDATE, VISIT,'
               || 'FK_VISIT,event_notes  ,fk_study, EVENT_SEQUENCE )';
                              --SV, 10/28/04, to overcome the visit # dilemma.
            v_sql :=
                  v_sql
               || '(SELECT lpad (to_char (SCH_EVENTS1_SEQ1.NEXTVAL), 10, ''0''), ''173'', null, '
               || 'NAME, ''0000000525'', ((:a  + DISPLACEMENT) - 1) ,(( :b + DISPLACEMENT) - 1) + DURATION,'
               || 'lpad (to_char ('
               || p_patientid
               || '), 10, ''0''), ''0000001476'', 0, ''165'','
               || 'lpad (to_char ('
               || p_protocol
               || '), 10, ''0''), FUZZY_PERIOD, EVENT_ID,'
               || v_notdone
               || ' , :c , '
               || p_dmlby
               || ', NULL, NULL , SYSDATE ,'''
               || p_ipadd
               || ''',  '
               || v_patprot
               || ', ((:d + DISPLACEMENT) - 1),';


-- SV, 09/27/04, added as part of the cal-enh-03-c; display defined visits, if available.
            IF (v_defined_visits = 0)
            THEN
               -- SV, if there are no predefined visits , then use the original method of calculating visit # using dense rank.
               -- added fk_visit field in insert statement above: in this mode, without defined visits, fk_visit can stay null.
               v_sql :=
                     v_sql
               -- || 'dense_rank () over (partition by d.mindisp order by (displacement - d.mindisp )) visit '
			 --   || '(SELECT visit_no FROM sch_protocol_visit  WHERE pk_protocol_visit=fk_visit order by displacement) visit'
			 	 || '(select act_num from(select rownum act_num,a.*	from(	(SELECT pk_protocol_visit, visit_no, displacement  FROM sch_protocol_visit '
				  || ' where fk_protocol = '||p_protocol||'	order by   displacement asc, visit_no)) a)b	where pk_protocol_visit = fk_visit) visit'

        	    || ' ,NULL ,notes,'
                  || v_protocol_study
                  || 'FROM EVENT_ASSOC  , (select min(displacement) mindisp from  event_assoc '
                  || 'where chain_id  = '
                  || p_protocol
                  || ' and EVENT_TYPE = ''A'' '
                  || 'and displacement <> 0 and displacement >= '
                  || v_days
                  || ' ) d';
               v_sql :=
                     v_sql
                  || ' WHERE CHAIN_ID = '
                  || p_protocol
                  || ' and EVENT_TYPE = ''A'' and displacement <> 0 and DISPLACEMENT >= '
                  || v_days
                  || ' )';
            ELSE

               v_sql :=
                     v_sql
                  --|| 'dense_rank () over (partition by d.mindisp order by (displacement - d.mindisp )) visit '
--		  			    || '(SELECT visit_no FROM sch_protocol_visit  WHERE pk_protocol_visit=fk_visit) visit'
				  || '(select act_num from(select rownum act_num,a.*	from(	(SELECT pk_protocol_visit, visit_no, displacement  FROM sch_protocol_visit '
				  || ' where fk_protocol = '||p_protocol||'	order by   displacement asc, visit_no)) a)b	where pk_protocol_visit = fk_visit) visit'
				  || ' , fk_visit ,notes,'
                  || v_protocol_study
                  ||', EVENT_SEQUENCE'--JM: 19APR2008, #C8: Mar-Apr2008 Enhancements
                  || ' FROM EVENT_ASSOC  assoc , (select min(displacement) mindisp from  event_assoc '
                  || 'where chain_id  = '
                  || p_protocol
                  || ' and EVENT_TYPE = ''A'' '
                  || 'and displacement <> 0 and displacement >= '
                  || v_days
                  || ' ) d';
               v_sql :=
                     v_sql
                  || ' WHERE CHAIN_ID = '
                  || p_protocol
                  || ' and EVENT_TYPE = ''A'' and (HIDE_FLAG is null or HIDE_FLAG <>1) and displacement <> 0 and DISPLACEMENT >= '--JM: added and (HIDE_FLAG is null or HIDE_FLAG <>1)
                  || v_days
                  || '  )';
            END IF;

			plog.DEBUG(pctx , 'schedule sql' || v_sql);


		    EXECUTE IMMEDIATE v_sql
                        USING p_date, p_date, p_date, p_date;


               IF v_calassoc = 'S' THEN
			   	  -- for admin schedule

				    sp_admin_msg_to_tran (   p_protocol  ,   v_study ,   p_dmlby,    p_ipadd );
			   ELSE -- patient schedule

		            -- for clubbed notifications
		            sp_addmsg_to_tran (p_patientid,
		                               v_study,
		                               p_dmlby,
		                               p_ipadd,
		                               v_patprot
		                              );

				END IF;


            --change status of mails according to current alert/notify setting
            sp_setmailstat (v_patprot, p_dmlby, p_ipadd);

            IF (NVL (v_calassoc, 'P') = 'P')
            THEN
               Sp_Copyprotcrf (p_patprotid, p_dmlby, p_ipadd);
                                 -- procedure to add crf on 7th feb -- dinesh
            END IF;

            COMMIT;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN

		 v_err := SQLERRM;


            RAISE;
      END;
   END deprecated_SP_GENCH;

   PROCEDURE sp_addmsg (
      p_patientid   IN   NUMBER,
      p_study       IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
      p_patprot     IN   NUMBER
   )
   AS
/*
** Modification History
**
** Modified By         Date     Remarks
** Charanjiv       29th Sept  Aded the Study Id in the sch_dispatchmsg table
** Charanjiv       06th Nov   Added the defulat column for creator, last_modified date/by and
**                            created on in the insert modules.
*/
      v_msgtemplate   VARCHAR2 (4000);
      v_static        VARCHAR2 (100);
      v_patcode       VARCHAR2 (25);
   BEGIN
      v_msgtemplate := Pkg_Common.sch_getmailmsg ('event_msg');
      v_static := '~1,~2,~3,';

      SELECT Pkg_Common.sch_getpatcode (p_patprot)
        INTO v_patcode
        FROM DUAL;                                     -- v_patcode is param 1

      INSERT INTO SCH_DISPATCHMSG
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_pat, fk_study, creator, last_modified_by,
                   last_modified_date, created_on, ip_add, fk_patprot,
                   fk_event)
         SELECT sch_dispatchmsg_seq1.NEXTVAL, actual_schdate - usr_daysbefore,
                0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           substr(a.description,1,500) || '...'
                                        || '~'
                                        || v_patcode
                                        || '~'
                                        || a.actual_schdate
                                        || '~'
                                        || usr_msgbefore
                                       ),
                'U', a.event_id, c.eventusr, p_study, p_dmlby, p_dmlby,
                SYSDATE, SYSDATE, p_ipadd, p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b, SCH_EVENTUSR c
          WHERE fk_patprot = p_patprot
            AND a.fk_assoc = b.event_id
            AND usr_msgbefore IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S';

      INSERT INTO SCH_DISPATCHMSG
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_pat, fk_study, creator, last_modified_by,
                   last_modified_date, created_on, ip_add, fk_patprot,
                   fk_event)
         SELECT sch_dispatchmsg_seq1.NEXTVAL, actual_schdate + usr_daysafter,
                0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           substr(a.description,1,500) || '...'
                                        || '~'
                                        || v_patcode
                                        || '~'
                                        || a.actual_schdate
                                        || '~'
                                        || usr_msgafter
                                       ),
                'U', a.event_id, c.eventusr, p_study, p_dmlby, p_dmlby,
                SYSDATE, SYSDATE, p_ipadd, p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b, SCH_EVENTUSR c
          WHERE fk_patprot = p_patprot
            AND a.fk_assoc = b.event_id
            AND usr_msgafter IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S';

      INSERT INTO SCH_DISPATCHMSG
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_pat, fk_study, creator, last_modified_by,
                   last_modified_date, created_on, ip_add, fk_patprot,
                   fk_event)
         SELECT sch_dispatchmsg_seq1.NEXTVAL, actual_schdate - pat_daysbefore,
                0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           substr(a.description,1,500) || '...'
                                        || '~'
                                        || v_patcode
                                        || '~'
                                        || a.actual_schdate
                                        || '~'
                                        || pat_msgbefore
                                       ),
                'P', a.event_id, p_patientid, p_study, p_dmlby, p_dmlby,
                SYSDATE, SYSDATE, p_ipadd, p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b
          WHERE fk_patprot = p_patprot
            AND a.fk_assoc = b.event_id
            AND pat_msgbefore IS NOT NULL;

      INSERT INTO SCH_DISPATCHMSG
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_pat, fk_study, creator, last_modified_by,
                   last_modified_date, created_on, ip_add, fk_patprot,
                   fk_event)
         SELECT sch_dispatchmsg_seq1.NEXTVAL, actual_schdate + pat_daysafter,
                0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           substr(a.description,1,500) || '...'
                                        || '~'
                                        || v_patcode
                                        || '~'
                                        || a.actual_schdate
                                        || '~'
                                        || pat_msgafter
                                       ),
                'P', a.event_id, p_patientid, p_study, p_dmlby, p_dmlby,
                SYSDATE, SYSDATE, p_ipadd, p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b
          WHERE fk_patprot = p_patprot
            AND a.fk_assoc = b.event_id
            AND pat_msgafter IS NOT NULL;

      COMMIT;
   END sp_addmsg;

   PROCEDURE sp_update_actdt (
      p_oldactdt     IN   VARCHAR,
      p_newactdt     IN   VARCHAR,
      p_event_id     IN   NUMBER,
      p_fk_patprot   IN   NUMBER,
      p_flag         IN   NUMBER,
      p_ip           IN   VARCHAR,
      p_modby        IN   NUMBER,
	    p_calassoc IN CHAR     ,
      p_synch_sugdate IN NUMBER
   )
   AS
      v_sqlstr    VARCHAR2 (2000);
      -- v_notdone   NUMBER;
      v_msgsql    VARCHAR2 (2000);
      v_chdt      DATE;
      v_days      NUMBER :=0;
      v_newdt     DATE;
	    v_cal NUMBER;
	   v_visit NUMBER;--JM

     fk_vis NUMBER;--KM
     fk_vis_next NUMBER;
     i number;



   BEGIN

      v_newdt := TO_DATE (p_newactdt, PKG_DATEUTIL.F_GET_DATEFORMAT);

      if (p_oldactdt <> 'Null') then
          v_chdt := TO_DATE (p_oldactdt, PKG_DATEUTIL.F_GET_DATEFORMAT);
          v_days := v_newdt - v_chdt;
      end if;
    
	   SELECT session_id INTO v_cal FROM SCH_EVENTS1 WHERE
	   event_id = LPAD (TO_CHAR (p_event_id), 10, '0');

	--KM-20Jan10-#4514
      IF p_flag = 1 THEN
   		/* to reschedule only current event*/
        UPDATE SCH_MSGTRAN                           --for clubbed email enh
        SET msg_sendon = msg_sendon + v_days
        WHERE fk_schevent = p_event_id;
        --KM
        UPDATE SCH_EVENTS1
        SET actual_schdate = (case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end),START_DATE_TIME = decode(p_synch_sugdate,0,(case when (actual_schdate is null) then v_newdt else START_DATE_TIME end),1,(case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end)),
            last_modified_by = p_modby, last_modified_date = sysdate,
            ip_add = p_ip
        WHERE event_id = LPAD (TO_CHAR (p_event_id), 10, '0');

      ELSIF p_flag = 0 THEN
		/*Move all subsequent events accordingly*/
		IF  (p_calassoc='S') THEN -- called from studyadmin screen
			UPDATE SCH_MSGTRAN                           -- for clubbed mail enh
            SET msg_sendon = msg_sendon + v_days
			WHERE fk_study = p_fk_patprot
            AND fk_schevent IN (
                SELECT event_id
                FROM SCH_EVENTS1
                WHERE fk_study = p_fk_patprot
                AND (event_id = LPAD (TO_CHAR (p_event_id), 10, '0')
                OR (event_id != LPAD (TO_CHAR (p_event_id), 10, '0')
                AND actual_schdate >= v_chdt and actual_schdate is not null))
                AND STATUS = 0
                AND session_id=v_cal );


			-- KM,01Oct09 - Modified for 'Not Defined visits'
			--YK Modified for Bug#9572
			UPDATE SCH_EVENTS1
			SET actual_schdate = (case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end),START_DATE_TIME = decode(p_synch_sugdate,0,(case when (actual_schdate is null) then v_newdt else START_DATE_TIME end),1,(case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end)),
				last_modified_by = p_modby, last_modified_date = sysdate,
				ip_add = p_ip
			WHERE fk_study = p_fk_patprot
			AND (event_id = LPAD (TO_CHAR (p_event_id), 10, '0')
			OR (event_id != LPAD (TO_CHAR (p_event_id), 10, '0')
            AND (actual_schdate >= v_chdt and actual_schdate is not null)))
            AND STATUS = 0
            AND session_id=v_cal;

		ELSE  -- else for p_calassoc="S"
			UPDATE SCH_MSGTRAN                           -- for clubbed mail enh
            SET msg_sendon = msg_sendon + v_days
			WHERE fk_patprot = p_fk_patprot
            AND fk_schevent IN (
                SELECT event_id
                FROM SCH_EVENTS1
                WHERE fk_patprot = p_fk_patprot
                AND (event_id = LPAD (TO_CHAR (p_event_id), 10, '0')
                OR (event_id != LPAD (TO_CHAR (p_event_id), 10, '0')
                AND (actual_schdate >= v_chdt and actual_schdate is not null)))
                );

			--KM-1Oct05 -#4313
			--YK Modified for Bug#9572
			UPDATE SCH_EVENTS1
            SET actual_schdate = (case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end),START_DATE_TIME = decode(p_synch_sugdate,0,(case when (actual_schdate is null) then v_newdt else START_DATE_TIME end),1,(case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end)),
                last_modified_by = p_modby, last_modified_date = sysdate,
                ip_add = p_ip
			WHERE fk_patprot = p_fk_patprot
			AND (event_id = LPAD (TO_CHAR (p_event_id), 10, '0')
			OR (event_id != LPAD (TO_CHAR (p_event_id), 10, '0')
            AND (actual_schdate >= v_chdt and actual_schdate is not null)));

		END IF;

      ELSIF p_flag = 2 THEN
		/*Move the events scheduled on current and next visit accordingly*/
		IF (p_calassoc='S') THEN
            --KM-10Dec09-#3213
            select fk_visit into fk_vis from sch_events1 where event_id = LPAD(TO_CHAR (p_event_id), 10, '0') and fk_study = p_fk_patprot and session_id = v_cal;
            for p in (select fk_visit from sch_events1 where fk_study = p_fk_patprot
			and (fk_visit = fk_vis or (fk_visit <> fk_vis and actual_schdate is not null))
			and session_id = v_cal and STATUS = 0 order by actual_schdate,fk_visit )
            loop
               if(fk_vis = p.fk_visit) then
                  i := 1;
                  fk_vis_next := p.fk_visit;
               end if;

               if (i = 1 and  fk_vis <> p.fk_visit) then
                  fk_vis_next := p.fk_visit;
                  exit;
               end if;
            end loop;



            UPDATE SCH_MSGTRAN
            SET msg_sendon = msg_sendon + v_days
            WHERE fk_study = p_fk_patprot
            AND (fk_schevent IN (SELECT event_id FROM SCH_EVENTS1 WHERE fk_study = p_fk_patprot AND session_id = v_cal 
            AND STATUS = 0 and fk_visit in (fk_vis, fk_vis_next) ));

            UPDATE SCH_EVENTS1
            SET actual_schdate = (case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end),START_DATE_TIME = decode(p_synch_sugdate,0,(case when (actual_schdate is null) then v_newdt else START_DATE_TIME end),1,(case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end)),
                last_modified_by = p_modby, last_modified_date = sysdate,
                ip_add = p_ip
            WHERE (fk_visit in (fk_vis,fk_vis_next) ) AND fk_study = p_fk_patprot AND session_id=v_cal AND STATUS = 0;

		ELSE
            select fk_visit into fk_vis from sch_events1 where event_id = LPAD (TO_CHAR (p_event_id), 10, '0');

            for p in (select fk_visit from sch_events1 where fk_patprot = p_fk_patprot
			and (fk_visit = fk_vis or (fk_visit <> fk_vis and actual_schdate is not null))
            order by actual_schdate,fk_visit )
            loop
               if(fk_vis = p.fk_visit) then
                  i := 1;
                  fk_vis_next := p.fk_visit;
               end if;

               if (i = 1 and  fk_vis <> p.fk_visit) then
                  fk_vis_next := p.fk_visit;
                  exit;
               end if;
            end loop;

          	UPDATE SCH_MSGTRAN
            SET msg_sendon = msg_sendon + v_days
			WHERE fk_patprot = p_fk_patprot
            AND (fk_schevent IN ( SELECT event_id FROM SCH_EVENTS1 WHERE fk_visit in (fk_vis,fk_vis_next) and fk_patprot = p_fk_patprot));

          	UPDATE SCH_EVENTS1
            SET actual_schdate = (case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end), START_DATE_TIME = decode(p_synch_sugdate,0,(case when (actual_schdate is null) then v_newdt else START_DATE_TIME end),1,(case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end)),
                last_modified_by = p_modby, last_modified_date = sysdate,
                ip_add = p_ip
			WHERE (fk_visit in (fk_vis,fk_vis_next))  AND fk_patprot = p_fk_patprot;
		END IF;


 		--JM:21July2006
		--JM: If actual date is chnaged for an event belonging to a visit then for all other events too shift the actual date, for that particular visit
      ELSIF p_flag = 3 THEN
		/*Move All events of this visit accordingly*/
		--JM:09August2006: changes made based on code review commnets from S Abrol
		--IA:13Oct2006:Fix Bug :2686

	    IF (p_calassoc='S') THEN
			SELECT fk_visit INTO v_visit FROM SCH_EVENTS1 WHERE event_id = p_event_id
			AND FK_STUDY = p_fk_patprot ;

			UPDATE SCH_MSGTRAN                           --for clubbed email enh
			SET msg_sendon = msg_sendon + v_days
			WHERE fk_schevent IN (SELECT event_id FROM SCH_EVENTS1 WHERE fk_visit = v_visit
			AND FK_STUDY = p_fk_patprot AND STATUS = 0);

			UPDATE SCH_EVENTS1
			SET actual_schdate = (case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end),START_DATE_TIME = decode(p_synch_sugdate,0,(case when (actual_schdate is null) then v_newdt else START_DATE_TIME end),1,(case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end)),
				last_modified_by = p_modby, last_modified_date = sysdate,
				ip_add = p_ip
			WHERE event_id IN (SELECT event_id FROM SCH_EVENTS1 WHERE  fk_visit = v_visit
			AND FK_STUDY = p_fk_patprot) AND FK_STUDY = p_fk_patprot AND STATUS = 0;

		ELSE

			SELECT fk_visit INTO v_visit FROM SCH_EVENTS1 WHERE event_id = p_event_id
			AND FK_PATPROT = p_fk_patprot ;

			UPDATE SCH_MSGTRAN                           --for clubbed email enh
			SET msg_sendon = msg_sendon + v_days
			WHERE fk_schevent IN (SELECT event_id FROM SCH_EVENTS1 WHERE  fk_visit = v_visit
			AND FK_PATPROT = p_fk_patprot);

            UPDATE SCH_EVENTS1
			SET actual_schdate = (case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end),START_DATE_TIME = decode(p_synch_sugdate,0,(case when (actual_schdate is null) then v_newdt else START_DATE_TIME end),1,(case when (actual_schdate is null) then v_newdt else (actual_schdate + v_days) end)),
				last_modified_by = p_modby, last_modified_date = sysdate,
				ip_add = p_ip
			WHERE event_id IN (SELECT event_id FROM SCH_EVENTS1 WHERE  fk_visit = v_visit
			AND FK_PATPROT = p_fk_patprot) AND FK_PATPROT = p_fk_patprot;

			-------
		END IF;
      END IF;

      COMMIT;
   END sp_update_actdt;

   PROCEDURE sp_chgmsgstat (
      p_fk_patprot   IN   NUMBER,
      p_flag         IN   NUMBER,                                         /* */
      p_msgtype      IN   VARCHAR,
                      /*values will passed will be 'P' for mails for patients,
                     'U' for mails for users, 'A' for all messages*/
      p_ip           IN   VARCHAR,
      p_modby        IN   NUMBER
   )
/* Author - sonia sahni
   date - 12 dec 2001 */
   AS
      v_msgsql       VARCHAR2 (2000);
      v_msgstat      NUMBER;
      v_curmsgstat   NUMBER;
   BEGIN
/*set msg status according to the p_flag*/
      IF p_flag = 1
      THEN                 /* 0 will be passed to deactivate the disp mails*/
         v_msgstat := -2;
                      /* -2 will be set for temporary deactivation of mails*/
         v_curmsgstat := 0;
              /* only messages that are not sent as yet will be deactivated*/
      ELSIF p_flag = 0
      THEN                    /* 0 will be passed to activate the disp mails*/
         v_msgstat := 0;          /* 0  will be set for activation of mails*/
         v_curmsgstat := -2;
                   /* only messages that were deactivated will be activated*/
      END IF;

      v_msgsql :=
            'update  sch_dispatchmsg set MSG_STATUS = '
         || v_msgstat
         || ', LAST_MODIFIED_BY ='
         || p_modby
         || ',   IP_ADD = '''
         || p_ip
         || ''', LAST_MODIFIED_DATE  = sysdate  where fk_patprot = '
         || p_fk_patprot
         || ' and MSG_STATUS = '
         || v_curmsgstat
         || ' and fk_study is not null ';

      --and MSG_SENDON > = sysdate
      IF p_msgtype <> 'A'
      THEN
/*p(v_msgsql) ;*/
         v_msgsql :=
                 v_msgsql || ' and RTRIM(MSG_TYPE) = ''' || p_msgtype || '''';
/*p(v_msgsql) ;*/
      END IF;

/*insert into tmp (c3) values (p_oldactdt ||' * ' ||p_newactdt ||' * ' || to_char(p_event_id) ||' * ' || to_char(p_fk_patprot) ||' * ' || to_char(p_flag) ||' * ' ||p_ip ||' * ' || to_char(p_modby) ||' * '|| to_char(v_days) ||' * ' || to_char(v_chdt) || ' * ' || to_char(v_newdt));*/
      EXECUTE IMMEDIATE v_msgsql;
--COMMIT;
   END sp_chgmsgstat;

   PROCEDURE sp_copynotifysetting (
      p_old_fkpatprot   IN   NUMBER,
      p_new_fkpatprot   IN   NUMBER,
      p_copyflag        IN   NUMBER,
      p_creator         IN   NUMBER,
      p_ip              IN   VARCHAR
   )
   AS
/* Author - Sonia Sahni
   Date - 01/07/2002
   Purpose -
*/
      v_protocol   NUMBER;
      v_study      NUMBER;
      v_globflag   VARCHAR2 (2);
      v_copy       NUMBER;
   BEGIN
-- get new patprot's study and protocol
-- get its global setting
-- if global setting is 'G' or 'GR' do not copy existing patprot's settings
      SELECT fk_protocol, fk_study
        INTO v_protocol, v_study
        FROM ER_PATPROT
       WHERE pk_patprot = p_new_fkpatprot;

--get global setting
      BEGIN
         SELECT alnot_globalflag
           INTO v_globflag
           FROM SCH_ALERTNOTIFY
          WHERE fk_study = v_study
            AND fk_protocol = v_protocol
            AND fk_patprot IS NULL
            AND alnot_type = 'G';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_globflag := 'N';
            P (v_globflag);
      END;

--
      v_copy := 0;

      IF p_copyflag = 0
      THEN                                /* to insert blank setting records*/
         v_copy := 0;                                           --do not copy
      END IF;

      IF p_copyflag = 1
      THEN                        /* to retain alerts n notification records*/
         IF v_globflag = 'N' OR v_globflag = 'NR'
         THEN
            v_copy := 1;                                      --copy existing
         END IF;
      END IF;

      IF v_copy = 1
      THEN              /* copy old alert/notification setting for the user */
-- delete default alerts
         DELETE FROM SCH_ALERTNOTIFY s
               WHERE fk_patprot = p_new_fkpatprot AND alnot_type = 'A';

         INSERT INTO SCH_ALERTNOTIFY
                     (pk_alnot,                                           --1
                               fk_patprot,                                --2
                                          fk_codelst_an,                  --3
                                                        alnot_type,
                      alnot_users, alnot_mobeve, alnot_flag, creator,   --- 8
                      created_on, ip_add)
            (SELECT seq_sch_alertnotify.NEXTVAL, p_new_fkpatprot,
                    fk_codelst_an, alnot_type, alnot_users, alnot_mobeve,
                    alnot_flag,                                             --
                               p_creator, SYSDATE, p_ip
               FROM SCH_ALERTNOTIFY c
              WHERE c.fk_patprot = p_old_fkpatprot AND alnot_type <> 'G');

         --copy crf settings for old patprot
         INSERT INTO SCH_CRFNOTIFY
                     (pk_crfnot, fk_crf, fk_codelst_notstat, crfnot_usersto,
                      crfnot_notes, creator, created_on, ip_add, fk_study,
                      fk_protocol, fk_patprot, crfnot_globalflag)
            (SELECT seq_sch_crfnotify.NEXTVAL, fk_crf, fk_codelst_notstat,
                    crfnot_usersto, crfnot_notes, p_creator, SYSDATE, p_ip,
                    fk_study, fk_protocol, p_new_fkpatprot,
                    crfnot_globalflag
               FROM SCH_CRFNOTIFY
              WHERE fk_study = v_study
                AND fk_protocol = v_protocol
                AND fk_patprot = p_old_fkpatprot);

         COMMIT;
      END IF;
   END sp_copynotifysetting;

   PROCEDURE sp_setmailstat (
      p_patprot      IN   NUMBER,
      p_modifiedby   IN   NUMBER,
      p_ip           IN   VARCHAR
   )
   AS
/* Author - Sonia Sahni
   Date - 01/07/2002
   Purpose -
*/
      v_patalcode     NUMBER (10);
      v_useralcode    NUMBER (10);
      v_usersetting   NUMBER (1);
      v_patsetting    NUMBER (1);
   BEGIN
      SELECT pk_codelst
        INTO v_patalcode
        FROM SCH_CODELST
       WHERE codelst_subtyp = 'n_patnot';

      SELECT pk_codelst
        INTO v_useralcode
        FROM SCH_CODELST
       WHERE codelst_subtyp = 'n_usernot';

      SELECT alnot_flag
        INTO v_usersetting
        FROM SCH_ALERTNOTIFY
       WHERE fk_codelst_an = v_useralcode AND fk_patprot = p_patprot;

      SELECT alnot_flag
        INTO v_patsetting
        FROM SCH_ALERTNOTIFY
       WHERE fk_codelst_an = v_patalcode AND fk_patprot = p_patprot;

      IF v_usersetting = 1 AND v_patsetting = 1
      THEN
         --SP_CHGMSGSTAT(p_patprot,1,'A',p_ip,p_modifiedby);
         sp_chgmsgstat_tran (p_patprot, 1, 'A', p_ip, p_modifiedby);
      END IF;

      IF v_usersetting = 1
      THEN
         --SP_CHGMSGSTAT(p_patprot,1,'U',p_ip,p_modifiedby);
         sp_chgmsgstat_tran (p_patprot, 1, 'U', p_ip, p_modifiedby);
      END IF;

      IF v_patsetting = 1
      THEN
         --SP_CHGMSGSTAT(p_patprot,1,'P',p_ip,p_modifiedby);
         sp_chgmsgstat_tran (p_patprot, 1, 'P', p_ip, p_modifiedby);
      END IF;

      COMMIT;


   END sp_setmailstat;

--clubbed mails related procedures begin
-- sonia sahni
   PROCEDURE sp_addmsg_to_tran (
      p_patientid   IN   NUMBER,
      p_study       IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
      p_patprot     IN   NUMBER
   )
   AS
/*
** By Sonia Sahni Date - 17 Oct
** Modification History
**
** Modified By         Date     Remarks

** Sonia Abrol           06/26/06 changed the way messages are stores. Now they will have place holder sinstead of actual data. We will replace the place holders in PKG_CLUB
** Sonia abrol 12/13/06 do not generate messages with send on date less than current date
*/
      v_msgtemplate   VARCHAR2 (4000);
      v_patcode       VARCHAR2 (25);
   BEGIN


      v_msgtemplate := Pkg_Common.sch_getmailmsg ('c_evsch');

      SELECT Pkg_Common.sch_getpatcode (p_patprot)
        INTO v_patcode
        FROM DUAL;                                     -- v_patcode is param 1
      --KM-#4267
      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate - usr_daysbefore, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500) || '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || usr_msgbefore
                                       ),
                'ev_usch', a.event_id, c.eventusr, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a,
                EVENT_ASSOC b,
                SCH_EVENTUSR c,
                ER_STUDY d,
                ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND usr_msgbefore IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S'
			AND TRUNC(actual_schdate - usr_daysbefore) >= TRUNC(SYSDATE);

      --KM-#4267
      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate + usr_daysafter, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                            '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500)|| '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || usr_msgafter
                                       ),
                'ev_usch', a.event_id, c.eventusr, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a,
                EVENT_ASSOC b,
                SCH_EVENTUSR c,
                ER_STUDY d,
                ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND usr_msgafter IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S'
		AND TRUNC( actual_schdate + usr_daysafter) >= TRUNC(SYSDATE);

      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate - pat_daysbefore, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                            '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500)|| '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        ||  '[*ACTSCHDATE*]'
                                        || '~'
                                        || pat_msgbefore
                                       ),
                'ev_psch', a.event_id, p_patientid, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b, ER_STUDY d, ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND pat_msgbefore IS NOT NULL
		AND TRUNC( actual_schdate - pat_daysbefore) >= TRUNC(SYSDATE);

      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate + pat_daysafter, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500)|| '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || pat_msgafter
                                       ),
                'ev_psch', a.event_id, p_patientid, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b, ER_STUDY d, ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND pat_msgafter IS NOT NULL
			AND TRUNC( actual_schdate + pat_daysafter) >= TRUNC(SYSDATE);

      COMMIT;

   END sp_addmsg_to_tran;

-----------------------------------------------------------------------
   PROCEDURE sp_chgmsgstat_tran (
      p_fk_patprot   IN   NUMBER,
      p_flag         IN   NUMBER,                                         /* */
      p_msgtype      IN   VARCHAR,
                      /*values will passed will be 'P' for mails for patients,
                     'U' for mails for users, 'A' for all messages*/
      p_ip           IN   VARCHAR,
      p_modby        IN   NUMBER
   )
/* Author - sonia sahni
   date - 12 dec 2001 */
   AS
      v_msgsql           VARCHAR2 (2000);
      v_msgstat          NUMBER;
      v_curmsgstat       NUMBER;
      v_msgtype_clause   VARCHAR2 (100);
   BEGIN
      /*set msg status according to the p_flag*/
      IF p_flag = 1
      THEN                 /* 0 will be passed to deactivate the disp mails*/
         v_msgstat := -2;
                      /* -2 will be set for temporary deactivation of mails*/
         v_curmsgstat := 0;
              /* only messages that are not sent as yet will be deactivated*/
      ELSIF p_flag = 0
      THEN                    /* 0 will be passed to activate the disp mails*/
         v_msgstat := 0;          /* 0  will be set for activation of mails*/
         v_curmsgstat := -2;
                   /* only messages that were deactivated will be activated*/
      END IF;

      IF p_msgtype = 'A'
      THEN
         v_msgtype_clause :=
                          ' and RTRIM(MSG_TYPE) in (''ev_usch'',''ev_psch'')';
      ELSIF p_msgtype = 'P'
      THEN
         v_msgtype_clause := ' and RTRIM(MSG_TYPE) = ''ev_psch''';
      ELSIF p_msgtype = 'U'
      THEN
         v_msgtype_clause := ' and RTRIM(MSG_TYPE) = ''ev_usch''';
      END IF;

      v_msgsql :=
            'update  sch_msgtran set MSG_STATUS = '
         || v_msgstat
         || ' where fk_patprot = '
         || p_fk_patprot
         || ' and MSG_STATUS = '
         || v_curmsgstat
         || ' and fk_study is not null '
         || v_msgtype_clause;

/*insert into tmp (c3) values (p_oldactdt ||' * ' ||p_newactdt ||' * ' || to_char(p_event_id) ||' * ' || to_char(p_fk_patprot) ||' * ' || to_char(p_flag) ||' * ' ||p_ip ||' * ' || to_char(p_modby) ||' * '|| to_char(v_days) ||' * ' || to_char(v_chdt) || ' * ' || to_char(v_newdt));*/
      EXECUTE IMMEDIATE v_msgsql;
--COMMIT;
   END sp_chgmsgstat_tran;

-----------------------------------------------------------------------
   PROCEDURE sp_delete_study_calendar (
      p_study   IN       NUMBER,
      p_cal     IN       NUMBER,
      o_ret     OUT      NUMBER
   )
   AS
      /***************************************************************************************************
        ** Procedure to delete calendar from study
        ** Author: Sonika Talwar May 12, 2003
        ** Modified by: Sonika Talwar Sep 10, 2003 to check if the calendar is associated with the milestone
        ** Input parameter: Study Id
        ** Input parameter: Calendar Id
        ** Output parameter: 0:successful, -1:error, -2: if patients enrolled in the calendar, -3: if budget linked to the calendar
        **                   -4: if milestone is linked to the calendar
        **/
      v_cnt   NUMBER;
   BEGIN
--check if any patient enrolled to the study protocol calendar
      SELECT COUNT (*)
        INTO v_cnt
        FROM ER_PATPROT
       WHERE fk_study = p_study AND fk_protocol = p_cal;

      IF v_cnt > 0
      THEN
         o_ret := -2;
         RETURN;
      END IF;

--check if any budget linked to the study protocol calendar
      SELECT COUNT (*)
        INTO v_cnt
        FROM SCH_BGTCAL
       WHERE bgtcal_protid = p_cal AND NVL (bgtcal_delflag, 'Z') <> 'Y';

      IF v_cnt > 0
      THEN
         o_ret := -3;
         RETURN;
      END IF;

      --check if any milestone linked to the study protocol calendar
      SELECT COUNT (*)
        INTO v_cnt
        FROM ER_MILESTONE
       WHERE fk_cal = p_cal AND NVL (milestone_delflag, 'Z') <> 'Y';

      IF v_cnt > 0
      THEN
         o_ret := -4;
         RETURN;
      END IF;

      --select all the events in the calendar
      FOR i IN (SELECT event_id
                  FROM EVENT_ASSOC
                 WHERE chain_id = p_cal AND event_type = 'A')
      LOOP
         BEGIN
            -- delete event cost
            DELETE FROM SCH_EVENTCOST
                  WHERE fk_event = i.event_id;

            -- delete event crf
            DELETE FROM SCH_CRFLIB
                  WHERE fk_events = i.event_id;

            -- delete event user
            DELETE FROM SCH_EVENTUSR
                  WHERE fk_event = i.event_id;

            -- KM -#3491
            -- delete event doc - doc data
            DELETE FROM SCH_DOCS
                  WHERE pk_docs IN (SELECT pk_docs
                                      FROM SCH_EVENTDOC
                                     WHERE fk_event = i.event_id);

            -- delete event doc - doc master data
            DELETE FROM SCH_EVENTDOC
                  WHERE fk_event = i.event_id;

            -- delete event messages
            DELETE FROM SCH_DISPATCHMSG
                  WHERE fk_event = i.event_id;

            -- delete event
            DELETE FROM EVENT_ASSOC
                  WHERE chain_id = i.event_id;
         END;
      END LOOP;

      -- delete all events of the calendar
      DELETE FROM EVENT_ASSOC
            WHERE chain_id = p_cal AND event_type = 'A';

      -- delete calendar status records
      DELETE FROM SCH_PROTSTAT
            WHERE fk_event = p_cal;

      -- finally delete the calendar
      DELETE FROM EVENT_ASSOC
            WHERE event_id = p_cal AND chain_id = p_study AND event_type = 'P';

      COMMIT;
      o_ret := 0;
   END;

-------------------------------------------------------
   PROCEDURE sp_get_schedule_page_records (
      p_page                  NUMBER,
      p_recs_per_page         NUMBER,
      p_schsql                VARCHAR2,
      p_countsql              VARCHAR2,
      p_schwhere              VARCHAR2,
      o_schres          OUT   Gk_Cv_Types.genericcursortype,
      o_schrows         OUT   NUMBER,
      o_crfres          OUT   Gk_Cv_Types.genericcursortype
   )
   AS
      firstrec          NUMBER;
      lastrec           NUMBER;
      v_crf_sql         VARCHAR2 (4000);
      v_crf_names_sql   VARCHAR2 (4000);
   BEGIN
      -- get resultset for schedule
      Sp_Get_Sch_Page_Records (p_page,
                               p_recs_per_page,
                               p_schsql,
                               p_countsql,
                               o_schres,
                               o_schrows
                              );
      plog.fatal(pctx,'schedule sql' || p_schsql );
      --Prepare SQL for CRF result set
      SELECT (p_page - 1) * p_recs_per_page
        INTO firstrec
        FROM DUAL;

      SELECT p_page * p_recs_per_page + 1
        INTO lastrec
        FROM DUAL;

        -- SV, 10/30, added alias ev for sch_events1 where p_schwhere is used as coming from front end, this where condition now has ev embedded in it.
        -- if you are changing this there, make sure to change here as well.

		--by sonia abrol, changed order by  - included  CRF_NAME

      v_crf_sql :=
            ' Select null as disp_in_spec, b.event_id event_id,  crf_name ,lower(crf_name) crf_name_lower,  b.visit visit,0 fk_form,''MCRF'' crftype, b.fk_visit, NULL lf_entrychar,'''' response_count ,0 form_stat ,'''' form_record_type
                   FROM
   			 	  ( SELECT DISTINCT visit,fk_patprot , fk_visit FROM ( SELECT event_id,visit,fk_patprot, ROWNUM rnum, fk_visit
                                FROM (SELECT event_id,visit,fk_patprot , fk_visit FROM SCH_EVENTS1  ev'
         || p_schwhere
         || ' ) a
						  WHERE ROWNUM < '
         || lastrec
         || ' )
			 WHERE rnum > '
         || firstrec
         || ') m, SCH_CRF,SCH_EVENTS1 b
			  WHERE m.fk_patprot = b.fk_patprot AND m.fk_visit = b.fk_visit AND b.event_id = FK_EVENTS1
			  AND fk_lf IS NULL
			  UNION --for event CRF forms
			  SELECT (SELECT nvl(lf_display_inspec,0) FROM er_linkedforms ff WHERE ff.fk_formlib = fk_form ) disp_in_spec, b.event_id event_id,  DECODE(fk_form,0,other_links,(SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_form))  AS crf_name,
			  LOWER(DECODE(fk_form,0,other_links,(SELECT form_name FROM ER_FORMLIB WHERE pk_formlib = fk_form)) ) AS crf_name_lower,
			   b.visit visit, fk_form,form_type crf_type, b.fk_visit, DECODE(fk_form,0,NULL,(SELECT lf_entrychar FROM ER_LINKEDFORMS WHERE fk_formlib = fk_form)) lf_entrychar,
			   DECODE(fk_form,0,'''',Pkg_Gensch.f_crfforms_resp_stat(fk_form,m.fk_patprot,b.event_id)) response_count  , (SELECT form_status FROM ER_FORMLIB WHERE pk_formlib = fk_form) form_stat ,
			   (SELECT record_type FROM ER_linkedforms WHERE fk_formlib = fk_form) form_record_type
                   FROM
   			 	  ( SELECT DISTINCT visit,fk_patprot , fk_visit FROM ( SELECT event_id,visit,fk_patprot, ROWNUM rnum, fk_visit
                                FROM (SELECT event_id,visit,fk_patprot, fk_visit FROM SCH_EVENTS1  ev'
         || p_schwhere
         || ' ) a
						  WHERE ROWNUM < '
         || lastrec
         || ' )
			 WHERE rnum > '
         || firstrec
         || ') m, esch.SCH_EVENT_CRF crf,SCH_EVENTS1 b
			  WHERE m.fk_patprot = b.fk_patprot AND m.fk_visit = b.fk_visit AND b.fk_assoc = crf.FK_EVENT
 			  ORDER BY visit, event_id,crf_name_lower ';

			   plog.fatal(pCtx,'v_crf_sql ' || v_crf_sql );

      OPEN o_crfres
       FOR v_crf_sql;
   END;

   ---------------------------------------------------------

   /* Created by Sonia Abrol, 08/24/06, return a comma separated string for patient from counts for each status the forms were answered*/
     FUNCTION f_crfforms_resp_stat(p_from NUMBER ,p_patprot NUMBER, p_event_id NUMBER) RETURN VARCHAR2
	 IS
	   v_respString  VARCHAR2(10000)  := '';
	   v_desc VARCHAR2(500);
	   v_ct NUMBER;
	 BEGIN
	 	  FOR i IN ( SELECT form_completed, codelst_desc, COUNT(*) AS ct
			  FROM ER_PATFORMS , ER_CODELST
			  WHERE fk_formlib = p_from AND pk_codelst = form_completed AND fk_patprot = p_patprot AND
			  fk_sch_events1 = p_event_id AND NVL(record_type , 'N') <> 'D'
			  GROUP BY form_completed, codelst_desc)
			  LOOP
					v_desc := i.codelst_desc;
					v_ct := i.ct;
			  	    v_respString := v_respString || ',' || v_desc || '(' || v_ct || ')';
			  END LOOP;

			   IF (LENGTH(v_respString)) > 1 THEN
			   	  v_respString := SUBSTR(v_respString,2);
			   END  IF;

			   RETURN v_respString ;

	 END;
---------------------------------------------------------


 FUNCTION f_get_event_roles(event_id NUMBER)
RETURN VARCHAR2 AS
v_retval VARCHAR2(32000);
--v_sql varchar2(1000);
--v_cur Gk_Cv_Types.GenericCursorType;

BEGIN

  --v_sql := 'SELECT codelst_desc  FROM sch_eventusr , er_codelst@lnk2eres where fk_event= ' || event_id || ' and
  --eventusr_type = ''R'' and pk_codelst = eventusr and  codelst_type = ''role'' order by codelst_desc';

  --plog.fatal('sql' || v_sql);

  --OPEN v_cur FOR v_sql;

  --select Pkg_Util.f_join(OPEN v_cur FOR v_sql ,',') into v_retval from dual;
  --execute immediate 'select Pkg_Util.f_join(cursor(:1),'','') from dual' into v_retval using v_sql;

  FOR i IN ( SELECT codelst_desc  FROM SCH_EVENTUSR , er_codelst WHERE fk_event= event_id  AND
   eventusr_type = 'R' AND pk_codelst = eventusr AND  codelst_type = 'role'  order by 1)
   LOOP
            IF v_retval IS NOT NULL THEN
            v_retval := v_retval || ',';
         END IF;

         v_retval := v_retval || i.codelst_desc;

   END LOOP;


  RETURN v_retval;

END;

   PROCEDURE sp_transfer_crf_to_schedule (
      p_crf             NUMBER,
      p_event           NUMBER,
      p_crflib_number   VARCHAR2,
      p_crflib_name     VARCHAR2,
      p_creator         NUMBER,
      p_ipadd           VARCHAR2,
      p_formflag        NUMBER
   )
   AS
      /*
      Author: Sonia Sahni
      Purpose : Transfer newly added CRF to existing patient schedules
      Date : 01/13/2004
      */
      v_pk_lf       NUMBER;
      v_entrychar   CHAR (1);
      v_crf_stat    NUMBER;
      v_pk_crf      NUMBER;
   BEGIN
      IF p_formflag = 1
      THEN
         BEGIN
            --get CRF linked form details (this sql will get data only if it has a form asscoiated)
            SELECT pk_lf, lf_entrychar
              INTO v_pk_lf, v_entrychar
              FROM ER_LINKEDFORMS
             WHERE fk_crf = p_crf;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_entrychar := '';
         END;
      END IF;

      -- Get CRF status ID for 'not done'
      SELECT pk_codelst
        INTO v_crf_stat
        FROM SCH_CODELST
       WHERE codelst_type = 'crfstatus' AND codelst_subtyp = 'notdone';

      -- INSERT CRF RECORD FOR EXISTING PATIENT SCHEDULES
      FOR i IN (SELECT event_id, fk_patprot
                  FROM SCH_EVENTS1
                 WHERE fk_assoc = p_event)
      LOOP
         SELECT seq_sch_crf.NEXTVAL
           INTO v_pk_crf
           FROM DUAL;

         INSERT INTO SCH_CRF
                     (pk_crf, fk_events1, crf_number, crf_name,
                      fk_patprot, crf_flag, crf_delflag, creator, created_on,
                      ip_add, fk_lf, lf_entrychar, crf_origlibid
                     )
              VALUES (v_pk_crf, i.event_id, p_crflib_number, p_crflib_name,
                      i.fk_patprot, 'E', 'N', p_creator, SYSDATE,
                      p_ipadd, v_pk_lf, v_entrychar, p_crf
                     );

         INSERT INTO SCH_CRFSTAT
                     (pk_crfstat, fk_crf, fk_codelst_crfstat,
                      crfstat_sentflag, crfstat_enterby, creator, created_on,
                      ip_add
                     )
              VALUES (seq_sch_crfstat.NEXTVAL, v_pk_crf, v_crf_stat,
                      0, p_creator, p_creator, SYSDATE,
                      p_ipadd
                     );
      END LOOP;
   END;


--------------------------------------------------------------------

   --Modified by Manimaran on 7,july06 to stop sending emails after the schedule is dicontinued
   PROCEDURE  sp_update_msgstatus_discdt(
   studyId     NUMBER,
   patProtPK   NUMBER
   )
   AS

   BEGIN
       UPDATE SCH_MSGTRAN
            SET msg_status= 2
          WHERE fk_study=studyId AND fk_patprot=patProtPk;

  END;

  /* Created by Sonia Abrol, 08/25/06, checks for all CRfs linked with a Patient schedule PK's schedule record
	 If all CRFs are filled atleast once and if  the event
	 is still marked 'not done', it will return 1.
	 If the event already has a status return 0
	 If there are events is still not done'  but there are no linked CRfs, return 0
	 If all CRFs are not marked done, it will return 0
	 */

	 FUNCTION f_getCRFEventMarkDoneFlag(p_fk_events1 NUMBER) RETURN NUMBER
	 IS

	   v_fk_visit NUMBER;
	   v_patprot NUMBER;

	   v_event_crfs NUMBER := 0;
  	   v_event_answered_crfs NUMBER := 0;
	   v_event_exe_on DATE;
	   v_fk_assoc NUMBER;
     v_eve_stat NUMBER;


	 BEGIN
	 	  -- check if  the event already has a status

		  --get fk_visit


		  BEGIN


		  SELECT fk_visit, fk_patprot , event_exeon,fk_assoc
		  INTO v_fk_visit, v_patprot , v_event_exe_on,v_fk_assoc
		  FROM SCH_EVENTS1 WHERE event_id = LPAD(p_fk_events1,10,'0');

		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  			RETURN 0;
		  END;

     --KM-17Nov09-#3867
      SELECT pk_codelst
        INTO v_eve_stat
        FROM SCH_CODELST
       WHERE codelst_type = 'eventstatus' AND codelst_subtyp = 'ev_done';

      IF v_event_exe_on IS NOT NULL  THEN -- the event already has a status
        for m in (select eventstat from sch_eventstat where fk_event = LPAD(p_fk_events1,10,'0') )
           loop
              if(m.eventstat = v_eve_stat) then
              	  RETURN 0;
              end if;
            end loop;
		  END IF;

		  -- see if there are CRf linked with the event

		  BEGIN
			  SELECT COUNT(DISTINCT fk_form)
			  INTO v_event_crfs
			  FROM SCH_EVENT_CRF
			  WHERE fk_event = v_fk_assoc AND fk_form > 0;

			  IF v_event_crfs = 0 THEN -- NO CRF Linked
			  	 RETURN 0;
			  END IF;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  			RETURN 0;
		  END;

		  -- get the count of all distinct CRF forms answered for this event

		  BEGIN
			  SELECT COUNT(DISTINCT fk_formlib)
			  INTO v_event_answered_crfs
			  FROM ER_PATFORMS
			  WHERE fk_patprot = v_patprot AND  fk_sch_events1 = p_fk_events1 AND NVL(record_type,'N') <> 'D';

        IF  v_event_answered_crfs = v_event_crfs  THEN

			  	  RETURN 1;

				ELSE

				RETURN 0;

			END IF;

		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  			RETURN 0;
	  END;

	 END;

	 /* Procedure to refresh notifications for existing schedules
	 Sonia Abrol, 10/30/06*/
   PROCEDURE sp_refresh_cal_messages (
      p_calendar IN NUMBER,
	  p_study IN NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2 ,
	  o_ret OUT VARCHAR2

   )
  AS
  v_cal_assoc VARCHAR(2);
BEGIN

	 -- get the calendar type

	 SELECT event_calassocto
	 INTO v_cal_assoc
	 FROM EVENT_ASSOC
	 WHERE event_id = p_calendar AND chain_id = p_study;



	BEGIN


	 IF v_cal_assoc = 'P' THEN --patient schedule

	  -- run through loop for all enrolled patients



	 FOR i  IN (SELECT fk_per, pk_patprot FROM ER_PATPROT WHERE fk_study = p_study AND fk_protocol = p_calendar AND patprot_stat = 1)
		  LOOP
		  	  		--update deactivate the current messages

					  sp_chgmsgstat_tran (i.pk_patprot,1,'A',   p_ipadd  , p_dmlby);

					  -- add new messages
            sp_addmsg_to_tran (  i.fk_per,p_study,    p_dmlby,      p_ipadd, i.pk_patprot	   );


   END LOOP;

   ELSIF  v_cal_assoc = 'S' THEN --patient schedule

	  sp_admin_msg_to_tran (p_calendar  ,   p_study ,   p_dmlby,    p_ipadd );


   END IF;

   	  COMMIT;

	  EXCEPTION WHEN OTHERS THEN
	  			o_ret := SQLERRM;
	  END;
END;

/*Procedure to create Admin schedule emails */

   PROCEDURE sp_admin_msg_to_tran (
      p_protocol  IN   NUMBER,
      p_study       IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2
   )
   AS
/*
** By Sonia Abrol Date - 12z13z06
** Modification History
**
** Modified By         Date     Remarks


*/
      v_msgtemplate   VARCHAR2 (4000);

   BEGIN
      v_msgtemplate := Pkg_Common.sch_getmailmsg ('c_evsch');

	   --deactivate current messages

	     UPDATE  SCH_MSGTRAN SET MSG_STATUS = -2
     WHERE fk_protocol = p_protocol AND fk_study = p_study AND NVL(fk_patprot,0)  = 0 AND RTRIM(MSG_TYPE) IN ('ev_usch','ev_psch')
	 AND MSG_STATUS =  0;

      --KM- #4267
      INSERT INTO SCH_MSGTRAN     (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event, fk_protocol)
         SELECT seq_msgtran.NEXTVAL, actual_schdate - usr_daysbefore, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500) || '...'
                                        || '~'
                                        || 'N/A'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || usr_msgbefore
                                       ),
                'ev_usch', a.event_id, c.eventusr, p_study, SYSDATE,
                0, a.fk_assoc,p_protocol
           FROM SCH_EVENTS1 a,
                EVENT_ASSOC b,
                SCH_EVENTUSR c
               WHERE a.fk_study = p_study
                        AND session_id=LPAD(TO_CHAR(p_protocol),10,'0') AND fk_patprot = 0   AND a.status = 0
            AND a.fk_assoc = b.event_id
            AND usr_msgbefore IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S'
			AND TRUNC(actual_schdate - usr_daysbefore) >= TRUNC(SYSDATE);

      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event,fk_protocol)
         SELECT seq_msgtran.NEXTVAL, actual_schdate + usr_daysafter, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                            '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500) || '...'
                                        || '~'
                                        || 'N/A'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || usr_msgafter
                                       ),
                'ev_usch', a.event_id, c.eventusr, p_study, SYSDATE,
                0, a.fk_assoc,p_protocol
           FROM SCH_EVENTS1 a,
                EVENT_ASSOC b,
                SCH_EVENTUSR c
              WHERE a.fk_study = p_study
                        AND session_id=LPAD(TO_CHAR(p_protocol),10,'0') AND fk_patprot = 0 AND a.status = 0
            AND a.fk_assoc = b.event_id
            AND usr_msgafter IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S'
			AND TRUNC(actual_schdate + usr_daysafter) >= TRUNC(SYSDATE);

      COMMIT;
   END sp_admin_msg_to_tran;


--JM: 20Jun2008:--------------------------------------------------------------------------

   PROCEDURE sp_addmsg_to_tran_per_event (
      p_eventid IN NUMBER,
      p_patientid   IN   NUMBER,
      p_study       IN   NUMBER,
      p_dmlby       IN   NUMBER,
      p_ipadd       IN   VARCHAR2,
      p_patprot     IN   NUMBER
   )
   AS
/*
*
*/
      v_msgtemplate   VARCHAR2 (4000);
      v_patcode       VARCHAR2 (25);
   BEGIN
      v_msgtemplate := Pkg_Common.sch_getmailmsg ('c_evsch');

      SELECT Pkg_Common.sch_getpatcode (p_patprot)
        INTO v_patcode
        FROM DUAL;                                     -- v_patcode is param 1
      --KM-#4267
      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate - usr_daysbefore, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500) || '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || usr_msgbefore
                                       ),
                'ev_usch', a.event_id, c.eventusr, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a,
                EVENT_ASSOC b,
                SCH_EVENTUSR c,
                ER_STUDY d,
                ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND usr_msgbefore IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S'
			AND TRUNC(actual_schdate - usr_daysbefore) >= TRUNC(SYSDATE)
                        AND a.fk_assoc = p_eventid;
      --KM-#4267
      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate + usr_daysafter, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                            '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500) || '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || usr_msgafter
                                       ),
                'ev_usch', a.event_id, c.eventusr, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a,
                EVENT_ASSOC b,
                SCH_EVENTUSR c,
                ER_STUDY d,
                ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND usr_msgafter IS NOT NULL
            AND c.fk_event = b.event_id
            AND c.eventusr_type = 'S'
		AND TRUNC( actual_schdate + usr_daysafter) >= TRUNC(SYSDATE)
                AND a.fk_assoc = p_eventid;

      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate - pat_daysbefore, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                            '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500) || '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        ||  '[*ACTSCHDATE*]'
                                        || '~'
                                        || pat_msgbefore
                                       ),
                'ev_psch', a.event_id, p_patientid, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b, ER_STUDY d, ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND pat_msgbefore IS NOT NULL
		AND TRUNC( actual_schdate - pat_daysbefore) >= TRUNC(SYSDATE)
                AND a.fk_assoc = p_eventid;

      INSERT INTO SCH_MSGTRAN
                  (pk_msg, msg_sendon, msg_status, msg_text, msg_type,
                   fk_schevent, fk_user, fk_study, created_on, fk_patprot,
                   fk_event)
         SELECT seq_msgtran.NEXTVAL, actual_schdate + pat_daysafter, 0,
                Pkg_Common.sch_getmail (v_msgtemplate,
                                           '[*VELSTUDYNUM*]'
                                        || '~'
                                        || substr(a.description,1,500) || '...'
                                        || '~'
                                        || '[*VELPATCODE*]'
                                        || '~'
                                        || '[*ACTSCHDATE*]'
                                        || '~'
                                        || pat_msgafter
                                       ),
                'ev_psch', a.event_id, p_patientid, p_study, SYSDATE,
                p_patprot, a.fk_assoc
           FROM SCH_EVENTS1 a, EVENT_ASSOC b, ER_STUDY d, ER_PATPROT e
          WHERE fk_patprot = p_patprot AND a.status = 0
            AND e.pk_patprot = p_patprot
            AND d.pk_study = e.fk_study
            AND a.fk_assoc = b.event_id
            AND pat_msgafter IS NOT NULL
			AND TRUNC( actual_schdate + pat_daysafter) >= TRUNC(SYSDATE)
                        AND a.fk_assoc = p_eventid;

      COMMIT;







   END sp_addmsg_to_tran_per_event;


PROCEDURE sp_setmailstat_per_event (
      p_eventid IN NUMBER,
      p_patprot      IN   NUMBER,
      p_modifiedby   IN   NUMBER,
      p_ip           IN   VARCHAR
   )
   AS

      v_patalcode     NUMBER (10);
      v_useralcode    NUMBER (10);
      v_usersetting   NUMBER (1);
      v_patsetting    NUMBER (1);
   BEGIN
      SELECT pk_codelst
        INTO v_patalcode
        FROM SCH_CODELST
       WHERE codelst_subtyp = 'n_patnot';

      SELECT pk_codelst
        INTO v_useralcode
        FROM SCH_CODELST
       WHERE codelst_subtyp = 'n_usernot';

      SELECT alnot_flag
        INTO v_usersetting
        FROM SCH_ALERTNOTIFY
       WHERE fk_codelst_an = v_useralcode AND fk_patprot = p_patprot;

      SELECT alnot_flag
        INTO v_patsetting
        FROM SCH_ALERTNOTIFY
       WHERE fk_codelst_an = v_patalcode AND fk_patprot = p_patprot;

      IF v_usersetting = 1 AND v_patsetting = 1
      THEN
         --SP_CHGMSGSTAT(p_patprot,1,'A',p_ip,p_modifiedby);
         sp_chgmsgstat_tran_event (p_eventid , p_patprot, 1, 'A', p_ip, p_modifiedby);
      END IF;

      IF v_usersetting = 1
      THEN
         --SP_CHGMSGSTAT(p_patprot,1,'U',p_ip,p_modifiedby);
         sp_chgmsgstat_tran_event (p_eventid , p_patprot, 1, 'U', p_ip, p_modifiedby);
      END IF;

      IF v_patsetting = 1
      THEN
         --SP_CHGMSGSTAT(p_patprot,1,'P',p_ip,p_modifiedby);
         sp_chgmsgstat_tran_event (p_eventid , p_patprot, 1, 'P', p_ip, p_modifiedby);
      END IF;

      COMMIT;


   END sp_setmailstat_per_event;


-----------------------------------------------------------------------
   PROCEDURE sp_chgmsgstat_tran_event (
      p_eventid IN NUMBER,
      p_fk_patprot   IN   NUMBER,
      p_flag         IN   NUMBER,                                         /* */
      p_msgtype      IN   VARCHAR,
                      /*values will passed will be 'P' for mails for patients,
                     'U' for mails for users, 'A' for all messages*/
      p_ip           IN   VARCHAR,
      p_modby        IN   NUMBER
   )

   AS
      v_msgsql           VARCHAR2 (2000);
      v_msgstat          NUMBER;
      v_curmsgstat       NUMBER;
      v_msgtype_clause   VARCHAR2 (100);
   BEGIN
      /*set msg status according to the p_flag*/
      IF p_flag = 1
      THEN                 /* 0 will be passed to deactivate the disp mails*/
         v_msgstat := -2;
                      /* -2 will be set for temporary deactivation of mails*/
         v_curmsgstat := 0;
              /* only messages that are not sent as yet will be deactivated*/
      ELSIF p_flag = 0
      THEN                    /* 0 will be passed to activate the disp mails*/
         v_msgstat := 0;          /* 0  will be set for activation of mails*/
         v_curmsgstat := -2;
                   /* only messages that were deactivated will be activated*/
      END IF;

      IF p_msgtype = 'A'
      THEN
         v_msgtype_clause :=
                          ' and RTRIM(MSG_TYPE) in (''ev_usch'',''ev_psch'')';
      ELSIF p_msgtype = 'P'
      THEN
         v_msgtype_clause := ' and RTRIM(MSG_TYPE) = ''ev_psch''';
      ELSIF p_msgtype = 'U'
      THEN
         v_msgtype_clause := ' and RTRIM(MSG_TYPE) = ''ev_usch''';
      END IF;

      v_msgsql :=
            'update  sch_msgtran set MSG_STATUS = '
         || v_msgstat
         || ' where fk_patprot = '
         || p_fk_patprot
         || ' and fk_event = '
         || p_eventid
         || ' and MSG_STATUS = '
         || v_curmsgstat
         || ' and fk_study is not null '
         || v_msgtype_clause;

COMMIT;


/*insert into tmp (c3) values (p_oldactdt ||' * ' ||p_newactdt ||' * ' || to_char(p_event_id) ||' * ' || to_char(p_fk_patprot) ||' * ' || to_char(p_flag) ||' * ' ||p_ip ||' * ' || to_char(p_modby) ||' * '|| to_char(v_days) ||' * ' || to_char(v_chdt) || ' * ' || to_char(v_newdt));*/
      EXECUTE IMMEDIATE v_msgsql;
--COMMIT;
   END sp_chgmsgstat_tran_event;

----------JM: 20JUN2008------------------------------------------------------------------------------



END Pkg_Gensch;
/


CREATE OR REPLACE SYNONYM ERES.PKG_GENSCH FOR PKG_GENSCH;


CREATE OR REPLACE SYNONYM EPAT.PKG_GENSCH FOR PKG_GENSCH;


GRANT EXECUTE, DEBUG ON PKG_GENSCH TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_GENSCH TO ERES;
