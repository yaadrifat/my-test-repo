CREATE OR REPLACE FUNCTION        "VALIDATENAME" (
   EVENTID     NUMBER,
   USERID      NUMBER,
   EVENTNAME   VARCHAR2,
   EVENTTYPE   VARCHAR2
)
   return varchar2
is
   EVENTCOUNT   NUMBER;
begin
   IF (EVENTID = -1) THEN
      IF ((EVENTTYPE = 'P')
          or (EVENTTYPE = 'E')
          or (EVENTTYPE = 'L')) THEN
         select count (EVENT_ID)
           into EVENTCOUNT
           from EVENT_DEF
          where USER_ID = USERID
            and EVENT_TYPE = EVENTTYPE
            and NAME = EVENTNAME;
         IF (EVENTCOUNT > 0) THEN
            return (-1);
         END IF;
      END IF;
   ELSE
      IF ((EVENTTYPE = 'P')
          or (EVENTTYPE = 'E')
          or (EVENTTYPE = 'L')) THEN
         select count (EVENT_ID)
           into EVENTCOUNT
           from EVENT_DEF
          where USER_ID = USERID
            and EVENT_TYPE = EVENTTYPE
            and NAME = EVENTNAME
            and EVENT_ID != EVENTID;
         IF (EVENTCOUNT > 0) THEN
            return (-1);
         END IF;
      END IF;
   END IF;
   return (0);
/**********************************
TEST
set serveroutput on
declare
status number ;
begin
status:=ValidateName(-1,510,'event22','E') ;
dbms_output.put_line(to_char(status) ) ;
end ;
**************************************/
end;
/


CREATE SYNONYM ERES.VALIDATENAME FOR VALIDATENAME;


CREATE SYNONYM EPAT.VALIDATENAME FOR VALIDATENAME;


GRANT EXECUTE, DEBUG ON VALIDATENAME TO EPAT;

GRANT EXECUTE, DEBUG ON VALIDATENAME TO ERES;

