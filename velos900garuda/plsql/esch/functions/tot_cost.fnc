CREATE OR REPLACE FUNCTION        "TOT_COST" (event number ) return number is
 totcost number ;
begin
 select sum(EVENTCOST_VALUE) event_cost
   into totcost
   from sch_eventcost
  where sch_eventcost.fk_event = event ;
 return totcost ;
end ;
/


CREATE SYNONYM ERES.TOT_COST FOR TOT_COST;


CREATE SYNONYM EPAT.TOT_COST FOR TOT_COST;


GRANT EXECUTE, DEBUG ON TOT_COST TO EPAT;

GRANT EXECUTE, DEBUG ON TOT_COST TO ERES;

