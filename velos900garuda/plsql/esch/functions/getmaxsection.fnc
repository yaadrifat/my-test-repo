create or replace
function GETMAXSECTION(bgtcal NUMBER) RETURN NUMBER IS 
maxsection NUMBER(38,0);
BEGIN
select max(BUDGETSECTION_SEQUENCE) into maxsection from ERV_BUDGET where BUDGETSEC_FKVISIT is null and PK_bgtcal = bgtcal;
  RETURN maxsection ;
END;