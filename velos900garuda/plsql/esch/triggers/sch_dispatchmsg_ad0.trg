CREATE OR REPLACE TRIGGER "SCH_DISPATCHMSG_AD0" 
AFTER DELETE
ON SCH_DISPATCHMSG
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_DISPATCHMSG', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_msg) || '|' ||
   to_char(:old.msg_sendon) || '|' ||
   to_char(:old.msg_status) || '|' ||
   :old.msg_type || '|' ||
   to_char(:old.fk_event) || '|' ||
   :old.msg_text || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.fk_pat) || '|' ||
   :old.fk_schevent || '|' ||
   to_char(:old.fk_study) || '|' ||
   to_char(:old.fk_patprot);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


