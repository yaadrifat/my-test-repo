CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTSTAT_AD0" 
AFTER DELETE
ON SCH_EVENTSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENTSTAT', :old.rid, 'D', :old.last_modified_by);

  deleted_data :=
   to_char(:old.pk_eventstat) || '|' ||
   to_char(:old.eventstat_dt) || '|' ||
   :old.eventstat_notes || '|' ||
   :old.fk_event || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.eventstat) || '|' ||
   to_char(:old.eventstat_enddt);

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


