CREATE OR REPLACE TRIGGER SCH_EVENTSTAT_AU0
AFTER UPDATE
ON SCH_EVENTSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTSTAT', :OLD.rid, 'U', usr );

   IF NVL(:OLD.pk_eventstat,0) !=
      NVL(:NEW.pk_eventstat,0) THEN
      audit_trail.column_update
        (raid, 'PK_EVENTSTAT',
        :OLD.pk_eventstat, :NEW.pk_eventstat);
   END IF;
   IF NVL(:OLD.eventstat_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.eventstat_dt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT_DT',
        to_char(:OLD.eventstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.eventstat_dt,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.eventstat_notes,' ') !=
      NVL(:NEW.eventstat_notes,' ') THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT_NOTES',
        :OLD.eventstat_notes, :NEW.eventstat_notes);
   END IF;
   IF NVL(:OLD.fk_event,' ') !=
      NVL(:NEW.fk_event,' ') THEN
      audit_trail.column_update
        (raid, 'FK_EVENT',
        :OLD.fk_event, :NEW.fk_event);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
   to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
   to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.eventstat,0) !=
      NVL(:NEW.eventstat,0) THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT',
        :OLD.eventstat, :NEW.eventstat);
   END IF;
   IF NVL(:OLD.eventstat_enddt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
      NVL(:NEW.eventstat_enddt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
      audit_trail.column_update
        (raid, 'EVENTSTAT_ENDDT',
        to_char(:OLD.eventstat_enddt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.eventstat_enddt,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;

END;
/


