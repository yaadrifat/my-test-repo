CREATE OR REPLACE TRIGGER "SCH_LINEITEM_AD0"
AFTER DELETE
ON SCH_LINEITEM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_LINEITEM', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_lineitem) || '|' ||
  TO_CHAR(:OLD.fk_bgtsection) || '|' ||
  :OLD.lineitem_desc || '|' ||
  :OLD.lineitem_sponsorunit || '|' ||
  :OLD.lineitem_clinicnofunit || '|' ||
  :OLD.lineitem_othercost || '|' ||
  :OLD.lineitem_delflag || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  :OLD.lineitem_name || '|' ||
  :OLD.lineitem_notes || '|' ||
  TO_CHAR(:OLD.lineitem_inpersec) || '|' ||
  TO_CHAR(:OLD.lineitem_stdcarecost) || '|' ||
  TO_CHAR(:OLD.lineitem_rescost) || '|' ||
  TO_CHAR(:OLD.lineitem_invcost) || '|' ||
  TO_CHAR(:OLD.lineitem_incostdisc) || '|' ||
  :OLD.lineitem_cptcode || '|' ||
  TO_CHAR(:OLD.lineitem_repeat) || '|' ||
  TO_CHAR(:OLD.lineitem_parentid) || '|' ||
  TO_CHAR(:OLD.lineitem_applyinfuture) || '|' ||
  TO_CHAR(:OLD.fk_codelst_category) || '|' ||
  :OLD.lineitem_tmid || '|' ||
  :OLD.lineitem_cdm || '|' ||
  :OLD.lineitem_applyindirects || '|' ||
  :OLD.lineitem_totalcost || '|' ||
  :OLD.lineitem_sponsoramount || '|' ||
  :OLD.lineitem_variance || '|' ||
  TO_CHAR(:OLD.FK_EVENT) ||'|'|| TO_CHAR(:OLD.lineitem_seq)
    ||'|'|| TO_CHAR(:OLD.fk_codelst_cost_type)
	||'|'|| TO_CHAR(:OLD.SUBCOST_ITEM_FLAG);

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid,dbms_lob.substr(deleted_data,4000,1));
END;
/


