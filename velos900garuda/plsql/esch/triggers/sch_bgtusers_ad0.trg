CREATE OR REPLACE TRIGGER "SCH_BGTUSERS_AD0" 
AFTER DELETE
ON SCH_BGTUSERS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'SCH_BGTUSERS', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_bgtusers) || '|' ||
  to_char(:old.fk_budget) || '|' ||
  to_char(:old.fk_user) || '|' ||
  :old.bgtusers_rights || '|' ||
  :old.bgtusers_type || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


