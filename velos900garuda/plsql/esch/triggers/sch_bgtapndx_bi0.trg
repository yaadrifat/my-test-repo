CREATE OR REPLACE TRIGGER "SCH_BGTAPNDX_BI0"
BEFORE INSERT
ON SCH_BGTAPNDX
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  insert_data CLOB;
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_BGTAPNDX', erid, 'I', :NEW.LAST_MODIFIED_BY );
    --   Added by Ganapathy on 06/23/05 for Audit insert
    insert_data:= :NEW.PK_BGTAPNDX||'|'|| :NEW.FK_BUDGET||'|'||:NEW.BGTAPNDX_DESC||'|'||
        :NEW.BGTAPNDX_URI||'|'||:NEW.BGTAPNDX_TYPE||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||
      :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
       TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


