CREATE OR REPLACE TRIGGER "SCH_EVENT_CRF_AD" 
AFTER DELETE
ON SCH_EVENT_CRF
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENT_CRF', :OLD.rid, 'D');

  deleted_data :=
   :OLD.pk_eventcrf || '|' ||
   :OLD.fk_event || '|' ||
   :OLD.fk_form || '|' ||
   :OLD.form_type || '|' ||
   :OLD.other_links || '|' ||
   :OLD.propagate_from || '|' ||
   TO_CHAR(:OLD.rid) || '|' ||
   TO_CHAR(:OLD.creator) || '|' ||
   TO_CHAR(:OLD.created_on) || '|' ||
   TO_CHAR(:OLD.last_modified_by) || '|' ||
   TO_CHAR(:OLD.last_modified_date) || '|' ||
   :OLD.ip_add;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END;
/


