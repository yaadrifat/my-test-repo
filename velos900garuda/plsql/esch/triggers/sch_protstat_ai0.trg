CREATE OR REPLACE TRIGGER "SCH_PROTSTAT_AI0" 
AFTER INSERT
ON SCH_PROTSTAT REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;

v_codelst_id number; --JM: 07FEB2011, #D-FIN9
/******************************************************************************
   NAME:       ESCH.SCH_PROTSTAT_AI0
   PURPOSE:    To Reset the mail notification

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2/23/2007             1. Created this trigger.

   NOTES:

******************************************************************************/
BEGIN
   tmpVar := 0;
   
   
   select PK_CODELST into v_codelst_id from sch_codelst where CODELST_TYPE='calStatStd' and CODELST_SUBTYP='D'; 
   
--IF NVL(:NEW.PROTSTAT,' ') ='D'  THEN
IF NVL(:NEW.FK_CODELST_CALSTAT,0) =v_codelst_id  THEN
   UPDATE SCH_MSGTRAN SET msg_status=-2 WHERE fk_protocol =:NEW.fk_event;
   END IF;

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/


