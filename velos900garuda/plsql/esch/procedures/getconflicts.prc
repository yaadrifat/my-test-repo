CREATE OR REPLACE PROCEDURE        "GETCONFLICTS" ( objectID char, StDate date, enDate date)
as
event_id  char(10);
description char(10);
start_date_time date;
end_date_time date;
begin
  select event_id,description,start_date_time,end_date_time into event_id,description, start_date_time ,end_date_time from sch_events1 where object_id=objectID and (not((end_date_time<=StDate) or(start_date_time>=enDate))) ;
end;
/


CREATE SYNONYM ERES.GETCONFLICTS FOR GETCONFLICTS;


CREATE SYNONYM EPAT.GETCONFLICTS FOR GETCONFLICTS;


GRANT EXECUTE, DEBUG ON GETCONFLICTS TO EPAT;

GRANT EXECUTE, DEBUG ON GETCONFLICTS TO ERES;

