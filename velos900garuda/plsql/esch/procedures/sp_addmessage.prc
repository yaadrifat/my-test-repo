CREATE OR REPLACE PROCEDURE        "SP_ADDMESSAGE" (
   P_EVENT       IN   NUMBER,
   P_PATIENTID   IN   NUMBER,
   P_DATE        IN   DATE,
   P_MSGTO       IN   CHAR,
   P_SCHEVENT    IN   CHAR,
   p_study       IN   NUMBER,
   P_DMLBY       IN   NUMBER,
   P_IPADD       IN   VARCHAR2
)
AS
/*
** Modification History
**
** Modified By         Date     Remarks
** Charanjiv       29th Sept 	Aded the Study Id in the sch_dispatchmsg table
** Charanjiv       06th Nov 	Added the defulat column for creator, last_modified date/by and
**                            created on in the insert modules.
*/

   USER_ID          NUMBER;
   USR_EMAIL        VARCHAR2 (50);
   USR_EMAILFINAL   VARCHAR2 (1000);
   PAT_EMAIL        VARCHAR2 (50);
   EVENT_ID         NUMBER;
   PAT_DAYSBEFORE   NUMBER;
   PAT_DAYSAFTER    NUMBER;
   USR_DAYSBEFORE   NUMBER;
   USR_DAYSAFTER    NUMBER;
   PAT_MSGBEFORE    VARCHAR2 (4000);
   PAT_MSGAFTER     VARCHAR2 (4000);
   USR_MSGBEFORE    VARCHAR2 (4000);
   USR_MSGAFTER     VARCHAR2 (4000);


BEGIN
   SELECT PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER,
          PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER
     INTO PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER,
          PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER
     FROM EVENT_ASSOC
    WHERE EVENT_ID = P_EVENT;

    IF USR_MSGBEFORE IS NOT NULL THEN
      INSERT INTO SCH_DISPATCHMSG
                  (
                                 PK_MSG,
                                 MSG_SENDON,
                                 MSG_STATUS,
                                 FK_EVENT,
                                 MSG_TEXT,
                                 MSG_TYPE,
                                 FK_SCHEVENT,
                                 FK_PAT,
                                 FK_STUDY,
						   CREATOR                ,
                                 LAST_MODIFIED_BY       ,
                                 LAST_MODIFIED_DATE ,
                                 CREATED_ON             ,
                                 IP_ADD
                  )
           VALUES(
              SCH_DISPATCHMSG_SEQ1.NEXTVAL,
              P_DATE - USR_DAYSBEFORE,
              0,
              P_EVENT,
              USR_MSGBEFORE,
              'U',
              P_SCHEVENT,
              P_PATIENTID,
              p_study,
		    P_DMLBY,
		    P_DMLBY,
		    SYSDATE ,
		    SYSDATE ,
		    P_IPADD
           );
   END IF;


   IF USR_MSGAFTER IS NOT NULL THEN
      INSERT INTO SCH_DISPATCHMSG
                  (
                                 PK_MSG,
                                 MSG_SENDON,
                                 MSG_STATUS,
                                 FK_EVENT,
                                 MSG_TEXT,
                                 MSG_TYPE,
                                 FK_SCHEVENT,
                                 FK_PAT,
                                 FK_STUDY ,
   						   CREATOR                ,
                                 LAST_MODIFIED_BY       ,
                                 LAST_MODIFIED_DATE ,
                                 CREATED_ON             ,
                                 IP_ADD

                  )
           VALUES(
              SCH_DISPATCHMSG_SEQ1.NEXTVAL,
              P_DATE + USR_DAYSAFTER,
              0,
              P_EVENT,
              USR_MSGAFTER,
              'U',
              P_SCHEVENT,
              P_PATIENTID,
              p_study ,
    		    P_DMLBY,
		    P_DMLBY,
		    SYSDATE ,
		    SYSDATE ,
		    P_IPADD
           );
   END IF;
/*END IF;*/

/*IF(p_msgto = 'P' OR  p_msgto = 'B') THEN
 select PERSON_EMAIL
   into pat_email
   from person
  where pk_person = p_patientid;*/
   IF PAT_MSGBEFORE IS NOT NULL THEN
      INSERT INTO SCH_DISPATCHMSG
                  (
                                 PK_MSG,
                                 MSG_SENDON,
                                 MSG_STATUS,
                                 FK_EVENT,
                                 MSG_TEXT,
                                 MSG_TYPE,
                                 FK_PAT,
                                 FK_SCHEVENT,
                                 FK_STUDY ,
    						   CREATOR                ,
                                 LAST_MODIFIED_BY       ,
                                 LAST_MODIFIED_DATE ,
                                 CREATED_ON             ,
                                 IP_ADD

                  )
           VALUES(
              SCH_DISPATCHMSG_SEQ1.NEXTVAL,
              P_DATE - PAT_DAYSBEFORE,
              0,
              P_EVENT,
              PAT_MSGBEFORE,
              'P',
              P_PATIENTID,
              P_SCHEVENT ,
              p_study ,
    		    P_DMLBY,
		    P_DMLBY,
		    SYSDATE ,
		    SYSDATE ,
		    P_IPADD
           );
   END IF;


   IF PAT_MSGAFTER IS NOT NULL THEN
      INSERT INTO SCH_DISPATCHMSG
                  (
                                 PK_MSG,
                                 MSG_SENDON,
                                 MSG_STATUS,
                                 FK_EVENT,
                                 MSG_TEXT,
                                 MSG_TYPE,
                                 FK_PAT,
                                 FK_SCHEVENT ,
                                 FK_STUDY ,
    						   CREATOR                ,
                                 LAST_MODIFIED_BY       ,
                                 LAST_MODIFIED_DATE ,
                                 CREATED_ON             ,
                                 IP_ADD
                  )
           VALUES(
              SCH_DISPATCHMSG_SEQ1.NEXTVAL,
              P_DATE + PAT_DAYSAFTER,
              0,
              P_EVENT,
              PAT_MSGAFTER,
              'P',
              P_PATIENTID,
              P_SCHEVENT,
              p_study ,
    		    P_DMLBY,
		    P_DMLBY,
		    SYSDATE ,
		    SYSDATE ,
		    P_IPADD
           );
   END IF;
/*END IF;*/


END;
/


CREATE SYNONYM ERES.SP_ADDMESSAGE FOR SP_ADDMESSAGE;


CREATE SYNONYM EPAT.SP_ADDMESSAGE FOR SP_ADDMESSAGE;


GRANT EXECUTE, DEBUG ON SP_ADDMESSAGE TO EPAT;

GRANT EXECUTE, DEBUG ON SP_ADDMESSAGE TO ERES;

