CREATE OR REPLACE PROCEDURE        "SP_COPYPROTCRF" (
   P_PATPROTID    IN  NUMBER,
   P_DMLBY       IN  NUMBER,
   P_IPADD        IN  VARCHAR2
)
AS

   /** Author: Dinesh Khurana 7th Feb 2002
   **  This procedure copies(from sch_crflib) the crf details of all the events of a protocol when schedule is generated.
   ** the procedure takes patprot id as and argument
   ** Modification History
   ** Modified By  Sonika Talwar on Jan 09, 03 to store the id of sch_crflib whose data is being copied.
   */


   EVENTS1_ID     CHAR (10);
   EVENTASSOC_ID  NUMBER ;
   CRF_ID		  NUMBER;
   CRF_STAT       NUMBER ;


   CURSOR C1
   IS
      SELECT EVENT_ID,FK_ASSOC
        FROM SCH_EVENTS1
       WHERE FK_PATPROT = P_PATPROTID;

	BEGIN
	SELECT PK_CODELST INTO CRF_STAT FROM SCH_CODELST WHERE CODELST_TYPE='crfstatus' AND CODELST_SUBTYP='notdone';

    OPEN C1;

   LOOP

      FETCH C1 INTO EVENTS1_ID, EVENTASSOC_ID ;
      EXIT WHEN C1%NOTFOUND;



	  INSERT INTO SCH_CRF
               ( PK_CRF,
				 FK_EVENTS1,
                 CRF_NUMBER,
                 CRF_NAME,
                 FK_PATPROT,
                 CRF_FLAG,
                 CRF_DELFLAG,
                 CREATOR,
                 CREATED_ON,
                 IP_ADD,
			  	 FK_LF,
				 LF_ENTRYCHAR,
				 CRF_ORIGLIBID
               )

	           SELECT SEQ_SCH_CRF.NEXTVAL,
			   		  EVENTS1_ID,
					  CRFLIB_NUMBER,
					  CRFLIB_NAME,
					  P_PATPROTID,'E','N',P_DMLBY,SYSDATE,P_IPADD,PK_LF , LF_ENTRYCHAR, PK_CRFLIB
	             FROM SCH_CRFLIB,ER_LINKEDFORMS
	            WHERE FK_EVENTS = EVENTASSOC_ID AND
					  FK_CRF (+)= PK_CRFLIB  ;
					  -- for associating CRF forms

      INSERT INTO SCH_CRFSTAT
               ( PK_CRFSTAT,
				 FK_CRF,
                 FK_CODELST_CRFSTAT,
                 CRFSTAT_SENTFLAG,
                 CRFSTAT_ENTERBY,
                 CREATOR,
                 CREATED_ON,
                 IP_ADD
               )
           SELECT SEQ_SCH_CRFSTAT.NEXTVAL,PK_CRF,CRF_STAT,0,P_DMLBY,P_DMLBY,SYSDATE,P_IPADD
           FROM SCH_CRF WHERE FK_EVENTS1 = EVENTS1_ID ;


   END LOOP;

   CLOSE C1;
/**********************************
TEST
set serveroutput on
declare
stat number;
begin

SP_COPYPROTCRF(873,1111,'172.16.0.158');
dbms_output.put_line(to_char(stat) ) ;
end ;
**************************************/

 --  COMMIT;
END;
/


CREATE SYNONYM ERES.SP_COPYPROTCRF FOR SP_COPYPROTCRF;


CREATE SYNONYM EPAT.SP_COPYPROTCRF FOR SP_COPYPROTCRF;


GRANT EXECUTE, DEBUG ON SP_COPYPROTCRF TO EPAT;

GRANT EXECUTE, DEBUG ON SP_COPYPROTCRF TO ERES;

