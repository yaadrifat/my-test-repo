<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Appx_Browser%><%--Appendix Browser*****--%></title>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<jsp:include page="panel.jsp" flush="true"/>
<body>

<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>

<DIV class="browserDefault" id="div1">

<%=LC.L_My_Links%><%--My Links*****--%>
<%

	String studyId = request.getParameter("studyId");
	int stId=EJBUtil.stringToNum(studyId);
	
	AppendixDao appendixDao =new AppendixDao();
	appendixDao=appendixB.getByStudyIdAndType(21, "url");
	
	ArrayList appendixIds = appendixDao.getAppendixIds(); 
	ArrayList appendixDescriptions = appendixDao.getAppendixDescriptions();
	ArrayList appendixTypes = appendixDao.getAppendixTypes();
	ArrayList appendixFile_Uris = appendixDao.getAppendixFile_Uris();
	ArrayList appendixFiles = appendixDao.getAppendixFiles();
	ArrayList appendixStudyIds = appendixDao.getAppendixStudyIds();
	ArrayList appendixPubFlags =appendixDao.getAppendixPubFlags();

	String appendixId=null;
	String appendixDescription=null;
	String appendixType=null;
	String appendixFile_Uri = null;
	String appendixFile = null;
	String appendixStudy = null;
	String appendixPubFlag = null;	
	
	int len = appendixIds.size();

   	int counter = 0;
	int counter1=0;

	Object temp;
	%>
	<BR>
	<%
	for(counter = 0;counter<len;counter++)
	{	counter1=counter+1;
		out.println(LC.L_Row/*"Row"*****/+"#"+ counter1);
		temp=appendixTypes.get(counter);
		if (!(temp ==null))
			appendixType=temp.toString();
			out.println(appendixType);
		temp=appendixDescriptions.get(counter);			
		if (!(temp ==null))
			appendixDescription=temp.toString();
			out.println(appendixDescription);
		temp=appendixFile_Uris.get(counter);	
		if (!(temp ==null))
			appendixFile_Uri = temp.toString();
			out.println(appendixFile_Uri);
		temp=appendixFiles.get(counter);
		if (!(temp ==null))	
			
			appendixFile = temp.toString();
			out.println(appendixFile);	
		temp=appendixStudyIds.get(counter);
		if (!(temp ==null))	
			
			appendixStudy = temp.toString();
			out.println(appendixStudy);	
		temp=appendixPubFlags.get(counter);
		if (!(temp ==null))	
			
			appendixPubFlag = temp.toString(); 			 
			out.println(appendixPubFlag);	
			%>
			<BR>
			<%			

		
		}
	%>


<%=LC.L_Got_Appx%><%--GOT Appendix*****--%>
</BODY>
</HTML>
