<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<title><%=LC.L_Manage_Pcols%><%--Manage Protocols*****--%></title>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="JavaScript1.1">

function openwinsnap() {
    windowName=window.open("snapshottext.htm","Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=500,height=400")
	windowName.focus();
;}

function confirmBox(pageRight) {

	if(f_check_perm(pageRight,'N'))
	{
	if (confirm("<%=MC.M_SureToCreate_StdVer%>"))/*if (confirm("Are you sure you want to create a version of this <%=LC.Std_Study_Lower%>?"))*****/ {
	    return true;
	}else{
		return false;
	}
	}
	else
	{
	return false;
	}
}

</SCRIPT>

</HEAD>
<% String src;
	src= request.getParameter("srcmenu");
%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<BODY>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@ page import="com.velos.eres.service.util.*"%>

<BR>
<DIV class="browserDefault" id="div1">

  <%HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))



{





 	String uName = (String) tSession.getValue("userName");





	int pageRight = 0;



	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

%>
  <%-- IN THE ABOVE LINE OF CODE THE Id (MSITES) FOR SITES NEED TO REPLACED BY THAT OF Study--%>
  <%

if (pageRight > 0 )

	{

	   	String userId = (String) tSession.getValue("userId");



		StudyDao studyDao = studyB.getStudyValuesForUsers(userId);



		ArrayList studyIds 		= studyDao.getStudyIds();

		ArrayList studyTitles 		= studyDao.getStudyTitles();

		ArrayList studyObjectives 	= studyDao.getStudyObjectives();

		ArrayList studySummarys 	= studyDao.getStudySummarys();

		ArrayList studyDurs 		= studyDao.getStudyDurations();

		ArrayList studyDurUnits 	= studyDao.getStudyDurationUnits();

		ArrayList studyEstBgnDts 	= studyDao.getStudyEstBeginDates();

		ArrayList studyActBghDts 	= studyDao.getStudyActBeginDates();

		ArrayList studyParentIds 	= studyDao.getStudyParents();

		ArrayList studyAuthors 		= studyDao.getStudyAuthors();

		ArrayList studyNumbers 		= studyDao.getStudyNumbers();

		ArrayList studyVersions = studyDao.getStudyVersions();


	   	int studyId = 0;

		String studyTitle = "";

		String studyNumber = "";

		String studyVersion = "";

	   	int len = studyIds.size();

	   	int counter = 0;



%>
  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=LC.L_MngPcol_Open%><%--Manage Protocols>> Open*****--%> </P>
  <Form name="studybrowser" method="post" action="" onsubmit="">
    <table width="100%" cellspacing="0" cellpadding="0" border=0 >
      <tr >
        <td width = "100%">
          <P class = "defComments"> <%=MC.M_AssignRgt_PatInfo%><%--You should have access to a protocol and have been assigned rights to <%=LC.Pat_Patient_Lower%> information to be able to Manage <%=LC.Pat_Patients%>.*****--%>
		  <Br>
		  <%=MC.M_AcesTo_FlwStd%><%--You have access to the following <%=LC.Std_Studies%>*****--%>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href = '#' onClick=openwinsnap()><%=MC.M_What_IsSnapshot%><%--What is a Snapshot*****--%> </A>
          </P>
        </td>
      </tr>
      <tr>
        <td height="10"></td>
        <td height="10"></td>
      </tr>
    </table>
    <table width="98%" >
      <tr>
        <th width="20%"> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%> </th>
	    <th width="10%"> <%=LC.L_Version%><%--Version*****--%> </th>
        <th width="40%"> <%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%> </th>
		<th width="30%">&nbsp;</th>
      </tr>
      <%

    for(counter = 0;counter<len;counter++)

	{	studyId = EJBUtil.stringToNum(((studyIds.get(counter)) == null)?"-":(studyIds.get(counter)).toString());



		studyTitle = ((studyTitles.get(counter)) == null)?"-":(studyTitles.get(counter)).toString();



		studyNumber = ((studyNumbers.get(counter)) == null)?"-":(studyNumbers.get(counter)).toString();

	   studyVersion = ((studyVersions.get(counter)) == null)?"-":(studyVersions.get(counter)).toString();

	   if (studyTitle.length() > 80)
				{
					studyTitle = studyTitle.substring(0,80) + "...";
				}


		if ((counter%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%

		}

		else{

  %>
      <tr class="browserOddRow">
        <%

		}

  %>
        <td><%=studyNumber%></td>
		<td><%=studyVersion%></td>
        <td><%=studyTitle%> </td>
		<td><A href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=studyId%>"><%=LC.L_Pcol_Dets%><%--Protocol Details*****--%></A>&nbsp;&nbsp;&nbsp;
		<!--<A href="managepatients.jsp?srcmenu=tdmenubaritem3&studyId=<%=studyId%>">Manage <%=LC.Pat_Patients%></A>
		<BR>-->
		<A href="createversion.jsp?srcmenu=tdmenubaritem3&calledfrom=S&studyId=<%=studyId%>" onClick="return confirmBox(<%=pageRight%>);"><%=MC.M_Create_NewVer%><%--Create a new Version*****--%></A>
		</td>
      </tr>
      <%

		}

%>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
<br>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</DIV>
</body>

</html>



