<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Visit_Defn%><%-- Visit Definition*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

 

</head>



 <SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT Language="javascript">
	function setParam(str,formobj){
			formobj.subVal.value=str;
		if (!(validate(formobj))) {
			 return false  }
//		 setVals(formobj.subVal.value,formobj);
		formobj.submit();
		//submitting the form.
	}

	function setVals(str,formobj){
		formobj.action = "visitdetailsave.jsp" + "?updateFlag=" + str;
		return true;
	}

function  validate(formobj){

 	formobj.action = "visitdetailsave.jsp" + "?updateFlag=Submit"; 
	var ret = 0;

//	if (!(validate_col('e-Signature',formobj.eSign))){

//	  formobj.subVal.value="Submit";
//	  return false
//	}

	if (!(validate_col('Visit Name',formobj.visitName))) {
		formobj.subVal.value="Submit";
		return false;
	}

	//KM-#4476
	insertAfterVal = formobj.insertAfter.options[formobj.insertAfter.selectedIndex].value ;
	if(insertAfterVal != "") {
		optVal = insertAfterVal.substring(insertAfterVal.indexOf("/")+1,insertAfterVal.length);
		if (optVal == '') {
			alert("<%=MC.M_IntervalDefn_NotApplicab%>");/*alert("This option of Interval definition is not applicable when the Parent Visit has 'No Interval Defined'");*****/
			return false;
		}
	}	




	if (formobj.noInterval.checked == false)
		noInterval = false;
	else 
		noInterval = true;

	if ( isWhitespace(formobj.months.value))
		m = false;
	else
		m = true;

	if (isWhitespace(formobj.weeks.value))
		w = false;
	else
		w=true;

	if (isWhitespace(formobj.days.value))
		d = false;
	else
		d = true;

	if (isWhitespace(formobj.insertAfterInterval.value))
		i = false;
	else
		i = true;

	if ( !m && !w && !d && !i && !noInterval) {
		alert("<%=MC.M_EtrInterval_MthWeeksDays%>");/*alert("Enter interval in months/weeks/days or Insert After format or Select No Interval option");*****/
		formobj.months.focus();
		formobj.subVal.value="Submit";
		return false;
	}
	
	//KM -- Validation for No interval visit
	else if ((m || w || d) && i && noInterval){
		alert("<%=MC.M_EtrInterval_MthWeeksDays%>");/*alert("Enter interval in months/weeks/days or Insert After format or Select No Interval option");*****/
		formobj.subVal.value="Submit";
		return false;
	}

	else if ( (m || w || d) && i) {
		alert("<%=MC.M_Interval_InMthWksDay%>");/*alert("Either Enter interval in months/weeks/days or Insert After format");*****/
		formobj.months.focus();
		formobj.subVal.value="Submit";
		return false;

	}

	//KM -- Validation for No interval visit
	else if ( (m || w || d) && noInterval) {
		alert("<%=MC.M_EtrInterval_OrSelNoOpt%>");/*alert("Either Enter interval in months/weeks/days or Select No Interval option");*****/
		formobj.months.focus();
		formobj.subVal.value="Submit";
		return false;
	}
	else if (i && noInterval) {
		alert("<%=MC.M_EtrInterval_OrSelNone%>");/*alert("Either Enter interval in Insert After format or Select No Interval option");*****/
		formobj.insertAfterInterval.focus();
		formobj.subVal.value="Submit";
		return false;
	}

	else if ( m || w || d) { // this means insert after is blank, as in the above check we made sure both cannot be true
	     ret = validate_interval(formobj.months.value, formobj.weeks.value, formobj.days.value,formobj);
	     if (ret < 0)
	     {
	     	return false;
	     }	
	}
	//KV:Fixed Bug No. 4860 & 4862 .Checked the integer Validations
	else if (noInterval) {
			if (!(isInteger(formobj.beforenum.value))){
				alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
		 		formobj.beforenum.focus();
		 	 	return false;
		 	}
		 	if (!(isInteger(formobj.afternum.value))){
		 		alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
		 		formobj.afternum.focus();
		 	 	return false;
		 	}
		return true;
	}
	else { // insert after is entered:
		if (!isInteger(formobj.insertAfterInterval.value)) {
			alert("<%=MC.M_EtrInter_NotValid%>");/*alert("Interval entered for Insert After is not valid");*****/
			formobj.insertAfterInterval.focus();
			formobj.subVal.value="Submit";
			return false;
		}
		else {
			p = parseInt(formobj.insertAfterInterval.value);
			if (isNaN(p) || (p == 0) ) {
				alert("<%=MC.M_EtrInter_NotValid%>");/*alert("Interval entered for Insert After is not valid");*****/
				formobj.insertAfterInterval.focus();
				formobj.subVal.value="Submit";
				return false;
			}

			if (isWhitespace(formobj.intervalUnit.value)) {
				formobj.insertAfterInterval.focus();
				alert("<%=MC.M_SelDayWeek_AftIntr%>");/*alert("Select Days/Weeks/Months for insert After interval");*****/
				return false;
			}
			else if (isWhitespace(formobj.insertAfter.value)) {
				alert("<%=MC.M_SelVisit_AftIntr%>");/*alert("Select a Visit Name for insert After interval");*****/
				formobj.insertAfterInterval.focus();
				return false;

			}
			//KV:Fixed Bug No. 4860 & 4862 .Checked the integer Validations
			if (!(isInteger(formobj.beforenum.value))){
				alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
			 	formobj.beforenum.focus();
			 	 return false;
			 }

			 if (!(isInteger(formobj.afternum.value))){
				 alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
			 	formobj.afternum.focus();
			 	 return false;
			 }
					
			else return true;
		}
	} //else

	/*JM: 19June2008, #3446, blocked as esign is not used in this page
	if(isNaN(formobj.eSign.value) == true) {
		alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		formobj.subVal.value="Submit";
		return false;

	}*/
	//SV REDTAG	return setVals(formobj.subVal.value,formobj);

	//KM-D-FIN4
	if (!(isInteger(formobj.beforenum.value))){
		alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
	 	formobj.beforenum.focus();
	 	 return false;
	 }

	 if (!(isInteger(formobj.afternum.value))){
		 alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
	 	formobj.afternum.focus();
	 	 return false;
	 }


	return true ;
   }

function validate_interval(month, week, day,formobj)
{
	var monthvalid = true;
	var weekvalid = true;
	var dayvalid = true;
	var ret = 0;
	var m = 0;
	var w = 0;
	var d = 0;

	if (isInteger(month))
		m = parseInt(month);
	else
		monthvalid = false;


	if (isInteger(week))
		w = parseInt(week);
	else
		weekvalid = false;

	if (isInteger(day))
		d = parseInt(day);
  	else if (isNegNum(day)) {
		d = parseInt(day.substring(1,day.length));
	}
	else
		dayvalid = false;

	//Modified by Manimaran to fix the Bug2354

	if  (m <= 0) monthvalid = false;
	if  (w <= 0) weekvalid = false;
	//if  (d <= 0) dayvalid = false;
////D-FIN-25-DAY0 BK
	if ( (m == 0) && (w == 0)) {
		alert("<%=MC.M_MonthDay_NotValid%>");/*alert("Month/Day/week entered is not valid");*****/
		formobj.days.focus();
		return -1;
	}
	//D-FIN-25-DAY0
    if( d==0 && (w > 1 || m >1)){
    	alert ("<%=MC.M_DayValid_FirstWeek%>");/*alert ("Day 0 interval is only valid for first week of first month");*****/
    formobj.days.focus();
		return -1;
    }
	if (isNaN(m) == true) m = 0;
	if (isNaN(w) == true) w = 0;
	if (isNaN(day) == true) {
	 if (!(isNegNum(day) ==true)) d=0;
	}

	if (!monthvalid) {
		alert("<%=MC.M_MonthEtr_NotValid%>");/*alert("Month entered is not valid");*****/
		formobj.months.focus();
		return -1;
	}


	if (!weekvalid) {
		alert("<%=MC.M_WeekEtr_NotValid%>");/*alert("Week entered is not valid");*****/
		formobj.weeks.focus();
		return -1;
	}

	if (!dayvalid) {
		alert("<%=MC.M_DayEtr_IsInvalid%>");/*alert("Day entered is not valid");*****/
		
		formobj.days.focus();
		return -1;
	}



	if ( (m > 0) && (w == 0) && (d > 30)) {
		alert("<%=MC.M_DaysCntMore30_WhenMth%>");/*alert("Days cannot be more than 30 when month is entered");*****/
		formobj.days.focus();
		return -6;
	}

	if (m > 0) {
		if (w > 4) {
			alert("<%=MC.M_WeeksCntMoreThan4_WhenMth%>");/*alert("Weeks cannot be more than 4 when month is entered");*****/
			formobj.weeks.focus();
			return -7;
		}

	}
	if (w > 0) 	{
		if ( d > 7) {
			alert("<%=MC.M_DaysCntMore7_WhenWeek%>");/*alert("Days cannot be more than 7 when week is entered");*****/
			formobj.days.focus();
			return -8;
		}
	}

	return 0;


}

</SCRIPT>





<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>

<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>
<jsp:include page="include.jsp" flush="true"/>
<% String src;
src= request.getParameter("srcmenu");
%>



<body id="forms">
<br>
<DIV class="popDefault" id="div1">

<%

	String visitId = "";
	String protocolId = request.getParameter("protocolId");
	String mode =request.getParameter("mode");

	String fromPage = request.getParameter("fromPage");
	String calledFrom = request.getParameter("calledFrom");
	String calStatus = request.getParameter("calStatus");

	//JM: 25Apr2008, added for, Enh. #C9
	String offLnFlag= request.getParameter("offLineFlag");
	offLnFlag = (offLnFlag==null)?"":offLnFlag;

	String eventId = "";
	String visitNo = "";

	String calAssoc=request.getParameter("calassoc");
    calAssoc=(calAssoc==null)?"":calAssoc;

	if(mode.equals("M"))
	{
		// in edit mode, pass in the visit id being edited.
		visitId = request.getParameter("visitId");
	}

	String visit_name = "";

	String description = "";
	String displacement = "";
	int months =0, weeks=0;
	Integer days = null;
	int disp=0, insertAfter = 0;
	String noIntervalVal = "";
	String duration="";
	int pageRight=0;
	String monthStr = "";
	String weekStr = "";
	String dayStr = "";
	String intervalStr = "";
	int interval = 0;
	String intervalUnit = "";

	ArrayList visitNames= new ArrayList();
	ArrayList visitIds= new ArrayList();
	ArrayList displacements = new ArrayList();
	ArrayList numOfDaysList = new ArrayList();

	String max_visit_no = request.getParameter("max_visit_no");
	String seq_no = request.getParameter("seq_no");
	duration=request.getParameter("duration");
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{
		String uName = (String) tSession.getValue("userName");
		String calName = "";

		String durationBefore ="";
		String durationUnitBefore = "";
		String durationAfter ="";
		String durationUnitAfter ="";

		if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule")|| fromPage.equals("selectEvent") )

		{

%>

			<P class="sectionHeadings"> <%=MC.M_PcolCal_VstDefn%><%-- Protocol Calendar >> Visit Definition*****--%> </P>

<%		}

		else {

			calName = (String) tSession.getValue("protocolname");
			Object[] arguments1 = {calName};
%>
			<P class="sectionHeadings">  <%=VelosResourceBundle.getMessageString("M_PcolCal_VisitDefn",arguments1)%><%--Protocol Calendar [ <%=calName%> ] >>  Visit Definition*****--%> </P>

<%		}%>

<%

		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){
			 	pageRight= 0;
		   }else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
	   	   }

		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));
		}

		visitId = request.getParameter("visitId");
		visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
		visitIds = visitdao.getVisit_ids();
		visitNames = visitdao.getNames();
		displacements = visitdao.getDisplacements();
		numOfDaysList = visitdao.getDays();


//JM: 25Apr2008, added for, Enh. #C9
		//if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("eventbrowser")) || fromPage.equals("selectEvent") )) )
			//KM-to fix the issue-3443
			
			if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("fetchProt")) || fromPage.equals("selectEvent") )) )

		    {

					protVisitB.setVisit_id(EJBUtil.stringToNum(visitId));
					protVisitB.getProtVisitDetails();
					
					//if (mode.equals("M")) { //KM-3445
					visit_name = protVisitB.getName();
						
					if (mode.equals("M")) {	//KM-3452
						visitNo= new Integer(protVisitB.getVisit_no()).toString();
						description = protVisitB.getDescription();
						description = (   description  == null)?"":(  description ) ;
						//disp = protVisitB.getDisplacement();
						

					}

					noIntervalVal = protVisitB.getIntervalFlag();
					noIntervalVal = (   noIntervalVal  == null)?"":(  noIntervalVal ) ;
					
					months = protVisitB.getMonths();
					weeks = protVisitB.getWeeks();
					days = protVisitB.getDays();
					Integer tempInteger = protVisitB.getInsertAfter();
					insertAfter = (tempInteger==null)?0:tempInteger.intValue();

					 if (insertAfter > 0) {
						monthStr = "";
						weekStr = "";
						dayStr = "";
						intervalStr = (protVisitB.getInsertAfterInterval()).toString();
						intervalUnit = protVisitB.getInsertAfterIntervalUnit();

					   }
					   else {

					   	if (months > 0)
							monthStr = new Integer(months).toString();

					   	if (weeks > 0)
							weekStr = new Integer(weeks).toString();
					   	////D-FIN-25-DAY0 BK
						if(days!=null)
					   	 dayStr = days.toString();
					   	 //D-FIN-25-DAY0-BK

						intervalStr = "";
						intervalUnit = "";

					   }
			
				durationBefore = protVisitB.getDurationBefore();
				durationBefore  = (   durationBefore  == null)?"0":(  durationBefore ) ;
				durationUnitBefore = protVisitB.getDurationUnitBefore();
				durationUnitBefore  = (   durationUnitBefore  == null)?"":(  durationUnitBefore ) ;
				durationAfter = protVisitB.getDurationAfter();
				durationAfter  = (   durationAfter  == null)?"0":(  durationAfter ) ;
				durationUnitAfter = protVisitB.getDurationUnitAfter();
				durationUnitAfter  = (   durationUnitAfter  == null)?"":(  durationUnitAfter ) ;

	   }
%>

   <form name=visitdetail id="visitdetfrm" METHOD=POST action=visitdetailsave.jsp onsubmit="if (validate(document.visitdetail)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">



<input type=hidden name=srcmenu value=<%=src%>>

<input type=hidden name=visitId value=<%=visitId%>>

<input type=hidden name=protocolId value=<%=protocolId%>>

<input type=hidden name=calledFrom value=<%=calledFrom%>>

<input type=hidden name=mode value=<%=mode%>>

<input type=hidden name=calassoc value=<%=calAssoc%>>

<input type=hidden name=fromPage value=<%=fromPage%>>

<input type=hidden name=calStatus value=<%=calStatus%>>
<input type=hidden name=duration value=<%=duration%>>
<%

if ((calStatus.equals("A")) || (calStatus.equals("F"))){%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_CntChgVisitDet_ForFrzPcol%><%-- Changes cannot be made to visit Interval and visit Details for an Active/Frozen Protocol*****--%></Font></P>

<%}

if (pageRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForVisit%><%-- You have only View permission for the visit*****--%>.</Font></P>

<%}%>





<TABLE width="60%" cellspacing="1" cellpadding="0" >

   <tr>
<% if (mode.equals("M")) {%>
	<td width="20%"> <%=LC.L_Visit_Sequence%><%-- Visit Sequence*****--%></td>
<%--SV, 10/29/04, visit_no may not serve the purpose of display sequence any more. simple seq no will do the job here. --%>
	<td >
		<INPUT NAME="visitNo" value="<%=visitNo%>" TYPE=hidden SIZE=3 MAXLENGTH=5 readonly>
		<INPUT NAME="seqNo" value="<%=seq_no%>" TYPE=text SIZE=3 MAXLENGTH=5 readonly>
	</td>
<%}
else {%>
	<td >
		<%--SV, 10/01, assign the automatically generated visit #. Item 4, requirements doc.--%>
		<INPUT NAME="visitNo" type="hidden" value="<%=EJBUtil.stringToNum(max_visit_no) + 1%>">
	</td>
<%}%>
   </tr>

  <%
  //KM-3452	  
  
  if(mode.equals("N")) { %>
	<tr>
    <td colspan="2" ><%=MC.M_Visit_BeingCopied%><%--Visit being copied*****--%> :  <%=visit_name%> </td> 
	</tr>

  <%
	visit_name ="";	  
   } %>


  <tr>

	<td width="25%"> <b> <%=LC.L_Visit_Name%><%-- Visit Name*****--%> </b><FONT class="Mandatory">* </FONT> </td>
	<%
	//JM: 25Apr2008, added for, Enh. #C9
	if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){
%>


	<td width="35%"> <INPUT NAME="visitName" value="<%=visit_name%>" TYPE=TEXT SIZE=60 MAXLENGTH=50 readOnly="true"> </td>
<%} else{%>

	<td > <INPUT NAME="visitName" value="<%=visit_name%>" TYPE=TEXT SIZE=60 MAXLENGTH=50> </td>
<%}%>
   </tr>

   <tr>

	<td > <%=LC.L_Description%><%-- Description*****--%> </td>

	<td > <INPUT NAME="description" value="<%=description%>" TYPE=TEXT SIZE=60 MAXLENGTH=200> </td>

   </tr>
   </TABLE>

   <table width="75%" cellspacing="0" cellpadding="0" border="0">
   	   <tr height="50">
		<td width="15%"> <b><%=LC.L_Interval%><%-- Interval*****--%></b><FONT class="Mandatory">* </FONT> </td>

		<td width="15%"> <%=LC.L_Month%><%-- Month*****--%><BR>

<%
	//JM: 25Apr2008, added for, Enh. #C9
	if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){
%>
		<INPUT NAME="months" value="<%=monthStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5 readOnly="true">

<%}else{%>

		<INPUT NAME="months" value="<%=monthStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5>

<%}%>
		</td>


		<td width="20%"> <%=LC.L_Week%><%-- Week*****--%><BR>
<%
	//JM: 25Apr2008, added for, Enh. #C9
	if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){
%>
		<INPUT NAME="weeks" value="<%=weekStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5 readOnly="true">
<%}else{%>
		<INPUT NAME="weeks" value="<%=weekStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5>

<%}%>
		</td>

		<td width="25%"> <%=LC.L_Day%><%-- Day*****--%><BR>
<%
	//JM: 25Apr2008, added for, Enh. #C9
	if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){
%>

		<INPUT NAME="days" value="<%=dayStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5 readOnly="true">
<%}else{%>
		<INPUT NAME="days" value="<%=dayStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5>
<%}%>

		</td>
		</tr>

		<tr>
		<td align="right"> <%=LC.L_Or%><%--Or*****--%>&nbsp;&nbsp;&nbsp; </td>
		<td >

<%
	//JM: 25Apr2008, added for, Enh. #C9
	if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){
%>
		<INPUT NAME="insertAfterInterval" value="<%=intervalStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5 readOnly="true">
<%}else{%>

		<INPUT NAME="insertAfterInterval" value="<%=intervalStr%>" TYPE=TEXT SIZE=3 MAXLENGTH=5>
<%}%>

		</td>
		<td >
		<%
		//JM: 25Apr2008, added for, Enh. #C9
			if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){%>

				 <select name="intervalUnit" disabled>
			<%}else{%>
				 <select name="intervalUnit">
		<%}%>


			<% if (intervalUnit.equals("D")) { %>
				<option value="D" Selected ><%=LC.L_Days%><%-- Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%-- Weeks*****--%></option>
				<option value="M"><%=LC.L_Months%><%-- Months*****--%></option>
			<%} else if  (intervalUnit.equals("W")) { %>
				<option value="D" ><%=LC.L_Days%><%-- Days*****--%></option>
				<option value="W" Selected><%=LC.L_Weeks%><%-- Weeks*****--%></option>
				<option value="M"><%=LC.L_Months%><%-- Months*****--%></option>
			<%} else if  (intervalUnit.equals("M")) { %>
				<option value="D"><%=LC.L_Days%><%-- Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%-- Weeks*****--%></option>
				<option value="M" Selected><%=LC.L_Months%><%-- Months*****--%></option>
			<%} else { %>
				<OPTION value='' SELECTED> <%=LC.L_Select_AnOption%><%-- Select an Option*****--%> </OPTION>
				<option value="D"><%=LC.L_Days%><%-- Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%-- Weeks*****--%></option>
				<option value="M" ><%=LC.L_Months%><%-- Months*****--%></option>
			<%}%>

		</select>
			&nbsp;<%=LC.L_After_Lower%><%--after*****--%>
			</td>
		<%--String insertAfterPullDown=EJBUtil.createPullDown("insertAfter", insertAfter, visitIds, visitNames);--%>
<%--SV, 10/11/04, REDTAG: currently try to pass in visit id and displacement into save by concatenating the two with "/" as separator. Is there a better way?? --%>
		<%
			int visitid = 0;
			int currVisit = EJBUtil.stringToNum(visitId);
			String insertAfterPullDown = "";

	
	//JM: 25Apr2008, added for, Enh. #C9
			if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){
				 insertAfterPullDown = new String("<Select name='insertAfter' disabled> ") ;
			}else{
				 insertAfterPullDown = new String("<Select name='insertAfter'> ") ;
			}


			if ( (currVisit == 0) || (insertAfter == 0) )
				insertAfterPullDown += "<OPTION value='' SELECTED> "+LC.L_Select_AnOption+" </OPTION>";/*insertAfterPullDown += "<OPTION value='' SELECTED> Select an Option </OPTION>";*****/

			String dispStr="";
			for (int counter = 0; counter < visitIds.size() ; counter++){
				visitid = EJBUtil.stringToNum(visitIds.get(counter).toString());
				
				if(mode.equals("M")) {  //KM-3445
				   if (visitid == currVisit)
					  continue;
				}
				//DFIN-25-DAY0
				String tempDisp;
                if("0".equals(""+numOfDaysList.get(counter))){
                		tempDisp = "0";
                	}
                else if("0".equals(""+displacements.get(counter))){
            		tempDisp = "";
            	}
                else{
                	tempDisp = ""+displacements.get(counter);
                }
                
				insertAfterPullDown += "<OPTION value = "+ visitIds.get(counter) + "/" + tempDisp;

		
				if (insertAfter == visitid) {
					insertAfterPullDown += " Selected ";
					dispStr = visitIds.get(counter) + "/" + tempDisp;
				}
				insertAfterPullDown += " >" + visitNames.get(counter)+ "</OPTION>";
			}
			insertAfterPullDown += ("</SELECT>");
		
		//KM-#4874
		if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))){	
		%>
			<input type ="hidden" name= "insertAfterOffline" value = "<%=dispStr%>">
			<input type ="hidden" name ="intervalUnitOffline" value ="<%=intervalUnit%>">
			
		<%}%>
			<input type ="hidden" name ="offLnFlag" value="<%=offLnFlag%>">
		<td colspan="3">
		
		<%=insertAfterPullDown%>

 
		</td>


		</tr>



		<tr height="30">
		<% 
		   String chkStr ="";
		   if(noIntervalVal.equals("1")) 
			  chkStr = "checked";
		   else 
			   chkStr = "";
			
		%>
		<td align="right"> <%=LC.L_Or%><%--Or*****--%>&nbsp;&nbsp;&nbsp; </td><td colspan="2" align="left"> <input type="checkbox" name="noInterval" <%=chkStr%> > <%=MC.M_NoIntervalDefined%><%--No Interval Defined*****--%></td>  </tr>
		<!--KV: Fixed Bug No.4863 Restricted user entry to three digit integer-->
		<tr> <td> <%=LC.L_Visit_Window%><%--Visit Window*****--%>  </td> <td nowrap> <input name="beforenum" type="text" value="<%=durationBefore%>" size="5" maxlength = "3" > 
		<Select Name="durUnitA">
		<Option value="D" <%if (durationUnitBefore.equals("D")){%> Selected <%}%>><%=LC.L_Days%><%-- Days*****--%></Option>
		<Option value="W" <%if (durationUnitBefore.equals("W")){%> Selected <%}%>><%=LC.L_Weeks%><%-- Weeks*****--%></Option>
		<Option value="M" <%if (durationUnitBefore.equals("M")){%> Selected <%}%>><%=LC.L_Months%><%-- Months*****--%></Option>
		<Option value="Y" <%if (durationUnitBefore.equals("Y")){%> Selected <%}%>><%=LC.L_Years%><%-- Years*****--%></Option>
	    </Select> <%=LC.L_Before%><%-- Before*****--%>
		</td>
		<!-- Rohit Fixed Bug No. 4870 -->
		<!--KV: Fixed Bug No.4863 Restricted user entry to three digit integer-->	
		<td nowrap >&nbsp;&nbsp;<input name="afternum" type="text" value="<%=durationAfter%>" size="5" maxlength = "3" > 
		<Select Name="durUnitB">
		<Option value="D" <%if (durationUnitAfter.equals("D")){%> Selected <%}%>><%=LC.L_Days%><%-- Days*****--%></Option>
		<Option value="W" <%if (durationUnitAfter.equals("W")){%> Selected <%}%>><%=LC.L_Weeks%><%-- Weeks*****--%></Option>
		<Option value="M" <%if (durationUnitAfter.equals("M")){%> Selected <%}%>><%=LC.L_Months%><%-- Months*****--%></Option>
		<Option value="Y" <%if (durationUnitAfter.equals("Y")){%> Selected <%}%>><%=LC.L_Years%><%-- Years*****--%></Option>
	    </Select> <%=LC.L_After%><%-- After*****--%>
		</td>
		 </tr>

</table> 
<BR>
<BR>

<TABLE width="80%" cellspacing="0" cellpadding="0" >
	<tr>
	 <!--  <td width="20%">
		e-Signature <FONT class="Mandatory">* </FONT>
	   </td>
	   <td width="30%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 >
	   </td>-->
<%
	if ((calStatus.equals("A")) || (calStatus.equals("F")))
	{

	}else{

		if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) ) {

		%>

		<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="N"/>
				<jsp:param name="formID" value="visitdetfrm"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

		<% }

		if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary")|| fromPage.equals("selectEvent")) {

			if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) ) {
			}

		}%>
	<%} // else%>

 </tr>

</table>
	<input type=hidden name=pageRight value=<%=pageRight%> >
   <input type="hidden" name="subVal" value="Sumbit">


</form>


<%



} else {  //else of if body for session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%}%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>
</html>
