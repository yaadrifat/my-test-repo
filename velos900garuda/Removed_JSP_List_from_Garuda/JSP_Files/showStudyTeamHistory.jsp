<!--
	File Name: showStudyTeamHistory.jsp

	Author : Manimaran

	Created Date	: 08/04/2006

	Last Modified Date :

	Purpose	: For July-August Enhancement(S4)
-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false;
if (isIrb) {
%>
    <title><%=MC.M_ResComp_NewStdPers%><%--Research Compliance >> New Application >> Study Personnel*****--%></title>
<% } else {  %>
	<title><%=MC.M_StdTeam_Hist%><%--Study Team History*****--%></title>
<% }  %>
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
	

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%
	String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<SCRIPT language="javascript"></SCRIPT>
<body>
<br>
<%
	HttpSession tSession = request.getSession(true);
	String tab = request.getParameter("selectedTab");
	String studyId = (String) tSession.getValue("studyId");
	String from = request.getParameter("from");
	String currentStatId = request.getParameter("currentStatId");
%>

<DIV class="BrowserTopn" id = "div1">
    <% String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; %>
	<jsp:include page="<%=includeTabsJsp%>" flush="true">
	<jsp:param name="from" value="<%=from%>"/>
	<jsp:param name="selectedTab" value="<%=tab%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
	</DIV>
<%
String stid= request.getParameter("studyId");
if(stid==null || stid==""){%>
   <DIV class="BrowserBotN BrowserBotN_S_1" id="div1">
	<%} else {%>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"><%}%>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%
	if (sessionmaint.isValidSession(tSession)){
		String uName = (String) tSession.getValue("userName");
		String modulePk = request.getParameter("modulePk");
		String fromjsp = request.getParameter("fromjsp");
		String userName = request.getParameter("userName");
		int pageRight = 0;
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
	 		pageRight= 0;
		}else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYTEAM"));
		}
 	        if ( pageRight > 0 ){
%>
			<jsp:include page="statusHistory.jsp" flush="true">
				<jsp:param name="modulePk" value="<%=modulePk%>"/>
				<jsp:param name="moduleTable" value="er_studyteam"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="fromjsp" value="<%=fromjsp%>"/>
				<jsp:param name="pageRight" value="<%=pageRight%>"/>
				<jsp:param name="userName" value="<%=userName%>"/>
				<jsp:param name="currentStatId" value="<%=currentStatId%>"/>
				<jsp:param name="showDeleteLink" value="false"/>

			</jsp:include>
<%		} //end of if body for page right
		else {
%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
<%
		}//end of else body for page right
	}//end of if body for session
		else{
%>
			<jsp:include page="timeout.html" flush="true"/>

<%	}
%>
	<div>
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
	<div class ="mainMenu" id = "emenu" >
		<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
	</div>
</body>

</html>

