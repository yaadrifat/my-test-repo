<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Cal_ToLib%><%--Calendar to Library*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

<SCRIPT Language="javascript">
 function  validate(formobj, mode){
 if (typeof(formobj.rbSharedWith)!="undefined")
   {
  if((formobj.rbSharedWith[2].checked == true) && (formobj.selGrpNames.value==""))
		 {
		 alert("<%=MC.M_PlsSelectGrp%>");/*alert("Please Select Group");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[3].checked == true) && (formobj.selStudy.value==""))
		 {
		 alert("<%=MC.M_PlsSelectStd%>");/*alert("Please Select <%=LC.Std_Study%>");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[4].checked == true) && (formobj.selOrg.value==""))
		 {
		 alert("<%=MC.M_PlsSelectOrg%>");/*alert("Please Select Organization");*****/
		 return false;
		 }
	 }
 	/* Bug#9899 5-Jun-2012 -Sudhir*/
 	if (!allsplcharcheck(formobj.name.value))  
	{
		formobj.name.focus();  return false;
	}
 	if (!(validate_col('Protocol Name',formobj.name))) return false;
 	
 	 	if (!(validate_col('Calendar Type',formobj.caltype))) return false;

			if (!(validate_col('Esign',formobj.eSign))) return false;


			 if(isNaN(formobj.eSign.value) == true) {
					alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
					formobj.eSign.focus();
					return false;
			}
		}
	
			







</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>


<% String src;
	

String selectedTab = request.getParameter("selectedTab");

src= request.getParameter("srcmenu");

%>





<body>
<DIV class="popDefault"> 

  <%

	String formId = request.getParameter("formId"); //?? Questionable; This should be protocol id (event id for event type = 'P')

	HttpSession tSession = request.getSession(true); 


	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");

	String	mode = request.getParameter("mode");
	String	eventId=request.getParameter("eventid");
//JM:
//	String name=request.getParameter("name");
	String name=StringUtil.decodeString(request.getParameter("name"));
	
	name=(name==null)?"":name;
	
	String accId=(String)tSession.getAttribute("accountId");
	int iaccId=EJBUtil.stringToNum(accId);
	
//JM: 13Dec2006	
//	String desc=request.getParameter("desc");
		String desc=StringUtil.decodeString(request.getParameter("desc"));
	
	desc=(desc==null)?"":desc;
	
	CatLibDao catLibDao= new CatLibDao();
	catLibDao = catLibJB.getAllCategories(iaccId,"L");
	ArrayList ids= new ArrayList();
	ArrayList names= new ArrayList();
	String pullDown;
	int rows;
	rows=catLibDao.getRows();
	ids = catLibDao.getCatLibIds();
	names= catLibDao.getCatLibNames();
	pullDown=EJBUtil.createPullDown("caltype",0, ids, names);
	
	
		int pageRight = 0;


		
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>

  <Form name="protocolCal" id="protCal" method="post" action="savecaltolibrary.jsp" onsubmit="if (validate(document.protocolCal, '<%=mode%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

    <table width="100%" cellspacing="0" cellpadding="0">
      <tr> 
        <td width="20%" ><%=LC.L_Cal_Name%><%--Calendar Name*****--%> <FONT class="Mandatory" >* </FONT> </td>
        <td width="90%">
			<input type="text" name="name" size = 30 MAXLENGTH = 50 value="<%=name%>">
        </td>
      </tr>
          
      <tr> 
        <td width="20"><%=LC.L_Description%><%--Description*****--%></td>
        <td> 
          <TextArea  name="desc" rows=3 cols=40><%=desc%></TextArea>
        </td>
      </tr>
      <tr> 
	 <td width="20%" ><%=LC.L_Cal_Type%><%--Calendar Type*****--%><FONT class="Mandatory" >* </FONT> </td>
        <td width="90%">
			<%=pullDown%>
        </td>
</tr>

      <tr> 
        <td> 
          <input type="hidden" name="eventid" MAXLENGTH = 15 value="<%=eventId%>">
        </td>
      </tr>
      </table> 
      <jsp:include page="objectsharewith.jsp" flush="true">
   	 <jsp:param name="objNumber" value="3"/>
   	 <jsp:param name="mode" value="<%=mode%>"/>
   	 <jsp:param name="formobject" value="document.protocolCal"/>
   	 <jsp:param name="formnamevalue" value="protocolCal"/>
   	 <jsp:param name="fkObj" value="0"/>	 		  	
   	 <jsp:param name="sharedWith" value="A"/>	 		  	 		 	 	 	 
	 </jsp:include>
      
	

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="protCal"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


  </Form>
<%
}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>

</html>


