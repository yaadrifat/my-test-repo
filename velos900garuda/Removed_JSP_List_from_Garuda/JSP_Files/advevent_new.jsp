<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title> <%=MC.M_MngPat_AdvEvtDets%><%-- Manage <%=LC.Pat_Patient%> >> Adverse Event Details*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="JavaScript1.1">
function openwin1(frm) {


      windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+frm,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
      windowName.focus();


}
function openAdverseEvent(studyId,personId,src) {

   windowName = window.open("adveventlookup.jsp?&studyId="+studyId+"&personPK="+personId+"&srcmenu="+src,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
      windowName.focus();




}
function openWin() {
      windowName = window.open("","Information","toolbar=yes,location=yes,scrollbars=yes,resizable=yes,menubar=yes,width=600,height=300")
	windowName.focus();
}
function openWinMoreDetails(modId, mode, modName,pageRight)
	{
		if (mode == 'M'){
			windowname=window.open("moredetails.jsp?modId=" + modId+"&modName="+modName,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, ");
			windowname.focus();
		} else if (mode == 'N')	{
			alert("<%=MC.M_SavePage_BeforeDets%>");/*alert("Please save the page first before entering More Details");*****/
			return false;
		}
	}
function openwindowdict(studyAdvlkpVer,versionnum,formobj){

      currentDictSettingValue = formobj.currentDictSetting.value;

	if (studyAdvlkpVer == "0"){
	 	windowName = window.open("defaultGrade.jsp?parentform=advevent&gradefld=grade&advnamefld=advName&dispfield=advGradeText&advDictionaryField=advDictionary&currentDictSettingValue="+currentDictSettingValue,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=200,left=200");   windowName.focus();
	}else{
   		var calc = "Grade : fdatacolumn fdispcolumn";
		if (versionnum == 3) {
	 		var keyexpr = "advDictionary|[VELEXPR]*[VELSTR="+currentDictSettingValue+"]~advGradeText|[VELEXPR]*[VELSTR=Grade:]*[VELSPACE]*[VELKEYWORD=nci3_grade]*[VELSPACE]*[VELKEYWORD=nci3_shrtname]~grade|nci3_grade~advName|nci3_shrtname~MedDRAcode|nci3_meddra";
	 	} else {
			var keyexpr = "advDictionary|[VELEXPR]*[VELSTR="+currentDictSettingValue+"]~advGradeText|[VELEXPR]*[VELSTR=Grade:]*[VELSPACE]*[VELKEYWORD=adv_lkp_grade]*[VELSPACE]*[VELKEYWORD=adv_lkp_toxic]~grade|adv_lkp_grade~advName|adv_lkp_toxic~MedDRAcode|adv_lkp_meddra";
	 	}
	 windowName = window.open("multilookup.jsp?viewId=" +studyAdvlkpVer + "&form=advevent&dfilter=&frameMode=TB&maxselect=1&keyword="+keyexpr ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=900,height=550 top=100,left=100")
	 windowName.focus();
	}
}



function openwindowcalc(nciVersion,pkey){
	if (nciVersion == '0' || nciVersion == ''){
		alert("<%=MC.M_NoAdvEvtDictAssoc_Std%>")/*alert("There is no Adverse Event Dictionary associated with the <%=LC.Std_Study%>")*****/
		return ;
	}
	else
	{
		windowname=window.open("getAdvToxicity.jsp?nciVersion=" + nciVersion + "&pkey=" + pkey + "&parentform=advevent&gradefld=grade&advnamefld=advName&dispfield=advGradeText" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=500,height=350 top=120,left=200 0, ");
		windowname.focus();
	}
}

function  validate(formobj){

	if (document.getElementById('mandae')) {
	     if (!(validate_col('Adverse-Eventtype',formobj.adve_type))) return false
	}

	if (document.getElementById('mandstdate')) {
		 if (!(validate_col('Start Date',formobj.startDt))) return false
	}

    if (document.getElementById('mandent')) {
		 if (!(validate_col('Entered By',formobj.enteredByName))) return false
	}

    var mVal = formobj.advMode.value;
	if (mVal){
	 	var fdaVal = document.getElementById("fdaStudy").value;
		if (fdaVal == "1" && mVal == "M"){
			if (  !(validate_col('<%=LC.L_ReasonForChangeFDA%>',formobj.remarks))) return false;
		}
	}
    
    if (!(validate_col('e-Signature',formobj.eSign))) return false


	 //KM

	 if (document.getElementById('pgcustomtype')) {
	   if (!(validate_col('Adverse-Eventtype',formobj.adve_type))) return false
	 }

	 if (document.getElementById('pgcustomaegrading')) {
	   if (!(validate_col('Adverse Event/Grading',formobj.advGradeText))) return false
	 }

     if (document.getElementById('pgcustommeddra')) {
	   if (!(validate_col('MedDRA code',formobj.MedDRAcode))) return false
	 }

     if (document.getElementById('pgcustomdict')) {
	   if (!(validate_col('Dictionary',formobj.advDictionary))) return false
	 }

	  if (document.getElementById('pgcustomdesc')) {
	   if (!(validate_col('Description',formobj.descripton))) return false
	 }

	  if (document.getElementById('pgcustomcourse')) {
	   if (!(validate_col('Treatment Course',formobj.treatment))) return false
	 }

	  if (document.getElementById('pgcustomstdt')) {
	   if (!(validate_col('Start Date ',formobj.startDt))) return false
	 }

	  if (document.getElementById('pgcustomstopdt')) {
	   if (!(validate_col('Stop Date ',formobj.stopDt))) return false
	 }

	  if (document.getElementById('pgcustomdiscdt')) {
	   if (!(validate_col('Discovery Date',formobj.aediscoveryDt))) return false
	 }

	  if (document.getElementById('pgcustomlogdt')) {
	   if (!(validate_col('Logged Date',formobj.aeloggedDt))) return false
	 }

	  if (document.getElementById('pgcustoment')) {
	   if (!(validate_col('Entered By ',formobj.enteredByName))) return false
	 }

	  if (document.getElementById('pgcustomrep')) {
	   if (!(validate_col('Reported By',formobj.reportedByName))) return false
	 }


	  if (document.getElementById('pgcustomattr')) {
	   if (!(validate_col('Attribution',formobj.adve_relation))) return false
	 }

	  if (document.getElementById('pgcustomlink')) {
	   if (!(validate_col('Linked To',formobj.linkedToName))) return false
	 }

	  if (document.getElementById('pgcustomaction')) {
	   if (!(validate_col('Action',formobj.outaction))) return false
	 }

	  if (document.getElementById('pgcustomrec')) {
	   if (!(validate_col('Recovery Description',formobj.adve_recovery))) return false
	 }

	  if (document.getElementById('pgcustomoutcome')) {
	   if (!(validate_col('Outcome Notes',formobj.outcomeNotes))) return false
	 }

	  if (document.getElementById('pgcustomnotes')) {
	   if (!(validate_col('Notes',formobj.notes))) return false
	 }

	  if (document.getElementById('pgcustomformstat')) {
	   if (!(validate_col('Form Status',formobj.formStatus))) return false
	 }


	//KM

	 //added for date field validation
     if (!(validate_date(formobj.startDt))) return false
     if (!(validate_date(formobj.stopDt))) return false



     if (!(validate_date(formobj.aediscoveryDt))) return false
     if (!(validate_date(formobj.aeloggedDt))) return false



	 if (!(validate_date(formobj.outcomeDt))) return false
	var test="";


	for(var i=0;i<formobj.outcomeType.length;i++)
	{
		if(formobj.outcomeType[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}
	}

	formobj.outcomeString.value=test;
	//alert(formobj.outcomeString.value);


	test="";
	for(var i=0;i<formobj.addInfo.length;i++)
	{
		if(formobj.addInfo[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}
	}
	formobj.addInfoString.value=test;

	test="";
	for(var i=0;i<formobj.advNotify.length;i++)
	{
		if(formobj.advNotify[i].checked==true)
		{
		test+="1" ;
		} else
		{
		test+="0";
		}
	}

	formobj.advNotifyString.value=test;
	if(formobj.outcomeType[0].checked == true && formobj.outcomeDt.value != '')	{
	if( confirm("<%=MC.M_SelDthPat_FutureNotific%>")){/*if( confirm("You have selected 'Death' as an outcome for this <%=LC.Pat_Patient%>. To prevent any further action connected to this <%=LC.Pat_Patient%>, such as future notifications, would you like to deactivate this <%=LC.Pat_Patient%>'s schedule now?")){*****/
	formobj.death.value = "Yes";
 	} else{
	formobj.death.value = "No";
	return false;
	}
	}
	if (formobj.outcomeDt.value != '')	{
		if(formobj.outcomeType[0].checked == false)
		{
			alert("<%=MC.M_EtrPatDtDth_DthOutcome%>");/*alert("You have entered <%=LC.Pat_Patient%>'s Date of Death. Please select 'Death' in Outcome Type");*****/
			return false;
		}
	}
	if (formobj.outcomeDt.value == '' && formobj.outcomeType[0].checked == true){
			alert("<%=MC.M_Selc_DtOfDth%>");/*alert("Please Select Date of Death");*****/
			formobj.outcomeDt.focus();
			return false;
	}




	if (document.getElementById('pgcustomouttype')) {
	  var val = false;
	  for(var i=0;i<formobj.outcomeType.length;i++)
	  {
		if(formobj.outcomeType[i].checked==true)
			val = true;
	  }

	  if (val == false)
	  {
		   alert("<%=MC.M_SelAtleast_OneOutcomeTyp%> ");/* alert("Please select atleast one option for 'Outcome Type' ");*****/
		   return false;
	  }
	}



	if (document.getElementById('pgcustomaddl')) {

	  var val = false;
	  for(var i=0;i<formobj.addInfo.length;i++)
	  {
		if(formobj.addInfo[i].checked==true)
			val = true;
	  }

	  if (val == false)
	  {
		   alert("<%=MC.M_SelAtleast_OneOptAddlInfo%> ");/*alert("Please select atleast one option for 'Additional Information' ");*****/
		   return false;
	  }
	}

 //Fixed Bug No:3687.Put validations on combination of Date and checkbox in case of Notified Dates(IRB,FDA...)
	if (document.getElementById('pgcustomnotified')) {

	  var val = false;
	  var datenotify="";
	  for(var i=0;i<formobj.advNotify.length;i++)
	  {
            //alert(i);
		if(formobj.advNotify[i].checked==true)
		{

			if(document.getElementById("notifyDate"+i).value=="")
			{
				alert("<%=MC.M_Selc_DtForNotif%>")/*alert("Please select date for Notified")*****/
				return false;
			}else
			{
			val = true;
			}
			//alert(formobj.advNotify[i].checked+ "and "+ val +" i "+i);
		}else
		{
			if(document.getElementById("notifyDate"+i).value!="")
			{
				alert("<%=MC.M_PlsSelOpt_DtSel%> ");/*alert("Please select option for 'The Following were Notified:' where date has been selected ");*****/
				   return false;
			}else
			{
			val = true;
			}


		}


	  }

	  if (val == false)
	  {
		   alert("<%=MC.M_SelAtleastOneOpt_ForNtfi%> ");/*alert("Please select atleast one option for 'The Following were Notified:' ");*****/
		   return false;
	  }
	}



	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
			 return false;
     }

	  //condition for date field validation by JM on 07April05

     if ((formobj.startDt.value != null && formobj.startDt.value != '' )&&(formobj.stopDt.value != null && formobj.stopDt.value !=''))
	{

		if (CompareDates(formobj.startDt.value,formobj.stopDt.value, '>'))
		{
		  	alert ("<%=MC.M_StartDtCnt_GtrThanStopDt%>");/*alert ("Start Date can not be greater than Stop Date");*****/

			formobj.startDt.focus();
			return false;
		}

	}

	//da= new Date();
	//var str1,str2;
	//str1=parseInt(da.getMonth());
	//str2=str1+1;
	//todate = str2+'/'+da.getDate()+'/'+da.getFullYear();


	//JM: 20Apr2009, #4026
	todate = formatDate(new Date(),calDateFormat);

	 if (formobj.outcomeDt.value != null && formobj.outcomeDt.value != '' )
	{

		if (CompareDates(formobj.outcomeDt.value,todate, '>'))
		{
		  	alert ("<%=MC.M_DthDtNotGtr_TodayDt%>");/*alert ("Death Date should not be greater than Today's Date");*****/

			formobj.outcomeDt.focus();
			return false;
		}
	}
return true;
}
function checkoutcome(formobj,boxval){

if(boxval==0){

	for( i = 1;i < formobj.outcomeType.length ; i++){
		if(formobj.outcomeType[i].checked == true)  {
			alert("<%=MC.M_CntSel_DthOptWithOth%> ");/*alert("You cannot select Death option along with any other option ");*****/
			formobj.outcomeType[0].checked=false;
			return false;
		}
	 }
	if(formobj.outcomeType[0].checked == false)	{
		formobj.outcomeDt.value ="";
	}
}else{
		if((formobj.outcomeType[0].checked)==true)	{
			alert("<%=MC.M_CntSelOpt_WithDthOpt%>");/*alert("You cannot select any option along with Death option");*****/
			formobj.outcomeType[boxval].checked=false;
			return false;
		}
}


//alert(boxval);
}

function openWinStatus(patId,patStatPk,pageRight,study,orgRight) {
	changeStatusMode = "yes";

	if (f_check_perm_org(pageRight,orgRight,'N')) {
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
}
</SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="ulinkB" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,com.velos.esch.business.common.*"%>

<%@ page import="com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject"%>



<% String src="";
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>


<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
  <%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession)){
		int pageRight = 7;
		int orgRight = 0;
		String studyId = (String) tSession.getValue("studyId");
		String userIdFromSession = (String) tSession.getValue("userId");
		//GET STUDY TEAM RIGHTS
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
		ArrayList tId = teamDao.getTeamIds();
		if (tId.size() == 0) {
			pageRight=0 ;
	    }else {
			stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

		 	ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();

				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();

    	if ((stdRights.getFtrRights().size()) == 0){
    	 	pageRight= 0;
    	}else{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    	}
    }
	String death = "";
	String meddracode="";
	String advDictionary="";
	String descripton="";
	String startDt ="";
	String stopDt = "";
	String treatment = "";
	String enteredBy="";
	String reportedBy="";


	String outcomeString="";
	String outcomeDt ="";
	String addInfoString="";
	String notes ="";
	String outcomeNotes="";
	String codelstId="";
	String outcomeActionM="";
	String severity = "";
	String sysaff = "";
	String drugdroc = "";
	String recoverydesc = "";
	String outactiondesc = "";
	String advNotifyDate = "";
	String advNotifyCodeId = "";
	String advNotifyString = "";
	String adveventId= request.getParameter("adveventId");
	String mode=request.getParameter("mode");
	char strIndex='0';
	String status="";
	String enteredByName="";
	ArrayList outcomeIds=null;
	ArrayList addInfoIds = null;
	ArrayList advNotifyCodeIds = null;
	String outcomeId="";
	String addInfoId = "";
	String protocolId = "";
	String statDesc=request.getParameter("statDesc");
	String statid=request.getParameter("statid");
	String accId = (String) tSession.getValue("accountId");
	String studyVer = request.getParameter("studyVer");
	String studyNum = request.getParameter("studyNum");
	String enrollId =(String) tSession.getValue("enrollId");
	String patientId = "";
	String patProtId=(String) request.getParameter("patProtId");
	String study = (String) tSession.getValue("studyId");
	String eventId=(String) request.getParameter("eventId");
	String pkey = request.getParameter("pkey");
	String eventName=request.getParameter("eventName");
	String visit=request.getParameter("visit");
	String patStatSubType = "";
	String patStudyStat =  "";
	String reportedByName = "";
	String discoveryDt = "";
	String loggedDt = "";
	String linkedTo = "";
	String linkedToId = "";
	String linkedToName = "";

	int patStudyStatpk = 0;
	patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
	patEnrollB.getPatProtDetails();
	protocolId =patEnrollB.getPatProtProtocolId();
	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	}

	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(studyId));

	studyB.getStudyDetails();
	studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	int fdaStudy = ("1".equals(studyB.getFdaRegulatedStudy()))? 1:0;

	String studyAdvlkpVer = "0";
	String studyAdvlkpVerNumber = "0";

	//get study lookup view for adverse events
	studyAdvlkpVer = studyB.getStudyAdvlkpVer();

		if (EJBUtil.isEmpty(studyAdvlkpVer))
		{
			studyAdvlkpVer  = "0";
		}
		else
		{
			studyAdvlkpVerNumber = studyB.getLkpTypeVersionForView(EJBUtil.stringToNum(studyAdvlkpVer) );
		}


		if (EJBUtil.isEmpty(studyAdvlkpVerNumber))
		{
			studyAdvlkpVerNumber  = "0";
		}


   //KM: added 05/10/06 May - June requirement PS1


	int index=-1;
    String studyLkpDict = studyB.getStudyAdvlkpVer();
    if (EJBUtil.isEmpty(studyLkpDict))
		studyLkpDict = "0";
   int studyLkpDictId=EJBUtil.stringToNum(studyLkpDict);
		LookupDao lookupDao = new LookupDao();
		lookupDao = studyB.getAllViewsForLkpType(EJBUtil.stringToNum(accId) ,"NCI");
		ArrayList viewIds = lookupDao.getLViewIds();
		ArrayList viewNames = lookupDao.getLViewNames();
		String settingValue="";

	 if (studyLkpDictId==0)
	 {
		settingValue="";
	 }
	 else
	 {
	   index=viewIds.indexOf(new Integer(studyLkpDict));
	   settingValue=(String)viewNames.get(index);
	  }

	  if (StringUtil.isEmpty(settingValue))
	  {
	  	settingValue = LC.L_Free_TextEntry;/*settingValue = "Free Text Entry";*****/
	  }
   //advDictionary=settingValue;

	int siteId = 0;
	String siteName = "";

	String advName = "";
	String grade = "";
	String gradeText = "";

	//boolean genSchedule = true;

	SchCodeDao cd = new SchCodeDao();

	//SchCodeDao cd_severe = new SchCodeDao();

	//SchCodeDao cd_sysaff = new SchCodeDao();

	SchCodeDao cd_drugproc = new SchCodeDao();
	SchCodeDao cd_recoverydesc = new SchCodeDao();

	SchCodeDao cd_actiondesc = new SchCodeDao(); //chn


	String dCur = "";



	String dCur_drugproc = "";
	String dCur_recoverydesc = "";
	String dFormStatus = "";
	String advFormStatus = "";
	String dCur_actiondesc= ""; //chn


	String disableStr ="";
	String readOnlyStr ="";

	//cd_severe.getCodeValues("adve_severity");
	//cd_sysaff.getCodeValues("adve_bdsystem");

	cd_drugproc.getCodeValues("adve_relation");
	cd_recoverydesc.getCodeValues("adve_recovery");
	cd_actiondesc.getCodeValues("outaction"); //chn



	ConfigFacade cFacade=ConfigFacade.getConfigFacade();
	HashMap hashPgCustFld = new HashMap();

	ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(accId), "adverseevent");

	if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
		for (int i=0;i<cdoPgField.getPcfField().size();i++){
			hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));
	    }
	}



	 String attrAtt = "";
	 String actAtt = "";
	 String recAtt = "";
	 String formStatAtt ="";
	 String typeAtt = "";

     if (hashPgCustFld.containsKey("attribution")) {
			int fldNumAttr = Integer.parseInt((String)hashPgCustFld.get("attribution"));
			attrAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAttr));
			if(attrAtt == null) attrAtt ="";
      }


	  if (hashPgCustFld.containsKey("action")) {
			int fldNumAct = Integer.parseInt((String)hashPgCustFld.get("action"));
			actAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAct));
			if(actAtt == null) actAtt ="";
      }

	  if (hashPgCustFld.containsKey("recoverydesc")) {
			int fldNumRec = Integer.parseInt((String)hashPgCustFld.get("recoverydesc"));
			recAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRec));
			if(recAtt == null) recAtt ="";
      }


	  if (hashPgCustFld.containsKey("formstatus")) {
			int fldNumFormStat = Integer.parseInt((String)hashPgCustFld.get("formstatus"));
			formStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFormStat));
			if(formStatAtt == null) formStatAtt ="";
      }


	  if (hashPgCustFld.containsKey("aetype")) {
			int fldNumType = Integer.parseInt((String)hashPgCustFld.get("aetype"));
			typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));
			if(typeAtt == null) typeAtt ="";
      }






	if(mode.equals("N"))
	{
		cd.getCodeValues("adve_type");

		//dCur =  cd.toPullDown("adve_type",0);

		if (typeAtt.equals("1"))
			dCur =  cd.toPullDown("adve_type",0,false);
		else
			dCur =  cd.toPullDown("adve_type",0);

		//reset cd
		cd.resetObject();

		cd.getCodeValues("fillformstat");

		if (formStatAtt.equals("1"))
		   dFormStatus =  cd.toPullDown("formStatus",0,false);
		else
			dFormStatus =  cd.toPullDown("formStatus",0);


		//dCur_severe =  cd_severe.toPullDown("adve_severity",0);

		//dCur_sysaff =  cd_sysaff.toPullDown("adve_bdsystem",0);

		//dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",0);

		if (attrAtt.equals("1"))
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",0,false);
		else
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",0);


		if (recAtt.equals("1"))
			dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",0,false);
		else
			dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",0);

		if (actAtt.equals("1"))
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0,false); //chn
		else
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0);


		String uName = (String) tSession.getValue("userName");
		enteredByName = uName;
		enteredBy=userIdFromSession;
		//reportedByName = uName;
		//reportedBy=userIdFromSession;

	}else	{

		//get current patient status and its subtype
		patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, pkey);
		patStudyStat = patB.getPatStudyStat();
		patStudyStatpk = patB.getId();
		statid = patStudyStat ;
		if (EJBUtil.isEmpty(patStatSubType))
			patStatSubType= "";

	///////////////////////////////////

		//dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0);

		if (actAtt.equals("1"))
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0,false);
		else
			dCur_actiondesc =  cd_actiondesc.toPullDown("outaction",0);


		adveventB.setAdvEveId(EJBUtil.stringToNum(adveventId));
		adveventB.getAdvEveDetails();

		codelstId=adveventB.getAdvEveCodelstAeTypeId();
		//severity=adveventB.getAdvEveSeverity();

		//sysaff=adveventB.getAdvEveBdsystemAff();

		drugdroc=adveventB.getAdvEveRelationship();

		recoverydesc=adveventB.getAdvEveRecoveryDesc();
		// Added by Gopu dated on 01/20/05
		outactiondesc = adveventB.getFkOutcomeAction();


//KM: added 05/10/06 May - June requirement PS1
		meddracode=adveventB.getAdvEveMedDRA();
		if(meddracode==null)
		{ meddracode="";}


		advDictionary=adveventB.getAdvEveDictionary();
		if (advDictionary==null)
		{ advDictionary="";}


		descripton=adveventB.getAdvEveDesc();
		if (descripton==null){ descripton=""; }

		treatment=adveventB.getAdvEveTreatment();
		if (treatment==null){ treatment=""; }


		startDt = adveventB.getAdvEveStDate();
		stopDt =  adveventB.getAdvEveEndDate();
	   	enteredBy= adveventB.getAdvEveEnterBy();
		reportedBy =adveventB.getAdvEveReportedBy();
		discoveryDt = adveventB.getAdvEveDiscvryDate(); ;
	        loggedDt = adveventB.getAdvEveLoggedDate();
		linkedToName = adveventB.getAdvEveLinkedTo();


		if(linkedToName == null) {
		  linkedToName = "";


		}


		outcomeString=adveventB.getAdvEveOutType();
		outcomeDt =adveventB.getAdvEveOutDate();

		outcomeNotes= adveventB.getAdvEveOutNotes();
		outcomeNotes = (   outcomeNotes  == null      )?"":(  outcomeNotes ) ;


		addInfoString=adveventB.getAdvEveAddInfo();

		notes=adveventB.getAdvEveNotes();
		notes = (   notes  == null      )?"":(  notes ) ;

		//get grade and adv event name

		grade = adveventB.getAdvEveGrade();
		// Added by Gopu dated on 01/20/05
		outcomeActionM = adveventB.getFkOutcomeAction();

		if (grade==null) grade="";

		advName = adveventB.getAdvEveName();
		if (advName==null) advName= "";

		if (grade.equals("-1") )
			grade = "";

		if ( ! EJBUtil.isEmpty(grade) )
		{
			gradeText =  LC.L_Grade+" : " +  grade + " " + advName ;/*gradeText =  "Grade : " +  grade + " " + advName ;*****/
		}
		else
		{
			gradeText =  advName;
		}

		advFormStatus = adveventB.getFormStatus();

		cd.getCodeValues("adve_type");

		//dCur =  cd.toPullDown("adve_type",EJBUtil.stringToNum(codelstId));
		if (typeAtt.equals("1"))
			dCur =  cd.toPullDown("adve_type",EJBUtil.stringToNum(codelstId),false);
		else
			dCur =  cd.toPullDown("adve_type",EJBUtil.stringToNum(codelstId));

		//reset cd
		cd.resetObject();

		cd.getCodeValues("fillformstat");

		if (formStatAtt.equals("1"))
		   dFormStatus =  cd.toPullDown("formStatus",EJBUtil.stringToNum(advFormStatus),false);
		else
		   dFormStatus =  cd.toPullDown("formStatus",EJBUtil.stringToNum(advFormStatus));



		//dCur_severe =  cd_severe.toPullDown("adve_severity",EJBUtil.stringToNum(severity));

		//dCur_sysaff =  cd_sysaff.toPullDown("adve_bdsystem",EJBUtil.stringToNum(sysaff));


		//dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",EJBUtil.stringToNum(drugdroc));

		if (attrAtt.equals("1"))
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",EJBUtil.stringToNum(drugdroc),false);
		else
			dCur_drugproc =  cd_drugproc.toPullDown("adve_relation",EJBUtil.stringToNum(drugdroc));


		if (recAtt.equals("1"))
 	 		dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",EJBUtil.stringToNum(recoverydesc),false);
		else
			dCur_recoverydesc =  cd_recoverydesc.toPullDown("adve_recovery",EJBUtil.stringToNum(recoverydesc));


		//dCur_actiondesc = cd_actiondesc.toPullDown("outaction", EJBUtil.stringToNum(outactiondesc));

		if (actAtt.equals("1"))
			dCur_actiondesc = cd_actiondesc.toPullDown("outaction", EJBUtil.stringToNum(outactiondesc),false);
		else
			dCur_actiondesc = cd_actiondesc.toPullDown("outaction", EJBUtil.stringToNum(outactiondesc));


		userB.setUserId(EJBUtil.stringToNum(enteredBy));
	        userB.getUserDetails();
	        enteredByName = userB.getUserFirstName() + " " + userB.getUserLastName();

		if(reportedBy != null){
		userB.setUserId(EJBUtil.stringToNum(reportedBy));
	        userB.getUserDetails();
		reportedByName = userB.getUserFirstName() + " " + userB.getUserLastName();
		}

		if (StringUtil.stringToNum(userB.getUserAccountId()) != StringUtil.stringToNum(accId)) {
%>
	<jsp:include page="accessdenied.jsp" flush="true"/>
   	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body>
</html>
<%
			return;
		}

	}
	String uName = (String) tSession.getValue("userName");
%>
<%
	int personPK = 0;
	personPK = EJBUtil.stringToNum(pkey);
	person.setPersonPKId(personPK);
	person.getPersonDetails();
	patientId = person.getPersonPId();
	siteId = EJBUtil.stringToNum(person.getPersonLocation());
	orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), personPK , EJBUtil.stringToNum(studyId) );
	if (orgRight > 0){
		System.out.println("patient adverse new orgRight" + orgRight);
		orgRight = 7;
	}
%>
<DIV class="BrowserTopn" id="div1">

	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="pkey" value="<%=pkey%>"/>
	<jsp:param name="patientCode" value="<%=patientId%>"/>
	<jsp:param name="patProtId" value="<%=patProtId%>"/>
	<jsp:param name="studyId" value="<%=study%>"/>
	<jsp:param name="studyVer" value="<%=studyVer%>"/>
	<jsp:param name="studyNum" value="<%=studyNum%>"/>
	</jsp:include>
</div>
<DIV class="tabFormBotN tabFormBotN_adventnew_1" id="div2">
<table width="100%" >
 <tr>
	<td class=tdDefault width = 20% >
	<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>: <a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=study%>"><%=studyNumber%></a> &nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(studyTitle)%>',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>');" onmouseout="return nd();"><%=LC.L_View_Title%><%--View Title*****--%></a>  &nbsp;&nbsp;&nbsp;&nbsp;
<%if(protocolId != null) {%>
 	<!--Protocol Calendar: <%=protName%>-->
 </tr>
 		<tr height="8"><td></td></tr>
	<%}%>
 </table>


 	 	<%
			//Modified by Manimaran for November Enhancement PS4.
			if((patStatSubType.trim().equalsIgnoreCase("lockdown")))
			{ %>
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_StdStatLkdwn_CntEdtAdvEvt%><%--<%=LC.Pat_Patient%>'s <%=LC.Std_Study_Lower%> status is 'Lockdown'.You cannot edit the Adverse Event.*****--%> </FONT>
				<A onclick="openWinStatus('<%=pkey%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=studyId%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A>
				 <FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>
				</P>

		<%	}
		 %>

	<Form name="advevent" id="advevtnew" method=post action="updateadvevent_new.jsp" onsubmit="if (validate(document.advevent)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

			<table width="80%" cellspacing="3" cellpadding="1" border="0">
			       <%
			       if(mode.equals("M")){
			       %>
			        <tr>

			    	<td class=tdDefault width="20%">
				        <%=LC.L_Response_Id%><%--Response ID*****--%>
					</td>
					<td>
					<input type="text"  size="4" name="Response ID"  value='<%=adveventId%>' readonly>
					</td>
				</tr>
				<%
				}
				%>



				<tr>
				 <%if (hashPgCustFld.containsKey("aetype")) {
						int fldNumType = Integer.parseInt((String)hashPgCustFld.get("aetype"));
						String typeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumType));
						String typeLable = ((String)cdoPgField.getPcfLabel().get(fldNumType));
						typeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));

						if(typeAtt == null) typeAtt ="";
						if(typeMand == null) typeMand ="";

						if(!typeAtt.equals("0")) {
						if(typeLable !=null){
						%> <td class=tdDefault width="20%">
						 <%=typeLable%> 
						<%} else {%> <td>
						  <%=LC.L_AdverseEvent_Type%><%--Adverse Event Type*****--%> 
						<%}

						if (typeMand.equals("1")) {
							%>
						   <FONT class="Mandatory" id="pgcustomtype">* </FONT>
						<% } %>

				</td>
				<td width="60%">

				<%=dCur%>

				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openWinMoreDetails('<%=adveventId%>','<%=mode%>','advtype','<%=pageRight%>');"><img src="../jsp/images/moreInfo.png" title="<%=LC.L_More_StdDets%>" border="0"></A>

				<%if(typeAtt.equals("2")) {%><input type="hidden" name="adve_type" value="<%=codelstId%>"> <%}%>
				</td>
				<%} else if(typeAtt.equals("0")) {%>

				<input type="hidden" name="adve_type" value="<%=codelstId%>">
				<%}} else {%>

					<td class=tdDefault width="20%">
				       <%=LC.L_AdverseEvent_Type%><%--Adverse Event Type*****--%> <FONT class="Mandatory" id ="mandae">* </FONT>
					</td>
					<td width="60%">	<%=dCur%>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openWinMoreDetails('<%=adveventId%>','<%=mode%>','advtype','<%=pageRight%>');"><img src="../jsp/images/moreInfo.png" title="<%=LC.L_More_StdDets%>" border="0"></A>
					</td>

				<%}%>
			  </tr>


				<!--<tr>
					<td class=tdDefault >
				       Severity
					</td>
					<td>	 </td>
				</tr> -->



			<tr>
				<%

				if (hashPgCustFld.containsKey("aegrading")) {

				int fldNumGrading = Integer.parseInt((String)hashPgCustFld.get("aegrading"));
				String gradingMand = ((String)cdoPgField.getPcfMandatory().get(fldNumGrading));
				String gradingLable = ((String)cdoPgField.getPcfLabel().get(fldNumGrading));
				String gradingAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumGrading));

				disableStr ="";
				readOnlyStr ="";

				if(gradingAtt == null) gradingAtt ="";
  				if(gradingMand == null) gradingMand ="";

				if(!gradingAtt.equals("0")) {

				if(gradingLable !=null){
				%>
				<td class=tdDefault >
				 <%=gradingLable%> 
				<%} else {%> <td class=tdDefault >
				  <%=LC.L_Adverse_EventOrGrading%><%--Adverse Event/Grading*****--%>
				<% }


			   if (gradingMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomaegrading">* </FONT>
		 	   <% }
			   %>

			   <%if(gradingAtt.equals("1")) {
			       disableStr = "disabled"; }
  		       %>
 			  </td>
			  <td>

			  <% if(!gradingAtt.equals("1")) { %>
			  <input type = "hidden" maxLength="30" size="15" name="grade" value= '<%=grade%>' >
			  <input type = "hidden" maxLength="200" size="15" name="advName" value= '<%=advName%>' >
			  <%}%>

			  <input maxLength="200" READONLY size="50" name="advGradeText" value = '<%=gradeText%>' <%=disableStr%> >

			  <%if(!gradingAtt.equals("1") && !gradingAtt.equals("2")) { %>
			  <A href="#" onClick="openwindowdict(<%=studyAdvlkpVer%>,<%=studyAdvlkpVerNumber%>,document.advevent);"><%=LC.L_Select_FromDict%><%--Select from Dictionary*****--%></A>
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openwindowcalc('<%=studyAdvlkpVerNumber%>',<%=pkey%>);"><%=LC.L_Calculate%><%--Calculate*****--%></A>
			  <%}%>

			  </td>

				<%} else { %>

			  <input type = "hidden" maxLength="30" size="15" name="grade" value= '<%=grade%>' >
			  <input type = "hidden" maxLength="200" size="15" name="advName" value= '<%=advName%>' >
			  <input type = "hidden" maxLength="200" READONLY size="50" name="advGradeText" value = '<%=gradeText%>' <%=disableStr%> >

				<%}}  else {%>

				   <td class=tdDefault >
				       <%=LC.L_Adverse_EventOrGrading%><%--Adverse Event/Grading*****--%>
					</td>
					<td class=tdDefault>
					<input maxLength="200" READONLY size="50" name="advGradeText" value = '<%=gradeText%>' >
					<input type = "hidden" maxLength="30" size="15" name="grade" value= '<%=grade%>' >
					<input type = "hidden" maxLength="200" size="15" name="advName" value= '<%=advName%>' >
					<A href="#" onClick="openwindowdict(<%=studyAdvlkpVer%>,<%=studyAdvlkpVerNumber%>,document.advevent);"><%=LC.L_Select_FromDict%><%--Select from Dictionary*****--%></A>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openwindowcalc('<%=studyAdvlkpVerNumber%>',<%=pkey%>);"><%=LC.L_Calculate%><%--Calculate*****--%></A></td>

			 <%}%>
			</tr>



	<tr>
		<%if (hashPgCustFld.containsKey("meddracode")) {

			int fldNumMeddra = Integer.parseInt((String)hashPgCustFld.get("meddracode"));
			String meddraMand = ((String)cdoPgField.getPcfMandatory().get(fldNumMeddra));
			String meddraLable = ((String)cdoPgField.getPcfLabel().get(fldNumMeddra));
			String meddraAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumMeddra));

			disableStr ="";
			readOnlyStr ="";
			if(meddraAtt == null) meddraAtt ="";
			if(meddraMand == null) meddraMand ="";


			if(!meddraAtt.equals("0")) {
			if(meddraLable !=null){
			%> <td class=tdDefault >
			&nbsp;&nbsp;&nbsp;&nbsp; <%=meddraLable%>
			<%} else {%> <td class=tdDefault>
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Meddra_code%><%--MedDRA code*****--%>
			<%}

			if (meddraMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustommeddra">* </FONT>
			<% }
		 %>

		  <%if(meddraAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (meddraAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	  </td>
	  <td class=tdDefault>
		<input type="text" maxLength="30"  size="50" name="MedDRAcode"  value='<%=meddracode%>' <%=disableStr%> <%=readOnlyStr%> >
	  </td>
    	<% } else { %>

	   <input type="hidden" maxLength="30"  size="50" name="MedDRAcode"  value='<%=meddracode%>' >

	  <% }}

	  else {
		 %>
		 <!--   //KM: added 05/10/06 May - June requirement PS1 -->
		<td class=tdDefault >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Meddra_code%><%--MedDRA code*****--%> </td>
		<td class=tdDefault>
		<input type="text" maxLength="30"  size="50" name="MedDRAcode"  value='<%=meddracode%>'>
		</td>

     <%}%>

	</tr>




<tr>
		<%if (hashPgCustFld.containsKey("dictionary")) {

			int fldNumDict = Integer.parseInt((String)hashPgCustFld.get("dictionary"));
			String dictMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDict));
			String dictLable = ((String)cdoPgField.getPcfLabel().get(fldNumDict));
			String dictAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDict));

			disableStr ="";
			readOnlyStr ="";
			if(dictAtt == null) dictAtt ="";
			if(dictMand == null) dictMand ="";


			if(!dictAtt.equals("0")) {
			if(dictLable !=null){
			%> <td class=tdDefault >
			&nbsp;&nbsp;&nbsp;&nbsp; <%=dictLable%>
			<%} else {%> <td class=tdDefault>
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Dictionary%><%--Dictionary*****--%>
			<%}

			if (dictMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdict">* </FONT>
			<% }
		 %>

		  <%if(dictAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (dictAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>

	  </td>
	  <td class=tdDefault>
		<input type="text" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' readonly <%=disableStr%> >

		<%
		if (!mode.equals("N")) {
			if (patStatSubType.equals("lockdown")) { pageRight=4;}
		}
		%>



	 </td>
    	<% } else { %>

	   <input type="hidden" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' >

	  <% }}

	  else {  %>

		<%
				if(mode.equals("N")) { %>


					<td class=tdDefault >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Dictionary%><%--Dictionary*****--%> </td>
					<td class=tdDefault>
					<input type="text" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' readonly>
					</td>

				<%} else { %>

					<td class=tdDefault >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Dictionary%><%--Dictionary*****--%> </td>
					<td class=tdDefault>
					<input type="text" maxLength="30"  size="50" name="advDictionary"  value='<%=advDictionary%>' readonly>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<%if (patStatSubType.equals("lockdown")) { pageRight=4;} %>
					</td>

				<% } %>


     <%}%>

	</tr>



	<tr>
        <%if (hashPgCustFld.containsKey("description")) {
			int fldNumDesc = Integer.parseInt((String)hashPgCustFld.get("description"));
			String descMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDesc));
			String descLable = ((String)cdoPgField.getPcfLabel().get(fldNumDesc));
			String descAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDesc));

			disableStr ="";
			readOnlyStr ="";
			if(descAtt == null) descAtt ="";
			if(descMand == null) descMand ="";


			if(!descAtt.equals("0")) {
			if(descLable !=null){
			%><td class=tdDefault> &nbsp;&nbsp;&nbsp;&nbsp;
			<%=descLable%>
			<%} else {%> <td>
			 &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Description%><%--Description*****--%>
			<%}

			if (descMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdesc">* </FONT>
			<% }
		 %>

		  <%if(descAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (descAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



		</td>
		<td class=tdDefault>
			<TEXTAREA id=descripton name=descripton  rows=3 cols=38  <%=disableStr%>  <%=readOnlyStr%> ><%=descripton%></TEXTAREA>
		</td>
			<% } else { %>

		 <TEXTAREA id=descripton name=descripton  Style = "visibility:hidden"  rows=3 cols=38> <%=descripton%></TEXTAREA>

		 <% }} else {
			 %>
		<td class=tdDefault >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Description%><%--Description*****--%> </td>
		<td class=tdDefault>
				<TEXTAREA id=descripton name=descripton  rows=3 cols=38><%=descripton%></TEXTAREA>

		</td>
	  <%}%>
  	  </tr>



<tr>
		<%
			String courseAtt ="";
			if (hashPgCustFld.containsKey("treatmentcourse")) {

			int fldNumCourse = Integer.parseInt((String)hashPgCustFld.get("treatmentcourse"));
			String courseMand = ((String)cdoPgField.getPcfMandatory().get(fldNumCourse));
			String courseLable = ((String)cdoPgField.getPcfLabel().get(fldNumCourse));
			courseAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumCourse));

			disableStr ="";
			readOnlyStr ="";
			if(courseAtt == null) courseAtt ="";
			if(courseMand == null) courseMand ="";

			if(!courseAtt.equals("0")) {
			if(courseLable !=null){
			%> <td class=tdDefault >
			&nbsp;&nbsp;&nbsp;&nbsp; <%=courseLable%>
			<%} else {%> <td class=tdDefault>
			&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Treatment_Course%><%--Treatment Course*****--%>
			<%}

			if (courseMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomcourse">* </FONT>
			<% }
		 %>

		  <%if(courseAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (courseAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	  </td>
	  <td class=tdDefault>
		<input type="text" name="treatment" maxlength="150" size="50" value="<%=treatment%>" <%=disableStr%> <%=readOnlyStr%>>
	  </td>
    	<% } else { %>

	  <input type="hidden" name="treatment" maxlength="150" size="50" value="<%=treatment%>">

	  <% }}

	  else {
		 %>
		<td class=tdDefault >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Treatment_Course%><%--Treatment Course*****--%></td>
		<td class=tdDefault>
			<input type="text" name="treatment" maxlength="150" size="50" value="<%=treatment%>">
		</td>

      <%}%>

	  </tr>

	   <% if(!courseAtt.equals("0")) { %>
	  <tr>
	  <td></td>
		<td id="tdComments">(<%=MC.M_CycleNum_CourseId%><%--e.g. cycle number, course ID etc.*****--%>)</td>
	  </tr>
	  <%}%>

		<tr>
		<%

				if (hashPgCustFld.containsKey("startdate")) {

				int fldNumStartDt = Integer.parseInt((String)hashPgCustFld.get("startdate"));
				String startDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStartDt));
				String startDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumStartDt));
				String startDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStartDt));

				disableStr ="";
				readOnlyStr ="";

				if(startDtAtt == null) startDtAtt ="";
  				if(startDtMand == null) startDtMand ="";

				if(!startDtAtt.equals("0")) {

				if(startDtLable !=null){
				%>
				<td class=tdDefault>
				 <%=startDtLable%>  
				<%} else {%> <td>
				   <%=LC.L_Start_Date%><%--Start Date*****--%> 
				<% }


			   if (startDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstdt">* </FONT>
		 	   <% }
			   %>

			   <%if(startDtAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (startDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>


 			   </td>
			  <td class=tdDefault>

			 
		<%-- INF-20084 Datepicker-- AGodara --%>
			  <%if (!startDtAtt.equals("1")) { %>
			 		<INPUT type=text name=startDt class="datefield" size=10 <%=readOnlyStr%> <%=disableStr%>  value=<%=startDt%>  >
			   <%}%>
			 </td>
			<%}else{ %>
					<INPUT type="hidden" name=startDt size=10  value=<%=startDt%> >
		<%}}else {%>
					<td class=tdDefault><%=LC.L_Start_Date%><%--Start Date*****--%> <FONT class="Mandatory" id="mandstdate">* </FONT></td>
					<td class=tdDefault><INPUT type=text name=startDt class="datefield" size=10  value=<%=startDt%>></td>
		<%}%>
			</tr>
		<tr>

		 <%
				if (hashPgCustFld.containsKey("stopdate")) {

				int fldNumStopDt = Integer.parseInt((String)hashPgCustFld.get("stopdate"));
				String stopDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStopDt));
				String stopDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumStopDt));
				String stopDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStopDt));

				disableStr ="";
				readOnlyStr ="";

				if(stopDtAtt == null) stopDtAtt ="";
  				if(stopDtMand == null) stopDtMand ="";

				if(!stopDtAtt.equals("0")) {

				if(stopDtLable !=null){
				%>
				<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;
				 <%=stopDtLable%>
				<%} else {%> <td>
				  &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Stop_Date%><%--Stop Date*****--%> 
				<% }


			   if (stopDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstopdt">* </FONT>
		 	   <% }
			   %>

			   <%if(stopDtAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (stopDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>


 			   </td>
			  <td class=tdDefault>

			 
<%-- INF-20084 Datepicker-- AGodara --%>
			 <%if(!stopDtAtt.equals("1")) { %>
	     			<INPUT type=text name=stopDt class="datefield" size=10 <%=readOnlyStr%> <%=disableStr%> value=<%=stopDt%>  >
			  <%}%>
					</td>
			<%}else{ %>
					<INPUT type="hidden" name=stopDt size=10  value=<%=stopDt%> >
		<%}}else{%>
					<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Stop_Date%><%--Stop Date*****--%></td>
					<td class=tdDefault><INPUT type=text name=stopDt class="datefield" size=10  value=<%=stopDt%>></td>
		<%}%>
			</tr>


			<tr>

		 <%
				if (hashPgCustFld.containsKey("discoverydate")) {

				int fldNumDiscDt = Integer.parseInt((String)hashPgCustFld.get("discoverydate"));
				String discDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDiscDt));
				String discDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumDiscDt));
				String discDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDiscDt));

				disableStr ="";
				readOnlyStr ="";

				if(discDtAtt == null) discDtAtt ="";
  				if(discDtMand == null) discDtMand ="";

				if(!discDtAtt.equals("0")) {

				if(discDtLable !=null){
				%>
				<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;
				 <%=discDtLable%>
				<%} else {%> <td>
				  &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_DiscoveryDate%><%--AE Discovery Date*****--%> 
				<% }


			   if (discDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdiscdt">* </FONT>
		 	   <% }
			   %>

			   <%if(discDtAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (discDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td class=tdDefault>
<%-- INF-20084 Datepicker-- AGodara --%>
			 <%if(!discDtAtt.equals("1")) { %>
				<INPUT type=text name=aediscoveryDt class="datefield" size=10 <%=readOnlyStr%> <%=disableStr%> value=<%=discoveryDt%> >
			 <%}%>
			  	</td>
			<%}else{ %>
				<INPUT type="hidden" name=aediscoveryDt size=10  value=<%=discoveryDt%> >
		<%}}else{%>
				<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_DiscoveryDate%><%--AE Discovery Date*****--%>  </FONT></td>
				<td class=tdDefault><INPUT type=text name=aediscoveryDt class="datefield" size=10  value=<%=discoveryDt%>></td>
		<%}%>
			</tr>
			<tr>

		 <%
				if (hashPgCustFld.containsKey("loggeddate")) {

				int fldNumLogDt = Integer.parseInt((String)hashPgCustFld.get("loggeddate"));
				String logDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLogDt));
				String logDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumLogDt));
				String logDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLogDt));

				disableStr ="";
				readOnlyStr ="";

				if(logDtAtt == null) logDtAtt ="";
  				if(logDtMand == null) logDtMand ="";

				if(!logDtAtt.equals("0")) {

				if(logDtLable !=null){
				%>
				<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;
				 <%=logDtLable%>
				<%} else {%> <td>
				  &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_LoggedDate%><%--AE Logged Date*****--%> 
				<% }


			   if (logDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomlogdt">* </FONT>
		 	   <% }
			   %>

			   <%if(logDtAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (logDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td class=tdDefault>

			 
<%-- INF-20084 Datepicker-- AGodara --%>
			 <%if(!logDtAtt.equals("1")) { %>
				 <INPUT type=text name=aeloggedDt class="datefield" size=10 <%=readOnlyStr%> <%=disableStr%> value=<%=loggedDt%> >
			 <%}%>
			  </td>
			<%}else{%>
				 <INPUT type="hidden" name=aeloggedDt size=10  value=<%=loggedDt%>>
		<%}}else{%>
				<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Ae_LoggedDate%><%--AE Logged Date*****--%>  </FONT></td>
				<td class=tdDefault><INPUT type=text name=aeloggedDt class="datefield" size=10  value=<%=loggedDt%>></td>
		<%}%>
			</tr>
			<tr>
		 <%
				if (hashPgCustFld.containsKey("enteredby")) {

				int fldNumEntby = Integer.parseInt((String)hashPgCustFld.get("enteredby"));
				String entMand = ((String)cdoPgField.getPcfMandatory().get(fldNumEntby));
				String entLable = ((String)cdoPgField.getPcfLabel().get(fldNumEntby));
				String entAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumEntby));

				disableStr ="";
				readOnlyStr ="";
				%>
					<input name="death" type=hidden value=<%=death%>>
					<input name="eventId" type=hidden value=<%=eventId %>>
					<input name="pkey" type=hidden value=<%=pkey%>>
					<input name="eventName" type=hidden value="<%=eventName%>">
					<input name="visit" type=hidden value=<%=visit%>>
					<input name="srcmenu" type=hidden value=<%=src%>>
					<input name="adveventId" type=hidden value=<%=adveventId%>>
					<input id="mode" name="mode" type=hidden value=<%=mode%>>
					<input id="advMode" name="advMode" type=hidden value=<%=mode%>>
					<input type="hidden" name=studyId value=<%=studyId%>>
					<input type="hidden" name=statDesc value=<%=statDesc%>>
					<input type="hidden" name=statid value=<%=statid%>>
					<input type="hidden" name=patProtId value=<%=patProtId%>>
					<input type=hidden name=studyVer value=<%=studyVer%>>
					<input type=hidden name=studyNum value=<%=studyNum%>>
					<input type=hidden name=patientCode value=<%=patientId%>>
				<%
				if(entAtt == null) entAtt ="";
  				if(entMand == null) entMand ="";

				if(!entAtt.equals("0")) {

				if(entLable !=null){
				%>
				<td>
				 <%=entLable%> 
				<%} else {%> <td>
				<%=LC.L_Entered_By%><%--Entered By*****--%> 
				<% }


			   if (entMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustoment">* </FONT>
		 	   <% }
			   %>

			   <%if(entAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (entAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td>


			 <input type=hidden name=enteredBy <%=disableStr%> value=<%=enteredBy%> >
			 <input type=text name=enteredByName readonly value="<%=enteredByName%>" <%=disableStr%>>


			 <%if(!entAtt.equals("1") && !entAtt.equals("2")) { %>
			 <A HREF=# onClick=openwin1("entrBy") ><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>

			  </td>

			<%} else { %>

			<input type=hidden name=enteredBy value=<%=enteredBy%>>
			<input type=hidden name=enteredByName readonly value="<%=enteredByName%>">

			<% }}  else {%>
			<td><%=LC.L_Entered_By%><%--Entered By*****--%> <FONT class="Mandatory" id="mandent">* </FONT>
					</td>
					<td>
					<input name="death" type=hidden value=<%=death%>>
					<input name="eventId" type=hidden value=<%=eventId %>>
					<input name="pkey" type=hidden value=<%=pkey%>>
					<input name="eventName" type=hidden value="<%=eventName%>">
					<input name="visit" type=hidden value=<%=visit%>>
					<input name="srcmenu" type=hidden value=<%=src%>>
					<input name="adveventId" type=hidden value=<%=adveventId%>>
					<input id="mode" name="mode" type=hidden value=<%=mode%>>
					<input id="advMode" name="advMode" type=hidden value=<%=mode%>>
					<input type=hidden name=enteredBy value=<%=enteredBy%>>
					<input type="hidden" name=studyId value=<%=studyId%>>
					<input type="hidden" name=statDesc value=<%=statDesc%>>
					<input type="hidden" name=statid value=<%=statid%>>
					<input type="hidden" name=patProtId value=<%=patProtId%>>
					<input type=hidden name=studyVer value=<%=studyVer%>>
					<input type=hidden name=studyNum value=<%=studyNum%>>
					<input type=hidden name=patientCode value=<%=patientId%>>
					<input type=text name=enteredByName readonly value="<%=enteredByName%>">
					<A HREF=# onClick=openwin1("entrBy") ><%=LC.L_Select_User%><%--Select User*****--%></A>
				</td>

			 <%}%>
			</tr>


		 <tr>
		 <%
				if (hashPgCustFld.containsKey("reportedby")) {

				int fldNumRepby = Integer.parseInt((String)hashPgCustFld.get("reportedby"));
				String repMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRepby));
				String repLable = ((String)cdoPgField.getPcfLabel().get(fldNumRepby));
				String repAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRepby));

				disableStr ="";
				readOnlyStr ="";
				%>

				<input name="death"   type=hidden value=<%=death%>>
				<input name="eventId" type=hidden value=<%=eventId %>>
				<input name="pkey"    type=hidden value=<%=pkey%>>
				<input name="eventName" type=hidden value="<%=eventName%>">
				<input name="visit" type=hidden value=<%=visit%>>
				<input name="srcmenu" type=hidden value=<%=src%>>
				<input name="adveventId" type=hidden value=<%=adveventId%>>
				<input id="mode" name="mode" type=hidden value=<%=mode%>>
				<input id="advMode" name="advMode" type=hidden value=<%=mode%>>
				<input type="hidden" name=studyId value=<%=studyId%>>
				<input type="hidden" name=statDesc value=<%=statDesc%>>
				<input type="hidden" name=statid value=<%=statid%>>
				<input type="hidden" name=patProtId value=<%=patProtId%>>
				<input type= hidden   name=studyVer value=<%=studyVer%>>
				<input type= hidden   name=studyNum value=<%=studyNum%>>
				<input type= hidden   name=patientCode value=<%=patientId%>>




				<%
				if(repAtt == null) repAtt ="";
  				if(repMand == null) repMand ="";

				if(!repAtt.equals("0")) {

				if(repLable !=null){
				%>
				<td>
				&nbsp;&nbsp;&nbsp;&nbsp; <%=repLable%>
				<%} else {%> <td>
				&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Rpted_By%><%--Reported By*****--%>
				<% }


			   if (repMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomrep">* </FONT>
		 	   <% }
			   %>

			   <%if(repAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (repAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td>

			 <input type=hidden name=reportedBy <%=disableStr%> value=<%=reportedBy%>>
			 <input type= text name=reportedByName readonly value="<%=reportedByName%>" <%=disableStr%>>

			 <%if(!repAtt.equals("1") && !repAtt.equals("2")) { %>
			 <A HREF=# onClick=openwin1("repBy") ><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>

			  </td>

			<%} else { %>

			<input type=hidden name=reportedBy value=<%=reportedBy%>>
			<input type= hidden name=reportedByName readonly value="<%=reportedByName%> ">
			<% }}  else {%>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Rpted_By%><%--Reported By*****--%>
					</td>
					<td>
					<input name="death"   type=hidden value=<%=death%>>
					<input name="eventId" type=hidden value=<%=eventId %>>
					<input name="pkey"    type=hidden value=<%=pkey%>>
					<input name="eventName" type=hidden value="<%=eventName%>">
			        <input name="visit" type=hidden value=<%=visit%>>
			        <input name="srcmenu" type=hidden value=<%=src%>>
			        <input name="adveventId" type=hidden value=<%=adveventId%>>
			        <input id="mode" name="mode" type=hidden value=<%=mode%>>

					<input type=hidden name=reportedBy value=<%=reportedBy%>>
					<input type="hidden" name=studyId value=<%=studyId%>>
					<input type="hidden" name=statDesc value=<%=statDesc%>>
					<input type="hidden" name=statid value=<%=statid%>>
					<input type="hidden" name=patProtId value=<%=patProtId%>>
					<input type= hidden   name=studyVer value=<%=studyVer%>>
					<input type= hidden   name=studyNum value=<%=studyNum%>>
					<input type= hidden   name=patientCode value=<%=patientId%>>
					<input type= text name=reportedByName readonly value="<%=reportedByName%>">
						<A HREF=# onClick=openwin1("repBy") ><%=LC.L_Select_User%><%--Select User*****--%></A>
					</td>
			 <%}%>
			</tr>



	 <tr>
	 <%if (hashPgCustFld.containsKey("attribution")) {
			int fldNumAttr = Integer.parseInt((String)hashPgCustFld.get("attribution"));
			String attrMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAttr));
			String attrLable = ((String)cdoPgField.getPcfLabel().get(fldNumAttr));
			attrAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAttr));

			if(attrAtt == null) attrAtt ="";
			if(attrMand == null) attrMand ="";


			if(!attrAtt.equals("0")) {
			if(attrLable !=null){
			%> <td class=tdDefault >
			 <%=attrLable%> 
			<%} else {%> <td>
			 <%=LC.L_Attribution%><%--Attribution*****--%>
			<%}

			if (attrMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomattr">* </FONT>
			<% } %>

    </td>
    <td>

	 <%=dCur_drugproc%>  <%if(attrAtt.equals("2")) {%><input type="hidden" name="adve_relation" value="<%=drugdroc%>"> <%}%>
    </td>
	<%} else if(attrAtt.equals("0")) {%>

	<input type="hidden" name="adve_relation" value ="<%=drugdroc%>" >
	<%}} else {%>
    <td class=tdDefault >
	<%=LC.L_Attribution%><%--Attribution*****--%>   <!-- Relationship to the study drug and/or procedure -->
	</td>
	<td>	<%=dCur_drugproc%> </td>
    <%}%>
  </tr>



		 <%
			if(linkedToName == null){
			  linkedToName = "";
			}
		 %>


		<tr>

		 <%
				if (hashPgCustFld.containsKey("linkedto")) {

				int fldNumLink = Integer.parseInt((String)hashPgCustFld.get("linkedto"));
				String linkMand = ((String)cdoPgField.getPcfMandatory().get(fldNumLink));
				String linkLable = ((String)cdoPgField.getPcfLabel().get(fldNumLink));
				String linkAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumLink));

				disableStr ="";
				readOnlyStr ="";

				if(linkAtt == null) linkAtt ="";
  				if(linkMand == null) linkMand ="";

				if(!linkAtt.equals("0")) {

				if(linkLable !=null){
				%>
				<td>
				 <%=linkLable%>
				<%} else {%> <td>
				  <%=LC.L_Linked_To%><%--Linked To*****--%>
				<% }


			   if (linkMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomlink">* </FONT>
		 	   <% }
			   %>

			   <%if(linkAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (linkAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>

 			   </td>
			  <td class=tdDefault>

			 <input type= hidden name=linkedToId readonly value="<%=linkedToId%>">
			 <input type= text name=linkedToName readonly value="<%=linkedToName%>" <%=readOnlyStr%> <%=disableStr%> >


			 <%if(!linkAtt.equals("1") && !linkAtt.equals("2")) { %>
			 <A HREF=# onClick=openAdverseEvent(<%=studyId%>,<%=personPK%>,'<%=src%>') ><%=LC.L_Select_AdvEvts%><%--Select Adverse Events*****--%></A>
			  <%}%>

			  </td>

			<%} else { %>

			 <input type= hidden name=linkedToName readonly value="<%=linkedToName%>">
			<% }}  else {%>
			<td><%=LC.L_Linked_To%><%--Linked To*****--%>
			</td>
			<td>

			<input type= hidden name=linkedToId readonly value="<%=linkedToId%>">
			<input type= text name=linkedToName readonly value="<%=linkedToName%>">
			<A HREF=# onClick=openAdverseEvent(<%=studyId%>,<%=personPK%>,'<%=src%>') ><%=LC.L_Select_AdvEvts%><%--Select Adverse Events*****--%></A>
			</td>
			 <%}%>
			</tr>

			</table>












			<table width="80%" cellspacing="0" cellpadding="0" >
				<tr>
					<td COLSPAN=2><br>
						<p class = "sectionHeadings" > <%=LC.L_Outcome_Info%><%--Outcome Information*****--%></p><br>
					</td>
					<td>&nbsp;</td>
				</tr>


				<!-- startkm -->


				<%SchCodeDao outcome = new SchCodeDao();
				outcome.getCodeValues("outcome");
				ArrayList codelstDescs=outcome.getCDesc();
				outcomeIds=outcome.getCId();
				ArrayList outcomeActions=outcome.getCDesc();
				String outcomeAction="";

				String codelstdesc="";
				int length=codelstDescs.size();

				ArrayList advInfoOutIds =null;
				String advInfoOutId =null;
				if(mode.equals("M")){
				EventInfoDao eventInfoDao = new EventInfoDao();
				eventInfoDao = eventdefB.getAdvInfo(EJBUtil.stringToNum(adveventId),"O") ;
				advInfoOutIds =eventInfoDao.getAdvInfoIds();

				}

				%>
			   <input  type="hidden" name="outcomeString">

				<%

				if (hashPgCustFld.containsKey("outcometype")) {

					int fldNumOutType = Integer.parseInt((String)hashPgCustFld.get("outcometype"));
					String outTypeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOutType));
					String outTypeLable = ((String)cdoPgField.getPcfLabel().get(fldNumOutType));
					String outTypeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOutType));

					disableStr ="";
					readOnlyStr ="";
					if(outTypeAtt == null) outTypeAtt ="";
					if(outTypeMand == null) outTypeMand ="";

					if(!outTypeAtt.equals("0")) {


					 for (int i=0;i<length; i++) {
						codelstdesc=(String)  codelstDescs.get(i);
						outcomeId=(String) outcomeIds.get(i).toString();
						if(mode.equals("M")){

						if ( i < advInfoOutIds.size())
						{
							advInfoOutId=(String) advInfoOutIds.get(i).toString();
							outcomeAction=(String) outcomeActions.get(i).toString();

							strIndex=(null == outcomeString)?0:outcomeString.charAt(i);
						}
						else
						{
							advInfoOutId= "";
							strIndex = '0';
						}

						if(strIndex=='1')
							{status="Checked=true" ;}
						else{status="" ;}
					}%>

					<tr>
						<td width="25%">
					<% if (i==0) {

									if(outTypeLable !=null) { %>
									   <%=outTypeLable%>
								<%	}
									else {
										out.println(LC.L_Outcome_Type/*"Outcome Type"*****/); }


							if (outTypeMand.equals("1")) {
					%>

					   <FONT class="Mandatory" id="pgcustomouttype">* </FONT>

					<% }

					}

					%>

					 <%if(outTypeAtt.equals("1")) {
						 disableStr = "disabled"; }
						 else if (outTypeAtt.equals("2") ) {
						 readOnlyStr = "disabled"; }
					 %>


						</td>
						<td>
						   <input type="checkbox"  <%=disableStr%>  name="outcomeType"    <%=readOnlyStr%> value=<%=outcomeId%>  <%=status%> onclick="checkoutcome(document.advevent,<%= i %>,<%=length%>)"> <%=codelstdesc%>
							<% if (i==0) {%>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=LC.L_Date%><%--Date*****--%> &nbsp;
<%-- INF-20084 Datepicker-- AGodara --%>
								<%if( (!outTypeAtt.equals("1")) && (!outTypeAtt.equals("2")) ) { %>
									<INPUT type=text name=outcomeDt class="datefield" <%=disableStr%> readonly size=10  value=<%=outcomeDt%>>
								<%}else { %>
									<INPUT type=text name=outcomeDt <%=disableStr%>  readonly size=10  value=<%=outcomeDt%>>
								<%}
							}%>
						</td>
					</tr>
					<input type="hidden" name=outcomeLen value=<%=length%>>
					<input type="hidden" name=outcomeId value=<%=outcomeId%>>
					<input type="hidden" name=advInfoOutId value=<%=advInfoOutId%>>
				<%}%>

				<% } else { %>


				<% for (int i=0;i<length; i++) {
					codelstdesc=(String)  codelstDescs.get(i);
					outcomeId=(String) outcomeIds.get(i).toString();
					if(mode.equals("M")){

						if ( i < advInfoOutIds.size())
						{
							advInfoOutId=(String) advInfoOutIds.get(i).toString();
							outcomeAction=(String) outcomeActions.get(i).toString();

							strIndex=(null == outcomeString)?0:outcomeString.charAt(i);
						}
						else
						{
							advInfoOutId= "";
							strIndex = '0';
						}

						if(strIndex=='1')
							{status="Checked=true" ;}
						else{status="" ;}
					}%>

						   <input type="checkbox" name="outcomeType"  Style = "visibility:hidden" value=<%=outcomeId%>  <%=status%> onclick="checkoutcome(document.advevent,<%= i %>,<%=length%>)">
							<% if (i==0) {%>
								 <INPUT type=hidden name=outcomeDt  size=10  value=<%=outcomeDt%>>

							<%}%>

					<input type="hidden" name=outcomeLen value=<%=length%>>
					<input type="hidden" name=outcomeId value=<%=outcomeId%>>
					<input type="hidden" name=advInfoOutId value=<%=advInfoOutId%>>
				<%}%>


				<% } } else { %>


				<% for (int i=0;i<length; i++) {
					codelstdesc=(String)  codelstDescs.get(i);
					outcomeId=(String) outcomeIds.get(i).toString();
					if(mode.equals("M")){

						if ( i < advInfoOutIds.size())
						{
							advInfoOutId=(String) advInfoOutIds.get(i).toString();
							outcomeAction=(String) outcomeActions.get(i).toString();

							strIndex=(null == outcomeString)?0:outcomeString.charAt(i);
						}
						else
						{
							advInfoOutId= "";
							strIndex = '0';
						}

						if(strIndex=='1')
							{status="Checked=true" ;}
						else{status="" ;}
					}%>

					<tr>
						<td width="25%">
							<% if (i==0) {
								out.println(LC.L_Outcome_Type/*"Outcome Type"*****/);
							}
							%>
						</td>
						<td>
						   <input type="checkbox" name="outcomeType" value=<%=outcomeId%>  <%=status%> onclick="checkoutcome(document.advevent,<%= i %>,<%=length%>)"> <%=codelstdesc%>
<%-- INF-20084 Datepicker-- AGodara --%>							
							<% if (i==0) {%>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%=LC.L_Date%><%--Date*****--%> &nbsp; <INPUT type=text name=outcomeDt class="datefield" size=10  value=<%=outcomeDt%>>
							<%}%>
						</td>
					</tr>
					<input type="hidden" name=outcomeLen value=<%=length%>>
					<input type="hidden" name=outcomeId value=<%=outcomeId%>>
					<input type="hidden" name=advInfoOutId value=<%=advInfoOutId%>>
				<%} }%>



<!-- endkm -->


				<tr>
					<td> &nbsp;
					</td>
					<td> &nbsp;
					</td>
				</tr>


				<tr>
				 <%if (hashPgCustFld.containsKey("action")) {
						int fldNumAct = Integer.parseInt((String)hashPgCustFld.get("action"));
						String actMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAct));
						String actLable = ((String)cdoPgField.getPcfLabel().get(fldNumAct));
						actAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAct));

						if(actAtt == null) actAtt ="";
						if(actMand == null) actMand ="";

						if(!actAtt.equals("0")) {
						if(actLable !=null){
						%> <td class=tdDefault >
						<%=actLable%>  <br>
						<%} else {%> <td>
						 <%=LC.L_Action%><%--Action*****--%><br>
						<%}

						if (actMand.equals("1")) {
							%>
						   <FONT class="Mandatory" id="pgcustomaction">* </FONT>
						<% } %>

				</td>
				<td>

				<%=dCur_actiondesc%>  <%if(actAtt.equals("2")) {%> <input type="hidden" name="outaction" value="<%=outactiondesc%>"> <%}%>
				</td>
				<%} else if(actAtt.equals("0")) {%>

				<input type="hidden" name="outaction" value ="<%=outactiondesc%>" >
				<%}} else {%>
				<td class="tdDefault"><%=LC.L_Action%><%--Action*****--%><br>
				</td>
				<td><%=dCur_actiondesc%></td>
				<%}%>
			  </tr>


				<br>


			<tr>
				 <%if (hashPgCustFld.containsKey("recoverydesc")) {
						int fldNumRecover = Integer.parseInt((String)hashPgCustFld.get("recoverydesc"));
						String recMand = ((String)cdoPgField.getPcfMandatory().get(fldNumRecover));
						String recLable = ((String)cdoPgField.getPcfLabel().get(fldNumRecover));
						recAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumRecover));

						if(recAtt == null) recAtt ="";
						if(recMand == null) recMand ="";

						if(!recAtt.equals("0")) {
						if(recLable !=null){
						%> <td>
						<%=recLable%>
						<%} else {%> <td>
						   <%=LC.L_Recovery_Description%><%--Recovery Description*****--%>
						<%}

						if (recMand.equals("1")) {
							%>
						   <FONT class="Mandatory" id="pgcustomrec">* </FONT>
						<% } %>

				</td>
				<td>

				<%=dCur_recoverydesc%>  <%if(recAtt.equals("2")) {%><input type="hidden" name="adve_recovery" value="<%=recoverydesc%>"> <%}%>
				</td>
				<%} else if(recAtt.equals("0")) {%>

				<input type="hidden" name="adve_recovery" value ="<%=recoverydesc%>" >
				<%}} else {%>
				<td>
				     <%=LC.L_Recovery_Description%><%--Recovery Description*****--%>
				</td>
				<td>	<%=dCur_recoverydesc%> </td>
				<%}%>
			  </tr>

			  <br>



		<tr>
		<%if (hashPgCustFld.containsKey("outcomenotes")) {

			int fldNumOutcome = Integer.parseInt((String)hashPgCustFld.get("outcomenotes"));
			String outcomeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOutcome));
			String outcomeLable = ((String)cdoPgField.getPcfLabel().get(fldNumOutcome));
			String outcomeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOutcome));

			disableStr ="";
			readOnlyStr ="";
			if(outcomeAtt == null) outcomeAtt ="";
			if(outcomeMand == null) outcomeMand ="";

			if(!outcomeAtt.equals("0")) {
			if(outcomeLable !=null){
			%> <td>
			 <%=outcomeLable%>
			<%} else {%> <td>
			<%=LC.L_Outcome_Notes%><%--Outcome Notes*****--%>
			<%}

			if (outcomeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomoutcome">* </FONT>
			<% }
		 %>

		  <%if(outcomeAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (outcomeAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



	  </td>
	  <td>
	   <INPUT type=text name="outcomeNotes" size=30 value="<%=outcomeNotes%>" <%=disableStr%> <%=readOnlyStr%> >
	  </td>
    	<% } else { %>

	  <INPUT type="hidden" name="outcomeNotes" size=30 value="<%=outcomeNotes%>">

	  <% }}

	  else {
		 %>
		<td>
		<%=LC.L_Outcome_Notes%><%--Outcome Notes*****--%>
		</td>
		<td><INPUT type=text name="outcomeNotes" size=30 value="<%=outcomeNotes%>">
		</td>

      <%}%>

	  </tr>
	</table>





		<table width=80% cellspacing="0" cellpadding="0" >

		<%if (hashPgCustFld.containsKey("additionalinfo")) {


			SchCodeDao addInfo= new SchCodeDao();
			addInfo.getCodeValues("adve_info");
			codelstDescs=addInfo.getCDesc();
			addInfoIds=addInfo.getCId();
			codelstdesc="";
			length=codelstDescs.size();
			ArrayList advInfoAddIds =null;
			String advInfoAddId =null;
			if(mode.equals("M")){
			EventInfoDao eventInfoDao = new EventInfoDao();
			eventInfoDao = eventdefB.getAdvInfo(EJBUtil.stringToNum(adveventId),"A") ;
			advInfoAddIds =eventInfoDao.getAdvInfoIds();
			}

			int fldNumaddl = Integer.parseInt((String)hashPgCustFld.get("additionalinfo"));
			String addlMand = ((String)cdoPgField.getPcfMandatory().get(fldNumaddl));
			String addlLable = ((String)cdoPgField.getPcfLabel().get(fldNumaddl));
			String addlAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumaddl));

			disableStr ="";
			readOnlyStr ="";
			if(addlAtt == null) addlAtt ="";
			if(addlMand == null) addlMand ="";

			if(!addlAtt.equals("0")) {
			if(addlLable !=null){
			%> <tr>
			<td COLSPAN=2> <br>
			<p class = "sectionHeadings" >
			 <%=addlLable%>  <% if (addlMand.equals("1")) { %>
			   <FONT class="Mandatory" id="pgcustomaddl">* </FONT>
			<% } %>  <p>
			<%} else {%> <tr> <td COLSPAN=2> <br>
			<p class = "sectionHeadings" > <%=LC.L_Addl_Info%><%--Additional Information*****--%>   <%if (addlMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomaddl">* </FONT>
			<% } %> </p>
			<%} %>


		  <%if(addlAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (addlAtt.equals("2") ) {
			 readOnlyStr = "disabled"; }
		 %>

	  </td>


	  <td>&nbsp;</td>
	  </tr>
	  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
	  </tr>

		<input type="hidden" name="addInfoString">
		<% for (int i=0;i<length; i++) {
			codelstdesc=(String)  codelstDescs.get(i);
			addInfoId=(String) addInfoIds.get(i).toString();
			if(mode.equals("M")){
				if (i < advInfoAddIds.size())
				{
					advInfoAddId=(String) advInfoAddIds.get(i).toString();
					strIndex=(null==addInfoString)?0:addInfoString.charAt(i);
				}
				else
				{
					advInfoAddId = "";
					strIndex = '0';
				}

				if(strIndex=='1')
				{status="Checked=true" ;}
				else{status="" ;}
				}%>


			<tr>
					<td>
					<input type="checkbox"  <%=disableStr%>  <%=readOnlyStr%> name="addInfo" <%=status%> > <%=codelstdesc%>
					</td>
					<td>&nbsp </td>
					</tr>
					<input type="hidden" name=addInfoLen value=<%=length%>>
					<input type="hidden" name=addInfoId value=<%=addInfoId%>>
					<input type="hidden" name=advInfoAddId value=<%=advInfoAddId%>>

				<%}%>

			<td>&nbsp </td><td>&nbsp </td>


    	<% } else {

			%>
		   <input type="hidden" name="addInfoString">
			<%
			for (int i=0;i<length; i++) {
			codelstdesc=(String)  codelstDescs.get(i);
			addInfoId=(String) addInfoIds.get(i).toString();
			if(mode.equals("M")){
				if (i < advInfoAddIds.size())
				{
					advInfoAddId=(String) advInfoAddIds.get(i).toString();
					strIndex=(null==addInfoString)?0:addInfoString.charAt(i);
				}
				else
				{
					advInfoAddId = "";
					strIndex = '0';
				}

				if(strIndex=='1')
				{status="Checked=true" ;}
				else { status="" ;}
				}

		%>

	  <input type="checkbox" Style = "visibility:hidden"  name="addInfo" <%=status%> >

	  <input type="hidden" name=addInfoLen value=<%=length%>>
	  <input type="hidden" name=addInfoId value=<%=addInfoId%>>
	  <input type="hidden" name=advInfoAddId value=<%=advInfoAddId%>>

	  <% }}}

	  else {
		 %>
		<tr>
					<td COLSPAN=2><br>
						<p class = "sectionHeadings" ><%=LC.L_Addl_Info%><%--Additional Information*****--%></p>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>


				<% SchCodeDao addInfo= new SchCodeDao();
				addInfo.getCodeValues("adve_info");
				codelstDescs=addInfo.getCDesc();
				addInfoIds=addInfo.getCId();
				codelstdesc="";
				length=codelstDescs.size();
				ArrayList advInfoAddIds =null;
				String advInfoAddId =null;
				if(mode.equals("M")){
				EventInfoDao eventInfoDao = new EventInfoDao();
				eventInfoDao = eventdefB.getAdvInfo(EJBUtil.stringToNum(adveventId),"A") ;
				advInfoAddIds =eventInfoDao.getAdvInfoIds();
				}
				%>
				<input type="hidden" name="addInfoString">
				<% for (int i=0;i<length; i++) {
					codelstdesc=(String)  codelstDescs.get(i);
					addInfoId=(String) addInfoIds.get(i).toString();
					if(mode.equals("M")){
						if (i < advInfoAddIds.size())
						{
							advInfoAddId=(String) advInfoAddIds.get(i).toString();
							strIndex=(null==addInfoString)?0:addInfoString.charAt(i);
						}
						else
						{
							advInfoAddId = "";
							strIndex = '0';
						}

					if(strIndex=='1')
						{status="Checked=true" ;}
					else{status="" ;}
					}%>
					<tr>
						<td>
							<input type="checkbox" name="addInfo" <%=status%> > <%=codelstdesc%>
						</td>
						<td>&nbsp </td>
					</tr>
					<input type="hidden" name=addInfoLen value=<%=length%>>
					<input type="hidden" name=addInfoId value=<%=addInfoId%>>
					<input type="hidden" name=advInfoAddId value=<%=advInfoAddId%>>
				<%}%>

				<td>&nbsp </td><td>&nbsp </td>

      <%}%>
	</table>






		<table>


		<%SchCodeDao advNotify= new SchCodeDao();
				String notifyDate = "";
				advNotify.getCodeValues("adve_notify");
				codelstDescs=advNotify.getCDesc();
				advNotifyCodeIds=advNotify.getCId();
				codelstdesc="";


				ArrayList advNotifyIds =null;
            	ArrayList advNotifyAdverseIds =null;
            	ArrayList advNotifyCodelstNotTypeIds =null;
            	ArrayList advNotifyDates =null;
            	ArrayList advNotifyNotes =null;
            	ArrayList advNotifyValues =null;


				String advNotifyId =null;
            	String advNotifyAdverseId =null;
            	String advNotifyCodelstNotTypeId =null;
            	String advNotifyDt =null;
            	String advNotifyNote =null;
            	String advNotifyValue =null;

			    if(mode.equals("M")){

				EventInfoDao eventInfoDao = new EventInfoDao();
				eventInfoDao = eventdefB.getAdvNotify(EJBUtil.stringToNum(adveventId)) ;


            	advNotifyIds =eventInfoDao.getAdvNotifyIds();
            	advNotifyAdverseIds =eventInfoDao.getAdvNotifyAdverseIds();
            	advNotifyCodelstNotTypeIds =eventInfoDao.getAdvNotifyCodelstNotTypeIds();

            	advNotifyDates =eventInfoDao.getAdvNotifyDates();
            	advNotifyNotes =eventInfoDao.getAdvNotifyNotes();
				advNotifyValues =eventInfoDao.getAdvNotifyValues();
				length=advNotifyIds.size();


				}else{

				length=codelstDescs.size();
				}
				%>
				<input type="hidden" name="advNotifyString">


		<%if (hashPgCustFld.containsKey("followingnotified")) {

			int fldNumNotified = Integer.parseInt((String)hashPgCustFld.get("followingnotified"));
			String notifiedMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNotified));
			String notifiedLable = ((String)cdoPgField.getPcfLabel().get(fldNumNotified));
			String notifiedAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNotified));

			disableStr ="";
			readOnlyStr ="";
			if(notifiedAtt == null) notifiedAtt ="";
			if(notifiedMand == null) notifiedMand ="";

			if(!notifiedAtt.equals("0")) {

			if(notifiedLable !=null) {
			%>
			<tr> <td>  <%=notifiedLable%>:    <% if (notifiedMand.equals("1")) {%> <FONT class="Mandatory" id="pgcustomnotified">* </FONT> <%}%>      </td></tr>
			<% } else { %>
			<tr> <td> <%=MC.M_Following_WereNotified%><%--The Following were Notified*****--%>:   <% if (notifiedMand.equals("1")) {%> <FONT class="Mandatory" id="pgcustomnotified">* </FONT> <%}%>   </td></tr>

			<%
			 }
			   if(notifiedAtt.equals("1")) {
					 disableStr = "disabled"; }
			   else if (notifiedAtt.equals("2") ) {
					 readOnlyStr = "disabled"; }
			 %>



			<% for (int i=0;i<length; i++) {

				notifyDate = "notifyDate"+i;

				if(mode.equals("N")){
					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();


					status="" ;


				} else {

					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodelstNotTypeIds.get(i).toString();
					advNotifyId=(String) advNotifyIds.get(i).toString();
					advNotifyDate=((advNotifyDates.get(i)) == null)?"":(advNotifyDates.get(i)).toString();
					advNotifyValue = (String)advNotifyValues.get(i);



					if(advNotifyValue.equals("1")){
					status="Checked=true" ;
					}else{
					status="" ;
					}

					}

					%>
					<tr>
						<td>
							<input type="checkbox" <%=disableStr%>  <%=readOnlyStr%> name="advNotify" <%=status%> > <%=codelstdesc%>
						</td>
						<td>&nbsp </td><td>&nbsp </td><td>&nbsp </td>
						<td class=tdDefault><%=LC.L_Date%><%--Date*****--%></td>
						<td class=tdDefault>
<%-- INF-20084 Datepicker-- AGodara --%>
					<% if( (!notifiedAtt.equals("1")) && (!notifiedAtt.equals("2")) ) { %>
						<INPUT type=text name=<%=notifyDate%> class="datefield" <%=disableStr%> size=10 id=<%=notifyDate%>  READONLY value=<%=advNotifyDate%> >
					<%} else {%>
						<INPUT type=text name=<%=notifyDate%> <%=disableStr%> size=10 id=<%=notifyDate%>  READONLY value=<%=advNotifyDate%> >
					<%}%>
						</td>
						<td>&nbsp</td>
					</tr>
					<input type="hidden" name=advNotifyLen value=<%=length%>>
					<input type="hidden" name=advNotifyCodeId value=<%=advNotifyCodeId%>>
					<input type="hidden" name=advNotifyId value=<%=advNotifyId%>>
					<input type="hidden" name=advNotifyDate value=<%=advNotifyDate%>>
				<%}%>

				<%} else { %>

				<% for (int i=0;i<length; i++) {

				notifyDate = "notifyDate"+i;

				if(mode.equals("N")){
					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();


					status="" ;


				} else {

					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodelstNotTypeIds.get(i).toString();
					advNotifyId=(String) advNotifyIds.get(i).toString();
					advNotifyDate=((advNotifyDates.get(i)) == null)?"":(advNotifyDates.get(i)).toString();
					advNotifyValue = (String)advNotifyValues.get(i);



					if(advNotifyValue.equals("1")){
					status="Checked=true" ;
					}else{
					status="" ;
					}

					}

					%>
					<input type="checkbox" name="advNotify" Style = "visibility:hidden" <%=status%> >
					<INPUT type="hidden" name=<%=notifyDate%> size=10 READONLY value=<%=advNotifyDate%>>

					<input type="hidden" name=advNotifyLen value=<%=length%>>
					<input type="hidden" name=advNotifyCodeId value=<%=advNotifyCodeId%>>
					<input type="hidden" name=advNotifyId value=<%=advNotifyId%>>
					<input type="hidden" name=advNotifyDate value=<%=advNotifyDate%>>
				<%}%>


				<% } } else {%>

				<tr><td><%=MC.M_Following_WereNotified%><%--The Following were Notified*****--%>: <FONT id="pgcustomnotified"></FONT>  </td></tr>

				<% for (int i=0;i<length; i++) {

				notifyDate = "notifyDate"+i;

				if(mode.equals("N")){
					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodeIds.get(i).toString();


					status="" ;


				} else {

					codelstdesc=(String)  codelstDescs.get(i);
					advNotifyCodeId=(String) advNotifyCodelstNotTypeIds.get(i).toString();
					advNotifyId=(String) advNotifyIds.get(i).toString();
					advNotifyDate=((advNotifyDates.get(i)) == null)?"":(advNotifyDates.get(i)).toString();
					advNotifyValue = (String)advNotifyValues.get(i);



					if(advNotifyValue.equals("1")){
					status="Checked=true" ;
					}else{
					status="" ;
					}

					}

					%>
					<tr>
						<td>
							<input type="checkbox" name="advNotify" <%=status%> > <%=codelstdesc%>
						</td>
						<td>&nbsp </td><td>&nbsp </td><td>&nbsp </td>
						<td class=tdDefault><%=LC.L_Date%><%--Date*****--%></td>
<%-- INF-20084 Datepicker-- AGodara --%>						
						<td class=tdDefault><INPUT type=text name=<%=notifyDate%> class="datefield" size=10 id=<%=notifyDate%> READONLY value=<%=advNotifyDate%> ></td>
						<td>&nbsp </td>
					</tr>
					<input type="hidden" name=advNotifyLen value=<%=length%>>
					<input type="hidden" name=advNotifyCodeId value=<%=advNotifyCodeId%>>
					<input type="hidden" name=advNotifyId value=<%=advNotifyId%>>
					<input type="hidden" name=advNotifyDate value=<%=advNotifyDate%>>
				<%}%>

				<%}%>

				</table>





		<table width="98%">

		<tr>
        <%if (hashPgCustFld.containsKey("notes")) {
			int fldNumNotes = Integer.parseInt((String)hashPgCustFld.get("notes"));
			String notesMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNotes));
			String notesLable = ((String)cdoPgField.getPcfLabel().get(fldNumNotes));
			String notesAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNotes));

			disableStr ="";
			readOnlyStr ="";
			if(notesAtt == null) notesAtt ="";
			if(notesMand == null) notesMand ="";


			if(!notesAtt.equals("0")) {
			if(notesLable !=null){
			%><td class=tdDefault width="15%"> 
			<%=notesLable%> 
			<%} else {%> <td>
			  <%=LC.L_Notes%><%--Notes*****--%>   <!--KM-->
			<%}

			if (notesMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomnotes">* </FONT>
			<% }
		 %>

		  <%if(notesAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (notesAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



		</td>
		<td class=tdDefault>
			<TEXTAREA id=notes name=notes rows=3 cols=60 <%=disableStr%>  <%=readOnlyStr%> ><%=notes%></TEXTAREA>
		</td>
			<% } else { %>

		 <TEXTAREA id=notes name=notes rows=3 Style = "visibility:hidden" cols=60 ><%=notes%></TEXTAREA>

		 <% }} else {
			 %>
		<td class=tdDefault width="15%"><%=LC.L_Notes%><%--Notes*****--%> </td>
		<td class=tdDefault>

				<TEXTAREA id=notes name=notes rows=3 cols=60><%=notes%></TEXTAREA>
		</td>
		 <%}%>
  	    </tr>
		<% if ("M".equals(mode)){%>
		<tr>
	      <td width="20%"><%=LC.L_ReasonForChangeFDA%>
	      	<%if (fdaStudy == 1){%>&nbsp;<FONT class="Mandatory">* </FONT><%} %>
	      </td>
		  <td width="60%"><textarea id="remarks" name="remarks" rows="3" cols="60" MAXLENGTH="4000"></textarea></td>
	  	</tr>
	  	<%} %>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>



			</table>


				<table width="98%" cellspacing="0" cellpadding="0" bgcolor="#cccccc" >
							<%
				//Modified by Manimaran for November Enhancement PS4.
			if ((mode.equals("M") && (pageRight >= 6) && (orgRight >= 6) && (!patStatSubType.equals("lockdown")) ) || (mode.equals("N") && (pageRight == 5  || pageRight == 7 || orgRight == 7) ))
				{
				%>
					<!--tr>
						<td class=tdDefault width="12%">Form Status</td>
						<td width="13%"><%= dFormStatus %></td-->

				<tr valign="middle">
				 <%if (hashPgCustFld.containsKey("formstatus")) {
						int fldNumFormStat  = Integer.parseInt((String)hashPgCustFld.get("formstatus"));
						String formStatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumFormStat));
						String formStatLable = ((String)cdoPgField.getPcfLabel().get(fldNumFormStat));
						formStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumFormStat));

						if(formStatAtt == null) formStatAtt ="";
						if(formStatMand == null) formStatMand ="";

						if(!formStatAtt.equals("0")) {
						if(formStatLable !=null){
						%> <td class=tdDefault width="12%" >
						<%=formStatLable%>
						<%} else {%> <td class=tdDefault width="12%" >
						 <%=LC.L_Form_Status%><%--Form Status*****--%>
						<%}

						if (formStatMand.equals("1")) {
							%>
						   <FONT class="Mandatory" id="pgcustomformstat">* </FONT>
						<% } %>

				</td>
				<td width="13%">

				<%= dFormStatus %>  <%if(formStatAtt.equals("2")) {%><input type="hidden" name="formStatus" value="<%=advFormStatus%>"> <%}%>
				</td>
				<%} else if(formStatAtt.equals("0")) {%>

				<input type="hidden" name="formStatus" value ="<%=advFormStatus%>" >
				<%}} else {%>
				<td class=tdDefault width="12%"><%=LC.L_Form_Status%><%--Form Status*****--%></td>
				<td width="13%"><%= dFormStatus %></td>

				<%}%>

				<input type="hidden" id="fdaStudy" name="fdaStudy" value="<%=fdaStudy%>">
				<input type= hidden   name=currentDictSetting value="<%=settingValue%>">
					<td width="70%" align="left" >

				<jsp:include page="submitBar.jsp" flush="true">
						<jsp:param name="displayESign" value="Y"/>
						<jsp:param name="formID" value="advevtnew"/>
						<jsp:param name="showDiscard" value="N"/>
				</jsp:include> </td>
			   </tr>
				<%
				}
				else
				{%>
				<tr>
				<td></td>
				<td></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
			 <%}	%>
			</table>




	</Form>

  		<DIV class="myLinksmgpat">
			<%
			UsrLinkDao usrLinkDao = ulinkB.getULinkValuesByAccountId(EJBUtil.stringToNum(accId),"lnk_adv");
		   	ArrayList lnksIds = usrLinkDao.getLnksIds();
			ArrayList lnksUris = usrLinkDao.getLnksUris();
			ArrayList lnksDescs = usrLinkDao.getLnksDescs();
			ArrayList lnksGrps = usrLinkDao.getLnksGrpNames();
			int len = lnksIds.size();
			int counter = 0;
	   		String lnkUri = "";
			String lnkDesc = "";
   			String oldGrp = "";
	   		String lnkGrp = "";

			%>

			<DIV class="accLinksmgpat">

			    <Form name="acclnkbrowser" method="post" action="" onsubmit="">

<TABLE width="100%" cellspacing="0" cellpadding="3" border="1" border="black" height="150" bgcolor="#000066">

			<tr><th width="80%" align="left" height="20"><%=LC.L_Important_Links%><%--Important Links*****--%></th>


<!--    				        <td width=19 height=20> <img src= "../images/link_upleft.jpg" height=20>
    				        </td>
    				        <td width="130" height=20 >
    				            <P class = "sectionHeadings"> Important Links </P>
    				        </td>
    						<td width=22>&nbsp</td>
    				        <td  width=18 height=20 align="right"> <img src= "../images/link_upright.jpg" width=23 height=20>
    				        </td>
<th colspan="2">Important Links</th>-->
<!--    			        <th width="20%" align="center">&nbsp;</th> -->

    			        <%
    				    for(counter = 0;counter<len;counter++)
    					{
    						lnkUri=((lnksUris.get(counter))==null)?"-":(lnksUris.get(counter)).toString();
    						lnkDesc=((lnksDescs.get(counter))==null)?"-":(lnksDescs.get(counter)).toString();
    						lnkGrp = ((lnksGrps.get(counter))==null)?"-":(lnksGrps.get(counter)).toString();
    						%>
    				        <% if ( ! lnkGrp.equals(oldGrp)){ %>

    					        </tr>
<% if (counter != 0) {%>
<tr><td colspan="2">&nbsp;</td></tr>
<% } %>

<tr >
    						        <td colspan="2">  <%= lnkGrp %>  </td></tr>
<%}else{%>
<!--						    <td colspan="2">&nbsp;</td></tr> -->

    						<%}%>
    				        	<tr>

<td colspan ="2"> <A href="<%= lnkUri%>" target="Information" onClick="openWin()" ><%= lnkDesc%></A>   </td>
    				        </tr>
    				        <%
    						oldGrp = lnkGrp;
    			 		}
				if (counter==0){
				%>
					<tr><td colspan="2">&nbsp;</td></tr>

				<%}%>
					</table>
			</div>
			</Form>
		</div>
	<%
	} //end of if session times out
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	} //end of else body for page right

	%>
   	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>
</html>
