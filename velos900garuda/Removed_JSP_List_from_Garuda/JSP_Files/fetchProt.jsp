<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<title>Calendar >> Schedule and Customize Events</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<%
String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<SCRIPT>

//JM: 15Apr2008: added this function to shuffle the event sequences
function shuffleSeq(formobj,eventId,sequence,shufflngEventId,sufflngSeq,calledFrom,calStatus,calAssoc,protId, VisitId){

	 formobj.action = "shuffleprotocoleventseq.jsp?calAssoc="+calAssoc+"&calStatus="+calStatus+"&calledFrom="+calledFrom+"&eventId="+eventId+"&sequence="+sequence+"&shufflngEventId="+shufflngEventId+"&sufflngSeq="+sufflngSeq+"&protId="+protId+"&VisitId="+VisitId;
 	 formobj.submit;
}

//JM: 17Apr2008: added, function to search the Visits, Evets
function fnSearchVisitAndEvents(formobj){
	formobj.action="fetchProt.jsp";
formobj.submit();
}

//KM- Added page rights for link
function addMultipleVisit(formobj,link, pageRight)
{
	//JM: 22Feb2011: #5858
	codelstDesc = formobj.calStatusDesc.value;
	
	if (formobj.calStatus.value == 'W' || formobj.calStatus.value == 'O') {
		if (f_check_perm(pageRight,'E') == false) {
			 return false;
		}
	}

	//JM: 22Feb2011: #5858
	if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A') || (formobj.calStatus.value == 'D')) {
		alert("Cannot Add / Edit / Copy Visit in a Calendar with status "+codelstDesc);
		return false;
	}

   windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=950,height=550 top=100,left=150 ");//KM
    	windowName.focus();

}

//Added by Manimaran for copying Vist.
function openForCopyVisit(formobj, link) {

	
	//JM: 22Feb2011: #5858
	codelstDesc = formobj.calStatusDesc.value;
	
	if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A') || (formobj.calStatus.value == 'D')) {
		alert("Cannot Add / Edit / Copy Visit in a Calendar with status "+codelstDesc);
		return false;
	}

		windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=650,height=400 top=100,left=150 ");
    	windowName.focus();
}




function checkAll(formobj){



 	totcount=formobj.totcount.value;

 	//alert(totcount);


    if (formobj.chkAll.checked){


    if (totcount==1){
          formobj.Del.checked =true ;
	}
    else {
         for (i=0;i<totcount;i++){


			formobj.Del[i].checked=true;


         }
    }
    }else{
    	if (totcount==1){
			formobj.Del.checked =false ;
		}
    	else {
        for (i=0;i<totcount;i++){
			formobj.Del[i].checked=false;
        }
    }

    }

}


//Added by Manimaran for the issue 3416
//JM: 04NOV2009: Modified for the issue #4402

function visitCheckAll(formobj, turn) {

	totcntVisit = formobj.totcntVisit.value;


	if (document.getElementById("vcheck"+turn).checked) {

		var eveChkCnt = document.getElementById("eventcount"+(turn+1)).value;

		if (eveChkCnt == 1)
			eval("formobj.echeck"+turn).checked = true;
		else
		{

			for(j=0;j<eveChkCnt;j++) {

				eval("formobj.echeck"+turn+"["+j+"]").checked= true;

			}
		}

	}
	if (document.getElementById("vcheck"+turn).checked == false) {

		var eveChkCnt = document.getElementById("eventcount"+(turn+1)).value;

		if (eveChkCnt == 1)
			eval("formobj.echeck"+turn).checked = false;
		else
		{

			for(j=0;j<eveChkCnt;j++) {
				eval("formobj.echeck"+turn+"["+j+"]").checked= false;

			}
		}
	}
}



function openVisitPopUp(formobj, link) {

	//JM: 22Feb2011: #5858
	 codelstDesc = formobj.calStatusDesc.value;


   if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A') || (formobj.calStatus.value == 'D')) {
		//alert("Cannot Add / Edit / Copy Visit in Deactivated Calendar");
		alert("Cannot Add / Edit / Copy Visit in a Calendar with status "+codelstDesc);
		
		return false;
	}

	windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=750,height=400 top=100,left=150 ");
    windowName.focus();

}



function openEvtDetailsPopUp(formobj, link) {


	windowName=window.open(link,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=610,top=70 ,left=150");
    windowName.focus();

}



function deleteSelected(formobj,pageRight){

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 visitFlags = new Array();
	 totcount = formobj.totcount.value;
	 //alert(totcount);
	 srcmenu  = formobj.srcmenu.value;
	 duration = formobj.duration.value;
	 protocolId = formobj.protocolId.value;
	 calledFrom = formobj.calledFrom.value;
	 calStatus = formobj.calStatus.value;
	 calassoc = formobj.calassoc.value;
	 selectedTab = formobj.selectedTab.value;
	 mode = formobj.mode.value;

	 var offlineflagcheck ;
	 var offlineflagcheckTot = '';
	 offlineflagVals = new Array();


	//JM: 22Feb2011: #5858
	 codelstDesc = formobj.calStatusDesc.value;



	 submit="no";

	 if(calledFrom == 'L' || calledFrom == 'P')
	   tableName ='event_def';
	 else
	   tableName ='event_assoc';

//JM: 09May2008, added for, #3461: calStatus == 'D' included
	if ( (calStatus == 'F') || (calStatus == 'A')|| (calStatus == 'D')) {

		//JM: 22Feb2011: #5858
		//alert("Cannot Delete Visit(s)/Event(s) in a Frozen/Active/Deactivated calendar");
		alert("Cannot Delete Visit(s)/Event(s) in calendar with status "+codelstDesc);
		return false;
	}

   if (f_check_perm(pageRight,'E') == true) {



	 if(totcount==0 ){

	   alert("Please select the Visit(s)/Event(s) to be deleted");
	   return false;
	 }


	 if (totcount==1){


		 if (formobj.Del.checked==true){
	//JM:
	offlineflagVal = formobj.offLnFlg.value;
			if (offlineflagVal.indexOf("1") >=0){

				//JM: 22Feb2011: #5858
				//alert("Cannot Delete Visit(s)/Event(s) in an Offline for Editing calendar");
				alert("Cannot Delete Visit(s)/Event(s) in calendar with status "+codelstDesc);
				return false;


			}else{

				msg="Do you want to delete the selected Visit(s)/Event(s)?";
				if (confirm(msg))
				{
					//Deletes visit or event single record...
					window.open("deletevisitseventsfromcal.jsp?srcmenu="+srcmenu+"&selectedTab="+selectedTab+"&selStrs="+formobj.Del.value
				     	 +"&visitFlags="+formobj.visitFlag.value+"&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&tableName="+tableName+"&mode="+mode
				      	 +"&calStatus="+calStatus+"&from=initial&calassoc="+calassoc,"_self");

				    window.focus();






			 	}else{
					return false;
				}

			}


		 }else{

		//JM: 01Aug2008: added: cosmetic change
		 alert("Please select Visit(s)/Event(s) to be deleted");

	 }


	 }else{



	 	for(i=0;i<totcount;i++){


	  		if (formobj.Del[i].checked){

		  		selStrs[j] = formobj.Del[i].value;
		  		visitFlags[j] = formobj.visitFlag[i].value;

		  		//JM: 09May2008, added for #3468
		  		offlineflagVals[j] = formobj.offLnFlg[i].value;

		  		j++;
		  		cnt++;
			}
	  	}




		if(cnt>0){



			   	for (g=0; g < offlineflagVals.length; g++){
			  		offlineflagcheck = offlineflagVals[g];
					offlineflagcheckTot =offlineflagcheckTot +  offlineflagcheck+ ',';
				}

				if (offlineflagcheckTot.indexOf("1") >=0 || offlineflagcheckTot.indexOf("2") > -1){

					//JM: 22Feb2011: #5858
			  		//alert("Cannot Delete Visit(s)/Event(s) in an Offline for Editing calendar");
			  		alert("Cannot Delete Visit(s)/Event(s) in a calendar with status "+codelstDesc);
					return false;

			    }else{


				  msg="Do you want to delete the selected Visit(s)/Event(s)?";
				  if(confirm(msg)) {


						//Deletes visits and events as well
						window.open("deletevisitseventsfromcal.jsp?srcmenu="+srcmenu+"&selectedTab="+selectedTab+"&selStrs="+selStrs
				      	 +"&visitFlags="+visitFlags+"&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&tableName="+tableName+"&mode="+mode
				      	 +"&calStatus="+calStatus+"&from=initial&calassoc="+calassoc,"_self");

				      	window.focus();




	  	    	  }else{
					  	return false;
				  }
			    }

		}



		if(cnt==0){
		 alert("Please select Visit(s)/Event(s) to be deleted");
		 return false;
		}

	 }


	 }


 }


//KM- #3207-Added page rights for link
function addEventsToVisitsWindow(formobj,link, pageRight) {

	//JM: 22Feb2011: #5858
	 codelstDesc = formobj.calStatusDesc.value;

	if (formobj.calStatus.value == 'W' || formobj.calStatus.value == 'O') {
		if (f_check_perm(pageRight,'E') == false) {
			 return false;
		}
	}

	if ((formobj.totcntVisit.value==0)||(formobj.totcntEvt.value==0)){

		if ((formobj.totcntVisit.value==0)&&(formobj.totcntEvt.value!=0)){

			alert("You do not have Visit(s) to add to the exisiting Event(s)");

		}else	if ((formobj.totcntVisit.value!=0)&&(formobj.totcntEvt.value==0)){

			alert("You do not have Event(s) to add to the existing visit(s)");

		}else{
			alert("Visit(s) and Event(s) do not exist");
		}
		return false;

	}

//JM: 09May2008, added for, #3460
	if ((formobj.calStatus.value == 'F') || (formobj.calStatus.value == 'A')|| (formobj.calStatus.value == 'D')) {
		//JM: 22Feb2011: #5858
		//alert("Cannot add Event(s) to Visit(s) in a Frozen/Active/Deactivated calendar");
		alert("Cannot add Event(s) to Visit(s) in a calendar with status "+codelstDesc);
		return false;
	}
	//KM:22Sep09
	windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=1000,height=575 top=300,left=250 ");
	windowName.focus();

}


//JM: 25Apr2008, added for, Enh. #C9: function to hide and unhide the events/visists when they are in offline status
function hideUnhideSelected(formobj,pageRight,hideFlag){


	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 visitFlags = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu  = formobj.srcmenu.value;
	 duration = formobj.duration.value;
	 protocolId = formobj.protocolId.value;
	 calledFrom = formobj.calledFrom.value;
	 calStatus = formobj.calStatus.value;
	 calassoc = formobj.calassoc.value;
	 selectedTab = formobj.selectedTab.value;
	 mode = formobj.mode.value;

	 submit="no";




   if (f_check_perm(pageRight,'E') == true) {



	 if(totcount==0 && hideFlag==1){

	   alert("Please select the Visit(s)/Event(s) to hide ");
	   return false;
	 }

	 if(totcount==0 && hideFlag==0){

	   alert("Please select the Visit(s)/Event(s) to unhide ");
	   return false;
	 }



	 if (totcount==1){



		 if (formobj.Del.checked==true){

		    if(hideFlag==0){
			  msg="Do you want to unhide the selected Visit(s)/Event(s)?";
			}else{
			  msg="Do you want to hide the selected Visit(s)/Event(s)?";
			}

			if (confirm(msg))
			{

				window.open("hideunhidecalendar.jsp?srcmenu="+srcmenu+"&selectedTab="+selectedTab+"&selStrs="+formobj.Del.value
			     	 +"&visitFlags="+formobj.visitFlag.value+"&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&mode="+mode
			      	 +"&calStatus="+calStatus+"&from=initial&calassoc="+calassoc+"&hideFlagVal="+hideFlag,"_self");

			    window.focus();

		 	}else{
				return false;
			}
		 }

	 }else{



	 	for(i=0;i<totcount;i++){


	  		if (formobj.Del[i].checked){

		  		selStrs[j] = formobj.Del[i].value;
		  		visitFlags[j] = formobj.visitFlag[i].value;
		  		j++;
		  		cnt++;
			}
	  	}




		if(cnt>0){

			  if(hideFlag==0){
			  msg="Do you want to unhide the selected Visit(s)/Event(s)?";
			  }else{
			  msg="Do you want to hide the selected Visit(s)/Event(s)?";
			  }
			  if(confirm(msg)) {

					window.open("hideunhidecalendar.jsp?srcmenu="+srcmenu+"&selectedTab="+selectedTab+"&selStrs="+selStrs
			      	 +"&visitFlags="+visitFlags+"&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&mode="+mode
			      	 +"&calStatus="+calStatus+"&from=initial&calassoc="+calassoc+"&hideFlagVal="+hideFlag,"_self");

			      	window.focus();

  	    	  }else{
				  	return false;
			  }
		}

	 }

	 if(cnt==0 && hideFlag==0){
		 alert("Please select Visit(s)/Event(s) to unhide");
		 return false;
	 }
	 if(cnt==0 && hideFlag==1){
		 alert("Please select Visit(s)/Event(s) to hide");
		 return false;
	 }

	 }


 }


</SCRIPT>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id="protVisitLocalB" scope="page" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<%@ page language = "java" import ="com.velos.eres.business.group.*,java.net.URLEncoder,com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB, com.velos.esch.web.eventdef.EventdefJB, com.velos.esch.web.eventassoc.EventAssocJB"%>

<jsp:useBean id="eventlistdao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="assoclistdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>


<BODY style="overflow: hidden;">
<!-- Changed by PK: BUG#4277 -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>  
<%
	int pageRight=0;
	int eventId=0;
	String protocolId=request.getParameter("protocolId");

	HttpSession tSession = request.getSession(true);
    if (sessionmaint.isValidSession(tSession))
	{
	 //KM-#5000

	String calledFrom = request.getParameter("calledFrom");
	String calAssoc=request.getParameter("calassoc");
	calAssoc=(calAssoc==null)?"":calAssoc;

	//GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");
	//pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));
	if (calledFrom.equals("S")) {

	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	   if ((stdRights.getFtrRights().size()) == 0){

			pageRight= 0;

	   }else{

			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

	   }

	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

	}
	//FIX #6126 Moved code above, before calling tabs.jsp
	//JM: 16Apr2008: added for to display the calendar name in the tab page
	String protocolName = "";
	String calStatusPk = "";
	SchCodeDao scho = new SchCodeDao();
	String calStatus = "";
	if (calledFrom.equals("P")  || calledFrom.equals("L")){

		eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventdefB.getEventdefDetails();
		protocolName = eventdefB.getName();
		tSession.putValue("protocolname", protocolName);
		//KM-#D-FIN9
		calStatusPk = eventdefB.getStatCode();
		calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
	}
	else if (calledFrom.equals("S")){

		eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventassocB.getEventAssocDetails();
		protocolName = eventassocB.getName();
		tSession.putValue("protocolname", protocolName);
		//KM-#D-FIN9
		calStatusPk = eventassocB.getStatCode();
		calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
	}

	%>
	<DIV class="BrowserTopn" id = "div1">
		<jsp:include page="protocoltabs.jsp" flush="true">
		<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calAssoc)%>" />
		
		</jsp:include>
	</DIV>

	<%
	if (pageRight > 0) {
		if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
}}
	 if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
	 %>
	<DIV class="BrowserBotN">
	  <jsp:include page="calDoesNotExist.jsp" flush="true"/>
	</DIV>
	  <%

	}else {


	String eventmode = request.getParameter("eventmode");

	//JM: 18Apr2008

	ArrayList noOfEventIds=null;



	if (calledFrom.equals("P")  || calledFrom.equals("L")){
		EventdefDao defdao = new EventdefDao();
   		defdao= eventdefB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId), "DESC","EVENT_ID" );
   		noOfEventIds=defdao.getEvent_ids();

	}else if (calledFrom.equals("S")){

		EventAssocDao assocdao = new EventAssocDao();

   		assocdao= eventassocB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),"DESC","EVENT_ID");
   		noOfEventIds=assocdao.getEvent_ids();
	}


	int noOfEvents = noOfEventIds.size() ;

	calStatus = request.getParameter("calStatus");
	
	//JM: 22Feb2011: #5858
	   String calStatDesc = "";
	   SchCodeDao schcodedao = new SchCodeDao();	   
	   
	   if (calledFrom.equals("P") || calledFrom.equals("L")) {
		   calStatDesc = schcodedao.getCodeDescription(schcodedao.getCodeId("calStatLib",calStatus));
	   }else{
		   calStatDesc = schcodedao.getCodeDescription(schcodedao.getCodeId("calStatStd",calStatus));
	   }	   
	   
	
	
	String evtName = "";
	evtName =request.getParameter("searchEvt");
	evtName=(evtName==null)?"":evtName;

	String visitNameId = request.getParameter("visitList");
	if (visitNameId==null) visitNameId = "";



	String eventType="";
	String name="";
	String eventName="";

	String duration= "";

	String newDuration = "";

	newDuration = (String) tSession.getAttribute("newduration");

	if (StringUtil.isEmpty(newDuration))
	{
		duration =   request.getParameter("duration");
	}
	else
	{
		duration = newDuration ;
	}


	String eventDur = "";
	String description="";
	Integer Id=new Integer(protocolId);
	int weeks = 0;
	ArrayList disps = new ArrayList();
	String displayDur = request.getParameter("displayDur");
	String eventVisitName="";
    int pageNo=EJBUtil.stringToNum(request.getParameter("pageNo"));
	String refresh_flag = request.getParameter("refresh_flag");
	String userId = (String) (tSession.getValue("userId"));
	String accId = (String) (tSession.getValue("accountId"));
	String studyId = (String) tSession.getValue("studyId");
	String selectedTab=request.getParameter("selectedTab");


			if (pageRight > 0 )
			{
				eventType = "P";
				String mode = request.getParameter("mode");
				String displayType=request.getParameter("displayType");

				ArrayList eventNames= null;
				ArrayList eventIds=null;
				ArrayList protocolIds=null;
				ArrayList names=null ;
				ArrayList descriptions= null;
				ArrayList eventTypes=null;
				ArrayList eventDurs= null;
				ArrayList displacements= null;
				ArrayList orgIds= null;
				ArrayList costs= null;
				ArrayList monthsArr = null; //SV, 10/25/04
				ArrayList weeksArr = null ;
				ArrayList daysArr = null ;
				ArrayList insertAfterArr = null; //BK,DFIN25,MAR-24-10
				ArrayList insertAfterStringArr = null;
				ArrayList eventVisitIds = null;
				ArrayList categoryIds = null;
				ArrayList sequnces = null;
				ArrayList flagsStrings = null;
				ArrayList offLineFlags = null;
				ArrayList hideFlags = null;
				ArrayList facilityIds = null; //KV:SW-FIN2c
				ArrayList actualeventCosts = null; //KV: SW-FIN2b


		   if (mode.equals("N"))
		   {
	%>

	<%
		   } //mode N
		else if (mode.equals("M"))
		{
	%>
	<%
		} //mode M

//KM-22Dec09
if (calledFrom.equals("P"))
{%>
<Form name="fetchProt" method="post" action="">
<%}
else if (calledFrom.equals("L"))
{%>
<Form name="fetchProt" method="post" action="">
<%}
else if (calledFrom.equals("S"))
{ %>
<Form name="fetchProt" method="post" action="">
<%}%>
<DIV class="BrowserBotN" id="div2">

		<% if (pageRight == 4) {%>

					<span id="comment" class = "alert"><FONT class="Mandatory">You have only View rights. Any changes made will not be saved.</Font></span>

				<%}
		if (calledFrom.equals("P")  || calledFrom.equals("L"))
		{

		   eventlistdao= eventdefB.getAllProtSelectedEvents(EJBUtil.stringToNum(protocolId), visitNameId, evtName);
		   eventNames= eventlistdao.getNames();
		   costs= eventlistdao.getCosts();
		   eventIds=eventlistdao.getEvent_ids() ;
		   protocolIds=eventlistdao.getChain_ids() ;
		   descriptions= eventlistdao.getDescriptions();
		   eventTypes= eventlistdao.getEvent_types();
		   eventDurs= eventlistdao.getDurations();
		   displacements= eventlistdao.getDisplacements();
		   orgIds= eventlistdao.getOrg_ids();
		   costs = eventlistdao.getCosts();
		   monthsArr = eventlistdao.getMonth(); //SV, 10/25
		   weeksArr = eventlistdao.getWeek();
		   daysArr = eventlistdao.getDay();
		   insertAfterArr = eventlistdao.getInsertAfter(); //BK,DFIN25,MAR-24-10
		   insertAfterStringArr = eventlistdao.getAfterIntervalStrings();
		   flagsStrings = eventlistdao.getFlags();//JM
		   eventVisitIds = eventlistdao.getFk_visit();
		   categoryIds = eventlistdao.getEventCategory();
		   sequnces = eventlistdao.getEventSequences();
		   facilityIds = eventlistdao.getFacility(); //KV:SW-FIN2c 
		   actualeventCosts = eventlistdao.getEventCostDetails();// KV: SW-FIN2b
		}
		else if (calledFrom.equals("S"))
		{  //called From Study
		   assoclistdao= eventassocB.getAllProtSelectedEvents(EJBUtil.stringToNum(protocolId), visitNameId, evtName);
		   eventNames= assoclistdao.getNames();
		   costs= assoclistdao.getCosts();
		   eventIds=assoclistdao.getEvent_ids() ;
		   protocolIds=assoclistdao.getChain_ids() ;
		   descriptions= assoclistdao.getDescriptions();
		   eventTypes= assoclistdao.getEvent_types();
		   eventDurs= assoclistdao.getDurations();
		   displacements= assoclistdao.getDisplacements();
		   orgIds= assoclistdao.getOrg_ids();
		   monthsArr = assoclistdao.getMonth(); //SV, 10/26
		   weeksArr = assoclistdao.getWeek();
		   daysArr = assoclistdao.getDay();
		   insertAfterArr = assoclistdao.getInsertAfter(); //BK,DFIN25,MAR-24-10
		   insertAfterStringArr = assoclistdao.getAfterIntervalStrings();
		   flagsStrings = assoclistdao.getFlags();//JM: 14Apr2008****
		   eventVisitIds = assoclistdao.getEventVisits();
		   categoryIds = assoclistdao.getEventCategory();
		   sequnces = assoclistdao.getEventSequences();
		   offLineFlags = assoclistdao.getOfflineFlags();
		   hideFlags = assoclistdao.getHideFlags();
		   facilityIds = assoclistdao.getFacility(); //KV:SW-FIN2c 	
		   actualeventCosts = assoclistdao.getEventCostDetails();// KV: SW-FIN2b
		}

		//Added by Manimaran to fix the issue 3416

		flagsStrings.add("V");

		int cntChk=0;
		int vcnt =0;
		for (int k=0; k<flagsStrings.size();k++) {
			if((flagsStrings.get(k).toString()).equals("E")) {
				cntChk++;
			}
			if((flagsStrings.get(k).toString()).equals("V")) {
				vcnt++;
		%>
			<input type="hidden" name="evecnt" id="eventcount<%=vcnt%>" value="<%=cntChk%>" />
		<%
			cntChk =0;
		   }
		}
		flagsStrings.remove(eventIds.size());



		if ((calStatus.equals("A")) || (calStatus.equals("F")))
		{%>
	       <span id="comment" class = "alert"><FONT class="Mandatory">Changes cannot be made to Event Interval and Event Details for a Calendar with status <%=calStatDesc%></Font></span>
		<%}%>

	  	 <!--
 		<P class = "defComments"><FONT class="Mandatory">
	  	 Please make sure all appropriate settings and changes have been applied to 'Select Events', 'Define Visits' and 'Schedule Events' pages before customizing Events in this page. <BR>
	    Any changes made to the above mentioned pages after customizing Events in this page will lead to the customizations been over-ridden and replaced with the Event attributes defined in the 'Select Events' page.
	   </Font></P>
	    -->


		<%if (calledFrom.equals("S")){%>
<!--			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href=# onclick="openCalPreview(<%=studyId%>,<%=protocolId%>,<%=pageRight%>)">Preview Calendar</A> -->
		<%}%>
		

		<INPUT NAME="mode" TYPE=hidden  value="<%=mode%>"/>
		<INPUT NAME="srcmenu" TYPE=hidden value="<%=src%>"/>
		<INPUT NAME="calledFrom" TYPE=hidden  value="<%=calledFrom%>"/>
		<INPUT NAME="duration" TYPE=hidden  value="<%=duration%>"/>
		<INPUT NAME="protocolId" TYPE=hidden  value="<%=protocolId%>"/>
		<INPUT NAME="calStatus" TYPE=hidden  value="<%=calStatus%>"/>
		<INPUT NAME="calassoc" TYPE=hidden  value="<%=calAssoc%>"/>
		<Input type="hidden" name="selectedTab" value="<%=selectedTab%>"/>
		
		<Input type="hidden" name="calStatusDesc" value="<%=calStatDesc%>"/>

<%
					ArrayList visitIds = null;
					ArrayList visitNames= null;


					String visitId = null;
					String visitName= null;
					visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));

					visitIds = visitdao.getVisit_ids();
					visitNames = visitdao.getNames();

%>




<table width="98%" cellspacing="0" cellpadding="0">
<tr bgcolor="#dcdcdc">
	<td>
	<table cellspacing="0" cellpadding="0" >
      <tr height="20" bgcolor="#dcdcdc">
		<td colspan="2"><b>Search By</b></td>
	  </tr>
	  <tr height="20" bgcolor="#dcdcdc">
		<td>Visit: 
			<!--JM:17Apr2008--Drop Down for the visit-->
			<select name="visitList">
			<%
			String vName ="";
			for (int i=0;i<visitIds.size();i++){

				vName = visitNames.get(i).toString();
				vName = (vName.length()>30)? vName.substring(0,27)+"...":vName;
				if (visitNameId.equals(visitIds.get(i).toString())) {
			%>
					<option value="<%=visitIds.get(i).toString()%>" selected><%=vName%></option>

				<%}else {%>
					<option value="<%=visitIds.get(i).toString()%>"><%=vName%></option>
			<%
				}
			}

			if (visitNameId.equals("")) {

			%>
				<option value="" selected> Select an Option</option>
			<%
			}else{
			%>


			<option value=""> Select an Option</option>
			<%}%>
			</select>

			<!--Drop Down for the visit-->
		</td>
	    <td>&nbsp;&nbsp;Event:&nbsp;<input type="text" name="searchEvt" value="<%=evtName%>" size="15">
	      	<!--
	      	<img src="../images/jpg/smallGo.gif" border="0" align="absmiddle" onclick="return fnSearchVisitAndEvents(document.fetchProt);">
	      	-->
	      	<button type="submit" name="submit" onclick="return fnSearchVisitAndEvents(document.fetchProt);"><%=LC.L_Search%></button>
			

	    </td>
		</tr>
	</table>
	</td>
	<%if ((!protocolId.equals("null"))) {%>
	<td>
	<table cellspacing="0" cellpadding="0" >
	   	<tr height="20" bgcolor="#dcdcdc">
			<td colspan="2"><b>View Report: </b></td>
		</tr>
		<tr>
			<td>
			<%//KM-#5000
			String acId=(String)tSession.getValue("accountId");
			
			ReportDaoNew repDao = new ReportDaoNew();
			repDao.getReports("rep_calendar",EJBUtil.stringToNum(acId));
			
			StringBuffer sb = new StringBuffer();
	
		 	sb.append("<SELECT id='calReps' NAME='calReps'>") ;
			for (int cnt = 0; cnt <= repDao.getPkReport().size() -1 ; cnt++)
			{
				sb.append("<OPTION value = '"+ repDao.getPkReport().get(cnt)+"' >" + repDao.getRepName().get(cnt)+ "</OPTION>");
	
			}
			sb.append("</SELECT>");
			String repdD = sb.toString();
			%>
			<%=repdD%>&nbsp;<A href="#"
			onClick="return validateReport(document.protocoltab);"><img
			src="../images/jpg/displayreport.jpg" align="absmiddle" border="0"
			alt=""></A>
			</td>
	</tr>
	</table>	
	</td>
	<%}%>
</tr>
</table>
<br/>
</div>
<DIV id="divBottom" class="BgtTabFormBotN" style="height:380px;">
	<TABLE cellPadding=1 cellSpacing=1 width="98%">
	<TR>
	<td colspan="4">
		<FONT class = "comments">Scheduled Events currently associated to this Calendar:</FONT>

		</td>
	</TR>
	<TR>
	<%
	int max_visit_no = 0;
	String link = "multiplevisits.jsp?srcmenu="+src+"&eventId=&duration="+duration+"&protocolId="+protocolId+"&calledFrom="+calledFrom+"&mode=N&calStatus="+calStatus+"&fromPage=eventbrowser&from=initial&tableName=sch_protocol_visit&max_visit_no="+max_visit_no+"&calassoc="+calAssoc;
	//KM-09Nov09
	%>
		<td class="tdDefault"><A href="#" onClick="addMultipleVisit(document.fetchProt,'<%=link%>',<%=pageRight%>);" >ADD VISIT(S)</A></td>

	<td width="2%"></td>

	<% String veLink="addevtvisits.jsp?mode="+mode+"&srcmenu="+src+"&duration="+duration+"&protocolid="+protocolId+"&calledfrom="+calledFrom+"&calstatus="+calStatus+"&calltime=new"+"&calassoc="+calAssoc;

	%>
	<td class="tdDefault"><A href="#"onClick ="addEventsToVisitsWindow(document.fetchProt, '<%=veLink%>',<%=pageRight%>)">ADD EVENTS TO VISITS</A></td>


 	<td class="tdDefault"><A href="#"onClick ="deleteSelected(document.fetchProt,<%=pageRight%>);" >DELETE SELECTED</A></td>

 <%//JM: 24Apr2008: added for #C9
   if (calStatus.equals("O")){%>
	<td class="tdDefault"><A href="#"onClick ="hideUnhideSelected(document.fetchProt,<%=pageRight%>, '1');" >HIDE SELECTED</A></td>
	<td class="tdDefault"><A href="#"onClick ="hideUnhideSelected(document.fetchProt,<%=pageRight%>, '0');" >UNHIDE SELECTED</A></td>
 <%}%>


	</TR>
	</table>

	<div id="TableContainer" class="TableContainer" border="1" style="overflow:auto; width:100%; height:300px; background-color:white;">

	<TABLE id="scrollTab" class="scrollTable" border="1" cellSpacing="1" width="100%">

	<thead id="tableHead" class="fixedHeader headerFormat">
	<TR class="title">
		<TH width="12%">Visit Name</TH>
		<TH width="10%"> Interval </TH>
		<TH width="08%">&nbsp;</TH>
 <%
	 //JM: 09May2008, for #3464
		if (!calStatus.equals("A") && !calStatus.equals("D") && !calStatus.equals("F")){ //Not for A/D/F
	 %>
		<TH width="12%"> Seq. No. </TH>
	 <%}
	 %>
		<TH width="15%"> Category </TH>
		<TH width="15%">Event </TH>
	 <%//JM: 24Apr2008: added for #C9
	  if (calStatus.equals("O")){%>
		<th width="5%"  align =center >Hidden</th>
	 <%}%>
		<TH width="15%"> Event Window </TH>
		<!-- KV:SW-FIN2c -->
		<TH width="15%"> Cost </TH>
		<TH width="15%"> Facility </TH>
		
		<TH width=10%  align =center  colspan="2">Select <input type="checkbox" name="chkAll" value="" onClick="checkAll(document.fetchProt)">
		</TH>

		<!--
		<TH width="60%" colspan=6> Attributes </TH>
		 -->
	</TR>
	</thead>
	 <%
		String monthString = ""; //SV, 10/25
		String weekString = "";
		String dayString = "";
		String afterString ="";
		String afterVisitString ="";
		String insertAfter = "";
		String prevCostStr = "";
		String costStr= "";
		String orgId = "";
		String evntName = "";
		String evntNameTrim="";
		String intervalString = "";
		String evntDetailHref = "";
		String msgHref = "";
		String costHref = "";
		String appendxHref = "";
		String resourceHref = "";
		String crfDetailHref = "";
		String attributeString = "";
		String prevVisitName = "";

		String categoryId = "";
		String eventChkId="";
		String sequnce = "";
		String flagStr ="";
		boolean visitChange = false;
		
		String offLineFlag ="";
		String hideFlag ="";
		String hideFlagStr ="";
        String facilityname = ""; //SW-FIN2c 
        String actualeventCost = ""; //SW-FIN2b


		//JM

			String fuzzyPeriodBefore = "";
			String fuzzyUnitBef= "";
			String fuzzyPeriodAfter = "";
			String fuzzyUnitAft = "";
			String visitWindow = "";
			String visitWindow_before = "";
			String visitWindow_after= "";


			int counterTotalVisit = 0;
			int counterTotalEvt = 0;



			String upperEventId = "";
			String lowerEventId = "";

			String upperSeq = "";
			String lowerSeq = "";


			int visitSeq =0;





		visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
	//	totalVisits = visitdao.getTotalVisits();
       // Java script added to fix Bug # 4277 by PK
       %>
       
          <script language=javascript>
	            var varViewTitle =  new Array (<%=eventIds.size()%>);
	      </script>
		<% 
		if (eventIds.size() > 0){ //Bug#5142-added conditional check and movedtbody inside 
		%>

	<tbody id="tableBody" class="scrollContent bodyFormat"  style="height:250px;">
       
     <% 
		for (int i=0;i<eventIds.size();i++)
		{



		    intervalString="";
			String cost = (String)costs.get(i);

			prevCostStr = "";
			costStr= cost  ;

			if ( i !=0 )
			{
				  prevCostStr = costs.get(i-1).toString()  ;
			 }

	/*		if (   ( i == 0 ) || ( ! costStr.equals(prevCostStr ) )    )
			{
					evntName= eventNames.get(i).toString();
			}
			else
			{
				evntName = "" ;
			}
	*/
			evntName= eventNames.get(i).toString(); //SV, 10/18, show event name always now that it's not the first col. per Rehan


//JM: 12Nov2009: #4399
//Changed By PK: #4277
            evntNameTrim = evntName;
            evntNameTrim= (evntNameTrim==null)?"":evntNameTrim;
	  		if (evntNameTrim.length()>50){
	  			evntNameTrim=evntNameTrim.substring(0,27);
	  			evntNameTrim +="...";
      		}

			//JM:27Mar -------------
			eventChkId = eventIds.get(i).toString();

			//JM: 17Apr2008: These variables needs be flushed out for evey loop
			 visitWindow_before = "";
			 visitWindow_after= "";

	//FIX #6245
	visitChange = false;
	if (i == 0){
		visitChange = true;
	} else {
		String prevEvent = eventIds.get(i-1).toString();
		if (prevEvent.equals("0")){
			visitChange = true;
		}
	}


	if (calledFrom.equals("P")  || calledFrom.equals("L")){

			EventdefJB evtDefB = new EventdefJB();
			evtDefB.setEvent_id(EJBUtil.stringToNum(eventChkId));
			evtDefB.getEventdefDetails();


    		fuzzyPeriodBefore = evtDefB.getFuzzy_period();	//---------------Number of DMW e.g., 7
    		fuzzyPeriodBefore = (fuzzyPeriodBefore==null)?"":fuzzyPeriodBefore;

    		fuzzyUnitBef=evtDefB.getDurationUnitBefore();	//---------------DMW e.g., Days ------before
    		fuzzyUnitBef = (fuzzyUnitBef==null)?"Days":fuzzyUnitBef;

			fuzzyPeriodAfter=evtDefB.getFuzzyAfter();	//-------------------Number of DMW e.g., 7
			fuzzyPeriodAfter = (fuzzyPeriodAfter==null)?"":fuzzyPeriodAfter;

			fuzzyUnitAft=evtDefB.getDurationUnitAfter();	//---------------DMW e.g., Days -------after
			fuzzyUnitAft = (fuzzyUnitAft==null)?"Days":fuzzyUnitAft;


		}else{

			//JM; 2April2008
			EventAssocJB evtassB = new EventAssocJB();
			evtassB.setEvent_id(EJBUtil.stringToNum(eventChkId));
			evtassB.getEventAssocDetails();



    		fuzzyPeriodBefore = evtassB.getFuzzy_period();
    		fuzzyPeriodBefore = (fuzzyPeriodBefore==null)?"":fuzzyPeriodBefore;

    		fuzzyUnitBef=evtassB.getDurationUnitBefore();
    		fuzzyUnitBef = (fuzzyUnitBef==null)?"Days":fuzzyUnitBef;


			fuzzyPeriodAfter=evtassB.getFuzzyAfter();
			fuzzyPeriodAfter = (fuzzyPeriodAfter==null)?"":fuzzyPeriodAfter;

			fuzzyUnitAft=evtassB.getDurationUnitAfter();
			fuzzyUnitAft = (fuzzyUnitAft==null)?"Days":fuzzyUnitAft;



		}



			flagStr = ((flagsStrings.get(i)) == null)?"-":(flagsStrings.get(i)).toString();



			monthString = "Month " + monthsArr.get(i); //SV, 10/25*/
			weekString = "Week "+ weeksArr.get(i)   ;

			dayString = "Day "+(Integer)daysArr.get(i) ;
			
			//DFIN25-DAY0
			if(!insertAfterArr.isEmpty()){
			insertAfter = insertAfterArr.get(i).toString();
			if(!insertAfter.equals("0"))
			   afterString = insertAfterStringArr.get(i).toString();
			}
			
			

			if ( ((Integer)monthsArr.get(i)).intValue() <0               )
			{
			monthString="";
			attributeString = "" ;
			}
 			else if ( ((Integer)monthsArr.get(i)).intValue()  == 0               )
			{
			//BK - DFIN25-DAY0
			    monthString = "";
				attributeString = "" ;

			}
			else {
				monthString = "Month " + monthsArr.get(i); //SV, 10/25*/
			}
			if (    ((Integer)weeksArr.get(i)).intValue()  < 0               )
			{
				weekString = "";
				attributeString = "" ;
			}
			else if (    ((Integer)weeksArr.get(i)).intValue()  == 0               )
			{
				weekString = "";
				attributeString = "" ;
			}
			else {
						weekString = "Week "+ weeksArr.get(i)   ;
			}
       //BK - DFIN25-DAY0

			if (monthString.length()>0)
				 intervalString = monthString;
			if (weekString.length()>0)
			  {
				if (intervalString.length()>0)
				 intervalString = intervalString+","+ weekString;
				 else
				 intervalString = weekString;
			 }
			 if (dayString.length()>0)
			  {
				if (intervalString.length()>0)
				 intervalString = intervalString+","+ dayString;
				 else
				 intervalString = dayString;
			 }
			 if(!StringUtil.isEmpty(insertAfter) &&!insertAfter.equals("0")){
              intervalString = intervalString +"("+ afterString;
              protVisitLocalB.setVisit_id(new Integer(insertAfter).intValue());
              protVisitLocalB.getProtVisitDetails();
              intervalString = intervalString +" "+protVisitLocalB.getName()+")";
			 }


			//intervalString = monthString + "," + weekString + "," + dayString;

			int eventVisitId = 0;
			String noInterval="";
			String tempEventVisitId = "";//JM

			if ( (eventVisitIds.size() > 0) && (eventVisitIds.get(i) != null)) {
				eventVisitId = ((Integer)eventVisitIds.get(i)).intValue();

				//JM:11Apr2008
				tempEventVisitId = eventVisitIds.get(i).toString();

				eventVisitName = visitdao.getVisitName(eventVisitId);

				noInterval = visitdao.getNoInterval(eventVisitId);
				if(noInterval == null)
					noInterval = "";

				//SV, we want to blank it all out, only if visitname is actually found (this shouldn't arise, but if it does then we don't want to blank out interval string as well)

				if (noInterval.equals("1"))
					intervalString = "No Interval Defined";

				if (eventVisitName.equals(prevVisitName) && (!eventVisitName.equals(""))) {
					eventVisitName = "";
					intervalString = ""; //SV, 11/04, Same visit name cannot have different interval string, this we don't need a separate prevIntervalStr??

				}
				else {
					prevVisitName = eventVisitName;
				}
	//DEBUG System.out.println("fetchprot: visit name="+eventVisitName);


			}


			categoryId = ((categoryIds.get(i)) == null)?"-":(categoryIds.get(i)).toString();
			sequnce = ((sequnces.get(i)) == null)?"":(sequnces.get(i)).toString();
			
			//KV:SW-FIN2c
			facilityname = ((facilityIds.get(i)) == null)?"-":(facilityIds.get(i)).toString();
			//KV:SW-FIN2b
			actualeventCost =((actualeventCosts.get(i)) == null)?"-":(actualeventCosts.get(i)).toString();
				
			//JM: 25Apr2008, added for, Enh. #C9
			if (calledFrom.equals("S")){

			offLineFlag = ((offLineFlags.get(i)) == null)?"":(offLineFlags.get(i)).toString();
			hideFlag = ((hideFlags.get(i)) == null)?"":(hideFlags.get(i)).toString();

			}



			if (hideFlag.equals("1"))
				hideFlagStr="Yes";
			else
				hideFlagStr="";



			if (fuzzyUnitBef.equals("D")) fuzzyUnitBef = "Days";
			if (fuzzyUnitBef.equals("H")) fuzzyUnitBef = "Hours";
			if (fuzzyUnitBef.equals("M")) fuzzyUnitBef = "Months";
			if (fuzzyUnitBef.equals("W")) fuzzyUnitBef = "Weeks";

			if (fuzzyUnitAft.equals("D")) fuzzyUnitAft = "Days";
			if (fuzzyUnitAft.equals("H")) fuzzyUnitAft = "Hours";
			if (fuzzyUnitAft.equals("M")) fuzzyUnitAft = "Months";
			if (fuzzyUnitAft.equals("W")) fuzzyUnitAft = "Weeks";



			if (!fuzzyPeriodBefore.equals("0") && !fuzzyPeriodBefore.equals("")){

				visitWindow_before = fuzzyPeriodBefore +" "+ fuzzyUnitBef + " Before" ;

			}

			if (!fuzzyPeriodAfter.equals("0") && !fuzzyPeriodAfter.equals("")){

				visitWindow_after =  fuzzyPeriodAfter + " " +  fuzzyUnitAft + " After";

			}

			if ( (!fuzzyPeriodBefore.equals("0")|| !fuzzyPeriodBefore.equals("")) && (fuzzyPeriodAfter.equals("0") ||fuzzyPeriodAfter.equals("") )){
				visitWindow =  visitWindow_before;
			}else if ( (fuzzyPeriodBefore.equals("")||fuzzyPeriodBefore.equals("0"))&& (!fuzzyPeriodAfter.equals("0")||!fuzzyPeriodAfter.equals(""))){
				visitWindow =  visitWindow_after;
			}else if((!fuzzyPeriodBefore.equals("0")&& !fuzzyPeriodAfter.equals("0")) && (!fuzzyPeriodBefore.equals("")&& !fuzzyPeriodAfter.equals(""))){
				visitWindow =  visitWindow_before+ ", "+ visitWindow_after;
			}




			//JM: 15April2008


			if (i > 0) {
		 	    upperEventId = eventIds.get(i-1).toString();
			    upperSeq =  (sequnces.get(i-1)).toString();//Pass them as String
			}

			if (i < eventIds.size()-1) {
			    lowerEventId = eventIds.get(i+1).toString();
			    lowerSeq =  (sequnces.get(i+1)).toString();//Pass them as String
			}


			if(flagStr.equals("V")){

			visitSeq++;

			}


		%>
		<%if (i%2 == 0)
			{ %>
			<tr class="browserEvenRow">
			<%}
			else
			{%>
			<tr class="browserOddRow">
			<%}%>

			<%

				if(flagStr.equals("V")){
					
					%><td>
					<%
					String edit_link="visitdetail.jsp?srcmenu=" + src +"&duration=" + duration + "&protocolId="+protocolId+ "&calledFrom=" + calledFrom + "&mode=M&calStatus=" + calStatus + "&visitId=" + eventVisitId + "&fromPage=fetchProt&from=initial&seq_no="+ (visitSeq)+"&calassoc="+calAssoc+"&offLineFlag="+offLineFlag ;
					%>

					<A href="#" onClick="openVisitPopUp(document.fetchProt,'<%=edit_link%>')" title="<%=eventVisitName %>"> <%=((eventVisitName.length()>30)?eventVisitName.substring(0,27)+"...":eventVisitName)%></td></A></td>

					<td width="15%"><%=intervalString%></td>
			 		<%
			 		counterTotalVisit++;%>

			 <%

			 //KM:25July08
			 link="visitdetail.jsp?srcmenu=" + src + "&eventId=" + eventId + "&duration=" + duration + "&protocolId="+protocolId+ "&calledFrom=" + calledFrom + "&mode=N&calStatus=" + calStatus + "&fromPage=fetchProt&from=initial&tableName=sch_protocol_visit&max_visit_no="+max_visit_no+"&calassoc="+calAssoc + "&visitId=" + eventVisitId;%>

			<!--td width="10%"><A href="#"><font color=blue> Copy visit </font></A></td-->
			<td width="08%"><A href="#"><font color=blue  onclick="openForCopyVisit(document.fetchProt,'<%=link%>')"> Copy visit</font></A></td>
		<%
		//JM: 09May2008, for #3464
		if (!calStatus.equals("A") && !calStatus.equals("D") && !calStatus.equals("F")){ //Not for A/D/F
		%>
			<td colspan="3">&nbsp;</td>
		<%}else{%>
			<td colspan="2">&nbsp;</td>
		<%} %>
 	<%//JM: 24Apr2008: added for #C9
  		if (calStatus.equals("O") && hideFlag.equals("1")){

  		%>
			<td width="5%"><%=hideFlagStr%>&nbsp;</td>
 	<%	}else if (calStatus.equals("O") && hideFlag.equals("0")){

 	%>
 		<td width="5%"><%=hideFlagStr%>&nbsp;</td>
 	<%}else if (calStatus.equals("O") && hideFlag.equals("")){%>
 		<td width="5%"><%=hideFlagStr%>&nbsp;</td>

 	<%}%>
			<td colspan="3">&nbsp;</td>
			<td width="10%" colspan="2" align="center" style="padding-right:50px;">
			<input type="checkbox" name="Del" id="vcheck<%=counterTotalVisit%>" value="<%=eventVisitId%>"
			onClick = "visitCheckAll(document.fetchProt, <%=counterTotalVisit%>)"> </td>
			<input type="hidden" name="visitFlag" value="1">		<Input type="hidden" name="offLnFlg" value="<%=offLineFlag%>"><!--//JM: 09May2008, added for #3468-->
			<%

			}

			if(flagStr.equals("E")){
			counterTotalEvt++;

			%>

			<td colspan="3">&nbsp;</td>

	<!--JM:11April2008 added for the sequences-->
	<%
		//JM: 09May2008, for #3464
		if (!calStatus.equals("A") && !calStatus.equals("D") && !calStatus.equals("F")){ //Not for A/D/F

	%>
		   <td width="10%">
			<table border="0" width="98%"><tr><td>

			<%

			if ( (eventIds.size() >= 1) && (flagStr.equals("E"))){//no of events in the visit



				if ( visitChange ){///FIX #6245 
				//First event of any visit will have visitChange = true
				%>
    				<input type="image" src="../images/jpg/down.gif" onclick="shuffleSeq(document.fetchProt, '<%=eventChkId%>', '<%=sequnce%>', '<%=lowerEventId%>','<%=lowerSeq%>','<%=calledFrom%>','<%=calStatus%>', '<%=calAssoc%>', '<%=protocolId%>', '<%=eventVisitId%>')">


					</td><td align="right">(<%=sequnce%>)&nbsp;

				<%}else if ( (i-2 <=0) && ! tempEventVisitId.equals((eventVisitIds.get(i-2)).toString()) ){


				%>
					<input type="image" src="../images/jpg/down.gif" onclick="shuffleSeq(document.fetchProt, '<%=eventChkId%>', '<%=sequnce%>', '<%=lowerEventId%>','<%=lowerSeq%>','<%=calledFrom%>','<%=calStatus%>', '<%=calAssoc%>', '<%=protocolId%>', '<%=eventVisitId%>')">


					</td><td align="right">(<%=sequnce%>)&nbsp;
				<%}	else if ( (i+1) != eventVisitIds.size() ){


						if ( ! tempEventVisitId.equals((eventVisitIds.get(i+1)).toString())){


						%>
							 <input type="image" src="../images/jpg/up.gif" onclick="shuffleSeq(document.fetchProt, '<%=eventChkId%>', '<%=sequnce%>', '<%=upperEventId%>','<%=upperSeq%>','<%=calledFrom%>','<%=calStatus%>', '<%=calAssoc%>', '<%=protocolId%>', '<%=eventVisitId%>')">


							 </td><td align="right">(<%=sequnce%>)&nbsp;

						<%}else{

						%>
							<input type="image" src="../images/jpg/up.gif" onclick="shuffleSeq(document.fetchProt, '<%=eventChkId%>', '<%=sequnce%>', '<%=upperEventId%>','<%=upperSeq%>','<%=calledFrom%>','<%=calStatus%>', '<%=calAssoc%>', '<%=protocolId%>', '<%=eventVisitId%>')">
							<input type="image" src="../images/jpg/down.gif" onclick="shuffleSeq(document.fetchProt, '<%=eventChkId%>', '<%=sequnce%>', '<%=lowerEventId%>','<%=lowerSeq%>','<%=calledFrom%>','<%=calStatus%>', '<%=calAssoc%>', '<%=protocolId%>', '<%=eventVisitId%>')">

							</td><td align="right">(<%=sequnce%>)&nbsp;

						<%}

				}else{ //i ==len

				%>
					<input type="image" src="../images/jpg/up.gif" onclick="shuffleSeq(document.fetchProt, '<%=eventChkId%>', '<%=sequnce%>', '<%=upperEventId%>','<%=upperSeq%>','<%=calledFrom%>','<%=calStatus%>', '<%=calAssoc%>', '<%=protocolId%>', '<%=eventVisitId%>')">
					</td><td align="right">(<%=sequnce%>)&nbsp;
				<%} //count ==len
			}  //end of formfldNum > 0	%>
			</td></tr>
			</table>
		   </td>
<%}%>		<!--JM:11April2008 added above for the sequences-->

			<td width="10%"> <%=categoryId%></td>
			<%
			//JM: 25Apr2008, added for, Enh. #C9: added calStatus
				String param = "eventdetails.jsp?srcmenu=" + src + "&eventmode=M&eventId=" + eventChkId + "&duration=" + duration + "&protocolId=" + protocolId + "&calledFrom=" + calledFrom + "&mode=" + mode + "&calStatus=" + calStatus + "&fromPage=fetchProt&selectedTab=1&calassoc="+calAssoc+"&calStatus="+calStatus+"&offLineFlag="+offLineFlag;
			%>
			<!--Changed by PK : Bug #4277  -->
			<script language=javascript>
		              varViewTitle[<%=i%>] = htmlEncode('<%=evntName%>');
		           
		           </script>
			<td>
			<A href="#" onmouseover="return overlib(varViewTitle[<%=i%>],CAPTION,'Event Name');" onmouseout="return nd();" onClick="openEvtDetailsPopUp(document.fetchProt,'<%=param%>')" > <%=evntNameTrim%></td></A>
			</td>
	<%//JM: 24Apr2008: added for #C9
  		if (calStatus.equals("O") && hideFlag.equals("1")){

  		%>
			<td width="5%"><%=hideFlagStr%></td>
 	<%	}else if (calStatus.equals("O") && hideFlag.equals("0")){

 	%>
 		<td width="5%"><%=hideFlagStr%></td>


 	<%}else if (calStatus.equals("O") && hideFlag.equals("")){%>
 		<td width="5%"><%=hideFlagStr%></td>

 	<%}%>
			<td width="10%"><%=visitWindow%>&nbsp;</td>
			<!-- KV:SW-FIN2b & SW-FIN2c  -->
			<td width="20%"><%=actualeventCost%>&nbsp;</td>
			<td width="10%"><%=facilityname%>&nbsp;</td>
			<td width="5%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>



			<td width="10%"><input type="checkbox" name="Del" id="echeck<%=counterTotalVisit%>" value="<%=eventChkId%>"></td> <!--KM- 03Dec08 -->

			<input type="hidden" name="visitFlag" value="0"></td>
			<Input type="hidden" name="offLnFlg" value="<%=offLineFlag%>"><!--//JM: 09May2008, added for #3468-->


<%

	}

			//			IA 12.06.2006
			//			Make Invisible the CRF link when the calendar is associated to a study Bug 2677
		} //loop of eventids
				%>

			</tr>

		</tbody>
		<%} %>
	</TABLE>
	</div>
	  <table width="100%" style="margin-top:10px" cellspacing="0" cellpadding="0">
		<tr>
	      <td align=center>
			<A href="eventbrowser.jsp?selectedTab=2&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=<%=pageNo%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>" type="submit"><%=LC.L_Back%></A>
	      </td>
	      <td>
			<%

				if (calledFrom.equals("P"))
			 {%>
				<A href="protocollist.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=1&calledFrom=<%=calledFrom%>"> <img src="../images/jpg/Finish.gif" align="absmiddle" border=0> </A>
			<%}
			else if (calledFrom.equals("L"))
			{%>
			<A href="protocollist.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=1&calledFrom=<%=calledFrom%>"> <img src="../images/jpg/Finish.gif" align="absmiddle" border=0> </A>
			<%}
			else if (calledFrom.equals("S"))
			{
			 if (calAssoc.equals("S")){   %>
			 <A href="studyadmincal.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=10"> <img src="../images/jpg/Finish.gif" align="absmiddle" border=0> </A>
			 <%} else { %>
			 <A href="studyprotocols.jsp?mode=<%=mode%>&srcmenu=<%=src%>&selectedTab=7"> <img src="../images/jpg/Finish.gif" align="absmiddle" border=0> </A>
			 <%} %>

			<%}%>
	      </td>
	      </tr>
	  </table>

</DIV>

	   <input type="hidden" id="totcount" name="totcount" Value="<%=counterTotalVisit + counterTotalEvt%>"> <!-- total need to be counted... -->
	   <input type="hidden" name="totcntVisit" Value="<%=counterTotalVisit%>">
	   <input type="hidden" name="totcntEvt" Value="<%=noOfEvents%>">
	</Form>

	<%
	} //end of if body for page right

	else

	{

	%>
<DIV class="BrowserBotN" id="div2">
	  <jsp:include page="accessdenied.jsp" flush="true"/>
</DIV>
	 <%

	 } //end of else body for page right
%>
     
     <script>
//Bug-5054 para-2
//Virendra: Fixed Bug No. 5171, Shifted Script inside 'else' condition of "if not calendar entered"
//Virendra: Previously was outside else condition of "session exists"
if (document.getElementById("TableContainer")){
	document.getElementById("TableContainer").style.backgroundColor="black";
	var divHeight = document.getElementById("TableContainer").offsetHeight;
	var tabHeight =document.getElementById("scrollTab").offsetHeight;
	
	if (tabHeight < divHeight){//IE
		document.getElementById("TableContainer").style.height = document.getElementById("scrollTab").style.height;
	}
	if (document.getElementById("totcount").value == "0"){//FF
		document.getElementById("TableContainer").style.height = document.getElementById("tableHead").clientHeight +"px";
		document.getElementById("TableContainer").style.overflow="hidden"; //Bug#5142
	}
	if (document.getElementById("comment")){
		if (document.getElementById("comment").visibility="visible"){
			document.getElementById("divBottom").style.top = (parseInt(document.getElementById("divBottom").offsetTop,10)-10)+'px';
		} 
	}else{
		document.getElementById("divBottom").style.top = (parseInt(document.getElementById("divBottom").offsetTop,10)-25)+'px';	
	}
}
</script>
     
     
     <% } // end of if not calendar entered
}//end of if body for session

else

{

%>

  <jsp:include page="timeout.html" flush="true"/>

<%

}

%>

  <!--div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div-->



<div class ="mainMenu" id = "emenu">

  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>



</BODY>
</HTML>











