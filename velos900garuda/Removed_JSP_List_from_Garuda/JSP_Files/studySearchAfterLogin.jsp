<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Broadcast_Trails%><%--Broadcast Trails*****--%></title>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT language="JavaScript1.1">

function openstudywin(studyId) {
    windowName = window.open("completeprotocol.jsp?id=" + studyId +"&reportNumber=9&type=P","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	windowName.focus();
;}
</SCRIPT>

<BODY>

<br>
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="com.velos.eres.service.util.*"%>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB"%>

<DIV class="browserDefault" id="div1">


<%
StudyDao studyDao = new StudyDao();
String search = request.getParameter("search");


String searchOn = request.getParameter("searchOn");
String tab = request.getParameter("selectedTab");
String criteria = "";
String userId = "";

HttpSession tSession = request.getSession(true);


if ( sessionmaint.isValidSession(tSession) )
{
 	String uName = (String) tSession.getValue("userName");
	String itemStr = "";
	ArrayList searchKeys = null;
	searchKeys = new ArrayList();
	StringTokenizer st = new StringTokenizer(search, ",");
	while (st.hasMoreTokens())
	{
		itemStr = st.nextToken();
		itemStr = itemStr.trim();
		searchKeys.add(itemStr);
	}



	userId = (String) tSession.getValue("userId");



if(searchOn.equals("keyword")) {
	//studyDao = studyB.getStudyValuesKeyword(search);
	studyDao = studyB.getStudyValuesKeywordAll(searchKeys);

	criteria = LC.L_Keyword;/*criteria = "Keyword";*****/
}
if(searchOn.equals("title")) {
	studyDao = studyB.getStudyValuesTitle(search);
	criteria = LC.L_Study_Title;/*criteria = LC.Std_Study+ " Title";*****/
}
if(searchOn.equals("author")) {
	studyDao = studyB.getStudyValuesAuthor(search);
	criteria = LC.L_Author_Name;/*criteria = "Author Name";*****/
}
if(searchOn.equals("tarea")) {
	studyDao = studyB.getStudyValuesTarea(search);
	criteria = LC.L_Therapeutic_Area;/*criteria = "Therapeutic Area";*****/
}

ArrayList studyIds 		= studyDao.getStudyIds();
ArrayList studyTitles 		= studyDao.getStudyTitles();
ArrayList studyObjectives 	= studyDao.getStudyObjectives();
ArrayList studySummarys 	= studyDao.getStudySummarys();
ArrayList studyDurs 		= studyDao.getStudyDurations();
ArrayList studyDurUnits 	= studyDao.getStudyDurationUnits();
ArrayList studyEstBgnDts 	= studyDao.getStudyEstBeginDates();
ArrayList studyActBghDts 	= studyDao.getStudyActBeginDates();
ArrayList studyParentIds 	= studyDao.getStudyParents();
ArrayList studyAuthors 		= studyDao.getStudyAuthors();
ArrayList studyNumbers 		= studyDao.getStudyNumbers();

int studyId = 0;
String studyTitle = "";
String studyAuthor = "";
String studyNumber = "";

int len = studyIds.size();
int counter = 0;

%>

<BR>
<P class = "userName"> <%= uName %> </P>
<jsp:include page="personalizetabs.jsp" flush="true">
<jsp:param name="selectedTab" value="<%=tab%>"/>
</jsp:include>

<Form name="studybrowser" method="post" action="" onsubmit="">

<table width="100%" cellspacing="0" cellpadding="0" border=0 >
 <tr >
	<td width = "100%">
		<P class = "defComments">
			<% if(len <= 0) {
			%>
			<BR><BR>
			<%=MC.M_NoStdPublicView_RegSignUp%><%--<B>None of the <%=LC.Std_Studies_Lower%> </B>in this category are available for public viewing at the moment. You may register and sign up for the Notification service to receive email announcements of <%=LC.Std_Studies_Lower%> released for public viewing in the future*****--%>
			<BR>
			<BR>
			<A HREF="mySubscriptions.jsp?srcmenu=tdMenuBarItem2&selectedTab=3"><%=MC.M_SignUp_NotificSrv%><%--Sign up for the Notification Service*****--%></A>
			<% } else {
				Object[] arguments = {criteria,search}; 
			%>
			<%=VelosResourceBundle.getMessageString("M_YourSrch_YieldStd",arguments)%> <%--Your search criteria ("<%=criteria%>" Like "<%=search%>") yielded the following Studies*****--%>
			<% }
			%>
		</P>

	</td>

 </tr>
 <tr>
	<td height="10"></td>
	<td height="10"></td>
 </tr>
</table>

<% if (len > 0) {
%>

<table width="98%" >
 <tr>
   <th width="20%">
	<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>
   </th>
   <th width="40%">
	<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>
   </th>
   <th width = "20%"></th>

 </tr>

 <%
    for(counter = 0;counter<len;counter++)
	{	studyId = EJBUtil.stringToNum(((studyIds.get(counter)) == null)?"-":(studyIds.get(counter)).toString());

		studyAuthor = (studyAuthors.get(counter)).toString();
%>


<%
		studyTitle = ((studyTitles.get(counter)) == null)?"-":(studyTitles.get(counter)).toString();

		studyNumber = ((studyNumbers.get(counter)) == null)?"-":(studyNumbers.get(counter)).toString();
		if ((counter%2)==0) {
  %>

		<tr class="browserEvenRow">
  <%
		}
		else{
  %>

			<tr class="browserOddRow">
  <%
		}
  %>
		<td>
		  <A HREF=# onClick="openstudywin(<%=studyId%>)"><%=studyNumber%></A>
		</td>
		<td>
		  <%=studyTitle%>
		</td>

		<td>
			<%=MC.M_Send_RequestFor %><%-- Send a request for *****--%>
			<BR>
			<A href="updateNewMsgcntr.jsp?userId=<%=userId%>&studyId=<%=studyId%>&authorId=<%=studyAuthor%>&reqType=V&msgText=Request for View right&searchOn=<%=searchOn%>&search=<%=search%>&selectedTab=<%=tab%>&srcmenu=<%=src%>"><img border="0" title="<%=LC.L_View%>" alt="<%=LC.L_View%>" src="./images/View.gif" ><%//=LC.L_View%><%--View*****--%></A>
			<A href="updateNewMsgcntr.jsp?userId=<%=userId%>&studyId=<%=studyId%>&authorId=<%=studyAuthor%>&reqType=M&msgText=Request for Modify right&searchOn=<%=searchOn%>&search=<%=search%>&selectedTab=<%=tab%>&srcmenu=<%=src%>"><img border="0" title="<%=LC.L_Modify%>" alt="<%=LC.L_Modify%>" src="./images/edit.gif" ><%//=LC.L_Modify %><%--Modify*****--%></A>

		</td>
		</tr>

<%
		}
	} //end of if body for len>0
%>

</table>
</Form>

<%

} //end of if body for session
else
{
%>

<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

<div>
<jsp:include page="bottompanel.jsp" flush="true"/>

</div>

</div>


<div class ="mainMenu" id="emenu">
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>
</html>

