<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.velos.esch.web.subCostItem.SubCostItemJB,com.velos.esch.web.subCostItemVisit.SubCostItemVisitJB"%>
<%@page import="com.velos.esch.service.util.EJBUtil, com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.esch.web.subCostItemVisit.SubCostItemVisitJB"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.web.milestone.MilestoneJB"%><jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("application/json");
	HttpSession tSession = request.getSession(true);
	JSONObject jsObj = new JSONObject();
	String myMilestoneGrid;
	String updateInfo;
	if (!sessionmaint.isValidSession(tSession)) {
		// Session is invalid; print an error and exit
		response.sendError(HttpServletResponse.SC_FORBIDDEN,
				"SESSION_TIME_OUT");
		jsObj.put("result", -1);
		jsObj.put("resultMsg", MC.M_UsrNot_LoggedIn/*"User is not logged in."*****/);
		out.println(jsObj.toString());
		return;
	}

	String oldESign = (String) tSession.getAttribute("eSign");
	String eSign = request.getParameter("eSign");
	if (!oldESign.equals(eSign)) {
		jsObj.put("result", -4);
		jsObj
				.put("resultMsg",
						MC.M_EtrWrgEsign_Svg/*"You entered a wrong e-signature. Please try saving again."*****/);
		out.println(jsObj.toString());
		return;
	}	
	String ipAdd = (String) tSession.getAttribute("ipAdd");
	String userId = (String) tSession.getAttribute("userId");
	String accountId = (String) tSession.getAttribute("accountId");
	String studyId = (String) tSession.getAttribute("studyId");

	String mode = request.getParameter("mode");
	String duration = request.getParameter("duration");
	String calstatus = request.getParameter("calstatus");
	String calassoc = request.getParameter("calassoc");
	String[] ops = request.getParameterValues("ops");
	String calledFrom = request.getParameter("calledfrom");
	String protId = request.getParameter("calProtocolId");

	myMilestoneGrid = request.getParameter("myMilestoneGrid");
	updateInfo = request.getParameter("updateInfo");

	String tableName = request.getParameter("tableName");
	String[] purgeItems = request.getParameterValues("purgedItems");
	int changeCount = EJBUtil.stringToNum(request
			.getParameter("changeCount"));
	boolean deleteFlag = false, updateFlag = false, createFlag = false;
	
	//calculating the flag for create,update and delete operations

	if (!"[]".equals(updateInfo) && !StringUtil.isEmpty(updateInfo)) {
		JSONArray updateArray = new JSONArray(updateInfo);
		updateFlag = true;
		//System.out.println("updateArray----" + updateArray);
	}

	JSONArray milestoneArray = new JSONArray(myMilestoneGrid);
	//System.out.println("myMilestoneGrid------" + myMilestoneGrid);
	JSONObject dataRecord = null;
	for (int i = 0; i < milestoneArray.length(); ++i) {
		dataRecord = milestoneArray.getJSONObject(i);
		if (dataRecord.getBoolean("delete")	&& dataRecord.getInt("mileId") != 0) {
			deleteFlag = true;
		}
		if (!dataRecord.getBoolean("delete") && dataRecord.getInt("mileId") == 0) {
			createFlag = true;
		}
	}

	
	MilestoneJB mileObject = new MilestoneJB();
	//update
	if(updateFlag){
		mileObject.updateMilestonesUsingJSON(myMilestoneGrid, updateInfo);
	}
	
   //create
		if(createFlag){
			
			mileObject.setCreator(userId);
			mileObject.setIpAdd(ipAdd);			
			mileObject.setMilestoneDelFlag("N");
			mileObject.setMilestoneStudyId(studyId);
			mileObject.createMilestonesUsingJSON(myMilestoneGrid);
		}

   //delete
	if(deleteFlag){	
		//Modified for INF-18183 ::: Akshi
		mileObject.deleteMilestonesUsingJSON(myMilestoneGrid,AuditUtils.createArgs(session,"",LC.L_Std_Mstones));
	}	
	
	
	jsObj.put("result", 0);
	jsObj.put("resultMsg", MC.M_Changes_SavedSucc/*"Changes saved successfully"*****/);
	out.println(jsObj.toString());
%>
