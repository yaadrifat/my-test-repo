<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=MC.M_Alerts_NotficSettings%><%--Alerts and Notifications Settings*****--%></title>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.esch.business.common.SchCodeDao,com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.user.*,com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>


<% String src;

src= request.getParameter("srcmenu");

%>

	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>

	<body style="overflow:scroll">
	<br>

<DIV class="browserDefault" id="div1">
<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
	{
	String uName = (String) tSession.getValue("userName");

	%>

	<P class = "userName"> <%= uName %> </P>
	<P class="sectionHeadings"><%=MC.M_MngPcol_MngAlrtNotify%><%--Manage Protocols >> Manage Alerts and Notifications*****--%></P>
	<%
	String studyId = "";
	String patientId = "";
	String selectedTab="" ;
	String from ="" ;
    from = request.getParameter("from");
	if (from==null) from="" ;
	if (from.equals("studypatients")) {
		selectedTab=request.getParameter("selectedTab");
	}
	studyId = request.getParameter("studyId");
	patientId = request.getParameter("pkey");
	String userIdFromSession = (String) tSession.getValue("userId");
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
	ArrayList tId = teamDao.getTeamIds();
	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
	 	ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();

	tSession.putValue("studyRights",stdRights);
	int patdetright = 0;
	int pageRight = 0;

	if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
			patdetright = 0;
		}else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
			patdetright = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYVPDET"));
		}

	tSession.putValue("studyManagePat",new Integer(pageRight));
	tSession.putValue("studyViewPatDet",new Integer(patdetright));
	//StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	//pageRight =7;
   	//pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
	if (pageRight == 5  || pageRight == 7)
	   {
		   UserJB user = (UserJB) tSession.getValue("currentUser");
	   	   //String studyId = (String) tSession.getValue("studyId");
 		    String searchFrom = request.getParameter("searchFrom");
		   //EventAssocDao eventAssoc = new EventAssocDao();
		   ctrldao= eventassocB.getStudyProts(EJBUtil.stringToNum(studyId));
			ArrayList eventIds=ctrldao.getEvent_ids() ;
			ArrayList chainIds=ctrldao.getChain_ids() ;
			ArrayList names= ctrldao.getNames();
			ArrayList descriptions= ctrldao.getDescriptions();
			ArrayList eventTypes= ctrldao.getEvent_types();
			ArrayList durations= ctrldao.getDurations();
			
			//JM: 07FEB2011, #D-FIN9
			//ArrayList stats= ctrldao.getStatus();
			ArrayList codeSubTypes= ctrldao.getCodeSubTypes();
			
			ArrayList codeDescriptions= ctrldao.getCodeDescriptions();		

			
			
			

	   	  	int len= eventIds.size();
			//out.print("Length" +len);

			 //len = 1;

  	         	int counter = 0;
			Integer id;
			String eventType = "";
			String name="";
			String description="";
			String chainId="";
			String duration="";
			String status="";
			String statusText="";
			int displayCounter = 0;
%>

  <Form name="protocolbrowser" method="post" action="" onsubmit="">
  	 <% if (from.equals("studypatients")) { %>
  	  <input type="hidden" name="from"  value="<%=from%>">
      <input type="hidden" name="selectedTab"  value="<%=selectedTab%>">
  	 <jsp:include page="mgpatienttabs.jsp" flush="true"/>
	 <br>

       <%}
  	    %>
   <input type="hidden" name="studyId" Value=<%=studyId%>>
    <input type="hidden" name="srcmenu" value='<%=src%>'>


   <table width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr >
      <td width = "100%">
        <P class = "defComments"><%=MC.M_ListDispPcolAssoc_StdMng%><%--The list below displays the 'Active' Protocol Calendars associated with this <%=LC.Std_Study_Lower%>. Click on View/Edit Settings' to manage Alerts' and Notifications' settings for a protocol.*****--%> </P>
      </td>
    </tr>
   </table>
<BR>
	  		 	<%
    if (from.equals("studypatients")){%>
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
	<%}%>

    <table width="100%" >
       <tr>
        	<th width="40%"> <%=LC.L_Name%><%--Name*****--%> </th>
	        <th width="40%"> <%=LC.L_Description%><%--Description*****--%> </th>
	        <th width="20%"> </th>
      </tr>
       <%

    for(counter = 0;counter<len;counter++)
	{
		id = (Integer)eventIds.get(counter);
		eventType=((eventTypes.get(counter)) == null)?"-":(eventTypes.get(counter)).toString();
		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
		description=((descriptions.get(counter)) == null)?"-":(descriptions.get(counter)).toString();
		chainId=((chainIds.get(counter)) == null)?"-":(chainIds.get(counter)).toString();
		duration=((durations.get(counter)) == null)?"-":(durations.get(counter)).toString();
		
		
		//JM: 07FEB2011, #D-FIN9
		//status=((stats.get(counter)) == null)?"-":(stats.get(counter)).toString();		
		
		status = ((codeSubTypes.get(counter)) == null)?"-":(codeSubTypes.get(counter)).toString();
			
		statusText = ((codeDescriptions.get(counter)) == null)?"-":(codeDescriptions.get(counter)).toString();
		

		//display only active protcols

		if (status.equals("A")) {

			if ((displayCounter %2)==0) {

	  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
	}

  %>
      <td> <%=name%></td>
     	<td><%=description%></td>
	<td><A href="editglobalsetting.jsp?studyId=<%=studyId%>&mode=M&srcmenu=<%=src%>&protocolId=<%=id%>"><%=LC.L_ViewOrEdit_Settings%><%--View/Edit Settings*****--%></A></td>

      </tr>
      <%
	displayCounter++;
	} //for status check
	} //main loop
%>

    </table>
  </Form>
<%
	} //end of if body for page right
	else
	{
%>
	  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%	} //end of else body for page right

}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</div>
<div class ="mainMenu" id = "emenu">
 <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>
</html>
