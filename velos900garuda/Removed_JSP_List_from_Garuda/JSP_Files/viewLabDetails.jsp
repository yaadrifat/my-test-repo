<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<title><%=LC.L_Lab_DetsPage%><%--Lab Details Page*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

<style type="text/css">
.detailsRow td {background .color=#150517;

}
</style>
</head>
<body>
<%@ page
	import="com.velos.eres.service.util.*,com.velos.esch.business.common.ScheduleDao,com.velos.esch.business.protvisit.impl.ProtVisitBean,com.velos.eres.service.util.StringUtil, com.velos.remoteservice.lab.*,java.util.*, com.velos.remoteservice.*, com.velos.eres.business.perId.impl.*, com.velos.eres.service.util.EJBUtil, com.velos.eres.business.common.PerIdDao,com.velos.esch.business.common.EventdefDao"%>

<jsp:useBean id="personB" scope="request"
	class="com.velos.eres.web.person.PersonJB" />
<jsp:useBean id="eventdefB" scope="request"
	class="com.velos.esch.web.eventdef.EventdefJB" />
<jsp:useBean id="eventassocB" scope="request"
	class="com.velos.esch.web.eventassoc.EventAssocJB" />
<jsp:useBean id="protVisitJB" scope="request"
	class="com.velos.esch.web.protvisit.ProtVisitJB" />
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="serviceFactory" scope="session"
	class="com.velos.remoteservice.lab.LabServiceFactory" />
<!-- <jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/> -->
<%
	HttpSession tSession = request.getSession(true);
	 if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true" />

<%

  	String accountId = (String) tSession.getValue("accountId");
  	String perId = request.getParameter("patientId");
    int visitId = EJBUtil.stringToNum(request.getParameter("visitId"));
  	String patProtId = request.getParameter("patProtId");
    int pageRights = EJBUtil.stringToNum(request.getParameter("pageRights"));

   	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	personB.setPersonPKId(EJBUtil.stringToNum(perId));
	personB.getPersonDetails();

  	String MRN = personB.getPersonPId();
    if (MRN == null)
    {
%>
	<h3><%=MC.M_CntFind_PatMrn%><%--Could not find MRN for this <%=LC.Pat_Patient_Lower%>, cannot retrieve results.*****--%></h3>


<%
    }
    else //Communicate with Lab Service
    {
    %>

    <table width="40%">
        <tr>
     	  <td width ="15%"> <b><%=LC.L_Mrn%><%--MRN*****--%>: </b></td>
     	  <td width ="15%"><%=MRN%> </td>


       </tr>




     <%


    //Calculate the range of dates to send to lab.
    //The range will be composed of the union of ranges of the visit's events.
    Date rangeBeginDate = null;
    Date rangeEndDate = null;

    //go get visit details out of the protVisitJB
    protVisitJB.setVisit_id(visitId);
    ProtVisitBean visitB = protVisitJB.getProtVisitDetails();
    //number (not primary key) of the visit
    int visitNum = visitB.getVisit_no();
    //visit name
    String visitName = visitB.getName();
    //First, see if the events in the visit are completed. If they are, this
    //will be the first chance to define a range, using the completed date.

    //get the events for this visit (keep in mind that visitId is the number of the
    //visit within the protocol and NOT the primary key of the visits table...this seems
    //odd, but that's what eventdefB uses in it's query
    EventdefDao eventDefDAO =
              eventdefB.
                getVisitEvents(
                  EJBUtil.stringToNum(patProtId),
                  visitNum);


    ArrayList CPTCodes = new ArrayList();

    //store the resulting eventIds
    List event_ids = eventDefDAO.getEvent_ids();
    //loop through the events, search for completion
    for(int i=0; i < event_ids.size(); i++){

        String eventId = ((Integer)event_ids.get(i)).toString();



        //this call will populate eventDefDAO with status histories
        eventDefDAO.getEventStatusHistory(eventId);

        List statDates = eventDefDAO.getEventStatDates();
        List stats = eventDefDAO.getEventStats();

        //loop through eventStats...if one is complete, grab the date
        //from it!

        for (int x=0; x<stats.size(); x++){

          if ("Done".equalsIgnoreCase((String)stats.get(x))){
            System.out.println("State Date: " + statDates.get(x));
            try{
            	String statDateStr = (String)statDates.get(x);
            	if (statDateStr.length() == 0) continue;
            	java.sql.Date statDate =
            		java.sql.Date.valueOf(statDateStr.substring(0,10));

            	//found a completed event...resetting the range
	            rangeBeginDate = setDateIfEarlier(rangeBeginDate, statDate);
	            rangeEndDate = setDateIfLater(rangeEndDate, statDate);

            }
            catch(Exception e){
            	continue;
            }
            System.out.println(rangeBeginDate.toString() + " TO " + rangeEndDate.toString());
          }
        }



    }

    ScheduleDao schDao = new ScheduleDao();
    schDao.getVisitWindowDateRange(visitId, patProtId);
	/** start date from SCH.EVENTS1 **/
	ArrayList eventStartDates =
		schDao.getEventStartDates();


	/** beginning of window for this event **/
	ArrayList eventWindowStartDates =
		schDao.getEventWindowStart();

	/** unit for beginning of window for this event **/
	ArrayList eventWindowEndDates =
		schDao.getEventWindowEnd();

	ArrayList eventCPTs =
		schDao.getEventCPTs();

    for (int x=0; x < eventStartDates.size(); x++){
    	//first we'll set date range based on the scheduled date

    	rangeBeginDate = setDateIfEarlier(rangeBeginDate, (Date)eventStartDates.get(x));
    	rangeEndDate = setDateIfLater(rangeEndDate, (Date)eventStartDates.get(x));

    	//now we'll see if the visit window changes it at all
      	if (eventWindowStartDates.get(x) != null){
      		rangeBeginDate = setDateIfEarlier(rangeBeginDate, (Date)eventWindowStartDates.get(x));
      	}
      	if (eventWindowEndDates.get(x) != null){
      		rangeEndDate = setDateIfLater(rangeEndDate, (Date)eventWindowEndDates.get(x));
      	}
        String eventCPT = (String)eventCPTs.get(x);

        if (eventCPT != null && eventCPT.length() != 0){
        	CPTCodes.add(eventCPT);
        }
    }


   %>
		<tr>
     	  <td width ="15%"> <b><%=LC.L_Date_Range%><%--Date Range*****--%></b></td>
     	  <td width ="15%"><%=DateUtil.dateToString(rangeBeginDate)%> <%=LC.L_To_Lower%><%--to*****--%> <%=DateUtil.dateToString(rangeEndDate)%> </td>
       	</tr>

 		<tr>

   <%

        String className = null;
        try{
          ILabService labService = serviceFactory.getService();
          if (labService == null){
        	  %>MC.M_RemoteLabSrv_NotFound/*Remote Lab Service Not Found*****/ <%
        	  return;
          }
          if (labService != null){

            List tests =
            	labService.getPatientTests(
			        MRN,
	             	rangeBeginDate,
			        rangeEndDate,
			        CPTCodes);

				if (tests == null || tests.size() == 0){
              	%>MC.M_NoTestsFound/*No tests found*****/<%
            	return;
            }
%>

<div class="labTest">


<%
		//table of accessions and dates, so we know how many columns to build
		Hashtable accessions = new Hashtable();

		//matrix of ILabTestDetails to store rows...each entry in the
		//top table corresponds to a new row
		//main key is the concept id of the labtestdetail, so that we know
		//how to put multiple in the same row

		//second inner table is keyed on accession, so that while we're building
		//the row, we know if each cell needs a detail
		Hashtable testMatrix =
			new Hashtable();



            for(int x=0; x < tests.size(); x++){ //begin tests loop
              ILabTest test = (ILabTest)tests.get(x);
              String testName = test.getTestName();
			String accession = test.getAccession();
			accessions.put(accession, test);
			List testDetailList = test.getTestDetails();
	        for (int y=0; y<testDetailList.size(); y++){ //begin test details loop


            	ILabTestDetail testDetail = (ILabTestDetail)testDetailList.get(y);

	        	String testDetailConceptId =
	        		EJBUtil.integerToString(new Integer(testDetail.getConceptId()));

	        	//if this test wasn't in the last of requested CPT codes, don't add it
	        	if (!CPTCodes.contains(String.valueOf(testDetail.getConceptId()))){

	        		continue;
	        	}

	        	Hashtable conceptTable = (Hashtable)testMatrix.get(testDetailConceptId);
	        	if (conceptTable == null){
	        		conceptTable = new Hashtable();
	        		conceptTable.put(accession, testDetail);
	        		testMatrix.put(testDetailConceptId, conceptTable);
	        	}
	        	else{
	        		((Hashtable)testMatrix.get(testDetailConceptId)).put(accession, testDetail);
	        	}


	    	}//end test details loop
	     }//end test loop
	%>
	<%=visitName%>
<table class="reportBorder">
	<tr class="reportHeading">
		<th/>
		<% //build the header row with one th for each accession
			Iterator accessionIt = accessions.keySet().iterator();
			while(accessionIt.hasNext()){

				String accession = (String)accessionIt.next();
				Date accessionDate = ((ILabTest)accessions.get(accession)).getLastUpdateDate();
			%>

		<th style="width:175px">
				<%=accessionDate%>
			<br/>
				<%=LC.L_Acc_Upper%><%--ACC*****--%>#: <%=accession%>
		</th>

		<%
		}
		%>
	</tr>

	<%
			//build one row for each unique concept id
			int conceptCnt = 0;
			Iterator concepts = testMatrix.keySet().iterator();
			while(concepts.hasNext()){
				conceptCnt++;
	%>
				<% if(conceptCnt%2==0){ %>
			<tr class="browserEvenRow">
		<%}else{%>
			<tr class="browserOddRow">
				<% }
				String conceptId = (String)concepts.next();

				ILabTestDetail firstTestDetail = (ILabTestDetail)((Hashtable)testMatrix.get(conceptId)).values().toArray()[0];
				String firstTestName = (firstTestDetail == null) ? "" : firstTestDetail.getName();

				%>
				<td><%=firstTestName%></td>
				<%
				Hashtable accessionTestTable = (Hashtable)testMatrix.get(conceptId);

				Iterator accessionTestIt = accessionTestTable.keySet().iterator();
				while(accessionTestIt.hasNext()){
					String thisAccession = (String)accessionTestIt.next();
					ILabTestDetail testDetail =
						(ILabTestDetail)accessionTestTable.get(thisAccession);


					%>
					<td>
					<%
					if(testDetail != null){
					String overlibText =

						buildLabeDetailsStr((ILabTest)accessions.get(thisAccession), testDetail);
					%><%=testDetail.getResultValue()%>&nbsp;
					<img src="../images/jpg/help.jpg" border="0"
				onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(overlibText)%>',CAPTION,'<%=testDetail.getName()%>', RIGHT );"
				onmouseout="return nd();" align="left" alt=""></img>

					<%
					}
					else{
						%><%=" " %><%
					}
					%>
			</td>
				<%
				}
				%>

		</tr>
	<%
	        }
	%>

			</td>


</table>
<br/>


<%



          }//end if labservice != null
          else{
            %>LC.L_Lab_ServiceNull/*lab service null*****/<%
          }
        }
        catch(Exception e){
          e.printStackTrace();
          %><h2>LC.L_Error/*Error*****/: <%=e.getMessage()%></h2>
<%
        }
    }
    } //end else mrn !=null
	 //end of  if (sessionmaint.isValidSession(tSession))
%>
</div>
</head>
</body>
<div id="overDiv"
	style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"
	type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"
	type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<br>
</div>
<br />
<br />
</html>

<%!

/**
 *  Pads the front of an idD with 0s
 */
public String padId(String id){
  int idLen = id.length();
  int bal = 0;
	bal =10 - idLen;
	String zeroStr = "";

	for (int i=0;i < bal;i++)
	{
			zeroStr = zeroStr + "0";
	}
	return zeroStr + id;
}

/**
 *  Set a date if earlier
 */
 public Date setDateIfEarlier(
		 Date testDate,
		 Date dateToSet){
	if (testDate == null) return dateToSet;
    if (testDate.compareTo(dateToSet) < 0){
      //this is earlier, send it instead
      return testDate;
    }
    //not earlier, send dateToTest
    return dateToSet;
 }

/**
 *  Set a date if later
 */
 public Date setDateIfLater(
		 Date testDate,
		 Date dateToSet){
	if (testDate == null) return dateToSet;
    if (testDate.compareTo(dateToSet) > 0){
      //this is later, send it instead
      return testDate;
    }
    //not later, send dateToTest
    return dateToSet;
 }

/**
*	Creates a string to be used in the mouse-over pop-up
*
*/
public String buildLabeDetailsStr(
		ILabTest test,
		ILabTestDetail testDetails){


	StringBuilder out = new StringBuilder();
	out.append("<table style='width:500px'>");
	out.append("<th style='width:200px'/><th/>");
	out.append("<tr><td>"+LC.L_Accession/*Accession*****/+": </td><td> " + test.getAccession() +"</td></tr>");
	out.append("<tr><td>"+LC.L_Test_Name/*Test Name*****/+": </td><td>" + testDetails.getName() +"</td></tr>");
	out.append("<tr><td>"+LC.L_Test_ConceptId/*Test Concept ID*****/+": </td><td>" + testDetails.getConceptId() +"</td></tr>");
	out.append("<tr><td>"+LC.L_Subject_Name/*Subject Name*****/+": </td><td> " +"</td></tr>");
	out.append("<tr><td>"+LC.L_Specimen_Collection/*Specimen Collection*****/+": </td><td> " + test.getSpecimenCollectionDate() + "</td></tr>");
	out.append("<tr><td>"+LC.L_Order_Date/*Order Date*****/+": </td><td> " + test.getOrderDate() + "</td></tr>");
	out.append("<tr><td>"+LC.L_Last_Update/*Last Update*****/+": </td><td> " + test.getLastUpdateDate() + "</td></tr>");
	out.append("<tr><td>"+LC.L_Ordered_By/*Ordered By*****/+": </td><td> " + test.getOrderingProvider() + "</td></tr>");
	out.append("<tr><td>"+LC.L_Status/*Status*****/+": </td><td> " + testDetails.getStatus() + "</td></tr>");
	out.append("<tr><td>"+LC.L_Reference_Range/*Reference Range*****/+": </td><td> " + testDetails.getReferenceRange() + "</td></tr>");
	out.append("<tr><td>"+LC.L_Abnormal/*Abnormal*****/+": </td><td> " + testDetails.getAbnormalFlag() + "</td></tr>");
	out.append("<tr><td>"+LC.L_Comments/*Comments*****/+": </td><td> " + testDetails.getComment() + "</td></tr>");
	out.append("</table>");

	return out.toString();
}
%>