<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<%
	String fromPage = request.getParameter("fromPage");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {		
	%>
		<title> <%=MC.M_EvtLibDet_CopyEvt%><%--Event Library >> Event Details >> Copy Event*****--%></title>
	<%
	} else {%>
		<title><%=LC.L_Copy_Evt%><%--Copy Event*****--%></title>	
	<%
	}
%>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>



<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">


</head>



<% if ((request.getParameter("fromPage")).equals("selectEvent"))

{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->






 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))



	%>



<SCRIPT Language="javascript">



 function  validate(formobj){



//     formobj=document.select



     if (!(validate_col('Event Name',formobj.eventName))) return false

	 if (!(validate_col('Event Library Type',formobj.cmbLibType))) return false

	 if (!(validate_col('Category',formobj.categoryId))) return false



	 if (!(validate_col('Esign',formobj.eSign))) return false



	 if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

		formobj.eSign.focus();

		return false;

	}



   }

//KM-04Sep09
function callAjaxGetCatDD(formobj){
	   selval = formobj.cmbLibType.value;
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval+"&ddName=categoryId&codeType=L&ddType=category" ,
		   reqType:"POST",
		   outputElement: "span_catId" }

   ).startRequest();

}




</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>



<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>




<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*"%> <!--KM-->





<% String src;



src= request.getParameter("srcmenu");



%>



<% 


if ((request.getParameter("fromPage")).equals("selectEvent")){}

 else{

%>

<jsp:include page="panel.jsp" flush="true">



<jsp:param name="src" value="<%=src%>"/>



</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<%}%>





<body>



<% if ((request.getParameter("fromPage")).equals("selectEvent")){%>

		<DIV id="div1">

	<%	}

else { %>

<DIV class="formDefault" id="div1">

<%}%>

  <%



	String catId ="";

	String eventName ="";

	String eventLibTypeId = "";

	ArrayList catIds = new ArrayList();

	ArrayList names = new ArrayList();



	int length = 0;



	HttpSession tSession = request.getSession(true);





	if (sessionmaint.isValidSession(tSession))

	{

		String uName = (String) tSession.getValue("userName");
		String userId = (String) tSession.getValue("userId");
		String ipAdd = (String) tSession.getValue("ipAdd");

		String accId = (String) tSession.getValue("accountId");

		int accountId = EJBUtil.stringToNum(accId);



		//String oldEvent = (String) tSession.getValue("eventname");



	  	String eventId = request.getParameter("eventId");
		//String evtName = request.getParameter("eventName");
		String evtName = "";//KM





	  	String eventmode = request.getParameter("eventmode");

  		String duration = request.getParameter("duration");

	  	String protocolId = request.getParameter("protocolId");

  		String calledFrom = request.getParameter("calledFrom");

	  	String mode = request.getParameter("mode");   

	  	String calStatus= request.getParameter("calStatus");

		String from = request.getParameter("from");

	    catId = request.getParameter("categoryId");

		int pageRight = EJBUtil.stringToNum(request.getParameter("pageRight"));


		int saveTo = 0;


		if (calledFrom.equals("P")||calledFrom.equals("L"))
		{
			  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
			  eventdefB.getEventdefDetails();
			  evtName = eventdefB.getName();
//JM: 02DEC2009: #4515
		  	  evtName = (evtName==null)?"":evtName;
		  	  if (evtName.length()>1000){
 	      		evtName=evtName.substring(0,1000);
          	  }
		}else{
			  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
			  eventassocB.getEventAssocDetails();
			  evtName = eventassocB.getName();
//JM: 02DEC2009: #4515
		  	  evtName = (evtName==null)?"":evtName;
		  	  if (evtName.length()>1000){
 	      		evtName=evtName.substring(0,1000);
          	  }
		 }






		if (calledFrom.equals("P") || calledFrom.equals("L")) {

			saveTo = 1;

		} else if (calledFrom.equals("S")) {

			saveTo = 2;

		}



%>




<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="eventmode" value="<%=eventmode%>"/>
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="calStatus" value="<%=calStatus%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
<jsp:param name="selectedTab" value="1"/>
 </jsp:include>	

<!-- P class="sectionHeadings"> Event >> Copy </P-->



<%

if(from.equals("initial")) {

%>

<Form name="select" method="post" action="copyevent.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=<%=fromPage%>&from=final" onsubmit="return validate(document.select);">

<table>
	<tr> <td>
<P class = "defComments"><%Object[] arguments = {evtName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PlsSelEvtLib_Copied",arguments)%><%--Please select the Event Library Type to which Event '<%=evtName%>' is to be copied*****--%><FONT class="Mandatory">* </FONT>  </P>
</td>
	</tr>
</table>


<%

	//KM-03Sep09
	String evtLibType = request.getParameter("evtLibType");
	String libTypePullDn = "";
	SchCodeDao schDao = new SchCodeDao();
	schDao.getCodeValues("lib_type");
	libTypePullDn = schDao.toPullDown("cmbLibType",EJBUtil.stringToNum(evtLibType)," onChange=callAjaxGetCatDD(document.select);");

%>
<%=libTypePullDn%>

<table>
<tr> <td>
<P class = "defComments"><%Object[] arguments1 = {evtName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PlsSelEvnCat_Copied",arguments1)%><%--Please select the Event Category to which Event '<%=evtName%>'is to be copied*****--%><FONT class="Mandatory">* </FONT>  </P>
</td>
	</tr>
</table>


<%


String eventType="L";

ctrldao= eventdefB.getHeaderList(accountId,eventType,EJBUtil.stringToNum(evtLibType));



catIds=ctrldao.getEvent_ids() ;

names= ctrldao.getNames();



length = catIds.size();

%>


 <%-- <span id = "span_catId"> <%=categoryId%> </span>  --%>

<span id = "span_catId">
<select  name="categoryId">

<%

Integer currentId;

for(int i = 0; i< length;i++)

{

	currentId=(Integer)catIds.get(i);

	if(EJBUtil.integerToString(currentId).equals(catId))

{



%>

<option value=<%=catIds.get(i)%> selected=true> <%=names.get(i)%>



</option>



<% } else { %>



<option value=<%=catIds.get(i)%>> <%=names.get(i)%> </option>

<%

}

}

%>

</select>
</span>



<P class = "defComments"> <%=MC.M_PlsEtrName_NewEvt%><%--Please enter a name to be given to the New Event*****--%> <FONT class="Mandatory">* </FONT>  </P>

<input type="text" name="eventName" maxlength="1000" >
<br><br>
<%
String showSubmit = "";	
if(pageRight > 5)
	{ showSubmit = "Y";} else { showSubmit = "N";}
%>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="copyEvtFrm"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
</jsp:include>

</form>



<%

} //end of if for checking whether from.equals("initial")





if(from.equals("final")) {

   eventLibTypeId = request.getParameter("cmbLibType");

   eventId = request.getParameter("eventId");

   catId = request.getParameter("categoryId");

   eventName = request.getParameter("eventName");

   String eSign = request.getParameter("eSign");

	String oldESign = (String) tSession.getValue("eSign");



	if(!oldESign.equals(eSign)) {

%>





 <br> <br>

 <table width=100%>



<tr>

<td align=center>

<p class = "sectionHeadings">

<%=MC.M_EtrWrongEsign_ClkBack%><%--You have entered a wrong e-Signature. Please click on Back button to enter again.*****--%>

</p>

</td>

</tr>

<tr height=20></tr>

<tr>

<td align=center>

		<!--<button onclick="window.history.back();"><%=LC.L_Back%></button>-->
	<button onClick="history.go(-1);"><%=LC.L_Back%></button>

</td>

</tr>

</table>



<%

	} else {

  //KM- 03Sep09

if(eventdefB.CopyEventToCat(EJBUtil.stringToNum(eventId),saveTo,EJBUtil.stringToNum(catId),EJBUtil.stringToNum(eventLibTypeId),eventName,userId,ipAdd) == -1) {



%>



<Form name="name" method="post" action="copyevent.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=<%=fromPage%>&from=final" onsubmit="">

<input type=hidden name="eventId" value="<%=eventId%>">

<input type=hidden name="categoryId" value="<%=catId%>">

<P class = "defComments"> <%=MC.M_EvtName_AldyExst%><%--The Event name you have entered already exists. Please enter a new name.*****--%> </P>

<!--input type="text" name="eventName" value="<%=eventName%>"-->
<!-- <input type="Submit" name="submit" value="Go"> -->
<!--input type="image" src="../images/jpg/Go.gif" border="0" align="absmiddle"-->
<!--Added by Ganapathy Krishnan on 01June05-->
 <button onclick="window.history.back();"><%=LC.L_Back%></button>

<%

  } else {

%>

<P class = "defComments"> <%=MC.M_Evt_CopiedSucc%><%--The Event has been copied successfully.*****--%> </P>



<META HTTP-EQUIV=Refresh CONTENT="1;URL=eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=&protocolId=&calledFrom=L&mode=M&calStatus=W">



<%

}



}

}



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



 <%if ((request.getParameter("fromPage")).equals("selectEvent")){}

else {

%>

  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>



</div>

<div class ="mainMenu" id = "emenu">


	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>

	<% }%>

</body>

<%if ((request.getParameter("fromPage")).equals("selectEvent")){ %>

<button onclick="window.history.back();"><%=LC.L_Back%></button> <%}%>

</html>





