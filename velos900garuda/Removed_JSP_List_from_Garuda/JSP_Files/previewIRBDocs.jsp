<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.appendix.AppendixJB" %>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	
<html>
	<body>

	
		<%
			
			HttpSession sessObj = request.getSession(true);
			
			if (sessionmaint.isValidSession(sessObj))
			{
					String getUrl = "";
					String url = "";
					String selType = "";
					String studyId = "";
					
					String filename = "";
					int filePk = 0;
					String dnldurl  = "";
					int formFlagIndex = -1;
					Hashtable htResponseInfo = new Hashtable();
					
					String message=MC.M_PlsSel_DocuCat/*"Please Select Document Category"*****/;
					
					String appSubmissionType = "";
					
					
					selType = request.getParameter("selType");
					
					String study_acc_form_right = "";
					String study_team_form_access_right = "";
					
					study_acc_form_right = request.getParameter("study_acc_form_right");
					study_team_form_access_right = request.getParameter("study_team_form_access_right");
					
					if (StringUtil.isEmpty(selType ))
					{
						selType ="";
						
					}
					
					appSubmissionType = request.getParameter("appSubmissionType");
					
					if (StringUtil.isEmpty(appSubmissionType))
					{
						appSubmissionType = "";
						
					}
					
					
					studyId = request.getParameter("studyId");
					
					
					formFlagIndex = selType.indexOf("*");
					boolean hasFormResponse = false;
					String formPK = "";
					String formlibver = "";
					String pkstudyforms = "";
					
					boolean checkedFormList = false;
					
					
					if (formFlagIndex > -1 && selType.equals("AllCheck*F"))
					{
						checkedFormList = true;
					}
					
					if (formFlagIndex > -1 && checkedFormList == false)
					{
						formPK = selType.substring(0,formFlagIndex);
						//get form respnse information
						htResponseInfo  = lnkformB.getLatestStudyFormResponeData(formPK,studyId);	
		  					
		  				formlibver = (String) htResponseInfo.get("formlibver");	
		  				pkstudyforms = (String) htResponseInfo.get("pkstudyforms");	
		  				
		  				url = "formprint.jsp?formId="+formPK+"&filledFormId="+pkstudyforms+"&studyId="+studyId+"&formLibVerNumber="+formlibver+"&linkFrom=S";
		  				
		  				hasFormResponse = true;
					}
					
					if (formFlagIndex == -1 && checkedFormList == false && EJBUtil.stringToNum(selType) > 0)
					{
						AppendixJB aJB = new AppendixJB();
						
						AppendixDao apDao = new AppendixDao();
						apDao =	aJB.getLatestDocumentForCategory(studyId,selType);
						
						if (apDao != null && apDao.getCRows() > 0)
						{
							filename = (String)(apDao.getAppendixFile_Uris()).get(0);
							filePk = ((Integer)(apDao.getAppendixIds()).get(0)).intValue();
							message = LC.L_Loading_Document/*"Loading document*****/+".....";
						 } 
						else
						{
							
							message = MC.M_NoDocu_UploadCat/*"No documents uploaded for this Category"*****/;	
						}
					
					if (! StringUtil.isEmpty(filename) && filePk > 0)
					{
						
					com.aithent.file.uploadDownload.Configuration.readSettings("eres");
					com.aithent.file.uploadDownload.Configuration.readUploadDownloadParam(com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "study");		  
					dnldurl = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET ;
				
					url =  dnldurl +  "?file="+filename + "&pkValue="+filePk+
					"&tableName=ER_STUDYAPNDX&columnName=STUDYAPNDX_FILEOBJ&pkColumnName=PK_STUDYAPNDX&module=study&db=eres";
			
					}
						%>
							
							<%= message %>
								
							<%   
				   if (! StringUtil.isEmpty(url) && checkedFormList == false)
				   {
				   	%>
			            <form name="dummy" method="post" action="<%=dnldurl%>" target="_self">
				          <input type="hidden" name="file" value="<%=filename%>" />
		                  <input type="hidden" name="pkValue" value="<%=filePk%>" />
		                  <input type="hidden" name="tableName" value="ER_STUDYAPNDX" />
		                  <input type="hidden" name="columnName" value="STUDYAPNDX_FILEOBJ" />
		                  <input type="hidden" name="pkColumnName" value="PK_STUDYAPNDX" />
		                  <input type="hidden" name="module" value="study" />
		                  <input type="hidden" name="db" value="eres" />
			            </form>
				   		<script>
				   			document.dummy.submit();
				   		</script>
				   		<%
				   }
					
					
				}
				if (hasFormResponse && checkedFormList == false)
				{
					%>
			            <form name="dummy" method="post" action="formprint.jsp" target="_self">
				          <input type="hidden" name="formId" value="<%=formPK%>" />
		                  <input type="hidden" name="filledFormId" value="<%=pkstudyforms%>" />
		                  <input type="hidden" name="studyId" value="<%=StringUtil.htmlEncodeXss(studyId)%>" />
		                  <input type="hidden" name="formLibVerNumber" value="<%=formlibver %>" />
		                  <input type="hidden" name="linkFrom" value="S" />
			            </form>
				   		<script>
				   			document.dummy.submit();
				   		</script>
						<%
				}	
				
				if (checkedFormList == true)
				{
					%>
						<form name="viewcheck" method="post" action="viewSubmissionResponses.jsp" target="_self">
				          <input type="hidden" name="studyId" value="<%=StringUtil.htmlEncodeXss(studyId)%>" />
		                  <input type="hidden" name="appSubmissionType" value="<%=StringUtil.htmlEncodeXss(appSubmissionType)%>" />
		                  <input type="hidden" name="study_acc_form_right" value="<%=study_acc_form_right%>" />
		                  <input type="hidden" name="study_team_form_access_right" value="<%=study_team_form_access_right%>" />
		        	
		               </form>
				   		<script>
				   			document.viewcheck.submit();
				   		</script>
						
						
						<%
				}
			}			
			%>
	
	</body>
		</html>
		