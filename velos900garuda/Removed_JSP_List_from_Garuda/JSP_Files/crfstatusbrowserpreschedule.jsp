<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>

<head>

<title><%=LC.L_Crf_StatusBrowser%><%--CRF Status Browser*****--%></title>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crfStatB" scope="request" class="com.velos.esch.web.crfStat.CrfStatJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.esch.business.common.CrfStatDao"%>

<body>
<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
   	{
	 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
		String visit = request.getParameter("visit");
		String visitName = request.getParameter("visitName");
		
		String eventName = request.getParameter("eventname");
		String patProtId= request.getParameter("patProtId");
		String studyId = (String) tSession.getValue("studyId");	
		String statDesc=request.getParameter("statDesc");
		String statid=request.getParameter("statid");	
		String studyVer=request.getParameter("studyVer");

		String status="";

		String eventId = request.getParameter("eventId");
		tSession.setAttribute("eventId",eventId);
		
		CrfStatDao crfStatDao = new CrfStatDao();
		crfStatDao = crfStatB.getCrfValues(EJBUtil.stringToNum(patProtId), EJBUtil.stringToNum(visit)) ;
		int lenCrf = crfStatDao.getCRows();
		if(lenCrf==0){
		status="Checked=true";		
		}

		
		String yob = "";
		String age = "";		
		int personPK = 0;
		int patientMob=0;
		int patientDob=0;
		int patientYob=0;
		int patientAge=0;
		int sysYear=0;
		int sysMonth=0;
		int sysDate=0;
		int noOfMonths=0;
		int noOfYears=0;
		int noOfDays=0;
	
		personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	    personB.setPersonPKId(personPK);
 		personB.getPersonDetails();
		String uName = (String) tSession.getValue("userName");
		String patientId = personB.getPersonPId();
		String genderId = personB.getPersonGender();
		String dob = personB.getPersonDob();
		String gender = codeLst.getCodeDescription(EJBUtil.stringToNum(genderId));
		Calendar cal1 = new GregorianCalendar();		
		if(! StringUtil.isEmpty(dob)){
		//yob = dob.substring(6,10);			
		//age = (new Integer(cal1.get(Calendar.YEAR) - EJBUtil.stringToNum(yob))).toString();
		    
			//Added by Manimaran on 041805 for Fixing the Bug # 1928 (Age Calculation)
		      patientYob = EJBUtil.stringToNum(dob.substring(6,10));
		      patientMob = EJBUtil.stringToNum(dob.substring(0,2));
			  patientDob = EJBUtil.stringToNum(dob.substring(3,5));	
			  sysMonth=cal1.get(Calendar.MONTH)+1;
			  sysDate=cal1.get(Calendar.DATE);
			  sysYear=cal1.get(Calendar.YEAR);
			  if (sysYear==patientYob)
	              patientAge=0;
	          if (sysYear > patientYob)
	          {
				  patientAge=sysYear-patientYob;
                  if(patientMob > sysMonth)
		             patientAge--;
                  if(patientMob==sysMonth && patientDob>sysDate)
			         patientAge--;
	          }
    		  if(patientAge!=0)
				  age = String.valueOf(patientAge)+"&nbsp;"+LC.L_Years_Lower/*years"*****/;
			  if(patientAge==0)
			  {
				  if(patientMob <= sysMonth && sysDate>=patientDob)
					  noOfMonths=sysMonth-patientMob;
				  else if(patientMob<sysMonth && patientDob>sysDate)
					  noOfMonths=(sysMonth-patientMob)-1;	
				  else if (patientMob>=sysMonth&&patientDob<=sysDate)
				  {
					  noOfMonths=(patientMob-sysMonth);
					  noOfMonths=12-(noOfMonths);
				  }
				  else if(patientMob>=sysMonth&&patientDob>sysDate)
				  {  
					  noOfMonths=patientMob-sysMonth;
					  noOfMonths=11-(noOfMonths);
				  }
				  age=String.valueOf(noOfMonths)+"&nbsp;"+LC.L_Months_Lower/*months"*****/;
			  }
			  if(patientAge==0 && noOfMonths==0)
			  {
				  if(patientDob<=sysDate)
					  noOfDays=(sysDate-patientDob)+1;
				  else 
				  {
					  noOfDays=sysDate-patientDob;
					  if (patientMob==1)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==2)
					  {
						  noOfDays=28-(-noOfDays);
						  if(sysYear%4==0 && (sysYear % 100 != 0 || sysYear % 400 == 0))
							  noOfDays=29-(-noOfDays);
					  }	
					  if (patientMob==3)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==4)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==5)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==6)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==7)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==8)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==9)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==10)
						  noOfDays=31-(-noOfDays);
					  if (patientMob==11)
						  noOfDays=30-(-noOfDays);
					  if (patientMob==12)
						  noOfDays=31-(-noOfDays);
					   noOfDays=noOfDays+1;
				  }
				  age= String.valueOf(noOfDays)+"&nbsp;"+LC.L_Days_Lower/*days"*****/;
			  }
		} else {
			age = "-";
		}		
		
		if (StringUtil.isEmpty(gender) )
		{
			gender = "-";
		}
		if(eventName == null){
			eventName = "";
		} else {
			eventName = eventName.replace('~',' ');
		}			
		
%>
  <table width="100%" cellspacing="0" cellpadding="0">
	<tr>
      <td width= 50% align=right> 
		<button onclick="self.close(); return false;"><%=LC.L_Close%></button> 
      </td>
	  <td width= 50% align=right> 
		<button onclick="history.back(); return false;"><%=LC.L_Close%></button>
      </td> 
      </tr>
  </table>
<br>
		<table border=0 cellPadding=0 cellSpacing=0 width="100%" bgcolor="Silver">
			<TR>
			 	<td class=tdDefault width=20%>	<B><%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> Id*****--%>:</B> <%=patientId%> </td>
				<td class=tdDefault width=20%>	<B> <%=LC.L_Age%><%--Age*****--%>: </B> <%=age%>	</td>
			</TR>
			<TR>
				<td class=tdDefault width=20%>	<B><%=LC.L_Gender%><%--Gender*****--%>: </B> <%=gender%> 	</td>
				<td class=tdDefault width = 20% ><B><%=LC.L_Patient_Visit%><%--<%=LC.Pat_Patient%> Visit*****--%>: </B> <%=visitName%></td>
			</TR>			
			</TR>
		</table>


<TABLE border=0 cellPadding=1 cellSpacing=1 width="100%">
  <TR>
    <TH><p class = "reportHeadings"><%=LC.L_Date%><%--Date*****--%></p></TH>
    <TH><p class = "reportHeadings"><%=LC.L_Status%><%--Status*****--%></p></TH>
    <TH><p class = "reportHeadings"><%=MC.M_RevApp_RejBy%><%--Reviewed/ Approved/ Rejected By*****--%></p></TH>
    <TH><p class = "reportHeadings"><%=MC.M_RevApp_RejOn%><%--Reviewed/ Approved/ Rejected On*****--%></p></TH>
    <TH><p class = "reportHeadings"><%=LC.L_Status%><%--Status*****--%></p></TH>
    <TH><p class = "reportHeadings"><%=LC.L_Sent_By%><%--Sent By*****--%></p></TH>
    <TH><p class = "reportHeadings"><%=LC.L_Sent_On%><%--Sent On*****--%></p></TH>	
  </TR>	 

<% 


  

  
  ArrayList crfNumbers = null;
  ArrayList crfNames = null;
  ArrayList crfEnterBys = null; 
  ArrayList crfEnterOns = null;  
  ArrayList crfStats = null;
  ArrayList crfReviewBys = null;
  ArrayList crfReviewOns = null;
  ArrayList crfSentTos = null;
  ArrayList crfSentBys = null;
  ArrayList crfSentOns = null;
  ArrayList crfIds = null;
  ArrayList crfStatIds = null;
  
  
  crfNumbers = crfStatDao.getCrfNumbers();
  crfNames = crfStatDao.getCrfNames();
  crfEnterBys = crfStatDao.getCrfEnterBys();
  crfEnterOns = crfStatDao.getCrfEnterOns();  
  crfStats = crfStatDao.getCrfStats();
  crfReviewBys = crfStatDao.getCrfReviewBys();
  crfReviewOns = crfStatDao.getCrfReviewOns();
  crfSentTos = crfStatDao.getCrfSentTos();
  crfSentBys = crfStatDao.getCrfSentBys();
  crfSentOns = crfStatDao.getCrfSentOns();
  crfIds = crfStatDao.getCrfIds();
  crfStatIds = crfStatDao.getCrfStatIds();
  
// Getting the size of the arrays.
//  int lenCrf = crfNumbers.size();
//  int lenCrf = crfStatDao.getCRows();
//  out.println("****" + lenCrf + "****");
  String crfNum = "";
  String precrfNumber = "";
  
	for(int i=0; i<lenCrf ; i++) 
	{
//out.println(i);
		String crfNumber = (String)crfNumbers.get(i); 
		String crfName = (String) crfNames.get(i);
		String crfEnterBy = (String)crfEnterBys.get(i);
		String crfEnterOn = (String) crfEnterOns.get(i);		
		String crfStat = (String) crfStats.get(i);
		String crfReviewBy = (String)crfReviewBys.get(i);
		String crfReviewOn = (String) crfReviewOns.get(i);
		String crfSentTo = (String) crfSentTos.get(i);
		String crfSentBy = (String)crfSentBys.get(i);
		String crfSentOn = (String) crfSentOns.get(i);
		String crfId = ((Integer) crfIds.get(i)).toString();
		String crfStatId = ((Integer) crfStatIds.get(i)).toString();
		
		crfNumber = (crfNumber == null)?"-":crfNumber;
		crfName = (crfName == null)?"-":crfName;
		crfEnterBy = (crfEnterBy == null)?"-":crfEnterBy;
		crfEnterOn = (crfEnterOn == null)?"-":crfEnterOn;		
		crfSentTo = (crfSentTo == null)?"-":crfSentTo;
		crfSentBy = (crfSentBy == null)?"-":crfSentBy;
		crfSentOn = (crfSentOn == null)?"-":crfSentOn;
		crfReviewBy = (crfReviewBy == null)?"-":crfReviewBy;
		crfReviewOn = (crfReviewOn == null)?"-":crfReviewOn;
		crfStat = (crfStat == null)?"-":crfStat;
		crfId = (crfId == null)?"-":crfId;		
		crfStatId = (crfStatId == null)?"-":crfStatId;
		
		if(!(crfNumber.equals(precrfNumber)))
		{
%>
<tr>
	<TD class=tdDefault colspan=5>
		<B><%=LC.L_Crf_Num%><%--Crf Number*****--%>: <%=crfNumber%>
		<BR>
		<%=LC.L_Crf_Name%><%--Crf Name*****--%>: <%=crfName%> </B> 
	</TD>
	
</tr>
<%
		}
%>

 <!-- place per > here in case even odd row doesn't work -->
<%	 
	if(i%2==0){ 
%>	
	<TR class="browserEvenRow">	
<%
	}else{
%>
	<TR class="browserOddRow">
<% 
	} 
%>
	 
	<td class=tdDefault><%=crfEnterOn%></td>
<%
		eventName = eventName.replace(' ','~');
%>	
	<td class=tdDefault><%=crfStat%></td>
<%
		eventName = eventName.replace('~',' ');
%>	
	<td class=tdDefault><%=crfReviewBy%></td>
	<td class=tdDefault><%=crfReviewOn%></td>			 
	<td class=tdDefault><%=crfSentTo%></td>
	<td class=tdDefault><%=crfSentBy%></td>
	<td class=tdDefault><%=crfSentOn%></td>
	
</tr>
		
	<%
	precrfNumber=crfNumber;
%>

<%	
	}
  %>
  
  </table>

<%  
 
} //end of if session times out

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%

} //end of else body for session times out

%>
</body>

</html>


