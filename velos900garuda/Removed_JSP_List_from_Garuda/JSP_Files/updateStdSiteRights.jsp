<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>
<jsp:useBean id ="stdSiteB" scope="request" class="com.velos.eres.web.stdSiteRights.StdSiteRightsJB"/>
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="teamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<%@ page language = "java" import = "com.velos.eres.business.common.UserSiteDao,java.util.*,com.velos.eres.service.util.*"%>
	
<%

int ret = 0;
String rights[] = null;
String pkStdSites[] = null;
String siteIds[] = null;
int totrows =0; 

String eSign = request.getParameter("eSign");
String calledFrom  = request.getParameter("calledFrom");
if(calledFrom == null)
	calledFrom = "";

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))  {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
  	String oldESign = (String) tSession.getValue("eSign");
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	
 String ipAdd = (String) tSession.getValue("ipAdd");
 int usr = EJBUtil.stringToNum(request.getParameter("userId"));
 int teamId = EJBUtil.stringToNum(request.getParameter("teamId"));
 String strSiteFlag = request.getParameter("siteFlag");
 String creator =	(String) tSession.getValue("userId");
 String studyId = request.getParameter("studyId");
 
 teamB.setId(teamId);
 teamB.getTeamDetails();
 teamB.setSiteFlag(strSiteFlag);
 teamB.updateTeam();

 totrows = Integer.parseInt(request.getParameter("totalrows")); //Total Rows
if (totrows > 1){
	pkStdSites = request.getParameterValues("pkStdSite");
	rights = request.getParameterValues("rights");
	siteIds = request.getParameterValues("siteIds");
	
}else {
    pkStdSites = new String[1];
    rights = new String[1];
	siteIds = new String[1];
	pkStdSites[0] = request.getParameter("pkStdSite");
	rights[0] = request.getParameter("rights");
	siteIds[0] = request.getParameter("siteIds");
}



/*for(int i=0;i<pkStdSites.length;i++)
{
out.println("pkStdSites::"+pkStdSites[i]);
out.println("siteIds::"+siteIds[i]);
out.println("rights::"+rights[i]);

}*/
//pass array of pkUserSites and array of corresponding rights
	
		ret = stdSiteB.updateStudySite(pkStdSites,siteIds,rights,EJBUtil.stringToNum(studyId),usr,ipAdd,creator); 
	
	
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
<script>
setTimeout("self.close()",1000); //wait for sometime for showing  msg then close
</script>
<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>

</BODY>

</HTML>





