<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%=MC.M_Del_EvtFromCal%><%--Delete Event from Calendar*****--%></title>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY> 
<br>

<DIV class="formDefault" id="div1">

<% 
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
	//String eventId =request.getParameter("eventId");
	String selStrs =request.getParameter("selStrs");//KM
	String tableName =request.getParameter("tableName");	
	String selectedTab=request.getParameter("selectedTab");
	String duration=request.getParameter("duration");
	String protocolId=request.getParameter("protocolId");
	String calledFrom=request.getParameter("calledFrom");
	String mode=request.getParameter("mode");
	String calStatus=request.getParameter("calStatus");
	
	String calAssoc=request.getParameter("calassoc");
    calAssoc=(calAssoc==null)?"":calAssoc;
    /*YK 10Jan - Bug #5725*/
    String resetSort="";
	resetSort = request.getParameter("resetSort"); 
	resetSort = (resetSort==null)?"":resetSort;
	
	
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>
	
	<FORM name="deleteFile" id="delevtfrmprot" method="post" action="deleteeventfromprot.jsp" onSubmit="if (validate(document.deleteFile)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="delevtfrmprot"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	
  	 <input type="hidden" name="tableName" value="<%=tableName%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
  	 <input type="hidden" name="duration" value="<%=duration%>">
  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">
  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">	 	 	 	 	 	 	 	 	 	 
   	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">	
   	 <input type="hidden" name="selStrs" value="<%=selStrs%>"> <!-- KM --> 	 
   	 <input type="hidden" name="resetSort" value="<%=resetSort%>">  	<%--YK 10Jan - Bug #5725  --%> 	 	 	 	 	 	 	 
 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
					
				StringTokenizer idStrs=new StringTokenizer(selStrs,",");
				int numIds=idStrs.countTokens();
				String[] strArrStrs = new String[numIds] ;
				for(int cnt=0;cnt<numIds;cnt++)
				{			
					strArrStrs[cnt] = idStrs.nextToken();	
				}
				//KM-Modified for Enh.#C7, Modified for INF-18183 ::: Ankit
				String moduleName = (calledFrom.equalsIgnoreCase("P"))? LC.L_Cal_Lib : LC.L_Study_Calendar;
				int i=eventdefB.RemoveProtEventInstance(strArrStrs,tableName,AuditUtils.createArgs(session,"",moduleName) );
%> 
<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_EvtDelSucc_FromPcol%><%--Event deleted successfully from Protocol.*****--%> </p>
<%--YK 10Jan - Bug #5725  --%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventbrowser.jsp?selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>&resetSort=<%=resetSort%>">

<% 
	} //end esign
	} //end of delMode	
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>

</BODY>
</HTML>

