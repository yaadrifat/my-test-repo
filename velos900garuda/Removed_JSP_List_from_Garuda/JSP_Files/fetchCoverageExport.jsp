<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.eres.service.util.Configuration,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.eres.service.util.Security"%>
<%@page import="com.velos.esch.web.protvisit.ProtVisitCoverageJB"%>
<%@page import="com.velos.esch.service.util.Rlog"%>
<%@page import="java.io.FileOutputStream,java.io.OutputStreamWriter"%>
<%@page import="java.text.DecimalFormat,java.util.ArrayList"%>
<%@page import="org.json.JSONObject,org.json.JSONArray,org.json.JSONException,com.velos.eres.service.util.*"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<html>
<head><title><%=LC.L_Coverage_AnalysisExport%><%--Coverage Analysis Export*****--%></title></head>
<body>
<%
HttpSession tSession = request.getSession(true);
String sessId = tSession.getId();
if (sessId.length()>8) { sessId = sessId.substring(0,8); }
sessId = Security.encrypt(sessId);
char[] chs = sessId.toCharArray();
StringBuffer sb1 = new StringBuffer();
DecimalFormat df = new DecimalFormat("000");
for (int iX=0; iX<chs.length; iX++) {
    sb1.append(df.format((int)chs[iX]));
}
String keySessId = sb1.toString();

if (!keySessId.equals(request.getParameter("key"))) {
%>
    <h1><%=LC.L_Forbidden%><%--Forbidden*****--%></h1>
    <p><%=MC.M_NotPerm_ToAcesServer%><%--You do not have permission to access this server or file.*****--%></p>
    </body>
    </html>
<%
    return;
}
if (sessionmaint.isValidSession(tSession)) {
	ProtVisitCoverageJB pvCoverageJB = new ProtVisitCoverageJB();
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
    int pageRight = 0;
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}
	if (pageRight > 0) {
		String jsString = null;
		JSONObject jsObj = null;
		try {
		    jsString = pvCoverageJB.fetchCoverageJSON(protocolId, calledFrom);
			jsObj = new JSONObject(jsString);
		} catch(Exception e) {
		    Rlog.fatal("fetchCoverageExport", "Error while calling fetchCoverageJSON "+e);
		}
		if (jsObj == null) { jsObj = new JSONObject(); }
		String calName = (String)jsObj.get("protocolName");
		JSONArray colArray = (JSONArray)jsObj.get("colArray");
		JSONArray dataArray = (JSONArray)jsObj.get("dataArray");
		String format = request.getParameter("format");
		String filename = null;
		String contentApp = null;
		if ("excel".equals(format)) {
		    filename = "reportexcel["+System.currentTimeMillis()+"].xls" ;
		    contentApp = "application/vnd.ms-excel; ";
		} else if ("word".equals(format)) {
		    filename = "reportword["+System.currentTimeMillis()+"].doc" ;
		    contentApp = "application/vnd.ms-word; ";
		} else if ("html".equals(format)) {
		    filename = "reporthtml["+System.currentTimeMillis()+"].html" ;
		    contentApp = "text/html; ";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("<HTML><HEAD>");
		sb.append("<META http-equiv=\"Content-Type\" content=\"").append(contentApp).append("charset=UTF-8\"/>");
		sb.append("</HEAD>");
		sb.append("<BODY>");
		sb.append("<table><tr><td colspan=\"3\"><p style=\"font-weight:bold;background-color: #e1dce0;\">"+MC.M_Cvrg_AnalyForCal/*Coverage Analysis for Calendar*****/+": ").append(calName).append("</p></td></tr></table>");
		if (dataArray.length() > 0) {
		    ArrayList visitIdList = new ArrayList();
		    sb.append("<TABLE BORDER='1' ALIGN='CENTER' WIDTH='100%'>");
		    if (colArray.length() > 0) {
		        sb.append("<tr>");
		        for (int iX=0; iX<colArray.length(); iX++) {
		            String key = (String)((JSONObject)colArray.get(iX)).get("key");
		            if ("eventId".equals(key)) { continue; }
		            sb.append("<th>").append(((JSONObject)colArray.get(iX)).get("label")).append("</th>");
		            if (key.startsWith("v")) { visitIdList.add(key); }
		        }
		        sb.append("</tr>");
		    }
		    int noteCounter = 0;
		    ArrayList notesList = new ArrayList();
		    for (int iX=0; iX<dataArray.length(); iX++) {
		        JSONObject rowData = (JSONObject)dataArray.get(iX);
			    sb.append("<tr>");
			    sb.append("<td>").append(rowData.get("event")).append("</td>");
			    for (int iV=0; iV<visitIdList.size(); iV++) {
			        sb.append("<td>");
			        String cellData = null;
			        try { cellData = (String)rowData.get((String)visitIdList.get(iV)); } catch(JSONException e) {}
			        if (cellData != null) {
			            if (!cellData.startsWith("<b>"+LC.L_X/*X*****/+"</b>")) { cellData = "<b>"+LC.L_X/*X*****/+"</b> "+cellData;}
			            sb.append(cellData);
			        }
			        String notesData = null;
			        try { notesData = (String)rowData.get("notes_"+visitIdList.get(iV)); } catch(JSONException e) {}
			        if (notesData != null && notesData.trim().length() > 0) {
			            sb.append(" &#167;<sup>").append(++noteCounter).append("</sup>");
			            notesList.add(notesData);
			        }
			        if (cellData == null && (notesData == null || notesData.trim().length() == 0)) {
			            sb.append("&nbsp;");
			        }
			        sb.append("</td>");
			    }
		        sb.append("</tr>");
		    }
		    sb.append("</TABLE>");
		    sb.append("<br/><table><tr><td><b>X</b>="+LC.L_Evt_VstSel/*Event-Visit selected*****/+"</td></tr></table>");
		    sb.append("<br/><table><tr><td colspan=\"3\">").append(jsObj.get("covTypeLegend")).append("</td></tr></table>");
		    if (notesList.size() > 0) {
		        sb.append("<br/><table>");
		        sb.append("<tr><td colspan=\"3\">&#167;</td></tr>");
		        for (int iX=0; iX<notesList.size(); iX++) {
		            sb.append("<tr valign='top'><td align='left' colspan=\"3\">").append(iX+1).append(" &ndash; ");
		            sb.append((String)notesList.get(iX)).append("</td></tr>");
		        }
		        sb.append("</table>");
		    }
		}
		sb.append("</BODY></HTML>");
		Configuration.readAppendixParam(Configuration.ERES_HOME+"eresearch.xml");
		FileOutputStream fos = null;
		OutputStreamWriter osw = null; 
		try {
	        String filepath = EJBUtil.getActualPath(Configuration.DOWNLOADFOLDER, filename);
		    fos = new FileOutputStream(filepath);
		    osw = new OutputStreamWriter(fos);
		    char[] c = sb.toString().toCharArray(); 
		    osw.write(c, 0, c.length);
		    osw.flush();
		    fos.flush();
		} catch(Exception e) {
		    Rlog.fatal("fetchCoverageExport", "Error creating download file: "+e);
		} finally {
		    try { osw.close(); } catch(Exception e) {}
		    try { fos.close(); } catch(Exception e) {}
		}
%>
<form name="dummy" method="post" action="/velos/servlet/Download" target="_self">
<input type ="hidden" name="file" id="file" value="<%=filename%>"/>
<input type ="hidden" name="mod" id="mod" value="R"/>
</form>
<script>
document.dummy.submit();
</script>
<%
	} // end of pageRight
	else {
	%>
		<jsp:include page="accessdenied.jsp" flush="true"/>
	<%
	} // end of else of pageRight
} else { // else of valid session
%>
<jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
<div> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>
</body>
</html>
