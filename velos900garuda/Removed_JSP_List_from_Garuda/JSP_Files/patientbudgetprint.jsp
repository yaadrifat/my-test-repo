<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Pat_Bgt%><%--<%=LC.Pat_Patient%> Budget*****--%></title>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>




<%@ page import="java.util.*,java.lang.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.budgetcal.BudgetcalStateKeeper,com.velos.eres.service.util.*,java.lang.*,java.math.BigDecimal" %>
<%@ page import="com.velos.eres.service.util.VelosResourceBundle"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="budgetB" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budgetcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id="lineitemB" scope="request" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.esch.business.common.SchCodeDao"/>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0"  style = "overflow:auto">
<%
	} else {
%>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0">
<%
	}
%>

<Br>
<div class="popDefault">
<%
	HttpSession tSession = request.getSession(true);

	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");

	if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

		SchCodeDao schD = new SchCodeDao();
		int perCodeId = schD.getCodeId("category","ctgry_per");
		int iCatId = 0;
		int budgetId = EJBUtil.stringToNum(request.getParameter("budgetId"));

		String calId = request.getParameter("bgtcalId");

		int bgtcalId = EJBUtil.stringToNum(calId);

		SchCodeDao schCodeDao = new SchCodeDao();
		String type = request.getParameter("type");

		BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);
		ArrayList bgtNames = budgetDao.getBgtNames();
		ArrayList bgtStats = budgetDao.getBgtStats();
		ArrayList bgtStudyTitles = budgetDao.getBgtStudyTitles();
		ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
		ArrayList bgtSites = budgetDao.getBgtSites();
		ArrayList bgtCurrs = budgetDao.getBgtCurrs();

		String bgtName = (String) bgtNames.get(0);
		String bgtStat = (String) bgtStats.get(0);
		String bgtStudyTitle = (String) bgtStudyTitles.get(0);
		String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
		String bgtSite = (String) bgtSites.get(0);
		String currency = "";

		if (bgtCurrs.size() >0){
			String bgtCurr = (String) bgtCurrs.get(0);

			int codeId = Integer.parseInt(bgtCurr);

			currency = codeLst.getCodeDescription(codeId);
		}

		bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
		bgtStat = (bgtStat == null || bgtStat.equals(""))?"-":bgtStat;
		if(bgtStat.equals("F")) {
			bgtStat = LC.L_Freeze/*"Freeze"*****/;
		} else if(bgtStat.equals("W")) {
			bgtStat = LC.L_Work_InProgress/*"Work in Progress"*****/;
		}else if(bgtStat.equals("T")) {
			bgtStat = LC.L_Template/*"Template"*****/;
		}

		bgtStudyTitle = (bgtStudyTitle == null || bgtStudyTitle.equals(""))?"-":bgtStudyTitle;
		bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
		bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;

		BudgetcalDao budgetcalDao = budgetcalB.getBgtcalDetail(bgtcalId);
		ArrayList bgtcalNames = budgetcalDao.getBgtcalProtNames();


		String calName = (String) bgtcalNames.get(0);

			LineitemDao lineitemDao = lineitemB.getLineitemsOfBgtcal(bgtcalId);

			ArrayList lineitemIds = lineitemDao.getLineitemIds();
            ArrayList lineitemNames = lineitemDao.getLineitemNames();

			ArrayList lineitemDescs = lineitemDao.getLineitemDescs();
            ArrayList lineitemSponsorUnits = lineitemDao.getLineitemSponsorUnits();

            ArrayList lineitemClinicNOfUnits = lineitemDao.getLineitemClinicNOfUnits();

            ArrayList lineitemDelFlags = lineitemDao.getLineitemDelFlags();
            ArrayList lineitemInPerSecs = lineitemDao.getLineitemInPerSecs();

			ArrayList lineitemAppIndirects = lineitemDao.getLineitemAppIndirects();


            ArrayList bgtSectionIds = lineitemDao.getBgtSectionIds();

            ArrayList bgtSectionNames = lineitemDao.getBgtSectionNames();
            ArrayList bgtSectionDelFlags = lineitemDao.getBgtSectionDelFlags();
            ArrayList bgtSectionSequences = lineitemDao.getBgtSectionSequences();

            ArrayList bgtSectionPatNos = lineitemDao.getBgtSectionPatNos();

            ArrayList bgtSectionPersonlFlags = lineitemDao.getBgtSectionPersonlFlags();
            ArrayList bgtSectionTypes = lineitemDao.getBgtSectionTypes();

			ArrayList lineitemStdCareCosts = lineitemDao.getLineitemStdCareCosts();


			ArrayList lineitemResCosts = lineitemDao.getLineitemResCosts();

 			ArrayList lineitemInCostDiscs =	lineitemDao.getLineitemInCostDiscs();

			ArrayList lineitemCptCodes = lineitemDao.getLineitemCptCodes();

			ArrayList lineitemCategories = lineitemDao.getLineitemCategories();


		int rows = 0;




		for(int i=0;i<lineitemDao.getBRows();i++) {
			if(lineitemNames.get(i) != null && !lineitemNames.get(i).equals("")){
				rows++;
			}
		}

				int totalrows = lineitemDao.getBRows();

			int lineitemId = 0;
			String lineitemName = "";
			String lineitemDesc = "";
			String lineitemSponsorUnit = "0.00";
			String lineitemClinicNOfUnit = "0.00";

			String lineitemStdCareCost = "";
			String lineitemResCost = "0.00";
 			String lineitemInCostDisc = "0";
			String lineitemCptCode = "";
			String lineitemDelFlag = "";
			String lineitemInPerSec = "";
			int bgtSectionId = 0;
			String bgtSectionName = "";
			String bgtSectionDelFlag = "";
			String bgtSectionSequence = "";
			String bgtSectionPatNo = "";
			String bgtSectionPersonlFlag = "";
			String lineitemAppIndirect = "";
			String bgtSectionType = "";
			String bgtSectionTypeRow = "";
			String ttlCost = "";
			String resFringe = "";
			String ttlFringe = "";

			String resDisc = "0.00";
			String ttlDisc = "0.00";

			String indResTotal = "0.00";
			String indTtlTotal = "0.00";

			int bgtSecNextId = 0;

			String salResTotal = "0.00";
			String salTtlTotal = "0.00";

			String resAfterFringe = "0.00";
			String ttlAfterFringe  = "0.00";
			//String stdCareAfterFringe = "0.00";

			String totalResearchCost = "0.00", grandTotal = "0.00";
			String totalAppIndResCost = "0.00";
			String totalIndNotApplied = "0.00";

			String totalAppIndTtlCost = "0.00";
			String totalTtlIndNotApplied = "0.00";
			//String totalStdCareCost = "0.00";
			String totalTtlCost = "0.00";
			String subTotalPatResearchCost = "0.00";
			String subTotalPatAppIndResCost = "0.00";
			String subTotalPatIndNotApplied = "0.00";
//			String subTotalPatStdCareCost ="0.00";
			String subTotalPatTtlCost = "0.00";
			String subTotalPatAppIndTtlCost = "0.00";

			String subTotalPatTtlIndNotApplied = "0.00";

			String subTotalResearchCost = "0.00";
			String subTotalAppIndResCost = "0.00";
			String subTotalIndNotApplied = "0.00";

			String subTotalAppIndTtlCost = "0.00";
			String subTotalTtlIndNotApplied = "0.00";
//			String subTotalStdCareCost ="0.00";
			String subTotalTtlCost = "0.00";

			String totalPatResearchCost = "0.00";
			String totalPatAppIndResCost = "0.00";
			String totalPatIndNotApplied = "0.00";

			String totalPatTtlCost = "0.00";
			String totalPatAppIndTtlCost = "0.00";
			String totalPatTtlIndNotApplied = "0.00";
//    		String totalPatStdCareCost = "0.00";
			String aftercost = "0.00";
			String afterTtlCost = "0.00";
			String afterstdcost =  "0.00";
			String ctgryPullDn = "";
			String categoryDesc = "";
			String categoryId =  "";
			String fringeResTotal = "0.00";
			String fringeTtlTotal = "0.00";
			String discResTotal = "0.00";
			String discTtlTotal = "0.00";
			SchCodeDao schDao = new SchCodeDao();
			schDao.getCodeValues("category");

			budgetcalB.setBudgetcalId(bgtcalId);
			BudgetcalStateKeeper bcsk = budgetcalB.getBudgetcalDetails();
			String sponsorOHead = budgetcalB.getSpOverHead();
			String sponsorOHeadApply = budgetcalB.getSpFlag();
			String clinicOHead = budgetcalB.getClOverHead();
			String clinicOHeadApply = budgetcalB.getClFlag();

			String fringeBenefit = budgetcalB.getBudgetFrgBenefit();
			String fringeBenefitApply = budgetcalB.getBudgetFrgFlag();
			String costDiscount = budgetcalB.getBudgetDiscount();
			String costDiscountApply = budgetcalB.getBudgetDiscountFlag();

			sponsorOHead = (sponsorOHead == null)?"0.0":sponsorOHead;
			clinicOHead = (clinicOHead == null)?"0.0":clinicOHead;
			sponsorOHeadApply = (sponsorOHeadApply == null)?"N":sponsorOHeadApply;
			clinicOHeadApply = (clinicOHeadApply == null)?"N":clinicOHeadApply;
			fringeBenefit = (fringeBenefit == null)?"0.0":fringeBenefit;
			costDiscount = (costDiscount == null)?"0.0":costDiscount;
			fringeBenefitApply = (fringeBenefitApply == null)?"0":fringeBenefitApply;
			costDiscountApply = (costDiscountApply == null)?"0":costDiscountApply;

		calName = (calName == null)?"-":calName;


		String itemId = "";

		StringBuffer htmlString=new StringBuffer();
		htmlString.append("<BR>");
		htmlString.append("<Form name='budget' method='post' action=''>");
		htmlString.append("<table width='100%' cellspacing='0' cellpadding='0'>");
		htmlString.append("<tr>");
		htmlString.append("<td class=tdDefault width='50%'>");
		htmlString.append("<B>");
		htmlString.append(LC.L_Budget_Name/*"Budget Name*****/+":</B>&nbsp;&nbsp;" +bgtName + "</td>");
		htmlString.append("<td class=tdDefault width='50%'><B> "+LC.L_Organization/*Organization*****/+":</B>&nbsp;&nbsp;"+bgtSite+"</td>");
		htmlString.append("</tr>");
	    htmlString.append("<tr>");
		htmlString.append("<td class=tdDefault width='50%'><B>"+LC.L_Study_Number/*+LC.Std_Study+" Number*****/+":</B>&nbsp;&nbsp;"+bgtStudyNumber+"</td>");
		htmlString.append("<td class=tdDefault width='50%'><B> "+LC.L_Bgt_Status/*Budget Status*****/+":</B>&nbsp;&nbsp;"+bgtStat+"</td>");
		htmlString.append("</tr>");
 		htmlString.append("<tr>");
		htmlString.append("<td class=tdDefault colspan='2'><B>"+LC.L_Study_Title/*+LC.Std_Study+" Title*****/+":</B>&nbsp;&nbsp;"+bgtStudyTitle+"</td></tr><tr>");
		if(calName.equals("None"))
		htmlString.append("<td class=tdDefault width='50%'><B> "+MC.M_NoCalSelected/*No Calendar Selected*****/+"</B>&nbsp;&nbsp;</td>");
			else
		htmlString.append("<td class=tdDefault width='50%'><B> "+LC.L_Protocol_Calendar/*Protocol Calendar*****/+":</B>&nbsp;&nbsp;"+calName+"</td>");
		htmlString.append("</tr>");
		htmlString.append("</table>");
	    htmlString.append("</FORM>");

		htmlString.append("<Form name='bgtprot' method=post action=''>");
		htmlString.append("<input type=hidden name=rows value="+rows+">");
		htmlString.append("<table width='100%' cellspacing='3' cellpadding='0' border='1'>");
		htmlString.append("<tr>");
		htmlString.append("<th width=20% align =center>"+LC.L_Event/*Event*****/+"</th>");
		htmlString.append("<th width=18% align =center>"+LC.L_Category/*Category*****/+"</th>");
		htmlString.append("<th width=2% align =center>"+LC.L_Apply_Indirects/*Apply Indirects*****/+"</th>");
		htmlString.append("<th width=12% align =center>"+LC.L_Standard_OfCare/*Standard of Care*****/+"</th>");
		htmlString.append("<th width=10% align =center>"+LC.L_Unit_Cost/*Unit Cost*****/+"</th>");
		htmlString.append("<th width=10% align =center># "+LC.L_Of_Units/*of Units*****/+"</th>");
		htmlString.append("<th width=12% align =center>"+LC.L_Cost_Patient/*Cost/"+LC.Pat_Patient*****/+"</th>");
		htmlString.append("<th width=12% align =center>"+LC.L_Total_Cost/*Total Cost*****/+"</th>");


		htmlString.append("<th width=5% align =center>"+LC.L_Apply_Discount/*Apply discount*****/+"</th>");
		htmlString.append("</tr>");


		int bgtSectionIdOld = ((Integer) bgtSectionIds.get(0)).intValue();
		int first = 0;
		for(int i=0;i<totalrows;i++) {
    		lineitemId = ((Integer) lineitemIds.get(i)).intValue();
    		itemId = EJBUtil.integerToString(new Integer(lineitemId));
    		lineitemName = (String) lineitemNames.get(i);

    		lineitemDesc = (String) lineitemDescs.get(i);

    		lineitemSponsorUnit = (String) lineitemSponsorUnits.get(i);
			if (lineitemSponsorUnit==null) lineitemSponsorUnit = "0.00";
    		lineitemClinicNOfUnit = (String) lineitemClinicNOfUnits.get(i);

			if (lineitemClinicNOfUnit==null) lineitemClinicNOfUnit = "0.00";

    		lineitemDelFlag = (String) lineitemDelFlags.get(i);

			categoryId = (String) lineitemCategories.get(i);
			iCatId = EJBUtil.stringToNum(categoryId);
			if(categoryDesc == null)
				categoryDesc = "";

    		lineitemInPerSec = (String) lineitemInPerSecs.get(i);
			lineitemAppIndirect  = (String) lineitemAppIndirects.get(i);

			if(lineitemAppIndirect == null)
				lineitemAppIndirect = "1";


    		if (lineitemInPerSec==null) lineitemInPerSec = "0";

    		lineitemStdCareCost = (String) lineitemStdCareCosts.get(i);
			if (lineitemStdCareCost==null) lineitemStdCareCost = "";

			bgtSectionType = (String) bgtSectionTypes.get(i);
			if (lineitemStdCareCost.equals("null"))
				lineitemStdCareCost = "";
    		lineitemResCost = (String) lineitemResCosts.get(i);

			/*if(bgtSectionType.equals("O"))
			{

				lineitemResCost = "0.00";
			}*/


			if (lineitemResCost==null) lineitemResCost = "0.00";


    		lineitemInCostDisc = (String) lineitemInCostDiscs.get(i);

    		if (lineitemInCostDisc==null) lineitemInCostDisc = "0";

    		lineitemCptCode = (String) lineitemCptCodes.get(i);
    		if(i!=totalrows-1){
			bgtSecNextId =  ((Integer) bgtSectionIds.get(i+1)).intValue();

			}
    		bgtSectionId = ((Integer) bgtSectionIds.get(i)).intValue();
    		bgtSectionName = (String) bgtSectionNames.get(i);
    		bgtSectionDelFlag = (String) bgtSectionDelFlags.get(i);
    		bgtSectionSequence = (String) bgtSectionSequences.get(i);
    		bgtSectionPatNo = (String) bgtSectionPatNos.get(i);

    		bgtSectionPatNo = (bgtSectionPatNo == null)?"0.00":bgtSectionPatNo;
    		bgtSectionPersonlFlag = (String) bgtSectionPersonlFlags.get(i);




			if (bgtSectionType.equals("P") ) {

			if((EJBUtil.stringToFloat(lineitemSponsorUnit) > 0 ) && (EJBUtil.stringToFloat(lineitemClinicNOfUnit) > 0 ))
				{
			lineitemResCost = (new BigDecimal(lineitemSponsorUnit).multiply( new BigDecimal(lineitemClinicNOfUnit))).toString();

				}
            ttlCost = (new BigDecimal(lineitemResCost).multiply( new BigDecimal(bgtSectionPatNo))).toString();
			}
			else{
			ttlCost = (new BigDecimal(lineitemSponsorUnit).multiply( new BigDecimal(lineitemClinicNOfUnit))).toString();
			}



			if(iCatId == perCodeId)
			{
				salResTotal=  (new BigDecimal(salResTotal).add(new BigDecimal(lineitemResCost))).toString();
			}

			if(iCatId == perCodeId)
			{
				salTtlTotal=  (new BigDecimal(salTtlTotal).add(new BigDecimal(ttlCost))).toString();
			}


			if((fringeBenefitApply.equals("1") && iCatId == perCodeId) )
			{

				resFringe = (new BigDecimal(lineitemResCost).multiply(new BigDecimal(fringeBenefit).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP))).toString();

				resAfterFringe = (new BigDecimal(lineitemResCost).add(new BigDecimal(lineitemResCost).multiply(new BigDecimal(fringeBenefit).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();

				ttlFringe = (new BigDecimal(ttlCost).multiply(new BigDecimal(fringeBenefit).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP))).toString();

				ttlAfterFringe = (new BigDecimal(ttlCost).add(new BigDecimal(ttlCost).multiply(new BigDecimal(fringeBenefit).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();

				fringeResTotal = (new BigDecimal(fringeResTotal).add(new BigDecimal(resFringe))).toString();

				fringeTtlTotal = (new BigDecimal(fringeTtlTotal).add(new BigDecimal(ttlFringe))).toString();

				//stdCareAfterFringe = (new BigDecimal(lineitemStdCareCost).add(new BigDecimal(lineitemStdCareCost).multiply(new BigDecimal(fringeBenefit).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();


			} else
			{
				resAfterFringe = lineitemResCost;
				ttlAfterFringe = ttlCost;

//				stdCareAfterFringe = lineitemStdCareCost;
			}




			if(costDiscountApply.equals("1") && lineitemInCostDisc.equals("1"))
			{
				resDisc = (new BigDecimal(resAfterFringe).multiply(new BigDecimal(costDiscount).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP))).toString();
				aftercost =  (new BigDecimal(resAfterFringe).subtract(new BigDecimal(resAfterFringe).multiply(new BigDecimal(costDiscount).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();

				ttlDisc = (new BigDecimal(ttlAfterFringe).multiply(new BigDecimal(costDiscount).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP))).toString();
				afterTtlCost =  (new BigDecimal(ttlAfterFringe).subtract(new BigDecimal(ttlAfterFringe).multiply(new BigDecimal(costDiscount).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();

				discResTotal = (new BigDecimal(discResTotal).add(new BigDecimal(resDisc))).toString();

				discTtlTotal = (new BigDecimal(discTtlTotal).add(new BigDecimal(ttlDisc))).toString();

				//afterstdcost = (new BigDecimal(stdCareAfterFringe).subtract(new BigDecimal(stdCareAfterFringe).multiply(new BigDecimal(costDiscount).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();

			}

			else
			{
				aftercost =  resAfterFringe;
				afterTtlCost = ttlAfterFringe;
				//afterstdcost = stdCareAfterFringe;
			}


            subTotalResearchCost = (new BigDecimal(subTotalResearchCost).add(new BigDecimal(aftercost))).toString();
			subTotalTtlCost = (new BigDecimal(subTotalTtlCost).add(new BigDecimal(afterTtlCost))).toString();

			if (lineitemAppIndirect.equals("1")) {
				subTotalAppIndResCost = (new BigDecimal(subTotalAppIndResCost).add(new BigDecimal(aftercost))).toString();

				subTotalAppIndTtlCost = (new BigDecimal(subTotalAppIndTtlCost).add(new BigDecimal(afterTtlCost))).toString();

			}
			else{
				subTotalIndNotApplied = (new BigDecimal(subTotalIndNotApplied).add(new BigDecimal(aftercost))).toString();

				subTotalTtlIndNotApplied = (new BigDecimal(subTotalTtlIndNotApplied).add(new BigDecimal(afterTtlCost))).toString();
			}




			if ((EJBUtil.stringToFloat(bgtSectionPatNo)) > 0) {
				subTotalPatResearchCost = subTotalResearchCost;
				subTotalPatAppIndResCost = subTotalAppIndResCost;
				subTotalPatIndNotApplied = subTotalIndNotApplied;

				subTotalPatTtlCost = subTotalTtlCost;
				subTotalPatAppIndTtlCost = subTotalAppIndTtlCost;
				subTotalPatTtlIndNotApplied = subTotalTtlIndNotApplied;


			}
			else
			{
    			subTotalPatResearchCost = subTotalResearchCost;
				subTotalPatAppIndResCost = subTotalAppIndResCost;
				subTotalPatIndNotApplied = subTotalIndNotApplied;

				subTotalPatTtlCost = subTotalTtlCost;
				subTotalPatAppIndTtlCost = subTotalAppIndTtlCost;
				subTotalPatTtlIndNotApplied = subTotalTtlIndNotApplied;



			}


			if(i==0 ) {
				htmlString.append("<tr>");
				htmlString.append("<td class=tdDefault><B>"+bgtSectionName+"</B>");

				htmlString.append("</td>");
				htmlString.append("<td colspan=7>&nbsp;");
				htmlString.append("</td>");
				htmlString.append("</tr>");

			}

			else if(bgtSectionIdOld != bgtSectionId ) {
				htmlString.append("<tr>");
				htmlString.append("<td class=tdDefault><B>"+bgtSectionName+"</B>");
				htmlString.append("</td>");
				htmlString.append("<td colspan=7>&nbsp;");
				htmlString.append("</td>");
				htmlString.append("</tr>");

			}

			//Sept 04 Enhn --16/09/04
			if(lineitemName != null && !lineitemName.equals("")) {
			if(i == 0 || bgtSectionIdOld != bgtSectionId){
				if (bgtSectionType.equals("P") ) {
					htmlString.append("<tr>");
					htmlString.append("<td class=tdDefault align=right>");
					htmlString.append("<B>"+LC.L_Number_OfPat/*Number of "+LC.Pat_Patients+*****/+": "+bgtSectionPatNo+"</B>");

				htmlString.append("</td>");

//				htmlString.append("<td class=tdDefault align=right>"+bgtSectionPatNo+"</td>");
//				htmlString.append("<td>&nbsp;</td>");
				htmlString.append("</tr>");
				}
			}
			}




			if(lineitemName != null && !lineitemName.equals("")) {
				 if (bgtSectionType.equals("O") && (bgtSecNextId != bgtSectionId || i == totalrows-1)) {

				 }
			}


			if(lineitemName != null && !lineitemName.equals("")) {

				categoryDesc = schCodeDao.getCodeDescription(EJBUtil.stringToNum(categoryId));
				if(categoryDesc == null)
					categoryDesc = "-";
		        htmlString.append("<tr>");
				htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;"+lineitemName+"</td>");
				htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;"+categoryDesc+"</td>");
				htmlString.append("<td  align=center>");
				if (lineitemAppIndirect.equals("0")) {
					htmlString.append("&nbsp;</td>");
				}
				else {
					htmlString.append(LC.L_Yes/*"Yes*****/+"</td>");
				}
				htmlString.append("<td  align=center>");
				if (lineitemStdCareCost.equals("")) {
					htmlString.append("&nbsp;</td>");
				}
				else {
					htmlString.append(LC.L_Yes/*"Yes*****/+"</td>");
				}
				htmlString.append("<td class=tdDefault align=right>"+EJBUtil.format(lineitemSponsorUnit,2,".")+"</td>");
//				htmlString.append("<input type=hidden name=sponsorCost size=5 maxlength=10 value="+lineitemSponsorUnit+">");
			    htmlString.append("<td class=tdDefault align=right>"+EJBUtil.format(lineitemClinicNOfUnit,2,".")+"</td>");
//				htmlString.append("<input type=hidden name=clinicCost size=5 maxlength=10 value="+lineitemClinicNOfUnit+">");

				htmlString.append("<td class=tdDefault align=right>"+new BigDecimal(lineitemResCost).setScale(2, BigDecimal.ROUND_HALF_UP)+"</td>");
				  htmlString.append("<td class=tdDefault align=right>"+new BigDecimal(ttlCost).setScale(2, BigDecimal.ROUND_HALF_UP)+"</td>");
//				htmlString.append("<input type=hidden name=otherCost size=5 maxlength=10 value="+lineitemOtherCost+">");

			//	htmlString.append("<td class=tdDefault align=right>"+EJBUtil.format(lineitemStdCareCost,2,".")+"</td>");
//				htmlString.append("<input type=hidden name=otherCost size=5 maxlength=10 value="+lineitemOtherCost+">");


				htmlString.append("<td  align=center>");
				if (lineitemInCostDisc.equals("1")) {
					htmlString.append(LC.L_Yes/*"Yes"*****/);
				}
				else {
					htmlString.append("&nbsp;");
				}

				htmlString.append("</tr>");


				htmlString.append("<tr>");
				htmlString.append("<td class=tdDefault align=right>");


				//*if (first != 0 && (bgtSectionType.equals("P") && (bgtSecNextId != bgtSectionId || i == totalrows-1 ))) {
					/*htmlString.append("<B>Total/Patient (in "+currency+")</B>");
					htmlString.append("</td>");

					htmlString.append("<td>&nbsp;</td>");
					htmlString.append("<td>&nbsp;</td>");
					htmlString.append("<td class=tdDefault align=right>" + EJBUtil.format(subTotalResearchCost,2,".") + "</td>");
					htmlString.append("<td class=tdDefault align=right>" + EJBUtil.format(subTotalStdCareCost,2,".") + "</td>");

					htmlString.append("</tr>");

					htmlString.append("<tr>");
					htmlString.append("<td class=tdDefault align=right>");
			}	*/

				 if (bgtSectionType.equals("O") && (bgtSecNextId != bgtSectionId || i == totalrows-1)) {
					 Object[] arguments5= {currency};
					 htmlString.append(VelosResourceBundle.getLabelString("L_Section_TotalIn",arguments5))/*<B>Section Total (in {0})</B>*****/;

			//	if (bgtSectionType.equals("P") && (bgtSecNextId != bgtSectionId || i == totalrows-1)) {
				htmlString.append("</td>");

            	htmlString.append("<td>&nbsp;</td>");
            	htmlString.append("<td>&nbsp;</td>");
            	htmlString.append("<td>&nbsp;</td>");
				htmlString.append("<td>&nbsp;</td>");
				htmlString.append("<td>&nbsp;</td>");
            	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(subTotalResearchCost).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
				htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(subTotalTtlCost).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
            	//htmlString.append("<td class=tdDefault align=right>" + EJBUtil.format(subTotalStdCareCost,2,".") + "</td>");

            	htmlString.append("</tr>");

				htmlString.append("<tr>");
				htmlString.append("<td class=tdDefault align=right>");
			}
				if (bgtSectionType.equals("P") && (bgtSecNextId != bgtSectionId || i == totalrows-1)) {
					//htmlString.append("<B>Number of Patients</B>");

				htmlString.append("</td>");

				//htmlString.append("<td class=tdDefault align=right>"+bgtSectionPatNo+"</td>");
				//htmlString.append("<td>&nbsp;</td>");
				//htmlString.append("</tr>");
				htmlString.append("<tr>");
				htmlString.append("<td>");
				Object[] arguments4= {currency};
				htmlString.append(VelosResourceBundle.getLabelString("L_Section_TotalIn",arguments4))/*<B>Section Total (in {0})</B>*****/;
				htmlString.append("</td>");

            	htmlString.append("<td>&nbsp;</td>");
            	htmlString.append("<td>&nbsp;</td>");
				htmlString.append("<td>&nbsp;</td>");
				htmlString.append("<td>&nbsp;</td>");
				htmlString.append("<td>&nbsp;</td>");
            	htmlString.append("<td class=tdDefault align=right>" +new BigDecimal(subTotalPatResearchCost).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
				htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(subTotalPatTtlCost).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
            	//htmlString.append("<td class=tdDefault align=right>" + EJBUtil.format(subTotalPatStdCareCost,2,".") + "</td>");

            	htmlString.append("</tr>");
				}


			}
			if(bgtSecNextId != bgtSectionId || i==totalrows-1){
				totalPatResearchCost = (new BigDecimal(totalPatResearchCost).add( new BigDecimal(subTotalPatResearchCost))).toString();

				totalPatTtlCost = (new BigDecimal(totalPatTtlCost).add( new BigDecimal(subTotalPatTtlCost))).toString();


						totalPatAppIndResCost = (new BigDecimal(totalPatAppIndResCost).add( new BigDecimal(subTotalPatAppIndResCost))).toString();
						totalPatAppIndTtlCost = (new BigDecimal(totalPatAppIndTtlCost).add( new BigDecimal(subTotalPatAppIndTtlCost))).toString();

						totalPatIndNotApplied = (new BigDecimal(totalPatIndNotApplied).add( new BigDecimal(subTotalPatIndNotApplied))).toString();
						totalPatTtlIndNotApplied = (new BigDecimal(totalPatTtlIndNotApplied).add( new BigDecimal(subTotalPatTtlIndNotApplied))).toString();

				//totalPatTtlCost = (new BigDecimal(totalPatTtlCost).add( new BigDecimal(subTotalPatTtlCost))).toString();



    			//totalPatStdCareCost = (new BigDecimal(totalPatStdCareCost).add( new BigDecimal(subTotalPatStdCareCost))).toString();
			}

			/*	totalPatResearchCost = totalPatResearchCost+ lineitemResCost;
    			totalPatOtherCost = totalPatOtherCost + lineitemOtherCost;
    			totalPatInvCost   = totalPatInvCost + lineitemInvCost;
    			totalPatStdCareCost =  totalPatStdCareCost + lineitemStdCareCost;*/
		if(bgtSecNextId != bgtSectionId){
			subTotalResearchCost = "0.00";
			subTotalAppIndResCost = "0.00";
			subTotalIndNotApplied = "0.00";

			subTotalTtlCost = "0.00";
			subTotalAppIndTtlCost = "0.00";
			subTotalTtlIndNotApplied = "0.00";

			//subTotalStdCareCost = "0.00";
		}


		bgtSectionIdOld = bgtSectionId;
		first = 1;


	} //for loop totalrows

			totalResearchCost = (new BigDecimal(totalResearchCost).add( new BigDecimal(lineitemResCost))).toString();
			totalTtlCost = (new BigDecimal(totalTtlCost).add( new BigDecimal(ttlCost))).toString();

			if(lineitemAppIndirect.equals("1")) {

				totalAppIndResCost = (new BigDecimal(totalAppIndResCost).add( new BigDecimal(lineitemResCost))).toString();

				totalAppIndTtlCost = (new BigDecimal(totalAppIndTtlCost).add( new BigDecimal(ttlCost))).toString();

			}
			else {
				totalIndNotApplied = (new BigDecimal(totalIndNotApplied).add( new BigDecimal(lineitemResCost))).toString();
				totalTtlIndNotApplied = (new BigDecimal(totalTtlIndNotApplied).add( new BigDecimal(ttlCost))).toString();
			}

			//totalTtlCost = (new BigDecimal(totalTtlCost).add( new BigDecimal(ttlCost))).toString();

			//totalStdCareCost = (new BigDecimal(totalStdCareCost).add( new BigDecimal(lineitemStdCareCost))).toString();





	htmlString.append("<input type=hidden name=sponsorOHeadApply value="+sponsorOHeadApply+">");
	htmlString.append("<input type=hidden name=clinicOHeadApply value="+clinicOHeadApply+">");
	htmlString.append("<input type=hidden name=sponsorOHead value="+sponsorOHead+">");
	htmlString.append("<input type=hidden name=clinicOHead value="+clinicOHead+">");

	htmlString.append("<tr height=10>");

    htmlString.append("<tr>");
    Object[] arguments3= {currency};
	htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;"+VelosResourceBundle.getMessageString("M_OverAll_Total",arguments3))/*<B>Over-all Total(in {0})</B>****/;
	htmlString.append("</td>");

	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");
	//stdCareAfterFringe = (new BigDecimal(lineitemStdCareCost).add(new BigDecimal(lineitemStdCareCost).multiply(new BigDecimal(fringeBenefit).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();
    if(sponsorOHeadApply.equals("Y")) {

	//totalPatResearchCost = (new BigDecimal(totalPatResearchCost).add(new BigDecimal(totalPatResearchCost).multiply(new BigDecimal(sponsorOHead).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))).toString();


    indResTotal = (new BigDecimal(totalPatAppIndResCost).multiply(new BigDecimal(sponsorOHead).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP))).toString();

	indTtlTotal = (new BigDecimal(totalPatAppIndTtlCost).multiply(new BigDecimal(sponsorOHead).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP))).toString();

	totalPatAppIndResCost = (new BigDecimal(totalPatIndNotApplied).add( (new BigDecimal(totalPatAppIndResCost).add(new BigDecimal(totalPatAppIndResCost).multiply(new BigDecimal(sponsorOHead).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))))).toString();

	totalPatAppIndTtlCost = (new BigDecimal(totalPatTtlIndNotApplied).add( (new BigDecimal(totalPatAppIndTtlCost).add(new BigDecimal(totalPatAppIndTtlCost).multiply(new BigDecimal(sponsorOHead).divide(new BigDecimal(100),4,BigDecimal.ROUND_UP)))))).toString();

	}
	//totalPatTtlCost = (new BigDecimal(totalPatTtlCost).add(new BigDecimal(totalPatTtlCost))).toString();


	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(totalPatAppIndResCost).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(totalPatAppIndTtlCost).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
	//htmlString.append("<td class=tdDefault align=right>" + EJBUtil.format(totalPatStdCareCost,2,".") + "</td>");
	htmlString.append("</tr>");
    htmlString.append("<tr>");
	//htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>Grand Total</B>");
	htmlString.append("</td>");
	htmlString.append("<td class=tdDefault>&nbsp;</td>");
	htmlString.append("<td class=tdDefault>&nbsp</td>");
//	htmlString.append("<td class=tdDefault align=right>" + currency + " " + EJBUtil.format(EJBUtil.floatToString(new Float(grandTotal)),2,".") + "</td>");
	htmlString.append("</tr>");
	//Sept enhancements
	//htmlString.append("<tr>");

    htmlString.append("<tr>");
	htmlString.append("<td class=tdDefault>&nbsp;&nbsp;&nbsp;<B>"+LC.L_Total_Salary/*Total Salary*****/+"</B>");
	htmlString.append("</td>");
	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");
	htmlString.append("<td>&nbsp;</td>");





	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(salResTotal).setScale(2, BigDecimal.ROUND_HALF_UP)+ "</td>");
	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(salTtlTotal).setScale(2, BigDecimal.ROUND_HALF_UP)+ "</td>");

	htmlString.append("</tr>");


	//htmlString.append("</table>");

	htmlString.append("<tr><td></td></tr>");
	htmlString.append("<tr><td></td></tr>");

	//htmlString.append("<table>");

	if(fringeBenefitApply.equals("1")){
		Object[] arguments= {fringeBenefit};
		htmlString.append("<tr><td colspan=4>"+VelosResourceBundle.getMessageString("M_FringeBenefitApplied_ToAll",arguments)/*<B>Fringe benefit of </B>{0}%<B> Applied to all Personnel Costs</B>*****/+"</td>");
	}
	else {

		htmlString.append("<tr><td colspan=4><B>"+MC.M_FringeBenefitNot_ToCalc/*Fringe benefit not applied to calculations*****/+"</B></td>");
	}


	htmlString.append("<td colspan=2 class=tdDefault>&nbsp;&nbsp;&nbsp;<B>"+LC.L_Total_Fringe/*Total Fringe*****/+"</B>");



	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(fringeResTotal).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(fringeTtlTotal).setScale(2, BigDecimal.ROUND_HALF_UP)+ "</td></tr>");

	if(costDiscountApply.equals("1")){
		Object[] arguments1= {costDiscount};
		htmlString.append("<tr><td colspan=4>"+VelosResourceBundle.getMessageString("M_CostDiscount_Applied",arguments1)/*<B>Cost Discount of</B>{0}%<B>Applied to selected line items above</B>*****/+"</td>");
	}
	else {

		htmlString.append("<tr><td colspan=4><B>"+MC.M_CostDisc_NtCal/*Cost Discount not applied to calculations*****/+"</B></td>");
	}



	htmlString.append("<td colspan=2 class=tdDefault>&nbsp;&nbsp;&nbsp<B>"+LC.L_Total_CostDiscount/*Total Cost Discount*****/+"</B>");



	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(discResTotal).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td>");
	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(discTtlTotal).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td></tr>");
	/*htmlString.append("<tr>");
	htmlString.append("<td class=tdDefault width='20%'>");
	htmlString.append("<B>Overhead</B>");
	htmlString.append("</td>");
	htmlString.append("<td class=tdDefault width='80%'> "+ EJBUtil.format(EJBUtil.floatToString(new Float(sponsorOHead)),2,".") +"&nbsp;%&nbsp;<B>");*/

	if(sponsorOHeadApply.equals("N")) {
		htmlString.append("&nbsp;&nbsp;<tr><td colspan=4><B>"+MC.M_IndirectsNot_ToCalcu/*Indirects not applied to calculations*****/+"</B</td>");
	} else {
		Object[] arguments6= {EJBUtil.format(EJBUtil.floatToString(new Float(sponsorOHead)),2,".")};
		htmlString.append("&nbsp;&nbsp;<tr><td colspan=4><B>"+VelosResourceBundle.getMessageString("M_IndirectsOf_ApplToAllCost",arguments6))/*<B>Indirects of</B>{0}% <B>Applied to Over-all Total Cost/Patient</B>*****/;
	}



	htmlString.append("<td colspan=2 class=tdDefault>&nbsp;&nbsp;&nbsp;<B>"+LC.L_Total_Indirects/*Total Indirects*****/+"</B>");
	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(indResTotal).setScale(2, BigDecimal.ROUND_HALF_UP)+ "</td>");
	htmlString.append("<td class=tdDefault align=right>" + new BigDecimal(indTtlTotal).setScale(2, BigDecimal.ROUND_HALF_UP) + "</td></tr>");

	budgetB.setBudgetId(budgetId);

	budgetB.getBudgetDetails();


	int usr = EJBUtil.stringToNum(budgetB.getModifiedBy());

	String lastModifiedDate = budgetB.getLastModifiedDate();



	userB.setUserId(usr);

	userB.getUserDetails();
	String fname = userB.getUserFirstName();
	if(fname==null)
		fname="";
	String lname = userB.getUserLastName();
	if(lname==null)
		lname="";
	Object[] arguments2= {fname,lname};
	htmlString.append("<tr><td colspan=2>"+VelosResourceBundle.getMessageString("M_LastModified_By",arguments2)/*<B>Last Modified By : {0} {1} </B>*****/+"</td></tr>");
	htmlString.append("<tr><td colspan=2><B>"+LC.L_Date_LastModified/*Date Last Modified*****/+" : "+lastModifiedDate+" </B></td></tr>");



	htmlString.append("</B></td>");
	htmlString.append("</tr>");
	htmlString.append("<tr>");
	//htmlString.append("<td class=tdDefault><B>Clinic Overhead</B></td>");
	//htmlString.append("<td class=tdDefault>"+ EJBUtil.format(EJBUtil.floatToString(new Float(clinicOHead)),2,".") +"&nbsp;%&nbsp;<B>");

	/*if(clinicOHeadApply.equals("N")) {
		htmlString.append("&nbsp;&nbsp;Not applied to calculations</B>");
	} else {
		htmlString.append("&nbsp;&nbsp;Applied to calculations</B>");
	}*/
	htmlString.append("</td></tr>");
	htmlString.append("</table>");
	htmlString.append("</Form>");
%>


<%


String contents = null;
String str = htmlString.toString();
contents = ReportIO.saveReportToDoc(str,"xls","budgetexcel");

if(type.equals("E")) {
%>
<form name="loc" METHOD="POST">
	<input type=hidden name="contents" value=<%=contents%> />

		<script>
			self.location = window.document.loc.contents.value ;
		</script>
</form>

<%}%>
<%=htmlString%>

<%
	} else {
%>
		<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
	}
%>
</Div>
</body>

</html>
