<!-- 
	File Name: editStudyTeamStatus.jsp

	Author : Manimaran
	
	Created Date	: 08/01/2006

	Last Modified Date : 

	Purpose	: For July-August Enhancement(S4)
-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Status_Dets%><%-- Status Details*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<jsp:include page="popupJS.js" flush="true"/>

<SCRIPT Language="javascript">

function  validate(formobj) {    
	if (formobj.status){
		if (!(validate_col('Status',formobj.status))) return false
	}
	
	if (formobj.currentstat.checked) {
            formobj.isCurrent.value=1;
	} else {
	    formobj.isCurrent.value=0;      
	}
	if (formobj.statusDate.value =="") {
		alert("<%=MC.M_Selc_StatusDate%>" );/*alert("Please select Status Date" );*****/
		formobj.statusDate.focus();
		return false;
	}	     
     	
    	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	
	//Added by Manimaran to fix the bug2708
	if(formobj.EndStatusDate)
	{
	    if (formobj.EndStatusDate.value =="") {
	    	alert("<%=MC.M_SelPrevStat_EndDt%>" );/*alert("Please select Previous Status End Date" );*****/
		formobj.EndStatusDate.focus();
		return false;
            }	
	}     
	if (isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again.");*****/
		formobj.eSign.focus();
		return false;
	}
	if (formobj.status){
	    formobj.newStatCode.value=formobj.status.options[formobj.status.selectedIndex].text;	
	} 
 }
 
  	

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.StatusHistoryDao, com.velos.eres.business.common.CodeDao,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<body>
<DIV class="popDefault" id="div1"> 
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	String mode=request.getParameter("mode");
	if(mode == null)
		mode = "";	
	String moduleTable = request.getParameter("moduleTable");
	String modulePk = request.getParameter("teamId");
	String statusCode = request.getParameter("statusCode");
	String pageRight = request.getParameter("pageRight");
	String statusId = request.getParameter("statusId");
	String sectionHeading = request.getParameter("sectionHeading");
	String teamStat=request.getParameter("teamStat");
	String userName=request.getParameter("userName");
	String statusCodeType = request.getParameter("statusCodeType");
	String isCurrentStat = request.getParameter("isCurrentStat");
	String accountId=(String)tSession.getAttribute("accountId");
	int tempAccountId=EJBUtil.stringToNum(accountId);
	String currentStatId="";
	String notes = "";
	if (EJBUtil.isEmpty(pageRight))
		pageRight = "7";
	if (EJBUtil.stringToNum(pageRight) >= 7) {
		StatusHistoryDao statusHistDao = new StatusHistoryDao();	
		ArrayList statusIds = new ArrayList();	
		ArrayList isCurrentStatArr= new ArrayList();
		statusHistDao = statusB.getStatusHistoryInfo(EJBUtil.stringToNum(modulePk), moduleTable);
		statusIds = statusHistDao.getStatusIds();
		isCurrentStatArr = statusHistDao.getIsCurrentStats();
   	  	int len= statusHistDao.getCRows();
		String currentVal="";
		for (int i=0;i<len;i++) {
			currentVal = (isCurrentStatArr.get(i) == null)?"":(isCurrentStatArr.get(i)).toString();
			if ( currentVal.equals("1")) {
			     currentStatId = (String)statusIds.get(i).toString();
		        }
    		}
		String statusDate="";
		if (isCurrentStat==null)
		    isCurrentStat="1";
						
		if (mode.equals("M"))
		{ 
			statusB.setStatusId(EJBUtil.stringToNum(statusId));
			statusB.getStatusHistoryDetails();
			statusCode = statusB.getStatusCodelstId();
			notes = statusB.getStatusNotes();
			if (EJBUtil.isEmpty(notes))
			notes = "";
			statusDate = statusB.getStatusStartDate();
			if (statusDate==null)
				statusDate="";
		}
		
		
%>

<Form name="status" id="teamStatId" method="post" action="updateTeamStatus.jsp" onsubmit="if (validate(document.status)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<table width="75%" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<% if (mode.equals("N")) { %>
		<td ><P class="sectionHeadings"> <%=MC.M_ChgStd_TeamStat%><%-- Change <%=LC.Std_Study%> Team Status*****--%> </P> </td>
		<%} else { %>
		<td ><P class="sectionHeadings"> <%=MC.M_EdtStd_TeamStat%><%-- Edit <%=LC.Std_Study%> Team Status*****--%> </P> </td>
		<%}%>
		</tr>   
	 </table>
	
<input name="moduleTable" type=hidden value=<%=moduleTable%>>
<input name="modulePk" type=hidden value=<%=modulePk%>>
<input name="mode" type=hidden value=<%=mode%>>
<input name="statusId" type=hidden value=<%=statusId%>>
<input name="statCode" type=hidden value=<%=statusCode%>>
<br>
<table width="80%" cellspacing="0" cellpadding="0" border="0"  >
	<tr> 
      	<td width="20%"><%=MC.M_StdTeam_Memb%><%--<%=LC.Std_Study%> Team Member*****--%></td>
        <td width="60%"><%=userName%></td>
	</tr>
	<tr> 
        <td width="20%"><%=LC.L_Current_Status%><%-- Current Status*****--%></td>
        <td width="60%">
		<%=teamStat%></td>
	</tr>
	  
<% 
if(mode.equals("N")) { 
	CodeDao codeStatus = new CodeDao();
	codeStatus.getCodeValues("teamstatus");	
	
	String statusDD="";
	statusDD = codeStatus.toPullDown("status");

       %>
       <tr>
       <td width="20%"><%=LC.L_New_Status%><%-- New Status*****--%> <FONT class="Mandatory">* </FONT></td>
       <td width="60%">
		<%=statusDD%></td>
       </tr>
  <%} %>
      
       
       <tr>
	<%
	if(mode.equals("N")) {
	%>
		
		<td colspan=2 width="20%"> <input type="checkbox" name="currentstat" checked> <%=MC.M_StdTeam_CurrStat%><%-- This is <%=LC.Std_Study_Lower%> team member's current status*****--%></td>

	 <% 
         } else if (EJBUtil.stringToNum(isCurrentStat)==0) {
	 %>
		 
		 <td colspan=2 width="20%"> <input type="checkbox" name="currentstat"> <%=MC.M_StdTeam_CurrStat%><%-- This is <%=LC.Std_Study_Lower%> team member's current status*****--%></td>

		 <% } else if (EJBUtil.stringToNum(isCurrentStat)==1)  { %>
		 
		 <td colspan=2 width="20%"> <input type="checkbox" name="currentstat" checked disabled> <%=MC.M_StdTeam_CurrStat%><%-- This is <%=LC.Std_Study_Lower%> team member's current status*****--%></td>

		 <%}%>
	       </tr>
       
      <tr> 
<%-- INF-20084 Datepicker-- AGodara --%>
		<td width="200"> <%=LC.L_Status_Date%><%-- Status Date*****--%> <FONT class="Mandatory">* </FONT></td>
   		<td width=300><input type="text" name="statusDate" class="datefield" size = 15 MAXLENGTH = 50	value ="<%=statusDate%>" readonly></td>
	</tr>
	<% if(mode.equals("N")) { %>
	<tr> 
        <td width="200"> <%=MC.M_Prev_StatusEndsOn%><%-- Previous Status Ends on*****--%> <FONT class="Mandatory">* </FONT></td>
        <td width=300><input type="text" name="EndStatusDate" class="datefield" size = 15 MAXLENGTH = 50 readonly value ="" ></td>
	</tr>
	
	<%} %>
	
	<tr> 

        <td width="200"> <%=LC.L_Notes%><%-- Notes*****--%></td>

        <td width=300> 

	<TEXTAREA name="notes"  rows=3 cols=30><%=notes%></TEXTAREA></td>

	</tr>

</table>
	<br>
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="copyverfrm"/>
		<jsp:param name="showDiscard" value="N"/>
  </jsp:include>

  <input name="newStatCode" type="hidden" value="">
  <input name="isCurrent" type="hidden"  value="">
  <input name="teamStat" type="hidden" value="<%=teamStat%>">
  <input name="currentStatId" type="hidden" value="<%=currentStatId%>">
  </Form>
  
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>

</html>

