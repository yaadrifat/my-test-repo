<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Evt_DetsPage%><%-- Event Details Page*****--%></title>

 

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*" %>
<%@ page import="com.velos.eres.service.util.VelosResourceBundle" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</head>
<%--Modified By Yogendra: Bug# 7985 --%>
<%
String fromPage1 =  request.getParameter("fromPage");
%>

<%// Added by Vishal to disable the panel and menus when called from Protocol Calendar->Select Events

	 if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt"))

{%>

 <SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))



	%>

<SCRIPT Language="javascript">


//KM-01Sep09
function callAjaxGetCatDD(formobj){
	   selval = formobj.cmbLibType.value;
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval+"&ddName=categoryId&codeType=L&ddType=category" ,
		   reqType:"POST",
		   outputElement: "span_catId" }

   ).startRequest();

}

	function loadEventCategory() {
		if (document.eventdetail == null) { return; }
		if (document.eventdetail.cmbLibType == null) { return; }
		if (document.eventdetail.cmbLibType.value == null) { return; }
		callAjaxGetCatDD(document.eventdetail);
		var count = 0;
		while(count < 10) {
			if (selectEventCategory()) { return; }
			setTimeout(selectEventCategory, 500);
			count++;
		}
	}

	function selectEventCategory() {
		if (document.eventdetail.selCategoryId == null ||
				document.eventdetail.selCategoryId.value == null) { return true; }
		if (document.eventdetail.categoryId.options == null ||
				document.eventdetail.categoryId.options.length == 0) { return false; }
		for(var iX=0; iX<document.eventdetail.categoryId.options.length; iX++) {
			if (document.eventdetail.categoryId.options[iX].value == 
					document.eventdetail.selCategoryId.value) {
				document.eventdetail.categoryId.options[iX].selected = true;
				return true;
			}
		}
		return false;
	}

	function openWinAdditinalCode(modId, mode, modName, calledFrom)
	{
		if (mode == 'M'){
		<%--Modified By Yogendra: Bug# 7985 --%>
			windowname=window.open("moredetails.jsp?modId=" + modId+"&modName="+modName+"&calledFrom="+calledFrom+"&accessFromPage=<%=fromPage1%>","Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=550 top=120,left=200 0, ");
			windowname.focus();
		} else if (mode == 'N')	{
			alert("<%=MC.M_SavePage_BeforeDets%>");/*alert("Please save the page first before entering More Details");*****/
			return false;
		}
	}



	<%--sonika--%>
	function setParam(str,formobj){
			formobj.subVal.value=str;
		if (!(validate(formobj))) {
			 return false  }
//		 setVals(formobj.subVal.value,formobj);
		
		formobj.selCategoryId.value = formobj.categoryId.value;
		formobj.action = "eventdetailsave.jsp?updateFlag=Add";
		formobj.submit();
	}

	function setVals(str,formobj){
		formobj.action = "eventdetailsave.jsp?updateFlag=" + str;
		if (!(validate(formobj))) {
			 return false  }
		formobj.selCategoryId.value = formobj.categoryId.value;
		return true;
	}

 function  validate(formobj){

	/*if (!allsplcharcheck(formobj.eventName.value))  
	{
		formobj.eventName.focus();  return false;
	}*/
	if (!(validate_col('e-Signature',formobj.eSign))){

	  formobj.subVal.value="Submit";
	  return false}

 	 //KM : Notes characters restriction.
	 var notesLen = formobj.eventNotes.value;
	 len = notesLen.length;
	 if(formobj.agentVal.value == 1) {
		 for(i=0;i<len;i++) {
			 if(notesLen.charAt(i) == "\n")
				len = len + 1;
		 }
	 }

	 if( len > 1000 ) {
		 alert("<%=MC.M_MoreChars_NotAllwd%>");/*alert("More than 1000 characters are not allowed");*****/
		 formobj.subVal.value="Submit";
		 formobj.eventNotes.focus();
	     return false;
	 }


	 if (formobj.fromPage.value == 'fetchProt'){

	 //JM: 25Apr2008, added for #3438
	 if (!(validate_col('Sequence',formobj.eventSeq))){
	  formobj.subVal.value="Submit";
	  return false
	 }

	 //JM: 25Apr2008, added for #3438
	 if(isNaN(formobj.eventSeq.value) == true) {
		 alert("<%=MC.M_Etr_NumValue%>");/*alert("Please enter numeric value");*****/
		formobj.eventSeq.focus();
		formobj.subVal.value="Submit";
		return false;
	 }

	}

	//KM-23Oct09, #4387
	if(formobj.eventmode.value == "N") {
		if (!(validate_col('Event Library Type',formobj.cmbLibType))) {
	 	 	 formobj.subVal.value="Submit";
	 	 	 return false
		}
	 }


	 if(formobj.eventmode.value == "N") {
	 	 if (!(validate_col('Category',formobj.categoryId))) {
	 	 	 formobj.subVal.value="Submit";
	 	 	  return false }
	 }

     if (!(validate_col('Event Name',formobj.eventName))) {
        formobj.subVal.value="Submit";
      return false; }

	 if (!(isInteger(formobj.eventDuration.value))){
		 alert("<%=LC.L_Invalid_Duration%>");/*alert("Invalid Duration");*****/
	 	formobj.subVal.value="Submit";
		formobj.eventDuration.focus();
	 	 return false;
	 }
  
 	 if (!(isInteger(formobj.eventFuzzyPerDays.value))){
 		alert("<%=LC.L_Invalid_EvtWindow%>");/*alert("Invalid Event Window");*****/
	 	formobj.subVal.value="Submit";
		formobj.eventFuzzyPerDays.focus();
	 	 return false;
	 }
	 

	 //JM: 06May2010: #4833
	 //BK-21May2010 #4946  
	 if (!(isInteger(formobj.eventFuzzyPerDaysA.value))){
		 alert("<%=LC.L_Invalid_EvtWindow%>");/*alert("Invalid Event Window");*****/
	 	formobj.subVal.value="Submit";
		formobj.eventFuzzyPerDaysA.focus();

	 	 return false;
	 }

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
		<%--sonika--%>
	formobj.subVal.value="Submit";


	return false;

   }
	<%--sonika--%>
	//return setVals(formobj.subVal.value,formobj);
	return true ;
   }



</SCRIPT>





<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>




<% String src;
int agentVal =0;
String agentName = request.getHeader("USER-AGENT");
if(agentName != null && agentName.indexOf("Firefox") != -1 )
   agentVal =1;

src= request.getParameter("srcmenu");

if ((request.getParameter("fromPage")).equals("selectEvent")|| (request.getParameter("fromPage")).equals("fetchProt")){

//KM-#4364
%>

<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

<%

}


 else{

%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>

<!--KM-- included for dynamic drop down changes -- >
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>


<%}%>

<script>
  $E.onDOMReady(loadEventCategory);
</script>


<body id="forms">


<%// Added by Vishal to disable the panel and menus when called from Protocol Calendar->Select Events

	
	if ((request.getParameter("fromPage")).equals("selectEvent")|| (request.getParameter("fromPage")).equals("fetchProt")){%>


		<DIV  class="popDefault" id="div1">
		<br>
	<%	}

else { %>

<DIV class="formDefault_event" id="div1">

<%}%>





<%

	String eventmode = request.getParameter("eventmode");
    String duration = request.getParameter("duration");
	String protocolId = request.getParameter("protocolId");


	String calledFrom = request.getParameter("calledFrom");
    String fromPage = request.getParameter("fromPage");
	String mode = request.getParameter("mode");
	String displayDur=request.getParameter("displayDur");
	String displayType=request.getParameter("displayType");
	String calStatus = request.getParameter("calStatus");

	//JM: 25Apr2008, added for, Enh. #C9
	String offLnFlag= request.getParameter("offLineFlag");
	offLnFlag = (offLnFlag==null)?"0":offLnFlag;


   //JM: 25Jun2008, added for, #3531
    String calledFromPg = request.getParameter("calledFromPage");
    calledFromPg = (calledFromPg==null)?"":calledFromPg;

	String patientId = request.getParameter("patId");
	patientId = (patientId==null)?"":patientId;


	String patProtKey = request.getParameter("patProtId");
	patProtKey = (patProtKey==null)?"":patProtKey;

	String dsplcmnt = request.getParameter("displacement");
	dsplcmnt = (dsplcmnt==null)?"0":dsplcmnt;

	String visitNumber = request.getParameter("visitId");
    visitNumber = (visitNumber==null)?"":visitNumber;



	String catName= request.getParameter("catName");
	String cost =request.getParameter("cost");
	String eventId = "";
	String libTypePullDn ="";
	String evtLibType ="";

	SchCodeDao schDao = new SchCodeDao();
	schDao.getCodeValues("lib_type");
	int libTypeId = schDao.getCodeId("lib_type","default");
	libTypePullDn = schDao.toPullDown("cmbLibType",0," onChange=callAjaxGetCatDD(document.eventdetail);");	//KM
	
	CodeDao codeDao = new CodeDao();
	codeDao.getCodeValues("site_type","service");
	
	ArrayList siteType = new ArrayList();
	siteType = codeDao.getCId();
	
	HttpSession tSession = request.getSession(true);
	int accountId=0;
	accountId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	
	String strSiteType = EJBUtil.ArrayListToString(siteType);
	SiteDao siteDao = siteB.getBySiteTypeCodeId(accountId,strSiteType);

	String ddSOS ="";
	String ddFacility ="";
	String ddCoverage ="";
	
	schDao = new SchCodeDao();
	schDao.getCodeValues("coverage_type");
	
	if(mode.equals("M"))
	{
	eventId = request.getParameter("eventId");
	}



	//variable mainmode is passed only when 'New Event' is clikced in select event otherwise it should be null.

	//it is used as a flag to set the session variable eventparam only when control came from 'select event's page

	String mainmode=request.getParameter("mainmode");


	String categoryId = "";

	String eventName = "";

	String eventSeq = "";


	String eventDesc = "";

	String eventDuration = "";
	String eventDurationUnit = "";

	String eventDurDays = "";

	String eventFuzzyPer = "0";

	String eventFuzzyPerDays = "";

	String eventFuzzyPerBA = "";

	String eventNotes = "";

	String cptCode="";

	String durationUnitAfter="";
	String eventFuzzyPerDaysA="";
	String eventFuzzyPerBAA = "";
	String durUnitA="";
	String durUnitB="";
	//String catId ="";
	
	int eventSOSId = 0;
	int eventFacilityId = 0;
	int eventCoverageType =0;


	int pageRight=0;

	if (sessionmaint.isValidSession(tSession))

	{

	   int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));

		String uName = (String) tSession.getValue("userName");

		String calAssoc = request.getParameter("calassoc");
		calAssoc=(calAssoc==null)?"":calAssoc;

%>

<%
	String calName = "";

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule")|| fromPage.equals("selectEvent") )
	{
		%><%
	}else{
		calName = (String) tSession.getValue("protocolname");
	%>
		<P class="sectionHeadings"> <%Object[] arguments = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtDetails",arguments)%><%--Protocol Calendar [ {0} ] >> Event Details*****--%></P>
	<%}%>



<%

		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

	   	   }

		} else if (calledFrom.equals("P")) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		else if(calledFrom.equals("L")){

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));

		}








		if(eventmode.equals("M"))

		{

		   eventId = request.getParameter("eventId");

//SV, 10/31/04, I don't know why this fetches rows from event_def for a study mode
// while coming from event browser : if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || ((calledFrom.equals("S") && (fromPage.equals("eventbrowser")) || fromPage.equals("selectEvent") )) )
if ((calledFrom.equals("P")) || (calledFrom.equals("L")) || (calledFrom.equals("S") && fromPage.equals("selectEvent") ))
		    {

  		  		eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));

				eventdefB.getEventdefDetails();

			    eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
		  		eventName = (eventName==null)?"":eventName;
		  		if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      			eventName=eventName.substring(0,1000);
          		}

			    eventSeq = eventdefB.getEventSequence();
			    eventSeq=(eventSeq==null)?"":eventSeq;

   			   	eventDesc = eventdefB.getDescription();
				eventDesc = (eventDesc == null)?"":(eventDesc) ;

	    		eventDuration = eventdefB.getDuration();
	    		eventDurationUnit = eventdefB.getDurationUnit();

		   	    eventFuzzyPerDays = eventdefB.getFuzzy_period();

  	   	      	eventNotes = eventdefB.getNotes();
				eventNotes = (   eventNotes  == null      )?"":(  eventNotes ) ;


			   	categoryId = eventdefB.getChain_id();

			   	cptCode=eventdefB.getCptCode();
				eventFuzzyPerDaysA=eventdefB.getFuzzyAfter();
				durationUnitAfter=eventdefB.getDurationUnitAfter();
				durUnitB=eventdefB.getDurationUnitBefore();
				evtLibType = eventdefB.getEventLibType();

				eventSOSId = EJBUtil.stringToNum(eventdefB.getEventSOSId());
				eventFacilityId = EJBUtil.stringToNum(eventdefB.getEventFacilityId());
				eventCoverageType = EJBUtil.stringToNum(eventdefB.getEventCoverageType());
				
		   	 }else

			 {

	  		  	eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));

				eventassocB.getEventAssocDetails();

			    eventName = eventassocB.getName();
				//JM: 12Nov2009: #4399
		  		eventName = (eventName==null)?"":eventName;
		  		if (eventName.length()>1000){//for the old existing event names > 1000 or larger
 	      			eventName=eventName.substring(0,1000);
          		}
			    eventSeq = eventassocB.getEventSequence();
			    eventSeq=(eventSeq==null)?"":eventSeq;

   		   		eventDesc = eventassocB.getDescription();
   		   		eventDesc = (eventDesc == null)?"":(eventDesc) ;
   		   	
		      	eventDuration = eventassocB.getDuration();

		      	eventDurationUnit = eventassocB.getDurationUnit();

	   	      	eventFuzzyPerDays = eventassocB.getFuzzy_period();

	   	      	if (StringUtil.isEmpty(eventFuzzyPerDays)) eventFuzzyPerDays="0";

  	   	      	eventNotes = eventassocB.getNotes();
  	   			eventNotes=(eventNotes==null)?"":eventNotes;

				cptCode=eventassocB.getCptCode();

				eventFuzzyPerDaysA=eventassocB.getFuzzyAfter();
				durationUnitAfter=eventassocB.getDurationUnitAfter();
				durUnitB=eventassocB.getDurationUnitBefore();
				evtLibType = eventassocB.getEventLibType();
				
				eventSOSId = EJBUtil.stringToNum(eventassocB.getEventSOSId());
				eventFacilityId = EJBUtil.stringToNum(eventassocB.getEventFacilityId());
				eventCoverageType = EJBUtil.stringToNum(eventassocB.getEventCoverageType());
		   	 }

		if (StringUtil.isEmpty(eventFuzzyPerDaysA)) eventFuzzyPerDaysA="0";
		durUnitB=(durUnitB==null)?"":durUnitB;
		durationUnitAfter=(durationUnitAfter==null)?"":durationUnitAfter;
		cptCode=(cptCode==null)?"":cptCode;

		   if(eventFuzzyPerDays.equals("0")){

			eventFuzzyPer = "0";

		   } else {

			eventFuzzyPer = "1";

			if(eventFuzzyPerDays.substring(0,1).equals("+")) {

			   eventFuzzyPerBA = "+";

				eventFuzzyPerDays = eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length());

				}else if(eventFuzzyPerDays.substring(0,1).equals("-")) {

			   eventFuzzyPerBA = "-";

			eventFuzzyPerDays = eventFuzzyPerDays.substring(1,eventFuzzyPerDays.length());

			} else  {

			   eventFuzzyPerBA = "";

			   eventFuzzyPerDays = eventFuzzyPerDays.substring(0,eventFuzzyPerDays.length());



			}

		   }
		   eventFuzzyPerDaysA=(eventFuzzyPerDaysA==null)?"":eventFuzzyPerDaysA;
		   if (eventFuzzyPerDaysA.length()>0){
		   if(eventFuzzyPerDaysA.equals("0")){


		   } else {


			if(eventFuzzyPerDaysA.substring(0,1).equals("+")) {

			   eventFuzzyPerBAA = "+";

				eventFuzzyPerDaysA = eventFuzzyPerDaysA.substring(1,eventFuzzyPerDaysA.length());

				}else if(eventFuzzyPerDaysA.substring(0,1).equals("-")) {

			   eventFuzzyPerBAA = "-";

			eventFuzzyPerDaysA = eventFuzzyPerDaysA.substring(1,eventFuzzyPerDaysA.length());

			} else  {

			   eventFuzzyPerBAA = "";

			   eventFuzzyPerDaysA = eventFuzzyPerDaysA.substring(0,eventFuzzyPerDaysA.length());



			}

		   }
}




		} else {

			categoryId = request.getParameter("categoryId");
			
		}
		
		ddSOS=siteDao.toPullDown("ddSos",eventSOSId, " ");
		ddFacility=siteDao.toPullDown("ddFacility",eventFacilityId, " ");
		ddCoverage = schDao.toPullDown("ddCoverage",eventCoverageType, " ");

	//KM-04Sep09 changes for category dropdown
	ctrldao= eventdefB.getHeaderList(accId,"L",libTypeId);

	ArrayList catIds = ctrldao.getEvent_ids();

	ArrayList catNames = ctrldao.getNames();



		int length = catIds.size();

    //Define a session variable to store parameter values to pass to Selectevent.jsp

    if (mainmode!=null)

    {

		ArrayList eventparam = new ArrayList() ;



		if (categoryId!=null) eventparam.add(0,categoryId);

		if (mode!=null) eventparam.add(1,mode);

		if (cost!=null)	eventparam.add(2,cost);

		if (duration!=null) eventparam.add(3,duration);

		if (calStatus!=null) eventparam.add(4,calStatus);

		if (calledFrom!=null)eventparam.add(5,calledFrom);

		if (protocolId!=null)eventparam.add(6,protocolId);

		if (catName!=null)eventparam.add(7,catName);

		 if (src!=null) eventparam.add(8,src);

		tSession.putValue("EventParam" , eventparam);

	}

//end setting up session varaible for select event



%>

 <% if (EJBUtil.stringToNum(patProtKey) == 0) {%>
<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="eventmode" value="<%=eventmode%>"/>
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="calStatus" value="<%=calStatus%>"/>
<jsp:param name="displayDur" value="<%=displayDur%>"/>
<jsp:param name="displayType" value="<%=displayType%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="calassoc" value="<%=calAssoc%>"/>
<jsp:param name="eventName" value="<%=StringUtil.encodeString(eventName)%>"/>
 </jsp:include>
<% } %>
<%

if(eventmode.equals("N")) {

%>

   <form name="eventdetail" id="evtdetailsfrm" METHOD=POST action="eventdetailsave.jsp" onsubmit="if (setVals('Submit',document.eventdetail)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<%

} else {
	//SV, 10/18/04, added eventbrowser to list: cal-enh-bug #1807.
	if (!(fromPage.equals("patientschedule")) && !(fromPage.equals("fetchProt")) && !(fromPage.equals("selectEvent")) && !(fromPage.equals("eventbrowser"))) {

		if (pageRight > 4) {

%>
	<!--KM--04Sep09 -->

   <A href="copyevent.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=<%=fromPage%>&from=initial&categoryId=<%=categoryId%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&pageRight=<%=pageRight%>&evtLibType=<%=evtLibType%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=LC.L_CopyEvt_Upper%><%-- COPY THIS EVENT*****--%></A>

<%

		}

	}

%>

   <form name="eventdetail" id="evtdetailsfrm" METHOD=POST action="eventdetailsave.jsp" onsubmit="if (setVals('Submit',document.eventdetail)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<%

}

%>



<input type=hidden name=srcmenu value=<%=src%>>

<input type=hidden name=eventmode value=<%=eventmode%>>

<input type=hidden name=eventId value=<%=eventId%>>

<input type=hidden name=duration value=<%=duration%>>

<input type=hidden name=protocolId value=<%=protocolId%>>

<input type=hidden name=calledFrom value=<%=calledFrom%>>

<input type=hidden name=mode value=<%=mode%>>

<input type=hidden name=fromPage value=<%=fromPage%>>

<input type=hidden name=calStatus value=<%=calStatus%>>

<input type=hidden name=displayDur value=<%=displayDur%>>

<input type=hidden name=displayType value=<%=displayType%>>

<input type=hidden name=catName value=<%=catName%>>

<input type=hidden name=cost value=<%=cost%>>

<input type="hidden" name="calassoc" value="<%=calAssoc%>">

<input type="hidden" name="offLineFlag" value="<%=offLnFlag%>"><!--//JM: 28Apr2008, added for, Enh. #C9 -->

<Input name="calledFromPage" type=hidden value=<%=calledFromPg%>><!--//JM: 25June2008, added for, #3531 -->

<Input name="patId" type=hidden value=<%=patientId%>><!--//JM: 25June2008, added for, #3531 -->

<Input name="patProtId" type=hidden value=<%=patProtKey%>><!--//JM: 25June2008, added for, #3531 -->
<Input name="displacement" type=hidden value=<%=dsplcmnt%>>
<Input name="visitId" type=hidden value=<%=visitNumber%>>

<%

//JM: 22Feb2011: #5857
String calStatDesc = "";

SchCodeDao scho = new SchCodeDao();
	
if (calledFrom.equals("P") || calledFrom.equals("L")){
	calStatDesc = scho.getCodeDescription(scho.getCodeId("calStatLib",calStatus));
}else{
	calStatDesc = scho.getCodeDescription(scho.getCodeId("calStatStd",calStatus));	
}

if ((calStatus.equals("A") && !"selectEvent".equals(fromPage)) || (calStatus.equals("F"))){%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_CntChgEvtDet_PcolStat%><%--Changes cannot be made to Event Interval and Event Details for a Protocol with status *****--%>&nbsp;<%=calStatDesc%></Font></P>     <%--Akshi: Added for Bug#7646 --%>

<%}

if (pageRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_OnlyViewPerm_ForEvt%><%-- You have only View permission for the Event.*****--%></Font></P>

<%}%>





<TABLE width="100%" cellspacing="0" cellpadding="0" class="basetbl MidAlign" >
	<tr height="6"><td colspan="2"></td></tr>

	<% if(eventmode.equals("N")){ %>
	<tr>

	<td > <%=LC.L_Evt_Lib%><%-- Event Library Type*****--%> <FONT class="Mandatory">* </FONT> </td>
	<td>

		 <%=libTypePullDn%>

	</td>
	</tr>
		 <%} else {%>

		<input type=hidden id="cmbLibType" name="cmbLibType" value="<%=evtLibType==null?"":evtLibType%>">
		<%}%>
<%

		if(eventmode.equals("N")) {

%>

   <tr>

	<td > <%=LC.L_Select_EvtCat%><%-- Select Event Category*****--%> <FONT class="Mandatory">* </FONT> </td>


	 <td > <span id = "span_catId">

	  <select  name="categoryId">

<%--
//KM-#4387
	for(int i = 0; i< length;i++)

	{

%>

	<option name="categoryId" value="<%=catIds.get(i)%>"> <%=catNames.get(i)%> </option>

<%

	}

--%>

	<OPTION value='' SELECTED> <%=LC.L_Select_AnOption%><%-- Select an Option*****--%> </OPTION>

	</select>
	</span>
	</td>

   </tr>

<%

} else {

%>

<input type=hidden name=categoryId value=<%=categoryId%>>

<%

}

//JM: 18Apr2008: #3406
if ((request.getParameter("fromPage")).equals("fetchProt")){
%>

	<!--JM: 03Apr2008: Added the sequence-->
	<tr>

	<td ><%=LC.L_Sequence%><%-- Sequence*****--%><FONT class="Mandatory">* </FONT> </td>

	<td >

		<INPUT NAME="eventSeq" value="<%=eventSeq%>" TYPE=TEXT SIZE=3 MAXLENGTH=5 >
	</td>

   </tr>

<%}%>



	<tr>

	<td width="15%" ><%=LC.L_Event_Name%><%-- Event Name*****--%></b> <FONT class="Mandatory">* </FONT> </td>

	<td width="83%">
<%
	//JM: 25Apr2008, added for, Enh. #C9
	if (calStatus.equals("O") && (offLnFlag.equals("1")||offLnFlag.equals("2"))){
%>
		<INPUT NAME="eventName" value="<%=eventName%>" TYPE=TEXT SIZE=30  readOnly="true">
<%} else{%>
		<INPUT NAME="eventName" value="<%=eventName%>" TYPE=TEXT SIZE=30  MAXLENGTH="1000">
<%}%>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="#" onClick="openWinAdditinalCode('<%=eventId%>','<%=eventmode%>','evtaddlcode','<%=calledFrom%>');"><img src="../jsp/images/moreInfo.png" border="0" title="<%=LC.L_Addl_CodeDets_Upper%><%-- ADDITIONAL CODE DETAILS*****--%>"></A>

	</b></td>

   </tr>

   <tr>

	<td> <%=LC.L_Description%><%-- Description*****--%> </td>

	<td > <INPUT NAME="eventDesc" value="<%=eventDesc%>" TYPE=TEXT SIZE=30 MAXLENGTH=200> </td>

   </tr>

      <tr>

	<td > <%=LC.L_Cpt_Code%><%-- CPT Code*****--%> </td>

	<td > <INPUT NAME="cptcode" value="<%=cptCode%>" TYPE=TEXT SIZE=30 MAXLENGTH=50> </td>

   </tr>


   <tr>

	<td> <%=LC.L_Evt_Duration%><%-- Event Duration*****--%> </td>

	<td > <INPUT NAME="eventDuration" value="<%=eventDuration%>" TYPE=TEXT SIZE=5 MAXLENGTH="5"><!--//KV:Fixed BugNo:4959 -->

        <select name="eventDurDays">

		<option value="D" <%if (eventDurationUnit.equals("D")){%> Selected <%}%>><%=LC.L_Days%><%-- Days*****--%></option>

		<option value="W" <%if (eventDurationUnit.equals("W")){%> Selected <%}%>><%=LC.L_Weeks%><%-- Weeks*****--%></option>

	   </select>

	</td>

   </tr>

   <tr>

	<td > <%=LC.L_Event_Window%><%-- Event Window*****--%> </td>

	<td > <INPUT NAME="eventFuzzyPerDays" value="<%=eventFuzzyPerDays%>" TYPE=TEXT SIZE=5 MAXLENGTH="3">
	<Select Name="durUnitB">
	<Option value="D" <%if (durUnitB.equals("D")){%> Selected <%}%>><%=LC.L_Days%><%-- Days*****--%></Option>
	<Option value="H" <%if (durUnitB.equals("H")){%> Selected <%}%>><%=LC.L_Hours%><%-- Hours*****--%></Option>
	<Option value="M" <%if (durUnitB.equals("M")){%> Selected <%}%>><%=LC.L_Months%><%-- Months*****--%></Option>
	<Option value="W" <%if (durUnitB.equals("W")){%> Selected <%}%>><%=LC.L_Weeks%><%-- Weeks*****--%></Option>
	</Select>
	<%=LC.L_Before%><%-- Before*****--%>
	</td>

   </tr>
   <tr>
   	<td ></td>
	<td > <INPUT NAME="eventFuzzyPerDaysA" value="<%=eventFuzzyPerDaysA%>" TYPE=TEXT SIZE=5 maxlength="3">
	<Select Name="durUnitA">
	<Option value="D" <%if (durationUnitAfter.equals("D")){%> Selected <%}%>><%=LC.L_Days%><%-- Days*****--%></Option>
	<Option value="H" <%if (durationUnitAfter.equals("H")){%> Selected <%}%>><%=LC.L_Hours%><%-- Hours*****--%></Option>
	<Option value="M" <%if (durationUnitAfter.equals("M")){%> Selected <%}%>><%=LC.L_Months%><%-- Months*****--%></Option>
	<Option value="W" <%if (durationUnitAfter.equals("W")){%> Selected <%}%>><%=LC.L_Weeks%><%-- Weeks*****--%></Option>	</Select>
	<%=LC.L_After%><%-- After*****--%>
	</td>

   </tr>

	<tr>
		<td > <%=LC.L_Site_OfService%><%-- Site of Service*****--%> </td>
		<td>
			 <%=ddSOS%>
		</td>
	</tr>
	<tr>
		<td > <%=LC.L_Facility%><%-- Facility*****--%> </td>
		<td>
			 <%=ddFacility%>
		</td>
	</tr>
	<tr>
		<td > <%=LC.L_Coverage_Type%><%-- <%=VelosResourceBundle.getLabelString("Evt_CoverageType")%>*****--%> </td>
		<td>
			 <%=ddCoverage%>
		</td>
	</tr>

   <tr>

	<td > <%=LC.L_Notes%><%-- Notes*****--%> </td>

	<td > <TEXTAREA name="eventNotes"  rows=3 cols=30><%=eventNotes%></TEXTAREA> </td>



   </tr>
<tr height="6"><td colspan="2"></td></tr>
</table>

<%if (fromPage.equals("fetchProt"))   { %>
	<p><font class="mandatory" size=2 ><%=MC.M_ChgEvtName_DescNotApplied%><%-- Changes to Event Name and Description will not be applied to other events when you select 'Apply to all Events in this Visit'*****--%></font></p>
<% } %>

<jsp:include page="propagateEventUpdate.jsp" flush="true">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="eventdetail"/>
<jsp:param name="eventName" value="<%=StringUtil.encodeString(eventName)%>"/>
</jsp:include>



<%

////JM: 15May2008, added for, issue #3464
if ((calStatus.equals("A") && !"selectEvent".equals(fromPage)) || (calStatus.equals("F"))|| (calStatus.equals("D")))

{

}else{

    int addAnotherWidth = 0;
	if ((eventmode.equals("M") && pageRight >=6) || (eventmode.equals("N") && (pageRight == 5 || pageRight == 7 )) ) {
	    if (fromPage.equals("eventlibrary")|| fromPage.equals("selectEvent")) {
	        addAnotherWidth = 200;
	    }
%>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="searchBg midalign">
<tr height="15" >
  <td valign="top" >
 <% if (fromPage.equals("selectEvent")) { %>
		<A href="selecteventus.jsp?fromPage=NewEvent&calledFromPage=<%=calledFromPg%>&visitId=<%=visitNumber%>&patId=<%=patientId%>&patProtId=<%=patProtKey%>&displacement=<%=dsplcmnt%>" type="submit"><%=LC.L_Back%></A>
 <% } %>

 <% if (fromPage.equals("eventlibrary")) { %>
		<A href="eventlibrary.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>" type="submit"><%=LC.L_Back%></A>
 <% } %>
  </td>
  
  <td  valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="evtdetailsfrm"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="noBR" value="Y"/>
	</jsp:include>
  </td>
<% if (addAnotherWidth > 0) {  %>
  <td valign="top" width="15%" align="left">
  <table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0"  >
	<tr valign="top" ><td valign="top" align="right" >
	<button type="button" id="add_another" onClick="return setParam('Add',document.eventdetail);"><%=LC.L_Submit_AddAnother%></button>
    </td></tr>
  </table>
  </td>
<% } %>
</tr>

<% }

}%>

 </tr>

</table>
	<input type=hidden name=pageRight value=<%=pageRight%> >
	<%--sonika--%>
   <input type="hidden" name="subVal" value="Submit">
 	<!--KM-->
   <input type="hidden" name="agentVal" value=<%=agentVal%>>

   <input type="hidden" name="selCategoryId" id="selCategoryId" value="">

</form>







<%



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}

//KM
if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
%>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

<%

}

else {

%>



  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>



</div>

<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>

	<% }%>

</body>



</html>





