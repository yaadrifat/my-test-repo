<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head> 
<title><%=LC.L_Manage_Mstones%><%--Manage Milestones*****--%></title> 

<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.appendix.AppendixJB,com.velos.eres.web.user.UserJB" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

			 
<script type="text/javascript"> 
  
</script> 
</head> 
   	 
<%
String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
	
<body> 
	
<%
	HttpSession tSession = request.getSession(true);
	
	if (sessionmaint.isValidSession(tSession))
	{
	
					String defUserGroup = (String) tSession.getAttribute("defUserGroup");

					String studyId = request.getParameter("studyId");
					
					String accId=(String)tSession.getAttribute("accountId");
					 
			 	    String userId = (String) tSession.getValue("userId");
				    UserJB userB = (UserJB) tSession.getValue("currentUser");
			   	    String siteId = userB.getUserSiteId();
					int protocolPK= EJBUtil.stringToNum(request.getParameter("protocolId"));
					
					String calName = request.getParameter("calName");
					
					if (StringUtil.isEmpty(calName))
					{
						calName="";
					}
					
					%>
 	<DIV class="BrowserTopn" id="div1">
	 				<jsp:include page="protocoltabs.jsp" flush="true"/>
	</div>
	<DIV class="BrowserBotN BrowserBotN_Managemilestone" id="div2">

				<table border="1" width="100%" height="100%" cellpadding="8" cellspacing="8"> 
				<tr> 
				 
					<td valign="top" width="99%"> 
<%
int startAt = StringUtil.trueValue(request.getQueryString()).indexOf("milestoneParams=");
String protLinkedParams  = startAt < 0 ? StringUtil.trueValue(request.getQueryString()) : StringUtil.trueValue(request.getQueryString()).substring(0,startAt-1);
String milestoneParams = startAt < 0 ? "" : StringUtil.trueValue(request.getQueryString()).substring(startAt+"milestoneParams=".length());
%>
	                	<jsp:include page="milestone.jsp" flush="true">
		                	<jsp:param name="calName" value="<%=calName%>" />
		                	<jsp:param name="studyId" value="<%=studyId%>" />
		                	<jsp:param name="includeMenus" value="N" />
		                	<jsp:param name="protLinkedParams"
		                	    value="<%=StringUtil.htmlEncodeXss(protLinkedParams)%>" />
		                	<jsp:param name="milestoneParams"
		                	    value="<%=StringUtil.htmlEncodeXss(milestoneParams)%>" />
	                	</jsp:include>
					 </td> 
					 	
				</tr> 
				</table> 

 	<% }
		else
		{
		%>
			<jsp:include page="timeout.html" flush="true"/>
		<%
		}
		%>
		

	  <div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	
	</div>
			  
	<div class ="mainMenu" id = "emenu">
	  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
	</div>
		
</body> 
</html>