<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil" %>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>


<title><%=LC.L_Std_Wizard%><%--<%=LC.Std_Study%> Wizard*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>





<body>
<% String src;

src= request.getParameter("srcmenu");

%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<br>
<DIV class="formDefault" id="div1">
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id ="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
  <jsp:useBean id ="accountB" scope="session" class="com.velos.eres.web.account.AccountJB"/>
  <%

int pageRight = 0;

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

 {

   String uName = (String) tSession.getValue("userName");

   String accountId = (String) tSession.getValue("accountId");

   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));

   tSession.putValue("studyId","");

   tSession.putValue("studyNo","");

   accountB.setAccId(EJBUtil.stringToNum(accountId));
   accountB.getAccountDetails();

    if (pageRight > 4 )

	{

	    String uSession  = (String) tSession.getValue("userSession");

	    if(studyB.getStudyCount(accountId) > 0 && (accountB.getAccType()).equals("E")){


%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "sectionHeadings" align = center> <%=MC.M_EvalAcc_CntEtrStd%><%--This is an Evaluation Account and you already have a <%=LC.Std_Study_Lower%>. You cannot enter more <%=LC.Std_Studies%>. Please contact Velos Administrator.*****--%></p>
<form method="POST">
  <table align=center>
    <tr width="100%">
      <td width="100%" >
	  <A href = "javascript:history.go(-1)"><img border="0" style = "border-style:none" src="../images/jpg/Return.gif" align="absmiddle"></A>
       <!--<input type="button" name="return" value="Return" onClick="window.history.back()"> -->
      </td>
    </tr>
  </table>
</form>


<%
		} else {
%>

  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=MC.M_MngPcol_New%><%--Manage Protocols >> New*****--%> </P>
  <Form name = "wiz" method="post" action="study.jsp" >
    <Input type="hidden" name="selectedTab" value="1">
    <Input type="hidden" name="srcmenu" value=<%=src%>>
    <Input type="hidden" name="mode" value="N">
    <table width="600" cellspacing="0" cellpadding="0" border=0>
      <tr>
        <td width = 130></td>
        <td>
          <P class = "bigNumbers"><%=LC.L_1%><%--1*****--%></p>
        </td>
        <td>
          <P class = "defComments"> <br>
            <br>
            <%=MC.M_EtrStdDets_DescribeStd%><%--Enter <%=LC.Std_Study%> Details to identify and describe your <%=LC.Std_Study_Lower%>*****--%>
 </P>
        </td>
      </tr>
    </table>
    <table width="600" cellspacing="0" cellpadding="0">
      <tr>
        <td width = 260></td>
        <td width=30>
          <P class = "bigNumbers"><%=LC.L_2%><%--2*****--%></p>
        </td>
        <td>
          <P class = "defComments"> <br>
            <br>
            <%=MC.M_AddSecAccoc_DocuStd%><%--Add customized Sections and Associate online documents in <%=LC.Std_Study%> Appendix*****--%>
          </P>
        </td>
      </tr>
    </table>
    <table width="600" cellspacing="0" cellpadding="0">
      <tr>
        <td width = 200></td>
        <td width=30>
          <P class = "bigNumbers"><%=LC.L_3%><%--3*****--%></p>
        </td>
        <td>
          <P class = "defComments"> <br>
            <br>
            <%=MC.M_CreateStdTeam_AssignRgt%><%--Create your own <%=LC.Std_Study%> Team and Assign Rights*****--%> </P>
        </td>
      </tr>
    </table>
    <br>
    <br>
    <table width="600" >
      <tr>
        <td align="right">
          <%

    if (pageRight > 4 )

	{

%>
         <!--<input type="Submit" name="submit" value="Proceed">-->
	  <input type="image" src="../images/jpg/Proceed.gif" align="absmiddle" border="0">

          <%

	}

else

	{

%>
          <!--<input type="Submit" name="submit" value="Proceed" DISABLED>-->
          <%

	}

%>
        </td>
      </tr>
    </table>
  </Form>


<%
	} //end of if for evaluation account type checking
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id="emenu">
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>

