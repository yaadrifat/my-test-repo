<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title>Import</title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

</head>

<SCRIPT Language="javascript">

	function openwin1(frm) {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
	;}


 function  validate(formobj){
	  if (!(validate_col('studynumber',formobj.studyNumber))) return false
  	  if (!(validate_col('prinInv',formobj.prinInv))) return false

	  if (!(validate_col('file',formobj.expfile))) return false
	  
	  if (! TestFileType(formobj.expfile.value, ['.req'])) return false 
		
      if (!(validate_col('Esign',formobj.eSign))) return false

	 if(isNaN(formobj.eSign.value) == true) {
		alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		return false;

	}
	
	function TestFileType( fileName, fileTypes ) {
      if (!fileName) return;
      
      dots = fileName.split(".")
      //get the part AFTER the LAST period.
      fileType = "." + dots[dots.length-1];
      
	 
      if (fileTypes.join(".").indexOf(fileType) != -1) 
	  {
		return true;
	  }
	  else
	  {
		 alert("Please select a valid: [" + (fileTypes.join(" .")) + "] file and try again.");
		 return false;
	  }
   
     }


   }


</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.impex.*"%>





<% String src;
	src= request.getParameter("srcmenu");
	src = "";
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>

<br>

<DIV class="formDefault" id="div1">

<P class="sectionHeadings"> Manage Protocols >> Import </P>	 
		
<p class="defComments">Please select the <font color="red"><b> eresearch.req </b></font> file provided with the export files:</p>

  <%

	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))

	{

	    String usr = null;
		usr = (String) tSession.getValue("userId");
		String uName = (String) tSession.getValue("userName");
		String accId = (String) tSession.getValue("accountId");
	 	String ipAdd = (String) tSession.getValue("ipAdd");	

	%>
	 <P class = "userName"><%= uName %></p>
	 
	 <form name="study" method="POST" action="startImport.jsp" onsubmit="return validate(document.study);">  

	 <input type="hidden" name = "impAccountId"  value = <%=accId%>>
  	 <input type="hidden" name = "impUserId"  value = <%=usr%>>
   	 <input type="hidden" name = "impIpAdd"  value = <%=ipAdd%>>
   	 <input type="hidden" name = "importCategory"  value = 'study_import'>
	 
		 
	<table>
	<tr> 
      <td> Study Number <FONT class="Mandatory">* </FONT> </td>
	<td> 
      <input type="text" name="studyNumber" size = 15 MAXLENGTH = 20 >
	 </td>
	 </tr> 
	   <tr> 
	  	<td> Principal Investigator
	    </td>
	     <td> 
			<input type=hidden name="prinInv"> 
			<input type=text name="prinInvName"  readonly> 
		<A HREF=# onClick=openwin1('studyinv') >Select User</A>
		</td>
		</tr>
	
	<tr> 
        <td width="35%"> File <FONT class="Mandatory">* </FONT> </td>
        <td width="65%"> 
          <input type=file name=expfile size=40 accept="text/.req">
        </td>
      </tr>
	</table>
	<table width = 70% border = 0><tr><td align = "right">
      <tr>
	  <td><br>
   	        e-Signature <FONT class="Mandatory">* </FONT>
	</td>
	<td><br>
       	<input type="password" name="eSign" maxlength="8">
	</td>
	<td><br>
		<button type="submit"><%=LC.L_Submit%></button>
	</td></tr>
	</table>
	
	</form>
   
	<%
		//get export module information
	
	

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}


%>


  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>



</div>

<div class ="mainMenu" id = "emenu">
	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>





