<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>
<!-- Changes By Sudhir 0n 02-Apr-2012 for Bug #9096 -->
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
%>
<title><%=MC.M_ResCompApp_UploadDoc %></title>
<% } else { %>
<TITLE><%=LC.L_Study_Appendix %><%-- <%=LC.Std_Study%> >> Appendix*****--%></TITLE>
<% } %>
<!-- Changes By Sudhir 0n 02-Apr-2012 for Bug #9096 -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
	String appendixId=request.getParameter("appId");
	String studyVerId = request.getParameter("studyVerId");
	String tab = request.getParameter("selectedTab");
	int appId=EJBUtil.stringToNum(appendixId);

	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>
	<FORM name="deleteFile" id="appDelFileId" method="post" action="appendix_file_delete.jsp" onSubmit="if (validate(document.deleteFile)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="appDelFileId"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
 	 <input type="hidden" name="appId" value="<%=appendixId%>">
 	 <input type="hidden" name="studyVerId" value="<%=studyVerId%>">	 
	 <input type="hidden" name="selectedTab" value="<%=tab%>">
		  
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	appendixB.setId(appId);

	int i=0;

	// Modified for INF-18183 ::: AGodara 
	i=appendixB.removeAppendix(AuditUtils.createArgs(tSession,"",LC.L_Study));     // Fixed Bug#7604 : Raviesh 
	// Added By RA Bug 4599 Dispaly message on successful delete
	%>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<BR>
	<% if(i == 0) {%>
		<p class = "successfulmsg" align = center><%=MC.M_Data_DelSucc%><%--Data deleted successfully*****--%></p>
	<%}%>
	<META HTTP-EQUIV=Refresh CONTENT="0; URL=appendixbrowser.jsp?selectedTab=<%=tab%>&srcmenu=<%=src%>&studyVerId=<%=studyVerId%>">
<%
	} //end esign
	} //end of delMode	
}//end of if body for session

else {
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>

</BODY>
</HTML>


