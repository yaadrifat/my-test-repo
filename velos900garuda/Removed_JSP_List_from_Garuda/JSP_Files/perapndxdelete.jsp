<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_PatAppx%><%--Delete Patient Appendix*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils"%>
<%@ page import="com.velos.eres.service.util.*"%>
<jsp:useBean id="perApndxB" scope="request" class="com.velos.eres.web.perApndx.PerApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<% String src;
src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%
	String patId= "";
	String perApndxId= "";
	String selectedTab="";

HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))	{
		perApndxId= request.getParameter("perApndxId");
		patId= request.getParameter("patId");
		selectedTab=request.getParameter("selectedTab");

	 	String studyId = null;
 		String statDesc = null;
	 	String statid = null;
 		String studyVer = null;
 		studyId = request.getParameter("studyId");
	 	statDesc = request.getParameter("statDesc");
 		statid = request.getParameter("statid");
	 	studyVer = request.getParameter("studyVer");
		studyId = request.getParameter("studyId");

		int ret=0;

		String delMode=request.getParameter("delMode");

		if (delMode.equals("null")) {
			delMode="final";
%>
	<FORM name="perApndxDelete" id="perapndxdelid" method="post" action="perapndxdelete.jsp" onSubmit="if (validate(document.perApndxDelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>

	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>

		</table>

	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="perapndxdelid"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="perApndxId" value="<%=perApndxId%>">
  	 <input type="hidden" name="patId" value="<%=patId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	 <input type=hidden name="statDesc" value="<%=statDesc%>">
	 <input type=hidden name="statid" value="<%=statid%>">
	 <input type=hidden name="studyVer" value="<%=studyVer%>">
	 <input type=hidden name="studyId" value="<%=studyId%>">

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
			// Modified for INF-18183 ::: AGodara
			ret =perApndxB.removePerApndx(EJBUtil.stringToNum(perApndxId),AuditUtils.createArgs(tSession,"",LC.L_Manage_Pats));
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_NotDelSucc%><%--Data not deleted successfully*****--%> </p>
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully*****--%></p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=perapndx.jsp?srcmenu=<%=src%>&pkey=<%=patId%>&selectedTab=<%=selectedTab%>&page=patientEnroll&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>">
			<%}
			} //end esign
	} //end of delMode
  }//end of if body for session
else { %>
 <jsp:include page="timeout.html" flush="true"/>
 <% } %>
 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>
</HTML>
