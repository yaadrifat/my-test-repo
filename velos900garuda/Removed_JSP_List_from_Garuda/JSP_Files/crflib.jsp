<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<%
	String fromPage = request.getParameter("fromPage");

	if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {		
	%>
		<title> <%=MC.M_EvtLibCrf_AccocCrf%><%--Event Library >> Event CRF >> Associated CRF*****--%></title>
	<%
	} else {%>
		<title><%=LC.L_Assoc_Crf%><%--Associated CRF*****--%></title>	
	<%
	}%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% if ((request.getParameter("fromPage")).equals("selectEvent")||  (request.getParameter("fromPage")).equals("fetchProt"))

{ %>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp --%>
	

	

 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))

	

	%>



<SCRIPT language="JavaScript1.1">

 function  validate(formobj){


     if (!(validate_col('Crf Number',formobj.crflibNumber))) return false

     if (!(validate_col('Crf Name',formobj.crflibName))) return false

    



	if (!(validate_col('e-Signature',formobj.eSign))) return false



	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   }



   }



</SCRIPT>



</head>





<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<!--  Virendra: Fixed Bug no. 4736,added import com.velos.eres.service.util.StringUtil-->
<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.web.crflib.CrflibJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.StringUtil"%>

<jsp:useBean id="crflibB" scope="request" class="com.velos.esch.web.crflib.CrflibJB"/>

<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


<% 

String src;
//Virendra: Fixed Bug no. 4736,added variable addAnotherWidth 
int addAnotherWidth = 0;

src= request.getParameter("srcmenu");

//String eventName = request.getParameter("eventName");
String eventName = "";

%>



<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
%>
<jsp:include page="include.jsp" flush="true"/>

<%



}

 else{

%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   

<%}%>



<body>
<% if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){%>

		<DIV  class="formDefaultpopup" id="div1"> 

	<%	} 

else { %>

<DIV class="formDefault" id="div1"> 

<%}%>	

<%

	int pageRight = 0;

	String eventmode = request.getParameter("eventmode");   

	String duration = request.getParameter("duration");   

	String protocolId = request.getParameter("protocolId");   

	String calledFrom = request.getParameter("calledFrom");   

	String mode = request.getParameter("mode");   

	String calStatus= request.getParameter("calStatus");

	String displayDur=request.getParameter("displayDur");

	String displayType=request.getParameter("displayType");	 	  

  	String eventId = request.getParameter("eventId"); 

	String crflibmode=request.getParameter("crflibmode");

	String crflibId=request.getParameter("crflibId");

	String calAssoc=request.getParameter("calassoc");
    calAssoc=(calAssoc==null)?"":calAssoc;


	String categoryId = "";


	String eventDesc = "";

	String crflibNumber="";

	String crflibName="";

	HttpSession tSession = request.getSession(true); 

	String users ="";

	//KM
	if (calledFrom.equals("P")||calledFrom.equals("L"))
	{
		  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
		  eventdefB.getEventdefDetails();
		  eventName = eventdefB.getName();
	}else{	
		  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
		  eventassocB.getEventAssocDetails();     
		  eventName = eventassocB.getName(); 
	 }			


	if (sessionmaint.isValidSession(tSession))	{

	

		if (calledFrom.equals("S")) {

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 

	  	   if ((stdRights.getFtrRights().size()) == 0){

			 	pageRight= 0;

		   }else{

				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

    	   }	

		} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

		}

	

		String uName = (String) tSession.getValue("userName");

	%>		
		<P class = "userName"><%= uName %></p>
	  <%

		String calName = "";

		if (fromPage.equals("eventbrowser") || fromPage.equals("eventlibrary") || fromPage.equals("patientschedule") ) {			
	%>
		<!-- P class="sectionHeadings"> Event Library >> Event CRF >> Add CRF</P-->
	<%
		} else {
			calName = (String) tSession.getValue("protocolname");
	%>
		<P class="sectionHeadings"> <%Object[] arguments1 = {calName}; %>
	    <%=VelosResourceBundle.getMessageString("M_PcolCal_EvtAddCrf",arguments1)%><%--Protocol Calendar [ <%=calName%> ] >> Event CRF >> Add CRF*****--%> </P>
	<%
		 }
	%>

<jsp:include page="eventtabs.jsp" flush="true">
<jsp:param name="eventmode" value="<%=eventmode%>"/>
<jsp:param name="duration" value="<%=duration%>"/>
<jsp:param name="protocolId" value="<%=protocolId%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="calStatus" value="<%=calStatus%>"/>
<jsp:param name="displayDur" value="<%=displayDur%>"/>
<jsp:param name="displayType" value="<%=displayType%>"/>
<jsp:param name="eventId" value="<%=eventId%>"/>
<jsp:param name="src" value="<%=src%>"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
<jsp:param name="calassoc" value="<%=calAssoc%>"/>
 </jsp:include>	

	<%	 	 		

	 if(eventId == "" || eventId == null) {

	%>

	  <jsp:include page="eventDoesNotExist.jsp" flush="true"/>

  <%

	}else {



		if(crflibmode.equals("M")){  
			
  		  	  crflibB.setCrflibId(EJBUtil.stringToNum(crflibId));

  			  crflibB.getCrflibDetails();

		     	  crflibNumber=crflibB.getCrflibNumber();

			  crflibName=crflibB.getCrflibName();

		}

		

	%>


<form name="crflib" id="crflibFrmId" METHOD=POST action="crflibsave.jsp?srcmenu=<%=src%>&eventmode=<%=eventmode%>&eventId=<%=eventId%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>" onsubmit="if (validate(document.crflib)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<%
if (pageRight == 4) {

%>

   <P class = "defComments"><FONT class="Mandatory"><%=MC. M_OnlyViewPerm_ForEvt%><%--You have only View permission for the Event.*****--%></FONT></P>

<%}%>



<table width="60%" cellspacing="0" cellpadding="0" >

<tr>

	<td width="30%"> <%=LC.L_Crf_Number%><%--Crf Number*****--%> <FONT class="Mandatory">* </FONT> </td>

	<td width="30%"> <INPUT NAME="crflibNumber" value="<%=crflibNumber%>" > </td>

	<td > <INPUT NAME="crflibmode" value=<%=crflibmode%> type=hidden > </td>

	<td > <INPUT NAME="crflibId" value=<%=crflibId%> type=hidden > </td>

	<td > <INPUT NAME="displayType" value=<%=displayType%> type=hidden > </td>

	<td > <INPUT NAME="displayDur" value=<%=displayDur%> type=hidden > </td>

   </tr>

   <tr>

	<td width="30%"> <%=LC.L_Crf_Name%><%--Crf Name*****--%> <FONT class="Mandatory">* </FONT> </td>

	<td width="30%"><INPUT NAME="crflibName" value="<%=crflibName%>" type=text > </td>

	</tr>
 </table>



<br>

<jsp:include page="propagateEventUpdate.jsp" flush="true"> 
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="crflib"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>   




<%
String showSubmit = "";
if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) ) 
	{ showSubmit = "Y";} else { showSubmit = "N";}
	
%>

<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td align=center width=10% bgcolor="#cccccc" >
		<%if (fromPage.equals("eventbrowser")) {%>
			<A type="submit" href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></button>
  		<%}
		if (fromPage.equals("eventlibrary")) { %>
			<A type="submit" href="crflibbrowser.jsp?eventmode=<%=eventmode%>&eventId=<%=eventId%>&srcmenu=<%=src%>&selectedTab=6&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&fromPage=<%=fromPage%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></button>
  		<%}
		if (fromPage.equals("selectEvent")){ %>
			<button onclick="window.history.back();"><%=LC.L_Back%></button>	
		<%}%>
		</td>
		<!-- Virendra: Fixed Bug no. 4736,added td with addAnotherWidth -->
		<td bgcolor="<%=StringUtil.eSignBgcolor%>" valign="baseline" align="left" width="85%" <%=addAnotherWidth > 0 ? "" : "colspan=5" %>>
			<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="crflibFrmId"/>
				<jsp:param name="showDiscard" value="N"/>
				<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
				<jsp:param name="noBR" value="Y"/>
			</jsp:include>
		</td>
	</tr>
</table>

</form>



<%

	} //event



} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>



  <%if ((request.getParameter("fromPage")).equals("selectEvent") || (request.getParameter("fromPage")).equals("fetchProt")){
  
  %>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%
  }

else {

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>

	<% }%>
</DIV>
</body>
</html>

