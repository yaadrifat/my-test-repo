<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Bgt_Browser%><%--Budget Browser*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="no-cache">

</head>
<Script>

function openBudget(id,bgtType,bgtrights){

		src = document.budget.srcmenu.value;	
		frompage =document.budget.frompage.value;
		mode =document.budget.mode.value;
		if(bgtType=="Patient"){
		budget.action= "patientbudget.jsp?mode=" +mode + "&budgetId=" +id +"&srcmenu="+src + "&selectedTab=1";
		}
		else{		
		budget.action= "studybudget.jsp?mode=" +mode + "&budgetId=" +id +"&srcmenu="+src + "&selectedTab=1";	
		}
		
		

		budget.submit();
}

function setAction(formObj,src){
	lsearchCriteria=formObj.searchCriteria.value;		
	formObj.action="budgetbrowser.jsp?srcmenu="+src+"&searchCriteria="+lsearchCriteria;
	formObj.submit();
}

</Script>
<% String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<body>
<br>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*"%>
<% int accountId=0;  
   int pageRight = 0;
		   
HttpSession tSession = request.getSession(true); 
%>
<DIV class="browserDefault" id="div1">
	<%
	if (sessionmaint.isValidSession(tSession))
	{	
String searchCriteria = request.getParameter("searchCriteria");

if (searchCriteria==null) {searchCriteria="";}
%>
<P class="sectionHeadings"> <%=LC.L_BgtHome_Open%><%--Budget Home >> Open*****--%></P>  


<form name="budget" method=post action="budgetbrowser.jsp">
	<table width=100%>
<tr>
	<td width=25%> <P class="defComments"><%=LC.L_Search_ABgt%><%--Search a Budget*****--%></P></td>
	<td class=tdDefault width=25%> <Input type=text name="searchCriteria" size="20%"> </td>
    <td width=5% align="left">
    <button type="submit" onClick="setAction(document.budget,'<%=src%>')"><%=LC.L_Search%></button></td>
	<td width=45%><P class="defComments">(<%=MC.M_EtrStdNum_BgtName%><%--Enter a study number or keywords in the study or budget name*****--%>)</P></td>
</tr>		
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>		
		<td>
		<A href="budgetbrowser.jsp?&srcmenu=<%=src%>"><P class="defComments"><%=LC.L_View_AllBgts%><%--View All Budgets--%></P></A>
		</td>
</tr>
</table>
	<input type=hidden name="srcmenu" value=<%=src%>>
	<input type=hidden name=frompage value="budgetbrowser">
	<input type=hidden name=mode value="M">
	<% 
	   	String acc = (String) tSession.getValue("accountId");
	   	String uName = (String) tSession.getValue("userName");
		String userId = (String) tSession.getValue("userId");
	   	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	   	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("BUDGT"));
	   	accountId = EJBUtil.stringToNum(acc);
	  
	   	String grpName = null;
	   	String grpDesc = null;
	   	int counter = 0;
	   	if (pageRight > 0 )
		{
			Vector vec = new Vector();
			
			Column pkGrp = new Column("Budget Id");
			pkGrp.hidden = true;
			
			vec.add(pkGrp);		

			Column bgtType = new Column("Budget Type");			
		
			Column pkBgtName = new Column("Budget Name");
			pkBgtName.funcName = "openBudget";
			bgtType.passAsParameter = true;
			pkBgtName.width = "10%"	;
			pkBgtName.align = "left"	;		
			vec.add(pkBgtName);
			
			Column bgtVer = new Column("Version");
			bgtVer.width = "10%" ;
			bgtVer.align = "left"	;
			vec.add(bgtVer);
			

			bgtType.width = "10%" ;
			bgtType.align = "left"	;
			vec.add(bgtType);
			
			Column studyNum = new Column("Study Number");
			studyNum.width = "10%" ;
			studyNum.align = "left"	;
			vec.add(studyNum);
			
			
			Column studyTitle = new Column("Title");
			studyTitle.width = "10%" ;
			studyTitle.align = "left"	;
			vec.add(studyTitle);
			
	
			
			Column orgName = new Column("Organization");
			orgName.width = "10%" ;
			orgName.align = "left"	;
			vec.add(orgName);
			
			Column bgtStatus = new Column("Status");
			bgtStatus.width = "10%" ;
			bgtStatus.align = "left"	;
			vec.add(bgtStatus);
			
			
			
			String sSQL = 
				" select a.PK_BUDGET, a.budget_name, a.budget_version , decode(a.budget_type,'P','Patient','S','Study','null'), b.STUDY_NUMBER ," +
				 " b.STUDY_TITLE,  c.SITE_NAME ,  decode(a.BUDGET_STATUS,'W','Work in Progress','F','Freeze','null') from ESCH.sch_budget a, er_study b, er_site c " 
				+ " where a.pk_budget in (select d.fk_budget from ESCH.sch_bgtusers d" 
				+ "                        Where d.fk_user = " + userId+ " ) and " 
				+   "a.FK_SITE=c.PK_SITE(+) and  a.FK_STUDY=b.PK_STUDY(+) and "
				+ "(lower(b.study_number) like lower('%"+searchCriteria+"%') or lower(b.study_keywrds) like lower('%"+searchCriteria+"%') or  lower(a.budget_name) like lower('%"+searchCriteria+"%'))";
				 
			String title =MC.M_Flw_AreSvdBgts/*"	The Following are your saved Budgets"*****/+":";
			
			String srchType=request.getParameter("srchType");
			String sortType=request.getParameter("sortType");
			String sortOrder=request.getParameter("sortOrder");

			request.setAttribute("sSQL",sSQL);

			request.setAttribute("srchType",srchType);
			request.setAttribute("vec",vec);
			request.setAttribute("sTitle",title);

			String s_scroll=request.getParameter("scroll");
			request.setAttribute("scroll",s_scroll);
			request.setAttribute("sortType",sortType);
			request.setAttribute("sortOrder",sortOrder);
		%>
 		<table width="100%" cellspacing="0" cellpadding="0" border="0" >
			<tr> 
			    <td width = "50%"> 
			        <P class="defComments"><%=MC.M_Flw_AreSvdBgts%><%--The following are your saved Budgets*****--%>:</P>
			    </td>
			    <td width="50%" align="right"> 
				<p>
				
				  <A href="budget.jsp?mode=N&srcmenu=<%=src%>&fromPage=budgetbrowser" onClick="return f_check_perm(<%=pageRight%>,'N')"> <%=MC.M_Create_ANewBgt%><%--Create a new Budget*****--%></A>
				  
				  				  
			  </p>
			    </td>
		    </tr>
		</table>

			<jsp:include page="result.jsp?jsFile=searchJS" flush="true"></jsp:include> 
	</form>
				<%	
		} //end of if body for page right
		else
		{
		%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		} //end of else body for page right
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
	</DIV>
<div class ="mainMenu" id="emenu"> 
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>
