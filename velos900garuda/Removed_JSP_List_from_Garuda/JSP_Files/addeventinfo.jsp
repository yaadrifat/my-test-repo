<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Event Details Page (Contd.) </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="javascript">

function confirmBox(fileName) {

	if (confirm("Delete " + fileName + " from Events?")) {

    return true;}

	else

	{

	return false;

	}

    }

</SCRIPT> 

<SCRIPT language="JavaScript1.1">

	function openWin1() {

      windowName=window.open("","Information","toolbar=no,location=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=300")
	windowName.focus();

;}

</SCRIPT>



</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventInfoDao"%>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="codelstB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>

<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   


<body>
<DIV class="formDefault" id="div1"> 

<%
	String duration = request.getParameter("duration");   
	String protocolId = request.getParameter("protocolId");   
	String calledFrom = request.getParameter("calledFrom");   
	String eventId = request.getParameter("eventId");
	String mode = request.getParameter("mode");
	String fromPage = request.getParameter("fromPage");
	String calStatus = request.getParameter("calStatus");

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
	   int count = 0;
	   String uName = (String) tSession.getValue("userName");
	   EventInfoDao eventinfoDao = eventdefB.getEventDefInfo(EJBUtil.stringToNum(eventId));
	   ArrayList costIds = eventinfoDao.getCostIds() ;
	   ArrayList costDescs = eventinfoDao.getCostDescriptions() ;
	   ArrayList costValues = eventinfoDao.getCostValues();
	   ArrayList eventUserIds = eventinfoDao.getEventUserIds();
	   ArrayList userTypes = eventinfoDao.getUserTypes();
	   ArrayList userIds = eventinfoDao.getUserIds();


	   ArrayList docIds= eventinfoDao.getDocIds();
	   ArrayList docNames= eventinfoDao.getDocNames();
	   ArrayList docTypes= eventinfoDao.getDocTypes();
	   ArrayList docDescs= eventinfoDao.getDocDescs();


	   CodeDao codeDao = codelstB.getDescForEventUsers(userIds, userTypes);
	   ArrayList eventUserDesc = codeDao.getCDesc();

	   String costId = "";
	   String costDesc = "";
	   String costValue = "";
	   String eventUserId = "";
	   String userType = "";
	   String userId ="";	
	   String docId="";
	   String docName="";
   	   String docType="";
   	   String docDesc="";	
	   String evName = "";	
%>


<P class = "userName"><%=uName%></p>

<%
	String calName = "";
	
	if (fromPage.equals("eventbrowser"))
	{
		
			
%>
	<P class="sectionHeadings"> Protocol Calendar >> Event Details (Contd.) </P>

<%
	}
	else
	{
		calName = (String) tSession.getValue("protocolname");
	
%>
	<P class="sectionHeadings"> Protocol Calendar [ <%=calName%> ] >> Event Details (Contd.)  </P>
	

<%
	}

%>

<%
evName =  (String) tSession.getValue("eventname");


%>

<P class="sectionHeadings"> Event Name : <%=evName%>	</P>

<%

if(fromPage.equals("eventbrowser")) {
%>

<form METHOD=POST action="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>">

<%
} else {
%>
<form METHOD=POST action="fetchProt.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=4&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>">

<%
}
%>
<TABLE width="90%">

   <tr>
   	<td> <A href="eventusersearch.jsp?userType=S&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">Select User(S)</A> </td>
   </tr>
   <tr>
	<td> <P class = "defComments">Users selected to receive this message are<br><br> </P></td>
<td>

</td>

   </tr>
</TABLE>
<TABLE width="90%">
<% 
	count = 0;
	for(int i=0;i<userIds.size(); i++) {
	   if(userTypes.get(i).equals("S")) {


		if ((count%2)==0) {

  %>
      <tr id="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr id="browserOddRow"> 
        <%

		}
		count++;

  %>

	<td>

<%=eventUserDesc.get(i)%>	

	</td>
<td width=15%> <A HREF=eventuserdelete.jsp?eventuserId=<%= eventUserIds.get(i) %>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%> onClick="return confirmBox('<%=eventUserDesc.get(i)%>')">Delete<A></td>


   </tr>

<%
	   }
	}
%>
</table>


<TABLE width="90%">
   <tr>
	<td width=70%> Forms linked to this event </td>
	<td colspan=3>
<% 	
	if(docIds.size()>0) {
%>
	   <INPUT NAME="formLinked" TYPE= radio>No 
	   <INPUT NAME="formLinked" TYPE= radio CHECKED>Yes
<% 	}
	else
	{
%>
	   <INPUT NAME="formLinked" TYPE= radio CHECKED>No 
	   <INPUT NAME="formLinked" TYPE= radio>Yes
<%	}
%>
	</td>
   </tr>
   <tr>
	<td width=70% colspan=2> <P class = "defComments">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp If yes link form </P></td>
	<td width=15% align=left> <A href="addeventurl.jsp?docmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">Link URL</A> </td>
	<td width=15% align=left> <A href="addeventfile.jsp?docmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">Upload Document</A> </td>
   </tr>
</TABLE>


<TABLE width="90%">
   <tr>
	<td colspan=4> <P class = "defComments">Currently linked urls are <br><br> </P></td>
   </tr>
<% 
   for(int i=0;i<docIds.size(); i++) {

	docId= (String)docIds.get(i);
	docDesc= (String)docDescs.get(i); 
	docName = (String)docNames.get(i); 
	docType = (String)docTypes.get(i); 
	if(docType.equals("F"))
	continue;


		if ((i%2)==0) {

  %>
      <tr id="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr id="browserOddRow"> 
        <%

		}

  %>
	<td><A href=<%=docName%> target="_new"><%=docName%></A></td>
	<td> <%=docDesc%> </td>
	<td width=15%> 
<A HREF="addeventurl.jsp?docmode=M&docId=<%=docId%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">Edit<A> </td>
	<td width=15%> <A HREF=urlsave.jsp?docType=U&docId=<%= docId%>&docmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%> onClick="return confirmBox('<%=docName%>')">Delete<A></td>
   </tr>
<%
   }
%>
</TABLE>


<TABLE width="90%">
   <tr>
	<td colspan=4> <P class = "defComments">Currently linked files are <br><br> </P></td>
   </tr>
<% 
   for(int i=0;i<docIds.size(); i++) {
	docId=   (String)docIds.get(i);
	docDesc= (String)docDescs.get(i); 
	docName = (String)docNames.get(i); 
	docType = (String)docTypes.get(i); 
	if(docType.equals("U"))
	continue;

		if ((i%2)==0) {

  %>
      <tr id="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr id="browserOddRow"> 
        <%

		}

  %>
	<% docName=docName.replace('~',' ');%>
	<td> <A HREF="filesave.jsp?docId=<%=docId%>" target="Information" onClick="openWin1()" ><%=docName %><A> </td>
	<td> <%=docDesc%> </td>
	<td width=15%> </td>
	<td width=15%> <A HREF=urlsave.jsp?docType=F&docId=<%=docId%>&docmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%> onClick="return confirmBox('<%=docName%>')">Delete<A></td>
   </tr>
<%
   }
%>
</TABLE>



<TABLE width="90%">
   <tr>
	<td width=70% colspan=2> Cost associated with this event </td>
	<td colspan=2> <A href="addeventcost.jsp?costmode=N&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&eventName=<%=evName%>"> Specify Cost</A> </td>
   </tr>
   <tr>
	<td> <P class = "defComments">Associated Costs are <p> </td>
   </tr>
</TABLE>
<TABLE width="90%">
<% 
   for(int i=0;i<costIds.size(); i++) {
	costValue= (String)costValues.get(i);
	costDesc= (String)costDescs.get(i); 
	costId = (String)costIds.get(i); 

		if ((i%2)==0) {

  %>
      <tr id="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr id="browserOddRow"> 
        <%

		}

  %>
	<td> <%=costDesc %></td>
	<td> $<%=costValue%> </td>
	<td width=15%> <A HREF="addeventcost.jsp?costmode=M&costId=<%=costId%>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">Edit<A> </td>
	<td width=15%> <A HREF=costsave.jsp?costId=<%=costId%>&costmode=D&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%> onClick="return confirmBox('<%=costDesc%>')">Delete<A></td>
   </tr>
<%
   }
%>
</TABLE>

<BR></BR>
<TABLE width="90%">
   <tr>
 	<td width=70% colspan=2> Resources associated with this event </td>
	<td width=15%> <A href="addeventrole.jsp?eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>"> Select Role type</A> </td>
	<td width=15%> <A href="eventusersearch.jsp?userType=U&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>">Select user</A> 	</td>
   </tr>
   <tr>
	<td> <P class = "defComments">Associated resources are<br><br> </P></td>
   </tr>
</TABLE>

<TABLE width="90%">

<% 
	count = 0;
	int flag = 0;
	for(int i=0;i<eventUserDesc.size(); i++) {
	   if(!(userTypes.get(i).equals("S"))) {
	      if(userTypes.get(i).equals("R")) {
		if(flag == 0){
		   flag=1;
		   count = 0;
%>
   <tr>
	<td> <P class = "defComments">Role Types </P></td>
   </tr>		
<%		
	   	} // end of if for flag
	      } else if(userTypes.get(i).equals("U")) { //else of if(userTypes.get(i).equals("R"))
		if(flag == 1){
		   flag=0;
		   count = 0;
%>
   <tr>
	<td> <P class = "defComments">Users </P></td>
   </tr>		
<%		
	   	} // end of if for flag
	      } //end of if(userTypes.get(i).equals("U"))
		if ((count%2)==0) {

%>
      <tr id="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr id="browserOddRow"> 
        <%

		}
		count++;

  %>
	<td>
<%=eventUserDesc.get(i)%>	
	</td>
<td>
<td width=15%> <A HREF=eventuserdelete.jsp?eventuserId=<%= eventUserIds.get(i) %>&eventId=<%=eventId%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%> onClick="return confirmBox('<%=eventUserDesc.get(i)%>')">Delete<A></td>

</td>
     </tr>
<%
	   }
	}
%>
</table>
<BR>
<table width=90%>
   <tr>
	<td align=right>
	 	<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
	 </td>
	 <td align=right>
	 	<button type="submit"><%=LC.L_Submit%></button>
	 </td>
   </tr>

</TABLE>

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>


