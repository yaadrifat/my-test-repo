<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<title><%=MC.M_Pat_Frm%><%--<%=LC.Pat_Patient%> Form*****--%></title>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.user.UserJB" %>

<% String src;

src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");

String page1 = request.getParameter("page");
int patId=EJBUtil.stringToNum(request.getParameter("pkey"));
String mode = request.getParameter("mode");
String patientCode = request.getParameter("patientCode");

%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>


<body>

<br>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<%@ page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>
<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"
%>
<DIV  class="browserDefault" id="div1">
<P class="sectionHeadings"> <%=MC.M_MngPat_PatProfile%><%--Manage <%=LC.Pat_Patients%> >> <%=LC.Pat_Patient%> Profile*****--%></P>
<%HttpSession tSession = request.getSession(true);
String enrollId = (String) tSession.getValue("enrollId");
String patientId="";
int personPK = 0;
		 personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
		 String pkey = request.getParameter("pkey");
	     person.setPersonPKId(personPK);
		 person.getPersonDetails();
		 patientId = person.getPersonPId();

%>
<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="pkey" value="<%=patId%>"/>
<jsp:param name="patientCode" value="<%=patientId%>"/>
<jsp:param name="patProtId" value="<%=enrollId%>"/>
<jsp:param name="page" value="patient"/>
</jsp:include>

<%


  if (sessionmaint.isValidSession(tSession))
	{


		///for page rightS
		 int pageRight = 0;
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PATFRMSACC"));
		String modRight = (String) tSession.getValue("modRight");
		int patProfileSeq = 0, formLibSeq = 0;



		 ///////////
		// To check for the account level rights
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProfileAppRight = '0';
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);





	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
	{
		 ArrayList arrFrmNames = null;
		 ArrayList arrDesc = null;
		 ArrayList arrLastEntryDate = null;
		 ArrayList arrNumEntries = null;
  		 ArrayList arrFrmIds = null;
 		 //by salil
		 ArrayList arrEntryChar=null;
		 int frmId=0;
		 String frmName = "";
		 String desc = "";
		 String lastEntryDate = "";
		 String numEntries = "";
		  String entryChar="";
		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();

		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getPatientForms(iaccId,patId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
		  arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrDesc = lnkFrmDao.getFormDescription();
		 arrLastEntryDate = lnkFrmDao.getFormLastEntryDate();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
		 int len = arrFrmNames.size();

 %>
 <Form name="formlib" action="formLibraryBrowser.jsp"  method="post">

 <table width="100%" cellspacing="1" cellpadding="0" >
					<tr>
				        <th width="20%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
				        <th width="25%"><%=LC.L_Description%><%--Description*****--%> </th>
						<th width="15%"><%=LC.L_Last_EntryOn%><%--Last Entry On*****--%> </th>
						<th width="20%"><%=LC.L_Number_OfEntries%><%--Number of Entries*****--%></th>
					</tr>
	 <%
	 int counter=0;
    int i=0;
	for(counter=0;counter<len;counter++)
		{
		 frmId = ((Integer)arrFrmIds.get(counter)).intValue();
		 frmName=((arrFrmNames.get(counter)) == null)?"-":(arrFrmNames.get(counter)).toString();
		 entryChar=(String)arrEntryChar.get(counter);
		 desc=((arrDesc.get(counter)) == null)?"-":(arrDesc.get(counter)).toString();
		 lastEntryDate=((arrLastEntryDate.get(counter))== null)?"-":(arrLastEntryDate.get(counter)).toString();
		 numEntries=((arrNumEntries.get(counter))== null)?"-":(arrNumEntries.get(counter)).toString();
		 int lk = Integer.parseInt(numEntries);

			if ((i%2)==0)
				{i++;%>
				<tr class="browserEvenRow">
				<%}else
				 {i++;%>
				 <tr class="browserOddRow">
					<%}%>
			   	<% if( lk==0){%>
					<td width="10%"><A href="patformdetails.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&formId=<%=frmId%>&mode=N&formDispLocation=PA&pkey=<%=patId%>&entryChar=<%=entryChar%>" ><%=frmName%></A></td>
				<%}
				 else
				 {%>
					<td width="10%"><A href="formfilledpatbrowser.jsp?formId=<%=frmId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patId%>&entryChar=<%=entryChar%>"><%=frmName%></A></td>
				<%}%>

				<!--<td width="10%"><A href="formfilledpatbrowser.jsp?formId=<%=frmId%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&pkey=<%=patId%>"><%=frmName%></A></td>-->
				<td width="8%"><%=desc%></td>
				 <%if(lastEntryDate!="-"){%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}else{%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}%>
				<td width="13%"><%=numEntries%></td>

		</tr>

		<%}%>


	</table>
	 <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--></div>

</body>

</html>



















