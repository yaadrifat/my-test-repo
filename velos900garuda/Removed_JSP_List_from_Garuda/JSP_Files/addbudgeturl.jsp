<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title> <%=LC.L_Bgt_BgtUrl %><%-- Budget >> Budget URL*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*, com.velos.esch.service.util.EJBUtil,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="bgtApndxB" scope="request" class="com.velos.esch.web.bgtApndx.BgtApndxJB"/>

<SCRIPT Language="javascript">
 function  validate(formobj){
    
     if (!(validate_col('URL',formobj.budgetApndxUri))) return false
     if (!(validate_col('Description',formobj.budgetApndxDesc))) return false
     if (!(validate_col('eSign',formobj.eSign))) return false

	
	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>


<% String src;
src= request.getParameter("srcmenu");
String fromRt = request.getParameter("fromRt");
if (fromRt==null || fromRt.equals("null")) fromRt="";

String budgetTemplate = request.getParameter("budgetTemplate");
String budgetMode = request.getParameter("budgetMode");
String 	bottomdivClass="tabDefBotN";
if ("S".equals(request.getParameter("budgetTemplate"))) { %>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<% } else { 
bottomdivClass="popDefault";
%>
<jsp:include page="include.jsp" flush="true"/> 
<% }  %>


<body>

<DIV class="tabDefTopN" id="div1">

<%
	String mode = request.getParameter("mode");
	String budgetApndxId=request.getParameter("budgetApndxId");	   
	String budgetId = request.getParameter("budgetId");
	String selectedTab = request.getParameter("selectedTab");
	
	String budgetApndxDesc= "";
	String budgetApndxUri = "";

	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))
	{
%>


<%
	if (mode.equals("M")) {
		bgtApndxB.setBgtApndxId(EJBUtil.stringToNum(budgetApndxId));
		bgtApndxB.getBgtApndxDetails();
		budgetApndxDesc = bgtApndxB.getBgtApndxDesc();
		budgetApndxUri = bgtApndxB.getBgtApndxUri();
	}
%>
<form name="addurl" id="addbdgturl" METHOD=POST action="budgeturlsave.jsp" onsubmit="if (validate(document.addurl)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="budgetApndxId" value=<%=budgetApndxId%>>
<input type=hidden name="budgetId" value=<%=budgetId%>>
<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>
<input type=hidden name="budgetTemplate" value=<%=budgetTemplate%>>
<input type=hidden name="budgetMode" value=<%=budgetMode%>>
<Input type="hidden" name="fromRt" value="<%=fromRt%>" />

</DIV>
	<DIV class="<%=bottomdivClass%>" id="div2">
<TABLE width="98%">
      <tr> 
        <td colspan="2" align="center"> 
          <P class = "defComments"> <%=MC.M_AddLnk_ToBgtAppx%><%-- Add Links to your Budget Appendix*****--%> </P>
        </td>
      </tr>
    <tr><td>&nbsp;</td></tr>
      <tr> 
        <td width="20%" align="right"> <%=LC.L_Url_Upper%><%-- URL*****--%> <FONT class="Mandatory">* </FONT> &nbsp; &nbsp;</td>
        <td width="65%"> 
          <input type=text name="budgetApndxUri" value='<%=budgetApndxUri%>' MAXLENGTH=255 size=50>
        </td>
      </tr>
    
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_EtrCpltUrl_255Max%><%-- Enter complete url eg. 'http://www.centerwatch.com' 
             or 'ftp://ftp.centerwatch.com' ( 255 char max.)*****--%> </P>
        </td>
      </tr>
      	  <tr><td>&nbsp;</td></tr>
      <tr> 
        <td align="right"> <%=LC.L_Short_Desc%><%-- Short Description*****--%> <FONT class="Mandatory" >* </FONT> &nbsp; &nbsp;
        </td>
        <td > 
          <input type=text name="budgetApndxDesc" MAXLENGTH=100 value='<%=budgetApndxDesc%>' size=40>
        </td>
      </tr>
      <tr> 
        <td > </td>
        <td > 
          <P class="defComments"> <%=MC.M_NameToLnk_100CharMax%><%-- Give a friendly name to your link. (100 char 
            max.)*****--%> </P>
        </td>
      </tr>
      		<tr><td>&nbsp;</td></tr>
      		<tr><td>&nbsp;</td></tr>
  </table>
	
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="addbdgturl"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>

</html>



