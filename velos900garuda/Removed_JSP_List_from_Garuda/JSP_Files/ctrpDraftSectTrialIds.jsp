<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>

<table>
	<tr>
		<td width="50%">
			<table>
				<tr>
					<td align="right" width="30%" >
						<%=LC.CTRP_DraftUniqTrialId%>
					</td>
					<td width="1%">&nbsp;&nbsp;</td>
					<td align="left" colspan="3">
						<s:fielderror id="ctrpDraftJB.studyNumber_error" >
						<s:param>ctrpDraftJB.studyNumber</s:param>
						</s:fielderror>
						<s:textfield name="ctrpDraftJB.studyNumber" 
								style="height:2em; width:100%;"
								cssClass="readonly-input" 
								readonly="true" maxsize="100" />
					</td>
				</tr>
				<tr>
					<td align="right" >
						<%=LC.CTRP_DraftNCITrialId%>
						<span id="ctrpDraftJB.nciTrialId_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
						<FONT id="nci_id_asterisk" style="visibility:hidden" class="Mandatory">* </FONT>
					</td>
					<td align="left" valign="top" colspan="3">
						<s:textfield name="ctrpDraftJB.nciTrialId" id="ctrpDraftJB.nciTrialId" style="height:2em; width:100%;" />
					</td>
				</tr>
				<tr>
					<td align="right" >
						<%=LC.CTRP_DraftLeadOrgTrialId%>
						<span id="ctrpDraftJB.leadOrgTrialId_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
						<FONT id="lead_org_id_asterisk" style="visibility:hidden" class="Mandatory">* </FONT>
					</td>
					<td align="left" valign="top" colspan="3">
						<s:textfield name="ctrpDraftJB.leadOrgTrialId" id="ctrpDraftJB.leadOrgTrialId" style="height:2em; width:100%;" />
					</td>
				</tr>
			</table>
		</td>
		<td width="50%">
			<table>
				<tr>
					<td align="right" width="30%" >
						<%=LC.CTRP_DraftNCTNumber%>
					</td>
					<td width="1%">&nbsp;&nbsp;</td>
					<td align="left">
						<s:fielderror id="ctrpDraftJB.nctNumber_error" >
						<s:param>ctrpDraftJB.nctNumber</s:param>
						</s:fielderror>
						<s:textfield name="ctrpDraftJB.nctNumber" id="ctrpDraftJB.nctNumber" style="height:2em; width:25em;" maxsize="20" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="right" >
						<%=LC.CTRP_DraftOtherTrialId%>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:fielderror id="ctrpDraftJB.otherTrialId_error" >
						<s:param>ctrpDraftJB.otherTrialId</s:param>
						</s:fielderror>
						<s:textfield name="ctrpDraftJB.otherTrialId" id="ctrpDraftJB.otherTrialId" style="height:2em; width:25em;" maxsize="20" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<FONT class="Grey">
							<%=MC.CTRP_DraftOtherIdInfo%>
						</FONT>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
