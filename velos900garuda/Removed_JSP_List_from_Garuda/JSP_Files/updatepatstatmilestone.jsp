<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<SCRIPT  Language="JavaScript1.2">
function fclose_to_role()
{
	window.opener.location.reload();
	self.close();
}
</SCRIPT>


</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*"%>

<%

String src = request.getParameter("srcmenu");
String eSign = request.getParameter("eSign");	
String addFlag = request.getParameter("addFlag");	
String mode = "";
String studyId = request.getParameter("studyId");	
String milestoneType= request.getParameter("milestoneType");	


//String studyId = "";
//String totRows = request.getParameter("totrows");
String totRows = "1";
String counts[] = null;

String amounts[] = null;
int cnt=0;
int ret =0;

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%	
//	String oldESign = (String) tSession.getValue("eSign");


//	if(!oldESign.equals(eSign)) {
%>
 
<%
//	} else {
String ipAdd = (String) tSession.getValue("ipAdd");
String usr = (String) tSession.getValue("userId");

String count="";
String mileRule ="";
String amount = "";

String limit = "";
String patStatus = "";
String paymentType = "";
String paymentDueIn = "";
String paymentDueUnit = "";
String paymentFor = "";
String milestoneStat = "";//KM
String description ="";

String limits[] = null;
String patStatuses[] = null;
String paymentTypes[] = null;
String paymentDueIns[] = null;
String paymentDueUnits[] = null;
String paymentFors[] = null;
String milestoneStats[] = null;
String descriptions[] = null;
          

counts = request.getParameterValues("count");
totRows = (new Integer(counts.length)).toString();


mileRule = "0";

if (totRows.equals("1")){
   count = request.getParameter("count");
   amount = request.getParameter("amount");
   
  
   if(!milestoneType.equals("AM")) {
     limit = request.getParameter("mLimit"); 
     patStatus = request.getParameter("dStatus");
   }
   paymentType = request.getParameter("dpayType");
 //  paymentDueIn = request.getParameter("paymentDueIn");
  // paymentDueUnit = request.getParameter("paymentDueUnit");
   paymentFor = request.getParameter("dpayFor");
   description = request.getParameter("description");//KM
   milestoneStat = request.getParameter("milestoneStat");
   
   
   if(amount.equals(".")){
	amount="0";
	}
   
     if (StringUtil.isEmpty(count))    
     {
     	count = "-1";
     }
   
   if ((count != null) && (count.trim()!= "") && (! StringUtil.isEmpty(patStatus)) )
	{
	milestoneB.setMilestoneStudyId(studyId); 
	milestoneB.setMilestoneAmount(amount);
	milestoneB.setMilestoneCount(count);
	milestoneB.setMilestoneDelFlag("N");
	milestoneB.setMilestoneRuleId(mileRule);
	milestoneB.setMilestoneVisit("1");	
	milestoneB.setMilestoneType(milestoneType);	
	milestoneB.setCreator(usr); 
	milestoneB.setIpAdd(ipAdd);
	
	milestoneB.setMilestoneStatFK(milestoneStat);
    
//    milestoneB.setMilestonePayByUnit( paymentDueUnit ) ;
//    milestoneB.setMilestonePaydDueBy( paymentDueIn) ;
    milestoneB.setMilestonePayFor( paymentFor) ;
    milestoneB.setMilestonePayType( paymentType) ;
	if(!milestoneType.equals("AM")) {
	milestoneB.setMilestoneLimit( limit) ;	
	milestoneB.setMilestoneStatus( patStatus );
	}

	milestoneB.setMilestoneDescription(description);//KM

	ret = milestoneB.setMilestoneDetails();
	

	   }
}else {
	counts = request.getParameterValues("count");

	amounts = 	request.getParameterValues("amount");
    
	
    if(!milestoneType.equals("AM")) {
      limits = request.getParameterValues("mLimit");
      patStatuses = request.getParameterValues("dStatus");
    }
   paymentTypes = request.getParameterValues("dpayType");
  // paymentDueIns = request.getParameterValues("paymentDueIn");
   //paymentDueUnits = request.getParameterValues("paymentDueUnit");
   paymentFors = request.getParameterValues("dpayFor");
   milestoneStats = request.getParameterValues("milestoneStat");

	if(milestoneType.equals("AM"))
	  descriptions = request.getParameterValues("description");
	
	for (cnt =0;cnt< EJBUtil.stringToNum(totRows); cnt++ ){

		count = counts[cnt];

		amount = amounts[cnt];
		
		if(!milestoneType.equals("AM")) {
	   limit = limits[cnt];
	   patStatus = patStatuses[cnt];
	   
		}
	   paymentType = paymentTypes[cnt];
	 //  paymentDueIn = paymentDueIns[cnt];
	  // paymentDueUnit = paymentDueUnits[cnt];
	   paymentFor = paymentFors[cnt];
		milestoneStat = milestoneStats[cnt];
		if(milestoneType.equals("AM"))
		   description = descriptions[cnt].trim(); //KM-3301
		
	    if (StringUtil.isEmpty(count))    
    	 {
     		count = "-1";
     		}
		
		
		 
		if (((count != null) && (!count.trim().equals(""))) && ( ! StringUtil.isEmpty(patStatus) || !StringUtil.isEmpty(description)))
		{
		
		if(amount.equals(".")){
		amount="0";
		}
		milestoneB.setMilestoneStudyId(studyId); 
		milestoneB.setMilestoneAmount(amount);
		milestoneB.setMilestoneCount(count);
		milestoneB.setMilestoneDelFlag("N");
		milestoneB.setMilestoneRuleId(mileRule);
		milestoneB.setMilestoneVisit("1");		
		milestoneB.setMilestoneType(milestoneType);	
		milestoneB.setCreator(usr); 
		milestoneB.setIpAdd(ipAdd);
		milestoneB.setMilestoneStatFK(milestoneStat);
        
	    //milestoneB.setMilestonePayByUnit( paymentDueUnit ) ;
	   // milestoneB.setMilestonePaydDueBy( paymentDueIn) ;
	    milestoneB.setMilestonePayFor( paymentFor) ;
	    milestoneB.setMilestonePayType( paymentType) ;
		if(!milestoneType.equals("AM")) {
		milestoneB.setMilestoneLimit( limit) ;
		milestoneB.setMilestoneStatus( patStatus);
		}
		milestoneB.setMilestoneDescription(description);//KM
   		ret =milestoneB.setMilestoneDetails();
		}
		
	}
} 
%>
<br><br><br><br><br>

<p class = "successfulmsg" align = center><%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>



<%
if(addFlag.equals("Add"))
{
%>

<SCRIPT>
window.opener.location.reload();
</SCRIPT>

<META HTTP-EQUIV=Refresh CONTENT="0; URL=patstatmilestone.jsp?studyId=<%=studyId%>&milestoneType=<%=milestoneType%>">

<%
}
else
{
mode="N";
%>
<%--
<META HTTP-EQUIV=Refresh CONTENT="0; URL=alertnotify.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=<%=mode%>&pkey=<%=pkey%>&patProtId=<%=enrollId%>&page=patientEnroll&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>">
--%>

<SCRIPT>
			fclose_to_role();
</SCRIPT>

<%
}
%>





<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


