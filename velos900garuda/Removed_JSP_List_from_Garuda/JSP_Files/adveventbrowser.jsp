<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>

<title><%=MC.M_MngPat_AdvEvtBrws%><%--Manage Patient >> Adverse Event Browser*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language=javascript>

//JM: 03DcDec2007: Dec_2007_enh#15 
function openwindow(formobj, study, patId, pgRight, orgRight){

	if (f_check_perm_org(pgRight,orgRight,'N'))  
	{
	  windowName =window.open('addmultipleadvevent.jsp?srcmenu=tdMenuBarItem5&page=patientEnroll&selectedTab=6&studyId='+study+'&pkey=' + patId, 'Information', 'toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1100,height=600, top=100, left=90');
	  windowName.focus();
	 } 

}

//JM: for sorting on the column header
function setOrder(formobj,orderBy) 
{
       
	if (formobj.orderType.value=="asc") {
		formobj.orderType.value= "desc";		
	} else 	if (formobj.orderType.value=="desc") {
		formobj.orderType.value= "asc";
	}
	formobj.orderBy.value= orderBy;
	
	formobj.submit(); 
}

 function openWinStatus(patId,patStatPk,pageRight,study,orgRight)  
{

	changeStatusMode = "yes";
	
	if (f_check_perm_org(pageRight,orgRight,'N'))  
	{
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
}
function functiondeladveve(advId,studyId,pageRight,orgRight){
  if (f_check_perm_org(pageRight,orgRight,'E')) 
	{
	
		if (confirm("<%=MC.M_WantToDel_AdvEvtForPat%>"))/*if (confirm("Do you want to delete this adverse event for the <%=LC.Pat_Patient%>?"))*****/  
		{
			windowName= window.open("patadverseevedelete.jsp?advId="+advId+"&studyId="+studyId,"adversedelete","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=400");
			windowName.focus();
		} else 
		{
			return false
		}
	} else
	{
	      return false;
	}
 
	
}

function f_delete(patStudyStatPk,pageRight,orgRight) {
	if (f_check_perm_org(pageRight,orgRight,'E')) 
	{
	
		if (confirm("<%=MC.M_DoYouWant_DelPatStdStat%>"))/*if (confirm("Do you want to delete this <%=LC.Std_Study_Lower%> status for the <%=LC.Pat_Patient%>?"))*****/ 
		{
			windowName= window.open("patstudystatusdelete.jsp?patStudyStatPk="+patStudyStatPk,"patstatdel","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=350");
			windowName.focus();
		} else 
		{
			return false
		}
	} else
	{
		return false;
	}//end of edit right
} //end of function

	
</Script>
</head>

<% String src="";
src= request.getParameter("srcmenu");
%>	
<jsp:include page="panel.jsp" flush="true">    
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="crfStatB" scope="request" class="com.velos.esch.web.crfStat.CrfStatJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
<jsp:useBean id="adveventB" scope="request" class="com.velos.esch.web.advEve.AdvEveJB"/><!-- gopu-->
	
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*, com.velos.eres.service.util.*"%>


<body>

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
   	{ 
		int pageRight=0;
		int orgRight=0;
		int personPK = 0;	
		int siteId = 0;
		String siteName = "";
		String protocolId="";
		String userIdFromSession = (String) tSession.getValue("userId");
		String study = request.getParameter("studyId");
   		personPK = EJBUtil.stringToNum(request.getParameter("pkey"));		
   		
		if(study == null || study.equals("null"))
		{
		    study = (String) tSession.getValue("studyId");
		 } 
		 else 
		 {
		 	tSession.setAttribute("studyId",study);		
		 }	

	//GET STUDY TEAM RIGHTS 
    TeamDao teamDao = new TeamDao();
    teamDao.getTeamRights(EJBUtil.stringToNum(study),EJBUtil.stringToNum(userIdFromSession));
    ArrayList tId = teamDao.getTeamIds();
    if (tId.size() == 0) 
	{ 
    	pageRight=0 ;
    }
	else 
	{
    	stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
    	 
	 	ArrayList teamRights ;
				teamRights  = new ArrayList();
				teamRights = teamDao.getTeamRights();
					 
				stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
				stdRights.loadStudyRights();
				
    	if ((stdRights.getFtrRights().size()) == 0)
		{
    	 	pageRight= 0;
    	}
		else
		{
    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
    	}
    }
    
    orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), personPK , EJBUtil.stringToNum(study) );
    
	if (orgRight > 0)
	{
		System.out.println(LC.Pat_Patient_Lower+" adverse br orgRight" + orgRight);
		orgRight = 7;
	}

		if (pageRight >=4 && orgRight >= 4){
			
    		personB.setPersonPKId(personPK);
     		personB.getPersonDetails();
    		String patientId = personB.getPersonPId();
    		siteId = EJBUtil.stringToNum(personB.getPersonLocation());



    		
    		
    			
    		String uName = (String) tSession.getValue("userName");
    				
    		String visit = request.getParameter("visit");
    		String patProtId= request.getParameter("patProtId");

    		String studyId = (String) tSession.getValue("studyId");	
    		String statDesc=request.getParameter("statDesc");
    		String statid="";
    		String studyVer = request.getParameter("studyVer");
    		String studyNum = request.getParameter("studyNum");
    		int patProtPK = 0;
    		
						
    		//KM-#3827
			if ( EJBUtil.isEmpty(patProtId) || (patProtId.equals("0"))) // if there is no patProtId passed/is empty
    		{
    			//check if there is any current enrollment
    			patEnrollB.findCurrentPatProtDetails(EJBUtil.stringToNum(studyId),personPK );
    			patProtPK =  patEnrollB.getPatProtId(); //get patProtId from current enrollment
    			
    			if (patProtPK  > 0)
    			{
    				patProtId = String.valueOf(patProtPK);
    			}
		  		
    		}
    		else
    		{	
				//SV, 8/13/04, as a fix for re-opened 1599, decided to use patProtId itself, as enrollid is always equal to patProtId and is not used for any other purpose.
    			patEnrollB.setPatProtId(EJBUtil.stringToNum(patProtId)); //SV, 8/13/04, changed to use patProtId
    			patEnrollB.getPatProtDetails();
    		}
    		
    			
    			
    			
    		protocolId =patEnrollB.getPatProtProtocolId();
    		
    		String protName = "";
    		if(protocolId != null) {
    		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
    		eventAssocB.getEventAssocDetails();
    		protName = 	eventAssocB.getName();
    		}
    	
    		String studyTitle = "";
    		String studyNumber = "";
    		studyB.setId(EJBUtil.stringToNum(studyId));
    
    		studyB.getStudyDetails();
    		studyTitle = studyB.getStudyTitle();
    		studyNumber = studyB.getStudyNumber();
    		

    		
    		
    		
    		
  //JM: 8Jan2006: added, FOR HANDLING COLUMN HEADER CLICK SORTING AND PAGINATION 
    		
		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		pagenum = request.getParameter("currentpage"); 		

		
		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum); 
		
		String orderBy = "";
		orderBy = request.getParameter("orderBy");		
		
		String orderType = "";
		orderType = request.getParameter("orderType");
		

		
		if (EJBUtil.isEmpty(orderBy))
		orderBy = "ae_stdate";

		

		if (EJBUtil.isEmpty(orderType))
		{
		orderType = "desc";
		}
			
    		
    %>
    
    	
<DIV class="BrowserTopn" id="div1"> 

    <jsp:include page="patienttabs.jsp" flush="true"> 
    	<jsp:param name="studyId" value="<%=studyId%>"/>
    	<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientId)%>"/>
		<jsp:param name="patProtId" value="<%=patProtId%>"/>
    </jsp:include>
    </div>
<DIV class="tabFormBotN tabFormBotN_PSc_1" id="div2"> 
    <%
	

	if (!(patProtId.equals("")) && (!patProtId.equals("0"))) 
    	{
    		//get current patient status and its subtype
    			
    		String patStatSubType = "";
			String patStudyStat =  "";
			int patStudyStatpk = 0;
			
			patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(study, String.valueOf(personPK));
			patStudyStat = patB.getPatStudyStat();
				patStudyStatpk = patB.getId();
			statid = patStudyStat ;
			if (EJBUtil.isEmpty(patStatSubType))
				patStatSubType= "";
							
			///////////////////////////////////	
    			
    	
    	%>
    	
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
    <%
    	studyTitle = StringUtil.escapeSpecialCharJS(studyTitle);
    %>
    
    <script language=javascript>
		var varViewTitle = htmlEncode('<%=studyTitle%>');
	</script>
	
	<td><font size="1">
	<%=LC.L_Study_Number%><%--Study Number*****--%>: <a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=study%>"><%=studyNumber%></a> &nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" onmouseover="return overlib(varViewTitle,CAPTION,'<%=LC.L_Study_Title%><%--Study Title*****--%>');" onmouseout="return nd();"><img border="0" src="./images/View.gif"/></a>  &nbsp;&nbsp;&nbsp;&nbsp;
    	<%if(protocolId != null){%>
	<!--Protocol Calendar: <%=protName%>-->
    	<%}%>
     </tr>
     </table>



  	 	<% 
		//Modified by Manimaran for November Enhancement PS4.
		if((patStatSubType.trim().toLowerCase().equals("lockdown")))
			{ %>
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_PatStdLkdown_CntAddEdt%><%--Patient's study status is 'Lockdown'. You cannot Add or Edit Adverse Events*****--%> </FONT> 
				 <A onclick="openWinStatus('<%=personPK%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=study%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A> <FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>
					
				</P>

		<%	}
		 %>

    <table width=98%>
    <tr height="25">
    	<td width = 50%>
    		<P class = "defComments"> <%=MC.M_RptAdv_EvtAre%><%--Reported adverse events are as follows*****--%>: </P>
    	</td>
    	<td width = 10% align=right	>
    
    	<%
    	 //Modified by Manimaran for November Enhancement PS4.
	 if(!((patStatSubType.trim().toLowerCase()).equals("lockdown")))
    	 {
		  
    		%>
    		<A onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'N')" HREF="advevent_new.jsp?srcmenu=<%=src%>&selectedTab=6&mode=N&pkey=<%=personPK%>&visit=<%=visit%>&page=patientEnroll&mode=N&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&studyNum=<%=studyNum%>&patientCode=<%=patientId%>"><%=LC.L_AddNew_Ae%><%--ADD NEW AE*****--%></A>   		
			<%
	   	 }
		else
		 {
			
		  %>	    			
		   &nbsp;
		 <%}
	  	%>	
       	</td>
       	
       	<td width = 10% align=right	>
    
    	<%
    	 //Modified by Manimaran for November Enhancement PS4.
	 if(!((patStatSubType.trim().toLowerCase()).equals("lockdown")))
    	 {
		  
    		%>    		
       		<A href="#" onClick="openwindow(document.advevent, '<%=study%>', '<%=personPK%>', <%=pageRight%>,<%=orgRight%>);" ><%=LC.L_Add_Multi_Aes%><%--ADD MULTIPLE AEs*****--%></A>
       	<%
	   	 }
		else
		 {
			
		  %>	    			
		   &nbsp;
		 <%}
	  	%>	
       	</td>
    </tr>
    </table>
    
    
    <!--JM: introduced the form-->
    <Form name="advevent" method="post" action="adveventbrowser.jsp">    
    
    <%
      
      //eventInfoDao = eventdefB.getAdverseEvents(EJBUtil.stringToNum(studyId),personPK) ;
      
           
      String mysql = "  select a.pk_adveve as advId,"
                    + " b.CODELST_DESC as advType,"
                    + " a.FK_CODLST_AETYPE as advTypeId,"
                    + "	a.AE_DESC as advDesc,"
                    + " a.AE_GRADE as advGrade,"
                    + " a.AE_Name  as advName,"
					+ " a.AE_RELATIONSHIP ,"
					+ " (select distinct codelst_desc from sch_codelst where sch_codelst.codelst_type='adve_relation' and "
					+ " sch_codelst.PK_CODELST=a.AE_RELATIONSHIP) as attribution,"
                    + "	to_char(a.ae_stdate,'yyyy-mm-dd') advSdate,"
                    + "	to_char(a.ae_enddate,'yyyy-mm-dd') advEdate,"
                    + "	c.usr_firstname ||' '|| c.usr_lastname  as advUser, FORM_STATUS  "
                    + "	from sch_adverseve a,"
                    + "	sch_codelst b,"
                    + "	ERES.er_user c"
                    + "	where a.FK_CODLST_AETYPE=b.pk_codelst"
                    + " and a.AE_ENTERBY= c.pk_user and fk_study= " + studyId +  " and fk_per= " + personPK  ; //+ "  order by a.ae_stdate desc,a.pk_adveve desc";
      
      
      
      String countSql = " select count(*) from ( " + mysql + " ) ";
      
      long rowsPerPage=0;
	  long totalPages=0;	
	  long rowsReturned = 0;
	  long showPages = 0;
	  	
	  boolean hasMore = false;
	  boolean hasPrevious = false;
	  long firstRec = 0;
	  long lastRec = 0;	   
	  long totalRows = 0;	   	   
	   
	  rowsPerPage =  Configuration.MOREBROWSERROWS ;
	  totalPages =Configuration.PAGEPERBROWSER ; 
  


      BrowserRows br = new BrowserRows();
      br.getPageRows(curPage,rowsPerPage,mysql ,totalPages,countSql,orderBy,orderType);
   	  rowsReturned = br.getRowReturned();
	  showPages = br.getShowPages();
	  startPage = br.getStartPage();
	  hasMore = br.getHasMore();
	  hasPrevious = br.getHasPrevious();	 
	  totalRows = br.getTotalRows();	   
	  firstRec = br.getFirstRec();
	  lastRec = br.getLastRec();
		
	
    
    %>
  
   	<input type="hidden" name=srcmenu value="<%=src%>">
   	<Input type="hidden" name="pageRight" value="<%=pageRight%>"> 	
	<Input type="hidden" name="page" value="patientEnroll">
	<Input type="hidden" name="selectedTab" value="6">
	<Input type="hidden" name="pkey" value="<%=personPK%>">
	<Input type="hidden" name="studyId" value="<%=studyId%>">
	<Input type="hidden" name="visit" value="<%=visit%>">
	<Input type="hidden" name="patientId" value="<%=patientId%>">   	
	<input type="hidden" name="orderBy" value="<%=orderBy%>">
	<input type="hidden" name="orderType" value="<%=orderType%>">	
	<input type="hidden" name="orgRight" value="<%=orgRight%>">	
	<Input type="hidden" name="currentpage" value="<%=curPage%>">
	
	
	
	
    	
    
    <TABLE border=0 cellPadding=1 cellSpacing=1 width="100%">
      <TR>
        <TH onClick="setOrder(document.advevent,'advType')" ><%=LC.L_Type%><%--Type*****--%> &loz;</TH>
        <TH onClick="setOrder(document.advevent,'advGrade')" ><%=LC.L_Adverse_EventOrGrading%><%--Adverse Event/Grading*****--%> &loz;</TH>
		<!--Added by Gopu for Nov.2005 Enhancement #6 -->
        <TH onClick="setOrder(document.advevent,'attribution')" ><%=LC.L_Attribution%><%--Attribution*****--%> &loz;</TH>  
        <TH onClick="setOrder(document.advevent,'advSdate')" ><%=LC.L_Start_Date%><%--Start Date*****--%> &loz;</TH>
        <TH onClick="setOrder(document.advevent,'advEdate')" ><%=LC.L_Stop_Date%><%--Stop Date*****--%> &loz;</TH>
        <TH onClick="setOrder(document.advevent,'advUser')" ><%=LC.L_Entered_By%><%--Entered By*****--%> &loz;</TH>
	<TH onClick="setOrder(document.advevent,'FORM_STATUS')" ><%=LC.L_Form_Status%><%--Form Status*****--%> &loz;</TH>	
	<TH><%=LC.L_Delete%><%--Delete*****--%></TH>	
      </TR>	 
    <% 
    
    
      EventInfoDao eventInfoDao = new EventInfoDao(); 
    
      //populate form status
      SchCodeDao cdFormStatus = new SchCodeDao();
      cdFormStatus.getCodeValues("fillformstat");
    	
    	String advId =null;
    	String advType =null;
    	String advDesc =null;
    	String advGrade="";
    	String advName="";
		// Added by Gopu for Nov.2005 Enhancement #6
		String attribution= null; 
		
    	String advStartDate =null;
    	String advEndDate =null;
    	
    	String advUser =null;
		String advFormStatus = null;
		String statusDesc = "";
  
  
  		int count = 0;
			
			
		for(count = 1 ; count <= rowsReturned ; count++)
    	{
    
    		
			
			advId= br.getBValues(count,"advId");	
			
			advType= br.getBValues(count,"advType"); 
			
			advDesc= ((br.getBValues(count,"advDesc") == null)?"-":br.getBValues(count,"advDesc"));
			
			advGrade= ((br.getBValues(count,"advGrade") == null)?"-":br.getBValues(count,"advGrade"));

			advName=  ((br.getBValues(count,"advName") == null)?"-":br.getBValues(count,"advName"));			

			attribution= ((br.getBValues(count,"attribution") == null)?"-":br.getBValues(count,"attribution"));			

			//advStartDate = br.getBValues(count,"advSdate");

			advStartDate =  ((br.getBValues(count,"advSdate") == null)?"-":br.getBValues(count,"advSdate")); //KM

			advEndDate=  ((br.getBValues(count,"advEdate") == null)?"-":br.getBValues(count,"advEdate"));									

			advUser= br.getBValues(count,"advUser");
			
			advFormStatus = br.getBValues(count,"FORM_STATUS");


		if (! EJBUtil.isEmpty(advFormStatus))
		{
			statusDesc = cdFormStatus.getDesc(advFormStatus);
		}
		else
		{
			statusDesc = "-";
		}
			if(!advStartDate.equals("-"))
    		{
    		//advStartDate = advStartDate .toString().substring(5,7) + "/" + advStartDate .toString().substring(8,10) + "/" + advStartDate .toString().substring(0,4);

			java.sql.Date st_date = java.sql.Date.valueOf(advStartDate);
			advStartDate = DateUtil.dateToString(st_date);

    		}
    		if(!advEndDate.equals("-")){
    		//advEndDate = advEndDate .toString().substring(5,7) + "/" + advEndDate .toString().substring(8,10) + "/" + advEndDate.toString().substring(0,4); 
			java.sql.Date end_date = java.sql.Date.valueOf(advEndDate);
			advEndDate = DateUtil.dateToString(end_date);
    		}
    	%>
	
    	<%	 
    	if(count%2==0){ 
    	%>	
    	<TR class="browserEvenRow">	
    <%
    	}else{
    %>
    	<TR class="browserOddRow">
    <% 
    	} 
    %>	 			 
    
    	<input type="hidden" name="adveventId" value="<%=advId%>">			
    			 <td class=tdDefault align="center">	
    			 <A HREF="advevent_new.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&pkey=<%=personPK%>&visit=<%=visit%>&page=patientEnroll&adveventId=<%=advId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyNum=<%=studyNum%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>"><%=advType%></A>
    			 </td>
    			 <%if ((advGrade.length()==0) && (advName.length()==0)) {%>
    			 <td class=tdDefault align="center">-</td>
    			 <%}else{%>
    			 <td class=tdDefault align="center"><%=LC.L_Grade%><%--Grade*****--%>: <%=advGrade%> <%=advName%></td>
    			 <%}%>
				 <!-- Added by Gopu for Nov.2005 Enhancement #6 -->
    			 <td class=tdDefault align="center"><%=attribution%></td>
    			 <td class=tdDefault align="center"><%=advStartDate %></td>
    			 <td class=tdDefault align="center"><%=advEndDate %></td>
    			 <td class=tdDefault align="center"><%=advUser%></td>
			     <td class=tdDefault align="center"><%=statusDesc%></td>
       			 <td class=tdDefault align="center">
			     <%if(!((patStatSubType.trim().toLowerCase()).equals("lockdown"))){ %>
			     <A HREF="#" onclick="return functiondeladveve('<%=advId%>','<%=study%>','<%=pageRight%>','<%=orgRight%>');"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A>
			     <%} %>
			 	
			 
    			 </tr>
    		
    	<%
    %>
    
    <%	
    	}
      %>
      </table>
      <table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows};  %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} %>
	</td>
	</tr>
	</table>
	<table align=center>

	<%
	if (curPage==1) startPage=1;

    for (count = 1; count <= showPages;count++)
	{
     cntr = (startPage - 1) + count;     
     
	if ((count == 1) && (hasPrevious))
	{   
    %>
	<td colspan = 2> 

  	<A HREF="adveventbrowser.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&pkey=<%=personPK%>&visit=<%=visit%>&currentpage=<%=cntr-1%>&page=patientEnroll&adveventId=<%=advId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyNum=<%=studyNum%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>>&pageRight=<%=pageRight%>&orderBy=<%=orderBy%>&&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>
	</td>	
	<%
  	}	
	%>
	<td>
	<%
 
	 if (curPage  == cntr)
	 {
     %>	   
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>		
		

	   <A HREF="adveventbrowser.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&pkey=<%=personPK%>&visit=<%=visit%>&currentpage=<%=cntr%>&page=patientEnroll&adveventId=<%=advId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyNum=<%=studyNum%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>>&pageRight=<%=pageRight%>&orderBy=<%=orderBy%>&&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}	
	 %>
	</td>
	<%
	  }
 
	if (hasMore)
	{   
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;
  	
  	   <A HREF="adveventbrowser.jsp?srcmenu=<%=src%>&selectedTab=6&mode=M&pkey=<%=personPK%>&visit=<%=visit%>&currentpage=<%=cntr+1%>&page=patientEnroll&adveventId=<%=advId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&studyNum=<%=studyNum%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientId%>>&pageRight=<%=pageRight%>&orderBy=<%=orderBy%>&&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>	
	<%
  	}	
	%>
   </tr>
  </table>
  
      
       </Form>	    <!--JM: introduced the form-->
      
     <%} // if patProtId
      else {%>
      <P class="defComments">
      	  <!--This patient has not been enrolled yet. Please enroll patient before adding Adverse Events.-->
      	  <%=MC.M_NoStdPat_AddAdvEvt%><%--There is no study status added for this patient. Please add a study status before adding Adverse Events.*****--%>
      	  </P>
      <%}
	
	
	%>   
	
	<%
	} //end of if body for page right
	else
	{
	%>
	<jsp:include page="accessdenied.jsp" flush="true"/> 

	<%
    } //end of else body for page right	    %>
	<%  
} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else body for session times out

%>
  
  <div> 
   <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->   

</div>
</body>
</html>
