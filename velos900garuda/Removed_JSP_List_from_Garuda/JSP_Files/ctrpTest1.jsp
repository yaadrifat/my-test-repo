<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC" %>

<table>
<tr>
<td width="40%" align="right">
<%=LC.CTRP_DraftSectSubmType %>
</td>
<td align="left">
<s:property escape="false" value="ctrpDraftJB.trialSubmTypeMenu" />
</td>
</tr>
<tr>
<td align="right">
<%=LC.CTRP_DraftXMLRequired %>
</td>
<td align="left">
<s:radio name="ctrpDraftJB.draftXmlRequired" list="ctrpDraftJB.draftXmlReqList" 
    listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.draftXmlRequired" />
</td>
</tr>
</table>
