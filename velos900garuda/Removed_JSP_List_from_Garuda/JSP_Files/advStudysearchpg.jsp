<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Mngpcol_Srch%><%--Manage Protocols >> Search*****--%></title>
<jsp:include page="panel.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/> 


<script>
			
 
//km
function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_Want_DelStd",paramArray)))/*if (confirm("Do you want to delete <%=LC.Std_Study%> "+ name+"?" ))*****/
	return true;
    else
	return false;   
    
}//km

function opensvwin(id) {
       windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
     windowName.focus();
;}
 

function checkSearch(formobj)
{ 
    /*   if( !(formobj.Tarea.checked) &&!(formobj.team.checked)&& !(formobj.kywrd.checked) && !(formobj.sec.checked) && !(formobj.dmg.checked) && !(formobj.appndx.checked) && !(formobj.stdtype.checked) && !(formobj.stdnumb.checked)
    && !(formobj.stdphase.checked) && !(formobj.stdstatus.checked) && !(formobj.sponsor.checked) && !(formobj.morestddetails.checked) && !(formobj.excldprmtclsr.checked) && (formobj.searchCriteria.value!=""))

    {
    
    alert("Please select at least one Search option");

    
    return false;
    
    }
 	if( ((formobj.Tarea.checked) || (formobj.team.checked) || (formobj.kywrd.checked) || (formobj.sec.checked) || (formobj.dmg.checked) || (formobj.appndx.checked) || (formobj.stdtype.checked) || (formobj.stdnumb.checked)
    || (formobj.stdphase.checked) || (formobj.stdstatus.checked) || (formobj.sponsor.checked) || (formobj.morestddetails.checked)) && (formobj.searchCriteria.value==""))
  
    {
    
    alert("Please enter Search text");
    formobj.searchCriteria.focus();
    return false;
    }
    
    */

 paginate_study.runFilter();
}


delLink=function(elCell, oRecord, oColumn, oData) {


var htmlStr="";
var data=oData;
var record=oRecord;
var Delete=L_Delete;
var gn=$('groupName').value.toLowerCase();
//var gn=document.getElementById("groupName").value;
gn=gn.toLowerCase();
var pc=oRecord.getData("PATCOUNT_NUMSORT");
var sn=htmlEncode(oRecord.getData("STUDY_NUMBER"));
if (gn=="admin")
{
if (pc==0) {
 htmlStr="<div align=\"center\" ><A href=\"deletepatstudy.jsp?srcmenu="+$('srcmenu').value+"&studyNo="+escape(sn)+"&studyId="+oRecord.getData("PK_STUDY")+"&delId=studyId\" onClick=\"return confirmBox('"+sn+"')\"><img src=\"./images/delete.gif\" title='"+Delete+"' border=\"0\" /></A></div>";
} else {//pc==0
htmlStr="<div align=\"center\" ><A href=\"#\" onClick=\"alert('<%=MC.M_StdLnkToPat_CntRem%>')/*alert('<%=LC.Std_Study%> is already linked to <%=LC.Pat_Patient%> and cannot be removed')*****/\"><img src=\"./images/delete.gif\" title='"+Delete+"' border=\"0\" /></A></div>";
}//else pc==0
} //gn=admin

elCell.innerHTML=htmlStr;

}


 studypatLink = function(elCell, oRecord, oColumn, oData) {

	var data=oData;
	var record=oRecord;
	var htmlStr="";
	htmlStr ="<table align=\"center\" border=\"0\" style=\"border:none\"><tr>";
	htmlStr+="<td width=\"50%\" style=\"border:none\"><a href=\"study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId="+record.getData("PK_STUDY")+"\"><img src=\"../images/jpg/study.gif\" alt=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" title=\"<%=LC.L_Std_Admin%><%--<%=LC.Std_Study%> Administration*****--%>\" border=0 ></a></td>";
	htmlStr+="<td width=\"50%\" style=\"border:none\">";
	var sTeamRight=oRecord.getData("STUDY_TEAM_RIGHTS");
	 sTeamRight=((sTeamRight==null) || (!sTeamRight) )?"":sTeamRight;
	var seq=parseInt($('rightSeq').value);
	if (!seq) seq=0;
	var mPatRight=0;
	if (sTeamRight.length>seq)
	{
	 mPatRight=parseInt(sTeamRight.substring(seq-1,seq));
	 mPatRight=((mPatRight==null) || (!mPatRight) )?"":mPatRight;
	}
	var sActDate=oRecord.getData("STUDY_ACTUALDT_DATESORT"); 
	sActDate=((sActDate==null) || (!sActDate) )?"":sActDate;
	//alert("sActDate="+sActDate+"mPatRight="+mPatRight);
	if ((sActDate.length>0) && (mPatRight>0))
	{
	htmlStr=htmlStr+"<a href=\"studypatients.jsp?srcmenu=tdmenubaritem5&srcmenu="+$('srcmenu')+"&studyId="+record.getData("PK_STUDY")+"&patid=&patstatus=&selectedTab=2\"><img src=\"../images/jpg/patient.gif\" alt=\"<%=LC.L_PatMgmt_EnrlPat%><%--Patient Management - Enrolled Patients*****--%>\" title=\"<%=LC.L_PatMgmt_EnrlPat%><%--Patient Management - Enrolled Patients*****--%>\" border=0 ></a>";
	}
	htmlStr+="</td></tr></table>";
	elCell.innerHTML=htmlStr;
};

   statusLink  =function(elCell, oRecord, oColumn, oData){
			
			var record=oRecord;
            var htmlStr="";
            var cl="";
          
			
			var sub=record.getData("STATUS_SUBTYPE"); 
            sub=((sub==null) || (!sub) )?"":sub;
            if (sub=="temp_cls") cl="Red"; 
            if (sub=="active") cl="Green";
            
			var nt=record.getData("STUDYSTAT_NOTE");
            nt=((nt==null) || (!nt) )?"":nt;
            nt=htmlEncode(nt);
            nt = escapeSingleQuoteEtc(nt);
            var tt="<tr><td><font size=2><%=LC.L_Note%><%--Note*****--%>:</font><font size=1>"+nt+"</font></td></tr><tr><td><font size=2><%=LC.L_Organizations%><%--Organizations*****--%>:</font><font size=1> " +record.getData("SITE_NAME")
            +"</font></td></tr>";
			/*Added by sudhir for s-22307 enhancement on 12-03-2012*/		
			if(record.getData("STATUS")==".."){
				htmlStr="<div align=\"center\" ><A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+record.getData("PK_STUDY")+ "\" onmouseover=\"return overlib('<%=MC.M_Status_Cannot_Be_Displayed%>',CAPTION,'<%=LC.L_Study_Status%>')\"; onmouseout=\"return nd();\"><img src=\"../images/help.png\" border=\"0\"/></A></div>";
			}else{	/*Added by sudhir for s-22307 enhancement on 12-03-2012*/		
				htmlStr="<A href=\"studystatusbrowser.jsp?srcmenu=tdmenubaritem3&selectedTab=3&studyId="+record.getData("PK_STUDY")+ "\" onmouseover=\"return overlib('"+tt+"',CAPTION,'"+record.getData("STUDYSTAT_DATE_DATESORT")+"')\"; onmouseout=\"return nd();\"><FONT class=\""+cl+"\">"+record.getData("STATUS")+"</FONT> </A>";
			}
			elCell.innerHTML=htmlStr;
			
          }

	//Added by Manimaran to implement view title mouse over link	
	titleLink = 
		function(elCell, oRecord, oColumn, oData)
		  {
			var record=oRecord;
            var htmlStr="";

			var title = record.getData("STUDY_TITLE"); 
			title = ((title==null) || (!title) )? "-":title;
			title = htmlEncode(title);
			title = escapeSingleQuoteEtc(title);
			
			//Modified for S-22306 Enhancement : Raviesh
			if(title.length>50)
			{
				var titleExceedingLimit=title.substring(0,50);
				htmlStr=titleExceedingLimit+"&nbsp;<img src=\"./images/More.png\" class='asIsImage' border=\"0\" onmouseover=\"return overlib(\'"+title+"\',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\"\>";
			}else{			
			htmlStr="&nbsp;&nbsp;"+title;
			}
			elCell.innerHTML = htmlStr;
		
          }
    
    //Added for S-22306 Enhancement : Raviesh
	studyNumLink = 
		function(elCell, oRecord, oColumn, oData)
		  {
			var record=oRecord;
            var htmlStr="";

			var studyNum = record.getData("STUDY_NUMBER"); 
			studyNum = ((studyNum==null) || (!studyNum) )? "-":studyNum;
			studyNum = htmlEncode(studyNum);
			studyNum = escapeSingleQuoteEtc(studyNum);

			if(studyNum.length>50)
			{
				var studyNumExceedingLimit=studyNum.substring(0,50);
				htmlStr=studyNumExceedingLimit+"&nbsp;<img src=\"./images/More.png\" class='asIsImage' border=\"0\" onmouseover=\"return overlib(\'"+studyNum+"\',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.L_Study_Number%> Study Number*****--%>' ,LEFT , ABOVE);\" onmouseout=\"return nd();\"\>";
			}else{			
			htmlStr="&nbsp;&nbsp;"+studyNum;
			}
			elCell.innerHTML = htmlStr;		
          }

        
 $E.addListener(window, "load", function() { 


  paginate_study=new VELOS.Paginator('advSearch',{
 				sortDir:"asc", 
				sortKey:"STUDY_NUMBER",
				defaultParam:"userId,accountId,grpId,superuserRights,pmtclsId",
				filterParam:"Tarea,team,kywrd,sec,dmg,appndx,stdtype,stdnumb,stdphase,stdstatus,sponsor,morestddetails,excldprmtclsr,searchCriteria,statusType,currentStat,agent_device,diseaseSite,researchType,division,studysites,excldinactive",				dataTable:'serverpagination_study',
				rowSelection:[5,10,15],
				navigation: true/*,
				searchEnable:false*/
								
				});
				//paginate_study.runFilter();
				//paginate_study.render();
				
	
	/*var paginate_pat=new VELOS.Paginator('allPatient',{
 				sortDir:"asc", 
				sortKey:"PERSON_CODE",
				defaultParam:"userId,accountId,grpId",
				filterParam:"patCode,patage,siteId,patName,gender,regby,pstat,studyId,speciality",
				dataTable:'serverpagination_pat',
				navigation:false
				});
				paginate_pat.render();*/
			
			 } 
				 ) 
 </script>

<%
String src="tdMenuBarItem3";
%>


<body  class="yui-skin-sam">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/> 
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km--> 
   <%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,java.sql.*,com.velos.eres.business.common.*"%>  

 
<!-- Page drastically modified by Sonia Abrol, 02/02/06. If anybody needs to refer the old code, pls. extract from CVS history
removed the redundant code, unions, unnecessary joins
-->

<div class="browserDefault" id="div1" style="height:90%; position:absolute;">
<%

String pagenum = "";
int curPage = 0;
long startPage = 1;
long cntr = 0;
String pagenumView = "";
int curPageView = 0;
long startPageView = 1;
long cntrView = 0;

pagenum = request.getParameter("page");
if (pagenum == null)
{
    pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");



String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
    orderType = "asc";
}


pagenumView = request.getParameter("pageView");
if (pagenumView == null)
{
    pagenumView = "1";
}
curPageView = EJBUtil.stringToNum(pagenumView);

String orderByView = "";
orderByView = request.getParameter("orderByView");
String userId ="" ;
String orderTypeView = "";
orderTypeView = request.getParameter("orderTypeView");

if (orderTypeView == null)
{
    orderTypeView = "asc";
}
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) {
    userId = (String) tSession.getValue("userId");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    
    
    String accountId = (String) tSession.getValue("accountId");
    
    //Added by Manimaran to give access right for default admin group to the delete link 
	int usrId = EJBUtil.stringToNum(userId);
	userB.setUserId(usrId);
	userB.getUserDetails();
	String defGroup = userB.getUserGrpDefault();
	int grpId=EJBUtil.stringToNum(defGroup);
	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();
	//km

String studySql="" ;
String countSql="" ;
String combineSql="" ;
String searchFilter="" ;
String searchFiltercount="" ;
String studyActualDt = null;  
String studyTeamRght = null;  
        
      
       acmod.getControlValues("study_rights","STUDYMPAT");  
       ArrayList aRightSeq = acmod.getCSeq();  
       String rightSeq = aRightSeq.get(0).toString();  
       int iRightSeq = EJBUtil.stringToNum(rightSeq);  



CodeDao  cDao= new CodeDao();
int pmtclsId = cDao.getCodeId("studystat", "prmnt_cls");


String pmtclsDesc = cDao.getCodeDescription();


//get default super user rights , returns null if user does not belong to a super user group
String superuserRights = "";

superuserRights = GroupDao.getDefaultStudySuperUserRights(userId);

	CodeDao cdPhase = new CodeDao();
	cdPhase.getCodeValues("phase");
	cdPhase.setCType("phase");
	cdPhase.setForGroup(defUserGroup);
		
		
	String ddPhase = "";
	ddPhase = cdPhase.toPullDown("stdphase",0); 
	
	CodeDao cdStat = new CodeDao();
	cdStat.getCodeValues("studystat");
	
	cdStat.setCType("studystat");
	cdStat.setForGroup(defUserGroup);
	
	
	String stdstatus = "",currentStatus = "";
	
	stdstatus = cdStat.toPullDown("stdstatus",0," id = 'stdstatus'"); 
	currentStatus = cdStat.toPullDown("currentStatus",0," id = 'currentStat'");
	 



//     userId = (String) tSession.getValue("userId");
    int pageRight = 0;
    GrpRightsJB grpRights = (GrpRightsJB)     tSession.getValue("GRights");        
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
    
    if (pageRight > 0 )    {
    	Object[] arguments = {pmtclsDesc};

    
    %>
        
  <Form name="advStudysearchpg" METHOD="POST" onsubmit="return false;">

    <table width="100%" cellspacing="0" cellpadding="0" class="basetbl">
    <!--  <tr height="5"><td></td></tr> -->
    <tr>
		<td colspan="10"><p><%=LC.L_Search_By%><%--Search By*****--%></p></td>
	</tr>
	<tr>
    	<td width="12%"><%=MC.M_StdTitle_Kword%><%--<%=LC.Std_Study%> #, Title or Keyword*****--%>: </td>
    	<td width="9%"><Input type=text name="searchCriteria" id="searchCriteria" size="15" ></td>
        <td width="9%" align="right"><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%> #:</td>
        <td width="20%"><input type="text"  size="15"  name="stdnumb" id="stdnumb" ></td>
        <td width="10%" align="right"><%=LC.L_PI%><%--PI*****--%>:</td> 
        <td width="9%"><input type="text"  size="15"  name="dmg" id="dmg"></td>
        <td width="22%" align="right"> <%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%>:</td>
        <td width="9%"><input type="text"  size="15"  name="team" id="team" ></td>   
     </tr>
     <tr >
<!--      <td> -->
	  	<!------------Added on Ganapathy  on 04-22-05 for Organization Search------------------->
        
			<!------------Added on Ganapathy  on 04-22-05 for Disease Site Search------------------->
	      <td ><%=LC.L_Therapeutic_Area%><%--Therapeutic Area*****--%>: </td><td ><input type="text"  size="15" name="Tarea" id="Tarea" ></td>
	      <td align="right"><%=LC.L_Division%><%--Division*****--%>:&nbsp; </td><td ><input type="text"  size="15"  name="division" id="division" ></td>
	      <td align="right"><%=LC.L_Study_Type%><%--<%=LC.Std_Study%> Type*****--%>:&nbsp; </td><td ><input type="text"  size="15"  name="stdtype" id="stdtype"></td>    
        <td align="right"><%=LC.L_Research_Type%><%--Research Type*****--%>:&nbsp; </td><td ><input type="text"  size="15"  name="researchType" id="researchType" ></td>
        
  	   
     </tr>
   
        <tr >
    	<td><%=LC.L_Std_Org%><%--<%=LC.Std_Study%> Organizations*****--%>: </td><td ><input type="text"  size="15"  name="studysites" id="studysites" ></td>
    	<td align="right"><%=LC.L_Sponsor%><%--Sponsor*****--%>:&nbsp; </td><td ><input type="text"  size="15"  name="sponsor" id="sponsor" ></td>   	
    	<td align="right"><%=LC.L_AgentOrDevice%><%--Agent/Device*****--%>:&nbsp; </td><td ><input type="text"  size="15"  name="agent_device" id="agent_device" ></td>                      
   		<td align="right"><%=LC.L_Std_Phase%><%--<%=LC.Std_Study%> Phase*****--%>:&nbsp; </td><td ><%= ddPhase %>
       	<!--<input type="text"  size="10"  name="stdphase" id="stdphase" > -->
       </td>                  
    </tr>
   
   <tr >
 	    <td><%=LC.L_More_StdDets%><%--More <%=LC.Std_Study%> Details*****--%>: </td><td ><input type="text"  size="15"  name="morestddetails" id="morestddetails" ></td>
 	    <td align="right"><%=LC.L_Disease_Site%><%--Disease Site*****--%>:&nbsp;</td><td ><input type="text"  size="15"  name="diseaseSite" id="diseaseSite" ></td>
 	    <td align="right"><%=LC.L_Sections%><%--Sections*****--%>:&nbsp; </td><td > <input type="text"  size="15"  name="sec" id="sec" ></td>      
  	    <td align="right"><%=LC.L_Appendix%><%--Appendix*****--%>:&nbsp; </td><td ><input type="text"  size="15"  name="appndx" id="appndx"></td>                          
 	 </tr>
       
    <tr > 
    <td ><%=LC.L_Status_Type%><%--Status Type*****--%>: </td><td > <input type="text"  size="15"  name="statusType" id="statusType" ></td>
     <td align="right"><%=LC.L_Current_Status%><%--Current Status*****--%>:&nbsp; </td><td colspan="3"><%= currentStatus %> 
     	<!--- <input type="text"  size="10"  name="currentStatus" id="currentStatus" > -->
    </td>  
    	<td colspan="2"><input type="checkbox" name="excldprmtclsr" id="excldprmtclsr"  checked><%=VelosResourceBundle.getLabelString("L_Exclude_Status",arguments) %><%-- Exclude <%=pmtclsDesc%> Status*****--%></td>          
 </tr>
 <tr >
    <td > <%=LC.L_Keyword%><%--Keywords*****--%>: </td><td ><input type="text"  size="15"  name="kywrd" id="kywrd" ></td>  	                    
    <td align="right"><%=LC.L_Status_Displayed%><%--Status Displayed*****--%>:&nbsp; </td><td colspan="3"><%=stdstatus %>
          	<!-- <input type="text"  size="10"  name="stdstatus" id="stdstatus" > -->
       </td> 
     <td colspan="2"><input type="checkbox" name="excldinactive" id="excldinactive"  ><%=MC.M_Exclude_InactStd%><%--Exclude Inactive <%=LC.Std_Studies%>*****--%></td>
 </tr>
    <tr > 
      <td colspan="8" align="right">
      <!--  <A href="javascript:void(0);"  onClick="checkSearch(document.advStudysearchpg);"><img type="image" src="../images/jpg/Go.gif" border="0"></A>-->
       <button type="submit" onClick="checkSearch(document.advStudysearchpg);"><%=LC.L_Search%></button>
    </tr>                
               
    </table>

        
    <input type="hidden" id="srcmenu" value="<%=src%>">
    <input type="hidden" name="page" value="<%=curPage%>">
    <input type="hidden" id="accountId" value="<%=accountId%>">
    <input type="hidden" id="userId" value="<%=userId%>">
    <input type="hidden" id="grpId" value="<%=grpId%>">
    <input type="hidden" id="groupName" value="<%=groupName%>">
    <input type="hidden" id="rightSeq" value="<%=iRightSeq%>">
    <input type="hidden" id="superuserRights" value="<%=superuserRights%>">
    <input type="hidden" id="pmtclsId" value="<%=pmtclsId%>">
    
       
        <div >
    <div id="serverpagination_study" ></div>
    </div>
    

    
    
    
  </Form>

    <%
    } //end of if body for page right
    else{
%>
    <%@include file="accessdenied.jsp" %>
<%
    } //end of else body for page right
}//end of if body for session
else {
%>
    
    <jsp:include page="timeout.html" flush="true"/>
<%}
%>
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->      
</DIV>

</body>
</html>


