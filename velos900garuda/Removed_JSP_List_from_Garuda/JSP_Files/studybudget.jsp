<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<SCRIPT>

function openBudReport(formobj,budgetId)
{

	reppk = formobj.dBudReport.value;

	var w = formobj.dBudReport.selectedIndex;
	var repName = formobj.dBudReport.options[w].text;

	win = window.open("budrepretrieve.jsp?repId="+reppk +"&budgetPk="+budgetId+"&repName="+repName,'BudgetReport','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');


}

function checkIfSubmit(formobj)
{
	 if (validate(formobj)==false)
	{
		setValidateFlag('false');

		return false;
	} else
	{
		setValidateFlag('true');
	//	document.getElementById("submit_btn").click();
		return true;
	}

}

function confirmBox(lineitemName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [decodeString(lineitemName)];
		if (confirm(getLocalizedMessageString("L_Del_FrmBgt",paramArray)))/*if (confirm("Delete " + decodeString(lineitemName) + " from Budget?"))*****/{
		    return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function  validate(formobj){

	if(calculate(formobj) == false) {
		return false;
	}

//	if (!(validate_col('e-Signature',formobj.eSign))) return false;

//	if(isNaN(formobj.eSign.value) == true) {
//	alert("Incorrect e-Signature. Please enter again");
//	formobj.eSign.focus();
//	return false;
//	}
}


function openPrint(formobj,type){
	budgetId=formobj.budgetId.value;
	bgtcalId=formobj.bgtcalId.value;
	windowName=window.open("studybudgetprint.jsp?budgetId="+budgetId+"&bgtcalId="+bgtcalId+"&type="+type,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,width=600,height=460");
	windowName.focus();
}

function maxDigits(value)
{

   cnt = 0;

   for(i=0;i<value.length;i++)
	{
	  if(value.charAt(i)=='.'){

		  if(cnt <= 8){

		  return true;
		  }
		  else {

		  return false;
		  }
	  }
	else{
		cnt = cnt+1;

	  }
	  if(cnt > 8)
		  return false;

	}

}
function openDelete(bgtcalId, pageRight)
{

if (f_check_perm(pageRight,'E') == false) {
		return false;
	}else{
    	param1="deleteMultipleLineitems.jsp?bgtcalId="+bgtcalId+"&pageMode=first";	windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=500,top=150 ,left=250");
    	windowName.focus();
		}

}

function  calculate(formobj) {

	var totalCost = 0;
	var subTotal = 0;
	var netTotal = 0;
	var indirectCost = 0;
	var grandTotat = 0;
	var prevSectionRowsIndex = 0;
	var sectionRowsIndex = 0;
	var i = 0;
	var j = 0;



	if(formobj.rows.value == 1) {

		if(isNaN(formobj.unitCost.value) == true) {
			alert("<%=MC.M_EtrValid_UnitCost%>");/*alert("Please enter a valid Unit Cost.");*****/
			formobj.unitCost.focus();
			return false;
		}
		if(maxDigits(formobj.unitCost.value)==false){
		   alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
		   formobj.unitCost.focus();
		   return false;
			}
		if(isNaN(formobj.noUnits.value) == true) {
			alert("<%=MC.M_Etr_ValidUnitsNum%>");/*alert("Please enter valid Number of Units.");*****/
			formobj.noUnits.focus();
			return false;
		}
		if(maxDigits(formobj.noUnits.value)==false){
			alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
		   formobj.noUnits.focus();
		   return false;
			}
		if(isNaN(formobj.indirectCostPer.value) == true) {
			alert("<%=MC.M_Vldt_CostPercent%>");/*alert("Please enter a valid Indirect Costs Percentage.");*****/
			formobj.indirectCostPer.focus();
			return false;
		}


		formobj.unitCost.value = (Math.round(formobj.unitCost.value *100)/100).toFixed(2);
		formobj.noUnits.value = (Math.round(formobj.noUnits.value *100)/100).toFixed(2);

		totalCost = parseFloat(formobj.unitCost.value) * parseFloat(formobj.noUnits.value);
		formobj.totalCost.value = (Math.round(totalCost *100)/100).toFixed(2);
		netTotal = totalCost;
	} else {

		for(i=0;i<formobj.rows.value;i++) {

			if(isNaN(formobj.unitCost[i].value) == true) {
				alert("<%=MC.M_EtrValid_UnitCost%>");/*alert("Please enter a valid Unit Cost.");*****/
				formobj.unitCost[i].focus();
				return false;
			}
			if(maxDigits(formobj.unitCost[i].value)==false){
				alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
		   formobj.unitCost[i].focus();
		   return false;
			}
			if(isNaN(formobj.noUnits[i].value) == true) {
				alert("<%=MC.M_Etr_ValidUnitsNum%>");/*alert("Please enter valid Number of Units.");*****/
				formobj.noUnits[i].focus();
				return false;
			}
			if(maxDigits(formobj.noUnits[i].value)==false){
				alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
		   formobj.noUnits[i].focus();
		   return false;
			}
		}

		if(isNaN(formobj.indirectCostPer.value) == true) {
			alert("<%=MC.M_Vldt_CostPercent%>");/*alert("Please enter a valid Indirect Costs Percentage.");*****/
			formobj.indirectCostPer.focus();
			return false;
		}

		for(i=0;i<formobj.rows.value;i++) {
			formobj.unitCost[i].value = (Math.round(formobj.unitCost[i].value *100)/100).toFixed(2);
			formobj.noUnits[i].value = (Math.round(formobj.noUnits[i].value *100)/100).toFixed(2);

			formobj.totalCost[i].value = (Math.round((parseFloat(formobj.unitCost[i].value) * parseFloat(formobj.noUnits[i].value))*100)/100).toFixed(2);
			netTotal = netTotal + parseFloat(formobj.totalCost[i].value);
		}
		if(formobj.sectionCount.value == 1){
			if(formobj.sectionRowsIndex.value == 0) {
				subTotal =  formobj.totalCost[0].value;
				formobj.subTotal.value = (Math.round(subTotal*100)/100).toFixed(2);
			} else {
				for(i=0;i<=formobj.sectionRowsIndex.value;i++){
					subTotal =  subTotal + parseFloat(formobj.totalCost[i].value);
				}
				formobj.subTotal.value = (Math.round(subTotal*100)/100).toFixed(2);
			}
		} else {
			sectionRowsIndex = -1;
			for(i=0;i<formobj.sectionCount.value;i++) {
				prevSectionRowsIndex = parseInt(sectionRowsIndex) + 1;
				sectionRowsIndex = formobj.sectionRowsIndex[i].value;
				subTotal = 0;
				for(j=prevSectionRowsIndex;j<=sectionRowsIndex;j++){
					subTotal =  subTotal + parseFloat(formobj.totalCost[j].value);
				}
				formobj.subTotal[i].value = (Math.round(subTotal*100)/100).toFixed(2);
			}
		}
	}

	formobj.indirectCostPer.value = (Math.round(formobj.indirectCostPer.value*100)/100).toFixed(2);

	indirectCost = (parseFloat(formobj.indirectCostPer.value) * netTotal)/100.0;
	grandTotal = indirectCost + netTotal;


	formobj.netTotal.value = (Math.round(netTotal*100)/100).toFixed(2);
	formobj.indirectCost.value = (Math.round(indirectCost*100)/100).toFixed(2);
	formobj.grandTotal.value = (Math.round(grandTotal*100)/100).toFixed(2);

}
// Added by gopu to fix the bugzilla Issue #2391 on 04th April 2006

function fnMessage(){
alert("<%=MC.M_Freeze_CntModified%>")/*alert(" Freeze / Template Status cannot be modified")*****/
}
</SCRIPT>
</head>


<% String src;
src= request.getParameter("srcmenu");
String selectedTab = "";
String selectedBgtcalId = "";
String mode = "";

int studyId = 0;
%>





<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" onFocus="setMessage('silentSave','','')" onClick="setMessage('silentSave','','')">
<title><%=LC.L_Std_Bgt%><%--<%=LC.Std_Study%> Budget*****--%></title>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="budgetB" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budgetDet" scope="request" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budgetcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id="lineitemB" scope="request" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="codeLst" scope="request" class="com.velos.esch.business.common.SchCodeDao"/>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="grpB" scope="request" class="com.velos.eres.web.group.GroupJB"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.ReportDaoNew,com.velos.esch.business.common.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.budgetcal.impl.BudgetcalBean" %>
<%
	HttpSession tSession = request.getSession(true);
	String uName = "";
	String userIdFromSession ="";
	String acc = "";
	String grandTotalTotalCost="",bgtStat="";
	String lineItemDeleted = "";
	String budgetStatusDesc = "";

	if (sessionmaint.isValidSession(tSession))
	{    //copy budget is controlled by budget new group right

		GrpRightsJB budgetGrpRights = (GrpRightsJB) tSession.getValue("GRights");
		int budgetGrpRight = Integer.parseInt(budgetGrpRights.getFtrRightsByValue("BUDGT"));

	    uName = (String) tSession.getValue("userName");
		userIdFromSession = (String) tSession.getValue("userId");
		acc = (String) tSession.getValue("accountId");
		lineItemDeleted = (String) tSession.getValue("lineItemDeleted");
		lineItemDeleted=(lineItemDeleted==null)?"N":lineItemDeleted;

		selectedTab = request.getParameter("selectedTab");
		mode = request.getParameter("mode");

		//variable pageMode can have initial and final as values. initial in case the page is opened from a page other than this one and final in case it is being opened from a link on this page itself directly or indirectly
		String pageMode = request.getParameter("pageMode");
		if(pageMode == null) pageMode = "initial";
		if(pageMode.equals("final")) {
			selectedBgtcalId = request.getParameter("bgtcalId");
		}


//

		int accId = EJBUtil.stringToNum(acc);
		int userId = EJBUtil.stringToNum(userIdFromSession);
		int budgetId = EJBUtil.stringToNum(request.getParameter("budgetId"));
		String budgetTemplate = "S";
		int pageRight = 0;
		int stdRight =0;

//JM: 17Jun2009: #3727
String budgetStudyId="";
if(budgetId>0)
{
	budgetDet.setBudgetId(budgetId);
	budgetDet.getBudgetDetails();
	budgetStudyId=budgetDet.getBudgetStudyId();

}

		userB=(com.velos.eres.web.user.UserJB)tSession.getAttribute("currentUser");
		String defaultGrp=userB.getUserGrpDefault();
		grpB.setGroupId(EJBUtil.stringToNum(defaultGrp));
		grpB.getGroupDetails();

		//*get user rights for the budget and put it in the session for use in other budget tabs

		String rightStr = budgetUsersB.getBudgetUserRight(budgetId,userId);
		int rightLen = rightStr.length();

		if (rightLen==0)
		{
		rightStr=grpB.getGroupSupBudRights();
		rightLen=rightStr.length();
		}

		ctrl.getControlValues("bgt_rights");

		ArrayList feature =  ctrl.getCValue();
		ArrayList ftrDesc = ctrl.getCDesc();
		ArrayList ftrSeq = ctrl.getCSeq();
		ArrayList ftrRight = new ArrayList();
		String strR;

		for (int counter = 0; counter <= (rightLen - 1);counter ++)
		{
			strR = String.valueOf(rightStr.charAt(counter));
			ftrRight.add(strR);
		}

		//grpRights bean has the methods for parding the rights so use its set methods
		grpRights.setGrSeq(ftrSeq);
    	grpRights.setFtrRights(ftrRight);
		grpRights.setGrValue(feature);
		grpRights.setGrDesc(ftrDesc);

		//set the grpRights Bean in session so that it can be used in other
		//budget pages directly to get the pageRight.

		tSession.putValue("BRights",grpRights);

		//*end of setting pageRight

		pageRight  = Integer.parseInt(grpRights.getFtrRightsByValue("BGTDET"));

%>


<%

		if ((mode.equals("M") && pageRight >=4) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 )) )
		{
			int i = 0;
			int j = 0;
			BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);
			ArrayList bgtNames = budgetDao.getBgtNames();
			ArrayList bgtStats = budgetDao.getBgtStats();

			ArrayList bgtStudyTitles = budgetDao.getBgtStudyTitles();
			ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
			ArrayList bgtSites = budgetDao.getBgtSites();
			ArrayList bgtCurrs = budgetDao.getBgtCurrs();

		    ArrayList bgtStatDescs= budgetDao.getBgtCodeListStatusesDescs();

			String bgtName =(String) bgtNames.get(0);
			bgtStat = (String) bgtStats.get(0);
			String bgtStudyTitle = (String) bgtStudyTitles.get(0);

			String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
			String bgtSite = (String) bgtSites.get(0);

			String currency = "";

				budgetStatusDesc = (String) bgtStatDescs.get(0);

			if (bgtCurrs.size() >0){
			String bgtCurr = (String) bgtCurrs.get(0);
			int codeId = Integer.parseInt(bgtCurr);
			currency = codeLst.getCodeDescription(codeId);

			}

			String bgtName_Disp = "";
			String bgtStudyNumber_Disp = "";
			String bgtSite_Disp = "";
			String bgtStudyTitle_Disp = "";




			boolean sectionFlag = true;

			bgtName = (bgtName == null || bgtName.equals(""))?"-":bgtName;
			bgtStat = (bgtStat == null || bgtStat.equals(""))?"-":bgtStat;
			if(bgtStat.equals("F")) {
				bgtStat = "Freeze";
			} else if(bgtStat.equals("W")) {
				bgtStat = "Work in Progress";
			} else if(bgtStat.equals("T")) {
				bgtStat = "Template";
				}
			bgtStudyTitle = (bgtStudyTitle == null || bgtStudyTitle.equals(""))?"-":bgtStudyTitle;
			bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
			bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;


			BudgetcalDao budgetcalDao = budgetcalB.getAllBgtCals(budgetId);
			ArrayList bgtcalIds = budgetcalDao.getBgtcalIds();
			ArrayList bgtcalProtNames = budgetcalDao.getBgtcalProtNames();

			String currentBgtcalId = "";
			int bgtcalId = 0;
			currentBgtcalId = ((Integer) bgtcalIds.get(0)).toString();
			bgtcalId = ((Integer) bgtcalIds.get(0)).intValue();

			LineitemDao lineitemDao = lineitemB.getLineitemsOfBgtcal(bgtcalId);
			ArrayList lineitemIds = lineitemDao.getLineitemIds();
            ArrayList lineitemNames = lineitemDao.getLineitemNames();
            ArrayList lineitemDescs = lineitemDao.getLineitemDescs();
            ArrayList lineitemSponsorUnits = lineitemDao.getLineitemSponsorUnits();

            ArrayList lineitemClinicNOfUnits = lineitemDao.getLineitemClinicNOfUnits();

            ArrayList lineitemDelFlags = lineitemDao.getLineitemDelFlags();
            ArrayList bgtSectionIds = lineitemDao.getBgtSectionIds();
            ArrayList bgtSectionNames = lineitemDao.getBgtSectionNames();
            ArrayList bgtSectionVisits = lineitemDao.getBgtSectionVisits();
            ArrayList bgtSectionDelFlags = lineitemDao.getBgtSectionDelFlags();
            ArrayList bgtSectionSequences = lineitemDao.getBgtSectionSequences();
			ArrayList lineitemCategories = lineitemDao.getLineitemCategories();
			ArrayList lineitemTotalCosts =  lineitemDao.getLineitemTotalCost();
			ArrayList srtCostGrandTotals = lineitemDao.getSrtCostGrandTotal();




			int sectionCount = 0;
			int rows = 0;
				for(i=0;i<lineitemDao.getBRows();i++) {
				if(lineitemNames.get(i) != null && !lineitemNames.get(i).equals("")){
					rows++;
				}
			}
			int totalrows = lineitemDao.getBRows();

			int lineitemId = 0;
			String lineitemName = "";
			String lineitemDesc = "";
			String lineitemSponsorUnit = "0.00";
			String lineitemClinicNOfUnit = "0.00";
			String lineitemDelFlag = "";
			int bgtSectionId = 0;
			String bgtSectionName = "";
			String bgtSectionVisit = "";
			String bgtSectionDelFlag = "";
			String bgtSectionSequence = "";
			String srtCostGrandTotal="";
			String srtCostGrandTotalOld="";

			String lineitemTotalCost = "";

			int sectionRowsIndex = 0;

			int bgtSectionIdOld = 0;

			budgetcalB.setBudgetcalId(bgtcalId);
			BudgetcalBean bcsk = budgetcalB.getBudgetcalDetails();
			String indirectCostPer = budgetcalB.getIndirectCost();
			String grandResTotalCost =  budgetcalB.getBudgetResearchTotalCost(); //request.getParameter("netTotal");
			String grandIndTotalCost = budgetcalB.getBudgetIndirectTotalCost(); // request.getParameter("indirectCost");
			grandTotalTotalCost =  budgetcalB.getBudgetTotalTotalCost(); // request.getParameter("grandTotal");
			indirectCostPer = (indirectCostPer == null)?"0.00":indirectCostPer;
			grandResTotalCost = (grandResTotalCost == null)?"0.00":grandResTotalCost;
			grandIndTotalCost = (grandIndTotalCost == null)?"0.00":grandIndTotalCost;
			grandTotalTotalCost = (grandTotalTotalCost == null)?"0.00":grandTotalTotalCost;

%>


<Form name="budget" method="post" action="#">
    <input type="hidden" name="srcmenu" size = 20  value = <%=src%> >
    <input type="hidden" name="selectedTab" size = 20  value = <%=selectedTab%> >
    <input type="hidden" name="mode" size = 20  value = <%=mode%> >
	<input type="hidden" name="pageMode" size = 20  value="final">
	<input type="hidden" name="budgetId" size = 20  value=<%=budgetId%>>
	<input type="hidden" name="budgetStudyId" size=20 value=<%=budgetStudyId%>>

<DIV class="tabDefTopN" id="div1">

	<jsp:include page="budgettabs.jsp" flush="true">
	<jsp:param name="budgetTemplate" value="S"/>
	<jsp:param name="mode" value="<%=mode%>"/>
	</jsp:include>
</DIV>

<DIV class="tabFormTopN" id="div1" style="overflow:hidden;">

    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline">
	    <tr>
			<td ><font size="1"><B> <%=LC.L_Budget_Name%><%--Budget Name*****--%>:</B>&nbsp;&nbsp;
			<%
			 //JM: 14Aug2008: #3727
			if(bgtName.length() > 27){
		   		bgtName_Disp = bgtName.substring(0,27);
		   		bgtName_Disp = bgtName_Disp + "...";
		   	%>
			<a href="#" onmouseover="return overlib('<%=bgtName%>',CAPTION,'<%=LC.L_Budget_Name%><%--Budget Name*****--%>:' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtName_Disp%></a>
			<%}else{%>
			<%=bgtName%>
			<%}%>
			</font>
			</td>

	        <td><font size="1"><B> <%=LC.L_Bgt_Status%><%--Budget Status*****--%>:</B>&nbsp;&nbsp;<%=budgetStatusDesc%></font></td>
			<td><font size="1"><B> <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:</B>&nbsp;&nbsp;
            <% if (!StringUtil.isEmpty(budgetStudyId)) { %>
			<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=budgetStudyId%>">

			<%
	        //JM: 14Aug2008: #3727
			if(bgtStudyNumber.length() > 27){
		   		bgtStudyNumber_Disp = bgtStudyNumber.substring(0,27);
		   		bgtStudyNumber_Disp = bgtStudyNumber_Disp + "...";
		   		%>
		   	<a href="study.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&studyId=<%=budgetStudyId%>" onmouseover="return overlib('<%=bgtStudyNumber%>',CAPTION,'<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtStudyNumber_Disp%></a>

			<%}else{%>
			<%=bgtStudyNumber%>
			<%}%></a></font>
			<img src="../images/jpg/help.jpg" border="0"
		onmouseover="return overlib('<%=StringUtil.escapeSpecialCharJS(bgtStudyTitle)%>',CAPTION,'<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>:' ,LEFT , ABOVE );"
		onmouseout="return nd();" alt=""></img>
            <% } else {  %>
            -
            <% }  %>
			</td>



	        <td><font size="1"><B> <%=LC.L_Organization%><%--Organization*****--%>:</B>&nbsp;&nbsp;

	        <%
	        //JM: 14Aug2008: #3727
			if(bgtSite.length() > 27){
		   		bgtSite_Disp = bgtSite.substring(0,27);
		   		bgtSite_Disp = bgtSite_Disp + "...";
		   		%>
		   	<a href="#" onmouseover="return overlib('<%=bgtSite%>',CAPTION,'<%=LC.L_Organization%><%--Organization*****--%>' ,LEFT , ABOVE);" onmouseout="return nd();"><%=bgtSite_Disp%></a>
			<%}else{%>
	        	<%=bgtSite%>
			<%}%>
			</font>
			</td>
		</tr>

					</table>
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
			<tr>


<%if ((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template"))&&(mode.equals("M"))) {
Object[] arguments = {budgetStatusDesc}; %>
	<td>
    	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=budgetStatusDesc%>'. You cannot make any changes to the budget.*****--%></Font></P>
	</td>

<%}%>



<%
if (pageRight == 4) {
%>
<td>
   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_YouOnly_ViewPerm%><%--You have only View permission*****--%></Font></P>
</td>
<%}%>


			</tr>



		</table>



<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
<tr>

   <td width="50%" >

	<%
			//for reports dropdown

			ReportDaoNew repDao = new ReportDaoNew();
			repDao.getReports("study_bud",accId);
			String strBudRep = "";
			int startIndx =0;
			int stopIndx =0;
			String labelKeyword ="";
			String labelString ="";
			String messageKeyword ="";
			String messageString ="";
			String messageKey ="";
			String tobeReplaced ="";
			String messageParaKeyword []=null;
			strBudRep = EJBUtil.createPullDownWithStrNoSelect("dBudReport","",repDao.getPkReport(),repDao.getRepName());
			//KM-Label Changes for Budget report dropdown
			while(strBudRep.contains("VELLABEL[")){
				startIndx = strBudRep.indexOf("VELLABEL[") + "VELLABEL[".length();
				stopIndx = strBudRep.indexOf("]",startIndx);
				labelKeyword = strBudRep.substring(startIndx, stopIndx);
				labelString = LC.getLabelByKey(labelKeyword);
				strBudRep=StringUtil.replaceAll(strBudRep, "VELLABEL["+labelKeyword+"]", labelString);
			}
			
			while(strBudRep.contains("VELMESSGE[")){
				startIndx = strBudRep.indexOf("VELMESSGE[") + "VELMESSGE[".length();
				stopIndx = strBudRep.indexOf("]",startIndx);
				messageKeyword = strBudRep.substring(startIndx, stopIndx);
				messageString = MC.getMessageByKey(messageKeyword);
				strBudRep=StringUtil.replaceAll(strBudRep, "VELMESSGE["+messageKeyword+"]", messageString);
			}
			while(strBudRep.contains("VELPARAMESSGE[")){
				startIndx = strBudRep.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
				stopIndx = strBudRep.indexOf("]",startIndx);
				messageKeyword = strBudRep.substring(startIndx, stopIndx);
				messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
				messageKey=messageKey.trim();
				messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
				messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
				strBudRep=StringUtil.replaceAll(strBudRep, "VELPARAMESSGE["+messageKeyword+"]", messageString);
			}
			if(!strBudRep.contains("word.GIF")){
				if(strBudRep.contains(LC.L_Word_Format)){
					tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
					strBudRep=StringUtil.replace(strBudRep, LC.L_Word_Format, tobeReplaced);
				}
				if(strBudRep.contains(LC.L_Excel_Format)){
					tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
					strBudRep=StringUtil.replace(strBudRep,LC.L_Excel_Format, tobeReplaced);
				}
				if(strBudRep.contains(LC.L_Printer_FriendlyFormat)){
					tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
					strBudRep=StringUtil.replace(strBudRep, LC.L_Printer_FriendlyFormat, tobeReplaced);
				}
			}
	
		%>
			<font size = -2 color="#696969"><b><%=LC.L_Reports%><%--Reports*****--%></b></font><BR>
			<%=strBudRep%> &nbsp;
			<button	onClick="return openBudReport(document.budget,<%=budgetId%> );"><%=LC.L_Display%></button>
	</td>

<!--    <td class=tdDefault width="20%" align="center">
	 <A HREF="#" onclick="openPrint(document.bgtprot, 'E')">Export to Excel</A>
	</td>
	<td class=tdDefault  width="20%" align="center">
	 <A HREF="#" onclick="openPrint(document.bgtprot, 'P')">Print Budget</A>
	</td> -->
    	 <%// sv, 08/10, fix for bug #1456, f_check_parm on pageRight %>
	<td class=tdDefault width = "8%" align="right"> 	
	<A HREF="#" onClick="return calculate(document.bgtprot);"><img title="<%=LC.L_Refresh_Calculations%>" src="./images/Refresh.gif" border ="0"><%//=LC.L_Refresh_Calculations%><%--Refresh Calculations*****--%></A>
	 &nbsp;&nbsp;
	<A onClick="return f_check_perm(<%=pageRight%>,'N')" href="copybudget.jsp?from=initial&srcmenu=<%=src%>&budgetId=<%=budgetId%>"><img title="<%=LC.L_Copy_Bgt%>" src="./images/copy.gif" border ="0"><%//=LC.L_Copy_ThisBgt%><%--Copy this Budget*****--%></A></td>
	<td class=tdDefault width = "2%" align="left">
			<A HREF="budgetsections.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&selectedTab=<%=selectedTab%>&calId=<%=bgtcalId%>&mode=<%=mode%>&bgtStat=<%=bgtStat%>"><img title="<%=LC.L_Edit_Sections%>" src="./images/EditSection.gif" border ="0"><%//=LC.L_Edit_Sections%><%--Edit Sections*****--%></A>
	</td>
	<!-- Added by gopu to fix the bugzilla Issue #2391 on 04th April 2006 -->
	<%if (!(bgtStat.equals("Freeze") || bgtStat.equals("Template"))) {%>
		<td width="2%" align="left" ><A href="#" onClick="openDelete(<%=bgtcalId%>,<%=pageRight%>)"><img title="<%=LC.L_Del_Multi%>" src="./images/DeleteSelected.gif" border ="0"><%//=LC.L_Del_Multi%><%--Delete Multiple*****--%></A>
	<%} else{%>
		<td width="2%" align="left"><A href="#" onClick="return fnMessage(); "><img title="<%=LC.L_Del_Multi%>" src="./images/DeleteSelected.gif" border ="0"><%//=LC.L_Del_Multi%><%--Delete Multiple*****--%></A>
		<%	}%>
	</tr>
</table>

<table width="100%" cellspacing="1" cellpadding="0" border="0" class="basetbl outline">
	<tr height="25px">
		<th width=15% align =center><%=LC.L_Type%><%--Type*****--%></th>
		<th width=15% align =center><%=LC.L_Category%><%--Category*****--%></th>
		<th width=23% align =center><%=LC.L_Description%><%--Description*****--%></th>
	    <th width=12% align =center><%=LC.L_Unit_Cost%><%--Unit Cost*****--%></th>
  	    <th width=12% align =center><%=LC.L_NumOfUnits%><%--No. of Units*****--%></th>
  	    <th width=15% align =center><%=LC.L_Total_Cost%><%--Total Cost*****--%></th>
  	    <th width=8% align =center><%=LC.L_Line_Item%><%--Line item*****--%></th>
	</tr>
		</table>
</div>

</FORM>

<DIV class="tabFormBotN_2 tabFormBotN_SB_2" id="div2">

<Form name="bgtprot" id="budgetForm" method=post action="savestudybudget.jsp"  onSubmit="if (checkIfSubmit(document.bgtprot)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); toggleNotification('on','<%=MC.M_BgtRef_SvgProgWait%><%--Budget Refreshing & Saving in progress. Please wait...*****--%>'); return true;}">

<input type=hidden name=rows value=<%=rows%>>
<input type=hidden name=mode value=<%=mode%>>
<input type=hidden name=srcmenu value=<%=src%>>
<input type=hidden name=selectedTab value=<%=selectedTab%>>
<input type=hidden name=budgetId value=<%=budgetId%>>
<input type=hidden name=bgtcalId value=<%=bgtcalId%>>
<input type=hidden name=budgetTemplate<%=budgetTemplate%>>




	<table width="100%" cellspacing="1" cellpadding="0" border="0" class="basetbl outline">
	<tr>

<%
	bgtSectionIdOld = ((Integer) lineitemIds.get(0)).intValue();
	String ctgryPullDn = "";
    SchCodeDao schDao = new SchCodeDao();
    schDao.getCodeValues("category");

	for(i=0;i<totalrows;i++) {
		lineitemId = ((Integer) lineitemIds.get(i)).intValue();
		lineitemName = (String) lineitemNames.get(i);
		lineitemDesc = (String) lineitemDescs.get(i);
		lineitemSponsorUnit =(String) lineitemSponsorUnits.get(i);
		if(lineitemSponsorUnit ==null)
			lineitemSponsorUnit = "0.00";
		lineitemClinicNOfUnit = (String) lineitemClinicNOfUnits.get(i);
		if(lineitemClinicNOfUnit == null)
			lineitemClinicNOfUnit = "0.00";
		lineitemDelFlag = (String) lineitemDelFlags.get(i);
		bgtSectionId = ((Integer) bgtSectionIds.get(i)).intValue();
		bgtSectionName = (String) bgtSectionNames.get(i);
		bgtSectionVisit = (String) bgtSectionVisits.get(i);
		bgtSectionDelFlag = (String) bgtSectionDelFlags.get(i);
		bgtSectionSequence = (String) bgtSectionSequences.get(i);
		ctgryPullDn = schDao.toPullDown("cmbCtgry",EJBUtil.stringToNum((String)lineitemCategories.get(i)),1);
		String itemId = EJBUtil.integerToString(new Integer(lineitemId));

		srtCostGrandTotal  = (String) srtCostGrandTotals.get(i);
		//Added by IA 12.13.2006 bug #2777

		if(srtCostGrandTotal == null)
			srtCostGrandTotal = "0.00";
		//end added
		lineitemDesc = (lineitemDesc == null)?"-":lineitemDesc;
		bgtSectionName = (bgtSectionName == null)?"-":bgtSectionName;
		bgtSectionVisit = (bgtSectionVisit == null)?"-":bgtSectionVisit;

		lineitemTotalCost =(String) lineitemTotalCosts.get(i);
		if(lineitemTotalCost == null)
			lineitemTotalCost = "0.00";


		if(bgtSectionIdOld != bgtSectionId) {
			if(j>0 && sectionFlag == true){
				sectionFlag = false;
				sectionCount++;
	 	   if ((j%2)==0) {
%>
      <tr class="browserEvenRow">
<%
	 		   }else{
%>
      <tr class="browserOddRow">
<%
	 		   }
			   j++;
%>			<input type="hidden" size=10 name="bgtSectionId" class="rightAlign" value="<%=bgtSectionIdOld%>">
			<input type=hidden name=sectionRowsIndex value=<%=(sectionRowsIndex-1)%>>
			<td align=right colspan=5>
				<B><%Object[] arguments1 = {currency}; %><%=VelosResourceBundle.getLabelString("L_Subtotal_In",arguments1)%><%--Sub-Total (in <%=currency%>)*****--%></B>
			</td>

			<td align=center>
				<input type=text name=subTotal size=10 class="rightAlign" value=<%=srtCostGrandTotalOld%> readonly>
			</td>
			<td>
				&nbsp;
			</td>

		</tr>
<%
			}
%>

		<tr>
			<td><B><%=bgtSectionName%></B>
			</td>

			<td colspan=5>&nbsp;</td>

				<td align=right>
				<%if (!(bgtStat.equals("Freeze") || bgtStat.equals("Template"))) {%>

				<A onClick="return f_check_perm(<%=pageRight%>,'N')" HREF="additemstudy.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&mode=M&selectedTab=<%=selectedTab%>&bgtcalId=<%=bgtcalId%>&lineitemMode=N&bgtSectionId=<%=bgtSectionId%>&bgtSectionName=<%=bgtSectionName%>&bgtStat=<%=bgtStat%>"><img title="<%=LC.L_Add_New%>" src="./images/Add.gif" border ="0"><%//=LC.L_Add_New%><%--Add New*****--%></A>
				<%}%>
	</td>
		</tr>

<%
		}

	if(lineitemName != null && !lineitemName.equals("")) {
		sectionRowsIndex++;
		sectionFlag = true;
	 	   if ((j%2)==0) {
%>
      <tr class="browserEvenRow">
<%
	 		   }else{
%>
      <tr class="browserOddRow">
<%
	 		   }
		j++;
//This is done to support netscape 4.79. Problem with request parameter .

bgtSectionName=bgtSectionName.replace(' ','~');
lineitemName=lineitemName.replace(' ','~');
lineitemDesc=lineitemDesc.replace(' ','~');

%>
			<td width="15%">

		<A onClick="return f_check_perm(<%=pageRight%>,'E')" HREF="additemstudy.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&budgetTemplate=<%=budgetTemplate%>&mode=M&selectedTab=<%=selectedTab%>&bgtcalId=<%=bgtcalId%>&lineitemMode=M&bgtSectionId=<%=bgtSectionId%>&bgtSectionName=<%= StringUtil.encodeString(bgtSectionName) %>&lineitemName=<%= StringUtil.encodeString(lineitemName) %>&lineitemDesc=<%= StringUtil.encodeString(lineitemDesc) %>&lineitemId=<%=itemId%>&bgtStat=<%=bgtStat%>"><%=lineitemName.replace('~',' ')%></A>

			</td>
				<td width="15%"><%=ctgryPullDn%></td>
		<%
			bgtSectionName=bgtSectionName.replace('~',' ');
		  	lineitemName=lineitemName.replace('~',' ');
			lineitemDesc=lineitemDesc.replace('~',' ');

			%>
			<td width="23%">
				<%=lineitemDesc%>
			</td>
			<td width="12%" align=center>
				<input type=text name=unitCost size=10 maxlength=11 class="rightAlign" value=<%=lineitemSponsorUnit%>>
			</td>
			<td width="12%" align=center>
				<input type=text name=noUnits size=10 maxlength=11 class="rightAlign" value=<%=lineitemClinicNOfUnit%>>
			</td>
			<td width="15%" align=center>
				<input type=text name=totalCost size=10 maxlength=11 class="rightAlign" value=<%=lineitemTotalCost%> readonly >
			</td>
			<td>
			<%if (!(bgtStat.equals("Freeze") || bgtStat.equals("Template"))) {%>
			<A onclick="return confirmBox('<%= StringUtil.encodeString(lineitemName) %>',<%=pageRight%>)" HREF="lineitemdelete.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetId=<%=budgetId%>&lineitemId=<%=lineitemId%>&budgetTemplate=<%=budgetTemplate%>&bgtcalId=<%=bgtcalId%>"><img title="<%=LC.L_Delete%><%--Delete*****--%>" src="./images/delete.gif" border="0" align="left"/></A>
			<%}%>
			</td>
		</tr>
		<input type=hidden name=lineitemId value=<%=lineitemId%>>
<%
	}
		bgtSectionIdOld = bgtSectionId;
		srtCostGrandTotalOld = srtCostGrandTotal ;
%>



<%
	}
		if(sectionFlag == true){
				sectionFlag = false;
				sectionCount++;
	 	   if ((j%2)==0) {
%>
      <tr class="browserEvenRow">
<%
	 		   }else{
%>
      <tr class="browserOddRow">
<%
	 		   }
			   j++;
%>			<input type="hidden" name="bgtSectionId" value="<%=bgtSectionId%>">
			<input type=hidden name=sectionRowsIndex value=<%=(sectionRowsIndex-1)%>>
			<td align=right colspan=5>
				<B><%Object[] arguments2 = {currency}; %><%=VelosResourceBundle.getLabelString("L_Subtotal_In",arguments2)%><%--Sub-Total (in <%=currency%>)*****--%></B>
			</td>


			<td align=center>
				<input type=text name=subTotal size=10 class="rightAlign" value=<%=srtCostGrandTotalOld%> readonly>
			</td>
			<td>
				&nbsp;
			</td>

		</tr>
<%
			}
%>
	<input type=hidden name=sectionCount value=<%=sectionCount%>>
<%
	if ((j%2)==0) {
%>
      <tr class="browserEvenRow">
<%
	 		   }else{
%>
      <tr class="browserOddRow">
<%
	 		   }
	j++;
%>
			<td align=right colspan=5>
				<B><%Object[] arguments4 = {currency}; %><%=VelosResourceBundle.getLabelString("L_Total_In_Dyn",arguments4)%><%-- Total (in <%=currency%>)*****--%></B>
			</td>

			<td align=center>
				<input type=text name=netTotal class="rightAlign" size=10  value=<%=grandResTotalCost%> readonly>
			</td>
			<td>
				&nbsp;
			</td>

		</tr>

<%
	if ((j%2)==0) {
%>
      <tr class="browserEvenRow">
<%
	 		   }else{
%>
      <tr class="browserOddRow">
<%
	 		   }
		j++;
%>
			<td align=right>
				<B><%=LC.L_Indirect_Costs%><%--Indirect Costs*****--%></B>
			</td>
			<td>
			<input type=text name=indirectCostPer class="rightAlign" size=10  value=<%=indirectCostPer%> > % <%=LC.L_Of_Total%><%--of Total*****--%>
			</td>
			<td>
				&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
				<td>
				&nbsp;
			</td>
			<td align=center>
				<input type=text name=indirectCost size=10 class="rightAlign" value=<%=grandIndTotalCost%> readonly>
			</td>
			<td>
				&nbsp;
			</td>

		</tr>
<%
	if ((j%2)==0) {
%>
      <tr class="browserEvenRow">
<%
	 		   }else{
%>
      <tr class="browserOddRow">
<%
	 		   }
		j++;
%>
			<td align=right colspan=5>
				<B><%Object[] arguments3 = {currency}; %><%=VelosResourceBundle.getLabelString("L_GrandTotal_In",arguments3)%><%-- Grand Total (in <%=currency%>)*****--%></B>
			</td>

			<td align=center>
				<input type=text name=grandTotal size=10 class="rightAlign" value=<%=grandTotalTotalCost%> readonly>
			</td>
			<td>
				&nbsp;
			</td>

		</tr>
	<tr height="5">
	<td>&nbsp;</td>
	</tr>
	<tr>
        <td class=tdDefault align="center" colspan="6">
			<A HREF="#" onClick="return calculate(document.bgtprot);"><img title="<%=LC.L_Refresh_Calculations%>" src="./images/Refresh.gif" border ="0"><%--Refresh Calculations*****--%></A>
		</td>
	</tr>

</table>

<%



if ((!((!(bgtStat ==null)) && (bgtStat.equals("Freeze") || bgtStat.equals("Template"))&&(mode.equals("M")))) && (pageRight>=6)) {%>


<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="budgetForm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


</table>
	<BR><BR>
<%}

if (((bgtStat.toLowerCase()).equals("template")) || ((bgtStat.toLowerCase()).equals("freeze")))
{
	%>
	<DIV id="hideme" style="display:none">
	<input type=image src="../images/jpg/Submit.gif"
			border=0 id="silentSubmit" onclick="javascript:return checkIfSubmit(document.bgtprot);">
			</DIV>
	<input	type="hidden" name="eSign" id="eSign" value="0">
	<table width="98%"><tr><td width="40%"></td>
	<td><span id="submitmsg" align="center"></span>
	</td>
	</tr>
<%}%>
</Form>



<%	}
else {
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
 } //end of else body for page right
	}//end of if body for session
   	else
	{
	%>

		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>

	<div>
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</DIV>
<%if ((((bgtStat.toLowerCase()).equals("template")) || ((bgtStat.toLowerCase()).equals("freeze"))) && (grandTotalTotalCost.equals("0.00"))){
%>
<script>linkForSilentSubmit("budgetForm","submitmsg","<%=MC.M_MigratingBgt_NewVer%><%--Migrating Budget to newer version*****--%>","<%=MC.M_Bgt_Migrated_Succ%><%--Budget Migrated Successfully*****--%>","<%=MC.M_Err_BgtMigration%><%--Error in Budget Migration*****--%>");
document.getElementById("silentSubmit").click();
toggleNotification("on","<%=MC.M_MigratingBgt_ToNewVerWait%><%--Migrating Budget to newer Version. Please wait...*****--%>");
</script>
<%}
else {%>

<script>
<%if(lineItemDeleted.equals("Y")){ %>
//document.bgtprot.eSign.value = "0";
document.getElementById("submit_btn").click();
//setDisableSubmit('true');
//document.bgtprot.eSign.value = "";
<% tSession.setAttribute("lineItemDeleted", "N"); %>
toggleNotification("on","<%=MC.M_BgtRef_SvgProgWait%><%--Budget Refreshing & Saving in progress. Please wait...*****--%>");
<%}%>
</script>
<%
}
%>

<div class ="mainMenu" id="emenu">
  	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>
</html>

