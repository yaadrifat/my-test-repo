<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<title><%=MC.M_Std_BrowserInEres%><%--<%=LC.Std_Study%> Browser in eResearch*****--%></title>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>


<jsp:include page="homepanel.jsp" flush="true">

</jsp:include>

<BODY>
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.ulink.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<DIV class="browserDefault" id="div1">


<%
String keyword = request.getParameter("keyword");
StudyDao studyDao = studyB.getStudyValues(keyword);

ArrayList studyIds 		= studyDao.getStudyIds();
ArrayList studyTitles 		= studyDao.getStudyTitles();
ArrayList studyObjectives 	= studyDao.getStudyObjectives();
ArrayList studySummarys 	= studyDao.getStudySummarys();
ArrayList studyDurs 		= studyDao.getStudyDurations();
ArrayList studyDurUnits 	= studyDao.getStudyDurationUnits();
ArrayList studyEstBgnDts 	= studyDao.getStudyEstBeginDates();
ArrayList studyActBghDts 	= studyDao.getStudyActBeginDates();
ArrayList studyParentIds 	= studyDao.getStudyParents();
ArrayList studyAuthors 		= studyDao.getStudyAuthors();
ArrayList studyNumbers 		= studyDao.getStudyNumbers();

int studyId = 0;
String studyTitle = "";
String studyNumber = "";

int len = studyIds.size();
int counter = 0;

%>



<Form name="studybrowser" method="post" action="" onsubmit="">

<table width="100%" cellspacing="0" cellpadding="0" border=0 >
 <tr >
	<td width = "100%">
		<P class = "defComments">
			<% if(len <= 0) {
			%>
			<%=MC.M_NoStdPublicView_RegSignUp%><%--<B>None of the studies </B>in this category are available for public viewing at the moment. You may register and sign up for the Notification service to receive email announcements of studies released for public viewing in the future*****--%>
			<BR>
			<BR>
			<A HREF=""><%=MC.M_LnkTo_SignUpNotifi%><%--Give the link to sign up for notificator here*****--%> </A>
			<% } else { Object[] arguments = {keyword}; 
			%>
			 <%=VelosResourceBundle.getMessageString("M_YourSrch_LikeYieldStd",arguments)%> <%--Your search criteria ({0}) yielded the following Studies*****--%>
			<% }
			%>
		</P>

	</td>

 </tr>
 <tr>
	<td height="10"></td>
	<td height="10"></td>
 </tr>
</table>

<% if (len > 0) {
%>

<table width="98%" >
 <tr>
   <th width="20%">
	<%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>
   </th>
   <th width="80%">
	<%=LC.L_Study_Title%><%--<%=LC.Std_Study%> Title*****--%>
   </th>

 </tr>

 <%
    for(counter = 0;counter<len;counter++)
	{	studyId = EJBUtil.stringToNum(((studyIds.get(counter)) == null)?"-":(studyIds.get(counter)).toString());

		studyTitle = ((studyTitles.get(counter)) == null)?"-":(studyTitles.get(counter)).toString();

		studyNumber = ((studyNumbers.get(counter)) == null)?"-":(studyNumbers.get(counter)).toString();

		if ((counter%2)==0) {
  %>

		<tr class="browserEvenRow">
  <%
		}
		else{
  %>

			<tr class="browserOddRow">
  <%
		}
  %>
		<td>
		  <%=studyNumber%>
		</td>
		<td>
		  <%=studyTitle%>
		</td>
		</tr>

<%
		}
	} //end of if body for len>0
%>

</table>
</Form>


<div>
<jsp:include page="bottompanel.jsp" flush="true">
</jsp:include>
</div>

</div>

<DIV class="mainmenu" id = "emenu">

</DIV>

</body>
</html>

