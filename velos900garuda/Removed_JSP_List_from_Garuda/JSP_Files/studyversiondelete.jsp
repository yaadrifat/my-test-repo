<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>

<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.StringUtil"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=LC.L_Std_VersionDel%><%--<%=LC.L_Study%> Version Delete*****--%></title>
<% } %>



<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<% String src;
src= request.getParameter("srcmenu");


			//JM:
			String from = request.getParameter("from");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%
	String studyVerId= "";
	String selectedTab="";


HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))	{
		studyVerId= request.getParameter("studyVerId");
		selectedTab=request.getParameter("selectedTab");

		int ret=0;
		String delMode=request.getParameter("delMode");
		if (delMode.equals("null")) {
			delMode="final";

%>
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>

  </jsp:include>

	<FORM name="studyverdelete" id="stdverdelfrm" method="post" action="studyversiondelete.jsp" onSubmit="if (validate(document.studyverdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>



	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="stdverdelfrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="studyVerId" value="<%=studyVerId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
			// Modified for INF-18183 ::: AGodara 
			ret = studyVerB.removeStudyVer(StringUtil.stringToNum(studyVerId),AuditUtils.createArgs(tSession,"",LC.L_Study));    //Akshi: Fixed for Bug #7604
			if (ret==-1) {%>
			<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_NotRemSucc%><%--Data not removed successfully*****--%> </p>
			<%}else { %>
			<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_RemSucc%><%--Data removed successfully*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyVerBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">
			<%}
			} //end esign
	} //end of delMode
  }//end of if body for session
else { %>
 <jsp:include page="timeout.html" flush="true"/>
 <% } %>

 <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>
</HTML>


