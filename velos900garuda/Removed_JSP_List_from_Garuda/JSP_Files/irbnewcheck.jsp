<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%
boolean isIrb = "irb_check_tab".equals(request.getParameter("selectedTab")) ? true : false; 
if (isIrb) {
%>
<title><%=MC.M_ResCompApp_ChkSub%><%--Research Compliance >> New Application >> Check and Submit*****--%></title>
<% } else { %>
<title><%=MC.M_Std_CkhSubmit%><%--<%=LC.Std_Study%> >> Check and Submit*****--%></title>
<% } %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%@ page import="com.velos.esch.business.common.SchMsgTxtDao" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
	
<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
ArrayList boardNameList = null;
ArrayList boardIdList = null;
ArrayList statusList = null;
ArrayList statusDateList = null;
EIRBDao eIrbDao = new EIRBDao();
if (sessionmaint.isValidSession(tSession))
{
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    grpId = (String) tSession.getValue("defUserGroup");
    usrId = (String) tSession.getValue("userId");
	
	if (StringUtil.isEmpty(usrId))
	{
		usrId="";
	}

	if("M".equals(request.getParameter("mode")) 
	        && request.getParameter("studyId") != null) {
        studyId = StringUtil.htmlEncodeXss(request.getParameter("studyId"));
	}
    if(accountId == null || accountId == "") {
    %>
		<jsp:include page="timeout.html" flush="true"/>
	<%
        return;
    }
    eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(grpId));
    boardNameList = eIrbDao.getBoardNameList();
    boardIdList = eIrbDao.getBoardIdList();
    
    eIrbDao.getCurrentStatuses(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(studyId),
            EJBUtil.stringToNum(grpId), EJBUtil.stringToNum(usrId), 0);
    statusList = eIrbDao.getCurrentStatuses();
    statusDateList = eIrbDao.getCurrentStatusDates();

}
%>
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT language="javascript">
function enableSubmitButton(boardIndexStr) {
	var allOff = true;
	for (var iX=0; iX < <%=boardNameList.size()%>; iX++) {
		var elem = document.getElementById("submitTo"+iX);
		if (elem != null && elem.checked) {
			allOff = false;
		}
	}
	if (allOff) {
		document.mainForm.action="";
	}
}

function submitForm(formobj) {
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
		
	var allOff = true;
	for (var iX=0; iX < <%=boardNameList.size()%>; iX++) {
		var elem = document.getElementById("submitTo"+iX);
		if (elem != null && elem.checked) {
			allOff = false;
		}
	}
	if (allOff) {
		alert("<%=MC.M_AtLeast_OneCheckbox%>");/*alert("Please check at least one checkbox.");*****/
		return false;
	}
	
	formobj.action="updateNewSubmission.jsp?&srcmenu=<%=request.getParameter("srcmenu")%>&selectedTab=irb_check_tab"; 
	return true;
}

function callAjaxForEachBoard(study_id, board_id, board_name,
		board_index, board_total, status, status_date) {
	new VELOS.ajaxObject("irbnewcheckajax.jsp", {
		urlData:"studyId="+study_id+"&boardId="+board_id+"&boardName="+board_name+
		    "&boardIndex="+board_index+"&totalBoardCount="+board_total+
		    "&status="+status+"&statusDate="+status_date,
		reqType:"POST",
		outputElement: "span_board"+board_index } 
	).startRequest();
}

function callAjax() {
	<% for (int iY=0; iY<boardIdList.size(); iY++) { %>
		callAjaxForEachBoard("<%=studyId%>", "<%=boardIdList.get(iY)%>",
				"<%=boardNameList.get(iY)%>", "<%=iY%>", "<%=boardNameList.size()%>",
				"<%=statusList.get(iY)%>", "<%=statusDateList.get(iY)%>");
	<% } %>
}

</SCRIPT> 

<body>

<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
	  
	String studyIdForTabs = ""; 
 	studyIdForTabs = request.getParameter("studyId");
 	
    String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
%>
<DIV class="BrowserTopn" id = "divtab">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
				<jsp:param name="from" value="<%=from%>"/> 
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>
</DIV>

<%

%>

<DIV class="BrowserBotN BrowserBotN_S_1" id = "div1">
	<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
	<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
	<%
	
	if (sessionmaint.isValidSession(tSession))
	{
		String studyPrinv ="";
		
		int pageRight = 0;
        if(studyId == "" || studyId == null) {
	    %>
	    <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	    <%
	    } else {
   	
   					studyB.setId(EJBUtil.stringToNum(studyId));
					studyB.getStudyDetails();
					
					studyPrinv = studyB.getStudyPrimInv();
					
					if(StringUtil.isEmpty(studyPrinv))
					{ 
					 	studyPrinv="-1";
					} 
				 

        %>
        <form name="mainForm" id="irbcheckid" method="post" onsubmit="if (submitForm(this) == false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
        <input type="hidden" name="totalBoardCount" value="<%=boardIdList.size()%>">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr><td align="center"><br /></td></tr>
        <tr><td align="center"><button name="checkSubmission" onclick="callAjax()" type="button"><%=LC.L_Check_Submission%></button></td></tr>
        <tr><td align="center"><br /></td></tr>
        </table>
	    <table class="tableDefault" width="100%" cellspacing="0" cellpadding="0" border="0">
        <%  for(int iBoard=0; iBoard<boardNameList.size(); iBoard++) { %>
        <tr><td width="100%">
        <span id="span_board<%=iBoard%>">
        <table class="tableDefault" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr class="browserEvenRow"><td colspan="5">&nbsp;</td></tr>
        <tr class="browserOddRow">
            <td width="20%"><b><%=boardNameList.get(iBoard)%></b></td>
            <td width="32%">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            <td width="29%" align="center">&nbsp;</td>
            <td width="4%" align="center"><input type="checkbox" disabled></td>
        </tr>
        </table>
        </span>
        </td><tr>
        <%  }  %>
	    </table>
        <br />
        <%
        SchMsgTxtDao schMsgTxtDao = new SchMsgTxtDao();
        String piCert = schMsgTxtDao.getMsgTxtLongByType("irb_picert", EJBUtil.stringToNum(accountId));
        if (piCert != null && piCert.length() > 0) {
            out.println(piCert);
        }
        
        if (studyPrinv.equals(usrId))
        {
        %>
	       <jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="irbcheckid"/>
					<jsp:param name="showDiscard" value="N"/>
	        </jsp:include>
	       <%
	    } else
	    {%>
	    	<p class="defcomments" align="center"><%=MC.M_OnlyPrplInvesti_AcptCert%><%--Only the Principal Investigator can accept the certification and submit the Application*****--%> </p>			
	    		
	    <% }%> 
	        
	        </form>
        <%
        if (request.getParameter("autoPopulate") != null) {
        %>
        <script>
        callAjax();
        </script>
        <%                
        }
        %>

<%
		}// end of else of study null check
	}//end of if body for session
	else
	{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>
</body>

</html>

