<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=MC.M_PorAdm_PorHist%><%--Portal Admin >> Portal History*****--%></title>
	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<% 
	String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<SCRIPT language="javascript"></SCRIPT> 
<body>
<br>
<%
	HttpSession tSession = request.getSession(true); 
	String tab = request.getParameter("selectedTab");
	String studyId = (String) tSession.getValue("studyId");
	String from = request.getParameter("from");
	String currentStatId = request.getParameter("currentStatId");
%>

<DIV class="tabDefTopN" id="divTab"> 
        
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="from" value="<%=from%>"/>
	<jsp:param name="selectedTab" value="<%=tab%>"/>
	<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%
	if (sessionmaint.isValidSession(tSession)){
		
		String modulePk = request.getParameter("modulePk");
		String fromjsp = request.getParameter("fromjsp");
		String portalName = request.getParameter("portalName");
		portalName=StringUtil.decodeString(portalName);
		String pageRight = "";
		pageRight = request.getParameter("pageRight");
                if(pageRight == null) 
                   pageRight ="0";
 	        if ( EJBUtil.stringToNum(pageRight) >= 0 ){

	
%>
</DIV>			
<DIV class="tabDefBotN" id="div1"> 

			<jsp:include page="statusHistory.jsp" flush="true">
				<jsp:param name="modulePk" value="<%=modulePk%>"/>
				<jsp:param name="moduleTable" value="er_portal"/>
				<jsp:param name="srcmenu" value="<%=src%>"/>
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="fromjsp" value="<%=fromjsp%>"/>
				<jsp:param name="pageRight" value="<%=pageRight%>"/>
				<jsp:param name="portalName" value="<%=StringUtil.encodeString(portalName)%>"/>
				<jsp:param name="currentStatId" value="<%=currentStatId%>"/>
				<jsp:param name="showDeleteLink" value="false"/>
				<jsp:param name="showEditLink" value="false"/>
				
			</jsp:include>			
<%		} //end of if body for page right
		else {
%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
<%
		}//end of else body for page right
	}//end of if body for session
		else{
%>
			<jsp:include page="timeout.html" flush="true"/>

<%	}	
%>
	<div> 
    	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
	<div class ="mainMenu" id = "emenu" > 
		<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
	</div>
</body>

</html>

