<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.storage.*,com.velos.eres.web.storageStatus.*,com.velos.eres.business.common.*,com.velos.eres.web.storageAllowedItems.*, com.velos.eres.web.storageKit.StorageKitJB, com.velos.eres.web.storage.StorageJB, com.velos.eres.web.storageStatus.StorageStatusJB"%>
  <jsp:useBean id="storageB" scope="page" class="com.velos.eres.web.storage.StorageJB"/>
  <jsp:useBean id="storageStatB" scope="page" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
  <jsp:useBean id="storageKitB" scope="page" class="com.velos.eres.web.storageKit.StorageKitJB"/>


  <%

	 String src = null;
     src = request.getParameter("srcmenu");
	 String eSign = request.getParameter("eSign");
	 String mode = request.getParameter("mode");
	 String pkStorage = request.getParameter("pkStorage");

	 String notes = request.getParameter("notes");
	 notes	 = (notes  == null)?"":(notes);

     String pkStorageKit = request.getParameter("pkStorageKit");


	 String stdy = request.getParameter("selStudyIds");
	 stdy = (stdy==null)?"":stdy;

	 String user = request.getParameter("creatorId");
     user = (user==null)?"":user;

	 String dummystatDate = "";
	 Date dt = new java.util.Date();
	 dummystatDate = DateUtil.dateToString(dt);


	 String id_Storages[] = null;
	 String id_Kits[] = null;

	 String kit_ComonentNames[] = null;
	 String kit_storageTypes[] = null;
	 String kit_specimenTypes[] = null;
	 String kit_quantities[] = null;
	 String kit_specimenqunits[] = null;

	 String kit_processingsteps[] = null;
	 String kit_sequences[] = null;

	 String kit_dispositions[] = null;
	 String kit_envtConstrs[] = null;
	 String totalLoops = "";

	 String kit_specChildrens[] = null;
	// String kit_checkCopyAttToChilds[] = null;
	 String num_indivComboOrSeqs[] = null;


	 String storage_Id = "";
	 String kit_Id = "";

	 String kit_ComonentName = "";
	 String kit_storageType = "";
	 String kit_specimenType = "";
	 String kit_quantity = "";
	 String kit_specimenqunit = "";
	 String kit_disposition = "";
	 String kit_envtConstr  = "";
	 int    num_indivComboOrSeq  = 0;
	 String kit_specChildren  = "";
	// String kit_checkCopyAttToChild  = "";

	 String id_storageKit="";
	 String category_sk="";
	 String name_sk="";	 
	 String pkStorageKitChldLevel1 ="";
	 String pkStorageKitChldLevel2 ="";
	 
	 String childStorageSk="";


	 int startFromCnt=0;
	 int len=0, len1=0;
	 int old_numIndvSeq = 0;

	 int retParKit=0;

	 int retChld1_Str=0, retChld2_Str=0, retChld1_StrKit=0, retChld2_StrKit=0;

	 CodeDao cd = new CodeDao();
	 int defaultItem = cd.getCodeId("store_type","Kit");

	 CodeDao cd1 = new CodeDao();
	 int fkCodelstSpecimenStat = cd1.getCodeId("storage_stat", "Available");


	 id_storageKit = request.getParameter("storageKitId");
	 category_sk = request.getParameter("storecate");
	 childStorageSk = request.getParameter("storeChildType");
	 name_sk = request.getParameter("storageKitName");

	 id_Storages = request.getParameterValues("idsStOrages");
	 id_Kits = request.getParameterValues("idsKits");



	 kit_ComonentNames = request.getParameterValues("kitCompName");
	 kit_storageTypes = request.getParameterValues("storageType");
	 kit_specimenTypes = request.getParameterValues("specimentype");
	 kit_quantities = request.getParameterValues("quantity");
	 kit_specimenqunits = request.getParameterValues("specimenqunit");

	 kit_processingsteps = request.getParameterValues("processingsteps");
	 kit_sequences = request.getParameterValues("sequence");

	 kit_dispositions = request.getParameterValues("disposition");
	 kit_envtConstrs = request.getParameterValues("envtStringChecked");

	 totalLoops = request.getParameter("lengthOfLoop");
	 String rows_existing = request.getParameter("totalExtgRows");
	 String old_rows_existing = request.getParameter("oldExtgRows");
	 old_rows_existing = (old_rows_existing==null || old_rows_existing.equals(""))?"-1":old_rows_existing;

	 //KM-#5698
	 if(kit_ComonentNames!=null && kit_ComonentNames.length >0){ //AK: Fixed BUG#6266 (May 24,2011)
	 rows_existing = kit_ComonentNames.length + "";
	 }
	 else{
		 rows_existing="0"; //AK: Fixed BUG#6266 (May 24,2011)
	 }


	 num_indivComboOrSeqs = request.getParameterValues("numDropDownPerRow");

	 kit_specChildrens = request.getParameterValues("numSpecChildren");	


	if(mode.equals("M")) {
		storageB.setPkStorage(EJBUtil.stringToNum(pkStorageKit));
		storageB.getStorageDetails();

	}


String modeManipVar="";

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession)){
%>
    	<jsp:include page="sessionlogging.jsp" flush="true"/>

<%

   		String oldESign = (String) tSession.getValue("eSign");

		if(!oldESign.equals(eSign)){

	%>
			<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
		}else{

			String ipAdd = (String) tSession.getValue("ipAdd");
			String accId = (String) tSession.getValue("accountId");
	    	String usr = null;
			usr = (String) tSession.getValue("userId");
			int ret=0;
			int sret=0;
			int cret=0;


			//mode="N";
			if ((mode.equals("N") || mode.equals("M")) && id_storageKit.equals("")) {
			id_storageKit = storageB.getStorageIdAuto();
			id_storageKit=(id_storageKit==null)?"":id_storageKit;

			}
			else{
			int count=0;
			count = storageB.getCountId(id_storageKit,accId);
			if(count ==0)
				storageB.getStorageIdAuto();
			}






/**
*JM: 18Aug2009: adding the storage kit and its components details
*
********************------------Parent Kit/Storage Kit Level-0-------------------******************************
**/


			if (!name_sk.trim().equals("")){

				storageB.setStorageId(id_storageKit);
				storageB.setStorageName(name_sk);
				storageB.setStorageKitCategory(category_sk);
				storageB.setFkCodelstStorageType(defaultItem+"");
				storageB.setAccountId(accId);
				storageB.setFkCodelstChildStype(childStorageSk);
				storageB.setFkStorage("");	//for parent storage kit alone #b
				storageB.setStorageNotes(notes);	//applicable for the parent kit alone			
				storageB.setTemplate("1");
				storageB.setIpAdd(ipAdd);

				if(mode.equals("N")) {
					storageB.setCreator(usr);
					retParKit=storageB.setStorageDetails();
					pkStorageKit = String.valueOf(retParKit);

		        }else{
		        	storageB.setModifiedBy(usr);
					retParKit=storageB.updateStorage();
				}


//status for this storage to go ...
				if (retParKit>0){
					storageStatB.setFkStorage(retParKit+"");
					storageStatB.setSsStartDate(dummystatDate);
					storageStatB.setFkCodelstStorageStat(fkCodelstSpecimenStat+"");
					storageStatB.setSsNotes("");
					storageStatB.setFkUser("");
					storageStatB.setFkStudy("");
					storageStatB.setIpAdd(ipAdd);

					if(mode.equals("N")) {
						storageStatB.setCreator(usr);
						sret=storageStatB.setStorageStatusDetails();
					}else{
						storageStatB.setModifiedBy(usr);
						sret=storageStatB.updateStorageStatus();
					}
				}
			}//end if (!name_sk.equals("")){
/*****************************************************************************************************************/
/********************---------------Child Kit/Child Storage Kit Level-1-------------------*******************************/
/*****************************************************************************************************************/


	 for (int n=0 ; n < EJBUtil.stringToNum(rows_existing) ; n ++){


		if(mode.equals("M") && n<EJBUtil.stringToNum(old_rows_existing)) {

		 storage_Id = id_Storages[n];
		 storage_Id = (storage_Id==null)?"":storage_Id;

		 kit_Id = id_Kits[n];
		 kit_Id = (kit_Id==null)?"":kit_Id;

		}//else {



		//if (EJBUtil.stringToNum(rows_existing) - EJBUtil.stringToNum(old_rows_existing)>0){
		if ( n == EJBUtil.stringToNum(old_rows_existing) && mode.equals("M")){
			modeManipVar="MN";

			mode="N";


		}



		kit_ComonentName = kit_ComonentNames[n] ;

		kit_ComonentName=(kit_ComonentName==null)?"":kit_ComonentName;
		kit_storageType = kit_storageTypes[n];
		if (kit_storageType.equals("") || kit_storageType==null){
			kit_storageType= "0";

		}


		kit_specimenType = kit_specimenTypes[n];
		if (kit_specimenType.equals("") || kit_specimenType==null){
			kit_specimenType= "0";
		}



		kit_quantity = kit_quantities[n];
		if (kit_quantity.equals("") || kit_quantity==null){
			kit_quantity= "0";
		}

		kit_specimenqunit = kit_specimenqunits[n];
		if (kit_specimenqunit.equals("") || kit_specimenqunit==null){
			kit_specimenqunit= "0";
		}

		num_indivComboOrSeq = EJBUtil.stringToNum(num_indivComboOrSeqs[n]);

		String procStepStr = "";
		String sequenceStr = "";
		String indReached = "0";

		old_numIndvSeq = old_numIndvSeq + num_indivComboOrSeq;
		for (int x=startFromCnt; x < kit_processingsteps.length ; x++){

			//When there is no processing step selected under for a component row in A/E mode
			if (kit_processingsteps[x].equals("")){
				kit_processingsteps[x]="0";
			}
			if (kit_sequences[x].equals("")){
				kit_sequences[x]="0";
			}

			procStepStr = procStepStr + kit_processingsteps[x]+",";
			sequenceStr = sequenceStr + kit_sequences[x]+",";

			if (old_numIndvSeq == (x+1))
			indReached = "1";

			if (indReached.equals("1"))
			break;

		}
		startFromCnt = old_numIndvSeq;

		len = procStepStr.length();
		procStepStr = procStepStr.substring(0, len-1);//remove the ',' from the end


		//procStepStr=(procStepStr==null)?"0,0":procStepStr;
		if (procStepStr.equals("") || procStepStr==null){
			procStepStr= "0,0";
		}
		len1 = sequenceStr.length();

		sequenceStr = sequenceStr.substring(0, len1-1);//remove the ',' from the end
		//sequenceStr=(sequenceStr==null)?"0,0":sequenceStr;
		if (sequenceStr.equals("") || sequenceStr==null){
			sequenceStr= "0,0";
		}


		kit_disposition = kit_dispositions[n];
		//kit_disposition=(kit_disposition==null)?"0":kit_disposition;
		if (kit_disposition.equals("") || kit_disposition==null){
			kit_disposition= "0";
		}

		kit_envtConstr = kit_envtConstrs[n];
		//kit_envtConstr=(kit_envtConstr==null)?"0":kit_envtConstr;
		if (kit_envtConstr.equals("") || kit_envtConstr==null){
			kit_envtConstr= "000";
		}


		if (!kit_ComonentName.trim().equals("") && retParKit>=0){//its like storage name *

//Child level-1s' storage related attributes go here...

		StorageJB strChildLevel_1B = new StorageJB();

		if(mode.equals("M")) {
			strChildLevel_1B.setPkStorage(EJBUtil.stringToNum(storage_Id));
			strChildLevel_1B.getStorageDetails();
		}

		String StorageIdForChild = storageB.getStorageIdAuto();
		StorageIdForChild=(StorageIdForChild==null)?"":StorageIdForChild;


		strChildLevel_1B.setStorageId(StorageIdForChild);
		strChildLevel_1B.setStorageName(kit_ComonentName);
		
//JM: 17Sep2009, #4310
		strChildLevel_1B.setFkCodelstStorageType(kit_storageType);		

		strChildLevel_1B.setAccountId(accId);
		if (mode.equals("N")|| modeManipVar.equals("MN")){
		 strChildLevel_1B.setFkStorage(pkStorageKit);//who is the parent one
		}
		strChildLevel_1B.setTemplate("1");//???
		strChildLevel_1B.setIpAdd(ipAdd);
		if(mode.equals("N")|| modeManipVar.equals("MN")) {
			strChildLevel_1B.setCreator(usr);
			retChld1_Str=strChildLevel_1B.setStorageDetails();
		}else{
			strChildLevel_1B.setModifiedBy(usr);
			retChld1_Str=strChildLevel_1B.updateStorage();

	    }

//Child level-1s' storage kit related attributes go here...

	  	if(retChld1_Str>=0) {
		  	StorageKitJB strKitB = new StorageKitJB();

		  	if(mode.equals("M")) {
			 strKitB.setPkStorageKit(EJBUtil.stringToNum(kit_Id));
			 strKitB.getStorageKitDetails();
			}

			if(mode.equals("N")|| modeManipVar.equals("MN")) {
			 strKitB.setFkStorage(retChld1_Str+"");
			}
			strKitB.setDefSpecimenType(kit_specimenType);
			strKitB.setDefProcType(procStepStr);
			strKitB.setDefProcSeq(sequenceStr);
			strKitB.setDefSpecQuant(kit_quantity);
			strKitB.setDefSpecQuantUnit(kit_specimenqunit);
			strKitB.setKitSpecDisposition(kit_disposition);
			strKitB.setKitSpecEnvtCons(kit_envtConstr);
			strKitB.setIpAdd(ipAdd);

			if(mode.equals("N")|| modeManipVar.equals("MN")) {
					strKitB.setCreator(usr);
					retChld1_StrKit=strKitB.setStorageKitDetails();
		    }else{
		    		strKitB.setModifiedBy(usr);
					retChld1_StrKit=strKitB.updateStorageKit();
		    }

		}


//status for the level-1
		if(retChld1_StrKit>=0 && !mode.equals("M")){

	    	StorageStatusJB innerChld2StrStatB = new StorageStatusJB();

	    	innerChld2StrStatB.setFkStorage(retChld1_Str+"");
			innerChld2StrStatB.setSsStartDate(dummystatDate);
			innerChld2StrStatB.setFkCodelstStorageStat(fkCodelstSpecimenStat+"");
			innerChld2StrStatB.setSsNotes("");
			innerChld2StrStatB.setFkUser("");
			innerChld2StrStatB.setFkStudy("");
			innerChld2StrStatB.setIpAdd(ipAdd);

			if(mode.equals("N")|| modeManipVar.equals("MN")) {
				innerChld2StrStatB.setCreator(usr);
				sret=innerChld2StrStatB.setStorageStatusDetails();
			}
			if(mode.equals("M")) {
				innerChld2StrStatB.setModifiedBy(usr);
				sret=innerChld2StrStatB.updateStorageStatus();
			}
		}

 }//end of if (!kit_ComonentName.equals("")){




		if(mode.equals("N") && modeManipVar.equals("MN")) {

		kit_specChildren = kit_specChildrens[n];// how many children		
		}



		if (mode.equals("N")&& !modeManipVar.equals("MN")){
			kit_specChildren = kit_specChildrens[n];// how many children			
		}


/*****************************************************************************************************************/
/********************--------------Child Kit/Child Storage Kit Level-2-------------*******************************/
/*****************************************************************************************************************/
String locString = "";


	  if (!kit_specChildren.equals("") && (mode.equals("N")||modeManipVar.equals("MN"))){
	  for ( int w = 0 ; w < EJBUtil.stringToNum(kit_specChildren); w++){


			//if (kit_checkCopyAttToChild.equals("0") || kit_checkCopyAttToChild.equals("")){


				locString = kit_ComonentName + "-CL2-"+ (w+1);//Storage Kit Child Level-2 naming convention

				category_sk = "";
				//BK,#6030,May-09-2011
				//kit_specimenType = "0";
				procStepStr="0,0";
				sequenceStr="1,2";
				double tempQuantity = (double)EJBUtil.stringToNum(kit_quantities[n])/EJBUtil.stringToNum(kit_specChildren);				
				java.text.DecimalFormat df = new java.text.DecimalFormat("0.00");
				kit_quantity = df.format(tempQuantity);
				kit_specimenqunit=kit_specimenqunits[n];
				kit_disposition="0";
				kit_envtConstr="000";

			//}else{
				//as follows, in case of copy attributes........
			//}




//Child level-2s' storage related attributes go here...

			StorageJB strChildLevel_2B = new StorageJB();

			String StorageIdForChild2 = storageB.getStorageIdAuto();
			StorageIdForChild2=(StorageIdForChild2==null)?"":StorageIdForChild2;

			strChildLevel_2B.setStorageId(StorageIdForChild2);

			//if (kit_checkCopyAttToChild.equals("0")|| kit_checkCopyAttToChild.equals("")){
				strChildLevel_2B.setStorageName(locString);
			//}
			//else{
			//	strChildLevel_2B.setStorageName(kit_ComonentName);
			//}

			strChildLevel_2B.setStorageKitCategory(category_sk);


			strChildLevel_2B.setFkCodelstStorageType(kit_storageType);

			strChildLevel_2B.setAccountId(accId);
			strChildLevel_2B.setFkStorage(retChld1_Str+"");//the parent to store
			strChildLevel_2B.setTemplate("1");
			strChildLevel_2B.setIpAdd(ipAdd);

			if(mode.equals("N") ||modeManipVar.equals("MN")) {
				strChildLevel_2B.setCreator(usr);
				retChld2_Str=strChildLevel_2B.setStorageDetails();
				pkStorageKitChldLevel2 = String.valueOf(retChld2_Str);
		    }else{//mode m not needed here
		    	strChildLevel_2B.setModifiedBy(usr);
				retChld2_Str=strChildLevel_2B.updateStorage();

		    }

//Child level-2s' storage kit related attributes go here...////////////
			StorageKitJB strKitChild2B = new StorageKitJB();

			strKitChild2B.setFkStorage(pkStorageKitChldLevel2);
			strKitChild2B.setDefSpecimenType(kit_specimenType);
			strKitChild2B.setDefProcType(procStepStr);
			strKitChild2B.setDefProcSeq(sequenceStr);
			strKitChild2B.setDefSpecQuant(kit_quantity);
			strKitChild2B.setDefSpecQuantUnit(kit_specimenqunit);
			strKitChild2B.setKitSpecDisposition(kit_disposition);
			strKitChild2B.setKitSpecEnvtCons(kit_envtConstr);
			strKitChild2B.setIpAdd(ipAdd);

			if(mode.equals("N") ||modeManipVar.equals("MN")) {
				strKitChild2B.setCreator(usr);
				retChld2_StrKit=strKitChild2B.setStorageKitDetails();
		    }else{//mode m not needed here
		    	strKitChild2B.setModifiedBy(usr);
				retChld2_StrKit=strKitChild2B.updateStorageKit();
		    }



//status
		if(retChld2_Str>=0 && (!mode.equals("M")||modeManipVar.equals("MN"))){
			StorageStatusJB innerStrStatB = new StorageStatusJB();

	    	innerStrStatB.setFkStorage(pkStorageKitChldLevel2);
			innerStrStatB.setSsStartDate(dummystatDate);
			innerStrStatB.setFkCodelstStorageStat(fkCodelstSpecimenStat+"");
			innerStrStatB.setSsNotes("");
			innerStrStatB.setFkUser("");
			innerStrStatB.setFkStudy("");
			innerStrStatB.setIpAdd(ipAdd);

			if(mode.equals("N")||modeManipVar.equals("MN")) {
				innerStrStatB.setCreator(usr);
				sret=innerStrStatB.setStorageStatusDetails();
			}else{//mode m not needed here
				innerStrStatB.setModifiedBy(usr);
				sret=innerStrStatB.updateStorageStatus();
			}
		}

	  }//end of loop w

	}//end of check for null
/********************--------------Child Kit/Child Storage Kit Level-2----------------*******************************/

	 }//end of loop for nummber of rows existing...




		  if (retParKit >=0){
		  %>
	      <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
		  <META HTTP-EQUIV=Refresh CONTENT="1; URL=editstoragekit.jsp?srcmenu=tdmenubaritem6&selectedTab=3&pkstoragekit=<%=pkStorageKit%>&mode=M">
		  <%
		  } else if(retParKit == -2) {%>
		  <p class = "successfulmsg" align = center> <%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully*****--%> </p>
		  <META HTTP-EQUIV=Refresh CONTENT="1; URL=storageadminbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=2&searchFrom=search">
		  <%} else if(retParKit == -3) {%>
		  <table align="center">
		  <tr>
		  <td align ="center">
	  	  <p class = "successfulmsg" align = center> <%=MC.M_StrKitName_AldyExst%><%--The Storage Kit Name you have entered already exists. Please click on Back button and enter it again*****--%> </p>
		  </td>
		  </tr>
		  <tr height=20></tr>
		  <tr>
		  <td align="center">
		  <button onclick="history.go(-1);"><%=LC.L_Back%></button>
		  </td>
		  <tr>
		  </table>
	      <%} else if(retParKit == -4){%>
		  <table align="center">
		  <tr height=20></tr>
		  <tr>

		  <td align ="center">
	  	  <p class = "successfulmsg" align = center> <%=MC.M_StrKitId_AldyExst%><%--The Storage Kit ID you have entered already exists. Please click on Back button and enter it again.*****--%> </p>
		  </td>
		  </tr>
		  <tr height=20></tr>
		  <tr>
		  <td align="center">
		  <button name="back" onClick="window.history.back();"><%=LC.L_Back%></button>
		  </td>
		  <tr>
		  </table>
		  <%}

		}//end of if for eSign check

	}//end of if body for session

	else
	{
%>
  		<jsp:include page="timeout.html" flush="true"/>
<%
	}
%>

	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>



</BODY>

</HTML>
