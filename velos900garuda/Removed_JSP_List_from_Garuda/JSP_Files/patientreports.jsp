<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC. L_MngPat_Rpts%><%--Manage <%=LC.Pat_Patient%> >> Reports*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<SCRIPT language="Javascript">

function fOpenWindow(type,section,frmobj){
	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=500;
		height=120;
	}
	if (type == "M"){
		width=270;
		height=70;
	}
	frmobj.year.value ="";
	frmobj.year1.value ="";
	frmobj.month.value ="";
	frmobj.dateFrom.value ="";
	frmobj.dateTo.value ="";
	frmobj.range.value ="";
	if (type == 'A'){
			frmobj.range.value = "All"
	}
	 if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?from=patientreports&parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}
}


function fValidate(frmobj){
	reportChecked=false;
	for(i=0;i<frmobj.reportName.length;i++){
		sel = frmobj.reportName[i].checked;
		if (frmobj.reportName[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
		return false;
	}




	frmobj.action = "repRetrieve.jsp";

	reportNo = frmobj.repId.value;
	/* SV, 7/30/04, bug #1476, added date range check to 93, 94, 95, 96. */
	switch (reportNo) {

		case "10": //Patient Schedule
		case "11": //Patient Events Completed
		case "12": //Patient Events To Be Done
		case "65": //Patient Event/CRF Status
		case "66": //Patient CRF Tracking
		case "88": //Adverse Events by Patient
		case "93": // Patient Timeline Report
		case "94": //Study Visit Calendar
		case "96": // PSA/Treatment
					dateChecked=false;
					if (frmobj.range.value != ""){
		   				dateChecked=true;
					}
					if (dateChecked==false) {
						alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
						return false;
					}

		   			break;

		case "47": //Patient Associated Cost
			dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (frmobj.protId.value == "") {
				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/
				return false;
			}
			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}

			break;

		case "95": // Patient Visit Calendar
			if(frmobj.range.value == "All")
			{
				alert("<%=MC.M_DtFilter_NotAval%>");/*alert("The Date Filter 'All' is not available for this report. Please select an appropriate Date Range or select a Month/Year");*****/
				return false;
			}
			dateChecked =false;
			if (frmobj.range.value != ""){
   				dateChecked=true;
			}

			if (dateChecked==false) {
				alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
				return false;
			}

			break;


	}

	if (fnTrimSpaces(frmobj.studyPk.value) == "" || frmobj.studyPk.value == "0")
	{
		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
		return false;
	}


}

function fSetId(ddtype,frmobj){
	if (ddtype == "patient") {
		frmobj.id.value = "";
		frmobj.selPatient.value = "None";
		frmobj.val.value = "";
		openwindow();
	}

	if (ddtype == "studyPk")
	{
		frmobj.selProtocol.value = "None";
		frmobj.protId.value = "";

	}

	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		for(i=0;i<frmobj.reportName.length;i++)	{
			if (frmobj.reportName[i].checked){
				lsReport = frmobj.reportName[i].value;
				ind = lsReport.indexOf("%");
				frmobj.repId.value = lsReport.substring(0,ind);
				frmobj.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}

}

</SCRIPT>

<SCRIPT language="JavaScript1.1">
function openwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");
	windowName.focus();
}


function openProtWindow(param,frmobj) {
	lstudyPk = frmobj.studyPk.value;

	if (lstudyPk == "" || lstudyPk == "0") {
			alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/
			return false;
	}
	windowName=window.open("protocolPopup.jsp?studyPk="+lstudyPk+"&reportType="+param,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=350,height=250,top=200,left=300");
	windowName.focus();
}


</SCRIPT>

</head>


<% String src="";
src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<script language="JavaScript" src="overlib.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js" type="text/javascript"><!-- overLIB (c) Erik Bosrup --></script>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>



<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>

<body style="overflow: hidden;">

<%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))
   {
   			String evDesc = "";
  			int personPK = 0;
  			String patientId = "";
			String protocolId="";
			String startdt="";
			String status="";//G-Generate Schedule,S-Show Schedule also
			String yob = "";
			int siteId = 0;
			String schDate = "";
			String acDate = "";

			personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
			String pkey = request.getParameter("pkey");
	        person.setPersonPKId(personPK);
		 	person.getPersonDetails();
			patientId = person.getPersonPId();
			siteId = EJBUtil.stringToNum(person.getPersonLocation());

			String enrollId =(String) tSession.getValue("enrollId");
			String page1 = request.getParameter("page");


			patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
			patEnrollB.getPatProtDetails();
			startdt = patEnrollB.getPatProtStartDt();
			protocolId =patEnrollB.getPatProtProtocolId();

	//String study = (String) tSession.getValue("studyId");
	String study = request.getParameter("studyId");

	/*String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(study));

    studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber(); */



	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	} else {
		protocolId = "";
	}

	String uName =(String) tSession.getValue("userName");
	String userId = (String) tSession.getValue("userId");
	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String tab = request.getParameter("selectedTab");

	Calendar cal = Calendar.getInstance();
	int currYear = cal.get(cal.YEAR);

	String filterText="";

	int counter=0;
	int studyId=0;
	// Not used in the page 14/oct/03
	/*StudyDao studyDao = new StudyDao();
	//studyDao.getReportStudyValuesForUsers(userId);*/
	String ver = "";

	CtrlDao ctrl1 = new CtrlDao();
	ctrl1.getControlValues("patspecific");
	String catId = (String) ctrl1.getCValue().get(0);
	repdao1.getAllRep(EJBUtil.stringToNum(catId),acc); //Patient Specific Reports
	ArrayList repIdsOnePatients= repdao1.getPkReport();
	ArrayList namesOnePatients= repdao1.getRepName();
   	ArrayList descOnePatients= repdao1.getRepDesc();
	ArrayList repFilterOnePatients = repdao1.getRepFilters();

	int repIdOnePatient=0;
	String nameOnePatient="";

	StringBuffer study1=new StringBuffer();

	int lenOnePatient = repIdsOnePatients.size() ;

	PatStudyStatDao pstudies = new PatStudyStatDao();
	pstudies = studyB.getPatientStudies(EJBUtil.stringToNum(pkey), EJBUtil.stringToNum(userId));

	ArrayList studyNums = pstudies.getStudyNums();
    ArrayList studyIds = pstudies.getStudyIds();
    String dPatStudy = "";

    int length = pstudies.getCRows();
		if (length == 0)
		{
			//patient is not enrolled to any protocol

			dPatStudy = "<P class=\"defComments\">"+MC.M_PatNotAssoc_StdProc/*This "+LC.Pat_Patient_Lower+" has not been associated to any "+LC.Std_Study_Lower+" protocol*****/+"</P> <Input type=hidden name=\"studyPk\" value=0>";

		}
		else
		{
				study1.append("<SELECT NAME=studyPk onChange=fSetId('studyPk',document.reports)>") ;

				for (int j = 0; j <=  studyIds.size() -1 ; j++)
				{
					study1.append("<OPTION value = "+ studyIds.get(j) +">" +  studyNums.get(j)+"</OPTION>");
				}
				study1.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
				study1.append("</SELECT>");
				//dPatStudy = EJBUtil.createPullDown("studyPk", 0 ,studyIds,studyNums);

				dPatStudy = study1.toString();

		}





%>
<DIV class="BrowserTopn" id="div1">

<jsp:include page="patienttabs.jsp" flush="true">
<jsp:param name="pkey" value="<%=pkey%>"/>
<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientId)%>"/>
<jsp:param name="patProtId" value="<%=enrollId%>"/>
<jsp:param name="studyId" value="<%=study%>"/>
<jsp:param name="page" value="<%=page1%>"/>
</jsp:include>

</div>
<br>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<DIV class="BrowserBotN BrowserBotN_top1" id="div2">

	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>

<!-- include report central-->
<jsp:include page="reportcentral.jsp" flush="true">
<jsp:param name="srcmenu" value="<%=src%>"/>
<jsp:param name="repcat" value="<%=LC.L_Patient_Lower %>"/>
<jsp:param name="calledfrom" value="patient"/>
<jsp:param name="pkey" value="<%=pkey%>"/>
<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientId)%>"/>

</jsp:include>


<%} //end of if session times out
else{
%>
	<jsp:include page="timeout.html" flush="true"/>

<%
}
%>

<div>
<jsp:include page="bottompanel.jsp" flush="true"/>

</div>

</div>

<DIV class="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</DIV>
</body>
</html>


