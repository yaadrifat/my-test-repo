<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

<jsp:useBean id ="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

String	src = request.getParameter("srcmenu");

String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))   {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
	  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	String tab = request.getParameter("selectedTab");
	String studyVerId = request.getParameter("studyVerId");
	String appndxId = request.getParameter("appndxId");
	String desc = request.getParameter("desc");
	String pubflag = request.getParameter("pubflag");
	String userId = (String) tSession.getValue("userId");

	appendixB.setId(EJBUtil.stringToNum(appndxId));
	appendixB.getAppendixDetails();
	appendixB.setAppendixDescription(desc);
	appendixB.setAppendixPubFlag(pubflag);
	appendixB.setModifiedBy(userId);
	
	int ret =appendixB.updateAppendix();

%>
<br>
<br>
<br>
<br>
<br>
<% if (ret==0) {%> 
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=appendixbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&studyVerId=<%=studyVerId%>">
<% }else {%>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
<%}%>

<%
	}//end of if for eSign check		
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





