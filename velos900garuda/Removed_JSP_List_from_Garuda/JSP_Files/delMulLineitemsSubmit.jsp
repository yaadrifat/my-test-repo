<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Del_MultiLineItems%><%--Delete Multiple Line Items*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*, com.velos.esch.business.common.*,com.aithent.audittrail.reports.AuditUtils"%>
<jsp:useBean id="lineitemB" scope="request" class="com.velos.esch.web.lineitem.LineitemJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<body>
<DIV class="popDefault" id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{
		
%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   
		
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} 
			else{
		  		int ret = 0;
				String userId = (String) tSession.getValue("userId");
				
				String ipAdd= (String) tSession.getValue("ipAdd");
				
				String selLineitemIds=request.getParameter("selLineitemIds");
				String src;
				src= request.getParameter("srcmenu");
				String budgetId = request.getParameter("budgetId");
				String bgtcalId = request.getParameter("bgtcalId");
				String budgetTemplate = request.getParameter("budgetTemplate");

	
				StringTokenizer idLineitems=new StringTokenizer(selLineitemIds,",");
				int numIds=idLineitems.countTokens();
				
				ArrayList arrLineitems =  new ArrayList();
				String[] lineItemIds= null ;
				lineItemIds= new String[numIds];
				
	
	for(int cnt=0;cnt<numIds;cnt++)
			{			
					arrLineitems.add(idLineitems.nextToken());				
					lineItemIds[cnt]=arrLineitems.get(cnt).toString();
			}

			//Modified for INF-18183 ::: Ankit	
			ret = lineitemB.deleteSelectedLineitems(arrLineitems,EJBUtil.stringToNum(userId), ipAdd, AuditUtils.createArgs(session,"",LC.L_Budget));
			
			 //added IA 12.29.2006 bug 2779
			LineitemDao lineDao = new LineitemDao();
			lineDao.clearSubTotals(lineItemIds);
			//end added
		%>
			<br><br><br><br>
			<%
		if(ret == 1){%>
			<p class = "successfulmsg" align = center> <%=MC.M_Del_Succ%><%--Deleted successfully.*****--%></p>
			
			<% 			tSession.setAttribute("lineItemDeleted", "Y");  %>

			
			
			
		<%}
		else{%>
			<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_BeDel%><%--Data could not be deleted .*****--%> </p>

		<%}
 %>
		

		
			<script>
					// Added by IA 1.02.2007 Bug #2778
					//window.opener.document.forms["supervisorDeskActionForm"].getElementById("stockBox").value

					window.opener.location.reload();
					setTimeout("self.close()",1000);
			</script>	
			
	
	
	

		 
		
	<% }
	}//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 
  
</DIV>
 



</body>

</html>
