<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%@page language="java" import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.esch.business.common.SchMsgTxtDao" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = "";
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "top_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("irb_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;

    tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("landing_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
                /*titleStr += " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession))
{
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    grpId = (String) tSession.getValue("defUserGroup");
    usrId = (String) tSession.getValue("userId");
    
    if(accountId == null || accountId == "") {
    %>
		<jsp:include page="timeout.html" flush="true"/>
	<%
        return;
    }
    
    if ("N".equals(request.getParameter("mode"))) {
        studyId = "";
        tSession.removeAttribute("studyId");
    }
}
%>

<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body>
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
	  
	String studyIdForTabs = request.getParameter("studyId");
 	
    String includeTabsJsp = "irbnewtabs.jsp";
%>
<DIV class="BrowserTopn" id = "divtab">
</DIV>

<DIV class="tabDefBotN" id = "div1">
<%
  if (sessionmaint.isValidSession(tSession))
  {
    int pageRight = 0;
    String initParam = null;
    if ("N".equals(request.getParameter("mode")) || studyId == null || studyId.length() == 0) {
        initParam = "&mode=N";
    }  else {
        initParam = "&mode=M&studyId="+studyId;
    }
    SchMsgTxtDao schMsgTxtDao = new SchMsgTxtDao();
    String irbLand = schMsgTxtDao.getMsgTxtLongByType("irb_land", EJBUtil.stringToNum(accountId));
    if (irbLand != null && irbLand.length() > 0) {
        out.println(irbLand);
    }
  }//end of if body for isValidSession
  else
  {
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
  }
  %>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>
</body>
</html>

