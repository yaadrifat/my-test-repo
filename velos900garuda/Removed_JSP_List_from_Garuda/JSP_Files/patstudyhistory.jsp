<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Pat_StdStatusHist%><%-- <%=LC.Pat_Patient%> <%=LC.Std_Study%> Status History*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<%@ page import="com.velos.eres.business.common.*, com.velos.eres.service.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>
<DIV class="browserDefault" id="div1">
  <%
	String studyId = "";
	String patId = "";
	String note = "";
	String status = "";
	String sDate = "";
	String eDate = "";
	String from = "" ;
	String selectedTab ="" ;

	PatStudyStatDao ps = new PatStudyStatDao();
	int len = 0;
	int startDateLen = 0;
	int endDateLen = 0;
	String pcode="";
	String studyNum = "";

	String patStatPk = "";

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{

		pcode = request.getParameter("pcode");
		studyNum = request.getParameter("studyNum");
		studyNum = StringUtil.decodeString(studyNum);
		from= request.getParameter("from" ) ;

		if (from.equals("studypatients")){
			selectedTab=request.getParameter("selectedTab");
		}

		String uName = (String) tSession.getValue("userName");


		int pageRight = 0;

		String tab = request.getParameter("selectedTab");

		pageRight=7;
		studyId= request.getParameter("studyId");
		patId = request.getParameter("patientId");

	if ((pageRight >=6) || ((pageRight == 5 || pageRight == 7 )) )
	{
%>
  <br>
  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=MC.M_MngPat_StdStatHist %><%-- Manage <%=LC.Pat_Patients%> >> <%=LC.Std_Study%> Status History*****--%> </P>
 <Form name="patstat" method="post" action="" onsubmit="">
    <% if (from.equals("studypatients")) {%>
    	<input type="hidden" name="selectedTab"  value="<%=selectedTab%>">
      <jsp:include page="mgpatienttabs.jsp" flush="true"/>
    <br>
    <%}

    	%>
    <input type="hidden" name="srcmenu" value='<%=src%>'>
    <input type="hidden" name="selectedTab" value='<%=tab%>'>

   <table width="100%" cellspacing="0" cellpadding="0" border=0 >

   <tr>
	  <td>
        <P class = "defComments"> <b><%=LC.L_Patient_Id%><%-- <%=LC.Pat_Patient%> ID*****--%>:</b> &nbsp;&nbsp;<%=pcode%> &nbsp;&nbsp;&nbsp; <b><%=LC.L_Study_Number%><%-- <%=LC.Std_Study%> Number*****--%>: </b>&nbsp;&nbsp;<%=studyNum%></P>
     </td>
    </tr>
   </table>
   	<BR>
   	<jsp:include page="patstudyhistorycommon.jsp" flush="true">
     	<jsp:param name="pcode" value="<%=pcode%>"/>
		<jsp:param name="studyNum" value="<%=studyNum%>"/>
		<jsp:param name="from" value="<%=from%>"/>
		<jsp:param name="studyId" value="<%=studyId%>"/>
		<jsp:param name="patientId" value="<%=patId%>"/>
		<jsp:param name="pageRight" value="<%=pageRight%>"/>
	</jsp:include>

   <table width="100%" cellspacing="0" cellpadding="0" border=0 >
	<tr>
		<td align=center>
			<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
		</td>
	</tr>
   </table>
  </Form>
  <%
	} //end of if body for page right
else
{

%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%



} //end of else body for page right



}//end of if body for session



else



{



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%



}



%>

  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>



<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>



</body>



</html>



