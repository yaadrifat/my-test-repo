<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<BODY>

<SCRIPT>


//JM: 22Apr2008: #C8.6-Reports to Preview the Calendar
function validateReport(formobj){

	protId = document.getElementById('protId').value;
	repId= document.getElementById('calReps').value;

	//KM-13Aug08
	var repDD = document.getElementById('calReps');
    repName = repDD.options[repDD.selectedIndex].text;

	var calledFrom = document.getElementById("calledFrom").value
	formobj.action="calrepretrieve.jsp?protId="+protId+"&repId="+repId+"&repName="+repName+"&calledFrom="+calledFrom;
	formobj.target="_new";
	formobj.submit();
}

</SCRIPT>

<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="budgetB" scope="request" class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language="java" import="com.velos.eres.service.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings,java.util.*"%>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>


<%String mode = "N";
int mileRight= 0;
int mileGrpRight =0;

            String selclass;
            //SV, commented 10/28/04, changes for cal-enh doc- req. 7
            String protocolId = "";

             String duration= "";

			String newDuration = "";

			HttpSession tSessionTab = request.getSession(true);

			String acc = (String) tSessionTab.getValue("accountId");

			ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
			ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(acc), "protocol_tab");
            protocolId = request.getParameter("protocolId");
			int budgetPK = 0;
    		if (sessionmaint.isValidSession(tSessionTab))
			{
				newDuration = (String) tSessionTab.getAttribute("newduration");
				if (protocolId != null && (!protocolId.equals("null")))
					budgetPK = budgetB.getDefaultBudgetForCalendar(EJBUtil.stringToNum(protocolId));
			}

			if (StringUtil.isEmpty(newDuration))
			{
				duration = request.getParameter("duration");
			}
			else
			{
				duration = newDuration ;
			}

            String calStatus = "";
            String calledFrom = "";
            String displayType = "";
            String displayDur = "";
            String study = "";
            String pageNo = "";
            String headingNo = "";
            String pageRight = "";
            String calAssoc = "";

            String tab = request.getParameter("selectedTab");

            mode = request.getParameter("mode");
            calledFrom = request.getParameter("calledFrom");
            calStatus = request.getParameter("calStatus");

            calAssoc = request.getParameter("calassoc");
            calAssoc=(calAssoc==null)?"P":calAssoc;

            displayType = request.getParameter("displayType");
            if (displayType == null)
                displayType = "D";
            displayDur = request.getParameter("displayDur");
            if (displayDur == null)
                displayDur = "3";
            pageNo = request.getParameter("pageNo");
            if (pageNo == null)
                pageNo = "1";
            duration = request.getParameter("duration");
            headingNo = request.getParameter("headingNo");
            pageRight = request.getParameter("pageRight");

            HttpSession tSession = request.getSession(true);

            String userId = (String) tSession.getValue("userId");
            String uName = (String) tSession.getValue("userName");
            String studyId = (String) tSession.getValue("studyId");

    		String studyNo = "";
            String protocolName = (String) tSession.getValue("protocolname");
            String refresh_flag = (String) tSession.getValue("refresh_flag");
			
			//KM-#6141
			if (StringUtil.isEmpty(protocolName) == true) {
				if (calledFrom.equals("P")  || calledFrom.equals("L")){
					eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
					eventdefB.getEventdefDetails();
					protocolName = eventdefB.getName();
				}
				else if (calledFrom.equals("S")){
					eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
					eventassocB.getEventAssocDetails();
					protocolName = eventassocB.getName();
				}
			}
			
			//KM-#5000
			if (StringUtil.isEmpty(protocolName) == true)
				protocolName ="";

            calStatus = (calStatus == null)?(String) tSession.getValue("calStatus"):calStatus;

            if (EJBUtil.stringToNum(protocolId) > 0) {
                mode = "M";
            }

            if (calledFrom.equals("S")) {
	            StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	            if (mode == null){
	             	stdRights = new StudyRightsJB();
	            }
	            if (stdRights==null)
	        	{
	        	    //use the one from parameter and set it in session
	        	    tSession.setAttribute("studyId",studyId);
	        		studyB.setId(EJBUtil.stringToNum(studyId));
	        		studyB.getStudyDetails();
	        		studyNo = studyB.getStudyNumber();
	        		tSession.setAttribute("studyNo",studyNo);

	        		TeamDao teamDao = new TeamDao();
	        		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId));

	        		ArrayList tId ;
	        		tId = new ArrayList();
	        		tId = teamDao.getTeamIds();

	        		stdRights = new StudyRightsJB();

	        		if (tId.size() <=0)
	        		{
	        			tSession.putValue("studyRights",stdRights);
	        		} else {

	        			stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));

	        			ArrayList teamRights ;
	        			teamRights  = new ArrayList();
	        			teamRights = teamDao.getTeamRights();

	        			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
	        			stdRights.loadStudyRights();
	        			tSession.putValue("studyRights",stdRights);
	        		}
	        	// Added else part By RA Bug No: 4072(reopen)
	        	}else{
	        		studyB.setId(EJBUtil.stringToNum(studyId));
	        		studyB.getStudyDetails();
	        		studyNo = studyB.getStudyNumber();
	        	}
            }

        	GrpRightsJB grprightsJB = (GrpRightsJB) tSession.getValue("GRights");
        	mileGrpRight = Integer.parseInt(grprightsJB.getFtrRightsByValue("MILEST"));

        	TeamDao teamDao = new TeamDao();
        	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),
        	        EJBUtil.stringToNum((String)tSession.getValue("userId")));
        	ArrayList teamId = teamDao.getTeamIds();
        	if (teamId.size() <= 0) {
        		mileRight=0 ;
        	} else {
        		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
        	 	ArrayList teamRights ;
        		teamRights  = new ArrayList();
        		teamRights = teamDao.getTeamRights();

        		stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
        		stdRightsB.loadStudyRights();

        		if ((stdRightsB.getFtrRights().size()) == 0){
        			mileRight = 0;
        		} else {
        			mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
        		}
        	}

//JM: 22Apr2008:

			String repId = request.getParameter("calReps");
			repId = (repId==null)?"":repId;

			//FIX #6326
			//Moved code to fetchProt.jsp
            %>




		<!--JM: 22Apr2008, introduced the form -->
		<Form name="protocoltab" method="post" action="">

		<!--JM: 22Apr2008, #C8.6: for Report display-->
		<Input type="hidden" id="protId" name="protId" value="<%=protocolId%>">
		<!--<Input type="hidden" name="repId" value="<%=repId%>">-->



<!-- <table class="selectedTab" cellspacing="0" cellpadding="0" border="0">  -->
<table cellspacing="0" cellpadding="0" border="0">
	<tr>

	<%

	for (int iX=0; iX<tabList.size(); iX++) {
	ObjectSettings settings = (ObjectSettings)tabList.get(iX);

	if ("0".equals(settings.getObjVisible())) {
		continue;
	}

	boolean showThisTab = false;
	if ("1".equals(settings.getObjSubType())) {
		 showThisTab = true;
	}
	else if ("2".equals(settings.getObjSubType())) {
		showThisTab = true;

	}
	else if ("3".equals(settings.getObjSubType())) {
		showThisTab = true;
	}
	else if ("4".equals(settings.getObjSubType())) {
		showThisTab = true;
		if (!"P".equals(calAssoc)) {
		    showThisTab = false; // Hide Budget tab for admin schedule
		}
		if (budgetPK < 1) {
		    showThisTab = false; // Hide Budget tab if budget doesn't exist for this protocol
		}
	}
	else if ("5".equals(settings.getObjSubType())) {

		if (calledFrom.equals("S") && mileRight > 0 && mileGrpRight >= 4)
		{
			showThisTab = true;
		}
		if (budgetPK < 1) {
		    showThisTab = false; // Hide Milestones tab if budget doesn't exist for this protocol
		}
	}
	else if ("6".equals(settings.getObjSubType())) {
		showThisTab = true;
	}
	else if ("7".equals(settings.getObjSubType())) {
		showThisTab = true;
	}
	else if ("8".equals(settings.getObjSubType())) {
		showThisTab = true;
	}
	else if ("9".equals(settings.getObjSubType())) {
			if (calledFrom.equals("S")&& mileRight > 0 && mileGrpRight >= 4) /* YK 16MAR Bug #5905*/
		{
			showThisTab = true;
		}
	}
	else {
		showThisTab = true;
	}

	if (!showThisTab) { continue; }


	if (tab == null) {
		selclass = "unselectedTab";
	} else if (tab.equals(settings.getObjSubType())) {
		selclass = "selectedTab";
	} else {
		selclass = "unselectedTab";
	}
	 %>


		<td valign="TOP">
		<table class="<%=selclass%>" cellspacing="0" cellpadding="0"
			border="0">
			<tr>
			<!-- 	<td rowspan="3" valign="top" wclassth="7"><img
					src="../images/leftgreytab.gif" wclassth="8" height="20" border="0"
					alt=""></td>  -->
				 <td>
				<%if ("1".equals(settings.getObjSubType())) { %>
				<a
					href="protocolcalendar.jsp?srcmenu=tdmenubaritem4&amp;selectedTab=1&amp;duration=<%=duration%>&amp;protocolId=<%=protocolId%>&amp;calledFrom=<%=calledFrom%>&amp;mode=<%=mode%>&amp;calStatus=<%=calStatus%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;pageNo=<%=pageNo%>&amp;calassoc=<%=calAssoc%>"> <%=settings.getObjDispTxt()%> </a>
				<%} else if ("2".equals(settings.getObjSubType())) {%>
					<a
					href="eventbrowser.jsp?srcmenu=tdmenubaritem4&amp;selectedTab=2&amp;duration=<%=duration%>&amp;protocolId=<%=protocolId%>&amp;calledFrom=<%=calledFrom%>&amp;mode=<%=mode%>&amp;calStatus=<%=calStatus%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;pageNo=<%=pageNo%>&amp;calassoc=<%=calAssoc%>"><%=settings.getObjDispTxt()%>	</a>
				<%} else if ("3".equals(settings.getObjSubType())) {%>
					<a
					href="manageVisits.jsp?selectedTab=3&amp;mode=<%=mode%>&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=protocolId%>&amp;srcmenu=tdmenubaritem4&amp;calStatus=<%=calStatus%>&amp;pageNo=<%=pageNo%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>"> <%=settings.getObjDispTxt()%> </a>
				<%} else if ("4".equals(settings.getObjSubType())) {%>
					<% if (! mode.equals("N")) {%>
						<a href="protLinkedBudget.jsp?selectedTab=4&amp;mode=<%=mode%>&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=protocolId%>&amp;srcmenu=tdmenubaritem4&amp;calStatus=<%=calStatus%>&amp;pageNo=<%=pageNo%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>"> <%=settings.getObjDispTxt()%> </a>
					<%} %>
				<%} else if ("5".equals(settings.getObjSubType())) {%>
					<a id="mprotocolTabLink"  href="protLinkedMilestones.jsp?calName=<%=StringUtil.encodeString(protocolName)%>&includeMenus=N&studyId=<%=studyId%>&amp;selectedTab=5&amp;mode=<%=mode%>&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=protocolId%>&amp;srcmenu=tdmenubaritem4&amp;calStatus=<%=calStatus%>&amp;pageNo=<%=pageNo%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>"> <%=settings.getObjDispTxt()%> </a>
				<%} else if ("6".equals(settings.getObjSubType())) { %>
                    <a
					href="fetchProt2.jsp?selectedTab=6&amp;mode=<%=mode%>&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=protocolId%>&amp;srcmenu=tdmenubaritem4&amp;calStatus=<%=calStatus%>&amp;pageNo=<%=pageNo%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>&amp;calName=<%=StringUtil.encodeString(protocolName)%>"> <%=settings.getObjDispTxt()%> </a>
                <%} else if ("7".equals(settings.getObjSubType())) { %>
                    <a
					href="fetchCoverage.jsp?selectedTab=7&amp;mode=<%=mode%>&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=protocolId%>&amp;srcmenu=tdmenubaritem4&amp;calStatus=<%=calStatus%>&amp;pageNo=<%=pageNo%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>&amp;calName=<%=StringUtil.encodeString(protocolName)%>"> <%=settings.getObjDispTxt()%> </a>
                <%} else if ("8".equals(settings.getObjSubType())) { %>
                    <a
					href="fetchSubCostItems.jsp?selectedTab=8&amp;mode=<%=mode%>&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=protocolId%>&amp;srcmenu=tdmenubaritem4&amp;calStatus=<%=calStatus%>&amp;pageNo=<%=pageNo%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>&amp;calName=<%=StringUtil.encodeString(protocolName)%>"> <%=settings.getObjDispTxt()%> </a>
                <%} else if ("9".equals(settings.getObjSubType())) { %>
                    <a
					href="calMilestoneSetup.jsp?selectedTab=9&amp;mode=<%=mode%>&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=protocolId%>&amp;srcmenu=tdmenubaritem4&amp;calStatus=<%=calStatus%>&amp;pageNo=<%=pageNo%>&amp;displayType=<%=displayType%>&amp;displayDur=<%=displayDur%>&amp;calassoc=<%=calAssoc%>&amp;calName=<%=StringUtil.encodeString(protocolName)%>"> <%=settings.getObjDispTxt()%> </a>
                <%} %>

				</td>
			<!-- 	<td rowspan="3" valign="top" wclassth="7"><img
					src="../images/rightgreytab.gif" wclassth="7" height="20"
					border="0" alt=""></td> -->
			</tr>
		</table>
		</td>
 	<%}%>

	  </tr>
	</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>



<%if (calledFrom.equals("S")) { //called from Study so show study no %>
<table width="98%">
	<tr>
		<td width="40%">
		<P class="defComments"><a href="study.jsp?mode=M&amp;srcmenu=tdmenubaritem3&amp;selectedTab=1&amp;studyId=<%=studyId%>"><img src="../images/jpg/study.gif" alt="<%=LC.L_Std_Admin%><%--Study Administration*****--%>" border=0 ></a>
			&nbsp;
			<%if(tab.equals("3")) { %><B>
			<%if (studyNo.length() > 30){%>
				<a href="#" title="<%=studyNo%>">
					<%=studyNo.substring(0,27)+"..."%>
				</a>
			<%}else{%>
				<%=studyNo%>
				   <%}%></B>
				<%}else { %>
					<%if (studyNo.length() > 30){%>
						<a href="#" title="<%=studyNo%>">
							<%=studyNo.substring(0,27)+"..."%>
						</a>
					<%}else{%>
						<%=studyNo%>
					   <%}%>
			<%} %>
		<%if (!(tab.equals("1"))) {
			%>&nbsp;&nbsp;<%=LC.L_Calendar%><%--Calendar*****--%>: <B>
			<%if(protocolName.length() > 30){%>
				<a href="#" title="<%=protocolName%>"><%=protocolName.substring(0,27)+"..."%></a>
			<%}else{%>
				<%=protocolName%>
			<%} %>
			</B>
        <%}%>
		&nbsp;&nbsp;&nbsp;</P>
		</td>
        <%if ("P".equals(calAssoc)) { %>
		<td width="50%" align="right">
		<span id="fakeVisitsStudy" style="display: none;"><%=MC.M_CalVisitDef_NotComplt%>&nbsp;&nbsp;</span>
		<%
		String calenderId =(String)session.getAttribute("wrongVisitCalender");
		calenderId=calenderId==null? "":calenderId;
		ArrayList wrongVisitArray = new ArrayList();
		wrongVisitArray =(ArrayList)session.getAttribute("wrongVisitArray");
		wrongVisitArray=wrongVisitArray==null? new ArrayList():wrongVisitArray;
		int length = wrongVisitArray.size();
		protocolId=(null != protocolId)?protocolId.trim():null;
		calenderId=(null != calenderId)?calenderId.trim():null;
		%>
		<% if(calenderId.equalsIgnoreCase(protocolId)) {if(length>0){ %>
		<script>
		$j("#fakeVisitsStudy").show();
		</script>
		<%}}%>
		<A href="studyprotocols.jsp?mode=M&amp;srcmenu=tdmenubaritem3&amp;selectedTab=7&amp;calFocus=true&amp;studyId=<%=studyId%>"><%=LC.L_BackTo_Cal%><%--Back to Calendars*****--%></A>
		</td>
        <%} %>
        <!-- Virendra: Added to fix Bug No.#4769 -->
        <%if ("S".equals(calAssoc)) { %>
		<td width="50%" align="right"> 
		<span id="fakeVisitsAdmin" style="display: none;"><%=MC.M_CalVisitDef_NotComplt%>&nbsp;&nbsp;</span>
			<%
		String calenderId =(String)session.getAttribute("wrongVisitCalender");
		calenderId=calenderId==null? "":calenderId;
		ArrayList wrongVisitArray = new ArrayList();
		wrongVisitArray =(ArrayList)session.getAttribute("wrongVisitArray");
		wrongVisitArray=wrongVisitArray==null? new ArrayList():wrongVisitArray;
		int length = wrongVisitArray.size();
		protocolId=(null != protocolId)?protocolId.trim():null;
		calenderId=(null != calenderId)?calenderId.trim():null;
		%>
		<% if(calenderId.equalsIgnoreCase(protocolId)) {if(length>0){ %>
		<script>
		$j("#fakeVisitsAdmin").show();
		</script>
		<%}}%>
		<A href="studyadmincal.jsp?srcmenu=tdmenubaritem3&selectedTab=10&studyId=<%=studyId%>"><%=MC.M_Go_ToAdminSch%><%--Go to Admin Schedule*****--%></A>
		</td>
        <%} %>
	</tr>
</table>

<% } else if ((calledFrom.equals("P"))||(calledFrom.equals("L"))) {%>    <!-- AK: Added to fix Bug No.#6940 -->
<table width="98%">
<tr>
<td width="50%">
		<P class="defComments"><%if (!(tab.equals("1"))) {
            %><%=LC.L_Calendar%><%--Calendar*****--%>: <B>
            <%if(protocolName.length() > 30){%>
				<a href="#" title="<%=protocolName%>"><%=protocolName.substring(0,27)+"..."%></a>
			<%}else{%>
				<%=protocolName%>
			<%} %>
			</B>
		<%}%>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</P>
		</td>
<td width="50%" align="right">
		<span id="fakeVisitsLib" style="display: none;"><%=MC.M_CalVisitDef_NotComplt%>&nbsp;&nbsp;</span>
		<%
		String calenderId =(String)session.getAttribute("wrongVisitCalender");
		calenderId=calenderId==null? "":calenderId;
		ArrayList wrongVisitArray = new ArrayList();
		wrongVisitArray =(ArrayList)session.getAttribute("wrongVisitArray");
		wrongVisitArray=wrongVisitArray==null? new ArrayList():wrongVisitArray;
		int length = wrongVisitArray.size();
		protocolId=(null != protocolId)?protocolId.trim():null;
		calenderId=(null != calenderId)?calenderId.trim():null;
		%>
		<% if(calenderId.equalsIgnoreCase(protocolId)) {if(length>0){ %>
		<script>
		$j("#fakeVisitsLib").show();
		</script>
		<%}}%>
		<A href="protocollist.jsp?mode=M&amp;srcmenu=tdmenubaritem3&amp;selectedTab=1&amp;calFocus=true"><%=LC.L_BackTo_Cal%><%--Back to Calendars*****--%></A>
		</td>
			</tr>
</table>

<%} else {%>
<table width="98%">
	<tr>
		<td width="50%">
		<P class="defComments"><%if (!(tab.equals("1"))) {
            %><%=LC.L_Calendar%><%--Calendar*****--%>: <B>
            <%if(protocolName.length() > 30){%>
				<a href="#" title="<%=protocolName%>"><%=protocolName.substring(0,27)+"..."%></a>
			<%}else{%>
				<%=protocolName%>
			<%} %>
			</B>
		<%}%>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</P>
		</td>
		<%
		//FIX #6326
		//Moved code to fetchProt.jsp
		%>
	</tr>
</table>
<%}%>
</Form>

</BODY>



</HTML>

