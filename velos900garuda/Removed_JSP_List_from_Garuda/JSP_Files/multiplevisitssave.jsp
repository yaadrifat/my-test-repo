<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil,com.velos.esch.web.protvisit.ProtVisitJB" %>
<%@ page import = "com.velos.eres.service.util.MC,com.velos.eres.service.util.LC"%>
<%@ page import = "com.velos.eres.service.util.VelosResourceBundle"%>
<%@ page import = "com.velos.esch.service.util.EJBUtil"%>
<%@ page import="com.velos.esch.business.common.SchCodeDao"%>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>
</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<%
String src = request.getParameter("srcmenu");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String mode = request.getParameter("mode");
String fromPage = request.getParameter("fromPage");
String calStatus = request.getParameter("calStatus");
String noInterval = request.getParameter("noInterval");

String categoryId = "";
String visitId = "";
String eSign = request.getParameter("eSign");
String visit_name = (request.getParameter("visitName")).trim() ;
String from = request.getParameter("from");


String visitNo = request.getParameter("visitNo");
String monthStr = request.getParameter("months");
String weekStr = request.getParameter("weeks");
String dayStr = request.getParameter("days");
String insertAfterDispStr = request.getParameter("insertAfter");;

String insertAfterIntervalStr = request.getParameter("insertAfterInterval");
int insertAfterInterval = 0;
int new_protocol_duration = 0;
String insertAfterIntervalUnit = request.getParameter("intervalUnit");
String description =  request.getParameter("description") ;
String duration=  request.getParameter("duration") ;
String dispStr = "";


//KM-DFIN4
String beforeNum = request.getParameter("beforenum");
beforeNum = (beforeNum == null) ? "": beforeNum;
if (beforeNum.length() == 0) {
	beforeNum = "0";
}

String afterNum = request.getParameter("afternum");

afterNum = (afterNum == null) ? "": afterNum;
if (afterNum.length() == 0) {
	afterNum = "0";
}

String durUnitA = request.getParameter("durUnitA");
durUnitA = (durUnitA == null) ? "" : durUnitA;

String durUnitB = request.getParameter("durUnitB");
durUnitB = (durUnitB == null) ? "" : durUnitB;


String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;

int months = 0, weeks = 0, days = 0;
int ret=0;
int insertAfter = 0 ;
int disp = 0, prevdisp = 0, interval = 0;
int noOfDays;
String intervalUnit = "";

description = (   description  == null      )?"":(  description ) ;

if ((insertAfterIntervalStr != null) && (insertAfterIntervalStr.length() > 0)) {
	int pos = insertAfterDispStr.indexOf('/');
	if (pos > 0) {
		String insertAfterStr =  insertAfterDispStr.substring(0, pos);
		dispStr = insertAfterDispStr.substring(pos+1);
		insertAfter = EJBUtil.stringToNum(insertAfterStr);
		prevdisp = EJBUtil.stringToNum(dispStr);
	}

	months = 0;
	weeks = 0;
	days = 0;

	insertAfterInterval = EJBUtil.stringToNum(insertAfterIntervalStr);
	if (insertAfterIntervalUnit.equals("M"))
		interval = insertAfterInterval *30;
	else if (insertAfterIntervalUnit.equals("W"))
		interval = insertAfterInterval *7;
	else //days
		interval = insertAfterInterval;

	disp = prevdisp + interval;

	if(disp == 0 && calStatus.equals("O")){
		SchCodeDao cd = new SchCodeDao();
		String calStatusDesc = cd.getCodeDescription(cd.getCodeId("calStatStd","O"));
		
		Object[] arguments = {calStatusDesc};
		%>
		<br><br><br><br><br><p class = "sectionHeadings" align = center><%=VelosResourceBundle.getMessageString("M_OfflineCalculatedVisitDayZero",arguments)%><Br><Br>
		<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% 
		return;
	}%>
<%
}

else {
	months = EJBUtil.stringToNum(monthStr);
	weeks = EJBUtil.stringToNum(weekStr);
	days = EJBUtil.stringToNum(dayStr);

	if (months == 0) months++;
	if (weeks == 0) weeks++;
	if (days == 0) days++;
	disp = (months - 1)*30 + (weeks - 1)*7 + days;


	disp = 0;
	if (months > 0) disp+= (months -1)*30;
	if (weeks > 0) disp+= (weeks -1)*7;
	disp += days;


	insertAfter = 0;
	insertAfterInterval = 0;
	insertAfterIntervalUnit = "";

}
//BK,May/20/2011,Fixed #6014
	ProtVisitJB pVisit = new ProtVisitJB();
	int checkVisit = 0;
	
	if(disp == EJBUtil.stringToNum(duration)){
		checkVisit = pVisit.validateDayZeroVisit(Integer.parseInt(duration),Integer.parseInt(protocolId),1,0 );
		}  

	if (disp > EJBUtil.stringToNum(duration) || checkVisit > 0){%>
		<br><br><br><br><br><p class = "sectionHeadings" align = center>
		<%if ((insertAfterIntervalStr != null) && (insertAfterIntervalStr.length() > 0)){%>
			<%=MC.M_CalcIntervalExceedsDuration%>
		<% }else{%>
			<%=MC.M_MaxIntervalExceedsDuration%>
		<%}%><Br><Br>
		<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<%
		return;
	}
	checkVisit = 0;
	if(disp == 1 && EJBUtil.stringToNum(dayStr) == 0 && StringUtil.isEmpty(noInterval)){
		checkVisit = pVisit.validateDayZeroVisit(Integer.parseInt(duration),Integer.parseInt(protocolId),0,0);
		}
	if (checkVisit > 0){%>
			<br><br><br><br><br><p class = "sectionHeadings" align = center><%=MC.M_DayZeroExceedsDuration%><Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;
	}

%>


<%
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");


		String ip_add = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

		String accountId = (String) tSession.getValue("accountId");


   if(mode.equals("N")) {


	protVisitB.setProtocol_id(EJBUtil.stringToNum(protocolId));
	protVisitB.setName(visit_name);
	protVisitB.setDescription(description   );

	if (noInterval == null) {
	protVisitB.setMonths(EJBUtil.stringToNum(monthStr));
	protVisitB.setWeeks(EJBUtil.stringToNum(weekStr));
	//D-FIN25-DAY0
	if(!StringUtil.isEmpty(dayStr) && insertAfter == 0){
		noOfDays = EJBUtil.stringToNum(dayStr);
    	 }
    else if(insertAfter != 0){
    	noOfDays = disp;
		}
    else {
	    noOfDays = 1; 	
		}	
    protVisitB.setDays(new Integer(noOfDays));

	protVisitB.setInsertAfter(new Integer(insertAfter));
	protVisitB.setInsertAfterInterval(new Integer(insertAfterInterval));
	protVisitB.setInsertAfterIntervalUnit(insertAfterIntervalUnit);

	protVisitB.setDisplacement(String.valueOf(disp));//KM
	protVisitB.setIntervalFlag("0");
	}
	else  {
		protVisitB.setIntervalFlag("1");
	}


	int visitNum=protVisitB.getMaxVisitNo(EJBUtil.stringToNum(protocolId));
	protVisitB.setVisit_no(visitNum + 1);
 	protVisitB.setCreator(usr);
 	protVisitB.setip_add(ip_add);

	//KM-DFIN4
	protVisitB.setDurationBefore(beforeNum);
	protVisitB.setDurationUnitBefore(durUnitA);

	protVisitB.setDurationAfter(afterNum);
	protVisitB.setDurationUnitAfter(durUnitB);

	protVisitB.setProtVisitDetails();

	ret=protVisitB.getVisit_id();

	visitId = (new Integer(protVisitB.getVisit_id())).toString();

	mode="M";
   } else {
		visitId = request.getParameter("visitId");
		protVisitB.setVisit_id(EJBUtil.stringToNum(visitId));
		protVisitB.setProtocol_id(EJBUtil.stringToNum(protocolId));
		protVisitB.setName(request.getParameter("visitName"));
		protVisitB.setDescription(description   );
		protVisitB.setMonths(EJBUtil.stringToNum(monthStr));
		protVisitB.setWeeks(EJBUtil.stringToNum(weekStr));
		//D-FIN25-DAY0
		if(!StringUtil.isEmpty(dayStr) && insertAfter == 0){
			noOfDays = EJBUtil.stringToNum(dayStr);
	    	 }
	    else if(insertAfter != 0){
	    	noOfDays = disp;
			}
	    else {
		    noOfDays = 1; 	
			}	
	    protVisitB.setDays(new Integer(noOfDays));

		protVisitB.setInsertAfter(new Integer(insertAfter));
		protVisitB.setInsertAfterInterval(new Integer(insertAfterInterval));
		protVisitB.setInsertAfterIntervalUnit(insertAfterIntervalUnit);

		protVisitB.setDisplacement(String.valueOf(disp));//KM
		protVisitB.setip_add(ip_add);

		protVisitB.setVisit_no(EJBUtil.stringToNum(visitNo));

	 	protVisitB.setModifiedBy(usr);
		
		protVisitB.setDurationBefore(beforeNum);
		protVisitB.setDurationUnitBefore(durUnitA);

		protVisitB.setDurationAfter(afterNum);
		protVisitB.setDurationUnitAfter(durUnitB);


			ret=protVisitB.updateProtVisit(calledFrom);
}


	if ((calledFrom.equals("P")) || (calledFrom.equals("S"))) {
			if (ret==-1) {%>
			<br><br><br><br><br><p class = "sectionHeadings" align = center>
			<%=MC.M_VisitNameDupli_ClkBack%><%--Visit Name is duplicate. Click on Back Button to change the name*****--%> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>

		<% }
		//D-FIN-25-DAY0
			else if (ret==-2) {%>
			<br><br><br><br><br><p class = "sectionHeadings" align = center>
			<%=MC.M_VisitInterval_BackBtn%><%--Visit intervals Month1,week1,day7 and day0 can not coexist. Click on "Back" Button to change.*****--%><Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>

		<% }
		else{

				tSession.putValue("visitname",visit_name);
				if (ret>=0)
				 {
					
					if (noInterval == null){
						// Update all the no interval visits that overlap with this visit's displacement
						ret = protVisitB.pushNoIntervalVisitsOut(new Integer(visitId).intValue(), ip_add, new Integer(usr).intValue());
					}

				  new_protocol_duration = protVisitB.generateRipple(new Integer(visitId).intValue(),calledFrom);


				  if (new_protocol_duration > 0) //if ripple succeeds
				  {
				  		tSession.setAttribute("newduration", String.valueOf(new_protocol_duration))	;

				  }else
				  {

				  		tSession.setAttribute("newduration", duration);
				  }

				 }


		%>
			<script>


			<%
			if (!StringUtil.isEmpty(from) && from.equals("submitadd")) { //KM-3402
			  int max_visit_no = 0;
			  String link = "multiplevisits.jsp?srcmenu=" + src + "&eventId=&duration=" + duration +"&visitId="+visitId+ "&protocolId="+protocolId+ "&calledFrom=" + calledFrom + "&mode=N&calStatus=" + calStatus + "&fromPage=eventbrowser&from=initial&tableName=sch_protocol_visit&max_visit_no="+max_visit_no+"&calassoc="+calAssoc;
	        %>

			 //JM: 09NOV2009: #3953
			 //windowName=window.open('<%=link%>',"Information","toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=yes,width=950,height=350 top=100,left=150 ");
			 //windowName.focus();

			  window.location.href = "<%=link%>";

			<%}else{%>

			  setTimeout("self.close()",1000);

			<%}%>
			//KM-#5000
			window.opener.location="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=3&visitId=<%=visitId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&duration=<%=duration%>&calassoc=<%=calAssoc%>"

			</script>

		 <%

		}
	}%>

<%

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


