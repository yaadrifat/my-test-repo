<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--PCAL-20461 For loading Events under the Visit--%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import ="com.velos.eres.business.group.*,java.net.URLEncoder,com.velos.eres.business.common.*,
com.velos.esch.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.StringUtil,
com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB, 
com.velos.esch.web.eventdef.EventdefJB, com.velos.esch.web.eventassoc.EventAssocJB, com.velos.eres.service.util.LC, com.velos.eres.service.util.MC"%>

<jsp:useBean id="eventlistdao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="assoclistdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<%@page import="java.util.ArrayList"%>
<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
		String protocolName = "";
		String calledFrom = request.getParameter("calledFrom");
		String calStatus = request.getParameter("calStatus");
		boolean isLibCal = false;
		boolean isStudyCal = false;
		String eventType="";
		
		eventType = "P";
		String mode = request.getParameter("mode");
		String displayType=request.getParameter("displayType");

		ArrayList eventNames= null;
		ArrayList eventIds=null;
		ArrayList eventTypes=null;
		ArrayList eventVisitIds = null;
		ArrayList sequences = null;
		ArrayList offLineFlags = null;
		ArrayList hideFlags = null;
		ArrayList eventLibraryType = null;
		ArrayList cptCodes = null;
		 
	    
	  if ("P".equals(calledFrom) || "L".equals(calledFrom) ) {
	        isLibCal = true;
	    } else if ("S".equals(calledFrom)) {
	        isStudyCal = true;
	    } else {
	        out.println(LC.L_NoCal_Found);
	        return;
	    }
	  
	  String protocolId = request.getParameter("protocolId");
	  SchCodeDao scho = new SchCodeDao();
	  
	  String visitNameId = request.getParameter("visitList");
		if (visitNameId==null) visitNameId = "";

		String evtName = "";
		evtName =request.getParameter("searchEvt");
		evtName=(evtName==null)?"":evtName;
		
		
	    if (calledFrom.equals("P")  || calledFrom.equals("L"))
			{

			   eventlistdao= eventdefB.getAllProtSelectedVisitsEvents(EJBUtil.stringToNum(protocolId), visitNameId, evtName);
			   eventNames= eventlistdao.getNames();
			   eventIds=eventlistdao.getEvent_ids() ;
			   eventTypes= eventlistdao.getEvent_types();
			   eventVisitIds = eventlistdao.getFk_visit();
			   sequences = eventlistdao.getEventSequences();
			   eventLibraryType = eventlistdao.getEventLibraryType();
			   cptCodes = eventlistdao.getCptCodes();
			   
			}
			else if (calledFrom.equals("S"))
			{  //called From Study
			   assoclistdao= eventassocB.getAllProtSelectedVisitsEvents(EJBUtil.stringToNum(protocolId), visitNameId, evtName);
			   eventNames= assoclistdao.getNames();
			   eventIds=assoclistdao.getEvent_ids() ;
			   eventVisitIds = assoclistdao.getEventVisits();
			   eventTypes= assoclistdao.getEvent_types();
			   sequences = assoclistdao.getEventSequences();
			   offLineFlags = assoclistdao.getOfflineFlags();
			   hideFlags = assoclistdao.getHideFlags();
			   eventLibraryType = assoclistdao.getEventLibraryType();
			   cptCodes = assoclistdao.getCptCodes();
			}
		
	
		int noOfEvents = eventIds == null ? 0 : eventIds.size();
	    String offLineFlag ="";
		String hideFlag ="";
		int visitCount=0;
	    String eventRow="";
		for (int iY=0; iY<noOfEvents; iY++){
					if (calledFrom.equals("S")){

						hideFlag = ((hideFlags.get(iY)) == null)?"":(hideFlags.get(iY)).toString();

						}
					String eventVisitID= eventVisitIds.get(iY).toString();
					if(!eventIds.get(iY).equals(0) && eventVisitID.equals(visitNameId) )
					{
						visitCount++;
						String visitName=(String)eventNames.get(iY);
						String cptCode=(String)cptCodes.get(iY);
						String libType= (String)eventLibraryType.get(iY);
						libType = scho.getCodeDescription(EJBUtil.stringToNum(libType));
						StringBuffer toolTip=  new StringBuffer();
						toolTip.append("<b>").append(LC.L_Event_Name).append(": </b>")
						.append((visitName.length()>25?visitName.substring(0,25)+"...":visitName)+"<br>")
						.append("<b>").append(LC.L_Evt_Lib).append(": </b>")
						.append((libType.length()>25?libType.substring(0,25)+"...":libType)+"<br>") //YK:Fixed Bug#7397
						.append("<b>").append(LC.L_Event+" ").append(LC.L_Cpt_Code).append(": </b>")
						.append((cptCode==null?" - ":cptCode)+"<br>");
						
								%>
					<tr id="<%=visitCount %>"  style="cursor:n-resize;" onmouseover="this.style.cursor='move'; return overlib('<%=toolTip%>',CAPTION,'<%=LC.L_Event_Name%>');" onmouseout="return nd();">
						<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC; display: none;" class="eventMove"><%=eventIds.get(iY)%></td>
						<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;" class="eventMove"><%=sequences.get(iY)%></td>
						<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;" class="eventMove"><%=visitName %></td>
						<td style="text-align: center; border-bottom: 1px solid #CCC;" class="eventMove">
						<%if(calStatus.equals("O")){ %>
						<%if (hideFlag.equals("1")){%>
						<input id="chk<%=eventIds.get(iY)%><%=eventVisitID%>" style="cursor:default;" type="checkbox" checked onclick="checkUncheck('<%=eventIds.get(iY)%><%=eventVisitID%>');"/>
						<input type="hidden" id="chkHid<%=eventIds.get(iY)%><%=eventVisitID%>" value="checked"></input>
						<%}else{%>
						<input id="chk<%=eventIds.get(iY)%><%=eventVisitID%>" style="cursor:default;" type="checkbox" onclick="checkUncheck('<%=eventIds.get(iY)%><%=eventVisitID%>');"/>
						<input type="hidden" id="chkHid<%=eventIds.get(iY)%><%=eventVisitID%>" value="unchecked"></input>
						<%}%>
						<%}else{
							%>
							<input id="chk<%=eventIds.get(iY)%><%=eventVisitID%>" style="cursor:default;" type="checkbox" disabled/>
							<%
						}%>
						</td>
						
					</tr>	
				<% }
				}
				
				if(visitCount==0)
				{ %>
				<tr class="noEvent">
				<td colspan="4" ><%=MC.M_NoEvt_InThisVisit%></td>
				</tr>	
				<%}
				%>
				<tr class="noEvent" height="25">
				<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;>&nbsp;"></td>
				<td style="border-bottom: 1px solid #CCC; border-right: 1px solid #CCC;>&nbsp;"></td>
				<td style="border-bottom: 1px solid #CCC; ">&nbsp;</td>
				</tr>
   <%
	  
	
}//end of if body for session
else
{
	%>
	<jsp:include page="timeout.html" flush="true" /> 
	<%
	}
%>

