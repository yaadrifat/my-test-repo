<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<jsp:include page="include.jsp" flush="true"/> 


<title><%=LC.L_Del_MeetingTopic%><%--Delete Meeting Topic*****--%></title>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>

function  validate(formobj){

	if (!(validate_col('e-Signature',formobj.eSign))) return false



	if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   }

}

</SCRIPT>



</head>
<jsp:include page="skinChoser.jsp" flush="true"/>


<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>

<jsp:useBean id="mjb" scope="request" class="com.velos.eres.web.submission.ReviewMeetingTopicJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:include page="panel.jsp" flush="true" /> 
	 
 

 
<BODY> 

<br>



<DIV class="formDefault" id="div1">

<% 

 	 
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	

		
		 
		int ret=0;
		String pk=request.getParameter("pk");
		
		String reviewBoard = request.getParameter("reviewBoard");
		String meetingDate = request.getParameter("meetingDate");

	
		pk=(pk==null)?"":pk;
				
		String delMode=request.getParameter("delMode");
		
		if (delMode==null) {
			delMode="Del";
%>

	<FORM name="del" method="post" id="delfrm" action="deleteMeetingTopic.jsp" onSubmit="if (validate(document.del)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="delfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


	 <input type="hidden" name="pk" value="<%=pk%>">
 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="reviewBoard" id="reviewBoard" value="<%=reviewBoard%>">
	<input type="hidden" name="meetingDate" id="meetingDate" value="<%=meetingDate%>">
	 
	</FORM>



<%

	} else {

			String eSign = request.getParameter("eSign");	

			String oldESign = (String) tSession.getValue("eSign");

			if(!oldESign.equals(eSign)) {

%>

	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	

<%

			} else {
   
   				int retVal = 0;
			     mjb.setId(EJBUtil.stringToNum(pk));
			  // Modified for INF-18183 ::: AGodara 
			     retVal = mjb.removeReviewMeetingTopic(AuditUtils.createArgs(tSession,"",LC.L_Res_Compliance));
     
   
		
	if (retVal != -1)
	{
	%>	
	<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>
	 
	<%
	} //retVal != -1
	else
	{
		%>	
	<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Succ%><%--Data could not be successfully.*****--%> </p>
	
	<%
	}
		%>
		
		
			<META HTTP-EQUIV=Refresh CONTENT="1; URL=irbmeeting.jsp?meetingDate=<%=meetingDate%>&reviewBoard=<%=reviewBoard%>">

			<%
	
		}
		} //end esign
		
	 }//end of if body for session 

else { %>

 <jsp:include page="timeout.html" flush="true"/> 

 <% } %>

 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

  

</DIV>



<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 

</div>



</body>

</HTML>





