<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<script language="JavaScript" src="budget.js"></script>
<head>

<style>
.yui-skin-sam .yui-dt th.yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-odd .yui-dt-hidden,
.yui-skin-sam .yui-dt tr.yui-dt-even .yui-dt-hidden {
border:0px;
}
</style>

<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Study/*LC.Std_Study*****/;
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "study_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("12".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);/*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
<%
//DFIN-13 BK MAR-16-2011
String mode =   request.getParameter("mode");

boolean isIrb = "irb_personnel_tab".equals(request.getParameter("selectedTab")) ? true : false; 

%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB" %>
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="budgetUsersB" scope="request"	class="com.velos.esch.web.budgetUsers.BudgetUsersJB" />
<jsp:useBean id="studyB" scope="session" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="grpB" scope="request"	class="com.velos.eres.web.group.GroupJB" />
<jsp:useBean id="ctrl" scope="request"	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="grpRights" scope="request"	class="com.velos.eres.web.grpRights.GrpRightsJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil, com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%
	String from = "combBudget";

	String pageMode = request.getParameter("pageMode");
	if(pageMode == null) pageMode = "initial";

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");
 	
%>
<body class="yui-skin-sam yui-dt yui-dt-liner" style="overflow-y:hidden">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
   HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)){
	String studyId = request.getParameter("studyId");
	tSession.putValue("studyId", studyId);
	if(studyId== null) studyId = "";	

	String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp"; 
	%>
	<DIV class="BrowserTopn" id="divTop">    
	  <jsp:include page="<%=includeTabsJsp%>" flush="true">
	  <jsp:param name="from" value="<%=from%>"/>
	  	<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
	   </jsp:include>
	</DIV>
	<%
	if(studyId == "" || studyId == null) {
		%> <DIV class="BrowserBotn BrowserBotN_S_1" id="divBot">   
				<jsp:include page="studyDoesNotExist.jsp" flush="true" /> 
			</DIV>
		<%
	} else {
		boolean withSelect = true;
	StringBuffer urlParamSB = new StringBuffer();
	urlParamSB.append("studyId=").append(StringUtil.htmlEncodeXss(studyIdForTabs));
		
	SchCodeDao schD = new SchCodeDao();
	int perCodeId = schD.getCodeId("category","ctgry_per");

	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");
	
	String userIdFromSession = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
	
	int accId = EJBUtil.stringToNum(acc);
	int userId = EJBUtil.stringToNum(userIdFromSession);
	
	userB.setUserId(userId);
	userB.getUserDetails();

	String includedIn ="S";
	
	String selectedTab = request.getParameter("selectedTab");
	String selectedBgtcalId = "";
	
	int pageRight= 0;
	
	ArrayList teamId = new ArrayList();
	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyIdForTabs),EJBUtil.stringToNum(userIdFromSession));
	teamId = teamDao.getTeamIds();

	if (teamId.size() <=0)
	{
		pageRight  = 0;
		StudyRightsJB stdRightstemp = new StudyRightsJB();
	}
	if (teamId.size() == 0) {
		pageRight=0 ;
	}else {
		stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
	 	ArrayList teamRights ;
		teamRights = new ArrayList();
		teamRights = teamDao.getTeamRights();

		stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
		stdRightsB.loadStudyRights();

		if ((stdRightsB.getFtrRights().size()) == 0){
			pageRight= 0;
		}else{
			//checking study setup access unless a new access right for combined budget
			pageRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
		}
	}
	 /*YK 29Mar2011 -DFIN20 */
	int studyIDs=Integer.parseInt(studyId);
	if (teamId.size()> 0) {
	studyB.setId(studyIDs);
	studyB.getStudyDetails();
	}
	CodeDao cdMsSetDao = new CodeDao();
	String roleCodePk="";

	if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
	{
		ArrayList tId = new ArrayList();

		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
		tId = teamDao.getTeamIds();

		if (tId != null && tId.size() > 0)
		{
			ArrayList arRoles = new ArrayList();
			arRoles = teamDao.getTeamRoleIds();

			if (arRoles != null && arRoles.size() >0 )
			{
				roleCodePk = (String) arRoles.get(0);

				if (StringUtil.isEmpty(roleCodePk))
				{
					roleCodePk="";
				}
			}
			else
			{
				roleCodePk ="";
			}

		}
		else
		{
			roleCodePk ="";
		}
	} else
		{
		  roleCodePk ="";
		}

	ArrayList setting = new ArrayList();
	SettingsDao setDao = new SettingsDao();
	setDao.retrieveSettings(accId, 1, "MILESTONE_SET_STATUS");
	setting = setDao.getSettingValue();
	
	cdMsSetDao.getCodeValuesForStudyRole("mile_setstat",roleCodePk);
	String mileSetStat = "";

	String selMilePayType = "";
	selMilePayType = request.getParameter("mileStat");
	if (selMilePayType==null) selMilePayType="";
	if ("N".equalsIgnoreCase(mode))
		mileSetStat = cdMsSetDao.toPullDown("mile_setstat id='mile_setstat' onchange='setMileStatus(this);'");
	else
		mileSetStat = cdMsSetDao.toPullDown("mile_setstat id='mile_setstat' onchange='setMileStatus(this);'",EJBUtil.stringToNum(studyB.getMilestoneSetStatus()),withSelect);
	
	String studyNo = (String) tSession.getValue("studyNo");
	/*YK 29Mar2011 -DFIN20 */
	
	//String mileStatDesc= cdMsPayDao.getCodeDesc();
%>
<DIV class="BrowserBotN BrowserBotN_S_3" id="divBottom"><%
if (pageRight > 0){		
	%>
	<input type="hidden" id ="pageRight" name="pageRight" value="<%=pageRight%>"/>
	<%
	if(studyId == "" || studyId == null) {%>
	<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
	<% }else{
	%>
	<script>
		function reloadMilestoneGrid(pMileTypeId) {
			YAHOO.example.milestoneGrid = function() {
				var args = {
					urlParams: "<%=urlParamSB.toString()%>",
					dataTable: "milestonegrid",
					pMileTypeId: pMileTypeId 
				};
				
				myMilestoneGrid = new VELOS.milestoneGrid('fetchMilestoneJSON.jsp', args);
				myMilestoneGrid.startRequest();
		    }();
		}
		/*YK 01Aug11:Commented for Enhancement FIN-20073, Now using onload of Panel for individual Milestones*/
		/*YAHOO.util.Event.addListener(window, "load", function() {
			reloadMilestoneGrid('ALL');
		});
		*/
		function validateNumber(evt){
			if(evt.shiftKey) return false;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			//delete, backspace & arrow keys
			if (charCode == 8 || charCode == 46 || charCode == 37 || charCode == 39)
				return true;

			if (charCode > 31 && (!((47 < charCode && charCode  < 58) || (95 < charCode &&  charCode < 106))))
				return false;
			return true;
		}

/*YK 29Mar2011 -DFIN20 */
function setMileStatus(oBj){

	var element=oBj.value;
	var elementDesc=oBj.options[oBj.selectedIndex].text
	if(element.length==0 || element=="")
		{
		 alert("<%=MC.M_PlsSel_StatChg%>");/*alert("Please select status to change.");*****/
			 document.getElementById("mileStat").value="";
			 document.getElementById("mileStatDesc").value="";
			 document.getElementById("mile_setstat").focus();
			 return false;
		}
		document.getElementById("mileStat").value=element;
		document.getElementById("mileStatDesc").value=elementDesc;
		return true;
	
}	

function createMileStone()
{
 if(!(setMileStatus(document.getElementById("mile_setstat")))){ return false;} 	
 var studyID=document.getElementById("studyId").value;
 var studyName=document.getElementById("studyName").value;
 var mileStatus=document.getElementById("mileStat").value;
 var oldmileStatus=document.getElementById("oldmileStat").value;
 var statusDesc=document.getElementById("mileStatDesc").value;
 if(oldmileStatus==mileStatus)
	{ 
		alert("<%=MC.M_TheStatAlrdy_StdSel%>");/*alert("This Status is already set in <%=LC.Std_Study %>. Please select other status to change.");*****/
		document.getElementById("mile_setstat").focus();
	    return false;
	}
 VELOS.milestoneGrid.setStudyMileStat(studyID,studyName,mileStatus,statusDesc);
 return false;
 
}
/*YK 29Mar2011 -DFIN20 */
	</script>
<!-- YK 01Aug11: Milestone Accordion Functions Starts -->
<script>
jQuery.noConflict(); /*stops conflicting code with jQuery syntax*/
jQuery(function() {

	/*Open/Closes Patient Milestone Panel*/
	jQuery( "#PatientMileStone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Visit Milestone Panel*/
	jQuery( "#VisitMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Event Milestone Panel*/
	jQuery( "#EventMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Study Milestone Panel*/
	jQuery( "#StudyMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	/*Open/Closes Additional Milestone Panel*/
	jQuery( "#AdditionalMilestone" ).accordion({
		autoHeight: false, collapsible: true, active: false, fillSpace: false, animated: false
	});
	
});
/**
 *
 * Loads Milestone Panel On-Demand 
 * param milestonetype as PM, VM, EM, SM, AM.
 */
function loadMilestoneData(mileType)
{
	var checkData = mileType+"_milestonegrid";
	if(document.getElementById(checkData).innerHTML.length<=0) /*Checks if grid is empty*/
	{
			reloadMilestoneGrid(mileType); /*Loads the grid*/
	}else
	{
		document.getElementById(checkData).innerHTML=""; /*Empty the grid when panel closes.*/
	}
	
}
</script>
<!-- Milestone Accordion Functions Ends -->

	<table width="98%" cellspacing="0" cellpadding="0" >
	<!-- SM 8Apr2011 -DFIN20 -->
	<%if (setting.size() > 0 && "1".equals(setting.get(0))){%>
	  <!-- YK 29Mar2011 -DFIN20 -->
	   <tr><td>&nbsp;</td></tr>
	  <tr> <td align="left">
	  <!--YK: 05APR2011 Bug#6007 & #6005  -->
	  <%=LC.L_StdMstone_Stat%><%--<%=LC.Std_Study %> Milestone Status*****--%>:&nbsp;<%=mileSetStat %>&nbsp;
	  <button name="set_mile_setStat" onclick="if (f_check_perm(<%=pageRight%>,'E')) { createMileStone(); }" ondblclick="return false;"><%=LC.L_Set_Status%></button>
	  </td>
 	  </tr>
 	<%} %>
 	  <input type="hidden" name="studyName" id="studyName" value="<%=studyNo%>">
 	  <input type="hidden" name="studyId" id="studyId" value="<%=studyId%>">
 	  <input type="hidden" name="mileStat" id="mileStat" >
 	  <input type="hidden" name="oldmileStat" id="oldmileStat" value="<%=studyB.getMilestoneSetStatus()%>">
 	  <input type="hidden" name="mileStatDesc" id="mileStatDesc">
 	    <tr><td>&nbsp;</td></tr>
 	    <!-- YK 29Mar2011 -DFIN20 -->
	 <!-- YK 01Aug11: Milestone Accordion Start Here -->
	  <tr><td align="center"><p class="defComments_txt"><%=MC.M_ExpdMstone_ExstNewMstone%><%--Please expand a Milestone Type panel below to manage existing  or add New Milestones*****--%></p></td></tr>
	  <tr><td>&nbsp;</td></tr>
	  </table>
	<!-- Start Patient Status MileStone --> 
	<div id="PatientMileStone">
	<h3 onclick="loadMilestoneData('PM');"><a href="#" ><%=LC.L_Pat_StatusMstones%><%--<%=LC.Pat_Patient %> Status Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
		 			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
		 		<button type="submit" id="save_changes" name="save_changes" 
		    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('PM'); }" ><%=LC.L_Preview_AndSave%></button>
		    <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%> 
	
			<td align="right">
				<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="PM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
				onkeydown = "return validateNumber(event);"/> <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
				<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('PM');">
			<span width="50%">&nbsp;</span>
			</td>
		<%} %>
		</tr>
		</table>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div width="100%" id="PM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Patient Status MileStone -->
	<!-- Start Visit Milestones --> 
	<div id="VisitMilestone">
	<h3 onclick="loadMilestoneData('VM');"><a href="#" ><%=LC.L_Visit_Mstones%><%--Visit Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
  			<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('VM'); }" ><%=LC.L_Preview_AndSave%></button>
	    <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	 	<td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="VM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/> <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('VM');"> 			<span width="50%">&nbsp;</span>
	  	</td>
	  	<%} %>
		</tr>
		</table>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div id="VM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Visit Milestones -->
	<!-- Start Event Milestones --> 
	<div id="EventMilestone">
	<h3 onclick="loadMilestoneData('EM');"><a href="#" ><%=LC.L_Evt_Mstone%><%--Event Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
  			<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('EM'); }" ><%=LC.L_Preview_AndSave%></button>
	   <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	 	<td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="EM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/>  <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>			
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('EM');">  
			<span width="50%">&nbsp;</span>
	  	</td>
	  	<%} %>
		</tr>
		</table>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div id="EM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Event Milestones -->
	<!-- Start Study Status Milestones --> 
	<div id="StudyMilestone">
	<h3 onclick="loadMilestoneData('SM');"><a href="#" ><%=LC.L_StdPat_Mstone%><%--<%=LC.Std_Study %> Status Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
	    	<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('SM'); }" ><%=LC.L_Preview_AndSave%></button>
	    <%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	 	<td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="SM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/>  <%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('SM');">   
			<span width="50%">&nbsp;</span>
	  	</td>
	  	<%} %>
		</tr>
		</table>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div id="SM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Study Status Milestones --> 	 
	<!-- Start Additional Milestones --> 
	<div id="AdditionalMilestone">
	<h3 onclick="loadMilestoneData('AM');"><a href="#" ><%=LC.L_Addl_Mstones%><%--Additional Milestones*****--%></a></h3> <%-- YK(05Aug11):Bug#6759  --%>
	<div >
		<table width="98%" cellspacing="0" cellpadding="0" >
		<tr>
		  <td>
			<%if (isAccessibleFor(pageRight, 'N')||isAccessibleFor(pageRight, 'E')){%> 
  			<FONT class='Mandatory'>(<%=MC.M_ClkRow_ToEdt%><%--Click row to edit*****--%>)</FONT>&nbsp;&nbsp;&nbsp;&nbsp;
			<button type="submit" id="save_changes" name="save_changes" 
	    	onclick="if (f_check_perm_noAlert(<%=pageRight%>,'N') || f_check_perm_noAlert(<%=pageRight%>,'E')) { VELOS.milestoneGrid.saveDialog('AM'); }" ><%=LC.L_Preview_AndSave%></button>
		<%} %>
		  </td>
		
		<%if (isAccessibleFor(pageRight, 'N')){%>
	  <td align="right">
	  		<%=LC.L_Add%><%--Add*****--%> &nbsp;<input id="AM_rowCount" type="text" value="0" size="2" maxlength="2" class="index"
	  		onkeydown = "return validateNumber(event);"/>
	  		<%=LC.L_Add_Mstone_S%><%--Add Milestone(s)*****--%>
			<img src="../images/add.png"/ title="<%=LC.L_Add%>" onclick="VELOS.milestoneGrid.addRows('AM');">    
			<span width="50%">&nbsp;</span>
	  </td>
	  <%} %>
	 </tr>
		</table>
		<table width="98%" cellspacing="0" cellpadding="0" >
		  <tr>
			<td>
				<div id="AM_milestonegrid"></div>
			</td>
		  </tr>
		</table>
	</div>
	</div>
	<!-- End Additional Milestones --> 
	<!-- Milestone Accordion Ends Here -->
	<%} %>
	<%}else{//page right%>
		<div class=formdefault>
			<BR/>
			<table>
				<tr> 
					<td>
						<p class = "sectionHeadings"><font color="red"> <%=MC.M_UnfortStd_LvlMstone%><%--Unfortunately you do not have <%=LC.Std_Study%> level Milestones access.*****--%> </font></p>
						<p class = "defComments"> <%=MC.M_ObtainRgt_RptErr%><%--To obtain appropriate rights or to report an error, please contact your account administrator.*****--%> <BR></p>
					</td>
				</tr>
			</table>
			<BR>
		</div>
	<%}%>
	</DIV>
</body>
<%}%>
<%}//end of if body for session
	else{%> 
	 <jsp:include page="timeout.html" flush="true" /> 
<%}%>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
<div class="mainMenu" id="emenu"><jsp:include page=getmenu.jsp flush="true" /></div>



</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>