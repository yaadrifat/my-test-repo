<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false;
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) {
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title> <%=MC.M_StdVer_SecDets%><%--<%=LC.Std_Study%> >> Version >> Section Details*****--%></title>
<% } %>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%@ page import="com.velos.eres.service.util.LC"%>
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="sectionB" scope="page" class="com.velos.eres.web.section.SectionJB" />
<jsp:useBean id="codelstB" scope="page" class="com.velos.eres.web.codelst.CodelstJB" />
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="./FCKeditor/fckeditor.js"></script>
<script type="text/javascript">
	var fck;
	var oFCKeditor;
   function init()
      {

      oFCKeditor = new FCKeditor("sectionContentsta") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = 800 ;
      oFCKeditor.Height = editorHeight ; // 400 pixels
      oFCKeditor.ToolbarSet="Velos1";
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;

      }
      var editor;
  function FCKeditor_OnComplete(oFCKeditor)
{
	 fck= FCKeditorAPI.GetInstance('sectionContentsta');


}

      </script>
</head>

<% String src="";
String from = "sectionver";
src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<SCRIPT Language="JavaScript">


 function  validate(mode, formobj){

   if((mode == 'N') && formobj.dd_text[0].checked)
   {
    if (!(validate_col('Section Name',formobj.sectionNameDD  ))) return false
   }
   else
   {
     if (!(validate_col('Section Name',formobj.sectionName))) return false
   }

   	//replace special chracters with html codes
	var htmlStr;
	if (!IsNetscape()) htmlStr=fck.GetXHTML( true );
	else htmlStr=formobj.sectionContentsta.value;

	if (htmlStr.length>0){

	if (htmlStr.indexOf("\"")>=0)
	{
	  htmlStr=replaceSubstring(htmlStr,"\"",'&#34;');

	  }
	if (htmlStr.indexOf("&quot;")>=0)
	{
	  htmlStr=replaceSubstring(htmlStr,"&quot;",'&#34;');

	 }

	 htmlStr=replaceSubstring(htmlStr,"\n","");


	  formobj.sectionContentsta.value=htmlStr;

	 }

	//end replacing

    if (checkChar(formobj.sectionContentsta.value,"'"))
	 {
    	var paramArray = ["'"];
		alert(getLocalizedMessageString("M_SplCharNotAllowed", paramArray));/*alert("Special Character(') not allowed for this Field");*****/
	   if (!IsNetscape())   fck.EditorWindow.focus();
	  else formobj.sectionContents.focus();

	    return false;
	 }
	formobj.sectionContents.value=formobj.sectionContentsta.value;

    if (!(validate_col('e-Signature',formobj.eSign))) return false

   if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
  }


 function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}

function fMakeTextDisabled(formobj){

   formobj.template.readOnly= false;
   formobj.sectionName.readOnly = false;

   formobj.sectionNameDD.value = '';
   formobj.dd_text[1].checked = true;

}

function fMakeDDDisabled(formobj){


formobj.sectionName.value="";
formobj.template.checked=false;
formobj.dd_text[0].checked = true;
formobj.template.readOnly= true;
formobj.sectionName.readOnly = true;


}

//by salil
function test(formobj){
if(formobj.dd_text[0].checked ){

formobj.sectionName.value="";
formobj.template.checked=false;
formobj.dd_text[0].checked = true;
formobj.template.readOnly= true;
formobj.sectionName.readOnly = true;

}
}

</SCRIPT>

<body>
<DIV class="BrowserTopn" id="div1">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
<jsp:param name="from" value="<%=from%>"/>
</jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1">
  <%

 HttpSession tSession = request.getSession(true);

 String mode="";

String tab="";
String studyVerId = "";

 int sectionId= 0;

 String acc = (String) tSession.getValue("accountId");


if(sessionmaint.isValidSession(tSession))
   {

	int accId = EJBUtil.stringToNum(acc);
	int pageRight = 0;
	mode = request.getParameter("mode");
	tab = request.getParameter("selectedTab");
	studyVerId = request.getParameter("studyVerId");
	studyVerB.setStudyVerId(EJBUtil.stringToNum(studyVerId));
	studyVerB.getStudyVerDetails();

	String verStatus = "";
	String verNumber = studyVerB.getStudyVerNumber();

	verStatus = statHistoryB.getLatestStatus(EJBUtil.stringToNum(studyVerId),"er_studyver");
	if(verStatus == null)
		verStatus = "W";


	sectionId = EJBUtil.stringToNum(request.getParameter("sectionId"));

	String study = (String) tSession.getValue("studyId");
      if(study == "" || study == null) {
%>
  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%
	   } else {

	    	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
		   }else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
	   	}
	 }

	if (pageRight > 0)
	   {
		String sectionName = "" ;
		String sectionNumber = "" ;
		String sectionContents = "" ;
		String sectionPublicFlag = "" ;
		String sectionSequence="";
		if (mode.equals("M"))
		   {
			sectionB.setId(sectionId);
			sectionB.getSectionDetails();
			sectionName = sectionB.getSecName();
			sectionNumber = sectionB.getSecNum();
			if(sectionNumber == null){
			sectionNumber = "";
			}
			sectionContents = sectionB.getContents();
			sectionPublicFlag = sectionB.getSecPubFlag();
			sectionSequence = sectionB.getSecSequence();
		   }
%>

  <Form name="section" id = "sectionForm" method="post" action="updatesection.jsp" onsubmit = "if (validate('<%=mode%>', document.section)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}" >
  <input type="hidden" name="studyVerId" value="<%=studyVerId%>">
    <input type="hidden" name="srcmenu" value="<%=src%>">
    <input type="hidden" name="selectedTab" value="<%=tab%>">
	<input type="hidden" name="sectionSequence" value="<%=sectionSequence%>">
    <%

		if (mode.equals("M"))
		   {
%>
    <input type="hidden" name="sectionId" size = 20  value = '<%=sectionId%>' >
    <input type="hidden" name="mode" size = 20  value = '<%=mode%>' >
    <% if (verStatus.equals("F")) { %>
        <P class = "sectionHeadings"> <%=LC.L_Std_Sec%><%--<%=LC.Std_Study%> Section*****--%> &nbsp;<FONT class="Mandatory"><%=MC.M_CntChg_InFreezeVer%><%--You cannot make any changes in a Freeze Version.*****--%></Font></P>
	 <% }%>

 <%if ((pageRight==4) || (pageRight==5)) {%>
   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_ChgCntSvd_WithoutRgt%><%--Any changes made cannot not be saved without appropriate Rights.--%></Font></P>
<%}%>
	<table width="500" cellspacing="0" cellpadding="0">
	<tr><td><br></td></tr>
	<tr>
	 <td width="150"> <%=LC.L_Version_Number%><%--Version Number*****--%> </td>
	 <td><%=verNumber%>   </td>
	   </tr>
	 <tr><td><br></td></tr>
	 <tr>
        <td width="150"><%=LC.L_Std_SectionNumber%><%--<%=LC.Std_Study%> Section Number*****--%> </td>
		 <td>
          <input type="text" name="sectionNumber" size=20 MAXLENGTH = 100 value="<%=sectionNumber%>" >
        </td>
	</tr>

      <tr>
        <td width="150"><%=LC.L_Sec_Name%><%--Section Name*****--%><FONT class="Mandatory">* </FONT> </td>
        <td>
          <input type="text" name="sectionName" size=20 MAXLENGTH = 100 value="<%=sectionName%>" >
        </td>
      </tr>
	  </table>
	  <table>
      <tr>
        <td width="300"> <%=LC.L_Sec_Contents%><%--Section Contents*****--%></td>
	  </tr>
	  </table>
	  <table>
	  <tr>
        <td>
          <TextArea id="sectionContentsta" name="sectionContentsta" rows=5 cols=80 wrap="hard" style="width:800;height:190;"><%=StringUtil.htmlEncode(sectionContents) %></TextArea>
	  <input name="sectionContents" type="hidden" value='<%=StringUtil.htmlEncode(sectionContents) %>'>
        </td>
      </tr>
      <tr>
        <td colspan=2>
          <p class = "defComments"><%=MC.M_SecLimit_20000Char%><%--Each section has a maximum limit of 20,000
            characters*****--%></P>
        </td>
      </tr>
      <tr>
        <td width="500" colspan=2> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to be available to the public?*****--%> </td>
      <tr>
        <td colspan=2>
          <input type="Radio"  name="sectionPubFlag" value='Y'

<%
		if((sectionPublicFlag!=null)&&(sectionPublicFlag.equals("Y")))
		   {
%>
checked
<%
		   }
%>
>
          <%=LC.L_Yes%><%--Yes*****--%>
          <input type="Radio" name="sectionPubFlag" value='N'

<%
		if((sectionPublicFlag!=null)&&(sectionPublicFlag.equals("N")))
		   {
%>
checked
<%
		   }
%>
>
          <%=LC.L_No%><%--No*****--%> &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_WhatPub_NonPubInfo%><%--What
          is Public vs Non Public Information?*****--%></A> </td>
      </tr></tr>
    </table>
    <%
		} else {
		   CodeDao codeDao = codelstB.getCodelstData(accId, "section");
		   ArrayList codelstDescs = codeDao.getCDesc();

		   String codelstDesc = "";



		   ArrayList cHides = new ArrayList();
		   cHides = codeDao.getCodeHide();
		   String codeHide  = "";

		   int rows = codeDao.getCRows();
%>
    <input type="hidden" name="mode" size = 20  value = '<%=mode%>' >
    <!--P class = "sectionHeadings"> <%=LC.Std_Study%> Section </P-->
    <table width="500" cellspacing="0" cellpadding="0" border=0 >
    <!-- Virendra fixed Bug No. 4690, Added a row<tr> with <br>space -->
    <tr><td><br></td></tr>
		<tr>
	        <td width="300"><%=LC.L_Std_SectionNumber%><%--<%=LC.Std_Study%> Section Number*****--%>&nbsp;</td>
	        <td valign=left><input  type="text" name="sectionNumber" size=20 MAXLENGTH = 100 ></td>
			<td width="300"></td>
		</tr>
      	<tr>
        	<td width="300"><%=LC.L_Sec_Name%><%--Section Name*****--%><FONT class="Mandatory">* </FONT> </td>
      	</tr>
    </table>
    <table width="500" cellspacing="0" cellpadding="0" >
      <tr>
        <td width="50" align=center>
          <input  type=radio name=dd_text value='dd'  checked onClick=fMakeDDDisabled(document.section) >
        </td>
        <td>
          <select  name='sectionNameDD' onClick=fMakeDDDisabled(document.section)>
          <option value=''><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
<%
		for(int count=0; count<rows ; count++) {
		   codelstDesc = (String) codelstDescs.get(count);
		   codeHide   = (String) cHides.get(count);

		   if (codeHide.equals("N")){
			%>
            	<option value='<%=codelstDesc%>'><%=codelstDesc%></option>
            <%
		   }
		}
%>
          </select>
        </td>
      </tr>
      <tr>
        <td width="50" align=center>
          <input  type=radio name=dd_text value='text' onClick=fMakeTextDisabled(document.section) >
        </td>
        <td>
		  <input  type="text" name="sectionName"  readonly size=30 MAXLENGTH = 100 value="<%=sectionName%>" >
          <input type="checkbox" name=template readonly onclick=test(document.section)  >
          <%=MC.M_Add_ToTemplateList%><%--Add to Template List*****--%> </td>
      </tr>
	  </table>
	  <table>
	  <tr>
        <td> <%=LC.L_Sec_Contents%><%--Section Contents*****--%> </td>
      </tr>
      <tr>
        <td>
          <TextArea id="sectionContentsta" name="sectionContentsta" rows=5 cols=80 wrap="hard" style="width:800;height:190;"><%=StringUtil.htmlEncode(sectionContents) %></TextArea>
	  <input name="sectionContents" type="hidden" value='<%=StringUtil.htmlEncode(sectionContents) %>'>
        </td>
      </tr>
	  </table>
	  <table>
	  <tr>
        <td width="500" colspan=2>
          <p class = "defComments"><%=MC.M_SecLimit_20000Char%><%--Each section has a maximum limit of 20,000
            characters*****--%></P>
        </td>
      </tr>
      <tr>
        <td width="500" colspan=2> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want information in this section to
          be available to the public?*****--%> </td>
      <tr>
        <td colspan=2>
          <input type="Radio" name="sectionPubFlag" value='Y'

<%
		if((sectionPublicFlag!=null)&&(sectionPublicFlag.equals("Y")))
		   {
%>
checked
<%
		   }
%>
>
          <%=LC.L_Yes%><%--Yes*****--%>
          <input  type="Radio" name="sectionPubFlag" value='N' checked >
          <%=LC.L_No%><%--No*****--%> &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_WhatPub_NonPubInfo%><%--What
          is Public vs Non Public Information?*****--%></A> </td>
      </tr></tr>
	  </table>
    <%
		}
%>


<% if  ((mode.equals("N")) || (((!(mode.equals("N"))) && (!(pageRight==4)) && (!(pageRight==5)) && (!verStatus.equals("F")) ))) {    %>
	<table width=100% cellpadding=0 cellspacing=0>
	<tr valign=baseline>
		<td>
			<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="sectionForm"/>
				<jsp:param name="showDiscard" value="N"/>
			</jsp:include>
		</td>
	</tr>
	</table>
<%}%>


<script>
init();
</script>
  </Form>

<%
	} else {      //end of else body for page right
%>

	<jsp:include page="studytabs.jsp" flush="true"/>

			<jsp:include page="accessdenied.jsp" flush="true"/>



<%
	}
} else {            //end of if body for session

%>

<jsp:include page="studytabs.jsp" flush="true"/>

<jsp:include page="timeout.html" flush="true"/>
<%

}

%>

<div class = "myHomebottomPanel">
   <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<div class="mainMenu" id="emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>
</html>