<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@page import="java.util.ArrayList,java.util.LinkedHashMap"%>
<%@page import="com.velos.eres.service.util.StringUtil"%>
<%@page import="com.velos.eres.ctrp.business.CtrpValidationDao,com.velos.eres.ctrp.business.CtrpValidationJB"%>
<%@page import="com.velos.eres.ctrp.web.CtrpDraftValidationJB,com.velos.eres.ctrp.web.CtrpDraftValidationPojo,com.velos.eres.web.user.UserJB"%>
<%!
private static final String EMPTY_TABLE = "<table></table>";
%>
<link Rel="stylesheet" HREF="./styles/velos.css" type="text/css" >
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<jsp:include page="velos_includes.jsp"></jsp:include>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<head>
<title><%=LC.L_CTRPDraftReport%><%--CTRP Draft Report*****--%> </title>
</head>
<script>
$j(document).ready(function () {
	var windowHeight = $j(document).height();
    var windowWidth = $j(window).width();
	$j( "#dialog:ui-dialog" ).dialog( "destroy" );
	$j( "#mainWindow" ).dialog({
		height: windowHeight,
		width: windowWidth,
		modal: true
	});
	$j('.ui-dialog-titlebar').hide();
});

function hidePrintImg() {
	$j("#printImg1").hide();
	$j("#printImg2").hide();
    window.print();
    $j("#printImg1").show();
    $j("#printImg2").show();
}
</script>

<body>
<%
	HttpSession tSession = request.getSession(false);	
	UserJB user = (UserJB) tSession.getValue("currentUser");    //Added to solve Bug#8720 : Raviesh
	if (!sessionmaint.isValidSession(tSession)){
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
</body>
<%		
		return;
	} // End of session timeout
	
	Integer draftId = StringUtil.stringToInteger(request.getParameter("draftId"));
	String radioFieldValue="";
	if (draftId == null || draftId == 0) {
%>
</body>
<%
		return;
	} // End of if clause
	CtrpDraftValidationJB validationJB = new CtrpDraftValidationJB(draftId);	
	validationJB.setLoggedUser(user.getUserId());      //Added to solve Bug#8720 : Raviesh
	int output = validationJB.runValidation(validationJB);
	if (output < 0) {
%>
<br/><br/><br/><br/>
<p align="center"><%=MC.M_ErrGetResp_CnctVelos %></p>
</body>
<%
		return;
	}
	LinkedHashMap<String,ArrayList<CtrpDraftValidationPojo>> validationMsgMap = validationJB.getValidationMsgMap();
%>
<form>
<div>
	<table width="100%">
		<tr>
			<td width="95%" align="right">
				<a id="print_link1" href="javascript:hidePrintImg();">
				<img height="20px"  id="printImg1" style="padding-top:5px; border:none; valign:bottom" src="./images/printer.gif" title="<%=LC.L_Print%>"/>
				</a>
			</td>
			<td width="5%"></td>
		</tr>
	</table>
</div>
<div class="col_100 column" id='validationPopup' >
<%	
	int sectionNo = 1;			
	for (String key : validationMsgMap.keySet()) {
%>
	<div id="ctrp_draft<%=sectionNo%>_section" class="portlet portletstatus ui-widget-border 
				ui-helper-clearfix ui-corner-all" style="margin-right:7pt" >
		<div id="ctrp_draft<%=sectionNo%>content" onclick="toggleDiv('ctrp_draft<%=sectionNo%>')" 
					class="portlet-header portletstatus 
					ui-widget-header ui-widget-content  ui-helper-clearfix ui-corner-all" >
			<%=key%>
			<span style="width:15px;" class="ui-icon ui-icon-triangle-1-s"></span>
		</div>
		<div id='ctrp_draft<%=sectionNo++%>'>
		<table class="printer-font-size">
		
<%		ArrayList<CtrpDraftValidationPojo> msgAL = validationMsgMap.get(key);
		for (CtrpDraftValidationPojo validationObj : msgAL) {
%>
			<tr>
			<%if(validationObj.getIsSubHeading()){ %>
				<td width="25%" align="right" height="20px"><%=validationObj.getLabel()%></td>
			<% }else{%>
				<td width="25%" align="right" ><%=validationObj.getLabel()%></td>
				<% if (validationObj.getIsMandatory()) { %>
				<td><FONT class='Mandatory' color='red'>*</FONT>&nbsp;</td>
				<% } else { %>
				<td>&nbsp;&nbsp;</td>
				<% }  %>
				<td width="33%" class="readonly-input"><%=StringUtil.isEmpty(validationObj.getValue()) ? EMPTY_TABLE : StringUtil.htmlEncodeXss(validationObj.getValue())%></td>
				<% if (validationObj.getMessage() == null) { %>
				<td><img src='../images/jpg/Done.gif'/></td><td width="40%"></td>
				<% } else { %>
				<td><img src='../images/jpg/delete_pg.png'/></td><td width="40%"><%=StringUtil.htmlEncodeXss(validationObj.getMessage())%></td>
				<% }}%>
			</tr>
<%		}   %>
		</table>
		</div>
	</div>
<%	}	%>
	<div class="portlet" style="border:none">
		<div>
		<table width="100%">
			<tr><td></td></tr>
			<tr>
				<td width="96%" align="right">
					<a id="print_link2" href="javascript:hidePrintImg();">
					<img height="20px" id="printImg2" style="padding-top:5px; border:none; valign:bottom" src="./images/printer.gif" title="<%=LC.L_Print%>"/>
					</a>
				</td>
				<td width="4%"></td>
			</tr>
		</table>
		</div>
	</div>
</div>
<%-- End of col_100 maincontain div --%>
</form>
</body>

