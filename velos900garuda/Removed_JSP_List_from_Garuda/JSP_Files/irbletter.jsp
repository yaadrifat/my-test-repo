<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:include page="include.jsp" flush="true"/>
<title><%=MC.M_Ltr_FromRevBoard%><%--Letter from Review Board*****--%></title>
</head>
<body>
<DIV style="font-family:Verdana,Arial,Helvetica,sans-serif">
<form name="gen" style="margin:0px" action="velos-doc.pdf" method="post" target="preview">
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>
<%@ page import="java.util.ArrayList,java.util.Calendar,java.util.Hashtable,java.text.DecimalFormat" %>
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    String sessId = tSession.getId();
    if (sessId.length()>8) { sessId = sessId.substring(0,8); }
    sessId = Security.encrypt(sessId);
    char[] chs = sessId.toCharArray();
    StringBuffer sb = new StringBuffer();
    DecimalFormat df = new DecimalFormat("000");
    for (int iX=0; iX<chs.length; iX++) {
        sb.append(df.format((int)chs[iX]));
    }
    String keySessId = sb.toString();

    CodeDao codeDao = new CodeDao();
    codeDao.getCodeValues("subm_status");
    ArrayList codeSubtypes1 = codeDao.getCSubType();
    ArrayList cIds1 = codeDao.getCId();
    String resubmittedStatus = "";
    for(int iX=0; iX<codeSubtypes1.size(); iX++) {
        if ("resubmitted".equals((String)codeSubtypes1.get(iX))) {
            resubmittedStatus = ""+cIds1.get(iX);
        }
    }
    boolean isMSIE = false;
    if (request.getHeader("USER-AGENT").toLowerCase().indexOf("msie") != -1) { isMSIE = true; }
    CodeDao cdSubmStatus = new CodeDao();
    cdSubmStatus.getCodeValues("subm_status");
    cdSubmStatus.setCType("subm_status");
    String cdStatusPowerBarMenu = cdSubmStatus.toPullDown("submissionStatus");

    String accountId = (String)tSession.getAttribute("accountId");
    String studyId = request.getParameter("studyId");
	tSession.setAttribute("studyId", StringUtil.htmlEncodeXss(studyId));
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String usrId = (String) tSession.getValue("userId");
    String usrFullName = UserDao.getUserFullName(EJBUtil.stringToNum(usrId));
    String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
    String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
    String submissionStatus = StringUtil.htmlEncodeXss(request.getParameter("submissionStatus"));
    String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
    
    EIRBDao eIrbDao = new EIRBDao();
    eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(defUserGroup));
    ArrayList boardNameList = eIrbDao.getBoardNameList();
    ArrayList boardIdList = eIrbDao.getBoardIdList();
    
    if (submissionPK == null || submissionPK.length() == 0 
            || submissionBoardPK == null || submissionBoardPK.length() == 0) { 
%>
<script>
  self.close();
</script></form></body></html>
<%      return;
    }  %>
<%
    String reviewBoardName = null;
	String fkReviewBoard = null;
	String reviewType = null;
    for(int iBoard=0; iBoard<boardNameList.size(); iBoard++) {
        submBoardB.findByPkSubmissionBoard(EJBUtil.stringToNum(submissionBoardPK));
        fkReviewBoard = submBoardB.getFkReviewBoard();
        reviewType = submBoardB.getSubmissionReviewType();
        if (fkReviewBoard.equals(boardIdList.get(iBoard))) {
            reviewBoardName = (String) boardNameList.get(iBoard);
            break;
        }
    }
    if (reviewBoardName == null) { reviewBoardName = "Review Board"; }
%>
<table width="100%" cellspacing="0" cellpadding="0" Border="0">
<tr><td><br /></td></tr>
<tr><td><P class="sectionHeadings"><%=LC.L_Letter_From%><%--Letter from*****--%> <%=reviewBoardName%></P></td></tr>
</table>
<table width="90%" cellspacing="0" cellpadding="0" Border="0" style="font-size:8pt" align="center">
<tr><td><br /></td></tr>
<tr align="center">
  <input type="hidden" id="submissionPK" name="submissionPK" value="<%=EJBUtil.stringToNum(submissionPK)%>" />
  <input type="hidden" id="submissionBoardPK" name="submissionBoardPK" value="<%=EJBUtil.stringToNum(submissionBoardPK)%>" />
  <input type="hidden" id="submissionStatus" name="submissionStatus" value="<%=EJBUtil.stringToNum(submissionStatus)%>" />
  <input type="hidden" id="fkReviewBoard" name="fkReviewBoard" value="<%=fkReviewBoard%>" />
  <input type="hidden" id="reviewType" name="reviewType" value="<%=reviewType%>" />
  <input type="hidden" id="accountId" name="accountId" value="<%=tSession.getAttribute("accountId")%>" />
  <input type="hidden" id="selectedTab" name="selectedTab" value="<%=selectedTab%>" />
  <input type="hidden" id="labelType" name="labelType" value="proviso" />
  <input type="hidden" id="key" name="key" value="<%=keySessId%>" />
  <td>
</tr>
</table>
  <iframe name="preview" src="" 
		  width="100%" height="98%" frameborder="1" scrolling="yes" allowautotransparency=true>
    <%=MC.M_PDFBeing_Generated%><%--PDF file is being generated*****--%>...
  </iframe>
<script>
  document.gen.submit();
</script>

<%    
} 
else { // session invalid
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</form>
</body>
</html>

