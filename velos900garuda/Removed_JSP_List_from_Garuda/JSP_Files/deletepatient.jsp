<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.*"%>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE><%=LC.L_Delete_Patient%><%--Delete <%=LC.Pat_Patient%>*****--%></TITLE>
</HEAD>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>


<% String src;

src= request.getParameter("srcmenu");


%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id ="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/> 

<body>

<br>

<DIV class="formDefault" id="div1">

<% 

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))	{
	
	String patientId=request.getParameter("patientId");
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
	
	
%>

	<FORM name="deleteUsr" method="post" action="delete_patient.jsp" onSubmit="return validate(document.deleteUsr)">
	<br><br>
	
	<P class="defComments">	<%=MC.M_YouAbtToDel_Pat%><%--You are about to delete <%=LC.Pat_Patient%>*****--%> &nbsp;<font color="red"><B> <%=patientId%></B></font>.  <%=MC.M_OnceDelCnt_PatEsign%><%--Once deleted, the data cannot be retrieved.  If you are sure that you would like to delete this <%=LC.Pat_Patient_Lower%>, enter your e-signature below and click 'Submit'.*****--%></P>	
	
	<TABLE width="100%" cellspacing="0" cellpadding="0" >
	<tr>
	   <td width="40%">
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT> 
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 >
	   </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		 <button type="submit"><%=LC.L_Submit%></button>
		</td>
	</tr>
	</table>
	 <input type="hidden" name="delMode" value="<%=delMode%>">
	 <input type ="hidden" name="patientId" value="<%=patientId%>">

	</FORM>
	<% } else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"> </jsp:include>	

<%
				} else {
		//Modified for INF-18183 ::: Akshi
		personB.deletePatient(EJBUtil.stringToNum(patientId),AuditUtils.createArgs(session,""));

%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "successfulmsg" align = center> <%=MC.M_PatDel_Succ%><%--The <%=LC.Pat_Patient%> was deleted successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="3; URL=allPatient.jsp?searchFrom=initial&selectedTab=1&srcmenu=<%=src%>">
<%
}
}
}//end of if body for session	

  

else

{

%>

<jsp:include page="timeout.html" flush="true"> </jsp:include> 

<%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"> </jsp:include>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page=getmenu.jsp flush="true"> </jsp:include> 
</div>

</body>

</HTML>

