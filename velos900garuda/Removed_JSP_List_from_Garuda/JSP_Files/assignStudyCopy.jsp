<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML> 
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> <%=LC.L_Assign_Users%><%--Assign Users*****--%> </TITLE>
</HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<body>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*,java.lang.*,java.util.*,java.text.*"%>
<%
String eSign = request.getParameter("eSign");
   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))

	{ %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	

	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	
	String userx = request.getParameter("userId");
	String src = request.getParameter("srcmenu");
	String tab = request.getParameter("selectedTab");
	String permission = request.getParameter("perGranted");
	String study = (String) tSession.getValue("studyId");

	//to create snapshot of the study if it does not exist
	%>
	<jsp:include page="getsnapshot.jsp" flush="true"> 

	<jsp:param name="studyId" value="<%=study%>"/>
	<jsp:param name="refreshFlag" value="F"/>

	</jsp:include>  
	
	<%
	
	studyB.setId(EJBUtil.stringToNum(study));
	studyB.getStudyDetails();
	
	studyB.copyStudy(EJBUtil.stringToNum(study),EJBUtil.stringToNum(userx),EJBUtil.stringToNum(studyB.getStudyAuthor()),permission,0);

%>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<p class = "sectionHeadings" align = center> <%=MC.M_Perm_GrantedSucc%><%--The permission was granted successfully*****--%></p>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=teamBrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>">
<%
}//end of if for eSign check	
}//end of if body for session

else

{

%>
<jsp:include page="timeout.html" flush="true"/>
<%

}

%>
</body>
</HTML>
