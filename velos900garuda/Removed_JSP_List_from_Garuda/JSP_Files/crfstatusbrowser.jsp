<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<script>
 function openWinStatus(patId,patStatPk,pageRight,study,orgRight)  
{

	changeStatusMode = "yes";
	if (f_check_perm_org(pageRight,orgRight,'N')) 
	{
		windowName= window.open("patstudystatus.jsp?studyId="+study+"&changeStatusMode=yes&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=550,height=560");
		windowName.focus();
	}
}

</script>	
<title><%=LC.L_Crf_StatusBrowser%><%--CRF Status Browser*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src="";
src= request.getParameter("srcmenu");
%>	
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crfStatB" scope="request" class="com.velos.esch.web.crfStat.CrfStatJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />
<jsp:useBean id="patB" scope="request" class="com.velos.eres.web.patStudyStat.PatStudyStatJB"/>
	
<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.esch.business.common.CrfStatDao,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<body>
<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
   	{
		int pageRight=0;
		int orgRight = 0;
		int siteId = 0;
		Integer rt = null;
		rt = (Integer)tSession.getValue("studyManagePat");
    	pageRight = rt.intValue();

		String visit = request.getParameter("visit");
		String visitName=request.getParameter("visitName");
		visitName=(visitName==null)?"":visitName;
		String eventName = request.getParameter("eventname");
		String patProtId= request.getParameter("patProtId");
		String studyId = (String) tSession.getValue("studyId");	
		String statDesc=request.getParameter("statDesc");
		String statid=request.getParameter("statid");	
		String studyVer=request.getParameter("studyVer");
		String formType = request.getParameter("formType");
		String status="";
		
		int personPK = 0;	
		personPK = EJBUtil.stringToNum(request.getParameter("pkey"));
	    personB.setPersonPKId(personPK);
 		personB.getPersonDetails();
		String uName = (String) tSession.getValue("userName");
		String patientId = personB.getPersonPId();
		
		siteId = EJBUtil.stringToNum(personB.getPersonLocation());
		String userIdFromSession = (String) tSession.getValue("userId");	
		
		orgRight = userSiteB.getMaxRightForStudyPatient(EJBUtil.stringToNum(userIdFromSession), personPK , EJBUtil.stringToNum(studyId) );
    
		if (orgRight > 0)
		{
			System.out.println("patient crf br orgRight" + orgRight);
			orgRight = 7;
		}
		orgRight = userSiteB.getRightForUserSite(EJBUtil.stringToNum(userIdFromSession),siteId);

		String eventId = request.getParameter("eventId");
		tSession.setAttribute("eventId",eventId);
		
		CrfStatDao crfStatDao = new CrfStatDao();
		crfStatDao = crfStatB.getCrfValues(EJBUtil.stringToNum(patProtId), EJBUtil.stringToNum(visit)) ;
		int lenCrf = crfStatDao.getCRows();
		if(lenCrf==0){
		status="Checked=true";		
		}

		if(eventName == null){
			eventName = "";
		} else {
			eventName = eventName.replace('~',' ');
		}			

		//get current patient status and its subtype
    			
    		String patStatSubType = "";
			String patStudyStat =  "";
			int patStudyStatpk = 0;
			
			patStatSubType = patB.getPatStudyCurrentStatusWithCodeSubType(studyId, String.valueOf(personPK));
			patStudyStat = patB.getPatStudyStat();
			patStudyStatpk = patB.getId();
			statid = patStudyStat ;
			if (EJBUtil.isEmpty(patStatSubType))
				patStatSubType= "";
							
			///////////////////////////////////		

		
%>
<br>
<DIV class="browserDefault" id="div1"> 
	
<P class = "userName"> <%= uName %> </P>
<%
	if(formType.equals("crf")){
%>
<P class="sectionHeadings"> <%=MC.M_MngPatsSch_CrfStat%><%--Manage <%=LC.Pat_Patients%> >> Schedule >> CRF Status*****--%> </P>
<%
	} else {
%>
<P class="sectionHeadings">  <%=MC.M_MngPatsSch_OthFrm%><%--Manage <%=LC.Pat_Patients%> >> Schedule >> Other Forms*****--%> </P>
<%
	} 
%>

<jsp:include page="patienttabs.jsp" flush="true"> 
<jsp:param name="studyVer" value="<%=studyVer%>"/>
<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientId)%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
<br>
<%
	if(formType.equals("crf")){
%>
<table width=100%>
<tr>
	<td class=tdDefault width = 20% ><B><%=LC.L_Patient_Visit%><%--<%=LC.Pat_Patient%> Visit*****--%>: <%=visitName%></B></td>
</tr>

</table>
<%
	}
%>


<%
	if(lenCrf==0 && formType.equals("other")) 
	{
	
	//Modified by Manimaran for November Enhancement PS4.
	if((patStatSubType.trim().equalsIgnoreCase("lockdown")))
	{ %>
		<P class = "defComments"><FONT class="Mandatory"><%=MC.M_PatStdStat_Lkdwn%><%--<%=LC.Pat_Patient%>'s study status is 'Lockdown'. You cannot change make any changes.*****--%> </FONT> 
		<A onclick="openWinStatus('<%=personPK%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=studyId%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A> 
		<FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>					
		</P>
	<%	
	}	
	else
	{	
		
%>		
<table>
<tr>
<td class=tdDefault width = 20% >
<%=MC.M_NoFrmsAttached_PcolCal%><%--There are no Forms attached to this Protocol Calendar. Please click*****--%> <A onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'N')" HREF="crfstatus.jsp?srcmenu=<%=src%>&selectedTab=3&mode=N&pkey=<%=personPK%>&visit=<%=visit%>&visitName=<%=visitName%>&eventName=<%=eventName%>&page=patientEnroll&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=StringUtil.encodeString(patientId)%>&formType=<%=formType%>"><%=MC.M_HereAdd_NewFrm%><%--here</A> to add a new form.*****--%>
</td>
</tr>
</table>
<%
	} //for else when status is not offstudy
} else {   // else of if(lenCrf==0 && formType.equals("other")) 
%> 

<% 
	//Modified by Manimaran for November Enhancement PS4.
	if((patStatSubType.trim().equalsIgnoreCase("lockdown")))
	{ %>
		<P class = "defComments"><FONT class="Mandatory"><%=MC.M_PatStdStat_Lkdwn%><%--<%=LC.Pat_Patient%>'s study status is 'Lockdown'. You cannot change make any changes.*****--%> </FONT> 
		<A onclick="openWinStatus('<%=personPK%>','<%=patStudyStatpk%>','<%=pageRight%>','<%=studyId%>','<%=orgRight%>')"  href="#"  ><%=LC.L_Click_Here%><%--Click Here*****--%></A> 
		<FONT class="Mandatory"> <%=MC.M_ToChg_TheStat%><%--to change the status.*****--%></FONT>					
		</P>
<%	
}
		 %>	
<table width=100%>
<tr>
	<td width = 50%>
		<P class = "defComments"> 
<%
	if(formType.equals("crf")){
%>		
		<%=MC.M_CRFStatus_Follows%><%--CRF Status is as follows*****--%>: 
<%
	} else {
%>		
		<%=LC.L_Assoc_Forms%><%--Associated Forms are*****--%>:
<%
	}
%>		
		</P>
	</td>
	<td width = 50% align=right	>
<%
	eventName = eventName.replace(' ','~');
	//Modified by Manimaran for November Enhancement PS4.
	if(!(patStatSubType.trim().equalsIgnoreCase("lockdown")))
	{	
%>	
		<A onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'N')" HREF="crfstatus.jsp?srcmenu=<%=src%>&selectedTab=3&mode=N&pkey=<%=personPK%>&visit=<%=visit%>&visitName=<%=visitName%>&eventName=<%=eventName%>&page=patientEnroll&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=StringUtil.encodeString(patientId)%>&formType=<%=formType%>"> 
<%
	
	if(formType.equals("crf")){
%>		
		<%=MC.M_Assoc_NewCrf%><%--Associate a new CRF*****--%> 
<%
	} else {
%>		
		<%=MC.M_Assoc_NewFrm%><%--Associate a new Form*****--%>
<%
	}
	
%>		
		</A>
<%
	}
	eventName = eventName.replace('~',' ');
%>			
	</td>
</tr>
</table>
<TABLE border=0 cellPadding=1 cellSpacing=1 width="100%">
  <TR>
    <TH width="10%" ><%=LC.L_Date%><%--Date*****--%></TH>
    <TH width="15%"><%=LC.L_Status%><%--Status*****--%></TH>
    <TH width="25%"><%=LC.L_Crf_StatusBy%><%--CRF Status By*****--%></TH>
    <TH width="25%"><%=LC.L_Crf_StatusDate%><%--CRF Status Date*****--%></TH>
    <TH width="10%"><%=LC.L_Sent_To%><%--Sent To*****--%></TH>
    <TH width="10%"><%=LC.L_Sent_By%><%--Sent By*****--%></TH>
    <TH width="15%"><%=LC.L_Sent_On%><%--Sent On*****--%></TH>	
  </TR>	 

<% 


  

  
  ArrayList crfNumbers = null;
  ArrayList crfNames = null;
  ArrayList crfEnterBys = null; 
  ArrayList crfEnterOns = null;  
  ArrayList crfStats = null;
  ArrayList crfReviewBys = null;
  ArrayList crfReviewOns = null;
  ArrayList crfSentTos = null;
  ArrayList crfSentBys = null;
  ArrayList crfSentOns = null;
  ArrayList crfIds = null;
  ArrayList crfStatIds = null;
  
  
  crfNumbers = crfStatDao.getCrfNumbers();
  crfNames = crfStatDao.getCrfNames();
  crfEnterBys = crfStatDao.getCrfEnterBys();
  crfEnterOns = crfStatDao.getCrfEnterOns();  
  crfStats = crfStatDao.getCrfStats();
  crfReviewBys = crfStatDao.getCrfReviewBys();
  crfReviewOns = crfStatDao.getCrfReviewOns();
  crfSentTos = crfStatDao.getCrfSentTos();
  crfSentBys = crfStatDao.getCrfSentBys();
  crfSentOns = crfStatDao.getCrfSentOns();
  crfIds = crfStatDao.getCrfIds();
  crfStatIds = crfStatDao.getCrfStatIds();
  
// Getting the size of the arrays.
//  int lenCrf = crfNumbers.size();
//  int lenCrf = crfStatDao.getCRows();
//  out.println("****" + lenCrf + "****");
  String crfNum = "";
  String precrfNumber = "";
  
	for(int i=0; i<lenCrf ; i++) 
	{
//out.println(i);
		String crfNumber = (String)crfNumbers.get(i); 
		String crfName = (String) crfNames.get(i);
		String crfEnterBy = (String)crfEnterBys.get(i);
		String crfEnterOn = (String) crfEnterOns.get(i);		
		String crfStat = (String) crfStats.get(i);
		String crfReviewBy = (String)crfReviewBys.get(i);
		String crfReviewOn = (String) crfReviewOns.get(i);
		String crfSentTo = (String) crfSentTos.get(i);
		String crfSentBy = (String)crfSentBys.get(i);
		String crfSentOn = (String) crfSentOns.get(i);
		String crfId = ((Integer) crfIds.get(i)).toString();
		String crfStatId = ((Integer) crfStatIds.get(i)).toString();
		
		crfNumber = (crfNumber == null)?"-":crfNumber;
		crfName = (crfName == null)?"-":crfName;
		crfEnterBy = (crfEnterBy == null)?"-":crfEnterBy;
		crfEnterOn = (crfEnterOn == null)?"-":crfEnterOn;		
		crfSentTo = (crfSentTo == null)?"-":crfSentTo;
		crfSentBy = (crfSentBy == null)?"-":crfSentBy;
		crfSentOn = (crfSentOn == null)?"-":crfSentOn;
		crfReviewBy = (crfReviewBy == null)?"-":crfReviewBy;
		crfReviewOn = (crfReviewOn == null)?"-":crfReviewOn;
		crfStat = (crfStat == null)?"-":crfStat;
		crfId = (crfId == null)?"-":crfId;		
		crfStatId = (crfStatId == null)?"-":crfStatId;
		
		if(!(crfNumber.equals(precrfNumber)))
		{
%>
<tr>
	<TD class=tdDefault colspan=5>
<%
	if(formType.equals("crf")){
%>		
		<B><%=LC.L_Crf_Num%><%--Crf Number*****--%>: <%=crfNumber%>
		<BR>
		<%=LC.L_Crf_Name%><%--Crf Name*****--%>: <%=crfName%> </B> 
<%
	} else {
%>		
		<B><%=LC.L_Form_Number%><%--Form Number*****--%>: <%=crfNumber%>
		<BR>
		<%=LC.L_Frm_Name%><%--Form Name*****--%>: <%=crfName%> </B>
<%
	}
%>			
		
	</TD>
	<TD class=tdDefault colspan=2 align=right>
<%
		eventName = eventName.replace(' ','~');
	//Modified by Manimaran for November Enhancement PS4.
	if(!(patStatSubType.trim().equalsIgnoreCase("lockdown")))
	{	
%>	
				<A onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'E')" HREF="crfstatus.jsp?srcmenu=<%=src%>&selectedTab=3&mode=NS&pkey=<%=personPK%>&visit=<%=visit%>&visitName=<%=visitName%>&eventName=<%=eventName%>&page=patientEnroll&crfId=<%=crfId%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=StringUtil.encodeString(patientId)%>&formType=<%=formType%>"> <%=LC.L_Add_NewStatus%><%--Add New Status*****--%> </A>
<%
	}
		eventName = eventName.replace('~',' ');
%>					
	</TD>
	
</tr>
<%
		}
%>

 <!-- place per > here in case even odd row doesn't work -->
<%	 
	if(i%2==0){ 
%>	
	<TR class="browserEvenRow">	
<%
	}else{
%>
	<TR class="browserOddRow">
<% 
	} 
%>
	 
	<td class=tdDefault><%=crfEnterOn%></td>
<%
		eventName = eventName.replace(' ','~');
%>	
	<td class=tdDefault><A onClick="return f_check_perm_org(<%=pageRight%>,<%=orgRight%>,'E')" HREF="crfstatus.jsp?srcmenu=<%=src%>&selectedTab=3&mode=M&pkey=<%=personPK%>&visit=<%=visit%>&visitName=<%=visitName%>&eventName=<%=eventName%>&page=patientEnroll&crfStatId=<%=crfStatId%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=StringUtil.encodeString(patientId)%>&formType=<%=formType%>"> <%=crfStat%> </A></td>
<%
		eventName = eventName.replace('~',' ');
%>	
	<td class=tdDefault><%=crfReviewBy%></td>
	<td class=tdDefault><%=crfReviewOn%></td>			 
	<td class=tdDefault><%=crfSentTo%></td>
	<td class=tdDefault><%=crfSentBy%></td>
	<td class=tdDefault><%=crfSentOn%></td>
	
</tr>
		
	<%
	precrfNumber=crfNumber;
%>

<%	
	}
  %>
  
  </table>

<%  
} // end of if(lenCrf==0 && formType.equals("other")) 
} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

} //end of else body for session times out

%>
  

  <div> 
   <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->   

</div>



</body>

</html>


