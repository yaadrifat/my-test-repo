<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=MC.M_BgtAddOrEdt_LineItm%><%--Budget >> Add/ Edit Line Item*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="bgtSectionB" scope="session" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*,java.math.BigDecimal
"%>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
   	<jsp:include page="include.jsp" flush="true"/>
   	<jsp:include page="ui-include.jsp" flush="true"></jsp:include>

 <%
%>


<SCRIPT Language="javascript">




	function checkDisc(formobj,chk,ctr,totalRows)
	{
		if (parseInt(totalRows) > 1)
		{
			fld = formobj.hidDiscountChkbox[ctr];
		}
		else
		{
		    fld = formobj.hidDiscountChkbox;
		}


		if (chk.checked)
		{
			fld.value="1";
		}
		else
		{
			fld.value="0";
		}


	}

	function checkIndirects(formobj,chk,ctr,totalRows)
	{
		if (parseInt(totalRows) > 1)
		{
			fld = formobj.hidAppIndirects[ctr];
		}
		else
		{
		    fld = formobj.hidAppIndirects;
		}


		if (chk.checked)
		{
			fld.value="1";
		}
		else
		{
			fld.value="0";
		}


	}

function maxDigits(value)
{

   cnt = 0;

   for(i=0;i<value.length;i++)
	{
	  if(value.charAt(i)=='.'){

		  if(cnt <= 8){

		  return true;
		  }
		  else {

		  return false;
		  }
	  }
	else{
		cnt = cnt+1;

	  }
	  if(cnt > 8)
		  return false;

	}

}

 function  validate(formobj){
	 // KV:Fixed Bug No.4517 Put Mandatory Field Validation 	 
  if (formobj.eventdata == null)
	  { 
	 	 if (!(validate_col('e-Signature',formobj.eSign)))
	 	  return false
	 	  else
	 	  return true;
   	   }
  
  var len = formobj.eventdata.length;
  
  len = len - 1;
  
  if(formobj.lineitemInPerSec.value==1)
	 {
	  /*YK 10Mar2011 Bug#5802*/
	  if(isNaN(formobj.unitCost.value) == true) {
			alert("<%=MC.M_EtrValid_UnitCost%>");/*alert("Please enter a valid Unit Cost.");*****/
			formobj.unitCost.focus();
			return false;
		}
	  if(isNaN(formobj.noUnits.value) == true) {
			alert("<%=MC.M_Etr_ValidUnitsNum%>");/*alert("Please enter valid Number of Units.");*****/
			formobj.noUnits.focus();
			return false;
		}

	  formobj.unitCost.value = (Math.round(formobj.unitCost.value *100)/100).toFixed(2);
	  formobj.noUnits.value = (Math.round(formobj.noUnits.value *100)/100).toFixed(2);
	  /*YK 10Mar2011 Bug#5802*/
	   if(isNaN(formobj.rate.value) == true) {
		alert("<%=MC.M_Etr_NumValueInRate%>");/*alert("Please enter numeric value in Rate");*****/
		formobj.rate.focus();
		return false;
		}
		if(maxDigits(formobj.rate.value)==false){
				alert("<%=MC.M_IntegerPartCnt_Exceed%>");/*alert("Integer part cannot exceed more than 8 digits.");*****/
				formobj.rate.focus();
				return false;
			}
	 }
  
  if(isNaN(len) == true)
   {
	  /*YK 10Mar2011 Bug#5802*/
	  if(isNaN(formobj.unitCost.value) == true) {
		  alert("<%=MC.M_EtrValid_UnitCost%>");/*alert("Please enter a valid Unit Cost.");*****/
			formobj.unitCost.focus();
			return false;
		}
	  if(isNaN(formobj.noUnits.value) == true) {
		  alert("<%=MC.M_Etr_ValidUnitsNum%>");/*alert("Please enter valid Number of Units.");*****/
			formobj.noUnits.focus();
			return false;
		}
	  formobj.unitCost.value = (Math.round(formobj.unitCost.value *100)/100).toFixed(2);
	  formobj.noUnits.value = (Math.round(formobj.noUnits.value *100)/100).toFixed(2);
	  /*YK 10Mar2011 Bug#5802*/
	   	   if (!(validate_col('Name',formobj.eventdata))) return false;
	    if (!(checkquote(formobj.eventdata.value)))
	    {
	    	formobj.eventdata.focus();
		    return false;
	    }

   }else
   {
	
	  	for(i=0;i<=len ;i++){
	  		/*YK 10Mar2011 Bug#5802*/
	  		if(isNaN(formobj.unitCost[i].value) == true) {
	  			alert("<%=MC.M_EtrValid_UnitCost%>");/*alert("Please enter a valid Unit Cost.");*****/
	  			formobj.unitCost[i].focus();
	  			return false;
	  		}
	  		 if(isNaN(formobj.noUnits[i].value) == true) {
	  			alert("<%=MC.M_Etr_ValidUnitsNum%>");/*alert("Please enter valid Number of Units.");*****/
	  				formobj.noUnits[i].focus();
	  				return false;
	  			}
	  		 formobj.unitCost[i].value = (Math.round(formobj.unitCost[i].value *100)/100).toFixed(2);
	  		 formobj.noUnits[i].value = (Math.round(formobj.noUnits[i].value *100)/100).toFixed(2);
	  		/*YK 10Mar2011 Bug#5802*/ 
		if (!(checkquote(formobj.eventdata[i].value)))
		{
			formobj.eventdata[i].focus();
			return false;
		}
	  }

   }

	for(i=0;i<=len ;i++){
	var blank = formobj.eventdata[i].value;
	if(blank == null || blank == "")
	{
	
	  	if (!(validate_col('Name',formobj.eventdata[i]))) return false;
	  	break;
				}
				else
				{
							break;
				}

   }


	
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
			 return false;

   }

   }



 
</SCRIPT>

<body onload="javascript:if (document.budget.eventdata && typeof(document.budget.eventdata[0])!='undefined')
                            document.budget.eventdata[0].focus();">

<jsp:useBean id="budgetB" scope="session" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budgetcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>


<%

String lineitemMode = request.getParameter("lineitemMode");
%>



  <%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
		{

	String lineitemCostType = "";
	String name = "";
	String descrip = "";
	String cptcode = "";
	String rate = "";
	float fRate = 0;
	String lineitemSponsorUnit="0.0";

	String bgtcalId = request.getParameter("bgtcalId");

	String bgtId=request.getParameter("budgetId");
	String bgtSectionId=request.getParameter("bgtSectionId");
	String bgtSectionName = request.getParameter("bgtSectionName");

	String allowSubmit =  request.getParameter("allowSubmit");

	if (StringUtil.isEmpty(allowSubmit))
	{
		allowSubmit="1";
	}

	String readOnlyAttribute = "";

	if (allowSubmit.equals("0"))
	{
		readOnlyAttribute = " READONLY ";
	}

	String templateType =  request.getParameter("templateType");
	String sponsorAmountDisplayAttribute ="hidden";

	if (StringUtil.isEmpty(templateType))
	{
		templateType="";
	}

	if (templateType.equals("C")) {
	 	 sponsorAmountDisplayAttribute = "text";
	 }



	bgtSectionName=StringUtil.decodeString(bgtSectionName);

	bgtSectionName=bgtSectionName.replace('~',' ');

	String bgtStatCodeParam = request.getParameter("bgtStatCode");

	String bgtStatCode ="";
	
	
	if (EJBUtil.isEmpty(bgtStatCodeParam ))
		bgtStatCodeParam="Work in Progress";
	// #5747 02/10/2011 @Ankit
	if(bgtStatCodeParam.equals("Freeze")) {
			bgtStatCode="F";
		} else if(bgtStatCodeParam.equals("Template")) {
			bgtStatCode="T";
		}else
		{ //Work in Progress
			bgtStatCode="W";
		}

	String sectionName = bgtSectionName ;


	%>





			<div class="popDefault" id="div2" style="overflow:auto; height:92%;">

  <%

	ArrayList eventdata = new ArrayList();
	int budgetId=EJBUtil.stringToNum(bgtId);
	String budgetname="";
	BigDecimal bRate = null;

	BudgetDao budgetDao = budgetB.getBudgetInfo(budgetId);
	ArrayList bgtStats = budgetDao.getBgtStats();
	ArrayList bgtSites = budgetDao.getBgtSites();
	ArrayList bgtStudyNumbers = budgetDao.getBgtStudyNumbers();
	String bgtStudyNumber = (String) bgtStudyNumbers.get(0);
	String bgtSite = (String) bgtSites.get(0);
	// #5747 02/10/2011 @Ankit
	ArrayList bgtStatDescs = budgetDao.getBgtCodeListStatusesDescs();
	String budgetStatusDesc = "";
	if(bgtStatDescs!=null && bgtStatDescs.size()>0){
		budgetStatusDesc = (String) bgtStatDescs.get(0);
	}

	BudgetcalDao budgetcalDao = budgetcalB.getBgtcalDetail(EJBUtil.stringToNum(bgtcalId));
	ArrayList bgtcalNames = budgetcalDao.getBgtcalProtNames();
	String calName = (String) bgtcalNames.get(0);


	budgetB.setBudgetId(budgetId);
	budgetB.getBudgetDetails();
	budgetname=budgetB.getBudgetName();

	budgetname = (budgetname == null || budgetname.equals(""))?"-":budgetname;
	bgtStudyNumber = (bgtStudyNumber == null || bgtStudyNumber.equals(""))?"-":bgtStudyNumber;
	calName = (calName == null || calName.equals(""))?"-":calName;

	bgtSite = (bgtSite == null || bgtSite.equals(""))?"-":bgtSite;
	String lineitemName = "";
	String lineitemId = "";
	int count = 5;
	String defaultCategory = "";
	String lineitemInPerSec = "";
	String ctgryPullDn = "";
	//Commented by Rohit CCF-FIN21
	//String tmid = "";
	//String cdm = "";
	SchCodeDao schDao = new SchCodeDao();
	SchCodeDao schCostDao = new SchCodeDao();
	String costTypeDD= "";
	String lineitemClinicNOfUnit = "1.0";
	String lineitemInCostDisc = "0";
	String appIndirects = "0";
	boolean bSection = false;
	String sectionLineItemPK = "";
	String lineItemSponsorAmount =  "";
	String sponsorAmount = "0.0";
	String bgtSectionPatNo  = "";
	String bgtSectionType="";

	ArrayList arFkEvents = new ArrayList();
	ArrayList arFKVisits = new ArrayList();
    ArrayList lineitemIds = new ArrayList();
    ArrayList lineitemNames = new ArrayList();
    ArrayList lineitemDescs = new ArrayList();
    ArrayList lineitemSponsorUnits = new ArrayList();
    ArrayList lineitemClinicNOfUnits = new ArrayList();
    ArrayList lineitemOtherCosts = new ArrayList();
    ArrayList lineitemInCostDiscs = new ArrayList();
    ArrayList lineitemCptCodes = new ArrayList();
   	ArrayList lineitemCategories = new ArrayList();
    //Commented by Rohit CCF-FIN21
   	//ArrayList lineitemTMIDs = new ArrayList();
    //ArrayList lineitemCDMs = new ArrayList();
    ArrayList lineitemAppIndirects = new ArrayList();
    ArrayList lineItemSponsorAmounts = new ArrayList();
    ArrayList arLineItemSequence = new ArrayList();
    ArrayList arLineItemCostType= new ArrayList();

	ArrayList lineitemInPerSecs =  new ArrayList();
	ArrayList lineitemNotes =  new ArrayList();
	ArrayList bgtSectionPatNos = new ArrayList();
	ArrayList bgtSectionTypes  = new ArrayList();

	schDao.getCodeValues("category");
	
	//@Ankit: 14-Jul-2011, #FIN-CostType-1
	String userId = (String) tSession.getValue("userId");
	String studyId = request.getParameter("studyId");
	if(studyId == null || studyId.equals("") )
	{
		studyId=budgetB.getBudgetStudyId();
	}
	
    String roleCodePk="";

    if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0){
    	
    	roleCodePk = eventassocB.getStudyUserRole(studyId,userId);
    	roleCodePk =(roleCodePk == null) ? "" : roleCodePk;
    	schCostDao.getCodeValuesForStudyRole("cost_desc",roleCodePk);
	} 
	else{
		
	  schCostDao.getCodeValues("cost_desc");
	}
    
	defaultCategory = EJBUtil.integerToString(new Integer (schDao.getCodeId("category", "ctgry_patcare")) );
	if (defaultCategory == null){

		ctgryPullDn = schDao.toPullDown("cmbCtgry");
	}
	else{

		ctgryPullDn = schDao.toPullDown("cmbCtgry", EJBUtil.stringToNum(defaultCategory));

	}

	if(lineitemMode.equals("N")) {
		costTypeDD = schCostDao.toPullDown("dCodeType");
	}

	if(lineitemMode.equals("M")) {
	count = 1;

	lineitemName = request.getParameter("lineitemName");


	lineitemName = StringUtil.decodeString(lineitemName);
	lineitemName=lineitemName.replace('~',' ');
	lineitemId = request.getParameter("lineitemId");
	lineitemInPerSec = request.getParameter("lineitemInPerSec");

	// Get Budget Section info
	bgtSectionB.setBgtSectionId(EJBUtil.stringToNum(bgtSectionId));
	bgtSectionB.getBgtSectionDetails();
	bgtSectionType = bgtSectionB.getBgtSectionType();
	bgtSectionPatNo = bgtSectionB.getBgtSectionPatNo();
	
	
	if (StringUtil.isEmpty(lineitemId) || EJBUtil.stringToNum(lineitemId) == 0)//mode iS M, but no lineitem PK passed, load data for entire section
	{
		//load data for entire section
		bSection = true;

		LineitemDao sectionLineDao = new LineitemDao();
		sectionLineDao =	lineitemB.getSectionLineitems(EJBUtil.stringToNum(bgtSectionId));

		bgtSectionPatNos = sectionLineDao.getBgtSectionPatNos();

		if (bgtSectionPatNos != null && bgtSectionPatNos.size() > 0)
		{
			bgtSectionPatNo = (String)bgtSectionPatNos.get(0);
		}

		if (StringUtil.isEmpty(bgtSectionPatNo))
		{
			bgtSectionPatNo="1";

		}

		bgtSectionTypes = sectionLineDao.getBgtSectionTypes();


		if (bgtSectionTypes!= null && bgtSectionTypes.size() > 0)
		{
			bgtSectionType = (String)bgtSectionTypes.get(0);

			if (StringUtil.isEmpty(bgtSectionType ))
			{
				bgtSectionType ="";
			}
		}

		lineitemIds = sectionLineDao.getLineitemIds();
        lineitemNames = sectionLineDao.getLineitemNames();
        lineitemDescs = sectionLineDao.getLineitemDescs();
        lineitemNotes = sectionLineDao.getLineitemNotes();
        lineitemSponsorUnits = sectionLineDao.getLineitemSponsorUnits();
        lineitemClinicNOfUnits = sectionLineDao.getLineitemClinicNOfUnits();
        lineitemOtherCosts = sectionLineDao.getLineitemOtherCosts();
		lineitemInCostDiscs = sectionLineDao.getLineitemInCostDiscs();
        lineitemCptCodes = sectionLineDao.getLineitemCptCodes();
        lineitemAppIndirects= sectionLineDao.getLineitemAppIndirects();
		lineItemSponsorAmounts = sectionLineDao.getLineItemSponsorAmount();
        arFKVisits = sectionLineDao.getArFKVisits();
        arFkEvents = sectionLineDao.getArFkEvents();
        arLineItemSequence = sectionLineDao.getArLineItemSequence( );
        //Commented by Rohit CCF-FIN21
        //lineitemCDMs = sectionLineDao.getLineitemCDMs();
        //lineitemTMIDs = sectionLineDao.getLineitemTMIDs();
        arLineItemCostType = sectionLineDao.getArLineItemCostType( );
        lineitemCategories = sectionLineDao.getLineitemCategories();
        lineitemInPerSecs = sectionLineDao.getLineitemInPerSecs();

        if (lineitemIds!=null)
        {
			count = lineitemIds.size();
		}

	}

	if (StringUtil.isEmpty(bgtSectionPatNo))
	{
		bgtSectionPatNo="1";

	}

	if(lineitemMode.equals("M") && bSection == false)
		{

			lineitemB.setLineitemId(EJBUtil.stringToNum(lineitemId));
			lineitemB.getLineitemDetails();
			name = lineitemB.getLineitemName();
			//Commented by Rohit CCF-FIN21
			/*tmid= lineitemB.getLineitemTMID();
			cdm= lineitemB.getLineitemCDM();
			if(tmid == null)
			tmid = "";
			if(cdm == null)
			cdm = "";*/


					sponsorAmount= lineitemB.getLineItemSponsorAmount();


		        	if (StringUtil.isEmpty(sponsorAmount))
		        	{
		        		sponsorAmount = "0.0";
		        	}

					lineitemCostType = lineitemB.getLineItemCostType();

                	lineitemSponsorUnit = lineitemB.getLineitemSponsorUnit( );
                	lineitemClinicNOfUnit = lineitemB.getLineitemClinicNOfUnit();

	                appIndirects =   	lineitemB.getLineitemAppIndirects();
	                lineitemInCostDisc =  	lineitemB.getLineitemInCostDisc();

	                if (StringUtil.isEmpty(appIndirects))
	                {
	                	appIndirects="0";
	                }

	                if (StringUtil.isEmpty(lineitemInCostDisc))
	                {
	                	lineitemInCostDisc="0";
	                }


	                costTypeDD = schCostDao.toPullDown("dCodeType",EJBUtil.stringToNum(lineitemCostType));

					ctgryPullDn = schDao.toPullDown("cmbCtgry",EJBUtil.stringToNum(lineitemB.getLineitemCategory()),1);

					if (StringUtil.isEmpty(lineitemInPerSec))
					{
						lineitemInPerSec = "0";
					}

					if(lineitemInPerSec.equals("1")){
					descrip = lineitemB.getLineitemNotes();
					rate= lineitemB.getLineitemSponsorUnit();
					if(rate == null)
							rate = "0";

					bRate = new BigDecimal(rate);
		            bRate = bRate.setScale(2,5);
					cptcode = lineitemB.getLineitemCptCode();
					}
					else{
					descrip = lineitemB.getLineitemDesc();
					cptcode = lineitemB.getLineitemCptCode();
					}


		}
	
	}//mode M
	
			%>

<Form name="budget" method="post" id="additempat" action="updateadditempatient.jsp" onSubmit="if (validate(document.budget)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >

<br/>
<table class="basetbl outline">
	<tr>
		<td  width=20%><%Object[] arguments0 = {budgetname}; %> <%=VelosResourceBundle.getMessageString("M_Bgt_Nme",arguments0)%><%--<B>Budget Name: </B> <%=budgetname%>*****--%></td>
	</tr>

	<tr>
		<td  width=20%><%Object[] arguments1 = {bgtStudyNumber}; %> <%=VelosResourceBundle.getMessageString("M_Std_Num",arguments1)%><%--<B>Study Number: </B> <%=bgtStudyNumber%>*****--%></td>
	</tr>
	<tr>
		<td  width=20%><%Object[] arguments2 = {bgtSite}; %> <%=VelosResourceBundle.getMessageString("M_Org",arguments2)%><%--<B>Organization: </B> <%=bgtSite%>*****--%></td>
	</tr>
	<tr>
		<td  width=20%><B> <%if(calName.equals("None")){%>
		<%=MC.M_NoCalSelected%><%--No Calendar Selected*****--%> </B> <%}else{Object[] arguments3 = {calName};%> <%=VelosResourceBundle.getLabelString("L_Calndr",arguments3)%><%--Calendar: </B><%=calName%>*****--%> <%}%>
		</td>

	</tr>
</table>
<br>
<%

	if ((!(bgtStatCode ==null)) && (bgtStatCode.equals("F") || bgtStatCode.equals("T")) )
		{
		%>
<table width="100%" cellspacing="2" cellpadding="2">
	<tr>
		<td>
		<P class="defComments"><FONT class="Mandatory">Budget
		<!--#5747 02/10/2011 @Ankit--><%Object[] arguments = {budgetStatusDesc}; %>
		<%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Status is '<%=budgetStatusDesc%>'. You cannot make any changes to the budget.*****--%></Font></P>
		</td>
	</tr>
</table>
<%
		}

if(lineitemMode.equals("M")){

%>
<P class="defComments"><%Object[] arguments5 = {sectionName}; %> <%=VelosResourceBundle.getMessageString("M_EdtEvtHere_BgtFlwgSec",arguments5)%><%--These Edit / Modified line items 'Events' specified here will be displayed in the budget in the following section
: <B><%=sectionName%></B>*****--%></P>
<%}else{%>
<P class="defComments"><%Object[] arguments4 = {sectionName}; %> <%=VelosResourceBundle.getMessageString("M_NewEvt_DispBgtSec",arguments4)%><%--The new line items 'Events' specified here will be displayed in the budget in the following section : <B><%=sectionName%></B>*****--%></P>
<%}%> <% if (bgtSectionType != null && bgtSectionType.equals("P")) { %>
<table>
	<tr>
		<td><b><%=LC.L_Number_OfPat%><%--Number of Patients*****--%>
</b></td>
		<td><input type=text name="patno" maxlength=20 size=5
			value='<%=bgtSectionPatNo%>'>&nbsp;&nbsp;&nbsp; <input
			type="checkbox" name="applyToAll" value="1"><b><%=MC.M_AllPer_PatSec%><%--Apply to All 'Per Patient' Sections*****--%></b></td>
	</tr>
	<tr>
		<td colspan=2><font color=red><%=MC.M_NotSpec_NumPatDef1%><%--If not specified, 'Number of Patients will default to 1.*****--%></font></td>
	</tr>
</table>
<br>
<% } %>
<table width="100%" class="basetbl outline">
	<tr>


		<%if(lineitemInPerSec.equals("1")){%>
		<th width="10%" align=center><%=LC.L_Personnel_Type%><%--Personnel Type*****--%></th>
		<th width="10%" align=center><%=LC.L_Cpt_Code%><%--CPT Code*****--%></th>
		<th width="10%" align=center><%=LC.L_Notes%><%--Notes*****--%></th>
		<%}else{%>
		<th width="10%" align=center><%=LC.L_Event%><%--Event*****--%></th>
		<th width="10%" align=center><%=LC.L_Cpt_Code%><%--CPT Code*****--%></th>
		<th width="10%" align=center><%=LC.L_Description%><%--Description*****--%></th>

		<%}%>
		<th width="10%" align=center><%=LC.L_Category%><%--Category*****--%></th>
		<th width="10%" align=center><%=LC.L_Cost_Type%><%--Cost Type*****--%></th>
		<th width="10%" align=center><%=LC.L_Unit_Cost%><%--Unit Cost*****--%></th>
		<th width="10%" align=center># <%=LC.L_Of_Units%><%--of Units*****--%></th>
		<th width="10%" align=center><%=LC.L_Apply_DiscountOrMarkup%><%--Apply Discount/Markup*****--%></th>
		<th width="10%" align=center><%=LC.L_Apply_Indirects%><%--Apply Indirects*****--%></th>
		<!-- <td width="10%" align=center>TMID</td> -->
		<!-- <td width="10%" align=center>CDM</td> -->

		<% if (templateType.equals("C")) { %>
		<th width="10%" align=center><%=LC.L_Sponsor_Amount%><%--Sponsor Amount*****--%></th>
		<% } %>
	</tr>
	<%
for (int i=0;i<count;i++)
{

 if (bSection) //section data, get in loop
 {





        	sponsorAmount = (String)lineItemSponsorAmounts.get(i);

        	if (StringUtil.isEmpty(sponsorAmount))
        	{
        		sponsorAmount = "0.0";
        	}

 			name = (String)lineitemNames.get(i);
 			sectionLineItemPK = ((Integer)lineitemIds.get(i)).toString();
 			//Commented by Rohit CCF-FIN21
			/*tmid= (String)lineitemTMIDs.get(i);
			cdm= (String) lineitemCDMs.get(i);
			if(tmid == null)	tmid = "";
			if(cdm == null)	cdm = "";*/

			lineitemCostType = (String)arLineItemCostType.get(i);
            lineitemSponsorUnit = (String)lineitemSponsorUnits.get(i);
            lineitemClinicNOfUnit = (String)lineitemClinicNOfUnits.get(i);
             appIndirects = (String)lineitemAppIndirects.get(i);
			lineitemInCostDisc =  (String)lineitemInCostDiscs.get(i);
			lineitemInPerSec= 	(String)lineitemInPerSecs.get(i);

			if (StringUtil.isEmpty(lineitemInPerSec))
            {
            	lineitemInPerSec="0";
            }


            if (StringUtil.isEmpty(appIndirects))
            {
            	appIndirects="0";
            }

            if (StringUtil.isEmpty(lineitemInCostDisc))
            {
            	lineitemInCostDisc="0";
            }
            costTypeDD = schCostDao.toPullDown("dCodeType",EJBUtil.stringToNum(lineitemCostType));
			ctgryPullDn = schDao.toPullDown("cmbCtgry",EJBUtil.stringToNum((String)lineitemCategories.get(i)),1);

			if(lineitemInPerSec.equals("1"))
			{
					descrip = (String)lineitemNotes.get(i);
					rate= lineitemSponsorUnit;

					if(rate == null)
							rate = "0";

					bRate = new BigDecimal(rate);
		            bRate = bRate.setScale(2,5);
					cptcode = (String)lineitemCptCodes.get(i);
					}
					else{
					descrip = (String) lineitemDescs.get(i);
					cptcode = (String)lineitemCptCodes.get(i);
					}


 }	//bsection
	  if (EJBUtil.isEmpty(cptcode))
	  cptcode = "";
	  if (EJBUtil.isEmpty(descrip))
	  descrip = "";
%>
	<tr>
		<td><input type=text name="eventdata" <%=readOnlyAttribute%>
			maxlength=1000 size=20 value='<%=name%>'></td>
		<%if(lineitemInPerSec.equals("1")){%>
		<td><input type=hidden name="rate" maxlength=11 size=20 value=''>
		<input type=text name="cptCode" maxlength=11 size=20
			value='<%=cptcode%>'></td>
		<%}else{%>
		<td><input type=text name="cptCode" <%=readOnlyAttribute%>
			maxlength=250 size=20 value='<%=cptcode%>'> <input
			type=hidden name="rate" maxlength=250 size=20 value=''></td>
		<%}%>
		<td><input type=text name="desc" <%=readOnlyAttribute%>
			maxlength=500 size=20 value='<%=descrip%>'></td>

		<td><%=ctgryPullDn%></td>
		<td><%=costTypeDD%></td>
		<td align=center><input type=text name=unitCost size=8
			maxlength=11 class="rightAlign" value="<%=lineitemSponsorUnit%>"></td>
		<td align=center><input type=text name=noUnits size=5
			maxlength=11 class="rightAlign" value="<%=lineitemClinicNOfUnit%>"></td>

		<td align=center>
		<%if(lineitemInCostDisc.equals("1")) { %> <input type="checkbox"
			name="discount" value="1" checked
			onclick="checkDisc(document.budget,this,<%=i%>,<%=count%>);">
		<%} else {%> <input type="checkbox" name="discount" value="0"
			onclick="checkDisc(document.budget,this,<%=i%>,<%=count%>);">
		<%}%> <input type="hidden" name="hidDiscountChkbox"
			value="<%=lineitemInCostDisc%>"></td>

		<td align=center>
		<%if(appIndirects.equals("0")){%> <input type="checkbox"
			name="appIndirects" value="0"
			onclick="checkIndirects(document.budget,this,<%=i%>,<%=count%>);">
		<%}else{%> <input type="checkbox" name="appIndirects" value="1" checked
			onclick="checkIndirects(document.budget,this,<%=i%>,<%=count%>);">
		<%}%> <input type="hidden" name="hidAppIndirects"
			value=<%=appIndirects%>></td>


		<%-- <td><input type=text name="tmid" maxlength=15 size=15 
			value=' <%=tmid%>'></td> 
		<td><input type=text name="cdm" maxlength=15 size=15
			value='--><%=cdm%>'></td>
		 --%>

		<% if (templateType.equals("C")) { %>
		<td><input type='<%=sponsorAmountDisplayAttribute%>'
			name="sponsorAmount" size=8 maxlength=11 class="rightAlign"
			value="<%=sponsorAmount%>"></td>
		<%}else{ %>
			<input type='<%=sponsorAmountDisplayAttribute%>'
			name="sponsorAmount" size=8 maxlength=11 class="rightAlign"
			value="<%=sponsorAmount%>">
		<%} %>
		
		<input type=hidden name="sectionLineItemPK" maxlength=15
			size=15 value='<%=sectionLineItemPK%>'>

	</tr>
	<%}
  %>
	<tr></tr>
	<%
if (!(bgtStatCode.equals("F") || bgtStatCode.equals("T") ))
	{

%>
	<tr>
		<td colspan=6 align="center"><jsp:include page="submitBar.jsp" flush="true">
			<jsp:param name="displayESign" value="Y" />
			<jsp:param name="formID" value="additempat" />
			<jsp:param name="showDiscard" value="N" />
		</jsp:include></td>
		<td colspan=5>&nbsp;</td>
	</tr>
	<%

  }
%>


</table>



<input type="hidden" name="bgtSectionId" MAXLENGTH=15
	value="<%=bgtSectionId%>"> <input type="hidden" name="bSection"
	MAXLENGTH=15 value="<%=bSection%>"> <input type="hidden"
	name="count" MAXLENGTH=15 value="<%=count%>"> <input
	type="hidden" name="bgtcalId" MAXLENGTH=15 value="<%=bgtcalId%>">
<input type="hidden" name="budgetId" MAXLENGTH=15 value="<%=budgetId%>">
<input type="hidden" name="lineitemMode" value="<%=lineitemMode%>">

<input type="hidden" name="lineitemId" value="<%=lineitemId%>">

<input type="hidden" name="bgtStatCode" value=<%=bgtStatCode%>>
<input type="hidden" name="lineitemInPerSec" value=<%=lineitemInPerSec%>>

<input type="hidden" name="templateType" value=<%=templateType%>>
<input type="hidden" name="bgtSectionType" value=<%=bgtSectionType%>>



</Form>

<%


}//end of if body for session

else

{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

</body>
</html>
