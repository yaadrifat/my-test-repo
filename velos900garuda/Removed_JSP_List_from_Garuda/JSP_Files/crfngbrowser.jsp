<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="JavaScript1.1">



function  validate(formobj){    
  
     if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
     }

}




function openwin1(formobj,counter) {
	  var names = formobj.enteredByName[counter].value;
	  var ids = formobj.enteredByIds[counter].value;
	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=alertnotify&mode=initial&ids=" + ids + "&names=" + names + "&rownum=" + counter;
//alert(windowToOpen);
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400")
;}




</SCRIPT>

</head>



<% String src="";
src= request.getParameter("srcmenu");
%>	

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="patEnrollB" scope="request" class="com.velos.eres.web.patProt.PatProtJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<body>
<br>

<DIV class="browserDefault" id="div1"> 


  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))
   {

   String mode="N";//request.getParameter("mode");
   String uName = (String) tSession.getValue("userName");

	int personPK = 0;
	String patientId = "";
	String globalFlag = "";
	String dob = "";
	String gender = "";
	String genderId = "";
	String yob = "";
	String protocolId="";
	int age = 0;
	String editLock;
	
	Calendar cal1 = new GregorianCalendar();

	String page1=request.getParameter("page");
	protocolId=request.getParameter("protocolId");
	//String studyId = (String) tSession.getValue("studyId");	
	String studyId = request.getParameter("studyId");
	String enrollId =(String) tSession.getValue("enrollId");
	globalFlag=request.getParameter("globalFlag");

	String lockSetting= request.getParameter("lockSetting");

	String studyTitle = "";
	String studyNumber = "";
	studyB.setId(EJBUtil.stringToNum(studyId));

    studyB.getStudyDetails();
    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();	
	
	
	patEnrollB.setPatProtId(EJBUtil.stringToNum(enrollId));
	patEnrollB.getPatProtDetails();
	//protocolId =patEnrollB.getPatProtProtocolId();
	

	
	//String protName = "";
	//if(protocolId != null) {
	//	eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
	//	eventAssocB.getEventAssocDetails();
	//	protName = 	eventAssocB.getName();
	//} else {
	//	protocolId = "";
	//}
	
	

%>
<br>
<P class="sectionHeadings"> <%=MC.M_MngPatAlrt_CrfNotify%><%--Manage <%=LC.Pat_Patients%> >> Alerts and Notifications >> CRF Notifications*****--%></P>
<%--
<jsp:include page="patienttabs.jsp" flush="true"> 
</jsp:include>
--%>
	<%
//if(protocolId != null) {

%>
<P class = "userName"> <%= uName %> </P>
<%--
<P class="sectionHeadings"> Manage <%=LC.Pat_Patients%> >> Schedule >> Alerts and Notificatons </P>
<jsp:include page="patienttabs.jsp" flush="true">
 

<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
--%>



<Form name="alertnotify" method=post >
	
</FORM>




<table class=tableDefault  width="100%" border=0>
<tr><td width= 40%>
	<p class = "sectionHeadings" ><%=LC.L_Crf_Notifications%><%--CRF Notifications*****--%> </p>
</td>

<td width=60% align=right>
<%
if(lockSetting.equals("0"))
{
%>
	 <a href="crfnotifyglobal.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=N&studyId=<%=studyId%>&globalFlag=<%=globalFlag%>&fromPage=patientEnroll&protocolId=<%=protocolId%>&lockSetting=<%=lockSetting%>"> <%=MC.M_Add_AnotherCrfNotfic%><%--Add Another CRF Notification*****--%></a>
<%
}
%>
	 
</td>
</tr>
</table>
<TABLE  width="95%">
  <TR>
  	<th width=25% ><%=LC.L_For_CrfStatus%><%--For CRF Status*****--%> </th>
	<th width=40% ><%=LC.L_Notify_Users%><%--Notify Users*****--%> </th>
	<th width=30% ><%=LC.L_Crf%><%--CRF*****--%></th>
  </TR>
<%  
  CrfNotifyDao crfNotifyDao = new CrfNotifyDao();
  crfNotifyDao = crfNotifyB.getCrfNotifyValues(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId));
  
  ArrayList crfNotIds = null;
  ArrayList crfNotStats = null;
  ArrayList crfNotUsers = null; 
  ArrayList crfNumbers = null;
  ArrayList crfIds = null;
  
  crfNotIds = crfNotifyDao.getCrfNotIds();
  crfNotStats = crfNotifyDao.getCrfNotStats();
  crfNotUsers = crfNotifyDao.getCrfNotUsers();
  crfNumbers = crfNotifyDao.getCrfNumbers();
  crfIds = crfNotifyDao.getCrfNotCrfIds();

  int lenCrf = crfNotifyDao.getCRows();
//  out.println("****" + lenCrf + "****");
	for(int i=0; i<lenCrf ; i++) 
	{
		String crfNotId = ((Integer) crfNotIds.get(i)).toString();
		String crfNotStat = (String)crfNotStats.get(i); 
		String crfNotUser = (String) crfNotUsers.get(i);
		String crfNumber = (String) crfNumbers.get(i);
		String crfId = (String) crfIds.get(i);
		
		crfNotId = (crfNotId == null)?"-":crfNotId;
		crfNotStat = (crfNotStat == null)?"-":crfNotStat;
		crfNotUser = (crfNotUser == null)?"-":crfNotUser;
		crfNumber = (crfNumber == null)?"All":crfNumber;
		crfId = (crfId == null)?"":crfId;
		
		
	if(i%2==0){ 
%>	
	<TR class="browserEvenRow">	
<%
	}else{
%>
	<TR class="browserOddRow">
<% 
	} 
%>
<%
if(lockSetting.equals("0"))
{
%>
	 
	<td class=tdDefault align="center"><A HREF=crfnotifyglobal.jsp?srcmenu=<%=src%>&page=patientEnroll&crfNotifyId=<%=crfNotId%>&globalFlag=<%=globalFlag%>&mode=M&studyId=<%=studyId%>&protocolId=<%=protocolId%>&lockSetting=<%=lockSetting%>> <%=crfNotStat%> </A></td> 	

<%}else{ %>

	<td class=tdDefault align="center"><%=crfNotStat%></td>
<%}%>

	<td class=tdDefault align="center"><%=crfNotUser%></td>
	<td class=tdDefault align="center"><%=crfNumber%></td>
</tr>
		
<%	
	}
%>
  
</TABLE>

<table width=100%>
	<tr>
      <td align=center>
		<A type="submit" href="editglobalsetting.jsp?studyId=<%=studyId%>&mode=N&srcmenu=<%=src%>&protocolId=<%=protocolId%>"><%=LC.L_Back%></A>	
      </td>
	 </tr>
</table>


<%
} //end of if session times out
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
   <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->   
</div>
</body>
</html>
