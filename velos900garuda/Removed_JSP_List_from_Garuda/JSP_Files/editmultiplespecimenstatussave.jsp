<%--

	Project Name:		Velos eResearch
	Author:				Jnanamay Majumdar
	Created on Date:	29Oct2007
	Purpose:			Update page fop Specimen Update Status page
	File Name:			editmultiplespecimenstatussave.jsp

--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT> 
</HEAD>

<body>
  

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>  
  <%@ page language = "java" import = "com.velos.eres.web.specimen.SpecimenJB,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.specimenStatus.SpecimenStatusJB"%>  

  
  <%

	String eSign = request.getParameter("eSign");	
		
	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))

   {
   
    String user = null;
	user = (String) tSession.getValue("userId");

	String accId = (String) tSession.getValue("accountId");	
	
	String ipAdd = null;
	ipAdd = (String) tSession.getValue("ipAdd");
   
   
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/> 

     <%   
   
   	String oldESign = (String) tSession.getValue("eSign");
	
	if(!oldESign.equals(eSign)) 
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
  	{
  	
  			
  	boolean bb = false;
  	CodeDao cdSpecStat = new CodeDao();  	
  	ArrayList subTypeStatuses = new ArrayList();
  	String subTypeStatus = ""; 
  	String specIdSeq = ""; 	
  	
  	int printme = -1;
  	int countRows = 0;
  	countRows = EJBUtil.stringToNum(request.getParameter("cntNumrows"));  	

  	  	  			
			String[] fkSpecimen = new String[countRows];			
			String[] specimenStatus = new String[countRows];						
			String[] spDate = new String[countRows];			
			String[] spStatusBy = new String[countRows];
			String[] spQunatity = new String[countRows];
			String[] spQuantityUnit = new String[countRows];
			String[] receipient = new String[countRows];
			String[] trackingNum = new String[countRows];
			String[] notes = new String[countRows];
			String[] statusSS = new String[countRows];
			String[] statusMM = new String[countRows];
			String[] statusHH = new String[countRows];
			String statusTime ="";
			
 	  	  	  	  	  	  	  	  	
 for (int g=0 ; g<countRows ; g++) {
 	  		
 	  		fkSpecimen[g] = request.getParameter("fkSpec"+(g+1));
 	  		specimenStatus[g] = request.getParameter("specStat"+(g+1));
 	  		spDate[g] = request.getParameter("statDt"+(g+1)); 	 
 	  		spStatusBy[g] = request.getParameter("specCreatorId"+(g+1));
 	  		spQunatity[g] = request.getParameter("quantity"+(g+1));
 	  		spQuantityUnit[g] = request.getParameter("specQtyUnitEdit"+(g+1)); 	 
 	  		receipient[g] = request.getParameter("creatorId"+(g+1));
 	  		trackingNum[g] = request.getParameter("trackNum"+(g+1));
 	  		notes[g] = request.getParameter("statusNotes"+(g+1));	

			//KM-#INVP2.10
			statusSS[g] =  request.getParameter("statusSS"+(g+1));
			if (StringUtil.isEmpty(statusSS[g]))
			{
			 	statusSS[g] = "00";
			}
			statusMM[g] =  request.getParameter("statusMM"+(g+1));
			if (StringUtil.isEmpty(statusMM[g]))
			{
			 	statusMM[g] = "00";
			}

			statusHH[g] =  request.getParameter("statusHH"+(g+1));
			if (StringUtil.isEmpty(statusHH[g]))
			{
			 	statusHH[g] = "00";
			}

			statusTime = statusHH[g] +":"+statusMM[g]+":"+statusSS[g];
			spDate[g] = spDate[g] + " "+ statusTime;

 }

 for (int f=0 ; f<countRows ; f++) {
  	
  	
  	SpecimenStatusJB spStatJB = new SpecimenStatusJB();			
			  	
		if (!specimenStatus[f].equals("") && !spDate[f].equals("")){
			spStatJB.setFkSpecimen(fkSpecimen[f]);
			spStatJB.setSsDate(spDate[f]);
			spStatJB.setFkCodelstSpecimenStat(specimenStatus[f]);			
			spStatJB.setSsQuantity(spQunatity[f]);
			spStatJB.setSsQuantityUnits(spQuantityUnit[f]);
			spStatJB.setSsTrackingNum(trackingNum[f]);
			spStatJB.setSsNote(notes[f]);
			spStatJB.setFkUserRecpt(receipient[f]);						
			spStatJB.setSsStatusBy(spStatusBy[f]);
			spStatJB.setCreator(user);
			spStatJB.setIpAdd(ipAdd); 						           
			printme=spStatJB.setSpecimenStatusDetails();
		}
		
	
 }
	  	
  	
	if(printme>=0){		
%>
	  <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>   
      
     
     <script>
			window.opener.location.reload();
			setTimeout("self.close()",2000);
	  </script>	 
    
	  
<%}else{%>

      <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "sectionHeadings" align = "center" > <%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>
      <table width="70%" >
      <tr>
      <td width="10%"></td>
      <td width="60%" align="center">&nbsp;&nbsp;
      <button onclick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table> 
     
<%}
		

}//end of eSign check

}//end session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

	<div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>



</BODY>

</HTML>
