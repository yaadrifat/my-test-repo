<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Refresh_Notifications%><%--Refresh Notifications*****--%></title>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
src= request.getParameter("srcmenu");

String calledFrom = "";
calledFrom = request.getParameter("calledFrom");

if (StringUtil.isEmpty(calledFrom))
{
	calledFrom = "P";
}

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>
<%
String from = "calendar";
%>

<DIV class="formDefault" id="div1">
  <jsp:include page="studytabs.jsp" flush="true">
  <jsp:param name="from" value="<%=from%>"/> 
  </jsp:include>

<% 
	String calendarId= "";
	String selectedTab="";
	
	HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{
  	
		calendarId= request.getParameter("calendarId");
		selectedTab=request.getParameter("selectedTab");
		String studyId = (String) tSession.getValue("studyId");		
		int ret=0;
		
		String mode =request.getParameter("mode");
			
		if (mode.equals("null") || StringUtil.isEmpty(mode)) {
			mode="final";
			
	
%>

	<FORM name="refProtMessages" id ="refProtMsgFrm" method="post" action="refreshProtocolMessages.jsp" onSubmit="if (validate(document.refProtMessages)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%> </P>
	
	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="refProtMsgFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>



 	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="calendarId" value="<%=calendarId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
   	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
   	 

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			
			String ipAdd = (String) tSession.getValue("ipAdd");
			String usr = (String) tSession.getValue("userId");

			ret = eventassocB.refreshCalendarMessages(calendarId,studyId,usr,ipAdd);  
			if (ret != 1) {%>
				<br><br> <p class = "sectionHeadings" align = center><%=MC.M_Notifics_CntRefreshed%><%--Notifications could not be refreshed*****--%></p>
				<P align="center"><button onClick="window.history.go(-2);"><%=LC.L_Back%></button></P>
			<%}
			 else { %>
				<br><br><br><br> <p class = "sectionHeadings" align = center><%=MC.M_Notifications_RefreshedSucc%><%--Notifications refreshed successfully*****--%> </p>
				
				<% if (calledFrom.equals("P")) { %>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyprotocols.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">
				<% }
				else
				{ %>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyadmincal.jsp?calId=<%=calendarId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">
				<%
				}
			}
			
			} //end esign
	} //end of mode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>

</body>
</HTML>


