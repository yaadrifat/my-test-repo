<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page language = "java" import="com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="studySiteB" scope="session" class="com.velos.eres.web.studySite.StudySiteJB"/>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
	int studySiteId = EJBUtil.stringToNum(request.getParameter("studySiteId"));
	int siteId = EJBUtil.stringToNum(request.getParameter("selsite"));
	int studyId = EJBUtil.stringToNum(request.getParameter("studyId"));
	int accId = EJBUtil.stringToNum((String)tSession.getValue("accountId"));
	String uId = (String) tSession.getValue("userId");
	
	
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
		
		//SA 05/26/09 checks for deletion:
		//emergency fix for UCLA performance problem with minimum impact	
			
			StudyStatusDao studyStatDao = new StudyStatusDao();
			ArrayList subTypeStatusLst = null;
			ArrayList studyStatusIdLst = null;
			ArrayList studyStatusDescs = null;
			int lenStudy =0;
			int enrPatCount  = 0;
			String activeStatusDesc  = "";

			studyStatDao = studyStatB.getStudyStatusDesc(studyId, siteId, EJBUtil.stringToNum(uId), accId);

			studyStatusIdLst = studyStatDao.getIds();
			lenStudy = studyStatusIdLst.size();

			subTypeStatusLst = studyStatDao.getSubTypeStudyStats();
			enrPatCount = studyStatDao.getEnrPatCnt();

			CodeDao  cd = new CodeDao();
			cd.getCodeId("studystat","active");
			activeStatusDesc = cd.getCodeDescription();


			String subType = "";
			int activeCount = 0;

			String displayMessage = "";
			boolean deleteThisOrg = true;
			
		 
			
			for(int tmpCnt = 0;tmpCnt < lenStudy; tmpCnt++)
			{

			subType =(((subTypeStatusLst.get(tmpCnt)) == null)?"-":(subTypeStatusLst.get(tmpCnt)).toString()).trim();

				if (subType.equals("active")){
						activeCount++;
				}

			} 
			 
			
			if (activeCount >= 1 && enrPatCount > 0 )
						{
							//JM: 01Sept2008, added for, issue #3203
							//alert("A patient is currently enrolled to the study and '"+studyStatDesc+"' status can not be deleted.");
							Object[] arguments = {activeStatusDesc};
							displayMessage =VelosResourceBundle.getMessageString("M_PatCurStd_LkdAct",arguments);/*displayMessage = "- A "+LC.Pat_Patient+" is currently enrolled to the "+LC.Std_Study+" and '"+activeStatusDesc+"' status is linked to this Organization";*****/
							deleteThisOrg  =  false;
						}

			if (!deleteThisOrg )
			{
				displayMessage = MC.M_OrgCntDel_Reasons+": <BR><BR>" + displayMessage;/*displayMessage = "This Organization cannot be deleted for the following reason(s): <BR><BR>" + displayMessage;*****/
			}
			
			if (!deleteThisOrg )
			{
					%>
						<table><tr><td ><p class = "defComments"><%=displayMessage%></p></td></tr>
						<tr>
						<td align="center"><BR><button onclick="window.history.go(-1);"><%=LC.L_Back%></button></td>
						</tr></table>
						<%
			
			}
			else
			{
%>
	<FORM name="deleteStudySite" id="delStdSite" method="post" action="deleteStudySite.jsp" onSubmit="if (validate(document.deleteStudySite)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			
		<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="copyverfrm"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
 	 <input type="hidden" name="studySiteId" value="<%=studySiteId%>">
 	 <input type="hidden" name="selsite" value="<%=siteId%>">
 	 <input type="hidden" name="studyId" value="<%=studyId%>">
     <input type="hidden" name="selectedTab" value="<%=StringUtil.htmlEncodeXss(request.getParameter("selectedTab"))%>">
	 
	</FORM>
<%
	} // for deleteThisOrg 
} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
	
	studySiteB.setStudySiteId(studySiteId);
	int i=0;
	//Modified for INF-18183 ::: Ankit
	i=studySiteB.deleteOrgFromStudyTeam(studyId, accId, siteId, EJBUtil.stringToNum(uId), AuditUtils.createArgs(session,"",LC.L_Study));     //Changed for Bug#7604 : Raviesh
	out.println("<META HTTP-EQUIV=Refresh CONTENT=\"0; URL=teamBrowser.jsp?selectedTab="+
	        StringUtil.htmlEncodeXss(request.getParameter("selectedTab"))+"&srcmenu=tdmenubaritem3\">");
	} //end esign
	} //end of delMode
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--> 
</div>

</BODY>
</HTML>

