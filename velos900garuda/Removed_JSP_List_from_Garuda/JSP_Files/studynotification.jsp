 <%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.service.util.*"%>

<title><%=MC.M_Std_AlertNotify%><%--Study >> Alerts and Notifications*****--%></title>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.DateUtil,com.velos.eres.business.common.*" %>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.user.*,com.velos.eres.web.studyRights.StudyRightsJB"%>

<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<!-- JM: 22Aug2006 -->
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<script>

function  validate(formobj){

     if (!(validate_col('e-Signature',formobj.eSign))) return false
     if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	 formobj.eSign.focus();
	 return false;
     }
}


function openwin1(formobj,cntr) {

	  var names, ids ;
	  names = formobj.userNames[cntr].value;

	  if ((names=='null' ) || (names==null ) || (names==0 )){
	  names = '';
	  }


	  ids = formobj.userIds[cntr].value;


	  if ((ids=='null' ) || (ids==null ) || (ids==0 )){
	  ids = '';
	  }

	  while(names.indexOf(' ') != -1){
	  	names = names.replace(' ','~');
	  }

	var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=studynotifyform&mode=initial&ids=" + ids + "&names=" + names  + "&rownum=" + cntr;
	window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")

}


</script>




<% String src;

src= request.getParameter("srcmenu");

%>



	<jsp:include page="panel.jsp" flush="true">

	<jsp:param name="src" value="<%=src%>"/>

	</jsp:include>



	<body style="overflow:scroll">



<%

  HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))

	{

	String studyId = "";
	String selectedTab="" ;
	String from  = "studynot";
	int pageRight = 0;

   	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");



	selectedTab=request.getParameter("selectedTab");


	%>
<DIV class="BrowserTopn" id="divtab">

	<jsp:include page="studytabs.jsp" flush="true">
		<jsp:param name="from" value="<%=from%>"/>
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
		<jsp:param name="hideRedmessage" value="hideRedmessage"/>
	</jsp:include>
</DIV>
<%
String stid= request.getParameter("studyId");
if(stid==null || stid==""){%>
   <DIV class="BrowserBotN BrowserBotN_S_1" id="div1">
	<%} else {%>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"><%}


    studyId = (String) tSession.getValue("studyId");
    int study = EJBUtil.stringToNum(studyId);

   if(studyId == "" || studyId == null) {
	%>
  		<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  	<%

  }
else
 {


	String userIdFromSession = (String) tSession.getValue("userId");



	stdRights =(StudyRightsJB) tSession.getValue("studyRights");


	if ((stdRights.getFtrRights().size()) == 0){

		 	pageRight= 0;

		}else{

			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));


		}


	if (pageRight >= 4 )

	   {

		   UserJB user = (UserJB) tSession.getValue("currentUser");

	   	   //String studyId = (String) tSession.getValue("studyId");

		   //EventAssocDao eventAssoc = new EventAssocDao();

		   ctrldao= eventassocB.getStudyProtsWithAlnotStat(EJBUtil.stringToNum(studyId ));

			ArrayList eventIds=ctrldao.getEvent_ids() ;
			ArrayList chainIds=ctrldao.getChain_ids() ;
			ArrayList names= ctrldao.getNames();
			ArrayList eventTypes= ctrldao.getEvent_types();
			
			//JM: 31JAN2011: Enh- D-FIN9
			//ArrayList stats = ctrldao.getStatus();
			ArrayList stats = ctrldao.getStatCodes();
			
			
			ArrayList alNotFlags = ctrldao.getCosts();



	   	  	int len= eventIds.size();

  	         	int counter = 0;

			Integer id;

			String eventType = "";

			String name="";

			String description="";

			String chainId="";

			String duration="";

			String status="";

			String statusText="";

			String alNotFlag = "";

			int displayCounter = 0;



//JM: 22August2006
			String pageMode = request.getParameter("pageMode");
			if (EJBUtil.isEmpty(pageMode))
			{
				pageMode = "initial";
			}

			String saeNotifyUserPK = "", saeNotifyUserValue ="";
			//String saeNotifyMailPK = "" , saeNotifyMailValue = "";
			String saeDeathNotifyUserPK = "", saeDeathNotifyUserValue = "";
			//String saeDeathNotifyEmailPK = "", saeDeathNotifyMailValue = "";



		String settingsPk = "";
		String 	settingsKeyword = "";
		String 	settingsUserName = "";
		String 	settingsValue = "";
		String saeUserList = "";
		String saeDeathUserList = "";




	if (pageMode.equals("initial")){

	UserDao userDao  = new UserDao();

	SettingsDao settingsDao=commonB.retrieveSettings(study,3);

	ArrayList settingsValues=settingsDao.getSettingValue();
	ArrayList settingsKeywords=settingsDao.getSettingKeyword();
	ArrayList settingsPks=settingsDao.getSettingPK();

	int index;
	int length = settingsPks.size();

	String[] format=new String[3];
	Arrays.fill(format,"");
	index=settingsKeywords.indexOf("SAE_NOTIFY_USER");
	if (index>=0)
	{
		saeNotifyUserPK=(String)settingsPks.get(index);
		saeNotifyUserValue=(String)settingsValues.get(index);
		saeUserList = userDao.getUserNamesFromIds(saeNotifyUserValue);
		saeUserList=(saeUserList==null) ? "" : saeUserList;
	}
	//Commented by Manimaran to fix the Bug2715
	/*index=settingsKeywords.indexOf("SAE_NOTIFY_EMAIL");
	if (index>=0)
	{
		saeNotifyMailPK=(String)settingsPks.get(index);
		saeNotifyMailValue=(String)settingsValues.get(index);
		saeNotifyMailValue=(saeNotifyMailValue==null)? "" :saeNotifyMailValue;
	}
	*/

	index=settingsKeywords.indexOf("SAE_DEATH_NOTIFY_USER");
	if (index>=0)
	{
		saeDeathNotifyUserPK=(String)settingsPks.get(index);
		saeDeathNotifyUserValue=(String)settingsValues.get(index);
		saeDeathUserList = userDao.getUserNamesFromIds(saeDeathNotifyUserValue);
		saeDeathUserList=(saeDeathUserList==null)? "" :saeDeathUserList;
	}

	//Commented by Manimaran to fix the Bug2715
	/*index=settingsKeywords.indexOf("SAE_DEATH_NOTIFY_EMAIL");
	if (index>=0)
	{
		saeDeathNotifyEmailPK=(String)settingsPks.get(index);
		saeDeathNotifyMailValue=(String)settingsValues.get(index);
		saeDeathNotifyMailValue=(saeDeathNotifyMailValue==null)? "" :saeDeathNotifyMailValue;
	}
	*/

}//initial
%>

<!-- --------------------JM: 22August2006 ---------------------------------------------------------------------------------------->

<Form name="studynotifyform" id="stdnotifyfrm" method=post action="updatestudynotification.jsp" onsubmit="if (validate(document.studynotifyform)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<INPUT type=hidden name= pageMode value = "final">
<input type="hidden" name="studyId" Value=<%=studyId%>>

    <input type="hidden" name="srcmenu" value='<%=src%>'>

<P class="redMessage lhsFont">
   			<%=MC.M_CalNotfic_NotAplStdAdmCal%><%--<%=LC.Pat_Patient%> Calendar Notifications are not applicable to <%=LC.Std_Study%> Admin Calendars*****--%>
   		</P>
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign" >
<tr>
		<!-- Modified by Manimaran to fix the Bug2715 -->
		<td COLSPAN=3 > <p class="sectionheadings"><%=LC.L_Adv_EvtNotifications%><%--Adverse Event Notifications*****--%></p>	</td>
</tr>

      <tr>

	<td width=60%>

	<!-- <p class = "sectionHeadings" >Alert</p> -->


	</td>

	<td width=40%>

	<%=LC.L_Send_AsEmail%><%--Send as Email*****--%>

	</td>



	<!--td width=20% align="center">

	Send To  CellPhone/Pager

	</td-->

	</tr>


	<!--tr>

<td width=60%>&nbsp</td>

<td width=20%>&nbsp</td>

<td width=20%>(Specify cellphone email ID or pager number)</td>

</tr-->

</table>

<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl midAlign" >

<tr>

	<td width=50%>
	<%=MC.M_AlertIf_SeriousEvtRpted%><%--Send alert if Serious Adverse Event is reported*****--%>
	</td>

	 <td width=25%>
	 <input type=text size=15 READONLY name=userNames value="<%=saeUserList %>">
	 <input type="hidden" name=userIds value="<%=saeNotifyUserValue %>">
 	 <input type="hidden" name=saeNotifyUserPK value="<%=saeNotifyUserPK %>">

	 </td>

	<td width=10%>
	<A HREF=# onClick='openwin1(document.studynotifyform, 0)'><%=LC.L_User_Search%><%--User Search*****--%></A>
	</td>
	<td width=15%>
	<!--Commented by Manimaran to fix the Bug2715 -->
	<!--INPUT type=text name=userMobs value='<!--%=saeNotifyMailValue%>'size=15 -->
	<!--INPUT type="hidden" name=saeNotifyMailPK value='<!--%=saeNotifyMailPK%>'size=15 -->

	</td>
</tr>
<tr>

	<td width=60%>
	<%=MC.M_AlertDthRptAs_AvdEvtOCome%><%--Send alert if Death is reported as Adverse Event outcome*****--%>
	</td>

	 <td width=15%>
	 <input type=text size=15 READONLY name=userNames value="<%=saeDeathUserList %>">

	 <input type="hidden" name=userIds value="<%=saeDeathNotifyUserValue %>">
	 <input type="hidden" name=userIds1 value="<%=saeDeathNotifyUserValue %>">
 	 <input type="hidden" name=saeDeathNotifyUserPK value="<%=saeDeathNotifyUserPK %>">


	</td>

	<td width=10%>
	<A HREF=# onClick='openwin1(document.studynotifyform,1)'><%=LC.L_User_Search%><%--User Search*****--%></A>
	</td>

	<td width=15%>

	<!--Commented by Manimaran to fix the Bug2715 -->
	<!--input type="text" name=userMobs1 value="<!--%=saeDeathNotifyMailValue %>" size=15-->
	<!--input type="hidden" name=saeDeathNotifyEmailPK value="<!--%=saeDeathNotifyEmailPK %>" size=15-->


	</td>

</tr>
</table>
	<%

	 if (pageRight > 4 )
	  {
	%>
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="stdnotifyfrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

	<%
		}
	%>

</table>
</Form>


<br>



<!-- JM: 22Aug2006 -->

	<Form name="protocolbrowser" method="post" action="" onsubmit="">


   <input type="hidden" name="studyId" Value=<%=studyId%>>

    <input type="hidden" name="srcmenu" value='<%=src%>'>


	<table width="99%" border="0" cellspacing="0" cellpadding="0" class="midAlign" >

	<tr>
		<td COLSPAN=6><br>
			<p class="sectionheadings"> <%=MC.M_Pcol_CalNotification%><%--Protocol Calendar Specific Notifications*****--%></p>	</td>
	</tr>

    <tr >

      <td width = "100%" COLSPAN=6>

        <P class = "defComments_txt" ><%=MC.M_ActvPcolCalStd_SelAprop%><%--The list below displays the 'Active' Protocol Calendars associated with this <%=LC.Std_Study_Lower%>. Select the appropriate link to specify notifications for the protocol.*****--%> </P><br>

      </td>

    </tr>

	<table width="99%" border="0" cellspacing="0" cellpadding="0" class="basetbl outline midAlign">
    <tr>

        	<th width="25%"> <%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%> </th>

	        <th width="15%"> </th>
	        <th width="15%"> </th>
   	        <!--th width="15%"> </th-->
   	        <!--<th width="15%"> </th>		-->
   	        <th width="15%"> </th>
      </tr>



       <%



    for(counter = 0;counter<len;counter++)

	{

		id = (Integer)eventIds.get(counter);

		eventType=((eventTypes.get(counter)) == null)?"-":(eventTypes.get(counter)).toString();

		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();



		chainId=((chainIds.get(counter)) == null)?"-":(chainIds.get(counter)).toString();


		status=((stats.get(counter)) == null)?"-":(stats.get(counter)).toString();

		alNotFlag = ((alNotFlags.get(counter)) == null)?"1":(alNotFlags.get(counter)).toString();

		//display only active protcols



		if ((displayCounter %2)==0) {



	  %>

      <tr class="browserEvenRow">

        <%

		}

		else{

  %>

      <tr class="browserOddRow">

        <%

	}



  %>

    <td> <%=name%></td>

    <td width="15%"><A href="studyalnotsettings.jsp?studyId=<%=studyId%>&mode=M&srcmenu=<%=src%>&protocolId=<%=id%>&selectedTab=<%=selectedTab%>&from=<%=from%>"><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></A></td>
    <td width="15%"><A href="studyeventnotbrowse.jsp?studyId=<%=studyId%>&mode=M&srcmenu=<%=src%>&protocolId=<%=id%>&selectedTab=<%=selectedTab%>&from=<%=from%>"><%=LC.L_Evt_StatusNotifications%><%--Event Status Notifications*****--%></A></td>


<!-- commented by gopu for Bugzilla Issues #2492 -->
    <!--td width="15%"><A href="studycrfnotbrowse.jsp?studyId=<%=studyId%>&mode=M&srcmenu=<%=src%>&protocolId=<%=id%>&selectedTab=<%=selectedTab%>&from=<%=from%>">Form Status Notifications</A></td-->

	<!--<td width="15%"></td> -->
	<%
		if (alNotFlag.trim().equals("1"))
			{
		%>
		    <td width="15%"><A href="refreshstudyalnot.jsp?studyId=<%=studyId%>&srcmenu=<%=src%>&protocolId=<%=id%>&selectedTab=<%=selectedTab%>&from=<%=from%>"><font color="red" ><%=MC.M_ApplyToAll_EnrlPat%><%--Apply to all enrolled <%=LC.Pat_Patients_Lower%>*****--%></font></A></td>
		<%
			}
			else
			{
		%>
			<td width="15%">&nbsp;</td>
		<%
			}
		%>

      </tr>

      <%

	displayCounter++;


	} //main loop

%>
</table>

    </table>

  </Form>

<%

	} //end of if body for page right

	else

	{

%>

	  <jsp:include page="accessdenied.jsp" flush="true"/>

 <%	} //end of else body for page right

} // end of if for study saved

 //}//initial
}//end of if body for session

else{

%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>


<div class ="mainMenu" id = "emenu">

 <!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>

</body>

</html>

