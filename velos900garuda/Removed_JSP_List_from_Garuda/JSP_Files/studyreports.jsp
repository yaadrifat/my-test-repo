<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>



<head>



<title><%=LC.L_My_Rpts%><%--My Reports*****--%></title>


<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<SCRIPT>

function fMakeVisible(type){

	typ = document.all("year");

	typ.style.visibility = "visible";

}



function fValidate(formobj){

	reportChecked=false;

	for(i=0;i<formobj.reportNumber.length;i++){

		sel = formobj.reportNumber[i].checked;

		if (formobj.reportNumber[i].checked){

	   		reportChecked=true;

		}

	}

	if (reportChecked==false) {

		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/

		return false;

	}



	if (formobj.studyPk.value == "") {

		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

		return false;

	}



	reportNo = formobj.repId.value;



	switch (reportNo) {

		case "44": //Protocol Calendar Template

			if (formobj.study2.value == "") {

				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}



			if (formobj.protId.value == "") {

				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/

				return false;

			}

		break;



		case "21": //Estimated Study Budget

			if (formobj.study2.value == "") {

				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}



			if (formobj.protId.value == "") {

				alert("<%=MC.M_Selc_PcolCal%>");/*alert("Please select a Protocol Calendar");*****/

				return false;

			}

		break;

	 default:

 			if (formobj.study1.value == "") {

 				alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

				return false;

			}

			break;

	}

}



function fSetId(ddtype,formobj){

	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them

		for(i=0;i<formobj.reportNumber.length;i++)	{

			if (formobj.reportNumber[i].checked){

				lsReport = formobj.reportNumber[i].value;

				ind = lsReport.indexOf("%");

				formobj.repId.value = lsReport.substring(0,ind);

				formobj.repName.value = lsReport.substring(ind+1,lsReport.length);

				break;

			}

		}

	}



	if (ddtype == "study1") {

		i=formobj.study1.options.selectedIndex;

		formobj.val.value = formobj.study1.options[i].text;

		formobj.studyPk.value = formobj.study1.options[i].value;

	}



	if (ddtype == "study2") {

		formobj.selProtocol.value = "None";

		formobj.protId.value = "";

		i=formobj.study2.options.selectedIndex;

		formobj.val.value = formobj.study2.options[i].text;

		formobj.studyPk.value = formobj.study2.options[i].value;

	}



}



</SCRIPT>



<SCRIPT language="JavaScript1.1">

function openProtWindow(formobj) {

	lstudyPk = formobj.studyPk.value;

	if ((lstudyPk == "") || (lstudyPk==null) ) {

		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

			return false;

	}



	if ((lstudyPk == "  ") || (lstudyPk==null) ) {

		alert("<%=MC.M_Selc_Std%>");/*alert("Please select a <%=LC.Std_Study%>");*****/

			return false;

	}





	rpType='all';

	windowName=window.open("protocolPopup.jsp?studyPk="+lstudyPk+"&reportType="+rpType,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400");

	windowName.focus();

}



</SCRIPT>





</head>







<% String src="";



src= request.getParameter("srcmenu");



%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB"%>



<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>

<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>





<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>



<body>

<br>



<DIV class="formDefault" id="div1">



<%



   HttpSession tSession = request.getSession(true);



   if (sessionmaint.isValidSession(tSession))



   {



	String uName =(String) tSession.getValue("userName");

	String userId = (String) tSession.getValue("userId");

 	String acc = (String) tSession.getValue("accountId");

	String tab = request.getParameter("selectedTab");

	int counter=0;

	int studyId=0;

	StringBuffer study=new StringBuffer();

	StudyDao studyDao = new StudyDao();

	studyDao.getReportStudyValuesForUsers(userId);



	study.append("<SELECT NAME=study1 onChange=fSetId('study1',document.reports)>") ;

	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){

		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();

		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");

	}

	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

	study.append("</SELECT>");



	%>

	<%/* SV, 8/24/04, fix for bug 1673, _new is not a keyword, _blank is and it opens new window instance each time which is what we want for reports.*/%>


<Form Name="reports" method="post" action="repRetrieve.jsp" target="_blank">



<Input type="hidden" name="val" value="">

<input type="hidden" name="type" >

<Input type = hidden  name=protId value="">

<Input type=hidden name="repId">

<Input type=hidden name="repName">

<Input type=hidden name="id">

<Input type=hidden name="studyPk">





<P class = "userName">

	<%= uName %>

</P>



<jsp:include page="reporttabs.jsp" flush="true"/>





<input type="hidden" name="srcmenu" value='<%=src%>'>



<input type="hidden" name="selectedTab" value='<%=tab%>'>

<br>

<table width="100%" cellspacing="0" cellpadding="0" border=0 >

	<tr>

		<td colspan = 3>

			<p class = "defComments"><%=MC.M_SelRpt_ListBelow%><%--Select a report from the list below, specify appropriate filters and click on the 'Display' button*****--%> </p>

			<BR>

		</td>

	</tr>

	<tr>

		<td width=40%>

			<Font class=numberFonts > <%=LC.L_1%><%--1*****--%>.</font><Font class=reportText ><I><%=MC.M_Selc_YourRptType%><%--Select your report type*****--%> </I></font>

		</td>

		<td width=40%>

			<Font class=numberFonts ><%=LC.L_2%><%--2*****--%>.</font> <Font class=reportText ><I><%=MC.M_Selc_YourRptFilters%><%--Select your report filters*****--%></I></font>

		</td>

		<td width=20%>

			<Font class=numberFonts ><%=LC.L_3%><%--3*****--%>.</font> <Font class=reportText ><I><%=LC.L_View_YourRpt%><%--View your report*****--%></I></font>

		</td>

	</tr>

</table>

<HR class=blue></hr>



<table width="100%" cellspacing="0" cellpadding="0" border=0 >

	<tr>

		<td colspan=3>

			<p class = "reportHeadings"><%=LC.L_Std_PcolRpts%><%--<%=LC.Std_Study%> Protocol Reports*****--%></p>

		</td>

	</tr>

	<tr>

		<td width=40%>

			<%

			int accId = EJBUtil.stringToNum(acc);

			CtrlDao ctrl = new CtrlDao();

			ctrl.getControlValues("studyspecific");

			String catId = (String) ctrl.getCValue().get(0);

			repdao.getAllRep(EJBUtil.stringToNum(catId),accId); //Study Related Reports

			ArrayList repIds= repdao.getPkReport();

			ArrayList names= repdao.getRepName();

		   	ArrayList desc= repdao.getRepDesc();

			int len = repIds.size() ;

			CtrlDao ctrl1 = new CtrlDao();

			ctrl1.getControlValues("protcalspecific");

			catId = (String) ctrl1.getCValue().get(0);

			repdao1.getAllRep(EJBUtil.stringToNum(catId),accId); //Study Related Reports

			ArrayList repIdsProtCal= repdao1.getPkReport();

			ArrayList namesProtCal= repdao1.getRepName();

		   	ArrayList descProtCal= repdao1.getRepDesc();

			int lenProtCal = repIdsProtCal.size() ;

			int repId=0;

			String name="";

			for(counter = 0;counter<len;counter++)

			{

				repId = ((Integer)repIds.get(counter)).intValue();

				name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();

				%>

				<Input Type="radio" name="reportNumber" value="<%=repId%>%<%=name%>" onClick="fSetId('report',document.reports)"> <%=name%>

				<BR>

			<%}%>

		</td>

		<td width=40%>

			<FONT class=comments>

			<%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%>: &nbsp;</FONT><%=study%>

		</td>



		<td width=10%>

		</td>

	</tr>

	</table>

<HR class=blue></HR>



<% study=new StringBuffer();

	study.append("<SELECT NAME=study2 onChange=fSetId('study2',document.reports)>") ;

	for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){

		studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();

		study.append("<OPTION value = "+ studyId +">" + (studyDao.getStudyNumbers()).get(counter) + "</OPTION>");

	}

	study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

	study.append("</SELECT>");



%>



<table width="100%" cellspacing="0" cellpadding="0" border="0">

	<tr>

		<td colspan=3>

			<p class = "reportHeadings"><%=MC.M_Pcol_CalRpts%><%--Protocol Calendar Specific Reports*****--%></p>

			<BR>

		</td>

	</tr>

	<tr>

		<td width=40%>



<%	for(counter = 0;counter<lenProtCal;counter++)

	{

	repId = ((Integer)repIdsProtCal.get(counter)).intValue();

	name=((namesProtCal.get(counter)) == null)?"-":(namesProtCal.get(counter)).toString();

%>



		<Input Type="radio" name="reportNumber" value="<%=repId%>%<%=name%>" onClick=fSetId('report',document.reports)> <%=name%>

		<BR>



<%}%>



		</td>

		<td width=40%>

			<FONT class="comments"><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%>:&nbsp;</FONT><%=study%>

			<BR>

			<A HREF=# onClick="openProtWindow(document.reports)"><%=LC.L_Select_PcolCal%><%--Select Protocol Calendar*****--%>:</A>

			<Input type=text name=selProtocol size=20 READONLY value=None>

		</td>



		<td width=10%>

		</td>

	</tr>

</table>





<table width="100%" cellspacing="0" cellpadding="0" border=0 >

<tr>

	<td colspan=2 align=right>

	<!--<Input type="submit" name="submit" value="Display" >-->

	  <input type="image" src="../images/jpg/Display.gif" onClick = "return fValidate(document.reports)" border=0>

	</td>

</tr>

</table>

</form>

<%} //end of if session times out



else



{



%>



<jsp:include page="timeout.html" flush="true">



</jsp:include>



<%



}



%>





<div>



<jsp:include page="bottompanel.jsp" flush="true">



</jsp:include>



</div>





</div>







<DIV class="mainMenu" id = "emenu">



  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->



</DIV>







</body>



</html>

