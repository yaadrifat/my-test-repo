<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Alerts_Notifications%><%--Alerts and Notifications*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="JavaScript1.1">

function  validate(formobj){    
  
     if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
     }

}

function changeLock(obj, frm)

	{

	  if (obj.checked)
	  {
			frm.lockSetting.checked = true;
			edLock = 1;
		}
		else
		{
			frm.lockSetting.checked = false;
			edLock = 0;
		}
		frm.lockText.value = edLock ;
    	 
	}
/////////////////////

</SCRIPT>




</head>



<% String src="";
src= request.getParameter("srcmenu");
%>	

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="alnot" scope="page" class="com.velos.esch.web.alertNotify.AlertNotifyJB" />
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>

<body>
<br>

<DIV class="browserDefault" id="div1"> 


  <%

   HttpSession tSession = request.getSession(true); 

   if (sessionmaint.isValidSession(tSession))
   {


   	String mode="M";//request.getParameter("mode");
	String uName = (String) tSession.getValue("userName");
	String studyId = request.getParameter("studyId");	
	String protocolId = request.getParameter("protocolId");

	String studyTitle = "";
	String studyNumber = "";
	
	studyB.setId(EJBUtil.stringToNum(studyId));
    studyB.getStudyDetails();

    studyTitle = studyB.getStudyTitle();
	studyNumber = studyB.getStudyNumber();
	String setting = "N";
	String editLock = "1";

	EventInfoDao alDao = new EventInfoDao();


	String radG = "";
	String radGR = "";
	String radN = "";
	String radNR = "";
	String checkedFlag = "";
	
	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	} else {
		protocolId = "";
	}

%>

<P class = "userName"> <%= uName %> </P>
<P class="sectionHeadings"> <%=MC.M_MngPat_AlrtNotify%><%--Manage <%=LC.Pat_Patients%> >> Alerts and Notifications*****--%> </P>
	<%

if(protocolId != null) {

alDao = alnot.getStudyAlertNotify(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId));
 
//setting = alnot.getStudyAlertNotify(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(protocolId));


			if (alDao.getAlnot_globalflags().size() > 0)
		   	{
			
			 setting = (String) (alDao.getAlnot_globalflags()).get(0);
			 editLock = (String) alDao.getAnFlags().get(0);
		  	}
		 	if (setting == null || setting.trim().equals("") ||  setting.equals("null"))
		  	{
		 		setting = "N";
				editLock = "1";
		 	}

if (setting.equals("N"))
	radN = "CHECKED";
else if (setting.equals("NR"))
	radNR = "CHECKED";
else if (setting.equals("G"))
	radG = "CHECKED";
else if	(setting.equals("GR"))
	radGR = "CHECKED";	

if (editLock.equals("1"))
	checkedFlag = "CHECKED";
	else
	checkedFlag = "";
	
%>

<Form name="alertnotify" method=post action="updatestudyalnot.jsp" onsubmit="return validate(document.alertnotify)">

<input type="hidden" name="studyId" Value="<%=studyId%>">
<input type="hidden" name="protocolId" Value="<%=protocolId%>">
<input type="hidden" name="srcmenu" Value="<%=src%>">

<table width=100%>

<table width=100%>
<tr>
	<td class=tdDefault width = 20% ><B><%=LC.L_Study_Number%><%--Study Number*****--%>:</B></td><td class=tdDefault><%=studyNumber%></td>
</tr>
<tr>
	<td class=tdDefault><B><%=LC.L_Study_Title%><%--Study Title*****--%>:</B></td><td class=tdDefault><%=studyTitle%></td>
</tr>
 <tr>
 	<td class=tdDefault><B><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%>:</B></td><td class=tdDefault><%=protName%></td>
 </tr>
</table>

<br>
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
<td>
<input type="Radio" name="alnotsetting" value="N" <%=radN%>><%=LC.L_Disable_GlobalSettings%><%--Disable Global Settings*****--%>
</td>
</tr>

<tr>
<td>
<input type="Radio" name="alnotsetting" value="NR" <%=radNR%>><%=MC.M_DsbleGlblSettRetainIndv_Enrl%><%--Disable Global Settings and Retain Individual Settings (for current enrollments)*****--%> 
</td>
</tr>

<tr>
<td>
<input type="Radio" name="alnotsetting" value="G" <%=radG%>><%=MC.M_Apply_GlobalSett%><%--Enable Global Settings and Apply to All*****--%>
</td>
</tr>

<tr>
<td>
<input type="Radio" name="alnotsetting" value="GR" <%=radGR%>><%=MC.M_EnblGlblSett_RtnInvSettForEnrl%><%--*Enable Global Settings and Retain Individual Settings (for current enrollments)****--%>	
</td>
</tr>
<tr height=10><td></td></tr>
<tr>
<td><P class = "defComments"><FONT class="Mandatory"><%=MC.M_UncheckLockSett_ReqChg%><%--To make changes in the Notifications below, uncheck the 'Lock Settings' checkbox and submit before editing. Do the required changes, Lock the Settings and submit to apply.*****--%></font></p>
</td>
</tr>
<tr>
<td>
<input type="CheckBox" name="lockSetting" value=<%=editLock%> <%=checkedFlag%> onclick="changeLock(this, document.alertnotify)"><%=LC.L_Lock_Settings%><%--Lock Settings*****--%>
<input type="hidden" name="lockText" value=<%=editLock%>>	
</td>
</tr>
</table>

<br>
<table width=100%>
<tr>
<td width=30%>
<a href="alertnotifyglobal.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=<%=mode%>&globalFlag=<%=setting%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&protocolId=<%=protocolId%>&lockSetting=<%=editLock%>"><%=LC.L_Evt_StatusNotifications%><%--Event Status Notifications*****--%></A>
</td>
<td width=30%>
<a href="alertnotifysettings.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=<%=mode%>&globalFlag=<%=setting%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&protocolId=<%=protocolId%>&lockSetting=<%=editLock%>"><%=LC.L_Alert_Notific%><%--Alerts/Notifications*****--%></A>
</td>

<td width=30%>
<a href="crfngbrowser.jsp?srcmenu=tdMenuBarItem5&selectedTab=5&mode=<%=mode%>&globalFlag=<%=setting%>&page=patientEnroll&generate=N&studyId=<%=studyId%>&protocolId=<%=protocolId%>&lockSetting=<%=editLock%>"><%=LC.L_Crf_Notifications%><%--Crf Notifications*****--%></A>
</td>

</tr>
</table>
<br>
<table>
<tr>
	   <td>
		<%=LC.L_EHypenSign%><%--E-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	  <td>&nbsp;&nbsp</td>	

	   <td>
		<input type="password" name="eSign" maxlength="8">
	   </td>
	  <td>&nbsp;&nbsp</td>	
	   <td align=right>
	<button type="submit"><%=LC.L_Submit%></button>
	</td>
	<td>&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp</td>
      <td>&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp 
		<A type="submit" href="studyalnotsetting.jsp?srcmenu=<%=src%>&studyId=<%=studyId%>"><%=LC.L_Back%></A>
		
      </td>
	</tr>
	</table>
</table>	
</FORM>

<%
 } 

} //end of if session times out
else
{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
   <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>

<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->   

</div>
</body>
</html>
