<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Inv_Creation%><%--Invoice Creation*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>


</HEAD>

<script>

function setMainDateFilter(formobj)
{
    var dtFilterFrom;
    var dtFilterTo;
    var yr;
    var month;
    var monthYr;
    var lastDayOfMonth;	
    var monthPrefix = "";
    		

	if (formobj.dateRangeType[0].checked) //ALL
	{
		dtFilterFrom = "";
		dtFilterTo = ""; 

	}
		else if (formobj.dateRangeType[1].checked) //Year Filter
	{
		
		
		yr = formobj.yearFilter.value;

		if (yr == '0')
		{
			alert("<%=MC.M_Selc_AnYear%>");/*alert("Please select an Year");*****/
			formobj.yearFilter.focus()			
			return false;
		}

		dtFilterFrom = getFormattedDateString(  yr,  "01",  "01");
		dtFilterTo = getFormattedDateString(  yr,  "31",  "12");

	}
	else if (formobj.dateRangeType[2].checked) //Month Filter
	{

		monthYr = formobj.monthFilterYear.value;			
		month =  formobj.monthFilter.value;			

		if (month == '0')
		{
			alert("<%=MC.M_Selc_Month%>");/*alert("Please select a Month");*****/
			formobj.monthFilter.focus()			
			return false;
		}

		if (monthYr == '0')
		{
			alert("<%=MC.M_Selc_AnYear%>");/*alert("Please select an Year");*****/
			formobj.monthFilterYear.focus()			
			return false;
		}
		lastDayOfMonth = getLastDayOfMonth(month,monthYr);
		
		if (month > 0 && month <10)
		{
			monthPrefix = "0";
		}		
		else
		{
			monthPrefix = "";
		}
		dtFilterFrom =  getFormattedDateString(  monthYr,  "01",  month );  
		dtFilterTo =  getFormattedDateString(  monthYr,  lastDayOfMonth,  month );   


	} else if (formobj.dateRangeType[3].checked) //date Range Filter
	{

		dtFilterFrom = formobj.drDateFrom.value;
		dtFilterTo = formobj.drDateTo.value;
		
		if(dtFilterFrom ==''||dtFilterFrom ==null){
			alert("<%=MC.M_SelFromDt_ForRangeFilter%>");/*alert("Please select a 'From Date' for the Date Range filter");*****/
			return false;
		}
		if(dtFilterTo ==''||dtFilterTo  ==null){
			alert("<%=MC.M_SelToDt_ForRangeFilter%>");/*alert("Please select a 'To Date' for the Date Range filter");*****/
			return false;
		}
		
	}


	formobj.dtFilterDateFrom.value = dtFilterFrom;
	formobj.dtFilterDateTo.value = dtFilterTo;
	
	return true;

}

function validate(formobj) {

	if ( setMainDateFilter(formobj) == false ) 
	return false;
	
    mode = formobj.mode.value;
	//04Apr2011 @Ankit #5771
	/*if (!(validate_col('Invoice Number',formobj.invNumber))) return false;@Ankit#5771*/
	if (!(validate_col('Payment Due In',formobj.paymentDueIn))) return false;
	if (!(validate_col('Receivable Status',formobj.milestoneReceivableStatus))) return false;
	if (!(validate_col('Invoice Date',formobj.invDate))) return false;
	 
	
	 //added for date field validation
	 if (!(validate_date(formobj.invDate))) return false
		 
//	if (!(validate_col('e-Signature',formobj.eSign))) return false;
//  if(isNaN(formobj.eSign.value) == true) {
//		alert("Incorrect e-Signature. Please enter again");
//		formobj.eSign.focus();
//		formobj.subVal.value="Submit";
//		return false;
//	   }

return true;
} 
</script>

<BODY>

<DIV class="popDefault" id="div1">

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<jsp:useBean id="invB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>
 <jsp:include page="include.jsp" flush="true"/>


<%
  HttpSession tSession = request.getSession(true); 
  String studyId = "";
  String mode = "";

 if (sessionmaint.isValidSession(tSession))
	{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
		//04Apr2011 @Ankit #5780 Desc:-This attribute is removed on updateinvoice.jsp
		session.setAttribute("flag","true");

		studyId = request.getParameter("studyId");
	
		studyB.setId(EJBUtil.stringToNum(studyId ));
		studyB.getStudyDetails();

//JM: added on 17Nov2006, 04Apr2011 @Ankit #5771

//String invNumber = invB.getInvoiceNumber();


	String acc = (String) tSession.getValue("accountId");
	int accId = 0;

	
		accId = EJBUtil.stringToNum(acc);	
	 String yrFilterDD = ""; String monthFilterDD = ""; String monthFilterYearDD = "";
	 String selMonth = ""; String selMonthYear = ""; String selYear="";
	 String defDateFilterDateFrom  = ""; String defDateFilterDateTo  = "";

	 
	
		yrFilterDD = DateUtil.getYearDropDown("yearFilter",selYear,false);
		monthFilterDD = DateUtil.getMonthDropDown("monthFilter",selMonth,false);
		monthFilterYearDD = DateUtil.getYearDropDown("monthFilterYear",selMonthYear,false);
		
		String defDateFilterType = "A";
		String userId = "";
		
		userId = (String) tSession.getValue("userId");
		
		///commented, will be implemented after the initial release
		/*StudySiteDao ssDao2 = new StudySiteDao();
		ssDao2	= studySiteB.getSitesForStatAndEnrolledPat(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userId), accId);
		ArrayList arrSites = ssDao2.getSiteIds();
		ArrayList arrSiteNames = ssDao2.getSiteNames();
		String dSites = "";
		
		dSites = EJBUtil.createPullDownOrg("dSites",0,arrSites,arrSiteNames); */
	
		String dSites = "<input name=\"dSites\" value=\"0\" type = \"hidden\" >";

%>
<Form name="inv" id="invstep1" action="updateinvoice.jsp" method="post" onSubmit="if (validate(document.inv)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<P class="comments"><%=MC.M_EtrDets_ToCreateYourInv%><%--Enter the following details to create your Invoice*****--%>:</P>

<table  width="100%" border="0">
 <tr> 
 	<td class=tdDefault width="18%"><%=LC.L_Study_Number%><%--Study Number*****--%></td>
	 <td class=tdDefault>
		<%= studyB.getStudyNumber()%>
	</td>
</tr> 
 <!--<tr> 
 	<td class=tdDefault>Study Title</td>
	 <td class=tdDefault>
		<%= studyB.getStudyTitle()%>
	</td>
</tr> -->

 <tr>
 <!-- 04-04-2011 @Ankit #5771 -->
 	<td class=tdDefault><%=LC.L_Inv_Number%><%--Invoice Number*****--%> <FONT class="Mandatory">* </FONT>(<%=LC.L_SysHypenGen%><%--system-generated*****--%>)</td>
	 <td>
		<input type=text name="invNumber" value="" size=10 maxlength=50 class="leftAlign" >
	</td>
</tr> 
<tr>
<%-- INF-20084 Datepicker-- AGodara --%>
 	<td class=tdDefault><%=LC.L_Invoice_Date%><%--Invoice Date*****--%>  <FONT class="Mandatory">* </FONT></td>
	 <td><input type="text" name="invDate" class="datefield" size = 15 MAXLENGTH = 50></td>

<!--</tr> 
	<tr> 
 	<td  class=tdDefault>Organization</td>
	<td>

	</td>
	 
</tr>  -->
		<%=dSites%>	
	
	<tr>
 <tr> 
 	<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Payment_Due%><%--Payment Due*****--%>  <FONT class="Mandatory">* </FONT></td>
	 <td>
		<input type=text name="paymentDueIn" size=5 maxlength=10 class="leftAlign" >
		<Select name="paymentDueUnit">
			<option value="D" SELECTED><%=LC.L_Day_S%><%--Day(s)*****--%></option>
			<option value="W" ><%=LC.L_Week_S%><%--Week(s)*****--%></option>
			<option value="M" ><%=LC.L_Month_S%><%--Month(s)*****--%></option>
			<option value="Y" ><%=LC.L_Year_S%><%--Year(s)*****--%></option>
		</Select>
	 </td> 
 </tr>
	<tr>
	<td class=tdDefault >&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Addressed_To%><%--Addressed To*****--%></td>
	<td>
	<input type=hidden name=addressedTo value=''>
          <input type=text name=addressedToName size=30 value='' readonly>
          <A HREF=# onClick=openCommonUserSearchWindow("inv","addressedTo","addressedToName") ><%=LC.L_Select_User%><%--Select User*****--%></A> 
    </td>
	
	
	</tr>
	<tr>
	<td class=tdDefault>&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Sent_From%><%--Sent From*****--%></td>
	<td>
	<input type=hidden name=sentFrom value=''>
          <input type=text name=sentFromName size=30 value='' readonly>
          <A HREF=# onClick=openCommonUserSearchWindow("inv","sentFrom","sentFromName") ><%=LC.L_Select_User%><%--Select User*****--%></A> 
    </td>
	
	
	</tr>
	<!--JM: 13NOV06 -->
	<tr>
	<td class= tdDefault> <%=LC.L_Internal_Acc%><%--Internal Account*****--%> # </td>
	<td><input type ="text" name="intAccNum" size=15 maxlength =60></td>
	</tr>
	<tr>
	<td class=tdDefault><%=LC.L_Header%><%--Header*****--%></td>
	<td>
		<Textarea name=invHeader rows=2 cols=75></textarea>
    </td>
	</tr>
	<tr>
	
	<td class=tdDefault><%=LC.L_Footer%><%--Footer*****--%></td>
	<td>
		<Textarea name=invFooter rows=2 cols=75></textarea>
    </td>
	</tr>
	
	<tr>
	
	<td  class=tdDefault><%=LC.L_Notes%><%--Notes*****--%></td>
	<td>
		<Textarea name=invNotes rows=2 cols=75 ></textarea>
    </td>
	</tr>
<tr><td colspan="2">
	<table width="100%">
	<tr>
	<td class=tdDefault width="18%"><%=LC.L_Disp_ListOf%><%--Display List of*****--%>  <FONT class="Mandatory">* </FONT></td>
		<td width="20%">
		<Select name="milestoneReceivableStatus">
			<option value="A" ><%=LC.L_All_Receivables%><%--All Receivables*****--%></option>
			<!--<option value="OR" >Outstanding Receivables</option>-->
			<option value="UI" ><%=LC.L_Uninv_Receivables%><%--Un-invoiced Receivables*****--%></option>
			<option value="" SELECTED><%=LC.L_Select_AnOption%><%--Select an Option*****--%></option>
		</Select>
	</td>
	
		<td class=tdDefault width="12%" align="center"><%=LC.L_For_Dates%><%--For Dates*****--%>:</td>
		<td class=tdDefault width="50%"><input type = "radio" name="dateRangeType" value="A" 
		<%if(StringUtil.isEmpty(defDateFilterType) || defDateFilterType.equals("A")) { %> CHECKED <% } %> ><i><%=LC.L_All%><%--All*****--%></i>
		
		&nbsp;&nbsp;&nbsp;<input type = "radio" name="dateRangeType" value="Y"
		<% if( defDateFilterType.equals("Y")) { %> CHECKED <% } %> ><i> <%=LC.L_Year%><%--Year*****--%> </i><%=yrFilterDD%>
		
		&nbsp;&nbsp;&nbsp;<input type = "radio" name="dateRangeType" value="M"
			<% if( defDateFilterType.equals("M")) { %> CHECKED <% } %> > <i><%=LC.L_Month%><%--Month*****--%> </i><%=monthFilterDD%>&nbsp;<%=monthFilterYearDD%>

		<BR><BR><input type = "radio" name="dateRangeType" value="DR"
			<% if( defDateFilterType.equals("DR")) { %> CHECKED <% } %>	><i> <%=LC.L_DateRange_Frm%><%--Date Range: From*****--%></i>
<%-- INF-20084 Datepicker-- AGodara --%>		
			<Input type=text name="drDateFrom" class="datefield" READONLY SIZE=7 
				<% if( defDateFilterType.equals("DR")) { %> value = <%=defDateFilterDateFrom%>  <% } %>> 
				
			 &nbsp;<i><%=LC.L_To%><%--To*****--%> </i>&nbsp;
			<Input type=text name="drDateTo" class="datefield" READONLY SIZE=7
				<% if( defDateFilterType.equals("DR")) { %> value = <%=defDateFilterDateTo %>  <% } %> > 
		
		</td>
	</tr>
	</table>
</td>
</tr>		

<!--Added by Manimaran for Enh.#FIN12 --Requirement Changed-->
<!-- tr><td class= tdDefault> <input type="checkbox" name="includeHeader" value=""> Include Account Specific Header </td> </tr-->
<!--  tr><td class= tdDefault> <input type="checkbox" name="includeFooter" value=""> Include Account Specific Footer </td> </tr-->

<Input type="hidden" name="dtFilterDateFrom" SIZE=10 value=<%=defDateFilterDateFrom%>> 
<Input type="hidden" name="dtFilterDateTo" SIZE=10 value=<%=defDateFilterDateTo%>> 
 

<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>> 
<INPUT NAME="studyId" TYPE=hidden  value=<%=studyId%>> 



</TABLE>

<table><tr><td><FONT class="Mandatory"><%=MC.M_InvItem_MstoneRule%><%--Note: To ensure inclusion of partially invoiced line items using the 'Un-invoiced Receivables' option, please ensure that these Milestone Rules do not have a Count greater than 1 AND always select the 'Detail View' option when including them in your invoice*****--%></FONT>
</td></tr></table>
<!-- Please select option 'All Receivables' if you have compound Milestones with a value of > 1 in 'count' field OR if you have selected 'High Level' option for the milestone previously. -->

</table>

 

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="N"/>
		<jsp:param name="formID" value="invstep1"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


   <input type="hidden" name="subVal" value="Sumbit">
</Form>

<%
	}
 else{%>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/> 
 <%}%>

</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>


