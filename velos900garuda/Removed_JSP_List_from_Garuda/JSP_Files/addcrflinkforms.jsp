<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<%@page import="com.velos.esch.web.portalForms.PortalFormsJB,com.velos.esch.business.portalForms.impl.PortalFormsBean"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<title><%=MC.M_Add_FrmStd%><%--Add Forms to <%=LC.Std_Study%>*****--%></title>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventcrf" scope="request" class="com.velos.esch.web.eventCrf.EventCrfJB"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<%@ page language = "java" import = "com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<DIV class="popDefault" id="div1">

<%

	String eSign = request.getParameter("eSign");	
 	HttpSession tSession = request.getSession(true);  
 	if (sessionmaint.isValidSession(tSession)){
%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");	
	if(!oldESign.equals(eSign)) 
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {	   
	int firstpos=0;
	int len=0;
        int errorCode=0;	
	String creator=(String) tSession.getAttribute("userId");
	String ipAdd=(String)tSession.getAttribute("ipAdd");
	String[] selForms = request.getParameterValues("selForms"); //selected form ids 
	String eventId=request.getParameter("eventId");
	int eventId1=StringUtil.stringToNum(eventId);
	if (!(selForms==null)) {
		len = selForms.length;
	}
	
	int strlen = 0;
	ArrayList<String> selId = new ArrayList<String>();
	int secondpos = 0;
	ArrayList<String> selName = new ArrayList<String>();
	ArrayList<String> selDisp = new ArrayList<String>();

	for (int i=0;i< len;i++) {
		strlen = (selForms[i]).length();
		firstpos = (selForms[i]).indexOf("[VELSEP]",1);
		selId.add(i,selForms[i].substring(0,firstpos));
		secondpos = (selForms[i]).indexOf("[VELSEP]",firstpos+8);
		selName.add((selForms[i]).substring(firstpos+8,secondpos));
		selDisp.add(i,(selForms[i]).substring(secondpos+8,strlen));
	}

	PortalFormsDao pfDao = new PortalFormsDao();
	PortalFormsJB portalFormsJB = new PortalFormsJB();
	if(selId.indexOf("~")!=0){
		pfDao = portalFormsJB.getPortalFormsDAO(eventId1,selId);
	}
	eventcrf.deleteEventCrf(eventId1,AuditUtils.createArgs(session,"",LC.L_Evt_Lib));
	String propagateInVisitFlag = request.getParameter("propagateInVisitFlag"); 
	String propagateInEventFlag = request.getParameter("propagateInEventFlag");
	String protocolId = request.getParameter("protocolId");
	String calledFrom = request.getParameter("calledFrom");
	
	for (int i=0;i< len;i++) {	
		eventcrf.setFkEvent(eventId1);
		if ((selDisp.get(i).equals("SP")) || (selDisp.get(i).equals("PS") || (selDisp.get(i).equals("PR")) )){
			eventcrf.setFkForm(selId.get(i));
		} else {
			eventcrf.setOtherLink(selName.get(i));
		   	eventcrf.setFkForm("0");		   
		}		
		eventcrf.setFormType(selDisp.get(i));
		eventcrf.setEventCrfCreator(creator);
		eventcrf.setEventCrfLastModBy(creator);
		eventcrf.setEventCrfIpAdd(ipAdd);				
		errorCode = eventcrf.setEventCrfDetails();
	}
	while (selId.remove("~"));
	if ( errorCode != -2 && !selId.isEmpty() ){
		portalFormsJB.setPortalFormsDAO(pfDao);
	}
		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))
			propagateInVisitFlag = "N";

		if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))
			propagateInEventFlag = "N";
			
			if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y"))) {
				 
				eventdefB.propagateEventUpdates(EJBUtil.stringToNum(protocolId), EJBUtil.stringToNum(eventId), "EVENT_CRF_FORM", 0, propagateInVisitFlag, propagateInEventFlag, "N", calledFrom);
			}
			
			
	if ( errorCode == -2  )	{
%>
		<br><br><br><br>
	<p class = "sectionHeadings" align="center"><%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully*****--%> 	</p>
<%
		
	} //end of if for Error Code

	else {
%>	
	<br><br><br><br>
			
	<p class = "sectionHeadings" align="center" ><%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>	
	
	</td>
	</tr>
	</table>	
	<script>
		window.opener.location.reload();
		setTimeout("self.close()",1000);
	</script>	
	<%
	}
       } // end of e-sign end
    }   //end of if body for session             	
    else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% }%>
</div>

</body>
</html>