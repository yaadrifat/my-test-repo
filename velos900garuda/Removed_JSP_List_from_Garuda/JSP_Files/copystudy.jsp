<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_CopyExisting_StdPcol%><%--Copy an Existing <%=LC.Std_Study%> Protocol*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>



<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">



</head>



<SCRIPT Language="javascript">
 if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; } 
 if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }
 
 function  blockSubmit() {
	 setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
	 setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
 }
 function  validate(formobj){
     if (!(validate_col('Select a Study',formobj.oldStudy))) return false
     if (!(validate_col('Study Number',formobj.newStudyNum))) return false
     if (!(validate_col('Data Manager',formobj.dataManagerName))) return false
 	 if (!(validate_col('Esign',formobj.eSign))) return false
	 if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

		formobj.eSign.focus();

		return false;

	}
   }

  function  validateEsign(formobj){
 	 if (!(validate_col('Esign',formobj.eSign))) return false
	 if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();

		return false;

	}
   }
	function openwin1() {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from=study","TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
	;}
</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>

<jsp:useBean id="TXArmJB" scope="request" class="com.velos.eres.web.studyTXArm.StudyTXArmJB" />
<jsp:useBean id="linkedformsB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB" />

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>





<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>

<br>

<DIV class="formDefault" id="div1"> 

  <%

	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))

	{

	    String usr = null;
		usr = (String) tSession.getValue("userId");

		String uName = (String) tSession.getValue("userName");
		String accId = (String) tSession.getValue("accountId");	
		int accountId = EJBUtil.stringToNum(accId);

	  	String fromstudyId = request.getParameter("fromstudyId");
	  	String studymode = request.getParameter("studymode");
		String from = request.getParameter("fromMode");
	
	%>

        <P class = "userName"><%= uName %></p>
      
   
	<%

		if(from.equals("initial")) 
			{
				StudyDao studyDao = new StudyDao();
  		studyDao = studyB.getStudyValuesForUsers(usr);
  		String studyAuthorName = ""; 
  
  		Integer studyIdVal;
  
  		int counter = 0;
  		StringBuffer studydd =new StringBuffer();
  		studydd.append("<SELECT NAME=oldStudy>") ;
  		if (Integer.parseInt(fromstudyId) <= 0)
  			{
  			studydd.append("<OPTION value=''>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
  			}
  			else
  			{
  			studydd.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
  			}
  		for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
  			studyIdVal = ((Integer)((studyDao.getStudyIds()).get(counter)));
  
  			if (Integer.parseInt(fromstudyId) == studyIdVal.intValue())
  			{
  			studydd.append("<OPTION value = "+ studyIdVal +" selected>" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
  			}
  			else
  			{
  			studydd.append("<OPTION value = "+ studyIdVal +">" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
  			}
  
  		}
  		studydd.append("</SELECT>");	
  
  		//get default user name 
  		
  		 userB.setUserId(EJBUtil.stringToNum(usr));
  		 userB.getUserDetails();
  		 studyAuthorName = userB.getUserFirstName() + " " + userB.getUserLastName()  ;
  

			%>
<br><br>
	     <P class="sectionHeadings"> <%=MC.M_MngPcol_CpyStdPcol%><%--Manage Protocols >> Copy an Existing <%=LC.Std_Study%> Protocol*****--%> </P>
		 		
            <Form name="study" id="copystudy" method="post" action="copystudy.jsp" onsubmit="if (validate(document.study)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		    <input type="hidden" name="srcmenu" value="<%=src%>">
		    <input type="hidden" name="studymode" value="<%=studymode%>">
		    <input type="hidden" name="fromstudyId" value="<%=fromstudyId%>">
		    <input type="hidden" name="fromMode" value="final">
			
			<table  cellpadding="0" cellspacing="0" border="0" width="100%" class="basetbl">
			<tr>
			<td>
   			<%=LC.L_Select_AStd%><%--Select a <%=LC.Std_Study%>*****--%> <FONT class="Mandatory">* </FONT> </td> <td>	<%=studydd.toString()%>	 </td>
			<tr><td colspan = 2>
					
            <P class = "defComments"> <br><%=MC.M_EtrFlw_InfoStdPcol%><%--Please enter the following information for the new <%=LC.Std_Study%> Protocol*****--%>: </FONT><br>  </P> 
			</td></tr>
			<tr><td>            
            <%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%> <FONT class="Mandatory">* </FONT>
			</td>
            <td><input type="text" name="newStudyNum" value="" size=40 maxlength =100 >
			</td></tr>
			<tr> 
		  	<td width="27%"> <%=LC.L_Std_DataManager%><%--<%=LC.Std_Study%> Data Manager*****--%> <FONT class="Mandatory">* </FONT> 
	        </td>
	        <td width="70%"> 
			<input type=hidden name="dataManager" value='<%=usr%>'> 
			<input type=text name="dataManagerName" value='<%=studyAuthorName%>' readonly> 
			<A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A>
			</td>
			</tr>
		    <tr> 
	        <td> <%=LC.L_Title%><%--Title*****--%></td>
	        <td><TextArea name="studyTitle" rows=3 cols=50 MAXLENGTH = 50></TextArea>
	        </td>
		    </tr>
   
		    <jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="copystudy"/>
				<jsp:param name="showDiscard" value="N"/>
			</jsp:include>    
	
		   </table>		   
            </form>            
		<%

		} //end of if for checking whether from.equals("initial")



		if(from.equals("final")) 
		{
		   String eSign = request.getParameter("eSign");  
		   String oldESign = (String) tSession.getValue("eSign");

		   String newStudyNum = request.getParameter("newStudyNum");
		   String newTitle = request.getParameter("studyTitle");
		   String dataManager = request.getParameter("dataManager");
		   String dataManagerName = request.getParameter("dataManagerName");
		   String oldStudy = request.getParameter("oldStudy");
		   
		   int recfound = 0;
		   
			if(!oldESign.equals(eSign)) 
			{

%>
				 <jsp:include page="incorrectesign.jsp" flush="true"/>
<%

				} 
			else 
			{   

				recfound = studyB.validateStudyNumber(accId,newStudyNum);
				if (recfound > 0 )
				{
				
				%>  
				  <jsp:include page="studynumexists.jsp" flush="true"/>
			
				<%
				}
				else
				{
				
				//get study versions
				
				StudyVerDao studyVerDao = studyVerB.getAllVers(EJBUtil.stringToNum(oldStudy));
				ArrayList studyVerIds = studyVerDao.getStudyVerIds();
			   	ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
				int len = studyVerIds.size();

				String verNumber ;
				String studyVerId;
				
							
				//get study protocols
				
				ctrldao= eventassocB.getStudyProts(EJBUtil.stringToNum(oldStudy));			
				
	
				ArrayList eventIds=ctrldao.getEvent_ids() ; 
				ArrayList names= ctrldao.getNames(); 				
				
		   	  	int calLen= eventIds.size();
				
				Integer calid;
				String calname="";
				

				//get the Admin Protocols
				//JM: 10Aug2006 
				ctrldao= eventassocB.getStudyAdminProts(EJBUtil.stringToNum(oldStudy),"");
				ArrayList admEventIds=ctrldao.getEvent_ids() ; 
				ArrayList admNames= ctrldao.getNames();								

		   	  	int admCalLen= admEventIds.size();

				Integer admCalid;
				String admCalname="";	
				
				StudyTXArmDao txDao = new StudyTXArmDao();
				txDao = TXArmJB.getStudyTrtmtArms(EJBUtil.stringToNum(oldStudy));
				ArrayList txIds = txDao.getStudyTXArmIds();
				ArrayList txNames = txDao.getTXNames();
				int txLen = txIds.size();

				String txId = "";
				String txName = "";				

				
				LinkedFormsDao 	linkedformsdao = new LinkedFormsDao();
				linkedformsdao = linkedformsB.getStudyLinkedForms(EJBUtil.stringToNum(oldStudy));

				ArrayList formLibIds=linkedformsdao.getFormId();
				ArrayList formnames= linkedformsdao.getFormName();
				
				String formId;
				String formname;
				int flen= formnames.size();				
				

				
				
			%>
			<br><br>
			     <P class="sectionHeadings"> <%=MC.M_MngPcol_CpyStdPcol%><%--Manage Protocols >> Copy an Existing <%=LC.Std_Study%> Protocol*****--%> </P>
				 <form  name = "savecopy" id = "savecopyForm" method="post" action="savecopystudy.jsp" onsubmit="if (validate(document.savecopy)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); blockSubmit(); return true;}">
				 
				  <input type="hidden" name="srcmenu" value="<%=src%>">
		    	  <input type="hidden" name="studymode" value="<%=studymode%>">
				  <input type="hidden" name="fromstudyId" value="<%=fromstudyId%>">
		    	  <input type="hidden" name="fromMode" value="save">
		  	      <input type="hidden" name="newStudyNum" value='<%=newStudyNum%>'>
  		  	      <input type="hidden" name="dataManager" value=<%=dataManager%>>
   		  	      <input type="hidden" name="dataManagerName" value=<%=dataManagerName%>>
   		  	      <input type="hidden" name="oldStudy" value=<%=oldStudy%>>
   		  	      <input type="hidden" name="studyTitle" value='<%=newTitle%>'>
			
			     <P class="defComments"><%=MC.M_CopyFlw_StdPcol%><%--Copy the following to the new <%=LC.Std_Study%> Protocol*****--%>: <br> </P>
				 <table cellpadding="0" cellspacing="0" border="0" width="100%" class="basetbl">
				 <tr>
				 <td>
				 <b><%=LC.L_Select_Versions%><%--Select Versions*****--%></b>
				 </td>
				 </tr>
			 <%
				for(int ctr = 0;ctr<len;ctr++)
					{
						verNumber=((studyVerNumbers.get(ctr))==null)?"-":(studyVerNumbers.get(ctr)).toString();
						studyVerId = ((Integer) studyVerIds.get(ctr)).toString();
					%>
						 <tr><td>
						 <input type="checkbox" name="checkver" value = <%=studyVerId%> CHECKED><%=verNumber%> <br>
						 </td>
						 </tr>
								
					<%}%>
			 	
				 <tr>
				 <td>
				 <br><b><%=LC.L_SelPatCal%><%--Select <%=LC.Pat_Patient%> Calendars*****--%></b>
				 </td>
				 </tr>
			 <%
		    for(int calctr = 0;calctr<calLen;calctr++)
				{
		    	calid = (Integer)eventIds.get(calctr);
				calname=((names.get(calctr)) == null)?"-":(names.get(calctr)).toString();				
				%>
						 <tr><td>
						 <input type="checkbox" name="checkcal" value = <%=calid%> CHECKED><%=calname%> <br>
						 </td>
						 </tr>						 
				<%
				}
				%>		 
				 <tr>
				 <td>				 
				 <br><b><%=LC.L_Select_AdminCals%><%--Select Admin Calendars*****--%></b>
				 </td>
				 </tr>
			 <%
		    for(int calctr = 0;calctr<admCalLen;calctr++)
				{				
			   	admCalid = (Integer)admEventIds.get(calctr);
			   	admCalname=((admNames.get(calctr)) == null)?"-":(admNames.get(calctr)).toString();
				
				%>
						 <tr><td>
						 <input type="checkbox" name="checkcal" value = <%=admCalid%> CHECKED><%=admCalname%> <br>
						 </td>
						 </tr>
				<%					
				}
							 
			 %>
			 
			 <tr>
				 <td>				 
				 <br><b><%=LC.L_StdTreatArm%><%--<%=LC.Std_Study%> Treatment Arms*****--%></b>
				 </td>
				 </tr>
				 <%
				 for (int j = 0 ; j < txLen; j++){
					 txId = txIds.get(j).toString();
					 txName = (txNames.get(j)==null)? "-" :  txNames.get(j).toString();
					 %>
					 
					 <tr><td>
						 <input type="checkbox" name="checktx" value = <%=txId%> CHECKED><%=txName%> <br>
						 </td>
						 </tr>		 
						 
						 								
					<%	
				 }
				 
				 
				 %>
				 <tr>
				 <td>				 
				 <br><b><%=LC.L_Associated_Forms%><%--Associated Forms*****--%></b>
				 </td>
				 </tr>
				 <%
				 
					for (int j = 0 ; j<flen ;j++     ){
						
						
						formId = formLibIds.get(j).toString();
						
						formname = (formnames.get(j)==null )? "-": formnames.get(j).toString();
					%>
					
					 
					 <tr><td>
						 <input type="checkbox" name="checkforms" value = <%=formId%> CHECKED><%=formname%> <br>
						 </td>
						 </tr>
						 
						 
			<!-- JM: 04AUG2006-->
						
						
					<%	
					}
				 
				 %>
				 
				</table>
				<HR>
				<table>
				 <tr><td><br>
				 <input type="checkbox" name="checkstat" CHECKED ><%=LC.L_Study_Status%><%--<%=LC.Std_Study%> Status*****--%> <br>
				 <input type="checkbox" name="checkteam" CHECKED ><%=LC.L_Std_Team%><%--<%=LC.Std_Study%> Team*****--%> <br>
				 <input type="checkbox" name="checkdict" CHECKED ><%=LC.L_StdOrDict_Settings%><%--<%=LC.Std_Study%>/Dictionaries Settings*****--%>
				 </td>
				 </tr>
				 </table>
				 
				<jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="savecopyForm"/>
					<jsp:param name="showDiscard" value="N"/>
				</jsp:include>

					<table><tr><td height="7">&nbsp;</td></tr></table>
				 </form>
				 
			<%			
				}
			}	
		}

} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}


%>


  <div class = "myHomebottomPanel"> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>



</div>

<div class ="mainMenu" id = "emenu">


	<!--<jsp:include page="getmenu.jsp" flush="true"/>-->

</div>
</body>

</html>





