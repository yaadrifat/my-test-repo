<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.eres.ctrp.web.CtrpDraftJBHelper"%>
<%
int result = 0;
int nonIndustryFlag = 0;
if(null != request.getAttribute("result"))
	 result = (Integer) request.getAttribute("result");
if(null != request.getAttribute("nonIndustryFlag"))
 	nonIndustryFlag = (Integer) request.getAttribute("nonIndustryFlag");
	String fileName = "";
	if (nonIndustryFlag==1) {
		fileName = CtrpDraftJBHelper.COMPLETE_BATCH_ULOAD;
	} else {
		fileName = CtrpDraftJBHelper.ABBREVIATED_BATCH_ULOAD;
	}
%>

<%@page import="com.velos.eres.service.util.LC"%>
<%@page import="com.velos.eres.service.util.MC"%><html>
<head>
<title><%=LC.L_DownloadCTRPDraft%><%--Download CTRP Draft*****--%>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="localization.jsp" flush="true" />
<jsp:include page="include.jsp" flush="true" />
<jsp:include page="ui-include.jsp" flush="true" />
</head>
<body>
<s:form action="openCSV">
	<%--Hidden Field For File Type --%>
	<input type="hidden" name="nonIndustryFlag" value="<%=nonIndustryFlag%>" />
	<input type="hidden" name="result" value="<%=result%>" />
	<table width="80%">
	<tr height="40px"></tr>
		<tr align="center">
			<%if (result==1) {%>
			<td colspan="2"><%=MC.CTRP_CSV_GENERATED%></td>
			<%} else {%>
			<td colspan="2"><%=MC.CTRP_CSV_NOT_GENERATED%></td>
			<%}%>
		</tr>
		<tr height="20px"></tr>
		<tr align="center">
			<td colspan="2">
			<%if (result==1) {	%>
			<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="submit" value="<%=LC.L_Open_CSV%>" name="<%=LC.L_Open_CSV%>"/> &nbsp;&nbsp;&nbsp;
			<%}%>
			<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="button" onclick="javascript:window.close();" value="<%=LC.L_Close%>" name="<%=LC.L_Close%>"></input>
			</td>

		</tr>
	</table>
	<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</s:form>
</body>
</html>