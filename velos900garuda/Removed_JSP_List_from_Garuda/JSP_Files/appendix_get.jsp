<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.velos.eres.service.util.*"%>
<HTML>
<HEAD>
<title><%=LC.L_Std_Appx%><%--Study Appendix*****--%></title>
</HEAD>

<jsp:useBean id="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>

<%=LC.L_GetAppx_Dets_Upper%><%--GET APPENDIX DETAILS*****--%>

<% appendixB.setId (349); %>
<% appendixB.getAppendixDetails(); %>
<%=LC.L_AppendixId%><%--appendixId*****--%> : 
<%out.print(appendixB.getId());%>
<BR>
<%=LC.L_Appendixtype%><%--appendixType*****--%> : 
<% String type=appendixB.getAppendixType();%>
<% type=type.trim();%>
<%out.println(type);%>
<BR>
<%=LC.L_Pub_Flag%><%--pub flag*****--%>: 
<%out.print(appendixB.getAppendixPubFlag());%>
<BR>
<%=LC.L_Userfilename%><%--userfilename*****--%>:
<% String s1 = appendixB.getAppendixUrl_File(); %>
<% String s2;%>

<%
if (type.equals("file")){
	s2 ="http://deepali:8080/appendixweb/servlet/Download/" + appendixB.getAppendixSavedFileName()+"";
	out.println("<a href=" + s2 + ">" + s1 + "</a>");
}
else
{
	out.println("<a href="+s1 +">"+s1+"</a>");
}
%>
<BR>
<%=LC.L_Appx_Desc%><%--appendix Description*****--%> :  
<%out.print(appendixB.getAppendixDescription());%>
<BR>
<%=LC.L_Appx_Std%><%--appendix study*****--%> :  
<%out.print(appendixB.getAppendixStudy());%>
<BR>
<%=MC.M_Appx_SvdFileName%><%--Appendix saved file name*****--%>:  
<% String s=appendixB.getAppendixSavedFileName();%>
<%out.print(s);%>
<BR>
<%=LC.L_Got_Appx%><%--GOT Appendix*****--%>

</BODY>
</HTML>
