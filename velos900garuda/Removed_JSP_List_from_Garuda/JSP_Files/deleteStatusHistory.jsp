<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<%@ page language = "java" import="com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.aithent.audittrail.reports.AuditUtils"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id="ulb" scope="request" class="com.velos.eres.web.ulink.ULinkJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="statHistoryB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");

%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))	{

	int waitTime = 0;
	int statId = EJBUtil.stringToNum(request.getParameter("statusId"));

	String moduleTable = request.getParameter("moduleTable");

    	String modulePk = request.getParameter("modulePk");

	String tab = request.getParameter("selectedTab");

	String fromjsp = request.getParameter("fromjsp");

	String portalName = request.getParameter("portalName");
	String studyId = request.getParameter("studyId");


	//Added by Manimaran for the July-August Enhancement S4.
	String userName=request.getParameter("userName");
	String from=request.getParameter("from");
	String pageRight = request.getParameter("pageRight");//KM-3040
	String verNumber = request.getParameter("verNumber");  // Amarnadh - #3110

	String invNumber = request.getParameter("invNumber");

	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>
	<FORM name="deleteStatus" id ="delStatForm" method="post" action="deleteStatusHistory.jsp" onSubmit="if (validate(document.deleteStatus)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>

	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>

		</table>
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="delStatForm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	

 	 <input type="hidden" name="delMode" value="<%=delMode%>">

 	 <input type="hidden" name="statusId" value="<%=statId%>">
	 <input type="hidden" name="srcmenu" value="<%=src%>">
	 <input type="hidden" name="moduleTable" value="<%=moduleTable%>">
	 <input type="hidden" name="modulePk" value="<%=modulePk%>">
	 <input type="hidden" name="fromjsp" value="<%=fromjsp%>">
	 <input type="hidden" name="selectedTab" value="<%=tab%>">
	 <!--Added by Manimaran for the July-August Enhancement S4.-->
	 <input type="hidden" name="userName" value="<%=userName%>">
	 <input type="hidden" name="from" value="<%=from%>">
	 <input type="hidden" name="portalName" value="<%=portalName%>">
	 <input type="hidden" name="pageRight" value="<%=pageRight%>">
	 <input type="hidden" name="verNumber" value="<%=verNumber%>"> <!-- Amarnadh # 3110 -->
	 <input type="hidden" name="studyId" value="<%=studyId%>">
	 <input type="hidden" name="invNumber" value="<%=invNumber%>">


	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {

	statHistoryB.setStatusId(statId);

	int i=0;

	// Modified for INF-18183 ::: Raviesh
	i=statHistoryB.deleteStatusHistory(AuditUtils.createArgs(session,"",LC.L_Status_History));

%><br><BR><BR><BR><BR><%
	if(i == 0) {
%>
<p class = "successfulmsg" align = center> <%=MC.M_Stat_DelSucc%><%--The status has been deleted successfully.*****--%></p>
<%
		}
		else if (i == -3){
%>
<p class = "successfulmsg" align = center> <%=MC.M_StatRec_NotBeDel%><%--This is the only status record. This status can not be deleted.*****--%> </p>
<%
		} else {
%>
<p class = "successfulmsg" align = center> <%=MC.M_Stat_CntDel%><%--The status could not be deleted.*****--%> </p>
<%
		}
	//Modified by Manimaran to fix the Bug 2973
	//JM: 06MAR2008, added studyId #FIN11
	String  targetstr = fromjsp + "?srcmenu=" + src + "&moduleTable=" + moduleTable + "&modulePk=" + modulePk+
		 "&selectedTab=" + tab + "&fromjsp=" +fromjsp+ "&studyId=" + studyId +"&userName="+userName+"&from="+from+"&portalName="+portalName+"&pageRight="+pageRight+"&verNumber="+verNumber+"&invNumber="+invNumber ;//KM-3040

	if (i < 0)
	{
		waitTime = 5;
	}
	else
	{
		waitTime = 1;
	}

	 %>
<META HTTP-EQUIV=Refresh CONTENT="<%=waitTime%>;URL=<%=targetstr%>">



	 <%


	} //end esign
	} //end of delMode
}//end of if body for session

else {
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</BODY>
</HTML>
