<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<BODY>
	<jsp:useBean id="budgetB" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
	<jsp:useBean id="budget" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
	<jsp:useBean id="bgtcalB" scope="request" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
	<jsp:useBean id="lineitemB" scope="page" class="com.velos.esch.web.lineitem.LineitemJB"/>
	<jsp:useBean id="bgtSectionB" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
	<jsp:useBean id="bgtSection" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
	<jsp:useBean id="bgtSection1" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
	<jsp:useBean id="bgtSection2" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
	<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
	<jsp:useBean id="eventassocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
	<jsp:useBean id="bgtcaldao" scope="request" class="com.velos.esch.business.common.BudgetcalDao"/>
	
  <%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.esch.business.common.*"%>
  

  
  <%
  
  	HttpSession tSession = request.getSession(true);
  	String accId= (String) tSession.getValue("accountId");
	String src = "",templateFlag="";
	src = request.getParameter("src");	
	String fromPage = request.getParameter("fromPage");
	String studyId = request.getParameter("studyId");
	String eSign = request.getParameter("eSign");
	
	String calStatus = request.getParameter("calStatus");

	SchCodeDao cd = new SchCodeDao();
    cd.getCodeValues("currency", EJBUtil.stringToNum(accId));
    
    String currencyId = "" + cd.getCodePKByDesc("currency","US Dollars","$" );
    
	ArrayList eventIds=new ArrayList() ;
	//String[] bgtcalArray = new String[100];
	
	eventassocdao= eventassocB.getStudyProts(EJBUtil.stringToNum(studyId),calStatus);

	eventIds=eventassocdao.getEvent_ids() ;
	//bgtcalArray = (String[])eventIds.toArray();
	
	int budgetId = 0;
	int budgetCalId = 0;
	int secId = 0;
	int perSecId= 0 ;
	int defSecId = 0;
	int	sectionresult=0;
	 

	if(sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
  	 	String oldESign = (String) tSession.getValue("eSign");
		if(!oldESign.equals(eSign)) {
    		%>
       	 	 <jsp:include page="incorrectesign.jsp" flush="true"/>  	
       		<%
       		} else {
       		String ipAdd = (String) tSession.getValue("ipAdd");
       		String usr = (String) tSession.getValue("userId");      
       		int ret = 0;
			
			
       		budgetId=EJBUtil.stringToNum(request.getParameter("budgetId")); 
			
			// includedIn will be passed as P when this page is included in Protocol Calendar tabs to show the default budget

			String includedIn = request.getParameter("includedIn");
			if(StringUtil.isEmpty(includedIn))
			{
			 includedIn = "";
			}

			String userIdFromSession = (String) tSession.getValue("userId");
			String StudyNo = (String) tSession.getAttribute("studyNo");
			Object[] arguments = {DateUtil.getCurrentDateTime()};
    		String budgetName= VelosResourceBundle.getLabelString("L_CombPat_Bgt",arguments);/*String budgetName= "Combined "+LC.Pat_Patient+" Budget "+ DateUtil.getCurrentDateTime();*****/
    		
    		budgetName = budgetName.trim();
    		
    		String budgetVer="1.0";

			if(budgetVer.trim().equals("")){
				budgetVer="null";
			}
			Object[] arguments1 = {StudyNo};	
    		String budgetDesc=VelosResourceBundle.getMessageString("M_AggBgt_PatStd",arguments1);/*String budgetDesc="An aggregated budget of all "+LC.Pat_Patient+" calendars for "+LC.Std_Study+" " + StudyNo;*****/
			
    		String budgetCreatedById=userIdFromSession;
    		String strbudgetTemplate=request.getParameter("budgetTemplate");
			

		strbudgetTemplate=(strbudgetTemplate==null)?"":strbudgetTemplate;
		
		StringTokenizer strTokenizer = new StringTokenizer(strbudgetTemplate,":");
		
		String selTemplateFlag ="";
		String selTemplateId="";
		if (strbudgetTemplate.length()>0) {
			selTemplateId = strTokenizer.nextToken();
			while(strTokenizer.hasMoreTokens())
		 		selTemplateFlag = strTokenizer.nextToken();
		 	}

			SchCodeDao scDao = new SchCodeDao();
	
    		String budgetStatus="";
    		
    		budgetStatus=""+scDao.getCodeId("budget_stat","W");
			budgetStatus = budgetStatus.trim();
 			System.out.println("budgetStatus" +budgetStatus+"*");			   		 	
 			String dBgtStatus ="W";
 			
			String orgId=request.getParameter("orgId");
			
			if(orgId.equals(""))
			{
			orgId=null;
			}
			
			String orgFlag=request.getParameter("orgFlag");
			
			if(orgFlag.equals("0"))
			{
			orgId=null;
			}	

			String bgtCalFlag=request.getParameter("bgtCalFlag");
    		String sAcc= (String) tSession.getValue("accountId");	
    		ipAdd = (String) tSession.getValue("ipAdd");
    		usr = (String) tSession.getValue("userId");
			
			String creator="";	
    		String mode="N";
			String msg="";
			String bgtType= "";
			int ctrlrows = 0;
	   	    String defRights = "";
			
			BudgetDao budgetDao = new BudgetDao();
			
			ret = budgetDao.checkDupBudget(EJBUtil.stringToNum(sAcc),budgetName,budgetVer, "N", budgetId);			
				
			if(ret == -2){
			%>
				
		<br><br><br><br><br><p class = "sectionHeadings" align = center>		
		<%=MC.M_BgtNameBgtVer_AldyExst%><%--The Budget Name and Budget Version combination that you have entered already exists. Please enter a new name or version.*****--%> 
		<Br><Br>
		
		
		<button onclick="window.history.back();"><%=LC.L_Back%></button>
		</p>

		
		<% return;  }
		 		
		
			if(budgetVer.trim().equals("null"))
			{
			budgetVer="";
			}
					
			//budgetB.setBudgetId(budgetId);
    		budgetB.setBudgetName(budgetName)	;
    		budgetB.setBudgetCombinedFlag("Y");
    		budgetB.setBudgetVersion(budgetVer);	
    		budgetB.setBudgetdesc(budgetDesc);	
    		budgetB.setBudgetCreator(budgetCreatedById);				
    		budgetB.setBudgetTemplate(selTemplateId);	
    		budgetB.setBudgetCurrency(currencyId);
    		budgetB.setBudgetSFlag(orgFlag);
    		budgetB.setBudgetSiteId(orgId);
    		if ( ! includedIn.equals("P")) { 
    				budgetB.setBudgetStudyId(studyId);				
    		}		
    		budgetB.setBudgetAccountId(sAcc);
			budgetB.setIpAdd(ipAdd); 
			
			budgetB.setBudgetStatus(dBgtStatus);
			budgetB.setBudgetCodeListStatus(budgetStatus);
			
//			budgetB.setBudgetCFlag(bgtCalFlag);
			

	    	if(mode.equals("N")) {
    	
		 		   budgetB.setCreator(usr);		
		    		//generate default budget rights 
					ctrl.getControlValues("bgt_rights");
					ctrlrows = ctrl.getCRows();
	
					for(int count=0;count<ctrlrows;count++)
					{ 
			        	defRights = defRights +"7";
				    }
					if(selTemplateFlag.equals("P")){
						budgetB.setBudgetType("P");
					}
					
					//Added by IA 11.3.2006 Add Comparative budget
					
					if(selTemplateFlag.equals("C")){
						budgetB.setBudgetType("C");
					}
					//End added by IA 11.3.2006
					
					if(selTemplateFlag.equals("T")){
						templateFlag="T";
						budget.setBudgetId(EJBUtil.stringToNum(selTemplateId));
	  					budget.getBudgetDetails();
						bgtType = budget.getBudgetType();
					
						if(bgtType.equals("P")){
							budgetB.setBudgetType("P");
						}
						else if(bgtType.equals("C")){
							budgetB.setBudgetType("C");
							selTemplateFlag = "C";
						}
					}
     				budgetB.setBudgetRights(defRights);
					budgetB.setBudgetRScope("S");
					 
					budgetB.setBudgetDetails();
					budgetId =  budgetB.getBudgetId();
					
					if(selTemplateFlag.equals("S") && (!templateFlag.equals("T"))) { 
				   	sectionresult= budgetDao.addStudyBudgetSections(budgetId,EJBUtil.stringToNum(usr),ipAdd);
					}			   

		 
				   //in case of patient budget add dummy protocol, default section,lineitem
				   //Add the or clause in the case of comparative 
				 				
				//removed the check for templateFlag (!templateFlag.equals("T"), to consider study-seup calendars,
				//when a custom template is selected as a template for combined budget 
				
				 if (budgetId > 0)
				 {  
					if(templateFlag.equals("T"))
					{        
						int retu = budgetB.copyBudgetForTemplate(EJBUtil.stringToNum(selTemplateId),budgetId,EJBUtil.stringToNum(usr),ipAdd);
					}else {
						 bgtcalB.setBudgetId(String.valueOf(budgetId));
       				     bgtcalB.setCreator(usr);		
             			 bgtcalB.setIpAdd(ipAdd); 
						 bgtcalB.setBudgetDelFlag("N");
						 bgtcalB.setBudgetExcldSOCFlag("1");
						 bgtcalB.setBudgetProtType("S");
						 
						 bgtcalB.setBudgetcalDetails();
						 budgetCalId = bgtcalB.getBudgetcalId(); //get PK
						 
						 if (budgetCalId > 0)
						 {
							//add Miscellaneous section
							     	
						 	bgtSectionB.setBgtCal(String.valueOf(budgetCalId));
							bgtSectionB.setBgtSectionName("Miscellaneous");
							bgtSectionB.setBgtSectionVisit("Miscellaneous");
							bgtSectionB.setBgtSectionSequence("20");
							bgtSectionB.setBgtSectionDelFlag("N");
							bgtSectionB.setBgtSectionType("P");			
															
							bgtSectionB.setBgtSectionPersonlFlag("0");
						 	bgtSectionB.setCreator(usr);
							bgtSectionB.setIpAdd(ipAdd);
							bgtSectionB.setBgtSectionDetails();   
							
							secId = bgtSectionB.getBgtSectionId();
							
							if (secId > 0)
							{
							  lineitemB.setLineitemName("Misc");
				   			  lineitemB.setLineitemBgtSection(String.valueOf(secId));							
							  lineitemB.setCreator(usr);
							  lineitemB.setIpAdd(ipAdd);
							  lineitemB.setLineitemDelFlag("N");
							  lineitemB.setLineitemDetails();
							}

							//add personnel cost section
							bgtSection.setBgtCal(String.valueOf(budgetCalId));
							bgtSection.setBgtSectionName("Personnel Cost");						
							bgtSection.setBgtSectionSequence("10");				
							bgtSection.setBgtSectionType("P");

							bgtSection.setBgtSectionPersonlFlag("1");

						 	bgtSection.setCreator(usr);
							bgtSection.setIpAdd(ipAdd);
							bgtSection.setBgtSectionDetails();   
							
							perSecId = bgtSection.getBgtSectionId();

							// Create default section (to be used in personnel section cost), not displayed
							//as delFlag is P
							bgtSection1.setBgtCal(String.valueOf(budgetCalId));
							bgtSection1.setBgtSectionName("Default section");						
							bgtSection1.setBgtSectionDelFlag("30");
							bgtSection1.setBgtSectionDelFlag("P");
							bgtSection1.setBgtSectionPersonlFlag("0");
						
						 	bgtSection1.setCreator(usr);
							bgtSection1.setIpAdd(ipAdd);
							bgtSection1.setBgtSectionDetails();   
							
							defSecId = bgtSection1.getBgtSectionId();

							// Create a repeat default section( to be used in 'Edit repeat line Items 
							bgtSection2.setBgtCal(String.valueOf(budgetCalId));
							bgtSection2.setBgtSectionName("Repeat Default section");						
							bgtSection2.setBgtSectionDelFlag("40");
							bgtSection2.setBgtSectionDelFlag("R");
							bgtSection2.setBgtSectionPersonlFlag("0");
						
						 	bgtSection2.setCreator(usr);
							bgtSection2.setIpAdd(ipAdd);
							bgtSection2.setBgtSectionDetails();   
							
							defSecId = bgtSection2.getBgtSectionId();
					  	}
					 }
					 String id =""; 
						 
					  for(int i=0;i<eventIds.size();i++)
					  {
						   id = ""+((Integer) eventIds.get(i)).intValue();
						   
							bgtcalB.setBudgetProtId(id);	
							bgtcalB.setBudgetId(String.valueOf(budgetId));	
							bgtcalB.setCreator(usr);		
	             			bgtcalB.setIpAdd(ipAdd); 
							bgtcalB.setBudgetDelFlag("N");
							bgtcalB.setBudgetExcldSOCFlag("1");
							bgtcalB.setBudgetProtType("S");
							bgtcalB.setBudgetcalDetails();	

							int budgetProtId=EJBUtil.stringToNum(bgtcalB.getBudgetProtId());					
							int creatorTest=EJBUtil.stringToNum(bgtcalB.getCreator());
							int output=bgtcaldao.addBudgetSection(budgetProtId,bgtcalB.getBudgetcalId(),bgtcalB.getBudgetProtType(),creatorTest,bgtcalB.getIpAdd(),"CB");
							
							tSession.setAttribute("lineItemDeleted", "Y");  
							
						}
						 
					}	
			   
			} else{
				//mode !=N
		    	budgetB.setModifiedBy(usr);
				budgetB.setIpAdd(ipAdd);
				ret=budgetB.updateBudget();
			}

			if( (ret < 0)|| sectionresult==-1) {
			msg = MC.M_Bgt_DetsNotSvd;/*msg = "Budget details not saved";*****/
		    }else {
   				msg = MC.M_Data_SvdSucc;/*msg = "Data was saved successfully";*****/			
		   
			 	%><META HTTP-EQUIV=Refresh CONTENT="1; URL=combinedBudget.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=11&studyId=<%=studyId%>&budgetId=<%=budgetId%>&budgetTemplate=<%=selTemplateFlag%>"> 			
				<br>
				<br>
				<br>
				<br>
				<br>
				<p class = "sectionHeadings" align = center> <%=msg%> </p>
			<%}
		} // end of esign
		}//end of if body for session
	else
	{
	%>
	<jsp:include page="timeout.html" flush="true"/>
	<%
	}	
	%>
</BODY>
</HTML>
