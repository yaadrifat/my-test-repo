<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head>
<title><%=LC.L_Alert_Notification%><%--Alert Notification*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT language="JavaScript1.1">

function setParam(str,formobj){
	        
			formobj.subVal.value=str;
		if (!(validate(formobj))) {
			 return false  }
			formobj.submit();
	}
function setVals(str,formobj)
{

	formobj.action = formobj.action + "?addFlag=" + str; 
	return true;	
}

function openwin1(formobj){
var names = formobj.alertNotifyToNames.value;
var ids = formobj.alertNotifyToId.value;
var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=notification&mode=initial&ids=" + ids + "&names=" + names ;
window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
}



function  validate(formobj)
{      

     if (!(validate_col('Event-Status',formobj.eventstatus))) {
     	 formobj.subVal.value="Submit";     	  
     	  return false }

     if (!(validate_col('e-Signature',formobj.eSign))){ 
     	 formobj.subVal.value="Submit";
     	 return false
     	  }
     	 

	if(isNaN(formobj.eSign.value) == true) 
	{
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	formobj.subVal.value="Submit";
	return false;
       }
       return setVals(formobj.subVal.value,formobj);	
	return true ;
}

</SCRIPT>
</head>

<% String src="";
src= request.getParameter("srcmenu");
%>	
 

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="alertNotifyB" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />


<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DateUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.business.common.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>

<body>

<br>

<DIV class="browserDefault" id="div1"> 

  <%
   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
   {


	String enrollId =(String) tSession.getValue("enrollId");
	int pageRight  = 0;

	ArrayList eventNames = null;
	String eventName="";

	String globalFlag="";
	String patientId = "";
	String dob = "";
	String gender = "";
	String genderId = "";
	String yob = "";
	int age = 0;
	String alertNotifyToName="";
	String adverseToId="";
	String alertNotifyToId = "";
	String str = "";
	Calendar cal1 = new GregorianCalendar();
	String alertNotifyId="";
	String mode = request.getParameter("mode");
	globalFlag = request.getParameter("globalFlag");
	
	String studyId = request.getParameter("studyId");
	String protocolId = request.getParameter("protocolId");	
	String from = request.getParameter("from");
    String selectedTab = request.getParameter("selectedTab");

	%>
		
	<jsp:include page="studytabs.jsp" flush="true"> 
		<jsp:param name="from" value="<%=from%>"/> 
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
	<%
	stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 

	if ((stdRights.getFtrRights().size()) == 0){
	 	pageRight= 0;
		}else{
		pageRight = 	Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
	}
	if (pageRight >= 4 )
	  {


	EventAssocDao eventAssoc = new EventAssocDao();
	eventAssoc = eventAssocB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId),"","");//KM-041808

	eventNames = eventAssoc.getNames();
	

	SchCodeDao cd = new SchCodeDao();
	String dCur = "";
	cd.getCodeValues("eventstatus",0); 
	dCur =  cd.toPullDown("eventstatus");
	
	

	String protName = "";
	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	} else {
		protocolId = "";
	}	
	

if(mode.equals("M"))
{

// To open the notification in the modified mode.
	alertNotifyId = request.getParameter("alertNotifyId");	
	alertNotifyB.setAlNotId(EJBUtil.stringToNum(alertNotifyId));
	alertNotifyB.getAlertNotifyDetails();	
	
	alertNotifyToId = alertNotifyB.getAlertNotifyAlNotUsers();
	alertNotifyToId = (alertNotifyToId==null)?"":alertNotifyToId;

	
	UserDao userDao = userB.getUsersDetails(alertNotifyToId);
	ArrayList fname = userDao.getUsrFirstNames();
	ArrayList lname = userDao.getUsrLastNames();
	
	for(int i=0;i<fname.size();i++)
	{
		

	alertNotifyToName = alertNotifyToName + ((String)fname.get(i)).trim() + " " + ((String)lname.get(i)).trim()  + ";";
	
	}
	if(alertNotifyToName.length() > 0) 
	{
	alertNotifyToName = alertNotifyToName.substring(0,alertNotifyToName.length()-1);
	}
	
// To open the notification in the modified mode.	
	//alertNotifyToName=alertNotifyB.getAlertNotifyAlNotUsers();
	eventName=alertNotifyB.getAlertNotifyAlNotMobEve();

	
	dCur = alertNotifyB.getAlertNotifyCodelstAnId();
	dCur =  cd.toPullDown("eventstatus",EJBUtil.stringToNum(dCur));
	
} 

%>

	
<table width=100%>
 <tr>
 	<td width="20%"  class=tdDefault><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%>:</td><td class=tdDefault width="80%"><%=protName%></td>
 </tr>
</table>

<Form name="adverse" id="advfrmid" method=post action="updatestudyeventnot.jsp" onsubmit="if (validate(document.adverse)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<p class = "sectionHeadings" ><%=LC.L_Notification%><%--Notification*****--%></p>

<table width="700" cellspacing="0" cellpadding="0" >
  <tr> 
    <td class=tdDefault ><%=LC.L_Send_MsgTo%><%--Send Message To*****--%>:</td>
	<td width='65%'>

   <input type=text name='alertNotifyToNames' maxlength=100 size=20 readonly  value="<%=alertNotifyToName%>" >
   <A HREF=# onClick='openwin1(document.adverse)'>  <%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%> 
   </A>
   </td>         
  </tr>
  <input type=hidden name=alertNotifyToId	 value='<%=alertNotifyToId%>'>



  <tr>		
    <td class=tdDefault ><%=MC.M_WhenEvtStatus_ChgTo%><%--When Event Status Changed To*****--%>:
        <FONT class="Mandatory">* </FONT>
    </td>
    <td><%=dCur%> </td> 
  </tr>
  <tr>		
    <td class=tdDefault ><%=LC.L_For_Evt%><%--For Event*****--%>: 

    </td>
     
<td>
<%
	int length=eventNames.size();
	str = "<select name=eventName>";
	str  = str + "<option value = 'All'> "+LC.L_All/*All*****/+"</option>";
for(int i=0 ;i<length; i++)
{		
	if(eventNames.get(i).equals(eventName))
	{
		str = str + "<option selected value='" + eventNames.get(i) + "'>" +  eventNames.get(i) + "</option>";
	}
	else
	{
		str = str + "<option value='" + eventNames.get(i) + "'>" +  eventNames.get(i) + "</option>";
	}
}
	str = str + "</select>";	
	
//	else
//	{
//	str = "<select name=eventName>";
//	str = str + "<option value='" + eventName + "'>" +  eventName + "</option>";
//	str = str + "</select>";	
//	}

%>


<%=str%>
</td>
    
  </tr>
</table>

<%	   


  

if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) 
{%>
<br>
<TABLE width="100%" cellspacing="0" cellpadding="0" border="0" >
<!--  align="left"  > -->
<tr align="left"  >
<td width="62%" align="right">
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="advfrmid"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="noBR" value="Y"/>
</jsp:include>
</td> 
<td width="38%" align="left" valign="top">
<!--  <table width="100%" border="0" cellspacing="0" cellpadding="0" > -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" ">
	<tr  align="left"> <td align="left">
        <input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" onClick="return setParam('Add',document.adverse);" name="<%=LC.L_Submit_AddAnother%>" value="<%=LC.L_Submit_AddAnother%>"/>		
	</td>	
	</tr>
</table>
</td>
</tr>
</table>
<%
}	
%>		

<input type="hidden" name=mode value=<%=mode%>>
<input type="hidden" name=alNotId value=<%=alertNotifyId%>>
<input type="hidden" name=alertNotifyToName value="<%=alertNotifyToName%>">
<input type="hidden"name=addFlag>
<input type="hidden" name=studyId value=<%=studyId%>>
<input type="hidden" name=protocolId value=<%=protocolId%>>
<input type="hidden" name=globalFlag value=<%=globalFlag%>>

<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
<input type="hidden" name="from" value="<%=from%>">
<input type="hidden" name="srcmenu" value="<%=src%>">


<%//follwing column added to fix the system crash in netscape 4.79. used in the javascript%>
   <input type="hidden" name="subVal" value="Sumbit">
</Form>


<%
	} //end of if for pageright
} //end of if session times out
else
{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<div class ="mainMenu" id = "emenu">

<!--<jsp:include page="getmenu.jsp" flush="true"/>-->   

</div>

</body>
</html>
