<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<html>
<HEAD>
<title><%=LC.L_Del_Crf%><%--Delete CRF*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<%
String fromPage = request.getParameter("fromPage");
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{ %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
 <% } //end for if ((request.getParameter("fromPage")).equals("selectEvent"))
 %>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crflibB" scope="request" class="com.velos.esch.web.crflib.CrflibJB"/>
<jsp:useBean id="linkedFormsB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<%@ page language = "java" import = "com.velos.esch.service.util.EJBUtil,com.velos.eres.business.linkedForms.impl.LinkedFormsBean,com.velos.esch.business.common.EventdefDao"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %><%@page import="com.velos.eres.service.util.*"%>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>




<% String src;
	src= request.getParameter("srcmenu");
%>


<%
//KM
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{

%>
<jsp:include page="include.jsp" flush="true"/>

<%

} else {%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<%}%>

<body>
<br>

<%
if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt"))){%>
 <DIV class="formDefaultpopup" id="div1">
<%	}
else { %>
 <DIV class="formDefault" id="div1">
<%}%>


<%
String duration = request.getParameter("duration");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String eventId = request.getParameter("eventId");
String crfmode= request.getParameter("crfmode");
String mode = request.getParameter("mode");
String calStatus = request.getParameter("calStatus");
String eventmode = request.getParameter("eventmode");
String displayDur=request.getParameter("displayDur");
String displayType=request.getParameter("displayType");
String eSign = request.getParameter("eSign");
String delMode=request.getParameter("delMode");
String crfId=request.getParameter("crfId");
//String eventName = request.getParameter("eventName");
String eventName = "";
String propagateInVisitFlag = request.getParameter("propagateInVisitFlag");
String propagateInEventFlag = request.getParameter("propagateInEventFlag");



String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;
int dataCnt = 0;


if (calledFrom.equals("P")||calledFrom.equals("L"))
{
	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventdefB.getEventdefDetails();
	  eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
}else{
	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
	  eventassocB.getEventAssocDetails();
	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
  	  eventName = (eventName==null)?"":eventName;
  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
  		eventName=eventName.substring(0,1000);
  	  }
 }



if (delMode.equals("initial" ) ){
String crfFormFlag=request.getParameter("crfFormFlag");
String lfDataCnt ="";

//int lfId=0;
LinkedFormsBean lnkFormSk = new LinkedFormsBean();

if(crfFormFlag.equals("1"))
	   {
		lnkFormSk = linkedFormsB.findByCrfEventId(EJBUtil.stringToNum(crfId),EJBUtil.stringToNum(eventId));
		lfDataCnt = lnkFormSk.getLfDataCnt();
		dataCnt = EJBUtil.stringToNum(lfDataCnt);
	   }

}
HttpSession tSession = request.getSession(true); %>
<jsp:include page="eventtabs.jsp" flush="true">
  <jsp:param name="duration" value="<%=duration%>"/>
  <jsp:param name="protocolId" value="<%=protocolId%>"/>
  <jsp:param name="calledFrom" value="<%=calledFrom%>"/>
  <jsp:param name="fromPage" value="<%=fromPage%>"/>
  <jsp:param name="mode" value="<%=mode%>"/>
  <jsp:param name="calStatus" value="<%=calStatus%>"/>
  <jsp:param name="displayDur" value="<%=displayDur%>"/>
  <jsp:param name="displayType" value="<%=displayType%>"/>
  <jsp:param name="eventId" value="<%=eventId%>"/>
  <jsp:param name="src" value="<%=src%>"/>
  <jsp:param name="selectedTab" value="6"/>
   <jsp:param name="calassoc" value="<%=calAssoc%>"/>
  </jsp:include>

<%
if (sessionmaint.isValidSession(tSession)) {
	String oldESign = (String) tSession.getValue("eSign");
/////////////////

	if(!oldESign.equals(eSign)&& delMode.equals("final")) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	}else {



	/////////////////////////
	if(dataCnt == 0)
		{
		if (delMode.equals("initial" )) {

			delMode="final";
		%>

		<FORM name="deleteFile" id="delcrffrm" method="post" action="deletecrf.jsp" onSubmit="if (validate(document.deleteFile)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		<br><br>


		<jsp:include page="propagateEventUpdate.jsp" flush="true">

		<jsp:param name="fromPage" value="<%=fromPage%>"/>

		<jsp:param name="formName" value="deleteFile"/>

		<jsp:param name="eventName" value="<%=eventName%>"/>

		</jsp:include>

		<P class="defComments"><%=MC.M_EsignToProc_WithCrfDel%><%--Please enter e-Signature to proceed with Crf Delete.*****--%></P>
		<jsp:include page="submitBar.jsp" flush="true">
				<jsp:param name="displayESign" value="Y"/>
				<jsp:param name="formID" value="delcrffrm"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>

		 <input type="hidden" name="delMode" value="<%=delMode%>">
		 <input type="hidden" name="srcmenu" value="<%=src%>">
		 <input type="hidden" name="duration" value="<%=duration%>">
		 <input type="hidden" name="protocolId" value="<%=protocolId%>">
		 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
		 <input type="hidden" name="eventId" value="<%=eventId%>">
		 <input type="hidden" name="crfmode" value="<%=crfmode%>">
		 <input type="hidden" name="mode" value="<%=mode%>">
		 <input type="hidden" name="fromPage" value="<%=fromPage%>">
		 <input type="hidden" name="calStatus" value="<%=calStatus%>">
		 <input type="hidden" name="eventmode" value="<%=eventmode%>">
		 <input type="hidden" name="displayDur" value="<%=displayDur%>">
		 <input type="hidden" name="displayType" value="<%=displayType%>">
		 <input type="hidden" name="crfId" value="<%=crfId%>">
		 <input type="hidden" name="eventName" value="<%=eventName%>">
 		 <input type="hidden" name="calassoc" value="<%=calAssoc%>">

		</FORM>
	<%
		} else {


		if ((propagateInVisitFlag == null) || (propagateInVisitFlag.equals("")))

				propagateInVisitFlag = "N";

			if ((propagateInEventFlag == null) || (propagateInEventFlag.equals("")))

			propagateInEventFlag = "N";

			 if ( (propagateInVisitFlag.equals("Y")) || (propagateInEventFlag.equals("Y")))
				 {

				EventdefDao eventdefdao = new EventdefDao();

				System.out.println("propagate CRf delete" + EJBUtil.stringToNum(crfId));

				eventdefdao.propagateEventUpdates(EJBUtil.stringToNum(protocolId), EJBUtil.stringToNum(eventId), "EVENT_CRF", EJBUtil.stringToNum(crfId), propagateInVisitFlag, propagateInEventFlag, "D", calledFrom);


				 }else
				 {
					//Modified for INF-18183 ::: Akshi
					 crflibB.removeCrflib(EJBUtil.stringToNum(crfId),AuditUtils.createArgs(session,"",LC.L_Evt_Lib));
					
				}

		%>

		<br><br><br><br><br>
		<p class = "successfulmsg" align = center><%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%></p>
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=crflibbrowser.jsp?selectedTab=6&srcmenu=<%=src%>&duration=<%=duration%>&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&eventmode=<%=eventmode%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>">

		<%


		} }//end of else if we have correct esign and want to delete
	else{%>	<br><br>
<p class = "successfulmsg" align = center><%=MC.M_FrmAns_CntRem%><%--Form has been answered and cannot be removed.*****--%></p>
<A type="submit" href="crflibbrowser.jsp?eventmode=M&mode=M&duration=<%=duration%>&srcmenu=<%=src%>&selectedTab=6&eventId=<%=eventId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>"><%=LC.L_Back%></A>

	<%}
			}

////////////////
		}//end of if body for session





else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

if ((fromPage.equals("selectEvent")) || (fromPage.equals("fetchProt")))
{
%>

 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
<%

} else {%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
<%}%>

</BODY>
</HTML>



