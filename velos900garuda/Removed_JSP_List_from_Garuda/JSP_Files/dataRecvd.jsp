<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Grid_Monitor%><%--Grid Monitor*****--%></title>

<script>

function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;


	 if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

if (orderBy==1) orderBy="lower(study_number)";
	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	searchCriteria=formObj.searchCriteria1.value;
	formObj.action="dataRecvd.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&searchCriteria="+searchCriteria;
	formObj.submit();
}


</script>

<%
String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.CtrlDao,java.util.* "%>
<br>

<div class="browserDefault" id="div1">
<P class="sectionHeadings"> <%=MC.M_GridMntr_AggInfo%><%--Grid Monitor >> Aggregation Information*****--%> </P>

<P class="defComments"><%=MC.M_TableShowRec_RecMod%><%--The following table shows the number of records received in last import for each study which is setup for aggregation. The record count includes main as well as sub records in respective modules.*****--%> </P>
<%
String pagenum = "";
int curPage = 0;
long startPage = 1;
String stPage;
long cntr = 0;

pagenum = request.getParameter("page");

if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");


String orderType = "";
orderType = request.getParameter("orderType");


if (orderType == null)
{
	orderType = "asc";
}

String searchCriteria = request.getParameter("searchCriteria");
if (searchCriteria==null) {searchCriteria="";}

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
	//get milestone module account right
	String modRight = (String) tSession.getValue("modRight");
	String accountId = (String) tSession.getValue("accountId");

	int modlen = modRight.length();
	CtrlDao acmod = new CtrlDao();
	acmod.getControlValues("module");
	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();
	//int a2aSeq = acmodfeature.indexOf("A2A");
	//a2aSeq	= ((Integer) acmodftrSeq.get(a2aSeq)).intValue();
	//char A2Aright = modRight.charAt(a2aSeq - 1);

	String userId = (String) tSession.getValue("userId");
	int pageRight = 7;
 char A2Aright = '1';

    	if (String.valueOf(A2Aright).compareTo("1") == 0 )	{

		//////////////////end by salil



	   StringBuffer sql= new StringBuffer();

	    sql.append("SELECT (SELECT study_number FROM ER_STUDY WHERE pk_study = r.fk_study) study_number, r.fk_study, r.rec_module, r.site_code, COUNT(*) data_count,TO_CHAR(MAX(rec_recvd_on),PKG_DATEUTIL.F_GET_DATEFORMAT||' hh24:mi:ss') rec_recvd_on ");
		  sql.append(" FROM ER_REV_RECVD r , ");
		 sql.append("(SELECT fk_study, rec_module, site_code, MAX(imp_id) imp_id FROM ER_REV_RECVD ");
		 sql.append(" WHERE fk_account = "+ accountId +" AND (fk_study IS NOT NULL OR rec_module = 'pat_nostudy')");
		 sql.append(" GROUP BY   fk_study,site_code , rec_module ) i ");
		 sql.append(" WHERE fk_account = "+ accountId+" AND i.imp_id = r.imp_id AND (r.fk_study IS NOT NULL OR r.rec_module = 'pat_nostudy') ");
		  sql.append(" GROUP BY   r.fk_study,r.site_code ,r.rec_module " );
		  sql.append(" ORDER BY  lower(study_number), r.site_code ,r.rec_module" );



	   String countSql = "select count( fk_study) from (" + sql.toString() +" ) " ;


	   long rowsPerPage=0;
   	   long totalPages=0;
	   long rowsReturned = 0;
	   long showPages = 0;

	   String studyNumber = null;
	   int studyId=0;
	   int oldStudyId = 0;
	   String module = "";
	   String lastRecvdOn;
	   String nextDue = "";
	   String siteCode ="";

	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;
	   long totalRows = 0;
	   String dataCount = "";

	   rowsPerPage =  50;
	   totalPages =Configuration.PAGEPERBROWSER ;

       BrowserRows br = new BrowserRows();
	   if ((orderBy==null ) ||orderBy.equals("null") ) orderBy = "lower(study_number)";

	   br.getPageRows(curPage,rowsPerPage,sql.toString(),totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();

%>

<Form name="dataRecvd" method=post onsubmit="" action="dataRecvd.jsp?srcmenu=<%=src%>&page=1">


	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	<Input type="hidden" name="searchCriteria1" value="<%=searchCriteria%>">


    <table width="100%" >
      <tr>
        <th width="20%" ><%=LC.L_Study_Number%><%--Study Number*****--%></th>
        <th width="15%" ><%=MC.M_SiteCodeData_RecvdFrom%><%--Site Code (data received from)*****--%> </th>
        <th width="20%"><%=LC.L_Module%><%--Module*****--%> </th>
        <th width="15%" ><%=LC.L_Last_ReceivedOn%><%--Last Received On*****--%></th>
        <th width="15%" ><%=MC.M_Rec_LastReceived%><%--Number of records Last Received*****--%></th>

      </tr>

      <%
    for(int counter = 1;counter<=rowsReturned;counter++)
	{
		oldStudyId = studyId;

		studyId=EJBUtil.stringToNum(br.getBValues(counter,"fk_study")) ;
		studyNumber=br.getBValues(counter,"study_number");
		module=br.getBValues(counter,"rec_module");
		lastRecvdOn =br.getBValues(counter,"rec_recvd_on");
		 dataCount =br.getBValues(counter,"data_count");
		siteCode =br.getBValues(counter,"site_code");

		if (StringUtil.isEmpty(studyNumber))
		{
			studyNumber = MC.M_DataNtLkd_Std/*"Data not linked with a Study"*****/;
		}

		if (StringUtil.isEmpty(module))
		{
			module = "";

		}
		if (module.equals("pat_nostudy"))
		{
			module = LC.L_LabsNotAssoc_Std/*"Labs not associated with a Study"*****/;
		}

		if (module.equals("pat_enr"))
		{
			module = LC.L_Patient_EnrlOrStat/*LC.Pat_Patient+" Enrollment/Status"*****/;
		}

		if (module.equals("pat_labs"))
		{
			module = LC.L_Patient_Labs/*LC.Pat_Patient+" Labs"*****/;
		}

		if (module.equals("pat_adverse"))
		{
			module =LC.L_Patient_AdvEvt/*LC.Pat_Patient+"Adverse Events"*****/;
		}

		if (module.equals("pat_filledforms"))
		{
			module = LC.L_Patient_Forms/*LC.Pat_Patient+" Forms"*****/;
		}


		if (oldStudyId != studyId )
		{
			%>
			    <tr><td> <b><%=studyNumber%></b>  </td></tr>
			<%
		}


		if ((counter%2)==0) {
  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}

  %>
        <td> &nbsp;</td>
        <td> <%=siteCode%> </td>
        <td> <%=module%> </td>
        <td> <%=lastRecvdOn%> </td>

         <td> <%=dataCount%> </td>


      </tr>
      <%

	}
%>
    </table>

	<table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
	<%}%>
	</td>
	</tr>
	</table>


	<table align=center>
	<tr>
<%
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
  	<A href="dataRecvd.jsp?srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="dataRecvd.jsp?srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="dataRecvd.jsp?<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}
	%>
   </tr>
  </table>

  </Form>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu">
  <!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</DIV>

</body>
</html>

