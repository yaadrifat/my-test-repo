<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_EvtStatus%><%--Delete Event Status*****--%></title>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>
function  validate(formobj){
	//INF-22237a 14-Mar-2012 @Ankit
	var fdaRegulated = document.getElementById("FDARegulated").value;
	var reasonDel = document.getElementById("reason_del").value;
	reasonDel = reasonDel.replace(/^\s+|\s+$/g, "");
	document.getElementById("reason_del").value = reasonDel;
	if(fdaRegulated=="1" && reasonDel.length<=0)
	{
		alert("<%=MC.M_Etr_MandantoryFlds%>");
		document.getElementById("reason_del").focus();
		return false;
			
	}
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id="EvtAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />


<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%
String eventStatId= "";


HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))	{

	eventStatId = request.getParameter("eventStatId");

	String eventId = request.getParameter("eventId");
	String uName = (String) tSession.getValue("userName");
	String studyNum = StringUtil.decodeString(request.getParameter("studyNum"));
	String pkey = request.getParameter("pkey");
	Calendar cal1 = new GregorianCalendar();

	//KM-14Sep09
	String org_id = request.getParameter("org_id");
	String calledFrom = request.getParameter("calledFrom");
	
	
	/*String eventName = request.getParameter("eventName");
	if(eventName!=null){
	eventName=eventName.replace('~',' ');
	eventName=StringUtil.encodeString(eventName);
	}*/

	String visit = request.getParameter("visit");
	String visitName=request.getParameter("visitName");

	visitName=(visitName==null)?"":visitName;
	visitName =  StringUtil.decodeString(visitName)	;

		String patProtId = request.getParameter("availableSch");


	String study = request.getParameter("studyId");
	if (study==null) study="";

	String calId = request.getParameter("calId");
	if (calId==null) calId="";



	String calledFromPage = request.getParameter("calledFromPage");
	if (calledFromPage==null) calledFromPage="";

	String nextpage = request.getParameter("nextpage");
	if (StringUtil.isEmpty(nextpage)){
		nextpage = "1";
	}



	int ret=0;
	int ret_on_update=0;

	//INF-22237a 14-Mar-2012 @Ankit
	String fdaRegulated="";
	study = study==null?"0":study;
	studyB.setId(StringUtil.stringToNum(study));
   	studyB.getStudyDetails();
   	fdaRegulated=studyB.getFdaRegulatedStudy();
	fdaRegulated = fdaRegulated==null?"0":fdaRegulated;
	
	String delMode=request.getParameter("delMode");

	if (delMode == null) {
		delMode="final";
%>
	
	<FORM name="evtStatDelete" id="evtStatDel" method="post" action="deleventstatus.jsp" onSubmit="if (validate(document.evtStatDelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
	<td align="right" valign="top" width="50%"><%=LC.L_Reason_ForDelete %> 
	<%if(fdaRegulated.equals("1")) {%>
	<font class="Mandatory">*</font>
	<%} %>&nbsp;
	</td>
	<Input type="hidden" name="FDARegulated" id="FDARegulated" value="<%=fdaRegulated %>">
	<td><textarea maxlength="4000" name="reason_del" id="reason_del"></textarea>
	<br><font class="Mandatory"><%=MC.M_Limit4000_Char %></font>
	</td>
	</tr>
	</table>
	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>

	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="evtStatDel"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
	

	<input type="hidden" name="delMode" value="<%=delMode%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="eventStatId" value="<%=eventStatId%>">

	<input type="hidden" name="pkey" value="<%=pkey%>">
	<input type="hidden" name="visit" value="<%=visit%>">
	<input type="hidden" name="visitName" value="<%=visitName%>">
	<input type="hidden" name="eventId" value="<%=eventId%>">
	<input type="hidden" name="studyNum" value="<%=studyNum%>">
	<input type="hidden" name="availableSch" value="<%=patProtId%>">


     <input type="hidden" name="studyId" value='<%=study%>'>
     <input type="hidden" name="calId" value='<%=calId%>'>
     <input type="hidden" name="calledFromPage" value='<%=calledFromPage%>'>
     <input type="hidden" name="nextpage" value='<%=nextpage%>'>
	<input type="hidden" name="org_id" value='<%=org_id%>'>
	 <input type="hidden" name="calledFrom" value='<%=calledFrom%>'>





	</FORM>
<%
	} else {%>
   <%

			String eSign = request.getParameter("eSign");
            eventStatId= request.getParameter("eventStatId");
            src=	request.getParameter("srcmenu");
            delMode=null;
			String oldESign = (String) tSession.getValue("eSign");

			if(!oldESign.equals(eSign)) {
%>
	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br> 	<br> <br>

 <table width=100%>



<tr>

<td align=center>

<p class = "successfulmsg">

<%=MC.M_EtrWrongEsign_ClkBack%><%--You have entered a wrong e-Signature. Please click on Back button to enter again.*****--%>

</p>

</td>

</tr>


</table>
   <table>	<tr><td width="85%"></td><td>&nbsp;&nbsp;
<button tabindex=2 onClick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table>






<%
			} else {

			//delete the status from the history table
			
			//Modified for INF-18183 ::: Akshi      // Ankit  25-Oct-2011 Bug#7393 and Bug#7259
			String moduleName = (calledFromPage.equalsIgnoreCase("studyadmincal"))? LC.L_Study : LC.L_Manage_Pats;      //Akshi 4-Nov Bug#7554
			String reason_del = request.getParameter("reason_del"); //INF-22237a 14-Mar-2012 @Ankit
			ret = EvtAssocB.deleteEventStatusHistory(EJBUtil.stringToNum(eventStatId),AuditUtils.createArgs(session,reason_del,moduleName));
			//once status is deleted from the history table it has to be updated in the sch_events1 table too.
			//and also for the latest among remaining statuses, update the end date with the null value
			
			if (ret >0){
			 EvtAssocB.updateCurrentStatus(EJBUtil.stringToNum(eventId));
			}



			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center>  <%=MC.M_DataCnt_DelSucc%><%--Data could not be deleted successfully.*****--%> </p>
			<%}else {
				//KM-14Sep09
				%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=eventstatushistory.jsp?srcmenu=<%=src%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&visit=<%=visit%>&visitName=<%=StringUtil.encodeString(visitName)%>&eventId=<%=eventId%>&studyNum=<%=StringUtil.encodeString(studyNum)%>&studyId=<%=study%>&calId=<%=calId%>&calledFromPage=<%=calledFromPage%>&nextpage=<%=nextpage%>&org_id=<%=org_id%>&calledFrom=<%=calledFrom%>">



		<%

			}


			} //end esign



	} //end of delMode

  }//end of if body for session
else { %>
 <jsp:include page="timeout.html" flush="true"/>
 <% } %> <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</DIV>


<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>

</body>
</HTML>


