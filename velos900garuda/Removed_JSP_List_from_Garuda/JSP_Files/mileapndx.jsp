<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Milestone_Appendix %><%-- Milestone >> Appendix*****--%> </title>
 
<%@ page import = "com.velos.eres.service.util.StringUtil"%>
<SCRIPT language="javascript">

function confirmBox(fileName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [fileName];
		if (confirm(getLocalizedMessageString("L_Del_FrmMstone",paramArray))) {/*if (confirm("Delete " + fileName + " from milestone?")) {*****/
		    return true;}
		else
		{
			return false;
		}
	} else {
		return false;
	}
}


function fdownload(formobj,pk,filename,dnldurl)
{
	formobj.file.value = filename;
	formobj.pkValue.value = pk ;
	formobj.dnldurl.value=dnldurl;
	formobj.action="postFileDownload.jsp";
	//formobj.action=dnldurl;
	
	formobj.target = "_filedownload";
	formobj.method = "POST";
	formobj.submit();
}

</SCRIPT> 
 
</head>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.aithent.file.uploadDownload.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.mileApndx.MileApndxJB,com.velos.eres.service.util.LC, com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="mileApndx" scope="page" class="com.velos.eres.web.mileApndx.MileApndxJB"/>

<% String src;
src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab"); 
String budgetType = "P";
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<body>
<!-- Bug NO: 3976 fixed by PK -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"> </div> 
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
	
<DIV class="BrowserTopn" id="div1">


<% String studyId=request.getParameter("studyId"); %> 

  <jsp:include page="milestonetabs.jsp" flush="true">
	<jsp:param name="studyId" value="<%=studyId%>"/>
  </jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_M_2" id="div2">

<%
//	int pageRight = 0;
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))	
	{
	
  String pageRight= "";	 
  int pgRight = 0;
 
 
	pageRight = (String) tSession.getValue("mileRight");	
  pgRight = EJBUtil.stringToNum(pageRight);
  
  if (pgRight > 0 ){

			String mode = request.getParameter("mode");
			//String selectedTab=request.getParameter("selectedTab");
			String space = "1111";//request.getParameter("outOfSpace");
			String studyStatus="";//to be changed
			String studyName="";
			String studyNumber="";
			space = (space==null)?"":space.toString();
	
			String incorrectFile = request.getParameter("incorrectFile");
			incorrectFile = (incorrectFile==null)?"":incorrectFile.toString();			
			%>


			<%
			if(space.equals("1")) {
			%>
				<br><br><br><br>
				<table width=100%>
				<tr><td align=center>
				<p class = "sectionHeadings">

				<%=MC.M_UploadSpace_ContAdmin%><%-- The space allotted to you for file uploading has exhausted. Please contact Velos Administrator.*****--%> 

				</p>
				</td>
				</tr>

				<tr height=20></tr>
				<tr>
				<td align=center>
			<button onclick="window.history.go(-2);"><%=LC.L_Back%></button>
			</td>		
			</tr>		
		</table>		
<%		
				} 
				else if(incorrectFile.equals("1")) {
%>
<br><br><br><br>
<table width=100%>
<tr>
<td align=center>

<p class = "sectionHeadings">

<%=MC.M_FileUpldEmpty_IncorrPath%><%-- The file that you are trying to upload is either empty or the path specified is incorrect.*****--%>  

</p>
</td>
</tr>

<tr height=20></tr>
<tr>
<td align=center>

		<button onclick="window.history.go(-1);"><%=LC.L_Back%></button>
</td>		
</tr>		
</table>		


<%		
	} else 
				{ //else of if for space.equals("1")

		
		   	int count = 0;
		    String mileApndxId="";
     	    String milestoneId="";
    	    String mileApndxDesc="";	
	   	    String mileApndxUri = "";		
			String mileApndxType="";
			
			
		ArrayList mileApndxIds = null;
		ArrayList milestoneIds = null;
		ArrayList mileApndxDescs =null;
		ArrayList mileApndxUris=null;
		ArrayList mileApndxTypes=null;
		
		
		ArrayList mileApndxFileIds =null;
		ArrayList mileApndxFileDescs =null;
		ArrayList mileApndxFileUris=null;

       %>
	   
	<form METHOD=POST action="" name="mile">
<%
if (pgRight == 4) {
%>
   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_YouOnly_ViewPerm%><%-- You have only View permission*****--%></Font></P>
<%}

	    MileApndxDao mileApndxDao=new  MileApndxDao();  //to be called using JB			
		mileApndxDao.getMileApndxUris(EJBUtil.stringToNum(studyId));
			
		 mileApndxIds = mileApndxDao.getMileApndxIds();		
		 mileApndxDescs = mileApndxDao.getMileApndxDescs();
		 mileApndxUris=mileApndxDao.getMileApndxUris();
		 mileApndxTypes=mileApndxDao.getMileApndxTypes();	

%>
<P class = "defComments"><%=MC.M_FlwWebPgDocu_LnkThisStd%><%-- The following web pages and documents are linked to this <%=LC.Std_Study_Lower%>*****--%>:<br> </P>

<TABLE width="90%">
<tr>
<th ><%=LC.L_My_Links%><%-- My Links*****--%></th>
</tr>
<tr>
<td>
<P class = "defComments"><%=MC.M_YouCanList_ImplLnkUrl%><%-- You can list your important links. Give full path of your URLs.*****--%>
<br><br> <%=MC.M_EditLnkDet_ClkNameOpen%><%-- Select [Edit] against the name of a link to edit its details. Click on the name of the link to open it in a new browser window.*****--%></P>
</td>
</tr>
</table>

<%if(!((!(studyStatus == null)) && (studyStatus.equals("F")))) {%>

<TABLE width="90%">
   <tr>
	<td colspan=3>&nbsp;</td>
	<td width=25% align=right>      
	 <A href="addmileurl.jsp?studyId=<%=studyId%>&mode=N&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=LC.L_Add_NewUrl%><%-- Add New URL*****--%></A> 
	</td>
   </tr>
</TABLE>
<TABLE width="90%" cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr>
		<th><%=LC.L_Url_Upper%><%-- URL*****--%></th>
		<th><%=LC.L_Description%><%-- Description*****--%></th>
		<th width="15%"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="15%"><%=LC.L_Delete%><%-- Delete*****--%></th>
   </tr>

<% 
   for(int i=0;i<mileApndxIds.size(); i++) {
   
	mileApndxId= (String)mileApndxIds.get(i);
	mileApndxDesc=(String)mileApndxDescs.get(i); 
	mileApndxUri =(String)mileApndxUris.get(i); 
	mileApndxType=(String)mileApndxTypes.get(i);
	if ((i%2)!=0) {
  %>
      <tr id="browserEvenRow"> 
        <%
		}
		else{
  %>
      <tr id="browserOddRow"> 
        <%
		}
  %>
	<td><A href="<%=mileApndxUri%>" target="_new"><%=mileApndxUri%></A></td>
	<td> <%=mileApndxDesc%> </td>
	<td width=15% align="center">
	<A HREF="addmileurl.jsp?studyId=<%=studyId%>&mode=M&srcmenu=<%=src%>&mileApndxId=<%=mileApndxId%>&selectedTab=<%=selectedTab%>&mileApndxType=<%=mileApndxType%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"/></A>
	 </td>
	<td width=15%>

	<A HREF="mileapndxdelete.jsp?mileApndxId=<%=mileApndxId%>&studyId=<%=studyId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mileApndxType=<%=mileApndxType%>" onClick="return confirmBox('<%=mileApndxUri%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
	
	</td>
   </tr>
<%
   }
    MileApndxDao mileApndxDaoFile =new MileApndxDao();
	
	
	 	mileApndxDaoFile.getMileApndxFiles(EJBUtil.stringToNum(studyId)); //to be changed
		
		 mileApndxIds = mileApndxDaoFile.getMileApndxIds();
		 mileApndxDescs = mileApndxDaoFile.getMileApndxDescs();
		 mileApndxUris=mileApndxDaoFile.getMileApndxUris();
		
%>		
		
</TABLE>
<%}%>



<BR>
<TABLE width="90%">
<tr>
<th ><%=LC.L_My_Files%><%-- My Files*****--%></th>
</tr>
<tr>
<td>
<P class = "defComments"><%=MC.M_AttachDocu_WithMstone%><%-- You can attach your important documents/forms with this milestone.*****--%>
<br><br> <%=MC.M_SelDel_RemAppx_ClkFile%><%-- Select [Delete] against the name of a file to remove it from appendix. Click on the file name to view it in a new browser window.*****--%></P>
</td>
</tr>
</table>

<%if(!((!(studyStatus == null)) && (studyStatus.equals("F")))) {%>

<TABLE width="90%">
   <tr>
	<td colspan=3></td>
	<td width=25% align=right>
	<A href="mileapndxfile.jsp?studyId=<%=studyId%>&mode=N&selectedTab=<%=selectedTab%>&srcmenu=<%=src%>&mileApndxType=<%=mileApndxType%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=LC.L_Upload_Docu%><%-- Upload Document*****--%></A>	
	</td>
   </tr>
</TABLE>
<TABLE width="90%" cellspacing="0" cellpadding="0" class="basetbl outline midalign">
	<tr>
		<th width="30%"><%=LC.L_File_Name%><%-- File name*****--%></th>
		<th width="60%"><%=LC.L_Description%><%-- Description*****--%></th>
		<th width="5%"><%=LC.L_Edit%><%-- Edit*****--%></th>
		<th width="5%"><%=LC.L_Delete%><%-- Delete*****--%></th>   
	</tr>
<%
	String dnld;
	Configuration.readSettings("eres");
	Configuration.readUploadDownloadParam(Configuration.FILE_UPLOAD_DOWNLOAD + "fileUploadDownload.xml", "milestone");
	dnld=Configuration.DOWNLOADSERVLET ;
	String modDnld = "";

   for(int i=0;i<mileApndxIds.size(); i++) {
	mileApndxId= (String)mileApndxIds.get(i);
	mileApndxDesc= (String)mileApndxDescs.get(i); 
	mileApndxUri = (String)mileApndxUris.get(i);	
	
	

	if ((i%2)!=0) {
  %>
      <tr id="browserEvenRow"> 
        <%
		}
		else{
  %>
      <tr id="browserOddRow"> 
        <%
		}
modDnld = dnld + "?file=" + StringUtil.encodeString(mileApndxUri) ;		
  %>

	


	<td> 
	
	<A href="#" onClick="fdownload(document.mile,<%=mileApndxId%>,'<%=mileApndxUri%>','<%=dnld%>');return false;" >
 	
	<%=mileApndxUri %></A> </td>
	<td> <%=mileApndxDesc%> </td>
	
	
	<td width=15% align="center">	  
	<A HREF="mileapndxfile.jsp?mileApndxId=<%=mileApndxId%>&studyId=<%=studyId%>&mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetType=<%=budgetType%>" onClick="return f_check_perm(<%=pageRight%>,'E')"><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"/></A>
	</td>

	<td width=15%>
	<A HREF="mileapndxdelete.jsp?mileApndxId=<%=mileApndxId%>&studyId=<%=studyId%>&delMode=null&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&mileApndxType=<%=mileApndxType%>" onClick="return confirmBox('<%=mileApndxUri%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
	
	</td>
   </tr>
   

<%
   }
%>
</TABLE>
  

<%}%>

<br>

 

    <input type="hidden" name="tableName" value="ER_MILEAPNDX">
    <input type="hidden" name="columnName" value="MILEAPNDX_FILEOBJ">
    <input type="hidden" name="pkColumnName" value="PK_MILEAPNDX">
    <input type="hidden" name="module" value="milestone">
    <input type="hidden" name="db" value="eres">
    <input type="hidden" name="pkValue" value="">
    <input type="hidden" name="file" value="">
    <input type="hidden" name="dnldurl" value="">
    
</form>

<%
}// end of if for space

} //end of if body for page right

else
 {
	%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
 } //end of else body for page right   

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>-->
</div>
</body>
</html>
