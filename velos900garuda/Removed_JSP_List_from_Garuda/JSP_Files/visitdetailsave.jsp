<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HEAD> 
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil,com.velos.esch.web.protvisit.ProtVisitJB" %>
<%@ page import = "com.velos.eres.service.util.MC,com.velos.eres.service.util.LC"%>
<%@ page import = "com.velos.eres.service.util.VelosResourceBundle"%>
<%@ page import = "com.velos.esch.service.util.EJBUtil"%>
<%@ page import = "com.velos.esch.business.common.SchCodeDao"%>

<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>
</head>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id="protVisitB_Compare" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
 
<%
String src = request.getParameter("srcmenu");
String protocolId = request.getParameter("protocolId");
String calledFrom = request.getParameter("calledFrom");
String mode = request.getParameter("mode");
String fromPage = request.getParameter("fromPage");
String calStatus = request.getParameter("calStatus");

String categoryId = "";
String visitId = "";
String eSign = request.getParameter("eSign");
String visit_name = (request.getParameter("visitName")).trim() ;
String frmVisitId = request.getParameter("visitId");


//String displacement = request.getParameter("displacement");
String visitNo = request.getParameter("visitNo");
String monthStr = request.getParameter("months");
String weekStr = request.getParameter("weeks");
String dayStr = request.getParameter("days");
String insertAfterDispStr = request.getParameter("insertAfter");
//String insertAfterStr = "";
String insertAfterIntervalStr = request.getParameter("insertAfterInterval");
int insertAfterInterval = 0;
int new_protocol_duration = 0;
String insertAfterIntervalUnit = request.getParameter("intervalUnit");
String description =  request.getParameter("description") ;
String duration=  request.getParameter("duration") ;
String dispStr = "";

//KM-#4874
String offLnFlag = request.getParameter("offLnFlag");
if (calStatus.equals("O") && (offLnFlag.equals("1") || offLnFlag.equals("2"))) {	
	insertAfterDispStr = request.getParameter("insertAfterOffline");
	insertAfterIntervalUnit = request.getParameter("intervalUnitOffline");
}
 
//KM-DFIN4
String beforeNum = request.getParameter("beforenum");
beforeNum = (beforeNum == null) ? "": beforeNum;
if (beforeNum.length() == 0) {
	beforeNum = "0";
}

String afterNum = request.getParameter("afternum");

afterNum = (afterNum == null) ? "": afterNum;
if (afterNum.length() == 0) {
	afterNum = "0"; 
}

String durUnitA = request.getParameter("durUnitA");
durUnitA = (durUnitA == null) ? "" : durUnitA;

String durUnitB = request.getParameter("durUnitB");
durUnitB = (durUnitB == null) ? "" : durUnitB;


String calAssoc=request.getParameter("calassoc");
calAssoc=(calAssoc==null)?"":calAssoc;

String noInterval = request.getParameter("noInterval");

int months = 0, weeks = 0, days = 0;
int ret=0;
int insertAfter = 0 ;
int disp = 0, prevdisp = 0, interval = 0;
int noOfDays;
String intervalUnit = "";
description = (   description  == null      )?"":(  description ) ;

if ((insertAfterIntervalStr != null) && (insertAfterIntervalStr.length() > 0)) {
	visitId = request.getParameter("visitId");
	protVisitB_Compare.setVisit_id(EJBUtil.stringToNum(visitId));
	protVisitB_Compare.getProtVisitDetails();
	
	int beanMonths = protVisitB_Compare.getMonths(), beanWeeks = protVisitB_Compare.getWeeks(), beanDays = (protVisitB_Compare.getDays()==null)?0:protVisitB_Compare.getDays().intValue();
	String beanNoIntervalFlag = protVisitB_Compare.getIntervalFlag();
	
	int pos = insertAfterDispStr.indexOf('/');
	if (pos > 0) {
		String insertAfterStr =  insertAfterDispStr.substring(0, pos);
		dispStr = insertAfterDispStr.substring(pos+1);
		insertAfter = EJBUtil.stringToNum(insertAfterStr);
		prevdisp = EJBUtil.stringToNum(dispStr);
	}

	months = 0;
	weeks = 0;
	days = 0;
//	insertAfter = EJBUtil.stringToNum(insertAfterStr);
	insertAfterInterval = EJBUtil.stringToNum(insertAfterIntervalStr);
	if (insertAfterIntervalUnit.equals("M"))
		interval = insertAfterInterval *30;
	else if (insertAfterIntervalUnit.equals("W"))
		interval = insertAfterInterval *7;
	else //days
		interval = insertAfterInterval;

	disp = prevdisp + interval;
	
	String tempNoInterval = (noInterval==null)?"0":noInterval;
	beanNoIntervalFlag = (beanNoIntervalFlag==null)?"0":beanNoIntervalFlag;
	if (tempNoInterval.equals(beanNoIntervalFlag)){
		if ((months != beanMonths) || (weeks != beanWeeks) || (days != beanDays)){
			if(disp == 0 && calStatus.equals("O")){
				SchCodeDao cd = new SchCodeDao();
				String calStatusDesc = cd.getCodeDescription(cd.getCodeId("calStatStd","O"));
				
				Object[] arguments = {calStatusDesc};
				%>
				<br><br><br><br><br><p class = "sectionHeadings" align = center><%=VelosResourceBundle.getMessageString("M_OfflineCalculatedVisitDayZero",arguments)%><Br><Br>
				<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
				<% 
				return;
			}
		}
	}%>
<%
}
else {
	visitId = request.getParameter("visitId");
	protVisitB_Compare.setVisit_id(EJBUtil.stringToNum(visitId));
	protVisitB_Compare.getProtVisitDetails();
	
	int beanMonths = protVisitB_Compare.getMonths(), beanWeeks = protVisitB_Compare.getWeeks(), beanDays = (protVisitB_Compare.getDays()==null)?0:protVisitB_Compare.getDays().intValue();
	String beanNoIntervalFlag = protVisitB_Compare.getIntervalFlag();
	
	months = EJBUtil.stringToNum(monthStr);
	weeks = EJBUtil.stringToNum(weekStr);
	days = EJBUtil.stringToNum(dayStr);

	String tempNoInterval = (noInterval==null)?"0":noInterval;
	beanNoIntervalFlag = (beanNoIntervalFlag==null)?"0":beanNoIntervalFlag;
	if (tempNoInterval.equals(beanNoIntervalFlag)){
		if ((months != beanMonths) || (weeks != beanWeeks) || (days != beanDays)){
			if ((months == 0) && (weeks == 0) && (days == 0)){
				if(calStatus.equals("O")){
					SchCodeDao cd = new SchCodeDao();
					String calStatusDesc = cd.getCodeDescription(cd.getCodeId("calStatStd","O"));
					
					Object[] arguments = {calStatusDesc};
					%>
					<br><br><br><br><br><p class = "sectionHeadings" align = center><%=VelosResourceBundle.getMessageString("M_OfflineVisitDayZero",arguments)%><Br><Br>
					<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
					<% 
					return;
				}
			}
		}
	}	
	if (months == 0) months++;
	if (weeks == 0) weeks++;
	if (days == 0) days++;
	disp = (months - 1)*30 + (weeks - 1)*7 + days;


	// SV, 10/23/04, store the values as entered and at display time, we can always display correctly.
	disp = 0;
	if (months > 0) disp+= (months -1)*30;
	if (weeks > 0) disp+= (weeks -1)*7;
	disp += days;


	insertAfter = 0;
	insertAfterInterval = 0;
	insertAfterIntervalUnit = "";

}
//BK,May/20/2011,Fixed #6014
   ProtVisitJB pVisit = new ProtVisitJB();
     int checkVisit = 0;
     int vId = EJBUtil.stringToNum(request.getParameter("visitId"));
    if(disp == EJBUtil.stringToNum(duration) ){
	checkVisit = pVisit.validateDayZeroVisit(Integer.parseInt(duration),Integer.parseInt(protocolId),1,vId);
	}  

	if (disp > EJBUtil.stringToNum(duration) || checkVisit > 0)
	{%>
	<br><br><br><br><br><p class = "sectionHeadings" align = center>
		<%if ((insertAfterIntervalStr != null) && (insertAfterIntervalStr.length() > 0)){%>
			<%=MC.M_CalcIntervalExceedsDuration%>
		<% }else{%>
			<%=MC.M_MaxIntervalExceedsDuration%>
		<%}%><Br><Br>
		<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% 
		return;
	}
	checkVisit = 0;
	if(disp == 1 && EJBUtil.stringToNum(dayStr) == 0 && StringUtil.isEmpty(noInterval)){
		checkVisit = pVisit.validateDayZeroVisit(Integer.parseInt(duration),Integer.parseInt(protocolId),0,vId);
		}
	if (checkVisit > 0){%>
			<br><br><br><br><br><p class = "sectionHeadings" align = center><%=MC.M_DayZeroExceedsDuration%><Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;
	}
%>


<%
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
//	String oldESign = (String) tSession.getValue("eSign");


//	if(!oldESign.equals(eSign)) {
%>
  
<%
//	} else {


		String ip_add = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");

		String accountId = (String) tSession.getValue("accountId");

   if(mode.equals("N")) {
	protVisitB.setProtocol_id(EJBUtil.stringToNum(protocolId));
	protVisitB.setName(visit_name);
	protVisitB.setDescription(description   );
	
	if (noInterval == null) {
	protVisitB.setMonths(EJBUtil.stringToNum(monthStr));	 //SV, 10/25/04, save the original values here.
	protVisitB.setWeeks(EJBUtil.stringToNum(weekStr));
//DFIN-25 DAY 0  BK	    Fix 6164 May 03 2011
	    if(!StringUtil.isEmpty(dayStr) && insertAfter == 0){
			noOfDays = EJBUtil.stringToNum(dayStr);
	    	 }
	    else if(insertAfter != 0){
	    	noOfDays = disp;
			}
	    else {
		    noOfDays = 1; 	
			}	
	protVisitB.setDays(new Integer(noOfDays));

	protVisitB.setInsertAfter(new Integer(insertAfter));
	protVisitB.setInsertAfterInterval(new Integer(insertAfterInterval));
	protVisitB.setInsertAfterIntervalUnit(insertAfterIntervalUnit);
	//KM
	protVisitB.setDisplacement(String.valueOf(disp));
	protVisitB.setIntervalFlag("0");
	}
	else  {
		//KM-09Oct09
		protVisitB.setDisplacement(null); 
		protVisitB.setIntervalFlag("1");
		
		protVisitB.setMonths(0);
		protVisitB.setWeeks(0);
		protVisitB.setDays(null);
		
		protVisitB.setInsertAfter(null);
		protVisitB.setInsertAfterInterval(null);
		protVisitB.setInsertAfterIntervalUnit(null);
	}


	int visitNum=protVisitB.getMaxVisitNo(EJBUtil.stringToNum(protocolId));

	protVisitB.setVisit_no(visitNum + 1);
  // Modified By Amarnadh for Bugzilla issue #3131
	protVisitB.setCreator(usr);
 // protVisitB.setModifiedBy(EJBUtil.stringToNum(usr));


	protVisitB.setip_add(ip_add);

	protVisitB.setDurationBefore(beforeNum);
	protVisitB.setDurationUnitBefore(durUnitA);

	protVisitB.setDurationAfter(afterNum);
	protVisitB.setDurationUnitAfter(durUnitB);

	protVisitB.setProtVisitDetails(); // SV, this actually creates a new bean/statekeeper.
	ret=protVisitB.getVisit_id(); //SV, this gets it from the newly created bean/statekeeper.


	visitId = (new Integer(protVisitB.getVisit_id())).toString();
	
	//mode="M";--KM
   } else {
		visitId = request.getParameter("visitId");
		protVisitB.setVisit_id(EJBUtil.stringToNum(visitId));
//SV, REDTAG, do we even need this here?		protVisitB.getProtVisitDetails();
		protVisitB.setProtocol_id(EJBUtil.stringToNum(protocolId));
		
		//KM-30July08
		protVisitB.setName(request.getParameter("visitName").trim());
		protVisitB.setDescription(description   );

		if (noInterval == null) {

		protVisitB.setMonths(EJBUtil.stringToNum(monthStr));
		protVisitB.setWeeks(EJBUtil.stringToNum(weekStr));
		//DFIN-25 DAY 0  BK FEB-14-2011
	    if(!StringUtil.isEmpty(dayStr) && insertAfter == 0){
			noOfDays = EJBUtil.stringToNum(dayStr);
	    	 }
	    else if(insertAfter != 0){
	    	noOfDays = disp;
			}
	    else {
		    noOfDays = 1; 	
			}		
	    protVisitB.setDays(new Integer(noOfDays));
		protVisitB.setInsertAfter(new Integer(insertAfter));
		protVisitB.setInsertAfterInterval(new Integer(insertAfterInterval));
		protVisitB.setInsertAfterIntervalUnit(insertAfterIntervalUnit);

		protVisitB.setDisplacement(String.valueOf(disp));

		protVisitB.setIntervalFlag("0");
		}
		else  {

			//KM-09Oct09	
			protVisitB.setIntervalFlag("1");
			protVisitB.setDisplacement(null);
			
			protVisitB.setMonths(0);
			protVisitB.setWeeks(0);
			protVisitB.setDays(null);
			
			protVisitB.setInsertAfter(null);
			protVisitB.setInsertAfterInterval(null);
			protVisitB.setInsertAfterIntervalUnit(null);
		}
		
		protVisitB.setip_add(ip_add);
		protVisitB.setVisit_no(EJBUtil.stringToNum(visitNo));

	
	 	protVisitB.setModifiedBy(usr);

		//KM-DFIN4
		protVisitB.setDurationBefore(beforeNum);
		protVisitB.setDurationUnitBefore(durUnitA);

		protVisitB.setDurationAfter(afterNum);
		protVisitB.setDurationUnitAfter(durUnitB);

			ret=protVisitB.updateProtVisit(calledFrom);
		}

	
	
	if ((calledFrom.equals("P")) || (calledFrom.equals("S"))) {
			if (ret==-1) {%>
			<br><br><br><br><br><p class = "sectionHeadings" align = center>
			<%=MC.M_VisitNameDupli_ClkBack%><%-- Visit Name is duplicate. Click on "Back" Button to change the name*****--%> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>

		<% }
			//D-FIN-25-DAY0
			else if (ret==-2) {%>			
			<br><br><br><br><br><p class = "sectionHeadings" align = center>
			<%=MC.M_VstIntvlWk_CoexistBkChg%><%-- Visit intervals Month1,week1,day7 and day 0 can not coexist. Click on "Back" Button to change.*****--%> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>

		<% }
		//BK,May/13/2011,Fix #6227
			else if(ret == -5) {%>	
			<br><br><br><br><br><p class = "sectionHeadings" align = center>
			 <%=MC.M_VstNoIntvlDef_ClkBackSet%><%-- Visit Interval "No Interval Defined" can not be set for parent visit.Click on "Back" Button to set different interval.*****--%><Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>

		<% }
		else{

				tSession.putValue("visitname",visit_name);
				if (ret>=0)
				 {
				  //Bug #6631
				  new_protocol_duration = protVisitB.generateRipple(new Integer(visitId).intValue(),calledFrom);



				  if (new_protocol_duration > 0) //if ripple succeeds
				  {
				  		tSession.setAttribute("newduration", String.valueOf(new_protocol_duration))	;

				  }else
				  {

				  		tSession.setAttribute("newduration", duration);
				  }

				 }

				 if(mode.equals("N"))	{
 				    
					 protVisitB.copyVisit(EJBUtil.stringToNum(protocolId),EJBUtil.stringToNum(frmVisitId),EJBUtil.stringToNum(visitId),calledFrom,usr,ip_add);
				 }


 		//if (fromPage.equals("fetchProt")){
		%>
			<script>
				window.opener.location="fetchProt.jsp?srcmenu=<%=src%>&selectedTab=3&visitId=<%=visitId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&duration=<%=duration%>&calassoc=<%=calAssoc%>"
				setTimeout("self.close()",1000);
			</script>

			<%
		//}else{%>

			<!--script>
				//window.opener.location="visitlist.jsp?srcmenu=<%=src%>&selectedTab=3&visitId=<%=visitId%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&fromPage=<%=fromPage%>&calStatus=<%=calStatus%>&duration=<%=duration%>&calassoc=<%=calAssoc%>"
				//setTimeout("self.close()",1000);
			</script-->
		<%
		//}
		}

	}%>


 <%//out.println(request.getParameter("updateFlag"));

if (request.getParameter("updateFlag").equals("Add")){
	mode = "N";
}
%>
<!--
				<script>
//					window.opener.location.reload(true);
	window.opener.location="visitlist.jsp?mode=<%--=mode--%>&srcmenu=<%--=src--%>&selectedTab=3&visitId=<%--=visitId--%>&protocolId=<%--=protocolId--%>&calledFrom=<%--=calledFrom--%>&mode=<%--=mode--%>&fromPage=<%--=fromPage--%>&calStatus=<%--=calStatus--%>&duration=<%--=duration--%>&calassoc=<%--=calAssoc--%>"

					setTimeout("self.close()",1000);
				</script> -->

<%
//}// end of if body for esign
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>


</BODY>
</HTML>


