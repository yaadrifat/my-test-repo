<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Calendar_Definition %><%-- Calendar >> Definition*****--%></title>
<%@ page import="com.velos.eres.service.util.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.*" %>
<jsp:useBean id="budgetB" scope="request"	class="com.velos.esch.web.budget.BudgetJB" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
</head>

<SCRIPT Language="javascript">
 function  validate(formobj, mode){
 if (typeof(formobj.rbSharedWith)!="undefined")
   {
  if((formobj.rbSharedWith[2].checked == true) && (formobj.selGrpNames.value==""))
		 {
	  		alert("<%=MC.M_PlsSelectGrp%>");/*alert("Please Select Group");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[3].checked == true) && (formobj.selStudy.value==""))
		 {
			 alert("<%=MC.M_PlsSelectStd%>");/*alert("Please Select <%=LC.Std_Study%>");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[4].checked == true) && (formobj.selOrg.value==""))
		 {
			 alert("<%=MC.M_PlsSelectOrg%>");/*alert("Please Select Organization");*****/
		 return false;
		 }
	 }
	if (!allsplcharcheck(formobj.protocolName.value))  
	{
		formobj.protocolName.focus();  return false;
	}	
 	if (!(validate_col('Protocol Name',formobj.protocolName))) return false;
 	 	if (!(validate_col('Calendar Type ',formobj.caltype))) return false;
	 if (!(validate_col('Duration',formobj.durNum))) return false;
     if (!(validate_col('Duration Unit',formobj.durUnit))) return false;
	 if (!(validate_col('Calendar Status',formobj.calStatus))) return false;
     if ((!(isInteger(formobj.durNum.value))) || (formobj.durNum.value=='0')) {
    	 alert("<%=MC.M_DurYou_SpecNotValid%>");/*alert("Duration that you have specified is not valid");*****/
		formobj.durNum.focus();
		 return false;
	}
   	if (fConfirmDurationChange(formobj, mode) == false) return false;

	//JM: 30May2008, atlease one option should be selected

	if ( formobj.actualSts.value=='O' && formobj.calassoc.value=='P') {
		if ( (formobj.calStatus.value=='A') && (formobj.patSchChk[0].checked==false)&&(formobj.patSchChk[1].checked==false)&&(formobj.patSchChk[2].checked==false)){
			alert("<%=MC.M_SelAtLeast_OneOpt%>");/*alert("Please select at least one option");*****/
			return false;
		}
	}


//JM: 30June2008, #3592

if ( formobj.actualSts.value=='O' && formobj.calassoc.value=='P') {

if ( 	(formobj.patSchChk[2].checked==true) && (formobj.calStatus.value=='A')){



	if ( (formobj.patstat.value !='') && (formobj.dateTxt.value=='')){
		alert("<%=MC.M_EtrDtFor_PatStat%>")/*alert("Please enter the Date for the <%=LC.Pat_Patient%> Status")*****/
		return false;
	}
	else if((formobj.patstat.value =='')  && (formobj.dateTxt.value!='')){
		alert("<%=MC.M_Selc_PatStatus%>")/*alert("Please select the <%=LC.Pat_Patient%> Status")*****/
		return false;
	}
	else if ((formobj.patstat.value =='') && (formobj.dateTxt.value=='')){
		alert("<%=MC.M_SelPatStat_EtrDt%>")/*alert("Please select the <%=LC.Pat_Patient%> Status and enter the Date")*****/
		return false;
	}

}
}

	//	if (!((formobj.status.value=='F') || ((formobj.status.value=='A') && (formobj.calStatus.value!='A')) )   {
	//SV, 8/13/04, fix for bug #1594, a little convoluted logic here: Read it as,
	// if the original status is not 'A' or 'F', then validate always, else (meaning status is 'A' or 'F' -second part of if below), validate only if status is being changed.
	if (!((formobj.status.value == 'F')  || (formobj.status.value == 'A')) || (formobj.calStatus.value != formobj.status.value)) {
	 	if (   (       (formobj.pageRight.value > 4)   && (formobj.mode.value == "N" )    )    ||          (formobj.pageRight.value > 5)      ) {
			if (!(validate_col('Esign',formobj.eSign))) return false;


			 if(isNaN(formobj.eSign.value) == true) {
				 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
					formobj.eSign.focus();
					return false;
			}
		}
	}



 if(formobj.actualSts.value=="A")
	{
		formobj.durUnit.disabled = false;

	}


}


function setDurationCodeEditBox(formobj)
{
	var dd = formobj.durUnit
    var idx = formobj.durUnit.selectedIndex;

	formobj.durUnitCd.value = dd.options[idx].value;
}


function  fConfirmDurationChange(formobj, mode){
	ret = true;
	formobj.durReduced.value = "0";		// SV, 8/17/04, added to address bug #1602.
	if (mode == "M") {
		prevDur = parseInt(formobj.prevDuration.value);
		prevDurUnitCd = formobj.prevDurUnitCd.value;
		thisDur = parseInt(formobj.durNum.value);

		thisDurUnitCd = formobj.durUnitCd.value;



		var days1, days2;
		days1 = fGetDays(prevDur, prevDurUnitCd);
		days2 = fGetDays(thisDur, thisDurUnitCd);


		if (days1 > days2) {
			/*if (confirm("There may be Events and Visits associated to the interval you are changing.If you proceed, you may lose some events and visits. Do you want to change it?")){*****/
				if (confirm("<%=MC.M_EvtVstAssoc_MayLooseEvt%>")){
				if ((formobj.pageRight.value > 5) && !(formobj.status.value == 'F')) {
					formobj.durReduced.value = "1";		// SV, 8/17/04, duration has been changed.
				}
				ret = true;
			}
			//JM: 23Nov06
			//else ret = true;
			else ret = false;
		}
		else ret = true;
	}

		return ret;
}
function  fGetDays(dur, unitCd)
{

	if (unitCd == 'D' || unitCd == 'day')
		return(dur);
	else if (unitCd == 'W' || unitCd == 'week')
		return(dur*7  );
	else if (unitCd == 'M' || unitCd == 'month')
		return (dur*30);
	else if (unitCd == 'Y' || unitCd == 'year')
		return (dur*365);

}
function fSetEsignVisibility(formobj)
{
//	alert(formobj.status.value + formobj.calStatus.value);
	if (formobj.status.value != 'A') {
		document.getElementById('eSignDiv').style.display = 'block';
	}
	else {
		if (formobj.calStatus.value == 'A') {
			document.getElementById('eSignDiv').style.display = 'none';
		}
		else {
			document.getElementById('eSignDiv').style.display = 'block';
		}
	}

}

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>


<% String src;

String selectedTab = request.getParameter("selectedTab");

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>


<body>


  <%

	String protocolId ="";
	String protocolName ="";

	String desc ="";

	String durationNum="";
	String durationUnitCode = "";
	String prevDuration ="";
	String prevDurUnitCd = ""; //SV, 10/22/04, to expand the duration  comparison for DurReduced flag.
	String mode="";
	String durationUnit="";
	String calStatus = "";
	String calledFrom="";
	String bgtTemplate ="";

	String inputClass = "inpDefault";
	String makeReadOnly = "";
	String makeGrey = "Black";
	String makeDisabled = "";
	String ftextGrey = "textDefault";
	String sharedWith="";
	int calendarType=0;
	String calAssoc="P";
	String formId = request.getParameter("formId"); //?? Questionable; This should be protocol id (event id for event type = 'P')

	HttpSession tSession = request.getSession(true);


	if (sessionmaint.isValidSession(tSession))
	{
		String uName = (String) tSession.getValue("userName");
		String accId=(String)tSession.getAttribute("accountId");
		String userId=(String)tSession.getAttribute("userId");

		String studyId = "";
		int iaccId=EJBUtil.stringToNum(accId);
		mode = request.getParameter("mode");
		int pageRight = 0;
		calledFrom = request.getParameter("calledFrom");
		String catLibType="T" ;
		CatLibDao catLibDao= new CatLibDao();
		catLibDao = catLibJB.getAllCategories(iaccId,"L");
		ArrayList ids= new ArrayList();
		ArrayList names= new ArrayList();
		String pullDown;
		int rows;
		rows=catLibDao.getRows();
		ids = catLibDao.getCatLibIds();
		names= catLibDao.getCatLibNames();




		//JM: 22May2008, added for, issue #3492

		String dEvStat ="";

		CodeDao cDao = new CodeDao();
		cDao.getCodeValues("patStatus",0);


		StringBuffer sb = new StringBuffer();

		int cnt = 0;
	 	sb.append("<SELECT NAME='patstat'>");
	 	sb.append("<Option value='' Selected>"+LC.L_Select_AnOption+"</option>") ;/*sb.append("<Option value='' Selected>Select an Option</option>") ;*****/
		for (cnt = 0; cnt <= cDao.getCDesc().size() -1 ; cnt++)
		{
			sb.append("<OPTION value = "+ cDao.getCId().get(cnt)+">" + cDao.getCDesc().get(cnt)+ "</OPTION>");
		}

		sb.append("</SELECT>");
		dEvStat = sb.toString();

//JM: 22May2008, added for, issue #3492 ---above


		if (calledFrom.equals("S")) {
   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	  	   if ((stdRights.getFtrRights().size()) == 0){
			 	pageRight= 0;
		   }else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));



	   	   }
		} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));


		}


		if (mode.equals("M")) {

	       		protocolId = request.getParameter("protocolId");

			if (calledFrom.equals("P") || calledFrom.equals("L")) {
				eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));

				eventdefB.getEventdefDetails();
				sharedWith=eventdefB.getCalSharedWith();

				sharedWith = (sharedWith == null)?"A":sharedWith;
				//DEBUG System.out.println("protocolcalendar: sharedwith="+sharedWith);
				protocolName = eventdefB.getName();
				//SV, 10/25/04, save this in the session, as with the new TAB selection, updateprotocol may not be called before
				// other screens are called and thus the name was showing old name.
				tSession.putValue("protocolname", protocolName);

				desc = eventdefB.getDescription();
//				desc = (   desc  == null      )?"":(  desc ) ;

				durationNum=eventdefB.getDuration();
				durationUnitCode=eventdefB.getDurationUnit();

				tSession.setAttribute("newduration", durationNum);

				durationUnitCode = (durationUnitCode == null)?"D":durationUnitCode;
				if (durationUnitCode.equals("W")) {
					int temp = EJBUtil.stringToNum(durationNum);
					temp = temp/7;
					durationNum = String.valueOf(temp);
				}
				else if (durationUnitCode.equals("M")) {
					// SV, 9/04, added as part of cal-enh-01
					int temp = EJBUtil.stringToNum(durationNum);
					temp = temp/30;
					durationNum = String.valueOf(temp);
				}
				else if (durationUnitCode.equals("Y")) {
					int temp = EJBUtil.stringToNum(durationNum);
					temp = temp/365;
					durationNum = String.valueOf(temp);
				}

				//JM: 02FEB2011: #Enh-D-FIN9
				//calStatus = eventdefB.getStatus();
				SchCodeDao cd = new SchCodeDao();
				calStatus = cd.getCodeSubtype(EJBUtil.stringToNum(eventdefB.getStatCode()));
				calStatus = (calStatus==null)?"":calStatus;
				
				calendarType=EJBUtil.stringToNum(eventdefB.getEventCalType());
			}
			else if (calledFrom.equals("S")) { //Protocol called from Study

				eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
				eventassocB.getEventAssocDetails();
				protocolName = eventassocB.getName();
				//KM-#5882
				studyId = eventassocB.getChain_id();
				//SV, 10/25/04, save this in the session, as with the new TAB selection, updateprotocol may not be called before
				// other screens are called and thus the name was showing old name.
				tSession.putValue("protocolname", protocolName);

				desc = eventassocB.getDescription();
				durationNum=eventassocB.getDuration();

				tSession.setAttribute("newduration", durationNum);

				durationUnitCode=eventassocB.getDurationUnit();
				if (durationUnitCode.equals("W")) {
					int temp = EJBUtil.stringToNum(durationNum);
					temp = temp/7;
					durationNum = String.valueOf(temp);
				}
				else if (durationUnitCode.equals("M")) {
					// SV, 9/04, added as part of cal-enh-01
					int temp = EJBUtil.stringToNum(durationNum);
					temp = temp/30;
					durationNum = String.valueOf(temp);
				}
				else if (durationUnitCode.equals("Y")) {
					int temp = EJBUtil.stringToNum(durationNum);
					temp = temp/365;
					durationNum = String.valueOf(temp);
				}

				//JM: 02FEB2011: #Enh-D-FIN9
				//calStatus = eventassocB.getStatus();
				SchCodeDao cd = new SchCodeDao();
				calStatus = cd.getCodeSubtype(EJBUtil.stringToNum(eventassocB.getStatCode()));
				calStatus = (calStatus==null)?"":calStatus;


				calendarType=EJBUtil.stringToNum(eventassocB.getEventCalType());
				calAssoc=eventassocB.getCalAssocTo();
				bgtTemplate = eventassocB.getBudgetTemplate();

				if (bgtTemplate == null) bgtTemplate ="0";

			}
				prevDuration =durationNum;
				prevDurUnitCd =durationUnitCode;


				   		}
//SV, 8/19/04, "null" shows up in copied protocol calendar, if library calendar had null.
		desc = ((desc  == null) || (desc.trim().equals("null")) )?"":(  desc ) ;

				pullDown=EJBUtil.createPullDown("caltype", calendarType, ids, names);
	if(!calStatus.equals("") && calStatus.equals("A")){
					makeReadOnly = "readonly";
					inputClass = "inpGrey";
					makeGrey = "Grey";
					makeDisabled ="disabled";
					ftextGrey = "textGrey";
	}
%>

<DIV class="BrowserTopn" id="div1">
<%
   if(mode.equals("N")) {
   tSession.setAttribute("newduration", "");
%>
<%
   } else if(mode.equals("M")) {
%>
<%
   }
%>

<jsp:include page="protocoltabs.jsp" flush="true"/>
</div>
<DIV class="BrowserBotN BrowserBotN_CL_1" id="div2">

<% if (pageRight == 4) {%>
 <P class = "defComments"><FONT class="Mandatory"><%=MC.M_YouViewRgt_ChgSvd%><%-- You have only View rights. Any changes made will not be saved.*****--%></Font></P>
<%}%>

  <%
	if (pageRight > 0)	{

%>

<% //if ((calledFrom.equals("S")) && (mode.equals("M"))) {
/*}
else {
	if (pageRight > 4) {*/%>
   <!--<A href="copyprotocol.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&from=initial&calledFrom=<%=calledFrom%>" onClick="return f_check_perm(<%=pageRight%>,'N')">Copy an Existing Calendar</A>-->
<% /*}
}*/%>

  <Form name="protocolCal" method="post" id="portCalFrm" action="updateprotocol.jsp" onsubmit="if (validate(document.protocolCal, '<%=mode%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
    <input type="hidden" name="srcmenu" value="<%=src%>">
    <input type="hidden" name="mode" value="<%=mode%>">
    <input type="hidden" name="protocolId" value=<%=protocolId%>>
    <input type="hidden" name="calledFrom" value=<%=calledFrom%>>
    <input type="hidden" name="prevDuration" value=<%=prevDuration%>>
    <input type="hidden" name="prevDurUnitCd" value=<%=prevDurUnitCd%>>
    <input type="hidden" name="actualSts" value=<%=calStatus%>>
    <input type="hidden" name="pageRight" value=<%=pageRight%>>
    <input type="hidden" name="status" value=<%=calStatus%>>
    <input type="hidden" name="calassoc" value=<%=calAssoc%>>
    <input type="hidden" name="durReduced">
	<input type="hidden" name="durUnitCd" value="<%=durationUnitCode%>">
	<% 
	
	//JM: 22Feb2011: #5857
	String calStatDesc = "";
	
	
	SchCodeDao scho = new SchCodeDao();
	
	if (calledFrom.equals("P") || calledFrom.equals("L")) {
		calStatDesc = scho.getCodeDescription(scho.getCodeId("calStatLib",calStatus));	
	}else{
		calStatDesc = scho.getCodeDescription(scho.getCodeId("calStatStd",calStatus));	
	}
	
	
	if ((calStatus.equals("A")) || (calStatus.equals("F"))){%>
          <P class = "defComments"><FONT class="Mandatory"><%Object[] arguments1 = {calStatDesc}; %>
	    <%=VelosResourceBundle.getMessageString("M_ChgNotSaved_ForCalStus",arguments1)%><%-- Changes made will not be saved for a Calendar with status <%=calStatDesc%>*****--%> </Font></P>
	<%}%>

    <table width="99%" cellspacing="2" cellpadding="2" border="0" class="basetbl midAlign">
      <tr height="5"><td></td></tr>
      <tr>
        <td width="18%" ><Font class="<%=makeGrey%>"><%=LC.L_Cal_Name%><%-- Calendar Name*****--%></Font> <FONT class="Mandatory" >* </FONT> </td>
        <td width="80%">
			<input class="<%=inputClass%>" type="text" name="protocolName" size = "60" MAXLENGTH = "50" 	value="<%=(protocolName!=null && protocolName!="")?protocolName:""%>" <%=makeReadOnly%> >
        </td>
      </tr>

      <tr>
        <td ><Font class="<%=makeGrey%>"><%=LC.L_Description%><%-- Description*****--%></font></td>
        <td>
          <TextArea class =<%=ftextGrey%>  name="desc" rows=3 cols=45 <%=makeReadOnly%>><%=desc%></TextArea>
        </td>
      </tr>
      <tr>
        <tr>
        <td  ><Font class="<%=makeGrey%>"><%=LC.L_Cal_Type%><%-- Calendar Type*****--%> </Font> <FONT class="Mandatory" >* </FONT> </td>
        <td >
			<%=pullDown%>
        </td>
      </tr>

        <td ><%=LC.L_Cal_Status%><%-- Calendar Status*****--%><FONT class="Mandatory">* </FONT> </td>
        <td>

<%


//JM: 08FEB2011, #D-FIN9
String roleCodePk="";

	//KM-#5882
	if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0){
	ArrayList tId = new ArrayList();

	TeamDao teamDao = new TeamDao();
	teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId));
	tId = teamDao.getTeamIds();

		if (tId != null && tId.size() > 0)
		{
			ArrayList arRoles = new ArrayList();
			arRoles = teamDao.getTeamRoleIds();
	
			if (arRoles != null && arRoles.size() >0 )
			{
				roleCodePk = (String) arRoles.get(0);
	
				if (StringUtil.isEmpty(roleCodePk))
				{
					roleCodePk="";
				}
			}
			else
			{
				roleCodePk ="";
			}
	
		}
		else
		{
			roleCodePk ="";
		}
	} else
	{
	  roleCodePk ="";

	}

	StringBuffer sbCalStat = new StringBuffer();
	int cnt1 = 0;
	String dCalStat = "";
  	SchCodeDao calStatDao = new SchCodeDao();
  	
  	String subTypeVar = "";
  	String cdDescVar = "";	
  	
  //JM: 27JAN2011: Enh-D-FIN9
	if(calledFrom.equals("P") || calledFrom.equals("L")) {
		calStatDao.getCodeValues("calStatLib");
		int len = calStatDao.getCDesc().size();
		
			if (mode.equals("N")) {
				sbCalStat.append("<select  name=\"calStatus\" >");
				for (cnt1 = 0; cnt1 <= len-1 ; cnt1++){
					sbCalStat.append("<OPTION value="+ calStatDao.getCSubType().get(cnt1)+">" + calStatDao.getCDesc().get(cnt1)+ "</OPTION>");
				}
				sbCalStat.append("</SELECT>");

	   	 	} else { //mode check
			   sbCalStat.append("<select  name=\"calStatus\" >");
				for (cnt1 = 0; cnt1 <= len-1 ; cnt1++){
					
					subTypeVar = (String)calStatDao.getCSubType().get(cnt1);
					cdDescVar = (String)calStatDao.getCDesc().get(cnt1);
					
					if ((!(calStatus ==null)) && (calStatus.equals("F"))) {
						if (subTypeVar.equals("F")){ // YK DFIN9 Bugs #5880
							sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
						}
					} //KM -#5879
					else if (!(calStatus ==null)) {
						if(subTypeVar.equals(calStatus))
							sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
						else
							sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
					}
			  	}
				sbCalStat.append("</SELECT>");
		   } //end of mode check

	} else { //calledFrom check	

		 //JM: 08FEB2011, #D-FIN9: Study team role based calendar status access
		calStatDao.getCodeValuesForStudyRole("calStatStd",roleCodePk);
		int len1 = calStatDao.getCDesc().size();


		if (mode.equals("N")) {
		  	sbCalStat.append("<select  name=\"calStatus\" id=\"calstatus\" onchange=\"return(fSetEsignVisibility(document.protocolCal))\">");
			for (cnt1 = 0; cnt1 <= len1 -1 ; cnt1++)
			{
				if (!(calStatDao.getCSubType().get(cnt1)).equals("O")){
					sbCalStat.append("<OPTION value="+ calStatDao.getCSubType().get(cnt1)+">" + calStatDao.getCDesc().get(cnt1)+ "</OPTION>");
				}
			}
			sbCalStat.append("</SELECT>");

		} else { //mode check

			sbCalStat.append("<select  name=\"calStatus\" id=\"calstatus\" onchange=\"return(fSetEsignVisibility(document.protocolCal))\">");

			//KM-#5925
			sbCalStat.append("<OPTION value ='' SELECTED>Select an Option</OPTION>");
			for (cnt1 = 0; cnt1 <= len1 -1 ; cnt1++){
				
				subTypeVar = (String)calStatDao.getCSubType().get(cnt1);
				cdDescVar = (String)calStatDao.getCDesc().get(cnt1);

				if ((!(calStatus ==null)) && (calStatus.equals("W"))) {
					if (!subTypeVar.equals("O")){
						if (subTypeVar.equals("W")){
							sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
						}else{
							sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
						}
					}
				} else if ((!(calStatus ==null)) && (calStatus.equals("A"))) {
					if (!subTypeVar.equals("W")){
						if (!subTypeVar.equals("O")){
							if (subTypeVar.equals("A")){
								sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
							}else if (subTypeVar.equals("D")){ // YK DFIN9 Bugs #5875
								sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
							}
						}else if (calAssoc.equals("P") && subTypeVar.equals("O")){
							sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
						}
					}
				} else if ((!(calStatus ==null)) && (calStatus.equals("O")) ) {
					if (!subTypeVar.equals("W")){
						if (!subTypeVar.equals("O") && (subTypeVar.equals("D") || subTypeVar.equals("A"))){// YK DFIN9 Bugs #5876
							sbCalStat.append("<OPTION value="+ subTypeVar+">" + cdDescVar+ "</OPTION>");
						}else if (calAssoc.equals("P") && subTypeVar.equals("O")){
							sbCalStat.append("<OPTION value="+ subTypeVar+" Selected>" + cdDescVar+ "</OPTION>");
						}
					}
				} else if ((!(calStatus ==null)) && (calStatus.equals("D"))) {
					if (subTypeVar.equals("D")){// YK DFIN9 Bugs #5877 & #5881
							sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
					}
				} else{					
					if (!subTypeVar.equals("O")){
						if(calStatus.equals(subTypeVar)) // YK DFIN9 Bugs #5878 & #5881
						{
							sbCalStat.append("<OPTION value="+ subTypeVar+" selected>" + cdDescVar+ "</OPTION>");
						}else
						{
							sbCalStat.append("<OPTION value="+ subTypeVar+" >" + cdDescVar+ "</OPTION>");
						}
					}
				}
			}
			sbCalStat.append("</SELECT>");
		} //end of mode check
	} //end of calledFrom
	dCalStat = sbCalStat.toString();
%><%=dCalStat%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="calendarstatusmodes.htm" target="Information" onClick="openwin()"><%=MC.M_What_ThisMean%><%-- What does this mean?*****--%></A>
	</td>

      </tr>
      </tr>
	<tr>
	<td>
	</td>
	</tr>
	<tr height=5>
	 <td></td>
	</tr>

      <tr>
        <td>
          <input type="hidden" name="protocolId" MAXLENGTH = 15 value="<%=protocolId%>">
        </td>
      </tr>
      <tr>
        <td ><Font class="<%=makeGrey%>"><%=LC.L_Cal_Duration%><%-- Calendar Duration*****--%></font> <FONT class="Mandatory">* </FONT> </td>
        <td>
          <input class ="<%=inputClass%>" type="text" name="durNum" size = 10 MAXLENGTH = 3 value="<%=durationNum%>" <%=makeReadOnly%>>
	  <select  onChange="return  setDurationCodeEditBox(document.protocolCal)" name="durUnit" <%=makeDisabled%> >

<%
if ((durationUnitCode == null) || (durationUnitCode.equals("")))
	durationUnitCode = "D"; //SV, 9/15, default is day
if (durationUnitCode.equals("D")) { %>
		<option value="day" SELECTED><%=LC.L_Days%><%--Days*****--%></option>
		<option value="week"><%=LC.L_Weeks%><%--Weeks*****--%></option>
		<option value="month"><%=LC.L_Months%><%--Months*****--%></option>
		<option value="year"><%=LC.L_Years%><%--Years*****--%></option>
<% } else if (durationUnitCode.equals("W")) { %>
		<option value="day"><%=LC.L_Days%><%--Days*****--%></option>
		<option value="week" SELECTED><%=LC.L_Weeks%><%--Weeks*****--%></option>
		<option value="month"><%=LC.L_Months%><%--Months*****--%></option>
		<option value="year"><%=LC.L_Years%><%--Years*****--%></option>
<% } else if (durationUnitCode.equals("M")) { %>
		<option value="day"><%=LC.L_Days%><%--Days*****--%></option>
		<option value="week"><%=LC.L_Weeks%><%--Weeks*****--%></option>
		<option value="month" SELECTED><%=LC.L_Months%><%--Months*****--%></option>
		<option value="year"><%=LC.L_Years%><%--Years*****--%></option>
<%}	else if (durationUnitCode.equals("Y")) { %>
		<option value="day"><%=LC.L_Days%><%--Days*****--%></option>
		<option value="week"><%=LC.L_Weeks%><%--Weeks*****--%></option>
		<option value="month"><%=LC.L_Months%><%--Months*****--%></option>
		<option value="year" SELECTED><%=LC.L_Years%><%--Years*****--%></option>
<%}%>

	  </select>
	</td>
	</tr>
	<tr>
		<%  if (calledFrom.equals("S") && calAssoc.equals("P") ) {

		BudgetDao bgtDao = new BudgetDao();
		int budgetId = 0;
		budgetId = bgtDao.getDefaultBudgetForCalendar(EJBUtil.stringToNum(protocolId));

		String templateId = "";
		String templateDesc="";
		String templatePullDown = "";

		if (budgetId == 0 || EJBUtil.stringToNum(bgtTemplate) == 0){
			boolean boolVal = bgtDao.getSavedBgtTemplates(EJBUtil.stringToNum(userId));

			ArrayList arrFlgSeparatedIds = bgtDao.getBgtIdWithFlgs();
			ArrayList arrTemplateDesc = bgtDao.getBgtDescs();

			if (!bgtTemplate.equals("0")){
				if (arrFlgSeparatedIds.size() > 0){
					if (arrFlgSeparatedIds.indexOf(bgtTemplate)>0){
						if (!EJBUtil.isEmpty(bgtTemplate)){
							templateId= bgtTemplate;
							templateDesc = arrTemplateDesc.get(arrFlgSeparatedIds.indexOf(bgtTemplate)).toString();
						}
					}
				}
			}else{
				String arrayElement="";

				for (int indx=0; indx < arrTemplateDesc.size(); indx++){
					arrayElement = arrTemplateDesc.get(indx).toString();

					if (arrayElement.length() > 47) {
						arrayElement= arrayElement.substring(0,47)+"...";
						arrTemplateDesc.set(indx, arrayElement);
					}
				}
				templatePullDown =  EJBUtil.createPullDownWithStr("budgetTemplate","", arrFlgSeparatedIds,arrTemplateDesc);
			}
		} else{
			bgtDao.resetObject();
			bgtDao.getBudgetInfo(EJBUtil.stringToNum(bgtTemplate));

			ArrayList arrTemplateName = bgtDao.getBgtNames();

			templateId= bgtTemplate;
			templateDesc = arrTemplateName.get(0).toString();

		}
		%>
			<td ><%=LC.L_Bgt_Template%><%-- Budget Template*****--%></td>

			<%if (EJBUtil.isEmpty(templateId)){ %>
			<td>
			  <%=templatePullDown%>
			</td>
			<%} else { //mode check %>
				<td><FONT color='red'><i><%=templateDesc%></i></FONT><input type="hidden" name="bgtTemplateTxt" value="<%=templateId%>"/></td>
			<% }%>
		<%} %>
	</tr>
		  <tr height="5"><td></td></tr>
    </table>

  	<!--sharewith-->

  	<%if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {%>

   	 <jsp:include page="objectsharewith.jsp" flush="true">
   	 <jsp:param name="objNumber" value="3"/>
   	 <jsp:param name="mode" value="<%=mode%>"/>
   	 <jsp:param name="formobject" value="document.protocolCal"/>
   	 <jsp:param name="formnamevalue" value="protocolCal"/>
   	 <jsp:param name="fkObj" value="<%=protocolId%>"/>
   	 <jsp:param name="sharedWith" value="<%=sharedWith%>"/>
	 </jsp:include>
<!--end sharewith-->
	<br>
	<%} //calledFrom L%>

    	<div id="eSignDiv" name="eSignDiv">


	<%
	if (mode.equals("M")){

	if (   !  (    (calStatus.equals("F")    )   )   &&  (pageRight >= 6)  )
	{

%>
	<!--//JM: 22May2008, added for, issue #3492 -->

<table width="500" cellspacing="0" cellpadding="0">
 	<%	if (calStatus.equals("O") && calAssoc.equals("P"))
		{ %>


				<br>
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_Hide_UnhideSch%><%-- Hide and Unhide feature will work only for the New Schedules.*****--%></Font></P>


				<tr>
				<td colspan=6>
				<input type="radio" name="patSchChk" value="1"><%=MC.M_ToAllPatSch_OnCal%><%-- Apply to all <%=LC.Pat_Patient%> Schedules based on this Calendar*****--%>
				</td>
				</tr>

				<tr>
				<td colspan=6>
				<input type="radio" name="patSchChk" value="2"><%=MC.M_ApplyCurPatSch_IgnorSch%><%-- Apply only to 'Current' <%=LC.Pat_Patient%> Schedules based on this Calendar(ignores Discontinued Schedules)*****--%>
				</td>
				</tr>


				<tr>
				<td colspan=5>
				<input type="radio" name="patSchChk" value="3" ><%=MC.M_ApplyCurPatSch_WithStat%><%-- Apply only to 'Current' <%=LC.Pat_Patient%> Schedules based on this Calendar for <%=LC.Pat_Patients%> with a Status of*****--%>
				</td>
				</tr>
				<tr>
				<td width="45%"><%=dEvStat%>
				<td width="15%"><%=LC.L_On_OrAfter%><%-- on or after*****--%></td>
<%-- INF-20084 Datepicker-- AGodara --%>
 				<td colspan=5><input type="text" name="dateTxt" class="datefield" size="15" READONLY></td>
				</tr>
		<% } else
		{
			%>
				<input type="hidden" name="patSchChk" value="0">
				<input type="hidden" name="patSchChk" value="0">
				<input type="hidden" name="patSchChk" value="0">

			<%
		}%>
</table>

<!--//JM: 22May2008, added for, issue #3492 -->

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="portCalFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


	<%}
	}// if for mode
	else{
	   if (pageRight>=5){%>

<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y"/>
	<jsp:param name="formID" value="portCalFrm"/>
	<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	<%}
	}	// end of else for mode %>
	</div>


<%  if(mode.equals("N")) { %>
<table width=100% border=0>
		<tr>
		<td> <P class="defComments">
			<FONT class = "Mandatory"><%=MC.M_CreateNewCal_ClkSbmt%><%-- You are currently creating a new calendar. It will be created by clicking on the Submit button. Any subsequent clicks on the Submit button will create another calendar. In case any modifications are required after clicking on Submit, please make use of the calendar library instead of the Back button.*****--%></FONT>
			</P>
		</td>
		</tr>
</table>
<%}%>




    <br>
<table width="100%" cellspacing="0" cellpadding="0">
     <!-- <td align="right">
		<button type="submit"><%=LC.L_Submit%></button>
      </td>-->
     <!-- <td align=center>
        <%--SV, 10/27, added "P" --%>
	  <% if (calledFrom.equals("L")) { %>
		<A href="protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&selectedTab=1&calledFrom=L" type="submit"><%=LC.L_Back%></A>
	  <% } else if (calledFrom.equals("P")) { %>
		<A href="protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&selectedTab=1&calledFrom=P" type="submit"><%=LC.L_Back%></A>
  	  <% } else if (calledFrom.equals("S")){
  	      if (calAssoc.equals("S")){%>
 	      <A href="studyadmincal.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=10" type="submit"><%=LC.L_Back%></A>
  	  <% } else {%>
  	  <A href="studyprotocols.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=7" type="submit"><%=LC.L_Back%></A>
		<%} %>
	  <% } else {%>
			<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
	  <% } %>
      </td>
-->

      </tr>
  </table>
	   	<SCRIPT> fSetEsignVisibility(document.protocolCal);</SCRIPT>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

  </Form>
</DIV>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

<div class ="mainMenu" id = "emenu">
<!--<jsp:include page="getmenu.jsp" flush="true"/>--></div>
</body>

</html>


