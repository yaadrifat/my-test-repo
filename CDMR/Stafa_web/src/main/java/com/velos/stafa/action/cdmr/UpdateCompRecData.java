package com.velos.stafa.action.cdmr;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     UpdateCompRecData
 * @description Update Comprec Table for Group(Service Section) Code
 * @version     1.0
 * @file        UpdateCompRecData.java
 * @author      Lalit Chattar
 */
public class UpdateCompRecData extends StafaBaseAction{
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	/*
	 * Parameter getting from request
	 */
	private Long pkid;
	private String effDate;
	private String grupCode;
	private String pkString;
	private String overwrite;
	private Integer fileid;
	
	/*
	 * Constructor
	 */
	public UpdateCompRecData(){
		cdmrController = new CDMRController();
	}
	
	/*
	 * getter and setter
	 */
	
	public Long getPkid() {
		return pkid;
	}
	public void setPkid(Long pkid) {
		this.pkid = pkid;
	}
	public String getEffDate() {
		return effDate;
	}
	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}
	public String getGrupCode() {
		return grupCode;
	}
	public void setGrupCode(String grupCode) {
		this.grupCode = grupCode;
	}
	public String getPkString() {
		return pkString;
	}

	public void setPkString(String pkString) {
		this.pkString = pkString;
	}
	public String getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(String overwrite) {
		this.overwrite = overwrite;
	}
	
	public Integer getFileid() {
		return fileid;
	}

	public void setFileid(Integer fileid) {
		this.fileid = fileid;
	}
		
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		update();
		return SUCCESS;
	}
	
	/**
	 * @description Update Group Code
	 * @return String
	 * @throws Exception
	 */
	public String update() throws Exception{
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			
			String query = "update StagingCompRec set compCol14 = '"+jsonObject.getString("effDate")+"'" +
					" where pkStagingComRec = " + jsonObject.getString("pkid");
			
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			
			jsonObject = cdmrController.updateComrec(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}
	
	/**
	 * @description Update Selected Group Code
	 * @return String
	 * @throws Exception
	 */
	public String updateSelected() throws Exception{
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String pkStr = jsonObject.getString("pkString");
			pkStr = pkStr.substring(0, pkStr.length() - 1);
			if(jsonObject.getString("overwrite").equals("true")){
				String query = "update cdm_stagingcomprec set COMP_COL14 = '"+jsonObject.getString("effDate")+"' where PK_CDM_STAGINGCOMPREC in ("+pkStr+")";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateComrec(jsonObject);
			}else{
				String query = "update cdm_stagingcomprec set COMP_COL14 = CASE WHEN COMP_COL14 = ' ' THEN '"+jsonObject.getString("effDate")+"' ELSE COMP_COL14 END where PK_CDM_STAGINGCOMPREC in ("+pkStr+")";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateComrec(jsonObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}

	public String updateAll() throws Exception{
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			if(jsonObject.getString("overwrite").equals("true")){
				String query = "update cdm_stagingcomprec set COMP_COL14 = '"+jsonObject.getString("effDate")+"' where FK_CDM_STAGINGMAIN = (select PK_CDM_STAGINGMAIN from CDM_STAGINGMAIN where FK_CDM_FILESLIB = "+jsonObject.getString("fileid")+")";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateComrec(jsonObject);
			}else{
				String query = "update cdm_stagingcomprec set COMP_COL14 = CASE WHEN COMP_COL14 = ' ' THEN '"+jsonObject.getString("effDate")+"' ELSE COMP_COL14 END where FK_CDM_STAGINGMAIN = (select PK_CDM_STAGINGMAIN from CDM_STAGINGMAIN where FK_CDM_FILESLIB = "+jsonObject.getString("fileid")+")";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateComrec(jsonObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}
	
}