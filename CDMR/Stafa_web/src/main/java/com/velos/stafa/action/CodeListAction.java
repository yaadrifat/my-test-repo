/**
 * 
 */
package com.velos.stafa.action;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.validator.annotations.RegexFieldValidator;
import com.velos.stafa.business.util.WebserviceUtil;
import com.velos.stafa.controller.CodeListController;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.objects.UserDetailsObject;
import com.velos.stafa.util.StafaConfig;
import com.velos.stafa.util.VelosStafaConstants;
import com.velos.stafa.util.VelosUtil;

/**
 * @author akajeera
 *
 */
public class CodeListAction extends StafaBaseAction{
	
	public static final Log log=LogFactory.getLog(CodeListAction.class);
	
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	//JSONObject maintreeObject = new JSONObject();
	
	public List mainTreelst;
	public List qcmaintlst;
	public List reagentlst;
	//public List protocollst;
	public List exisQclst;
	public List exisReagentlst;
	public List calculationlst;
	public List protocolFields;
	public List protocolList;
	public List immnoPhenoList;
	private List maintenanceList;
	public List dropDownList;
	public Long codelstId;
	public List childDetails;
	public List codeListValueList;
	public List moduleList;
	public List qcResults;
	private String qcHidden;
	public String returnMessage;
	private String hiddenSubType;
	private String parentCode;
	private String orgName;
	private String orgID;
	private String fieldType;
	HttpSession session = null;
	UserDetailsObject userObject = new UserDetailsObject();
	private List lstOrganizations= new ArrayList();
	private List lstDatafieldOrganizations= new ArrayList();
	public static final String  SOAPHEADERSTARTTAGS="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\"> <soapenv:Header/><soapenv:Body>";
	public static final String SOAPHEADERCLOSETAGES="</soapenv:Body></soapenv:Envelope>";
	CodeListController codeListController = new CodeListController();
	
	//Save Qc Result
	public List fetchOrganizationByLevel() throws Exception{
		
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			String esecurityURL = resource.getString(ESECURITY_URL);
			session=request.getSession(false);
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			
			//System.out.println("eSecurity URL :::::::::::::  " + esecurityURL + " / " + userObject.getCurrentOrgId());
			StringBuffer xmlIp = new StringBuffer();
			xmlIp.append(SOAPHEADERSTARTTAGS);
			xmlIp.append("<ser:fetchOrganizationByLevel> <organizationCode></organizationCode>");
			//xmlIp.append("<organizationId>"+userObject.getCurrentOrgId()+"</organizationId></ser:fetchOrganizationByLevel>");
			xmlIp.append(SOAPHEADERCLOSETAGES);;
			StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
			String[] stringNodes = {"organizationCode","organizationName","organization"};
			lstOrganizations = WebserviceUtil.getArrayToList(result,"userOrganization",stringNodes);
		}catch(Exception e){
			System.out.println("Exception in getUsersByRole" + e);
			e.printStackTrace();
		}
		
		return lstOrganizations;
	}
	public String saveQcresult()throws Exception{
		log.debug("starts");

		JSONObject jsonDataObject = new JSONObject();  
		  
		try{
			String query;
			String jsonData = request.getParameter("jsonData");
			//System.out.println("jsonData" + jsonData);	
			
			JSONObject jsonObject = new JSONObject(jsonData);
			JSONArray dataArray =jsonObject.getJSONArray("qcResultData");
			JSONArray subDomains = new JSONArray();
			JSONObject subObject = new JSONObject();
			subObject = new JSONObject();
			subObject.put("domain", QCRESULT_DOMAIN);
			subObject.put("domainKey", QCRESULT_ID);
			subObject.put("data", dataArray);
			subDomains.put(subObject);
			//----------------
			jsonDataObject.put("subDomains", subDomains);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			//System.out.println(" final data --------------------");
			log.trace( "finalData"+jsonDataObject.toString());
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	
	}
	public String saveProtocolCalculation() throws Exception {
		log.debug("starts");
		 // ======================================== 
		JSONObject jsonDataObject = new JSONObject();  
		  //=========================
		try{
			String query;
			String jsonData = request.getParameter("jsonData");
			  
			
			JSONObject jsonObject = new JSONObject(jsonData);
			
			String protoCol = jsonObject.get("protocol").toString();
			log.trace(" protocol id " + protoCol);
			log.trace(" panelData " + jsonObject.get("panelCalcData").toString()); 
			log.trace(" QCData " + jsonObject.get("qcData").toString()); 
			JSONArray jsonPanelCalcData = jsonObject.getJSONArray("panelCalcData");
			JSONObject qcData = jsonObject.getJSONObject("qcData");
			
			
			log.trace("Length " + jsonPanelCalcData.length());
		/*	
			for(int i=0;i<jsonPanelCalcData.length();i++){
				jsonObject = jsonPanelCalcData.getJSONObject(i);
				//System.out.println( i + " -> " + jsonObject.toString());
			}  
		*/	

			//List queries = new ArrayList();
			JSONArray queries = new JSONArray();
			JSONObject subObject = new JSONObject();
			
			//List subList = new ArrayList();		
			JSONArray subDomains = new JSONArray();
			
			
			// QC values
			String deletedIds = qcData.getString("deletedQC");
			if(deletedIds != null && !deletedIds.trim().equals("")){
				 query = "update " + QC_DOMAIN + " set deletedFlag =1 where fkCodelstProtocol = " + protoCol + " and  fkCodelstQc in("+ deletedIds +")";
				queries.put(query);
				
			}
			
			
		
			
			String addedQC = qcData.getString("addedQC");
			if(addedQC != null && !addedQC.trim().equals("")){
				String[] addedQCids = addedQC.split(",");
				JSONObject tmpObject; 
				JSONArray dataArray = new JSONArray();
				//List tmpList = new ArrayList();		
				for(int i=0;i<addedQCids.length;i++){
					tmpObject = new JSONObject();
					
					tmpObject.put("fkCodelstProtocol", protoCol);
					tmpObject.put("fkCodelstQc", addedQCids[i]);
					//tmpList.add(tmpObject);
					dataArray.put(tmpObject);
				}
				subObject.put("domain", QC_DOMAIN);
				subObject.put("domainKey", "qcId");
				subObject.put("data", dataArray);
				subDomains.put(subObject);
			}
			// protocol
			if(jsonObject.has("isTemplate")){
			String isTemplate = jsonObject.get("isTemplate").toString();
			//String protocolName = jsonObject.get("protocolName").toString();
			query ="update " + DOMAIN_PROTOCOL + " set isTemplate = "+ isTemplate + " where protocol = " + protoCol ;
			queries.put(query);
			}
			// calculation values
			 query = "update " + DOMAIN_PROTOCOLCALC + " set deletedFlag =1 where protocol = " + protoCol ;
			queries.put(query);
			jsonDataObject.put("queries", queries);
			
			subObject = new JSONObject();
			subObject.put("domain", DOMAIN_PROTOCOLCALC);
			subObject.put("domainKey", "pcid");
			subObject.put("data", jsonPanelCalcData);
			//subList.add(subObject);
			subDomains.put(subObject);
			
			//----------------
			jsonDataObject.put("subDomains", subDomains);
			
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			
			//System.out.println(" final data --------------------");
			log.trace( "finaldata"+jsonDataObject.toString());
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
			
		  
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeListAction.saveProtocolCalculation Method ends ********************** ");
		
			return null;
	}
	
	public String fetchChildDetails()throws Exception {
		log.debug("starts");
		String id = request.getParameter("parentId");
		String dominName = "com.velos.stafa.business.domain.CodeList";
		String criteria= " type = (select subType from com.velos.stafa.business.domain.CodeList where pkCodelst="+id + ")";
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Domain", dominName);
		jsonObject.put("criteria", criteria);
		jsonArray.put(0,jsonObject);
		List objectList = CommonController.getObjectList(jsonArray);
		if(objectList !=null && objectList.size()>0){
			childDetails = (List)objectList.get(0);
		}
		log.debug("Ends");
		return SUCCESS;
	}
	
	public String saveCodeList() throws Exception{
		log.debug("starts");
		//System.out.println("CodeListAction.saveCodeList Method Start ");
		JSONObject jsonObject = new JSONObject();
		try{
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject.put("Domain", CODELST_DOMAIN);
			if(jsonObject.has("subType") && jsonObject.getString("subType") != null && !jsonObject.getString("subType").equals("")){
				jsonObject.put("domainKey", CODELST_ID);
			}
			String seqparam = "";
			if( request.getParameter("seqarray") != null){
				seqparam = (String)request.getParameter("seqarray");
			}		
			if(seqparam != null && !seqparam.equals("")){
				String[] seqarr = seqparam.split(";");
				//System.out.println("Action sequence " + seqparam);
				String query = "";
				int index= 0;
				for(int i=0;i<seqarr.length;i++){
					index = seqarr[i].indexOf(":"); 
					if(index != -1){
						query += " update " + CODELST_DOMAIN + " set " + CODELST_SEQUENCE + " = " + seqarr[i].substring(index+1) + " where " + CODELST_ID + " = " + seqarr[i].substring(0,index) + ";";
					}
				}
				log.trace("Action : Query : " + query);
				if(query != null && !query.equals("")){
					jsonObject.put("query", query);
				}
			}	
			jsonObject = CommonController.saveObject(jsonObject);
			if(jsonObject.has(CODELST_ID) && jsonObject.getString(CODELST_ID) != null && !jsonObject.getString(CODELST_ID).equals("")){
				codelstId = jsonObject.getLong(CODELST_ID);
			}	
			if(codelstId != null && codelstId != 0){
				returnMessage = SUCCESS_MESSAGE;
			}
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeListAction.saveCodeList Method End ");
		return SUCCESS;
	}
	
	public String loadMaintenance() throws Exception{
		log.debug("starts");
		
		
		try{
			JSONObject jsonObject1 = new JSONObject();
			String jsonData = request.getParameter("jsonData");
			System.out.println("jsonData--->" + jsonData);	
			//jsonObject1=
			jsonObject1 = new JSONObject(jsonData);
			String type=jsonObject1.getString("type");
			JSONObject jsonObject = new JSONObject();
			System.out.println("CodeListAction : loadMaintenance Method Start-->"+type);
			//String type = request.getParameter("type");
			String criteria = " type= '"+type+"' order by sequence, description";
			
			if(type != null && type.equals(CODELSTTYPE_STAFA)){
				criteria = " isHide = 'N' and " + criteria;
			}
			jsonObject.put("Domain", CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			log.trace("Action : type = " + type);
			if(type != null && (type.equals(CODELSTTYPE_STAFA) || type.equals(CODELSTTYPE_PANEL))){
				//mainTreelst = codeListController.getMaintTreelst(jsonObject);
			}else{
				//mainTreelst = CommonController.getObjectList(jsonObject);
			}
			
		      
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeListAction : loadMaintenance Method End");
		return SUCCESS;
	}
	public String loadMaintainancePopup() throws Exception{
		System.out.println("hidden sub Type----->"+request.getParameter("hiddenSubType"));
		try{
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			String criteria= "";
			String jsonData = request.getParameter("jsonData");
			JSONObject jsonDataObject = new JSONObject(jsonData);
			setHiddenSubType(jsonDataObject.getString("hiddenSubType"));
			setOrgName(jsonDataObject.getString("orgName"));
			setOrgID(jsonDataObject.getString("orgID"));
			setParentCode(jsonDataObject.getString("parentCode"));
			
			//if(jsonDataObject.has("orgID")){
				//if(jsonDataObject.getString("orgID").split(",").length==1){
					System.out.println("jsonData------->->" + jsonData);
					ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
					String esecurityURL = resource.getString(ESECURITY_URL);
					session=request.getSession(false);
					userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
					fetchOrganizationByLevel();
					//System.out.println("eSecurity URL :::::::::::::  " + esecurityURL + " / " + userObject.getCurrentOrgId());
					if(jsonDataObject.getString("hiddenSubType")!=null){
						criteria="select distinct CODELST_FIELDTYPE from er_codelst where codelst_type='"+jsonDataObject.getString("parentCode")+"'and CODELST_SUBTYP='"+jsonDataObject.getString("hiddenSubType")+"'and DELETEDFLAG=0 and CODELST_FIELDTYPE IS NOT NULL";
					}
					jsonObject.put("Query", criteria);
					jsonObject.put("type", "SQL");
					jsonArray.put(0,jsonObject);
					List objectsList = CommonController.getObjectList(jsonArray);
					if(objectsList != null && objectsList.size()>0){
						List tmpList = (List)objectsList.get(0);
						
						if(tmpList!=null && tmpList.size()>0 ){
							fieldType = tmpList.get(0).toString(); 
						}
					}
					System.out.println("fieldType------->"+fieldType);
					/*StringBuffer xmlIp = new StringBuffer();
					xmlIp.append(SOAPHEADERSTARTTAGS);
					xmlIp.append("<ser:fetchOrganizationByLevel> <organizationCode></organizationCode>");
					xmlIp.append("<organizationId>"+jsonDataObject.getString("orgID")+"</organizationId></ser:fetchOrganizationByLevel>");
					xmlIp.append(SOAPHEADERCLOSETAGES);;
					StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
					String[] stringNodes = {"organizationCode","organizationName","organization"};
					lstDatafieldOrganizations = WebserviceUtil.getArrayToList(result,"userOrganization",stringNodes);*/
				//}
			//}
			
			/*if(request.getParameter("hiddenSubType")!=null && !request.getParameter("hiddenSubType").toString().equals(null)){
				System.out.println("-------inside if------>"+request.getParameter("hiddenCode").toString());
				setHiddenSubType(request.getParameter("hiddenSubType").toString());
			}
			if(request.getParameter("orgName")!=null && !request.getParameter("orgName").toString().equals(null)){
				System.out.println("-------inside if------>"+request.getParameter("orgName").toString());
				setOrgName(request.getParameter("orgName").toString());
			}
			if(request.getParameter("orgID")!=null && !request.getParameter("orgID").toString().equals(null)){
				System.out.println("-------inside if------>"+request.getParameter("orgID").toString());
				setOrgID(request.getParameter("orgName").toString());
			}*/
		      
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		System.out.println("CodeListAction : loadMaintainancePopup Method End");
		return SUCCESS;
	}

	public String loadQCMaintenance() throws Exception{
		
		log.debug("starts");
		qcHidden=request.getParameter("qcHidden");
		System.out.println("qcHidden"+qcHidden);
		session=request.getSession(false);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		fetchOrganizationByLevel();
		try{
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			
			
			//SubHeader drop list 
			/*String criteria = " isHide = 'N' and type= '"+CODELSTTYPE_SUBHEADER+"'  "+ VelosUtil.constructOrganizationCriteria(request, "HSQL_CURRENT_ORG") +" order by sequence, description";
			jsonObject.put("Domain", CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(0,jsonObject);
			*/
			//QC
			/*jsonObject = new JSONObject();
			//criteria = " isHide = 'N' and type = '" + CODELSTTYPE_QC +"'  order by sequence, description";
			criteria = " type = '" + CODELSTTYPE_QC +"'  "+ VelosUtil.constructOrganizationCriteria(request, "HSQL") +"  order by sequence, description";
			jsonObject.put("Domain", CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(1,jsonObject);
			
			//Reagents and Supplies
			jsonObject = new JSONObject();
			criteria = " isHide = 'N' and type = '" + CODELSTTYPE_REAGENTSUPPLIER +"'  "+ VelosUtil.constructOrganizationCriteria(request, "HSQL_CURRENT_ORG") +" order by sequence, description";
			jsonObject.put("Domain", CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(2,jsonObject);
			*/
			//Protocol - BB: not in use 
			
			/*//protocol list
			jsonObject = new JSONObject();
			 criteria = " deletedFlag =0  "+ VelosUtil.constructOrganizationCriteria(request, "HSQL_CURRENT_ORG") +" order by protocolName ";
			jsonObject.put("Domain", DOMAIN_PROTOCOL);
			jsonObject.put("criteria", criteria);
			jsonArray.put(3,jsonObject);
			
			// Calculation fields 
			jsonObject = new JSONObject();
			// BB get all values 
			//criteria = " isHide = 'N' and type = '" + CODELSTTYPE_CALCULATION +"'  order by sequence, description";
			criteria = " type in(select subType from " + CODELST_DOMAIN +" where type = 'stafa' and isHide = 'N' and deletedFlag =0 ) and isHide = 'N' and deletedFlag =0  "+ VelosUtil.constructOrganizationCriteria(request, "HSQL_CURRENT_ORG") +" order by sequence, description";
			
			jsonObject.put("Domain", CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(4,jsonObject);*/
			
			// Dropdown values
			jsonObject = new JSONObject();
	//BB working include calfields
			/*
			criteria = " select PK_CODELST id,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC des,trim(lower(CODELST_DESC)) ord,  level seq from( select PK_CODELST,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC from ER_CODELST  where  DELETEDFLAG = 0 and CODELST_HIDE='N') start with CODELST_TYPE = 'stafa' "+
				" connect by prior CODELST_SUBTYP= CODELST_TYPE and level =2 " +
				" union " +
				" select PK_CODELST id,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC des,trim(lower(CODELST_DESC)) ord, 2 seq from ER_CODELST  where  codelst_type ='calclist' and CODELST_HIDE='N' and  DELETEDFLAG = 0 " +
				" order by seq,ord ";
			*/

			/*criteria = " select PK_CODELST id,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC des,trim(lower(CODELST_DESC)) ord,  level seq from( select PK_CODELST,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE from ER_CODELST  where  DELETEDFLAG = 0 and CODELST_HIDE='N') start with CODELST_TYPE = 'stafa' "+
				" connect by prior CODELST_SUBTYP= CODELST_TYPE and level =2 and CODELST_HIDE = 'N' order by seq,ord" ;
			
			jsonObject.put("Query", criteria);
			jsonObject.put("type", "SQL");
			jsonArray.put(5,jsonObject);
			
			jsonObject = new JSONObject();
			criteria = " isHide = 'N' and type = '" + CODELSTTYPE_MODULE +"'  order by sequence, description";
			jsonObject.put("Domain", CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(6,jsonObject);*/
			
			List objectslst = CommonController.getObjectList(jsonArray);
			mainTreelst = (List)objectslst.get(0);
			qcmaintlst = (List)objectslst.get(1);
			reagentlst = (List)objectslst.get(2);
			
			//BB get all values for calculation 
			calculationlst = (List)objectslst.get(4);
			// need to check calculationlst = (List)objectslst.get(5);
			protocolList = (List)objectslst.get(3);
			dropDownList = (List)objectslst.get(5);
			moduleList = (List)objectslst.get(6);
			//System.out.println("drop wonl list " +dropDownList );
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		System.out.println("qcHidden====>"+qcHidden);
		return SUCCESS;
	}
	public String saveQcDetails() throws Exception{
		log.debug("starts");
		//System.out.println("CodeList : saveQcDetails Method Start");
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		JSONObject subJSONObject = new JSONObject();
		int index = 0;
		int reagentindex = 0;
		try{
			String protocol = request.getParameter("fkCodelstProtocol");
			String subheader = request.getParameter("fkCodelstSubheader");
			//Protocol 
			jsonObject = new JSONObject();
			if(protocol != null && subheader != null && !protocol.equals("") && !subheader.equals("")){
				jsonObject.put("Domain", QC_DOMAIN);
				jsonObject.put("query", "delete from " + QC_DOMAIN + " where fkCodelstProtocol = " + protocol + " and fkCodelstSubheader = " + subheader);
				jsonArray.put(0,jsonObject);
				
				String tempqc = request.getParameter("tempqc");
				String[] qcarr = tempqc.split(",");
				for(int i=0;i<qcarr.length;i++){
					if(qcarr[i] != null && !qcarr[i].equals("")){
						jsonObject = new JSONObject();
						jsonObject = VelosUtil.constructJSON(request);
						jsonObject.put("Domain", QC_DOMAIN);
						jsonObject.put("domainKey", QC_ID);
						if(jsonObject.has("fkCodelstQc")){
							jsonObject.remove("fkCodelstQc");
						}
						jsonObject.put("fkCodelstQc", qcarr[i]);
						
						jsonArray.put(i+1,jsonObject);
					}
				}
				
			}else{

				String tempqcreagent = request.getParameter("tempqcreagent");
				String[] qcreagentarr = tempqcreagent.split(";");

				//Reagents and suppliers
				//System.out.println("Action : " + tempqcreagent);
				for(int j=0;j<qcreagentarr.length;j++){
					jsonObject = new JSONObject();
					index = qcreagentarr[j].indexOf("::");
					String qcValue = qcreagentarr[j].substring(0,index);
					qcreagentarr[j] = qcreagentarr[j].substring(index+2);
					String[] reagentquantity = qcreagentarr[j].split(",");
					log.trace("Qc Value  " + qcreagentarr );
					jsonObject.put("Domain", REAGENTSUPPLIER_DOMAIN);
					jsonObject.put("query", "delete from " + REAGENTSUPPLIER_DOMAIN + " where qcId = " + qcValue);
					jsonArray.put(0,jsonObject);
					//System.out.println(" ============== " + qcreagentarr[j]);
					for(int k=0;k<reagentquantity.length;k++){
						jsonObject = new JSONObject();
						jsonObject = VelosUtil.setUserDetails(jsonObject, request);
						jsonObject.put("Domain", REAGENTSUPPLIER_DOMAIN);
						jsonObject.put("domainKey", REAGENTSUPPLIERS_ID);
						jsonObject.put("reagentSuppliersId","");
						jsonObject.put("qcId", qcValue);
						reagentindex = reagentquantity[k].indexOf(":");
						if(reagentindex != -1){
							jsonObject.put("fkCodelstReagent",reagentquantity[k].substring(0,reagentindex));
							jsonObject.put("qunatity",reagentquantity[k].substring(reagentindex+1));
						}	
						//System.out.println("json Object " + jsonObject+"/"+k);
						jsonArray.put(k+1,jsonObject);
					}	
				}
					
			}
			
			
			
			//jsonArray.put(0,jsonObject);
			
			//jsonObject.put("Domain", REAGENTSUPPLIER_DOMAIN);
			//jsonObject.put("domainKey", REAGENTSUPPLIERS_ID);
			//jsonArray.put(1,jsonObject);
			
			jsonArray = CommonController.saveObject(jsonArray);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeList : saveQcDetails Method End");
		return loadQCMaintenance();
	}
	
	public String loadQcDetails() throws Exception{
		log.debug("starts");
		//System.out.println("CodeList : loadQcDetails method Start");
		try{
	//	String protocol = request.getParameter("fkCodelstProtocol");
	//	String subheader = request.getParameter("fkCodelstSubheader");
		String qc = request.getParameter("qcpkCodelst");

		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		
		
		String criteria = "";
	//	System.out.println("Protocol : " + protocol + " Qc " + qc);
		if(qc != null && !qc.equals("")){ 	
			//SubHeader drop list 
			criteria = " qcId= "+qc;
			jsonObject.put("Domain", REAGENTSUPPLIER_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(0,jsonObject);
		}
		/*
		else if(protocol != null && !protocol.equals("")){
			criteria = " fkCodelstProtocol = "+protocol;
			jsonObject.put("Domain", QC_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(0,jsonObject);
			// BB
			jsonObject = new JSONObject();
			criteria = " protocol = "+protocol;
			jsonObject.put("Domain", DOMAIN_PROTOCOLCALC);
			jsonObject.put("criteria", criteria);
			jsonArray.put(1,jsonObject);
			
			
		}
		*/
		List objectslst = CommonController.getObjectList(jsonArray);
		if(qc != null && !qc.equals("")){
			exisReagentlst = (List)objectslst.get(0);
		}
		/*else if(protocol != null && !protocol.equals("")){
			exisQclst = (List)objectslst.get(0);
			protocolFields=(List)objectslst.get(1);
		}	
		*/
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeList : loadQcDetails method End");
		return SUCCESS;
	}
	
	
		
	public String saveImmnoDetails() throws Exception{
		log.debug("starts");
		//System.out.println("CodeListAction : saveImmnoDetails Method Start");
		JSONObject jsonObject = new JSONObject();
		try{
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject.put("Domain", IMMNOPHENOTYPING_DOMAIN);
			jsonObject.put("domainKey", IMMNOPHENOTYPING_ID);
			jsonObject = CommonController.saveObject(jsonObject);
			codelstId = jsonObject.getLong(IMMNOPHENOTYPING_ID);
			if(codelstId != null && codelstId != 0){
				returnMessage = SUCCESS_MESSAGE;
			}
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeListAction : saveImmnoDetails Method End");
		return SUCCESS;
	}
	
	public String saveImmnoPhenoTyping() throws Exception{
		log.debug("starts");
		//System.out.println("CodeListAction : saveImmnoPhenoTyping Method Start");

		JSONObject jsonDataObject = new JSONObject();  
		try{
			String query;
			String jsonData = request.getParameter("jsonData");
			//System.out.println("jsonData" + jsonData);	
			
			JSONObject jsonObject = new JSONObject(jsonData);
			JSONArray dataArray =jsonObject.getJSONArray("protocolData");
			JSONArray subDomains = new JSONArray();
			JSONObject subObject = new JSONObject();
			subObject = new JSONObject();
			subObject.put("domain", IMMNOPHENOTYPING_DOMAIN);
			subObject.put("domainKey", IMMNOPHENOTYPING_ID);
			subObject.put("data", dataArray);
			subDomains.put(subObject);
			//----------------
			jsonDataObject.put("subDomains", subDomains);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			//System.out.println(" final data --------------------");
			log.trace("finaldata"+ jsonDataObject.toString());
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeListAction : saveImmnoPhenoTyping Method End");
		return SUCCESS;
	
	}
	
	public String deleteImmunoPhenoTyping()throws Exception{
		log.debug("starts");
		try{
			//System.out.println(" deleteImmunoPhenoTyping");
			JSONObject jsonDataObject = new JSONObject(); 
			JSONArray queries = new JSONArray();
			String jsonData = request.getParameter("jsonData");
			System.out.println("jsonData" + jsonData);	
			
			JSONObject jsonObject = new JSONObject(jsonData);
			
			String deletedIds = jsonObject.getString("deletedImmunos");
			String query;
			if(deletedIds != null && !deletedIds.trim().equals("")){
				 query = "update " + IMMNOPHENOTYPING_DOMAIN + " set deletedFlag =1 where immnopheId in("+ deletedIds +")";
				queries.put(query);
				
			}
			jsonDataObject.put("queries", queries);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		
		return SUCCESS;
	
	}
	
	public String loadImmnophetyping()throws Exception{
		log.debug("starts");
		try{
			JSONObject jsonObject = new JSONObject();
			String subImmnoId = request.getParameter("subImmnoId");
			String criteria = "";
			if(subImmnoId != null && !subImmnoId.equals("")){
				criteria = " subImmnopheId = "+subImmnoId;
			}else{
				criteria = " isHeader=1 ";
			}
			log.trace("Criteria : " + criteria);
			jsonObject.put("Domain", IMMNOPHENOTYPING_DOMAIN);
			jsonObject.put("criteria", criteria);
			mainTreelst = CommonController.getObjectList(jsonObject);
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}
	

	public String fetchImmnoPhenoData()throws Exception{
		log.debug("starts");
		//System.out.println("---------*****************--------------fetchImmnoPhenoData");
		try{
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request); 
			CommonController commonController = new CommonController();
			/*String criteria = "select PK_IMMNOPHENOTYPING, IMMNO_PARENTCODE,IMMNO_CODE,IMMNO_DESC,  level " +
					" from( select PK_IMMNOPHENOTYPING, IMMNO_PARENTCODE,IMMNO_CODE,IMMNO_DESC from ER_IMMNOPHENOTYPING where deletedflag=0) start with IMMNO_PARENTCODE = 'stafa'  " +
					" connect by prior IMMNO_CODE= IMMNO_PARENTCODE order by level desc,trim(lower(IMMNO_DESC))";
					*/
			String param = "";
			if(request.getParameter("immuno")!=null && !request.getParameter("immuno").equals("")){
				param = request.getParameter("immuno");
			}
			if(request.getParameter("immunoCode")!=null && !request.getParameter("immunoCode").equals("")){
				param = request.getParameter("immunoCode");
			}
			String immuno = param;
			String criteria = "SELECT PK_IMMNOPHENOTYPING, IMMNO_CODE, IMMNO_PARENTCODE,IMMNO_DESC, LEVEL,IMMNO_HIDE,IMMUNO_COMMENTS " +
					" FROM er_immnophenotyping where deletedflag = 0 START WITH IMMNO_CODE = '"+immuno+"'" +
					" CONNECT BY PRIOR IMMNO_CODE = IMMNO_PARENTCODE ORDER SIBLINGS BY IMMNO_DESC";
			log.debug("Immuno Criteria : "+criteria);
			jsonObject.put("criteria", criteria);
			
			jsonObject = commonController.getObjectListByCriteria(jsonObject);
			immnoPhenoList = (ArrayList)jsonObject.get("returnList");
			log.trace(immnoPhenoList);
			log.trace("immnoPhenoList.size"+immnoPhenoList.size());
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}
	
	public String fetchMaintenance()throws Exception{
		log.debug("starts");
		JSONArray jsonArray = new JSONArray();
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request); 
			/*String criteria = "select PK_IMMNOPHENOTYPING, IMMNO_PARENTCODE,IMMNO_CODE,IMMNO_DESC,  level " +
					" from( select PK_IMMNOPHENOTYPING, IMMNO_PARENTCODE,IMMNO_CODE,IMMNO_DESC from ER_IMMNOPHENOTYPING where deletedflag=0) start with IMMNO_PARENTCODE = 'stafa'  " +
					" connect by prior IMMNO_CODE= IMMNO_PARENTCODE order by level desc,trim(lower(IMMNO_DESC))";
					*/
			//String immuno = request.getParameter("immuno");
			String criteria = "";
			/*String criteria = "SELECT PK_CODELST, CODELST_TYPE, CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE, LEVEL " +
					" FROM ER_CODELST where deletedflag = 0 and  level<3 START WITH CODELST_TYPE = 'stafa' " +
					" CONNECT BY PRIOR CODELST_SUBTYP = CODELST_TYPE";
			jsonObject.put("criteria", criteria);
			
			jsonObject = CommonController.getObjectListByCriteria(jsonObject);
			maintenanceList = (ArrayList)jsonObject.get("returnList");
			System.out.println(maintenanceList);
			*/
			jsonObject = new JSONObject();
			criteria = "SELECT PK_CODELST, CODELST_TYPE, CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE, LEVEL " +
			" FROM ER_CODELST where deletedflag = 0 and  level<3 START WITH CODELST_TYPE = 'stafa' " +
			" CONNECT BY PRIOR CODELST_SUBTYP = CODELST_TYPE";
			jsonObject.put("type", "SQL");
			jsonObject.put("Query", criteria);
			jsonArray.put(0,jsonObject);
			
			jsonObject = new JSONObject();
			criteria = " isHide = 'N' and type = '" + CODELSTTYPE_MODULE +"'  order by sequence, description";
			jsonObject.put("Domain", CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(1,jsonObject);
			
			List objectslst = CommonController.getObjectList(jsonArray);
			maintenanceList = (List)objectslst.get(0);
			moduleList=(List)objectslst.get(1);
			fetchOrganizationByLevel();
			//System.out.println("-----------------------"+maintenanceList.size());
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}

	/*?: mohanpublic String saveDataFiledValues()throws Exception{
		log.debug("starts");
		JSONObject jsonObj = null;
		JSONArray jsonArray = null;
		try {
			CodeListController codeListController = new CodeListController();
			if(request.getParameter("jsonData")!=null){
				jsonObj = new JSONObject(request.getParameter("jsonData"));
				jsonArray = jsonObj.getJSONArray("panelData");
			}
			//System.out.println("jsonObj ::: "+jsonObj);
			//jsonArray = CommonController.saveObject(jsonArray);
			jsonObj = VelosUtil.setUserDetails(jsonObj, request);
			System.out.println("json data " + jsonObj);
			codeListController.saveDataFiledValues(jsonObj);
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}*/
	/*public String saveDataFiledValues()throws Exception{
		JSONObject jsonDataObject = new JSONObject();  
		try{
			String query;
			String jsonData = request.getParameter("jsonData");
			//System.out.println("jsonData" + jsonData);	
			
			JSONObject jsonObject = new JSONObject(jsonData);
			JSONArray dataArray =jsonObject.getJSONArray("protocolData");
			JSONArray subDomains = new JSONArray();
			JSONObject subObject = new JSONObject();
			subObject = new JSONObject();
			subObject.put("domain", CODELST_DOMAIN);
			subObject.put("domainKey", "pkCodelst");
			subObject.put("data", dataArray);
			subDomains.put(subObject);
			//----------------
			jsonDataObject.put("subDomains", subDomains);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			jsonDataObject.remove("organizationId");
			//System.out.println(" final data --------------------");
			log.trace("finaldata"+ jsonDataObject.toString());
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeListAction : saveImmnoPhenoTyping Method End");
		return SUCCESS;
		
	}*/
	public String saveDataFiledValues()throws Exception{
		JSONObject jsonDataObject = new JSONObject();  
		try{
			String query;
			String jsonData = request.getParameter("jsonData");
			//System.out.println("jsonData" + jsonData);	
			JSONArray queries = new JSONArray();
			JSONObject jsonObject = new JSONObject(jsonData);
			JSONArray dataArray =jsonObject.getJSONArray("protocolData");
			JSONObject parentCodelstDataObject= jsonObject.getJSONObject("parentCodelstData");
			JSONArray subDomains = new JSONArray();
			JSONObject subObject = new JSONObject();
			subObject = new JSONObject();
			subObject.put("domain", CODELST_DOMAIN);
			subObject.put("domainKey", "pkCodelst");
			subObject.put("data", dataArray);
			if(parentCodelstDataObject.has("type") ){
				//String query ;
				queries = new JSONArray();
				JSONObject queryObject = new JSONObject();
				query = "update er_codelst set  CODELST_FIELDTYPE = '"+parentCodelstDataObject.get("fieldType") +"'  where CODELST_TYPE='"+parentCodelstDataObject.get("type")+"'and CODELST_SUBTYP='"+parentCodelstDataObject.get("subType")+"' and DELETEDFLAG=0";
				queries.put(query);
				queryObject.put("beforeQueries", queries);
				
				System.out.println("query --------------->"+query);
				System.out.println("queries --------------->"+queries);
				subObject.put("queries", queryObject);
			}
			subDomains.put(subObject);
			//----------------
			jsonDataObject.put("subDomains", subDomains);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			jsonDataObject.remove("organizationId");
			//System.out.println(" final data --------------------");
			log.trace("finaldata"+ jsonDataObject.toString());
			jsonObject = CommonController.saveJSONObjects(jsonDataObject);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		//System.out.println("CodeListAction : saveImmnoPhenoTyping Method End");
		return SUCCESS;
		
	}
	
	public String deleteDataField()throws Exception{
		log.debug("starts");
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = VelosUtil.constructJSON(request);
			//System.out.println("jsonObject : "+jsonObject);
			String criteria  = "update CodeList set deletedFlag=1 where pkCodelst in("+jsonObject.get("deleteDataFieldIds")+")";
			jsonObject.put("criteria", criteria);
			log.trace("jsonObject.get(criteria)=="+jsonObject.get("criteria"));
			//CommonController.updateQueryByCriteria(jsonObject);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}
	
	public String saveCodeListQc()throws Exception{
		log.debug("starts");
		JSONObject jsonDataObject = new JSONObject();  
		  
		try{
			String query;
			String jsonData = request.getParameter("jsonData");
			//System.out.println("jsonData" + jsonData);	
			
			JSONObject jsonObject = new JSONObject(jsonData);
			JSONArray dataArray =jsonObject.getJSONArray("protocolData");
			JSONArray subDomains = new JSONArray();
			JSONObject subObject = new JSONObject();
			subObject = new JSONObject();
			subObject.put("domain", CODELST_DOMAIN);
			subObject.put("domainKey", "pkCodelst");
			subObject.put("data", dataArray);
			subDomains.put(subObject);
			//----------------
			jsonDataObject.put("subDomains", subDomains);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			jsonDataObject.remove("organizationId");
			//System.out.println(" final data --------------------");
			log.trace("finaldata"+ jsonDataObject.toString());
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}
	
	public String deleteCodeListQc()throws Exception{
		log.debug("starts");
		try{
			//System.out.println(" delete Qc");
			JSONObject jsonDataObject = new JSONObject(); 
			JSONArray queries = new JSONArray();
			String jsonData = request.getParameter("jsonData");
			//System.out.println("jsonData" + jsonData);	
			  
			
			JSONObject jsonObject = new JSONObject(jsonData);
			
			String deletedIds = jsonObject.getString("deletedQcs");
			String query;
			if(deletedIds != null && !deletedIds.trim().equals("")){
				 query = "update " + CODELST_DOMAIN + " set deletedFlag =1 where pkCodelst in("+ deletedIds +")";
				queries.put(query);
				
			}
			jsonDataObject.put("queries", queries);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		
		return SUCCESS;
	}
	
	//get CodeList qc details
	public String fetchCodeListQc()throws Exception{
		log.debug("starts");
		JSONObject jsonObject = new JSONObject();
		String criteria = "";
		try {
			
			/*
			CommonController commonController = new CommonController();
			String pkCodelist = request.getParameter("param");
			if(pkCodelist!=null){
			criteria = "SELECT PK_CODELST, CODELST_TYPE, CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_COMMENTS " +
					" FROM ER_CODELST where deletedflag = 0 and PK_CODELST='"+pkCodelist+"'";
			}
			jsonObject.put("criteria", criteria);
			
			jsonObject = commonController.getObjectListByCriteria(jsonObject);
			codeListValueList = (ArrayList)jsonObject.get("returnList");
			*/
			JSONArray jsonArray = new JSONArray();
			
			jsonObject = new JSONObject();
			//BB working include calfields
					/*
					criteria = " select PK_CODELST id,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC des,trim(lower(CODELST_DESC)) ord,  level seq from( select PK_CODELST,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC from ER_CODELST  where  DELETEDFLAG = 0 and CODELST_HIDE='N') start with CODELST_TYPE = 'stafa' "+
						" connect by prior CODELST_SUBTYP= CODELST_TYPE and level =2 " +
						" union " +
						" select PK_CODELST id,CODELST_TYPE, CODELST_SUBTYP, CODELST_DESC des,trim(lower(CODELST_DESC)) ord, 2 seq from ER_CODELST  where  codelst_type ='calclist' and CODELST_HIDE='N' and  DELETEDFLAG = 0 " +
						" order by seq,ord ";
					*/
			String pkCodelist = request.getParameter("param");
			if(pkCodelist!=null){
				criteria = "SELECT PK_CODELST, CODELST_TYPE, CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_COMMENTS " +
				" FROM ER_CODELST where deletedflag = 0 and PK_CODELST='"+pkCodelist+"'";
		
				jsonObject.put("Query", criteria);
				jsonObject.put("type", "SQL");
				jsonArray.put(0,jsonObject);
				
				
				jsonObject = new JSONObject();
				criteria = " isHide = 'N' and type = '" + CODELSTTYPE_REAGENTSUPPLIER +"'  order by sequence, description";
				jsonObject.put("Domain", CODELST_DOMAIN);
				jsonObject.put("criteria", criteria);
				jsonArray.put(1,jsonObject);
				
				jsonObject = new JSONObject();
				criteria = " isHide = 'N' and type = '" + CODELSTTYPE_QCRESULT +"'  order by sequence, description";
				jsonObject.put("Domain", CODELST_DOMAIN);
				jsonObject.put("criteria", criteria);
				jsonArray.put(2,jsonObject);
				
				List objectslst = CommonController.getObjectList(jsonArray);
				codeListValueList = (List)objectslst.get(0);
				reagentlst=  (List)objectslst.get(1);
				qcResults = (List)objectslst.get(2);
				
			}
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	
	}
	
	public String refreshCodeList(){
		
		ServletContext context = ServletActionContext.getServletContext();
		//StafaConfig.setInitializeothers(false, this.getClass());
		
		//StafaConfig.reInitializeOthers(context);
		
		
		
		return NONE;
	}

	/*******************Code List start***********************************************/
	
	public List getImmnoPhenoList() {
		return immnoPhenoList;
	}

	public void setImmnoPhenoList(List immnoPhenoList) {
		this.immnoPhenoList = immnoPhenoList;
	}

	public List getMaintenanceList() {
		return maintenanceList;
	}

	public void setMaintenanceList(List maintenanceList) {
		this.maintenanceList = maintenanceList;
	}

	public List getCodeListValueList() {
		return codeListValueList;
	}

	public void setCodeListValueList(List codeListValueList) {
		this.codeListValueList = codeListValueList;
	}

	public List getChildDetails() {
		return childDetails;
	}

	public void setChildDetails(List childDetails) {
		this.childDetails = childDetails;
	}

	public List getDropDownList() {
		return dropDownList;
	}

	public void setDropDownList(List dropDownList) {
		this.dropDownList = dropDownList;
	}

	public List getProtocolFields() {
		return protocolFields;
	}

	public void setProtocolFields(List protocolFields) {
		this.protocolFields = protocolFields;
	}

	public List getCalculationlst() {
		return calculationlst;
	}

	public void setCalculationlst(List calculationlst) {
		this.calculationlst = calculationlst;
	}

	public List getExisQclst() {
		return exisQclst;
	}

	public void setExisQclst(List exisQclst) {
		this.exisQclst = exisQclst;
	}

	public List getExisReagentlst() {
		return exisReagentlst;
	}

	public void setExisReagentlst(List exisReagentlst) {
		this.exisReagentlst = exisReagentlst;
	}
	public List getMainTreelst() {
		return mainTreelst;
	}

	public void setMainTreelst(List mainTreelst) {
		this.mainTreelst = mainTreelst;
	}
/* BB
	public List getProtocollst() {
		return protocollst;
	}

	public void setProtocollst(List protocollst) {
		this.protocollst = protocollst;
	}
*/
	public List getReagentlst() {
		return reagentlst;
	}

	public void setReagentlst(List reagentlst) {
		this.reagentlst = reagentlst;
	}

	public List getQcmaintlst() {
		return qcmaintlst;
	}

	public void setQcmaintlst(List qcmaintlst) {
		this.qcmaintlst = qcmaintlst;
	}

	
	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getCodelstId() {
		return codelstId;
	}

	public void setCodelstId(Long codelstId) {
		this.codelstId = codelstId;
	}

	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public List getQcResults() {
		return qcResults;
	}
	public void setQcResults(List qcResults) {
		this.qcResults = qcResults;
	}
	public List getModuleList() {
		return moduleList;
	}
	public void setModuleList(List moduleList) {
		this.moduleList = moduleList;
	}
	public String getQcHidden() {
		return qcHidden;
	}
	public void setQcHidden(String qcHidden) {
		this.qcHidden = qcHidden;
	}
	public List getLstOrganizations() {
		return lstOrganizations;
	}
	public void setLstOrganizations(List lstOrganizations) {
		this.lstOrganizations = lstOrganizations;
	}
	public String getHiddenSubType() {
		return hiddenSubType;
	}
	public void setHiddenSubType(String hiddenSubType) {
		this.hiddenSubType = hiddenSubType;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgID() {
		return orgID;
	}
	public void setOrgID(String orgID) {
		this.orgID = orgID;
	}
	public List getLstDatafieldOrganizations() {
		return lstDatafieldOrganizations;
	}
	public void setLstDatafieldOrganizations(List lstDatafieldOrganizations) {
		this.lstDatafieldOrganizations = lstDatafieldOrganizations;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getParentCode() {
		return parentCode;
	}
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	
}