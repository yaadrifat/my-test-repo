package com.velos.stafa.action.cdmr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.helper.UploadAndProcessHelper;
import com.velos.stafa.util.CDMRUtil;




public class GetNoOfFileValue extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * Parameter getting from request
	 */
	private Integer nooffiles;
	
	public GetNoOfFileValue() {
		cdmrController = new CDMRController();
	}
	

	/*
	 * Getter and Setter
	 */
	
	public int getNooffiles() {
		return nooffiles;
	}

	public void setNooffiles(int nooffiles) {
		this.nooffiles = nooffiles;
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String query = "select PARAM_VAL from VBAR_CONTROLPARAM where PARAM_TYPE = 'FILES' and PARAM_SUBTYP = 'HOME_FILES'";
			jsonObject.put("SQLQuery", true);
			jsonObject.put("query", query);
			jsonObject = cdmrController.getNumberOfFiles(jsonObject);
			List<Map> nooffiles = (List<Map>)jsonObject.get("nooffiles");
			this.setNooffiles(Integer.parseInt(nooffiles.get(0).get("PARAM_VAL").toString()));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
}
