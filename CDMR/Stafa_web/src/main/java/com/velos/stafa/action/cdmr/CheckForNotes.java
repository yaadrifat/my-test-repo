package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     CheckForNotes
 * @description Check Notes Added or Not
 * @version     1.0
 * @file        CheckForNotes.java
 * @author      Lalit Chattar
 */
public class CheckForNotes extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 
	private boolean checkForNotes;

	/*
	 * Getter and Setter

	*/
	public boolean isCheckForNotes() {
		return checkForNotes;
	}

	public void setCheckForNotes(boolean checkForNotes) {
		this.checkForNotes = checkForNotes;
	}

	
	public CheckForNotes(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String checkForNotes() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			NotesAction notes = new NotesAction();
			if(notes.isNotesAdded(jsonObj.getString("fileID")).equals("SUCCESS")){
				this.setCheckForNotes(true);
			}else{
				this.setCheckForNotes(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return SUCCESS;
	}
	
	
}
