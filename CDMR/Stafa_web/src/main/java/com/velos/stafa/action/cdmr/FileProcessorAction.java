package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.StagingMain;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import java.lang.StringBuilder;
import com.velos.stafa.util.CDMRConstant;

/**
 * @summary     FileProcessorAction
 * @description Process File data and store in StagingMain and StagingDetail Table
 * @version     1.0
 * @file        FileProcessorAction.java
 * @author      Lalit Chattar
 */
public class FileProcessorAction extends StafaBaseAction{
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	/*
	 * Parameter getting from request
	 */
	private Integer fileID;
	private Integer sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<List<String>> aaData;
	private Integer iDisplayStart;
	private Integer iDisplayLength;
	
	/*
	 * Constructor
	 */
	public FileProcessorAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<List<String>>();
	}
	
	/*
	 * getter and setter
	 */
	public Integer getFileID() {
		return fileID;
	}

	public void setFileID(Integer fileID) {
		this.fileID = fileID;
	}
	
	public Integer getsEcho() {
		return sEcho;
	}
	public void setsEcho(Integer sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<List<String>> getAaData() {
		return aaData;
	}
	public void setAaData(List<List<String>> aaData) {
		this.aaData = aaData;
	}
	public Integer getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(Integer iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}

	public Integer getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(Integer iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

	
		
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		processFile();
		return SUCCESS;
	}
	
	/**
	 * @description Process uploaded file and Store file data in Staging Tables
	 * @return String
	 * @throws Exception
	 */
	public String processFile() throws Exception{
		
		JSONObject jsonObject = new JSONObject();
		jsonObject = VelosUtil.constructJSON(request);
		jsonObject = this.getStagingColumnMapDetail(jsonObject);
		jsonObject = this.getStagingPrimaryID(jsonObject);
		jsonObject = this.createDynamicQuery(jsonObject);
		jsonObject = this.getStagingDetailData(jsonObject);
		this.processData(jsonObject);
		
		return SUCCESS;
	}
	
	
	/**
	 * @description Process data fro staging
	 * @return String
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject) throws Exception{
		List<Map> dataList = (List<Map>)jsonObject.get("stagingdetail");
		
		try{
			for (Map map : dataList) {
				List<String> data = new ArrayList<String>();
				data.add("<input type=\"checkbox\">");
				if((map.get("SERVICECODE")) == null || (map.get("SERVICECODE")) == "")
					data.add("");
				else
					data.add(map.get("SERVICECODE").toString());

				if((map.get("DESCP")) == null || (map.get("DESCP")) == "")
					data.add("");
				else
					data.add(map.get("DESCP").toString());

				if((map.get("CPTCODE")) == null || (map.get("CPTCODE")) == "")
					data.add("");
				else
					data.add(map.get("CPTCODE").toString());

				if((map.get("PRSCODE")) == null || (map.get("PRSCODE")) == "")
					data.add("");
				else
					data.add(map.get("PRSCODE").toString());

				if((map.get("HCCCODE")) == null || (map.get("HCCCODE")) == "")
					data.add("");
				else
					data.add(map.get("HCCCODE").toString());

				if((map.get("PRSCHG")) == null || (map.get("PRSCHG")) == "")
					data.add("");
				else
					data.add(map.get("PRSCHG").toString());

				if((map.get("REVCLASS")) == null || (map.get("REVCLASS")) == "")
					data.add("");
				else
					data.add(map.get("REVCLASS").toString());

				if((map.get("EFFDATE")) == null || (map.get("EFFDATE")) == "")
					data.add("");
				else
					data.add(map.get("EFFDATE").toString());

				data.add("");
				data.add("<a href=\"#\" class=\"edit\"><img src=\"images/cdmr/edit.png\"/></a>");
				data.add("<a href=\"#\"><img src=\"images/cdmr/add.png\"/></a>");
				
//				
//				data.add(map.get("DESCP").toString());
//				data.add(map.get("CPTCODE").toString());
//				data.add(map.get("PRSCODE").toString());
//				data.add(map.get("HCCCODE").toString());
//				data.add(map.get("PRSCHG").toString());
//				data.add(map.get("REVCLASS").toString());
//				data.add(map.get("EFFDATE").toString());
//				data.add("");
//				data.add("<a href=\"#\">Click</a>");
//				data.add("<a href=\"#\">Click</a>");
				aaData.add(data);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		this.sEcho= Integer.parseInt(jsonObject.getString("sEcho"));
		this.iTotalRecords = 5000;
		this.iTotalDisplayRecords =  5000;
		
	}
	
	/**
	 * @description Get Staging Primary ID
	 * @return String
	 * @throws Exception
	 */
	public JSONObject getStagingPrimaryID(JSONObject jsonObject) throws Exception{
		jsonObject.put("query", CDMRSqlConstants.GET_STAGING_PK + jsonObject.getString("fileID").toString());
		jsonObject.put("SQLQuery", false);
		jsonObject = cdmrController.getStagingPrimaryID(jsonObject);
		List<StagingMain> listObj = (List<StagingMain>)jsonObject.get("stagingpk");
		StagingMain mainObj = listObj.get(0);
		return  jsonObject.put("stagingMainID", mainObj.getPkStagingMain());
		
		
	}
	
	/**
	 * @description Get Staging Detail Data
	 * @return String
	 * @throws Exception
	 */
	public JSONObject getStagingDetailData(JSONObject jsonObject) throws Exception{
		jsonObject.put("SQLQuery", true);
		jsonObject = cdmrController.getStagingDetailData(jsonObject);
		return jsonObject;
		
	}
	
	/**
	 * @description Get Column Mapping for staging 
	 * @return String
	 * @throws Exception
	 */
	public JSONObject getStagingColumnMapDetail(JSONObject jsonObject) throws Exception{
		String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP + " where DISP_INSTAGING  = 1";
		jsonObject.put("query", CDMRSqlConstants.GET_STAGING_COLUMN_MAP);
		jsonObject.put("SQLQuery", false);
		jsonObject = cdmrController.getStagingColumnMapDetail(jsonObject);
		return jsonObject;
	}
	
	/**
	 * @description Create duynamic query for getting data from staging
	 * @return String
	 * @throws Exception
	 */
	public JSONObject createDynamicQuery(JSONObject jsonObject) throws Exception{
		List<StagingMaps> mapsObjList = (List<StagingMaps>)jsonObject.get("stagingcolumnmap");
		StringBuilder query = new StringBuilder();
		
		query.append("select (");
		for (StagingMaps stagingMaps : mapsObjList) {
			
			
			if(stagingMaps.getDispInStaging() == 1 && stagingMaps.getCompColID().equals(CDMRConstant.SERVICE_CODE)){
				query.append(getColumns(stagingMaps.getPullFromCols())).append(" as ServiceCode,");
				continue;
			}
			if(stagingMaps.getDispInStaging() == 1 && stagingMaps.getCompColID().equals(CDMRConstant.SERVICE_DESC1)){
				query.append(getColumns(stagingMaps.getPullFromCols())).append(" as Descp,");
				continue;
			}
			if(stagingMaps.getDispInStaging() == 1 && stagingMaps.getCompColID().equals(CDMRConstant.CPT_CODE)){
				query.append(getColumns(stagingMaps.getPullFromCols())).append(" as CPTCode,");
				continue;
			}
			if(stagingMaps.getDispInStaging() == 1 && stagingMaps.getCompColID().equals(CDMRConstant.PRS_CODE)){
				query.append(getColumns(stagingMaps.getPullFromCols())).append(" as PRSCode,");
				continue;
			}
			if(stagingMaps.getDispInStaging() == 1 && stagingMaps.getCompColID().equals(CDMRConstant.HCC_TECH_CHG)){
				query.append(getColumns(stagingMaps.getPullFromCols())).append(" as HCCCode,");
				continue;
			}
			if(stagingMaps.getDispInStaging() == 1 && stagingMaps.getCompColID().equals(CDMRConstant.PRS_PHY_CHG)){
				query.append(getColumns(stagingMaps.getPullFromCols())).append(" as PRSChg,");
				continue;
			}
			
			if(stagingMaps.getDispInStaging() == 1 && stagingMaps.getCompColID().equals(CDMRConstant.EFF_FROM_DATE)){
				query.append(getColumns(stagingMaps.getPullFromCols())).append(" as EffDate,");
				continue;
			}
		}
		query = query.deleteCharAt(query.length()-1);
		int iDisLen = Integer.parseInt(jsonObject.getString("iDisplayLength")) * Integer.parseInt(jsonObject.getString("sEcho"));
		query.append(" from (" +
				"SELECT t.*, Row_Number() OVER (ORDER BY pk_cdm_stagingdetail) MyRow FROM cdm_stagingdetail t)" +
				" WHERE MyRow BETWEEN "+jsonObject.getString("iDisplayStart")+" AND "+iDisLen+" and RECORD_TYPE = 'D' and FK_CDM_STAGINGMAIN = " + jsonObject.getString("stagingMainID"));
		jsonObject.put("query", query);
		Enumeration enAttr = request.getAttributeNames(); 
		while(enAttr.hasMoreElements()){
		 String attributeName = (String)enAttr.nextElement();
		}
		return jsonObject;
	}
	
	/**
	 * @description Get Columns Name and Process Accordingly
	 * @return String
	 * @throws Exception
	 */
	public String getColumns(String colunNames) throws Exception{
		String names[] = colunNames.split("\\+");
		String cname = "";
		if(names.length > 1){
			for (int i = 0; i < names.length; i++) {
				cname = cname + names[i] + " ||" ;
			}
			cname = cname.substring(0, cname.length()-3);
			cname = cname + ") ";
			return cname;
		
		}else{
			if(names[0].equals("DDE")){
				cname = "' '";
			}else{
				cname = names[0];
			}
			return cname;
		}
	}


	
	
}