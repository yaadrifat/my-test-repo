package com.velos.stafa.util;
import java.util.List;

public class VFISNativeConnect {
	static
    {
        System.load("D:\\UBARProject\\TestDll.dll");
    }
// Operations
	public native long Find(List<String> paramlist, List<String> resultarray);
	public native void FindAllPatients(List<String> paramlist, List<String> resultarray);
	public native void  GetPatientDetails(List<String> paramlist, List<String> resultarray);
	public native void  GetAllPatients(List<String> paramlist, List<String> resultarray);
	public native long GetPInfoDetails(List<String> paramlist, List<String> resultarray);
	public native long FindNext(List<String> paramlist, List<String> resultarray);
	public native long GetPInfoFacilities(List<String> paramlist, List<String> resultarray);
	public native long GetPatientName(List<String> paramlist, List<String> resultarray);
	public native long Update(List<String> paramlist);
	public native void inDll();
};
