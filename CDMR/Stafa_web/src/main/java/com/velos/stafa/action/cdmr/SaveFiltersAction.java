package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;

import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;

/**
 * @summary SaveFiltersAction
 * @description Save and Get saved filters info from DB and show in datatable
 * @version 1.0
 * @file SaveFiltersAction.java
 * @author Shikha Srivastava
 */

public class SaveFiltersAction extends StafaBaseAction {
	/*
	 * Parameter getting from request
	 */
	private String advancesrchcols;
	private boolean checkForExisting;
	private String filterNames;
	private String fkid;
    private String columnname;
    private String optval;
    private String datatype;
	private List savedfiltersVal;
	private String filtersetname;
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request = (HttpServletRequest) ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession session = request.getSession();

	/*
	 * getter and setter
	 */

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getFiltersetname() {
		return filtersetname;
	}

	public void setFiltersetname(String filtersetname) {
		this.filtersetname = filtersetname;
	}
	public String getOptval() {
		return optval;
	}

	public void setOptval(String optval) {
		this.optval = optval;
	}
	public String getFilterNames() {
		return filterNames;
	}

	public void setFilterNames(String filterNames) {
		this.filterNames = filterNames;
	}

	public boolean isCheckForExisting() {
		return checkForExisting;
	}

	public void setCheckForExisting(boolean checkForExisting) {
		this.checkForExisting = checkForExisting;
	}

	public String getFkid() {
		return fkid;
	}

	public void setFkid(String fkid) {
		this.fkid = fkid;
	}

		
	public String getColumnname() {
		return columnname;
	}

	public void setColumnname(String columnname) {
		this.columnname = columnname;
	}

	public List getSavedfiltersVal() {
		return savedfiltersVal;
	}

	public void setSavedfiltersVal(List savedfiltersVal) {
		this.savedfiltersVal = savedfiltersVal;
	}
		
	public String getAdvancesrchcols() {
		return advancesrchcols;
	}

	public void setAdvancesrchcols(String advancesrchcols) {
		this.advancesrchcols = advancesrchcols;
	}

	/* constructor */
	public SaveFiltersAction() {
		cdmrController = new CDMRController();
		savedfiltersVal = new ArrayList();
		
	}

	/*
	 * Default Method of Action class
	 */

	public String execute() throws Exception {
		return SUCCESS;
	}

	/**
	 * @description get columns from DB
	 * @return String
	 * @throws Exception
	 */

	public String fetchAdvnceSrchColsToLoad() throws Exception{
		try{
			JSONObject jsonObject = new JSONObject();
			String query = "select CODELST_DESC,CODELST_SUBTYP,CODELST_VALUE from VBAR_CODELST where CODELST_TYPE = 'ADVANCE_SEARCH_COLS' ORDER BY CODELST_DESC";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getAdvsearchCols(jsonObject);
			List<Map> listObj = (List<Map>)jsonObject.get("advncesrch_cols");
			StringBuffer buffer = new StringBuffer("<option value='' disabled='disabled' selected='selected'>Select Filter</option>");
			for (Map map : listObj) {
				buffer.append("<option value=\""+map.get("CODELST_SUBTYP").toString()+"\" datatype=\""+map.get("CODELST_VALUE").toString()+"\">"+map.get("CODELST_DESC").toString()+"</option>");
			}
			this.setAdvancesrchcols(buffer.toString());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		 return "SUCCESS";
	}
	
	public String advanceSrchcolsforSavedfilterSet() throws Exception{
		try{
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			JSONArray allvals = new JSONArray(jsonObject.getString("excludecols"));
			ArrayList<String> mylist = new ArrayList<String>();
			for(int i =0;i< allvals.length();i++){
				mylist.add("'"+allvals.get(i).toString().trim()+"'");
			}
			String query = "select CODELST_SUBTYP,CODELST_DESC,CODELST_VALUE from VBAR_CODELST where CODELST_TYPE = 'ADVANCE_SEARCH_COLS' and CODELST_SUBTYP NOT IN ("+mylist.toString().replaceAll("\\[", "").replaceAll("\\]","")+") ORDER BY CODELST_DESC";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getAdvsearchCols(jsonObject);
			List<Map> listObj = (List<Map>)jsonObject.get("advncesrch_cols");
			StringBuffer buffer = new StringBuffer("<option value='' disabled='disabled' selected='selected'>Select Filter</option>");
			for (Map map : listObj) {
				buffer.append("<option value=\""+map.get("CODELST_SUBTYP").toString()+"\" datatype=\""+map.get("CODELST_VALUE").toString()+"\">"+map.get("CODELST_DESC").toString()+"</option>");
			}
			this.setAdvancesrchcols(buffer.toString());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		 return "SUCCESS";
	}
	
	public String savefilter() throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject = CDMRUtil.fetchEventLibId(jsonObject);
			if(!this.checkForExistingFilter(jsonObject)){
				setCheckForExisting(false);
			}
			else{
				setCheckForExisting(true);
				jsonObject.put("filterName", jsonObject.getString("filtrName"));
				jsonObject.put("userId", jsonObject.getInt("user"));
				jsonObject.put("eventLibid", jsonObject.getString("EVENT_ID"));
				cdmrController.saveNewFilter(jsonObject);
				this.fkid = jsonObject.get("filterID").toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String updateFilterName() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String filtername = jsonObj.getString("filtrName");
			String changeflag = jsonObj.getString("changeFlagstatus");
			String pkid = jsonObj.getString("FKID");
			
			if (changeflag.equals("true")) {
				if (!this.checkForExistingFilter(jsonObj)) {
					setCheckForExisting(false);
					return SUCCESS;

				} else {
					setCheckForExisting(true);
					this.updateCdmMaint(filtername, pkid);
				}

			}
			else{
				setCheckForExisting(true);
				//this.updateCdmMaint(filtername,pkid);
				System.out.println("****no change in filter set name****");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	
	}
	public String saveEditedFilter() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			this.updateCdmParam(jsonObj);
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	/**
	 * @description Check For existing Filters
	 * @return void
	 * @throws Exception
	 */
	public boolean checkForExistingFilter(JSONObject jsonObj) throws Exception {
			try {
				String query = "select FILTER_NAME from CDM_FILTER_MAINT where TRIM(LOWER(FILTER_NAME)) = '"
						+ jsonObj.getString("filtrName").toLowerCase().trim()
						+ "' and DELETEDFLAG = 0 and user_id = '"
						+ jsonObj.getInt("user") + "'";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.checkForFilterName(jsonObj);
				List<Map> list = (List<Map>) jsonObj.get("filternamelist");
	              if(list.size() > 0){
	            	  return false;
	              }
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
	}

	@SuppressWarnings("unchecked")
	public String loadFilterNames() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			StringBuffer buffer = new StringBuffer();
			buffer.append("<option disabled='disabled' selected='selected'>Saved Filter Sets</option>");
			String query = "select FILTER_NAME from CDM_FILTER_MAINT where DELETEDFLAG = 0 and user_id = '"
					+ jsonObj.getInt("user") + "' ORDER BY FILTER_NAME";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.loadFilters(jsonObj);
			List<Map> dataMap = (List<Map>) jsonObj.get("filternames");
			for (Map map : dataMap) {
				buffer.append("<option value=\""+ map.get("FILTER_NAME").toString() + "\">"+ map.get("FILTER_NAME").toString() + "</option>");
			}
			this.setFilterNames(buffer.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}

	/**
	 * @description new advance search savefilter data
	 * @return json
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String saveInFilterParam() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			JSONArray allvals = new JSONArray(jsonObj.getString("col_vals"));
			LinkedHashMap<String, String> datamap = new LinkedHashMap<String, String>();
			if (allvals.length() != 0) {
				for(int i =0; i< allvals.length();i++){
					String[] splitvals = allvals.get(i).toString().split(":",2);
					datamap.put(splitvals[0],splitvals[1]);
				}
			}
			Iterator it = datamap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				String filtercol = (String) entry.getKey();
				String []value = entry.getValue().toString().split(":");
				String filtervalue = value[0].replaceAll("X","");
				String cdmcol_key = value[1];
				jsonObj.put("cdmcolsname",cdmcol_key.trim());
				jsonObj.put("filterColName", filtercol.trim());
				jsonObj.put("filterval",filtervalue.trim());
				jsonObj.put("fkpkFiltermaint", jsonObj.getLong("FKID"));
				jsonObj = cdmrController.saveFilterParam(jsonObj);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}

	public void updateCdmMaint(String filname,String pkid) {
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "update CDM_FILTER_MAINT set FILTER_NAME = '"+ filname + "' where PK_FILTER_MAINT = " + pkid;
			jsonObj.put("query", query);
            jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateCdmFilMaint(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void updateCdmParam(JSONObject jsonObj) {
		try {
				JSONArray allvals = new JSONArray(jsonObj.getString("updated_val_array"));
				LinkedHashMap<String, String> updateddatamap = new LinkedHashMap<String, String>();
				LinkedHashMap<String, String> olddataMap = new LinkedHashMap<String, String>();
				String[] newVal = {};
				String filval="";
				String cdmcol="";
				String oldVal = "";
				String newkey = "";
				String oldkey = "";
				String temp_newkey="";
				String temp_updateoldkey="";
				String temp_delkey="";
				Map.Entry pairs;
				if (allvals.length() != 0) {
					for(int i =0; i< allvals.length();i++){
						String[] splitvals = allvals.get(i).toString().split(":",2);
						updateddatamap.put(splitvals[0].trim(),splitvals[1]);
					}
				}
				String query = "select * from CDM_FILTER_PARAM where FK_FILTER_MAINT =(Select PK_FILTER_MAINT from cdm_filter_maint where filter_name = '"
					+ jsonObj.get("filtrName") + "' and user_id = '"
					+ jsonObj.getInt("user") + "' and deletedflag = 0) and deletedflag = 0";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getCdmFilParam(jsonObj);
				List<Map> datalist = (List<Map>) jsonObj.get("savedfiltersdetails");
				for (Map<?, ?> map : datalist) {
					olddataMap.put(map.get("FILTER_COL").toString().trim(), map.get("FILTER_VAL").toString().trim());
					}
				Iterator<?> iterupdated = updateddatamap.entrySet().iterator();
				Iterator<?> iterold = olddataMap.entrySet().iterator();
				while (iterold.hasNext()||iterupdated.hasNext()) {
					
						if (iterupdated.hasNext()) {
							pairs = (Map.Entry) iterupdated.next();
							newVal = pairs.getValue().toString().split(":");
							filval =newVal[0].replaceAll("X","").trim();
							cdmcol= newVal[1].trim();
							newkey = ((String) pairs.getKey()).trim();
						}
						if (iterold.hasNext()) {
							pairs = (Map.Entry) iterold.next();
							oldVal = ((String) pairs.getValue()).trim();
							oldkey = ((String) pairs.getKey()).trim();
						}
						if (olddataMap.containsKey(newkey)){
							if (!olddataMap.containsValue(filval)){
								if(temp_updateoldkey!=filval){
								System.out.println("update old as-- "+newkey+" = "+filval);
								String queryupdate = "UPDATE cdm_filter_param SET FILTER_VAL = "
									+ "'"+ filval+ "'"+ " WHERE filter_col  = "+ "'"+ newkey+ "'"	+ 
									" and deletedflag = 0 AND fk_filter_maint =(Select PK_FILTER_MAINT from cdm_filter_maint where filter_name = '"
									+ jsonObj.get("filtrName") + "' and user_id = '"+ jsonObj.getInt("user") + "' and deletedflag = 0)";
									jsonObj.put("query", queryupdate);
									jsonObj.put("SQLQuery", true);
									jsonObj = cdmrController.updateCdmParam(jsonObj);
									temp_updateoldkey=filval;
								}
							}
						}
						if (!olddataMap.containsKey(newkey)) {
							if(temp_newkey!=newkey){
								System.out.println("new values-- "+ newkey);
								jsonObj.put("cdmcolsname",cdmcol);
								jsonObj.put("filterColName", newkey);
								jsonObj.put("filterval", filval);
								jsonObj.put("fkpkFiltermaint", jsonObj.getLong("FKID"));
								jsonObj = cdmrController.saveFilterParam(jsonObj);
								temp_newkey=newkey;
							}
						}
						if(!updateddatamap.containsKey(oldkey)){
							if(temp_delkey!=oldkey){
							System.out.println("delete-- "+oldkey);
							String queryupdate2 = "UPDATE cdm_filter_param SET deletedflag=1 where FILTER_COL='"+ oldkey+"' and FILTER_VAL = '"+oldVal+"' and fk_filter_maint =(Select PK_FILTER_MAINT from cdm_filter_maint where filter_name = '"
							+ jsonObj.get("filtrName") + "' and user_id = '"+ jsonObj.getInt("user") + "' and deletedflag=0 )";
							jsonObj.put("query", queryupdate2);
							jsonObj.put("SQLQuery", true);
							jsonObj = cdmrController.updateCdmParam(jsonObj);
							temp_delkey=oldkey;
							}
						}
					}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String delFilter() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String query = "update CDM_FILTER_MAINT set DELETEDFLAG = 1 where FILTER_NAME = '"
					+ jsonObj.get("filtrName") + "' and user_id = '"+ jsonObj.getInt("user") + "'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateCdmFilMaint(jsonObj);
			this.delFromParam(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public void delFromParam(JSONObject jsonObj){
		try{
			String query = "update CDM_FILTER_PARAM set DELETEDFLAG = 1 where FK_FILTER_MAINT = (Select PK_FILTER_MAINT from cdm_filter_maint where filter_name = '"
					+ jsonObj.get("filtrName") + "' and creator = '"	+ jsonObj.getInt("user") + "')";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateCdmParam(jsonObj);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public String applyFilter() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String query = "Select PK_FILTER_PARAM,FK_FILTER_MAINT,CDM_COLS_NAME,FILTER_COL,FILTER_VAL from cdm_filter_param where DELETEDFLAG = 0 and FK_FILTER_MAINT =(Select PK_FILTER_MAINT from cdm_filter_maint where filter_name = '"
					+ jsonObj.get("filtrName") + "' and deletedflag = 0 and user_id = '"+ jsonObj.getInt("user") + "')";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getSavedFilters(jsonObj);
			List<Map> listObj = (List<Map>) jsonObj.get("savedfilters");
			for (Map map : listObj) {
				Map dataMap = new HashMap();
				dataMap.put("PKMaint", map.get("PK_FILTER_PARAM").toString());
				dataMap.put("FK", map.get("FK_FILTER_MAINT").toString());
				dataMap.put("CDM_COLS_NAME", map.get("CDM_COLS_NAME")
						.toString());
				dataMap.put("colname", map.get("FILTER_COL").toString());
				dataMap.put("FILTER_VAL", map.get("FILTER_VAL").toString());
				savedfiltersVal.add(dataMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
		
	public String removeonefilter() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String [] colparts = jsonObj.get("columnname").toString().split(":");
			String optionval = jsonObj.getString("cdmcol");
			if(optionval.contains("DATE")){
				this.datatype="TIMESTAMP";
			}
			else if(optionval.contains("CHG")){
				this.datatype="NUMERIC";
			}
			String colname = colparts[0].trim();
			this.optval = optionval;
			this.columnname = colname;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
}
