package com.velos.stafa.action.cdmr;

import java.io.File;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRExcelParser;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.util.CDMRUtil;

/**
 * @summary GetAndProcessRemoteData
 * @description Get Data from remote server and process for staging
 * @version 1.0
 * @file GetAndProcessRemoteData.java
 * @author Lalit Chattar, Ved Prakash, Shiv,Shikha, Shubham Agarwal
 */

public class GetAndProcessRemoteData {

	private static CDMRController cdmrController;
	private String lcode;
	private String fileStatusPk;

	/**
	 * @description Helper Method for calling Different methods for processing  *              data
	 * @return void
	 * @throws Exception
	 */
	synchronized public static void getAndProcessRemoteData() throws Exception {
		long stime = new Date().getTime();
		String interfaceID;
		cdmrController = new CDMRController();
		GetAndProcessRemoteData remoteData = new GetAndProcessRemoteData();
		CDMRExcelParser readdata = new CDMRExcelParser();
		try {
			// data import from file(s) started
			List intList = remoteData.getInterfaceList();
			for (Object FileID : intList) {
				Map map = (Map) FileID;
				interfaceID = map.get("FILE_ID").toString();
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("FILE_ID", interfaceID);
				CDMRUtil.updateTransferStatus("1","'"+"IMPORT_DATA"+"'");
				if (remoteData.getPullMethod(jsonObj).equalsIgnoreCase("F")) {
					remoteData.getFileSourceLocation(jsonObj);
					remoteData.getFileInformation(jsonObj);
					jsonObj = remoteData.getEventLibId(jsonObj);
					jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
					jsonObj = remoteData.getFilekey(jsonObj);
					jsonObj = remoteData.getFileName(jsonObj);
					List<String> filesLocation = remoteData.readAllFiles(jsonObj);

					if (filesLocation.isEmpty()) {
						// need to write mail notification code
						System.out.println("No file found for file type - "
								+ jsonObj.getString("FILE_ID"));
						CDMRUtil.updateTransferStatus("0","'"+"IMPORT_DATA"+"'");
						continue;

					}

					for (String fileLocation : filesLocation) {

						int index = fileLocation.lastIndexOf("\\");
						String fileName = fileLocation.substring(index + 1);

						if (!remoteData.checkForExistingFile(fileLocation)) {
							remoteData.truncateStagingTemp();
							jsonObj= remoteData.fetchcolTypeMapDetail(jsonObj);
							readdata.ReadAndParseExcel(jsonObj.put("final_file_name", fileLocation),cdmrController);

							remoteData.processDataFromFile(jsonObj);
							remoteData.moveFileInProcessedDir(remoteData.getProcessedDirLocation(), fileLocation);
							CDMRUtil.updateTransferStatus("0","'"+"IMPORT_DATA"+"'");// data import from file(s) completed
						}

						else {
							System.out.println("File with name " + fileName
									+ " already exist");
							CDMRUtil.updateTransferStatus("0","'"+"IMPORT_DATA"+"'");
						}

					}

				} else {
					remoteData.truncateStagingTemp();
					remoteData.getTechnicalCharges();
					remoteData.compareTechnicalCharges();
					remoteData.truncateStagingTemp();
					remoteData.getProfessionalCharges();
					remoteData.compareProfessionalCharges();
				}

			}

		}

		catch (Exception e) {

			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getAndProcessRemoteData", "1");
			e.printStackTrace();
		}
		long etime = new Date().getTime();
	}

	/**
	 * @description Get Technical Data from remote server
	 * @return void
	 * @throws Exception
	 */
	public void getTechnicalCharges() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query", CDMRSqlConstants.GET_TECHNICAL_CHARGES_QUERY);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getActiveMapVesion(jsonObj);
			List<Map> mapVersionList = (List<Map>) jsonObj.get("mapversion");

			jsonObj.put("query", mapVersionList.get(0).get("SELECT_SQL"));
			jsonObj.put("SQLQuery", true);
			jsonObj.put("bindMapVersion", mapVersionList.get(0).get(
					"PK_CDM_MAPSVERSION"));
			jsonObj = cdmrController.getTechnicalCharges(jsonObj);
			this.insertTechnicalDataIntoStagingTemp(jsonObj);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getTechnicalCharges", "1");
			e.printStackTrace();
			System.out.println("================getTechnicalCharges========================");
			System.out.println(e.getMessage());
			System.out.println("========================================");
		}
	}

	/**
	 * @description Get professional Data from remote server
	 * @return void
	 * @throws Exception
	 */
	public void getProfessionalCharges() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query",
					CDMRSqlConstants.GET_PROFESSIONAL_CHARGES_QUERY);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getActiveMapVesion(jsonObj);
			List<Map> mapVersionList = (List<Map>) jsonObj.get("mapversion");
			jsonObj.put("query", mapVersionList.get(0).get("SELECT_SQL"));
			jsonObj.put("SQLQuery", true);
			jsonObj.put("bindMapVersion", mapVersionList.get(0).get(
					"PK_CDM_MAPSVERSION"));
			jsonObj = cdmrController.getProfessionalCharges(jsonObj);
			this.insertProfessionalDataIntoStagingTemp(jsonObj);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getProfessionalCharges", "1");
			e.printStackTrace();
			System.out
					.println("================getProfessionalCharges========================");
			System.out.println(e.getMessage());
			System.out.println("========================================");

		}
	}

	/**
	 * @description Compare technical remote data with existing in cdm main
	 * @return void
	 * @throws Exception
	 */
	public void compareTechnicalCharges() throws Exception {
		try {
			this.processData("EIWTD");
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::compareTechnicalCharges", "1");
			e.printStackTrace();
			System.out
					.println("================compareTechnicalCharges========================");
			System.out.println(e.getMessage());
			System.out.println("========================================");
		}
	}

	/**
	 * @description Compare professional remote data with existing in cdm main
	 * @return void
	 * @throws Exception
	 */
	public void compareProfessionalCharges() throws Exception {
		try {

			this.processData("EIWPD");
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::compareProfessionalCharges", "1");
			e.printStackTrace();
			System.out
					.println("================compareProfessionalCharges========================");
			System.out.println(e.getMessage());
			System.out.println("========================================");

		}
	}

	/**
	 * @description Helper Method for calling different method for processing
	 *              data
	 * @return void
	 * @throws Exception
	 */
	public void processData(String fileId) throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("FILE_ID", fileId);

			jsonObject = this.getChgIndc(jsonObject);
			jsonObject = this.getDataFromCodeList(jsonObject);
			jsonObject = CDMRUtil.getCurrentVersion(jsonObject);
			jsonObject = this.getStagingColumnMapDetail(jsonObject);
			jsonObject = this.createQuery(jsonObject);
			jsonObject = this.getStagingTemp(jsonObject);

			jsonObject = this.getFileName(jsonObject);

			jsonObject = this.getFileInfo(jsonObject);
			this.compareData(jsonObject);
			jsonObject = this.createDynamicQueryForComreq(jsonObject);
			this.saveInStagingCompReq(jsonObject);
		} catch (Exception e) {

			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::processData", "1");
			e.printStackTrace();
			System.out
					.println("================processData========================");
			System.out.println(e.getMessage());
			System.out.println("========================================");
		}

	}

	public void processDataFromFile(JSONObject jsonObject) throws Exception {
		try {

			String[] intflibId = CDMRUtil.fetchPropertyVal(jsonObject.getString("FILE_ID")+ "_LIBID");
			
			jsonObject.put("RTYPE", intflibId[0]);

			jsonObject = this.fetchRTRule(jsonObject);
			String rtrule = jsonObject.getString("RTRULE");

			jsonObject = this.getDataFromCodeList(jsonObject);
			jsonObject = CDMRUtil.getCurrentVersion(jsonObject);
			jsonObject = this.getChgIndc(jsonObject);
			
			jsonObject = this.getStagingColumnMapDetail(jsonObject);
			
			jsonObject = this.createQuery(jsonObject);
			jsonObject = this.getStagingTemp(jsonObject);
			jsonObject = this.getFileName(jsonObject);
			jsonObject = this.getFileInfo(jsonObject);

			if (rtrule.equals("FLAG")) {
				this.compareDataFlag(jsonObject);
			} else if (rtrule.equals("DATE")) {
				this.compareData(jsonObject);
			}

			jsonObject = this.createDynamicQueryForComreq(jsonObject);
			this.saveInStagingCompReq(jsonObject);
		} catch (Exception e) {

			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::processData", "1");
			e.printStackTrace();
			System.out
					.println("================processData========================");
			System.out.println(e.getMessage());
			System.out.println("========================================");
		}

	}

	/**
	 * @description Save data in staging comprec
	 * @return void
	 * @throws Exception
	 */
	public void saveInStagingCompReq(JSONObject jsonObject) throws Exception {
		try {
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.saveInStagingCompReq(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::saveInStagingCompReq", "1");
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @description Insert remote technical data in staging temp
	 * @return void
	 * @throws Exception
	 */
	public void insertTechnicalDataIntoStagingTemp(JSONObject jsonObj)
			throws Exception {
		try {
			List<Map> dataList = (List<Map>) jsonObj.get("remotedata");
			String mapId = jsonObj.getString("bindMapVersion");
			jsonObj.put("query", CDMRSqlConstants.SQL_BINDING_MAP + mapId);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMappingForSQLBinding(jsonObj);
			List<Map> mapData = (List<Map>) jsonObj.get("sqlbindingmapdata");
			for (Map map : dataList) {
				int index = 1;
				StringBuilder insertQuery = new StringBuilder(
						"insert into cdm_staging_temp(PK_CDM_STAGING_TEMP, ");
				StringBuilder dataQuery = new StringBuilder(
						" values(SEQ_CDM_STAGING_TEMP.NEXTVAL, ");
				String data = "";
				Iterator iterator = map.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry entry = (Map.Entry) iterator.next();
					if (entry.getValue() == null) {
						data = "";
					} else {
						data = entry.getValue().toString()
								.replaceAll("'", "''");
					}
					for (Map columnMapping : mapData) {

						if (columnMapping
								.get("SQL_COL_NAME")
								.toString()
								.toUpperCase()
								.equals(entry.getKey().toString().toUpperCase())) {
							insertQuery.append(columnMapping
									.get("TBL_COL_NAME").toString()
									+ ", ");
							dataQuery.append("'" + data + "', ");
						}
					}

				}
				insertQuery = insertQuery.replace((insertQuery.length() - 2),
						insertQuery.length(), "");
				dataQuery = dataQuery.replace((dataQuery.length() - 2),
						dataQuery.length(), "");
				insertQuery.append(")");
				dataQuery.append(")");
				jsonObj.put("query", insertQuery.append(dataQuery).toString());
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.insertInStagingTemp(jsonObj);
			}
		} catch (Exception e) {
			CDMRUtil
					.errorLogUtil(
							e,
							"",
							cdmrController,
							"GetAndProcessRemoteData::insertTechnicalDataIntoStagingTemp",
							"1");
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @description Insert remote professional data in staging temp
	 * @return void
	 * @throws Exception
	 */
	public void insertProfessionalDataIntoStagingTemp(JSONObject jsonObj)
			throws Exception {
		try {
			List<Map> dataList = (List<Map>) jsonObj.get("remotedata");
			String mapId = jsonObj.getString("bindMapVersion");
			jsonObj.put("query", CDMRSqlConstants.SQL_BINDING_MAP + mapId);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMappingForSQLBinding(jsonObj);
			List<Map> mapData = (List<Map>) jsonObj.get("sqlbindingmapdata");
			for (Map map : dataList) {
				int index = 1;
				StringBuilder insertQuery = new StringBuilder(
						"insert into cdm_staging_temp(PK_CDM_STAGING_TEMP, ");
				StringBuilder dataQuery = new StringBuilder(
						" values(SEQ_CDM_STAGING_TEMP.NEXTVAL, ");
				String data = "";
				Iterator iterator = map.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry entry = (Map.Entry) iterator.next();
					if (entry.getValue() == null) {
						data = "";
					} else {
						data = entry.getValue().toString()
								.replaceAll("'", "''");
					}
					for (Map columnMapping : mapData) {
						if (columnMapping
								.get("SQL_COL_NAME")
								.toString()
								.toUpperCase()
								.equals(entry.getKey().toString().toUpperCase())) {
							insertQuery.append(columnMapping
									.get("TBL_COL_NAME").toString()
									+ ", ");
							dataQuery.append("'" + data + "', ");
						}
					}
				}
				insertQuery = insertQuery.replace((insertQuery.length() - 2),
						insertQuery.length(), "");
				dataQuery = dataQuery.replace((dataQuery.length() - 2),
						dataQuery.length(), "");
				insertQuery.append(")");
				dataQuery.append(")");
				jsonObj.put("query", insertQuery.append(dataQuery).toString());
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.insertInStagingTemp(jsonObj);
			}
		} catch (Exception e) {
			CDMRUtil
					.errorLogUtil(
							e,
							"",
							cdmrController,
							"GetAndProcessRemoteData::insertProfessionalDataIntoStagingTemp",
							"1");
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @description Truncate staging temp table
	 * @return void
	 * @throws Exception
	 */
	public void truncateStagingTemp() throws Exception {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query", CDMRSqlConstants.TRUNCATE_STAGING_TEMP);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.truncateStagingTemp(jsonObj);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::truncateStagingTemp", "1");
			e.printStackTrace();
		}
	}

	/**
	 * @description Get Mapping Detail from CDM_MAPVERSION
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getStagingColumnMapDetail(JSONObject jsonObject)
			throws Exception {
		try {
			jsonObject = this.getMapVersion(jsonObject);
			
			String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP
					+ " where fkMapVersion = "
					+ jsonObject.getString("MAPSVERSION");
			
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", false);
			jsonObject = cdmrController.getStagingColumnMapDetail(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getStagingColumnMapDetail", "1");
			e.printStackTrace();
			throw e;
		}

		return jsonObject;
	}

	/**
	 * @description Create Dynamic Query
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createQuery(JSONObject jsonObject) throws Exception {
		try {
			List<StagingMaps> mapsObjList = (List<StagingMaps>) jsonObject
					.get("stagingcolumnmap");
			StringBuilder queryForSagingTemp = new StringBuilder();
			StringBuilder queryForCdmMain = new StringBuilder();
			Map mappingForCdmMainAndSagingTemp = new HashMap();
			Map MAINCDM_COLTYPE = new HashMap();
			queryForSagingTemp.append("select  ");
			queryForCdmMain.append("select  ");
			for (StagingMaps stagingMaps : mapsObjList) {
				if (stagingMaps.getPullFromCols().equals("INST")
						|| stagingMaps.getPullFromCols().equals("DDE")
						|| stagingMaps.getPullFromCols().equals("DDEI")) {
					continue;
				} else {
					
					queryForSagingTemp
							.append(this.getColumnsNames(stagingMaps.getPullFromCols())).append(",");
					queryForCdmMain
							.append(this.getColumnsNames(stagingMaps.getEqMainCdmCol())).append(",");
					mappingForCdmMainAndSagingTemp.put(stagingMaps
							.getEqMainCdmCol(), stagingMaps.getPullFromCols());
					MAINCDM_COLTYPE.put(stagingMaps.getEqMainCdmCol(),
							stagingMaps.getMainCdmColType());
				}
			}
			queryForSagingTemp = queryForSagingTemp
					.deleteCharAt(queryForSagingTemp.length() - 1);
			queryForSagingTemp.append(" from CDM_STAGING_TEMP");

			queryForCdmMain = queryForCdmMain.deleteCharAt(queryForCdmMain
					.length() - 1);
			queryForCdmMain.append(" from CDM_MAIN where ");
			queryForCdmMain
					.append(" CHG_IND = '"
							+ jsonObject.getString("chgind")
							+ "' and  IS_THIS_SS = 0 and DELETEDFLAG = 0 and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "
							+ jsonObject.getString("versionid")
							+ ") group by LSERVICE_CODE, CHG_IND)");

			jsonObject.put("queryForSagingTemp", queryForSagingTemp);
			jsonObject.put("queryForCdmMain", queryForCdmMain);
			jsonObject.put("mappingForCdmMainAndSagingTemp",
					mappingForCdmMainAndSagingTemp);
			
			jsonObject.put("MAINCDM_COLTYPE", MAINCDM_COLTYPE);
			
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::createQuery", "1");
			e.printStackTrace();
			throw e;
		}

		return jsonObject;
	}

	/**
	 * @description Get Mapping for Staging Temp
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getStagingTemp(JSONObject jsonObject) throws Exception {
		try {
			// jsonObject.put("query", CDMRSqlConstants.GET_STAGING_DETAIL);

			jsonObject.put("query", jsonObject.getString("queryForSagingTemp"));
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromStagingTemp(jsonObject);
			// List<Map> listObj = (List<Map>)jsonObject.get("stagingTemp");
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getStagingTemp", "1");
			e.printStackTrace();
			throw e;
		}

		return jsonObject; // ;jsonObject.put("stagingMainID",
		// mainObj.getPkStagingMain());

	}

	/**
	 * @description Get Mapping for CDM Main
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getCdmMain(JSONObject jsonObject) throws Exception {

		try {
			// jsonObject.put("query", CDMRSqlConstants.GET_STAGING_DETAIL);
			jsonObject.put("query", jsonObject.getString("queryForCdmMain"));
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCdmMain(jsonObject);
			List<Map> listObj = (List<Map>) jsonObject.get("cdmmain");
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getCdmMain", "1");
			e.printStackTrace();
		}
		return jsonObject; // ;jsonObject.put("stagingMainID",
		// mainObj.getPkStagingMain());

	}

	/**
	 * @description Compare Data of staging temp and cdm main
	 * @return void
	 * @throws Exception
	 */
	public void compareData(JSONObject jsonObject) throws Exception {

		int track = 0;
		List<Map> stagingTempList = new ArrayList();
		List<Map> cdmMainList = new ArrayList();
		Map mappingForCdmMainAndSagingTemp = new HashMap();
		Map maincdmColType = new HashMap();
		try {
			stagingTempList = (List<Map>) jsonObject.get("stagingTemp");
			// cdmMainList = (List<Map>)jsonObject.get("cdmmain");
			mappingForCdmMainAndSagingTemp = (Map) jsonObject.get("mappingForCdmMainAndSagingTemp");
			maincdmColType = (Map) jsonObject.get("MAINCDM_COLTYPE");
			Iterator stagingTempIt = stagingTempList.iterator();

			String key = (String) mappingForCdmMainAndSagingTemp.get("LSERVICE_CODE");

			String fromEffDateMapping = mappingForCdmMainAndSagingTemp.get("EFF_FROM_DATE").toString();
			String toEffDateMapping = mappingForCdmMainAndSagingTemp.get("EFF_TO_DATE").toString();
			String fromNum = fromEffDateMapping.substring(8);
			String toNum = toEffDateMapping.substring(8);
			JSONObject temp = new JSONObject();
			while (stagingTempIt.hasNext()) {

				boolean serviceCodeMatcherFlag = false;
				Map rowStagingTemp = (Map) stagingTempIt.next();
				Iterator cdmMainIt = cdmMainList.iterator();
				String lserviceCodeStagingTemp = (String) rowStagingTemp.get(key);

				// String stgngFromDate =
				// rowStagingTemp.get(fromEffDateMapping)==null?null:rowStagingTemp.get(fromEffDateMapping).toString();
				String stgngToDate = rowStagingTemp.get(toEffDateMapping) == null ? null: rowStagingTemp.get(toEffDateMapping).toString();
				String cdmTodate = "";

				lcode = lserviceCodeStagingTemp;
				StringBuilder stagingDetailsCol = new StringBuilder();

				Iterator itr = mappingForCdmMainAndSagingTemp.entrySet().iterator();
				while (itr.hasNext()) {
					
					Map.Entry entry = (Map.Entry) itr.next();
					String cdmMain = (String) entry.getKey();
					String sagingTemp = (String) entry.getValue();

					String colStagingTemp = (rowStagingTemp.get(sagingTemp)) == null ? "": rowStagingTemp.get(sagingTemp).toString();
					String maincdmCol = (maincdmColType.get(cdmMain)) == null ? "": maincdmColType.get(cdmMain).toString();
					
					if (maincdmCol.equalsIgnoreCase("NUMERIC")) {
						rowStagingTemp.put(sagingTemp, CDMRUtil.formateValue(colStagingTemp));

					}

				}
               
				String sql = jsonObject.getString("queryForCdmMain")+ " and LSERVICE_CODE = '" + lserviceCodeStagingTemp+ "' and EVENT_LIB_ID='" + jsonObject.getString("EVENT_ID") + "'";
				temp.put("query", sql);
				temp.put("SQLQuery", true);
				temp = cdmrController.getCdmMain(temp);
				List<Map> data = (List<Map>) temp.get("cdmmain");
				if (data.size() == 0) {
					// New Row Data Will Be Added...............
					jsonObject.put("stagingDetailsCol", stagingDetailsCol);
					jsonObject.put("rowStagingTemp", rowStagingTemp);
					jsonObject.put("INST_CODE","A");
					track++;
					this.saveStagingDetail(jsonObject);
				} else {
					serviceCodeMatcherFlag = true;
					Map rowcdmMain = data.get(0);

					String lserviceCodeCdmMain = (String) rowcdmMain.get("LSERVICE_CODE");
					try {
						cdmTodate = rowcdmMain.get("EFF_TO_DATE") == null ? null: rowcdmMain.get("EFF_TO_DATE").toString();
						if (cdmTodate != null) {
							cdmTodate = CDMRUtil.formateDate("yyyy-MM-dd HH:mm:ss", "dd-MMM-yyyy",cdmTodate, "1");
						}

					} catch (Exception e) {
						CDMRUtil.errorLogUtil(e, lcode, cdmrController,	"GetAndProcessRemoteData::compareData", "1");
						e.printStackTrace();
					}

					// for(Map.Entry entryMaincdm : maincdmColType.entrySet())
					Iterator it = mappingForCdmMainAndSagingTemp.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry entry = (Map.Entry) it.next();
						String cdmMain = (String) entry.getKey();
						String sagingTemp = (String) entry.getValue();

						String colStagingTemp = (rowStagingTemp.get(sagingTemp)) == null ? "": rowStagingTemp.get(sagingTemp).toString();
						String colCdmMain = (rowcdmMain.get(cdmMain)) == null ? "": rowcdmMain.get(cdmMain).toString();
						String maincdmCol = (maincdmColType.get(cdmMain)) == null ? "": maincdmColType.get(cdmMain).toString();
                       
						
						if (maincdmCol.equals("TIMESTAMP")&& !(colCdmMain.equals(""))&& !(colStagingTemp.equals(""))) {
							if (!this.compareDate(colStagingTemp, colCdmMain))
								stagingDetailsCol.append((sagingTemp.substring(8))).append(",");
						} else if (maincdmCol.equalsIgnoreCase("NUMERIC")) {
							
							if (!colStagingTemp.equalsIgnoreCase(CDMRUtil.formateValue(colCdmMain)))
								stagingDetailsCol.append((sagingTemp.substring(8))).append(",");
						} 
						else if (!colStagingTemp.equalsIgnoreCase(colCdmMain) && !(colCdmMain.equals(""))&& !(colStagingTemp.equals(""))) {
							
							stagingDetailsCol.append((sagingTemp.substring(8))).append(",");
						}

					}
					if (stagingDetailsCol.length() > 0) {
						stagingDetailsCol = stagingDetailsCol.deleteCharAt(stagingDetailsCol.length() - 1);

					}
                    
                     
					if (stagingDetailsCol.length() > 0 && serviceCodeMatcherFlag) {
						List<String> numList = Arrays.asList(stagingDetailsCol.toString().split(","));
						
						if (stgngToDate != null) {
							if (CDMRUtil.getDifferenceBetweenDate(stgngToDate) == 0) {
								jsonObject.put("stagingDetailsCol",stagingDetailsCol);
								jsonObject.put("rowStagingTemp", rowStagingTemp);
								jsonObject.put("INST_CODE", "D");
								track++;
								this.saveStagingDetail(jsonObject);
							} else if (cdmTodate != null) {
								
								if (CDMRUtil.getDifferenceBetweenDate(cdmTodate) >= 0
										&& numList.contains(fromNum)) {
									jsonObject.put("stagingDetailsCol",stagingDetailsCol);
									jsonObject.put("rowStagingTemp",rowStagingTemp);
									jsonObject.put("INST_CODE", "R");
									track++;
									this.saveStagingDetail(jsonObject);
								} else {
									
									jsonObject.put("stagingDetailsCol",
											stagingDetailsCol);
									jsonObject.put("rowStagingTemp",
											rowStagingTemp);
									jsonObject.put("INST_CODE", "C");
									track++;
									this.saveStagingDetail(jsonObject);
								}

							} else {
								
								jsonObject.put("stagingDetailsCol",
										stagingDetailsCol);
								jsonObject
										.put("rowStagingTemp", rowStagingTemp);
								jsonObject.put("INST_CODE", "C");
								track++;
								this.saveStagingDetail(jsonObject);
							}
						} else {
							
							jsonObject.put("stagingDetailsCol",
									stagingDetailsCol);
							jsonObject.put("rowStagingTemp", rowStagingTemp);
							jsonObject.put("INST_CODE", "C");
							track++;
							this.saveStagingDetail(jsonObject);
						}

					}
				}
			}
			if (track == 0) {
				this.updateFilesLib(jsonObject);
			}

			this.updateFilesProgressStatus(jsonObject);

			JSONObject mailObj = this.getUsernameAndEmailAddress(this.fileStatusPk);
			JSONObject mailConfig = this.getMailConfiguration();
			List<Map> mailObjList = (List<Map>) mailObj.get("notifycnf");
			if (mailObjList.size() != 0 && track!=0) {
				for (Map map : mailObjList) {
					List<String> message = this.getNotificationMessages(map.get("PK_CDM_NOTIFYCNF").toString());
					for (String msg : message) {
						String to = map.get("USER_EMAIL") == null ? null : map.get("USER_EMAIL").toString();
						String body = "Hello User" + "\n" + msg.toString();
						mailConfig.put("TO", to);
						mailConfig.put("BODY", body);
						if (to != null)
							try{
								CDMRUtil.sendNotificationMail(mailConfig);
								}
							catch(Exception e){
								System.out.println(e.getMessage());
								e.printStackTrace();
								System.out.println("mail not sent/issue in email server-----------------------------");
							}
						// CDMRUtil.sendNotificationMail(mailConfig.getString("FROM"),to, mailConfig.getString("HOST"),"CDM-R Aler" ,body);
					}
					this.updateNotification(map.get("PK_CDM_NOTIFYCNF").toString());
				}
			}

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, lcode, cdmrController,"GetAndProcessRemoteData::compareData", "1");
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * @description Compare Data of staging temp and cdm main
	 * @return void
	 * @throws Exception
	 */
	public void compareDataFlag(JSONObject jsonObject) throws Exception {

		int track = 0;
		List<Map> stagingTempList = new ArrayList();
		List<Map> cdmMainList = new ArrayList();
		Map mappingForCdmMainAndSagingTemp = new HashMap();
		Map maincdmColType = new HashMap();
		try {
			stagingTempList = (List<Map>) jsonObject.get("stagingTemp");

			mappingForCdmMainAndSagingTemp = (Map) jsonObject.get("mappingForCdmMainAndSagingTemp");
			maincdmColType = (Map) jsonObject.get("MAINCDM_COLTYPE");
			Iterator stagingTempIt = stagingTempList.iterator();

			String key = (String) mappingForCdmMainAndSagingTemp.get("LSERVICE_CODE");

			String isActiveYNMapping = mappingForCdmMainAndSagingTemp.get("IS_ACTIVE_YN").toString();

			JSONObject temp = new JSONObject();
			while (stagingTempIt.hasNext()) {

				boolean serviceCodeMatcherFlag = false;
				Map rowStagingTemp = (Map) stagingTempIt.next();
				Iterator cdmMainIt = cdmMainList.iterator();
				String lserviceCodeStagingTemp = (String) rowStagingTemp
						.get(key);

				String stgngIsActiveYN = rowStagingTemp.get(isActiveYNMapping) == null ? null
						: rowStagingTemp.get(isActiveYNMapping).toString();
				String cdmIsActiveYN = "";

				lcode = lserviceCodeStagingTemp;
				StringBuilder stagingDetailsCol = new StringBuilder();

				Iterator itr = mappingForCdmMainAndSagingTemp.entrySet()
						.iterator();
				while (itr.hasNext()) {
					Map.Entry entry = (Map.Entry) itr.next();
					String cdmMain = (String) entry.getKey();
					String sagingTemp = (String) entry.getValue();

					String colStagingTemp = (rowStagingTemp.get(sagingTemp)) == null ? ""
							: rowStagingTemp.get(sagingTemp).toString();
					String maincdmCol = (maincdmColType.get(cdmMain)) == null ? ""
							: maincdmColType.get(cdmMain).toString();

					if (maincdmCol.equalsIgnoreCase("NUMERIC")) {

						rowStagingTemp.put(sagingTemp, CDMRUtil
								.formateValue(colStagingTemp));
					}

				}

				String sql = jsonObject.getString("queryForCdmMain")
						+ " and LSERVICE_CODE = '" + lserviceCodeStagingTemp
						+ "'";
				temp.put("query", sql);
				temp.put("SQLQuery", true);
				temp = cdmrController.getCdmMain(temp);
				List<Map> data = (List<Map>) temp.get("cdmmain");
				if (data.size() == 0) {
					// New Row Data Will Be Added...............
					jsonObject.put("stagingDetailsCol", stagingDetailsCol);
					jsonObject.put("rowStagingTemp", rowStagingTemp);
					jsonObject.put("INST_CODE", "A");
					track++;
					this.saveStagingDetail(jsonObject);
				} else {
					serviceCodeMatcherFlag = true;
					Map rowcdmMain = data.get(0);

					String lserviceCodeCdmMain = (String) rowcdmMain.get("LSERVICE_CODE");
					try {
						cdmIsActiveYN = rowcdmMain.get("IS_ACTIVE_YN") == null ? null
								: rowcdmMain.get("IS_ACTIVE_YN").toString();
					} catch (Exception e) {
						CDMRUtil.errorLogUtil(e, lcode, cdmrController,
								"GetAndProcessRemoteData::compareData", "1");
						e.printStackTrace();
					}

					// for(Map.Entry entryMaincdm : maincdmColType.entrySet())
					Iterator it = mappingForCdmMainAndSagingTemp.entrySet()
							.iterator();
					while (it.hasNext()) {
						Map.Entry entry = (Map.Entry) it.next();
						String cdmMain = (String) entry.getKey();
						String sagingTemp = (String) entry.getValue();
                       
						String colStagingTemp = (rowStagingTemp.get(sagingTemp)) == null ? ""
								: rowStagingTemp.get(sagingTemp).toString();
						String colCdmMain = (rowcdmMain.get(cdmMain)) == null ? ""
								: rowcdmMain.get(cdmMain).toString();
						String maincdmCol = (maincdmColType.get(cdmMain)) == null ? ""
								: maincdmColType.get(cdmMain).toString();

						if (maincdmCol.equals("TIMESTAMP")
								&& !(colCdmMain.equals(""))
								&& !(colStagingTemp.equals(""))) {
							if (!this.compareDate(colStagingTemp, colCdmMain))
								stagingDetailsCol.append(
										(sagingTemp.substring(8))).append(",");
						} else if (maincdmCol.equalsIgnoreCase("NUMERIC")) {
							if (!colStagingTemp.equalsIgnoreCase(CDMRUtil
									.formateValue(colCdmMain)))
								stagingDetailsCol.append(
										(sagingTemp.substring(8))).append(",");
						} else if (!colStagingTemp.equalsIgnoreCase(colCdmMain)) {
							stagingDetailsCol.append((sagingTemp.substring(8)))
									.append(",");
						}

					}
					if (stagingDetailsCol.length() > 0) {
						stagingDetailsCol = stagingDetailsCol
								.deleteCharAt(stagingDetailsCol.length() - 1);

					}

					if (stagingDetailsCol.length() > 0
							&& serviceCodeMatcherFlag) {
						List<String> numList = Arrays.asList(stagingDetailsCol
								.toString().split(","));
						if (stgngIsActiveYN.equalsIgnoreCase("N") == true) {
							jsonObject.put("stagingDetailsCol",
									stagingDetailsCol);
							jsonObject.put("rowStagingTemp", rowStagingTemp);
							jsonObject.put("INST_CODE", "D");
							track++;
							this.saveStagingDetail(jsonObject);
						} else if (stgngIsActiveYN.equalsIgnoreCase("Y") == true
								&& cdmIsActiveYN.equalsIgnoreCase("N") == true) {

							jsonObject.put("stagingDetailsCol",
									stagingDetailsCol);
							jsonObject.put("rowStagingTemp", rowStagingTemp);
							jsonObject.put("INST_CODE", "R");
							track++;
							this.saveStagingDetail(jsonObject);

						} else if (stgngIsActiveYN.equalsIgnoreCase("Y") == true
								&& cdmIsActiveYN.equalsIgnoreCase("Y") == true) {
							jsonObject.put("stagingDetailsCol",
									stagingDetailsCol);
							jsonObject.put("rowStagingTemp", rowStagingTemp);
							jsonObject.put("INST_CODE", "C");
							track++;
							this.saveStagingDetail(jsonObject);
						}

					}

				}
			}

			if (track == 0) {
				this.updateFilesLib(jsonObject);
			}

			this.updateFilesProgressStatus(jsonObject);

			JSONObject mailObj = this.getUsernameAndEmailAddress(this.fileStatusPk);
			JSONObject mailConfig = this.getMailConfiguration();
			List<Map> mailObjList = (List<Map>) mailObj.get("notifycnf");
			if (mailObjList.size() != 0 && track!= 0) {
				for (Map map : mailObjList) {
					List<String> message = this.getNotificationMessages(map.get("PK_CDM_NOTIFYCNF").toString());
					for (String msg : message) {
						String to = map.get("USER_EMAIL") == null ? null : map.get("USER_EMAIL").toString();
						String body = "Hello User" + "\n" + msg.toString();
						mailConfig.put("TO", to);
						mailConfig.put("BODY", body);
						if (to != null)
							//CDMRUtil.sendNotificationMail(mailConfig);
							try{
								CDMRUtil.sendNotificationMail(mailConfig);
								}
							catch(Exception e){
								System.out.println(e.getMessage());
								e.printStackTrace();
								System.out.println("mail not sent/issue in email server-----------------------------");
							}
					}
					this.updateNotification(map.get("PK_CDM_NOTIFYCNF").toString());
				}
			}

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, lcode, cdmrController,
					"GetAndProcessRemoteData::compareData", "1");
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * @description Update Files Lib in Case Complete Data Processed
	 * @return void
	 * @throws Exception
	 */
	public void updateFilesProgressStatus(JSONObject jsonObject)
			throws Exception {
		try {
			String query = "update CDM_FILESLIB set IN_PROGRESS = 0 where PK_CDM_FILESLIB = "+ jsonObject.getString("pkfileID");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			cdmrController.updateFilesLibForRemoteData(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::updateFilesProgressStatus", "1");
			e.printStackTrace();
		}
	}

	/**
	 * @description Update Files Lib in Case of No new Data found
	 * @return void
	 * @throws Exception
	 */
	public void updateFilesLib(JSONObject jsonObject) throws Exception {
		try {
			String query = "update CDM_FILESLIB set IMPORT_STATUS = 1,  COMMENTS = 'No Data Found', FILE_DESC = 'No Data Found' where PK_CDM_FILESLIB = "
					+ jsonObject.getString("pkfileID");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			cdmrController.updateFilesLibForRemoteData(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::updateFilesLib", "1");
			e.printStackTrace();
		}
	}

	/**
	 * @description Compare date of staging temp and CDM Main
	 * @return boolean
	 * @throws Exception
	 */
	public boolean compareDate(String dateStagingtemp, String dateCdmMain)
			throws Exception {
		try {
			dateCdmMain = CDMRUtil.formateDate("yyyy-MM-dd HH:mm:ss",
					"dd-MMM-yyyy", dateCdmMain, "1");
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, lcode, cdmrController,
					"GetAndProcessRemoteData::compareDate", "1");
			e.printStackTrace();
		}
		if (dateStagingtemp.equalsIgnoreCase(dateCdmMain)) {

			return true;

		}

		return false;
	}

	/**
	 * @description Save Data in staging detail after comparison
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject saveStagingDetail(JSONObject jsonObject) throws Exception {
		try {
			jsonObject = this.createInsertQueryStagingDetail(jsonObject);
			jsonObject.put("SQLQuery", true);

			jsonObject = cdmrController.saveStagingDetail(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, lcode, cdmrController,
					"GetAndProcessRemoteData::saveStagingDetail", "1");
			e.printStackTrace();
		}
		return jsonObject;
	}

	/**
	 * @description Create Dynamic Query for submitting data in Stsaging Detail
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createInsertQueryStagingDetail(JSONObject jsonObject)
			throws Exception {
		try {
			Map rowStagingTemp = (Map) jsonObject.get("rowStagingTemp");
			Iterator it = rowStagingTemp.entrySet().iterator();
			StringBuilder names = new StringBuilder();
			StringBuilder values = new StringBuilder();
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				String key = (entry.getKey()) == null ? "" : entry.getKey()
						.toString();
				String value = (entry.getValue()) == null ? "" : entry
						.getValue().toString();
				names.append(key).append(",");
				values.append("'" + value.replaceAll("'", "''") + "',");

			}
			if (jsonObject.getString("INST_CODE").equalsIgnoreCase("C")) {
				names.append("CHG_LOG").append(",");
				values.append("'" + jsonObject.get("stagingDetailsCol") + "',");
				names.append("INST_CODE").append(",");
				values.append("'C',");

			} else if (jsonObject.getString("INST_CODE").equalsIgnoreCase("A")) {
				names.append("INST_CODE").append(",");
				values.append("'A',");
			} else if (jsonObject.getString("INST_CODE").equalsIgnoreCase("R")) {
				names.append("CHG_LOG").append(",");
				values.append("'" + jsonObject.get("stagingDetailsCol") + "',");
				names.append("INST_CODE").append(",");
				values.append("'R',");
			} else if (jsonObject.getString("INST_CODE").equalsIgnoreCase("D")) {
				names.append("CHG_LOG").append(",");
				values.append("'" + jsonObject.get("stagingDetailsCol") + "',");
				names.append("INST_CODE").append(",");
				values.append("'D',");
			}
			names = names.deleteCharAt(names.length() - 1);
			values = values.deleteCharAt(values.length() - 1);
			String query = "insert into CDM_STAGINGDETAIL ( PK_CDM_STAGINGDETAIL,FK_CDM_STAGINGMAIN, RECORD_TYPE, "
					+ names
					+ ") values (SEQ_CDM_STAGINGDETAIL.NEXTVAL, "
					+ jsonObject.getString("stagingMainID")
					+ ", 'D', "
					+ values.toString() + ")";

			jsonObject.put("query", query);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::createInsertQueryStagingDetail",
					"1");
			e.printStackTrace();
		}
		return jsonObject;
	}

	/**
	 * @description Get Columns Names from mapping for creating dynamic query
	 * @return String
	 * @throws Exception
	 */
	public String getColumnsNames(String colunNames) throws Exception {
		String cname = "";
		String names[];
		String $names[];
		try {
			names = colunNames.split("\\+");
			$names = colunNames.split("\\$");
		} catch (Exception e) {
			cname = "' '";
			return cname;
		}

		if ($names.length > 1) {
			if ($names[0].equals("SUBSTR")) {
				cname = "SUBSTR(" + $names[1] + "," + $names[2] + ","
						+ $names[3] + ")";
				return cname;
			}
		}

		if (names.length > 1) {
			for (int i = 0; i < names.length; i++) {
				cname = cname + names[i] + " ||";
			}
			cname = cname.substring(0, cname.length() - 3);
			cname = cname;
			return cname;

		} else if (names[0].equals("INST")) {
			cname = "INST_CODE";
			return cname;
		} else if (names[0].equals("DDE") || names[0].equals("")
				|| names[0] == null) {
			cname = "' '";
			return cname;
		} else if (names[0].equals("DDEI")) {
			cname = "0";
			return cname;
		} else {
			return names[0];
		}

	}

	/**
	 * @description Get Map version according file type
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getMapVersion(JSONObject jsonObj) throws Exception {

		try {
			String query = "select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID='"
					+ jsonObj.getString("FILE_ID") + "'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMapVersion(jsonObj);
			List<Map> list = (List<Map>) jsonObj.get("mapversion");
			Map dataMap = list.get(0);

			jsonObj.put("MAPSVERSION", dataMap.get("MAPSVERSION").toString());

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getMapVersion", "1");
			e.printStackTrace();
		}
		return jsonObj;
	}

	/**
	 * @description Create Dynamic Query for transferring data from staging
	 *              detail to Comprec
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createDynamicQueryForComreq(JSONObject jsonObject)
			throws Exception {
		try {
			List<StagingMaps> mapsObjList = (List<StagingMaps>) jsonObject
					.get("stagingcolumnmap");
			StringBuilder query = new StringBuilder();
			StringBuilder insertquery = new StringBuilder();
			int index = 1;
			query
					.append("select (SEQ_CDM_STAGINGCOMPREC.NEXTVAL) as pk, FK_CDM_STAGINGMAIN, PK_CDM_STAGINGDETAIL, CHG_LOG, ");
			insertquery
					.append("insert into CDM_STAGINGCOMPREC( PK_CDM_STAGINGCOMPREC, FK_CDM_STAGINGMAIN, FK_CDM_STAGINGDETAIL, CHG_LOG, ");

			for (StagingMaps stagingMaps : mapsObjList) {

				insertquery.append(stagingMaps.getEqCompReqCol() + ",");
				query.append(
						"("
								+ this.getColumnsNames(stagingMaps
										.getPullFromCols())).append(
						") as COL" + index + ",");
				index++;
			}
			query = query.deleteCharAt(query.length() - 1);
			insertquery = insertquery.deleteCharAt((insertquery.length() - 1));
			insertquery.append(")");
			query
					.append(" from CDM_STAGINGDETAIL WHERE RECORD_TYPE = 'D' and FK_CDM_STAGINGMAIN = "
							+ jsonObject.getString("stagingMainID"));
			jsonObject.put("query", insertquery + " " + query);

		} catch (Exception e) {
			CDMRUtil
					.errorLogUtil(
							e,
							"",
							cdmrController,
							"GetAndProcessRemoteData::createDynamicQueryForComreq",
							"1");
			e.printStackTrace();
			throw e;
		}
		return jsonObject;
	}

	/**
	 * @description Get Data from Code list
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getDataFromCodeList(JSONObject jsonObject)
			throws Exception {
		try {
			String query = "select PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC from vbar_codelst";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCodeList(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getDataFromCodeList", "1");
			e.printStackTrace();
			throw e;
		}
		return jsonObject;
	}

	/**
	 * @description Get file info from code list and update cdm fileslib
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getFileInfo(JSONObject jsonObject) throws Exception {

		try {
			List<Map> codeList = (List<Map>) jsonObject.get("codelist");
			String fileName = "";
			for (Map map : codeList) {
				if (map.get("CODELST_TYPE").toString().equals("filestatus")	&& map.get("CODELST_SUBTYP").toString().equals("new")) {
					fileName = jsonObject.getString("file_name")+ " " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
					jsonObject.put("fileId", jsonObject.getString("FILE_ID"));
					jsonObject.put("filename", fileName);
					jsonObject.put("fileDes", map.get("CODELST_DESC").toString());
					jsonObject.put("fileLocation", jsonObject.get("final_file_name"));
					this.fileStatusPk = map.get("PK_CODELST").toString();
					jsonObject.put("fileCurrentStatus", map.get("PK_CODELST").toString());
					jsonObject.put("comment", map.get("CODELST_DESC").toString());
					jsonObject.put("fkAccount", CDMRUtil.getFkAccount());
					jsonObject.put("createdOn", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
					jsonObject.put("lastModifiedOn", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date()));
					jsonObject.put("ipAddress", InetAddress.getLocalHost().getHostAddress());
					jsonObject.put("deletedFlag", 0);
				}

				if (map.get("CODELST_TYPE").toString().equals("sysac")) {
					jsonObject.put("createdBy", map.get("CODELST_SUBTYP").toString());
				}
			}

			cdmrController.saveCDMRFileInfo(jsonObject);

			this.saveStagingInfo(jsonObject);

			JSONObject object = new JSONObject();
			object.put("query", "update CDM_ERRORLOG set FILE_NAME = '"
					+ fileName + "' where TMP_IND = 1 and DELETEDFLAG = 0");
			object.put("SQLQuery", true);
			object = cdmrController.updateErrorLog(object);
			object
					.put("query",
							"update CDM_ERRORLOG set TMP_IND = 0 where TMP_IND = 1 and DELETEDFLAG = 0");
			object.put("SQLQuery", true);
			object = cdmrController.updateErrorLog(object);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getFileInfo", "1");
			e.printStackTrace();
			throw e;
		}

		return jsonObject;
	}

	/**
	 * @description Save information in Staging Main
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject saveStagingInfo(JSONObject jsonObject) throws Exception {
		try {
			List<Map> codeList = (List<Map>) jsonObject.get("codelist");
			for (Map map : codeList) {
				if (map.get("CODELST_TYPE").toString().equals("stagingtype")
						&& map.get("CODELST_SUBTYP").toString().equals("file")) {

					jsonObject.put("stagingType", map.get("PK_CODELST")
							.toString());
					jsonObject.put("stagingID", this.getStagingID());
					jsonObject.put("fkMapVersion", jsonObject
							.getString("MAPSVERSION"));
					jsonObject.put("fkAccount", CDMRUtil.getFkAccount());
					jsonObject.put("createdOn", new SimpleDateFormat(
							"MM/dd/yyyy HH:mm:ss").format(new Date()));
					jsonObject.put("lastModifiedOn", new SimpleDateFormat(
							"MM/dd/yyyy HH:mm:ss").format(new Date()));
					jsonObject.put("ipAddress", InetAddress.getLocalHost()
							.getHostAddress());
					jsonObject.put("deletedFlag", 0);
				}

				if (map.get("CODELST_TYPE").toString().equals("sysac")) {
					jsonObject.put("createdBy", map.get("CODELST_SUBTYP")
							.toString());
				}
			}

			jsonObject = cdmrController.saveStagingMainInfo(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::saveStagingInfo", "1");
			e.printStackTrace();

		}
		return jsonObject;
	}

	/**
	 * @description Get max Staging ID
	 * @return JSONObject
	 * @throws Exception
	 */
	public Long getStagingID() throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			String query = CDMRSqlConstants.MAX_STAGING_ID;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getStagingID(jsonObject);

			Map obj = ((List<Map>) jsonObject.get("staginid")).get(0);
			if ((obj.get("STAGINGID")) == null)
				return Long.parseLong("1");
			else
				return Long.valueOf(obj.get("STAGINGID").toString()) + 1;
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getStagingID", "1");
			e.printStackTrace();
		}

		return null;

	}

	public JSONObject getUsernameAndEmailAddress(String pkCodeList)
			throws Exception {
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "select PK_CDM_NOTIFYCNF, USER_NAME, USER_EMAIL from CDM_NOTIFYCNF where DELETEDFLAG=0 and STATE_TO = "
					+ pkCodeList;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromNotifycnf(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getUsernameAndEmailAddress", "1");
			e.printStackTrace();
		}
		return jsonObject;
	}

	public JSONObject getMailConfiguration() throws Exception {
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "select PARAM_SUBTYP, PARAM_VAL from vbar_controlparam where PARAM_TYPE in ('EMAIL') and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT', 'SSL') and DELETEDFLAG = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
			List<Map> data = (List<Map>) jsonObject.get("mailconfig");
			for (Map map : data) {
				jsonObject.put(map.get("PARAM_SUBTYP").toString(), map.get(
						"PARAM_VAL").toString());
			}
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getMailConfiguration", "1");
			e.printStackTrace();
		}
		return jsonObject;
	}

	public List<String> getNotificationMessages(String pkNotifyCnf)
			throws Exception {
		List<String> message = new ArrayList<String>();
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "select MSG_TEXT from CDM_NOTIFICATION where FK_CDM_NOTIFYCNF = "
					+ pkNotifyCnf
					+ " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getNotificationMessages(jsonObject);
			List<Map> data = (List<Map>) jsonObject.get("notimesg");
			for (Map map : data) {
				message.add(map.get("MSG_TEXT").toString());
			}
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getNotificationMessages", "1");
			e.printStackTrace();
		}

		return message;

	}

	public void updateNotification(String pkNotifyCnf) throws Exception {
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "update CDM_NOTIFICATION set SENT_STATUS = 'Y' where FK_CDM_NOTIFYCNF = "
					+ pkNotifyCnf
					+ " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.updateNotification(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::updateNotification", "1");
			e.printStackTrace();
		}

	}

	public void SendNotificationforError(JSONObject jsonObject) {
		try {

			JSONObject mailObj = this.getUsernameAndEmailAddress(Integer
					.toString(this.getErrorinFilePK(jsonObject)));
			JSONObject mailConfig = this.getMailConfiguration();
			List<Map> mailObjList = (List<Map>) mailObj.get("notifycnf");
			if (mailObjList.size() != 0) {
				for (Map map : mailObjList) {
					// List<String> message =
					// this.getNotificationMessages(map.get("PK_CDM_NOTIFYCNF").toString());
					// for (String msg : message) {
					String to = map.get("USER_EMAIL") == null ? null : map.get(
							"USER_EMAIL").toString();
					String body = "Hello User111111111111";
					mailConfig.put("TO", to);
					mailConfig.put("BODY", body);
					if (to != null)
						CDMRUtil.sendNotificationMail(mailConfig);
					// CDMRUtil.sendNotificationMail(mailConfig.getString("FROM"),
					// to, mailConfig.getString("HOST"),"CDM-R Aler" , body);
					// }
					// this.updateNotification(map.get("PK_CDM_NOTIFYCNF").toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public int getErrorinFilePK(JSONObject jsonObject) {
		int filestatus = 0;
		try {

			String query = "select PK_CODELST from VBAR_CODELST where CODELST_TYPE = 'filestatus' and CODELST_SUBTYP = 'err'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFilePkForStatus(jsonObject);
			// System.out.println(jsonObject);
			List<Map> data = (List<Map>) jsonObject.get("errorStatuspk");
			for (Map map : data) {
				filestatus = Integer.parseInt(map.get("PK_CODELST").toString());
			}

			return filestatus;

		} catch (Exception e) {

			e.printStackTrace();
		}
		return 0;

	}

	public JSONObject getFileSourceLocation(JSONObject jsonObject)
			throws Exception {

		try {
			String query = "select CODELST_DESC from VBAR_CODELST where CODELST_TYPE = 'FILE_SRC_LOC' and CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFileSourceLocation(jsonObject);
			List<String> fileSourceLocations = (List<String>) jsonObject
					.get("file_source_location");

			jsonObject.put("file_source_location", fileSourceLocations.get(0));

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getFileLocation", "1");
			e.printStackTrace();
		}
		return jsonObject;

	}

	public JSONObject getFileInformation(JSONObject jsonObject)
			throws Exception {

		try {
			String query = "select SHEET_NOS,TH_ROWS,LM_COLS,FILE_EXT from CDM_MAPSVERSION where IS_ACTIVE= '1' and FILE_ID='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFileInformation(jsonObject);

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getFileInformation", "1");
			e.printStackTrace();
		}

		return jsonObject;
	}

	public List<String> readAllFiles(JSONObject jsonObject) throws Exception {

		List<String> fileLocations = new ArrayList<String>();
		try {
			String fileType = jsonObject.getString("file_key");

			File folder = new File(jsonObject.getString("file_source_location"));
			File[] allFiles = folder.listFiles();
        
			for (int i = 0; i < allFiles.length; i++) {
				if (allFiles[i].isFile()) {
					String basename = FilenameUtils.getBaseName(allFiles[i]
							.getName());
					if (basename.toLowerCase().contains(fileType.toLowerCase())) {
						fileLocations.add(allFiles[i].getAbsolutePath());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileLocations;
	}

	public boolean checkForExistingFile(String filename) throws Exception {
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "select FILE_NAME from CDM_FILESLIB where FILE_LOCATION='"
					+ filename + "' and DELETEDFLAG = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			return cdmrController.checkForExistingFile(jsonObject);

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::checkForExistingFile", "1");
			e.printStackTrace();
		}

		return false;
	}

	public String getPullMethod(JSONObject jsonObject) throws Exception {
		String pullMethod = "";
		try {

			String query = "select CODELST_CUSTOM_COL from VBAR_CODELST where CODELST_TYPE = 'FILE_TYPE' and CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getPullMethod(jsonObject);
			List<Map> dataPullMethod = (List<Map>) jsonObject.get("pullmethod");
			if (dataPullMethod.size() > 0) {
				pullMethod = dataPullMethod.get(0).get("CODELST_CUSTOM_COL")
						.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return pullMethod;

	}

	public List<String> getInterfaceList() throws Exception {

		List<String> intfList = new ArrayList();
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "select FILE_ID from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID != 'ALLLIB'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			intfList = cdmrController.getInterfaceList(jsonObj);
			
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getInterfaceList", "1");
			e.printStackTrace();
		}
		return intfList;
	}

	public void moveFileInProcessedDir(String processedDir,
			String sourceFilePath) throws Exception {
		try {
			File source = new File(sourceFilePath);
			File destination = new File(processedDir);
			String appendfileName = new SimpleDateFormat("yyyyMMddhhmm")
					.format(new Date());
			if (!destination.exists()) {
				destination.mkdirs();
			}

			if (source.exists()) {

				if (source.renameTo(new File(destination + "/" + appendfileName
						+ "_" + source.getName()))) {
					System.out.println("File Moved Successfully");
				} else {
					System.out.println("File failed to move");
				}
			} else {
				System.out.println("Source file not found");
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	private String getProcessedDirLocation() {

		try {
			JSONObject jsonObject = new JSONObject();
			String query = "select CODELST_VALUE from VBAR_CODELST where CODELST_TYPE = 'FILE_PROC_LOC' and CODELST_SUBTYP='directory'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getProcessedDirLocation(jsonObject);
			System.out.println(jsonObject.get("processedDir"));
			return ((List<Map<String, String>>) jsonObject.get("processedDir"))
					.get(0).get("CODELST_VALUE").toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public JSONObject getFileName(JSONObject jsonObject) throws Exception {
		try {
			CDMRController cdmrController = new CDMRController();
			String query = "select CODELST_CUSTOM_COL from VBAR_CODELST where CODELST_TYPE = 'FILE_SRC_LOC' and CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFileName(jsonObject);
			List fileName = (List) jsonObject.get("file_name");
			jsonObject.put("file_name", ((Map<String, String>) fileName.get(0))
					.get("CODELST_CUSTOM_COL"));

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getFileName", "1");
			e.printStackTrace();
		}
		return jsonObject;

	}

	public JSONObject getChgIndc(JSONObject jsonObject) throws Exception {
		// String chgIndc = "";
		try {

			CDMRController cdmrController = new CDMRController();
			String query = CDMRSqlConstants.GET_CHG_IND + "'"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getChgIndc(jsonObject);
			List chgIndc = (List) jsonObject.get("chgind");
			jsonObject.put("chgind", ((Map<String, String>) chgIndc.get(0))
					.get("CODELST_DESC"));

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getChgIndc", "1");
			e.printStackTrace();
		}
		return jsonObject;

	}

	public JSONObject getFilekey(JSONObject jsonObject) throws Exception {

		try {

			CDMRController cdmrController = new CDMRController();
			String query = "select CODELST_CUSTOM_COL1 from VBAR_CODELST where CODELST_TYPE = 'FILE_SRC_LOC' and CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getMatchingCriteria(jsonObject);
			List filekey = (List) jsonObject.get("file_key");
			jsonObject.put("file_key", ((Map<String, String>) filekey.get(0))
					.get("CODELST_CUSTOM_COL1"));

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getMatchingCriteria", "1");
			e.printStackTrace();
		}
		return jsonObject;

	}

	/**
	 * @description Get event id
	 * @return JSONObject
	 * @throws Exception
	 */

	@SuppressWarnings("unchecked")
	public JSONObject getEventLibId(JSONObject jsonObject) throws Exception {
		try {

			CDMRController cdmrController = new CDMRController();
			String query = "select CODELST_DESC from VBAR_CODELST where CODELST_TYPE = 'EVENT_LIB_ID' and CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getEventLibId(jsonObject);

			List eventLibId = (List) jsonObject.get("eventLibID");
			jsonObject.put("eventLibID", ((Map<String, String>) eventLibId
					.get(0)).get("CODELST_DESC"));

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getEventId", "1");
			e.printStackTrace();
		}
		return jsonObject;

	}

	public JSONObject fetchRTRule(JSONObject jsonObject) throws Exception {

		try {
			String query = "select CODELST_DESC from VBAR_CODELST where CODELST_TYPE = 'ACTIVATION_RULE' and CODELST_SUBTYP = '"
					+ jsonObject.getString("RTYPE") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.fetchRTRule(jsonObject);
			List<Map> actrule = (List<Map>) jsonObject.get("rtyperule");
			jsonObject.put("RTRULE", actrule.get(0).get("CODELST_DESC")
					.toString());
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::fetchRTRule", "1");
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	
	
	/**
	 * @description Get mapping between col type and col seq
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject fetchcolTypeMapDetail(JSONObject jsonObject)
			throws Exception {
		try {
			jsonObject = this.getMapVersion(jsonObject);
			String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP
					+ " where fkMapVersion = "
					+ jsonObject.getString("MAPSVERSION");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", false);
			jsonObject = cdmrController.fetchcolTypeMapDetail(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::fetchcolTypeMapDetail", "1");
			e.printStackTrace();
			throw e;
		}

		return jsonObject;
		
	}

}