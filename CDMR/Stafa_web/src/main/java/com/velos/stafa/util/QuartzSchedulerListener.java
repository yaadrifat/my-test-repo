package com.velos.stafa.util;

import javax.servlet.ServletContextEvent;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import com.velos.stafa.action.cdmr.SchedularJobs;
import com.velos.stafa.action.cdmr.SchedularJobsExport;


public class QuartzSchedulerListener {

	 private Scheduler sched = null;
     private SchedulerFactory sf = null;
     
	public void contextDestroyed(ServletContextEvent arg0) {
		//
	}
 
	
	public QuartzSchedulerListener() {
		sf = new StdSchedulerFactory();
		try{
			sched = sf.getScheduler();
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	 public Scheduler startScheduler(String timer){
         try {
        	 
              JobDetail jd = new JobDetail("Lockjob", "Lockgroup", SchedularJobs.class);
              CronTrigger cronTrigger = new CronTrigger(
                        "Trigger",sched.DEFAULT_GROUP, timer);  
              sched.scheduleJob(jd, cronTrigger);
             
              sched.start();
         } catch (SchedulerException e) {
        	 e.printStackTrace();
             // logger.error("Error ..JobScheduler :"+e);
         } catch (Exception e) {
        	 e.printStackTrace();
              //logger.error("Error ..JobScheduler :"+e);
         }
         return sched;
    }
      
    public void rescheduleScheduler(String timer){
       try {
    	   
    	  this.shutDownScheduler();
    	  sf = new StdSchedulerFactory();
    	  sched = sf.getScheduler();
    	  JobDetail jd = new JobDetail("Lockjob", "Lockgroup", SchedularJobs.class);
          CronTrigger cronTrigger = new CronTrigger(
                    "Trigger",sched.DEFAULT_GROUP, timer);  
          sched.scheduleJob(jd, cronTrigger);
          sched.start();
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       
    }
    
    
    public void shutDownScheduler(){
        if(sched != null ){
             try {
                  if(!sched.isShutdown()){
                       sched.shutdown();
                  }
             } catch (SchedulerException e) {
           	  e.printStackTrace();
                  //logger.error("Error ..JobScheduler :"+e);
             }
        }
   }
    
    /*for expoting data to crms*/
    public void rescheduleSchedulerExport(String timer){
        try {
     	   
     	  this.shutDownScheduler();
     	  sf = new StdSchedulerFactory();
     	  sched = sf.getScheduler();
	     	 JobDetail jd1 = new JobDetail("Lockjob1", "Lockgroup1", SchedularJobsExport.class);
	         CronTrigger cronTrigger = new CronTrigger("Trigger1",sched.DEFAULT_GROUP, timer);
	         sched.scheduleJob(jd1, cronTrigger);
	         sched.start();
        } catch (Exception e) {
     	   e.printStackTrace();
        }
        
     }
    public Scheduler startSchedulerexport(String timer){
        try {
       	 
             JobDetail jd1 = new JobDetail("Lockjob1", "Lockgroup1", SchedularJobsExport.class);
             CronTrigger cronTrigger = new CronTrigger("Trigger1",sched.DEFAULT_GROUP, timer);
             sched.scheduleJob(jd1, cronTrigger);
            
             sched.start();
        } catch (SchedulerException e) {
       	 e.printStackTrace();
          
        } catch (Exception e) {
       	 e.printStackTrace();
             
        }
        return sched;
   }
}
