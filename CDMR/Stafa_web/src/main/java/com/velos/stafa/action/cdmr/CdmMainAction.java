package com.velos.stafa.action.cdmr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.helper.UploadAndProcessHelper;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.action.cdmr.GetAndProcessRemoteData;



/**
 * @summary     CDMRHomeAction
 * @description Insert Line Items from Comprec to Cdm Main table and update fields
 * @version     1.0
 * @file        CDMRHomeAction.java
 * @author      Lalit Chattar
 */
public class CdmMainAction extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * Parameter getting from request
	 */
	private Integer fileID;
	private Integer statusbar;
	private String text;
	private String fileStatus;
	private String value;
	

	/*
	 * Getter and Setter
	 */
	public Integer getFileID() {
		return fileID;
	}

	public void setFileID(Integer fileID) {
		this.fileID = fileID;
	}
	
	public Integer getStatusbar() {
		return statusbar;
	}

	public void setStatusbar(Integer statusbar) {
		this.statusbar = statusbar;
	}
	
	
	public CdmMainAction(){
		cdmrController = new CDMRController();
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return saveInCdmMain();
	}
	
	/**
	 * @description Save Line Items in CDM Main
	 * @return String
	 * @throws Exception
	 */
	public String saveInCdmMain()throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String query = CDMRSqlConstants.PROCESSING_FILE_INFO + jsonObject.getString("fileid");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCDMRFilesInfo(jsonObject);
			List<Map> fileinfo = (List<Map>)jsonObject.get("filesinfo");
			Map data = fileinfo.get(0);
			
			GetAndProcessRemoteData processremotadata = new GetAndProcessRemoteData();
			jsonObject = UploadAndProcessHelper.getMapVersion(jsonObject, data.get("FILE_ID").toString(), cdmrController);
			
			boolean chkType = this.checkStatsType(jsonObject.getString("statusbar"));
			if(chkType){
				
				jsonObject = fetchVersionID(jsonObject);
				
			
				String versionNumber = this.getCdmVersionNumber(jsonObject);
				
				Long versionId = this.getCdmVersionID(jsonObject);
				
				String versionDesc = this.getCdmVersionDesc();
				jsonObject.put("versionNo", versionNumber);
				jsonObject.put("versionDesc", versionNumber + "_" + versionDesc);
				jsonObject.put("versionId", versionId);
				jsonObject.put("versionName", "codelist");
				jsonObject.put("versionStatus", 1);
				jsonObject.put("creationDate", new SimpleDateFormat ("MM/dd/yyyy HH:mm").format(new Date()));
				jsonObject.put("isCurrent", 1);
				jsonObject.put("isClosed", 0);
				jsonObject.put("fkAccount", 1);
				jsonObject.put("file_id",(jsonObject.getString("VER_ID")));
				jsonObject = cdmrController.saveCdmVersionDef(jsonObject);
				jsonObject = this.updateCdmVersionDef(jsonObject);
				jsonObject = this.getCdmMainMapDetail(jsonObject);
				jsonObject = this.getStagingMainID(jsonObject);
				jsonObject = processremotadata.getChgIndc(jsonObject);
				
				jsonObject = this.createDynamicQuery(jsonObject);
				jsonObject = cdmrController.saveInMainCDM(jsonObject);
				this.updateForFkServiceSection(jsonObject);
				
			}
			
			jsonObject.put("desc", jsonObject.getString("text"));
			jsonObject.put("newStatus", jsonObject.getString("statusbar"));
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			Date parsedDate = formatter.parse(jsonObject.getString("lastModifiedOn"));
			jsonObject.put("lastModifiedOn", parsedDate);
			
//			query = "update CDM_FILESLIB set FILE_CUR_STATUS = " + jsonObject.getString("statusbar") + 
//			", COMMENTS = '"+jsonObject.getString("text")+"' where PK_CDM_FILESLIB = " + jsonObject.getString("fileID");
//			jsonObject.put("SQLQuery", true);
//			jsonObject.put("query", query);
//			jsonObject = cdmrController.updateFileStatus(jsonObject);
//			query = "update CDM_STAGINGMAIN set FILE_CUR_STATUS = " + jsonObject.getString("statusbar") + 
//			", COMMENTS = '"+jsonObject.getString("text")+"' where FK_CDM_FILESLIB = " + jsonObject.getString("fileID");
//			jsonObject.put("SQLQuery", true);
//			jsonObject.put("query", query);
			jsonObject = cdmrController.updateFileStatus(jsonObject);
			
			JSONObject mailObj = this.getUsernameAndEmailAddress(jsonObject.getString("newStatus"));
			JSONObject mailConfig = this.getMailConfiguration();
			List<Map> mailObjList = (List<Map>)mailObj.get("notifycnf");
			if(mailObjList.size() != 0){
				for (Map map : mailObjList) {
					List<String> message = this.getNotificationMessages(map.get("PK_CDM_NOTIFYCNF").toString());
					for (String msg : message) {
						String to = map.get("USER_EMAIL") == null?null:map.get("USER_EMAIL").toString();
						String body = "Hello User " + "\n"+ msg.toString();
						mailConfig.put("TO", to);
						mailConfig.put("BODY", body);
						if(to != null)
							try{
								CDMRUtil.sendNotificationMail(mailConfig);
								}
							catch(Exception e){
								System.out.println(e.getMessage());
								System.out.println("mail not sent/issue in email server-----------------------------");
							}
					}
					this.updateNotification(map.get("PK_CDM_NOTIFYCNF").toString());
				}
			}
			
			if(chkType){
			
				//jsonObject = cdmrController.updateFileStatus(jsonObject);
				this.getNextPossibleStatus(jsonObject);
				jsonObject.put("desc", this.fileStatus);
				jsonObject.put("newStatus", this.value);
//				query = "update CDM_FILESLIB set FILE_CUR_STATUS = " + this.value + 
//				", COMMENTS = '"+this.fileStatus+"' where PK_CDM_FILESLIB = " + jsonObject.getString("fileID");
//				jsonObject.put("SQLQuery", true);
//				jsonObject.put("query", query);
//				jsonObject = cdmrController.updateFileStatus(jsonObject);
//				query = "update CDM_STAGINGMAIN set FILE_CUR_STATUS = " + this.value + 
//				", COMMENTS = '"+this.fileStatus+"' where FK_CDM_FILESLIB = " + jsonObject.getString("fileID");
//				jsonObject.put("SQLQuery", true);
//				jsonObject.put("query", query);
				jsonObject = cdmrController.updateFileStatus(jsonObject);
				mailObj = this.getUsernameAndEmailAddress(this.value);
				mailConfig = this.getMailConfiguration();
				mailObjList = (List<Map>)mailObj.get("notifycnf");
				if(mailObjList.size() != 0){
					for (Map map : mailObjList) {
						List<String> message = this.getNotificationMessages(map.get("PK_CDM_NOTIFYCNF").toString());
						for (String msg : message) {
							String to = map.get("USER_EMAIL") == null?null:map.get("USER_EMAIL").toString();
							String body = "Hello User " + "\n"+ msg.toString();
							mailConfig.put("TO", to);
							mailConfig.put("BODY", body);
							if(to != null)
								try{
									CDMRUtil.sendNotificationMail(mailConfig);
									}
								catch(Exception e){
									System.out.println(e.getMessage());
									System.out.println("mail not sent/issue in email server-----------------------------");
								}
						}
						this.updateNotification(map.get("PK_CDM_NOTIFYCNF").toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**
	 * @description Get Mapping Details of CDM Main
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getCdmMainMapDetail(JSONObject jsonObject)throws Exception {
		try {
			
			String query = CDMRSqlConstants.GET_STAGING_MAPS + " where fkMapVersion = " + jsonObject.getString("MAPSVERSION");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", false);
			
			jsonObject =  cdmrController.getStagingColumnMapDetail(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	
	/**
	 * @description Get Next Possible status for line items in staging
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getNextPossibleStatus(JSONObject jsonObj) throws Exception{
		try{
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			String query = CDMRSqlConstants.SELECT_NEXT_POSSIBLE_STATUS + jsonObj.getString("fileid");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getNextPossibleStatus(jsonObj);
			List<Map> dataMap = (List<Map>)jsonObj.get("possiblestatus");
			for (Map map : dataMap) {
				getFileStatus(map.get("NXT_POSS_STATE").toString(),codelist);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonObj;
	}
	
	/**
	 * @description Get File Current Status
	 * @return JSONObject
	 * @throws Exception
	 */
	public void getFileStatus(String status, List<CodeList> codeList) throws Exception{
		for (CodeList codeListObj : codeList) {
			if(codeListObj.getPkCodeList() == Long.parseLong(status)){
				this.fileStatus = codeListObj.getCodeListDesc();
				this.value = codeListObj.getPkCodeList().toString();
			}
		}
	}
	
	/**
	 * @description Create Dynamic Query for Transferring data from Comprec to CDM Main
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createDynamicQuery(JSONObject jsonObject) throws Exception{
		try {
			List<StagingMaps> list = (List<StagingMaps>)jsonObject.get("stagingcolumnmap");
			StringBuffer insertQuery = new StringBuffer();
			StringBuffer selectQuery = new StringBuffer();
			insertQuery.append("insert into CDM_MAIN(PK_CDM_MAIN, CUR_VERSION, FK_ACCOUNT, CHG_IND, ");
			selectQuery.append("select (SEQ_CDM_STAGINGCOMPREC.NEXTVAL) as pk, "+jsonObject.getString("versionId")+" as cur_version, "+ CDMRUtil.getFkAccount()+",'" +jsonObject.getString("chgind")+ "',");
			for (StagingMaps stagingMaps : list) {
				
				if(stagingMaps.getEqMainCdmCol() == null || stagingMaps.getEqMainCdmCol().equals("") || stagingMaps.getEqMainCdmCol().equals(" ")){
					
					continue;
				}else{
					
					String checkFornull = ((stagingMaps.getMainCdmColType() == null) ? "" : stagingMaps.getMainCdmColType().toString());
					if(checkFornull.equals("TIMESTAMP") && !(checkFornull.equals(""))){
						insertQuery.append(stagingMaps.getEqMainCdmCol() + ",");
						selectQuery.append("case when "+stagingMaps.getEqCompReqCol()+" = ' ' then NULL else TO_TIMESTAMP ("+stagingMaps.getEqCompReqCol()+", 'DD-MM-YYYY HH24:MI:SS.FF') end as effdate,");
						
					}else{
						insertQuery.append(stagingMaps.getEqMainCdmCol() + ",");
						selectQuery.append("case when to_char("+stagingMaps.getEqCompReqCol()+")= ' ' then "+null+" else "+stagingMaps.getEqCompReqCol()+ " end ,");
					}
					
				}
			}
			selectQuery.deleteCharAt(selectQuery.length()-1);
			insertQuery.deleteCharAt(insertQuery.length()-1);
			insertQuery.append(")");
			selectQuery.append(" from CDM_STAGINGCOMPREC where FK_CDM_STAGINGMAIN = " + jsonObject.getDouble("staginid"));
			jsonObject.put("query", insertQuery + " " + selectQuery);
			jsonObject.put("SQLQuery", true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	/**
	 * @description Get Staging main ID
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getStagingMainID(JSONObject jsonObject) throws Exception{
		try {
			String query = CDMRSqlConstants.GET_STAGING_MAIN_ID + jsonObject.getString("fileid");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getStagingID(jsonObject);
			List<Map> data = (List<Map>)jsonObject.get("staginid");
			Map obj = data.get(0);
			jsonObject.put("staginid", obj.get("PK_CDM_STAGINGMAIN").toString());
		} catch (Exception e) {
			e.printStackTrace();
			
			
		}
		return jsonObject;
	}
	
	/**
	 * @description Get Type of Status for processing Data
	 * @return boolean
	 * @throws Exception
	 */
	public boolean checkStatsType(String statusCode) throws Exception{
		try {
			httpSession = request.getSession();
			List<Object> codeListObj = (List<Object>)httpSession.getAttribute("codelist");
			Iterator<Object> iterator = codeListObj.iterator();
			while (iterator.hasNext()) {
				CodeList codeList = (CodeList) iterator.next();
				if(codeList.getPkCodeList() == Long.parseLong(statusCode)){
					if(codeList.getCodeListSubType().equals("approved")){
						return true;
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	/**
	 * @description Get CDM version ID
	 * @return Long
	 * @throws Exception
	 */
	public Long getCdmVersionID(JSONObject jsonObject) throws Exception{
		try {
			//JSONObject jsonObject = new JSONObject();
			String query = CDMRSqlConstants.MAX_CDM_VERSION_ID + " WHERE FILE_ID ='"+jsonObject.getString("VER_ID")+"'";
				
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCDMVersionDetail(jsonObject);
			
			if(((List<Map>)jsonObject.get("versioninfo")).isEmpty()){
				return Long.parseLong("1");
			}
			Map obj = ((List<Map>)jsonObject.get("versioninfo")).get(0);
			if((obj.get("VERSIONID")) == null)
				return Long.parseLong("1");
			else
				return Long.valueOf(obj.get("VERSIONID").toString()) + 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @description Get CDM Version Number 
	 * @return String
	 * @throws Exception
	 */
	public String getCdmVersionNumber(JSONObject jsonObject) throws Exception{
		try {
			//JSONObject jsonObject = new JSONObject();
			//String query = CDMRSqlConstants.MAX_CDM_VERSION_NO;
			
			String query = "select VERSION_NO as versionno from cdm_versiondef where VERSION_ID = (select max(VERSION_ID) as versionid from cdm_versiondef WHERE FILE_ID ='"+jsonObject.getString("VER_ID")+"') and FILE_ID ='"+jsonObject.getString("VER_ID")+"'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCDMVersionDetail(jsonObject);
			
			if(((List<Map>)jsonObject.get("versioninfo")).isEmpty()){
				return "1.0.1";
			}
			Map obj = ((List<Map>)jsonObject.get("versioninfo")).get(0);
			if((obj.get("VERSIONNO")) == null){
				return "1.0.1";
			}
			else{
//				String[]  codeArry = obj.get("VERSIONNO").toString().split("\\.");
//				if(Integer.parseInt(codeArry[2]) == 9){
//					int digit = Integer.parseInt(codeArry[1])  + 1;
//					return "1." + digit + ".1";
//				}else{
//					int digit = Integer.parseInt(codeArry[2])  + 1;
//					String str = obj.get("VERSIONNO").toString().substring(0,(obj.get("VERSIONNO").toString().length() - 1));
//					return str + digit;
//				}
				
				String version = obj.get("VERSIONNO").toString();
				version = version.replace(".", "");
				int ver = Integer.parseInt(version) + 1;
				char[] charArray = Integer.toString(ver).toCharArray();
				String finaver = "";
				for (int i = 0; i < charArray.length; i++) {
					finaver = finaver + charArray[i] + ".";
				}
				return finaver.substring(0, finaver.length()-1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @description Get CDM Version Description 
	 * @return String
	 * @throws Exception
	 */
	public String getCdmVersionDesc() throws Exception{
		  try {
			  Date dNow = new Date( );
		      SimpleDateFormat ft = new SimpleDateFormat ("ddMMMyyyy");
		      return ft.format(dNow);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * @description Update CDM Version Defination
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject updateCdmVersionDef(JSONObject jsonObject) throws Exception{
		try {
			//String query = CDMRSqlConstants.UPDATE_CDMR_VERSION_INFO + "("+jsonObject.getString("versionId")+")";
			
			String query = CDMRSqlConstants.UPDATE_CDMR_VERSION_INFO + "("+jsonObject.getString("versionId")+") and file_id = '"+jsonObject.getString("VER_ID")+"'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.updateCdmVersionDef(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonObject;
	}
	
	
	public void updateForFkServiceSection(JSONObject jsonObject) throws Exception{
		try {
			
			String query = "select PK_CDM_MAIN, LSERVICE_CODE from CDM_MAIN where EVENT_LIB_ID = '"+jsonObject.getString("VER_ID")+"' and CUR_VERSION = " + jsonObject.getString("versionId");
						
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromCdmMain(jsonObject);
			List<Map> dataList = (List<Map>)jsonObject.get("cdmmain");
			for (Map map : dataList) {
				query = "update CDM_MAIN set FK_CDM_GRPCODE = (select FK_CDM_GRPCODE from CDM_MAIN where PK_CDM_MAIN = (Select MAX(PK_CDM_MAIN) from cdm_main where (  CUR_VERSION < "+jsonObject.getString("versionId")+") and lservice_code = '"+map.get("LSERVICE_CODE").toString()+"' and CHG_IND = '"+jsonObject.getString("chgind")+"' and EVENT_LIB_ID = '"+jsonObject.getString("VER_ID")+"' group by LSERVICE_CODE)) where PK_CDM_MAIN = " + map.get("PK_CDM_MAIN").toString();
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.editServiceSection(jsonObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JSONObject getUsernameAndEmailAddress(String pkCodeList) throws Exception{
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "select PK_CDM_NOTIFYCNF, USER_NAME, USER_EMAIL from CDM_NOTIFYCNF where DELETEDFLAG=0 and STATE_TO = " + pkCodeList;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromNotifycnf(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::getUsernameAndEmailAddress","1");
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	
	 public  JSONObject getMailConfiguration() throws Exception{
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "select PARAM_SUBTYP, PARAM_VAL from vbar_controlparam where PARAM_TYPE in ('EMAIL') and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT','SSL') and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("mailconfig");
				for (Map map : data) {
					jsonObject.put(map.get("PARAM_SUBTYP").toString(), map.get("PARAM_VAL").toString());
				}
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::getMailConfiguration","1");
				e.printStackTrace();
			}
			return jsonObject;
	  }
	  
	  public  List<String> getNotificationMessages(String pkNotifyCnf) throws Exception{
		  List<String> message = new ArrayList<String>();
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "select MSG_TEXT from CDM_NOTIFICATION where FK_CDM_NOTIFYCNF = " + pkNotifyCnf + " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getNotificationMessages(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("notimesg");
				for (Map map : data) {
					message.add(map.get("MSG_TEXT").toString());
				}
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::getNotificationMessages","1");
				e.printStackTrace();
			}
			
			return message;
			
	  }
	  
	  
	  public  void updateNotification(String pkNotifyCnf) throws Exception{
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "update CDM_NOTIFICATION set SENT_STATUS = 'Y' where FK_CDM_NOTIFYCNF = " + pkNotifyCnf + " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateNotification(jsonObject);
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::updateNotification","1");
				e.printStackTrace();
			}
			
	  }
	  
	  /**
		 * @description Get CDM Version Number 
		 * @return String
		 * @throws Exception
		 */
		public JSONObject fetchVersionID(JSONObject jsonObject) throws Exception{
			try {
				
				String query = "SELECT CODELST_DESC FROM VBAR_CODELST where CODELST_TYPE = 'EVENT_LIB_ID' and CODELST_SUBTYP = '"
				+ jsonObject.getString("FILE_ID") + "'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.fetchVersionID(jsonObject);
				
				List<Map> chgidc = (List<Map>) jsonObject.get("verid");
				jsonObject.put("VER_ID", chgidc.get(0).get("CODELST_DESC").toString());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObject;
		}
		
}
