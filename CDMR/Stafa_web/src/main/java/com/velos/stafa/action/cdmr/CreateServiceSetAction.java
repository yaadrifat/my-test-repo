package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;

/**
 * @summary     CreateServiceSetAction
 * @description Get uploaded file info from DB and show in datatable
 * @version     1.0
 * @file        CreateServiceSetAction.java
 * @author      Lalit Chattar
 */
public class CreateServiceSetAction extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	
	private boolean checkForExisting;
	

	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession = null;
	/*
	 * getter and setter
	 */
	

	/*
	 * Constructor
	 */
	public CreateServiceSetAction(){
		cdmrController = new CDMRController();
		
	}
	
	
	public boolean isCheckForExisting() {
		return checkForExisting;
	}

	public void setCheckForExisting(boolean checkForExisting) {
		this.checkForExisting = checkForExisting;
	}
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		return SUCCESS;
	}
	
	
	/**
	 * @description Create New Service Set
	 * @return String
	 * @throws Exception
	 */
	public String createServiceSet() throws Exception{
		JSONObject jsonObject = new JSONObject();
		jsonObject = VelosUtil.constructJSON(request);
		jsonObject = CDMRUtil.fetchEventLibId(jsonObject);
		try{
			this.checkForExistingSet(jsonObject);
			if(!this.checkForExisting){
				return SUCCESS;
			}
			String maxId = this.createNewSSID(jsonObject);
			jsonObject = CDMRUtil.getCurrentVersion(jsonObject);
			jsonObject.put("fkAccount", 1);
			jsonObject.put("lServiceCode", maxId);
			jsonObject.put("serviceCode", maxId);
			jsonObject.put("currentVersion", jsonObject.getString("versionid"));
			jsonObject.put("isThisSS", 1);
			jsonObject.put("serviceDesc1", jsonObject.getString("servsetname"));
			jsonObject.put("serviceDesc2", jsonObject.getString("servsetname"));
			jsonObject.put("eventlibid", jsonObject.getString("EVENT_ID"));
			cdmrController.saveNewServiceSet(jsonObject);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * @description Create New Service Set ID
	 * @return String
	 * @throws Exception
	 */
	public String createNewSSID(JSONObject jsonObject) throws Exception{
		try {
			String codeString = "";
			httpSession = request.getSession();
			List<Object> codeList = (List<Object>)httpSession.getAttribute("codelist");
			Iterator iterator = codeList.iterator();
			while (iterator.hasNext()) {
				CodeList object = (CodeList) iterator.next();
				if(object.getCodeListType().equals("namefmt") && object.getCodeListSubType().equals("nm2")){
					codeString = object.getCodeListCustomCol();
				}
				
			}
			String query = "Select to_char((to_number(Substr(max(SERVICE_CODE),-5,5))+1),'FM0000000') as MAXID from cdm_main where service_code like 'SS%'";
			jsonObject.put("SQLQuery", true);
			jsonObject.put("query", query);
			jsonObject = cdmrController.getFromCdmMain(jsonObject);
			List<Map> listObj = (List<Map>)jsonObject.get("cdmmain");
			if(listObj.get(0).get("MAXID") == null || listObj.get(0).get("MAXID").toString().equals("")){
				return codeString + "0000001";
			}
			else{
				return codeString + listObj.get(0).get("MAXID").toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	/**
	 * @description Create latest version of Service set
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
		try {
			String query = "select version_id from cdm_versiondef where is_current = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("versioninfo");
			Map dataMap = list.get(0);
			jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}*/
	
	/**
	 * @description Check For existing Sets
	 * @return void
	 * @throws Exception
	 */
	public void checkForExistingSet(JSONObject jsonObj) throws Exception{
		try {
			String query = "select * from cdm_main where TRIM(LOWER(SERVICE_DESC1)) = '" + jsonObj.getString("servsetname").toLowerCase().trim() + "' and IS_THIS_SS = 1 and DELETEDFLAG = 0 and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("cdmmain");
			if(list.size() > 0){
				this.setCheckForExisting(false);
			}else{
				this.setCheckForExisting(true);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}