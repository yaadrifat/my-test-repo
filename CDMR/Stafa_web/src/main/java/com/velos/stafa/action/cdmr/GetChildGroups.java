package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;



/**
 * @summary     GetChildGroups
 * @description Load chiled Group(Services Section) in tree view
 * @version     1.0
 * @file        GetChildGroups.java
 * @author      Lalit Chattar
 */
public class GetChildGroups extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 private int pkId;
	 private List<Map<String, String>> dataList;
	 private Set idSet;
	 private int constant, end, start;
	 private Integer grpIdLength;
	 private Boolean chekType;
	/*
	 * Getter and Setter

	*/
	
	 public int getPkId() {
			return pkId;
	 }
	 public void setPkId(int pkId) {
			this.pkId = pkId;
	 }
	 public List<Map<String, String>> getDataList() {
			return dataList;
	 }
	 public void setDataList(List<Map<String, String>> dataList) {
		this.dataList = dataList;
	 }
	 public Boolean getChekType() {
		return chekType;
	 }

	public void setChekType(Boolean chekType) {
		this.chekType = chekType;
	}
	public Set getIdSet() {
		return idSet;
	}
	public void setIdSet(Set idSet) {
		this.idSet = idSet;
	}
	public GetChildGroups(){
		cdmrController = new CDMRController();
		dataList = new ArrayList<Map<String,String>>();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * @description Get Child Group (Sewrvice Section) in tree View
	 * @return String
	 * @throws Exception
	 */
	public String getChildGroups()throws Exception {
	
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			idSet = new HashSet();
			idSet.add(jsonObj.getString("pkId"));
			idSet = this.getAllGroupLevels(idSet);
			grpIdLength =0;
			for(;;){
				if(idSet.size() == grpIdLength){
					break;
				}else{
					grpIdLength = idSet.size();
					idSet = this.getAllGroupLevels(idSet);
				}
				
			}
			
			if(idSet.size() > 1){
				String query = "select PK_CDM_GRPCODE, GRP_DISP_NAME from CDM_GRPCODE where PARENT_NODE_ID in("+jsonObj.getString("pkId")+")";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getGroups(jsonObj);
				dataList = (List<Map<String, String>>)jsonObj.get("groupcode");
				chekType = true;
			}else{
				
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**
	 * @description Delete Group(Service Section)
	 * @return String
	 * @throws Exception
	 */
	public String deleteChildGroups()throws Exception {
		
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			idSet = new HashSet();
			idSet.add(jsonObj.getString("pkId"));
			idSet = this.getAllGroupLevels(idSet);
			grpIdLength =0;
			for(;;){
				if(idSet.size() == grpIdLength){
					break;
				}else{
					grpIdLength = idSet.size();
					idSet = this.getAllGroupLevels(idSet);
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	

	/**
	 * @description Get All Group(Service Section ) and Child Service Section
	 * @return Set
	 * @throws Exception
	 */
	public Set getAllGroupLevels(Set idSet) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = CDMRSqlConstants.GET_GROUP_PARENT_ID+"("+StringUtils.join(idSet, ",")+")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getGroups(jsonObj);
			List<Map> listData = (List<Map>)jsonObj.get("groupcode");
			for (Map map : listData) {
				idSet.add(map.get("PK_CDM_GRPCODE").toString());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idSet;
	}
}
