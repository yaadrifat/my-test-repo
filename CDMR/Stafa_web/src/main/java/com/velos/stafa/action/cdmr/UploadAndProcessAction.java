package com.velos.stafa.action.cdmr;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.velos.stafa.util.CDMRUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;

import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.helper.UploadAndProcessHelper;
import com.velos.stafa.util.CDMRExcelParser;

public class UploadAndProcessAction extends StafaBaseAction{
	
	private File fileUpload;
	private String fileUploadContentType;
	private String fileUploadFileName;
	
	private InputStream inputStream;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession = null;
	CDMRController cdmrController;
	
	
	public UploadAndProcessAction(){
		cdmrController = new CDMRController();
	}
	/*
	 * Getter and Setter
	 */
    public InputStream getInputStream() {
       return inputStream;
    }
 
	public String getFileUploadContentType() {
		return fileUploadContentType;
	}
 
	public void setFileUploadContentType(String fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}
 
	public String getFileUploadFileName() {
		return fileUploadFileName;
	}
 
	public void setFileUploadFileName(String fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}
 
	public File getFileUpload() {
		return fileUpload;
	}
 
	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}
	
	/**
	 * @description upload xls/xlsx file on server
	 * @return String
	 * @throws Exception
	 */
	public String uploadFile() throws Exception{
		Long start = new Date().getTime();
		String status = "";
		httpSession = request.getSession();
		
		
		
		
		status="<div id='message'>successfully uploaded</div>";
		inputStream = new ByteArrayInputStream(status.getBytes());
		if(null != this.getFileUploadFileName()){
			
			
			try{
				
				
				
				JSONObject jsonObject = new JSONObject();
				
				jsonObject = VelosUtil.constructJSON(request);
				jsonObject.put("filename", this.fileUploadFileName);
				
				jsonObject = UploadAndProcessHelper.getFileInfo(jsonObject, httpSession);
				File uploadFile = new File(jsonObject.getString("fileDest") + "/", this.fileUploadFileName);
				FileUtils.copyFile(this.fileUpload, uploadFile);
				jsonObject.put("fkAccount", CDMRUtil.getFkAccount());
				cdmrController.saveCDMRFileInfo(jsonObject);
				
				jsonObject = UploadAndProcessHelper.getMapVersion(jsonObject, 
						this.fileUploadFileName, cdmrController);
				
				jsonObject = UploadAndProcessHelper.saveStagingInfo(jsonObject, 
						httpSession, cdmrController);
				
				jsonObject = cdmrController.saveStagingMainInfo(jsonObject);
				
				//jsonObject = UploadAndProcessHelper.getCompReqColumnMapDetail(jsonObject, cdmrController);
				
				CDMRExcelParser cdmXlsParser = new CDMRExcelParser();
				cdmXlsParser.ReadAndParseExcel(jsonObject, cdmrController);
				
				//CdmRParseExcel cdmXlsParser = new  CdmRParseExcel();
				//cdmXlsParser.ReadAndParseExcel(jsonObject);
				
				
				jsonObject = UploadAndProcessHelper.getStagingColumnMapDetail(jsonObject, cdmrController);
				jsonObject = UploadAndProcessHelper.createDynamicQuery(jsonObject);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.saveInStagingCompReq(jsonObject);				
			}catch (Exception e) {
				e.printStackTrace();
			}
				
			
			
			
		}
		Long end = new Date().getTime();
		
		return SUCCESS;
	}
 
}