package com.velos.stafa.action;


import java.io.File;
import java.lang.reflect.Field;
import java.security.KeyPair;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.business.util.WebserviceUtil;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.objects.UserDetailsObject;
import com.velos.stafa.util.JCryptionUtil;
import com.velos.stafa.util.VelosStafaConstants;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.eSecurity.ESecurityConstants;


public class VelosCommonAction extends StafaBaseAction{
	
	public static final Log log=LogFactory.getLog(VelosCommonAction.class);
	
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	String userAuthentication;
	String path;
	private List resultList;
	private int returnCount;
	private String nextFlow;

	private String eSignFlag;
	private List trackerList;
	
	private String verifyStatus;
	private List nextFlowList;
	private String moduleRights;
	
	public String fetchRights() throws Exception{
		String moduleName = request.getParameter("moduleName");
		moduleRights ="";
		if(moduleName != null && !moduleName.trim().equals("")){
			HttpSession session=request.getSession(false);
			UserDetailsObject userObject = null;
			if (session!=null){
				userObject = (UserDetailsObject) session.getAttribute(VelosStafaConstants.USER_OBJECT);
				if(userObject != null && userObject.getCustomerId() !=null  ){
					System.out.println(" user info " + userObject);
					Map moduleMap= userObject.getModuleMap();
					System.out.println(" moduleMap " + moduleMap);	
					System.out.println(moduleName + " / " + moduleMap.get(moduleName));
					moduleRights= (String)moduleMap.get(moduleName);
					System.out.println(" module rights " + moduleRights);
				} 
				
				
			}
		}
		System.out.println(" end " + moduleRights);
		return SUCCESS;
	}
	public String verifyUserESign()throws Exception{
		String eSign="";
		String custom3Id="";
		try{
			String jsonData = request.getParameter("jsonData");
			JSONObject jsonObject = new JSONObject(jsonData);
			
			eSign = jsonObject.getString("eSign");
			custom3Id = jsonObject.getString("custom3Id");
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			System.out.println("eSign=>"+eSign+"custom3Id==>"+custom3Id);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ser='http://service.aithent.com/'>");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:validateSign>");
				xmlIp.append("<"+ESecurityConstants.esign+">"+eSign+"</"+ESecurityConstants.esign+">");
				xmlIp.append("<"+ESecurityConstants.clientUserId+">"+custom3Id+"</"+ESecurityConstants.clientUserId+">");
				xmlIp.append(" </ser:validateSign></soapenv:Body></soapenv:Envelope>");
				System.out.println("xmlIp==>"+xmlIp);
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				String strXmlValue = WebserviceUtil.getXmlValue(result, "result","statusCode");
				System.out.println("strXmlValue  : "+strXmlValue);
					if(strXmlValue!=null && strXmlValue.equalsIgnoreCase("fail")){
						eSignFlag = "false";
					}else{
						eSignFlag = "true";
					}
					log.trace("eSignFlag-->"+eSignFlag);
			}
			if(eSecurityFlag.equalsIgnoreCase("false")){
				eSignFlag = "true";
			}
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS; 
	}
	public String verifyCustomerId()throws Exception{
		
		verifyStatus ="false";
		String jsonData = request.getParameter("jsonData");
		JSONObject jsonObject = new JSONObject(jsonData);
		
		String customerId = jsonObject.getString("customerId");
		if(customerId != null && !customerId.trim().equals("")){
			HttpSession session=request.getSession(false);
			UserDetailsObject userObject = null;
			if (session!=null){
				userObject = (UserDetailsObject) session.getAttribute(VelosStafaConstants.USER_OBJECT);
				if(userObject != null && userObject.getCustomerId() !=null && userObject.getCustomerId().equals(customerId) ){
					verifyStatus = "true";
				}
			}
		}
		return SUCCESS;
	}
	public String autoLogOof()throws Exception{
		return "autoSessionError";
	}
	public String deleteRecords() throws Exception {
		try{
			System.out.println(" delete ");
			JSONObject jsonDataObject = new JSONObject(); 
			JSONArray queries = new JSONArray();
			String jsonData = request.getParameter("jsonData");
				
			JSONObject jsonObject = new JSONObject(jsonData);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			String deletedIds = jsonObject.getString("deletedIds");
			if(deletedIds != null && !deletedIds.trim().equals("")){
			
				String domainName = jsonObject.getString("domainName");
				
				String domainKey = jsonObject.getString("domainKey");
				// get from refelection 
			//	Field[] fields = VelosStafaConstants.class.getFields();
				System.out.println("domainName  " + domainName + "/ " +domainKey ) ;
				String query = "update ";
				Class  constantsClass = VelosStafaConstants.class;
				Field field = constantsClass.getField(domainName);
				Object value = field.get(null);
				System.out.println(" constant value " + value.toString());
				query += value.toString() + " set deletedFlag =1,LAST_MODIFIED_DATE =sysdate,LAST_MODIFIED_BY="+jsonDataObject.getString("lastModifiedBy")  +" where ";
				field = constantsClass.getField(domainKey);
				value = field.get(null);
				System.out.println(" constant key value " + value.toString());
				query += value.toString() + " in("+ deletedIds +"))";
				 System.out.println(" query ++++++++ " + query);
				queries.put(query);
			}
			
			jsonDataObject.put("queries", queries);
			
			jsonObject = CommonController.saveJSONDataObject(jsonDataObject);
		}catch(Exception e){
			 System.out.println("exception in PreparationAction.delete Reagents Results " + e);
				e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String loadTrackerList() throws Exception {
//		log.debug("starts");
//		try{
//			String jsonData = request.getParameter("jsonData");
//			
//			//System.out.println("jsonData" + jsonData);	
//			
//			JSONArray jsonArray = new JSONArray();
//			JSONObject jsonDataObject = new JSONObject(jsonData);
//			
//			String entityId = jsonDataObject.getString("entityId");
//			String entityType=jsonDataObject.getString("entityType");
//			String flowType=jsonDataObject.getString("flowType"); 
//			String nextflowFlag=jsonDataObject.getString("nextflowFlag");
//			
//			log.trace("entityId" +entityId+ "/  entityType"+entityType+"/flowType"+flowType+"/nextflowFlag"+nextflowFlag);
//			
//			JSONObject jsonObject = new JSONObject();
//			
//			String criteria=" "; 
//			criteria =VelosStafaSqlConstants.SQL_TRACKERLIST.replace(VelosStafaSqlConstants.FIELD_ENTITY_ID, entityId);
//			criteria =criteria.replace(VelosStafaSqlConstants.FIELD_FLOW_TYPE, flowType);
//			
//			jsonObject.put("Query", criteria);
//			jsonObject.put("type", "SQL");
//			jsonArray.put(0,jsonObject);
//			
//			// next flow
//			String activity ="";
//			if(jsonDataObject.has("activity")){
//				
//				activity=jsonDataObject.getString("activity"); 
//				if(activity!=null && activity.trim().length()>0){
//					System.out.println( " inside if");
//					jsonObject = new JSONObject();
//					criteria = VelosStafaSqlConstants.SQL_NEXTLIST.replace(VelosStafaSqlConstants.FIELD_ENTITY_ID, entityId);
//					criteria =criteria.replace(VelosStafaSqlConstants.FIELD_ACTIVITY, activity);
//					jsonObject.put("Query", criteria);
//					jsonObject.put("type", "SQL");
//					jsonArray.put(1,jsonObject);
//					System.out.println(" Criteria " + criteria);
//				}else{
//					System.out.println( " inside else");
//					activity ="";
//				}
//				
//			}
//			
//			//System.out.println("criteria ::::::::: "+criteria);
//			
//			List objectslst = CommonController.getObjectList(jsonArray);
//			
//			trackerList = (List)objectslst.get(0);
//			if(!activity.equals("")){
//				nextFlowList = (List)objectslst.get(1);
//			}
//		//System.out.println("listttttttt"+trackerList.size());
//		}catch(Exception e){
//			log.debug("Exception " + e);
//			log.error("Error " + e.getStackTrace());
//		}
//		log.debug("Ends");
		return SUCCESS;
				
	}
	
	public String verifyeSign()throws Exception{
		log.debug("Starts");
		String eSign="";
		String userLoginName="";
		try{
			//JSONObject jsonObj = VelosUtil.constructJSON(request);
			//System.out.println("jsonObj : "+jsonObj);
			/*if(jsonObj.has("eSign") && jsonObj.get("eSign")!=null){
					eSign = jsonObj.get("eSign").toString();
			}*/
			
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);

			HttpSession session =request.getSession(true);
			KeyPair keys = (KeyPair) request.getSession().getAttribute("keys");
			eSign = request.getParameter("eSign");
			String decryptedEsign = JCryptionUtil.decrypt(eSign, keys);
			//System.out.println(session.getAttribute(USER_OBJECT));
			if(session!=null && session.getAttribute(USER_OBJECT)!=null){
				System.out.println("UserObject");
				UserDetailsObject userDetailsObject = (UserDetailsObject)session.getAttribute(USER_OBJECT);
				log.trace("user id ::"+userDetailsObject.getUserId());
				userLoginName = userDetailsObject.getLoginName().toString();
				
			}
			//System.out.println(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			StringBuffer moduleRightsXml = new StringBuffer();
			if(eSecurityFlag.equalsIgnoreCase("true")){
				// eSecurity validation
				//System.out.println("eSecurity is true");
				String esecurityURL = resource.getString(ESECURITY_URL);
				//System.out.println("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				/*xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body>");
								
				xmlIp.append("<ser:getUsers>");
				xmlIp.append("<loginName>"+userLoginName+"</loginName>");
				xmlIp.append("<firstName></firstName>");
				xmlIp.append("<lastName></lastName>");
				xmlIp.append("</ser:getUsers>");
				xmlIp.append("</soapenv:Body></soapenv:Envelope>");*/
				
				
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:checkAuthentication>");
				xmlIp.append("<user><application>Stafa</application>");
				xmlIp.append("<"+ESecurityConstants.login+">"+userLoginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("<"+ESecurityConstants.password+"/>");
				xmlIp.append("<"+ESecurityConstants.newpassword+"/>");
				xmlIp.append("<"+ESecurityConstants.esign+">"+decryptedEsign+"</"+ESecurityConstants.esign+">");
				xmlIp.append("<"+ESecurityConstants.newesign+"/>");
				xmlIp.append("</user></ser:checkAuthentication> </soapenv:Body></soapenv:Envelope>");
				
				
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				
				String strXmlValue = WebserviceUtil.getXmlValue(result, "result","statusCode");
				
				//System.out.println("Result " + result);
				//System.out.println("====================>"+WebserviceUtil.getXmlValue(result, "result", "esign"));
				//String loginName = WebserviceUtil.getXmlValue(result, "result", "loginName");
				//String dbESign = WebserviceUtil.getXmlValue(result, "result", "esign");
				/*System.out.println("loginName : "+loginName);
				System.out.println("userLoginName : "+userLoginName);*/
			//	System.out.println("eSign : "+eSign);
				System.out.println("strXmlValue  : "+strXmlValue);
				//if(!userLoginName.isEmpty() && userLoginName.equalsIgnoreCase(loginName)){
					if(strXmlValue!=null && strXmlValue.equalsIgnoreCase("fail")){
						eSignFlag = "false";
					}else{
						eSignFlag = "true";
					}
				//}
					log.trace("eSignFlag-->"+eSignFlag);
				
			}
			if(eSecurityFlag.equalsIgnoreCase("false")){
				eSignFlag = "true";
			}
			
			
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS; 
	}

	
	public String fetchNextFlow()throws Exception{
		log.debug("Starts");
		String jsonData = request.getParameter("jsonData");
		log.trace("jsonData" + jsonData);	
		try{
			JSONObject jsonObject = new JSONObject(jsonData);
			
			String entityType = jsonObject.getString("entityType");
			String entityId = jsonObject.getString("entityId");
			//for workflow (String entitytype,String entityId,int workflowid)
			HttpSession session =request.getSession(true);
			//System.out.println(session.getAttribute(USER_OBJECT));
			Long userId=null;
			if(session!=null && session.getAttribute(USER_OBJECT)!=null){
				UserDetailsObject userDetailsObject = (UserDetailsObject)session.getAttribute(USER_OBJECT);
				log.trace("user id ::"+userDetailsObject.getUserId());
				userId =userDetailsObject.getUserId();
			}
			//String workflow = VelosUtil.callWorkflow(entityType,entityId,userId);
			//jsonObject.put("nextFlow", workflow);
			//nextFlow = jsonObject.toString();
			//nextFlow = workflow;
			//log.trace(" workflow next step " + nextFlow);
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}
	
	
	
	public String checkDuplicate()throws Exception{
		log.debug("Starts");
		try {
			//checkDuplicate(String objectName,String property,String value)
			String objectName = request.getParameter("domainName");
			String property = request.getParameter("property");
			String value = request.getParameter("value");
			returnCount = CommonController.findCount(objectName, property, value);
			
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}
	


	public String execute() throws Exception {
		return SUCCESS;
	}

	public String validateUser() throws Exception {
		
		log.debug("Starts ");
		try{
		//System.out.println(" request " + request);
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		//System.out.println("userName " + userName + "/ " + password);
		
		userAuthentication = CommonController.userAuthentication(userName,password,"STAFA",null);
		File f= new File("a.txt");
		// System.out.println(" new path " + f.getAbsolutePath());
		 path=f.getAbsolutePath();
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		 
		return SUCCESS;
				
	}
	public String logOut() throws Exception {
		System.out.println("Logout called");
		return SUCCESS;
				
	}
	public String fetchValues() throws Exception {
		log.debug("Starts");
		try{	
			String dominName = request.getParameter("object");
			String criteria = request.getParameter("criteria");
			log.trace( " dominName " + dominName  + "/ " +criteria);
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("Domain", dominName);
			jsonObject.put("criteria", criteria);
			jsonArray.put(0,jsonObject);
			List objectList = CommonController.getObjectList(jsonArray);
			if(objectList !=null && objectList.size()>0){
				resultList = (List)objectList.get(0);
			}
		}catch(Exception e){
			log.debug("Exception " + e);
			log.error("Error " + e.getStackTrace());
		}
		log.debug("Ends");
		return SUCCESS;
	}



	public String getNextFlow() {
		return nextFlow;
	}



	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}


	public String geteSignFlag() {
		return eSignFlag;
	}

	public void seteSignFlag(String eSignFlag) {
		this.eSignFlag = eSignFlag;
	}
	public List getTrackerList() {
		return trackerList;
	}

	public void setTrackerList(List trackerList) {
		this.trackerList = trackerList;
	}

	public int getReturnCount() {
		return returnCount;
	}

	public void setReturnCount(int returnCount) {
		this.returnCount = returnCount;
	}

	public List getResultList() {
		return resultList;
	}
	public void setResultList(List resultList) {
		this.resultList = resultList;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getUserAuthentication() {
		return userAuthentication;
	}
	public void setUserAuthentication(String userAuthentication) {
		this.userAuthentication = userAuthentication;
	}

	public String getVerifyStatus() {
		return verifyStatus;
	}

	public void setVerifyStatus(String verifyStatus) {
		this.verifyStatus = verifyStatus;
	}
	public List getNextFlowList() {
		return nextFlowList;
	}
	public void setNextFlowList(List nextFlowList) {
		this.nextFlowList = nextFlowList;
	}
	public String getModuleRights() {
		return moduleRights;
	}
	public void setModuleRights(String moduleRights) {
		this.moduleRights = moduleRights;
	}
	
	
	
}
