package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRUtil;



/**
 * @summary     GetSSLinkDataAction
 * @description Get Service Set Link Data 
 * @version     1.0
 * @file        GetSSLinkDataAction.java
 * @author      Lalit Chattar
 */
public class GetSSLinkDataEditAction extends StafaBaseAction{
	
	
	
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	private int constant, end, start;
	private StringBuilder sWhere;
	private String sOrderBy = null;
	private String[] aColumnsSer;// = {"SERVICE_DESC1", "SERVICE_DESC2", "CMS_HCPCS_CPT", "CMS_HCPCS_CPT_EFF_DATE", "SERVICE_CODE", "PRS_CODE", "MCAID_HCPCS_CPT", "MCAID_HCPCS_CPT_EFF_DATE", "CPT_CODE", "BLUE_CROSS_HCPCS_CPT", "BLUE_CROSS_HCPCS_CPT_EFF_DATE", "REV_CLASS", "CUSTOM_DESC1", "REV_CODE", "HCC_TECH_CHG", "PRS_PHY_CHG", "EFF_FROM_DATE", "EFF_TO_DATE"};
	//private String[] aColumns;// = { "SERVICE_DESC1", "SERVICE_DESC2", "CMS_HCPCS_CPT", "TO_DATE(CMS_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "SERVICE_CODE", "PRS_CODE", "MCAID_HCPCS_CPT", "TO_DATE(MCAID_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "CPT_CODE", "BLUE_CROSS_HCPCS_CPT", "TO_DATE(BLUE_CROSS_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "TO_NUMBER(REV_CLASS)", "CUSTOM_DESC1", "TO_NUMBER(REV_CODE)", "TO_NUMBER(HCC_TECH_CHG)", "TO_NUMBER(PRS_PHY_CHG)", "TO_DATE(EFF_FROM_DATE, 'DD-MM-YYYY')", "TO_DATE(EFF_TO_DATE, 'DD-MM-YYYY')"};
	private String[] dateArray;// = {"CMS_HCPCS_CPT_EFF_DATE", "MCAID_HCPCS_CPT_EFF_DATE", "BLUE_CROSS_HCPCS_CPT_EFF_DATE", "EFF_FROM_DATE", "EFF_TO_DATE"};
	private String[] numberArray;// = {"HCC_TECH_CHG", "PRS_PHY_CHG"};
	
	
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}
	
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	public GetSSLinkDataEditAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return getSsLinkData();
	}
	
	/**
	 * @description Get Service set link data
	 * @return String
	 * @throws Exception
	 */
	public String getSsLinkData()throws Exception {
		
		try {
			
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			
			
			aColumnsSer = CDMRUtil.fetchPropertyVal(jsonObject.getString("FILE_ID")+"_COLS");
			dateArray = CDMRUtil.fetchPropertyVal(jsonObject.getString("FILE_ID")+"_DATEARY");
			numberArray = CDMRUtil.fetchPropertyVal(jsonObject.getString("FILE_ID")+"_NUMARY");
			
			List<String> dateList = Arrays.asList(dateArray);
			List<String> numberList = Arrays.asList(numberArray);
			
			
			constant = Integer.parseInt(jsonObject.getString("iDisplayLength"));
			end = Integer.parseInt(jsonObject.getString("iDisplayStart")) + Integer.parseInt(jsonObject.getString("iDisplayLength"));
			start = Integer.parseInt(jsonObject.getString("iDisplayStart"))+1;
			
			jsonObject = CDMRUtil.fetchEventLibId(jsonObject);
			jsonObject = CDMRUtil.getActiveInactiveRule(jsonObject);
			
			sWhere = new StringBuilder();
			if ( jsonObject.getString("sSearch") != "" )
			{
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					if(aColumnsSer[i].contains("DATE") || aColumnsSer[i].contains("date")){
						sWhere.append("LOWER(TO_CHAR("+aColumnsSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObject.getString("sSearch").toLowerCase()+"%' OR ");
					}else{
						sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObject.getString("sSearch").toLowerCase()+"%' OR ");
					}
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			if (jsonObject.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObject.getInt("iSortingCols") ; i++ )
				{
					if((jsonObject.getString("bSortable_" + jsonObject.getString("iSortCol_"+i))).equals("true")){
					
						String columnName = "";
					
						if(numberList.contains(jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0")))){
							columnName = "TO_NUMBER(" +jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0"))+ ")";
						}else{
							columnName = jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0"));
						}
						sOrderBy = sOrderBy + columnName + " " + (jsonObject.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = sOrderBy + " PK_CDM_MAIN";
				}
				
			}
			jsonObject = this.getServiceSetInfo(jsonObject);
			jsonObject = this.getSSLinkInfo(jsonObject);
			jsonObject = CDMRUtil.getCurrentVersion(jsonObject);
			jsonObject = this.getCDMDataAssociateWithServiceSet(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	

	/**
	 * @description Get Service set Information
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getServiceSetInfo(JSONObject jsonObject) throws Exception{
		try {
			String query = "select LSERVICE_CODE from cdm_main where PK_CDM_MAIN = " + jsonObject.getString("ssID");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromCdmMain(jsonObject);
			List<Map> list = (List<Map>)jsonObject.get("cdmmain");
			if(list.size()>0){
				Map mapObj = list.get(0);
				jsonObject.put("SS_LSERVICE_CODE", mapObj.get("LSERVICE_CODE"));
			}else{
				jsonObject.put("SS_LSERVICE_CODE", "NO DATA");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	/**
	 * @description Get Service set link Information
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getSSLinkInfo(JSONObject jsonObject) throws Exception{
		try {
			String ids = "";
			String query = "select SS_CHILD_ID from CDM_SSLINK where SS_ID = '" + jsonObject.getString("SS_LSERVICE_CODE") + "' and DELETEDFLAG = 0" ;

			jsonObject.put("SS_CHILD_ID", query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	
	/**
	 * @description Get Current Version
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
		try {
			String query = "select version_id from cdm_versiondef where is_current = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("versioninfo");
			Map dataMap = list.get(0);
			jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}*/
	
	/**
	 * @description Get data associate with Service set
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getCDMDataAssociateWithServiceSet(JSONObject jsonObj) throws Exception{
		try {
			String ids = jsonObj.getString("SS_CHILD_ID");
			
			CDMRUtil.getMapVersion(jsonObj);
			this.getColumnFromStagingMap(jsonObj);
			String columnNames = "";
			List<Map> columnsNameMap = (List<Map>)jsonObj.get("columnsNames");
			for (Map map : columnsNameMap) {
				String type = map.get("MAINCDM_COLTYPE") == null? "":map.get("MAINCDM_COLTYPE").toString();
				if(type.equalsIgnoreCase("TIMESTAMP")){
					columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", 'dd-MON-YYYY') as " +map.get("EQMAINCDM_COL").toString()  + ",";
				}else{
					columnNames = columnNames +map.get("EQMAINCDM_COL").toString()+ ",";
				}
				
			}
			columnNames = columnNames.substring(0, (columnNames.length()-1));
		String query = "select TotalRows,PK_CDM_SSLINK,PK_CDM_MAIN,"+columnNames+" from (SELECT t.*,cdm_sslink.PK_CDM_SSLINK, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(PK_CDM_SSLINK) OVER () AS TotalRows FROM CDM_MAIN t, cdm_sslink where EFF_FROM_DATE IS NOT NULL and cdm_sslink.DELETEDFLAG = 0 and (t.LSERVICE_CODE = cdm_sslink.ss_child_id and SS_ID = '"+ jsonObj.getString("SS_LSERVICE_CODE")+"') "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where LSERVICE_CODE = cdm_sslink.ss_child_id and CUR_VERSION <= "+jsonObj.getString("versionid")+" group by LSERVICE_CODE, CHG_IND)) WHERE MyRow BETWEEN "+start+" AND "+end;

			
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmSSLink(jsonObj);
			this.processData(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	
	/**
	 * @description Process data for displaying in datatable
	 * @return void
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject) throws Exception{
		try{
			List<Map> dataList = (List<Map>)jsonObject.get("cdmsslink");
			String activerule = jsonObject.getString("ACTDACTRULE");
			for (Map map : dataList) {
				Set keySet = map.keySet();
				
				Map<String, String> data = new HashMap<String, String>();
				data.put("check", "<input type=\"checkbox\" onclick=\"addInArray(this.value);\" value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"check\"/>");
				data.put("deleteset", "<img value=\""+map.get("PK_CDM_MAIN").toString()+ ","+map.get("PK_CDM_SSLINK").toString()+"\" src=\"images/cdmr/delete.png\" name=\"deletesetItem\" id=\"deletesetItem\" onclick=\"deleteServSet(this.value, this);\"/>");
				long days = 0;
				if(map.get("EFF_TO_DATE") != null){
					days = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_TO_DATE").toString());
				}
				
				for (Object key : keySet) {
					
					if(key.toString().equalsIgnoreCase("TOTALROWS") || key.toString().equalsIgnoreCase("PK_CDM_MAIN")){
						this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						continue;
					}

					if (activerule.equals("DATE"))
					{
						if(days >= 0 && map.get("EFF_TO_DATE") != null){
							long diffBetDate = CDMRUtil.getDifferenceBetweenTwoDates(map.get("EFF_FROM_DATE").toString(), map.get("EFF_TO_DATE").toString());
							long diffBBtwnFromAndPrest = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_FROM_DATE").toString());
							if(diffBetDate > 0 && diffBBtwnFromAndPrest>=0){
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "N/A");
								}else{
									data.put(key.toString(),  map.get(key.toString()).toString());
								}
							}else{
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
								}else{
									data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
								}
							}
						}else{
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "N/A");
							}else{
								data.put(key.toString(),  map.get(key.toString()).toString());
							}
						}
					}
					else
						if (activerule.equals("FLAG"))
						{
							if(map.get("IS_ACTIVE_YN").toString().equalsIgnoreCase("N"))
							{		
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
								}else{
									data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
								}
							}
								
							else
							{
								
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "N/A");
								}else{
									data.put(key.toString(),  map.get(key.toString()).toString());
								}
							}
						
						}
					
				}
				aaData.add(data);
			}
			
			if(aaData.isEmpty()){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
	/**
	 * @description Get Latest map version 
	 * @return JSONObject
	 * @throws Exception
	 */
		
	/*public JSONObject getMapVersion(JSONObject jsonObj) throws Exception{
		try {
						
			String vermap = CDMRUtil.fetchPropertyValStr(jsonObj.getString("FILE_ID")+"_MAPS");
			
			String query="select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID IN("+vermap+")";
							
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMapVersion(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");		
			jsonObj.put("MAPSVERSION", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}*/
		
	
	
	/**
	 * @description Get Columns from Staging Maps
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getColumnFromStagingMap(JSONObject jsonObj) throws Exception{
		try {
			
			List<Map> versionList = (List<Map>)jsonObj.get("MAPSVERSION");
			String versionInfo = "";
			for (Map map : versionList) {
				versionInfo = versionInfo + map.get("MAPSVERSION").toString() + ",";
			}
			versionInfo = versionInfo.substring(0, (versionInfo.length()-1));
			String query = "select distinct(EQMAINCDM_COL),MAINCDM_COLTYPE from CDM_STAGINGMAPS where FK_CDM_MAPSVERSION in ("+versionInfo+") and DISP_INCDM = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getStagingMaps(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");
			jsonObj.put("columnsNames", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	
}
