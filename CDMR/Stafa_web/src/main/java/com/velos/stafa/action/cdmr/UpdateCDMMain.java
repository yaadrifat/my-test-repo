package com.velos.stafa.action.cdmr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     UpdateCDMMain
 * @description Upadte Group Code
 * @version     1.0
 * @file        UpdateCDMMain.java
 * @author      Ved Prakash
 */
public class UpdateCDMMain extends StafaBaseAction{
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
		
	private String grupCode;
	private String pkString;
	private String fileType;
	
	/*
	 * Constructor
	 */
	public UpdateCDMMain(){
		cdmrController = new CDMRController();
	}
	
	/*
	 * getter and setter
	 */
	
	
	public String getGrupCode() {
		return grupCode;
	}
	public void setGrupCode(String grupCode) {
		this.grupCode = grupCode;
	}
	public String getPkString() {
		return pkString;
	}

	public void setPkString(String pkString) {
		this.pkString = pkString;
	}
	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
		
	
	/**
	 * @description Update Selected Group Code
	 * @return String
	 * @throws Exception
	 */
	
	
	public String updateSelected() throws Exception{
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			
			this.getStagingColumnMapDetail(jsonObject);
			String pkStr = jsonObject.getString("pkString");
			pkStr = pkStr.substring(0, pkStr.length() - 1);
			List<StagingMaps> mapsObjList = (List<StagingMaps>)jsonObject.get("stagingcolumnmap");
			String fkcdmgrpcode="";
			for (StagingMaps stagingMaps : mapsObjList) {
				if(stagingMaps.getEqMainCdmCol().equals("FK_CDM_GRPCODE")){
					fkcdmgrpcode=stagingMaps.getEqCompReqCol();
				  break;
				}
				
			}
			    
				String query = "update CDM_STAGINGCOMPREC set "+fkcdmgrpcode+" = '"+jsonObject.getString("grupCode")+"' where PK_CDM_STAGINGCOMPREC in ("+pkStr+")";
				System.out.print("Query   >>>>>>>  "+query);
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateComrec(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}

	
	/**
	 * @description Get Mapping Detail
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getStagingColumnMapDetail(JSONObject jsonObject) throws Exception{
		jsonObject= this.getMapVersion(jsonObject);
		String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP + " where fkMapVersion = " + jsonObject.getString("MAPSVERSION");
		jsonObject.put("query", query);
		jsonObject.put("SQLQuery", false);
		jsonObject = cdmrController.getStagingColumnMapDetail(jsonObject);
		
		return jsonObject;
	}
	
	/**
	 * @description Get Map Version
	 * @return JSONObject
	 * @throws Exception
	 */
	public  JSONObject getMapVersion(JSONObject jsonObj){
		
		try {
			String query="select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID='"+jsonObj.getString("fileType")+"'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMapVersion(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");
			Map dataMap = list.get(0);
			
			//jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
			jsonObj.put("MAPSVERSION", dataMap.get("MAPSVERSION").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}	
	
}