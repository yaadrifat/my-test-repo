package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LoadTreeGroupViewAction
 * @description Load Tree View in Charge Library
 * @version     1.0
 * @file        LoadTreeGroupViewAction.java
 * @author      Lalit Chattar
 */
public class DeleteServiceSection extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 
	private Set idSet;
	 private Integer grpIdLength;
	/*
	 * Getter and Setter

	*/
	
	
	
	public DeleteServiceSection(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String deleteServiceSection() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			
			jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
			
			idSet = new HashSet();
			idSet.add(jsonObj.getString("sersecId"));
			idSet = this.getAllGroupLevels(idSet);
			grpIdLength =0;
			for(;;){
				if(idSet.size() == grpIdLength){
					break;
				}else{
					grpIdLength = idSet.size();
					idSet = this.getAllGroupLevels(idSet);
				}
				
			}
			
			this.updateCDMGrpCode(idSet);
			CDMRUtil.getCurrentVersion(jsonObj);
			this.getDataFromCDMMain(jsonObj);
			this.updateCDMain(jsonObj);
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return SUCCESS;
	}
	
	public void updateCDMGrpCode(Set idSet) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "update CDM_GRPCODE set DELETEDFLAG = 1 where PK_CDM_GRPCODE in (" + StringUtils.join(idSet, ",") + ")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.editServiceSection(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateCDMain(JSONObject jsonObject) throws Exception{
		try {
			String query = "update CDM_MAIN set FK_CDM_GRPCODE = null where PK_CDM_MAIN in ("+jsonObject.getString("pkdeleteID")+")";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.editServiceSection(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @description Get All Group(Service Section ) and Child Service Section
	 * @return Set
	 * @throws Exception
	 */
	public Set getAllGroupLevels(Set idSet) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = CDMRSqlConstants.GET_GROUP_PARENT_ID+"("+StringUtils.join(idSet, ",")+")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getGroups(jsonObj);
			List<Map> listData = (List<Map>)jsonObj.get("groupcode");
			for (Map map : listData) {
				idSet.add(map.get("PK_CDM_GRPCODE").toString());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idSet;
	}
	
	/**
	 * @description Get Current version of data 
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
		try {
			String query = "select version_id from cdm_versiondef where is_current = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("versioninfo");
			Map dataMap = list.get(0);
			jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonObj;
	}*/
	
	public JSONObject getDataFromCDMMain(JSONObject jsonObj) throws Exception{
		try {
			String query = "select PK_CDM_MAIN from CDM_MAIN where FK_CDM_GRPCODE in ("+StringUtils.join(idSet, ",")+")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("cdmmain");
			String pkID = "";
			for (Map map : list) {
				pkID = pkID + map.get("PK_CDM_MAIN").toString() + ",";
			}
			pkID = pkID.substring(0, (pkID.length()-1));
			jsonObj.put("pkdeleteID", pkID);
		} catch (Exception e) {
			e.printStackTrace();
		}
				return jsonObj;
	}
	
}
