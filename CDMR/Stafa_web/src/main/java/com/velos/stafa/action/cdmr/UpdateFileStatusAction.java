package com.velos.stafa.action.cdmr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.velos.stafa.util.CDMRUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.business.domain.cdmr.CodeList;

/**
 * @summary     UpdateFileStatusAction
 * @description Update File Status in Files Lib
 * @version     1.0
 * @file        UpdateFileStatusAction.java
 * @author      Lalit Chattar,Shikha
 */
public class UpdateFileStatusAction extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	private Integer fileid;
	private Integer statusCode;
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * getter and setter
	 */
	
	public Integer getFileid() {
		return fileid;
	}

	public void setFileid(Integer fileid) {
		this.fileid = fileid;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	/*
	 * Constructor
	 */
	public UpdateFileStatusAction(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		updateStatus();
		return SUCCESS;
	}
	
	
	/**
	 * @description Update File Status
	 * @return String
	 * @throws Exception
	 */
	public String updateStatus() throws Exception{
		try{
			Long newStatus = null;
			String desc = "";
			JSONObject jsonObj = new JSONObject();
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			jsonObj = VelosUtil.constructJSON(request);
			String esec_DB = CDMRUtil.fetchPropertyValStr("ESEC_DB");
			String query = CDMRSqlConstants.STAGING_HEADER_FILE_INFO + esec_DB +".sec_user u where u.pk_userid=(CASE WHEN c.last_modified_by IS NULL THEN c.creator ELSE c.last_modified_by END) and  c.pk_cdm_fileslib = "+jsonObj.getString("fileid");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMRFilesInfo(jsonObj);
			System.out.println("############################################");
			List<Map> dataMap = (List<Map>)jsonObj.get("filesinfo");
			Map map = dataMap.get(0);
			for (CodeList codeListObj : codelist) {
				if(codeListObj.getPkCodeList() == Long.parseLong(map.get("FILE_CUR_STATUS").toString()) &&
						codeListObj.getCodeListType().equals("filestatus") && codeListObj.getCodeListSubType().equals("new")){
					for(CodeList codeList: codelist){
						if(codeList.getCodeListType().equals("filestatus") && codeList.getCodeListSubType().equals("inproc")){
							newStatus = codeList.getPkCodeList();
							desc = codeList.getCodeListDesc();
							jsonObj.put("desc", desc);
							jsonObj.put("newStatus", newStatus);
							SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
							Date parsedDate = formatter.parse(jsonObj.getString("lastModifiedOn"));
							jsonObj.put("lastModifiedOn", parsedDate);
							jsonObj.put("lastModifiedOn", parsedDate);
							jsonObj = cdmrController.updateFileStatus(jsonObj);
							JSONObject mailObj = this.getUsernameAndEmailAddress(jsonObj.getString("newStatus"));
							JSONObject mailConfig = this.getMailConfiguration();
							List<Map> mailObjList = (List<Map>)mailObj.get("notifycnf");
							if(mailObjList.size() != 0){
								for (Map object : mailObjList) {
									List<String> message = this.getNotificationMessages(object.get("PK_CDM_NOTIFYCNF").toString());
									for (String msg : message) {
										String to = object.get("USER_EMAIL") == null?null:object.get("USER_EMAIL").toString();
										String body = "Hello User " + "\n"+ msg.toString();
										mailConfig.put("TO", to);
										mailConfig.put("BODY", body);
										if(to != null)
											try{
												CDMRUtil.sendNotificationMail(mailConfig);
												}
											catch(Exception e){
												System.out.println(e.getMessage());
												System.out.println("mail not sent/issue in email server-----------------------------");
											}
									}
									this.updateNotification(object.get("PK_CDM_NOTIFYCNF").toString());
								}
							}
							
//							query = "update CDM_FILESLIB set COMMENTS = '"+desc+"', FILE_CUR_STATUS = " + newStatus + " where PK_CDM_FILESLIB = " + jsonObj.getString("fileid");
//							jsonObj.put("query", query);
//							jsonObj.put("SQLQuery", true);
//							jsonObj = cdmrController.updateFileStatus(jsonObj);
//							query = "update CDM_STAGINGMAIN set COMMENTS = '"+desc+"', FILE_CUR_STATUS = " + newStatus + " where FK_CDM_FILESLIB = " + jsonObj.getString("fileid");
//							jsonObj.put("query", query);
//							jsonObj = cdmrController.updateFileStatus(jsonObj);
						}
					}
				}
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * @description Update Specific Status
	 * @return String
	 * @throws Exception
	 */
	public String updateSpecificStatus() throws Exception{
		try {
			Long newStatus = null;
			String desc = "";
			String query= "";
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			for(CodeList codeList: codelist){
				if(codeList.getPkCodeList() == Long.parseLong(jsonObj.getString("statusCode"))){
					newStatus = codeList.getPkCodeList();
					desc = codeList.getCodeListDesc();
					query = "update CDM_FILESLIB set COMMENTS = '"+desc+"', FILE_CUR_STATUS = " + newStatus + ", LAST_MODIFIED_BY = "+jsonObj.getString("user")+", LAST_MODIFIED_DATE = '"+CDMRUtil.getCurrentTimeStamp()+"' where PK_CDM_FILESLIB = " + jsonObj.getString("fileid");
					jsonObj.put("query", query);
					jsonObj.put("SQLQuery", true);
					jsonObj = cdmrController.updateFileStatus(jsonObj);
					query = "update CDM_STAGINGMAIN set COMMENTS = '"+desc+"', FILE_CUR_STATUS = " + newStatus + ", LAST_MODIFIED_BY = "+jsonObj.getString("user")+", LAST_MODIFIED_DATE = '"+CDMRUtil.getCurrentTimeStamp()+"' where FK_CDM_FILESLIB = " + jsonObj.getString("fileid");
					jsonObj.put("query", query);
					jsonObj = cdmrController.updateFileStatus(jsonObj);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	 public  JSONObject getMailConfiguration() throws Exception{
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "select PARAM_SUBTYP, PARAM_VAL from vbar_controlparam where PARAM_TYPE in ('EMAIL') and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT','SSL') and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("mailconfig");
				for (Map map : data) {
					jsonObject.put(map.get("PARAM_SUBTYP").toString(), map.get("PARAM_VAL").toString());
				}
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::getMailConfiguration","1");
				e.printStackTrace();
			}
			return jsonObject;
	  }
	  
	  public  List<String> getNotificationMessages(String pkNotifyCnf) throws Exception{
		  List<String> message = new ArrayList<String>();
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "select MSG_TEXT from CDM_NOTIFICATION where FK_CDM_NOTIFYCNF = " + pkNotifyCnf + " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getNotificationMessages(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("notimesg");
				for (Map map : data) {
					message.add(map.get("MSG_TEXT").toString());
				}
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::getNotificationMessages","1");
				e.printStackTrace();
			}
			
			return message;
			
	  }
	  
	  
	  public  void updateNotification(String pkNotifyCnf) throws Exception{
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "update CDM_NOTIFICATION set SENT_STATUS = 'Y' where FK_CDM_NOTIFYCNF = " + pkNotifyCnf + " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateNotification(jsonObject);
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::updateNotification","1");
				e.printStackTrace();
			}
			
	  }
	  
	  public JSONObject getUsernameAndEmailAddress(String pkCodeList) throws Exception{
			JSONObject jsonObject = new JSONObject();
			try {
				String query = "select PK_CDM_NOTIFYCNF, USER_NAME, USER_EMAIL from CDM_NOTIFYCNF where DELETEDFLAG=0 and STATE_TO = " + pkCodeList;
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getFromNotifycnf(jsonObject);
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "CdmMainAction::getUsernameAndEmailAddress","1");
				e.printStackTrace();
			}
			return jsonObject;
		}
	
	 
}