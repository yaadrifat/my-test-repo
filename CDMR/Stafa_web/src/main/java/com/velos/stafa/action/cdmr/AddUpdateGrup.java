package com.velos.stafa.action.cdmr;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.velos.stafa.business.domain.cdmr.GrpCode;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRUtil;
/**
 * @summary     AddUpdateGrup
 * @description Add And Update Group(Service Section) 
 * @version     1.0
 * @file        FileProcessorAction.java
 * @author      Ved Prakash
 */
public class AddUpdateGrup extends StafaBaseAction{
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession = null;
	
	/*
	 * Parameter getting from request
	 */
	private GrpCode mainObj;
	private String pkID;
	private String grpName;
	private boolean flag;
	private String parentId;
	private String grpLev;
	private Set idSet;
	private boolean checkData;
	private boolean checkForExisting;
	
	/*
	 * Constructor
	 */
	public AddUpdateGrup(){
		cdmrController = new CDMRController();
	}
	
	/*
	 * getter and setter
	 */

	public boolean isCheckForExisting() {
		return checkForExisting;
	}

	public void setCheckForExisting(boolean checkForExisting) {
		this.checkForExisting = checkForExisting;
	}
	
	
	public String getGrpName() {
		return grpName;
	}

	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getGrpLev() {
		return grpLev;
	}

	public void setGrpLev(String grpLev) {
		this.grpLev = grpLev;
	}
		
	public boolean isCheckData() {
		return checkData;
	}

	public void setCheckData(boolean checkData) {
		this.checkData = checkData;
	}
	/*
	 * Default Method of Action class
	 */
	
	/**
	 * @description Add Group(Service Section)
	 * @return String
	 * @throws Exception
	 */
	
	
	public String add() throws Exception{
		String maxGrupCode ="";
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject = CDMRUtil.fetchEventLibId(jsonObject);
			this.checkForExistingSet(jsonObject);
			if(!this.checkForExisting){
				return SUCCESS;
			}
			if(jsonObject.has("pkID"))
				pkID = jsonObject.getString("pkID");
			else
				pkID = "";
			flag = jsonObject.getBoolean("flag");
			
			if(flag){
				maxGrupCode=this.maxGrupCode();
				jsonObject.put("fkAccount", 1);
				jsonObject.put("grpCode", maxGrupCode);
				jsonObject.put("grpDispName", jsonObject.getString("grpName"));
				jsonObject.put("grpLevel", jsonObject.getString("grpLev"));
				jsonObject.put("parentNodeId", jsonObject.getString("parentId"));
				jsonObject.put("eventlibid", jsonObject.getString("EVENT_ID"));
				jsonObject = cdmrController.saveInCdmGrupCode(jsonObject);
			}
			else {
				maxGrupCode=this.maxGrupCode();
				if(pkID.equals("")){
					jsonObject.put("fkAccount", 1);
					jsonObject.put("grpCode", maxGrupCode);
					jsonObject.put("grpDispName", jsonObject.getString("grpName"));
					jsonObject.put("grpLevel", "1");
					jsonObject.put("parentNodeId", "0");
					jsonObject.put("eventlibid", jsonObject.getString("EVENT_ID"));
				}else{
					this.getGropuCodeValue(pkID);
					jsonObject.put("fkAccount", 1);
					jsonObject.put("grpCode", maxGrupCode);
					jsonObject.put("grpDispName", jsonObject.getString("grpName"));
					jsonObject.put("grpLevel", (mainObj.getGrpLevel())+1);
					jsonObject.put("parentNodeId", mainObj.getPkGrpCode());
					jsonObject.put("eventlibid", jsonObject.getString("EVENT_ID"));
				}
				jsonObject = cdmrController.saveInCdmGrupCode(jsonObject);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}
	

	/**
	 * @description Get All Groups(Service Section) for all chiled and sub child
	 * @return String
	 * @throws Exception
	 */
	public Set getAllGroupLevels(Set idSet) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = CDMRSqlConstants.GET_GROUP_PARENT_ID+"("+StringUtils.join(idSet, ",")+")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getGroups(jsonObj);
			List<Map> listData = (List<Map>)jsonObj.get("groupcode");
			for (Map map : listData) {
				idSet.add(map.get("PK_CDM_GRPCODE").toString());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idSet;
	}
	
	/**
	 * @description Delete Group (Service Section)
	 * @return String
	 * @throws Exception
	 */
	public String delete() throws Exception{
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			pkID = jsonObject.getString("pkID");
			idSet = new HashSet();
			idSet.add(pkID);
			idSet = this.getAllGroupLevels(idSet);
			int grpIdLength =0;
			for(;;){
				if(idSet.size() == grpIdLength){
					break;
				}else{
					grpIdLength = idSet.size();
					idSet = this.getAllGroupLevels(idSet);
				}
				
			}
			
			String checkQuery = "select * from cdm_main where FK_CDM_GRPCODE in ("+StringUtils.join(idSet, ",")+")";
			jsonObject.put("query", checkQuery);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromCdmMain(jsonObject);
			List<Map> dataList = (List<Map>)jsonObject.get("cdmmain");
			if(!(dataList.isEmpty())){
				this.setCheckData(false);
				return SUCCESS;
			}
			this.getGropuCodeValue(pkID);
			String query = "update CDM_GRPCODE set DELETEDFLAG = '1' where PK_CDM_GRPCODE = '"+mainObj.getPkGrpCode()+"'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.updateInCdmGrupCode(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.setCheckData(true);
		return SUCCESS;
	}

	
	/**
	 * @description Delete Group(Service Section) from comprec
	 * @return String
	 * @throws Exception
	 */
	public String deletefromcomprec() throws Exception{
	
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			pkID = jsonObject.getString("pkID");
			idSet = new HashSet();
			idSet.add(pkID);
			idSet = this.getAllGroupLevels(idSet);
			int grpIdLength =0;
			for(;;){
				if(idSet.size() == grpIdLength){
					break;
				}else{
					grpIdLength = idSet.size();
					idSet = this.getAllGroupLevels(idSet);
				}
				
			}
			Iterator iterator = idSet.iterator();
			String data = "";
			while (iterator.hasNext()) {
				String value = (String) iterator.next();
				data = "'" + value + "',";
			}
			data = data.substring(0, (data.length()-1));
			String checkQuery = "select * from cdm_stagingcomprec where COMP_COL26 in ("+data+")";
			jsonObject.put("query", checkQuery);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromCdmMain(jsonObject);
			List<Map> dataList = (List<Map>)jsonObject.get("cdmmain");
			if(!(dataList.isEmpty())){
				this.setCheckData(false);
				return SUCCESS;
			}
			this.getGropuCodeValue(pkID);
			String query = "update CDM_GRPCODE set DELETEDFLAG = '1' where PK_CDM_GRPCODE = '"+mainObj.getPkGrpCode()+"'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.updateInCdmGrupCode(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.setCheckData(true);
		return SUCCESS;
	}

	/**
	 * @description Get MAx Group(Service Section) Code
	 * @return String
	 * @throws Exception
	 */
	public String maxGrupCode(){
		String maxGrupCode = "";
		try {
			String query = "Select concat((max(Substr(GRP_CODE,1,2))),(max(to_number(Substr(GRP_CODE,3)+1))))  as MaxGrpCode from CDM_GRPCODE";
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getMaxGrupCode(jsonObject);
			List returnList = (List)jsonObject.get("MaxGrupCode");
			if ((returnList != null) && (returnList.get(0) != null)) {
		        maxGrupCode = returnList.get(0).toString();
		      } else {
		        maxGrupCode = "SG0";
		      }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maxGrupCode;
	}
	
	/**
	 * @description Get Group(Service Section) Code Value
	 * @return String
	 * @throws Exception
	 */
	public void getGropuCodeValue(String pkID) throws Exception{
		
		try{
			JSONObject jsonObject = new JSONObject();
			String query = CDMRSqlConstants.GET_GROUPNAME_BY_CODE + pkID;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", false);
			jsonObject = cdmrController.getGroupCodeValue(jsonObject);
			List<GrpCode> listObj = (List<GrpCode>)jsonObject.get("groupcodename");
			mainObj = listObj.get(0);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * @description Check For existing Sets
	 * @return void
	 * @throws Exception
	 */
	public void checkForExistingSet(JSONObject jsonObj) throws Exception{
		try {
			String query = "select * from cdm_grpcode where TRIM(LOWER(GRP_NAME)) = '" + jsonObj.getString("grpName").toLowerCase().trim() + "' and TRIM(LOWER(GRP_DISP_NAME)) = '" + jsonObj.getString("grpName").toLowerCase().trim() + "' and DELETEDFLAG = 0 and event_lib_id= "+jsonObj.getString("EVENT_ID");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("cdmmain");
			if(list.size() > 0){
				this.setCheckForExisting(false);
			}else{
				this.setCheckForExisting(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}