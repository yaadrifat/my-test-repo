package com.velos.stafa.util;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

/**
 * @summary AdvanceSearchUtil
 * @description Advance search processing
 * @version 1.0
 * @file AdvanceSearchUtil.java
 * @author Shikha Srivastava
 */
public class AdvanceSearchUtil{

/**
	 * @description new advance search processing
	 * @return String
	 * @throws Exception
	 */
	public static String getNewAdvanceSearchCriteria(JSONObject jsonObj) throws Exception{
		String query = "";
		
		try {
			JSONArray searcharry = new JSONArray();
			JSONArray savedarry = new JSONArray();
			Map<String, String> datamap = new HashMap<String, String>();
			if(jsonObj.has("advanceSearch")&& jsonObj.getBoolean("advanceSearch")){
				if(jsonObj.has("filterValues")){
				searcharry = new JSONArray(jsonObj.getString("filterValues"));
				}
				if(jsonObj.has("savedfilters")){
				savedarry = new JSONArray(jsonObj.getString("savedfilters"));
				}
				if(searcharry.length()!=0){
					for(int i =0;i< searcharry.length();i++){
						String[] keyval = searcharry.get(i).toString().split(":");
						datamap.put(keyval[0].trim(), keyval[1].trim());
					}
				}
				if(savedarry.length()!=0){
					for(int i =0;i< savedarry.length();i++){
						String[] keyval = savedarry.get(i).toString().split(":");
						datamap.put(keyval[0].trim(), keyval[1].trim());
					}
				}
				Iterator it = datamap.entrySet().iterator();
				System.out.println(datamap);
				while (it.hasNext()) {
					Map.Entry entry = (Map.Entry) it.next();
					String key = entry.getKey().toString().trim();
					 String value = entry.getValue().toString().trim();
					 if(key.contains("_AS")){
				    	if(key.contains("DATE")){
				    		if(value.contains("To")){
				    			String []daterange = value.split("To");
				    			String from=daterange[0].trim();
				    			String to = daterange[1].trim();
				    			query = query + " AND " + key.replace("_AS", "") + " BETWEEN '"+ from + "' AND '" + to + "'";
				    		}
				    		else if(value.contains("From")){
				    			String []datefrom = value.split("From");
				    			String fromval=datefrom[1].trim();
				    			query = query + " AND " + key.replace("_AS", "") + " >= TO_DATE('"+ fromval + "')";
				    		}
                            else if(value.startsWith("Up")){//contains("Up to")){
                            	String []dateto = value.split("Up to");
				    			String toval=dateto[1].trim();
                            	query = query + " AND " + key.replace("_AS", "") + " <= TO_DATE('"+ toval + "')";
				    		}
				    		else{
				    		query = query + " AND " + key.replace("_AS", "") + " = '"+value+"'";
				    		}
				    	}
				    	else if(key.contains("CHG")){
				    		if(value.contains("-")){
				    			String []chgrange = value.split("-");
				    			String from=chgrange[0].trim();
				    			String to = chgrange[1].trim();
				    			query = query + " AND " + key.replace("_AS", "") + " BETWEEN "+ from + " AND " + to + "";
				    		}
				    		else if(value.contains("From")){
				    			String []chgfrom = value.split("From");
				    			String fromval=chgfrom[1].trim();
				    			query = query + " AND " + key.replace("_AS", "") + " >= "+ fromval + "";
				    		}
                            else if(value.contains("Up to")){
                            	String []chgto = value.split("Up to");
				    			String toval=chgto[1].trim();
                            	query = query + " AND " + key.replace("_AS", "") + " <="+ toval + "";
				    		}
				    		else{
				    			query = query + " AND " + key.replace("_AS", "") + "='"+value + "'";
				    		}
				    	}
				    	else{
				    		query = query + " AND UPPER(" + key.replace("_AS", "") + ") like UPPER('%"+value + "%')";
				    	}
					 }
				}
				return query;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return query;
	}	
}
		