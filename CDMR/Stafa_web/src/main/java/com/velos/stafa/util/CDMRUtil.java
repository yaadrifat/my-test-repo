package com.velos.stafa.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     CDMRUtil
 * @description Provide utility methods
 * @version     1.0
 * @file        CDMRUtil.java
 * @author      Lalit Chattar
 */
public class CDMRUtil{
	
	private static CDMRController cdmrController = new CDMRController();
	/**
	 * @description Generic date formatter
	 * @param from (Actual date format)
	 * @param to (Required date format)
	 * @param cdate (Date in String form)
	 * @return String (Converted Date)
	 */
	public static String formateDate(String from, String to, String cdate, String type) throws Exception{
			SimpleDateFormat formatter, FORMATTER;
			formatter = new SimpleDateFormat(from);
			String oldDate = cdate;
			Date date = null;
			try {
				date = formatter.parse(oldDate.substring(0, 19));
			} catch (ParseException e) {
				errorLogUtil(e, "", cdmrController, "CDMRUtil::formateDate", type);
				e.printStackTrace();
			}
			FORMATTER = new SimpleDateFormat(to);
			return FORMATTER.format(date);
	}
	
	/**
	 * @description Return codelist object base on criteria
	 * @param  List (CodeList Objects)
	 * @param  HashMap (Search Criteria)
	 * @return CodeList
	 */
	public static CodeList fetchFromCodeList(List<Object> object, Map<Integer, String> criteria) throws Exception{
		Iterator<Object> iterator = object.iterator();
		List<Boolean> logic = new ArrayList<Boolean>();
		int index = 1;
		while (iterator.hasNext()) {
			index++;
			CodeList codeList = (CodeList) iterator.next();
			for (Map.Entry entry : criteria.entrySet()) {
			    switch (Integer.parseInt(entry.getKey().toString())) {
				case 2:
					if(codeList.getCodeListType().equals(entry.getValue().toString())){
						logic.add(true);
					}
					break;

				case 3:
					if(codeList.getCodeListSubType().equals(entry.getValue().toString())){
						logic.add(true);
					}
					break;
				default:
					logic.add(false);
					break;
				}
			}
			if(checkList(logic)){
				return codeList;
			}
		}
		return null;
	}
	
	/**
	 * @description Check boolen list for all true object
	 * @param  List (Boolean Objects)
	 * @return Boolean
	 */
	public static Boolean checkList(List<Boolean> list) throws Exception{
		Iterator iterator = list.iterator();
		Boolean temp = true;
		if(list.isEmpty()){
			return false;
		}
		while (iterator.hasNext()) {
			Boolean object = (Boolean) iterator.next();
			if(!(object && temp)){
				return false;
			}
		}
		return true;
	}
	
	public static String formateValue(String value) throws Exception{
		String formated = "0.00";
		try {
			if(value.equals("") || value == null || value.equals(" ")){
				return formated;
			}
			double val = Double.parseDouble(value);
		    DecimalFormat df = new DecimalFormat("#0.00");
		    formated = df.format(val);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return formated;
	}
	
	
	public static int getFkAccount() throws Exception{
		return 1;
	}
	
	
	
	  public static long getDifferenceBetweenDate(String cdmdate)  throws Exception {
		    
		  	Date systemDate = new Date();
	        long MILLISECS_PER_DAY = 24 * 60 * 60 * 1000;
	        long days = 0l;
	        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy"); // "dd/MM/yyyy HH:mm:ss");
	        String today = format.format(systemDate);
	        Date dateCDM = null;
	        Date dateTD = null;        
	        try {       
	            dateCDM = (Date) format.parse(cdmdate);
	            dateTD = (Date) format.parse(today);
	            days = (dateTD.getTime() - dateCDM.getTime())/MILLISECS_PER_DAY;                        
	        } catch (Exception e) {  e.printStackTrace();  }   
          
	        return days; 
	     }
	  
	  
	  public static void errorLogUtil(Exception exception, String lservcode, CDMRController cdmrController, String errorArea, String indc) throws Exception{
			try {
				
				if(exception instanceof org.hibernate.SessionException){
					return;
				}
				
				String today = new SimpleDateFormat("MM/dd/yyyy H:m").format(new Date());
				String errorDesc = exception.getLocalizedMessage();
				errorDesc = errorDesc == null?"":errorDesc;
				String errorName = exception.toString();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("errorArea", errorArea);
				jsonObject.put("errorName", errorName);
				jsonObject.put("errorDesc", errorDesc +" "+ lservcode);
				jsonObject.put("errorRepOn", today);
				jsonObject.put("deletedFalg", 0);
				if(indc != null){
					jsonObject.put("temIndc", 1);
				}
					
				cdmrController.saveErrorLog(jsonObject);
				
			} catch (Exception e) {
				errorLogUtil(exception, lservcode, cdmrController, "CDMRUtil::errorLog", null);
				
			}
		}
	  public  String getEmailNotificationChk() throws Exception{
		  JSONObject jsonObject = new JSONObject();
		  String check = "";
			try {
				String query = "select PARAM_VAL from vbar_controlparam where PARAM_TYPE ='CDMR EMAIL'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("mailconfig");
				check = data.get(0).get("PARAM_VAL").toString();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return check;
	  }
	  public  String getExportEmailNotificationChk() throws Exception{
		  JSONObject jsonObject = new JSONObject();
		  String check = "";
			try {
				String query = "select PARAM_VAL from vbar_controlparam where PARAM_TYPE ='CTMSEMAIL' and PARAM_SUBTYP='EMAIL_NOTIFICATION'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("mailconfig");
				check = data.get(0).get("PARAM_VAL").toString();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return check;
	  }	  
	  @SuppressWarnings("static-access")
	public static void sendNotificationMail(JSONObject jsonObject) throws Exception{
		  CDMRUtil cdmutil = new CDMRUtil();
			 if(cdmutil.getEmailNotificationChk().equals("N")){
				 System.out.println("Email notification has been disabled");
				 return;
			 }
		  final String username = jsonObject.getString("FROM");
		  final String password =  com.velos.stafa.util.EncryptionUtil.decrypt(jsonObject.getString("PASSWORD"));
		  
			Properties props = new Properties();
			String authflag= cdmutil.getPasswordAuthFlag();
			
			if(jsonObject.getString("SSL").equals("true")){
				props.put("mail.smtp.auth",authflag);
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", jsonObject.getString("SMTP"));
				props.put("mail.smtp.port", jsonObject.getString("PORT"));
				props.put("mail.smtp.socketFactory.port", jsonObject.getString("PORT"));
				props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				props.put("mail.smtp.timeout", 60000);
			}else{
				props.put("mail.smtp.auth",authflag);
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host",jsonObject.getString("SMTP"));
				props.put("mail.smtp.port", jsonObject.getString("PORT"));
				props.put("mail.smtp.timeout", 60000);
			}
			Session session =null;
			
			if(authflag=="true" || authflag.equals("true")){
				
				try {
					session =Session.getInstance(props,new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					  });
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(jsonObject.getString("FROM")));
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(jsonObject.getString("TO")));
					message.setSubject(jsonObject.getString("SUBJECT"));
					message.setText(jsonObject.getString("BODY"));
		 			Transport.send(message);
		 			System.out.println("Done ");
		 
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}				
			}
			
			else{
				
				try {
					session = Session.getDefaultInstance(props);
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(jsonObject.getString("FROM")));
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(jsonObject.getString("TO")));
					message.setSubject(jsonObject.getString("SUBJECT"));
					message.setText(jsonObject.getString("BODY"));
		 			Transport.send(message);
		 			System.out.println("Done without authentication");
		 
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
			}
			
			
	 		
	  }
	  
	  public static void sendNotificationMailExport(JSONObject jsonObject) throws Exception{
		  CDMRUtil cdmutil = new CDMRUtil();
			 if(cdmutil.getExportEmailNotificationChk().equals("N")){
				 System.out.println("Export Email notification has been disabled");
				 return;
			 }
		  final String username = jsonObject.getString("FROM");
		  final String password =  com.velos.stafa.util.EncryptionUtil.decrypt(jsonObject.getString("PASSWORD"));
		  
			Properties props = new Properties();
			String authflag= cdmutil.getPasswordAuthFlag();
			
			if(jsonObject.getString("SSL").equals("true")){
				props.put("mail.smtp.auth",authflag);
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", jsonObject.getString("SMTP"));
				props.put("mail.smtp.port", jsonObject.getString("PORT"));
				props.put("mail.smtp.socketFactory.port", jsonObject.getString("PORT"));
				props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				props.put("mail.smtp.timeout", 60000);
			}else{
				props.put("mail.smtp.auth",authflag);
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host",jsonObject.getString("SMTP"));
				props.put("mail.smtp.port", jsonObject.getString("PORT"));
				props.put("mail.smtp.timeout", 60000);
			}
			Session session =null;
			
			if(authflag=="true" || authflag.equals("true")){
				
				try {
					session =Session.getInstance(props,new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					  });
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(jsonObject.getString("FROM")));
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(jsonObject.getString("TO")));
					message.setSubject(jsonObject.getString("SUBJECT"));
					message.setText(jsonObject.getString("BODY"));
		 			Transport.send(message);
		 			System.out.println("Done ");
		 
				} catch (MessagingException e) {
					throw new RuntimeException(e);
					
				}				
			}
			
			else{
				
				try {
					session = Session.getDefaultInstance(props);
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(jsonObject.getString("FROM")));
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(jsonObject.getString("TO")));
					message.setSubject(jsonObject.getString("SUBJECT"));
					message.setText(jsonObject.getString("BODY"));
		 			Transport.send(message);
		 			System.out.println("Done without authentication");
		 
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
			}
	  }
	  
	  public static long getDifferenceBetweenTwoDates(String firstDate, String secondDate)  throws Exception {
		  
		  long days = 0l;
	        long MILLISECS_PER_DAY = 24 * 60 * 60 * 1000;
	        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy"); // "dd/MM/yyyy HH:mm:ss");
	        Date fDate = null;
	        Date sDate = null;        
	        try {       
	        	fDate = (Date) format.parse(firstDate);
	        	sDate = (Date) format.parse(secondDate);
	            days = (fDate.getTime() - sDate.getTime())/MILLISECS_PER_DAY;                        
	        } catch (Exception e) {  e.printStackTrace();  }   

	        return days; 
	     }
	  
	  
	  public static String getCurrentTimeStamp() {
		  
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy hh.mm.ss");
			return formatter.format(new Date());
		 
		}
	  
	  public  static JSONObject getMailConfiguration() throws Exception{
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "select PARAM_SUBTYP, PARAM_VAL from vbar_controlparam where PARAM_TYPE in ('EMAIL') and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT', 'SSL') and DELETEDFLAG = 0";
				
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("mailconfig");
				for (Map map : data) {
					
					jsonObject.put(map.get("PARAM_SUBTYP").toString(), map.get("PARAM_VAL").toString());
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObject;
	  }
	  
	  public static JSONObject getUsernameAndEmailAddress(JSONObject jsonObject) throws Exception{
			//JSONObject jsonObject = new JSONObject();
			try {
				String query = "select PK_CDM_NOTIFYCNF, USER_NAME, USER_EMAIL from CDM_NOTIFYCNF where DELETEDFLAG=0 and STATE_TO = (select PK_CODELST from VBAR_CODELST where CODELST_TYPE = 'filestatus' and CODELST_SUBTYP = 'err')";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getFromNotifycnf(jsonObject);
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "GetAndProcessRemoteData::getUsernameAndEmailAddress","1");
				e.printStackTrace();
			}
			return jsonObject;
		}
	
	  public static JSONObject getActiveInactiveRule(JSONObject jsonObject) throws Exception{
			//JSONObject jsonObject = new JSONObject();
			try {
				String query = "select CODELST_DESC from VBAR_CODELST where CODELST_TYPE = 'ACTIVATION_RULE' and CODELST_SUBTYP = '"
				+ jsonObject.getString("FILE_ID") + "'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getActiveRule(jsonObject);				
				List<Map> actrule = (List<Map>) jsonObject.get("activerule");
				jsonObject.put("ACTDACTRULE", actrule.get(0).get("CODELST_DESC").toString());
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController, "GetAndProcessRemoteData::getUsernameAndEmailAddress","1");
				e.printStackTrace();
			}
			return jsonObject;
		}
	  
	  
	  public static String[] fetchPropertyVal(String fileID) throws Exception{
			String[] strCols = null;
				
			Properties prop = new Properties();
			InputStream input = null;

			try {
				
				prop.load(CDMRUtil.class.getClassLoader().getResourceAsStream("/cdmr.properties"));
				strCols = prop.getProperty(fileID).split(",");
				
			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			return strCols;
		  }
		public static String fetchPropertyValStr(String fileID) throws Exception{
			String strCols = null;
				
			Properties prop = new Properties();
			InputStream input = null;

			try {
				
				prop.load(CDMRUtil.class.getClassLoader().getResourceAsStream("/cdmr.properties"));
				strCols = prop.getProperty(fileID);
				
			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			return strCols;
		  }
		
		public static JSONObject fetchEventLibId(JSONObject jsonObject) throws Exception{
			try{			
				
				CDMRController cdmrController = new CDMRController();
				
				String query = "select CODELST_DESC from VBAR_CODELST where CODELST_TYPE = 'EVENT_LIB_ID' and CODELST_SUBTYP = '"
					+ jsonObject.getString("FILE_ID") + "'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.fetchEventLibId(jsonObject);
				
				List<Map> chgidc = (List<Map>) jsonObject.get("eventlid");
				jsonObject.put("EVENT_ID", chgidc.get(0).get("CODELST_DESC").toString());
				
			}
					
			 catch (Exception e) {
					e.printStackTrace();
				}
			 return jsonObject;
		
		}
		
		public static JSONObject fetchChgIndcBySeq(JSONObject jsonObject) throws Exception{
			try{
				//StringBuffer buffer = new StringBuffer();
				
				CDMRController cdmrController = new CDMRController();
				//JSONObject jsonObject = new JSONObject();
				String query = "select CODELST_SUBTYP from VBAR_CODELST where CODELST_TYPE = 'CHG_TYPE' and CODELST_SEQ = '"
					+jsonObject.getString("chgtype")+"'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getChgIndcBySeq(jsonObject);
				
				List<Map> chgidc = (List<Map>) jsonObject.get("chgseqind");
				 if (chgidc.size() > 0) {
			        jsonObject.put("CHG_ID", ((Map)chgidc.get(0)).get("CODELST_SUBTYP").toString());
			      }
				
			}
					
			 catch (Exception e) {
					e.printStackTrace();
				}
			 return jsonObject;
		
		}
		
		/**
		 * @description Get Latest version of Data in cdm main
		 * @return JSONObject
		 * @throws Exception
		 */
		public static JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
			try {			
				String query = "select version_id from cdm_versiondef where is_current = 1 and FILE_ID = '"+jsonObj.getString("EVENT_ID")+"' ";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
				List<Map> list = (List<Map>)jsonObj.get("versioninfo");
				Map dataMap = list.get(0);
				jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObj;
		}
		
		public static JSONObject getMapVersion(JSONObject jsonObj) throws Exception{
			try {
							
				String vermap = fetchPropertyValStr(jsonObj.getString("FILE_ID")+"_MAPS");
				
				String query="select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID IN("+vermap+")";
								
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getMapVersion(jsonObj);
				List<Map> list = (List<Map>)jsonObj.get("mapversion");		
				jsonObj.put("MAPSVERSION", list);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObj;
		}
		
		public static void updateTransferStatus(String status,String transfertype) throws Exception{
			try {
				JSONObject jsonObject = new JSONObject();
				String query = "UPDATE vbar_codelst SET CODELST_VALUE = "+ status +" WHERE codelst_type= "+ transfertype +" AND CODELST_SUBTYP='STATUS' and DELETEDFLAG=0";		
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateTransferstatus(jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		public static String ChkFileTransferStatus(String transfertype) throws Exception{
			String status = null;
			try {
					JSONObject jsonObject = new JSONObject();
					String query = "Select CODELST_VALUE FROM vbar_codelst WHERE codelst_type= "+transfertype +" AND CODELST_SUBTYP='STATUS' and DELETEDFLAG=0";
					jsonObject.put("query", query);
					jsonObject.put("SQLQuery", true);
					jsonObject = cdmrController.dataTransferStatus(jsonObject);
					
					if(jsonObject.getString("datatransferflag")=="1"){
						System.out.println("Data transfer already in process.");
					}
					
					status=jsonObject.getString("datatransferflag");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return status;
		}
		public static String getPasswordAuthFlag() throws Exception{
			String authstatus = null;
			try {
					JSONObject jsonObject = new JSONObject();
					String query = "Select PARAM_VAL FROM VBAR_CONTROLPARAM WHERE PARAM_TYPE= 'EMAIL' AND PARAM_SUBTYP='SMTP_AUTH' and DELETEDFLAG=0";
					jsonObject.put("query", query);
					jsonObject.put("SQLQuery", true);
					jsonObject = cdmrController.getPasswordAuthFlag(jsonObject);
					authstatus=jsonObject.getString("passwordauthflag").trim();
					
			} catch (Exception e) {
				e.printStackTrace();
			}
			return authstatus;
		}
		
		/**
		 * @description Get Latest version of Data in cdm main
		 * @return JSONObject
		 * @throws Exception
		 */
		public static JSONObject getCurrentVersionAllLib(JSONObject jsonObj) throws Exception{
			try {			
				String query = "select version_id from cdm_versiondef where is_current = 1 and FILE_ID = '"+jsonObj.getString("EVENT_ID")+"' ";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
				List<Map> list = (List<Map>)jsonObj.get("versioninfo");
				Map dataMap = list.get(0);
				jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObj;
		}
}


