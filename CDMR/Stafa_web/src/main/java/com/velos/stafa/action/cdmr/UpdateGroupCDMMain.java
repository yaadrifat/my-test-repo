package com.velos.stafa.action.cdmr;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     UpdateGroupCDMMain
 * @description Update Group(Service Section) in CDM Main Table
 * @version     1.0
 * @file        UpdateGroupCDMMain.java
 * @author      Lalit Chattar
 */
public class UpdateGroupCDMMain extends StafaBaseAction{
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);

	private String grupCode;
	private String pkString;
	
	
	/*
	 * Constructor
	 */
	public UpdateGroupCDMMain(){
		cdmrController = new CDMRController();
	}
	
	/*
	 * getter and setter
	 */
	
	
	public String getGrupCode() {
		return grupCode;
	}
	public void setGrupCode(String grupCode) {
		this.grupCode = grupCode;
	}
	public String getPkString() {
		return pkString;
	}

	public void setPkString(String pkString) {
		this.pkString = pkString;
	}
	
	
	/**
	 * @description Update Selected Group(Service Section)
	 * @return String
	 * @throws Exception
	 */
	public String updateSelected() throws Exception{
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String pkStr = jsonObject.getString("pkString");
			pkStr = pkStr.substring(0, pkStr.length() - 1);
			    
				String query = "update CDM_MAIN set FK_CDM_GRPCODE = '"+jsonObject.getString("grupCode")+"' where PK_CDM_MAIN in ("+pkStr+")";
				System.out.print("Query   >>>>>>>  "+query);
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateComrec(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}

}
	
	
	
