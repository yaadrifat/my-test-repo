package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;

/**
 * @summary     GetServiceSets
 * @description Get Service Sets for Service Set view in charge Library
 * @version     1.0
 * @file        GetServiceSets.java
 * @author      Lalit Chattar
 */
public class GetServiceSets extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	private int constant, end, start;
	private StringBuilder sWhere;
	private String[] aColumns = {"SERVICE_DESC1"};
	private boolean defaltChecked;
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * getter and setter
	 */
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	/*
	 * Constructor
	 */
	public GetServiceSets(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
		this.defaltChecked = true;
	}
	
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		return SUCCESS;
	}
	
	
	/**
	 * @description Get All service Sets
	 * @return String
	 * @throws Exception
	 */
	public String getServiceSets() throws Exception{
		
		try{
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			sWhere = new StringBuilder();
			//jsonObj = fetchChgIndcBySeq(jsonObj);
			jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
			
			if ( jsonObj.getString("sSearch") != "" )
			{
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumns.length ; i++ )
				{
					sWhere.append("LOWER("+aColumns[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			jsonObj = CDMRUtil.getCurrentVersion(jsonObj);
			
			String orderby = "";
			if (jsonObj.has("iSortCol_0")){
				orderby = orderby + " ORDER BY SERVICE_DESC1 " + jsonObj.getString("sSortDir_0");
			}
			String query = "select TotalRows, PK_CDM_MAIN, SERVICE_DESC1 from (SELECT t.*, Row_Number() OVER (ORDER BY PK_CDM_MAIN desc) MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where IS_THIS_SS =1 and DELETEDFLAG = 0 and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"' "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"' group by LSERVICE_CODE, CHG_IND ) "+orderby+") WHERE MyRow BETWEEN "+start+" AND "+end;
			
			//String query = " select PK_CDM_MAIN, SERVICE_DESC1 from CDM_MAIN where IS_THIS_SS =1 and CUR_VERSION = " + jsonObj.getString("versionid") ;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			if(jsonObj.getString("checkedId").equals("")){
				this.processData(jsonObj);
			}else{
				this.processDataForCheckedId(jsonObj);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * @description process data for dispaying in Service Set view
	 * @return void
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject) throws Exception{
		try{
			String checked = "";
			
			
			List<Map> dataList = (List<Map>)jsonObject.get("cdmmain");
			for (Map map : dataList) {
				if(this.defaltChecked){
					checked = "checked";
					this.defaltChecked = false;
					
				}else{
					checked = "";
				}
				Map<String, String> data = new HashMap<String, String>();
				if(jsonObject.has("viewtype")){
					data.put("check", "<input "+checked+" type=\"radio\" id=\""+map.get("PK_CDM_MAIN").toString()+"\" value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"check\" onclick=\"getServiceSetValueForView(this.value);\"/>");
				}else{
					data.put("check", "<input "+checked+" type=\"radio\" id=\"SS"+map.get("PK_CDM_MAIN").toString()+"\"  value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"check\" onclick=\"getServiceSetValue(this.value);\"/>");
				}
				
				if((map.get("SERVICE_DESC1")) == null || (map.get("SERVICE_DESC1")) == "")
					data.put("sersets", "");
				else
					data.put("sersets", map.get("SERVICE_DESC1").toString());
				
				
				this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
				this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
				aaData.add(data);
			}
			if(this.iTotalDisplayRecords == null && this.iTotalRecords == null){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
	/**
	 * @description process data for dispaying in Service Set view
	 * @return void
	 * @throws Exception
	 */
	public void processDataForCheckedId(JSONObject jsonObject) throws Exception{
		try{
			String checked = "";
			JSONObject object = new JSONObject();
			String query = "select * from CDM_MAIN where PK_CDM_MAIN = " + jsonObject.getString("checkedId") + " and DELETEDFLAG = 0";
			object.put("query", query);
			object.put("SQLQuery", true);
			object = cdmrController.getMatchedData(object);
			List<Map> serSetData = (List<Map>)object.get("searched");
			if(serSetData.size()==0){
				processData(jsonObject);
				return;
			}
			List<Map> dataList = (List<Map>)jsonObject.get("cdmmain");
			
			for (Map map : dataList) {
				if(this.defaltChecked && jsonObject.getString("checkedId").equals(map.get("PK_CDM_MAIN").toString())){
					checked = "checked";
					this.defaltChecked = false;
					
				}else{
					checked = "";
				}
				
				Map<String, String> data = new HashMap<String, String>();
				if(jsonObject.has("viewtype")){
					data.put("check", "<input "+checked+" type=\"radio\" id=\""+map.get("PK_CDM_MAIN").toString()+"\" value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"check\" onclick=\"getServiceSetValueForView(this.value);\"/>");
				}else{
					data.put("check", "<input "+checked+" type=\"radio\" id=\"SS"+map.get("PK_CDM_MAIN").toString()+"\"  value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"check\" onclick=\"getServiceSetValue(this.value);\"/>");
				}
				
				if((map.get("SERVICE_DESC1")) == null || (map.get("SERVICE_DESC1")) == "")
					data.put("sersets", "");
				else
					data.put("sersets", map.get("SERVICE_DESC1").toString());
				
				
				this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
				this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
				aaData.add(data);
			}
			if(this.iTotalDisplayRecords == null && this.iTotalRecords == null){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
	
	/**
	 * @description Get Current version of data
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
		try {
			String query = "select version_id from cdm_versiondef where is_current = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("versioninfo");
			Map dataMap = list.get(0);
			jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonObj;
	}*/
	
}