package com.velos.stafa.util;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
* This class made for loading codelist value on start up.
* @author Mohiuddin Ali Ahmed 
* @version 1
*/
public class StafaContextListner implements ServletContextListener {

	private ServletContext context = null;
	public static final Log log=LogFactory.getLog(StafaContextListner.class);
	
	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		// TODO Auto-generated method stub
		//log.debug("Context Event Initialized");
		StafaConfig.initializeOthers(contextEvent.getServletContext());
		setContext(contextEvent.getServletContext());
		
	}

	
	
	@Override
	public void contextDestroyed(ServletContextEvent contextEvent) {
		// TODO Auto-generated method stub
		//log.debug("Context Event destroyed");
		
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}

	
}
