package com.velos.stafa.controller;

import atg.taglib.json.util.JSONObject;
import java.util.*;
import com.velos.stafa.service.VelosService;
import com.velos.stafa.service.impl.VelosServiceImpl;

/**
 * @summary     CDMRController
 * @description Controller class for passing data to the business layer
 * @version     1.0
 * @file        CDMRController.java
 * @author      Lalit Chattar
 */
public class CDMRController{
	VelosService service;
	
	public JSONObject saveCDMRFileInfo(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveFileInfo(jsonObject);
	}
	
	public JSONObject getCDMRFilesInfo(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFilesInfo(jsonObject);
	}
	
	public JSONObject saveStagingMainInfo(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveStagingMainInfo(jsonObject);
	}
	
	public JSONObject saveStagingDetailInfo(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveStagingDetailInfo(jsonObject);
	}
	
	public JSONObject getCodeList(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCodeList(jsonObject);
	}
	
	public JSONObject getStagingID(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getStagingID(jsonObject);
	}
	
	public JSONObject getActiveMapVesion(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getActiveMapVesion(jsonObject);
	}
	
	public JSONObject getStagingMaps(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getStagingMaps(jsonObject);
	}
	
	public JSONObject getStagingPrimaryID(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getStagingPrimaryID(jsonObject);
	}
	
	public JSONObject getStagingDetailData(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getStagingDetailData(jsonObject);
	}
	
	public JSONObject getCompReqColumnMapDetail(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCompReqColumnMapDetail(jsonObject);
	}
	
	public JSONObject saveCompRecInfo(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveCompRecInfo(jsonObject);
	}
	
	public JSONObject getStagingColumnMapDetail(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getStagingColumnMapDetail(jsonObject);
	}
	
	public JSONObject saveInStagingCompReq(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveInStagingCompReq(jsonObject);
	}
	
	public JSONObject getCompRecData(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCompRecData(jsonObject);
	}
	
	public JSONObject countRecord(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.countRecord(jsonObject);
	}
	
	public JSONObject getGroups(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getGroups(jsonObject);
	}
	
	public JSONObject updateComrec(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateComrec(jsonObject);
	}
	
	public JSONObject getGroupCodeValue(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getGroupCodeValue(jsonObject);
	}
	
	public JSONObject updateFileStatus(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateFileStatus(jsonObject);
	}
	
	public JSONObject getNextPossibleStatus(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getNextPossibleStatus(jsonObject);
	}
	
	public JSONObject insertInCDMMain(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.insertInCDMMain(jsonObject);
	}
	public JSONObject getMatchedData(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getMatchedData(jsonObject);
	}
	public JSONObject saveInMainCDM(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveInMainCDM(jsonObject);
	}
	
	public JSONObject getCDMVersionDetail(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCDMVersionDetail(jsonObject);
	}
	
	public JSONObject saveCdmVersionDef(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveCdmVersionDef(jsonObject);
	}
	
	public JSONObject updateCdmVersionDef(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateCdmVersionDef(jsonObject);
	}
	
	public JSONObject getFromCdmMain(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFromCdmMain(jsonObject);
	}
	
	public JSONObject saveInSSLink(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveInSSLink(jsonObject);
	}
	
	public JSONObject getFromCdmSSLink(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFromCdmSSLink(jsonObject);
	}
	
	public JSONObject saveNewServiceSet(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveNewServiceSet(jsonObject);
	}
	
	public JSONObject getMaxGrupCode(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getMaxGrupCode(jsonObject);
	}
	public JSONObject saveInCdmGrupCode(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveInCdmGrupCode(jsonObject);
	}
	public JSONObject updateInCdmGrupCode(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateInCdmGrupCode(jsonObject);
	}
	public JSONObject getFromCDMGroupCode(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFromCDMGroupCode(jsonObject);
	}
	public JSONObject getTechnicalCharges(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getTechnicalCharges(jsonObject);
	}
	public JSONObject getProfessionalCharges(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getProfessionalCharges(jsonObject);
	}
	public JSONObject insertInStagingTemp(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.insertInStagingTemp(jsonObject);
	}
	public JSONObject truncateStagingTemp(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.truncateStagingTemp(jsonObject);
	}
	public JSONObject getMappingForSQLBinding(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getMappingForSQLBinding(jsonObject);
	}
	
	public JSONObject getFromStagingTemp(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFromStagingTemp(jsonObject);
	}
	public  JSONObject getMapVersion(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getMapVersion(jsonObject);
	}
	public JSONObject getCdmMain(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCdmMain(jsonObject);
	}
	public JSONObject saveStagingDetail(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveStagingDetail(jsonObject);
	}
	public JSONObject getChagIndc(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getChagIndc(jsonObject);
	}
	public JSONObject updateFilesLibForRemoteData(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateFilesLibForRemoteData(jsonObject);
	}
	
	public JSONObject editServiceSection(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.editServiceSection(jsonObject);
	}
	
	public JSONObject editServiceSet(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.editServiceSection(jsonObject);
	}
	public JSONObject getUserDetail(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getUserDetail(jsonObject);
	}
	
	public JSONObject getFromNotifycnf(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFromNotifycnf(jsonObject);
	}
	
	public JSONObject getFromEsec(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFromEsec(jsonObject);
	}
	
	public JSONObject saveNotificationInfo(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveNotificationInfo(jsonObject);
	}
	
	public JSONObject getSchedulerTime(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSchedulerTime(jsonObject);
	}
	
	public JSONObject updateSchdulerTime(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateSchdulerTime(jsonObject);
	}
	
	public JSONObject deleteNotifiedUser(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.deleteNotifiedUser(jsonObject);
	}
	
	public JSONObject deleteNotification(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.deleteNotification(jsonObject);
	}
	
	public JSONObject loadAllNotification(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.loadAllNotification(jsonObject);
	}
	
	public JSONObject saveErrorLog(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveErrorLog(jsonObject);
	}
	
	public JSONObject updateErrorLog(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateErrorLog(jsonObject);
	}
	
	public JSONObject getMailConfigurationInfo(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getMailConfigurationInfo(jsonObject);
	}
	
	public JSONObject getNotificationMessages(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getNotificationMessages(jsonObject);
	}
	
	public JSONObject updateNotification(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateNotification(jsonObject);
	}
	
	public JSONObject updateEmailSettings(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateEmailSettings(jsonObject);
	}
	
	public JSONObject getEmailSettings(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getEmailSettings(jsonObject);
	}
	
	public JSONObject updateNoOfFiles(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateNoOfFiles(jsonObject);
	}
	public JSONObject getNumberOfFiles(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getNumberOfFiles(jsonObject);
	}
	public JSONObject getSuperAdmin(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSuperAdmin(jsonObject);
	}
	public JSONObject saveForgotPasswordRequest(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveForgotPasswordRequest(jsonObject);
	}
	
	public JSONObject getDefaultPassword(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getDefaultPassword(jsonObject);
	}
	
	public JSONObject getUserEmail(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getUserEmail(jsonObject);
	}
	
	public JSONObject updateNotificationStatus(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateNotificationStatus(jsonObject);
	}
	
	public JSONObject saveInObsSvcBfr(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveInObsSvcBfr(jsonObject);
	}
	public List<Map> fetchColumnForDataTable(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.fetchColumnForDataTable(jsonObject);
	}
	public void saveColumnState(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		service.saveColumnState(jsonObject);
	}
	public void saveColumnOrder(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		service.saveColumnOrder(jsonObject);
	}
	
	public JSONObject getFilePkForStatus(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFilePkForStatus(jsonObject);
		
	}
	public JSONObject getFileSourceLocation(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFileSourceLocation(jsonObject);
		
	}
	public JSONObject getFileInformation(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFileInformation(jsonObject);
		
	}
	public boolean checkForExistingFile(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.checkForExistingFile(jsonObject);
		
	}
	public JSONObject getPullMethod(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getPullMethod(jsonObject);
		
	}
	public JSONObject getChgIndc(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getChgIndc(jsonObject);
		
	}
	public List getInterfaceList(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getInterfaceList(jsonObject);
		
	}
	public JSONObject getProcessedDirLocation(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getProcessedDirLocation(jsonObject);
	}
	public JSONObject getFileName(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFileName(jsonObject);
		
	}
	public JSONObject getFileID(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getFileID(jsonObject);
		
	}
	public JSONObject getModulename(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getModulename(jsonObject);
		
	}
	public JSONObject getMatchingCriteria(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getMatchingCriteria(jsonObject);
		
	}
	public JSONObject getEventLibId(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getEventLibId(jsonObject);
		
	}
	public JSONObject getchgtype(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getchgtype(jsonObject);
		
	}
	public JSONObject getChgIndcBySeq(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getChgIndcBySeq(jsonObject);
		
	}
	public JSONObject fetchEventLibId(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.fetchEventLibId(jsonObject);
		
	}
	public JSONObject fetchVersionID(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.fetchVersionID(jsonObject);
		
	}
	public JSONObject getActiveRule(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getActiveRule(jsonObject);
		
	}
	public JSONObject fetchRTRule(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.fetchRTRule(jsonObject);
		
	}
	
	public JSONObject fetchcolTypeMapDetail(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.fetchcolTypeMapDetail(jsonObject);
		
	}

	/*Advanced search methods start*/
	public JSONObject getAdvsearchCols(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getAdvsearchCols(jsonObject);
		
	}
	public JSONObject checkForFilterName(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.checkForFilterName(jsonObject);
		
	}
		
	public JSONObject saveNewFilter(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveNewFilter(jsonObject);
	}
	public JSONObject loadFilters(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.loadFilters(jsonObject);
	}
	public JSONObject saveFilterParam(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveFilterParam(jsonObject);
	}
	public JSONObject updateCdmFilMaint(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateCdmFilMaint(jsonObject);
	}
	public JSONObject getCdmFilParam(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getCdmFilParam(jsonObject);
	}
	public JSONObject getSavedFilters(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getSavedFilters(jsonObject);
	}
	public JSONObject getPkforFilter(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getPkforFilter(jsonObject);
	}
	public JSONObject updateCdmParam(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateCdmParam(jsonObject);
	}
	public JSONObject transferToCtms()throws Exception{
		service = VelosServiceImpl.getService();
		return service.transferToCtms();
	}
	public JSONObject getexportDatacount(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getexportDatacount(jsonObject);
	}
	public JSONObject dataTransferStatus(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.dataTransferStatus(jsonObject);
	}
	public JSONObject updateTransferstatus(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.updateTransferstatus(jsonObject);
	}
	public JSONObject saveNewNotification(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.saveNewNotification(jsonObject);
	}
	public JSONObject getPasswordAuthFlag(JSONObject jsonObject)throws Exception{
		service = VelosServiceImpl.getService();
		return service.getPasswordAuthFlag(jsonObject);
	}
	public JSONObject getLibraryName(JSONObject jsonObject) throws Exception{
		service = VelosServiceImpl.getService();
		return service.getLibraryName(jsonObject);
		
	}
	
}