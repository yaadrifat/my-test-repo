package com.velos.stafa.action.cdmr;
import java.util.Calendar;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @summary     SchedularJobsExport
 * @description Execute Job According Schedular for exporting to CRMS
 * @version     1.0
 * @file        SchedularJobsExport.java
 * @author      Shikha Srivastav
 */

public class SchedularJobsExport implements Job {
	
	private static boolean fireFlag = true;
	
	/**
	 * @description Execute Job
	 * @return void
	 * @throws Exception
	 */
	public void execute(JobExecutionContext context)throws JobExecutionException {

		try {
			System.out.println("Scheduled export started  -----------------------------");
			ProcessandExportData.getAndExportData();
			System.out.println("Scheduled export done  --------------------------------");
		} catch (Exception e) {
		 if(fireFlag){
			 JobExecutionException e2 = new JobExecutionException();
				this.OnErrorScheduleJob(context);
				fireFlag = false;
		 }
			
			
			
		}
	}
	
	
	/**
	 * @description Handle Error in JOb Execution and Reschedule after 15 Minute
	 * @return void
	 * @throws Exception
	 */
	 private void OnErrorScheduleJob(JobExecutionContext context){
		 
	  try {
		  	 SchedulerFactory sf = new StdSchedulerFactory();
			 Scheduler sched = sf.getScheduler();
			 
			 Calendar now = Calendar.getInstance();
			 int hours = now.get(Calendar.HOUR_OF_DAY);
			 int minute = now.get(Calendar.MINUTE);
			 if(minute == 59){
				 minute = 0;
			 }
	         JobDetail job = new JobDetail("ONERRORJOB1", "ERROR1", SchedularJobsExport.class);
	         CronTrigger cronTrigger = new CronTrigger("NewTrigger1",sched.DEFAULT_GROUP, "0 "+(minute + 15)+" "+hours+" * * ?");  
	         sched.scheduleJob(job, cronTrigger);
	         sched.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
