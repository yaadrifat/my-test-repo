package com.velos.stafa.action;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.velos.stafa.business.domain.CodeList;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.util.VelosStafaConstants;

public  abstract class StafaBaseAction extends ActionSupport implements VelosStafaConstants {

	public List nextOptions;
	ServletContext context = ServletActionContext.getServletContext();
	HttpServletRequest request = (HttpServletRequest)ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
	Map<String,CodeList> codeListMapByTypeSubtype = null;	
	Map<Long,CodeList> codeListMapByIds = null;	
	Map<String,List<CodeList>> codeListMap = null;
	private Long transplantId;
	private Long recipient;
	private Long personId;
	private Long visitId;
	private List<Object> workflowList;
	
	

	public Long getVisitId() {
		return visitId;
	}

	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}

	public Long getRecipient() {
		return recipient;
	}

	public void setRecipient(Long recipient) {
		this.recipient = recipient;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public List<Object> getWorkflowList() {
		return workflowList;
	}

//	public void setWorkflowList(List<Object> workflowList) {
//		this.workflowList = workflowList;
//	}

	public Long getTransplantId() {
		return transplantId;
	}

	public void setTransplantId(Long transplantId) {
		this.transplantId = transplantId;
	}

	public List getNextOptions() {
		return nextOptions;
	}

	public void setNextOptions(List nextOptions) {
		this.nextOptions = nextOptions;
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}
	
	public StafaBaseAction(){
		HttpSession session=request.getSession(false);
		if(session!=null){
			codeListMap = (Map<String,List<CodeList>>)getContext().getAttribute("codeListValues");
			codeListMapByTypeSubtype = (Map<String,CodeList>)getContext().getAttribute("codeListMapByTypeSubtype");
			codeListMapByIds =(Map<Long,CodeList>)getContext().getAttribute("codeListMapByIds");
		}
	}
	
	public Long getCodeListPkByTypeAndSubtype(String type, String subType){
		  String pk = type+"-"+subType;
		  CodeList c = codeListMapByTypeSubtype.get(pk);
		  return c!=null?c.getPkCodelst():null;
	}
	
	public String getCodeListDescByTypeAndSubtype(String type, String subType){
		  String pk = type+"-"+subType;
		  CodeList c = codeListMapByTypeSubtype.get(pk);
		  return c!=null && !StringUtils.isEmpty(c.getDescription())?c.getDescription():null;
	}	
	
	public String getCodeListDescById(Long Id){
		 return codeListMapByIds.get(Id).getDescription();	
	}
	
//	public List<Object> getWorkflowList(Long transplantId){
//		List<Object> list = null;
//		setTransplantId(transplantId);
//		try{
//			String sql = VelosStafaSqlConstants.SQL_RECIPIENT_WORKFLOW;
//			sql = sql.replace(":WORKFLOWID:", transplantId.toString());
//			list = CommonController.getDataFromSQLQuery(sql);
//			if(list!=null)
//				setWorkflowList(list);
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		return list;
//	}
	
}
