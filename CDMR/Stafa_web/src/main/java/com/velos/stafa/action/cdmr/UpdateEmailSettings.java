package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;


public class UpdateEmailSettings extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	private List<Map> data; 
	
	public List<Map> getData() {
		return data;
	}

	public void setData(List<Map> data) {
		this.data = data;
	}
	
	public UpdateEmailSettings(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * @description Get Child Group (Sewrvice Section) in tree View
	 * @return String
	 * @throws Exception
	 */
	public String updateEmailSettings()throws Exception {
	
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("host")+"' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'SMTP'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("port")+"' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'PORT'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("fromemail")+"' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'FROM'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +com.velos.stafa.util.EncryptionUtil.encrypt(jsonObj.getString("password"))+"' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'PASSWORD'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("subject")+"' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'SUBJECT'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("sslenable")+"' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'SSL'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			////for export settings---------
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("host")+"' where PARAM_TYPE = 'CTMSEMAIL' and PARAM_SUBTYP = 'SMTP'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("port")+"' where PARAM_TYPE = 'CTMSEMAIL' and PARAM_SUBTYP = 'PORT'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("fromemail")+"' where PARAM_TYPE = 'CTMSEMAIL' and PARAM_SUBTYP = 'FROM'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +com.velos.stafa.util.EncryptionUtil.encrypt(jsonObj.getString("password"))+"' where PARAM_TYPE = 'CTMSEMAIL' and PARAM_SUBTYP = 'PASSWORD'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			
			/*query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("subject")+"' where PARAM_TYPE = 'CTMSEMAIL' and PARAM_SUBTYP = 'SUBJECT'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			*/
			query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" +jsonObj.getString("sslenable")+"' where PARAM_TYPE = 'CTMSEMAIL' and PARAM_SUBTYP = 'SSL'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateEmailSettings(jsonObj);
			
			/////password authentication realted flag updation according to value provided in password field
			if(jsonObj.getString("password").equals("")|| jsonObj.getString("password")==""){
				query = "update VBAR_CONTROLPARAM set PARAM_VAL = 'false' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'SMTP_AUTH'";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.updateEmailSettings(jsonObj);
			}
			else{
				query = "update VBAR_CONTROLPARAM set PARAM_VAL = 'true' where PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP = 'SMTP_AUTH'";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.updateEmailSettings(jsonObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String getEmailSettings()throws Exception {
		
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String query = "SELECT PARAM_VAL, PARAM_SUBTYP from VBAR_CONTROLPARAM WHERE PARAM_TYPE = 'EMAIL' and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT', 'SSL')";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getEmailSettings(jsonObj);
			this.data = (List<Map>)jsonObj.get("mailsetting");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
}
