package com.velos.stafa.util;

import java.util.ResourceBundle;

import com.velos.stafa.business.util.WebserviceUtil;
import com.velos.stafa.objects.TaskObject;

public class TaylorServices {
	private final String TAYLOR_SERVICEURL ="TAYLOR_SERVICEURL";
	private final String TAYLOR_USERNAME ="TAYLOR_USERNAME";
	private final String TAYLOR_PASSWORD ="TAYLOR_PASSWORD";
	
	private String taylorUrl;
	private String taylorUserName;
	private String taylorPassword;
	
	private String taskHeader ="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.business.taylor.velos.com/\"><soapenv:Header/>" +
			"<soapenv:Body>";
	private String taskFooter="</soapenv:Body></soapenv:Envelope>";
	      
	
	public TaylorServices(){
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		taylorUrl = resource.getString(TAYLOR_SERVICEURL);
		taylorUserName = resource.getString(TAYLOR_USERNAME);
		taylorPassword = resource.getString(TAYLOR_PASSWORD);
		System.out.println("taylorUrl URL :::::::::::::  " + taylorUrl);
	}
	
	
	public  StringBuffer createTask(TaskObject taskObject){
		StringBuffer result=new StringBuffer();
		StringBuffer xmlIp = new StringBuffer();
		xmlIp.append(taskHeader);
		xmlIp.append("<ser:saveTask>");
		xmlIp.append("<task>");
			xmlIp.append("<applnCode>"+taskObject.getApplicationCode()+"</applnCode>");
			xmlIp.append("<assignUserName>"+taskObject.getAssignUserNames()+"</assignUserName>");
			xmlIp.append("<contextCode>"+taskObject.getContextCode()+"</contextCode>");
			xmlIp.append("<createUserId>"+taskObject.getCreateUserId()+"</createUserId>");
			xmlIp.append("<entityName>"+taskObject.getEntityName()+"</entityName>");
			xmlIp.append("<entityId>"+taskObject.getEntityId()+"</entityId>");
			xmlIp.append("<entityType>"+taskObject.getEntityType()+"</entityType>");
			xmlIp.append("<password>"+taylorPassword+"</password>");
			xmlIp.append("<sharedUserNames>"+taskObject.getSharedUserNames()+"</sharedUserNames>");
			xmlIp.append("<taskDescription>"+taskObject.getTaskDescription()+"</taskDescription>");
			xmlIp.append("<taskName>"+taskObject.getTaskName()+"</taskName>");
			xmlIp.append("<taskstatus>"+taskObject.getTaskstatus()+"</taskstatus>");
			xmlIp.append("<userName>"+taylorUserName+"</userName>");
			// not in use stafa
			 /* <applnCode>STAFA</applnCode>
	          <!--Zero or more repetitions:-->
            <assignUserName>?</assignUserName>
            <!--Zero or more repetitions:-->
            <contextCode>?</contextCode>
            <!--Optional:-->
            <createUserName>?</createUserName>
            <!--Optional:-->
            <endDate>?</endDate>
            <!--Optional:-->
            <entityId>?</entityId>
            <!--Optional:-->
            <entityName>?</entityName>
            <!--Optional:-->
            <entityType>?</entityType>
            <!--Optional:-->
            <loginName>?</loginName>
            <!--Optional:-->
            <priority>?</priority>
            <!--Zero or more repetitions:-->
            <sharedUserNames>?</sharedUserNames>
            <!--Optional:-->
            <startDate>?</startDate>
            <!--Optional:-->
            <taskDescription>?</taskDescription>
            <!--Optional:-->
            <taskName>?</taskName>
            <!--Optional:-->
            <taskstatus>?</taskstatus>
	            <taskName>Stafa  task1</taskName>
	            <taskstatus>NEW</taskstatus>
	            <userName>admin</userName>
			*/
			
		xmlIp.append(" </task>");
		xmlIp.append("</ser:saveTask>");
		xmlIp.append(taskFooter);
		result = WebserviceUtil.invokeService(taylorUrl, xmlIp);
		
		return result;
	}
	public  StringBuffer savingNotes(String xmlParam){
		StringBuffer result = null;
		try{
			
			StringBuffer xmlIp = new StringBuffer();
			xmlIp.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ser='http://service.business.taylor.velos.com/'><soapenv:Header/><soapenv:Body>");
			xmlIp.append(xmlParam);
			xmlIp.append("</soapenv:Body></soapenv:Envelope>");
			result = WebserviceUtil.invokeService(taylorUrl, xmlIp);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
}
