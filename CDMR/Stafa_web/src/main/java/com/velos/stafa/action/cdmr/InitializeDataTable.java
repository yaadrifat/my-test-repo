package com.velos.stafa.action.cdmr;

import java.util.*;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;




/**
 * @summary     InitializeDataTable
 * @description To Initialize Datatable at the time of load
 * @version     1.0
 * @file        InitializeDataTable.java
 * @author      Rajendra Prasad Padhi
 */
public class InitializeDataTable extends StafaBaseAction{
	
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	private List<Map> colData = new ArrayList<Map>();
	CDMRController cdmrController=null;
	
	public void setColData(List<Map> colData){
		this.colData=colData;
	}
	public List<Map> getColData(){
		return colData;
	}
	public InitializeDataTable(){
		cdmrController = new CDMRController();
	}
	
	/**
	 * @description FetchColumn from database
	 * @return String
	 * @throws Exception
	 */
	public String fetchColumn() throws Exception{
		try{
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			colData= cdmrController.fetchColumnForDataTable(jsonObj);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String saveColumnState() throws Exception{
		try{
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			cdmrController.saveColumnState(jsonObj);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String saveColumnOrder() throws Exception{
		try{
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			cdmrController.saveColumnOrder(jsonObj);
			System.out.println("json------------------------- "+jsonObj);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	
	
	
	
}