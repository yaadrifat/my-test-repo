package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.velos.stafa.util.CDMRUtil;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;


/**
 * @summary     FilesInfoAction
 * @description Get File Information for displaying in header in staging.
 * @version     1.0
 * @file        FilesInfoAction.java
 * @author      Lalit Chattar
 */
public class FilesInfoAction extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	private Integer fileeID;
	private List<String> fileInfo;
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * getter and setter
	 */
	public Integer getFileeID() {
		return fileeID;
	}
	public void setFileeID(Integer fileeID) {
		this.fileeID = fileeID;
	}
	public List<String> getFileInfo() {
		return fileInfo;
	}
	public void setFileInfo(List<String> fileInfo) {
		this.fileInfo = fileInfo;
	}

	/*
	 * Constructor
	 */
	public FilesInfoAction(){
		cdmrController = new CDMRController();
		fileInfo = new ArrayList<String>();
	}
	
	
	/**
	 * @description Default Method of action for processing file information
	 * @return String
	 * @throws Exception
	 */
	public String execute() throws Exception{
		JSONObject jsonObj = new JSONObject();
		try{
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			jsonObj = VelosUtil.constructJSON(request);
			String esec_DB = CDMRUtil.fetchPropertyValStr("ESEC_DB");
			String query = CDMRSqlConstants.STAGING_HEADER_FILE_INFO +esec_DB+".sec_user u where u.pk_userid=(CASE WHEN c.last_modified_by IS NULL THEN c.creator ELSE c.last_modified_by END) and  c.pk_cdm_fileslib = "+ jsonObj.getString("fileID");
			
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMRFilesInfo(jsonObj);
			System.out.println("############################################");
			List<Map> dataMap = (List<Map>)jsonObj.get("filesinfo");
			Map map = dataMap.get(0);
			fileInfo.add(this.getFileStatus(map.get("FILE_CUR_STATUS").toString(), codelist));
			fileInfo.add(map.get("USER_FIRSTNAME").toString());
			fileInfo.add(map.get("USER_LASTNAME").toString());
			fileInfo.add(map.get("FILE_NAME").toString());
			
			fileInfo.add(CDMRUtil.formateDate("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", map.get("REVICEDON").toString(), null));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * @description Get File Current Status
	 * @return String
	 * @throws Exception
	 */
	public String getFileStatus(String status, List<CodeList> codeList) throws Exception{
		for (CodeList codeListObj : codeList) {
			if(codeListObj.getPkCodeList() == Long.parseLong(status)){
				return codeListObj.getCodeListDesc();
			}
		}
		return "";
	}
}