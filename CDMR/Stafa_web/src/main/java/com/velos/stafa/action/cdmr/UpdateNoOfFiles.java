package com.velos.stafa.action.cdmr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.helper.UploadAndProcessHelper;
import com.velos.stafa.util.CDMRUtil;




public class UpdateNoOfFiles extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * Parameter getting from request
	 */
	private Integer nooffiles;
	
	public UpdateNoOfFiles() {
		cdmrController = new CDMRController();
	}
	

	/*
	 * Getter and Setter
	 */
	
	public int getNooffiles() {
		return nooffiles;
	}

	public void setNooffiles(int nooffiles) {
		this.nooffiles = nooffiles;
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return updateNoOfFiles();
	}
	
	/**
	 * @description Save Line Items in CDM Main
	 * @return String
	 * @throws Exception
	 */
	public String updateNoOfFiles()throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String query = "update VBAR_CONTROLPARAM set PARAM_VAL = '" + jsonObject.getString("nooffiles")  + "' where PARAM_TYPE = 'FILES' and PARAM_SUBTYP = 'HOME_FILES' and DELETEDFLAG = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			cdmrController.updateNoOfFiles(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
}
