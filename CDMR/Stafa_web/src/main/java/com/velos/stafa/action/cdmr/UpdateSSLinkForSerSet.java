package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LoadTreeGroupViewAction
 * @description Load Tree View in Charge Library
 * @version     1.0
 * @file        LoadTreeGroupViewAction.java
 * @author      Lalit Chattar
 */
public class UpdateSSLinkForSerSet extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 private boolean checkForExistingName;

	/*
	 * Getter and Setter

	*/
	
	 public boolean isCheckForExistingName() {
			return checkForExistingName;
	}
	public void setCheckForExistingName(boolean checkForExistingName) {
		this.checkForExistingName = checkForExistingName;
	}
	
	public UpdateSSLinkForSerSet(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String updateSSLinkForSerSet() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
			String sersetname = jsonObj.getString("sersetname");
			String deletedPk = jsonObj.getString("deletedPk");
			String deletedCdmpk = jsonObj.getString("deleteCDMPk");
			String sersetId = jsonObj.getString("sersetId");
			String changeFlagSS = jsonObj.getString("changeFlagSS");
			String eventlib_id= jsonObj.getString("EVENT_ID");
			if(changeFlagSS.equals("true")){
				if(!this.checkForExistingName(sersetname)){
					setCheckForExistingName(false);
					return SUCCESS;
				}else{
					setCheckForExistingName(true);
					JSONObject ssInfo = this.getServiceSetInfo(sersetId);
					List<Map> data = this.getServiceSetItemInfo(deletedCdmpk);
					this.updateCDMain(sersetname, sersetId, jsonObj.getInt("user"));
					this.updateSSLink(deletedPk, sersetId);
					this.saveInObsSvcBfr(ssInfo, sersetname, data, changeFlagSS,eventlib_id);
				}
			}else{
				setCheckForExistingName(true);
				JSONObject ssInfo  = this.getServiceSetInfo(sersetId);
				List<Map> data = this.getServiceSetItemInfo(deletedCdmpk);
				this.updateSSLink(deletedPk, sersetId);
				this.updateCDMain(sersetname, sersetId, jsonObj.getInt("user"));
				this.saveInObsSvcBfr(ssInfo, sersetname, data, changeFlagSS, eventlib_id);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return SUCCESS;
	}
	
	
	public void updateCDMain(String sersetname, String sersetId, int user) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "update CDM_MAIN set SERVICE_DESC1 = '"+sersetname+"' , SERVICE_DESC2 = '"+sersetname+"', LAST_MODIFIED_BY = "+user+", LAST_MODIFIED_DATE =  TO_TIMESTAMP('"+CDMRUtil.getCurrentTimeStamp()+"', 'DD-MON-RR HH.MI.SS') where PK_CDM_MAIN = " + sersetId ;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.editServiceSet(jsonObj);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * @description Get Service set link Information
	 * @return JSONObject
	 * @throws Exception
	 */
	public void updateSSLink(String deletedPk, String sersetId) throws Exception{
		try {
			if(deletedPk.equalsIgnoreCase(""))
				return;
			JSONObject jsonObj = new JSONObject();
			String pkId = deletedPk.substring(0, (deletedPk.length()-1));
			String query = "update CDM_SSLINK set DELETEDFLAG = 1 where pk_CDM_SSLINK in ("+pkId+")" ;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.editServiceSet(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean checkForExistingName(String sersetname) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "select * from CDM_MAIN where UPPER(SERVICE_DESC1) = '"+sersetname.toUpperCase()+"' and UPPER(SERVICE_DESC2) = '"+sersetname.toUpperCase()+"' and DELETEDFLAG = 0" ;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			List<Map> dataMap = (List<Map>)jsonObj.get("cdmmain");
			if(dataMap.size() > 0){
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * @description Make entry in CDM_OBISVCBFR table
	 * @return void
	 * @throws Exception
	 */
	public void saveInObsSvcBfr(JSONObject ssInfo, String sNewName, List<Map> data, String changeFlagSS,String eventlib_id) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			if(changeFlagSS.equalsIgnoreCase("true")){
				jsonObj.put("ssId", ssInfo.getString("ssId"));
				jsonObj.put("ssNameOrg", ssInfo.getString("ssNameOrg"));
				jsonObj.put("ssNameNew", sNewName);
				jsonObj.put("opCode", "R");
				jsonObj.put("eventLibid", eventlib_id);
				cdmrController.saveInObsSvcBfr(jsonObj);
			}
			jsonObj.remove("ssNameNew");
			if(data.size() != 0){
				for (Map map : data) {
					jsonObj.put("ssId", map.get("LSERVICE_CODE").toString());
					jsonObj.put("serviceName", map.get("SERVICE_DESC1").toString());
					jsonObj.put("opCode", "D");
					jsonObj.put("ssNameOrg", ssInfo.getString("ssNameOrg"));
					jsonObj.put("chargeType", map.get("CHG_IND").toString());
					jsonObj.put("eventLibid", eventlib_id);
					JSONObject jsonSvcCnt = getDeletedServiceCount(jsonObj);
					
					if(jsonSvcCnt.getInt("svccnt")==0)
					{
						cdmrController.saveInObsSvcBfr(jsonObj);
					}
					
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JSONObject getServiceSetInfo(String sersetId) throws Exception{
		JSONObject jsonObj = new JSONObject();
		try{
			
			//String query = "select LSERVICE_CODE, SERVICE_DESC1 from CDM_MAIN where PK_CDM_MAIN = " + sersetId;
			String query = "select LSERVICE_CODE, SERVICE_DESC1 from CDM_MAIN where PK_CDM_MAIN = " + sersetId;
			
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			
			List<Map> data = (List<Map>)jsonObj.get("cdmmain");
			jsonObj.put("ssId", data.get(0).get("LSERVICE_CODE").toString());
			jsonObj.put("ssNameOrg", data.get(0).get("SERVICE_DESC1").toString());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return jsonObj;
	}
	
	public List<Map> getServiceSetItemInfo(String deletedCdmpk) throws Exception{
		JSONObject jsonObj = new JSONObject();
		List<Map> data = new ArrayList<Map>();
		try{
			if(deletedCdmpk.equalsIgnoreCase(""))
				return data;
			String pkId = deletedCdmpk.substring(0, (deletedCdmpk.length()-1));
			String query = "select LSERVICE_CODE, SERVICE_DESC1, CHG_IND from CDM_MAIN where PK_CDM_MAIN in (" + pkId + ")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			data = (List<Map>)jsonObj.get("cdmmain");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return data;
	}
	
	public JSONObject getDeletedServiceCount(JSONObject ssInfo) throws Exception{
		JSONObject jsonObj = new JSONObject();
		try{				
			
			//String query = "select LSERVICE_CODE, SERVICE_DESC1 from CDM_MAIN where PK_CDM_MAIN = " + sersetId;
			String query = "Select count(PK_CDM_SSLINK) AS SVCCOUNT FROM cdm_sslink "+
						   "where SS_ID = (SELECT LSERVICE_CODE from CDM_MAIN where SERVICE_DESC1 = '"+ ssInfo.getString("ssNameOrg") +"' and IS_THIS_SS = '1' and deletedflag=0) "+
						   "and SS_CHILD_ID = '"+ssInfo.getString("ssId")+"' and deletedflag = '0' and event_lib_id="+ssInfo.getString("eventLibid");
			
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			
			List<Map> data = (List<Map>)jsonObj.get("cdmmain");
			jsonObj.put("svccnt", data.get(0).get("SVCCOUNT").toString());			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return jsonObj;
	}
}
