package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRUtil;

/**
 * @summary     GetServiceSetData
 * @description Get data for Servcie Sets
 * @version     1.0
 * @file        GetServiceSetData.java
 * @author      Lalit Chattar
 */
public class GetServiceSetData extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	private int constant, end, start;
	private StringBuilder sWhere;
	private String sOrderBy = null;
	private String[] aColumnsSer;// = { "SERVICE_DESC1", "SERVICE_DESC2", "CMS_HCPCS_CPT", "CMS_HCPCS_CPT_EFF_DATE", "SERVICE_CODE", "PRS_CODE", "MCAID_HCPCS_CPT", "MCAID_HCPCS_CPT_EFF_DATE", "CPT_CODE", "BLUE_CROSS_HCPCS_CPT", "BLUE_CROSS_HCPCS_CPT_EFF_DATE", "REV_CLASS", "CUSTOM_DESC1", "REV_CODE", "HCC_TECH_CHG", "PRS_PHY_CHG", "EFF_FROM_DATE", "EFF_TO_DATE"};
	//private String[] aColumns; //= { "SERVICE_DESC1", "SERVICE_DESC2", "CMS_HCPCS_CPT", "TO_DATE(CMS_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "SERVICE_CODE", "PRS_CODE", "MCAID_HCPCS_CPT", "TO_DATE(MCAID_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "CPT_CODE", "BLUE_CROSS_HCPCS_CPT", "TO_DATE(BLUE_CROSS_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "TO_NUMBER(REV_CLASS)", "CUSTOM_DESC1", "TO_NUMBER(REV_CODE)", "TO_NUMBER(HCC_TECH_CHG)", "TO_NUMBER(PRS_PHY_CHG)", "TO_DATE(EFF_FROM_DATE, 'DD-MM-YYYY')", "TO_DATE(EFF_TO_DATE, 'DD-MM-YYYY')"};
	private String[] dateArray;// = {"CMS_HCPCS_CPT_EFF_DATE", "MCAID_HCPCS_CPT_EFF_DATE", "BLUE_CROSS_HCPCS_CPT_EFF_DATE", "EFF_FROM_DATE", "EFF_TO_DATE"};
	private String[] numberArray;// = {"HCC_TECH_CHG", "PRS_PHY_CHG"};
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * getter and setter
	 */
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	/*
	 * Constructor
	 */
	public GetServiceSetData(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		return SUCCESS;
	}
	
	
	/**
	 * @description Get Data for Servcie Sets
	 * @return String
	 * @throws Exception
	 */
	public String getServiceSetData() throws Exception{
		try {			
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			aColumnsSer = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_COLS");
			dateArray = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_DATEARY");
			numberArray = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_NUMARY");
			
			List<String> dateList = Arrays.asList(dateArray);
			List<String> numberList = Arrays.asList(numberArray);
			
			sWhere = new StringBuilder();
			
			CDMRUtil.getMapVersion(jsonObj);
			this.getColumnFromStagingMap(jsonObj);
			jsonObj = CDMRUtil.getActiveInactiveRule(jsonObj);
			
			String columnNames = "";
			List<Map> columnsNameMap = (List<Map>)jsonObj.get("columnsNames");
			for (Map map : columnsNameMap) {
				String type = map.get("MAINCDM_COLTYPE") == null? "":map.get("MAINCDM_COLTYPE").toString();
				if(type.equalsIgnoreCase("TIMESTAMP")){
					columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", 'dd-MON-YYYY') as " +map.get("EQMAINCDM_COL").toString()  + ",";
				}else{
					columnNames = columnNames +map.get("EQMAINCDM_COL").toString()+ ",";
				}
				
			}
			columnNames = columnNames.substring(0, (columnNames.length()-1));
			
			
			
			if ( jsonObj.getString("sSearch") != "" )
			{
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					if(aColumnsSer[i].contains("DATE") || aColumnsSer[i].contains("date")){
						sWhere.append("LOWER(TO_CHAR("+aColumnsSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					}else{
						sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					}
					//sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
						
						String columnName = "";
						
						if(numberList.contains(jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0")))){
							columnName = "TO_NUMBER(" +jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"))+ ")";
						}else{
							columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
						}
						sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = sOrderBy + " PK_CDM_MAIN";
				}
				
			}
			String pkStr = jsonObj.getString("ids");
			String tempStr = jsonObj.getString("tempids");
			String query = "";
			if(tempStr.equals("")){
				pkStr = pkStr.substring(0, pkStr.length() - 1);
				query = "select TotalRows,"+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where PK_CDM_MAIN in ("+pkStr+") "+sWhere+") WHERE MyRow BETWEEN "+start+" AND "+end;
			}else{
				pkStr = pkStr.substring(0, pkStr.length() - 1);
				tempStr = tempStr.substring(0, tempStr.length() - 1);
				query = "select TotalRows,"+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t INNER JOIN CDM_SSLINK s on t.LSERVICE_CODE = s.SS_CHILD_ID  and t.PK_CDM_MAIN in ("+pkStr+") and s.PK_CDM_SSLINK in ("+tempStr+") "+sWhere+") WHERE MyRow BETWEEN "+start+" AND "+end;
			}
			
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			this.processData(jsonObj);
			
			try{
				
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	

	/**
	 * @description Process Data for displaying in Data table
	 * @return void
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject) throws Exception{
		try{
			List<Map> dataList = (List<Map>)jsonObject.get("cdmmain");
			String activerule = jsonObject.getString("ACTDACTRULE");
			
			for (Map map : dataList) {
				Set keySet = map.keySet();
				Map<String, String> data = new HashMap<String, String>();
				
				long days = 0;
				if(map.get("EFF_TO_DATE") != null){
					days = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_TO_DATE").toString());
				}
				for (Object key : keySet) {
					
					if(key.toString().equalsIgnoreCase("TOTALROWS") || key.toString().equalsIgnoreCase("PK_CDM_MAIN")){
						this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						continue;
					}

					
					if (activerule.equals("DATE"))
					{
						if(days >= 0 && map.get("EFF_TO_DATE") != null){
							long diffBetDate = CDMRUtil.getDifferenceBetweenTwoDates(map.get("EFF_FROM_DATE").toString(), map.get("EFF_TO_DATE").toString());
							long diffBBtwnFromAndPrest = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_FROM_DATE").toString());
							if(diffBetDate > 0 && diffBBtwnFromAndPrest>=0){
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "N/A");
								}else{
									data.put(key.toString(),  map.get(key.toString()).toString());
								}
							}else{
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
								}else{
									data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
								}
							}
						}else{
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "N/A");
							}else{
								data.put(key.toString(),  map.get(key.toString()).toString());
							}
						}
					}
					else
						if (activerule.equals("FLAG"))
						{
							if(map.get("IS_ACTIVE_YN").toString().equalsIgnoreCase("N"))
							{		
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
								}else{
									data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
								}
							}
								
							else
							{
								
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "N/A");
								}else{
									data.put(key.toString(),  map.get(key.toString()).toString());
								}
							}
						
						}
					
				}
				aaData.add(data);
			}
		
			if(aaData.isEmpty()){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
	/**
	 * @description Get Latest map version 
	 * @return JSONObject
	 * @throws Exception
	 */
	
	
	/*public JSONObject getMapVersion(JSONObject jsonObj) throws Exception{
		try {
						
			String vermap = CDMRUtil.fetchPropertyValStr(jsonObj.getString("FILE_ID")+"_MAPS");
			
			String query="select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID IN("+vermap+")";
							
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMapVersion(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");		
			jsonObj.put("MAPSVERSION", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}*/
	
	/**
	 * @description Get Columns from Staging Maps
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getColumnFromStagingMap(JSONObject jsonObj) throws Exception{
		try {
			
			List<Map> versionList = (List<Map>)jsonObj.get("MAPSVERSION");
			String versionInfo = "";
			for (Map map : versionList) {
				versionInfo = versionInfo + map.get("MAPSVERSION").toString() + ",";
			}
			versionInfo = versionInfo.substring(0, (versionInfo.length()-1));
			String query = "select distinct(EQMAINCDM_COL),MAINCDM_COLTYPE from CDM_STAGINGMAPS where FK_CDM_MAPSVERSION in ("+versionInfo+") and DISP_INCDM = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getStagingMaps(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");
			jsonObj.put("columnsNames", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	
}