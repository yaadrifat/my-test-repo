/**
 * 
 */
package com.velos.stafa.objects.eSecurity;

import java.io.Serializable;

/**
 * @author vlakshmipriya
 *
 */
public class EsecurityUserDetailsObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private String jobType;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String addr1;
    private String addr2;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    private String userIsActive;
    private String acctIsActive;
    private String createdon;
    private String createdby;
    private String loginName;
    private String password;
    private String esign;
    private String application;
    private String acctexpDate;
	private String ipaddress;
	private String rightsXml;
	private String passwordExpDate; 
	private String clientUserId; 
	private String timeZone;
	
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getUserIsActive() {
		return userIsActive;
	}
	public void setUserIsActive(String userIsActive) {
		this.userIsActive = userIsActive;
	}
	public String getAcctIsActive() {
		return acctIsActive;
	}
	public void setAcctIsActive(String acctIsActive) {
		this.acctIsActive = acctIsActive;
	}
	public String getCreatedon() {
		return createdon;
	}
	public void setCreatedon(String createdon) {
		this.createdon = createdon;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEsign() {
		return esign;
	}
	public void setEsign(String esign) {
		this.esign = esign;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getAcctexpDate() {
		return acctexpDate;
	}
	public void setAcctexpDate(String acctexpDate) {
		this.acctexpDate = acctexpDate;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public String getRightsXml() {
		return rightsXml;
	}
	public void setRightsXml(String rightsXml) {
		this.rightsXml = rightsXml;
	}
    public String getPasswordExpDate() {
		return passwordExpDate;
	}
	public void setPasswordExpDate(String passwordExpDate) {
		this.passwordExpDate = passwordExpDate;
	}
	public String getClientUserId() {
		return clientUserId;
	}
	public void setClientUserId(String clientUserId) {
		this.clientUserId = clientUserId;
	}
	
}
