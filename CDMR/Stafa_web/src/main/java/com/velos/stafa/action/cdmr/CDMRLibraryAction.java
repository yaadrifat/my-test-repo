package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.business.domain.cdmr.CodeList;

/**
 * @summary     CDMRLibraryAction
 * @description Get uploaded file info from DB and show in datatable
 * @version     1.0
 * @file        CDMRLibraryAction.java
 * @author      Lalit Chattar
 */
public class CDMRLibraryAction extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	private String[] aColumns = {"file_name", "file_cur_status", "comments","last_modified_date", "created_on", "u.user_firstname", "u.user_lastname"};
	private StringBuilder sWhere;
	
	private String sOrderBy = "";
	private String[] sortColumn = {"COMMENTS", "FILE_NAME", "LAST_MODIFIED_DATE", "USER_FIRSTNAME"};
	private String[] sortColumnSer = {"COMMENTS", "FILE_NAME", "LAST_MODIFIED_DATE", "USER_FIRSTNAME || USER_LASTNAME"};
	
	/*
	 * getter and setter
	 */
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	/*
	 * Constructor
	 */
	public CDMRLibraryAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		
		return SUCCESS;
	}
	
	
	
	
	
	/**
	 * @description Featch file info from database and show in datatable
	 * @return String
	 * @throws Exception
	 */
	public String getFilesLibInfo() throws Exception{
		JSONObject jsonObj = new JSONObject();
		httpSession = request.getSession();
		String esec_DB = CDMRUtil.fetchPropertyValStr("ESEC_DB");
		List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
		try{
			jsonObj = VelosUtil.constructJSON(request);
			
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
						//sOrderBy = sOrderBy + sortColumn[jsonObj.getInt("iSortCol_"+i)] + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						sOrderBy = sOrderBy + jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0")) + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = sOrderBy + " pk_cdm_fileslib";
				}
				
			}
		
			int constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			sWhere = new StringBuilder();
			if ( jsonObj.getString("sSearch") != "" )
			{
				sWhere.append(" where (");
				for ( int i=0 ; i<sortColumnSer.length ; i++ )
				{
					if(sortColumnSer[i].contains("DATE") || sortColumnSer[i].contains("date")){
						sWhere.append("LOWER(TO_CHAR("+sortColumnSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					}else{
						sWhere.append("LOWER("+sortColumnSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					}
					
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(") ");
				
			}
			
			String query = "select TOTAL, pk_cdm_fileslib, import_status, file_name, file_cur_status,comments , case when last_modified_date IS NULL THEN created_on  ELSE last_modified_date END as revicedon, user_firstname, user_lastname  from (" +
					"SELECT t.*,u.user_firstname, u.user_lastname, Row_Number() OVER ("+sOrderBy+") MyRow, count(pk_cdm_fileslib) over() as TOTAL FROM cdm_fileslib t LEFT JOIN "+esec_DB+".sec_user u ON u.pk_userid = t.last_modified_by "+sWhere+" )" +
					" where IN_PROGRESS = 0 and DELETEDFLAG = 0 and MyRow BETWEEN "+start+" AND " + end;
			jsonObj.put("query", query);			
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMRFilesInfo(jsonObj);
			
						
			List<Map> fileList = (List<Map>)jsonObj.get("filesinfo");
			for (Map object : fileList) {
				Map<String, String> data = new HashMap<String, String>();
				data.put("COMMENTS",object.get("COMMENTS").toString());
				if(object.get("IMPORT_STATUS").toString().equals("1")){
					data.put("FILE_NAME", object.get("FILE_NAME").toString());
				}
				else{
					data.put("FILE_NAME", "<a href=\"#\" onclick=processFile("+object.get("PK_CDM_FILESLIB").toString()+");>"+object.get("FILE_NAME").toString()+"</a>");
				}
				
				data.put("LAST_MODIFIED_DATE", CDMRUtil.formateDate("yyyy-MM-dd HH:mm:ss", "dd-MMM-yyyy", object.get("REVICEDON").toString(), null));
				
				String lname = object.get("USER_LASTNAME")==null?"":object.get("USER_LASTNAME").toString();
				String fname = object.get("USER_FIRSTNAME")==null?"":object.get("USER_FIRSTNAME").toString();
				data.put("USER_FIRSTNAME",fname +" "+lname);
			
				
				this.aaData.add(data);
				this.iTotalRecords = Integer.parseInt(object.get("TOTAL").toString());
				this.iTotalDisplayRecords =  Integer.parseInt(object.get("TOTAL").toString());
			}
			this.sEcho=jsonObj.getString("sEcho");
			
			if(aaData.isEmpty()){
				this.iTotalRecords = 0;
				
				this.iTotalDisplayRecords =  0;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
		
	
	/**
	 * @description Get Files Current Status
	 * @return String
	 * @throws Exception
	 */
	public String getFileStatus(String status, List<CodeList> codeList) throws Exception{
		for (CodeList codeListObj : codeList) {
			if(codeListObj.getPkCodeList() == Long.parseLong(status)){
				return codeListObj.getCodeListDesc();
			}
		}
		return "";
	}
	
}