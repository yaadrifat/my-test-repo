package com.velos.stafa.util;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * 
 */

/**
 * @author Babu
 * 
 *
 */
public class XMLUtil {
	
	
	
	/*
	
	public static void printNodes(Node printNode){
		if(printNode.getNodeType() != Node.TEXT_NODE){
			//System.out.println(i + "--> "+ node.getNodeName() + " / " + node.getNodeValue() + " / " + node.getTextContent() );
			System.out.println("<" + printNode.getNodeName() + ">");
		}
		//System.out.println( "Node " + printNode.getNodeName());
		if(printNode.hasChildNodes()){
			NodeList child = printNode.getChildNodes();
			for(int i=0;i<child.getLength();i++){
				Node node = child.item(i);
					printNodes(node);
			}
		}else{
			//System.out.println( printNode.getNodeName() + " ->node.getNodeType() " + printNode.getNodeType());
			if(printNode.getNodeType() == Node.TEXT_NODE){
					System.out.println(printNode.getTextContent());
			}
		}
		
		if(printNode.getNodeType() != Node.TEXT_NODE){	
			System.out.println("</" + printNode.getNodeName() + ">");
		}
		
	}
	
	*/

	
	
	public String xmlToString(Node printNode){
		StringBuffer strResult=new StringBuffer("");
		if(printNode.getNodeType() != Node.TEXT_NODE){
			//System.out.println(i + "--> "+ node.getNodeName() + " / " + node.getNodeValue() + " / " + node.getTextContent() );
			//System.out.println("<" + printNode.getNodeName() + ">");
			strResult.append("<" + printNode.getNodeName() + ">");
		}
		//System.out.println( "Node " + printNode.getNodeName());
		if(printNode.hasChildNodes()){
			NodeList child = printNode.getChildNodes();
			for(int i=0;i<child.getLength();i++){
				Node node = child.item(i);
				strResult.append(xmlToString(node));
			}
		}else{
			//System.out.println( printNode.getNodeName() + " ->node.getNodeType() " + printNode.getNodeType());
			if(printNode.getNodeType() == Node.TEXT_NODE){
					//System.out.println(printNode.getTextContent());
					strResult.append(printNode.getTextContent());
			}
		}
		
		if(printNode.getNodeType() != Node.TEXT_NODE){	
			//System.out.println("</" + printNode.getNodeName() + ">");
			strResult.append("</" + printNode.getNodeName() + ">");
		}
		return strResult.toString();
	}
	
	

	public Node getNode(String fileName,String parentNode,String nodeName,String nodeValue){
		Node searchNode = null;
		Node searchTag;
		Element rootNode = getRootNode(fileName);
		//System.out.println(" root Node " + rootNode.toString());
		if (rootNode!= null){
			NodeList childList = rootNode.getElementsByTagName(parentNode);
			 if(childList != null && childList.getLength() >0){
				for (int j=0;j<childList.getLength();j++){
					searchNode = childList.item(j);
					searchTag = findNode(searchNode,nodeName);
					if(searchTag != null ){
						if(nodeValue.trim().equalsIgnoreCase( getNodeValue(searchTag))){
							System.out.println( nodeValue + " node match ");
							//System.out.println(searchNode.getTextContent());
							
							return searchNode;
						}else{
							searchNode = null;
						}
					}else{
						searchNode = null;
					}
				}
				}
			}
		
		return searchNode;
	}
	
	public Element getRootNode(String fileName){
		Element rootElement = null;
		try{
		File file = readXMLFile(fileName);
		 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	     DocumentBuilder db = dbf.newDocumentBuilder();
	     Document doc = db.parse(file);
	     doc.getDocumentElement().normalize();
	     rootElement = doc.getDocumentElement();
		// formNameList = rootElement.getElementsByTagName(parentNode);
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return rootElement;
		
	}
	

	
	public File readXMLFile(String fileName)throws Exception{
	     /*  String applicationDir = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
	       System.out.println(" path *****************" + applicationDir);
	       String path = new File(applicationDir).getParent();
	       System.out.println(" path *****************" + path);
	      */ 
	       File f = new File("a.txt");
	       System.out.println(" new path " + f.getAbsolutePath());
	       String tmpPath =  f.getAbsolutePath();
	       tmpPath = tmpPath.substring(0, tmpPath.indexOf("a.txt"));
	       System.out.println("Path : " + tmpPath   );
	     
	       File file = new File(tmpPath + "/searchxml/"+fileName);
	       System.out.println("file path " + file.getAbsolutePath());
	       
	       return file;
	   }

	/**
	 * Method Used to Read Node of an XML File
	 * @param node
	 * @param name
	 * @return
	 */
	public  Node findNode(Node node,String name){
		if(node.getNodeName().equals(name)){
			return node;
		}

		if(node.hasChildNodes()){
			NodeList nodeList=node.getChildNodes();
			int size=nodeList.getLength();
			for(int i=0;i<size;i++){
				Node found=findNode(nodeList.item(i),name);
				if(found!=null){
					return found;
				}
			}
		}
		return null;
	}

	public  NodeList getChildNodes(Node parentNode,String strChildNode){
		NodeList nodeList= null;

		if(parentNode != null  && parentNode.hasChildNodes()){
			Node searchNode = findNode(parentNode, strChildNode);
			//System.out.println(" get child nodes **************************");
			//printNodes(searchNode);
			if(searchNode!=null){
				nodeList=searchNode.getChildNodes();
			}
		}
		return nodeList;
	}
	
	public  NodeList getChildNodes(Node rootNode){
		NodeList nodeList= null;

		if(rootNode != null  && rootNode.hasChildNodes()){
			nodeList=rootNode.getChildNodes();
			
		}
		return nodeList;
	}

	public String getNodeValue(Node node){
		if (node !=null && node.getFirstChild() !=null && node.getFirstChild().getNodeValue()!=null){
			return node.getFirstChild().getNodeValue().trim();
		}else{
			return null;
		}
	}
	
	public String getNodeValue(Node node,String nodeName){
		
			return getNodeValue(findNode(node, nodeName));
		
	}
	

/*	BB not in use
	public static Node constructXMLElement(Document doc, String rootElement,Map childDetails, HttpServletRequest request){
		//System.out.println("************ rootElement ***" + rootElement);
		
		 Element rootNode = doc.createElement(rootElement); // create HHQ node
		 if(childDetails.containsKey(rootElement)){
			 // loop it
			 String[] childNodes = (String[])childDetails.get(rootElement);
			 for(int i=0;i<childNodes.length;i++){
				 rootNode.appendChild(constructXMLElement(doc,childNodes[i],childDetails,request)); 
			 }
		 }else{
			 String value = "";	
			// System.out.println(" Qid " + qid + " = " +responseMap.containsKey(rootElement));
			 if(request.getParameter(rootElement) != null){
				 value = (String)request.getParameter(rootElement);
			 }
			 System.out.println("value : "+value+"rootelement : "+rootElement);
			 rootNode.setTextContent(value);
			 return rootNode;
		 }
		
		
		//System.out.println( " response ****************************" + responsenode.getTextContent());
		return rootNode;
		
	}
*/	
	/* BB not in use
	
	public void updateXmlData(HttpServletRequest request) throws ServletException, IOException,Exception {
		try{
			 System.out.println("updateXmlData Start ***********************");
			 
			 String filepath = request.getParameter("xmlFileName");
			 File file = readXMLFile(filepath);
			 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		     DocumentBuilder db = dbf.newDocumentBuilder();
		     Document doc = db.parse(file);
		     doc.getDocumentElement().normalize();
		     
		     String xmlnodeName = request.getParameter("nodeName");
		     String xmlnodeValue = request.getParameter("nodeValue");
		     String xmldonorid = request.getParameter("donorid");
		     String xmlobjectname = request.getParameter("objectName");
		     System.out.println("filename : "+filepath+"nodeName : "+xmlnodeName+"nodeValue : "+xmlnodeValue+" OBjectName : "+xmlobjectname);
 		     
		     Element rootEle = doc.getDocumentElement();
			 NodeList formNameList = rootEle.getElementsByTagName(xmlobjectname);
			 System.out.println("length : "+formNameList.getLength());
				for (int i = 0; i < formNameList.getLength(); i++) {

					Node firstForm = formNameList.item(i);

					if (firstForm.getNodeType() == Node.ELEMENT_NODE) {

						Element ListTag = (Element) firstForm;

						NodeList idList = ListTag.getElementsByTagName("donorid");
						Element fstId = (Element) idList.item(0);
						String assignDonorIds=fstId.getTextContent();
						System.out.println("assignDonorIds : "+assignDonorIds+" xmldonorid : "+xmldonorid); 
						if(xmldonorid.equals(assignDonorIds)) {

							System.out.println("assignDonorIds : "+assignDonorIds); 	
							
							NodeList nodeList=ListTag.getChildNodes();
							
							
							for(int j=0;j<nodeList.getLength();j++){

								Node node = nodeList.item(j);
								if(node.getNodeType()==Node.ELEMENT_NODE) {

									String nodeName = node.getNodeName();
									System.out.println("nodeName : "+nodeName);
									if ( nodeName.equalsIgnoreCase(xmlnodeName)) {
										node.setTextContent(xmlnodeValue);
									
									}
									
								}			
								
							}
						}
					}
				}
					
			 

			//write the content into xml file
		     TransformerFactory transformerFactory = TransformerFactory.newInstance();
		     Transformer transformer = transformerFactory.newTransformer();
		     DOMSource source = new DOMSource(doc);
		     StreamResult result =  new StreamResult(readXMLFile(filepath));
		     //System.out.println("Before Transform : " + source + result);
		     transformer.transform(source, result);
		 
		     System.out.println("Done**********************");
		 
		     //readxmlfile();
		     
		     
		   }catch(ParserConfigurationException pce){
			 pce.printStackTrace();
		   }catch(TransformerException tfe){
			 tfe.printStackTrace();
		   }catch(IOException ioe){
			 ioe.printStackTrace();
		   }catch(SAXException sae){
			 sae.printStackTrace();
		   }
		   System.out.println("WriteAcNumber End ");
		   
		 } 
	
	*/
	
	
}





