package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.business.domain.cdmr.CodeList;

/**
 * @summary     GetFilesInfoAction
 * @description Get uploaded file info from DB and show in datatable
 * @version     1.0
 * @file        GetFilesInfoAction.java
 * @author      Lalit Chattar
 */
public class GetFilesInfoAction extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	private String[] aColumns = {"file_name", "file_cur_status", "comments","last_modified_date", "created_on", "u.user_firstname", "u.user_lastname"};
	private StringBuilder sWhere;
	private String sOrderBy = "";
	private String[] sortColumn = {"COMMENTS", "FILE_NAME", "REVICEDON", "USER_FIRSTNAME"};
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * getter and setter
	 */
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	/*
	 * Constructor
	 */
	public GetFilesInfoAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		return SUCCESS;
	}
	
	
	/**
	 * @description Featch file info from database and show in datatable
	 * @return String
	 * @throws Exception
	 */
	public String getFilesInfo() throws Exception{
		JSONObject jsonObj = new JSONObject();
		httpSession = request.getSession();
		String esec_DB = CDMRUtil.fetchPropertyValStr("ESEC_DB");
		List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
		try{
		
			
			jsonObj = VelosUtil.constructJSON(request);
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
					//	sOrderBy = sOrderBy + sortColumn[jsonObj.getInt("iSortCol_"+i)] + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						sOrderBy = sOrderBy + jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0")) + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						
					
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = "";
				}
				
			}
		
			String query = "select * from (select c.pk_cdm_fileslib, c.import_status,c.comments, c.file_name, " +
					"c.file_cur_status,case when c.last_modified_date IS NULL THEN c.created_on ELSE c.last_modified_date END as revicedon ," +
					"u.user_firstname,u.user_lastname from cdm_fileslib c LEFT JOIN "+esec_DB+".sec_user u On u.pk_userid = c.last_modified_by " +
					" where c.IN_PROGRESS = 0 and c.DELETEDFLAG = 0 order by c.last_modified_date desc) where rownum <= " + this.getNumberOfFiles();
			jsonObj.put("query", query + " " + sOrderBy);
			//jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMRFilesInfo(jsonObj);
			List<Map> fileList = (List<Map>)jsonObj.get("filesinfo");
			for (Map object : fileList) {
				Map<String, String> data = new HashMap<String, String>();
				
				data.put("COMMENTS", object.get("COMMENTS").toString());
				if(object.get("IMPORT_STATUS").toString().equals("1")){
					data.put("FILE_NAME", object.get("FILE_NAME").toString());
					//data.put("status", "No Data Found");
				}
				else{
					data.put("FILE_NAME", "<a href=\"#\" onclick=processFile("+object.get("PK_CDM_FILESLIB").toString()+");>"+object.get("FILE_NAME").toString()+"</a>");
					//data.put("status", this.getFileStatus(object.get("FILE_CUR_STATUS").toString(), codelist));
				}
				//data.put("lastrevision", "");
				data.put("REVICEDON", CDMRUtil.formateDate("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", object.get("REVICEDON").toString(), null));
				
				String lname = object.get("USER_LASTNAME")==null?"":object.get("USER_LASTNAME").toString();
				String fname = object.get("USER_FIRSTNAME")==null?"":object.get("USER_FIRSTNAME").toString();
				data.put("USER_FIRSTNAME",fname +" "+lname);
				
				//data.put("revicedby", "System");
				this.aaData.add(data);
			}
			this.sEcho=jsonObj.getString("sEcho");
			this.iTotalRecords =100;
			this.iTotalDisplayRecords =  10;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String getFileStatus(String status, List<CodeList> codeList) throws Exception{
		for (CodeList codeListObj : codeList) {
			if(codeListObj.getPkCodeList() == Long.parseLong(status)){
				return codeListObj.getCodeListDesc();
			}
		}
		return "";
	}
	
	public String getNumberOfFiles() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "select PARAM_VAL from VBAR_CONTROLPARAM where PARAM_TYPE = 'FILES' and PARAM_SUBTYP = 'HOME_FILES'";
			jsonObj.put("SQLQuery", true);
			jsonObj.put("query", query);
			jsonObj = cdmrController.getNumberOfFiles(jsonObj);
			List<Map> nooffiles = (List<Map>)jsonObj.get("nooffiles");
			return nooffiles.get(0).get("PARAM_VAL").toString();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "5";
	}
	
}