package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRSqlConstants;



/**
 * @summary     AttachServiceSets
 * @description Attach Service Set to the line items
 * @version     1.0
 * @file        AttachServiceSets.java
 * @author      Lalit Chattar
 */
public class LoadErrorLog extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	
	private String[] aColumnsSer = { "ERR_REP_ON", "ERR_NAME", "ERR_DESC"};
	private String sOrderBy = null;
	private StringBuilder sWhere;
	
	public LoadErrorLog(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return loadErrorLog();
	}
	
	public String loadErrorLog() throws Exception{
		try {
			List<String> userList = Arrays.asList(aColumnsSer);
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			
			int constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			sWhere = new StringBuilder();
			if ( jsonObj.getString("sSearch") != "" ){
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					
					sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					
					
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
						//sOrderBy = sOrderBy + aColumnsSer[jsonObj.getInt("iSortCol_"+i)] + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						String columnName = "";
						columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
						sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = "";
				}
				
			}
			
			String query = "select * from(select ERR_NAME, ERR_DESC, TO_CHAR(ERR_REP_ON, 'DD-MON-YYYY HH24:MI:SS') as ERR_REP_ON,  COUNT(*) OVER () AS TotalRows,Row_Number() OVER (ORDER BY PK_CDM_ERRORLOG desc) MyRow from CDM_ERRORLOG where DELETEDFLAG = 0"+ sWhere + sOrderBy+") where MyRow between "+start+" and " + end;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.loadAllNotification(jsonObj);
			this.processData(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	/**
	 * @description Process data for displaying in datatable
	 * @return void
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject) throws Exception{
		try{
			List<Map> dataList = (List<Map>)jsonObject.get("notification");
			for (Map map : dataList) {
				Set keySet = map.keySet();
				Map<String, String> data = new HashMap<String, String>();
				for (Object key : keySet) {
					
					//data.put("delete", "<img src=\"images/cdmr/delete.png\" name=\"deleteNotification\" id=\"deleteNotification\" onclick=\"deleteFileNotification("+map.get("PK_CDM_NOTIFICATION").toString()+");\"/>");
					if(key.toString().equalsIgnoreCase("TOTALROWS")){
						this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						continue;
					}
					if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
						data.put(key.toString(), "N/A");
					}else{
						
						data.put(key.toString(), map.get(key.toString()).toString());
						
						
					}
					
				}
				aaData.add(data);
			}
			
			if(aaData.isEmpty()){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
	
}
