package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.velos.stafa.util.CDMRUtil;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.business.domain.cdmr.StagingMain;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.business.domain.cdmr.GrpCode;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.util.CDMRConstant;
import atg.taglib.json.util.JSONObject;

/**
 * @summary     GetStagingCompRecData
 * @description Get Technical Data for Staging 
 * @version     1.0
 * @file        GetStagingCompRecData.java
 * @author      Lalit Chattar
 */
public class GetStagingCompRecData extends StafaBaseAction{
	
	private Integer fileID;
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
//	private String[] aColumns = {"INST_CODE", "COMP_COL1", "COMP_COL2","COMP_COL3","COMP_COL4","COMP_COL5",
//			"COMP_COL6","COMP_COL7","COMP_COL8","COMP_COL9","COMP_COL10",
//			"COMP_COL11","COMP_COL12","COMP_COL13","COMP_COL14","COMP_COL15",
//			"COMP_COL16","COMP_COL17","COMP_COL18","COMP_COL19","COMP_COL20",
//			"COMP_COL21","COMP_COL22","COMP_COL23","COMP_COL24","COMP_COL25",
//			"COMP_COL26","COMP_COL27","COMP_COL28","COMP_COL29","COMP_COL29",
//			"COMP_COL31","COMP_COL32","COMP_COL33","COMP_COL34","COMP_COL35",
//			"COMP_COL36","COMP_COL37","COMP_COL38","COMP_COL39","COMP_COL40"};
//	private String[] sortColumn = {"INST_CODE", "COMP_COL1", "TO_DATE(COMP_COL2, 'DD-MM-YYYY')","COMP_COL3","TO_DATE(COMP_COL4, 'DD-MM-YYYY')","COMP_COL5",
//			"TO_DATE(COMP_COL6, 'DD-MM-YYYY')","COMP_COL7","TO_DATE(COMP_COL8, 'DD-MM-YYYY')","TO_NUMBER(COMP_COL9)","COMP_COL10","COMP_COL12","COMP_COL13","COMP_COL14","TO_NUMBER(COMP_COL15)","TO_DATE(COMP_COL16, 'DD-MM-YYYY')","TO_NUMBER(COMP_COL17)","TO_DATE(COMP_COL18, 'DD-MM-YYYY')"};
//	
//	String technicalMap = "instcode:INST_CODE BCBSHCPCSCode:COMP_COL1 BCBSEffDate:TO_DATE(COMP_COL2,'DD-MM-YYYY') MCAIDHCPCSCPTCode:COMP_COL3 MCAIDEffDate:TO_DATE(COMP_COL4,'DD-MM-YYYY') CMSHCPCSCPTCode:COMP_COL5 CMSEffDate:TO_DATE(COMP_COL6,'DD-MM-YYYY') PRSCode:COMP_COL7 PRSEffDate:TO_DATE(COMP_COL8,'DD-MM-YYYY') RevClass:TO_NUMBER(COMP_COL9) RevClassDesc:COMP_COL10 SVCCode:COMP_COL12 SVCCodeDesc:COMP_COL13 SVCCodeGNLDesc:COMP_COL14 GLKey:TO_NUMBER(COMP_COL15) SVCCodeINACTDate:TO_DATE(COMP_COL16,'DD-MM-YYYY') SVCPrice:TO_NUMBER(COMP_COL17) SVCPriceEffDate:TO_DATE(COMP_COL18,'DD-MM-YYYY')";
	private StringBuilder sWhere;
	String sOrderBy = "";
	
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	public Integer getFileID() {
		return fileID;
	}
	public void setFileID(Integer fileID) {
		this.fileID = fileID;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	
	public GetStagingCompRecData(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	public String execute() throws Exception{
		processData();
		return SUCCESS;
	}
	
	/**
	 * @description Helper Method for calling other methods
	 * @return String
	 * @throws Exception
	 */
	public String processData() throws Exception{
		try{
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject = this.getFileID(jsonObject);

			jsonObject = this.getStagingPrimaryID(jsonObject);
			jsonObject = this.getStagingColumnMapDetail(jsonObject);

			jsonObject = this.createDynamicQueryForStagingData(jsonObject);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCompRecData(jsonObject);
			jsonObject = this.countTotalRecord(jsonObject);
			this.processStagingData(jsonObject);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	
	/**
	 * @description Count total no of records
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject countTotalRecord(JSONObject jsonObject) throws Exception{
		if(sWhere == null || sWhere.toString().equals(""))
			jsonObject.put("query", CDMRSqlConstants.COUNT_RECORD + "CDM_STAGINGCOMPREC where FK_CDM_STAGINGMAIN = "+jsonObject.getString("stagingMainID"));
		else{
			String queryModify = sWhere.toString().replace("and (", "");
			jsonObject.put("query", CDMRSqlConstants.COUNT_SEARCH_RECORD + jsonObject.getString("stagingMainID") + " and ("+queryModify+")");
		}
			
		jsonObject.put("SQLQuery", true);
		jsonObject = cdmrController.countRecord(jsonObject);
		List<Map> data = (List<Map>)jsonObject.get("recordcount");
		Map map = data.get(0);
		return jsonObject.put("recordcount", map.get("TOTAL"));
		
	}
	
	
	/**
	 * @description Get Staging primary id
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getStagingPrimaryID(JSONObject jsonObject) throws Exception{
		
		jsonObject.put("query", CDMRSqlConstants.GET_STAGING_PK + jsonObject.getString("fileID").toString());
		jsonObject.put("SQLQuery", false);
		jsonObject = cdmrController.getStagingPrimaryID(jsonObject);
		List<StagingMain> listObj = (List<StagingMain>)jsonObject.get("stagingpk");
		StagingMain mainObj = listObj.get(0);
		jsonObject.put("MAPSVERSION", mainObj.getFkMapVersion());
		jsonObject.put("FILE_NAME", mainObj.getFileName());
		
		return  jsonObject.put("stagingMainID", mainObj.getPkStagingMain());
		
	}
	
	/**
	 * @description Get Mapping info for staging
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getStagingColumnMapDetail(JSONObject jsonObject) throws Exception{
		String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP + " where dispInStaging  = 1 and fkMapVersion = " + jsonObject.getString("MAPSVERSION");
		jsonObject.put("query", query);
		jsonObject.put("SQLQuery", false);
		jsonObject = cdmrController.getStagingColumnMapDetail(jsonObject);
		return jsonObject;
	}
	
	/**
	 * @description Create Dynamic Query
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createDynamicQueryForStagingData(JSONObject jsonObject) throws Exception{
		
		Map<String, String> mapForSort = this.getMappingProperty(jsonObject.getString("file_id"));
		
		List<StagingMaps> mapsObjList = (List<StagingMaps>) jsonObject
				.get("stagingcolumnmap");
		StringBuilder query = new StringBuilder();
		Map MAINCOM_COLTYPE = new HashMap();
		query.append("select PK_CDM_STAGINGCOMPREC as PK, ");

		sWhere = new StringBuilder();
		sWhere.append("");
		for (StagingMaps stagingMaps : mapsObjList) {
			query.append(stagingMaps.getEqCompReqCol()).append(
					" as " + stagingMaps.getEqMainCdmCol() + ",");
			MAINCOM_COLTYPE.put(stagingMaps.getPullFromCols(), stagingMaps
					.getEqMainCdmCol());

			if (jsonObject.getString("sSearch") != "") {
				sWhere.append("LOWER(" + stagingMaps.getEqCompReqCol()
						+ ") LIKE '%"
						+ jsonObject.getString("sSearch").toLowerCase()
						+ "%' OR ");
			}

		}
		if (jsonObject.getString("sSearch") != "") {
			sWhere = sWhere.insert(0, " and (");
			sWhere = sWhere.replace((sWhere.length() - 3), sWhere.length(), "");
			sWhere.append(")");
		}

		query.append("CHG_LOG as CHG_LOG,");
		query = query.deleteCharAt(query.length() - 1);

		if (jsonObject.has("iSortCol_0")) {
			sOrderBy = " ORDER BY  ";
			for (int i = 0; i < jsonObject.getInt("iSortingCols"); i++) {
				if ((jsonObject.getString("bSortable_"
						+ jsonObject.getString("iSortCol_" + i)))
						.equals("true")) {

					sOrderBy = sOrderBy
							+ mapForSort.get(
									jsonObject.getString("mDataProp_"
											+ jsonObject.getInt("iSortCol_0")))
									.toString()
							+ " "
							+ (jsonObject.getString("sSortDir_" + i).equals(
									"asc") ? "asc" : "desc") + ", ";
				}

			}

			sOrderBy = sOrderBy.substring(0, (sOrderBy.length() - 2));
			if (sOrderBy.equals(" ORDER BY")) {
				sOrderBy = sOrderBy + " PK_CDM_STAGINGCOMPREC";
			}
			
		}

		int constant = Integer.parseInt(jsonObject.getString("iDisplayLength"));
		int end = Integer.parseInt(jsonObject.getString("iDisplayStart"))
				+ Integer.parseInt(jsonObject.getString("iDisplayLength"));
		int start = Integer.parseInt(jsonObject.getString("iDisplayStart")) + 1;
		query
				.append(" from ("
						+ "SELECT t.*, Row_Number() OVER ("
						+ sOrderBy
						+ ") MyRow FROM CDM_STAGINGCOMPREC t where FK_CDM_STAGINGMAIN = "
						+ jsonObject.getString("stagingMainID") + sWhere + ")"
						+ " WHERE MyRow BETWEEN " + start + " AND " + end);
		jsonObject.put("query", query);
		
		jsonObject.put("MAINCOM_COLTYPE", MAINCOM_COLTYPE);
		return jsonObject;
	}
		
	/**
	 * @description Process Data for displying in grid
	 * @return void
	 * @throws Exception
	 */
	public void processStagingData(JSONObject jsonObject) throws Exception{
		List<Map> dataList = (List<Map>) jsonObject.get("comprecdata");
		List<String> pkList = new ArrayList<String>();
		for (Map map : dataList) {
			pkList.add(map.get("PK").toString());
		}
		NotesAction notes = new NotesAction();
		Map isNotes = notes.fetchLatestNote(pkList);
		Map MAINCOM_COLTYPE = (Map) jsonObject.get("MAINCOM_COLTYPE");

		try {
			for (Map map : dataList) {

				List<String> index = new ArrayList<String>();
				String chgLog = map.get("CHG_LOG") == null ? "" : map.get(
						"CHG_LOG").toString().trim().length() == 0 ? "" : map
						.get("CHG_LOG").toString();
				if ((chgLog.trim().length()) > 0) {
					index = Arrays.asList(chgLog.split(",")); // chgLog.split(",");
				}
				Map<String, String> data = new HashMap<String, String>();
				// data.put("check",
				// "<input type=\"checkbox\" name=\"check\" onclick=\"addInArray(this.value);\" value=\""+map.get("PK").toString()+"\">");

				Iterator it = MAINCOM_COLTYPE.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry entry = (Map.Entry) it.next();
					String key = entry.getKey().toString();
					String value = entry.getValue().toString();
					String ind="";
					StringBuilder val = new StringBuilder();
					if ((map.get(value)) == null || (map.get(value)) == "") {
						data.put(value, "");

					} else {
						if(key.length()>8)
						ind = key.substring(8);
						for (int i = 0; i < index.size(); i++) {
							if (ind.equals(index.get(i))) {
								val.append("<span style=\"background-color:#33FF99;width:100% \">");
							}
						}val.append(map.get(value));
						data.put(value, val.toString());
					}
				}

				if (isNotes.containsKey(map.get("PK").toString())) {

					data.put("notestd",
							"<a href=\"#\"><img src=\"images/cdmr/notes.png\" onclick=\"notesForRow("
									+ map.get("PK").toString() + ");\"/></a>");
				} else {

					data.put("notestd",
							"<a href=\"#\"><img src=\"images/cdmr/add.png\" onclick=\"notesForRow("
									+ map.get("PK").toString() + ");\"/></a>");
				}
				
				aaData.add(data);

			}
			if (aaData.isEmpty()) {
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		this.sEcho = jsonObject.getString("sEcho");
		this.iTotalRecords = Integer.parseInt(jsonObject
				.getString("recordcount"));
		this.iTotalDisplayRecords = Integer.parseInt(jsonObject
				.getString("recordcount"));

	}
	
	
	/**
	 * @description Create Dynamic Query
	 * @return JSONObject
	 * @throws Exception
	 */
	
	
	/**
	 * @description Get Group code value
	 * @return String
	 * @throws Exception
	 */
	public String getGropuCodeValue(String grpCode) throws Exception{
		
		try{
			if(grpCode.equals(null) || grpCode == null || grpCode == "" || grpCode.equals("") || grpCode.equals(" "))
				return "";
			JSONObject  jsonObject = new JSONObject();
			String query = CDMRSqlConstants.GET_GROUPNAME_BY_CODE + grpCode;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", false);
			jsonObject = cdmrController.getGroupCodeValue(jsonObject);
			List<GrpCode> listObj = (List<GrpCode>)jsonObject.get("groupcodename");
			GrpCode mainObj = listObj.get(0);
			return mainObj.getGrpDispName();
		}catch (Exception e) {
			return "";
		}
		
	}
	
	public Map<String, String> convertToMap(String strMap) throws Exception{
		Map<String, String> map = null;
		try {
			String[] tokens = strMap.split(" |:");
		     map = new HashMap<String, String>();
		    for (int i=0; i<tokens.length-1; ) map.put(tokens[i++], tokens[i++]);
		    return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	public Map<String, String> getMappingProperty(String fileID) throws Exception{
		Map<String, String> map = null;
			
		Properties prop = new Properties();
		InputStream input = null;

		try {
			
			prop.load(this.getClass().getClassLoader().getResourceAsStream("/cdmr.properties"));
			map = convertToMap(prop.getProperty(fileID));
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return map;
	  }
		
	public JSONObject getFileID(JSONObject jsonObject) throws Exception {
		try {
			CDMRController cdmrController = new CDMRController();
			String query = "select FILE_ID from CDM_FILESLIB where PK_CDM_FILESLIB ='"
					+ jsonObject.getString("fileID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFileID(jsonObject);

			List fileName = (List) jsonObject.get("file_id");
			jsonObject.put("file_id", ((Map<String, String>) fileName.get(0))
					.get("FILE_ID"));

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"GetAndProcessRemoteData::getFileID", "1");
			e.printStackTrace();
		}
		return jsonObject;

	}

	
	
}