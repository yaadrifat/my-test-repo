package com.velos.stafa.action.cdmr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRUtil;



/**
 * @summary     GetGroupDataLinkAction
 * @description Loads Group(Service Section) data in charge library in group(Service Section) view
 * @version     1.0
 * @file        GetGroupDataLinkAction.java
 * @author      Lalit Chattar
 */
public class GetGroupDataLinkAction extends StafaBaseAction{
	
	
	
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	private int constant, end, start;
	private StringBuilder sWhere;
	private String sOrderBy = null;
	
	private String[] aColumnsSer;// = {"SERVICE_DESC1", "SERVICE_DESC2", "CMS_HCPCS_CPT", "CMS_HCPCS_CPT_EFF_DATE", "SERVICE_CODE", "PRS_CODE", "MCAID_HCPCS_CPT", "MCAID_HCPCS_CPT_EFF_DATE", "CPT_CODE", "BLUE_CROSS_HCPCS_CPT", "BLUE_CROSS_HCPCS_CPT_EFF_DATE", "REV_CLASS", "CUSTOM_DESC1", "REV_CODE", "HCC_TECH_CHG", "PRS_PHY_CHG", "EFF_FROM_DATE", "EFF_TO_DATE"};
	//private String[] aColumns;// = { "SERVICE_DESC1", "SERVICE_DESC2", "CMS_HCPCS_CPT", "TO_DATE(CMS_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "SERVICE_CODE", "PRS_CODE", "MCAID_HCPCS_CPT", "TO_DATE(MCAID_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "CPT_CODE", "BLUE_CROSS_HCPCS_CPT", "TO_DATE(BLUE_CROSS_HCPCS_CPT_EFF_DATE, 'DD-MM-YYYY')", "TO_NUMBER(REV_CLASS)", "CUSTOM_DESC1", "TO_NUMBER(REV_CODE)", "TO_NUMBER(HCC_TECH_CHG)", "TO_NUMBER(PRS_PHY_CHG)", "TO_DATE(EFF_FROM_DATE, 'DD-MM-YYYY')", "TO_DATE(EFF_TO_DATE, 'DD-MM-YYYY')"};
	private String[] dateArray;// = {"CMS_HCPCS_CPT_EFF_DATE", "MCAID_HCPCS_CPT_EFF_DATE", "BLUE_CROSS_HCPCS_CPT_EFF_DATE", "EFF_FROM_DATE", "EFF_TO_DATE"};
	private String[] numberArray;// = {"HCC_TECH_CHG", "PRS_PHY_CHG"};
	
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}
	
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	public GetGroupDataLinkAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return getGroupLinkData();
	}
	
	/**
	 * @description Get Data frtom cdm main related to Group(Service Section)
	 * @return String
	 * @throws Exception
	 */
	public String getGroupLinkData()throws Exception {
		
		try {
						
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			
			
			constant = Integer.parseInt(jsonObject.getString("iDisplayLength"));
			end = Integer.parseInt(jsonObject.getString("iDisplayStart")) + Integer.parseInt(jsonObject.getString("iDisplayLength"));
			start = Integer.parseInt(jsonObject.getString("iDisplayStart"))+1;
			
			aColumnsSer = CDMRUtil.fetchPropertyVal(jsonObject.getString("FILE_ID")+"_COLS");
			dateArray = CDMRUtil.fetchPropertyVal(jsonObject.getString("FILE_ID")+"_DATEARY");
			numberArray = CDMRUtil.fetchPropertyVal(jsonObject.getString("FILE_ID")+"_NUMARY");
			
			List<String> dateList = Arrays.asList(dateArray);
			List<String> numberList = Arrays.asList(numberArray);
			
			jsonObject = CDMRUtil.fetchEventLibId(jsonObject);
			
			jsonObject = CDMRUtil.getActiveInactiveRule(jsonObject);
			
			
			sWhere = new StringBuilder();
			if ( jsonObject.getString("sSearch") != "" )
			{
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					//sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObject.getString("sSearch").toLowerCase()+"%' OR ");
					if(aColumnsSer[i].contains("DATE") || aColumnsSer[i].contains("date")){
						sWhere.append("LOWER(TO_CHAR("+aColumnsSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObject.getString("sSearch").toLowerCase()+"%' OR ");
					}else{
						sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObject.getString("sSearch").toLowerCase()+"%' OR ");
					}
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObject.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObject.getInt("iSortingCols") ; i++ )
				{
					if((jsonObject.getString("bSortable_" + jsonObject.getString("iSortCol_"+i))).equals("true")){
						//sOrderBy = sOrderBy + aColumns[jsonObject.getInt("iSortCol_"+i)] + " " + (jsonObject.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						String columnName = "";
//						if(dateList.contains(jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0")))){
//							columnName = "TO_DATE(" + jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0")) + ", 'DD-MM-YYYY')";
//						}else if(numberList.contains(jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0")))){
//							columnName = "TO_NUMBER(" +jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0"))+ ")";
//						}else{
//							columnName = jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0"));
//						}
						if(numberList.contains(jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0")))){
							columnName = "TO_NUMBER(" +jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0"))+ ")";
						}else{
							columnName = jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0"));
						}
						sOrderBy = sOrderBy + columnName + " " + (jsonObject.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = sOrderBy + " PK_CDM_MAIN";
				}
				
			}
			jsonObject = CDMRUtil.getCurrentVersion(jsonObject);
			jsonObject = this.getCDMDataAssociateWithGroups(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**
	 * @description Get Current Version
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
		try {
			String query = "select version_id from cdm_versiondef where is_current = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("versioninfo");
			Map dataMap = list.get(0);
			jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}*/
	
	/**
	 * @description Get CDM Data Associate with perticular group(Service Section)
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getCDMDataAssociateWithGroups(JSONObject jsonObj) throws Exception{
		try {
			CDMRUtil.getMapVersion(jsonObj);
			this.getColumnFromStagingMap(jsonObj);
			String columnNames = "";
			List<Map> columnsNameMap = (List<Map>)jsonObj.get("columnsNames");
			for (Map map : columnsNameMap) {
				String type = map.get("MAINCDM_COLTYPE") == null? "":map.get("MAINCDM_COLTYPE").toString();
				if(type.equalsIgnoreCase("TIMESTAMP")){
					columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", 'dd-MON-YYYY') as " +map.get("EQMAINCDM_COL").toString()  + ",";
				}else{
					columnNames = columnNames +map.get("EQMAINCDM_COL").toString()+ ",";
				}
				
			}
			columnNames = columnNames.substring(0, (columnNames.length()-1));

			String query = "select TotalRows,"+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where EFF_FROM_DATE IS NOT NULL and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"' and FK_CDM_GRPCODE = "+jsonObj.getString("groupcode").replace("grp", "")+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"' group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)) WHERE MyRow BETWEEN "+start+" AND "+end;
			//String query = "select * from CDM_MAIN where LSERVICE_CODE in (" +ids+") and CUR_VERSION = " + jsonObj.getString("versionid");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			this.processData(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	/**
	 * @description Process Data for displaying in Group (Service Section) view in charge Library
	 * @return void
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject) throws Exception{
		try{
			List<Map> dataList = (List<Map>)jsonObject.get("cdmmain");
			String activerule = jsonObject.getString("ACTDACTRULE");
			for (Map map : dataList) {
				Set keySet = map.keySet();
				Map<String, String> data = new HashMap<String, String>();
				long days = 0;
				if(map.get("EFF_TO_DATE") != null){
					days = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_TO_DATE").toString());
				}
				for (Object key : keySet) {
					
					if(key.toString().equalsIgnoreCase("TOTALROWS") || key.toString().equalsIgnoreCase("PK_CDM_MAIN")){
						this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						continue;
					}

					if (activerule.equals("DATE"))
					{
						if(days >= 0 && map.get("EFF_TO_DATE") != null){
							long diffBetDate = CDMRUtil.getDifferenceBetweenTwoDates(map.get("EFF_FROM_DATE").toString(), map.get("EFF_TO_DATE").toString());
							long diffBBtwnFromAndPrest = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_FROM_DATE").toString());
							if(diffBetDate > 0 && diffBBtwnFromAndPrest>=0){
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "N/A");
								}else{
									data.put(key.toString(),  map.get(key.toString()).toString());
								}
							}else{
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
								}else{
									data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
								}
							}
						}else{
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "N/A");
							}else{
								data.put(key.toString(),  map.get(key.toString()).toString());
							}
						}
					}
					else
					if (activerule.equals("FLAG"))
					{
						if(map.get("IS_ACTIVE_YN").toString().equalsIgnoreCase("N"))
						{		
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
							}else{
								data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
							}
						}
							
						else
						{
							
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "N/A");
							}else{
								data.put(key.toString(),  map.get(key.toString()).toString());
							}
						}
					
					}
				}
				aaData.add(data);
			}
			
			if(aaData.isEmpty()){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
	/**
	 * @description Get Latest map version 
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getMapVersion(JSONObject jsonObj) throws Exception{
		try {
			String query="select MAPSVERSION, FILE_ID from CDM_MAPSVERSION where SHEET_NOS=0 and IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID IN('EIWTD','EIWPD')";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMapVersion(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");		
			jsonObj.put("MAPSVERSION", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	*/
	/**
	 * @description Get Columns from Staging Maps
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getColumnFromStagingMap(JSONObject jsonObj) throws Exception{
		try {
			
			List<Map> versionList = (List<Map>)jsonObj.get("MAPSVERSION");
			String versionInfo = "";
			for (Map map : versionList) {
				versionInfo = versionInfo + map.get("MAPSVERSION").toString() + ",";
			}
			versionInfo = versionInfo.substring(0, (versionInfo.length()-1));
			String query = "select distinct(EQMAINCDM_COL),MAINCDM_COLTYPE from CDM_STAGINGMAPS where FK_CDM_MAPSVERSION in ("+versionInfo+") and DISP_INCDM = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getStagingMaps(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");
			jsonObj.put("columnsNames", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	
	
}
