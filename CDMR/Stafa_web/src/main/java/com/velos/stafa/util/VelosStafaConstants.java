/**
 * 
 */
package com.velos.stafa.util;

/**
 * @author akajeera
 *
 */
public interface VelosStafaConstants {
	public static final String WORKFLOW_CTLPRODUCT="ctl_product";
	
	public static final String DOMAIN_ADTSCREENING = "com.velos.stafa.business.domain.apheresis.ADTScreeningResults";
	public static final String ADTSCREENING_DOMAINKEY = "adtScreening";
	
	public static final String DOMAIN_WIDGETCONFIG ="com.velos.stafa.widget.domain.WidgetConfig";
	final String WIDGETCONFIG_DOMAINKEY="widgetConfig";
	public static final String DOMAIN_WIDGETLIB ="com.velos.stafa.widget.domain.WidgetLib";
	public static final String WIDGETLIB_DOMAINKEY="widgetLib";
	
	public static final String DOMAIN_TASK = "com.velos.stafa.business.domain.Task";
	//public static final String DOMAIN_VALIDATION="com.velos.stafa.business.domain.validation.VelosValidation";
	public static final String VALIDATION_DOMAINKEY="validation";
	
	public static final String DOMAIN_PROTOCOLREAGENTSSUPPLIES="com.velos.stafa.business.domain.ProtocolReagentsSupplies";
	public static final String PROTOCOLREAGENTSSUPPLIES_DOMAINKEY="protocolReagents";
	
	public static final String WORKFLOW_DONOR="apheresis_nurse";
	
	public static final String STAFA_PROPERTYFILE="Stafa";
	
	public static final String ENCRYPTKEY = "ENCRYPTKEY";
	/* Inventory */
	public static final String DOMAIN_REAGENTSSUPPLIES="com.velos.stafa.business.domain.ReagentsSupplies";
	public static final String REAGENTSSUPPLIES_DOMAINKEY="reagentsupplies";
	
	public static final String DOMAIN_PLANNEDPROCEDURE="com.velos.stafa.business.domain.apheresis.PlannedProcedure";

	public static final String PLANNEDPROCEDURE_DOMAINKEY="plannedProcedure";
	
	public static final String DOMAIN_MOBILIZATION="com.velos.stafa.business.domain.apheresis.Mobilization";
	public static final String MOBILIZATION_DOMAINKEY="mobilization";

	public static final String DOMAIN_MOBILIZATIONDETAILS="com.velos.stafa.business.domain.apheresis.MobilizationDetails";
	public static final String MOBILIZATIONDETAILS_DOMAINKEY="mobilizationDetail";

	public static final String DOMAIN_COLLECTIONORDER="com.velos.stafa.business.domain.apheresis.CollectionOrder";
	public static final String COLLECTIONORDER_DOMAINKEY="collectionOrder";

	public static final String DOMAIN_DONORSCREENING="com.velos.stafa.business.domain.apheresis.DonorScreening";
	public static final String DONORSCREENING_DOMAINKEY="donorScreening";

	public static final String DOMAIN_TASKLIBRARY="com.velos.stafa.business.domain.TaskLibrary";
	public static final String TASKLIBRARY_DOMAINKEY="taskLibrary";


	public static final String DOMAIN_PRODUCTLABRESULT="com.velos.stafa.business.domain.apheresis.ProductLabResult";
	public static final String PRODUCTLABRESULT_DOMAINKEY="productLabResult";

	public static final String DOMAIN_DONORLABRESULT="com.velos.stafa.business.domain.apheresis.DonorLabResult";
	public static final String DONORLABRESULT_DOMAINKEY="donorLabResult";

	public static final String DOMAIN_PERSON="com.velos.stafa.business.domain.Person";
	public static final String PERSON_DOMAINKEY="pkPerson" ;
	public static final String DOMAIN_DONOR="com.velos.stafa.business.domain.apheresis.Donor";
	public static final String DONOR_DOMAINKEY="donorId";

	public static final String DOMAIN_PROCESSINGCALC="com.velos.stafa.business.domain.ProcessingCalculation";
	public static final String PROCESSINGCALC_DOMAINKEY="processingCalculation" ;

	public static final String  RESET_PASSWORD ="RESET PASSWORD";
	public static final String  SUBJECT ="subject";
	public static final String NOTIFICATION_DOMAIN="com.velos.stafa.business.domain.Notification";
	public static final String NOTIFICATION_DOMAINKEY="notifyId";

	// Next option code
	public static final String MODULE_ACQUISITION ="Acquisition";
	public static final String MODULE_ACCESSION ="Accession";
	public static final String MODULE_CRYOPREP ="CryoPrep";
	public static final String MODULE_STORAGE ="Storage";
	public static final String MODULE_TRANSFER ="Transfer";
	public static final String MODULE_PROCESSING ="Processing";


	// --- End of next option

	public static final String CODELST_PRODUCT_STATUS_TYPE="productstatus";
	public static final String PRODUCT_STATUS_ACTIVE="active";
	public static final String PRODUCT_STATUS_INACTIVE="inactive";
	public static final String PRODUCT_STATUS_ACQUIRED="acquired";
	public static final String PRODUCT_STATUS_ACCESSED="accessed";
	public static final String PRODUCT_STATUS_PROCESSED="processed";
	public static final String PRODUCT_STATUS_CRYOPREP="cryoprepared";
	public static final String PRODUCT_STATUS_STORED="stored";
	public static final String PRODUCT_STATUS_TRANSFERED="transfered";


	public static final String PHYSICIAN_TOTAL_DOSE="com.velos.stafa.business.domain.PhysicianTotalDose";
	public static final String PHYSICIAN_TOTAL_DOSE_ID = "physicianTotalDose";
	public static final String PHYSICIAN_SELECTED_PRODUCTS="com.velos.stafa.business.domain.PhysicianSelectedProducts";
	public static final String PHYSICIAN_SELECTED_PRODUCTS_ID="physSelectProduct";
	public static final String CHILDS_CREATION="com.velos.stafa.business.domain.CryoChildCreation";
	public static final String CHILDS_CREATION_ID="cryoChilds";
	public static final String SPECIMEN="com.velos.stafa.business.domain.Specimen";
	public static final String SPECIMEN_ID="pkSpecimen";
	public static final String SPECIMEN_SPECID="specId";
	public static final String PRODUCT_PROTOCOL="com.velos.stafa.business.domain.ProductProtocol";
	public static final String PRODUCT_PROTOCOL_ID="productProtocol";
	public static final String PRODUCT_REAGENT_SUPPLIERS="com.velos.stafa.business.domain.ProductReagentSuppliers";
	public static final String PRODUCT_REAGENT_SUPPLIERS_ID="productReagntSupp";
	public static final String PRODUCT_QC="com.velos.stafa.business.domain.ProductQc";
	public static final String PRODUCT_QC_ID="productQc";
	public static final String PRODUCT_EQUIPMENTS="com.velos.stafa.business.domain.Equipments";
	public static final String PRODUCT_EQUIPMENTS_ID="equipmentsId";

	public static final String APPLICATIONVERSION_PARENTCODE = "APPLICATIONVERSION";
	public static final String TYPE = "TYPE";
	public static final String ESECURITY_SERVICEFLAG="ESECURITY_SERVICEFLAG";
	public static final String ESECURITY_URL="ESECURITY_URL";
	public static final String USER_OBJECT = "userDetailsObject";
	public static final String SECURITY_ENABLE_KEY = "true";

	public static final String DOMAIN_PROTOCOLCALC="com.velos.stafa.business.domain.ProtocolCalcualation";
	public static final String DOMAIN_PROTOCOL="com.velos.stafa.business.domain.Protocol";
	public static final String DOMAIN_PROTOCOLPAGE ="com.velos.stafa.widget.domain.PageConfig";

	public static final String DOMAIN_PROCESSING="com.velos.stafa.business.domain.Processing";
	public static final String SUCCESS_MESSAGE = "Data Saved Successfully";
	public static final String CODELST_DOMAIN="com.velos.stafa.business.domain.CodeList";
	public static final String TEMPPRODUCT_DOMAIN="com.velos.stafa.business.domain.ErTempProduct";
	public static final String QCRESULT_DOMAIN="com.velos.stafa.business.domain.QcResult";
	public static final String QC_DOMAIN="com.velos.stafa.business.domain.QC";
	public static final String REAGENTSUPPLIER_DOMAIN="com.velos.stafa.business.domain.ReagentsSuppliers";
	public static final String IMMNOPHENOTYPING_DOMAIN="com.velos.stafa.business.domain.Immnophenotyping";
	public static final String BARCODEMAINTENANCE_DOMAIN="com.velos.stafa.business.domain.BarcodeMaintenance";
	public static final String BARCODEIDENTIFIER_DOMAIN="com.velos.stafa.business.domain.barcode.BarcodeIdentifier";
	public static final String QCRESULT_ID = "qcResultId";
	public static final String CODELST_ID = "pkCodelst"; 
	public static final String QC_ID = "qcId";
	public static final String REAGENTSUPPLIERS_ID = "reagentSuppliersId";
	public static final String IMMNOPHENOTYPING_ID = "immnopheId";
	public static final String PRODUCT_DESCRIPTION_CODE_DOMAIN="com.velos.stafa.business.domain.ProductDescriptionCode";
	public static final String COLLECTION_FACILITIES_DOMAIN="com.velos.stafa.business.domain.CollectionFacilities";
	public static final String DOMAIN_DOCUMENT_ADDITIVES="com.velos.stafa.business.domain.DocumentsAdditives";
	public static final String DOMAIN_DOCUMENT_ADDITIVES_KEY="documentAdditives";

	public static final String DOMAIN_BARCODE_LABLES="com.velos.stafa.business.domain.BarcodeLables";
	public static final String DOMAIN_BARCODE_LABLES_KEY="pkBarcodeLable";
	public static final String BARCODE_LABLES_ENTITY="barcodeEntity";
	public static final String BARCODE_LABLES_TEXT="barcodeText";

	public static final String PERSONPRODUCTID = "personproductid";
	public static final String CODELST_SEQUENCE = "sequence";
	public static final String ACQUISITION_DOMAIN="com.velos.stafa.business.domain.Acquisition";
	public static final String ACQUISITION_ID = "pkAcquisition";
	public static final String CODELSTTYPE_STAFA = "stafa";
	public static final String CODELSTTYPE_PANEL = "panel";
	public static final String CODELSTTYPE_SUBHEADER = "prodtabs";
	public static final String CODELSTTYPE_QC = "qc";
	public static final String CODELSTTYPE_REAGENTSUPPLIER = "reagentssuppl";
	public static final String CODELSTTYPE_QCRESULT = "qcresult";
	public static final String CODELSTTYPE_PROTOCOL = "prodprotocol";
	public static final String CODELST_TYPE_WORKFLOW="workflow";
	public static final String CODELST_SUBTYPE_WORKFLOW="workflowflag";
	public static final String CODELSTTYPE_CALCULATION ="calcfields";
	public static final String PROCESSING_ID = "processing";
	public static final String CODELSTTYPE_MODULE = "stafamodule";
	public static final String DOCUMENT_OBJECTNAME = "Documents";
	public static final String DOMAIN_DOCUMENT="com.velos.stafa.business.domain.Documents";
	public static final String SUB_DOMAIN_DOCUMENT="com.velos.stafa.business.domain.UploadedDocInfo";
	public static final String DOCUMENT_ID = "pkDocumentId";
	public static final String ACQUISITION = "Acquisition";
	public static final String ACCESSION = "Accession";
	

	public static final String DELETEFLAG = "1";
	public static final String UPDATEFLAG = "1";

	/**For codelist Start*/
	
	//Common 
	public static final String PRODUCTTYPE="producttype";
	public static final String PROCSELEC="procfacility";
	public static final String PRODUCTSOURCE="prodsource";
	public static final String RECORDLIMITTYPE="recordlimit";
	public static final String RECORDLIMITSTYPE="50";
	
	
	//Acquisition
	public static final String AQULOCATION="acqlocation";
	public static final String FINALDISP="finaldispos";
	public static final String DONORLOC="location";

	//Accession
	public static final String ACCLOCATION="acqlocation";
	public static final String DIAGNOSIS="diagnosis";
	public static final String ABORH="abo/rh";
	public static final String BARCODEABORH="barcodeaborh";
	public static final String BEGSTATUS="begstatus";
	public static final String PLANPROTOCOL="planprotocol";
	public static final String RECIPRID="reciprid";
	public static final String PRODREGISTRY="prodregistry";
	public static final String PRODPROTOCOL="prodprotocol";
	public static final String FINALSTATUS="finalstatus";
	public static final String COLLLOCATION="colllocation";
	public static final String COURIERNAME="couriername";
	//for Barcode
	public static final String CLASSNAME="class";
	public static final String CRYOPROTECTANT="cryoprotectant";
	public static final String MANIPULATION="manipulation";
	public static final String INTENDEDUSE="intendeduse";
	public static final String DONATIONTYPE="donationtype";
	public static final String DONATIONSUBTYPEAUTOLOGUS="donationtypeautologus";
	public static final String DONATIONSUBTYPEDESIGNATED="donationtypedesignated";
	public static final String MODIFIER="modifier";
	public static final String ANTICOAGULANT_ADDITIVE="anticoagulant/additive";
	public static final String STORAGETEMPERATURE="storagetemperature";
	public static final String STORAGETEMPERATURETEXT="storagetemperaturetext";
	public static final String PRODUCTDESCRIPTIONCODE="ProductDescriptionCode";
	public static final String BARCODEMODEL="barcodemodel";
	public static final String SHIPENVIRONTMENT="shipenvirontment";
	public static final String DONORRELATION="donorrelation";
	public static final String DONORRELATIONTEXT="donorrelationtext";

	//for OriginalProduct
	public static final String DOMAIN_ORIGINALPRODUCT="com.velos.stafa.business.domain.OriginalProduct";
	public static final String ACCESSINGSTATION="accessingstation";
	public static final String ACCESSINGHOOD="accessinghood";

	//Storage
	public static final String CURRENTLOCATION="currlocation";
	public static final String INTENDEDLOCATION="intlocation";
	public static final String SLOTARRANG="slotarrang";

	//for Processing
	public static final String PROCESSINGSTATION="processingstation";
	public static final String PROCESSINGHOOD="processinghood";
	public static final String PROCEDURE="procedure";
	public static final String METHOD="method";
	/**For codelist End*/

	//CryoPreparation
	public static final String CRYOPREPARATION_DOMAIN="com.velos.stafa.business.domain.CryoPrep";
	public static final String CRYOPREPARATION_ID="cryoPrep";
	public static final String REFERENCEVIALS = "referencevials";
	public static final String CRYOBAGVOLUME="cryobagvolume";

	public static final String COMPONENT="component";
	public static final String CONTAINER="container";
	public static final String INTENTEDPROCESS="intentedprocess";

	// Apheresis Preparation
	public static final String DOMAIN_PREPARATION_REAGENTS="com.velos.stafa.business.domain.apheresis.PreparationReagents";
	public static final String PREPARATION_REAGENTS_ID="preparationReagents";
	public static final String DOMAIN_PREPARATION_DISPOSAL="com.velos.stafa.business.domain.apheresis.DisposableSupplies";
	public static final String PREPARATION_DISPOSABLE_ID="disposableId";
	public static final String DOMAIN_PREPARATION_MACHINE="com.velos.stafa.business.domain.apheresis.PreparationMachine";
	public static final String PREPARATION_EQUIMENT_ID="machineId";
	public static final String DOMAIN_PREPARATION="com.velos.stafa.business.domain.apheresis.Preparation";
	public static final String PREPARATION_ID="preparation";
	public static final String DOMAIN_PREPARATION_EQUIMENTS="com.velos.stafa.business.domain.apheresis.ApheresisPreparationEquipment";
	public static final String PREPARATION_EQUIMENTS_ID="preparationEquipmentsId";
	public static final String MANUFACTURE="manufacture";
	public static final String REAGENTSLOT="apheresislot";
	public static final String MODELNO="modelnumber";
	public static final String MHSNO="mhsnumber";
	public static final String REAGENTSNAME="reagentsname";
	
	////Apheresis Verification
	public static final String DOMAIN_VERIFICATION="com.velos.stafa.business.domain.apheresis.Verification";
	public static final String VERIFICATION_ID="verification";
	public static final String COLLECTIONLOCATION="collectionlocation";
	
	public static final String DOMAIN_PROCESSING_PARAMETER="com.velos.stafa.business.domain.apheresis.ProcessingParameter";
	public static final String PROCESSING_PARAMETER_ID="processingParameter";
	

	// Apheresis Add donor
	public static final String DONORTYPE="donortype";
	public static final String PLANNEDPROCEDURE="plannedprocedure";
	public static final String DONORSTATUS="donorstatus";
	public static final String DONORPHYSICIAN="donorphysician";
	public static final String TRANSPLANTPHYSICIAN="transplantphysician";
	public static final String ABS="abs";
	public static final String RECIPIENTDIAGNOSIS="recipientdiagnosis";
	public static final String UNITS_HEIGHT="units_height";
	public static final String UNITS_WEIGHT="units_weight";
	
	//Apheresis donor details
	public static final String GENDER="gender";
	public static final String LANGUAGE="language";
	public static final String HIVTYPE1="hivtype1";
	public static final String HIVTYPE2="hivtype2";
	public static final String HEPATITISB="hepatitisB";
	public static final String HEPATITISC="hepatitisC";
	public static final String TREPONEMA="treponemapallidum";
	public static final String HTCLVIRUS1="htclvirus1";
	public static final String HTCLVIRUS2="htclvirus2";
	public static final String WESTNILEVIRUS="westnilevirus";
	public static final String TYPONOSOMA="typonosoma";
	public static final String CMV="cmv";
	public static final String MOBILIZATIONMETHOD="mobilizationmethod";
	public static final String MOBILIZERNAME="mobilizername";

	public static final String MOBILIZATIONORDERISSUEDBY="mobilizationorderissuedby";
	public static final String MOBILIZATIONNURSE="mobilizationnurse";
	public static final String DOCUMENTTYPE="documenttype";
	public static final String COLLECTYIONORDERISSUEDBY="collectionorderissuedby";
	
	public static final String TASK_OTHERTASKS="othertasks";
	public static final String DESTINATION ="destination";
	public static final String TASKLEVEL ="tasklevel";
	public static final String TASKACTION = "taskaction";
	public static final String TASKASIGN = "taskasign";
	public static final String FREQUENCY = "frequency";
	public static final String RECURRINGTASK = "recurringtask";
	public static final String APHERESISRECURRINGTASKS = "apheresisrecurringtasks";
	public static final String DONORELIGIBILITY = "donoreligibility";
	public static final String TRAGETCD34UNIT = "targetCD34";
	
	//Apheresis Collection
	public static final String DOMAIN_COLLECTION="com.velos.stafa.business.domain.apheresis.ApheresisCollection";
	public static final String COLLECTION_ID="aphCollection";
	public static final String COLLECTION_PRODUCT_REF_ID="productId";
	public static final String ACCESS="access";
	public static final String PRECOLLECTIONRETURN="return";
	public static final String WITHDRAW="withdraw";
	public static final String COMMENTS="comments";
	
	//Apheresis Transport
	public static final String DOMAIN_TRANSPORT="com.velos.stafa.business.domain.apheresis.Transport";
	public static final String TRANSPORT_ID="transport";
	public static final String PRODUCTDESTINATION="productdestination";
	
	//Apheresis Followup
	public static final String DOMAIN_FOLLOWUP="com.velos.stafa.business.domain.apheresis.Followup";
	public static final String FOLLOWUP_ID="followUp";
	
	//Apheresis PostCollection
	public static final String DOMAIN_POSTCOLLECTION="com.velos.stafa.business.domain.apheresis.PostCollection";
	public static final String POSTCOLLECTION_ID="postCollection"; 
	public static final String INJECTIONPORT="injectionport";
	public static final String PERIPHERALACCESS="peripheralaccess";
	//Apheresis Reagents_
	public static final String DOMAIN_REAGENTS="com.velos.stafa.business.domain.ReagentsSupplies";
	public static final String REAGENTS_ID="reagentsupplies"; 
	
	//Apheresis Medication 
	public static final String DOMAIN_MEDICATION="com.velos.stafa.business.domain.apheresis.Medication";
	public static final String MEDICATION_ID="medication"; 
	public static final String MEDICATIONNAME="medicationname";
	//Apheresis Anticoagulant and Additives
	public static final String DOMAIN_ANTICOAGULASNTS="com.velos.stafa.business.domain.apheresis.Anticoagulants";
	public static final String ANTICOAGULASNTS_ID="antigulants"; 
	public static final String DOMAIN_ADDITIVES="com.velos.stafa.business.domain.apheresis.Additives";
	public static final String ADDITIVES_ID="additives"; 
	
	//Questions
	public static final String DOMAIN_QUESTIONS="com.velos.stafa.business.domain.apheresis.Questions";
	public static final String QUESTIONS_ID="questions"; 
	
	public static final String DOMAIN_RECURRINGTASK="com.velos.stafa.business.domain.apheresis.RecurringTask";
	public static final String RECURRING_ID="recurringTask";
	
	//Barcode Constants
	public static final String THIRD_PARTY_PRESENT = "Component from 3rd Party Present";
	public static final String OTHER_ADDITIVES_PRESENT = "Other Additives Present";
	public static final String GENETICALLY_MODIFIED = "Genetically Modified";
	public static final String SEE_ATTACHEMENT = "See Attached Documentation For Details";
	
	
	public static final String AUTOLOGOUS_USE_ONLY = "Autologous Use Only";
	public static final String INTENDED_RECIPIENT_ONLY = "For Use Intended Recipient Only";
	public static final String DO_NOT_IRRADIATE = "Do Not Irradiate";
	public static final String DO_NOT_USE_LEUKOREDUCTION = "Do Not Use Leukoreduction Filter";
	
	//Apheresis BloodBank
	public static final String DOMAIN_BLOODBANK="com.velos.stafa.business.domain.apheresis.BloodBank";
	public static final String BLOODBANK_ID="bloodbank";

	//CTL
	
	public static final String DOMAIN_INFUSIONINFO="com.velos.stafa.business.domain.ctl.InfusionInformation";
	public static final String INFUSIONINFO_DOMAINKEY="pkInfusionInformation";
	public static final String DOMAIN_INFUSION="com.velos.stafa.business.domain.ctl.Infusion";
	public static final String INFUSION_DOMAINKEY="pkInfusion" ;
	public static final String DOMAIN_SOPLIBRARY="com.velos.stafa.business.domain.ctl.SOPLibrary";
	public static final String SOPLIBRARY_DOMAINKEY="sopLibarary" ;
	public static final String PRODUCTSTATE="productstate";
	public static final String PRODUCTFORM="productform";
	public static final String PRODUCTSTATUS="ctlproductstatus";
	public static final String RECIPEINTLOCATION="ctlrecipeintlocation";
	public static final String THAWSTATION="ctlthawstation";
	public static final String THAWMETHOD="ctlthawmethod";
	public static final String WATERBATH="ctlwaterbath";
	
	public static final String DONORLOCATION="donorlocation";
	public static final String CTLRECURRINGTASKS = "ctlrecurringtasks";
	public static final String SOPCATEGORY="sopcategory";
	public static final String CTLDOCUMENTTYPE="ctldocumenttype";
	public static final String COLLECTIONFACILITY="collectionfacility";
	public static final String CTLDAILYASSIGNMENT = "ctldailyassignment";
	public static final String PRODUCTCOMINGFROM = "productcomingfrom";
	public static final String DOMAIN_INTERNALTRANSPORT = "com.velos.stafa.business.domain.ctl.InternalTransport";
	public static final String INTERNALTRANSPORT_DOMAINKEY = "internalTransport";
	public static final String DOMAIN_CTLTASKS = "com.velos.stafa.business.domain.ctl.CtlTasks";
	public static final String CTLTASKS_DOMAINKEY = "ctlTasks";
	public static final String DOMAIN_INSPECTION = "com.velos.stafa.business.domain.ctl.Inspection";
	public static final String INSPECTION_DOMAINKEY = "pkInspection";
	public static final String DOMAIN_RETRIEVAL = "com.velos.stafa.business.domain.ctl.Retrieval";
	public static final String RETRIVAL_DOMAINKEY = "pkRetrival";
	public static final String DOMAIN_INFUSIONPREPARARTION = "com.velos.stafa.business.domain.ctl.InfusionPreparation";
	public static final String INFUSIONPREPARARTION_ID = "pkPreparation";
	
	public static final String DOMAIN_CTLDAILYASSIGNMENT = "com.velos.stafa.business.domain.ctl.CTLDailyAssignment";
	public static final String CTLDAILYASSIGNMENT_DOMAINKEY = "dailyAssignments";
	
	//CTL Relocation
	public static final String DOMAIN_SHIPPINGINFO = "com.velos.stafa.business.domain.ctl.ShippingInformation";
	public static final String SHIPPINGINFO_DOMAINKEY="shippingInformation";
	public static final String DOMAIN_RELOCATION = "com.velos.stafa.business.domain.ctl.Relocation";
	public static final String RELOCATION_DOMAINKEY="pkRelocation";
	public static final String SHIPPINGENVIRONMENT = "shippingenvironment";
	public static final String SHIPPINGCOMPANY = "shippingcompany";
	
	//CTL Discard
	public static final String DOMAIN_DISCARD="com.velos.stafa.business.domain.ctl.Discard";
	public static final String DISCARD_DOMAINKEY="discard";
	public static final String REASONFORDISCARD = "reasonfordiscard";
	
	//Product Processing
	public static final String DOMAIN_PRODUCTPROCESS ="com.velos.stafa.business.domain.ctl.ProductProcessing";
	public static final String PRODUCTPROCESS_DOMAINKEY = "productProcessId";
	
	//Thaw
	public static final String DOMAIN_THAW ="com.velos.stafa.business.domain.ctl.Thaw";
	public static final String THAW_DOMAINKEY = "thawId";
	
	//Track Shipping
	public static final String TRACKSHIPPINGSTATUS = "trackshippingstatus";
	
	//Product Pre Processing
	public static final String SPLITCOMPONENT = "splitcomponent";
	public static final String SPLITBAG = "splitbag";
	public static final String SPLITCOMPONENTID = "splitcomponentid";
	public static final String SPLITINTENDEDPROCESS = "splitintendedprocess";
	
	public static final String POOLCOMPONENT = "poolcomponent";
	public static final String POOLBAG = "poolbag";
	public static final String POOLCOMPONENTID = "poolcomponentid";
	public static final String POOLINTENDEDPROCESS = "poolintendedprocess";
	
	public static final String DOMAIN_PREPROCESSING = "com.velos.stafa.business.domain.ctl.PreProcessing";
	public static final String PREPROCESSING_ID = "preProcessing";
	
	public static final String DOMAIN_SPLITPOOLPRODUCT = "com.velos.stafa.business.domain.ctl.SplitPoolProduct";
	public static final String SPLITPOOLPRODUCT_ID = "splitPoolProduct";
	public static final String DOMAIN_SPLITPOOL = "com.velos.stafa.business.domain.ctl.SplitPool";
	public static final String TYPE_POOL = "POOL";
	public static final String TYPE_SPLIT = "SPLIT";
	
	//CTL Product Inventory
	public static final String DOMAIN_RECEIPTINFO = "com.velos.stafa.business.domain.ctl.ReceiptInformation";
	public static final String RECEIPTINFO_DOMAINKEY = "receiptInformation";
	
	//UserSessionLog Tracking
	public static final String DOMAIN_USERSESSIONLOG = "com.velos.stafa.business.domain.usertracking.UserSessionLog";
	public static final String USERSESSIONLOG_DOMAINKEY = "pkUsl";
	
	public static final String DOMAIN_USERSESSION = "com.velos.stafa.business.domain.usertracking.UserSession";
	public static final String USERSESSION_DOMAINKEY = "pkUserSession";
	
	public static final String USER_SESSION_OBJECT = "userSessionObject";
	
	//Clinical
	
	public static final String MARITIAL_STATUS="maritalstatus";
	public static final String RELATIONSHIP="relationship";
	public static final String DOMAIN_RECIPIENT="com.velos.stafa.business.domain.apheresis.Recipient";
	public static final String RECIPIENT_DOMAINKEY="pkRecipient";
	
	//Upload Document Code
	
	public static final String TYPE_REFDOCTITLE="refdoctitle";
	public static final String SUBTYPE_REFERRAL="referral";
	
	public static final String TYPE_INSAPPDOCTITLE="insappdoctitle";
	public static final String SUBTYPE_CONSLTEVALAUTH="consltevalauth";	
	public static final String SUBTYPE_TXAUTH="txauth";
	public static final String SUBTYPE_CONSLTAUTH="consltauth";
	public static final String SUBTYPE_EVALAUTH="evalauth";
	public static final String SUBTYPE_CONCAREAUTH="concareauth";
	
	public static final String TYPE_PERSONTYPE="persontype";
	public static final String SUBTYPE_RECIPIENT="recipient";
	public static final String ALREADY_EXISTS_MESSAGE = "Mrn  already exists";
	
	public static final String TYPE_REFERRAL="referral";
	public static final String SUBTYPE_REFERRALDOC="referraldoc";
	public static final String SUBTYPE_INSUAPPROVREFERRALDOC="insuapprovreferraldoc";
	public static final String ATTACHMENT_ID = "fkAttachmentId";
	public static final String PRIM_LANG = "primlanguage";
	public static final String KIN_RELATION = "kinrelationship";
	public static final String RECIPIENT_STATUS = "recipientstatus";
	
	public static final String TYPE_RECIPIENTCONSENT = "recipientconsent";
	public static final String SUBTYPE_TXCONSENT = "txconsent";
	
	public static final String PERSON_MRN = "person_mrn";
	public static final String RACE = "race";
	public static final String ETHINICITY = "ethnicity";
	 public static final String STATE = "STATE";
	 public static final String COUNTRY = "country";
	 public static final String ABO = "aborh";
	 public static final String PRIM_CNT_RELATION = "primcntrelation";
	//public static final String TRANSPLANTPHYSICIAN = "transplantphysician";

	//Insurance
	public static final String INSURANCEAPPROVALNAME = "insappname"; 
	public static final String INSURANCESTATUS = "insstatus"; 
	public static final String INSURANCEAPPROVALSTATUS = "insappstatus"; 
	public static final String DOMAIN_INSURANCE_STATUS="com.velos.stafa.business.domain.insurance.InsuranceStatus";
	public static final String INSURANCESTATUS_DOMAINKEY= "insuranceStatusId";
	public static final String DOMAIN_INSURANCE_APPOVAL="com.velos.stafa.business.domain.insurance.InsuranceApproval";
	public static final String INSURANCEAPPROVAL_DOMAINKEY= "insuranceApprovalId";
	public static final String INSURANCEPLANSTATUS_TYPE="insplanstatus";
	/*public static final String INSURANCEPLANSTATUSACTIVE_SUBTYPE="insplanactive";
	public static final String INSURANCEPLANSTATUSEXPIRED_SUBTYPE="insplanexp";*/
	public static final String INSURANCESUSCRIBERREL_TYPE="inssusreation";
	public static final String INSURANCEGLOBALSTATUS_TYPE="insglobalstatus";
	public static final String INSURANCEGLOBALPLAN_TYPE="insglobalplan";
	public static final String INSURANCEPAYERLEVEL_TYPE="inspayerlevel";
	public static final String INSURANCEPAYMENTMETHODOLOGY="inspayemtmethod";
	public static final String INSURANCEMAINT_DOMAIN="com.velos.stafa.business.domain.insurance.InsuranceMaintenance";
	public static final String RECIPIENTINSURANCE_DOMAIN="com.velos.stafa.business.domain.insurance.RecipientInsurance";
    public static final String RECIPIENTINSURANCE_DOMAINKEY="pkPersonIns";
    public static final String INSURANCEROOMANDBOARD="roomandboards";

    public static final String TYPE_DOC_INSURANCE="doc_insurance";
    public static final String SUBTYPE_RECIINSURANCEDOC="reciinsurancedoc";
    public static final String SUBTYPE_PHARMACYDOC="pharmacydoc";
    public static final String SUBTYPE_HEALTHDOC="healthdoc";
    
    public static final String TYPE_INSURANCE="insurance";
    public static final String SUBTYPE_RECIPIENTINSDOC="recipientinsdoc";
    public static final String SUBTYPE_PATIENTBENIDOC="patientbenidoc";
    
    //Clinical	
	public static final String DOMAIN_CLINICAL="com.velos.stafa.business.domain.Clinical";
	public static final String CLINICAL_ID = "pkPersonVitalsign";
	
	public static final String TYPE_VITALSIGN="vitalsign";
	public static final String SUBTYPE_VITALSIGNHEIGHT="vitalsignheight";
	public static final String SUBTYPE_VITALSIGNWEIGHT="vitalsignweight";
	
	public static final String TYPE_ANTIBODY="antibody";
	public static final String SUBTYPE_ANTIBODYPOS="antibodypos";
	public static final String SUBTYPE_ANTIBODYNEG="antibodyneg";
	public static final String SUBTYPE_ANTIBODYNTDN="antibodyntdn";
	
	public static final String TYPE_ABORH="aborh";
	public static final String SUBTYPE_APOSITIVE="apositive";
	public static final String SUBTYPE_ANEGITIVE="anegitive";
	public static final String SUBTYPE_ABPOSITIVE="abpositive";
	public static final String SUBTYPE_ABNEGATIVE="abnegative";
	public static final String SUBTYPE_BPOSITIVE="bpositive";
	public static final String SUBTYPE_BNEGATIVE="bnegative";
	public static final String SUBTYPE_OPOSITIVE="opositive";
	public static final String SUBTYPE_ONEGATIVE="onegative";
	
	public static final String TYPE_UNITS_WEIGHT ="units_weight";
	public static final String SUBTYPE_LB ="lb";
	public static final String SUBTYPE_KG ="kg";
	
	public static final String TYPE_UNITS_HEIGHT ="units_height";
	public static final String SUBTYPE_CM ="cm";
	public static final String SUBTYPE_INCHES ="inches";

	
	//Referral
	public static final String REFERPHYSICIAN_DOMAINKEY = "refPhysicianId"; 
	public static final String DOMAIN_REFERRAL_PHYSICIAN="com.velos.stafa.business.domain.ReferringPhysician";
	//Registration Detail
	public static final String INTERNAL_PHYSICIAN = "I"; 
	//Disclimer Code
	
	public static final String DISC_CODE_REF = "referralDisclaimer";
	public static final String DISC_CODE_CONSULT = "consultEvalDiscliamer";
	public static final String DISC_CODE_INTERIM = "interimVistDiscliamer";
	public static final String DISC_CODE_PRETRANS ="preTransplantDisclaimer";
	public static final String DISC_CODE_PRECOLLEC ="preCollectionDisclaimer";
	public static final String DOMAIN_DISCLAIMER="com.velos.stafa.business.domain.Disclaimer";
	
	public static final String DISCLAIMER_DOMAINKEY="pkDisclaimer";
	public static final String TYPE_DEPARTMENT="department";
	public static final String SUBTYPE_ADULT="adult";
	public static final String SUBTYPE_PEDS="peds";
	
	public static final String TRANSPLANT_DOMAIN="com.velos.stafa.business.domain.Transplant";
	public static final String TRANSPLANT_ID = "pkTxWorkflow";
	
	// Entity Type
	public static final String ENTITY_TYPE_RECIPIENT="RECIPIENT";
	public static final String ENTITY_TYPE_DONOR="DONOR";
	public static final String ENTITY_TYPE_FOLLOWUP="FOLLOWUP";
	public static final String ENTITY_TYPE_PRODUCT="PRODUCT";
	public static final String ENTITY_TYPE_PERSON="PERSON";
	
	//visit
	public static final String DOMAIN_VISIT="com.velos.stafa.business.domain.Visit";
	public static final String PK_VISIT = "pkVisit";
	
	//Account Status
	public static final String ACCOUNT_STATUS = "accountstatus";
	public static final String ACCOUNT_STATUS_ACTIVE = "accstatactive";
	//Person Employment
	public static final String EMPLOYMENTSTATUS="employmentstatus";
	//Recipient Workflow
	public static final String REC_WORKFLOW= "workflow";
	public static final String REC_WORKFLOW_STATUS= "workflow_status";
	
	public static final String REC_WORKFLOW_REFERRAL = "referral";
	public static final String REC_WORKFLOW_CONSLUT = "consult_eval";
	public static final String REC_WORKFLOW_INTERIM = "interim_visit";
	public static final String REC_WORKFLOW_PRE_TRANSPLANT = "pre_transplant";
	public static final String REC_WORKFLOW_PRE_COLL = "pre_collection";
	public static final String REC_WORKFLOW_COLLECTION = "collection";
	public static final String REC_WORKFLOW_PREPREP = "preprep";
	public static final String REC_WORKFLOW_INFUSION = "infusion";
	public static final String REC_WORKFLOW_FOLLOWUP = "followup";
	
	public static final String REC_WORKFLOW_STATUS_REFERRAL_PAND = "referral_pend";
	public static final String REC_WORKFLOW_STATUS_CONSLUT_PAND = "consult_eval_pend";
	public static final String REC_WORKFLOW_STATUS_INTERIM_PAND = "interim_visit_pend";
	public static final String REC_WORKFLOW_STATUS_PRE_TRANSPLANT_PAND = "pre_transplant_pend";
	public static final String REC_WORKFLOW_STATUS_PRE_COLL_PAND = "pre_collection_pend";
	public static final String REC_WORKFLOW_STATUS_COLLECTION_PAND = "collection_pend";
	public static final String REC_WORKFLOW_STATUS_PREPREP_PAND = "preprep_pend";
	public static final String REC_WORKFLOW_STATUS_INFUSION_PAND = "infusion_pend";
	public static final String REC_WORKFLOW_STATUS_FOLLOWUP_PAND = "followup_pend";
	//Line Placement Detail
	public static final String LINE_PLACEMENT_LOCATION = "lineplacementlocation";
	public static final String CENTRAL_LINE_PLACEMENT = "centrallineplacement";
    public static final String DOMAIN_LINRPLACEMENT = "com.velos.stafa.business.domain.LinePlacementDetail";
	public static final String LINEPLACEMENT_DOMAINKEY = "pkLinePlacement";
	public static final String REC_WORKFLOW_STATUS_REFERRAL_COMP = "referral_comp";
	public static final String REC_WORKFLOW_STATUS_CONSLUT_COMP = "consult_eval_comp";
	public static final String REC_WORKFLOW_STATUS_INTERIM_COMP = "interim_visit_comp";
	public static final String REC_WORKFLOW_STATUS_PRE_TRANSPLANT_COMP = "pre_transplant_comp";
	public static final String REC_WORKFLOW_STATUS_PRE_COLL_COMP = "pre_collection_comp";
	public static final String REC_WORKFLOW_STATUS_COLLECTION_COMP = "collection_comp";
	public static final String REC_WORKFLOW_STATUS_PREPREP_COMP = "preprep_comp";
	public static final String REC_WORKFLOW_STATUS_INFUSION_COMP = "infusion_comp";
	public static final String REC_WORKFLOW_STATUS_FOLLOWUP_COMP = "followup_comp";
	
	// Visit Check List
	
	public static final String DOMAIN_CHECKLIST="com.velos.stafa.business.domain.CheckList";
	public static final String CHECKLIST_ID = "pkVisitChecklist";
	
	public static final String TYPE_CHECKLIST="checklist";
	public static final String SUBTYPE_PATINFOPAC="patinfopac";
	public static final String SUBTYPE_VIDEDUCLASS="videduclass";
	public static final String SUBTYPE_CLINCINPTOUR="clincinptour";
	
	public static final String SUBTYPE_VARIDMCURR="varidmcurr";
	public static final String SUBTYPE_VARPREGTESTCURR="varpregtestcurr";
	
	public static final String SUBTYPE_NA="na";
	public static final String SUBTYPE_INCOMPLETE="incomplete";
	public static final String SUBTYPE_COMPLETE="complete";

	//HPCM Pre-Collection Details	
	public static final String TYPE_ANELOC="anshthesialocation";
	public static final String DOMAIN_PRE_COLLECTION= "com.velos.stafa.business.domain.PreCollectionDetails";
	public static final String PRE_COLLECTION_DOMAINKEY = "pkPreCollectionDetails";
	
	
	// visit Documents
	public static final String TYPE_VISITDOC="visit_doc";
	public static final String SUBTYPE_HEALHISSQUES ="healhissques";
	public static final String SUBTYPE_NURSEASSESS ="nurseassess";
	public static final String SUBTYPE_IMMUNREC ="immunrec";
	public static final String SUBTYPE_EDUREC ="edurec";
	
	public static final String TYPE_DOCTYPE="doctype";
	public static final String SUBTYPE_VISITDOCUMENT ="visitdocument";
	
	//widget
	public static final String TYPE_WIDGET="widget";
	public static final String SUBTYPE_INSAPPROVDOCS ="insapprovdocs";
	//sql type
	public static final String TYPE_HQL = "hql";
	public static final String TYPE_SQL = "sql";
	public static final String ENTITY_RECIPIENT = "RECIPIENT";
	
	//labs
	public static final String TYPE_LAB_GROUP = "lab_group";
	public static final String TYPE_LAB_GROUP_PRE = "med_labs";
	public static final String TYPE_LAB_GROUP_ATTACH = "attach_labs";
	public static final String TYPE_LAB_GROUP_IDM = "idm_labs";
	

}