package com.velos.stafa.action.cdmr;

import java.util.List;
import java.util.Map;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import javax.servlet.http.HttpServletRequest;
import com.velos.stafa.action.StafaBaseAction;
import com.opensymphony.xwork2.ActionContext;
import org.apache.struts2.ServletActionContext;
import com.velos.stafa.util.QuartzSchedulerListener;
import com.velos.stafa.controller.CDMRController;


/**
 * @summary     UpdateSchedulerAction
 * @description Update Scheduler
 * @version     1.0
 * @file        UpdateSchedulerAction.java
 * @author      Lalit Chattar,Shikha Srivastav
 */
public class UpdateSchedulerAction extends StafaBaseAction{
	
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	public UpdateSchedulerAction(){
	}
	
	/**
	 * @description update scheduler(Set New Time)
	 * @return String
	 * @throws Exception
	 */
	public String updateScheduler() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			QuartzSchedulerListener qsl = new QuartzSchedulerListener();
			this.updateValueinDB(jsonObject.getString("timer"));
			qsl.rescheduleScheduler(jsonObject.getString("timer"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	public void updateValueinDB(String timer) throws Exception{
    	try {
    		CDMRController cdmrController = new CDMRController();
			String query = "UPDATE VBAR_CONTROLPARAM SET PARAM_VAL = '"+timer+"' WHERE PARAM_TYPE = 'SCHEDULER' and PARAM_SUBTYP = 'PST'";
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateSchdulerTime(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    }
	public String updateSchedulerExport() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			QuartzSchedulerListener qsl = new QuartzSchedulerListener();
			this.updateValueinDBExport(jsonObject.getString("timer"));
			qsl.rescheduleSchedulerExport(jsonObject.getString("timer"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public void updateValueinDBExport(String timer) throws Exception{
    	try {
    		CDMRController cdmrController = new CDMRController();
			String query = "UPDATE VBAR_CONTROLPARAM SET PARAM_VAL = '"+timer+"' WHERE PARAM_TYPE = 'EXPORT_SCHEDULER' and PARAM_SUBTYP = 'PST'";
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.updateSchdulerTime(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    }
	public String importFromFileNow() throws Exception{
		try{
			GetAndProcessRemoteData.getAndProcessRemoteData();
			
		}
	 catch (Exception e) {
		e.printStackTrace();
	}
	return SUCCESS;
		
	}
	
	public String exportToctmsNow() throws Exception{
		try{
			ProcessandExportData.getAndExportData();
		}
	 catch (Exception e) {
		e.printStackTrace();
	}
	return SUCCESS;
		
	}
}