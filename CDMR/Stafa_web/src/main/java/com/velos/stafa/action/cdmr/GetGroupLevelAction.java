package com.velos.stafa.action.cdmr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;

/**
 * @summary     GetGroupLevelAction
 * @description Get Group Levels
 * @version     1.0
 * @file        GetGroupLevelAction.java
 * @author      Lalit Chattar
 */
public class GetGroupLevelAction extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	
	private List<Map> dataList;
	private Integer level;
	private Integer pkGrpID;
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	/*
	 * getter and setter
	 */
	public List<Map> getDataList() {
		return dataList;
	}

	public void setDataList(List<Map> dataList) {
		this.dataList = dataList;
	}
	
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Integer getPkGrpID() {
		return pkGrpID;
	}
	public void setPkGrpID(Integer pkGrpID) {
		this.pkGrpID = pkGrpID;
	}
	/*
	 * Constructor
	 */
	public GetGroupLevelAction(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		getGroupLevel();
		return SUCCESS;
	}
	
	
	/**
	 * @description Get Group Levels
	 * @return String
	 * @throws Exception
	 */
	public String getGroupLevel() throws Exception{
		JSONObject jsonObject = new JSONObject();
		jsonObject = VelosUtil.constructJSON(request);
		try{
			String query = "";
			if(jsonObject.getString("level").equals("1"))
				query = CDMRSqlConstants.GET_GROUP_NAMES + " where GRP_LEVEL = 1";
			else if(jsonObject.getString("level").equals("2"))
				query = CDMRSqlConstants.GET_GROUP_NAMES + " where GRP_LEVEL = 2 and PARENT_NODE_ID = " + jsonObject.getString("pkGrpID");
			else if(jsonObject.getString("level").equals("3"))
				query = CDMRSqlConstants.GET_GROUP_NAMES + " where GRP_LEVEL = 3 and PARENT_NODE_ID = " + jsonObject.getString("pkGrpID");
			jsonObject.put("SQLQuery", true);
			jsonObject.put("query", query);
			jsonObject = cdmrController.getGroups(jsonObject);
			dataList = (List<Map>)jsonObject.get("groupcode");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
}