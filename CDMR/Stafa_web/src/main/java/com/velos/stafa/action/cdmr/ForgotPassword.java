package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.velos.stafa.util.CDMRUtil;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;


public class ForgotPassword extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);

	
	/*
	 * getter and setter
	 */
	

	/*
	 * Constructor
	 */
	public ForgotPassword() {
		cdmrController = new CDMRController();
	}
	
	
	/**
	 * @description Default Method of action for processing file information
	 * @return String
	 * @throws Exception
	 */
	public String execute() throws Exception{
		execute();
		return SUCCESS;
	}
	

	public String forgotPassword()throws Exception{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject.put("query", com.velos.stafa.util.CDMRSqlConstants.GET_SUPER_ADMIN);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getSuperAdmin(jsonObject);
			List<Map> list = (List<Map>)jsonObject.get("superadmin");
			for (Map map : list) {
				if(map.get("PARAM_SUBTYP").toString().equalsIgnoreCase("SUPER_ADMIN")){
					jsonObject.put("userId", map.get("PARAM_VAL").toString());
				}
				if(map.get("PARAM_SUBTYP").toString().equalsIgnoreCase("ADMIN_EMAIL")){
					jsonObject.put("adminEmail", map.get("PARAM_VAL").toString());
				}
			}
			jsonObject.put("loginName", jsonObject.getString("from"));
			jsonObject.put("userEmail", jsonObject.getString("notifyTo"));
			jsonObject.put("sentStatus", "N");
			jsonObject.put("msgTxt", "Dear Admin, User '"+jsonObject.getString("from")+"' has requested to reset password.");
			jsonObject.put("sentOn", jsonObject.getString("createdOn"));
			jsonObject.put("deletedFalg", 0);
			cdmrController.saveForgotPasswordRequest(jsonObject);
			JSONObject mailConfig = this.getMailConfiguration();
			mailConfig.put("TO", jsonObject.getString("adminEmail"));
			mailConfig.put("BODY", jsonObject.getString("msgTxt"));
			try{
				CDMRUtil.sendNotificationMail(mailConfig);
				}
			catch(Exception e){
				System.out.println(e.getMessage());
				System.out.println("mail not sent/issue in email server-----------------------------");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	public String resetForgotPassword() throws Exception{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject.put("query", com.velos.stafa.util.CDMRSqlConstants.GET_DEFAULT_PASSWORD);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getDefaultPassword(jsonObject);
			List<Map> list = (List<Map>)jsonObject.get("defaultpassword");
			String defaultPassword = list.get(0).get("PARAM_VAL").toString();
			jsonObject.put("query", com.velos.stafa.util.CDMRSqlConstants.GET_USER_EMAIl + "'"+jsonObject.getString("loginName")+"'");
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getUserEmail(jsonObject);
			list = (List<Map>)jsonObject.get("usermail");
			String userEmail = list.get(0).get("USER_EMAIL").toString();
			String body = "Dear " + jsonObject.getString("loginName") + ", Your password has been reset. Your default password is " + defaultPassword;
			JSONObject mailConfig = this.getMailConfiguration();
			mailConfig.put("TO", userEmail);
			mailConfig.put("BODY", body);
			try{
				CDMRUtil.sendNotificationMail(mailConfig);
				}
			catch(Exception e){
				e.getStackTrace();
				System.out.println("mail not sent/issue in email server-----------------------------");
			}
			this.updateNotificationStatus(jsonObject);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public void updateNotificationStatus(JSONObject jsonObject) throws Exception{
		jsonObject.put("query", com.velos.stafa.util.CDMRSqlConstants.UPDATE_NOTIFICATION + "'"+jsonObject.getString("loginName")+"'");
		jsonObject.put("SQLQuery", true);
		jsonObject = cdmrController.updateNotificationStatus(jsonObject);
	}
	 public  JSONObject getMailConfiguration() throws Exception{
		  JSONObject jsonObject = new JSONObject();
			try {
				String query = "select PARAM_SUBTYP, PARAM_VAL from vbar_controlparam where PARAM_TYPE in ('EMAIL') and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT', 'SSL') and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
				List<Map> data = (List<Map>)jsonObject.get("mailconfig");
				for (Map map : data) {
					jsonObject.put(map.get("PARAM_SUBTYP").toString(), map.get("PARAM_VAL").toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObject;
	  }
}