package com.velos.stafa.action.cdmr;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.velos.stafa.business.domain.cdmr.GrpCode;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     AddUpdateGrup
 * @description Add And Update Group(Service Section) 
 * @version     1.0
 * @file        FileProcessorAction.java
 * @author      Ved Prakash
 */
public class DownLoadHelpPDF extends StafaBaseAction{
	
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpServletResponse response = (HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	HttpSession httpSession = null;
	
	private InputStream fileInputStream;
    private String fileName;
    
	public InputStream getFileInputStream() {
		return fileInputStream;
	}


	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
    
	public DownLoadHelpPDF() {
		cdmrController = new CDMRController();
	}
	
	public String downloadHelpFile() throws Exception{
		JSONObject jsonObject = new JSONObject();
		jsonObject = VelosUtil.constructJSON(request);
		 response.setContentType("application/octet-stream");
	     response.setHeader("Content-Disposition","attachment;filename=CDMRHelp.pdf");
//		 this.fileName = "CDMRHelp.pdf";
//		 this.fileInputStream = new FileInputStream(new File("C:/" + this.fileName));
	     FileInputStream in = 
	      		new FileInputStream(new File("C:\\CDMRHelp.pdf"));
	     ServletOutputStream out = response.getOutputStream();
	     
	        byte[] outputByte = new byte[4096];
	        //copy binary content to output stream
	        while(in.read(outputByte, 0, 4096) != -1){
	        	out.write(outputByte, 0, 4096);
	        }
	        in.close();
	        out.flush();
	        out.close();
	
		return SUCCESS;
	}
	
	
}