package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.velos.stafa.util.CDMRUtil;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.business.domain.cdmr.GrpCode;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.util.CDMRConstant;
import atg.taglib.json.util.JSONObject;

/**
 * @summary     GetStagingData
 * @description Get Staging Data for Service Set View
 * @version     2.0
 * @file        GetStagingData.java
 * @author      Lalit Chattar, Ved Prakash
 */
public class GetStagingData extends StafaBaseAction{
	
	    private String fileType;
		private int recordcount; 
		private String pkString;
		private String pkStr;
		private String sEcho;
		private Integer iTotalRecords;
		private Integer iTotalDisplayRecords;
		private List<Map<String, String>> aaData;
		private String[] aColumns = {"INST_CODE", "COMP_COL1", "COMP_COL2","COMP_COL3","COMP_COL4","COMP_COL5",
				"COMP_COL6","COMP_COL7","COMP_COL8","COMP_COL9","COMP_COL10",
				"COMP_COL11","COMP_COL12","COMP_COL13","COMP_COL14","COMP_COL15",
				"COMP_COL16","COMP_COL17","COMP_COL18","COMP_COL19","COMP_COL20",
				"COMP_COL21","COMP_COL22","COMP_COL23","COMP_COL24","COMP_COL25",
				"COMP_COL26","COMP_COL27","COMP_COL28","COMP_COL29","COMP_COL29",
				"COMP_COL31","COMP_COL32","COMP_COL33","COMP_COL34","COMP_COL35",
				"COMP_COL36","COMP_COL37","COMP_COL38","COMP_COL39","COMP_COL40"};
		private String[] sortColumnForTech = {"INST_CODE", "COMP_COL1", "COMP_COL2","COMP_COL3","COMP_COL4","COMP_COL5",
				"COMP_COL6","COMP_COL7","COMP_COL8","COMP_COL9","COMP_COL10","COMP_COL11","COMP_COL12","COMP_COL13","COMP_COL14","COMP_COL15","COMP_COL16","COMP_COL17","COMP_COL18","COMP_COL20"};
		private String[] sortColumnForPro = {"INST_CODE", "COMP_COL2", "COMP_COL8","COMP_COL5","COMP_COL6","COMP_COL7",
				"COMP_COL4","COMP_COL1","COMP_COL3","COMP_COL10"};
		String sOrderBy = null;
		private StringBuilder sWhere;
		
		private CDMRController cdmrController;
		ActionContext ac = ActionContext.getContext();
		HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
		
		public String getFileType() {
			return fileType;
		}

		public void setFileType(String fileType) {
			this.fileType = fileType;
		}
		
		public String getPkString() {
			return pkString;
		}

		public void setPkString(String pkString) {
			this.pkString = pkString;
		}
		public String getPkStr() {
			return pkStr;
		}

		public void setPkStr(String pkStr) {
			this.pkStr = pkStr;
		}
		public String getsEcho() {
			return sEcho;
		}

		public void setsEcho(String sEcho) {
			this.sEcho = sEcho;
		}
		public Integer getiTotalRecords() {
			return iTotalRecords;
		}

		public void setiTotalRecords(Integer iTotalRecords) {
			this.iTotalRecords = iTotalRecords;
		}

		public Integer getiTotalDisplayRecords() {
			return iTotalDisplayRecords;
		}

		public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
			this.iTotalDisplayRecords = iTotalDisplayRecords;
		}
		public List<Map<String, String>> getAaData() {
			return aaData;
		}

		public void setAaData(List<Map<String, String>> aaData) {
			this.aaData = aaData;
		}

		
		public GetStagingData(){
			cdmrController = new CDMRController();
			aaData = new ArrayList<Map<String,String>>();
		}
		

		/**
		 * @description Get Columns maps from staging maps
		 * @return JSONObject
		 * @throws Exception
		 */
		public JSONObject getStagingColumnMapDetail(JSONObject jsonObject) throws Exception{
			String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP + " where dispInStaging  = 1 and fkMapVersion = " + jsonObject.getString("MAPSVERSION");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", false);
			jsonObject = cdmrController.getStagingColumnMapDetail(jsonObject);
			return jsonObject;
		}
		
		/**
		 * @description Create Dynamic Query for Technical data
		 * @return JSONObject
		 * @throws Exception
		 */
		public JSONObject createDynamicQueryForTechnicalData(JSONObject jsonObject) throws Exception{
			List<StagingMaps> mapsObjList = (List<StagingMaps>)jsonObject.get("stagingcolumnmap");
			StringBuilder query = new StringBuilder();
			
			query.append("select PK_CDM_STAGINGCOMPREC as PK, ");
			for (StagingMaps stagingMaps : mapsObjList) {
				
				if(stagingMaps.getCompColID().equals(CDMRConstant.INST_CODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as INST_CODE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.BLUE_CROSS_HCPCS_CPT)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as BLUE_CROSS_HCPCS_CPT,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.BLUE_CROSS_HCPCS_CPT_EFF_DATE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as BLUE_CROSS_HCPCS_CPT_EFF_DATE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.MCAID_HCPCS_CPT)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as MCAID_HCPCS_CPT,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.MCAID_HCPCS_CPT_EFF_DATE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as MCAID_HCPCS_CPT_EFF_DATE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.CMS_HCPCS_CPT)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as CMS_HCPCS_CPT,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.CMS_HCPCS_CPT_EFF_DATE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as CMS_HCPCS_CPT_EFF_DATE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.PRS_CODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as PRS_CODE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.CUSTOM_DATE1)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as CUSTOM_DATE1,");
					continue;
				}
				
				if(stagingMaps.getCompColID().equals(CDMRConstant.REV_CLASS)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as REV_CLASS,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.CUSTOM_DESC1)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as CUSTOM_DESC1,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.CUSTOM_CODE1)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as CUSTOM_CODE1,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.SERVICE_CODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as SERVICE_CODE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.SERVICE_DESC1)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as SERVICE_DESC1,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.SERVICE_DESC2)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as SERVICE_DESC2,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.REV_CODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as REV_CODE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.EFF_TO_DATE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as EFF_TO_DATE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.HCC_TECH_CHG)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as HCC_TECH_CHG,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.EFF_FROM_DATE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as EFF_FROM_DATE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.FK_CDM_GRPCODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as FK_CDM_GRPCODE,");
					continue;
				}
				
				
			}
            query = query.deleteCharAt(query.length()-1);
			
			sWhere = new StringBuilder();
			if ( jsonObject.getString("sSearch") != "" )
			{
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumns.length ; i++ )
				{
					sWhere.append("LOWER("+aColumns[i]+") LIKE '%"+jsonObject.getString("sSearch").toLowerCase()+"%' OR ");
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObject.has("iSortCol_0"))
			{
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObject.getInt("iSortingCols") ; i++ )
				{
					if((jsonObject.getString("bSortable_" + jsonObject.getString("iSortCol_"+i))).equals("true")){
						sOrderBy = sOrderBy + sortColumnForTech[jsonObject.getInt("iSortCol_"+i)] + " " + (jsonObject.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = "";
				}
				
			}
			
			int constant = Integer.parseInt(jsonObject.getString("iDisplayLength"));
			int end = Integer.parseInt(jsonObject.getString("iDisplayStart")) + Integer.parseInt(jsonObject.getString("iDisplayLength"));
			int start = Integer.parseInt(jsonObject.getString("iDisplayStart"))+1;
			query.append(" from (" +
					"SELECT t.*, Row_Number() OVER (ORDER BY PK_CDM_STAGINGCOMPREC) MyRow FROM CDM_STAGINGCOMPREC t  where PK_CDM_STAGINGCOMPREC in ("+this.getPkStr()+")"+sWhere+")"+
				" WHERE MyRow BETWEEN "+start+" AND "+end + sOrderBy);
			jsonObject.put("query", query);
			return jsonObject;
		}
		
		/**
		 * @description Process Technical Data for datatable
		 * @return void
		 * @throws Exception
		 */
		public void processTechnicalData(JSONObject jsonObject) throws Exception{
			List<Map> dataList = (List<Map>)jsonObject.get("comprecdata");
			try{
				for (Map map : dataList) {
					Map<String, String> data = new HashMap<String, String>();
					if((map.get("INST_CODE")) == null || (map.get("INST_CODE")) == "")
						data.put("instcode", "");
					else
						data.put("instcode", map.get("INST_CODE").toString());
					
					if((map.get("BLUE_CROSS_HCPCS_CPT")) == null || (map.get("BLUE_CROSS_HCPCS_CPT")) == "")
						data.put("BCBSHCPCSCode", "");
					else
						data.put("BCBSHCPCSCode",map.get("BLUE_CROSS_HCPCS_CPT").toString());

					if((map.get("BLUE_CROSS_HCPCS_CPT_EFF_DATE")) == null || (map.get("BLUE_CROSS_HCPCS_CPT_EFF_DATE")) == "")
						data.put("BCBSEffDate","");
					else
						data.put("BCBSEffDate",map.get("BLUE_CROSS_HCPCS_CPT_EFF_DATE").toString());

					
					if((map.get("MCAID_HCPCS_CPT")) == null || (map.get("MCAID_HCPCS_CPT")) == "")
						data.put("MCAIDHCPCSCPTCode","");
					else
						data.put("MCAIDHCPCSCPTCode",map.get("MCAID_HCPCS_CPT").toString());
					
					if((map.get("MCAID_HCPCS_CPT_EFF_DATE")) == null || (map.get("MCAID_HCPCS_CPT_EFF_DATE")) == "")
						data.put("MCAIDEffDate","");
					else
						data.put("MCAIDEffDate",map.get("MCAID_HCPCS_CPT_EFF_DATE").toString());
					
					if((map.get("CMS_HCPCS_CPT")) == null || (map.get("CMS_HCPCS_CPT")) == "")
						data.put("CMSHCPCSCPTCode","");
					else
						data.put("CMSHCPCSCPTCode",map.get("CMS_HCPCS_CPT").toString());
					
					if((map.get("CMS_HCPCS_CPT_EFF_DATE")) == null || (map.get("CMS_HCPCS_CPT_EFF_DATE")) == "")
						data.put("CMSEffDate","");
					else
						data.put("CMSEffDate",map.get("CMS_HCPCS_CPT_EFF_DATE").toString());
					
					if((map.get("PRS_CODE")) == null || (map.get("PRS_CODE")) == "")
						data.put("PRSCode","");
					else
						data.put("PRSCode",map.get("PRS_CODE").toString());
					
					if((map.get("CUSTOM_DATE1")) == null || (map.get("CUSTOM_DATE1")) == "")
						data.put("PRSEffDate","");
					else
						data.put("PRSEffDate",map.get("CUSTOM_DATE1").toString());
					
					if((map.get("REV_CLASS")) == null || (map.get("REV_CLASS")) == "")
						data.put("RevClass","");
					else
						data.put("RevClass",map.get("REV_CLASS").toString());
					
					if((map.get("CUSTOM_DESC1")) == null || (map.get("CUSTOM_DESC1")) == "")
						data.put("RevClassDesc","");
					else
						data.put("RevClassDesc",map.get("CUSTOM_DESC1").toString());
					
					if((map.get("CUSTOM_CODE1")) == null || (map.get("CUSTOM_CODE1")) == "")
						data.put("SVCCodeType","");
					else
						data.put("SVCCodeType",map.get("CUSTOM_CODE1").toString());
					
					
					if((map.get("SERVICE_CODE")) == null || (map.get("SERVICE_CODE")) == "")
						data.put("SVCCode","");
					else
						data.put("SVCCode",map.get("SERVICE_CODE").toString());
					
					if((map.get("SERVICE_DESC1")) == null || (map.get("SERVICE_DESC1")) == "")
						data.put("SVCCodeDesc","");
					else
						data.put("SVCCodeDesc",map.get("SERVICE_DESC1").toString());
					
					if((map.get("SERVICE_DESC2")) == null || (map.get("SERVICE_DESC2")) == "")
						data.put("SVCCodeGNLDesc","");
					else
						data.put("SVCCodeGNLDesc",map.get("SERVICE_DESC2").toString());
					
					if((map.get("REV_CODE")) == null || (map.get("REV_CODE")) == "")
						data.put("GLKey","");
					else
						data.put("GLKey",map.get("REV_CODE").toString());
					
					if((map.get("EFF_TO_DATE")) == null || (map.get("EFF_TO_DATE")) == "")
						data.put("SVCCodeINACTDate","");
					else
						data.put("SVCCodeINACTDate",map.get("EFF_TO_DATE").toString());
					
					if((map.get("HCC_TECH_CHG")) == null || (map.get("HCC_TECH_CHG")) == "")
						data.put("SVCPrice","");
					else
						data.put("SVCPrice",CDMRUtil.formateValue(map.get("HCC_TECH_CHG").toString()));
					
					if((map.get("EFF_FROM_DATE")) == null || (map.get("EFF_FROM_DATE")) == "")
						data.put("SVCPriceEffDate","");
					else
						data.put("SVCPriceEffDate",map.get("EFF_FROM_DATE").toString());
					
					
					if((map.get("FK_CDM_GRPCODE")) == null || (map.get("FK_CDM_GRPCODE")) == "")
						data.put("GroupCode","");
					else
						data.put("GroupCode",this.getGropuCodeValue(map.get("FK_CDM_GRPCODE").toString()));
					
					
					//data.put("edittd","<a href=\"#\" id=\""+map.get("PK").toString()+"\" class=\"edit\"><img src=\"images/cdmr/edit.png\"/></a>");
					
					aaData.add(data);
				}
				if(aaData.isEmpty()){
					this.iTotalDisplayRecords = 0;
					this.iTotalRecords = 0;
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			this.sEcho= jsonObject.getString("sEcho");
			this.iTotalRecords = recordcount;//Integer.parseInt(jsonObject.getString("recordcount"));
			this.iTotalDisplayRecords = recordcount;// Integer.parseInt(jsonObject.getString("recordcount"));
			
		}
		
		/**
		 * @description Create Dynamic Query for Professional Records
		 * @return JSONObject
		 * @throws Exception
		 */
		public JSONObject createDynamicQueryForProfessionalData(JSONObject jsonObject) throws Exception{
			List<StagingMaps> mapsObjList = (List<StagingMaps>)jsonObject.get("stagingcolumnmap");
			StringBuilder query = new StringBuilder();
			
			query.append("select PK_CDM_STAGINGCOMPREC as PK, ");
			for (StagingMaps stagingMaps : mapsObjList) {
				
				if(stagingMaps.getCompColID().equals(CDMRConstant.INST_CODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as INST_CODE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.CPT_CODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as CPT_CODE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.MCAID_HCPCS_CPT)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as MCAID_HCPCS_CPT,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.CMS_HCPCS_CPT)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as CMS_HCPCS_CPT,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.PRS_CODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as PRS_CODE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.SERVICE_DESC1)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as SERVICE_DESC1,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.EFF_TO_DATE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as EFF_TO_DATE,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.PRS_PHY_CHG)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as PRS_PHY_CHG,");
					continue;
				}
				if(stagingMaps.getCompColID().equals(CDMRConstant.EFF_FROM_DATE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as EFF_FROM_DATE,");
					continue;
				}
				
				if(stagingMaps.getCompColID().equals(CDMRConstant.FK_CDM_GRPCODE)){
					query.append(stagingMaps.getEqCompReqCol()).append(" as FK_CDM_GRPCODE,");
					continue;
				}
				
				
			}
            query = query.deleteCharAt(query.length()-1);
			
			sWhere = new StringBuilder();
			if ( jsonObject.getString("sSearch") != "" )
			{
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumns.length ; i++ )
				{
					sWhere.append(aColumns[i]+" LIKE '%"+jsonObject.getString("sSearch")+"%' OR ");
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObject.has("iSortCol_0"))
			{
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObject.getInt("iSortingCols") ; i++ )
				{
					if((jsonObject.getString("bSortable_" + jsonObject.getString("iSortCol_"+i))).equals("true")){
						sOrderBy = sOrderBy + sortColumnForPro[jsonObject.getInt("iSortCol_"+i)] + " " + (jsonObject.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = "";
				}
				
			}
			
			int constant = Integer.parseInt(jsonObject.getString("iDisplayLength"));
			int end = Integer.parseInt(jsonObject.getString("iDisplayStart")) + Integer.parseInt(jsonObject.getString("iDisplayLength"));
			int start = Integer.parseInt(jsonObject.getString("iDisplayStart"))+1;
			query.append(" from (" +
					"SELECT t.*, Row_Number() OVER (ORDER BY PK_CDM_STAGINGCOMPREC) MyRow FROM CDM_STAGINGCOMPREC t  where PK_CDM_STAGINGCOMPREC in ("+this.getPkStr()+")"+sWhere+")"+
				" WHERE MyRow BETWEEN "+start+" AND "+end + sOrderBy);
					
			System.out.print("+++---TestQuery---+++"+query);
			jsonObject.put("query", query);
			return jsonObject;
		}
		
		
		/**
		 * @description Process Professional data for datatable
		 * @return void
		 * @throws Exception
		 */
		public void processProfessionalData(JSONObject jsonObject) throws Exception{
			List<Map> dataList = (List<Map>)jsonObject.get("comprecdata");
			try{
				for (Map map : dataList) {
					Map<String, String> data = new HashMap<String, String>();
					if((map.get("INST_CODE")) == null || (map.get("INST_CODE")) == "")
						data.put("instcode", "");
					else
						data.put("instcode", map.get("INST_CODE").toString());
					
					if((map.get("CPT_CODE")) == null || (map.get("CPT_CODE")) == "")
						data.put("cptcode", "");
					else
						data.put("cptcode",map.get("CPT_CODE").toString());
					
					if((map.get("MCAID_HCPCS_CPT")) == null || (map.get("MCAID_HCPCS_CPT")) == "")
						data.put("MCAIDHCPCSCPTCode","");
					else
						data.put("MCAIDHCPCSCPTCode",map.get("MCAID_HCPCS_CPT").toString());
					
					if((map.get("CMS_HCPCS_CPT")) == null || (map.get("CMS_HCPCS_CPT")) == "")
						data.put("CMSHCPCSCPTCode","");
					else
						data.put("CMSHCPCSCPTCode",map.get("CMS_HCPCS_CPT").toString());
					
					
					if((map.get("PRS_CODE")) == null || (map.get("PRS_CODE")) == "")
						data.put("PRSCode","");
					else
						data.put("PRSCode",map.get("PRS_CODE").toString());
					
					if((map.get("SERVICE_DESC1")) == null || (map.get("SERVICE_DESC1")) == "")
						data.put("genrdesc","");
					else
						data.put("genrdesc",map.get("SERVICE_DESC1").toString());
					
					if((map.get("EFF_TO_DATE")) == null || (map.get("EFF_TO_DATE")) == "")
						data.put("SVCCodeINACTDate","");
					else
						data.put("SVCCodeINACTDate",map.get("EFF_TO_DATE").toString());
					
					if((map.get("PRS_PHY_CHG")) == null || (map.get("PRS_PHY_CHG")) == "")
						data.put("prschrg","");
					else
						data.put("prschrg",CDMRUtil.formateValue(map.get("PRS_PHY_CHG").toString()));
					
					if((map.get("EFF_FROM_DATE")) == null || (map.get("EFF_FROM_DATE")) == "")
						data.put("SVCPriceEffDate","");
					else
						data.put("SVCPriceEffDate",map.get("EFF_FROM_DATE").toString());
					
					if((map.get("FK_CDM_GRPCODE")) == null || (map.get("FK_CDM_GRPCODE")) == "")
						data.put("GroupCode","");
					else
						data.put("GroupCode",this.getGropuCodeValue(map.get("FK_CDM_GRPCODE").toString()));
					
					
					//data.put("edittd","<a href=\"#\" id=\""+map.get("PK").toString()+"\" class=\"edit\"><img src=\"images/cdmr/edit.png\"/></a>");
					
					aaData.add(data);
				}
				if(aaData.isEmpty()){
					this.iTotalDisplayRecords = 0;
					this.iTotalRecords = 0;
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			this.sEcho= jsonObject.getString("sEcho");
			
			
			this.iTotalRecords = recordcount;//Integer.parseInt(jsonObject.getString("recordcount"));
			this.iTotalDisplayRecords = recordcount;// Integer.parseInt(jsonObject.getString("recordcount"));
			
		}
		
		
		
		/**
		 * @description Process Data for datatable
		 * @return void
		 * @throws Exception
		 */
		public void processData(JSONObject jsonObject) throws Exception{
			List<Map> dataList = (List<Map>)jsonObject.get("comprecdata");
			try{
				for (Map map : dataList) {
					Map<String, String> data = new HashMap<String, String>();
					//data.put("check", "<input type=\"checkbox\" name=\"check\" onclick=\"addInArray(this.value);\" value=\""+map.get("PK").toString()+"\">");
					
					if((map.get("IC")) == null || (map.get("IC")) == "")
						data.put("instcode", "");
					else
						data.put("instcode", map.get("IC").toString());
					
					if((map.get("SERVICE_CODE")) == null || (map.get("SERVICE_CODE")) == "")
						data.put("servcode", "");
					else
						data.put("servcode",map.get("SERVICE_CODE").toString());

					if((map.get("SERVICE_DESC1")) == null || (map.get("SERVICE_DESC1")) == "")
						data.put("desc","");
					else
						data.put("desc",map.get("SERVICE_DESC1").toString());

					if((map.get("CPT_CODE")) == null || (map.get("CPT_CODE")) == "")
						data.put("cptcode","");
					else
						data.put("cptcode",map.get("CPT_CODE").toString());

					if((map.get("PRS_CODE")) == null || (map.get("PRS_CODE")) == "")
						data.put("prscode","");
					else
						data.put("prscode",map.get("PRS_CODE").toString());

					if((map.get("HCC_TECH_CHG")) == null || (map.get("HCC_TECH_CHG")) == "")
						data.put("prschrg","");
					else
						data.put("prschrg",map.get("HCC_TECH_CHG").toString());

					if((map.get("PRS_PHY_CHG")) == null || (map.get("PRS_PHY_CHG")) == "")
						data.put("hccchrg","");
					else
						data.put("hccchrg",map.get("PRS_PHY_CHG").toString());

					if((map.get("REV_CLASS")) == null || (map.get("REV_CLASS")) == "")
						data.put("revclass","");
					else
						data.put("revclass",map.get("REV_CLASS").toString());

					if((map.get("EFF_FROM_DATE")) == null || (map.get("EFF_FROM_DATE")) == "")
						data.put("effdate","");
					else
						data.put("effdate",map.get("EFF_FROM_DATE").toString());

//					if((map.get("FK_CDM_GRPCODE")) == null || (map.get("FK_CDM_GRPCODE")) == "")
//						data.put("grpcode","");
//					else
//						data.put("grpcode",this.getGropuCodeValue(map.get("FK_CDM_GRPCODE").toString()));
//					data.put("edittd","<a href=\"#\" id=\""+map.get("PK").toString()+"\" class=\"edit\"><img src=\"images/cdmr/edit.png\"/></a>");
//					data.put("notestd","<a href=\"#\"><img src=\"images/cdmr/add.png\" onclick=\"openNoteDialog();\"/></a>");
					aaData.add(data);
				}
				if(aaData.isEmpty()){
					this.iTotalDisplayRecords = 0;
					this.iTotalRecords = 0;
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			this.sEcho= jsonObject.getString("sEcho");
			
			
			this.iTotalRecords = recordcount;//Integer.parseInt(jsonObject.getString("recordcount"));
			this.iTotalDisplayRecords = recordcount;// Integer.parseInt(jsonObject.getString("recordcount"));
			
		}
		
		/**
		 * @description Get Group Code Value 
		 * @return String
		 * @throws Exception
		 */
		public String getGropuCodeValue(String grpCode) throws Exception{
			
			try{
				if(grpCode.equals(null) || grpCode == null || grpCode == "" || grpCode.equals("") || grpCode.equals(" "))
					return "";
				JSONObject  jsonObject = new JSONObject();
				String query = CDMRSqlConstants.GET_GROUPNAME_BY_CODE + grpCode;
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", false);
				jsonObject = cdmrController.getGroupCodeValue(jsonObject);
				List<GrpCode> listObj = (List<GrpCode>)jsonObject.get("groupcodename");
				GrpCode mainObj = listObj.get(0);
				return mainObj.getGrpDispName();
			}catch (Exception e) {
				return "";
			}
			
		}
		
	
	
	/**
	 * @description Get Technical Data
	 * @return String
	 * @throws Exception
	 */
	public String thecData() throws Exception{
		
		try{
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String pkStr = jsonObject.getString("pkString");
			
			
			pkStr = pkStr.substring(0, pkStr.length() - 1);
			this.setPkStr(pkStr);
			boolean flag=jsonObject.has("pkString");
			 
			
			if(flag){
				 Character c = ',';
				 int count=0;
				 for (int i = 0; i < pkStr.length(); i++) {
				  if(((Character)pkStr.charAt(i)).equals(c)){
					  count++;
				  }
				  
				}
				recordcount=count+1;
				jsonObject.remove("pkString");
				jsonObject.put("pkString", pkStr);
			}
			
			
			//jsonObject.put("MAPSVERSION", "4");
			this.getMapVersion(jsonObject);
			jsonObject = this.getStagingColumnMapDetail(jsonObject);
			jsonObject = this.createDynamicQueryForTechnicalData(jsonObject);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCompRecData(jsonObject);
		
			this.processTechnicalData(jsonObject);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
		return SUCCESS;
	}
	
	
	/**
	 * @description Get Professional Data
	 * @return String
	 * @throws Exception
	 */
	public String proData() throws Exception{
		
		try{
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String pkStr = jsonObject.getString("pkString");
			
			
			pkStr = pkStr.substring(0, pkStr.length() - 1);
			this.setPkStr(pkStr);
			boolean flag=jsonObject.has("pkString");
			 
			
			if(flag){
				 Character c = ',';
				 int count=0;
				 for (int i = 0; i < pkStr.length(); i++) {
				  if(((Character)pkStr.charAt(i)).equals(c)){
					  count++;
				  }
				  
				}
				recordcount=count+1;
				jsonObject.remove("pkString");
				jsonObject.put("pkString", pkStr);
			}
		
			
			//jsonObject.put("MAPSVERSION","5");
			this.getMapVersion(jsonObject);
			jsonObject = this.getStagingColumnMapDetail(jsonObject);
			jsonObject = this.createDynamicQueryForProfessionalData(jsonObject);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCompRecData(jsonObject);
			
			this.processProfessionalData(jsonObject);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
		return SUCCESS;
	}
	
	
	/**
	 * @description Get Map Version
	 * @return JSONObject
	 * @throws Exception
	 */
	public  JSONObject getMapVersion(JSONObject jsonObj){
		
		try {
			String query="select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID='"+jsonObj.getString("fileType")+"'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMapVersion(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");
			Map dataMap = list.get(0);
			
			//jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
			jsonObj.put("MAPSVERSION", dataMap.get("MAPSVERSION").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}	
}
