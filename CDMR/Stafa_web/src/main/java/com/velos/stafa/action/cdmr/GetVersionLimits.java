package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.velos.stafa.business.domain.cdmr.GrpCode;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRUtil;
/**
 * @summary     AddUpdateGrup
 * @description Add And Update Group(Service Section) 
 * @version     1.0
 * @file        FileProcessorAction.java
 * @author      Ved Prakash
 */
public class GetVersionLimits extends StafaBaseAction{
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession = null;
	
	/*
	 * Parameter getting from request
	 */
	
	private List dataList;
	
	 /* Constructor
	 */
	public GetVersionLimits(){
		cdmrController = new CDMRController();
		dataList = new ArrayList();
		
	}
	
	/*
	 * getter and setter
	 */
	public List getDataList() {
		return dataList;
	}

	public void setDataList(List dataList) {
		this.dataList = dataList;
	}
		

	/**
	 * @description Get Version of files
	 * @return String
	 * @throws Exception
	 */
	public String getVersionLimit() throws Exception{
		
		try{
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject = CDMRUtil.fetchEventLibId((JSONObject)jsonObject);
			String query = "SELECT * FROM (select PK_CDM_VERSIONDEF, VERSION_ID, VERSION_DESC from CDM_VERSIONDEF WHERE FILE_ID ='" + jsonObject.getString("EVENT_ID") + "'ORDER BY PK_CDM_VERSIONDEF DESC) VERDEF WHERE rownum <= 10";//CDMRSqlConstants.GET_VERSION_LIMIT + "10";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getCDMVersionDetail(jsonObject);
			List<Map> listObj = (List<Map>)jsonObject.get("versioninfo");
			for (Map map : listObj) {
				Map dataMap = new HashMap();
				dataMap.put("VERSION_ID", map.get("VERSION_ID").toString());
				dataMap.put("VERSION_DESC", map.get("VERSION_DESC").toString());
				dataList.add(dataMap);
			}
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
		
}