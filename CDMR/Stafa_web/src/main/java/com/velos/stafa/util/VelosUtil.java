package com.velos.stafa.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.business.util.Utilities;
import com.velos.stafa.controller.CodeListController;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.objects.UserDetailsObject;

public class VelosUtil {
	public static final Log log=LogFactory.getLog(VelosUtil.class);
	public static int TARGETLIST_STARTROWNUM = 0;
	public static int TARGETLIST_STARTCOLUMN = 0;
	
	public static Double toNumeric (String strParam){
		try{
			return Double.parseDouble(strParam);
		}catch(Exception e){
			return 0.0;
		}
		
		
	}
	public static boolean isNumeric (String strParam){
		try{
			Double d = Double.parseDouble(strParam);
		}catch(Exception e){
			return false;
		}
		
		return true;
	}
	public static ArrayList csvFileProcess(File file) {
		ArrayList arrlist = new ArrayList();
		try {
			  String text="";
			  String type="";
			  String searchString = "THERMO FORMA CRF Run Data Export";
			  String searchHeader = "Elapsed Time";
			 BufferedReader buf = new BufferedReader(new FileReader(file));
			 boolean typeFlag = false;
			 boolean isFreezer1 = false;
			 boolean isFreezer2 = false;
			 boolean freezerDatafound = false;
			
			 StringBuffer sb1 = new StringBuffer();
			 StringBuffer sb2 = new StringBuffer();
			 StringBuffer sb3 = new StringBuffer();
			 
			 while ((text=buf.readLine()) != null) {
                       text=text.trim();
                     //  System.out.println( "text " + text);
                       if (text.length()==0){ // empty line
                    	   continue;
                       }
                       String[] split = text.split(",");
                       if(split.length ==0){
                    	   continue;
                       }
                       type=split[0];
                       if(!typeFlag){
                    	   typeFlag = true;
                          if((type.toUpperCase()).contains(searchString)){
	                    	   // freezer 2
	                    	   isFreezer2 = true;
	                       }else{
	                    	   // freezer 1
	                    	   isFreezer1 = true;
	                       }
                       }
                       if(isFreezer2){
                    	   // freezer2 process
                    	   if(!freezerDatafound){
                    		   if((type.toUpperCase()).contains(searchHeader)){
                    			   freezerDatafound = true;
                    		   }
                    		   continue;
                    	   }
                       }else{
                    	   // freezer 1 process
                    	   freezerDatafound = true;
                       }   
                       if(freezerDatafound){
                    	  
                    	   if(split.length> 1 && VelosUtil.isNumeric(split[0]) & VelosUtil.isNumeric(split[1]) ){
                    		   for(int l=0;l<split.length;l++){
                    			   if (l==1 ){
                    				  sb1.append("[" + split[0]+"," + split[1] +"],");
                    			   }else if (l==2 ){
                    				  sb2.append("[" + split[0]+"," + split[2] +"],");
                    			   }else if (l==3 ){
                    				  sb3.append("[" + split[0]+"," + split[3] +"],");
                    			   }
                    		   }
                    	   }
                       }
			 }
			 if(sb1.length() > 1){
				 arrlist.add(0, sb1.subSequence(0, sb1.length()-1));
			 }
			 if(sb2.length() > 1){
				 arrlist.add(1, sb2.subSequence(0, sb2.length()-1));
			 }
			 if(sb3.length() > 1){
				 arrlist.add(2, sb3.subSequence(0, sb3.length()-1));
			 }
		}catch(Exception e){
			e.printStackTrace();
		}
		return arrlist;
	}
	
	
	
	/*public static ArrayList<String[]> xlsFileProcess(InputStream input) {
		ArrayList<String[]> arrlist = new ArrayList<String[]>();
		try {
			System.out.println("file process ============== " + input);
			POIFSFileSystem fs = new POIFSFileSystem(input); // Suppose if you want to read xls file we have to remove these two line command.
																// And Here we have to change all HSSF instead of XSSF.
		   														// And command that XSSFWorkbook line.
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			
			//XSSFWorkbook  wb1 = new XSSFWorkbook(input); // for this line xlsx file reading.
			
			int noOfSheet = wb.getNumberOfSheets();
			noOfSheet = 1;
			String cellValue;
			String[] cellValues;
			for (int k = 0; k < noOfSheet; k++) {
				HSSFSheet sheet = wb.getSheetAt(k); //Here we have to change XSSF instead of HSSF.
				if (sheet != null && sheet.getPhysicalNumberOfRows() > 1) {

					for (int j = TARGETLIST_STARTROWNUM; j < sheet.getPhysicalNumberOfRows(); j++) {
						HSSFRow row = sheet.getRow(j);  //Here we have to change XSSF instead of HSSF.

						if (row != null && row.getPhysicalNumberOfCells() > 0) {
							cellValues = new String[row.getLastCellNum()];

							for (int i = TARGETLIST_STARTCOLUMN; i < row.getLastCellNum(); i++) {
								HSSFCell cell = row.getCell(new Integer(i).shortValue());  //Here we have to change XSSF instead of HSSF.
								
								if (cell != null) {

									switch (cell.getCellType()) {
									case HSSFCell.CELL_TYPE_NUMERIC: { //Here we have to change XSSF instead of HSSF. This is optional.

											cellValue = String.valueOf(new Double(cell.getNumericCellValue()).doubleValue());
											cellValues[i] = cellValue;
										
										break;
									}
									case HSSFCell.CELL_TYPE_STRING: { //Here we have to change XSSF instead of HSSF. This is optional.
										cellValue = cell.getStringCellValue();
										if (cellValue != null) {
											cellValues[i] = cellValue.replaceAll("'", "\b");
										}
										break;
									}
									default:
										break;
									}// end of switch
								}// end of if to check if the cell is null
							}// end of for loop of lastcell num
							arrlist.add(cellValues);
						}// end of if to check row is null
					}// end for loop of lastRownum
				}// end of if loop.
			}
		} catch (Exception ex) {
			System.out.println(" Exception in fileProcess ::" + ex);
			ex.printStackTrace();
		}
		//System.out.println("arrlist : "+arrlist);
		return arrlist;
	}*/
	
	
	public static ArrayList<String[]> xlsxFileProcess(InputStream input) {
		ArrayList<String[]> arrlist = new ArrayList<String[]>();
		try {
			System.out.println("file process ============== " + input);
			/*POIFSFileSystem fs = new POIFSFileSystem(input); // Suppose if you want to read xls file we have to remove these two line command.
																// And Here we have to change all HSSF instead of XSSF.
		   														// And command that XSSFWorkbook line.
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			*/
			XSSFWorkbook  wb = new XSSFWorkbook(input); // for this line xlsx file reading.
			
			int noOfSheet = wb.getNumberOfSheets();
			noOfSheet = 1;
			String cellValue;
			String[] cellValues;
			for (int k = 0; k < noOfSheet; k++) {
				XSSFSheet sheet = wb.getSheetAt(k); //Here we have to change XSSF instead of HSSF.
				if (sheet != null && sheet.getPhysicalNumberOfRows() > 1) {

					for (int j = TARGETLIST_STARTROWNUM; j < sheet.getPhysicalNumberOfRows(); j++) {
						XSSFRow row = sheet.getRow(j);  //Here we have to change XSSF instead of HSSF.

						if (row != null && row.getPhysicalNumberOfCells() > 0) {
							cellValues = new String[row.getLastCellNum()];

							for (int i = TARGETLIST_STARTCOLUMN; i < row.getLastCellNum(); i++) {
								XSSFCell cell = row.getCell(new Integer(i).shortValue());  //Here we have to change XSSF instead of HSSF.
								
								if (cell != null) {

									switch (cell.getCellType()) {
									case XSSFCell.CELL_TYPE_NUMERIC: { //Here we have to change XSSF instead of HSSF. This is optional.

											cellValue = String.valueOf(new Double(cell.getNumericCellValue()).doubleValue());
											cellValues[i] = cellValue;
										
										break;
									}
									case XSSFCell.CELL_TYPE_STRING: { //Here we have to change XSSF instead of HSSF. This is optional.
										cellValue = cell.getStringCellValue();
										if (cellValue != null) {
											cellValues[i] = cellValue.replaceAll("'", "\b");
										}
										break;
									}
									default:
										break;
									}// end of switch
								}// end of if to check if the cell is null
							}// end of for loop of lastcell num
							arrlist.add(cellValues);
						}// end of if to check row is null
					}// end for loop of lastRownum
				}// end of if loop.
			}
		} catch (Exception ex) {
			System.out.println(" Exception in fileProcess ::" + ex);
			ex.printStackTrace();
		}
		//System.out.println("arrlist : "+arrlist);
		return arrlist;
	}
	
	
	public static JSONObject constructJSON(String jsonString){
		System.out.println("VelosUtil : constructJSON Method Start ");
		 JSONObject valueObject = new JSONObject();
		 int index;
		 try{
			
			/* while(paramNames.hasMoreElements()) {
				String paramName = (String)paramNames.nextElement();
				String[] paramValues = request.getParameterValues(paramName);
				//Check ParamName
				index = paramName.indexOf(".");
				if(index>0){
					paramName = paramName.substring(index+1);
				}
				valueObject.put(paramName,paramValues[0]);
			}
			 
			*/
			
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 System.out.println("VelosUtil : constructJSON Method End ");
		 return valueObject;
	 }
	
//	public static String callWorkflow(String entitytype,String entityId,Long userId)throws Exception{
//		System.out.println("VelosUtil : callWorkflow Method Start");
//		 String forwardAction ="success";
//		  List lstResult = new ArrayList();
//          CodeListController codeListController = new CodeListController();
//          CommonController commonController = new CommonController();
//          try{
//        	  //Get Workflow Flag
//        	 String workflowflag = codeListController.getCodelstDesc(VelosStafaConstants.CODELST_TYPE_WORKFLOW, VelosStafaConstants.CODELST_SUBTYPE_WORKFLOW);
//	          if(workflowflag != null && workflowflag.trim().equalsIgnoreCase("true")){
//	        	  System.out.println(":entityId "+entityId +"/" + entitytype );
//	        	  lstResult = commonController.fetchNextTask(entityId, entitytype, userId);
//	                if(lstResult != null && lstResult.size() >0 && ((String[])lstResult.get(0))[0]!=null){
//	                	System.out.println("lstResult : "+lstResult);
//	                    forwardAction = ((String[])lstResult.get(0))[0];
//	                    System.out.println("Util "+forwardAction);
//	                    return forwardAction;
//	                }
//	          }   
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		System.out.println("VelosUtil : callWorkflow Method End");
//		return forwardAction;
//	}
	public static JSONObject constructJSON(HttpServletRequest request){
		System.out.println("VelosUtil : constructJSON Method Start ");
		 JSONObject valueObject = new JSONObject();
		 int index;
		 try{
			 Enumeration paramNames = request.getParameterNames();
			 while(paramNames.hasMoreElements()) {
				String paramName = (String)paramNames.nextElement();
				System.out.println(" param name " + paramName);
				String[] paramValues = request.getParameterValues(paramName);
				//Check ParamName
				index = paramName.indexOf(".");
				if(index>0){
					paramName = paramName.substring(index+1);
				}
				System.out.println( " values " + paramValues[0]);
				valueObject.put(paramName,paramValues[0]);
			}
			 
			 valueObject = setUserDetails(valueObject,request);
			
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 System.out.println("VelosUtil : constructJSON Method End ");
		 System.out.println("$$$$$$$$$$" + valueObject + "%%%%%%%%%%");
		 return valueObject;
	 }
	
	public static JSONObject setUserDetails(JSONObject jsonObject,HttpServletRequest request)throws Exception{
		try{
			jsonObject.put("organizationId", "0");
			HttpSession session=request.getSession(false);
			UserDetailsObject userObject = null;
			if (session!=null){
				userObject = (UserDetailsObject) session.getAttribute(VelosStafaConstants.USER_OBJECT);
			}
			if(userObject != null && userObject.getUserId() !=null){
				jsonObject.put("user", userObject.getUserId());
				jsonObject.put("createdBy", userObject.getUserId());
				jsonObject.put("lastModifiedBy", userObject.getUserId());
			}else{
				jsonObject.put("createdBy", "1");
				jsonObject.put("lastModifiedBy", "1");
			}
			jsonObject.put("createdOn", Utilities.getFormattedDate(Utilities.getCurrentDate()));
			jsonObject.put("lastModifiedOn", Utilities.getFormattedDate(Utilities.getCurrentDate()));
			jsonObject.put("ipAddress", request.getRemoteAddr());
			System.out.println("request.getParameter('deletedFlag')==>"+request.getParameter("deletedFlag"));
			if(request.getParameter("deletedFlag")!=null && !request.getParameter("deletedFlag").equals("0")){
				System.out.println("--------------------------request.getParameter('deletedFlag')==>"+request.getParameter("deletedFlag"));
				jsonObject.put("deletedFlag", request.getParameter("deletedFlag"));
			}else{
				System.out.println("-------------1-------------request.getParameter('deletedFlag')==>"+request.getParameter("deletedFlag"));
				jsonObject.put("deletedFlag", "0");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonObject;
	}
	public static String getCharacter(String checkCharStr)throws Exception {
		System.out.println("VelosUtil : getCharacter : Starts :- "+checkCharStr);
		String checkChar ="";
		try {
			if(checkCharStr!=null && !checkCharStr.equalsIgnoreCase("")){
				Map<String , String> iso7064Table = new HashMap<String, String>();
				iso7064Table.put("0","0");
				iso7064Table.put("1","1");
				iso7064Table.put("2","2");
				iso7064Table.put("3","3");
				iso7064Table.put("4","4");
				iso7064Table.put("5","5");
				iso7064Table.put("6","6");
				iso7064Table.put("7","7");
				iso7064Table.put("8","8");
				iso7064Table.put("9","9");
				iso7064Table.put("A","10");
				iso7064Table.put("B","11");
				iso7064Table.put("C","12");
				iso7064Table.put("D","13");
				iso7064Table.put("E","14");
				iso7064Table.put("F","15");
				iso7064Table.put("G","16");
				iso7064Table.put("H","17");
				iso7064Table.put("I","18");
				iso7064Table.put("J","19");
				iso7064Table.put("K","20");
				iso7064Table.put("L","21");
				iso7064Table.put("M","22");
				iso7064Table.put("N","23");
				iso7064Table.put("O","24");
				iso7064Table.put("P","25");
				iso7064Table.put("Q","26");
				iso7064Table.put("R","27");
				iso7064Table.put("S","28");
				iso7064Table.put("T","29");
				iso7064Table.put("U","30");
				iso7064Table.put("V","31");
				iso7064Table.put("W","32");
				iso7064Table.put("X","33");
				iso7064Table.put("Y","34");
				iso7064Table.put("Z","35");
				iso7064Table.put("*","36");
				
				System.out.println("Starts");
				
				//String din = "G123412654321";
				String din = checkCharStr;
				int dinLen = din.length();
				//System.out.println("dinLen : "+dinLen);
				int tot=0;
				int tot1=0;
				for (int i=dinLen,j=0; i>=1; j++,i--) {
					//System.out.println("i : "+i);
					//System.out.println("j : "+j);
					String dinStr = din.charAt(j)+"";
					int isoVal = Integer.parseInt(iso7064Table.get(dinStr));
					//System.out.println(iso7064Table.get(dinStr));
					tot1=powerN(2,i)*isoVal;
					tot+=powerN(2,i)*isoVal;
					System.out.println(i +"\t"+ (powerN(2,i)) +" \t"+dinStr +"\t"+isoVal +"\t"+tot1);
					
				}
				System.out.println("Step 3 : tot : "+tot);
				System.out.println("Step 4 : : "+tot%37);
				int sub = (38)-(tot%37);
				System.out.println("Step 5 : : "+sub);
				System.out.println("Step 6 : : "+sub%37);
				String val = sub%37+"";
				
				//System.out.println("Step 6 : : "+iso7064Table.);
				
				checkChar = getKeyByValue(iso7064Table,val);
				
				System.out.println("Ends "+checkChar);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("VelosUtil : getCharacter : Ends ");
		return checkChar;
		}
		
		public static int powerN(int base, int n) {
		    int result = 1;
		    for (int i = 0; i < n; i++) {
		        result *= base;
		    }

		    return result;
		}

		public static String getKeyByValue(Map<String, String> map,String val){
			for (Entry entry : map.entrySet()) {
		        if (val.equals(entry.getValue())) {
		        	return entry.getKey()+"";
		        }
		    }
			return null;
			
		}
		
}