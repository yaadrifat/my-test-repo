package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRSqlConstants;



/**
 * @summary     AttachServiceSets
 * @description Attach Service Set to the line items
 * @version     1.0
 * @file        AttachServiceSets.java
 * @author      Lalit Chattar
 */
public class DeleteNotification extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	
	public DeleteNotification(){
		cdmrController = new CDMRController();
	}
	
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return deleteNotification();
	}
	
	public String deleteNotification() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String query = "UPDATE CDM_NOTIFICATION SET DELETEDFLAG = 1 where PK_CDM_NOTIFICATION = " + jsonObj.getString("pkID");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.deleteNotification(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
}
