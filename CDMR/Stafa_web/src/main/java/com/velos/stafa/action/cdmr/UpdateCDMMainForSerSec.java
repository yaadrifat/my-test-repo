package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LoadTreeGroupViewAction
 * @description Load Tree View in Charge Library
 * @version     1.0
 * @file        LoadTreeGroupViewAction.java
 * @author      Lalit Chattar
 */
public class UpdateCDMMainForSerSec extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 private boolean checkForExistingName;

	/*
	 * Getter and Setter

	*/
	
	 public boolean isCheckForExistingName() {
			return checkForExistingName;
	}
	public void setCheckForExistingName(boolean checkForExistingName) {
		this.checkForExistingName = checkForExistingName;
	}
	
	
	public UpdateCDMMainForSerSec(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String updateCDMMainForSerSec() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String sersecname = jsonObj.getString("sersecname");
			String deletedPk = jsonObj.getString("deletedPk");
			String sersecId = jsonObj.getString("sersecId");
			String changeFlagSec = jsonObj.getString("changeFlagSec");
			if(changeFlagSec.equals("true")){
				if(!this.checkForExistingName(sersecname)){
					setCheckForExistingName(false);
					return SUCCESS;
				}else{
					setCheckForExistingName(true);
					this.updateCDMGrpCode(sersecname, sersecId, jsonObj.getInt("user"));
					this.updateCDMain(deletedPk, jsonObj.getInt("user"));
				}
			}else{
				setCheckForExistingName(true);
				this.updateCDMGrpCode(sersecname, sersecId, jsonObj.getInt("user"));
				this.updateCDMain(deletedPk, jsonObj.getInt("user"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return SUCCESS;
	}
	
	public void updateCDMGrpCode(String sersecname, String sersecId, int user) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "update CDM_GRPCODE set GRP_NAME = '" + sersecname +"', GRP_DISP_NAME = '" +sersecname+ "', LAST_MODIFIED_BY = "+user+", LAST_MODIFIED_DATE =  TO_TIMESTAMP('"+CDMRUtil.getCurrentTimeStamp()+"', 'DD-MON-RR HH.MI.SS') where PK_CDM_GRPCODE = " + sersecId;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.editServiceSection(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateCDMain(String deletedPk, int user) throws Exception{
		try {
			if(deletedPk.equalsIgnoreCase(""))
				return;
			JSONObject jsonObj = new JSONObject();
			String pkId = deletedPk.substring(0, (deletedPk.length()-1));
			String query = "update CDM_MAIN set FK_CDM_GRPCODE = null, LAST_MODIFIED_BY = "+user+", LAST_MODIFIED_DATE =  TO_TIMESTAMP('"+CDMRUtil.getCurrentTimeStamp()+"', 'DD-MON-RR HH.MI.SS') where PK_CDM_MAIN in ("+pkId+")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.editServiceSection(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkForExistingName(String sersecname) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "select * from CDM_GRPCODE where UPPER(GRP_NAME) = '"+sersecname.toUpperCase()+"' and UPPER(GRP_DISP_NAME) = '"+sersecname.toUpperCase()+"' and DELETEDFLAG = 0" ;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			List<Map> dataMap = (List<Map>)jsonObj.get("cdmmain");
			if(dataMap.size() > 0){
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
}
