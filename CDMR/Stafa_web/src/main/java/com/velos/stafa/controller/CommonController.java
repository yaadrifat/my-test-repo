package com.velos.stafa.controller;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.w3c.dom.Node;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.velos.stafa.helper.VelosHelper;
/*import com.velos.stafa.pojo.EmploymentInfoDetailsObject;
import com.velos.stafa.pojo.PersonFundCaseManagementObjects;
import com.velos.stafa.pojo.RecipientInsViewDetailsObject;*/
import com.velos.stafa.service.VelosService;
import com.velos.stafa.service.impl.VelosServiceImpl;
import com.velos.stafa.util.XMLUtil;

public class CommonController {

	VelosService service;
	public static JSONObject saveMultipleBaseAndSubObjects(JSONObject jsonObject) throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveMultipleBaseAndSubObjects(jsonObject);
	}
	
	public static JSONObject saveReferenceSubObjects(JSONObject jsonObject) throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveReferenceSubObjects(jsonObject);
	}
	
	public static int findCount(String objectName,String property,String value)throws Exception{
		VelosService service = VelosServiceImpl.getService();
		return service.findCount(objectName, property, value);
	}
	
//	public static List fetchNextOptions(String module)throws Exception{
//		
//		JSONArray jsonArray = new JSONArray();
//		JSONObject jsonObject = new JSONObject();
//		String criteria = VelosStafaSqlConstants.SQL_NEXTOPTION.replaceAll(VelosStafaSqlConstants.FIELD_FLOW_PARENTCODE, module);
//		jsonObject.put("Query", criteria);
//		jsonObject.put("type", "SQL");
//		jsonArray.put(0,jsonObject);
//		List resultList = CommonController.getObjectList(jsonArray);
//		if(resultList != null && resultList.size()>0){
//			return (List)resultList.get(0);
//		}else{
//			return null ;
//		}
//	}
	
	// generic method to save json values
	// new method
	public static JSONObject saveJSONObjects (JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveJSONObjects(jsonObject);
	}
	
	public static JSONObject saveJSONObject (JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveJSONObject(jsonObject);
	}
	
	public static JSONObject saveJSONData (JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveJSONData(jsonObject);
	}
	
	public static JSONObject saveJSONDataObject(JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveJSONDataObject(jsonObject);
	}
	
	public static JSONObject executeSQLQueries(JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.executeSQLQueries(jsonObject);
	}
	public static String userAuthentication(String userName,String password,String authenticationType,String addtionalinfo ){
		System.out.println("common controller . userAuthentication");
		String userAuthentication = null;
		
		/*
		XMLUtil xmlUtil = new XMLUtil();
		//List lstUsers = 
		//Node userDetails=xmlUtil.getXmlValues("UserDetails.xml","users","loginname",userName);
		Node userDetails=xmlUtil.getNode("UserDetails.xml","user","loginname",userName);
		//xmlUtil.printNodes(qcNode);
		
		if (userDetails != null){
			userAuthentication = xmlUtil.xmlToString(userDetails);
			
		}else{
			userAuthentication = "ERROR";
		}
		*/
		System.out.println("userAuthentication " + userAuthentication);
		return userAuthentication;
	}
	
	public static JSONObject saveObject(JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveObject(jsonObject);
	}
	
	public static JSONArray saveObject(JSONArray jsonArray)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.saveObject(jsonArray);
	}
	
	public static List getObjectList(JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.getObjectList(jsonObject);
	}
	
	public static List getObjectList(JSONArray jsonArray)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.getObjectList(jsonArray);
	}
	
	public List fetchNextTask(String entityId, String entityType, Long userId)throws Exception{
		service = VelosServiceImpl.getService();
		return service.fetchNextTask(entityId, entityType, userId);
	}
//	public HashMap getCodeListValues(HashMap codeLstMap)throws Exception{
//		service = VelosServiceImpl.getService();
//		return service.getCodeListValues(codeLstMap);
//	}
	public static JSONObject getObjectListByCriteria(JSONObject jsonObject)throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.getObjectListByCriteria(jsonObject);
	}
//	public static JSONObject updateQueryByCriteria(JSONObject jsonObject)throws Exception{
//		VelosService service = VelosServiceImpl.getService();		
//		return service.updateQueryByCriteria(jsonObject);
//	}
	
	public static Object getObject(String objectName, Long id) throws Exception {
		VelosService service = VelosServiceImpl.getService();		
		return service.getObject(objectName, id);
	}
	
	public static List getDataFromSQLQuery(String mainQuery) throws Exception{
		VelosService service = VelosServiceImpl.getService();		
		return service.getDataFromSQLQuery(mainQuery);
	}

	/*public static void saveInsEmploymentInfo(
			EmploymentInfoDetailsObject insEmploymentDetails) throws Exception {
		VelosService service = VelosServiceImpl.getService();	
		service.saveInsEmploymentInfo(insEmploymentDetails);
	}

	public static void saveCompleteInsuranceInfo(
			RecipientInsViewDetailsObject recipientInsViewDetails, EmploymentInfoDetailsObject employmentDetails) throws Exception {
		VelosService service = VelosServiceImpl.getService();	
		service.saveCompleteInsuranceInfo(recipientInsViewDetails,employmentDetails);
		
	}	*/
	
	public static List<Object> getPojoList(String domainName, Object pojoObject,
			String criteria, String type) throws Exception {		
		VelosService service = VelosServiceImpl.getService();	
		return service.getPojoList(domainName, pojoObject, criteria, type);
	}	
	public static void saveDomainFromPojoList(Object domainObject, String keyId,
			List savePojoList) throws Exception {
		VelosService service = VelosServiceImpl.getService();	
		service.saveDomainFromPojoList(domainObject, keyId, savePojoList);		
	}

	/*public static void savePersonFundCaseManagement(
			PersonFundCaseManagementObjects personFundCaseMgmtDetails)throws Exception {
		   VelosService service=VelosServiceImpl.getService();
		   service.savePersonFundCaseManagement(personFundCaseMgmtDetails);
		
	}*/
}
