package com.velos.stafa.action.cdmr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;



/**
 * @summary     AttachServiceSets
 * @description Attach Service Set to the line items
 * @version     1.0
 * @file        AttachServiceSets.java
 * @author      Lalit Chattar
 */
public class AttachServiceSets extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	private boolean checkExisting;
	
	
	public boolean isCheckExisting() {
		return checkExisting;
	}

	public void setCheckExisting(boolean checkExisting) {
		this.checkExisting = checkExisting;
	}
	
	public AttachServiceSets(){
		cdmrController = new CDMRController();
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return attachServiceSets();
	}
	
	/**
	 * @description Attach service set to the line items
	 * @return String
	 * @throws Exception
	 */
	public String attachServiceSets()throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			jsonObject = CDMRUtil.fetchEventLibId(jsonObject);
			String pkIds = jsonObject.getString("ids").substring(0, (jsonObject.getString("ids").length())-1);
			jsonObject.put("ids", pkIds);
			jsonObject = this.serviceSetInfo(jsonObject);
			jsonObject = this.saveInSSlink(jsonObject);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	

	/**
	 * @description Get Serivice Set Info from CDM_MAIN
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject serviceSetInfo(JSONObject jsonObject) throws Exception{
		try {
			String query = "select LSERVICE_CODE from cdm_main where PK_CDM_MAIN = " + jsonObject.getString("serSetId");
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromCdmMain(jsonObject);
			List<Map> list = (List<Map>)jsonObject.get("cdmmain");
			Map mapObj = list.get(0);
			jsonObject.put("SS_LSERVICE_CODE", mapObj.get("LSERVICE_CODE"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	/**
	 * @description Save Service Set Line in CDM Main
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject saveInSSlink(JSONObject jsonObject) throws Exception{
		try {
			String[] pkids = jsonObject.getString("ids").split(",");
			for (int i = 0; i < pkids.length; i++) {
				String query = "select LSERVICE_CODE, IS_THIS_SS from CDM_MAIN where PK_CDM_MAIN in ("+pkids[i]+")";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getFromCdmMain(jsonObject);
				
				
				List<Map> listObj = (List<Map>)jsonObject.get("cdmmain");
				for (Map map : listObj) {
					jsonObject.put("ssID", jsonObject.getString("SS_LSERVICE_CODE"));
					jsonObject.put("ssChildId", map.get("LSERVICE_CODE").toString());
					jsonObject.put("isChildSS", map.get("IS_THIS_SS").toString());
					jsonObject.put("isInclude", 1);
					jsonObject.put("eventlibid", jsonObject.getString("EVENT_ID"));
					
					cdmrController.saveInSSLink(jsonObject);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	
	/**
	 * @description Check For existing item in Set
	 * @return JSONObject
	 * @throws Exception
	 */
	public String checkForExistingItems() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String pkIds = jsonObject.getString("ids").substring(0, (jsonObject.getString("ids").length())-1);
			jsonObject.put("ids", pkIds);
			String query = "select * from cdm_sslink where ss_id = (select LSERVICE_CODE from cdm_main where PK_CDM_MAIN = " + jsonObject.getString("serSetId") + ") and ss_child_id in (select LSERVICE_CODE from cdm_main where PK_CDM_MAIN = " + jsonObject.getString("ids") + ") and deletedflag = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromCdmMain(jsonObject);
			List<Map> listObj = (List<Map>)jsonObject.get("cdmmain");
			if(listObj.size() > 0){
				this.setCheckExisting(true);
			}else{
				this.setCheckExisting(false);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
}
