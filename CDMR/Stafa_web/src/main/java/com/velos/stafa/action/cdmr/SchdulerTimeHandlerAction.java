package com.velos.stafa.action.cdmr;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     GetSchdulerTime
 * @description Add And Update Group(Service Section) 
 * @version     1.0
 * @file        FileProcessorAction.java
 * @author      Ved Prakash
 */
public class SchdulerTimeHandlerAction extends StafaBaseAction{
	
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession = null;
	
	/*
	 * Parameter getting from request
	 */
	private String time;
	private String timeexport;
	/*
	 * Constructor
	 */
	public SchdulerTimeHandlerAction(){
		cdmrController = new CDMRController();
	}
	
	/*
	 * getter and setter
	 */
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	/*
	 * Default Method of Action class
	 */
	
	public String getTimeexport() {
		return timeexport;
	}

	public void setTimeexport(String timeexport) {
		this.timeexport = timeexport;
	}

	public String saveNewScheduleATime() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}
	
	
	
	public String getScheduledTime() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String time =  this.getValueFromDB();
			String[] elemArray = time.split(" ");
			if(elemArray[5].equals("*")){
				//time = "Current Scheduled Time : " + elemArray[2] + ":" + elemArray[1] + "(Daily)";
				time = "Current Import Schedule : Interface begins everyday at " + elemArray[2] + ":" + elemArray[1];
				
			}else{
				//time = "Current Scheduled Time : " +  elemArray[2] + ":" + elemArray[1] + " " + elemArray[5] + "(Weekly)";
				time = "Current Import Schedule : Interface begins every " +elemArray[5]+" at "+elemArray[2] + ":" + elemArray[1];
			}
			this.setTime(time);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}
	
	public String getValueFromDB() throws Exception{
    	try {
    		CDMRController cdmrController = new CDMRController();
			String query = "SELECT PARAM_VAL FROM VBAR_CONTROLPARAM WHERE PARAM_TYPE = 'SCHEDULER' and PARAM_SUBTYP = 'PST'";
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getSchedulerTime(jsonObj);
			List<Map> dataList = (List<Map>)jsonObj.get("scheduler");
			return dataList.get(0).get("PARAM_VAL").toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
    }
	
	
	public String getScheduledTimeExport() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String time =  this.getValueFromDBexport();
			String[] elemArray = time.split(" ");
			if(elemArray[5].equals("*")){
				
				time = "Current Export Schedule : Interface begins everyday at " + elemArray[2] + ":" + elemArray[1];
				
			}else{
				
				time = "Current Export Schedule : Interface begins every " +elemArray[5]+" at "+elemArray[2] + ":" + elemArray[1];
			}
			this.setTimeexport(time);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}
	public String getValueFromDBexport() throws Exception{
    	try {
    		JSONObject jsonObj = new JSONObject();
    		CDMRController cdmrController = new CDMRController();
			String query = "SELECT PARAM_VAL FROM VBAR_CONTROLPARAM WHERE PARAM_TYPE = 'EXPORT_SCHEDULER' and PARAM_SUBTYP = 'PST'";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getSchedulerTime(jsonObj);
			List<Map> dataList = (List<Map>)jsonObj.get("scheduler");
			if(dataList.size()>0){
			return dataList.get(0).get("PARAM_VAL").toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
    }
}