package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LoadTreeGroupViewAction
 * @description Load Tree View in Charge Library
 * @version     1.0
 * @file        LoadTreeGroupViewAction.java
 * @author      Lalit Chattar
 */
public class GetServiceSetName extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 
	private String serviceSetName;
	/*
	 * Getter and Setter

	*/
	
	public String getServiceSetName() {
		return serviceSetName;
	}


	public void setServiceSetName(String serviceSetName) {
		this.serviceSetName = serviceSetName;
	}
	
	public GetServiceSetName(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String getServiceSetNameMethod() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String query = "select SERVICE_DESC1 from CDM_MAIN where PK_CDM_MAIN = " + jsonObj.getString("sersetId");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("cdmmain");
			this.setServiceSetName(list.get(0).get("SERVICE_DESC1").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return SUCCESS;
	}

}
