package com.velos.stafa.action.cdmr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     GetStagingType
 * @description 
 * @version     1.0
 * @file        GetStagingType.java
 * @author      Lalit Chattar
 */
public class GetStagingType extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 private String fileID;
	 private String fileType;
	 private String moduleName;
	
		/*
	 * Getter and Setter

	*/
	 	public String getFileID() {
			return fileID;
		}
		public void setFileID(String fileID) {
			this.fileID = fileID;
		}
		public String getFileType() {
			return fileType;
		}

		public void setFileType(String fileType) {
			this.fileType = fileType;
		}
		
		public String getModuleName() {
			return moduleName;
		}
		public void setModuleName(String moduleName) {
			this.moduleName = moduleName;
		}
	 
	public GetStagingType(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * @description Get Type of Staging Files
	 * @return String
	 * @throws Exception
	 */
	public String getStagingType()throws Exception {
	
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			
			fileType = this.getFileId(jsonObj);
			jsonObj.put("file_id",fileType);
			moduleName = this.getStagingModule(jsonObj);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * @description Get File info from files lib
	 * @return String
	 * @throws Exception
	 */
	public String getFileInfo(JSONObject jsonObj)throws Exception {
		
		try {
			jsonObj.put("query", CDMRSqlConstants.PROCESSING_FILE_INFO + jsonObj.getString("fileID"));			
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMRFilesInfo(jsonObj);
			List<Map> info = (List<Map>)jsonObj.get("filesinfo");
			return info.get(0).get("FILE_NAME").toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @description Get Active map version related to file
	 * @return String
	 * @throws Exception
	 */
	public String getMapVersion(String fileid)throws Exception {
		
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query", CDMRSqlConstants.ACTIVE_MAP_VERSION + " and FILE_ID = '" + fileid + "'");			
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getActiveMapVesion(jsonObj);
			List<Map> info = (List<Map>)jsonObj.get("mapversion");
			return info.get(0).get("PK_CDM_MAPSVERSION").toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * @description Get File id from files lib
	 * @return String
	 * @throws Exception
	 */
	public String getFileId(JSONObject jsonObject) throws Exception {
		String fileid="";
		try {
			CDMRController cdmrController = new CDMRController();
			
			String query = "select FILE_ID from CDM_FILESLIB where PK_CDM_FILESLIB ='"
					+ jsonObject.getString("fileID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFileID(jsonObject);
			List<Map> fileName = (List<Map>) jsonObject.get("file_id");
			
			fileid = fileName.get(0).get("FILE_ID")
						.toString();


		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return fileid;

	}
	/**
	 * @description Get staging type from module list
	 * @return String
	 * @throws Exception
	 */
	public String getStagingModule(JSONObject jsonObject) throws Exception {
		String modname="";
		try {
			CDMRController cdmrController = new CDMRController();
			
			String query = "select MODULE_NAME from CDM_MODULE_LIST where FILE_ID ='"
					+ jsonObject.getString("file_id") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getModulename(jsonObject);
			List<Map> modulename = (List<Map>) jsonObject.get("module_name");
			
			modname = modulename.get(0).get("MODULE_NAME")
						.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modname;

	}
	
}
