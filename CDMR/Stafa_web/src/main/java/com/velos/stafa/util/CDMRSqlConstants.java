package com.velos.stafa.util;

public interface CDMRSqlConstants{
	
	public static final String LATEST_FILE_FETCH = "select * from (select c.pk_cdm_fileslib, c.import_status,c.comments, c.file_name, c.file_cur_status,  case when c.last_modified_date IS NULL THEN c.created_on ELSE c.last_modified_date END as revicedon , u.user_firstname,u.user_lastname from cdm_fileslib c,esec_stafa.sec_user u where u.pk_userid=(CASE WHEN c.last_modified_by IS NULL THEN c.creator ELSE c.last_modified_by END) order by c.last_modified_date desc ) where rownum < 6";
	//public static final String LATEST_FILE_FETCH = "select * from (select PK_CDM_FILESLIB, FILE_NAME,row_number() over(order by PK_CDM_FILESLIB desc ) rnm from cdm_fileslib) where rnm<6";
	public static final String PROCESSING_FILE_INFO = "select * from cdm_fileslib where pk_cdm_fileslib = ";
	public static final String FETCH_CODELIST_DATA = "from CodeList";
	public static final String MAX_STAGING_ID = "select max(STAGING_ID) as stagingid from cdm_stagingmain";
	public static final String ACTIVE_MAP_VERSION = "select * from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG = 0";
	public static final String GET_STAGING_MAPS = "from StagingMaps";
	public static final String GET_STAGING_PK = "from StagingMain where fkFilelib = ";
	public static final String GET_STAGING_DETAIL_DATA = "from StagingDetail where fkStagingMain = ";
	public static final String GET_STAGING_COLUMN_MAP = "from StagingMaps";
	public static final String STAGING_HEADER_FILE_INFO = "select c.pk_cdm_fileslib, c.file_name, c.file_cur_status,  case when c.last_modified_date IS NULL THEN c.created_on ELSE c.last_modified_date END as revicedon , u.user_firstname,u.user_lastname from cdm_fileslib c,";//esec_stafa.sec_user u where u.pk_userid=(CASE WHEN c.last_modified_by IS NULL THEN c.creator ELSE c.last_modified_by END) and  c.pk_cdm_fileslib = ";
	public static final String COUNT_RECORD  = "select count(*) as total from ";
	public static final String GET_GROUPS = "select * from CDM_GRPCODE where PARENT_NODE_ID =";
	public static final String GET_GROUPNAME_BY_CODE = "from GrpCode where pkGrpCode = ";
	public static final String COUNT_SEARCH_RECORD = "select count(*) as total from (SELECT t.*, Row_Number() OVER (ORDER BY PK_CDM_STAGINGCOMPREC) MyRow FROM CDM_STAGINGCOMPREC t where FK_CDM_STAGINGMAIN = ";
	public static final String SELECT_NEXT_POSSIBLE_STATUS = "select NXT_POSS_STATE from VBAR_STATEMACHINE INNER JOIN CDM_STAGINGMAIN ON CDM_STAGINGMAIN.FILE_CUR_STATUS = VBAR_STATEMACHINE.CUR_STATE AND CDM_STAGINGMAIN.FK_CDM_FILESLIB = ";
	public static final String GET_STAGING_MAIN_ID = "select PK_CDM_STAGINGMAIN from CDM_STAGINGMAIN where FK_CDM_FILESLIB = ";
	public static final String GET_GROUP_NAMES = "select PK_CDM_GRPCODE as id, GRP_NAME as value from CDM_GRPCODE";
	public static final String GET_GROUP_DISP_NAME = "select GRP_DISP_NAME from CDM_GRPCODE where PK_CDM_GRPCODE = ";
	public static final String GET_GROUP_PARENT_ID = "select PK_CDM_GRPCODE from CDM_GRPCODE where PARENT_NODE_ID in ";
	public static final String MAX_CDM_VERSION_ID = "select max(VERSION_ID) as versionid from cdm_versiondef";
	public static final String MAX_CDM_VERSION_NO = "select VERSION_NO as versionno from cdm_versiondef where VERSION_ID = (select max(VERSION_ID) as versionid from cdm_versiondef)";
	public static final String UPDATE_CDMR_VERSION_INFO = "update CDM_VERSIONDEF set IS_CURRENT = 0 where VERSION_ID not in ";
	public static final String GET_LATEST_FILE_ID = "select max(PK_CDM_FILESLIB) as maxid from cdm_fileslib";
	public static final String GET_SUPER_PARENT_GROUP = "select GRP_DISP_NAME, PK_CDM_GRPCODE from CDM_GRPCODE where PARENT_NODE_ID = 0 and DELETEDFLAG = 0";
	public static final String GET_TECHNICAL_CHARGES_QUERY = "select * from cdm_mapsversion where SHEET_NOS = 0 and DELETEDFLAG = 0 and IS_ACTIVE = 1 and FILE_ID =  'EIWTD'";
	public static final String GET_PROFESSIONAL_CHARGES_QUERY = "select * from cdm_mapsversion where SHEET_NOS = 0 and DELETEDFLAG = 0 and IS_ACTIVE = 1 and FILE_ID =  'EIWPD'";
	public static final String TRUNCATE_STAGING_TEMP = "truncate table cdm_staging_temp";
	public static final String SQL_BINDING_MAP = "select * from CDM_SQLBIND where FK_CDM_MAPSVERSION = ";
	public static final String GET_COLUMN_NAME = "select COMPCOL_NAME from CDM_STAGINGMAPS where DELETEDFLAG = 0 and DISP_INSTAGING = 1 and FK_CDM_MAPSVERSION = ";
	public static final String GET_STAGING_TYPE = "select FILE_ID from CDM_MAPSVERSION where PK_CDM_MAPSVERSION = ";
	public static final String GET_VERSION_LIMIT = "SELECT * FROM (select PK_CDM_VERSIONDEF, VERSION_ID, VERSION_DESC from CDM_VERSIONDEF ORDER BY PK_CDM_VERSIONDEF DESC) VERDEF WHERE rownum <= ";
	public static final String GET_SUPER_ADMIN = "select PARAM_SUBTYP, PARAM_VAL from VBAR_CONTROLPARAM where PARAM_TYPE = 'ADMIN' and PARAM_SUBTYP in('SUPER_ADMIN','ADMIN_EMAIL') and DELETEDFLAG=0";
	public static final String GET_DEFAULT_PASSWORD = "select PARAM_VAL from VBAR_CONTROLPARAM where PARAM_TYPE='RESET' and PARAM_SUBTYP='PASSWORD' and DELETEDFLAG=0";
	public static final String GET_USER_EMAIl = "select USER_EMAIL from CDM_NOTIFICATION where SENT_STATUS = 'N' and LOGIN_NAME = ";
	public static final String UPDATE_NOTIFICATION = "update CDM_NOTIFICATION set SENT_STATUS = 'Y' where SENT_STATUS = 'N' and LOGIN_NAME = ";
	public static final String GET_CHG_IND = "select CODELST_DESC from VBAR_CODELST where CODELST_TYPE = 'CHG_INDC' and CODELST_SUBTYP = ";
	
	/*//This code is used for MDA CDMR*/
	public static final String GET_CHG_COUNT_FLAG="select count(*) from CDM_MAIN c LEFT OUTER JOIN CDM_SSLINK s	ON  c.lservice_code = s.ss_child_id AND s.deletedflag = 0 LEFT OUTER JOIN cdm_main cm ON s.ss_id  =  cm.lservice_code AND cm.deletedflag = 0 Where c.deletedflag = 0 AND c.IS_THIS_SS = 0 " 
			+" and ((c.is_posted_ext = 0 or s.is_posted_ext = 0) and c.lservice_code = s.ss_child_id) AND c.PK_CDM_MAIN in (SELECT MAX(c_m.PK_CDM_MAIN) FROM cdm_main c_m WHERE (c_m.CUR_VERSION <= (SELECT version_id FROM cdm_versiondef WHERE is_current = 1)) GROUP BY c_m.LSERVICE_CODE,chg_ind) AND c.IS_ACTIVE_YN = 'Y'";
	
	//This code is used for UT CDMR
	public static final String GET_CHG_COUNT_DATE = "select count(*) from CDM_MAIN c LEFT OUTER JOIN CDM_SSLINK s ON c.lservice_code = s.ss_child_id AND s.deletedflag = 0 LEFT OUTER JOIN cdm_main cm ON s.ss_id  =  cm.lservice_code AND cm.deletedflag = 0 Where c.deletedflag = 0 AND c.IS_THIS_SS = 0 " 
	+" and (s.is_posted_ext = 0 and c.lservice_code = s.ss_child_id) " 
	+" AND (TO_DATE((TO_CHAR(c.EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')  <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')"
	+" AND ((TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')    > TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')"
	+" OR TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')      IS NULL)"
	+" OR (TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')     <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY'))"
	+" AND ((TO_DATE((TO_CHAR(c.EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') >= TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY'))"
	+" AND (TO_DATE((TO_CHAR(c.EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')  <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')))))";

	public static final String GET_CHG_COUNT = "select count(*) from CDM_MAIN c LEFT OUTER JOIN CDM_SSLINK s ON c.lservice_code = s.ss_child_id AND s.deletedflag = 0 LEFT OUTER JOIN cdm_main cm ON s.ss_id  =  cm.lservice_code AND cm.deletedflag = 0 Where c.deletedflag = 0 AND c.IS_THIS_SS = 0 " 
		+" and (s.is_posted_ext = 0 and c.lservice_code = s.ss_child_id) " 
		+" AND (TO_DATE((TO_CHAR(c.EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')  <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')"
		+" AND ((TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')    > TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')"
		+" OR TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')      IS NULL)"
		+" OR (TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')     <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY'))"
		+" AND ((TO_DATE((TO_CHAR(c.EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') >= TO_DATE((TO_CHAR(c.EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY'))"
		+" AND (TO_DATE((TO_CHAR(c.EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')  <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')))))";

			
	
	//public static final String GET_CHG_COUNT="select INT_SQL from CDM_OBIDEF";
}