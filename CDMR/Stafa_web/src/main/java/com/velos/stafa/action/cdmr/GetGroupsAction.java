package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRUtil;

/**
 * @summary     GetGroupsAction
 * @description Get All Parent Group(Service Section) for tree 
 * @version     1.0
 * @file       GetGroupsAction.java
 * @author      Lalit Chattar
 */
public class GetGroupsAction extends StafaBaseAction{
	
	private Integer parentNode;
	private List<List> groups;
	
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	public GetGroupsAction(){
		cdmrController = new CDMRController();
		groups = new ArrayList<List>();
	}
	
	public Integer getParentNode() {
		return parentNode;
	}

	public void setParentNode(Integer parentNode) {
		this.parentNode = parentNode;
	}
	
	public List<List> getGroups() {
		return groups;
	}

	public void setGroups(List<List> groups) {
		this.groups = groups;
	}
	
	/**
	 * @description Get All Parent Group(Service Section) Name
	 * @return String
	 * @throws Exception
	 */
	public String getGroupsName() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
			String query = CDMRSqlConstants.GET_GROUPS + jsonObj.getString("parentNode") + " and DELETEDFLAG = 0 and event_lib_id = "+jsonObj.getString("EVENT_ID");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getGroups(jsonObj);
			
			List<Map> dataMap = (List<Map>)jsonObj.get("groupcode");
			for (Map map : dataMap) {
				List<String> data = new ArrayList<String>();
				data.add(map.get("PK_CDM_GRPCODE").toString());
				data.add(map.get("PK_CDM_GRPCODE").toString());
				data.add(map.get("GRP_DISP_NAME").toString());
				groups.add(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	
}