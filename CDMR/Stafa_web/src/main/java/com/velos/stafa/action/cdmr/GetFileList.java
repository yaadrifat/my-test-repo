package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VFISNativeConnect;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRSqlConstants;



/**
 * @summary     AttachServiceSets
 * @description Attach Service Set to the line items
 * @version     1.0
 * @file        AttachServiceSets.java
 * @author      Lalit Chattar
 */
public class GetFileList extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	private List<Map> statusList;
	private List check;
	private String transferstate;
	//private String export_count;
	private int export_count;
	public GetFileList(){
		cdmrController = new CDMRController();
		statusList= new ArrayList<Map>();
		check= new ArrayList();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public List getStatusList() {
		return statusList;
	}

	public void setStatusList(List<Map> statusList) {
		this.statusList = statusList;
	}

	
	public String getTransferstate() {
		return transferstate;
	}


	public void setTransferstate(String transferstate) {
		this.transferstate = transferstate;
	}


	public int getExport_count() {
		return export_count;
	}


	public void setExport_count(int exportCount) {
		export_count = exportCount;
	}


	public String getFileList() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			this.getFileStatus(codelist);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	
	public void getFileStatus(List<CodeList> codeList) throws Exception{
		for (CodeList codeListObj : codeList) {
			if(codeListObj.getCodeListType().equals("filestatus") && codeListObj.getCodeListDefaultValue() == 1){
				if(check.contains(codeListObj.getPkCodeList())){
					continue;
				}
				Map map = new HashMap();
				map.put("desc", codeListObj.getCodeListDesc());
				map.put("pkID", codeListObj.getPkCodeList());
				check.add(codeListObj.getPkCodeList());
				statusList.add(map);
			}
		}
	}
	public String getexportStatus()throws Exception{
		try{
			
			JSONObject jsonObject = new JSONObject();
			ProcessandExportData exportData = new ProcessandExportData();
			jsonObject=exportData.getTransferStatusData();
			List<Map> codeList = (List<Map>) jsonObject.get("codelist");
			StringBuffer buffer = new StringBuffer();
			for (Map map : codeList) {
				buffer.append("<option value=\""+map.get("PK_CODELST").toString()+ "\">"+map.get("CODELST_DESC").toString()+"</option>");
			}
			this.setTransferstate(buffer.toString());
			
		}
		catch (Exception e) {
		e.printStackTrace();
		}
	return "SUCCESS";
	}
	public String getcountToexport() throws Exception{
		try{
					
			ProcessandExportData exportData = new ProcessandExportData();
			export_count=exportData.getcountToexport();
		}
	 catch (Exception e) {
		e.printStackTrace();
	}
	return "SUCCESS";
		
	}	
	
}
