package com.velos.stafa.action.cdmr;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.expr.NewArray;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.struts2.ServletActionContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;
import com.velos.stafa.business.util.WebserviceUtil;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.objects.UserDetailsObject;
import com.velos.stafa.util.TaylorServices;
import com.velos.stafa.util.VelosStafaConstants;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.eSecurity.ESecurityConstants;
import com.velos.stafa.action.StafaBaseAction;
public class NotesAction extends StafaBaseAction{
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	private String notesDetails;
	 private String entityId;
	 private String entityType;
	 private String resultFlag;
	 private String createdBy;
	 private static String usrNoteID;
	 
	 ArrayList notesList = new ArrayList(); 
	 // private String noteList;
	 //	ArrayList<StringBuffer> noteList = new ArrayList<StringBuffer>();
	public String loadNotesPage() throws Exception{
		
		
		fetchNotes();	
		return SUCCESS;
	}
	
	public String savingNotes() throws Exception {
		
		JSONObject jsonObj = null;
		String userName="";
		String createdLoginName="";
		try {
			resultFlag=request.getParameter("falg");
			
			jsonObj = VelosUtil.constructJSON(request);
			String tempNoteDescription = jsonObj.get("dialogEditor").toString();
			
			String noteDescirption=tempNoteDescription.replaceAll("<", "&lt;");
			noteDescirption=noteDescirption.replaceAll(">", "&gt;");
			noteDescirption=noteDescirption.replace("&nbsp;","");
			HttpSession session=request.getSession(false);
			UserDetailsObject userObject = null;
			if (session!=null){
			userObject = (UserDetailsObject) session.getAttribute(VelosStafaConstants.USER_OBJECT);
			String tempUserName=userObject.getFirstName();
			
			String tempLastName=userObject.getLastName();
			
			userName=tempLastName+" "+tempUserName;
			createdLoginName=userObject.getLoginName();
			
			}
		
			String entityId=jsonObj.get("entityId").toString();
			String entityType=jsonObj.get("entityType").toString();
			
			String noteName=jsonObj.get("noteName").toString();
			TaylorServices taylorServices = new TaylorServices();
			StringBuffer sb = new StringBuffer();
			sb.append("<ser:saveNote>");
			sb.append("<note> <applnCode>STAFA</applnCode>");
			sb.append("<notedescription>"+noteDescirption+"</notedescription>");
			sb.append(" <entityId>"+entityId+"</entityId>");
			sb.append("<displayUserName>"+userName+"</displayUserName>");
			sb.append(" <entityType>"+entityType+"</entityType>");
			sb.append(" <notesName>"+noteName+"</notesName>");
			sb.append(" <createUserName>"+createdLoginName+"</createUserName>");
			sb.append("</note></ser:saveNote>"); 
			StringBuffer serviceResult = taylorServices.savingNotes(sb.toString());
			
			if(serviceResult!=null){
				notesDetails = serviceResult.toString();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(usrNoteID.compareToIgnoreCase("USER")==0)
		{ 			
			fetchNotesUser();
			
			
		}
		else
		{			
			fetchNotes();	
		}
		return SUCCESS;
	}
	
	public String deleteNoteData() throws Exception {
		
		//JSONObject jsonObj = null;
		//String userName="";
		try {
			
			//jsonObj = VelosUtil.constructJSON(request);
			
			//String uniqueId = request.getParameter("param");
			String jsonData = request.getParameter("jsonData");
			JSONObject jsonObject = new JSONObject(jsonData);
			String uniqueId= jsonObject.get("data").toString();
			
			TaylorServices taylorServices = new TaylorServices();
			StringBuffer sb = new StringBuffer();
			sb.append("<ser:deleteNote>");
			sb.append("<note> <noteUniqueId>"+uniqueId+"</noteUniqueId>");
			sb.append("<toDelete>true</toDelete>");
			sb.append("</note></ser:deleteNote>"); 
			StringBuffer serviceResult = taylorServices.savingNotes(sb.toString());
			if(serviceResult!=null){
				notesDetails = serviceResult.toString();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		if(usrNoteID.compareToIgnoreCase("USER")==0)
		{ 		
			fetchNotesUser();		
			
		}
		else
		{		
			fetchNotes();
		}
		return SUCCESS;
	}
		public String fetchNotes() throws Exception {
		
		//String jsonData = request.getParameter("jsonData");
		//JSONObject jsonObject = new JSONObject(jsonData);
		String noteDescription="";
		String createdOnDate="";
		String createdUserLogin="";
		String noteUniqueId="";
		String notesTitle="";
		String isDeleted="";
		createdBy = "";
		String jsonData = request.getParameter("jsonData");
		
		if(jsonData !=null){
		JSONObject jsonObject = new JSONObject(jsonData);
		if(jsonObject.get("entityId")!=null){ 
			entityId = jsonObject.get("entityId").toString();
		}
	
		if(jsonObject.get("entityType")!=null){
			entityType =jsonObject.get("entityType").toString();
			
			
		}
	
		//String applnCode = jsonObject.get("applnCode").toString();
		if(jsonObject.get("flag")!=null){
			resultFlag=jsonObject.get("flag").toString();
		}
		if(jsonObject.has("userNoteID"))
		{
			if(jsonObject.get("userNoteID")!=null){
				usrNoteID=jsonObject.get("userNoteID").toString();
			}else
			{
				usrNoteID = "STAG"; 
			}
		}
		else
		{
			usrNoteID = "STAG"; 
		}
		}
		
		TaylorServices taylorServices = new TaylorServices();
		StringBuffer sb = new StringBuffer();
		sb.append("<ser:fetchNote>");
		sb.append("<note> <applnCode>STAFA</applnCode>");
		sb.append(" <entityId>"+entityId+"</entityId>");
		sb.append(" <entityType>"+entityType+"</entityType>");
		sb.append("</note></ser:fetchNote>"); 
		StringBuffer serviceResult = taylorServices.savingNotes(sb.toString());
	try{
				if(serviceResult!=null){
					DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					InputSource is = new InputSource();
					is.setCharacterStream(new StringReader(serviceResult.toString()));
					Document doc = db.parse(is);
					NodeList nodes = doc.getElementsByTagName("note");
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						ArrayList subArraylst = new ArrayList(); 
						
						NodeList noteDescrip = element.getElementsByTagName("notedescription");
						Element noteDesc = (Element) noteDescrip.item(0);
						noteDescription = WebserviceUtil.getCharacterDataFromElement(noteDesc);
						
						
						NodeList createdOn = element.getElementsByTagName("createdOn");
						Element createdEle = (Element) createdOn.item(0);
						createdOnDate = WebserviceUtil.getCharacterDataFromElement(createdEle);
						
						NodeList createdUser = element.getElementsByTagName("displayUserName");
						Element createUserName = (Element) createdUser.item(0);
						createdUserLogin = WebserviceUtil.getCharacterDataFromElement(createUserName);
						
						
						NodeList noteName = element.getElementsByTagName("notesName");
						Element notesName = (Element) noteName.item(0);
						notesTitle= WebserviceUtil.getCharacterDataFromElement(notesName);
						
						
						NodeList noteId = element.getElementsByTagName("noteUniqueId");
						Element noteIdElement = (Element) noteId.item(0);
						noteUniqueId= WebserviceUtil.getCharacterDataFromElement(noteIdElement);
						
						
						NodeList deletedNote = element.getElementsByTagName("isDeleted");
						Element deletedNoteElement = (Element) deletedNote.item(0);
						isDeleted= WebserviceUtil.getCharacterDataFromElement(deletedNoteElement );
						
						
						//<noteUniqueId>?</noteUniqueId>
						
						subArraylst.add(0,createdUserLogin);
						subArraylst.add(1,createdOnDate);
						subArraylst.add(2,noteDescription);
						subArraylst.add(3,notesTitle);
						subArraylst.add(4,noteUniqueId);
						subArraylst.add(5,isDeleted);
						notesList.add(i, subArraylst);
					}
					setNotesList(notesList);
					//noteList.add(serviceResult);
				}
	}catch (Exception e) {
		e.printStackTrace();
	}
	//	request.setAttribute("ListNotes", noteList)
		
		if(resultFlag.equalsIgnoreCase("0")){
			return "success1";
		}else{
			return SUCCESS;
		}
		
	}
	

	//	
		public String fetchNotesUser() throws Exception {
			
			//String jsonData = request.getParameter("jsonData");
			//JSONObject jsonObject = new JSONObject(jsonData);
			String noteDescription="";
			String createdOnDate="";
			String createdUserLogin="";
			String noteUniqueId="";
			String notesTitle="";
			String isDeleted="";
			createdBy = "";
			String jsonData = request.getParameter("jsonData");
			
			if(jsonData !=null){
			JSONObject jsonObject = new JSONObject(jsonData);
			if(jsonObject.get("entityId")!=null){ 
				entityId = jsonObject.get("entityId").toString();
			}
			
//			if(jsonObject.get("createdBy")!=null){ 
//				createdBy = jsonObject.get("createdBy").toString();
//				}
			//
			
			
			if(jsonObject.get("entityType")!=null){
				entityType =jsonObject.get("entityType").toString();
				
				
			}
			
			//String applnCode = jsonObject.get("applnCode").toString();
			if(jsonObject.get("flag")!=null){
				resultFlag=jsonObject.get("flag").toString();
			}
			if(jsonObject.has("userNoteID"))
			{
				if(jsonObject.get("userNoteID")!=null){
					usrNoteID=jsonObject.get("userNoteID").toString();
				}
				else
				{
					usrNoteID = "USER"; 
				}
			}
			else
			{
				usrNoteID = "USER"; 
			}
			
			
			}
			
			TaylorServices taylorServices = new TaylorServices();
			StringBuffer sb = new StringBuffer();
			sb.append("<ser:fetchNote>");
			sb.append("<note> <applnCode>STAFA</applnCode>");
			sb.append(" <createdBy>"+entityId+"</createdBy>");
			sb.append(" <entityType>"+entityType+"</entityType>");
			sb.append("</note></ser:fetchNote>"); 
			StringBuffer serviceResult = taylorServices.savingNotes(sb.toString());
		try{
					if(serviceResult!=null){
						DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						InputSource is = new InputSource();
						is.setCharacterStream(new StringReader(serviceResult.toString()));
						Document doc = db.parse(is);
						NodeList nodes = doc.getElementsByTagName("note");
						for (int i = 0; i < nodes.getLength(); i++) {
							Element element = (Element) nodes.item(i);
							ArrayList subArraylst = new ArrayList(); 
							
							NodeList noteDescrip = element.getElementsByTagName("notedescription");
							Element noteDesc = (Element) noteDescrip.item(0);
							noteDescription = WebserviceUtil.getCharacterDataFromElement(noteDesc);
							
							
							NodeList createdOn = element.getElementsByTagName("createdOn");
							Element createdEle = (Element) createdOn.item(0);
							createdOnDate = WebserviceUtil.getCharacterDataFromElement(createdEle);
							
							
							NodeList createdUser = element.getElementsByTagName("displayUserName");
							Element createUserName = (Element) createdUser.item(0);
							createdUserLogin = WebserviceUtil.getCharacterDataFromElement(createUserName);
							
							
							NodeList noteName = element.getElementsByTagName("notesName");
							Element notesName = (Element) noteName.item(0);
							notesTitle= WebserviceUtil.getCharacterDataFromElement(notesName);
							
							
							NodeList noteId = element.getElementsByTagName("noteUniqueId");
							Element noteIdElement = (Element) noteId.item(0);
							noteUniqueId= WebserviceUtil.getCharacterDataFromElement(noteIdElement);
							
							
							NodeList deletedNote = element.getElementsByTagName("isDeleted");
							Element deletedNoteElement = (Element) deletedNote.item(0);
							isDeleted= WebserviceUtil.getCharacterDataFromElement(deletedNoteElement );
							
							
							//<noteUniqueId>?</noteUniqueId>
							
							subArraylst.add(0,createdUserLogin);
							subArraylst.add(1,createdOnDate);
							subArraylst.add(2,noteDescription);
							subArraylst.add(3,notesTitle);
							subArraylst.add(4,noteUniqueId);
							subArraylst.add(5,isDeleted);
							notesList.add(i, subArraylst);
						}
						setNotesList(notesList);
						//noteList.add(serviceResult);
					}
		}catch (Exception e) {
			e.printStackTrace();
		}
		//	request.setAttribute("ListNotes", noteList)
			
			if(resultFlag.equalsIgnoreCase("0")){
				return "success1";
			}else{
				return SUCCESS;
			}
			
		}	
	//
		
		public String isNotesAdded(String fileId) throws Exception {
			
			String jsonData = request.getParameter("jsonData");
				
			
			TaylorServices taylorServices = new TaylorServices();
			StringBuffer sb = new StringBuffer();
			sb.append("<ser:fetchNote>");
			sb.append("<note> <applnCode>STAFA</applnCode>");
			sb.append(" <entityId>"+fileId+"</entityId>");
			sb.append(" <entityType>"+"Staging"+"</entityType>");
			sb.append("</note></ser:fetchNote>"); 
			StringBuffer serviceResult = taylorServices.savingNotes(sb.toString());
		try{
					if(serviceResult!=null)
					{
						DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						InputSource is = new InputSource();
						is.setCharacterStream(new StringReader(serviceResult.toString()));
						Document doc = db.parse(is);
						NodeList nodes = doc.getElementsByTagName("note");
						if(nodes.getLength() > 0) 
						{
							
							return "SUCCESS";
						}
						else
						{
							
							return "FAIL";
						}
						
					}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return "FAIL";
		}	
	
		
	//
	public String getModel(){
		return SUCCESS;
	}
	
	// Getters and setters

	public String getNotesDetails() {
		return notesDetails;
	}
	public void setNotesDetails(String notesDetails) {
		this.notesDetails = notesDetails;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public ArrayList getNotesList() {
		return notesList;
	}

	public void setNotesList(ArrayList notesList) {
		this.notesList = notesList;
	}
	
	public Map<String,String> fetchLatestNote(List entityList) throws Exception {
		//log.debug("fetchPlan.fetchStorageDetails Method Start *******1************ ");
		
		Map<String,String> entityIDTypeNote = new HashMap<String, String>();
		String noteDescription="";
		String createdOnDate="";
		String createdUserLogin="";
		String noteUniqueId="";
		String noteEntityId="";
		String notesTitle="";
		String isDeleted="";
		try{
			
		TaylorServices taylorServices = new TaylorServices();
		StringBuffer sb = new StringBuffer();
		sb.append("<ser:fetchLatestNote>");
		sb.append("<note> <applnCode>STAFA</applnCode>");
		for(Object entityId:entityList){
		sb.append(" <entityId>"+entityId+"</entityId>");
		}
		sb.append(" <entityType>"+"Staging"+"</entityType>");
		sb.append("</note></ser:fetchLatestNote>"); 
		
		StringBuffer serviceResult = taylorServices.savingNotes(sb.toString());

		if(serviceResult!=null){
					DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					InputSource is = new InputSource();
					is.setCharacterStream(new StringReader(serviceResult.toString()));
					Document doc = db.parse(is);
					NodeList nodes = doc.getElementsByTagName("note");
			
					
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);	
						
						NodeList noteDescrip = element.getElementsByTagName("notedescription");
						Element noteDesc = (Element) noteDescrip.item(0);
						noteDescription = WebserviceUtil.getCharacterDataFromElement(noteDesc);
						//log.debug(WebserviceUtil.getCharacterDataFromElement(noteDesc));
						/*
						NodeList createdOn = element.getElementsByTagName("createdOn");
						Element createdEle = (Element) createdOn.item(0);
						createdOnDate = WebserviceUtil.getCharacterDataFromElement(createdEle);
						log.debug(WebserviceUtil.getCharacterDataFromElement(createdEle));
						
						NodeList noteId = element.getElementsByTagName("noteUniqueId");
						Element noteIdElement = (Element) noteId.item(0);
						noteUniqueId= WebserviceUtil.getCharacterDataFromElement(noteIdElement);
						log.debug(WebserviceUtil.getCharacterDataFromElement(noteIdElement)); 
						*/

						NodeList createdUser = element.getElementsByTagName("displayUserName");
						Element createUserName = (Element) createdUser.item(0);
						createdUserLogin = WebserviceUtil.getCharacterDataFromElement(createUserName);
						//log.debug(WebserviceUtil.getCharacterDataFromElement(createUserName));
						
						NodeList noteName = element.getElementsByTagName("notesName");
						Element notesName = (Element) noteName.item(0);
						notesTitle= WebserviceUtil.getCharacterDataFromElement(notesName);
						//log.debug(WebserviceUtil.getCharacterDataFromElement(notesName));
						
						NodeList entityId = element.getElementsByTagName("entityId");
						Element entityIdElement = (Element) entityId.item(0);
						noteEntityId= WebserviceUtil.getCharacterDataFromElement(entityIdElement);
						//log.debug(WebserviceUtil.getCharacterDataFromElement(entityIdElement));
						
						
						NodeList deletedNote = element.getElementsByTagName("isDeleted");
						Element deletedNoteElement = (Element) deletedNote.item(0);
						isDeleted= WebserviceUtil.getCharacterDataFromElement(deletedNoteElement );
						//log.debug(WebserviceUtil.getCharacterDataFromElement(deletedNoteElement ));
						
						//<noteUniqueId>?</noteUniqueId>
						/*
						subArraylst.add(0,createdUserLogin);
						subArraylst.add(1,createdOnDate);
						subArraylst.add(2,noteDescription);
						subArraylst.add(3,notesTitle);
						subArraylst.add(4,noteUniqueId);
						subArraylst.add(5,isDeleted);*/
						String completePlanStr=notesTitle+" : "+noteDescription+" : "+createdUserLogin;
						completePlanStr=completePlanStr.replaceAll("<br>", " ");
						completePlanStr=completePlanStr.replaceAll("</P>", " ");
						entityIDTypeNote.put(noteEntityId,completePlanStr );
						
					}
					
					
				}
		}
		catch(Exception exception){
			//log.error(exception.getMessage());	
		}
		return entityIDTypeNote;
	}
	

}
