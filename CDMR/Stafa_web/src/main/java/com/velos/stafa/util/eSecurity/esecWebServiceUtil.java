package com.velos.stafa.util.eSecurity;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.business.util.WebserviceUtil;
import com.velos.stafa.objects.eSecurity.EsecurityUserDetailsObject;
import com.velos.stafa.util.VelosStafaConstants;


/**
 * @author vlakshmipriya
 *
 */

public class esecWebServiceUtil {
	
	public static EsecurityUserDetailsObject getXmlValueObject(StringBuffer xmlRecords,String resultNodeName){
		
		EsecurityUserDetailsObject esecUserObject = new EsecurityUserDetailsObject(); 
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(resultNodeName);

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList firstName = element.getElementsByTagName(ESecurityConstants.firstName);
				Element firstNameEle = (Element) firstName.item(0);
				esecUserObject.setFirstName(WebserviceUtil.getCharacterDataFromElement(firstNameEle));
				
				NodeList lastName = element.getElementsByTagName(ESecurityConstants.lastName);
				Element lastNameEle = (Element) lastName.item(0);
				esecUserObject.setLastName(WebserviceUtil.getCharacterDataFromElement(lastNameEle));
				
				NodeList addr1 = element.getElementsByTagName(ESecurityConstants.addr1);
				Element addrEle1 = (Element) addr1.item(0);
				esecUserObject.setAddr1(WebserviceUtil.getCharacterDataFromElement(addrEle1));
				
				NodeList addr2 = element.getElementsByTagName(ESecurityConstants.addr2);
				Element addrEle2 = (Element) addr2.item(0);
				esecUserObject.setAddr2(WebserviceUtil.getCharacterDataFromElement(addrEle2));
				
				NodeList userId = element.getElementsByTagName(ESecurityConstants.userId);
				Element userIdEle = (Element) userId.item(0);
				esecUserObject.setUserId(WebserviceUtil.getCharacterDataFromElement(userIdEle));
				
				NodeList city = element.getElementsByTagName(ESecurityConstants.city);
				Element cityEle = (Element) city.item(0);
				esecUserObject.setCity(WebserviceUtil.getCharacterDataFromElement(cityEle));
				
				NodeList email = element.getElementsByTagName(ESecurityConstants.email);
				Element emailEle = (Element) email.item(0);
				esecUserObject.setEmail(WebserviceUtil.getCharacterDataFromElement(emailEle));
				
				NodeList state = element.getElementsByTagName(ESecurityConstants.state);
				Element stateEle = (Element) state.item(0);
				esecUserObject.setState(WebserviceUtil.getCharacterDataFromElement(stateEle));
				
				NodeList phone = element.getElementsByTagName(ESecurityConstants.phone);
				Element phoneEle = (Element) phone.item(0);
				esecUserObject.setPhone(WebserviceUtil.getCharacterDataFromElement(phoneEle));
				
				NodeList postalCode= element.getElementsByTagName(ESecurityConstants.postalCode);
				Element postalCodeEle = (Element) postalCode.item(0);
				esecUserObject.setState(WebserviceUtil.getCharacterDataFromElement(postalCodeEle));
				
						
				NodeList country = element.getElementsByTagName(ESecurityConstants.country);
				Element countryEle = (Element) country.item(0);
				esecUserObject.setCountry(WebserviceUtil.getCharacterDataFromElement(countryEle));
				
				NodeList timeZone = element.getElementsByTagName(ESecurityConstants.timeZone);
				Element timeZoneEle = (Element) timeZone.item(0);
				esecUserObject.setTimeZone(WebserviceUtil.getCharacterDataFromElement(timeZoneEle));
				
				NodeList passwordExp = element.getElementsByTagName(ESecurityConstants.passwordExpDate);
				Element passwordExpEle = (Element) passwordExp.item(0);
				esecUserObject.setPasswordExpDate(WebserviceUtil.getCharacterDataFromElement(passwordExpEle));
				
				NodeList loginName = element.getElementsByTagName(ESecurityConstants.login);
				Element loginNameEle = (Element) loginName.item(0);
				esecUserObject.setLoginName(WebserviceUtil.getCharacterDataFromElement(loginNameEle));
				
				NodeList clientUserId = element.getElementsByTagName(ESecurityConstants.clientUserId);
				Element clientUserIdEle = (Element) clientUserId.item(0);
				esecUserObject.setClientUserId(WebserviceUtil.getCharacterDataFromElement(clientUserIdEle));
				
				NodeList statusId = element.getElementsByTagName(ESecurityConstants.status);
				Element statusIdEle = (Element) statusId.item(0);
				esecUserObject.setAcctIsActive(WebserviceUtil.getCharacterDataFromElement(statusIdEle));
				
				NodeList userIsActiveId = element.getElementsByTagName(ESecurityConstants.userIsActive);
				Element userIsActiveEle = (Element) userIsActiveId.item(0);
				esecUserObject.setUserIsActive(WebserviceUtil.getCharacterDataFromElement(userIsActiveEle));
				
				NodeList accExpDateId = element.getElementsByTagName(ESecurityConstants.accExpDate);
				Element accExpDateEle = (Element) accExpDateId.item(0);
				esecUserObject.setAcctexpDate(WebserviceUtil.getCharacterDataFromElement(accExpDateEle));
				
				NodeList jobType = element.getElementsByTagName(ESecurityConstants.jobType);
				Element jobTypeEle = (Element) jobType.item(0);
				esecUserObject.setJobType(WebserviceUtil.getCharacterDataFromElement(jobTypeEle));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return esecUserObject;
	}
	
	public static ArrayList getXmlRoleValue(StringBuffer xmlRecords,String resultNodeName){
		
		ArrayList arraylst = new ArrayList(); 
		
		String appRights = "";
		String moduleName = "";
		String rights = "";
		String treeSeq = "";
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(resultNodeName);

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				ArrayList subarraylst = new ArrayList(); 
				NodeList apprights = element.getElementsByTagName(ESecurityConstants.appRights);
				Element appRightsEle = (Element) apprights.item(0);
				appRights = WebserviceUtil.getCharacterDataFromElement(appRightsEle);
				
				NodeList modulename = element.getElementsByTagName(ESecurityConstants.moduleName);
				Element moduleNameEle = (Element) modulename.item(0);
				moduleName = WebserviceUtil.getCharacterDataFromElement(moduleNameEle);
				
				NodeList rightss = element.getElementsByTagName(ESecurityConstants.rights);
				Element rightsEle = (Element) rightss.item(0);
				rights = WebserviceUtil.getCharacterDataFromElement(rightsEle);
				
				NodeList treeSeqs = element.getElementsByTagName(ESecurityConstants.treeSeq);
				Element treeSeqEle = (Element) treeSeqs.item(0);
				treeSeq = WebserviceUtil.getCharacterDataFromElement(treeSeqEle);
				
				subarraylst.add(0, appRights);
				subarraylst.add(1, moduleName);
				subarraylst.add(2, rights);
				subarraylst.add(3, treeSeq);
				
				arraylst.add(i, subarraylst);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return arraylst;
	}
	
	public static ArrayList getXmlValueForESec(StringBuffer xmlRecords,String NodeName){
		ArrayList subarraylst = new ArrayList(); 
		String returnValue="";
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("result");
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				NodeList nodeName = element.getElementsByTagName(NodeName);
				Element nodeNameEle = (Element) nodeName.item(0);
				returnValue = WebserviceUtil.getCharacterDataFromElement(nodeNameEle);
				subarraylst.add(0, returnValue);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return subarraylst;
	}
	
	public static ArrayList getRoleValue(StringBuffer xmlRecords,String resultNodeName){
		
		ArrayList arraylst = new ArrayList(); 
		
		String roleName = "";
		String roleDescription = "";
		String roleCode = "";
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(resultNodeName);

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				ArrayList subarraylst = new ArrayList(); 
				NodeList roleName1 = element.getElementsByTagName(ESecurityConstants.roleName);
				Element roleNameEle = (Element) roleName1.item(0);
				roleName = WebserviceUtil.getCharacterDataFromElement(roleNameEle);
				
				NodeList roleDesc = element.getElementsByTagName(ESecurityConstants.roleDesc);
				Element roleDescEle = (Element) roleDesc.item(0);
				roleDescription = WebserviceUtil.getCharacterDataFromElement(roleDescEle);
					
				NodeList roleCode1 = element.getElementsByTagName(ESecurityConstants.roleCode);
				Element roleCodeEle = (Element) roleCode1.item(0);
				roleCode = WebserviceUtil.getCharacterDataFromElement(roleCodeEle);
					
				subarraylst.add(0, roleName);
				subarraylst.add(1, roleDescription);
				subarraylst.add(2,roleCode);
				
				arraylst.add(i, subarraylst);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return arraylst;
	}
	
	public static ArrayList getUserValue(StringBuffer xmlRecords,String resultNodeName){
		
		ArrayList arraylst = new ArrayList(); 
		
		String loginName = "";
		String groupNames = "";
		String name = "";
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(resultNodeName);

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				ArrayList subarraylst = new ArrayList(); 
				NodeList login = element.getElementsByTagName(ESecurityConstants.login);
				Element loginNameEle = (Element) login.item(0);
				loginName = WebserviceUtil.getCharacterDataFromElement(loginNameEle);
				
				NodeList roleName = element.getElementsByTagName(ESecurityConstants.roleNames);
				Element roleNameEle = (Element) roleName.item(0);
				groupNames = WebserviceUtil.getCharacterDataFromElement(roleNameEle);
				groupNames = groupNames.replaceAll(",", ", ");
					
				NodeList firstNameNode = element.getElementsByTagName(ESecurityConstants.firstName);
				Element firstNameEle = (Element) firstNameNode.item(0);
				NodeList lastNameNode = element.getElementsByTagName(ESecurityConstants.lastName);
				Element lastNameEle = (Element) lastNameNode.item(0);
				String firstName = WebserviceUtil.getCharacterDataFromElement(firstNameEle);
				String lastName = WebserviceUtil.getCharacterDataFromElement(lastNameEle);
				if(!lastName.equalsIgnoreCase("")){
					name =  lastName + ", " + firstName;
				}else{
					name = firstName;					
				}
					
				subarraylst.add(0, loginName);
				subarraylst.add(1, groupNames);
				subarraylst.add(2, name);
				
				arraylst.add(i, subarraylst);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return arraylst;
	}
	
	public static ArrayList getPolicyValueObject(StringBuffer xmlRecords,String resultNodeName){
		
		ArrayList arraylst = new ArrayList(); 
		String code = "";
		String description = "";
		String field = "";
		String sequence = "";
		String value = "";
		String comboValue = "";
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(resultNodeName);

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				ArrayList subarraylst = new ArrayList(); 
				NodeList code1 = element.getElementsByTagName(ESecurityConstants.code);
				Element codeEle = (Element) code1.item(0);
				code = WebserviceUtil.getCharacterDataFromElement(codeEle);
				
				NodeList desc = element.getElementsByTagName(ESecurityConstants.policyDescription);
				Element descriptionEle = (Element) desc.item(0);
				description = WebserviceUtil.getCharacterDataFromElement(descriptionEle);
				
				NodeList field1 = element.getElementsByTagName(ESecurityConstants.field);
				Element fieldEle = (Element) field1.item(0);
				field = WebserviceUtil.getCharacterDataFromElement(fieldEle);
				
				NodeList sequences = element.getElementsByTagName(ESecurityConstants.sequence);
				Element sequenceEle = (Element) sequences.item(0);
				sequence = WebserviceUtil.getCharacterDataFromElement(sequenceEle);
				
				NodeList value1 = element.getElementsByTagName(ESecurityConstants.value);
				Element valueEle = (Element) value1.item(0);
				value = WebserviceUtil.getCharacterDataFromElement(valueEle);
				
				NodeList comboValue1 = element.getElementsByTagName(ESecurityConstants.comboValue);
				Element comboValueEle = (Element) comboValue1.item(0);
				comboValue = WebserviceUtil.getCharacterDataFromElement(comboValueEle);
				
				subarraylst.add(0, code);
				subarraylst.add(1, description);
				subarraylst.add(2, field);
				subarraylst.add(3, sequence);
				subarraylst.add(4, value);
				subarraylst.add(5,comboValue);
				
				arraylst.add(i, subarraylst);
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return arraylst;
	}
    /*private static String encryptaString(SecretKey key, String strToEncrypt) {
    	//log.debug("LoginAction : encryptaString Method Start ");
    	try {
    		Cipher encrypter = Cipher.getInstance("DES");
            encrypter.init(Cipher.ENCRYPT_MODE, key);
           
            byte[] utf8 = strToEncrypt.getBytes("UTF8");

            byte[] enc = encrypter.doFinal(utf8);
            
            return new sun.misc.BASE64Encoder().encode(enc);
            
        }catch(Exception e) {
            e.printStackTrace();
        }
       // log.debug("LoginAction : encryptaString Method End ");
        return null;
    }*/
    
    public static SecretKey readKey() {
		//log.debug("LoginAction : readKey Method Start ");
        SecretKey myKey = null;
        try {
           /* FileInputStream keyfis = new FileInputStream("/home/kbalaji/mypvtKey.txt");
            byte[] encKey = new byte[keyfis.available()];
            keyfis.read(encKey);
            keyfis.close();
            
           */
        	ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
        	String encryptKey = resource.getString(VelosStafaConstants.ENCRYPTKEY);
        	 byte encKey[] = new byte[encryptKey.length()];
             for(int j = 0; j < encryptKey.length(); j++)
             {
            	 encKey[j] = (byte)encryptKey.charAt(j);
             }
        	
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
            DESKeySpec keySpec = new DESKeySpec(encKey);
            myKey = factory.generateSecret(keySpec);
           
        }catch(Exception e) {
            e.printStackTrace();
        }
        //log.debug("LoginAction : readKey Method End ");
        return myKey;
    }
}
